﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResidenceBuildingsTableLine
struct ResidenceBuildingsTableLine_t325800007;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceBuildingsTableController
struct  ResidenceBuildingsTableController_t3217399565  : public MonoBehaviour_t1158329972
{
public:
	// ResidenceBuildingsTableLine ResidenceBuildingsTableController::m_cellPrefab
	ResidenceBuildingsTableLine_t325800007 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceBuildingsTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Boolean ResidenceBuildingsTableController::buildings
	bool ___buildings_4;
	// System.Collections.ArrayList ResidenceBuildingsTableController::buildingsList
	ArrayList_t4252133567 * ___buildingsList_5;
	// System.Int32 ResidenceBuildingsTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t3217399565, ___m_cellPrefab_2)); }
	inline ResidenceBuildingsTableLine_t325800007 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceBuildingsTableLine_t325800007 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceBuildingsTableLine_t325800007 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t3217399565, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_buildings_4() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t3217399565, ___buildings_4)); }
	inline bool get_buildings_4() const { return ___buildings_4; }
	inline bool* get_address_of_buildings_4() { return &___buildings_4; }
	inline void set_buildings_4(bool value)
	{
		___buildings_4 = value;
	}

	inline static int32_t get_offset_of_buildingsList_5() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t3217399565, ___buildingsList_5)); }
	inline ArrayList_t4252133567 * get_buildingsList_5() const { return ___buildingsList_5; }
	inline ArrayList_t4252133567 ** get_address_of_buildingsList_5() { return &___buildingsList_5; }
	inline void set_buildingsList_5(ArrayList_t4252133567 * value)
	{
		___buildingsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsList_5, value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t3217399565, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
