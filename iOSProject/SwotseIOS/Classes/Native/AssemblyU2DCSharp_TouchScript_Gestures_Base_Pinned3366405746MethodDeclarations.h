﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.Base.PinnedTrasformGestureBase
struct PinnedTrasformGestureBase_t3366405746;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Base_PinnedT724632304.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::.ctor()
extern "C"  void PinnedTrasformGestureBase__ctor_m1057090794 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_add_TransformStarted_m3491581829 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_remove_TransformStarted_m4154259438 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_add_Transformed_m4180096641 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_remove_Transformed_m2007403830 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_add_TransformCompleted_m1846806801 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTrasformGestureBase_remove_TransformCompleted_m607197730 (PinnedTrasformGestureBase_t3366405746 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_Type()
extern "C"  int32_t PinnedTrasformGestureBase_get_Type_m359926350 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::set_Type(TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType)
extern "C"  void PinnedTrasformGestureBase_set_Type_m2663565885 (PinnedTrasformGestureBase_t3366405746 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_ScreenTransformThreshold()
extern "C"  float PinnedTrasformGestureBase_get_ScreenTransformThreshold_m2258012236 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::set_ScreenTransformThreshold(System.Single)
extern "C"  void PinnedTrasformGestureBase_set_ScreenTransformThreshold_m3606402153 (PinnedTrasformGestureBase_t3366405746 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_DeltaRotation()
extern "C"  float PinnedTrasformGestureBase_get_DeltaRotation_m97274101 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_DeltaScale()
extern "C"  float PinnedTrasformGestureBase_get_DeltaScale_m567490273 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_ScreenPosition()
extern "C"  Vector2_t2243707579  PinnedTrasformGestureBase_get_ScreenPosition_m897277329 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::get_PreviousScreenPosition()
extern "C"  Vector2_t2243707579  PinnedTrasformGestureBase_get_PreviousScreenPosition_m1186552550 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::OnEnable()
extern "C"  void PinnedTrasformGestureBase_OnEnable_m1923088718 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void PinnedTrasformGestureBase_touchesBegan_m1930637858 (PinnedTrasformGestureBase_t3366405746 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void PinnedTrasformGestureBase_touchesEnded_m4128921977 (PinnedTrasformGestureBase_t3366405746 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::onBegan()
extern "C"  void PinnedTrasformGestureBase_onBegan_m417525820 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::onChanged()
extern "C"  void PinnedTrasformGestureBase_onChanged_m1614839277 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::onRecognized()
extern "C"  void PinnedTrasformGestureBase_onRecognized_m1828593591 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::reset()
extern "C"  void PinnedTrasformGestureBase_reset_m1598815463 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Base.PinnedTrasformGestureBase::relevantTouches(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  bool PinnedTrasformGestureBase_relevantTouches_m1943605226 (PinnedTrasformGestureBase_t3366405746 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::getPointScreenPosition()
extern "C"  Vector2_t2243707579  PinnedTrasformGestureBase_getPointScreenPosition_m2625665832 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::getPointPreviousScreenPosition()
extern "C"  Vector2_t2243707579  PinnedTrasformGestureBase_getPointPreviousScreenPosition_m1404461715 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.PinnedTrasformGestureBase::updateScreenTransformThreshold()
extern "C"  void PinnedTrasformGestureBase_updateScreenTransformThreshold_m2065792762 (PinnedTrasformGestureBase_t3366405746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
