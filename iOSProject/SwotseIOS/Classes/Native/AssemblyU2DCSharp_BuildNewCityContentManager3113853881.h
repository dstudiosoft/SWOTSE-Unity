﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;
// ResourcesModel
struct ResourcesModel_t2978985958;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildNewCityContentManager
struct  BuildNewCityContentManager_t3113853881  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField BuildNewCityContentManager::cityName
	InputField_t1631627530 * ___cityName_2;
	// System.Int64 BuildNewCityContentManager::posX
	int64_t ___posX_3;
	// System.Int64 BuildNewCityContentManager::posY
	int64_t ___posY_4;
	// UnityEngine.UI.Text BuildNewCityContentManager::foodPresent
	Text_t356221433 * ___foodPresent_5;
	// UnityEngine.UI.Text BuildNewCityContentManager::woodPresent
	Text_t356221433 * ___woodPresent_6;
	// UnityEngine.UI.Text BuildNewCityContentManager::stonePresent
	Text_t356221433 * ___stonePresent_7;
	// UnityEngine.UI.Text BuildNewCityContentManager::ironPresent
	Text_t356221433 * ___ironPresent_8;
	// UnityEngine.UI.Text BuildNewCityContentManager::copperPresent
	Text_t356221433 * ___copperPresent_9;
	// UnityEngine.UI.Text BuildNewCityContentManager::silverPresent
	Text_t356221433 * ___silverPresent_10;
	// UnityEngine.UI.Text BuildNewCityContentManager::goldPresent
	Text_t356221433 * ___goldPresent_11;
	// ResourcesModel BuildNewCityContentManager::cityResources
	ResourcesModel_t2978985958 * ___cityResources_12;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___cityName_2)); }
	inline InputField_t1631627530 * get_cityName_2() const { return ___cityName_2; }
	inline InputField_t1631627530 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(InputField_t1631627530 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_2, value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___posX_3)); }
	inline int64_t get_posX_3() const { return ___posX_3; }
	inline int64_t* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(int64_t value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___posY_4)); }
	inline int64_t get_posY_4() const { return ___posY_4; }
	inline int64_t* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(int64_t value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_foodPresent_5() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___foodPresent_5)); }
	inline Text_t356221433 * get_foodPresent_5() const { return ___foodPresent_5; }
	inline Text_t356221433 ** get_address_of_foodPresent_5() { return &___foodPresent_5; }
	inline void set_foodPresent_5(Text_t356221433 * value)
	{
		___foodPresent_5 = value;
		Il2CppCodeGenWriteBarrier(&___foodPresent_5, value);
	}

	inline static int32_t get_offset_of_woodPresent_6() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___woodPresent_6)); }
	inline Text_t356221433 * get_woodPresent_6() const { return ___woodPresent_6; }
	inline Text_t356221433 ** get_address_of_woodPresent_6() { return &___woodPresent_6; }
	inline void set_woodPresent_6(Text_t356221433 * value)
	{
		___woodPresent_6 = value;
		Il2CppCodeGenWriteBarrier(&___woodPresent_6, value);
	}

	inline static int32_t get_offset_of_stonePresent_7() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___stonePresent_7)); }
	inline Text_t356221433 * get_stonePresent_7() const { return ___stonePresent_7; }
	inline Text_t356221433 ** get_address_of_stonePresent_7() { return &___stonePresent_7; }
	inline void set_stonePresent_7(Text_t356221433 * value)
	{
		___stonePresent_7 = value;
		Il2CppCodeGenWriteBarrier(&___stonePresent_7, value);
	}

	inline static int32_t get_offset_of_ironPresent_8() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___ironPresent_8)); }
	inline Text_t356221433 * get_ironPresent_8() const { return ___ironPresent_8; }
	inline Text_t356221433 ** get_address_of_ironPresent_8() { return &___ironPresent_8; }
	inline void set_ironPresent_8(Text_t356221433 * value)
	{
		___ironPresent_8 = value;
		Il2CppCodeGenWriteBarrier(&___ironPresent_8, value);
	}

	inline static int32_t get_offset_of_copperPresent_9() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___copperPresent_9)); }
	inline Text_t356221433 * get_copperPresent_9() const { return ___copperPresent_9; }
	inline Text_t356221433 ** get_address_of_copperPresent_9() { return &___copperPresent_9; }
	inline void set_copperPresent_9(Text_t356221433 * value)
	{
		___copperPresent_9 = value;
		Il2CppCodeGenWriteBarrier(&___copperPresent_9, value);
	}

	inline static int32_t get_offset_of_silverPresent_10() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___silverPresent_10)); }
	inline Text_t356221433 * get_silverPresent_10() const { return ___silverPresent_10; }
	inline Text_t356221433 ** get_address_of_silverPresent_10() { return &___silverPresent_10; }
	inline void set_silverPresent_10(Text_t356221433 * value)
	{
		___silverPresent_10 = value;
		Il2CppCodeGenWriteBarrier(&___silverPresent_10, value);
	}

	inline static int32_t get_offset_of_goldPresent_11() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___goldPresent_11)); }
	inline Text_t356221433 * get_goldPresent_11() const { return ___goldPresent_11; }
	inline Text_t356221433 ** get_address_of_goldPresent_11() { return &___goldPresent_11; }
	inline void set_goldPresent_11(Text_t356221433 * value)
	{
		___goldPresent_11 = value;
		Il2CppCodeGenWriteBarrier(&___goldPresent_11, value);
	}

	inline static int32_t get_offset_of_cityResources_12() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t3113853881, ___cityResources_12)); }
	inline ResourcesModel_t2978985958 * get_cityResources_12() const { return ___cityResources_12; }
	inline ResourcesModel_t2978985958 ** get_address_of_cityResources_12() { return &___cityResources_12; }
	inline void set_cityResources_12(ResourcesModel_t2978985958 * value)
	{
		___cityResources_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityResources_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
