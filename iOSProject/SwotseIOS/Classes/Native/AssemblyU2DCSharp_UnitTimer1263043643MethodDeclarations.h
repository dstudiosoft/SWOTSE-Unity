﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitTimer
struct UnitTimer_t1263043643;

#include "codegen/il2cpp-codegen.h"

// System.Void UnitTimer::.ctor()
extern "C"  void UnitTimer__ctor_m4202776174 (UnitTimer_t1263043643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
