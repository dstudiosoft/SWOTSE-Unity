﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageContentManager
struct MessageContentManager_t1655935879;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MessageContentManager::.ctor()
extern "C"  void MessageContentManager__ctor_m793834166 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::add_gotMessageDetails(SimpleEvent)
extern "C"  void MessageContentManager_add_gotMessageDetails_m858561891 (MessageContentManager_t1655935879 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::remove_gotMessageDetails(SimpleEvent)
extern "C"  void MessageContentManager_remove_gotMessageDetails_m2813872542 (MessageContentManager_t1655935879 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::OnEnable()
extern "C"  void MessageContentManager_OnEnable_m749254242 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::Reply()
extern "C"  void MessageContentManager_Reply_m3562861522 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::SendUserMessage()
extern "C"  void MessageContentManager_SendUserMessage_m2799473318 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::OnMessageSent(System.Object,System.String)
extern "C"  void MessageContentManager_OnMessageSent_m236241584 (MessageContentManager_t1655935879 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::DeleteMessage()
extern "C"  void MessageContentManager_DeleteMessage_m580379636 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::AcceptCityDeed()
extern "C"  void MessageContentManager_AcceptCityDeed_m633791097 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::RejectCityDeed()
extern "C"  void MessageContentManager_RejectCityDeed_m4224499954 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MessageContentManager::GetMesssageDetails()
extern "C"  Il2CppObject * MessageContentManager_GetMesssageDetails_m663641366 (MessageContentManager_t1655935879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager::OnGetMessageDetails(System.Object,System.String)
extern "C"  void MessageContentManager_OnGetMessageDetails_m3598697274 (MessageContentManager_t1655935879 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
