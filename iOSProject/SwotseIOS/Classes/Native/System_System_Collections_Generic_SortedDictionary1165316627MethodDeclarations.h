﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3931121312MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::.ctor()
#define SortedDictionary_2__ctor_m3004925980(__this, method) ((  void (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2__ctor_m2646846914_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m3114245941(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t1165316627 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m2297982677_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1175936249(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t1165316627 *, KeyValuePair_2_t1568017256 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1627652201_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1509059671(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, KeyValuePair_2_t1568017256 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1375072327_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3116153736(__this, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1154929448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m152133110(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, KeyValuePair_2_t1568017256 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1895620934_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Add_m4067565480(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1435283656_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.Contains(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m3313074524(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m2099850172_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.GetEnumerator()
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2324384853(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1167000725_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.get_IsFixedSize()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1364021057(__this, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m463095569_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3888797654(__this, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3722589254_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.get_Keys()
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m230167416(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2424577816_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.Remove(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m2588832449(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m3973233665_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.get_Values()
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m1556497922(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m4006702514_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m2874122706(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m749784322_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m3483811317(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2214150629_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m2223774574(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2402063550_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.get_IsSynchronized()
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m3401960026(__this, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1383457962_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.get_SyncRoot()
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m86967790(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2986761918_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.IEnumerable.GetEnumerator()
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m171775959(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m50684391_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3593570128(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1660205152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::get_Count()
#define SortedDictionary_2_get_Count_m3532127716(__this, method) ((  int32_t (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_get_Count_m738745858_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::get_Item(TKey)
#define SortedDictionary_2_get_Item_m2203779861(__this, ___key0, method) ((  LocalizedObject_t1895892772 * (*) (SortedDictionary_2_t1165316627 *, String_t*, const MethodInfo*))SortedDictionary_2_get_Item_m2496989701_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::set_Item(TKey,TValue)
#define SortedDictionary_2_set_Item_m1800458703(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, String_t*, LocalizedObject_t1895892772 *, const MethodInfo*))SortedDictionary_2_set_Item_m2198693116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::get_Keys()
#define SortedDictionary_2_get_Keys_m3215607387(__this, method) ((  KeyCollection_t1379462240 * (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_get_Keys_m1481998532_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::Add(TKey,TValue)
#define SortedDictionary_2_Add_m2800144340(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, String_t*, LocalizedObject_t1895892772 *, const MethodInfo*))SortedDictionary_2_Add_m3261466727_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::Clear()
#define SortedDictionary_2_Clear_m1979248023(__this, method) ((  void (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_Clear_m2350355463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::ContainsKey(TKey)
#define SortedDictionary_2_ContainsKey_m695963799(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, String_t*, const MethodInfo*))SortedDictionary_2_ContainsKey_m2379881333_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::ContainsValue(TValue)
#define SortedDictionary_2_ContainsValue_m3350189133(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, LocalizedObject_t1895892772 *, const MethodInfo*))SortedDictionary_2_ContainsValue_m3842886989_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedDictionary_2_CopyTo_m160706664(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t1165316627 *, KeyValuePair_2U5BU5D_t717459961*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m2987881320_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m2288119845(__this, method) ((  Enumerator_t2533878127  (*) (SortedDictionary_2_t1165316627 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m3792996474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::Remove(TKey)
#define SortedDictionary_2_Remove_m2128538055(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, String_t*, const MethodInfo*))SortedDictionary_2_Remove_m2920055847_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::TryGetValue(TKey,TValue&)
#define SortedDictionary_2_TryGetValue_m1826328659(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t1165316627 *, String_t*, LocalizedObject_t1895892772 **, const MethodInfo*))SortedDictionary_2_TryGetValue_m3440732424_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::ToKey(System.Object)
#define SortedDictionary_2_ToKey_m1725260742(__this, ___key0, method) ((  String_t* (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m874477334_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::ToValue(System.Object)
#define SortedDictionary_2_ToValue_m3678963718(__this, ___value0, method) ((  LocalizedObject_t1895892772 * (*) (SortedDictionary_2_t1165316627 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m1308966358_gshared)(__this, ___value0, method)
