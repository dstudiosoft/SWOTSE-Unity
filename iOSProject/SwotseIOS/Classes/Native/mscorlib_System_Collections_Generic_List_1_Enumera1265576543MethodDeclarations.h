﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m128169646(__this, ___l0, method) ((  void (*) (Enumerator_t1265576543 *, List_1_t1730846869 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3851382324(__this, method) ((  void (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4225379402(__this, method) ((  Il2CppObject * (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::Dispose()
#define Enumerator_Dispose_m699671637(__this, method) ((  void (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::VerifyState()
#define Enumerator_VerifyState_m3793158916(__this, method) ((  void (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::MoveNext()
#define Enumerator_MoveNext_m2307195931(__this, method) ((  bool (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SmartLocalization.SmartCultureInfo>::get_Current()
#define Enumerator_get_Current_m294296591(__this, method) ((  SmartCultureInfo_t2361725737 * (*) (Enumerator_t1265576543 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
