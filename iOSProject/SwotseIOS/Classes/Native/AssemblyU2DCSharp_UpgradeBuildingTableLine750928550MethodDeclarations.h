﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpgradeBuildingTableLine
struct UpgradeBuildingTableLine_t750928550;
// UpgradeBuildingTableController
struct UpgradeBuildingTableController_t49696016;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpgradeBuildingTableController49696016.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UpgradeBuildingTableLine::.ctor()
extern "C"  void UpgradeBuildingTableLine__ctor_m1496304333 (UpgradeBuildingTableLine_t750928550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::SetOwner(UpgradeBuildingTableController)
extern "C"  void UpgradeBuildingTableLine_SetOwner_m241338346 (UpgradeBuildingTableLine_t750928550 * __this, UpgradeBuildingTableController_t49696016 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::SetBuildingName(System.String)
extern "C"  void UpgradeBuildingTableLine_SetBuildingName_m791226968 (UpgradeBuildingTableLine_t750928550 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::SetLevel(System.Int64)
extern "C"  void UpgradeBuildingTableLine_SetLevel_m2111109531 (UpgradeBuildingTableLine_t750928550 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::SetId(System.Int64)
extern "C"  void UpgradeBuildingTableLine_SetId_m4169683176 (UpgradeBuildingTableLine_t750928550 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::SetIcon(System.String)
extern "C"  void UpgradeBuildingTableLine_SetIcon_m3208701194 (UpgradeBuildingTableLine_t750928550 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableLine::UpgradeBuilding()
extern "C"  void UpgradeBuildingTableLine_UpgradeBuilding_m1160547459 (UpgradeBuildingTableLine_t750928550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
