﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShopTableCell
struct ShopTableCell_t4262618216;
// IReloadable
struct IReloadable_t861412162;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ShopTableCell::.ctor()
extern "C"  void ShopTableCell__ctor_m1754552035 (ShopTableCell_t4262618216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetOwner(IReloadable)
extern "C"  void ShopTableCell_SetOwner_m360636068 (ShopTableCell_t4262618216 * __this, Il2CppObject * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetEvent(System.Int64)
extern "C"  void ShopTableCell_SetEvent_m1386244167 (ShopTableCell_t4262618216 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetType(System.String)
extern "C"  void ShopTableCell_SetType_m4022445503 (ShopTableCell_t4262618216 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetUsable(System.Boolean)
extern "C"  void ShopTableCell_SetUsable_m3823127198 (ShopTableCell_t4262618216 * __this, bool ___usable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetLabel1(System.String)
extern "C"  void ShopTableCell_SetLabel1_m354282516 (ShopTableCell_t4262618216 * __this, String_t* ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetDescription1(System.String)
extern "C"  void ShopTableCell_SetDescription1_m2150687540 (ShopTableCell_t4262618216 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetCount1(System.Int64)
extern "C"  void ShopTableCell_SetCount1_m930719137 (ShopTableCell_t4262618216 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetImage1(System.String)
extern "C"  void ShopTableCell_SetImage1_m3638843713 (ShopTableCell_t4262618216 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetCost1(System.Int64)
extern "C"  void ShopTableCell_SetCost1_m3457269337 (ShopTableCell_t4262618216 * __this, int64_t ___cost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetId1(System.String)
extern "C"  void ShopTableCell_SetId1_m310707287 (ShopTableCell_t4262618216 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetConstantId1(System.String)
extern "C"  void ShopTableCell_SetConstantId1_m2720074345 (ShopTableCell_t4262618216 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetLabel2(System.String)
extern "C"  void ShopTableCell_SetLabel2_m504248035 (ShopTableCell_t4262618216 * __this, String_t* ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetDescription2(System.String)
extern "C"  void ShopTableCell_SetDescription2_m467845251 (ShopTableCell_t4262618216 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetCount2(System.Int64)
extern "C"  void ShopTableCell_SetCount2_m2886632284 (ShopTableCell_t4262618216 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetImage2(System.String)
extern "C"  void ShopTableCell_SetImage2_m1747883004 (ShopTableCell_t4262618216 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetCost2(System.Int64)
extern "C"  void ShopTableCell_SetCost2_m547637814 (ShopTableCell_t4262618216 * __this, int64_t ___cost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetId2(System.String)
extern "C"  void ShopTableCell_SetId2_m2662229816 (ShopTableCell_t4262618216 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetConstantId2(System.String)
extern "C"  void ShopTableCell_SetConstantId2_m769099400 (ShopTableCell_t4262618216 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::SetAction(System.String)
extern "C"  void ShopTableCell_SetAction_m3786826401 (ShopTableCell_t4262618216 * __this, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::PerformAction1()
extern "C"  void ShopTableCell_PerformAction1_m3278351609 (ShopTableCell_t4262618216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::PerformAction2()
extern "C"  void ShopTableCell_PerformAction2_m3278351448 (ShopTableCell_t4262618216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::Use1()
extern "C"  void ShopTableCell_Use1_m2240062301 (ShopTableCell_t4262618216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableCell::Use2()
extern "C"  void ShopTableCell_Use2_m2240062140 (ShopTableCell_t4262618216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
