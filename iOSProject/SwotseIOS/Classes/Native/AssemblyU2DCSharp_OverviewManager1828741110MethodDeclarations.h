﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OverviewManager
struct OverviewManager_t1828741110;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void OverviewManager::.ctor()
extern "C"  void OverviewManager__ctor_m893582259 (OverviewManager_t1828741110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OverviewManager::OnEnable()
extern "C"  void OverviewManager_OnEnable_m3394488775 (OverviewManager_t1828741110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OverviewManager::FixedUpdate()
extern "C"  void OverviewManager_FixedUpdate_m1370694170 (OverviewManager_t1828741110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OverviewManager::closePanel(System.String)
extern "C"  void OverviewManager_closePanel_m3400106145 (OverviewManager_t1828741110 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OverviewManager::OpenUserInfo()
extern "C"  void OverviewManager_OpenUserInfo_m4039326602 (OverviewManager_t1828741110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
