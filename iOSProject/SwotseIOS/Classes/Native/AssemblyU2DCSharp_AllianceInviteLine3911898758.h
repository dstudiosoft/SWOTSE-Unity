﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UserModel
struct UserModel_t3025569216;
// AllianceInviteTableController
struct AllianceInviteTableController_t989323868;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInviteLine
struct  AllianceInviteLine_t3911898758  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text AllianceInviteLine::memberName
	Text_t356221433 * ___memberName_2;
	// UnityEngine.UI.Text AllianceInviteLine::allianceName
	Text_t356221433 * ___allianceName_3;
	// UnityEngine.UI.Text AllianceInviteLine::rank
	Text_t356221433 * ___rank_4;
	// UnityEngine.UI.Text AllianceInviteLine::experience
	Text_t356221433 * ___experience_5;
	// UnityEngine.UI.Text AllianceInviteLine::cities
	Text_t356221433 * ___cities_6;
	// UnityEngine.UI.Text AllianceInviteLine::capitalName
	Text_t356221433 * ___capitalName_7;
	// UnityEngine.UI.Text AllianceInviteLine::lastOnline
	Text_t356221433 * ___lastOnline_8;
	// UserModel AllianceInviteLine::user
	UserModel_t3025569216 * ___user_9;
	// AllianceInviteTableController AllianceInviteLine::owner
	AllianceInviteTableController_t989323868 * ___owner_10;

public:
	inline static int32_t get_offset_of_memberName_2() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___memberName_2)); }
	inline Text_t356221433 * get_memberName_2() const { return ___memberName_2; }
	inline Text_t356221433 ** get_address_of_memberName_2() { return &___memberName_2; }
	inline void set_memberName_2(Text_t356221433 * value)
	{
		___memberName_2 = value;
		Il2CppCodeGenWriteBarrier(&___memberName_2, value);
	}

	inline static int32_t get_offset_of_allianceName_3() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___allianceName_3)); }
	inline Text_t356221433 * get_allianceName_3() const { return ___allianceName_3; }
	inline Text_t356221433 ** get_address_of_allianceName_3() { return &___allianceName_3; }
	inline void set_allianceName_3(Text_t356221433 * value)
	{
		___allianceName_3 = value;
		Il2CppCodeGenWriteBarrier(&___allianceName_3, value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___rank_4)); }
	inline Text_t356221433 * get_rank_4() const { return ___rank_4; }
	inline Text_t356221433 ** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(Text_t356221433 * value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier(&___rank_4, value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___experience_5)); }
	inline Text_t356221433 * get_experience_5() const { return ___experience_5; }
	inline Text_t356221433 ** get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(Text_t356221433 * value)
	{
		___experience_5 = value;
		Il2CppCodeGenWriteBarrier(&___experience_5, value);
	}

	inline static int32_t get_offset_of_cities_6() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___cities_6)); }
	inline Text_t356221433 * get_cities_6() const { return ___cities_6; }
	inline Text_t356221433 ** get_address_of_cities_6() { return &___cities_6; }
	inline void set_cities_6(Text_t356221433 * value)
	{
		___cities_6 = value;
		Il2CppCodeGenWriteBarrier(&___cities_6, value);
	}

	inline static int32_t get_offset_of_capitalName_7() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___capitalName_7)); }
	inline Text_t356221433 * get_capitalName_7() const { return ___capitalName_7; }
	inline Text_t356221433 ** get_address_of_capitalName_7() { return &___capitalName_7; }
	inline void set_capitalName_7(Text_t356221433 * value)
	{
		___capitalName_7 = value;
		Il2CppCodeGenWriteBarrier(&___capitalName_7, value);
	}

	inline static int32_t get_offset_of_lastOnline_8() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___lastOnline_8)); }
	inline Text_t356221433 * get_lastOnline_8() const { return ___lastOnline_8; }
	inline Text_t356221433 ** get_address_of_lastOnline_8() { return &___lastOnline_8; }
	inline void set_lastOnline_8(Text_t356221433 * value)
	{
		___lastOnline_8 = value;
		Il2CppCodeGenWriteBarrier(&___lastOnline_8, value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___user_9)); }
	inline UserModel_t3025569216 * get_user_9() const { return ___user_9; }
	inline UserModel_t3025569216 ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(UserModel_t3025569216 * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier(&___user_9, value);
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t3911898758, ___owner_10)); }
	inline AllianceInviteTableController_t989323868 * get_owner_10() const { return ___owner_10; }
	inline AllianceInviteTableController_t989323868 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(AllianceInviteTableController_t989323868 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier(&___owner_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
