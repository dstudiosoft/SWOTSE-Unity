﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Aes
struct Aes_t2354947465;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Security.Cryptography.Aes::.ctor()
extern "C"  void Aes__ctor_m4020215067 (Aes_t2354947465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Aes System.Security.Cryptography.Aes::Create()
extern "C"  Aes_t2354947465 * Aes_Create_m551870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Aes System.Security.Cryptography.Aes::Create(System.String)
extern "C"  Aes_t2354947465 * Aes_Create_m3629350860 (Il2CppObject * __this /* static, unused */, String_t* ___algName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
