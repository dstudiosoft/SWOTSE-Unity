﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CredentialCache
struct CredentialCache_t1992799279;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.NetworkCredential
struct NetworkCredential_t1714133953;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_NetworkCredential1714133953.h"

// System.Void System.Net.CredentialCache::.ctor()
extern "C"  void CredentialCache__ctor_m2974902719 (CredentialCache_t1992799279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::.cctor()
extern "C"  void CredentialCache__cctor_m4286319530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.CredentialCache::get_DefaultCredentials()
extern "C"  Il2CppObject * CredentialCache_get_DefaultCredentials_m3928434477 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::get_DefaultNetworkCredentials()
extern "C"  NetworkCredential_t1714133953 * CredentialCache_get_DefaultNetworkCredentials_m3652134091 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::GetCredential(System.Uri,System.String)
extern "C"  NetworkCredential_t1714133953 * CredentialCache_GetCredential_m1586396877 (CredentialCache_t1992799279 * __this, Uri_t19570940 * ___uriPrefix0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.CredentialCache::GetEnumerator()
extern "C"  Il2CppObject * CredentialCache_GetEnumerator_m2787340727 (CredentialCache_t1992799279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Add(System.Uri,System.String,System.Net.NetworkCredential)
extern "C"  void CredentialCache_Add_m3840230748 (CredentialCache_t1992799279 * __this, Uri_t19570940 * ___uriPrefix0, String_t* ___authType1, NetworkCredential_t1714133953 * ___cred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Remove(System.Uri,System.String)
extern "C"  void CredentialCache_Remove_m187551864 (CredentialCache_t1992799279 * __this, Uri_t19570940 * ___uriPrefix0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::GetCredential(System.String,System.Int32,System.String)
extern "C"  NetworkCredential_t1714133953 * CredentialCache_GetCredential_m3719219599 (CredentialCache_t1992799279 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Add(System.String,System.Int32,System.String,System.Net.NetworkCredential)
extern "C"  void CredentialCache_Add_m4274340114 (CredentialCache_t1992799279 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, NetworkCredential_t1714133953 * ___credential3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Remove(System.String,System.Int32,System.String)
extern "C"  void CredentialCache_Remove_m3135282030 (CredentialCache_t1992799279 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
