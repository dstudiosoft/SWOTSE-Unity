﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Tuple`2<System.Object,System.Object>
struct Tuple_2_t1036200771;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m3248227570_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, const MethodInfo* method);
#define Tuple_2__ctor_m3248227570(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t1036200771 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m3248227570_gshared)(__this, ___item10, ___item21, method)
// T1 System.Tuple`2<System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m3565184246_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method);
#define Tuple_2_get_Item1_m3565184246(__this, method) ((  Il2CppObject * (*) (Tuple_2_t1036200771 *, const MethodInfo*))Tuple_2_get_Item1_m3565184246_gshared)(__this, method)
// System.Void System.Tuple`2<System.Object,System.Object>::set_Item1(T1)
extern "C"  void Tuple_2_set_Item1_m135613129_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Tuple_2_set_Item1_m135613129(__this, ___value0, method) ((  void (*) (Tuple_2_t1036200771 *, Il2CppObject *, const MethodInfo*))Tuple_2_set_Item1_m135613129_gshared)(__this, ___value0, method)
// T2 System.Tuple`2<System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_2_get_Item2_m26266116_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method);
#define Tuple_2_get_Item2_m26266116(__this, method) ((  Il2CppObject * (*) (Tuple_2_t1036200771 *, const MethodInfo*))Tuple_2_get_Item2_m26266116_gshared)(__this, method)
// System.Void System.Tuple`2<System.Object,System.Object>::set_Item2(T2)
extern "C"  void Tuple_2_set_Item2_m2864499617_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Tuple_2_set_Item2_m2864499617(__this, ___value0, method) ((  void (*) (Tuple_2_t1036200771 *, Il2CppObject *, const MethodInfo*))Tuple_2_set_Item2_m2864499617_gshared)(__this, ___value0, method)
// System.Boolean System.Tuple`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m1387835382_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m1387835382(__this, ___obj0, method) ((  bool (*) (Tuple_2_t1036200771 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m1387835382_gshared)(__this, ___obj0, method)
// System.Int32 System.Tuple`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m3929157358_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m3929157358(__this, method) ((  int32_t (*) (Tuple_2_t1036200771 *, const MethodInfo*))Tuple_2_GetHashCode_m3929157358_gshared)(__this, method)
