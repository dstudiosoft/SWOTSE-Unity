﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyItemContentManager
struct BuyItemContentManager_t58540967;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// IReloadable
struct IReloadable_t861412162;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void BuyItemContentManager::.ctor()
extern "C"  void BuyItemContentManager__ctor_m436556406 (BuyItemContentManager_t58540967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::SetContent(UnityEngine.Sprite,System.String,System.String,System.String,System.String,IReloadable,System.Boolean)
extern "C"  void BuyItemContentManager_SetContent_m2777201320 (BuyItemContentManager_t58540967 * __this, Sprite_t309593783 * ___image0, String_t* ___name1, String_t* ___count2, String_t* ___unitCost3, String_t* ___id4, Il2CppObject * ___controller5, bool ___buy6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::ChangeTotalCost()
extern "C"  void BuyItemContentManager_ChangeTotalCost_m3914422105 (BuyItemContentManager_t58540967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::BuyItems()
extern "C"  void BuyItemContentManager_BuyItems_m1779225240 (BuyItemContentManager_t58540967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::ItemsBought(System.Object,System.String)
extern "C"  void BuyItemContentManager_ItemsBought_m3093798711 (BuyItemContentManager_t58540967 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::ItemsGifted(System.Object,System.String)
extern "C"  void BuyItemContentManager_ItemsGifted_m1485823697 (BuyItemContentManager_t58540967 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::Close(System.Object,System.String)
extern "C"  void BuyItemContentManager_Close_m1430224062 (BuyItemContentManager_t58540967 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyItemContentManager::Cancel()
extern "C"  void BuyItemContentManager_Cancel_m2424771226 (BuyItemContentManager_t58540967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
