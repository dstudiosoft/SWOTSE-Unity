﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.ReleaseGesture
struct ReleaseGesture_t248506278;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture2352305985.h"

// System.Void TouchScript.Gestures.ReleaseGesture::.ctor()
extern "C"  void ReleaseGesture__ctor_m230835007 (ReleaseGesture_t248506278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ReleaseGesture::add_Released(System.EventHandler`1<System.EventArgs>)
extern "C"  void ReleaseGesture_add_Released_m3719329804 (ReleaseGesture_t248506278 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ReleaseGesture::remove_Released(System.EventHandler`1<System.EventArgs>)
extern "C"  void ReleaseGesture_remove_Released_m4128839447 (ReleaseGesture_t248506278 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.ReleaseGesture::get_IgnoreChildren()
extern "C"  bool ReleaseGesture_get_IgnoreChildren_m1336915529 (ReleaseGesture_t248506278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ReleaseGesture::set_IgnoreChildren(System.Boolean)
extern "C"  void ReleaseGesture_set_IgnoreChildren_m3629721474 (ReleaseGesture_t248506278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.ReleaseGesture::ShouldReceiveTouch(TouchScript.TouchPoint)
extern "C"  bool ReleaseGesture_ShouldReceiveTouch_m3738549365 (ReleaseGesture_t248506278 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.ReleaseGesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern "C"  bool ReleaseGesture_CanPreventGesture_m1989284875 (ReleaseGesture_t248506278 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.ReleaseGesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern "C"  bool ReleaseGesture_CanBePreventedByGesture_m1443562826 (ReleaseGesture_t248506278 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ReleaseGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void ReleaseGesture_touchesEnded_m2137587092 (ReleaseGesture_t248506278 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ReleaseGesture::onRecognized()
extern "C"  void ReleaseGesture_onRecognized_m3200472994 (ReleaseGesture_t248506278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
