﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Single TouchScript.Utils.Geom.TwoD::PointToLineDistance(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float TwoD_PointToLineDistance_m194317436 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lineStart0, Vector2_t2243707579  ___lineEnd1, Vector2_t2243707579  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Utils.Geom.TwoD::PointToLineDistance2(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single&)
extern "C"  void TwoD_PointToLineDistance2_m487670722 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lineStart0, Vector2_t2243707579  ___lineEnd1, Vector2_t2243707579  ___point12, Vector2_t2243707579  ___point23, float* ___dist14, float* ___dist25, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Utils.Geom.TwoD::Rotate(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  TwoD_Rotate_m3865690867 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, float ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
