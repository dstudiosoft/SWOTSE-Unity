﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t3898926532;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.UploadStringCompletedEventArgs::.ctor(System.String,System.Exception,System.Boolean,System.Object)
extern "C"  void UploadStringCompletedEventArgs__ctor_m953141823 (UploadStringCompletedEventArgs_t3898926532 * __this, String_t* ___result0, Exception_t1927440687 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.UploadStringCompletedEventArgs::get_Result()
extern "C"  String_t* UploadStringCompletedEventArgs_get_Result_m2979489953 (UploadStringCompletedEventArgs_t3898926532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
