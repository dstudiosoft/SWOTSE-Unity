﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingConstantModel
struct BuildingConstantModel_t2546650549;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildingConstantModel::.ctor()
extern "C"  void BuildingConstantModel__ctor_m2716034344 (BuildingConstantModel_t2546650549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
