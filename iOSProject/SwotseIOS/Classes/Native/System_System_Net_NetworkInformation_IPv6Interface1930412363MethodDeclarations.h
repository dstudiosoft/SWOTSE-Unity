﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IPv6InterfaceProperties
struct IPv6InterfaceProperties_t1930412363;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IPv6InterfaceProperties::.ctor()
extern "C"  void IPv6InterfaceProperties__ctor_m1989766357 (IPv6InterfaceProperties_t1930412363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
