﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingPrice
struct BuildingPrice_t4120786961;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildingPrice::.ctor()
extern "C"  void BuildingPrice__ctor_m2919777528 (BuildingPrice_t4120786961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
