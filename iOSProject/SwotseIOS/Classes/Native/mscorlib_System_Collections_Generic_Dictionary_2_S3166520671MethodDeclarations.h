﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>
struct ShimEnumerator_t3166520671;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m112420235_gshared (ShimEnumerator_t3166520671 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m112420235(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3166520671 *, Dictionary_2_t3061395850 *, const MethodInfo*))ShimEnumerator__ctor_m112420235_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2324958462_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2324958462(__this, method) ((  bool (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_MoveNext_m2324958462_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3218951198_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3218951198(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_get_Entry_m3218951198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2284555955_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2284555955(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_get_Key_m2284555955_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m582342131_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m582342131(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_get_Value_m582342131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2024727857_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2024727857(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_get_Current_m2024727857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m1765049101_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1765049101(__this, method) ((  void (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))ShimEnumerator_Reset_m1765049101_gshared)(__this, method)
