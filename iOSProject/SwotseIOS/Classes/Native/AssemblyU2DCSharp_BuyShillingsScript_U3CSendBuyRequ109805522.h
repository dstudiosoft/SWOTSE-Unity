﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyShillingsScript/<SendBuyRequest>c__Iterator5
struct  U3CSendBuyRequestU3Ec__Iterator5_t109805522  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm BuyShillingsScript/<SendBuyRequest>c__Iterator5::<requestForm>__0
	WWWForm_t3950226929 * ___U3CrequestFormU3E__0_0;
	// System.String BuyShillingsScript/<SendBuyRequest>c__Iterator5::count
	String_t* ___count_1;
	// UnityEngine.Networking.UnityWebRequest BuyShillingsScript/<SendBuyRequest>c__Iterator5::<request>__1
	UnityWebRequest_t254341728 * ___U3CrequestU3E__1_2;
	// JSONObject BuyShillingsScript/<SendBuyRequest>c__Iterator5::<response>__2
	JSONObject_t1971882247 * ___U3CresponseU3E__2_3;
	// System.Int32 BuyShillingsScript/<SendBuyRequest>c__Iterator5::$PC
	int32_t ___U24PC_4;
	// System.Object BuyShillingsScript/<SendBuyRequest>c__Iterator5::$current
	Il2CppObject * ___U24current_5;
	// System.String BuyShillingsScript/<SendBuyRequest>c__Iterator5::<$>count
	String_t* ___U3CU24U3Ecount_6;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestFormU3E__0_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___count_1)); }
	inline String_t* get_count_1() const { return ___count_1; }
	inline String_t** get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(String_t* value)
	{
		___count_1 = value;
		Il2CppCodeGenWriteBarrier(&___count_1, value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U3CrequestU3E__1_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__1_2() const { return ___U3CrequestU3E__1_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__1_2() { return &___U3CrequestU3E__1_2; }
	inline void set_U3CrequestU3E__1_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U3CresponseU3E__2_3)); }
	inline JSONObject_t1971882247 * get_U3CresponseU3E__2_3() const { return ___U3CresponseU3E__2_3; }
	inline JSONObject_t1971882247 ** get_address_of_U3CresponseU3E__2_3() { return &___U3CresponseU3E__2_3; }
	inline void set_U3CresponseU3E__2_3(JSONObject_t1971882247 * value)
	{
		___U3CresponseU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecount_6() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator5_t109805522, ___U3CU24U3Ecount_6)); }
	inline String_t* get_U3CU24U3Ecount_6() const { return ___U3CU24U3Ecount_6; }
	inline String_t** get_address_of_U3CU24U3Ecount_6() { return &___U3CU24U3Ecount_6; }
	inline void set_U3CU24U3Ecount_6(String_t* value)
	{
		___U3CU24U3Ecount_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecount_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
