﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1640144840;
// Mono.Security.ASN1
struct ASN1_t924533536;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Mono.Security.X509.X509Extension
struct X509Extension_t1439760128;
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t2486010406;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_ASN1924533535.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1439760127.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl1640144839.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor()
extern "C"  void X509ExtensionCollection__ctor_m1119345384 (X509ExtensionCollection_t1640144840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor(Mono.Security.ASN1)
extern "C"  void X509ExtensionCollection__ctor_m2672322588 (X509ExtensionCollection_t1640144840 * __this, ASN1_t924533536 * ___asn10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Security.X509.X509ExtensionCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2600902401 (X509ExtensionCollection_t1640144840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::Add(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_Add_m1431250405 (X509ExtensionCollection_t1640144840 * __this, X509Extension_t1439760128 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509Extension[])
extern "C"  void X509ExtensionCollection_AddRange_m1637065284 (X509ExtensionCollection_t1640144840 * __this, X509ExtensionU5BU5D_t2486010406* ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509ExtensionCollection)
extern "C"  void X509ExtensionCollection_AddRange_m2121178524 (X509ExtensionCollection_t1640144840 * __this, X509ExtensionCollection_t1640144840 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(Mono.Security.X509.X509Extension)
extern "C"  bool X509ExtensionCollection_Contains_m1549249589 (X509ExtensionCollection_t1640144840 * __this, X509Extension_t1439760128 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(System.String)
extern "C"  bool X509ExtensionCollection_Contains_m2342701741 (X509ExtensionCollection_t1640144840 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::CopyTo(Mono.Security.X509.X509Extension[],System.Int32)
extern "C"  void X509ExtensionCollection_CopyTo_m25104559 (X509ExtensionCollection_t1640144840 * __this, X509ExtensionU5BU5D_t2486010406* ___extensions0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m2710878999 (X509ExtensionCollection_t1640144840 * __this, X509Extension_t1439760128 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(System.String)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m3972100651 (X509ExtensionCollection_t1640144840 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Insert(System.Int32,Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Insert_m3211852314 (X509ExtensionCollection_t1640144840 * __this, int32_t ___index0, X509Extension_t1439760128 * ___extension1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Remove_m1947391600 (X509ExtensionCollection_t1640144840 * __this, X509Extension_t1439760128 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(System.String)
extern "C"  void X509ExtensionCollection_Remove_m472460344 (X509ExtensionCollection_t1640144840 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.Int32)
extern "C"  X509Extension_t1439760128 * X509ExtensionCollection_get_Item_m3601133726 (X509ExtensionCollection_t1640144840 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.String)
extern "C"  X509Extension_t1439760128 * X509ExtensionCollection_get_Item_m4212651995 (X509ExtensionCollection_t1640144840 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509ExtensionCollection::GetBytes()
extern "C"  ByteU5BU5D_t3397334013* X509ExtensionCollection_GetBytes_m10395679 (X509ExtensionCollection_t1640144840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
