﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.GenericUriParser
struct GenericUriParser_t2599285286;

#include "codegen/il2cpp-codegen.h"
#include "System_System_GenericUriParserOptions1048705754.h"

// System.Void System.GenericUriParser::.ctor(System.GenericUriParserOptions)
extern "C"  void GenericUriParser__ctor_m1323566592 (GenericUriParser_t2599285286 * __this, int32_t ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
