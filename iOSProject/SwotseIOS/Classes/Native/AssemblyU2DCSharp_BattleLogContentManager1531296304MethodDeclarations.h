﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleLogContentManager
struct BattleLogContentManager_t1531296304;

#include "codegen/il2cpp-codegen.h"

// System.Void BattleLogContentManager::.ctor()
extern "C"  void BattleLogContentManager__ctor_m3421279587 (BattleLogContentManager_t1531296304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleLogContentManager::OnEnable()
extern "C"  void BattleLogContentManager_OnEnable_m3854146551 (BattleLogContentManager_t1531296304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
