﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<>c__AnonStorey4
struct U3CU3Ec__AnonStorey4_t1110811683;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/<>c__AnonStorey4::.ctor()
extern "C"  void U3CU3Ec__AnonStorey4__ctor_m584980008 (U3CU3Ec__AnonStorey4_t1110811683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<>c__AnonStorey4::<>m__0()
extern "C"  void U3CU3Ec__AnonStorey4_U3CU3Em__0_m2283203531 (U3CU3Ec__AnonStorey4_t1110811683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
