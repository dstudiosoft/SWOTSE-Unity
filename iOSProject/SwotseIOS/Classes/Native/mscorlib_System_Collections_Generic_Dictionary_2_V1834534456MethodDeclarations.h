﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>
struct ValueCollection_t1834534456;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va523040081.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1760151388_gshared (ValueCollection_t1834534456 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1760151388(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1834534456 *, Dictionary_2_t3131474613 *, const MethodInfo*))ValueCollection__ctor_m1760151388_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4281311350_gshared (ValueCollection_t1834534456 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4281311350(__this, ___item0, method) ((  void (*) (ValueCollection_t1834534456 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4281311350_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3471385195_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3471385195(__this, method) ((  void (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3471385195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650367146_gshared (ValueCollection_t1834534456 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650367146(__this, ___item0, method) ((  bool (*) (ValueCollection_t1834534456 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650367146_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4188992987_gshared (ValueCollection_t1834534456 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4188992987(__this, ___item0, method) ((  bool (*) (ValueCollection_t1834534456 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4188992987_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3930535201_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3930535201(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3930535201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2212134845_gshared (ValueCollection_t1834534456 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2212134845(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1834534456 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2212134845_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2109355710_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2109355710(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2109355710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3022741191_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3022741191(__this, method) ((  bool (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3022741191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2217081553_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2217081553(__this, method) ((  bool (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2217081553_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3253749389_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3253749389(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3253749389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m254858027_gshared (ValueCollection_t1834534456 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m254858027(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1834534456 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m254858027_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::GetEnumerator()
extern "C"  Enumerator_t523040081  ValueCollection_GetEnumerator_m2859673798_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2859673798(__this, method) ((  Enumerator_t523040081  (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_GetEnumerator_m2859673798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3277366953_gshared (ValueCollection_t1834534456 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3277366953(__this, method) ((  int32_t (*) (ValueCollection_t1834534456 *, const MethodInfo*))ValueCollection_get_Count_m3277366953_gshared)(__this, method)
