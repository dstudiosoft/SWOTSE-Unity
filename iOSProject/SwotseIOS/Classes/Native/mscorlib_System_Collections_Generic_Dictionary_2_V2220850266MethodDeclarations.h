﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3235466541(__this, ___host0, method) ((  void (*) (Enumerator_t2220850266 *, Dictionary_2_t534317502 *, const MethodInfo*))Enumerator__ctor_m419014865_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3970984426(__this, method) ((  Il2CppObject * (*) (Enumerator_t2220850266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1316264340(__this, method) ((  void (*) (Enumerator_t2220850266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::Dispose()
#define Enumerator_Dispose_m4139770421(__this, method) ((  void (*) (Enumerator_t2220850266 *, const MethodInfo*))Enumerator_Dispose_m942415041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::MoveNext()
#define Enumerator_MoveNext_m1302886139(__this, method) ((  bool (*) (Enumerator_t2220850266 *, const MethodInfo*))Enumerator_MoveNext_m2045321910_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ItemTimer>::get_Current()
#define Enumerator_get_Current_m352009204(__this, method) ((  ItemTimer_t3839766396 * (*) (Enumerator_t2220850266 *, const MethodInfo*))Enumerator_get_Current_m1952080304_gshared)(__this, method)
