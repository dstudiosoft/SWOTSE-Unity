﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceCitiesTableController
struct ResidenceCitiesTableController_t1287619937;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void ResidenceCitiesTableController::.ctor()
extern "C"  void ResidenceCitiesTableController__ctor_m1937303528 (ResidenceCitiesTableController_t1287619937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableController::OnEnable()
extern "C"  void ResidenceCitiesTableController_OnEnable_m4066705632 (ResidenceCitiesTableController_t1287619937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ResidenceCitiesTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t ResidenceCitiesTableController_GetNumberOfRowsForTableView_m3100998758 (ResidenceCitiesTableController_t1287619937 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ResidenceCitiesTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float ResidenceCitiesTableController_GetHeightForRowInTableView_m2939019174 (ResidenceCitiesTableController_t1287619937 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell ResidenceCitiesTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * ResidenceCitiesTableController_GetCellForRowInTableView_m1263408537 (ResidenceCitiesTableController_t1287619937 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableController::OpenBuildingWindow(System.Int64)
extern "C"  void ResidenceCitiesTableController_OpenBuildingWindow_m3738155958 (ResidenceCitiesTableController_t1287619937 * __this, int64_t ___pitId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
