﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginProgressbarContentManager
struct LoginProgressbarContentManager_t2739795147;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginProgressbarContentManager::.ctor()
extern "C"  void LoginProgressbarContentManager__ctor_m1132132146 (LoginProgressbarContentManager_t2739795147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginProgressbarContentManager::Start()
extern "C"  void LoginProgressbarContentManager_Start_m383478518 (LoginProgressbarContentManager_t2739795147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginProgressbarContentManager::Update()
extern "C"  void LoginProgressbarContentManager_Update_m4150925427 (LoginProgressbarContentManager_t2739795147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
