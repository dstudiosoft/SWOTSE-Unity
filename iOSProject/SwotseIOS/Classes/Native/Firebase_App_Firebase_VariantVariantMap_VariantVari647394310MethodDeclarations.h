﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.VariantVariantMap/VariantVariantMapEnumerator
struct VariantVariantMapEnumerator_t647394310;
// Firebase.VariantVariantMap
struct VariantVariantMap_t4097173110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_VariantVariantMap4097173110.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22573636725.h"

// System.Void Firebase.VariantVariantMap/VariantVariantMapEnumerator::.ctor(Firebase.VariantVariantMap)
extern "C"  void VariantVariantMapEnumerator__ctor_m1881717056 (VariantVariantMapEnumerator_t647394310 * __this, VariantVariantMap_t4097173110 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant> Firebase.VariantVariantMap/VariantVariantMapEnumerator::get_Current()
extern "C"  KeyValuePair_2_t2573636725  VariantVariantMapEnumerator_get_Current_m3875637035 (VariantVariantMapEnumerator_t647394310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.VariantVariantMap/VariantVariantMapEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * VariantVariantMapEnumerator_System_Collections_IEnumerator_get_Current_m1114471583 (VariantVariantMapEnumerator_t647394310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap/VariantVariantMapEnumerator::MoveNext()
extern "C"  bool VariantVariantMapEnumerator_MoveNext_m3045105527 (VariantVariantMapEnumerator_t647394310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap/VariantVariantMapEnumerator::Reset()
extern "C"  void VariantVariantMapEnumerator_Reset_m1225427122 (VariantVariantMapEnumerator_t647394310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap/VariantVariantMapEnumerator::Dispose()
extern "C"  void VariantVariantMapEnumerator_Dispose_m914956404 (VariantVariantMapEnumerator_t647394310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
