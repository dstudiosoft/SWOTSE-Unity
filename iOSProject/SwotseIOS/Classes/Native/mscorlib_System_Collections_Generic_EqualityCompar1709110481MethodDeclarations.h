﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>
struct DefaultComparer_t1709110481;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::.ctor()
extern "C"  void DefaultComparer__ctor_m1251145037_gshared (DefaultComparer_t1709110481 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1251145037(__this, method) ((  void (*) (DefaultComparer_t1709110481 *, const MethodInfo*))DefaultComparer__ctor_m1251145037_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m401186960_gshared (DefaultComparer_t1709110481 * __this, RaycastHit_t87180320  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m401186960(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1709110481 *, RaycastHit_t87180320 , const MethodInfo*))DefaultComparer_GetHashCode_m401186960_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3834728656_gshared (DefaultComparer_t1709110481 * __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3834728656(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1709110481 *, RaycastHit_t87180320 , RaycastHit_t87180320 , const MethodInfo*))DefaultComparer_Equals_m3834728656_gshared)(__this, ___x0, ___y1, method)
