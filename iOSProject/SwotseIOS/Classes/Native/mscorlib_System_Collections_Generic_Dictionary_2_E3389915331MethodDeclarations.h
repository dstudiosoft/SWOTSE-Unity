﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m898088412(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3389915331 *, Dictionary_2_t2069890629 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1800599793(__this, method) ((  Il2CppObject * (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2376303889(__this, method) ((  void (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4223327770(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1484494499(__this, method) ((  Il2CppObject * (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2632496907(__this, method) ((  Il2CppObject * (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::MoveNext()
#define Enumerator_MoveNext_m2941203729(__this, method) ((  bool (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::get_Current()
#define Enumerator_get_Current_m674779349(__this, method) ((  KeyValuePair_2_t4122203147  (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m596817188(__this, method) ((  int32_t (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1743514724(__this, method) ((  Action_t3062064994 * (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::Reset()
#define Enumerator_Reset_m4269035642(__this, method) ((  void (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::VerifyState()
#define Enumerator_VerifyState_m1577706371(__this, method) ((  void (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1224028073(__this, method) ((  void (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureString/Action>::Dispose()
#define Enumerator_Dispose_m2602512196(__this, method) ((  void (*) (Enumerator_t3389915331 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
