﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.PKCS9
struct PKCS9_t1922213172;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.PKCS9::.ctor()
extern "C"  void PKCS9__ctor_m3818953629 (PKCS9_t1922213172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
