﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPInterfaceProperties2
struct Win32IPInterfaceProperties2_t3641679752;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t680756680;
// System.Net.NetworkInformation.IPv4InterfaceProperties
struct IPv4InterfaceProperties_t1411071681;
// System.Net.NetworkInformation.IPv6InterfaceProperties
struct IPv6InterfaceProperties_t1930412363;
// System.Net.NetworkInformation.IPAddressInformationCollection
struct IPAddressInformationCollection_t3688726299;
// System.Net.NetworkInformation.IPAddressCollection
struct IPAddressCollection_t2986660307;
// System.String
struct String_t;
// System.Net.NetworkInformation.GatewayIPAddressInformationCollection
struct GatewayIPAddressInformationCollection_t3537717171;
// System.Net.NetworkInformation.MulticastIPAddressInformationCollection
struct MulticastIPAddressInformationCollection_t2350102231;
// System.Net.NetworkInformation.UnicastIPAddressInformationCollection
struct UnicastIPAddressInformationCollection_t347163204;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAPT680756680.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"

// System.Void System.Net.NetworkInformation.Win32IPInterfaceProperties2::.ctor(System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES,System.Net.NetworkInformation.Win32_MIB_IFROW,System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPInterfaceProperties2__ctor_m3917205584 (Win32IPInterfaceProperties2_t3641679752 * __this, Win32_IP_ADAPTER_ADDRESSES_t680756680 * ___addr0, Win32_MIB_IFROW_t4215928996  ___mib41, Win32_MIB_IFROW_t4215928996  ___mib62, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPv4InterfaceProperties System.Net.NetworkInformation.Win32IPInterfaceProperties2::GetIPv4Properties()
extern "C"  IPv4InterfaceProperties_t1411071681 * Win32IPInterfaceProperties2_GetIPv4Properties_m2250827596 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPv6InterfaceProperties System.Net.NetworkInformation.Win32IPInterfaceProperties2::GetIPv6Properties()
extern "C"  IPv6InterfaceProperties_t1930412363 * Win32IPInterfaceProperties2_GetIPv6Properties_m3437497868 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressInformationCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_AnycastAddresses()
extern "C"  IPAddressInformationCollection_t3688726299 * Win32IPInterfaceProperties2_get_AnycastAddresses_m3185561622 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_DhcpServerAddresses()
extern "C"  IPAddressCollection_t2986660307 * Win32IPInterfaceProperties2_get_DhcpServerAddresses_m1652723913 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_DnsAddresses()
extern "C"  IPAddressCollection_t2986660307 * Win32IPInterfaceProperties2_get_DnsAddresses_m1140426308 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_DnsSuffix()
extern "C"  String_t* Win32IPInterfaceProperties2_get_DnsSuffix_m738103670 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.GatewayIPAddressInformationCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_GatewayAddresses()
extern "C"  GatewayIPAddressInformationCollection_t3537717171 * Win32IPInterfaceProperties2_get_GatewayAddresses_m3809861383 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_IsDnsEnabled()
extern "C"  bool Win32IPInterfaceProperties2_get_IsDnsEnabled_m148292293 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_IsDynamicDnsEnabled()
extern "C"  bool Win32IPInterfaceProperties2_get_IsDynamicDnsEnabled_m1035907852 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.MulticastIPAddressInformationCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_MulticastAddresses()
extern "C"  MulticastIPAddressInformationCollection_t2350102231 * Win32IPInterfaceProperties2_get_MulticastAddresses_m4070334687 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.UnicastIPAddressInformationCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_UnicastAddresses()
extern "C"  UnicastIPAddressInformationCollection_t347163204 * Win32IPInterfaceProperties2_get_UnicastAddresses_m3041646099 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.Win32IPInterfaceProperties2::get_WinsServersAddresses()
extern "C"  IPAddressCollection_t2986660307 * Win32IPInterfaceProperties2_get_WinsServersAddresses_m960381258 (Win32IPInterfaceProperties2_t3641679752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
