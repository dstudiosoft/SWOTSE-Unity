﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailInboxLine
struct MailInboxLine_t3727558719;
// MailInboxTableController
struct MailInboxTableController_t27963299;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MailInboxTableController27963299.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailInboxLine::.ctor()
extern "C"  void MailInboxLine__ctor_m1617065042 (MailInboxLine_t3727558719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetOwner(MailInboxTableController)
extern "C"  void MailInboxLine_SetOwner_m3234435118 (MailInboxLine_t3727558719 * __this, MailInboxTableController_t27963299 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetMessageId(System.Int64)
extern "C"  void MailInboxLine_SetMessageId_m2114527022 (MailInboxLine_t3727558719 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetPlayerName(System.String)
extern "C"  void MailInboxLine_SetPlayerName_m469666182 (MailInboxLine_t3727558719 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetSubject(System.String)
extern "C"  void MailInboxLine_SetSubject_m922913022 (MailInboxLine_t3727558719 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetDate(System.String)
extern "C"  void MailInboxLine_SetDate_m464816020 (MailInboxLine_t3727558719 * __this, String_t* ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::SetRead()
extern "C"  void MailInboxLine_SetRead_m3230314262 (MailInboxLine_t3727558719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::ShowMessage()
extern "C"  void MailInboxLine_ShowMessage_m3844860478 (MailInboxLine_t3727558719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::ReplyMessage()
extern "C"  void MailInboxLine_ReplyMessage_m2759590275 (MailInboxLine_t3727558719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxLine::DeleteMessage()
extern "C"  void MailInboxLine_DeleteMessage_m3476137300 (MailInboxLine_t3727558719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
