﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct EventHandler_1_t3126626844;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct Dictionary_2_t2974023424;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct List_1_t1262417539;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct List_1_t3532169525;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct EventHandler_1_t4113216011;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct Dictionary_2_t1693470997;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct List_1_t4276832408;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t319478644;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t1881969775;
// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct List_1_t2674567522;
// System.Uri
struct Uri_t100236324;
// WebSocketSharp.WebSocket
struct WebSocket_t62038747;
// System.Collections.Generic.Queue`1<System.Byte[]>
struct Queue_1_t3962907151;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2597455733;
// WebSocket
struct WebSocket_t1645401340;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t3574483607;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t2617245831;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2060094783;
// SmartLocalization.ILocalizedAssetLoader
struct ILocalizedAssetLoader_t2808879256;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Shader
struct Shader_t4151988712;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct EventHandler_1_t2455490493;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct Dictionary_2_t3186476502;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct List_1_t1474870617;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// System.Char[]
struct CharU5BU5D_t3528271667;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t4085310093;
// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t2372265022;
// System.Void
struct Void_t1185182177;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t2795875;
// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t2804757666;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3872604895;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2541822629;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// SmartLocalization.LanguageManager
struct LanguageManager_t2767934455;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>
struct List_1_t2881543979;
// SmartLocalization.ChangeLanguageEventHandler
struct ChangeLanguageEventHandler_t2032193146;
// SmartLocalization.SmartCultureInfoCollection
struct SmartCultureInfoCollection_t1809038765;
// SmartLocalization.LanguageDataHandler
struct LanguageDataHandler_t3889722316;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Object
struct Object_t631007953;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t4087348596;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t3168066469;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t3982445645;




#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef U3CMODULEU3E_T692745557_H
#define U3CMODULEU3E_T692745557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745557 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745557_H
#ifndef VIRTUALBUTTON_T2756566330_H
#define VIRTUALBUTTON_T2756566330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t2756566330  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T2756566330_H
#ifndef BLOBPROCESSOR_T2126871202_H
#define BLOBPROCESSOR_T2126871202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.BlobProcessor
struct  BlobProcessor_t2126871202  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobAdded
	EventHandler_1_t3126626844 * ___BlobAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobUpdated
	EventHandler_1_t3126626844 * ___BlobUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobRemoved
	EventHandler_1_t3126626844 * ___BlobRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.BlobProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::blobs
	Dictionary_2_t2974023424 * ___blobs_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::updatedBlobs
	List_1_t1262417539 * ___updatedBlobs_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::addedBlobs
	List_1_t128053199 * ___addedBlobs_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::removedBlobs
	List_1_t128053199 * ___removedBlobs_7;

public:
	inline static int32_t get_offset_of_BlobAdded_0() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobAdded_0)); }
	inline EventHandler_1_t3126626844 * get_BlobAdded_0() const { return ___BlobAdded_0; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobAdded_0() { return &___BlobAdded_0; }
	inline void set_BlobAdded_0(EventHandler_1_t3126626844 * value)
	{
		___BlobAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___BlobAdded_0), value);
	}

	inline static int32_t get_offset_of_BlobUpdated_1() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobUpdated_1)); }
	inline EventHandler_1_t3126626844 * get_BlobUpdated_1() const { return ___BlobUpdated_1; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobUpdated_1() { return &___BlobUpdated_1; }
	inline void set_BlobUpdated_1(EventHandler_1_t3126626844 * value)
	{
		___BlobUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___BlobUpdated_1), value);
	}

	inline static int32_t get_offset_of_BlobRemoved_2() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobRemoved_2)); }
	inline EventHandler_1_t3126626844 * get_BlobRemoved_2() const { return ___BlobRemoved_2; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobRemoved_2() { return &___BlobRemoved_2; }
	inline void set_BlobRemoved_2(EventHandler_1_t3126626844 * value)
	{
		___BlobRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___BlobRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_blobs_4() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___blobs_4)); }
	inline Dictionary_2_t2974023424 * get_blobs_4() const { return ___blobs_4; }
	inline Dictionary_2_t2974023424 ** get_address_of_blobs_4() { return &___blobs_4; }
	inline void set_blobs_4(Dictionary_2_t2974023424 * value)
	{
		___blobs_4 = value;
		Il2CppCodeGenWriteBarrier((&___blobs_4), value);
	}

	inline static int32_t get_offset_of_updatedBlobs_5() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___updatedBlobs_5)); }
	inline List_1_t1262417539 * get_updatedBlobs_5() const { return ___updatedBlobs_5; }
	inline List_1_t1262417539 ** get_address_of_updatedBlobs_5() { return &___updatedBlobs_5; }
	inline void set_updatedBlobs_5(List_1_t1262417539 * value)
	{
		___updatedBlobs_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedBlobs_5), value);
	}

	inline static int32_t get_offset_of_addedBlobs_6() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___addedBlobs_6)); }
	inline List_1_t128053199 * get_addedBlobs_6() const { return ___addedBlobs_6; }
	inline List_1_t128053199 ** get_address_of_addedBlobs_6() { return &___addedBlobs_6; }
	inline void set_addedBlobs_6(List_1_t128053199 * value)
	{
		___addedBlobs_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedBlobs_6), value);
	}

	inline static int32_t get_offset_of_removedBlobs_7() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___removedBlobs_7)); }
	inline List_1_t128053199 * get_removedBlobs_7() const { return ___removedBlobs_7; }
	inline List_1_t128053199 ** get_address_of_removedBlobs_7() { return &___removedBlobs_7; }
	inline void set_removedBlobs_7(List_1_t128053199 * value)
	{
		___removedBlobs_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedBlobs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOBPROCESSOR_T2126871202_H
#ifndef RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#define RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.RuntimeLocalizedAssetLoader
struct  RuntimeLocalizedAssetLoader_t316402138  : public RuntimeObject
{
public:

public:
};

struct RuntimeLocalizedAssetLoader_t316402138_StaticFields
{
public:
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::GameObjectType
	Type_t * ___GameObjectType_0;
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::AudioClipType
	Type_t * ___AudioClipType_1;
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::TextureType
	Type_t * ___TextureType_2;

public:
	inline static int32_t get_offset_of_GameObjectType_0() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___GameObjectType_0)); }
	inline Type_t * get_GameObjectType_0() const { return ___GameObjectType_0; }
	inline Type_t ** get_address_of_GameObjectType_0() { return &___GameObjectType_0; }
	inline void set_GameObjectType_0(Type_t * value)
	{
		___GameObjectType_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectType_0), value);
	}

	inline static int32_t get_offset_of_AudioClipType_1() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___AudioClipType_1)); }
	inline Type_t * get_AudioClipType_1() const { return ___AudioClipType_1; }
	inline Type_t ** get_address_of_AudioClipType_1() { return &___AudioClipType_1; }
	inline void set_AudioClipType_1(Type_t * value)
	{
		___AudioClipType_1 = value;
		Il2CppCodeGenWriteBarrier((&___AudioClipType_1), value);
	}

	inline static int32_t get_offset_of_TextureType_2() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___TextureType_2)); }
	inline Type_t * get_TextureType_2() const { return ___TextureType_2; }
	inline Type_t ** get_address_of_TextureType_2() { return &___TextureType_2; }
	inline void set_TextureType_2(Type_t * value)
	{
		___TextureType_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextureType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef SMARTCULTUREINFOCOLLECTION_T1809038765_H
#define SMARTCULTUREINFOCOLLECTION_T1809038765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfoCollection
struct  SmartCultureInfoCollection_t1809038765  : public RuntimeObject
{
public:
	// System.Int32 SmartLocalization.SmartCultureInfoCollection::version
	int32_t ___version_1;
	// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.SmartCultureInfoCollection::cultureInfos
	List_1_t3532169525 * ___cultureInfos_2;

public:
	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(SmartCultureInfoCollection_t1809038765, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_cultureInfos_2() { return static_cast<int32_t>(offsetof(SmartCultureInfoCollection_t1809038765, ___cultureInfos_2)); }
	inline List_1_t3532169525 * get_cultureInfos_2() const { return ___cultureInfos_2; }
	inline List_1_t3532169525 ** get_address_of_cultureInfos_2() { return &___cultureInfos_2; }
	inline void set_cultureInfos_2(List_1_t3532169525 * value)
	{
		___cultureInfos_2 = value;
		Il2CppCodeGenWriteBarrier((&___cultureInfos_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFOCOLLECTION_T1809038765_H
#ifndef SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#define SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfoCollectionDeserializer
struct  SmartCultureInfoCollectionDeserializer_t1510284870  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#ifndef SMARTCULTUREINFO_T2060094783_H
#define SMARTCULTUREINFO_T2060094783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfo
struct  SmartCultureInfo_t2060094783  : public RuntimeObject
{
public:
	// System.String SmartLocalization.SmartCultureInfo::languageCode
	String_t* ___languageCode_0;
	// System.String SmartLocalization.SmartCultureInfo::englishName
	String_t* ___englishName_1;
	// System.String SmartLocalization.SmartCultureInfo::nativeName
	String_t* ___nativeName_2;
	// System.Boolean SmartLocalization.SmartCultureInfo::isRightToLeft
	bool ___isRightToLeft_3;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}

	inline static int32_t get_offset_of_englishName_1() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___englishName_1)); }
	inline String_t* get_englishName_1() const { return ___englishName_1; }
	inline String_t** get_address_of_englishName_1() { return &___englishName_1; }
	inline void set_englishName_1(String_t* value)
	{
		___englishName_1 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_1), value);
	}

	inline static int32_t get_offset_of_nativeName_2() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___nativeName_2)); }
	inline String_t* get_nativeName_2() const { return ___nativeName_2; }
	inline String_t** get_address_of_nativeName_2() { return &___nativeName_2; }
	inline void set_nativeName_2(String_t* value)
	{
		___nativeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___nativeName_2), value);
	}

	inline static int32_t get_offset_of_isRightToLeft_3() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___isRightToLeft_3)); }
	inline bool get_isRightToLeft_3() const { return ___isRightToLeft_3; }
	inline bool* get_address_of_isRightToLeft_3() { return &___isRightToLeft_3; }
	inline void set_isRightToLeft_3(bool value)
	{
		___isRightToLeft_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFO_T2060094783_H
#ifndef CURSORPROCESSOR_T3936223090_H
#define CURSORPROCESSOR_T3936223090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.CursorProcessor
struct  CursorProcessor_t3936223090  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorAdded
	EventHandler_1_t4113216011 * ___CursorAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorUpdated
	EventHandler_1_t4113216011 * ___CursorUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorRemoved
	EventHandler_1_t4113216011 * ___CursorRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.CursorProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::cursors
	Dictionary_2_t1693470997 * ___cursors_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::updatedCursors
	List_1_t4276832408 * ___updatedCursors_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::addedCursors
	List_1_t128053199 * ___addedCursors_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::removedCursors
	List_1_t128053199 * ___removedCursors_7;

public:
	inline static int32_t get_offset_of_CursorAdded_0() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorAdded_0)); }
	inline EventHandler_1_t4113216011 * get_CursorAdded_0() const { return ___CursorAdded_0; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorAdded_0() { return &___CursorAdded_0; }
	inline void set_CursorAdded_0(EventHandler_1_t4113216011 * value)
	{
		___CursorAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___CursorAdded_0), value);
	}

	inline static int32_t get_offset_of_CursorUpdated_1() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorUpdated_1)); }
	inline EventHandler_1_t4113216011 * get_CursorUpdated_1() const { return ___CursorUpdated_1; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorUpdated_1() { return &___CursorUpdated_1; }
	inline void set_CursorUpdated_1(EventHandler_1_t4113216011 * value)
	{
		___CursorUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___CursorUpdated_1), value);
	}

	inline static int32_t get_offset_of_CursorRemoved_2() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorRemoved_2)); }
	inline EventHandler_1_t4113216011 * get_CursorRemoved_2() const { return ___CursorRemoved_2; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorRemoved_2() { return &___CursorRemoved_2; }
	inline void set_CursorRemoved_2(EventHandler_1_t4113216011 * value)
	{
		___CursorRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_cursors_4() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___cursors_4)); }
	inline Dictionary_2_t1693470997 * get_cursors_4() const { return ___cursors_4; }
	inline Dictionary_2_t1693470997 ** get_address_of_cursors_4() { return &___cursors_4; }
	inline void set_cursors_4(Dictionary_2_t1693470997 * value)
	{
		___cursors_4 = value;
		Il2CppCodeGenWriteBarrier((&___cursors_4), value);
	}

	inline static int32_t get_offset_of_updatedCursors_5() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___updatedCursors_5)); }
	inline List_1_t4276832408 * get_updatedCursors_5() const { return ___updatedCursors_5; }
	inline List_1_t4276832408 ** get_address_of_updatedCursors_5() { return &___updatedCursors_5; }
	inline void set_updatedCursors_5(List_1_t4276832408 * value)
	{
		___updatedCursors_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedCursors_5), value);
	}

	inline static int32_t get_offset_of_addedCursors_6() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___addedCursors_6)); }
	inline List_1_t128053199 * get_addedCursors_6() const { return ___addedCursors_6; }
	inline List_1_t128053199 ** get_address_of_addedCursors_6() { return &___addedCursors_6; }
	inline void set_addedCursors_6(List_1_t128053199 * value)
	{
		___addedCursors_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedCursors_6), value);
	}

	inline static int32_t get_offset_of_removedCursors_7() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___removedCursors_7)); }
	inline List_1_t128053199 * get_removedCursors_7() const { return ___removedCursors_7; }
	inline List_1_t128053199 ** get_address_of_removedCursors_7() { return &___removedCursors_7; }
	inline void set_removedCursors_7(List_1_t128053199 * value)
	{
		___removedCursors_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedCursors_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORPROCESSOR_T3936223090_H
#ifndef TUIOSERVER_T3158170151_H
#define TUIOSERVER_T3158170151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.TuioServer
struct  TuioServer_t3158170151  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> TUIOsharp.TuioServer::ErrorOccured
	EventHandler_1_t319478644 * ___ErrorOccured_0;
	// System.Int32 TUIOsharp.TuioServer::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_1;
	// OSCsharp.Net.UDPReceiver TUIOsharp.TuioServer::udpReceiver
	UDPReceiver_t1881969775 * ___udpReceiver_2;
	// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor> TUIOsharp.TuioServer::processors
	List_1_t2674567522 * ___processors_3;

public:
	inline static int32_t get_offset_of_ErrorOccured_0() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___ErrorOccured_0)); }
	inline EventHandler_1_t319478644 * get_ErrorOccured_0() const { return ___ErrorOccured_0; }
	inline EventHandler_1_t319478644 ** get_address_of_ErrorOccured_0() { return &___ErrorOccured_0; }
	inline void set_ErrorOccured_0(EventHandler_1_t319478644 * value)
	{
		___ErrorOccured_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_0), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___U3CPortU3Ek__BackingField_1)); }
	inline int32_t get_U3CPortU3Ek__BackingField_1() const { return ___U3CPortU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_1() { return &___U3CPortU3Ek__BackingField_1; }
	inline void set_U3CPortU3Ek__BackingField_1(int32_t value)
	{
		___U3CPortU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_udpReceiver_2() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___udpReceiver_2)); }
	inline UDPReceiver_t1881969775 * get_udpReceiver_2() const { return ___udpReceiver_2; }
	inline UDPReceiver_t1881969775 ** get_address_of_udpReceiver_2() { return &___udpReceiver_2; }
	inline void set_udpReceiver_2(UDPReceiver_t1881969775 * value)
	{
		___udpReceiver_2 = value;
		Il2CppCodeGenWriteBarrier((&___udpReceiver_2), value);
	}

	inline static int32_t get_offset_of_processors_3() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___processors_3)); }
	inline List_1_t2674567522 * get_processors_3() const { return ___processors_3; }
	inline List_1_t2674567522 ** get_address_of_processors_3() { return &___processors_3; }
	inline void set_processors_3(List_1_t2674567522 * value)
	{
		___processors_3 = value;
		Il2CppCodeGenWriteBarrier((&___processors_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOSERVER_T3158170151_H
#ifndef TUIOENTITY_T4229790798_H
#define TUIOENTITY_T4229790798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioEntity
struct  TuioEntity_t4229790798  : public RuntimeObject
{
public:
	// System.Int32 TUIOsharp.Entities.TuioEntity::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Single TUIOsharp.Entities.TuioEntity::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single TUIOsharp.Entities.TuioEntity::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_3;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_4;
	// System.Single TUIOsharp.Entities.TuioEntity::<Acceleration>k__BackingField
	float ___U3CAccelerationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CVelocityXU3Ek__BackingField_3)); }
	inline float get_U3CVelocityXU3Ek__BackingField_3() const { return ___U3CVelocityXU3Ek__BackingField_3; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_3() { return &___U3CVelocityXU3Ek__BackingField_3; }
	inline void set_U3CVelocityXU3Ek__BackingField_3(float value)
	{
		___U3CVelocityXU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CVelocityYU3Ek__BackingField_4)); }
	inline float get_U3CVelocityYU3Ek__BackingField_4() const { return ___U3CVelocityYU3Ek__BackingField_4; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_4() { return &___U3CVelocityYU3Ek__BackingField_4; }
	inline void set_U3CVelocityYU3Ek__BackingField_4(float value)
	{
		___U3CVelocityYU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CAccelerationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CAccelerationU3Ek__BackingField_5)); }
	inline float get_U3CAccelerationU3Ek__BackingField_5() const { return ___U3CAccelerationU3Ek__BackingField_5; }
	inline float* get_address_of_U3CAccelerationU3Ek__BackingField_5() { return &___U3CAccelerationU3Ek__BackingField_5; }
	inline void set_U3CAccelerationU3Ek__BackingField_5(float value)
	{
		___U3CAccelerationU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOENTITY_T4229790798_H
#ifndef UNIWEBVIEWPLUGIN_T2474266150_H
#define UNIWEBVIEWPLUGIN_T2474266150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewPlugin
struct  UniWebViewPlugin_t2474266150  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWPLUGIN_T2474266150_H
#ifndef WEBSOCKET_T1645401340_H
#define WEBSOCKET_T1645401340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket
struct  WebSocket_t1645401340  : public RuntimeObject
{
public:
	// System.Uri WebSocket::mUrl
	Uri_t100236324 * ___mUrl_0;
	// WebSocketSharp.WebSocket WebSocket::m_Socket
	WebSocket_t62038747 * ___m_Socket_1;
	// System.Collections.Generic.Queue`1<System.Byte[]> WebSocket::m_Messages
	Queue_1_t3962907151 * ___m_Messages_2;
	// System.Boolean WebSocket::m_IsConnected
	bool ___m_IsConnected_3;
	// System.String WebSocket::m_Error
	String_t* ___m_Error_4;

public:
	inline static int32_t get_offset_of_mUrl_0() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___mUrl_0)); }
	inline Uri_t100236324 * get_mUrl_0() const { return ___mUrl_0; }
	inline Uri_t100236324 ** get_address_of_mUrl_0() { return &___mUrl_0; }
	inline void set_mUrl_0(Uri_t100236324 * value)
	{
		___mUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUrl_0), value);
	}

	inline static int32_t get_offset_of_m_Socket_1() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Socket_1)); }
	inline WebSocket_t62038747 * get_m_Socket_1() const { return ___m_Socket_1; }
	inline WebSocket_t62038747 ** get_address_of_m_Socket_1() { return &___m_Socket_1; }
	inline void set_m_Socket_1(WebSocket_t62038747 * value)
	{
		___m_Socket_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Socket_1), value);
	}

	inline static int32_t get_offset_of_m_Messages_2() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Messages_2)); }
	inline Queue_1_t3962907151 * get_m_Messages_2() const { return ___m_Messages_2; }
	inline Queue_1_t3962907151 ** get_address_of_m_Messages_2() { return &___m_Messages_2; }
	inline void set_m_Messages_2(Queue_1_t3962907151 * value)
	{
		___m_Messages_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Messages_2), value);
	}

	inline static int32_t get_offset_of_m_IsConnected_3() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_IsConnected_3)); }
	inline bool get_m_IsConnected_3() const { return ___m_IsConnected_3; }
	inline bool* get_address_of_m_IsConnected_3() { return &___m_IsConnected_3; }
	inline void set_m_IsConnected_3(bool value)
	{
		___m_IsConnected_3 = value;
	}

	inline static int32_t get_offset_of_m_Error_4() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Error_4)); }
	inline String_t* get_m_Error_4() const { return ___m_Error_4; }
	inline String_t** get_address_of_m_Error_4() { return &___m_Error_4; }
	inline void set_m_Error_4(String_t* value)
	{
		___m_Error_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Error_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T1645401340_H
#ifndef CROSSPLATFORMINPUTMANAGER_T191731427_H
#define CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t191731427  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t191731427_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2597455733 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2597455733 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2597455733 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2597455733 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2597455733 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2597455733 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2597455733 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2597455733 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2597455733 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2597455733 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2597455733 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2597455733 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifndef VIRTUALAXIS_T4087348596_H
#define VIRTUALAXIS_T4087348596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t4087348596  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T4087348596_H
#ifndef U3CCONNECTU3EC__ITERATOR0_T1583584490_H
#define U3CCONNECTU3EC__ITERATOR0_T1583584490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket/<Connect>c__Iterator0
struct  U3CConnectU3Ec__Iterator0_t1583584490  : public RuntimeObject
{
public:
	// WebSocket WebSocket/<Connect>c__Iterator0::$this
	WebSocket_t1645401340 * ___U24this_0;
	// System.Object WebSocket/<Connect>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean WebSocket/<Connect>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 WebSocket/<Connect>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__Iterator0_t1583584490, ___U24this_0)); }
	inline WebSocket_t1645401340 * get_U24this_0() const { return ___U24this_0; }
	inline WebSocket_t1645401340 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(WebSocket_t1645401340 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__Iterator0_t1583584490, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__Iterator0_t1583584490, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__Iterator0_t1583584490, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTU3EC__ITERATOR0_T1583584490_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ENTRIES_T3168066469_H
#define ENTRIES_T3168066469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t3168066469  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t3574483607* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t3168066469, ___entries_0)); }
	inline EntryU5BU5D_t3574483607* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t3574483607** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t3574483607* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T3168066469_H
#ifndef APPLICATIONEXTENSIONS_T792742538_H
#define APPLICATIONEXTENSIONS_T792742538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.ApplicationExtensions
struct  ApplicationExtensions_t792742538  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXTENSIONS_T792742538_H
#ifndef U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#define U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0
struct  U3CFOVKickUpU3Ec__Iterator0_t3738408313  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifndef U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#define U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1
struct  U3CFOVKickDownU3Ec__Iterator1_t1440840980  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifndef LANGUAGEDATAHANDLER_T3889722316_H
#define LANGUAGEDATAHANDLER_T3889722316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageDataHandler
struct  LanguageDataHandler_t3889722316  : public RuntimeObject
{
public:
	// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::loadedValuesDictionary
	SortedDictionary_2_t2617245831 * ___loadedValuesDictionary_0;
	// System.Boolean SmartLocalization.LanguageDataHandler::verboseLogging
	bool ___verboseLogging_1;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::loadedCultureInfo
	SmartCultureInfo_t2060094783 * ___loadedCultureInfo_2;
	// SmartLocalization.ILocalizedAssetLoader SmartLocalization.LanguageDataHandler::assetLoader
	RuntimeObject* ___assetLoader_3;

public:
	inline static int32_t get_offset_of_loadedValuesDictionary_0() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___loadedValuesDictionary_0)); }
	inline SortedDictionary_2_t2617245831 * get_loadedValuesDictionary_0() const { return ___loadedValuesDictionary_0; }
	inline SortedDictionary_2_t2617245831 ** get_address_of_loadedValuesDictionary_0() { return &___loadedValuesDictionary_0; }
	inline void set_loadedValuesDictionary_0(SortedDictionary_2_t2617245831 * value)
	{
		___loadedValuesDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadedValuesDictionary_0), value);
	}

	inline static int32_t get_offset_of_verboseLogging_1() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___verboseLogging_1)); }
	inline bool get_verboseLogging_1() const { return ___verboseLogging_1; }
	inline bool* get_address_of_verboseLogging_1() { return &___verboseLogging_1; }
	inline void set_verboseLogging_1(bool value)
	{
		___verboseLogging_1 = value;
	}

	inline static int32_t get_offset_of_loadedCultureInfo_2() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___loadedCultureInfo_2)); }
	inline SmartCultureInfo_t2060094783 * get_loadedCultureInfo_2() const { return ___loadedCultureInfo_2; }
	inline SmartCultureInfo_t2060094783 ** get_address_of_loadedCultureInfo_2() { return &___loadedCultureInfo_2; }
	inline void set_loadedCultureInfo_2(SmartCultureInfo_t2060094783 * value)
	{
		___loadedCultureInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___loadedCultureInfo_2), value);
	}

	inline static int32_t get_offset_of_assetLoader_3() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___assetLoader_3)); }
	inline RuntimeObject* get_assetLoader_3() const { return ___assetLoader_3; }
	inline RuntimeObject** get_address_of_assetLoader_3() { return &___assetLoader_3; }
	inline void set_assetLoader_3(RuntimeObject* value)
	{
		___assetLoader_3 = value;
		Il2CppCodeGenWriteBarrier((&___assetLoader_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATAHANDLER_T3889722316_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef OBJECTPROCESSOR_T2877610401_H
#define OBJECTPROCESSOR_T2877610401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.ObjectProcessor
struct  ObjectProcessor_t2877610401  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectAdded
	EventHandler_1_t2455490493 * ___ObjectAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectUpdated
	EventHandler_1_t2455490493 * ___ObjectUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectRemoved
	EventHandler_1_t2455490493 * ___ObjectRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.ObjectProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::objects
	Dictionary_2_t3186476502 * ___objects_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::updatedObjects
	List_1_t1474870617 * ___updatedObjects_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::addedObjects
	List_1_t128053199 * ___addedObjects_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::removedObjects
	List_1_t128053199 * ___removedObjects_7;

public:
	inline static int32_t get_offset_of_ObjectAdded_0() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectAdded_0)); }
	inline EventHandler_1_t2455490493 * get_ObjectAdded_0() const { return ___ObjectAdded_0; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectAdded_0() { return &___ObjectAdded_0; }
	inline void set_ObjectAdded_0(EventHandler_1_t2455490493 * value)
	{
		___ObjectAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectAdded_0), value);
	}

	inline static int32_t get_offset_of_ObjectUpdated_1() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectUpdated_1)); }
	inline EventHandler_1_t2455490493 * get_ObjectUpdated_1() const { return ___ObjectUpdated_1; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectUpdated_1() { return &___ObjectUpdated_1; }
	inline void set_ObjectUpdated_1(EventHandler_1_t2455490493 * value)
	{
		___ObjectUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectUpdated_1), value);
	}

	inline static int32_t get_offset_of_ObjectRemoved_2() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectRemoved_2)); }
	inline EventHandler_1_t2455490493 * get_ObjectRemoved_2() const { return ___ObjectRemoved_2; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectRemoved_2() { return &___ObjectRemoved_2; }
	inline void set_ObjectRemoved_2(EventHandler_1_t2455490493 * value)
	{
		___ObjectRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___objects_4)); }
	inline Dictionary_2_t3186476502 * get_objects_4() const { return ___objects_4; }
	inline Dictionary_2_t3186476502 ** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(Dictionary_2_t3186476502 * value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier((&___objects_4), value);
	}

	inline static int32_t get_offset_of_updatedObjects_5() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___updatedObjects_5)); }
	inline List_1_t1474870617 * get_updatedObjects_5() const { return ___updatedObjects_5; }
	inline List_1_t1474870617 ** get_address_of_updatedObjects_5() { return &___updatedObjects_5; }
	inline void set_updatedObjects_5(List_1_t1474870617 * value)
	{
		___updatedObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedObjects_5), value);
	}

	inline static int32_t get_offset_of_addedObjects_6() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___addedObjects_6)); }
	inline List_1_t128053199 * get_addedObjects_6() const { return ___addedObjects_6; }
	inline List_1_t128053199 ** get_address_of_addedObjects_6() { return &___addedObjects_6; }
	inline void set_addedObjects_6(List_1_t128053199 * value)
	{
		___addedObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedObjects_6), value);
	}

	inline static int32_t get_offset_of_removedObjects_7() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___removedObjects_7)); }
	inline List_1_t128053199 * get_removedObjects_7() const { return ___removedObjects_7; }
	inline List_1_t128053199 ** get_address_of_removedObjects_7() { return &___removedObjects_7; }
	inline void set_removedObjects_7(List_1_t128053199 * value)
	{
		___removedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedObjects_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPROCESSOR_T2877610401_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef LANGUAGERUNTIMEDATA_T3907746602_H
#define LANGUAGERUNTIMEDATA_T3907746602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageRuntimeData
struct  LanguageRuntimeData_t3907746602  : public RuntimeObject
{
public:

public:
};

struct LanguageRuntimeData_t3907746602_StaticFields
{
public:
	// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFileName
	String_t* ___AvailableCulturesFileName_0;
	// System.String SmartLocalization.LanguageRuntimeData::rootLanguageName
	String_t* ___rootLanguageName_1;
	// System.String SmartLocalization.LanguageRuntimeData::AudioFileFolderName
	String_t* ___AudioFileFolderName_2;
	// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderName
	String_t* ___TexturesFolderName_3;
	// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderName
	String_t* ___PrefabsFolderName_4;

public:
	inline static int32_t get_offset_of_AvailableCulturesFileName_0() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___AvailableCulturesFileName_0)); }
	inline String_t* get_AvailableCulturesFileName_0() const { return ___AvailableCulturesFileName_0; }
	inline String_t** get_address_of_AvailableCulturesFileName_0() { return &___AvailableCulturesFileName_0; }
	inline void set_AvailableCulturesFileName_0(String_t* value)
	{
		___AvailableCulturesFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableCulturesFileName_0), value);
	}

	inline static int32_t get_offset_of_rootLanguageName_1() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___rootLanguageName_1)); }
	inline String_t* get_rootLanguageName_1() const { return ___rootLanguageName_1; }
	inline String_t** get_address_of_rootLanguageName_1() { return &___rootLanguageName_1; }
	inline void set_rootLanguageName_1(String_t* value)
	{
		___rootLanguageName_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootLanguageName_1), value);
	}

	inline static int32_t get_offset_of_AudioFileFolderName_2() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___AudioFileFolderName_2)); }
	inline String_t* get_AudioFileFolderName_2() const { return ___AudioFileFolderName_2; }
	inline String_t** get_address_of_AudioFileFolderName_2() { return &___AudioFileFolderName_2; }
	inline void set_AudioFileFolderName_2(String_t* value)
	{
		___AudioFileFolderName_2 = value;
		Il2CppCodeGenWriteBarrier((&___AudioFileFolderName_2), value);
	}

	inline static int32_t get_offset_of_TexturesFolderName_3() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___TexturesFolderName_3)); }
	inline String_t* get_TexturesFolderName_3() const { return ___TexturesFolderName_3; }
	inline String_t** get_address_of_TexturesFolderName_3() { return &___TexturesFolderName_3; }
	inline void set_TexturesFolderName_3(String_t* value)
	{
		___TexturesFolderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___TexturesFolderName_3), value);
	}

	inline static int32_t get_offset_of_PrefabsFolderName_4() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___PrefabsFolderName_4)); }
	inline String_t* get_PrefabsFolderName_4() const { return ___PrefabsFolderName_4; }
	inline String_t** get_address_of_PrefabsFolderName_4() { return &___PrefabsFolderName_4; }
	inline void set_PrefabsFolderName_4(String_t* value)
	{
		___PrefabsFolderName_4 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabsFolderName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGERUNTIMEDATA_T3907746602_H
#ifndef LANGUAGEPARSER_T1152791527_H
#define LANGUAGEPARSER_T1152791527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageParser
struct  LanguageParser_t1152791527  : public RuntimeObject
{
public:

public:
};

struct LanguageParser_t1152791527_StaticFields
{
public:
	// System.String SmartLocalization.LanguageParser::xmlHeader
	String_t* ___xmlHeader_0;

public:
	inline static int32_t get_offset_of_xmlHeader_0() { return static_cast<int32_t>(offsetof(LanguageParser_t1152791527_StaticFields, ___xmlHeader_0)); }
	inline String_t* get_xmlHeader_0() const { return ___xmlHeader_0; }
	inline String_t** get_address_of_xmlHeader_0() { return &___xmlHeader_0; }
	inline void set_xmlHeader_0(String_t* value)
	{
		___xmlHeader_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlHeader_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEPARSER_T1152791527_H
#ifndef U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#define U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0
struct  U3CResetCoroutineU3Ec__Iterator0_t3232105836  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::delay
	float ___delay_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar0
	TransformU5BU5D_t807237628* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$this
	ObjectResetter_t639177103 * ___U24this_3;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar0_1)); }
	inline TransformU5BU5D_t807237628* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t807237628** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t807237628* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24this_3)); }
	inline ObjectResetter_t639177103 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectResetter_t639177103 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifndef U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#define U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0
struct  U3CDoBobCycleU3Ec__Iterator0_t1149538828  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$this
	LerpControlledBob_t1895875871 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24this_1)); }
	inline LerpControlledBob_t1895875871 * get_U24this_1() const { return ___U24this_1; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LerpControlledBob_t1895875871 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifndef U3CSTARTU3EC__ITERATOR0_T980021917_H
#define U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t980021917  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<systems>__0
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3089334924* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<stopTime>__0
	float ___U3CstopTimeU3E__0_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar2
	ParticleSystemU5BU5D_t3089334924* ___U24locvar2_4;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar3
	int32_t ___U24locvar3_5;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$this
	ParticleSystemDestroyer_t558680695 * ___U24this_6;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CsystemsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CsystemsU3E__0_0)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E__0_0() const { return ___U3CsystemsU3E__0_0; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E__0_0() { return &___U3CsystemsU3E__0_0; }
	inline void set_U3CsystemsU3E__0_0(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar0_1)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CstopTimeU3E__0_3)); }
	inline float get_U3CstopTimeU3E__0_3() const { return ___U3CstopTimeU3E__0_3; }
	inline float* get_address_of_U3CstopTimeU3E__0_3() { return &___U3CstopTimeU3E__0_3; }
	inline void set_U3CstopTimeU3E__0_3(float value)
	{
		___U3CstopTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar2_4)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar2_4() const { return ___U24locvar2_4; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar2_4() { return &___U24locvar2_4; }
	inline void set_U24locvar2_4(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar3_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar3_5)); }
	inline int32_t get_U24locvar3_5() const { return ___U24locvar3_5; }
	inline int32_t* get_address_of_U24locvar3_5() { return &___U24locvar3_5; }
	inline void set_U24locvar3_5(int32_t value)
	{
		___U24locvar3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24this_6)); }
	inline ParticleSystemDestroyer_t558680695 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleSystemDestroyer_t558680695 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifndef U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#define U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0
struct  U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::englishName
	String_t* ___englishName_0;

public:
	inline static int32_t get_offset_of_englishName_0() { return static_cast<int32_t>(offsetof(U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546, ___englishName_0)); }
	inline String_t* get_englishName_0() const { return ___englishName_0; }
	inline String_t** get_address_of_englishName_0() { return &___englishName_0; }
	inline void set_englishName_0(String_t* value)
	{
		___englishName_0 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#ifndef U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#define U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3
struct  U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::languageCode
	String_t* ___languageCode_0;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#ifndef U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#define U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2
struct  U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::englishName
	String_t* ___englishName_0;

public:
	inline static int32_t get_offset_of_englishName_0() { return static_cast<int32_t>(offsetof(U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582, ___englishName_0)); }
	inline String_t* get_englishName_0() const { return ___englishName_0; }
	inline String_t** get_address_of_englishName_0() { return &___englishName_0; }
	inline void set_englishName_0(String_t* value)
	{
		___englishName_0 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#ifndef U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#define U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1
struct  U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::languageCode
	String_t* ___languageCode_0;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#define MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t1582016909  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TUIOBLOBEVENTARGS_T907500115_H
#define TUIOBLOBEVENTARGS_T907500115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct  TuioBlobEventArgs_t907500115  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioBlob TUIOsharp.DataProcessors.TuioBlobEventArgs::Blob
	TuioBlob_t4085310093 * ___Blob_1;

public:
	inline static int32_t get_offset_of_Blob_1() { return static_cast<int32_t>(offsetof(TuioBlobEventArgs_t907500115, ___Blob_1)); }
	inline TuioBlob_t4085310093 * get_Blob_1() const { return ___Blob_1; }
	inline TuioBlob_t4085310093 ** get_address_of_Blob_1() { return &___Blob_1; }
	inline void set_Blob_1(TuioBlob_t4085310093 * value)
	{
		___Blob_1 = value;
		Il2CppCodeGenWriteBarrier((&___Blob_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOBEVENTARGS_T907500115_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef MESSAGERECEIVEDEVENTARGS_T1247432759_H
#define MESSAGERECEIVEDEVENTARGS_T1247432759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.MessageReceivedEventArgs
struct  MessageReceivedEventArgs_t1247432759  : public EventArgs_t3591816995
{
public:
	// Firebase.Messaging.FirebaseMessage Firebase.Messaging.MessageReceivedEventArgs::<Message>k__BackingField
	FirebaseMessage_t2372265022 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageReceivedEventArgs_t1247432759, ___U3CMessageU3Ek__BackingField_1)); }
	inline FirebaseMessage_t2372265022 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline FirebaseMessage_t2372265022 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(FirebaseMessage_t2372265022 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDEVENTARGS_T1247432759_H
#ifndef TOKENRECEIVEDEVENTARGS_T4150709950_H
#define TOKENRECEIVEDEVENTARGS_T4150709950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.TokenReceivedEventArgs
struct  TokenReceivedEventArgs_t4150709950  : public EventArgs_t3591816995
{
public:
	// System.String Firebase.Messaging.TokenReceivedEventArgs::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TokenReceivedEventArgs_t4150709950, ___U3CTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_1() const { return ___U3CTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_1() { return &___U3CTokenU3Ek__BackingField_1; }
	inline void set_U3CTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENRECEIVEDEVENTARGS_T4150709950_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef TUIOOBJECT_T2795875_H
#define TUIOOBJECT_T2795875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioObject
struct  TuioObject_t2795875  : public TuioEntity_t4229790798
{
public:
	// System.Int32 TUIOsharp.Entities.TuioObject::<ClassId>k__BackingField
	int32_t ___U3CClassIdU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioObject::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CClassIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CClassIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CClassIdU3Ek__BackingField_6() const { return ___U3CClassIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CClassIdU3Ek__BackingField_6() { return &___U3CClassIdU3Ek__BackingField_6; }
	inline void set_U3CClassIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CClassIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CAngleU3Ek__BackingField_7)); }
	inline float get_U3CAngleU3Ek__BackingField_7() const { return ___U3CAngleU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_7() { return &___U3CAngleU3Ek__BackingField_7; }
	inline void set_U3CAngleU3Ek__BackingField_7(float value)
	{
		___U3CAngleU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CRotationVelocityU3Ek__BackingField_8)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_8() const { return ___U3CRotationVelocityU3Ek__BackingField_8; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_8() { return &___U3CRotationVelocityU3Ek__BackingField_8; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_8(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CRotationAccelerationU3Ek__BackingField_9)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_9() const { return ___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_9() { return &___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_9(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECT_T2795875_H
#ifndef TUIOBLOB_T4085310093_H
#define TUIOBLOB_T4085310093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioBlob
struct  TuioBlob_t4085310093  : public TuioEntity_t4229790798
{
public:
	// System.Single TUIOsharp.Entities.TuioBlob::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioBlob::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioBlob::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioBlob::<Area>k__BackingField
	float ___U3CAreaU3Ek__BackingField_9;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_10;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CAngleU3Ek__BackingField_6)); }
	inline float get_U3CAngleU3Ek__BackingField_6() const { return ___U3CAngleU3Ek__BackingField_6; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_6() { return &___U3CAngleU3Ek__BackingField_6; }
	inline void set_U3CAngleU3Ek__BackingField_6(float value)
	{
		___U3CAngleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CWidthU3Ek__BackingField_7)); }
	inline float get_U3CWidthU3Ek__BackingField_7() const { return ___U3CWidthU3Ek__BackingField_7; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_7() { return &___U3CWidthU3Ek__BackingField_7; }
	inline void set_U3CWidthU3Ek__BackingField_7(float value)
	{
		___U3CWidthU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CHeightU3Ek__BackingField_8)); }
	inline float get_U3CHeightU3Ek__BackingField_8() const { return ___U3CHeightU3Ek__BackingField_8; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_8() { return &___U3CHeightU3Ek__BackingField_8; }
	inline void set_U3CHeightU3Ek__BackingField_8(float value)
	{
		___U3CHeightU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CAreaU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CAreaU3Ek__BackingField_9)); }
	inline float get_U3CAreaU3Ek__BackingField_9() const { return ___U3CAreaU3Ek__BackingField_9; }
	inline float* get_address_of_U3CAreaU3Ek__BackingField_9() { return &___U3CAreaU3Ek__BackingField_9; }
	inline void set_U3CAreaU3Ek__BackingField_9(float value)
	{
		___U3CAreaU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CRotationVelocityU3Ek__BackingField_10)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_10() const { return ___U3CRotationVelocityU3Ek__BackingField_10; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_10() { return &___U3CRotationVelocityU3Ek__BackingField_10; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_10(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CRotationAccelerationU3Ek__BackingField_11)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_11() const { return ___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_11() { return &___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_11(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOB_T4085310093_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef TUIOCURSOR_T2804757666_H
#define TUIOCURSOR_T2804757666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioCursor
struct  TuioCursor_t2804757666  : public TuioEntity_t4229790798
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOR_T2804757666_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TUIOOBJECTEVENTARGS_T236363764_H
#define TUIOOBJECTEVENTARGS_T236363764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct  TuioObjectEventArgs_t236363764  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioObject TUIOsharp.DataProcessors.TuioObjectEventArgs::Object
	TuioObject_t2795875 * ___Object_1;

public:
	inline static int32_t get_offset_of_Object_1() { return static_cast<int32_t>(offsetof(TuioObjectEventArgs_t236363764, ___Object_1)); }
	inline TuioObject_t2795875 * get_Object_1() const { return ___Object_1; }
	inline TuioObject_t2795875 ** get_address_of_Object_1() { return &___Object_1; }
	inline void set_Object_1(TuioObject_t2795875 * value)
	{
		___Object_1 = value;
		Il2CppCodeGenWriteBarrier((&___Object_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTEVENTARGS_T236363764_H
#ifndef TUIOCURSOREVENTARGS_T1894089282_H
#define TUIOCURSOREVENTARGS_T1894089282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct  TuioCursorEventArgs_t1894089282  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioCursor TUIOsharp.DataProcessors.TuioCursorEventArgs::Cursor
	TuioCursor_t2804757666 * ___Cursor_1;

public:
	inline static int32_t get_offset_of_Cursor_1() { return static_cast<int32_t>(offsetof(TuioCursorEventArgs_t1894089282, ___Cursor_1)); }
	inline TuioCursor_t2804757666 * get_Cursor_1() const { return ___Cursor_1; }
	inline TuioCursor_t2804757666 ** get_address_of_Cursor_1() { return &___Cursor_1; }
	inline void set_Cursor_1(TuioCursor_t2804757666 * value)
	{
		___Cursor_1 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOREVENTARGS_T1894089282_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef ACTION_T837364808_H
#define ACTION_T837364808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t837364808 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t837364808, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T837364808_H
#ifndef HANDLEREF_T3745784362_H
#define HANDLEREF_T3745784362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784362 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T3745784362_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef AXISOPTION_T1372819835_H
#define AXISOPTION_T1372819835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1372819835 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1372819835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1372819835_H
#ifndef LOCALIZEDOBJECTTYPE_T858129287_H
#define LOCALIZEDOBJECTTYPE_T858129287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedObjectType
struct  LocalizedObjectType_t858129287 
{
public:
	// System.Int32 SmartLocalization.LocalizedObjectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LocalizedObjectType_t858129287, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDOBJECTTYPE_T858129287_H
#ifndef AXISOPTIONS_T3101732129_H
#define AXISOPTIONS_T3101732129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t3101732129 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t3101732129, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T3101732129_H
#ifndef ACTIVEINPUTMETHOD_T139315314_H
#define ACTIVEINPUTMETHOD_T139315314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t139315314 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t139315314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T139315314_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MAPPINGTYPE_T2039944511_H
#define MAPPINGTYPE_T2039944511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t2039944511 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t2039944511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T2039944511_H
#ifndef AXISOPTION_T3128671669_H
#define AXISOPTION_T3128671669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t3128671669 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t3128671669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3128671669_H
#ifndef VIRTUALINPUT_T2597455733_H
#define VIRTUALINPUT_T2597455733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2597455733  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t3722313464  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3872604895 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2541822629 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t3319525431 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3872604895 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3872604895 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3872604895 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2541822629 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2541822629 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2541822629 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t3319525431 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t3319525431 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t3319525431 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2597455733_H
#ifndef CONTROLSTYLE_T1372986211_H
#define CONTROLSTYLE_T1372986211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t1372986211 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t1372986211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T1372986211_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef STANDALONEINPUT_T1343950252_H
#define STANDALONEINPUT_T1343950252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1343950252  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1343950252_H
#ifndef ENTRY_T2725803170_H
#define ENTRY_T2725803170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t2725803170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1113636619 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___target_0)); }
	inline GameObject_t1113636619 * get_target_0() const { return ___target_0; }
	inline GameObject_t1113636619 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1113636619 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2725803170_H
#ifndef AXISMAPPING_T3982445645_H
#define AXISMAPPING_T3982445645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t3982445645  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T3982445645_H
#ifndef LOCALIZEDOBJECT_T1409469237_H
#define LOCALIZEDOBJECT_T1409469237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedObject
struct  LocalizedObject_t1409469237  : public RuntimeObject
{
public:
	// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::objectType
	int32_t ___objectType_2;
	// System.String SmartLocalization.LocalizedObject::textValue
	String_t* ___textValue_3;
	// UnityEngine.GameObject SmartLocalization.LocalizedObject::thisGameObject
	GameObject_t1113636619 * ___thisGameObject_4;
	// UnityEngine.AudioClip SmartLocalization.LocalizedObject::thisAudioClip
	AudioClip_t3680889665 * ___thisAudioClip_5;
	// UnityEngine.Texture SmartLocalization.LocalizedObject::thisTexture
	Texture_t3661962703 * ___thisTexture_6;
	// UnityEngine.TextAsset SmartLocalization.LocalizedObject::thisTextAsset
	TextAsset_t3022178571 * ___thisTextAsset_7;
	// System.Boolean SmartLocalization.LocalizedObject::overrideLocalizedObject
	bool ___overrideLocalizedObject_8;
	// System.String SmartLocalization.LocalizedObject::overrideObjectLanguageCode
	String_t* ___overrideObjectLanguageCode_9;

public:
	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___objectType_2)); }
	inline int32_t get_objectType_2() const { return ___objectType_2; }
	inline int32_t* get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(int32_t value)
	{
		___objectType_2 = value;
	}

	inline static int32_t get_offset_of_textValue_3() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___textValue_3)); }
	inline String_t* get_textValue_3() const { return ___textValue_3; }
	inline String_t** get_address_of_textValue_3() { return &___textValue_3; }
	inline void set_textValue_3(String_t* value)
	{
		___textValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_3), value);
	}

	inline static int32_t get_offset_of_thisGameObject_4() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisGameObject_4)); }
	inline GameObject_t1113636619 * get_thisGameObject_4() const { return ___thisGameObject_4; }
	inline GameObject_t1113636619 ** get_address_of_thisGameObject_4() { return &___thisGameObject_4; }
	inline void set_thisGameObject_4(GameObject_t1113636619 * value)
	{
		___thisGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisGameObject_4), value);
	}

	inline static int32_t get_offset_of_thisAudioClip_5() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisAudioClip_5)); }
	inline AudioClip_t3680889665 * get_thisAudioClip_5() const { return ___thisAudioClip_5; }
	inline AudioClip_t3680889665 ** get_address_of_thisAudioClip_5() { return &___thisAudioClip_5; }
	inline void set_thisAudioClip_5(AudioClip_t3680889665 * value)
	{
		___thisAudioClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___thisAudioClip_5), value);
	}

	inline static int32_t get_offset_of_thisTexture_6() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisTexture_6)); }
	inline Texture_t3661962703 * get_thisTexture_6() const { return ___thisTexture_6; }
	inline Texture_t3661962703 ** get_address_of_thisTexture_6() { return &___thisTexture_6; }
	inline void set_thisTexture_6(Texture_t3661962703 * value)
	{
		___thisTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___thisTexture_6), value);
	}

	inline static int32_t get_offset_of_thisTextAsset_7() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisTextAsset_7)); }
	inline TextAsset_t3022178571 * get_thisTextAsset_7() const { return ___thisTextAsset_7; }
	inline TextAsset_t3022178571 ** get_address_of_thisTextAsset_7() { return &___thisTextAsset_7; }
	inline void set_thisTextAsset_7(TextAsset_t3022178571 * value)
	{
		___thisTextAsset_7 = value;
		Il2CppCodeGenWriteBarrier((&___thisTextAsset_7), value);
	}

	inline static int32_t get_offset_of_overrideLocalizedObject_8() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___overrideLocalizedObject_8)); }
	inline bool get_overrideLocalizedObject_8() const { return ___overrideLocalizedObject_8; }
	inline bool* get_address_of_overrideLocalizedObject_8() { return &___overrideLocalizedObject_8; }
	inline void set_overrideLocalizedObject_8(bool value)
	{
		___overrideLocalizedObject_8 = value;
	}

	inline static int32_t get_offset_of_overrideObjectLanguageCode_9() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___overrideObjectLanguageCode_9)); }
	inline String_t* get_overrideObjectLanguageCode_9() const { return ___overrideObjectLanguageCode_9; }
	inline String_t** get_address_of_overrideObjectLanguageCode_9() { return &___overrideObjectLanguageCode_9; }
	inline void set_overrideObjectLanguageCode_9(String_t* value)
	{
		___overrideObjectLanguageCode_9 = value;
		Il2CppCodeGenWriteBarrier((&___overrideObjectLanguageCode_9), value);
	}
};

struct LocalizedObject_t1409469237_StaticFields
{
public:
	// System.String SmartLocalization.LocalizedObject::keyTypeIdentifier
	String_t* ___keyTypeIdentifier_0;
	// System.String SmartLocalization.LocalizedObject::endBracket
	String_t* ___endBracket_1;

public:
	inline static int32_t get_offset_of_keyTypeIdentifier_0() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237_StaticFields, ___keyTypeIdentifier_0)); }
	inline String_t* get_keyTypeIdentifier_0() const { return ___keyTypeIdentifier_0; }
	inline String_t** get_address_of_keyTypeIdentifier_0() { return &___keyTypeIdentifier_0; }
	inline void set_keyTypeIdentifier_0(String_t* value)
	{
		___keyTypeIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyTypeIdentifier_0), value);
	}

	inline static int32_t get_offset_of_endBracket_1() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237_StaticFields, ___endBracket_1)); }
	inline String_t* get_endBracket_1() const { return ___endBracket_1; }
	inline String_t** get_address_of_endBracket_1() { return &___endBracket_1; }
	inline void set_endBracket_1(String_t* value)
	{
		___endBracket_1 = value;
		Il2CppCodeGenWriteBarrier((&___endBracket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDOBJECT_T1409469237_H
#ifndef FIREBASEMESSAGE_T2372265022_H
#define FIREBASEMESSAGE_T2372265022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessage
struct  FirebaseMessage_t2372265022  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseMessage::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseMessage::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseMessage_t2372265022, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseMessage_t2372265022, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGE_T2372265022_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MOBILEINPUT_T2025745297_H
#define MOBILEINPUT_T2025745297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t2025745297  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T2025745297_H
#ifndef U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#define U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0
struct  U3CDragObjectU3Ec__Iterator0_t4151609119  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldDrag>__0
	float ___U3ColdDragU3E__0_0;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldAngularDrag>__0
	float ___U3ColdAngularDragU3E__0_1;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<mainCamera>__0
	Camera_t4157153871 * ___U3CmainCameraU3E__0_2;
	// UnityEngine.Ray UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<ray>__1
	Ray_t3785851493  ___U3CrayU3E__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::distance
	float ___distance_4;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$this
	DragRigidbody_t1600652016 * ___U24this_5;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3ColdDragU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdDragU3E__0_0)); }
	inline float get_U3ColdDragU3E__0_0() const { return ___U3ColdDragU3E__0_0; }
	inline float* get_address_of_U3ColdDragU3E__0_0() { return &___U3ColdDragU3E__0_0; }
	inline void set_U3ColdDragU3E__0_0(float value)
	{
		___U3ColdDragU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdAngularDragU3E__0_1)); }
	inline float get_U3ColdAngularDragU3E__0_1() const { return ___U3ColdAngularDragU3E__0_1; }
	inline float* get_address_of_U3ColdAngularDragU3E__0_1() { return &___U3ColdAngularDragU3E__0_1; }
	inline void set_U3ColdAngularDragU3E__0_1(float value)
	{
		___U3ColdAngularDragU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CmainCameraU3E__0_2)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E__0_2() const { return ___U3CmainCameraU3E__0_2; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E__0_2() { return &___U3CmainCameraU3E__0_2; }
	inline void set_U3CmainCameraU3E__0_2(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CrayU3E__1_3)); }
	inline Ray_t3785851493  get_U3CrayU3E__1_3() const { return ___U3CrayU3E__1_3; }
	inline Ray_t3785851493 * get_address_of_U3CrayU3E__1_3() { return &___U3CrayU3E__1_3; }
	inline void set_U3CrayU3E__1_3(Ray_t3785851493  value)
	{
		___U3CrayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24this_5)); }
	inline DragRigidbody_t1600652016 * get_U24this_5() const { return ___U24this_5; }
	inline DragRigidbody_t1600652016 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragRigidbody_t1600652016 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifndef FIREBASENOTIFICATION_T1925808173_H
#define FIREBASENOTIFICATION_T1925808173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseNotification
struct  FirebaseNotification_t1925808173  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseNotification::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseNotification::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseNotification_t1925808173, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseNotification_t1925808173, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASENOTIFICATION_T1925808173_H
#ifndef CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#define CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.ChangeLanguageEventHandler
struct  ChangeLanguageEventHandler_t2032193146  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#ifndef TOKENRECEIVEDDELEGATE_T1016457320_H
#define TOKENRECEIVEDDELEGATE_T1016457320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct  TokenReceivedDelegate_t1016457320  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENRECEIVEDDELEGATE_T1016457320_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LOCALIZEDGUITEXTURE_T2717994319_H
#define LOCALIZEDGUITEXTURE_T2717994319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedGUITexture
struct  LocalizedGUITexture_t2717994319  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedGUITexture::localizedKey
	String_t* ___localizedKey_2;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedGUITexture_t2717994319, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDGUITEXTURE_T2717994319_H
#ifndef LOCALIZEDAUDIOSOURCE_T267215401_H
#define LOCALIZEDAUDIOSOURCE_T267215401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedAudioSource
struct  LocalizedAudioSource_t267215401  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedAudioSource::localizedKey
	String_t* ___localizedKey_2;
	// UnityEngine.AudioClip SmartLocalization.LocalizedAudioSource::audioClip
	AudioClip_t3680889665 * ___audioClip_3;
	// UnityEngine.AudioSource SmartLocalization.LocalizedAudioSource::audioSource
	AudioSource_t3935305588 * ___audioSource_4;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}

	inline static int32_t get_offset_of_audioClip_3() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___audioClip_3)); }
	inline AudioClip_t3680889665 * get_audioClip_3() const { return ___audioClip_3; }
	inline AudioClip_t3680889665 ** get_address_of_audioClip_3() { return &___audioClip_3; }
	inline void set_audioClip_3(AudioClip_t3680889665 * value)
	{
		___audioClip_3 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_3), value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___audioSource_4)); }
	inline AudioSource_t3935305588 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t3935305588 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDAUDIOSOURCE_T267215401_H
#ifndef LOCALIZEDGUITEXT_T2415330885_H
#define LOCALIZEDGUITEXT_T2415330885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedGUIText
struct  LocalizedGUIText_t2415330885  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedGUIText::localizedKey
	String_t* ___localizedKey_2;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedGUIText_t2415330885, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDGUITEXT_T2415330885_H
#ifndef UNIWEBVIEWEVENTPROCESSOR_T530563931_H
#define UNIWEBVIEWEVENTPROCESSOR_T530563931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewEventProcessor
struct  UniWebViewEventProcessor_t530563931  : public MonoBehaviour_t3962482529
{
public:
	// System.Object UniWebViewEventProcessor::_queueLock
	RuntimeObject * ____queueLock_2;
	// System.Collections.Generic.List`1<System.Action> UniWebViewEventProcessor::_queuedEvents
	List_1_t2736452219 * ____queuedEvents_3;
	// System.Collections.Generic.List`1<System.Action> UniWebViewEventProcessor::_executingEvents
	List_1_t2736452219 * ____executingEvents_4;

public:
	inline static int32_t get_offset_of__queueLock_2() { return static_cast<int32_t>(offsetof(UniWebViewEventProcessor_t530563931, ____queueLock_2)); }
	inline RuntimeObject * get__queueLock_2() const { return ____queueLock_2; }
	inline RuntimeObject ** get_address_of__queueLock_2() { return &____queueLock_2; }
	inline void set__queueLock_2(RuntimeObject * value)
	{
		____queueLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____queueLock_2), value);
	}

	inline static int32_t get_offset_of__queuedEvents_3() { return static_cast<int32_t>(offsetof(UniWebViewEventProcessor_t530563931, ____queuedEvents_3)); }
	inline List_1_t2736452219 * get__queuedEvents_3() const { return ____queuedEvents_3; }
	inline List_1_t2736452219 ** get_address_of__queuedEvents_3() { return &____queuedEvents_3; }
	inline void set__queuedEvents_3(List_1_t2736452219 * value)
	{
		____queuedEvents_3 = value;
		Il2CppCodeGenWriteBarrier((&____queuedEvents_3), value);
	}

	inline static int32_t get_offset_of__executingEvents_4() { return static_cast<int32_t>(offsetof(UniWebViewEventProcessor_t530563931, ____executingEvents_4)); }
	inline List_1_t2736452219 * get__executingEvents_4() const { return ____executingEvents_4; }
	inline List_1_t2736452219 ** get_address_of__executingEvents_4() { return &____executingEvents_4; }
	inline void set__executingEvents_4(List_1_t2736452219 * value)
	{
		____executingEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&____executingEvents_4), value);
	}
};

struct UniWebViewEventProcessor_t530563931_StaticFields
{
public:
	// UniWebViewEventProcessor UniWebViewEventProcessor::_instance
	UniWebViewEventProcessor_t530563931 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(UniWebViewEventProcessor_t530563931_StaticFields, ____instance_5)); }
	inline UniWebViewEventProcessor_t530563931 * get__instance_5() const { return ____instance_5; }
	inline UniWebViewEventProcessor_t530563931 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(UniWebViewEventProcessor_t530563931 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((&____instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWEVENTPROCESSOR_T530563931_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_3)); }
	inline Vector3_t3722313464  get_offset_3() const { return ___offset_3; }
	inline Vector3_t3722313464 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t3722313464  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef EVENTSYSTEMCHECKER_T1882757729_H
#define EVENTSYSTEMCHECKER_T1882757729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t1882757729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T1882757729_H
#ifndef LANGUAGEMANAGER_T2767934455_H
#define LANGUAGEMANAGER_T2767934455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager
struct  LanguageManager_t2767934455  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::serializedKeys
	List_1_t3319525431 * ___serializedKeys_6;
	// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject> SmartLocalization.LanguageManager::serializedValues
	List_1_t2881543979 * ___serializedValues_7;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::serializedCulture
	SmartCultureInfo_t2060094783 * ___serializedCulture_8;
	// SmartLocalization.ChangeLanguageEventHandler SmartLocalization.LanguageManager::OnChangeLanguage
	ChangeLanguageEventHandler_t2032193146 * ___OnChangeLanguage_9;
	// System.String SmartLocalization.LanguageManager::defaultLanguage
	String_t* ___defaultLanguage_10;
	// SmartLocalization.SmartCultureInfoCollection SmartLocalization.LanguageManager::availableLanguages
	SmartCultureInfoCollection_t1809038765 * ___availableLanguages_11;
	// SmartLocalization.LanguageDataHandler SmartLocalization.LanguageManager::languageDataHandler
	LanguageDataHandler_t3889722316 * ___languageDataHandler_12;

public:
	inline static int32_t get_offset_of_serializedKeys_6() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedKeys_6)); }
	inline List_1_t3319525431 * get_serializedKeys_6() const { return ___serializedKeys_6; }
	inline List_1_t3319525431 ** get_address_of_serializedKeys_6() { return &___serializedKeys_6; }
	inline void set_serializedKeys_6(List_1_t3319525431 * value)
	{
		___serializedKeys_6 = value;
		Il2CppCodeGenWriteBarrier((&___serializedKeys_6), value);
	}

	inline static int32_t get_offset_of_serializedValues_7() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedValues_7)); }
	inline List_1_t2881543979 * get_serializedValues_7() const { return ___serializedValues_7; }
	inline List_1_t2881543979 ** get_address_of_serializedValues_7() { return &___serializedValues_7; }
	inline void set_serializedValues_7(List_1_t2881543979 * value)
	{
		___serializedValues_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializedValues_7), value);
	}

	inline static int32_t get_offset_of_serializedCulture_8() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedCulture_8)); }
	inline SmartCultureInfo_t2060094783 * get_serializedCulture_8() const { return ___serializedCulture_8; }
	inline SmartCultureInfo_t2060094783 ** get_address_of_serializedCulture_8() { return &___serializedCulture_8; }
	inline void set_serializedCulture_8(SmartCultureInfo_t2060094783 * value)
	{
		___serializedCulture_8 = value;
		Il2CppCodeGenWriteBarrier((&___serializedCulture_8), value);
	}

	inline static int32_t get_offset_of_OnChangeLanguage_9() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___OnChangeLanguage_9)); }
	inline ChangeLanguageEventHandler_t2032193146 * get_OnChangeLanguage_9() const { return ___OnChangeLanguage_9; }
	inline ChangeLanguageEventHandler_t2032193146 ** get_address_of_OnChangeLanguage_9() { return &___OnChangeLanguage_9; }
	inline void set_OnChangeLanguage_9(ChangeLanguageEventHandler_t2032193146 * value)
	{
		___OnChangeLanguage_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeLanguage_9), value);
	}

	inline static int32_t get_offset_of_defaultLanguage_10() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___defaultLanguage_10)); }
	inline String_t* get_defaultLanguage_10() const { return ___defaultLanguage_10; }
	inline String_t** get_address_of_defaultLanguage_10() { return &___defaultLanguage_10; }
	inline void set_defaultLanguage_10(String_t* value)
	{
		___defaultLanguage_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultLanguage_10), value);
	}

	inline static int32_t get_offset_of_availableLanguages_11() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___availableLanguages_11)); }
	inline SmartCultureInfoCollection_t1809038765 * get_availableLanguages_11() const { return ___availableLanguages_11; }
	inline SmartCultureInfoCollection_t1809038765 ** get_address_of_availableLanguages_11() { return &___availableLanguages_11; }
	inline void set_availableLanguages_11(SmartCultureInfoCollection_t1809038765 * value)
	{
		___availableLanguages_11 = value;
		Il2CppCodeGenWriteBarrier((&___availableLanguages_11), value);
	}

	inline static int32_t get_offset_of_languageDataHandler_12() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___languageDataHandler_12)); }
	inline LanguageDataHandler_t3889722316 * get_languageDataHandler_12() const { return ___languageDataHandler_12; }
	inline LanguageDataHandler_t3889722316 ** get_address_of_languageDataHandler_12() { return &___languageDataHandler_12; }
	inline void set_languageDataHandler_12(LanguageDataHandler_t3889722316 * value)
	{
		___languageDataHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___languageDataHandler_12), value);
	}
};

struct LanguageManager_t2767934455_StaticFields
{
public:
	// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::instance
	LanguageManager_t2767934455 * ___instance_2;
	// System.Boolean SmartLocalization.LanguageManager::IsQuitting
	bool ___IsQuitting_3;
	// System.Boolean SmartLocalization.LanguageManager::DontDestroyOnLoadToggle
	bool ___DontDestroyOnLoadToggle_4;
	// System.Boolean SmartLocalization.LanguageManager::DidSetDontDestroyOnLoad
	bool ___DidSetDontDestroyOnLoad_5;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___instance_2)); }
	inline LanguageManager_t2767934455 * get_instance_2() const { return ___instance_2; }
	inline LanguageManager_t2767934455 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LanguageManager_t2767934455 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_IsQuitting_3() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___IsQuitting_3)); }
	inline bool get_IsQuitting_3() const { return ___IsQuitting_3; }
	inline bool* get_address_of_IsQuitting_3() { return &___IsQuitting_3; }
	inline void set_IsQuitting_3(bool value)
	{
		___IsQuitting_3 = value;
	}

	inline static int32_t get_offset_of_DontDestroyOnLoadToggle_4() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___DontDestroyOnLoadToggle_4)); }
	inline bool get_DontDestroyOnLoadToggle_4() const { return ___DontDestroyOnLoadToggle_4; }
	inline bool* get_address_of_DontDestroyOnLoadToggle_4() { return &___DontDestroyOnLoadToggle_4; }
	inline void set_DontDestroyOnLoadToggle_4(bool value)
	{
		___DontDestroyOnLoadToggle_4 = value;
	}

	inline static int32_t get_offset_of_DidSetDontDestroyOnLoad_5() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___DidSetDontDestroyOnLoad_5)); }
	inline bool get_DidSetDontDestroyOnLoad_5() const { return ___DidSetDontDestroyOnLoad_5; }
	inline bool* get_address_of_DidSetDontDestroyOnLoad_5() { return &___DidSetDontDestroyOnLoad_5; }
	inline void set_DidSetDontDestroyOnLoad_5(bool value)
	{
		___DidSetDontDestroyOnLoad_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEMANAGER_T2767934455_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_2;

public:
	inline static int32_t get_offset_of_m_ReplacementList_2() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_2)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_2() const { return ___m_ReplacementList_2; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_2() { return &___m_ReplacementList_2; }
	inline void set_m_ReplacementList_2(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef ALPHABUTTONCLICKMASK_T141136539_H
#define ALPHABUTTONCLICKMASK_T141136539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t141136539  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t2670269651 * ____image_2;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t141136539, ____image_2)); }
	inline Image_t2670269651 * get__image_2() const { return ____image_2; }
	inline Image_t2670269651 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2670269651 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T141136539_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_2;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_3;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_4;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_5;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_6;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_2)); }
	inline int32_t get_action_2() const { return ___action_2; }
	inline int32_t* get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(int32_t value)
	{
		___action_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_3)); }
	inline Object_t631007953 * get_target_3() const { return ___target_3; }
	inline Object_t631007953 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Object_t631007953 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_4)); }
	inline GameObject_t1113636619 * get_source_4() const { return ___source_4; }
	inline GameObject_t1113636619 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(GameObject_t1113636619 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_triggerCount_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_5)); }
	inline int32_t get_triggerCount_5() const { return ___triggerCount_5; }
	inline int32_t* get_address_of_triggerCount_5() { return &___triggerCount_5; }
	inline void set_triggerCount_5(int32_t value)
	{
		___triggerCount_5 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_6)); }
	inline bool get_repeatTrigger_6() const { return ___repeatTrigger_6; }
	inline bool* get_address_of_repeatTrigger_6() { return &___repeatTrigger_6; }
	inline void set_repeatTrigger_6(bool value)
	{
		___repeatTrigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_2;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_3;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_4;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_5;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_2() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_2)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_2() const { return ___moveUnitsPerSecond_2; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_2() { return &___moveUnitsPerSecond_2; }
	inline void set_moveUnitsPerSecond_2(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_2), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_3() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_3)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_3() const { return ___rotateDegreesPerSecond_3; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_3() { return &___rotateDegreesPerSecond_3; }
	inline void set_rotateDegreesPerSecond_3(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_3), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_4)); }
	inline bool get_ignoreTimescale_4() const { return ___ignoreTimescale_4; }
	inline bool* get_address_of_ignoreTimescale_4() { return &___ignoreTimescale_4; }
	inline void set_ignoreTimescale_4(bool value)
	{
		___ignoreTimescale_4 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_5)); }
	inline float get_m_LastRealTime_5() const { return ___m_LastRealTime_5; }
	inline float* get_address_of_m_LastRealTime_5() { return &___m_LastRealTime_5; }
	inline void set_m_LastRealTime_5(float value)
	{
		___m_LastRealTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_2;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_3;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_12;

public:
	inline static int32_t get_offset_of_sunLight_2() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_2)); }
	inline Light_t3756812086 * get_sunLight_2() const { return ___sunLight_2; }
	inline Light_t3756812086 ** get_address_of_sunLight_2() { return &___sunLight_2; }
	inline void set_sunLight_2(Light_t3756812086 * value)
	{
		___sunLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_2), value);
	}

	inline static int32_t get_offset_of_minHeight_3() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_3)); }
	inline float get_minHeight_3() const { return ___minHeight_3; }
	inline float* get_address_of_minHeight_3() { return &___minHeight_3; }
	inline void set_minHeight_3(float value)
	{
		___minHeight_3 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_4)); }
	inline float get_minShadowDistance_4() const { return ___minShadowDistance_4; }
	inline float* get_address_of_minShadowDistance_4() { return &___minShadowDistance_4; }
	inline void set_minShadowDistance_4(float value)
	{
		___minShadowDistance_4 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_5)); }
	inline float get_minShadowBias_5() const { return ___minShadowBias_5; }
	inline float* get_address_of_minShadowBias_5() { return &___minShadowBias_5; }
	inline void set_minShadowBias_5(float value)
	{
		___minShadowBias_5 = value;
	}

	inline static int32_t get_offset_of_maxHeight_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_6)); }
	inline float get_maxHeight_6() const { return ___maxHeight_6; }
	inline float* get_address_of_maxHeight_6() { return &___maxHeight_6; }
	inline void set_maxHeight_6(float value)
	{
		___maxHeight_6 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_7)); }
	inline float get_maxShadowDistance_7() const { return ___maxShadowDistance_7; }
	inline float* get_address_of_maxShadowDistance_7() { return &___maxShadowDistance_7; }
	inline void set_maxShadowDistance_7(float value)
	{
		___maxShadowDistance_7 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_8)); }
	inline float get_maxShadowBias_8() const { return ___maxShadowBias_8; }
	inline float* get_address_of_maxShadowBias_8() { return &___maxShadowBias_8; }
	inline void set_maxShadowBias_8(float value)
	{
		___maxShadowBias_8 = value;
	}

	inline static int32_t get_offset_of_adaptTime_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_9)); }
	inline float get_adaptTime_9() const { return ___adaptTime_9; }
	inline float* get_address_of_adaptTime_9() { return &___adaptTime_9; }
	inline void set_adaptTime_9(float value)
	{
		___adaptTime_9 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_10)); }
	inline float get_m_SmoothHeight_10() const { return ___m_SmoothHeight_10; }
	inline float* get_address_of_m_SmoothHeight_10() { return &___m_SmoothHeight_10; }
	inline void set_m_SmoothHeight_10(float value)
	{
		___m_SmoothHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_11)); }
	inline float get_m_ChangeSpeed_11() const { return ___m_ChangeSpeed_11; }
	inline float* get_address_of_m_ChangeSpeed_11() { return &___m_ChangeSpeed_11; }
	inline void set_m_ChangeSpeed_11(float value)
	{
		___m_ChangeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_12)); }
	inline float get_m_OriginalStrength_12() const { return ___m_OriginalStrength_12; }
	inline float* get_address_of_m_OriginalStrength_12() { return &___m_OriginalStrength_12; }
	inline void set_m_OriginalStrength_12(float value)
	{
		___m_OriginalStrength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_8;

public:
	inline static int32_t get_offset_of_m_SpringJoint_8() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_8)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_8() const { return ___m_SpringJoint_8; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_8() { return &___m_SpringJoint_8; }
	inline void set_m_SpringJoint_8(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef TOUCHPAD_T539039257_H
#define TOUCHPAD_T539039257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t539039257  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_2;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_3;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t3722313464  ___m_StartPos_8;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t2156229523  ___m_PreviousDelta_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t3722313464  ___m_JoytickOutput_10;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_11;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_12;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_13;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_14;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_15;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_16;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t2156229523  ___m_PreviousTouchPos_17;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t3722313464  ___m_Center_18;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2670269651 * ___m_Image_19;

public:
	inline static int32_t get_offset_of_axesToUse_2() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___axesToUse_2)); }
	inline int32_t get_axesToUse_2() const { return ___axesToUse_2; }
	inline int32_t* get_address_of_axesToUse_2() { return &___axesToUse_2; }
	inline void set_axesToUse_2(int32_t value)
	{
		___axesToUse_2 = value;
	}

	inline static int32_t get_offset_of_controlStyle_3() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___controlStyle_3)); }
	inline int32_t get_controlStyle_3() const { return ___controlStyle_3; }
	inline int32_t* get_address_of_controlStyle_3() { return &___controlStyle_3; }
	inline void set_controlStyle_3(int32_t value)
	{
		___controlStyle_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_6() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Xsensitivity_6)); }
	inline float get_Xsensitivity_6() const { return ___Xsensitivity_6; }
	inline float* get_address_of_Xsensitivity_6() { return &___Xsensitivity_6; }
	inline void set_Xsensitivity_6(float value)
	{
		___Xsensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_7() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Ysensitivity_7)); }
	inline float get_Ysensitivity_7() const { return ___Ysensitivity_7; }
	inline float* get_address_of_Ysensitivity_7() { return &___Ysensitivity_7; }
	inline void set_Ysensitivity_7(float value)
	{
		___Ysensitivity_7 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_StartPos_8)); }
	inline Vector3_t3722313464  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t3722313464  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_9() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousDelta_9)); }
	inline Vector2_t2156229523  get_m_PreviousDelta_9() const { return ___m_PreviousDelta_9; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousDelta_9() { return &___m_PreviousDelta_9; }
	inline void set_m_PreviousDelta_9(Vector2_t2156229523  value)
	{
		___m_PreviousDelta_9 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_10() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_JoytickOutput_10)); }
	inline Vector3_t3722313464  get_m_JoytickOutput_10() const { return ___m_JoytickOutput_10; }
	inline Vector3_t3722313464 * get_address_of_m_JoytickOutput_10() { return &___m_JoytickOutput_10; }
	inline void set_m_JoytickOutput_10(Vector3_t3722313464  value)
	{
		___m_JoytickOutput_10 = value;
	}

	inline static int32_t get_offset_of_m_UseX_11() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseX_11)); }
	inline bool get_m_UseX_11() const { return ___m_UseX_11; }
	inline bool* get_address_of_m_UseX_11() { return &___m_UseX_11; }
	inline void set_m_UseX_11(bool value)
	{
		___m_UseX_11 = value;
	}

	inline static int32_t get_offset_of_m_UseY_12() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseY_12)); }
	inline bool get_m_UseY_12() const { return ___m_UseY_12; }
	inline bool* get_address_of_m_UseY_12() { return &___m_UseY_12; }
	inline void set_m_UseY_12(bool value)
	{
		___m_UseY_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_13() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_HorizontalVirtualAxis_13)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_13() const { return ___m_HorizontalVirtualAxis_13; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_13() { return &___m_HorizontalVirtualAxis_13; }
	inline void set_m_HorizontalVirtualAxis_13(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_14() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_VerticalVirtualAxis_14)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_14() const { return ___m_VerticalVirtualAxis_14; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_14() { return &___m_VerticalVirtualAxis_14; }
	inline void set_m_VerticalVirtualAxis_14(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_14), value);
	}

	inline static int32_t get_offset_of_m_Dragging_15() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Dragging_15)); }
	inline bool get_m_Dragging_15() const { return ___m_Dragging_15; }
	inline bool* get_address_of_m_Dragging_15() { return &___m_Dragging_15; }
	inline void set_m_Dragging_15(bool value)
	{
		___m_Dragging_15 = value;
	}

	inline static int32_t get_offset_of_m_Id_16() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Id_16)); }
	inline int32_t get_m_Id_16() const { return ___m_Id_16; }
	inline int32_t* get_address_of_m_Id_16() { return &___m_Id_16; }
	inline void set_m_Id_16(int32_t value)
	{
		___m_Id_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_17() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousTouchPos_17)); }
	inline Vector2_t2156229523  get_m_PreviousTouchPos_17() const { return ___m_PreviousTouchPos_17; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousTouchPos_17() { return &___m_PreviousTouchPos_17; }
	inline void set_m_PreviousTouchPos_17(Vector2_t2156229523  value)
	{
		___m_PreviousTouchPos_17 = value;
	}

	inline static int32_t get_offset_of_m_Center_18() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Center_18)); }
	inline Vector3_t3722313464  get_m_Center_18() const { return ___m_Center_18; }
	inline Vector3_t3722313464 * get_address_of_m_Center_18() { return &___m_Center_18; }
	inline void set_m_Center_18(Vector3_t3722313464  value)
	{
		___m_Center_18 = value;
	}

	inline static int32_t get_offset_of_m_Image_19() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Image_19)); }
	inline Image_t2670269651 * get_m_Image_19() const { return ___m_Image_19; }
	inline Image_t2670269651 ** get_address_of_m_Image_19() { return &___m_Image_19; }
	inline void set_m_Image_19(Image_t2670269651 * value)
	{
		___m_Image_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T539039257_H
#ifndef SIMPLEACTIVATORMENU_T1387811551_H
#define SIMPLEACTIVATORMENU_T1387811551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t1387811551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t402233326 * ___camSwitchButton_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t3328599146* ___objects_3;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;

public:
	inline static int32_t get_offset_of_camSwitchButton_2() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___camSwitchButton_2)); }
	inline GUIText_t402233326 * get_camSwitchButton_2() const { return ___camSwitchButton_2; }
	inline GUIText_t402233326 ** get_address_of_camSwitchButton_2() { return &___camSwitchButton_2; }
	inline void set_camSwitchButton_2(GUIText_t402233326 * value)
	{
		___camSwitchButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___objects_3)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_3() const { return ___objects_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___m_CurrentActiveObject_4)); }
	inline int32_t get_m_CurrentActiveObject_4() const { return ___m_CurrentActiveObject_4; }
	inline int32_t* get_address_of_m_CurrentActiveObject_4() { return &___m_CurrentActiveObject_4; }
	inline void set_m_CurrentActiveObject_4(int32_t value)
	{
		___m_CurrentActiveObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T1387811551_H
#ifndef SIMPLEMOUSEROTATOR_T2364742953_H
#define SIMPLEMOUSEROTATOR_T2364742953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2364742953  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t2156229523  ___rotationRange_2;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_4;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_5;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_7;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t3722313464  ___m_TargetAngles_8;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_10;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_11;

public:
	inline static int32_t get_offset_of_rotationRange_2() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationRange_2)); }
	inline Vector2_t2156229523  get_rotationRange_2() const { return ___rotationRange_2; }
	inline Vector2_t2156229523 * get_address_of_rotationRange_2() { return &___rotationRange_2; }
	inline void set_rotationRange_2(Vector2_t2156229523  value)
	{
		___rotationRange_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_dampingTime_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___dampingTime_4)); }
	inline float get_dampingTime_4() const { return ___dampingTime_4; }
	inline float* get_address_of_dampingTime_4() { return &___dampingTime_4; }
	inline void set_dampingTime_4(float value)
	{
		___dampingTime_4 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroVerticalOnMobile_5)); }
	inline bool get_autoZeroVerticalOnMobile_5() const { return ___autoZeroVerticalOnMobile_5; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_5() { return &___autoZeroVerticalOnMobile_5; }
	inline void set_autoZeroVerticalOnMobile_5(bool value)
	{
		___autoZeroVerticalOnMobile_5 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroHorizontalOnMobile_6)); }
	inline bool get_autoZeroHorizontalOnMobile_6() const { return ___autoZeroHorizontalOnMobile_6; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_6() { return &___autoZeroHorizontalOnMobile_6; }
	inline void set_autoZeroHorizontalOnMobile_6(bool value)
	{
		___autoZeroHorizontalOnMobile_6 = value;
	}

	inline static int32_t get_offset_of_relative_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___relative_7)); }
	inline bool get_relative_7() const { return ___relative_7; }
	inline bool* get_address_of_relative_7() { return &___relative_7; }
	inline void set_relative_7(bool value)
	{
		___relative_7 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_TargetAngles_8)); }
	inline Vector3_t3722313464  get_m_TargetAngles_8() const { return ___m_TargetAngles_8; }
	inline Vector3_t3722313464 * get_address_of_m_TargetAngles_8() { return &___m_TargetAngles_8; }
	inline void set_m_TargetAngles_8(Vector3_t3722313464  value)
	{
		___m_TargetAngles_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowAngles_9)); }
	inline Vector3_t3722313464  get_m_FollowAngles_9() const { return ___m_FollowAngles_9; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_9() { return &___m_FollowAngles_9; }
	inline void set_m_FollowAngles_9(Vector3_t3722313464  value)
	{
		___m_FollowAngles_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowVelocity_10)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_10() const { return ___m_FollowVelocity_10; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_10() { return &___m_FollowVelocity_10; }
	inline void set_m_FollowVelocity_10(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_OriginalRotation_11)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2364742953_H
#ifndef SMOOTHFOLLOW_T4204731361_H
#define SMOOTHFOLLOW_T4204731361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t4204731361  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3600365921 * ___target_2;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___rotationDamping_5)); }
	inline float get_rotationDamping_5() const { return ___rotationDamping_5; }
	inline float* get_address_of_rotationDamping_5() { return &___rotationDamping_5; }
	inline void set_rotationDamping_5(float value)
	{
		___rotationDamping_5 = value;
	}

	inline static int32_t get_offset_of_heightDamping_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___heightDamping_6)); }
	inline float get_heightDamping_6() const { return ___heightDamping_6; }
	inline float* get_address_of_heightDamping_6() { return &___heightDamping_6; }
	inline void set_heightDamping_6(float value)
	{
		___heightDamping_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T4204731361_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_2;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_5;

public:
	inline static int32_t get_offset_of_originalPosition_2() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_2)); }
	inline Vector3_t3722313464  get_originalPosition_2() const { return ___originalPosition_2; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_2() { return &___originalPosition_2; }
	inline void set_originalPosition_2(Vector3_t3722313464  value)
	{
		___originalPosition_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_3)); }
	inline Quaternion_t2301928331  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t2301928331  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalStructure_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_4)); }
	inline List_1_t777473367 * get_originalStructure_4() const { return ___originalStructure_4; }
	inline List_1_t777473367 ** get_address_of_originalStructure_4() { return &___originalStructure_4; }
	inline void set_originalStructure_4(List_1_t777473367 * value)
	{
		___originalStructure_4 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_4), value);
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_5)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_4;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_5;

public:
	inline static int32_t get_offset_of_minDuration_2() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_2)); }
	inline float get_minDuration_2() const { return ___minDuration_2; }
	inline float* get_address_of_minDuration_2() { return &___minDuration_2; }
	inline void set_minDuration_2(float value)
	{
		___minDuration_2 = value;
	}

	inline static int32_t get_offset_of_maxDuration_3() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_3)); }
	inline float get_maxDuration_3() const { return ___maxDuration_3; }
	inline float* get_address_of_maxDuration_3() { return &___maxDuration_3; }
	inline void set_maxDuration_3(float value)
	{
		___maxDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_4)); }
	inline float get_m_MaxLifetime_4() const { return ___m_MaxLifetime_4; }
	inline float* get_address_of_m_MaxLifetime_4() { return &___m_MaxLifetime_4; }
	inline void set_m_MaxLifetime_4(float value)
	{
		___m_MaxLifetime_4 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_5)); }
	inline bool get_m_EarlyStop_5() const { return ___m_EarlyStop_5; }
	inline bool* get_address_of_m_EarlyStop_5() { return &___m_EarlyStop_5; }
	inline void set_m_EarlyStop_5(bool value)
	{
		___m_EarlyStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef MOBILECONTROLRIG_T1964600252_H
#define MOBILECONTROLRIG_T1964600252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t1964600252  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T1964600252_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_3;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_4;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_5;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_2() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_2)); }
	inline int32_t get_m_BuildTargetGroup_2() const { return ___m_BuildTargetGroup_2; }
	inline int32_t* get_address_of_m_BuildTargetGroup_2() { return &___m_BuildTargetGroup_2; }
	inline void set_m_BuildTargetGroup_2(int32_t value)
	{
		___m_BuildTargetGroup_2 = value;
	}

	inline static int32_t get_offset_of_m_Content_3() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_3() const { return ___m_Content_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_3() { return &___m_Content_3; }
	inline void set_m_Content_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_3), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_4)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_4() const { return ___m_MonoBehaviours_4; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_4() { return &___m_MonoBehaviours_4; }
	inline void set_m_MonoBehaviours_4(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_4), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_5)); }
	inline bool get_m_ChildrenOfThisObject_5() const { return ___m_ChildrenOfThisObject_5; }
	inline bool* get_address_of_m_ChildrenOfThisObject_5() { return &___m_ChildrenOfThisObject_5; }
	inline void set_m_ChildrenOfThisObject_5(bool value)
	{
		___m_ChildrenOfThisObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef TIMEDOBJECTACTIVATOR_T1846709985_H
#define TIMEDOBJECTACTIVATOR_T1846709985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1846709985  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t3168066469 * ___entries_2;

public:
	inline static int32_t get_offset_of_entries_2() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1846709985, ___entries_2)); }
	inline Entries_t3168066469 * get_entries_2() const { return ___entries_2; }
	inline Entries_t3168066469 ** get_address_of_entries_2() { return &___entries_2; }
	inline void set_entries_2(Entries_t3168066469 * value)
	{
		___entries_2 = value;
		Il2CppCodeGenWriteBarrier((&___entries_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1846709985_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef BUTTONHANDLER_T823762219_H
#define BUTTONHANDLER_T823762219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t823762219  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(ButtonHandler_t823762219, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T823762219_H
#ifndef AXISTOUCHBUTTON_T3522881333_H
#define AXISTOUCHBUTTON_T3522881333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3522881333  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_2;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_5;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3522881333 * ___m_PairedWith_6;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t4087348596 * ___m_Axis_7;

public:
	inline static int32_t get_offset_of_axisName_2() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisName_2)); }
	inline String_t* get_axisName_2() const { return ___axisName_2; }
	inline String_t** get_address_of_axisName_2() { return &___axisName_2; }
	inline void set_axisName_2(String_t* value)
	{
		___axisName_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_2), value);
	}

	inline static int32_t get_offset_of_axisValue_3() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisValue_3)); }
	inline float get_axisValue_3() const { return ___axisValue_3; }
	inline float* get_address_of_axisValue_3() { return &___axisValue_3; }
	inline void set_axisValue_3(float value)
	{
		___axisValue_3 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___responseSpeed_4)); }
	inline float get_responseSpeed_4() const { return ___responseSpeed_4; }
	inline float* get_address_of_responseSpeed_4() { return &___responseSpeed_4; }
	inline void set_responseSpeed_4(float value)
	{
		___responseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___returnToCentreSpeed_5)); }
	inline float get_returnToCentreSpeed_5() const { return ___returnToCentreSpeed_5; }
	inline float* get_address_of_returnToCentreSpeed_5() { return &___returnToCentreSpeed_5; }
	inline void set_returnToCentreSpeed_5(float value)
	{
		___returnToCentreSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_PairedWith_6)); }
	inline AxisTouchButton_t3522881333 * get_m_PairedWith_6() const { return ___m_PairedWith_6; }
	inline AxisTouchButton_t3522881333 ** get_address_of_m_PairedWith_6() { return &___m_PairedWith_6; }
	inline void set_m_PairedWith_6(AxisTouchButton_t3522881333 * value)
	{
		___m_PairedWith_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_6), value);
	}

	inline static int32_t get_offset_of_m_Axis_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_Axis_7)); }
	inline VirtualAxis_t4087348596 * get_m_Axis_7() const { return ___m_Axis_7; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_Axis_7() { return &___m_Axis_7; }
	inline void set_m_Axis_7(VirtualAxis_t4087348596 * value)
	{
		___m_Axis_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3522881333_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_3;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_4;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_5;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t1901882714 * ___m_Text_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_3() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_3)); }
	inline int32_t get_m_FpsAccumulator_3() const { return ___m_FpsAccumulator_3; }
	inline int32_t* get_address_of_m_FpsAccumulator_3() { return &___m_FpsAccumulator_3; }
	inline void set_m_FpsAccumulator_3(int32_t value)
	{
		___m_FpsAccumulator_3 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_4() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_4)); }
	inline float get_m_FpsNextPeriod_4() const { return ___m_FpsNextPeriod_4; }
	inline float* get_address_of_m_FpsNextPeriod_4() { return &___m_FpsNextPeriod_4; }
	inline void set_m_FpsNextPeriod_4(float value)
	{
		___m_FpsNextPeriod_4 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_5)); }
	inline int32_t get_m_CurrentFps_5() const { return ___m_CurrentFps_5; }
	inline int32_t* get_address_of_m_CurrentFps_5() { return &___m_CurrentFps_5; }
	inline void set_m_CurrentFps_5(int32_t value)
	{
		___m_CurrentFps_5 = value;
	}

	inline static int32_t get_offset_of_m_Text_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_Text_7)); }
	inline Text_t1901882714 * get_m_Text_7() const { return ___m_Text_7; }
	inline Text_t1901882714 ** get_address_of_m_Text_7() { return &___m_Text_7; }
	inline void set_m_Text_7(Text_t1901882714 * value)
	{
		___m_Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef JOYSTICK_T2204371675_H
#define JOYSTICK_T2204371675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t2204371675  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_2;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_3;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_5;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t3722313464  ___m_StartPos_6;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_7;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_9;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_10;

public:
	inline static int32_t get_offset_of_MovementRange_2() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___MovementRange_2)); }
	inline int32_t get_MovementRange_2() const { return ___MovementRange_2; }
	inline int32_t* get_address_of_MovementRange_2() { return &___MovementRange_2; }
	inline void set_MovementRange_2(int32_t value)
	{
		___MovementRange_2 = value;
	}

	inline static int32_t get_offset_of_axesToUse_3() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___axesToUse_3)); }
	inline int32_t get_axesToUse_3() const { return ___axesToUse_3; }
	inline int32_t* get_address_of_axesToUse_3() { return &___axesToUse_3; }
	inline void set_axesToUse_3(int32_t value)
	{
		___axesToUse_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_m_StartPos_6() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_StartPos_6)); }
	inline Vector3_t3722313464  get_m_StartPos_6() const { return ___m_StartPos_6; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_6() { return &___m_StartPos_6; }
	inline void set_m_StartPos_6(Vector3_t3722313464  value)
	{
		___m_StartPos_6 = value;
	}

	inline static int32_t get_offset_of_m_UseX_7() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseX_7)); }
	inline bool get_m_UseX_7() const { return ___m_UseX_7; }
	inline bool* get_address_of_m_UseX_7() { return &___m_UseX_7; }
	inline void set_m_UseX_7(bool value)
	{
		___m_UseX_7 = value;
	}

	inline static int32_t get_offset_of_m_UseY_8() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseY_8)); }
	inline bool get_m_UseY_8() const { return ___m_UseY_8; }
	inline bool* get_address_of_m_UseY_8() { return &___m_UseY_8; }
	inline void set_m_UseY_8(bool value)
	{
		___m_UseY_8 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_9() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_HorizontalVirtualAxis_9)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_9() const { return ___m_HorizontalVirtualAxis_9; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_9() { return &___m_HorizontalVirtualAxis_9; }
	inline void set_m_HorizontalVirtualAxis_9(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_9), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_10() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_VerticalVirtualAxis_10)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_10() const { return ___m_VerticalVirtualAxis_10; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_10() { return &___m_VerticalVirtualAxis_10; }
	inline void set_m_VerticalVirtualAxis_10(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2204371675_H
#ifndef TILTINPUT_T1639936653_H
#define TILTINPUT_T1639936653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t1639936653  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t3982445645 * ___mapping_2;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_5;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t4087348596 * ___m_SteerAxis_6;

public:
	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___mapping_2)); }
	inline AxisMapping_t3982445645 * get_mapping_2() const { return ___mapping_2; }
	inline AxisMapping_t3982445645 ** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(AxisMapping_t3982445645 * value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_3() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___tiltAroundAxis_3)); }
	inline int32_t get_tiltAroundAxis_3() const { return ___tiltAroundAxis_3; }
	inline int32_t* get_address_of_tiltAroundAxis_3() { return &___tiltAroundAxis_3; }
	inline void set_tiltAroundAxis_3(int32_t value)
	{
		___tiltAroundAxis_3 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_4() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___fullTiltAngle_4)); }
	inline float get_fullTiltAngle_4() const { return ___fullTiltAngle_4; }
	inline float* get_address_of_fullTiltAngle_4() { return &___fullTiltAngle_4; }
	inline void set_fullTiltAngle_4(float value)
	{
		___fullTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_5() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___centreAngleOffset_5)); }
	inline float get_centreAngleOffset_5() const { return ___centreAngleOffset_5; }
	inline float* get_address_of_centreAngleOffset_5() { return &___centreAngleOffset_5; }
	inline void set_centreAngleOffset_5(float value)
	{
		___centreAngleOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_6() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___m_SteerAxis_6)); }
	inline VirtualAxis_t4087348596 * get_m_SteerAxis_6() const { return ___m_SteerAxis_6; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_SteerAxis_6() { return &___m_SteerAxis_6; }
	inline void set_m_SteerAxis_6(VirtualAxis_t4087348596 * value)
	{
		___m_SteerAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T1639936653_H
#ifndef INPUTAXISSCROLLBAR_T457958266_H
#define INPUTAXISSCROLLBAR_T457958266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t457958266  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_2;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t457958266, ___axis_2)); }
	inline String_t* get_axis_2() const { return ___axis_2; }
	inline String_t** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(String_t* value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T457958266_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (TokenReceivedDelegate_t1016457320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (MonoPInvokeCallbackAttribute_t1582016909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (FirebaseMessage_t2372265022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[2] = 
{
	FirebaseMessage_t2372265022::get_offset_of_swigCPtr_0(),
	FirebaseMessage_t2372265022::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (FirebaseNotification_t1925808173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[2] = 
{
	FirebaseNotification_t1925808173::get_offset_of_swigCPtr_0(),
	FirebaseNotification_t1925808173::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (MessageReceivedEventArgs_t1247432759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[1] = 
{
	MessageReceivedEventArgs_t1247432759::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (TokenReceivedEventArgs_t4150709950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[1] = 
{
	TokenReceivedEventArgs_t4150709950::get_offset_of_U3CTokenU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (ApplicationExtensions_t792742538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (LanguageDataHandler_t3889722316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[4] = 
{
	LanguageDataHandler_t3889722316::get_offset_of_loadedValuesDictionary_0(),
	LanguageDataHandler_t3889722316::get_offset_of_verboseLogging_1(),
	LanguageDataHandler_t3889722316::get_offset_of_loadedCultureInfo_2(),
	LanguageDataHandler_t3889722316::get_offset_of_assetLoader_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (ChangeLanguageEventHandler_t2032193146), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (LanguageManager_t2767934455), -1, sizeof(LanguageManager_t2767934455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3111[11] = 
{
	LanguageManager_t2767934455_StaticFields::get_offset_of_instance_2(),
	LanguageManager_t2767934455_StaticFields::get_offset_of_IsQuitting_3(),
	LanguageManager_t2767934455_StaticFields::get_offset_of_DontDestroyOnLoadToggle_4(),
	LanguageManager_t2767934455_StaticFields::get_offset_of_DidSetDontDestroyOnLoad_5(),
	LanguageManager_t2767934455::get_offset_of_serializedKeys_6(),
	LanguageManager_t2767934455::get_offset_of_serializedValues_7(),
	LanguageManager_t2767934455::get_offset_of_serializedCulture_8(),
	LanguageManager_t2767934455::get_offset_of_OnChangeLanguage_9(),
	LanguageManager_t2767934455::get_offset_of_defaultLanguage_10(),
	LanguageManager_t2767934455::get_offset_of_availableLanguages_11(),
	LanguageManager_t2767934455::get_offset_of_languageDataHandler_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[1] = 
{
	U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546::get_offset_of_englishName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[1] = 
{
	U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798::get_offset_of_languageCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[1] = 
{
	U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582::get_offset_of_englishName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[1] = 
{
	U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473::get_offset_of_languageCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (LanguageParser_t1152791527), -1, sizeof(LanguageParser_t1152791527_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3116[1] = 
{
	LanguageParser_t1152791527_StaticFields::get_offset_of_xmlHeader_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (LanguageRuntimeData_t3907746602), -1, sizeof(LanguageRuntimeData_t3907746602_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3117[5] = 
{
	LanguageRuntimeData_t3907746602_StaticFields::get_offset_of_AvailableCulturesFileName_0(),
	LanguageRuntimeData_t3907746602_StaticFields::get_offset_of_rootLanguageName_1(),
	LanguageRuntimeData_t3907746602_StaticFields::get_offset_of_AudioFileFolderName_2(),
	LanguageRuntimeData_t3907746602_StaticFields::get_offset_of_TexturesFolderName_3(),
	LanguageRuntimeData_t3907746602_StaticFields::get_offset_of_PrefabsFolderName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (LocalizedAudioSource_t267215401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[3] = 
{
	LocalizedAudioSource_t267215401::get_offset_of_localizedKey_2(),
	LocalizedAudioSource_t267215401::get_offset_of_audioClip_3(),
	LocalizedAudioSource_t267215401::get_offset_of_audioSource_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (LocalizedGUIText_t2415330885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[1] = 
{
	LocalizedGUIText_t2415330885::get_offset_of_localizedKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (LocalizedGUITexture_t2717994319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[1] = 
{
	LocalizedGUITexture_t2717994319::get_offset_of_localizedKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (LocalizedObjectType_t858129287)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3121[6] = 
{
	LocalizedObjectType_t858129287::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (LocalizedObject_t1409469237), -1, sizeof(LocalizedObject_t1409469237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3122[10] = 
{
	LocalizedObject_t1409469237_StaticFields::get_offset_of_keyTypeIdentifier_0(),
	LocalizedObject_t1409469237_StaticFields::get_offset_of_endBracket_1(),
	LocalizedObject_t1409469237::get_offset_of_objectType_2(),
	LocalizedObject_t1409469237::get_offset_of_textValue_3(),
	LocalizedObject_t1409469237::get_offset_of_thisGameObject_4(),
	LocalizedObject_t1409469237::get_offset_of_thisAudioClip_5(),
	LocalizedObject_t1409469237::get_offset_of_thisTexture_6(),
	LocalizedObject_t1409469237::get_offset_of_thisTextAsset_7(),
	LocalizedObject_t1409469237::get_offset_of_overrideLocalizedObject_8(),
	LocalizedObject_t1409469237::get_offset_of_overrideObjectLanguageCode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (RuntimeLocalizedAssetLoader_t316402138), -1, sizeof(RuntimeLocalizedAssetLoader_t316402138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3123[3] = 
{
	RuntimeLocalizedAssetLoader_t316402138_StaticFields::get_offset_of_GameObjectType_0(),
	RuntimeLocalizedAssetLoader_t316402138_StaticFields::get_offset_of_AudioClipType_1(),
	RuntimeLocalizedAssetLoader_t316402138_StaticFields::get_offset_of_TextureType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (SmartCultureInfoCollection_t1809038765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[3] = 
{
	0,
	SmartCultureInfoCollection_t1809038765::get_offset_of_version_1(),
	SmartCultureInfoCollection_t1809038765::get_offset_of_cultureInfos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (SmartCultureInfo_t2060094783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[4] = 
{
	SmartCultureInfo_t2060094783::get_offset_of_languageCode_0(),
	SmartCultureInfo_t2060094783::get_offset_of_englishName_1(),
	SmartCultureInfo_t2060094783::get_offset_of_nativeName_2(),
	SmartCultureInfo_t2060094783::get_offset_of_isRightToLeft_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (SmartCultureInfoCollectionDeserializer_t1510284870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (BlobProcessor_t2126871202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[8] = 
{
	BlobProcessor_t2126871202::get_offset_of_BlobAdded_0(),
	BlobProcessor_t2126871202::get_offset_of_BlobUpdated_1(),
	BlobProcessor_t2126871202::get_offset_of_BlobRemoved_2(),
	BlobProcessor_t2126871202::get_offset_of_U3CFrameNumberU3Ek__BackingField_3(),
	BlobProcessor_t2126871202::get_offset_of_blobs_4(),
	BlobProcessor_t2126871202::get_offset_of_updatedBlobs_5(),
	BlobProcessor_t2126871202::get_offset_of_addedBlobs_6(),
	BlobProcessor_t2126871202::get_offset_of_removedBlobs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (TuioBlobEventArgs_t907500115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[1] = 
{
	TuioBlobEventArgs_t907500115::get_offset_of_Blob_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (CursorProcessor_t3936223090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[8] = 
{
	CursorProcessor_t3936223090::get_offset_of_CursorAdded_0(),
	CursorProcessor_t3936223090::get_offset_of_CursorUpdated_1(),
	CursorProcessor_t3936223090::get_offset_of_CursorRemoved_2(),
	CursorProcessor_t3936223090::get_offset_of_U3CFrameNumberU3Ek__BackingField_3(),
	CursorProcessor_t3936223090::get_offset_of_cursors_4(),
	CursorProcessor_t3936223090::get_offset_of_updatedCursors_5(),
	CursorProcessor_t3936223090::get_offset_of_addedCursors_6(),
	CursorProcessor_t3936223090::get_offset_of_removedCursors_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (TuioCursorEventArgs_t1894089282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[1] = 
{
	TuioCursorEventArgs_t1894089282::get_offset_of_Cursor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (ObjectProcessor_t2877610401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[8] = 
{
	ObjectProcessor_t2877610401::get_offset_of_ObjectAdded_0(),
	ObjectProcessor_t2877610401::get_offset_of_ObjectUpdated_1(),
	ObjectProcessor_t2877610401::get_offset_of_ObjectRemoved_2(),
	ObjectProcessor_t2877610401::get_offset_of_U3CFrameNumberU3Ek__BackingField_3(),
	ObjectProcessor_t2877610401::get_offset_of_objects_4(),
	ObjectProcessor_t2877610401::get_offset_of_updatedObjects_5(),
	ObjectProcessor_t2877610401::get_offset_of_addedObjects_6(),
	ObjectProcessor_t2877610401::get_offset_of_removedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (TuioObjectEventArgs_t236363764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[1] = 
{
	TuioObjectEventArgs_t236363764::get_offset_of_Object_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (TuioBlob_t4085310093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[6] = 
{
	TuioBlob_t4085310093::get_offset_of_U3CAngleU3Ek__BackingField_6(),
	TuioBlob_t4085310093::get_offset_of_U3CWidthU3Ek__BackingField_7(),
	TuioBlob_t4085310093::get_offset_of_U3CHeightU3Ek__BackingField_8(),
	TuioBlob_t4085310093::get_offset_of_U3CAreaU3Ek__BackingField_9(),
	TuioBlob_t4085310093::get_offset_of_U3CRotationVelocityU3Ek__BackingField_10(),
	TuioBlob_t4085310093::get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (TuioObject_t2795875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[4] = 
{
	TuioObject_t2795875::get_offset_of_U3CClassIdU3Ek__BackingField_6(),
	TuioObject_t2795875::get_offset_of_U3CAngleU3Ek__BackingField_7(),
	TuioObject_t2795875::get_offset_of_U3CRotationVelocityU3Ek__BackingField_8(),
	TuioObject_t2795875::get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (TuioCursor_t2804757666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (TuioEntity_t4229790798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[6] = 
{
	TuioEntity_t4229790798::get_offset_of_U3CIdU3Ek__BackingField_0(),
	TuioEntity_t4229790798::get_offset_of_U3CXU3Ek__BackingField_1(),
	TuioEntity_t4229790798::get_offset_of_U3CYU3Ek__BackingField_2(),
	TuioEntity_t4229790798::get_offset_of_U3CVelocityXU3Ek__BackingField_3(),
	TuioEntity_t4229790798::get_offset_of_U3CVelocityYU3Ek__BackingField_4(),
	TuioEntity_t4229790798::get_offset_of_U3CAccelerationU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (TuioServer_t3158170151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[4] = 
{
	TuioServer_t3158170151::get_offset_of_ErrorOccured_0(),
	TuioServer_t3158170151::get_offset_of_U3CPortU3Ek__BackingField_1(),
	TuioServer_t3158170151::get_offset_of_udpReceiver_2(),
	TuioServer_t3158170151::get_offset_of_processors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (UniWebViewEventProcessor_t530563931), -1, sizeof(UniWebViewEventProcessor_t530563931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3141[4] = 
{
	UniWebViewEventProcessor_t530563931::get_offset_of__queueLock_2(),
	UniWebViewEventProcessor_t530563931::get_offset_of__queuedEvents_3(),
	UniWebViewEventProcessor_t530563931::get_offset_of__executingEvents_4(),
	UniWebViewEventProcessor_t530563931_StaticFields::get_offset_of__instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (UniWebViewPlugin_t2474266150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (WebSocket_t1645401340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[5] = 
{
	WebSocket_t1645401340::get_offset_of_mUrl_0(),
	WebSocket_t1645401340::get_offset_of_m_Socket_1(),
	WebSocket_t1645401340::get_offset_of_m_Messages_2(),
	WebSocket_t1645401340::get_offset_of_m_IsConnected_3(),
	WebSocket_t1645401340::get_offset_of_m_Error_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (U3CConnectU3Ec__Iterator0_t1583584490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[4] = 
{
	U3CConnectU3Ec__Iterator0_t1583584490::get_offset_of_U24this_0(),
	U3CConnectU3Ec__Iterator0_t1583584490::get_offset_of_U24current_1(),
	U3CConnectU3Ec__Iterator0_t1583584490::get_offset_of_U24disposing_2(),
	U3CConnectU3Ec__Iterator0_t1583584490::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (AxisTouchButton_t3522881333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[6] = 
{
	AxisTouchButton_t3522881333::get_offset_of_axisName_2(),
	AxisTouchButton_t3522881333::get_offset_of_axisValue_3(),
	AxisTouchButton_t3522881333::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3522881333::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3522881333::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3522881333::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (ButtonHandler_t823762219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[1] = 
{
	ButtonHandler_t823762219::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (CrossPlatformInputManager_t191731427), -1, sizeof(CrossPlatformInputManager_t191731427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3147[3] = 
{
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (ActiveInputMethod_t139315314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3148[3] = 
{
	ActiveInputMethod_t139315314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (VirtualAxis_t4087348596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[3] = 
{
	VirtualAxis_t4087348596::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t4087348596::get_offset_of_m_Value_1(),
	VirtualAxis_t4087348596::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (VirtualButton_t2756566330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[5] = 
{
	VirtualButton_t2756566330::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2756566330::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2756566330::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2756566330::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2756566330::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (InputAxisScrollbar_t457958266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[1] = 
{
	InputAxisScrollbar_t457958266::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (Joystick_t2204371675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[9] = 
{
	Joystick_t2204371675::get_offset_of_MovementRange_2(),
	Joystick_t2204371675::get_offset_of_axesToUse_3(),
	Joystick_t2204371675::get_offset_of_horizontalAxisName_4(),
	Joystick_t2204371675::get_offset_of_verticalAxisName_5(),
	Joystick_t2204371675::get_offset_of_m_StartPos_6(),
	Joystick_t2204371675::get_offset_of_m_UseX_7(),
	Joystick_t2204371675::get_offset_of_m_UseY_8(),
	Joystick_t2204371675::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2204371675::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (AxisOption_t3128671669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3153[4] = 
{
	AxisOption_t3128671669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (MobileControlRig_t1964600252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (MobileInput_t2025745297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (StandaloneInput_t1343950252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (TiltInput_t1639936653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[5] = 
{
	TiltInput_t1639936653::get_offset_of_mapping_2(),
	TiltInput_t1639936653::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t1639936653::get_offset_of_fullTiltAngle_4(),
	TiltInput_t1639936653::get_offset_of_centreAngleOffset_5(),
	TiltInput_t1639936653::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (AxisOptions_t3101732129)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3158[3] = 
{
	AxisOptions_t3101732129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (AxisMapping_t3982445645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[2] = 
{
	AxisMapping_t3982445645::get_offset_of_type_0(),
	AxisMapping_t3982445645::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (MappingType_t2039944511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3160[5] = 
{
	MappingType_t2039944511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (TouchPad_t539039257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[18] = 
{
	TouchPad_t539039257::get_offset_of_axesToUse_2(),
	TouchPad_t539039257::get_offset_of_controlStyle_3(),
	TouchPad_t539039257::get_offset_of_horizontalAxisName_4(),
	TouchPad_t539039257::get_offset_of_verticalAxisName_5(),
	TouchPad_t539039257::get_offset_of_Xsensitivity_6(),
	TouchPad_t539039257::get_offset_of_Ysensitivity_7(),
	TouchPad_t539039257::get_offset_of_m_StartPos_8(),
	TouchPad_t539039257::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t539039257::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t539039257::get_offset_of_m_UseX_11(),
	TouchPad_t539039257::get_offset_of_m_UseY_12(),
	TouchPad_t539039257::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t539039257::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t539039257::get_offset_of_m_Dragging_15(),
	TouchPad_t539039257::get_offset_of_m_Id_16(),
	TouchPad_t539039257::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t539039257::get_offset_of_m_Center_18(),
	TouchPad_t539039257::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (AxisOption_t1372819835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3162[4] = 
{
	AxisOption_t1372819835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (ControlStyle_t1372986211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3163[4] = 
{
	ControlStyle_t1372986211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (VirtualInput_t2597455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[4] = 
{
	VirtualInput_t2597455733::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2597455733::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_2(),
	ActivateTrigger_t3349759092::get_offset_of_target_3(),
	ActivateTrigger_t3349759092::get_offset_of_source_4(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_5(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3166[7] = 
{
	Mode_t3024470803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (AlphaButtonClickMask_t141136539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[1] = 
{
	AlphaButtonClickMask_t141136539::get_offset_of__image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4151609119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (EventSystemChecker_t1882757729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_2(),
	FollowTarget_t166153614::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t3738408313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1440840980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1149538828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_2(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_3(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_4(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t3232105836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (U3CStartU3Ec__Iterator0_t980021917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[10] = 
{
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3192[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (SimpleActivatorMenu_t1387811551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3193[3] = 
{
	SimpleActivatorMenu_t1387811551::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t1387811551::get_offset_of_objects_3(),
	SimpleActivatorMenu_t1387811551::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (SimpleMouseRotator_t2364742953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[10] = 
{
	SimpleMouseRotator_t2364742953::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t2364742953::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t2364742953::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t2364742953::get_offset_of_relative_7(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (SmoothFollow_t4204731361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[5] = 
{
	SmoothFollow_t4204731361::get_offset_of_target_2(),
	SmoothFollow_t4204731361::get_offset_of_distance_3(),
	SmoothFollow_t4204731361::get_offset_of_height_4(),
	SmoothFollow_t4204731361::get_offset_of_rotationDamping_5(),
	SmoothFollow_t4204731361::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (TimedObjectActivator_t1846709985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[1] = 
{
	TimedObjectActivator_t1846709985::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (Action_t837364808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3197[6] = 
{
	Action_t837364808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (Entry_t2725803170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[3] = 
{
	Entry_t2725803170::get_offset_of_target_0(),
	Entry_t2725803170::get_offset_of_action_1(),
	Entry_t2725803170::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (Entries_t3168066469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[1] = 
{
	Entries_t3168066469::get_offset_of_entries_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
