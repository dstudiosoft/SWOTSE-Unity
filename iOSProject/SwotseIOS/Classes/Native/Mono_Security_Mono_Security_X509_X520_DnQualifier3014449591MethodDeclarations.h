﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/DnQualifier
struct DnQualifier_t3014449591;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/DnQualifier::.ctor()
extern "C"  void DnQualifier__ctor_m4138894913 (DnQualifier_t3014449591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
