﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1455931992.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3427160064_gshared (Enumerator_t1455931992 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3427160064(__this, ___host0, method) ((  void (*) (Enumerator_t1455931992 *, Dictionary_2_t3061395850 *, const MethodInfo*))Enumerator__ctor_m3427160064_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2233211951_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2233211951(__this, method) ((  Il2CppObject * (*) (Enumerator_t1455931992 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2233211951_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3955104227_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3955104227(__this, method) ((  void (*) (Enumerator_t1455931992 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3955104227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3674293844_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3674293844(__this, method) ((  void (*) (Enumerator_t1455931992 *, const MethodInfo*))Enumerator_Dispose_m3674293844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4041166087_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4041166087(__this, method) ((  bool (*) (Enumerator_t1455931992 *, const MethodInfo*))Enumerator_MoveNext_m4041166087_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m2733093241_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2733093241(__this, method) ((  int64_t (*) (Enumerator_t1455931992 *, const MethodInfo*))Enumerator_get_Current_m2733093241_gshared)(__this, method)
