﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MayorResidenceOverviewContentManager
struct MayorResidenceOverviewContentManager_t1616643487;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MayorResidenceOverviewContentManager::.ctor()
extern "C"  void MayorResidenceOverviewContentManager__ctor_m3140630346 (MayorResidenceOverviewContentManager_t1616643487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::OnEnable()
extern "C"  void MayorResidenceOverviewContentManager_OnEnable_m1448578398 (MayorResidenceOverviewContentManager_t1616643487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::FixedUpdate()
extern "C"  void MayorResidenceOverviewContentManager_FixedUpdate_m3966159541 (MayorResidenceOverviewContentManager_t1616643487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::UpdateTax()
extern "C"  void MayorResidenceOverviewContentManager_UpdateTax_m3025551688 (MayorResidenceOverviewContentManager_t1616643487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::TaxUpdated(System.Object,System.String)
extern "C"  void MayorResidenceOverviewContentManager_TaxUpdated_m1439569368 (MayorResidenceOverviewContentManager_t1616643487 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::SwitchIcon(System.Boolean)
extern "C"  void MayorResidenceOverviewContentManager_SwitchIcon_m2411411716 (MayorResidenceOverviewContentManager_t1616643487 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::ChangeNameAndIcon()
extern "C"  void MayorResidenceOverviewContentManager_ChangeNameAndIcon_m3672641139 (MayorResidenceOverviewContentManager_t1616643487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MayorResidenceOverviewContentManager::NameAndIconChanged(System.Object,System.String)
extern "C"  void MayorResidenceOverviewContentManager_NameAndIconChanged_m692491887 (MayorResidenceOverviewContentManager_t1616643487 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
