﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[,]
struct GameObjectU5BU2CU5D_t3057952155;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestWorldChunksManager
struct  TestWorldChunksManager_t3581849513  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[,] TestWorldChunksManager::worldChunks
	GameObjectU5BU2CU5D_t3057952155* ___worldChunks_2;
	// System.Int64 TestWorldChunksManager::targetX
	int64_t ___targetX_3;
	// System.Int64 TestWorldChunksManager::targetY
	int64_t ___targetY_4;

public:
	inline static int32_t get_offset_of_worldChunks_2() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t3581849513, ___worldChunks_2)); }
	inline GameObjectU5BU2CU5D_t3057952155* get_worldChunks_2() const { return ___worldChunks_2; }
	inline GameObjectU5BU2CU5D_t3057952155** get_address_of_worldChunks_2() { return &___worldChunks_2; }
	inline void set_worldChunks_2(GameObjectU5BU2CU5D_t3057952155* value)
	{
		___worldChunks_2 = value;
		Il2CppCodeGenWriteBarrier(&___worldChunks_2, value);
	}

	inline static int32_t get_offset_of_targetX_3() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t3581849513, ___targetX_3)); }
	inline int64_t get_targetX_3() const { return ___targetX_3; }
	inline int64_t* get_address_of_targetX_3() { return &___targetX_3; }
	inline void set_targetX_3(int64_t value)
	{
		___targetX_3 = value;
	}

	inline static int32_t get_offset_of_targetY_4() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t3581849513, ___targetY_4)); }
	inline int64_t get_targetY_4() const { return ___targetY_4; }
	inline int64_t* get_address_of_targetY_4() { return &___targetY_4; }
	inline void set_targetY_4(int64_t value)
	{
		___targetY_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
