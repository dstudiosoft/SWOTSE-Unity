﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.GlobalProxySelection/EmptyWebProxy
struct EmptyWebProxy_t3297389645;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"

// System.Void System.Net.GlobalProxySelection/EmptyWebProxy::.ctor()
extern "C"  void EmptyWebProxy__ctor_m2197877513 (EmptyWebProxy_t3297389645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.GlobalProxySelection/EmptyWebProxy::get_Credentials()
extern "C"  Il2CppObject * EmptyWebProxy_get_Credentials_m3275446326 (EmptyWebProxy_t3297389645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.GlobalProxySelection/EmptyWebProxy::set_Credentials(System.Net.ICredentials)
extern "C"  void EmptyWebProxy_set_Credentials_m2089058247 (EmptyWebProxy_t3297389645 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.GlobalProxySelection/EmptyWebProxy::GetProxy(System.Uri)
extern "C"  Uri_t19570940 * EmptyWebProxy_GetProxy_m4088757160 (EmptyWebProxy_t3297389645 * __this, Uri_t19570940 * ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.GlobalProxySelection/EmptyWebProxy::IsBypassed(System.Uri)
extern "C"  bool EmptyWebProxy_IsBypassed_m1416086545 (EmptyWebProxy_t3297389645 * __this, Uri_t19570940 * ___host0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
