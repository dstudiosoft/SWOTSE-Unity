﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceValleysTableController
struct ResidenceValleysTableController_t2038149672;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void ResidenceValleysTableController::.ctor()
extern "C"  void ResidenceValleysTableController__ctor_m1000518261 (ResidenceValleysTableController_t2038149672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableController::OnEnable()
extern "C"  void ResidenceValleysTableController_OnEnable_m3537640589 (ResidenceValleysTableController_t2038149672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ResidenceValleysTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t ResidenceValleysTableController_GetNumberOfRowsForTableView_m2442878481 (ResidenceValleysTableController_t2038149672 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ResidenceValleysTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float ResidenceValleysTableController_GetHeightForRowInTableView_m3517939517 (ResidenceValleysTableController_t2038149672 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell ResidenceValleysTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * ResidenceValleysTableController_GetCellForRowInTableView_m2383322442 (ResidenceValleysTableController_t2038149672 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableController::OpenBuildingWindow(System.Int64)
extern "C"  void ResidenceValleysTableController_OpenBuildingWindow_m2510084201 (ResidenceValleysTableController_t2038149672 * __this, int64_t ___pitId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
