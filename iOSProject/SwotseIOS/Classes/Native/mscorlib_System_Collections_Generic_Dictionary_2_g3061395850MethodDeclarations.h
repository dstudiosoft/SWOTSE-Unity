﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t121710815;
// System.Collections.Generic.IDictionary`2<System.Int64,System.Int32>
struct IDictionary_2_t1060479271;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2366572913;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>
struct IEnumerator_1_t2589232195;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>
struct KeyCollection_t1249926325;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Int32>
struct ValueCollection_t1764455693;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu86453256.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m3935535078_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3935535078(__this, method) ((  void (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2__ctor_m3935535078_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2758353739_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2758353739(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2758353739_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1491402812_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1491402812(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1491402812_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3297323537_gshared (Dictionary_2_t3061395850 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3297323537(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3061395850 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3297323537_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1164649329_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1164649329(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1164649329_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1430516062_gshared (Dictionary_2_t3061395850 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1430516062(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3061395850 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1430516062_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3415149819_gshared (Dictionary_2_t3061395850 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3415149819(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3061395850 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3415149819_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1905902436_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1905902436(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1905902436_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3953100538_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3953100538(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3953100538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m449182103_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m449182103(__this, method) ((  bool (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m449182103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m337547414_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m337547414(__this, method) ((  bool (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m337547414_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2916081286_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2916081286(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2916081286_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m852791459_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m852791459(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m852791459_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1819791796_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1819791796(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1819791796_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2442585900_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2442585900(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2442585900_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3923183735_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3923183735(__this, ___key0, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3923183735_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m745707726_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m745707726(__this, method) ((  bool (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m745707726_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3978856878_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3978856878(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3978856878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m238910708_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m238910708(__this, method) ((  bool (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m238910708_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m501059219_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2_t818741072  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m501059219(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3061395850 *, KeyValuePair_2_t818741072 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m501059219_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1424679717_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2_t818741072  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1424679717(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3061395850 *, KeyValuePair_2_t818741072 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1424679717_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1308238239_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2U5BU5D_t2366572913* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1308238239(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3061395850 *, KeyValuePair_2U5BU5D_t2366572913*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1308238239_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1155322210_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2_t818741072  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1155322210(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3061395850 *, KeyValuePair_2_t818741072 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1155322210_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2981846974_gshared (Dictionary_2_t3061395850 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2981846974(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2981846974_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3183439681_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3183439681(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3183439681_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3348614940_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3348614940(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3348614940_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2959961303_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2959961303(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2959961303_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m22418102_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22418102(__this, method) ((  int32_t (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_get_Count_m22418102_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2381609815_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2381609815(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3061395850 *, int64_t, const MethodInfo*))Dictionary_2_get_Item_m2381609815_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3720053228_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3720053228(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3061395850 *, int64_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m3720053228_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1761791930_gshared (Dictionary_2_t3061395850 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1761791930(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3061395850 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1761791930_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3325268081_gshared (Dictionary_2_t3061395850 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3325268081(__this, ___size0, method) ((  void (*) (Dictionary_2_t3061395850 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3325268081_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3067459119_gshared (Dictionary_2_t3061395850 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3067459119(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3067459119_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t818741072  Dictionary_2_make_pair_m2768479793_gshared (Il2CppObject * __this /* static, unused */, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2768479793(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t818741072  (*) (Il2CppObject * /* static, unused */, int64_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m2768479793_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::pick_key(TKey,TValue)
extern "C"  int64_t Dictionary_2_pick_key_m1987973809_gshared (Il2CppObject * __this /* static, unused */, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1987973809(__this /* static, unused */, ___key0, ___value1, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, int64_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1987973809_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m3970959001_gshared (Il2CppObject * __this /* static, unused */, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3970959001(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int64_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m3970959001_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2539837456_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2U5BU5D_t2366572913* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2539837456(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3061395850 *, KeyValuePair_2U5BU5D_t2366572913*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2539837456_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m4213831044_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4213831044(__this, method) ((  void (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_Resize_m4213831044_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m305848917_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m305848917(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3061395850 *, int64_t, int32_t, const MethodInfo*))Dictionary_2_Add_m305848917_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m3082493665_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3082493665(__this, method) ((  void (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_Clear_m3082493665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4063096439_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4063096439(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3061395850 *, int64_t, const MethodInfo*))Dictionary_2_ContainsKey_m4063096439_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1038095119_gshared (Dictionary_2_t3061395850 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1038095119(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3061395850 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1038095119_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3713462258_gshared (Dictionary_2_t3061395850 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3713462258(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3061395850 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3713462258_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1924864948_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1924864948(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1924864948_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m976535285_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m976535285(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3061395850 *, int64_t, const MethodInfo*))Dictionary_2_Remove_m976535285_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4034234992_gshared (Dictionary_2_t3061395850 * __this, int64_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4034234992(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3061395850 *, int64_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m4034234992_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::get_Keys()
extern "C"  KeyCollection_t1249926325 * Dictionary_2_get_Keys_m2996056203_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2996056203(__this, method) ((  KeyCollection_t1249926325 * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_get_Keys_m2996056203_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::get_Values()
extern "C"  ValueCollection_t1764455693 * Dictionary_2_get_Values_m2756299163_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2756299163(__this, method) ((  ValueCollection_t1764455693 * (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_get_Values_m2756299163_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::ToTKey(System.Object)
extern "C"  int64_t Dictionary_2_ToTKey_m1255176190_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1255176190(__this, ___key0, method) ((  int64_t (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1255176190_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m845809806_gshared (Dictionary_2_t3061395850 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m845809806(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t3061395850 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m845809806_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2805104432_gshared (Dictionary_2_t3061395850 * __this, KeyValuePair_2_t818741072  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2805104432(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3061395850 *, KeyValuePair_2_t818741072 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2805104432_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t86453256  Dictionary_2_GetEnumerator_m2451821637_gshared (Dictionary_2_t3061395850 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2451821637(__this, method) ((  Enumerator_t86453256  (*) (Dictionary_2_t3061395850 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2451821637_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2507122006_gshared (Il2CppObject * __this /* static, unused */, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2507122006(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int64_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2507122006_gshared)(__this /* static, unused */, ___key0, ___value1, method)
