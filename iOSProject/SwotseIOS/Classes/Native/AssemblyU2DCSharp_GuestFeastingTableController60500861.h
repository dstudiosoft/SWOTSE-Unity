﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GuestHouseTableLine
struct GuestHouseTableLine_t1665954750;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<ArmyGeneralModel>
struct List_1_t4273847676;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuestFeastingTableController
struct  GuestFeastingTableController_t60500861  : public MonoBehaviour_t1158329972
{
public:
	// GuestHouseTableLine GuestFeastingTableController::m_cellPrefab
	GuestHouseTableLine_t1665954750 * ___m_cellPrefab_2;
	// Tacticsoft.TableView GuestFeastingTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Boolean GuestFeastingTableController::guest
	bool ___guest_4;
	// System.Collections.Generic.List`1<ArmyGeneralModel> GuestFeastingTableController::generalsContainer
	List_1_t4273847676 * ___generalsContainer_5;
	// ArmyGeneralModel GuestFeastingTableController::requestedGeneral
	ArmyGeneralModel_t609759248 * ___requestedGeneral_6;
	// System.Int32 GuestFeastingTableController::displayedHeroesCount
	int32_t ___displayedHeroesCount_7;
	// System.Int32 GuestFeastingTableController::m_numRows
	int32_t ___m_numRows_8;
	// ConfirmationEvent GuestFeastingTableController::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_9;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___m_cellPrefab_2)); }
	inline GuestHouseTableLine_t1665954750 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline GuestHouseTableLine_t1665954750 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(GuestHouseTableLine_t1665954750 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_guest_4() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___guest_4)); }
	inline bool get_guest_4() const { return ___guest_4; }
	inline bool* get_address_of_guest_4() { return &___guest_4; }
	inline void set_guest_4(bool value)
	{
		___guest_4 = value;
	}

	inline static int32_t get_offset_of_generalsContainer_5() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___generalsContainer_5)); }
	inline List_1_t4273847676 * get_generalsContainer_5() const { return ___generalsContainer_5; }
	inline List_1_t4273847676 ** get_address_of_generalsContainer_5() { return &___generalsContainer_5; }
	inline void set_generalsContainer_5(List_1_t4273847676 * value)
	{
		___generalsContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___generalsContainer_5, value);
	}

	inline static int32_t get_offset_of_requestedGeneral_6() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___requestedGeneral_6)); }
	inline ArmyGeneralModel_t609759248 * get_requestedGeneral_6() const { return ___requestedGeneral_6; }
	inline ArmyGeneralModel_t609759248 ** get_address_of_requestedGeneral_6() { return &___requestedGeneral_6; }
	inline void set_requestedGeneral_6(ArmyGeneralModel_t609759248 * value)
	{
		___requestedGeneral_6 = value;
		Il2CppCodeGenWriteBarrier(&___requestedGeneral_6, value);
	}

	inline static int32_t get_offset_of_displayedHeroesCount_7() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___displayedHeroesCount_7)); }
	inline int32_t get_displayedHeroesCount_7() const { return ___displayedHeroesCount_7; }
	inline int32_t* get_address_of_displayedHeroesCount_7() { return &___displayedHeroesCount_7; }
	inline void set_displayedHeroesCount_7(int32_t value)
	{
		___displayedHeroesCount_7 = value;
	}

	inline static int32_t get_offset_of_m_numRows_8() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___m_numRows_8)); }
	inline int32_t get_m_numRows_8() const { return ___m_numRows_8; }
	inline int32_t* get_address_of_m_numRows_8() { return &___m_numRows_8; }
	inline void set_m_numRows_8(int32_t value)
	{
		___m_numRows_8 = value;
	}

	inline static int32_t get_offset_of_confirmed_9() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t60500861, ___confirmed_9)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_9() const { return ___confirmed_9; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_9() { return &___confirmed_9; }
	inline void set_confirmed_9(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_9 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
