﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2583977971(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4085980872 *, Dictionary_2_t2765956170 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3383991634(__this, method) ((  Il2CppObject * (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2605818636(__this, method) ((  void (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4041242593(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2163460612(__this, method) ((  Il2CppObject * (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3274195598(__this, method) ((  Il2CppObject * (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::MoveNext()
#define Enumerator_MoveNext_m3521340980(__this, method) ((  bool (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Current()
#define Enumerator_get_Current_m3426110420(__this, method) ((  KeyValuePair_2_t523301392  (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3451785063(__this, method) ((  Canvas_t209405766 * (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2146348639(__this, method) ((  ProjectionParams_t2712959773 * (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Reset()
#define Enumerator_Reset_m2894129577(__this, method) ((  void (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::VerifyState()
#define Enumerator_VerifyState_m4021031580(__this, method) ((  void (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3962478556(__this, method) ((  void (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Dispose()
#define Enumerator_Dispose_m1595541063(__this, method) ((  void (*) (Enumerator_t4085980872 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
