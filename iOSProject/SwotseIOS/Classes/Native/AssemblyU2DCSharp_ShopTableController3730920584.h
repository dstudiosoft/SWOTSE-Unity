﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;
// ShopTableCell
struct ShopTableCell_t4262618216;
// Tacticsoft.TableView
struct TableView_t3179510217;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableController
struct  ShopTableController_t3730920584  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.ArrayList ShopTableController::scriptShopCells
	ArrayList_t4252133567 * ___scriptShopCells_2;
	// System.Collections.ArrayList ShopTableController::speedUpShopCells
	ArrayList_t4252133567 * ___speedUpShopCells_3;
	// System.Collections.ArrayList ShopTableController::productionShopCells
	ArrayList_t4252133567 * ___productionShopCells_4;
	// System.Collections.ArrayList ShopTableController::diamondShopCells
	ArrayList_t4252133567 * ___diamondShopCells_5;
	// System.Collections.ArrayList ShopTableController::promotionsShopCells
	ArrayList_t4252133567 * ___promotionsShopCells_6;
	// System.Collections.ArrayList ShopTableController::armyShopCells
	ArrayList_t4252133567 * ___armyShopCells_7;
	// System.Collections.ArrayList ShopTableController::currentSourceArray
	ArrayList_t4252133567 * ___currentSourceArray_8;
	// System.Boolean ShopTableController::_shop
	bool ____shop_9;
	// System.Int64 ShopTableController::cityId
	int64_t ___cityId_10;
	// System.String ShopTableController::content
	String_t* ___content_11;
	// SmartLocalization.LanguageManager ShopTableController::langManager
	LanguageManager_t132697751 * ___langManager_12;
	// ShopTableCell ShopTableController::m_cellPrefab
	ShopTableCell_t4262618216 * ___m_cellPrefab_13;
	// Tacticsoft.TableView ShopTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_14;
	// UnityEngine.UI.Text ShopTableController::Label
	Text_t356221433 * ___Label_15;
	// UnityEngine.UI.Text ShopTableController::shillingsCount
	Text_t356221433 * ___shillingsCount_16;
	// UnityEngine.UI.Image ShopTableController::icon
	Image_t2042527209 * ___icon_17;
	// UnityEngine.Sprite ShopTableController::tabSelectedTop
	Sprite_t309593783 * ___tabSelectedTop_18;
	// UnityEngine.Sprite ShopTableController::tabSelectedBottom
	Sprite_t309593783 * ___tabSelectedBottom_19;
	// UnityEngine.Sprite ShopTableController::tabUnselectedTop
	Sprite_t309593783 * ___tabUnselectedTop_20;
	// UnityEngine.Sprite ShopTableController::tabUnselectedBottom
	Sprite_t309593783 * ___tabUnselectedBottom_21;
	// UnityEngine.Sprite ShopTableController::shopIcon
	Sprite_t309593783 * ___shopIcon_22;
	// UnityEngine.Sprite ShopTableController::treasureIcon
	Sprite_t309593783 * ___treasureIcon_23;
	// UnityEngine.GameObject ShopTableController::buyMoreBtn
	GameObject_t1756533147 * ___buyMoreBtn_24;
	// UnityEngine.GameObject ShopTableController::buyMoreTxt
	GameObject_t1756533147 * ___buyMoreTxt_25;
	// UnityEngine.UI.Image[] ShopTableController::tabs
	ImageU5BU5D_t590162004* ___tabs_26;
	// SimpleEvent ShopTableController::onGetShopItems
	SimpleEvent_t1509017202 * ___onGetShopItems_27;

public:
	inline static int32_t get_offset_of_scriptShopCells_2() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___scriptShopCells_2)); }
	inline ArrayList_t4252133567 * get_scriptShopCells_2() const { return ___scriptShopCells_2; }
	inline ArrayList_t4252133567 ** get_address_of_scriptShopCells_2() { return &___scriptShopCells_2; }
	inline void set_scriptShopCells_2(ArrayList_t4252133567 * value)
	{
		___scriptShopCells_2 = value;
		Il2CppCodeGenWriteBarrier(&___scriptShopCells_2, value);
	}

	inline static int32_t get_offset_of_speedUpShopCells_3() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___speedUpShopCells_3)); }
	inline ArrayList_t4252133567 * get_speedUpShopCells_3() const { return ___speedUpShopCells_3; }
	inline ArrayList_t4252133567 ** get_address_of_speedUpShopCells_3() { return &___speedUpShopCells_3; }
	inline void set_speedUpShopCells_3(ArrayList_t4252133567 * value)
	{
		___speedUpShopCells_3 = value;
		Il2CppCodeGenWriteBarrier(&___speedUpShopCells_3, value);
	}

	inline static int32_t get_offset_of_productionShopCells_4() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___productionShopCells_4)); }
	inline ArrayList_t4252133567 * get_productionShopCells_4() const { return ___productionShopCells_4; }
	inline ArrayList_t4252133567 ** get_address_of_productionShopCells_4() { return &___productionShopCells_4; }
	inline void set_productionShopCells_4(ArrayList_t4252133567 * value)
	{
		___productionShopCells_4 = value;
		Il2CppCodeGenWriteBarrier(&___productionShopCells_4, value);
	}

	inline static int32_t get_offset_of_diamondShopCells_5() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___diamondShopCells_5)); }
	inline ArrayList_t4252133567 * get_diamondShopCells_5() const { return ___diamondShopCells_5; }
	inline ArrayList_t4252133567 ** get_address_of_diamondShopCells_5() { return &___diamondShopCells_5; }
	inline void set_diamondShopCells_5(ArrayList_t4252133567 * value)
	{
		___diamondShopCells_5 = value;
		Il2CppCodeGenWriteBarrier(&___diamondShopCells_5, value);
	}

	inline static int32_t get_offset_of_promotionsShopCells_6() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___promotionsShopCells_6)); }
	inline ArrayList_t4252133567 * get_promotionsShopCells_6() const { return ___promotionsShopCells_6; }
	inline ArrayList_t4252133567 ** get_address_of_promotionsShopCells_6() { return &___promotionsShopCells_6; }
	inline void set_promotionsShopCells_6(ArrayList_t4252133567 * value)
	{
		___promotionsShopCells_6 = value;
		Il2CppCodeGenWriteBarrier(&___promotionsShopCells_6, value);
	}

	inline static int32_t get_offset_of_armyShopCells_7() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___armyShopCells_7)); }
	inline ArrayList_t4252133567 * get_armyShopCells_7() const { return ___armyShopCells_7; }
	inline ArrayList_t4252133567 ** get_address_of_armyShopCells_7() { return &___armyShopCells_7; }
	inline void set_armyShopCells_7(ArrayList_t4252133567 * value)
	{
		___armyShopCells_7 = value;
		Il2CppCodeGenWriteBarrier(&___armyShopCells_7, value);
	}

	inline static int32_t get_offset_of_currentSourceArray_8() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___currentSourceArray_8)); }
	inline ArrayList_t4252133567 * get_currentSourceArray_8() const { return ___currentSourceArray_8; }
	inline ArrayList_t4252133567 ** get_address_of_currentSourceArray_8() { return &___currentSourceArray_8; }
	inline void set_currentSourceArray_8(ArrayList_t4252133567 * value)
	{
		___currentSourceArray_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentSourceArray_8, value);
	}

	inline static int32_t get_offset_of__shop_9() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ____shop_9)); }
	inline bool get__shop_9() const { return ____shop_9; }
	inline bool* get_address_of__shop_9() { return &____shop_9; }
	inline void set__shop_9(bool value)
	{
		____shop_9 = value;
	}

	inline static int32_t get_offset_of_cityId_10() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___cityId_10)); }
	inline int64_t get_cityId_10() const { return ___cityId_10; }
	inline int64_t* get_address_of_cityId_10() { return &___cityId_10; }
	inline void set_cityId_10(int64_t value)
	{
		___cityId_10 = value;
	}

	inline static int32_t get_offset_of_content_11() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___content_11)); }
	inline String_t* get_content_11() const { return ___content_11; }
	inline String_t** get_address_of_content_11() { return &___content_11; }
	inline void set_content_11(String_t* value)
	{
		___content_11 = value;
		Il2CppCodeGenWriteBarrier(&___content_11, value);
	}

	inline static int32_t get_offset_of_langManager_12() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___langManager_12)); }
	inline LanguageManager_t132697751 * get_langManager_12() const { return ___langManager_12; }
	inline LanguageManager_t132697751 ** get_address_of_langManager_12() { return &___langManager_12; }
	inline void set_langManager_12(LanguageManager_t132697751 * value)
	{
		___langManager_12 = value;
		Il2CppCodeGenWriteBarrier(&___langManager_12, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_13() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___m_cellPrefab_13)); }
	inline ShopTableCell_t4262618216 * get_m_cellPrefab_13() const { return ___m_cellPrefab_13; }
	inline ShopTableCell_t4262618216 ** get_address_of_m_cellPrefab_13() { return &___m_cellPrefab_13; }
	inline void set_m_cellPrefab_13(ShopTableCell_t4262618216 * value)
	{
		___m_cellPrefab_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_13, value);
	}

	inline static int32_t get_offset_of_m_tableView_14() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___m_tableView_14)); }
	inline TableView_t3179510217 * get_m_tableView_14() const { return ___m_tableView_14; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_14() { return &___m_tableView_14; }
	inline void set_m_tableView_14(TableView_t3179510217 * value)
	{
		___m_tableView_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_14, value);
	}

	inline static int32_t get_offset_of_Label_15() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___Label_15)); }
	inline Text_t356221433 * get_Label_15() const { return ___Label_15; }
	inline Text_t356221433 ** get_address_of_Label_15() { return &___Label_15; }
	inline void set_Label_15(Text_t356221433 * value)
	{
		___Label_15 = value;
		Il2CppCodeGenWriteBarrier(&___Label_15, value);
	}

	inline static int32_t get_offset_of_shillingsCount_16() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___shillingsCount_16)); }
	inline Text_t356221433 * get_shillingsCount_16() const { return ___shillingsCount_16; }
	inline Text_t356221433 ** get_address_of_shillingsCount_16() { return &___shillingsCount_16; }
	inline void set_shillingsCount_16(Text_t356221433 * value)
	{
		___shillingsCount_16 = value;
		Il2CppCodeGenWriteBarrier(&___shillingsCount_16, value);
	}

	inline static int32_t get_offset_of_icon_17() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___icon_17)); }
	inline Image_t2042527209 * get_icon_17() const { return ___icon_17; }
	inline Image_t2042527209 ** get_address_of_icon_17() { return &___icon_17; }
	inline void set_icon_17(Image_t2042527209 * value)
	{
		___icon_17 = value;
		Il2CppCodeGenWriteBarrier(&___icon_17, value);
	}

	inline static int32_t get_offset_of_tabSelectedTop_18() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___tabSelectedTop_18)); }
	inline Sprite_t309593783 * get_tabSelectedTop_18() const { return ___tabSelectedTop_18; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedTop_18() { return &___tabSelectedTop_18; }
	inline void set_tabSelectedTop_18(Sprite_t309593783 * value)
	{
		___tabSelectedTop_18 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedTop_18, value);
	}

	inline static int32_t get_offset_of_tabSelectedBottom_19() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___tabSelectedBottom_19)); }
	inline Sprite_t309593783 * get_tabSelectedBottom_19() const { return ___tabSelectedBottom_19; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedBottom_19() { return &___tabSelectedBottom_19; }
	inline void set_tabSelectedBottom_19(Sprite_t309593783 * value)
	{
		___tabSelectedBottom_19 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedBottom_19, value);
	}

	inline static int32_t get_offset_of_tabUnselectedTop_20() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___tabUnselectedTop_20)); }
	inline Sprite_t309593783 * get_tabUnselectedTop_20() const { return ___tabUnselectedTop_20; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedTop_20() { return &___tabUnselectedTop_20; }
	inline void set_tabUnselectedTop_20(Sprite_t309593783 * value)
	{
		___tabUnselectedTop_20 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedTop_20, value);
	}

	inline static int32_t get_offset_of_tabUnselectedBottom_21() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___tabUnselectedBottom_21)); }
	inline Sprite_t309593783 * get_tabUnselectedBottom_21() const { return ___tabUnselectedBottom_21; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedBottom_21() { return &___tabUnselectedBottom_21; }
	inline void set_tabUnselectedBottom_21(Sprite_t309593783 * value)
	{
		___tabUnselectedBottom_21 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedBottom_21, value);
	}

	inline static int32_t get_offset_of_shopIcon_22() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___shopIcon_22)); }
	inline Sprite_t309593783 * get_shopIcon_22() const { return ___shopIcon_22; }
	inline Sprite_t309593783 ** get_address_of_shopIcon_22() { return &___shopIcon_22; }
	inline void set_shopIcon_22(Sprite_t309593783 * value)
	{
		___shopIcon_22 = value;
		Il2CppCodeGenWriteBarrier(&___shopIcon_22, value);
	}

	inline static int32_t get_offset_of_treasureIcon_23() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___treasureIcon_23)); }
	inline Sprite_t309593783 * get_treasureIcon_23() const { return ___treasureIcon_23; }
	inline Sprite_t309593783 ** get_address_of_treasureIcon_23() { return &___treasureIcon_23; }
	inline void set_treasureIcon_23(Sprite_t309593783 * value)
	{
		___treasureIcon_23 = value;
		Il2CppCodeGenWriteBarrier(&___treasureIcon_23, value);
	}

	inline static int32_t get_offset_of_buyMoreBtn_24() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___buyMoreBtn_24)); }
	inline GameObject_t1756533147 * get_buyMoreBtn_24() const { return ___buyMoreBtn_24; }
	inline GameObject_t1756533147 ** get_address_of_buyMoreBtn_24() { return &___buyMoreBtn_24; }
	inline void set_buyMoreBtn_24(GameObject_t1756533147 * value)
	{
		___buyMoreBtn_24 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreBtn_24, value);
	}

	inline static int32_t get_offset_of_buyMoreTxt_25() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___buyMoreTxt_25)); }
	inline GameObject_t1756533147 * get_buyMoreTxt_25() const { return ___buyMoreTxt_25; }
	inline GameObject_t1756533147 ** get_address_of_buyMoreTxt_25() { return &___buyMoreTxt_25; }
	inline void set_buyMoreTxt_25(GameObject_t1756533147 * value)
	{
		___buyMoreTxt_25 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreTxt_25, value);
	}

	inline static int32_t get_offset_of_tabs_26() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___tabs_26)); }
	inline ImageU5BU5D_t590162004* get_tabs_26() const { return ___tabs_26; }
	inline ImageU5BU5D_t590162004** get_address_of_tabs_26() { return &___tabs_26; }
	inline void set_tabs_26(ImageU5BU5D_t590162004* value)
	{
		___tabs_26 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_26, value);
	}

	inline static int32_t get_offset_of_onGetShopItems_27() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584, ___onGetShopItems_27)); }
	inline SimpleEvent_t1509017202 * get_onGetShopItems_27() const { return ___onGetShopItems_27; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetShopItems_27() { return &___onGetShopItems_27; }
	inline void set_onGetShopItems_27(SimpleEvent_t1509017202 * value)
	{
		___onGetShopItems_27 = value;
		Il2CppCodeGenWriteBarrier(&___onGetShopItems_27, value);
	}
};

struct ShopTableController_t3730920584_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map1D
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1D_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map1E
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1E_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map1F
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1F_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map20
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map20_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map21
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map21_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableController::<>f__switch$map22
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map22_33;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_28() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map1D_28)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1D_28() const { return ___U3CU3Ef__switchU24map1D_28; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1D_28() { return &___U3CU3Ef__switchU24map1D_28; }
	inline void set_U3CU3Ef__switchU24map1D_28(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1D_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1D_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_29() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map1E_29)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1E_29() const { return ___U3CU3Ef__switchU24map1E_29; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1E_29() { return &___U3CU3Ef__switchU24map1E_29; }
	inline void set_U3CU3Ef__switchU24map1E_29(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1E_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1E_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_30() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map1F_30)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1F_30() const { return ___U3CU3Ef__switchU24map1F_30; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1F_30() { return &___U3CU3Ef__switchU24map1F_30; }
	inline void set_U3CU3Ef__switchU24map1F_30(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1F_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1F_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_31() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map20_31)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map20_31() const { return ___U3CU3Ef__switchU24map20_31; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map20_31() { return &___U3CU3Ef__switchU24map20_31; }
	inline void set_U3CU3Ef__switchU24map20_31(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map20_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map20_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map21_32() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map21_32)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map21_32() const { return ___U3CU3Ef__switchU24map21_32; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map21_32() { return &___U3CU3Ef__switchU24map21_32; }
	inline void set_U3CU3Ef__switchU24map21_32(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map21_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map21_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map22_33() { return static_cast<int32_t>(offsetof(ShopTableController_t3730920584_StaticFields, ___U3CU3Ef__switchU24map22_33)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map22_33() const { return ___U3CU3Ef__switchU24map22_33; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map22_33() { return &___U3CU3Ef__switchU24map22_33; }
	inline void set_U3CU3Ef__switchU24map22_33(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map22_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map22_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
