﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrainingIconManager
struct TrainingIconManager_t2314554274;

#include "codegen/il2cpp-codegen.h"

// System.Void TrainingIconManager::.ctor()
extern "C"  void TrainingIconManager__ctor_m2135959539 (TrainingIconManager_t2314554274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrainingIconManager::Start()
extern "C"  void TrainingIconManager_Start_m2280180903 (TrainingIconManager_t2314554274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrainingIconManager::FixedUpdate()
extern "C"  void TrainingIconManager_FixedUpdate_m3729467630 (TrainingIconManager_t2314554274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrainingIconManager::OpenTrainMenu()
extern "C"  void TrainingIconManager_OpenTrainMenu_m1051980620 (TrainingIconManager_t2314554274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
