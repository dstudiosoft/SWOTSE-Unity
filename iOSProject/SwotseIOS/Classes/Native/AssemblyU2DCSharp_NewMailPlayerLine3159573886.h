﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// NewMailContentManager
struct NewMailContentManager_t2045651277;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailPlayerLine
struct  NewMailPlayerLine_t3159573886  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text NewMailPlayerLine::playeName
	Text_t356221433 * ___playeName_2;
	// System.Int64 NewMailPlayerLine::playerId
	int64_t ___playerId_3;
	// NewMailContentManager NewMailPlayerLine::manager
	NewMailContentManager_t2045651277 * ___manager_4;

public:
	inline static int32_t get_offset_of_playeName_2() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t3159573886, ___playeName_2)); }
	inline Text_t356221433 * get_playeName_2() const { return ___playeName_2; }
	inline Text_t356221433 ** get_address_of_playeName_2() { return &___playeName_2; }
	inline void set_playeName_2(Text_t356221433 * value)
	{
		___playeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playeName_2, value);
	}

	inline static int32_t get_offset_of_playerId_3() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t3159573886, ___playerId_3)); }
	inline int64_t get_playerId_3() const { return ___playerId_3; }
	inline int64_t* get_address_of_playerId_3() { return &___playerId_3; }
	inline void set_playerId_3(int64_t value)
	{
		___playerId_3 = value;
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t3159573886, ___manager_4)); }
	inline NewMailContentManager_t2045651277 * get_manager_4() const { return ___manager_4; }
	inline NewMailContentManager_t2045651277 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(NewMailContentManager_t2045651277 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier(&___manager_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
