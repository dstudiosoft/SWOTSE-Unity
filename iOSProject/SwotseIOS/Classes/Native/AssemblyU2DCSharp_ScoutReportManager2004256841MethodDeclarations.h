﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoutReportManager
struct ScoutReportManager_t2004256841;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoutReportManager::.ctor()
extern "C"  void ScoutReportManager__ctor_m3669516906 (ScoutReportManager_t2004256841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoutReportManager::OnEnable()
extern "C"  void ScoutReportManager_OnEnable_m2542621066 (ScoutReportManager_t2004256841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoutReportManager::CloseReport()
extern "C"  void ScoutReportManager_CloseReport_m3003781244 (ScoutReportManager_t2004256841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
