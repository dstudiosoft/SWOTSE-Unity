﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TributesContentManager
struct TributesContentManager_t508388144;

#include "codegen/il2cpp-codegen.h"

// System.Void TributesContentManager::.ctor()
extern "C"  void TributesContentManager__ctor_m1555439291 (TributesContentManager_t508388144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TributesContentManager::OnEnable()
extern "C"  void TributesContentManager_OnEnable_m1443879407 (TributesContentManager_t508388144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
