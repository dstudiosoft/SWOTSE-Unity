﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.PinnedTransformGesture
struct PinnedTransformGesture_t2478931093;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_PinnedTransf510080709.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_ProjectionPar2712959773.h"

// System.Void TouchScript.Gestures.PinnedTransformGesture::.ctor()
extern "C"  void PinnedTransformGesture__ctor_m801456720 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.PinnedTransformGesture/ProjectionType TouchScript.Gestures.PinnedTransformGesture::get_Projection()
extern "C"  int32_t PinnedTransformGesture_get_Projection_m1915831200 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::set_Projection(TouchScript.Gestures.PinnedTransformGesture/ProjectionType)
extern "C"  void PinnedTransformGesture_set_Projection_m3326062441 (PinnedTransformGesture_t2478931093 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.PinnedTransformGesture::get_ProjectionPlaneNormal()
extern "C"  Vector3_t2243707580  PinnedTransformGesture_get_ProjectionPlaneNormal_m1230078373 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::set_ProjectionPlaneNormal(UnityEngine.Vector3)
extern "C"  void PinnedTransformGesture_set_ProjectionPlaneNormal_m2345958254 (PinnedTransformGesture_t2478931093 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane TouchScript.Gestures.PinnedTransformGesture::get_TransformPlane()
extern "C"  Plane_t3727654732  PinnedTransformGesture_get_TransformPlane_m3653287083 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.PinnedTransformGesture::get_RotationAxis()
extern "C"  Vector3_t2243707580  PinnedTransformGesture_get_RotationAxis_m1902994130 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::ApplyTransform(UnityEngine.Transform)
extern "C"  void PinnedTransformGesture_ApplyTransform_m3296449125 (PinnedTransformGesture_t2478931093 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::Awake()
extern "C"  void PinnedTransformGesture_Awake_m2709945359 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::OnEnable()
extern "C"  void PinnedTransformGesture_OnEnable_m265341716 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void PinnedTransformGesture_touchesBegan_m2688723644 (PinnedTransformGesture_t2478931093 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void PinnedTransformGesture_touchesMoved_m3167630242 (PinnedTransformGesture_t2478931093 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.PinnedTransformGesture::doRotation(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float PinnedTransformGesture_doRotation_m2311364625 (PinnedTransformGesture_t2478931093 * __this, Vector3_t2243707580  ___center0, Vector2_t2243707579  ___oldScreenPos1, Vector2_t2243707579  ___newScreenPos2, ProjectionParams_t2712959773 * ___projectionParams3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.PinnedTransformGesture::doScaling(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float PinnedTransformGesture_doScaling_m484955462 (PinnedTransformGesture_t2478931093 * __this, Vector3_t2243707580  ___center0, Vector2_t2243707579  ___oldScreenPos1, Vector2_t2243707579  ___newScreenPos2, ProjectionParams_t2712959773 * ___projectionParams3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::updateProjectionPlane()
extern "C"  void PinnedTransformGesture_updateProjectionPlane_m4102232998 (PinnedTransformGesture_t2478931093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformStarted_m3487094955 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformStarted_m3632073056 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_add_Transformed_m1454297587 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_remove_Transformed_m3019886148 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformCompleted_m1113765687 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PinnedTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void PinnedTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformCompleted_m1670513108 (PinnedTransformGesture_t2478931093 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
