﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/CommonName
struct CommonName_t2138393215;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/CommonName::.ctor()
extern "C"  void CommonName__ctor_m817223525 (CommonName_t2138393215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
