﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// MailInboxTableController
struct MailInboxTableController_t27963299;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInboxLine
struct  MailInboxLine_t3727558719  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text MailInboxLine::playerName
	Text_t356221433 * ___playerName_2;
	// UnityEngine.UI.Text MailInboxLine::subject
	Text_t356221433 * ___subject_3;
	// UnityEngine.UI.Text MailInboxLine::date
	Text_t356221433 * ___date_4;
	// System.Int64 MailInboxLine::messageId
	int64_t ___messageId_5;
	// MailInboxTableController MailInboxLine::owner
	MailInboxTableController_t27963299 * ___owner_6;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(MailInboxLine_t3727558719, ___playerName_2)); }
	inline Text_t356221433 * get_playerName_2() const { return ___playerName_2; }
	inline Text_t356221433 ** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(Text_t356221433 * value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_2, value);
	}

	inline static int32_t get_offset_of_subject_3() { return static_cast<int32_t>(offsetof(MailInboxLine_t3727558719, ___subject_3)); }
	inline Text_t356221433 * get_subject_3() const { return ___subject_3; }
	inline Text_t356221433 ** get_address_of_subject_3() { return &___subject_3; }
	inline void set_subject_3(Text_t356221433 * value)
	{
		___subject_3 = value;
		Il2CppCodeGenWriteBarrier(&___subject_3, value);
	}

	inline static int32_t get_offset_of_date_4() { return static_cast<int32_t>(offsetof(MailInboxLine_t3727558719, ___date_4)); }
	inline Text_t356221433 * get_date_4() const { return ___date_4; }
	inline Text_t356221433 ** get_address_of_date_4() { return &___date_4; }
	inline void set_date_4(Text_t356221433 * value)
	{
		___date_4 = value;
		Il2CppCodeGenWriteBarrier(&___date_4, value);
	}

	inline static int32_t get_offset_of_messageId_5() { return static_cast<int32_t>(offsetof(MailInboxLine_t3727558719, ___messageId_5)); }
	inline int64_t get_messageId_5() const { return ___messageId_5; }
	inline int64_t* get_address_of_messageId_5() { return &___messageId_5; }
	inline void set_messageId_5(int64_t value)
	{
		___messageId_5 = value;
	}

	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(MailInboxLine_t3727558719, ___owner_6)); }
	inline MailInboxTableController_t27963299 * get_owner_6() const { return ___owner_6; }
	inline MailInboxTableController_t27963299 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(MailInboxTableController_t27963299 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier(&___owner_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
