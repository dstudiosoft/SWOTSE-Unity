﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::.ctor()
#define LinkedList_1__ctor_m3042259333(__this, method) ((  void (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1__ctor_m1337573311_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m510495139(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1581322852 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))LinkedList_1__ctor_m1570295574_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2510846693(__this, ___value0, method) ((  void (*) (LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3874300024_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m3438319108(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1581322852 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2587830470(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4026686671_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m4018629077(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1585231526_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1471872653(__this, method) ((  bool (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m382194114_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m3311198888(__this, method) ((  bool (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2849171683_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3750980810(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m550341923_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m500598299(__this, ___node0, method) ((  void (*) (LinkedList_1_t1581322852 *, LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m1333791742_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::AddLast(T)
#define LinkedList_1_AddLast_m1837668616(__this, ___value0, method) ((  LinkedListNode_1_t172720536 * (*) (LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedList_1_AddLast_m3575571360_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Clear()
#define LinkedList_1_Clear_m1257631429(__this, method) ((  void (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_Clear_m3288132428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Contains(T)
#define LinkedList_1_Contains_m4206544387(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedList_1_Contains_m3220831510_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m3936286377(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1581322852 *, TableViewCellU5BU5D_t3121613254*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3700485924_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Find(T)
#define LinkedList_1_Find_m2890606441(__this, ___value0, method) ((  LinkedListNode_1_t172720536 * (*) (LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedList_1_Find_m1218372336_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m2755524525(__this, method) ((  Enumerator_t404228874  (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_GetEnumerator_m4181273888_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m80016896(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1581322852 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))LinkedList_1_GetObjectData_m2848049047_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m647430094(__this, ___sender0, method) ((  void (*) (LinkedList_1_t1581322852 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m264209791_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Remove(T)
#define LinkedList_1_Remove_m2203866404(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedList_1_Remove_m2925241645_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m2768112381(__this, ___node0, method) ((  void (*) (LinkedList_1_t1581322852 *, LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedList_1_Remove_m18160224_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m373822089(__this, method) ((  void (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1001176547_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::RemoveLast()
#define LinkedList_1_RemoveLast_m794265718(__this, method) ((  void (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_RemoveLast_m2904732739_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_Count()
#define LinkedList_1_get_Count_m1423178621(__this, method) ((  int32_t (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_get_Count_m402269195_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_First()
#define LinkedList_1_get_First_m1825090263(__this, method) ((  LinkedListNode_1_t172720536 * (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_get_First_m732033480_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_Last()
#define LinkedList_1_get_Last_m4048038647(__this, method) ((  LinkedListNode_1_t172720536 * (*) (LinkedList_1_t1581322852 *, const MethodInfo*))LinkedList_1_get_Last_m734243100_gshared)(__this, method)
