﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Image
struct Image_t2042527209;
// LanguageChangerContentManager
struct LanguageChangerContentManager_t3463970044;
// UserModel
struct UserModel_t3025569216;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsContentManager
struct  SettingsContentManager_t3562212113  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField SettingsContentManager::currentUserNick
	InputField_t1631627530 * ___currentUserNick_2;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserEmail
	InputField_t1631627530 * ___currentUserEmail_3;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserMobile
	InputField_t1631627530 * ___currentUserMobile_4;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserFlag
	InputField_t1631627530 * ___currentUserFlag_5;
	// UnityEngine.UI.Image SettingsContentManager::currentUserImage
	Image_t2042527209 * ___currentUserImage_6;
	// LanguageChangerContentManager SettingsContentManager::languageChanger
	LanguageChangerContentManager_t3463970044 * ___languageChanger_7;
	// UserModel SettingsContentManager::user
	UserModel_t3025569216 * ___user_8;
	// ConfirmationEvent SettingsContentManager::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_9;

public:
	inline static int32_t get_offset_of_currentUserNick_2() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___currentUserNick_2)); }
	inline InputField_t1631627530 * get_currentUserNick_2() const { return ___currentUserNick_2; }
	inline InputField_t1631627530 ** get_address_of_currentUserNick_2() { return &___currentUserNick_2; }
	inline void set_currentUserNick_2(InputField_t1631627530 * value)
	{
		___currentUserNick_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserNick_2, value);
	}

	inline static int32_t get_offset_of_currentUserEmail_3() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___currentUserEmail_3)); }
	inline InputField_t1631627530 * get_currentUserEmail_3() const { return ___currentUserEmail_3; }
	inline InputField_t1631627530 ** get_address_of_currentUserEmail_3() { return &___currentUserEmail_3; }
	inline void set_currentUserEmail_3(InputField_t1631627530 * value)
	{
		___currentUserEmail_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserEmail_3, value);
	}

	inline static int32_t get_offset_of_currentUserMobile_4() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___currentUserMobile_4)); }
	inline InputField_t1631627530 * get_currentUserMobile_4() const { return ___currentUserMobile_4; }
	inline InputField_t1631627530 ** get_address_of_currentUserMobile_4() { return &___currentUserMobile_4; }
	inline void set_currentUserMobile_4(InputField_t1631627530 * value)
	{
		___currentUserMobile_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserMobile_4, value);
	}

	inline static int32_t get_offset_of_currentUserFlag_5() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___currentUserFlag_5)); }
	inline InputField_t1631627530 * get_currentUserFlag_5() const { return ___currentUserFlag_5; }
	inline InputField_t1631627530 ** get_address_of_currentUserFlag_5() { return &___currentUserFlag_5; }
	inline void set_currentUserFlag_5(InputField_t1631627530 * value)
	{
		___currentUserFlag_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserFlag_5, value);
	}

	inline static int32_t get_offset_of_currentUserImage_6() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___currentUserImage_6)); }
	inline Image_t2042527209 * get_currentUserImage_6() const { return ___currentUserImage_6; }
	inline Image_t2042527209 ** get_address_of_currentUserImage_6() { return &___currentUserImage_6; }
	inline void set_currentUserImage_6(Image_t2042527209 * value)
	{
		___currentUserImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserImage_6, value);
	}

	inline static int32_t get_offset_of_languageChanger_7() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___languageChanger_7)); }
	inline LanguageChangerContentManager_t3463970044 * get_languageChanger_7() const { return ___languageChanger_7; }
	inline LanguageChangerContentManager_t3463970044 ** get_address_of_languageChanger_7() { return &___languageChanger_7; }
	inline void set_languageChanger_7(LanguageChangerContentManager_t3463970044 * value)
	{
		___languageChanger_7 = value;
		Il2CppCodeGenWriteBarrier(&___languageChanger_7, value);
	}

	inline static int32_t get_offset_of_user_8() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___user_8)); }
	inline UserModel_t3025569216 * get_user_8() const { return ___user_8; }
	inline UserModel_t3025569216 ** get_address_of_user_8() { return &___user_8; }
	inline void set_user_8(UserModel_t3025569216 * value)
	{
		___user_8 = value;
		Il2CppCodeGenWriteBarrier(&___user_8, value);
	}

	inline static int32_t get_offset_of_confirmed_9() { return static_cast<int32_t>(offsetof(SettingsContentManager_t3562212113, ___confirmed_9)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_9() const { return ___confirmed_9; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_9() { return &___confirmed_9; }
	inline void set_confirmed_9(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_9 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
