﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Title
struct Title_t3233171629;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/Title::.ctor()
extern "C"  void Title__ctor_m1559156075 (Title_t3233171629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
