﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.X509Certificate/CertificateContext
struct CertificateContext_t3886519681;
struct CertificateContext_t3886519681_marshaled_pinvoke;
struct CertificateContext_t3886519681_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct CertificateContext_t3886519681;
struct CertificateContext_t3886519681_marshaled_pinvoke;

extern "C" void CertificateContext_t3886519681_marshal_pinvoke(const CertificateContext_t3886519681& unmarshaled, CertificateContext_t3886519681_marshaled_pinvoke& marshaled);
extern "C" void CertificateContext_t3886519681_marshal_pinvoke_back(const CertificateContext_t3886519681_marshaled_pinvoke& marshaled, CertificateContext_t3886519681& unmarshaled);
extern "C" void CertificateContext_t3886519681_marshal_pinvoke_cleanup(CertificateContext_t3886519681_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CertificateContext_t3886519681;
struct CertificateContext_t3886519681_marshaled_com;

extern "C" void CertificateContext_t3886519681_marshal_com(const CertificateContext_t3886519681& unmarshaled, CertificateContext_t3886519681_marshaled_com& marshaled);
extern "C" void CertificateContext_t3886519681_marshal_com_back(const CertificateContext_t3886519681_marshaled_com& marshaled, CertificateContext_t3886519681& unmarshaled);
extern "C" void CertificateContext_t3886519681_marshal_com_cleanup(CertificateContext_t3886519681_marshaled_com& marshaled);
