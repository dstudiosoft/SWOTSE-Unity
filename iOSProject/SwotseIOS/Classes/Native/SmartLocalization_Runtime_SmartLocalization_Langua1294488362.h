﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2
struct  U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t1294488362  : public Il2CppObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::englishName
	String_t* ___englishName_0;

public:
	inline static int32_t get_offset_of_englishName_0() { return static_cast<int32_t>(offsetof(U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t1294488362, ___englishName_0)); }
	inline String_t* get_englishName_0() const { return ___englishName_0; }
	inline String_t** get_address_of_englishName_0() { return &___englishName_0; }
	inline void set_englishName_0(String_t* value)
	{
		___englishName_0 = value;
		Il2CppCodeGenWriteBarrier(&___englishName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
