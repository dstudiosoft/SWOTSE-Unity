﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnityLoggingService
struct UnityLoggingService_t1590581383;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Unity.UnityLoggingService::.ctor()
extern "C"  void UnityLoggingService__ctor_m3464978716 (UnityLoggingService_t1590581383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::get_Instance()
extern "C"  UnityLoggingService_t1590581383 * UnityLoggingService_get_Instance_m695600932 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnityLoggingService::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void UnityLoggingService_LogMessage_m791045356 (UnityLoggingService_t1590581383 * __this, int32_t ___level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnityLoggingService::.cctor()
extern "C"  void UnityLoggingService__cctor_m1258159725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
