﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedAudioSource
struct  LocalizedAudioSource_t450990658  : public MonoBehaviour_t1158329972
{
public:
	// System.String SmartLocalization.LocalizedAudioSource::localizedKey
	String_t* ___localizedKey_2;
	// UnityEngine.AudioClip SmartLocalization.LocalizedAudioSource::audioClip
	AudioClip_t1932558630 * ___audioClip_3;
	// UnityEngine.AudioSource SmartLocalization.LocalizedAudioSource::audioSource
	AudioSource_t1135106623 * ___audioSource_4;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t450990658, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___localizedKey_2, value);
	}

	inline static int32_t get_offset_of_audioClip_3() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t450990658, ___audioClip_3)); }
	inline AudioClip_t1932558630 * get_audioClip_3() const { return ___audioClip_3; }
	inline AudioClip_t1932558630 ** get_address_of_audioClip_3() { return &___audioClip_3; }
	inline void set_audioClip_3(AudioClip_t1932558630 * value)
	{
		___audioClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_3, value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t450990658, ___audioSource_4)); }
	inline AudioSource_t1135106623 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t1135106623 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
