﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportResourcesTableController
struct BattleReportResourcesTableController_t4005631621;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void BattleReportResourcesTableController::.ctor()
extern "C"  void BattleReportResourcesTableController__ctor_m657187554 (BattleReportResourcesTableController_t4005631621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesTableController::Start()
extern "C"  void BattleReportResourcesTableController_Start_m2647149714 (BattleReportResourcesTableController_t4005631621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BattleReportResourcesTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t BattleReportResourcesTableController_GetNumberOfRowsForTableView_m2008415084 (BattleReportResourcesTableController_t4005631621 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BattleReportResourcesTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float BattleReportResourcesTableController_GetHeightForRowInTableView_m3850235732 (BattleReportResourcesTableController_t4005631621 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell BattleReportResourcesTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * BattleReportResourcesTableController_GetCellForRowInTableView_m1389579433 (BattleReportResourcesTableController_t4005631621 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BattleReportResourcesTableController::Format(System.Int64)
extern "C"  String_t* BattleReportResourcesTableController_Format_m390855574 (BattleReportResourcesTableController_t4005631621 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
