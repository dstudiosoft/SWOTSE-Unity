﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyModel
struct  ArmyModel_t2157120938  : public Il2CppObject
{
public:
	// System.Int64 ArmyModel::id
	int64_t ___id_0;
	// System.Int64 ArmyModel::worldMap
	int64_t ___worldMap_1;
	// ArmyGeneralModel ArmyModel::hero
	ArmyGeneralModel_t609759248 * ___hero_2;
	// WorldFieldModel ArmyModel::destination_point
	WorldFieldModel_t3469935653 * ___destination_point_3;
	// System.Int64 ArmyModel::workers_count
	int64_t ___workers_count_4;
	// System.Int64 ArmyModel::spy_count
	int64_t ___spy_count_5;
	// System.Int64 ArmyModel::swards_man_count
	int64_t ___swards_man_count_6;
	// System.Int64 ArmyModel::spear_man_count
	int64_t ___spear_man_count_7;
	// System.Int64 ArmyModel::pike_man_count
	int64_t ___pike_man_count_8;
	// System.Int64 ArmyModel::scout_rider_count
	int64_t ___scout_rider_count_9;
	// System.Int64 ArmyModel::light_cavalry_count
	int64_t ___light_cavalry_count_10;
	// System.Int64 ArmyModel::heavy_cavalry_count
	int64_t ___heavy_cavalry_count_11;
	// System.Int64 ArmyModel::archer_count
	int64_t ___archer_count_12;
	// System.Int64 ArmyModel::archer_rider_count
	int64_t ___archer_rider_count_13;
	// System.Int64 ArmyModel::holly_man_count
	int64_t ___holly_man_count_14;
	// System.Int64 ArmyModel::wagon_count
	int64_t ___wagon_count_15;
	// System.Int64 ArmyModel::trebuchet_count
	int64_t ___trebuchet_count_16;
	// System.Int64 ArmyModel::siege_towers_count
	int64_t ___siege_towers_count_17;
	// System.Int64 ArmyModel::battering_ram_count
	int64_t ___battering_ram_count_18;
	// System.Int64 ArmyModel::ballista_count
	int64_t ___ballista_count_19;
	// System.String ArmyModel::army_status
	String_t* ___army_status_20;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_worldMap_1() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___worldMap_1)); }
	inline int64_t get_worldMap_1() const { return ___worldMap_1; }
	inline int64_t* get_address_of_worldMap_1() { return &___worldMap_1; }
	inline void set_worldMap_1(int64_t value)
	{
		___worldMap_1 = value;
	}

	inline static int32_t get_offset_of_hero_2() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___hero_2)); }
	inline ArmyGeneralModel_t609759248 * get_hero_2() const { return ___hero_2; }
	inline ArmyGeneralModel_t609759248 ** get_address_of_hero_2() { return &___hero_2; }
	inline void set_hero_2(ArmyGeneralModel_t609759248 * value)
	{
		___hero_2 = value;
		Il2CppCodeGenWriteBarrier(&___hero_2, value);
	}

	inline static int32_t get_offset_of_destination_point_3() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___destination_point_3)); }
	inline WorldFieldModel_t3469935653 * get_destination_point_3() const { return ___destination_point_3; }
	inline WorldFieldModel_t3469935653 ** get_address_of_destination_point_3() { return &___destination_point_3; }
	inline void set_destination_point_3(WorldFieldModel_t3469935653 * value)
	{
		___destination_point_3 = value;
		Il2CppCodeGenWriteBarrier(&___destination_point_3, value);
	}

	inline static int32_t get_offset_of_workers_count_4() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___workers_count_4)); }
	inline int64_t get_workers_count_4() const { return ___workers_count_4; }
	inline int64_t* get_address_of_workers_count_4() { return &___workers_count_4; }
	inline void set_workers_count_4(int64_t value)
	{
		___workers_count_4 = value;
	}

	inline static int32_t get_offset_of_spy_count_5() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___spy_count_5)); }
	inline int64_t get_spy_count_5() const { return ___spy_count_5; }
	inline int64_t* get_address_of_spy_count_5() { return &___spy_count_5; }
	inline void set_spy_count_5(int64_t value)
	{
		___spy_count_5 = value;
	}

	inline static int32_t get_offset_of_swards_man_count_6() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___swards_man_count_6)); }
	inline int64_t get_swards_man_count_6() const { return ___swards_man_count_6; }
	inline int64_t* get_address_of_swards_man_count_6() { return &___swards_man_count_6; }
	inline void set_swards_man_count_6(int64_t value)
	{
		___swards_man_count_6 = value;
	}

	inline static int32_t get_offset_of_spear_man_count_7() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___spear_man_count_7)); }
	inline int64_t get_spear_man_count_7() const { return ___spear_man_count_7; }
	inline int64_t* get_address_of_spear_man_count_7() { return &___spear_man_count_7; }
	inline void set_spear_man_count_7(int64_t value)
	{
		___spear_man_count_7 = value;
	}

	inline static int32_t get_offset_of_pike_man_count_8() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___pike_man_count_8)); }
	inline int64_t get_pike_man_count_8() const { return ___pike_man_count_8; }
	inline int64_t* get_address_of_pike_man_count_8() { return &___pike_man_count_8; }
	inline void set_pike_man_count_8(int64_t value)
	{
		___pike_man_count_8 = value;
	}

	inline static int32_t get_offset_of_scout_rider_count_9() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___scout_rider_count_9)); }
	inline int64_t get_scout_rider_count_9() const { return ___scout_rider_count_9; }
	inline int64_t* get_address_of_scout_rider_count_9() { return &___scout_rider_count_9; }
	inline void set_scout_rider_count_9(int64_t value)
	{
		___scout_rider_count_9 = value;
	}

	inline static int32_t get_offset_of_light_cavalry_count_10() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___light_cavalry_count_10)); }
	inline int64_t get_light_cavalry_count_10() const { return ___light_cavalry_count_10; }
	inline int64_t* get_address_of_light_cavalry_count_10() { return &___light_cavalry_count_10; }
	inline void set_light_cavalry_count_10(int64_t value)
	{
		___light_cavalry_count_10 = value;
	}

	inline static int32_t get_offset_of_heavy_cavalry_count_11() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___heavy_cavalry_count_11)); }
	inline int64_t get_heavy_cavalry_count_11() const { return ___heavy_cavalry_count_11; }
	inline int64_t* get_address_of_heavy_cavalry_count_11() { return &___heavy_cavalry_count_11; }
	inline void set_heavy_cavalry_count_11(int64_t value)
	{
		___heavy_cavalry_count_11 = value;
	}

	inline static int32_t get_offset_of_archer_count_12() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___archer_count_12)); }
	inline int64_t get_archer_count_12() const { return ___archer_count_12; }
	inline int64_t* get_address_of_archer_count_12() { return &___archer_count_12; }
	inline void set_archer_count_12(int64_t value)
	{
		___archer_count_12 = value;
	}

	inline static int32_t get_offset_of_archer_rider_count_13() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___archer_rider_count_13)); }
	inline int64_t get_archer_rider_count_13() const { return ___archer_rider_count_13; }
	inline int64_t* get_address_of_archer_rider_count_13() { return &___archer_rider_count_13; }
	inline void set_archer_rider_count_13(int64_t value)
	{
		___archer_rider_count_13 = value;
	}

	inline static int32_t get_offset_of_holly_man_count_14() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___holly_man_count_14)); }
	inline int64_t get_holly_man_count_14() const { return ___holly_man_count_14; }
	inline int64_t* get_address_of_holly_man_count_14() { return &___holly_man_count_14; }
	inline void set_holly_man_count_14(int64_t value)
	{
		___holly_man_count_14 = value;
	}

	inline static int32_t get_offset_of_wagon_count_15() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___wagon_count_15)); }
	inline int64_t get_wagon_count_15() const { return ___wagon_count_15; }
	inline int64_t* get_address_of_wagon_count_15() { return &___wagon_count_15; }
	inline void set_wagon_count_15(int64_t value)
	{
		___wagon_count_15 = value;
	}

	inline static int32_t get_offset_of_trebuchet_count_16() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___trebuchet_count_16)); }
	inline int64_t get_trebuchet_count_16() const { return ___trebuchet_count_16; }
	inline int64_t* get_address_of_trebuchet_count_16() { return &___trebuchet_count_16; }
	inline void set_trebuchet_count_16(int64_t value)
	{
		___trebuchet_count_16 = value;
	}

	inline static int32_t get_offset_of_siege_towers_count_17() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___siege_towers_count_17)); }
	inline int64_t get_siege_towers_count_17() const { return ___siege_towers_count_17; }
	inline int64_t* get_address_of_siege_towers_count_17() { return &___siege_towers_count_17; }
	inline void set_siege_towers_count_17(int64_t value)
	{
		___siege_towers_count_17 = value;
	}

	inline static int32_t get_offset_of_battering_ram_count_18() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___battering_ram_count_18)); }
	inline int64_t get_battering_ram_count_18() const { return ___battering_ram_count_18; }
	inline int64_t* get_address_of_battering_ram_count_18() { return &___battering_ram_count_18; }
	inline void set_battering_ram_count_18(int64_t value)
	{
		___battering_ram_count_18 = value;
	}

	inline static int32_t get_offset_of_ballista_count_19() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___ballista_count_19)); }
	inline int64_t get_ballista_count_19() const { return ___ballista_count_19; }
	inline int64_t* get_address_of_ballista_count_19() { return &___ballista_count_19; }
	inline void set_ballista_count_19(int64_t value)
	{
		___ballista_count_19 = value;
	}

	inline static int32_t get_offset_of_army_status_20() { return static_cast<int32_t>(offsetof(ArmyModel_t2157120938, ___army_status_20)); }
	inline String_t* get_army_status_20() const { return ___army_status_20; }
	inline String_t** get_address_of_army_status_20() { return &___army_status_20; }
	inline void set_army_status_20(String_t* value)
	{
		___army_status_20 = value;
		Il2CppCodeGenWriteBarrier(&___army_status_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
