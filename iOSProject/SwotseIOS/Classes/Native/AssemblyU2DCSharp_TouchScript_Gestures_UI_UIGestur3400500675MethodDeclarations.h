﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.UI.UIGesture
struct UIGesture_t3400500675;
// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture2352305985.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Gestures.UI.UIGesture::.ctor()
extern "C"  void UIGesture__ctor_m2155629292 (UIGesture_t3400500675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.UI.UIGesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern "C"  bool UIGesture_CanPreventGesture_m2400646480 (UIGesture_t3400500675 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.UI.UIGesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern "C"  bool UIGesture_CanBePreventedByGesture_m3306286421 (UIGesture_t3400500675 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void UIGesture_touchesBegan_m4224003792 (UIGesture_t3400500675 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void UIGesture_touchesMoved_m3402816982 (UIGesture_t3400500675 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void UIGesture_touchesEnded_m739834155 (UIGesture_t3400500675 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::touchesCancelled(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void UIGesture_touchesCancelled_m674821446 (UIGesture_t3400500675 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.UI.UIGesture/TouchData TouchScript.Gestures.UI.UIGesture::getPointerData(TouchScript.TouchPoint)
extern "C"  TouchData_t3880599975  UIGesture_getPointerData_m2537422534 (UIGesture_t3400500675 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::setPointerData(TouchScript.TouchPoint,TouchScript.Gestures.UI.UIGesture/TouchData)
extern "C"  void UIGesture_setPointerData_m2990321381 (UIGesture_t3400500675 * __this, TouchPoint_t959629083 * ___touch0, TouchData_t3880599975  ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.UI.UIGesture::removePointerData(TouchScript.TouchPoint)
extern "C"  void UIGesture_removePointerData_m986032726 (UIGesture_t3400500675 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
