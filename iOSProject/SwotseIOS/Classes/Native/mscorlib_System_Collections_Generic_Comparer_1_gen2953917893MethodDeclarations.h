﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.RaycastHit2D>
struct Comparer_1_t2953917893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void Comparer_1__ctor_m3539161570_gshared (Comparer_1_t2953917893 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3539161570(__this, method) ((  void (*) (Comparer_1_t2953917893 *, const MethodInfo*))Comparer_1__ctor_m3539161570_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RaycastHit2D>::.cctor()
extern "C"  void Comparer_1__cctor_m4138365013_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4138365013(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4138365013_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.RaycastHit2D>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1748755929_gshared (Comparer_1_t2953917893 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1748755929(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2953917893 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1748755929_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.RaycastHit2D>::get_Default()
extern "C"  Comparer_1_t2953917893 * Comparer_1_get_Default_m832351038_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m832351038(__this /* static, unused */, method) ((  Comparer_1_t2953917893 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m832351038_gshared)(__this /* static, unused */, method)
