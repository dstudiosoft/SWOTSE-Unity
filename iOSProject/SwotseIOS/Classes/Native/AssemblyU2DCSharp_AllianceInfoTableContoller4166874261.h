﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceInfoAlliancesLine
struct AllianceInfoAlliancesLine_t3814560029;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<AllianceModel>
struct List_1_t1204012330;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInfoTableContoller
struct  AllianceInfoTableContoller_t4166874261  : public MonoBehaviour_t1158329972
{
public:
	// AllianceInfoAlliancesLine AllianceInfoTableContoller::m_cellPrefab
	AllianceInfoAlliancesLine_t3814560029 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceInfoTableContoller::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Boolean AllianceInfoTableContoller::neutral
	bool ___neutral_4;
	// System.Boolean AllianceInfoTableContoller::ally
	bool ___ally_5;
	// System.Boolean AllianceInfoTableContoller::enemy
	bool ___enemy_6;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::neutralAlliances
	List_1_t1204012330 * ___neutralAlliances_7;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::allyAlliances
	List_1_t1204012330 * ___allyAlliances_8;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::enemyAlliances
	List_1_t1204012330 * ___enemyAlliances_9;
	// System.Int32 AllianceInfoTableContoller::m_numRows
	int32_t ___m_numRows_10;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___m_cellPrefab_2)); }
	inline AllianceInfoAlliancesLine_t3814560029 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceInfoAlliancesLine_t3814560029 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceInfoAlliancesLine_t3814560029 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_neutral_4() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___neutral_4)); }
	inline bool get_neutral_4() const { return ___neutral_4; }
	inline bool* get_address_of_neutral_4() { return &___neutral_4; }
	inline void set_neutral_4(bool value)
	{
		___neutral_4 = value;
	}

	inline static int32_t get_offset_of_ally_5() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___ally_5)); }
	inline bool get_ally_5() const { return ___ally_5; }
	inline bool* get_address_of_ally_5() { return &___ally_5; }
	inline void set_ally_5(bool value)
	{
		___ally_5 = value;
	}

	inline static int32_t get_offset_of_enemy_6() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___enemy_6)); }
	inline bool get_enemy_6() const { return ___enemy_6; }
	inline bool* get_address_of_enemy_6() { return &___enemy_6; }
	inline void set_enemy_6(bool value)
	{
		___enemy_6 = value;
	}

	inline static int32_t get_offset_of_neutralAlliances_7() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___neutralAlliances_7)); }
	inline List_1_t1204012330 * get_neutralAlliances_7() const { return ___neutralAlliances_7; }
	inline List_1_t1204012330 ** get_address_of_neutralAlliances_7() { return &___neutralAlliances_7; }
	inline void set_neutralAlliances_7(List_1_t1204012330 * value)
	{
		___neutralAlliances_7 = value;
		Il2CppCodeGenWriteBarrier(&___neutralAlliances_7, value);
	}

	inline static int32_t get_offset_of_allyAlliances_8() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___allyAlliances_8)); }
	inline List_1_t1204012330 * get_allyAlliances_8() const { return ___allyAlliances_8; }
	inline List_1_t1204012330 ** get_address_of_allyAlliances_8() { return &___allyAlliances_8; }
	inline void set_allyAlliances_8(List_1_t1204012330 * value)
	{
		___allyAlliances_8 = value;
		Il2CppCodeGenWriteBarrier(&___allyAlliances_8, value);
	}

	inline static int32_t get_offset_of_enemyAlliances_9() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___enemyAlliances_9)); }
	inline List_1_t1204012330 * get_enemyAlliances_9() const { return ___enemyAlliances_9; }
	inline List_1_t1204012330 ** get_address_of_enemyAlliances_9() { return &___enemyAlliances_9; }
	inline void set_enemyAlliances_9(List_1_t1204012330 * value)
	{
		___enemyAlliances_9 = value;
		Il2CppCodeGenWriteBarrier(&___enemyAlliances_9, value);
	}

	inline static int32_t get_offset_of_m_numRows_10() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4166874261, ___m_numRows_10)); }
	inline int32_t get_m_numRows_10() const { return ___m_numRows_10; }
	inline int32_t* get_address_of_m_numRows_10() { return &___m_numRows_10; }
	inline void set_m_numRows_10(int32_t value)
	{
		___m_numRows_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
