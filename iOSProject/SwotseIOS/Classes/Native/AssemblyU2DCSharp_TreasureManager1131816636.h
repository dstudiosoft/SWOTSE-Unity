﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.OrderedDictionary
struct OrderedDictionary_t14805809;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager
struct  TreasureManager_t1131816636  : public Il2CppObject
{
public:
	// System.Collections.Specialized.OrderedDictionary TreasureManager::scriptTreasures
	OrderedDictionary_t14805809 * ___scriptTreasures_0;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::speedUpTreasures
	OrderedDictionary_t14805809 * ___speedUpTreasures_1;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::productionTreasures
	OrderedDictionary_t14805809 * ___productionTreasures_2;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::diamondTreasures
	OrderedDictionary_t14805809 * ___diamondTreasures_3;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::promotionsTreasures
	OrderedDictionary_t14805809 * ___promotionsTreasures_4;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::armyTreasures
	OrderedDictionary_t14805809 * ___armyTreasures_5;
	// SimpleEvent TreasureManager::onGetItems
	SimpleEvent_t1509017202 * ___onGetItems_6;
	// SimpleEvent TreasureManager::onItemBought
	SimpleEvent_t1509017202 * ___onItemBought_7;
	// SimpleEvent TreasureManager::onItemSold
	SimpleEvent_t1509017202 * ___onItemSold_8;
	// SimpleEvent TreasureManager::onItemGifted
	SimpleEvent_t1509017202 * ___onItemGifted_9;
	// SimpleEvent TreasureManager::onItemUsed
	SimpleEvent_t1509017202 * ___onItemUsed_10;

public:
	inline static int32_t get_offset_of_scriptTreasures_0() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___scriptTreasures_0)); }
	inline OrderedDictionary_t14805809 * get_scriptTreasures_0() const { return ___scriptTreasures_0; }
	inline OrderedDictionary_t14805809 ** get_address_of_scriptTreasures_0() { return &___scriptTreasures_0; }
	inline void set_scriptTreasures_0(OrderedDictionary_t14805809 * value)
	{
		___scriptTreasures_0 = value;
		Il2CppCodeGenWriteBarrier(&___scriptTreasures_0, value);
	}

	inline static int32_t get_offset_of_speedUpTreasures_1() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___speedUpTreasures_1)); }
	inline OrderedDictionary_t14805809 * get_speedUpTreasures_1() const { return ___speedUpTreasures_1; }
	inline OrderedDictionary_t14805809 ** get_address_of_speedUpTreasures_1() { return &___speedUpTreasures_1; }
	inline void set_speedUpTreasures_1(OrderedDictionary_t14805809 * value)
	{
		___speedUpTreasures_1 = value;
		Il2CppCodeGenWriteBarrier(&___speedUpTreasures_1, value);
	}

	inline static int32_t get_offset_of_productionTreasures_2() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___productionTreasures_2)); }
	inline OrderedDictionary_t14805809 * get_productionTreasures_2() const { return ___productionTreasures_2; }
	inline OrderedDictionary_t14805809 ** get_address_of_productionTreasures_2() { return &___productionTreasures_2; }
	inline void set_productionTreasures_2(OrderedDictionary_t14805809 * value)
	{
		___productionTreasures_2 = value;
		Il2CppCodeGenWriteBarrier(&___productionTreasures_2, value);
	}

	inline static int32_t get_offset_of_diamondTreasures_3() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___diamondTreasures_3)); }
	inline OrderedDictionary_t14805809 * get_diamondTreasures_3() const { return ___diamondTreasures_3; }
	inline OrderedDictionary_t14805809 ** get_address_of_diamondTreasures_3() { return &___diamondTreasures_3; }
	inline void set_diamondTreasures_3(OrderedDictionary_t14805809 * value)
	{
		___diamondTreasures_3 = value;
		Il2CppCodeGenWriteBarrier(&___diamondTreasures_3, value);
	}

	inline static int32_t get_offset_of_promotionsTreasures_4() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___promotionsTreasures_4)); }
	inline OrderedDictionary_t14805809 * get_promotionsTreasures_4() const { return ___promotionsTreasures_4; }
	inline OrderedDictionary_t14805809 ** get_address_of_promotionsTreasures_4() { return &___promotionsTreasures_4; }
	inline void set_promotionsTreasures_4(OrderedDictionary_t14805809 * value)
	{
		___promotionsTreasures_4 = value;
		Il2CppCodeGenWriteBarrier(&___promotionsTreasures_4, value);
	}

	inline static int32_t get_offset_of_armyTreasures_5() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___armyTreasures_5)); }
	inline OrderedDictionary_t14805809 * get_armyTreasures_5() const { return ___armyTreasures_5; }
	inline OrderedDictionary_t14805809 ** get_address_of_armyTreasures_5() { return &___armyTreasures_5; }
	inline void set_armyTreasures_5(OrderedDictionary_t14805809 * value)
	{
		___armyTreasures_5 = value;
		Il2CppCodeGenWriteBarrier(&___armyTreasures_5, value);
	}

	inline static int32_t get_offset_of_onGetItems_6() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___onGetItems_6)); }
	inline SimpleEvent_t1509017202 * get_onGetItems_6() const { return ___onGetItems_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetItems_6() { return &___onGetItems_6; }
	inline void set_onGetItems_6(SimpleEvent_t1509017202 * value)
	{
		___onGetItems_6 = value;
		Il2CppCodeGenWriteBarrier(&___onGetItems_6, value);
	}

	inline static int32_t get_offset_of_onItemBought_7() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___onItemBought_7)); }
	inline SimpleEvent_t1509017202 * get_onItemBought_7() const { return ___onItemBought_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onItemBought_7() { return &___onItemBought_7; }
	inline void set_onItemBought_7(SimpleEvent_t1509017202 * value)
	{
		___onItemBought_7 = value;
		Il2CppCodeGenWriteBarrier(&___onItemBought_7, value);
	}

	inline static int32_t get_offset_of_onItemSold_8() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___onItemSold_8)); }
	inline SimpleEvent_t1509017202 * get_onItemSold_8() const { return ___onItemSold_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onItemSold_8() { return &___onItemSold_8; }
	inline void set_onItemSold_8(SimpleEvent_t1509017202 * value)
	{
		___onItemSold_8 = value;
		Il2CppCodeGenWriteBarrier(&___onItemSold_8, value);
	}

	inline static int32_t get_offset_of_onItemGifted_9() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___onItemGifted_9)); }
	inline SimpleEvent_t1509017202 * get_onItemGifted_9() const { return ___onItemGifted_9; }
	inline SimpleEvent_t1509017202 ** get_address_of_onItemGifted_9() { return &___onItemGifted_9; }
	inline void set_onItemGifted_9(SimpleEvent_t1509017202 * value)
	{
		___onItemGifted_9 = value;
		Il2CppCodeGenWriteBarrier(&___onItemGifted_9, value);
	}

	inline static int32_t get_offset_of_onItemUsed_10() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636, ___onItemUsed_10)); }
	inline SimpleEvent_t1509017202 * get_onItemUsed_10() const { return ___onItemUsed_10; }
	inline SimpleEvent_t1509017202 ** get_address_of_onItemUsed_10() { return &___onItemUsed_10; }
	inline void set_onItemUsed_10(SimpleEvent_t1509017202 * value)
	{
		___onItemUsed_10 = value;
		Il2CppCodeGenWriteBarrier(&___onItemUsed_10, value);
	}
};

struct TreasureManager_t1131816636_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> TreasureManager::<>f__switch$map1A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1A_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_11() { return static_cast<int32_t>(offsetof(TreasureManager_t1131816636_StaticFields, ___U3CU3Ef__switchU24map1A_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1A_11() const { return ___U3CU3Ef__switchU24map1A_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1A_11() { return &___U3CU3Ef__switchU24map1A_11; }
	inline void set_U3CU3Ef__switchU24map1A_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1A_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1A_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
