﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t4250402154;
// System.ComponentModel.MemberDescriptor
struct MemberDescriptor_t3749827553;
// System.ComponentModel.IComponent
struct IComponent_t1000253244;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_t962731901;
// System.ComponentModel.ListSortDescription
struct ListSortDescription_t3194554012;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t2438624375;
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>
struct LinkedList_1_t2743332604;
// System.ComponentModel.WeakObjectWrapper
struct WeakObjectWrapper_t2012978780;
// System.Net.NetworkInformation.GatewayIPAddressInformation
struct GatewayIPAddressInformation_t153474369;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.NetworkInformation.IPAddressInformation
struct IPAddressInformation_t732454853;
// System.Net.NetworkInformation.TcpConnectionInformation
struct TcpConnectionInformation_t3392050187;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCPROW
struct Win32_MIB_TCPROW_t3355732075;
// System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW
struct Win32_MIB_TCP6ROW_t4007155549;
// System.Net.NetworkInformation.MulticastIPAddressInformation
struct MulticastIPAddressInformation_t1580768453;
// System.Net.NetworkInformation.NetworkInterface
struct NetworkInterface_t63927633;
// System.Net.NetworkInformation.LinuxNetworkInterface
struct LinuxNetworkInterface_t3864470295;
// System.Net.NetworkInformation.MacOsNetworkInterface
struct MacOsNetworkInterface_t1454185290;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t680756680;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_INFO
struct Win32_IP_ADAPTER_INFO_t2310876292;
// System.Net.NetworkInformation.UnicastIPAddressInformation
struct UnicastIPAddressInformation_t2919786644;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Net.Cookie
struct Cookie_t3154017544;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_t528874471;
// System.Security.Cryptography.X509Certificates.X509Extension
struct X509Extension_t1320896183;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t463456204;
// System.Security.Cryptography.Oid
struct Oid_t3221867120;
// System.Text.RegularExpressions.Capture
struct Capture_t4157900610;
// System.Text.RegularExpressions.Group
struct Group_t3761430853;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>
struct LinkedList_1_t1581322852;

#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"
#include "System_System_ComponentModel_ListSortDescription3194554012.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2743332604.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_Net_NetworkInformation_GatewayIPAddre153474369.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADDR2646152127.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_NetworkInformation_IPAddressInfor732454853.h"
#include "System_System_Net_NetworkInformation_TcpConnection3392050187.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_NetworkInformation_Win32IPGlobal3355732075.h"
#include "System_System_Net_NetworkInformation_Win32IPGlobal4007155549.h"
#include "System_System_Net_NetworkInformation_MulticastIPAd1580768453.h"
#include "System_System_Net_NetworkInformation_NetworkInterfac63927633.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1454185290.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAPT680756680.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP2310876292.h"
#include "System_System_Net_NetworkInformation_UnicastIPAddr2919786644.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_Socket_WSABUF2199312694.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "System_System_Security_Cryptography_X509Certificate528874471.h"
#include "System_System_Security_Cryptography_X509Certificat1320896183.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "System_System_Security_Cryptography_Oid3221867120.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_Group3761430853.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1581322852.h"

#pragma once
// System.Collections.Generic.RBTree/Node[]
struct NodeU5BU5D_t591605987  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Node_t2499136326 * m_Items[1];

public:
	inline Node_t2499136326 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Node_t2499136326 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Node_t2499136326 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_t2049367471  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyDescriptor_t4250402154 * m_Items[1];

public:
	inline PropertyDescriptor_t4250402154 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyDescriptor_t4250402154 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyDescriptor_t4250402154 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.MemberDescriptor[]
struct MemberDescriptorU5BU5D_t3789464444  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberDescriptor_t3749827553 * m_Items[1];

public:
	inline MemberDescriptor_t3749827553 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberDescriptor_t3749827553 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberDescriptor_t3749827553 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.IComponent[]
struct IComponentU5BU5D_t1193609301  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.EventDescriptor[]
struct EventDescriptorU5BU5D_t2599422448  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventDescriptor_t962731901 * m_Items[1];

public:
	inline EventDescriptor_t962731901 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventDescriptor_t962731901 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventDescriptor_t962731901 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.ListSortDescription[]
struct ListSortDescriptionU5BU5D_t3730792309  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ListSortDescription_t3194554012 * m_Items[1];

public:
	inline ListSortDescription_t3194554012 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ListSortDescription_t3194554012 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ListSortDescription_t3194554012 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.TypeDescriptionProvider[]
struct TypeDescriptionProviderU5BU5D_t1447751630  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeDescriptionProvider_t2438624375 * m_Items[1];

public:
	inline TypeDescriptionProvider_t2438624375 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeDescriptionProvider_t2438624375 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeDescriptionProvider_t2438624375 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>[]
struct LinkedList_1U5BU5D_t1362160021  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedList_1_t2743332604 * m_Items[1];

public:
	inline LinkedList_1_t2743332604 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedList_1_t2743332604 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedList_1_t2743332604 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.WeakObjectWrapper[]
struct WeakObjectWrapperU5BU5D_t2488185013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WeakObjectWrapper_t2012978780 * m_Items[1];

public:
	inline WeakObjectWrapper_t2012978780 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WeakObjectWrapper_t2012978780 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WeakObjectWrapper_t2012978780 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.GatewayIPAddressInformation[]
struct GatewayIPAddressInformationU5BU5D_t566923420  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GatewayIPAddressInformation_t153474369 * m_Items[1];

public:
	inline GatewayIPAddressInformation_t153474369 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GatewayIPAddressInformation_t153474369 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GatewayIPAddressInformation_t153474369 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32_IP_ADDR_STRING[]
struct Win32_IP_ADDR_STRINGU5BU5D_t945341030  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_IP_ADDR_STRING_t2646152127  m_Items[1];

public:
	inline Win32_IP_ADDR_STRING_t2646152127  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_IP_ADDR_STRING_t2646152127 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_IP_ADDR_STRING_t2646152127  value)
	{
		m_Items[index] = value;
	}
};
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IPAddress_t1399971723 * m_Items[1];

public:
	inline IPAddress_t1399971723 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IPAddress_t1399971723 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IPAddress_t1399971723 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.IPAddressInformation[]
struct IPAddressInformationU5BU5D_t4161181576  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IPAddressInformation_t732454853 * m_Items[1];

public:
	inline IPAddressInformation_t732454853 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IPAddressInformation_t732454853 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IPAddressInformation_t732454853 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.TcpConnectionInformation[]
struct TcpConnectionInformationU5BU5D_t910531434  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TcpConnectionInformation_t3392050187 * m_Items[1];

public:
	inline TcpConnectionInformation_t3392050187 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TcpConnectionInformation_t3392050187 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TcpConnectionInformation_t3392050187 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.IPEndPoint[]
struct IPEndPointU5BU5D_t294524195  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IPEndPoint_t2615413766 * m_Items[1];

public:
	inline IPEndPoint_t2615413766 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IPEndPoint_t2615413766 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IPEndPoint_t2615413766 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.EndPoint[]
struct EndPointU5BU5D_t3044655698  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EndPoint_t4156119363 * m_Items[1];

public:
	inline EndPoint_t4156119363 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EndPoint_t4156119363 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EndPoint_t4156119363 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCPROW[]
struct Win32_MIB_TCPROWU5BU5D_t2455177610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_MIB_TCPROW_t3355732075 * m_Items[1];

public:
	inline Win32_MIB_TCPROW_t3355732075 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_MIB_TCPROW_t3355732075 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_MIB_TCPROW_t3355732075 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW[]
struct Win32_MIB_TCP6ROWU5BU5D_t3073218192  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_MIB_TCP6ROW_t4007155549 * m_Items[1];

public:
	inline Win32_MIB_TCP6ROW_t4007155549 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_MIB_TCP6ROW_t4007155549 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_MIB_TCP6ROW_t4007155549 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.MulticastIPAddressInformation[]
struct MulticastIPAddressInformationU5BU5D_t302302856  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MulticastIPAddressInformation_t1580768453 * m_Items[1];

public:
	inline MulticastIPAddressInformation_t1580768453 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MulticastIPAddressInformation_t1580768453 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MulticastIPAddressInformation_t1580768453 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.NetworkInterface[]
struct NetworkInterfaceU5BU5D_t4157644364  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkInterface_t63927633 * m_Items[1];

public:
	inline NetworkInterface_t63927633 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NetworkInterface_t63927633 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NetworkInterface_t63927633 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.LinuxNetworkInterface[]
struct LinuxNetworkInterfaceU5BU5D_t4181037742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinuxNetworkInterface_t3864470295 * m_Items[1];

public:
	inline LinuxNetworkInterface_t3864470295 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinuxNetworkInterface_t3864470295 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinuxNetworkInterface_t3864470295 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.MacOsNetworkInterface[]
struct MacOsNetworkInterfaceU5BU5D_t1872702543  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MacOsNetworkInterface_t1454185290 * m_Items[1];

public:
	inline MacOsNetworkInterface_t1454185290 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MacOsNetworkInterface_t1454185290 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MacOsNetworkInterface_t1454185290 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES[]
struct Win32_IP_ADAPTER_ADDRESSESU5BU5D_t2519466521  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_IP_ADAPTER_ADDRESSES_t680756680 * m_Items[1];

public:
	inline Win32_IP_ADAPTER_ADDRESSES_t680756680 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_IP_ADAPTER_ADDRESSES_t680756680 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_IP_ADAPTER_ADDRESSES_t680756680 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32_IP_ADAPTER_INFO[]
struct Win32_IP_ADAPTER_INFOU5BU5D_t1944624365  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_IP_ADAPTER_INFO_t2310876292 * m_Items[1];

public:
	inline Win32_IP_ADAPTER_INFO_t2310876292 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_IP_ADAPTER_INFO_t2310876292 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_IP_ADAPTER_INFO_t2310876292 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.UnicastIPAddressInformation[]
struct UnicastIPAddressInformationU5BU5D_t2212961949  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UnicastIPAddressInformation_t2919786644 * m_Items[1];

public:
	inline UnicastIPAddressInformation_t2919786644 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UnicastIPAddressInformation_t2919786644 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UnicastIPAddressInformation_t2919786644 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t830390908  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509ChainStatus_t4278378721  m_Items[1];

public:
	inline X509ChainStatus_t4278378721  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509ChainStatus_t4278378721 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509ChainStatus_t4278378721  value)
	{
		m_Items[index] = value;
	}
};
// System.Net.Sockets.Socket[]
struct SocketU5BU5D_t2867560000  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Socket_t3821512045 * m_Items[1];

public:
	inline Socket_t3821512045 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Socket_t3821512045 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Socket_t3821512045 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Sockets.Socket/WSABUF[]
struct WSABUFU5BU5D_t2696674611  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WSABUF_t2199312694  m_Items[1];

public:
	inline WSABUF_t2199312694  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WSABUF_t2199312694 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WSABUF_t2199312694  value)
	{
		m_Items[index] = value;
	}
};
// System.Net.Cookie[]
struct CookieU5BU5D_t3617850841  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cookie_t3154017544 * m_Items[1];

public:
	inline Cookie_t3154017544 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cookie_t3154017544 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cookie_t3154017544 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509Certificate2[]
struct X509Certificate2U5BU5D_t944324070  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Certificate2_t4056456767 * m_Items[1];

public:
	inline X509Certificate2_t4056456767 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Certificate2_t4056456767 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Certificate2_t4056456767 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509ChainElement[]
struct X509ChainElementU5BU5D_t77376670  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509ChainElement_t528874471 * m_Items[1];

public:
	inline X509ChainElement_t528874471 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509ChainElement_t528874471 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509ChainElement_t528874471 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509Extension[]
struct X509ExtensionU5BU5D_t4020980878  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Extension_t1320896183 * m_Items[1];

public:
	inline X509Extension_t1320896183 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Extension_t1320896183 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Extension_t1320896183 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.AsnEncodedData[]
struct AsnEncodedDataU5BU5D_t727122821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AsnEncodedData_t463456204 * m_Items[1];

public:
	inline AsnEncodedData_t463456204 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AsnEncodedData_t463456204 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AsnEncodedData_t463456204 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.Oid[]
struct OidU5BU5D_t1809518033  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Oid_t3221867120 * m_Items[1];

public:
	inline Oid_t3221867120 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Oid_t3221867120 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Oid_t3221867120 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t3470489975  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Capture_t4157900610 * m_Items[1];

public:
	inline Capture_t4157900610 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Capture_t4157900610 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Capture_t4157900610 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t2272681992  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Group_t3761430853 * m_Items[1];

public:
	inline Group_t3761430853 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Group_t3761430853 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Group_t3761430853 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t3801731412  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Mark_t2724874473  m_Items[1];

public:
	inline Mark_t2724874473  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Mark_t2724874473 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Mark_t2724874473  value)
	{
		m_Items[index] = value;
	}
};
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t2962278982  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UriScheme_t1876590943  m_Items[1];

public:
	inline UriScheme_t1876590943  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UriScheme_t1876590943 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UriScheme_t1876590943  value)
	{
		m_Items[index] = value;
	}
};
// System.Security.Cryptography.X509Certificates.X509CertificateCollection[]
struct X509CertificateCollectionU5BU5D_t126139632  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509CertificateCollection_t1197680765 * m_Items[1];

public:
	inline X509CertificateCollection_t1197680765 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509CertificateCollection_t1197680765 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509CertificateCollection_t1197680765 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t3677892936  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Regex_t1803876613 * m_Items[1];

public:
	inline Regex_t1803876613 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Regex_t1803876613 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Regex_t1803876613 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>[]
struct LinkedList_1U5BU5D_t3036021645  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedList_1_t1581322852 * m_Items[1];

public:
	inline LinkedList_1_t1581322852 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedList_1_t1581322852 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedList_1_t1581322852 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
