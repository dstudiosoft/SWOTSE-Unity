﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FutureVoid/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t296574561;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Firebase.FutureVoid/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m3673019717 (MonoPInvokeCallbackAttribute_t296574561 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
