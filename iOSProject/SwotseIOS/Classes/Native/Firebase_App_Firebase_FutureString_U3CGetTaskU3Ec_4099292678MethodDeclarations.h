﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FutureString/<GetTask>c__AnonStorey0
struct U3CGetTaskU3Ec__AnonStorey0_t4099292678;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0__ctor_m3043421459 (U3CGetTaskU3Ec__AnonStorey0_t4099292678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::<>m__0()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652 (U3CGetTaskU3Ec__AnonStorey0_t4099292678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
