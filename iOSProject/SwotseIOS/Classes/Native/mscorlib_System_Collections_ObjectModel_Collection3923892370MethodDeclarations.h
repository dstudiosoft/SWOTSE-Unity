﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>
struct Collection_1_t3923892370;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1857671443;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit>
struct IList_1_t628120921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::.ctor()
extern "C"  void Collection_1__ctor_m3156716588_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3156716588(__this, method) ((  void (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1__ctor_m3156716588_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m400488489_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m400488489(__this, method) ((  bool (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m400488489_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m573641732_gshared (Collection_1_t3923892370 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m573641732(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3923892370 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m573641732_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373780857_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373780857(__this, method) ((  Il2CppObject * (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373780857_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m252035200_gshared (Collection_1_t3923892370 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m252035200(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3923892370 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m252035200_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3236787146_gshared (Collection_1_t3923892370 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3236787146(__this, ___value0, method) ((  bool (*) (Collection_1_t3923892370 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3236787146_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m817453034_gshared (Collection_1_t3923892370 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m817453034(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3923892370 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m817453034_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m246932757_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m246932757(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m246932757_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1694127621_gshared (Collection_1_t3923892370 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1694127621(__this, ___value0, method) ((  void (*) (Collection_1_t3923892370 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1694127621_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3158273924_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3158273924(__this, method) ((  bool (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3158273924_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3914638122_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3914638122(__this, method) ((  Il2CppObject * (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3914638122_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2854382233_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2854382233(__this, method) ((  bool (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2854382233_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4214003896_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4214003896(__this, method) ((  bool (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4214003896_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3617341629_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3617341629(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3923892370 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3617341629_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1711052550_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1711052550(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1711052550_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Add(T)
extern "C"  void Collection_1_Add_m3603814433_gshared (Collection_1_t3923892370 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3603814433(__this, ___item0, method) ((  void (*) (Collection_1_t3923892370 *, RaycastHit_t87180320 , const MethodInfo*))Collection_1_Add_m3603814433_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Clear()
extern "C"  void Collection_1_Clear_m1300937037_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1300937037(__this, method) ((  void (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_Clear_m1300937037_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3651661299_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3651661299(__this, method) ((  void (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_ClearItems_m3651661299_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool Collection_1_Contains_m3113770167_gshared (Collection_1_t3923892370 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3113770167(__this, ___item0, method) ((  bool (*) (Collection_1_t3923892370 *, RaycastHit_t87180320 , const MethodInfo*))Collection_1_Contains_m3113770167_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4257537385_gshared (Collection_1_t3923892370 * __this, RaycastHitU5BU5D_t1214023521* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4257537385(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3923892370 *, RaycastHitU5BU5D_t1214023521*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4257537385_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4632236_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4632236(__this, method) ((  Il2CppObject* (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_GetEnumerator_m4632236_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3447360977_gshared (Collection_1_t3923892370 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3447360977(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3923892370 *, RaycastHit_t87180320 , const MethodInfo*))Collection_1_IndexOf_m3447360977_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m825180158_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, RaycastHit_t87180320  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m825180158(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))Collection_1_Insert_m825180158_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2849033111_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, RaycastHit_t87180320  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2849033111(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))Collection_1_InsertItem_m2849033111_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Remove(T)
extern "C"  bool Collection_1_Remove_m193344664_gshared (Collection_1_t3923892370 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m193344664(__this, ___item0, method) ((  bool (*) (Collection_1_t3923892370 *, RaycastHit_t87180320 , const MethodInfo*))Collection_1_Remove_m193344664_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2470737722_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2470737722(__this, ___index0, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2470737722_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3708648984_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3708648984(__this, ___index0, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3708648984_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3906976000_gshared (Collection_1_t3923892370 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3906976000(__this, method) ((  int32_t (*) (Collection_1_t3923892370 *, const MethodInfo*))Collection_1_get_Count_m3906976000_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t87180320  Collection_1_get_Item_m3577482486_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3577482486(__this, ___index0, method) ((  RaycastHit_t87180320  (*) (Collection_1_t3923892370 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3577482486_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3365801217_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, RaycastHit_t87180320  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3365801217(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))Collection_1_set_Item_m3365801217_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2629996854_gshared (Collection_1_t3923892370 * __this, int32_t ___index0, RaycastHit_t87180320  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2629996854(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3923892370 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))Collection_1_SetItem_m2629996854_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3372027035_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3372027035(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3372027035_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::ConvertItem(System.Object)
extern "C"  RaycastHit_t87180320  Collection_1_ConvertItem_m3912242079_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3912242079(__this /* static, unused */, ___item0, method) ((  RaycastHit_t87180320  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3912242079_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1371434215_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1371434215(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1371434215_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m633437935_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m633437935(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m633437935_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3998794010_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3998794010(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3998794010_gshared)(__this /* static, unused */, ___list0, method)
