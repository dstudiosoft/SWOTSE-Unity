﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportUnitsLine
struct  BattleReportUnitsLine_t392298293  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text BattleReportUnitsLine::unitName
	Text_t356221433 * ___unitName_2;
	// UnityEngine.UI.Text BattleReportUnitsLine::beforeCount
	Text_t356221433 * ___beforeCount_3;
	// UnityEngine.UI.Text BattleReportUnitsLine::lostCount
	Text_t356221433 * ___lostCount_4;
	// UnityEngine.UI.Text BattleReportUnitsLine::remainingCount
	Text_t356221433 * ___remainingCount_5;

public:
	inline static int32_t get_offset_of_unitName_2() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t392298293, ___unitName_2)); }
	inline Text_t356221433 * get_unitName_2() const { return ___unitName_2; }
	inline Text_t356221433 ** get_address_of_unitName_2() { return &___unitName_2; }
	inline void set_unitName_2(Text_t356221433 * value)
	{
		___unitName_2 = value;
		Il2CppCodeGenWriteBarrier(&___unitName_2, value);
	}

	inline static int32_t get_offset_of_beforeCount_3() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t392298293, ___beforeCount_3)); }
	inline Text_t356221433 * get_beforeCount_3() const { return ___beforeCount_3; }
	inline Text_t356221433 ** get_address_of_beforeCount_3() { return &___beforeCount_3; }
	inline void set_beforeCount_3(Text_t356221433 * value)
	{
		___beforeCount_3 = value;
		Il2CppCodeGenWriteBarrier(&___beforeCount_3, value);
	}

	inline static int32_t get_offset_of_lostCount_4() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t392298293, ___lostCount_4)); }
	inline Text_t356221433 * get_lostCount_4() const { return ___lostCount_4; }
	inline Text_t356221433 ** get_address_of_lostCount_4() { return &___lostCount_4; }
	inline void set_lostCount_4(Text_t356221433 * value)
	{
		___lostCount_4 = value;
		Il2CppCodeGenWriteBarrier(&___lostCount_4, value);
	}

	inline static int32_t get_offset_of_remainingCount_5() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t392298293, ___remainingCount_5)); }
	inline Text_t356221433 * get_remainingCount_5() const { return ___remainingCount_5; }
	inline Text_t356221433 ** get_address_of_remainingCount_5() { return &___remainingCount_5; }
	inline void set_remainingCount_5(Text_t356221433 * value)
	{
		___remainingCount_5 = value;
		Il2CppCodeGenWriteBarrier(&___remainingCount_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
