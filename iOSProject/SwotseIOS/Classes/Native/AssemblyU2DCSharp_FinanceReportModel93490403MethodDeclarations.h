﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinanceReportModel
struct FinanceReportModel_t93490403;

#include "codegen/il2cpp-codegen.h"

// System.Void FinanceReportModel::.ctor()
extern "C"  void FinanceReportModel__ctor_m956668988 (FinanceReportModel_t93490403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
