﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Transform_1_t2115438338;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1066330490_gshared (Transform_1_t2115438338 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1066330490(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2115438338 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1066330490_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,TouchScript.Gestures.UI.UIGesture/TouchData>::Invoke(TKey,TValue)
extern "C"  TouchData_t3880599975  Transform_1_Invoke_m3158600674_gshared (Transform_1_t2115438338 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3158600674(__this, ___key0, ___value1, method) ((  TouchData_t3880599975  (*) (Transform_1_t2115438338 *, int32_t, TouchData_t3880599975 , const MethodInfo*))Transform_1_Invoke_m3158600674_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,TouchScript.Gestures.UI.UIGesture/TouchData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m61535403_gshared (Transform_1_t2115438338 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m61535403(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2115438338 *, int32_t, TouchData_t3880599975 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m61535403_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,TouchScript.Gestures.UI.UIGesture/TouchData>::EndInvoke(System.IAsyncResult)
extern "C"  TouchData_t3880599975  Transform_1_EndInvoke_m707295952_gshared (Transform_1_t2115438338 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m707295952(__this, ___result0, method) ((  TouchData_t3880599975  (*) (Transform_1_t2115438338 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m707295952_gshared)(__this, ___result0, method)
