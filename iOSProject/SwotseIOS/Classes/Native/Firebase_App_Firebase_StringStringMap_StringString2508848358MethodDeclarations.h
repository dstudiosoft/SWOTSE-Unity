﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.StringStringMap/StringStringMapEnumerator
struct StringStringMapEnumerator_t2508848358;
// Firebase.StringStringMap
struct StringStringMap_t3841415930;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_StringStringMap3841415930.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"

// System.Void Firebase.StringStringMap/StringStringMapEnumerator::.ctor(Firebase.StringStringMap)
extern "C"  void StringStringMapEnumerator__ctor_m3726221322 (StringStringMapEnumerator_t2508848358 * __this, StringStringMap_t3841415930 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Firebase.StringStringMap/StringStringMapEnumerator::get_Current()
extern "C"  KeyValuePair_2_t1701344717  StringStringMapEnumerator_get_Current_m1383291675 (StringStringMapEnumerator_t2508848358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.StringStringMap/StringStringMapEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * StringStringMapEnumerator_System_Collections_IEnumerator_get_Current_m2843098879 (StringStringMapEnumerator_t2508848358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap/StringStringMapEnumerator::MoveNext()
extern "C"  bool StringStringMapEnumerator_MoveNext_m3586904407 (StringStringMapEnumerator_t2508848358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap/StringStringMapEnumerator::Reset()
extern "C"  void StringStringMapEnumerator_Reset_m236615954 (StringStringMapEnumerator_t2508848358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap/StringStringMapEnumerator::Dispose()
extern "C"  void StringStringMapEnumerator_Dispose_m4089046420 (StringStringMapEnumerator_t2508848358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
