﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1153496711(__this, ___host0, method) ((  void (*) (Enumerator_t1120840722 *, Dictionary_2_t2726304580 *, const MethodInfo*))Enumerator__ctor_m3400877137_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2550523878(__this, method) ((  Il2CppObject * (*) (Enumerator_t1120840722 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m247337542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4127698000(__this, method) ((  void (*) (Enumerator_t1120840722 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4166432760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::Dispose()
#define Enumerator_Dispose_m1318691467(__this, method) ((  void (*) (Enumerator_t1120840722 *, const MethodInfo*))Enumerator_Dispose_m3239242417_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::MoveNext()
#define Enumerator_MoveNext_m676215509(__this, method) ((  bool (*) (Enumerator_t1120840722 *, const MethodInfo*))Enumerator_MoveNext_m1062748132_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,MyCityModel>::get_Current()
#define Enumerator_get_Current_m2894874673(__this, method) ((  int64_t (*) (Enumerator_t1120840722 *, const MethodInfo*))Enumerator_get_Current_m3153023504_gshared)(__this, method)
