﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerTimeUpdater
struct ServerTimeUpdater_t2312549125;

#include "codegen/il2cpp-codegen.h"

// System.Void ServerTimeUpdater::.ctor()
extern "C"  void ServerTimeUpdater__ctor_m453380762 (ServerTimeUpdater_t2312549125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerTimeUpdater::Start()
extern "C"  void ServerTimeUpdater_Start_m1191085258 (ServerTimeUpdater_t2312549125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerTimeUpdater::OnEnable()
extern "C"  void ServerTimeUpdater_OnEnable_m444988394 (ServerTimeUpdater_t2312549125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerTimeUpdater::FixedUpdate()
extern "C"  void ServerTimeUpdater_FixedUpdate_m3209784595 (ServerTimeUpdater_t2312549125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
