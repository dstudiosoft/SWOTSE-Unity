﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyTimer
struct  ArmyTimer_t470110518  : public Il2CppObject
{
public:
	// System.Int64 ArmyTimer::id
	int64_t ___id_0;
	// System.Int64 ArmyTimer::army
	int64_t ___army_1;
	// System.Int64 ArmyTimer::event_id
	int64_t ___event_id_2;
	// System.DateTime ArmyTimer::start_time
	DateTime_t693205669  ___start_time_3;
	// System.DateTime ArmyTimer::finish_time
	DateTime_t693205669  ___finish_time_4;
	// System.String ArmyTimer::army_status
	String_t* ___army_status_5;
	// System.Boolean ArmyTimer::enemy
	bool ___enemy_6;
	// System.String ArmyTimer::targetType
	String_t* ___targetType_7;
	// System.String ArmyTimer::old_army_status
	String_t* ___old_army_status_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_army_1() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___army_1)); }
	inline int64_t get_army_1() const { return ___army_1; }
	inline int64_t* get_address_of_army_1() { return &___army_1; }
	inline void set_army_1(int64_t value)
	{
		___army_1 = value;
	}

	inline static int32_t get_offset_of_event_id_2() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___event_id_2)); }
	inline int64_t get_event_id_2() const { return ___event_id_2; }
	inline int64_t* get_address_of_event_id_2() { return &___event_id_2; }
	inline void set_event_id_2(int64_t value)
	{
		___event_id_2 = value;
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___start_time_3)); }
	inline DateTime_t693205669  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t693205669 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t693205669  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___finish_time_4)); }
	inline DateTime_t693205669  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t693205669 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t693205669  value)
	{
		___finish_time_4 = value;
	}

	inline static int32_t get_offset_of_army_status_5() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___army_status_5)); }
	inline String_t* get_army_status_5() const { return ___army_status_5; }
	inline String_t** get_address_of_army_status_5() { return &___army_status_5; }
	inline void set_army_status_5(String_t* value)
	{
		___army_status_5 = value;
		Il2CppCodeGenWriteBarrier(&___army_status_5, value);
	}

	inline static int32_t get_offset_of_enemy_6() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___enemy_6)); }
	inline bool get_enemy_6() const { return ___enemy_6; }
	inline bool* get_address_of_enemy_6() { return &___enemy_6; }
	inline void set_enemy_6(bool value)
	{
		___enemy_6 = value;
	}

	inline static int32_t get_offset_of_targetType_7() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___targetType_7)); }
	inline String_t* get_targetType_7() const { return ___targetType_7; }
	inline String_t** get_address_of_targetType_7() { return &___targetType_7; }
	inline void set_targetType_7(String_t* value)
	{
		___targetType_7 = value;
		Il2CppCodeGenWriteBarrier(&___targetType_7, value);
	}

	inline static int32_t get_offset_of_old_army_status_8() { return static_cast<int32_t>(offsetof(ArmyTimer_t470110518, ___old_army_status_8)); }
	inline String_t* get_old_army_status_8() const { return ___old_army_status_8; }
	inline String_t** get_address_of_old_army_status_8() { return &___old_army_status_8; }
	inline void set_old_army_status_8(String_t* value)
	{
		___old_army_status_8 = value;
		Il2CppCodeGenWriteBarrier(&___old_army_status_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
