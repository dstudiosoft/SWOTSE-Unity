﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceInviteTableController
struct AllianceInviteTableController_t989323868;
// SimpleEvent
struct SimpleEvent_t1509017202;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UserModel
struct UserModel_t3025569216;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_UserModel3025569216.h"

// System.Void AllianceInviteTableController::.ctor()
extern "C"  void AllianceInviteTableController__ctor_m2463910957 (AllianceInviteTableController_t989323868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteTableController::add_onGetAllUsers(SimpleEvent)
extern "C"  void AllianceInviteTableController_add_onGetAllUsers_m1792106805 (AllianceInviteTableController_t989323868 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteTableController::remove_onGetAllUsers(SimpleEvent)
extern "C"  void AllianceInviteTableController_remove_onGetAllUsers_m2413603294 (AllianceInviteTableController_t989323868 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteTableController::Start()
extern "C"  void AllianceInviteTableController_Start_m600239949 (AllianceInviteTableController_t989323868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllianceInviteTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t AllianceInviteTableController_GetNumberOfRowsForTableView_m2018626497 (AllianceInviteTableController_t989323868 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AllianceInviteTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float AllianceInviteTableController_GetHeightForRowInTableView_m3221720373 (AllianceInviteTableController_t989323868 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell AllianceInviteTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * AllianceInviteTableController_GetCellForRowInTableView_m1247788938 (AllianceInviteTableController_t989323868 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceInviteTableController::GetAllUsers()
extern "C"  Il2CppObject * AllianceInviteTableController_GetAllUsers_m263082078 (AllianceInviteTableController_t989323868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteTableController::GotAllUsers(System.Object,System.String)
extern "C"  void AllianceInviteTableController_GotAllUsers_m3614933992 (AllianceInviteTableController_t989323868 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteTableController::RemoveUser(UserModel)
extern "C"  void AllianceInviteTableController_RemoveUser_m218701422 (AllianceInviteTableController_t989323868 * __this, UserModel_t3025569216 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
