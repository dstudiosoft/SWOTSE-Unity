﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.MonoHttpDate
struct MonoHttpDate_t3791217207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.MonoHttpDate::.ctor()
extern "C"  void MonoHttpDate__ctor_m988147115 (MonoHttpDate_t3791217207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.MonoHttpDate::.cctor()
extern "C"  void MonoHttpDate__cctor_m1963194158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Net.MonoHttpDate::Parse(System.String)
extern "C"  DateTime_t693205669  MonoHttpDate_Parse_m3650750357 (Il2CppObject * __this /* static, unused */, String_t* ___dateStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
