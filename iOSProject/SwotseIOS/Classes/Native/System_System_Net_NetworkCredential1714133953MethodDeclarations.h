﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkCredential
struct NetworkCredential_t1714133953;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"

// System.Void System.Net.NetworkCredential::.ctor()
extern "C"  void NetworkCredential__ctor_m584275461 (NetworkCredential_t1714133953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkCredential::.ctor(System.String,System.String)
extern "C"  void NetworkCredential__ctor_m1823481869 (NetworkCredential_t1714133953 * __this, String_t* ___userName0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkCredential::.ctor(System.String,System.String,System.String)
extern "C"  void NetworkCredential__ctor_m1160748635 (NetworkCredential_t1714133953 * __this, String_t* ___userName0, String_t* ___password1, String_t* ___domain2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkCredential::get_Domain()
extern "C"  String_t* NetworkCredential_get_Domain_m2892408761 (NetworkCredential_t1714133953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkCredential::set_Domain(System.String)
extern "C"  void NetworkCredential_set_Domain_m758477458 (NetworkCredential_t1714133953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkCredential::get_UserName()
extern "C"  String_t* NetworkCredential_get_UserName_m1552968607 (NetworkCredential_t1714133953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkCredential::set_UserName(System.String)
extern "C"  void NetworkCredential_set_UserName_m2413904086 (NetworkCredential_t1714133953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkCredential::get_Password()
extern "C"  String_t* NetworkCredential_get_Password_m2037205148 (NetworkCredential_t1714133953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkCredential::set_Password(System.String)
extern "C"  void NetworkCredential_set_Password_m1723042521 (NetworkCredential_t1714133953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.NetworkCredential::GetCredential(System.Uri,System.String)
extern "C"  NetworkCredential_t1714133953 * NetworkCredential_GetCredential_m1819519067 (NetworkCredential_t1714133953 * __this, Uri_t19570940 * ___uri0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
