﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.AsnEncodedDataEnumerator
struct AsnEncodedDataEnumerator_t884119648;
// System.Security.Cryptography.AsnEncodedDataCollection
struct AsnEncodedDataCollection_t912862782;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t463456204;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Security_Cryptography_AsnEncodedDataC912862782.h"

// System.Void System.Security.Cryptography.AsnEncodedDataEnumerator::.ctor(System.Security.Cryptography.AsnEncodedDataCollection)
extern "C"  void AsnEncodedDataEnumerator__ctor_m2912883526 (AsnEncodedDataEnumerator_t884119648 * __this, AsnEncodedDataCollection_t912862782 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.AsnEncodedDataEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * AsnEncodedDataEnumerator_System_Collections_IEnumerator_get_Current_m150851881 (AsnEncodedDataEnumerator_t884119648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.AsnEncodedDataEnumerator::get_Current()
extern "C"  AsnEncodedData_t463456204 * AsnEncodedDataEnumerator_get_Current_m728382797 (AsnEncodedDataEnumerator_t884119648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.AsnEncodedDataEnumerator::MoveNext()
extern "C"  bool AsnEncodedDataEnumerator_MoveNext_m3242013341 (AsnEncodedDataEnumerator_t884119648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsnEncodedDataEnumerator::Reset()
extern "C"  void AsnEncodedDataEnumerator_Reset_m1097910242 (AsnEncodedDataEnumerator_t884119648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
