﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke134444076.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2001676353_gshared (Enumerator_t134444076 * __this, Dictionary_2_t1739907934 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2001676353(__this, ___host0, method) ((  void (*) (Enumerator_t134444076 *, Dictionary_2_t1739907934 *, const MethodInfo*))Enumerator__ctor_m2001676353_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2780305934_gshared (Enumerator_t134444076 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2780305934(__this, method) ((  Il2CppObject * (*) (Enumerator_t134444076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2780305934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m59809502_gshared (Enumerator_t134444076 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m59809502(__this, method) ((  void (*) (Enumerator_t134444076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m59809502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Dispose()
extern "C"  void Enumerator_Dispose_m1406642057_gshared (Enumerator_t134444076 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1406642057(__this, method) ((  void (*) (Enumerator_t134444076 *, const MethodInfo*))Enumerator_Dispose_m1406642057_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3763568090_gshared (Enumerator_t134444076 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3763568090(__this, method) ((  bool (*) (Enumerator_t134444076 *, const MethodInfo*))Enumerator_MoveNext_m3763568090_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3006254278_gshared (Enumerator_t134444076 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3006254278(__this, method) ((  int32_t (*) (Enumerator_t134444076 *, const MethodInfo*))Enumerator_get_Current_m3006254278_gshared)(__this, method)
