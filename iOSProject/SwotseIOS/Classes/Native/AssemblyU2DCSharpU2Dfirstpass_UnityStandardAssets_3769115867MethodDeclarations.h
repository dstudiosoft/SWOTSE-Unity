﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6
struct U3CStartU3Ec__Iterator6_t3769115867;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::.ctor()
extern "C"  void U3CStartU3Ec__Iterator6__ctor_m2699450198 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1993401060 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1753364988 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator6_MoveNext_m9118714 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::Dispose()
extern "C"  void U3CStartU3Ec__Iterator6_Dispose_m3683541163 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator6::Reset()
extern "C"  void U3CStartU3Ec__Iterator6_Reset_m3133137417 (U3CStartU3Ec__Iterator6_t3769115867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
