﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldMapTimer
struct WorldMapTimer_t3127003883;

#include "codegen/il2cpp-codegen.h"

// System.Void WorldMapTimer::.ctor()
extern "C"  void WorldMapTimer__ctor_m1193997990 (WorldMapTimer_t3127003883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
