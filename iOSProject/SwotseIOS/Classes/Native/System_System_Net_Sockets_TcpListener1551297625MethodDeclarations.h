﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.TcpListener
struct TcpListener_t1551297625;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Net.Sockets.TcpClient
struct TcpClient_t408947970;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.Sockets.TcpListener::.ctor(System.Int32)
extern "C"  void TcpListener__ctor_m2530284148 (TcpListener_t1551297625 * __this, int32_t ___port0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::.ctor(System.Net.IPEndPoint)
extern "C"  void TcpListener__ctor_m3668260645 (TcpListener_t1551297625 * __this, IPEndPoint_t2615413766 * ___local_end_point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::.ctor(System.Net.IPAddress,System.Int32)
extern "C"  void TcpListener__ctor_m104423921 (TcpListener_t1551297625 * __this, IPAddress_t1399971723 * ___listen_ip0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::Init(System.Net.Sockets.AddressFamily,System.Net.EndPoint)
extern "C"  void TcpListener_Init_m1169079544 (TcpListener_t1551297625 * __this, int32_t ___family0, EndPoint_t4156119363 * ___ep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.TcpListener::get_Active()
extern "C"  bool TcpListener_get_Active_m2776212802 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.EndPoint System.Net.Sockets.TcpListener::get_LocalEndpoint()
extern "C"  EndPoint_t4156119363 * TcpListener_get_LocalEndpoint_m2950030068 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket System.Net.Sockets.TcpListener::get_Server()
extern "C"  Socket_t3821512045 * TcpListener_get_Server_m2098356889 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.TcpListener::get_ExclusiveAddressUse()
extern "C"  bool TcpListener_get_ExclusiveAddressUse_m2358306727 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::set_ExclusiveAddressUse(System.Boolean)
extern "C"  void TcpListener_set_ExclusiveAddressUse_m745430140 (TcpListener_t1551297625 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket System.Net.Sockets.TcpListener::AcceptSocket()
extern "C"  Socket_t3821512045 * TcpListener_AcceptSocket_m2641857024 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.TcpClient System.Net.Sockets.TcpListener::AcceptTcpClient()
extern "C"  TcpClient_t408947970 * TcpListener_AcceptTcpClient_m3428842100 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::Finalize()
extern "C"  void TcpListener_Finalize_m998971995 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.TcpListener::Pending()
extern "C"  bool TcpListener_Pending_m2411742554 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::Start()
extern "C"  void TcpListener_Start_m2176032785 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::Start(System.Int32)
extern "C"  void TcpListener_Start_m4269559932 (TcpListener_t1551297625 * __this, int32_t ___backlog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.TcpListener::BeginAcceptSocket(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TcpListener_BeginAcceptSocket_m2709024863 (TcpListener_t1551297625 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.TcpListener::BeginAcceptTcpClient(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TcpListener_BeginAcceptTcpClient_m597883738 (TcpListener_t1551297625 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket System.Net.Sockets.TcpListener::EndAcceptSocket(System.IAsyncResult)
extern "C"  Socket_t3821512045 * TcpListener_EndAcceptSocket_m3696911042 (TcpListener_t1551297625 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.TcpClient System.Net.Sockets.TcpListener::EndAcceptTcpClient(System.IAsyncResult)
extern "C"  TcpClient_t408947970 * TcpListener_EndAcceptTcpClient_m1806483946 (TcpListener_t1551297625 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.TcpListener::Stop()
extern "C"  void TcpListener_Stop_m3738266225 (TcpListener_t1551297625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
