﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceContentChanger
struct AllianceContentChanger_t4213706348;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void AllianceContentChanger::.ctor()
extern "C"  void AllianceContentChanger__ctor_m714581407 (AllianceContentChanger_t4213706348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::add_confirmed(ConfirmationEvent)
extern "C"  void AllianceContentChanger_add_confirmed_m3739969737 (AllianceContentChanger_t4213706348 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::remove_confirmed(ConfirmationEvent)
extern "C"  void AllianceContentChanger_remove_confirmed_m2012457628 (AllianceContentChanger_t4213706348 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::OpenContent(System.String)
extern "C"  void AllianceContentChanger_OpenContent_m231026588 (AllianceContentChanger_t4213706348 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::CloseAllContent()
extern "C"  void AllianceContentChanger_CloseAllContent_m3379059837 (AllianceContentChanger_t4213706348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::ResignRank()
extern "C"  void AllianceContentChanger_ResignRank_m1937844093 (AllianceContentChanger_t4213706348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::ResignConfirmed(System.Boolean)
extern "C"  void AllianceContentChanger_ResignConfirmed_m4209939651 (AllianceContentChanger_t4213706348 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::QuitAlliance()
extern "C"  void AllianceContentChanger_QuitAlliance_m1244301875 (AllianceContentChanger_t4213706348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::QuitConfirmed(System.Boolean)
extern "C"  void AllianceContentChanger_QuitConfirmed_m1900455442 (AllianceContentChanger_t4213706348 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceContentChanger::OnQuitAlliance(System.Object,System.String)
extern "C"  void AllianceContentChanger_OnQuitAlliance_m2984998056 (AllianceContentChanger_t4213706348 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
