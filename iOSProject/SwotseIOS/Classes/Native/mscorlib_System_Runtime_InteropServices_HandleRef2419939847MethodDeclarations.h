﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C"  void HandleRef__ctor_m1370162289 (HandleRef_t2419939847 * __this, Il2CppObject * ___wrapper0, IntPtr_t ___handle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C"  IntPtr_t HandleRef_get_Handle_m769981143 (HandleRef_t2419939847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
