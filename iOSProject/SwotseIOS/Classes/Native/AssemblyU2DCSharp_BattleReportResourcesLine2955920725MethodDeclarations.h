﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportResourcesLine
struct BattleReportResourcesLine_t2955920725;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BattleReportResourcesLine::.ctor()
extern "C"  void BattleReportResourcesLine__ctor_m2179358922 (BattleReportResourcesLine_t2955920725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetGold(System.String)
extern "C"  void BattleReportResourcesLine_SetGold_m3578855714 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetSilver(System.String)
extern "C"  void BattleReportResourcesLine_SetSilver_m916984839 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetFood(System.String)
extern "C"  void BattleReportResourcesLine_SetFood_m1690040580 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetWood(System.String)
extern "C"  void BattleReportResourcesLine_SetWood_m2745840307 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetIron(System.String)
extern "C"  void BattleReportResourcesLine_SetIron_m3654512062 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetCopper(System.String)
extern "C"  void BattleReportResourcesLine_SetCopper_m1143255235 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportResourcesLine::SetStone(System.String)
extern "C"  void BattleReportResourcesLine_SetStone_m162120219 (BattleReportResourcesLine_t2955920725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
