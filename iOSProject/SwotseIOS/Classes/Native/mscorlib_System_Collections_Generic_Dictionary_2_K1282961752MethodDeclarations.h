﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1282961752.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1056007245_gshared (Enumerator_t1282961752 * __this, Dictionary_2_t2888425610 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1056007245(__this, ___host0, method) ((  void (*) (Enumerator_t1282961752 *, Dictionary_2_t2888425610 *, const MethodInfo*))Enumerator__ctor_m1056007245_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3194937162_gshared (Enumerator_t1282961752 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3194937162(__this, method) ((  Il2CppObject * (*) (Enumerator_t1282961752 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3194937162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2638202932_gshared (Enumerator_t1282961752 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2638202932(__this, method) ((  void (*) (Enumerator_t1282961752 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2638202932_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Dispose()
extern "C"  void Enumerator_Dispose_m1523319765_gshared (Enumerator_t1282961752 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1523319765(__this, method) ((  void (*) (Enumerator_t1282961752 *, const MethodInfo*))Enumerator_Dispose_m1523319765_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090404168_gshared (Enumerator_t1282961752 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3090404168(__this, method) ((  bool (*) (Enumerator_t1282961752 *, const MethodInfo*))Enumerator_MoveNext_m3090404168_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m856435704_gshared (Enumerator_t1282961752 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m856435704(__this, method) ((  int32_t (*) (Enumerator_t1282961752 *, const MethodInfo*))Enumerator_get_Current_m856435704_gshared)(__this, method)
