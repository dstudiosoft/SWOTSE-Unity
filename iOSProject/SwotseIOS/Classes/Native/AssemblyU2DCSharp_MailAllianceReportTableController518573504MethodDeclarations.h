﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailAllianceReportTableController
struct MailAllianceReportTableController_t518573504;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailAllianceReportTableController::.ctor()
extern "C"  void MailAllianceReportTableController__ctor_m3860170419 (MailAllianceReportTableController_t518573504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::Start()
extern "C"  void MailAllianceReportTableController_Start_m4272026375 (MailAllianceReportTableController_t518573504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::OnEnable()
extern "C"  void MailAllianceReportTableController_OnEnable_m3054443335 (MailAllianceReportTableController_t518573504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MailAllianceReportTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t MailAllianceReportTableController_GetNumberOfRowsForTableView_m688622551 (MailAllianceReportTableController_t518573504 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MailAllianceReportTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float MailAllianceReportTableController_GetHeightForRowInTableView_m782075439 (MailAllianceReportTableController_t518573504 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell MailAllianceReportTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * MailAllianceReportTableController_GetCellForRowInTableView_m3357367518 (MailAllianceReportTableController_t518573504 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::DeleteReport(System.Int64)
extern "C"  void MailAllianceReportTableController_DeleteReport_m2958848562 (MailAllianceReportTableController_t518573504 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::ReportDeleted(System.Object,System.String)
extern "C"  void MailAllianceReportTableController_ReportDeleted_m4050261340 (MailAllianceReportTableController_t518573504 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::ReloadTable()
extern "C"  void MailAllianceReportTableController_ReloadTable_m1032112756 (MailAllianceReportTableController_t518573504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableController::GotAllReports(System.Object,System.String)
extern "C"  void MailAllianceReportTableController_GotAllReports_m2765329235 (MailAllianceReportTableController_t518573504 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
