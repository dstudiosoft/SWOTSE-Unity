﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceContentChanger
struct  ResidenceContentChanger_t3315848035  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ResidenceContentChanger::contentLabel
	Text_t356221433 * ___contentLabel_2;
	// UnityEngine.UI.Text ResidenceContentChanger::cityName
	Text_t356221433 * ___cityName_3;
	// UnityEngine.UI.InputField ResidenceContentChanger::cityNameInput
	InputField_t1631627530 * ___cityNameInput_4;
	// UnityEngine.UI.Image ResidenceContentChanger::cityIcon
	Image_t2042527209 * ___cityIcon_5;
	// UnityEngine.GameObject ResidenceContentChanger::buildingsContent
	GameObject_t1756533147 * ___buildingsContent_6;
	// UnityEngine.GameObject ResidenceContentChanger::productionContent
	GameObject_t1756533147 * ___productionContent_7;
	// UnityEngine.GameObject ResidenceContentChanger::citiesContent
	GameObject_t1756533147 * ___citiesContent_8;
	// UnityEngine.GameObject ResidenceContentChanger::valleysContent
	GameObject_t1756533147 * ___valleysContent_9;
	// UnityEngine.GameObject ResidenceContentChanger::coloniesContent
	GameObject_t1756533147 * ___coloniesContent_10;
	// System.Int64 ResidenceContentChanger::iconIndex
	int64_t ___iconIndex_11;

public:
	inline static int32_t get_offset_of_contentLabel_2() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___contentLabel_2)); }
	inline Text_t356221433 * get_contentLabel_2() const { return ___contentLabel_2; }
	inline Text_t356221433 ** get_address_of_contentLabel_2() { return &___contentLabel_2; }
	inline void set_contentLabel_2(Text_t356221433 * value)
	{
		___contentLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentLabel_2, value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___cityName_3)); }
	inline Text_t356221433 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t356221433 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t356221433 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_3, value);
	}

	inline static int32_t get_offset_of_cityNameInput_4() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___cityNameInput_4)); }
	inline InputField_t1631627530 * get_cityNameInput_4() const { return ___cityNameInput_4; }
	inline InputField_t1631627530 ** get_address_of_cityNameInput_4() { return &___cityNameInput_4; }
	inline void set_cityNameInput_4(InputField_t1631627530 * value)
	{
		___cityNameInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityNameInput_4, value);
	}

	inline static int32_t get_offset_of_cityIcon_5() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___cityIcon_5)); }
	inline Image_t2042527209 * get_cityIcon_5() const { return ___cityIcon_5; }
	inline Image_t2042527209 ** get_address_of_cityIcon_5() { return &___cityIcon_5; }
	inline void set_cityIcon_5(Image_t2042527209 * value)
	{
		___cityIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityIcon_5, value);
	}

	inline static int32_t get_offset_of_buildingsContent_6() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___buildingsContent_6)); }
	inline GameObject_t1756533147 * get_buildingsContent_6() const { return ___buildingsContent_6; }
	inline GameObject_t1756533147 ** get_address_of_buildingsContent_6() { return &___buildingsContent_6; }
	inline void set_buildingsContent_6(GameObject_t1756533147 * value)
	{
		___buildingsContent_6 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsContent_6, value);
	}

	inline static int32_t get_offset_of_productionContent_7() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___productionContent_7)); }
	inline GameObject_t1756533147 * get_productionContent_7() const { return ___productionContent_7; }
	inline GameObject_t1756533147 ** get_address_of_productionContent_7() { return &___productionContent_7; }
	inline void set_productionContent_7(GameObject_t1756533147 * value)
	{
		___productionContent_7 = value;
		Il2CppCodeGenWriteBarrier(&___productionContent_7, value);
	}

	inline static int32_t get_offset_of_citiesContent_8() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___citiesContent_8)); }
	inline GameObject_t1756533147 * get_citiesContent_8() const { return ___citiesContent_8; }
	inline GameObject_t1756533147 ** get_address_of_citiesContent_8() { return &___citiesContent_8; }
	inline void set_citiesContent_8(GameObject_t1756533147 * value)
	{
		___citiesContent_8 = value;
		Il2CppCodeGenWriteBarrier(&___citiesContent_8, value);
	}

	inline static int32_t get_offset_of_valleysContent_9() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___valleysContent_9)); }
	inline GameObject_t1756533147 * get_valleysContent_9() const { return ___valleysContent_9; }
	inline GameObject_t1756533147 ** get_address_of_valleysContent_9() { return &___valleysContent_9; }
	inline void set_valleysContent_9(GameObject_t1756533147 * value)
	{
		___valleysContent_9 = value;
		Il2CppCodeGenWriteBarrier(&___valleysContent_9, value);
	}

	inline static int32_t get_offset_of_coloniesContent_10() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___coloniesContent_10)); }
	inline GameObject_t1756533147 * get_coloniesContent_10() const { return ___coloniesContent_10; }
	inline GameObject_t1756533147 ** get_address_of_coloniesContent_10() { return &___coloniesContent_10; }
	inline void set_coloniesContent_10(GameObject_t1756533147 * value)
	{
		___coloniesContent_10 = value;
		Il2CppCodeGenWriteBarrier(&___coloniesContent_10, value);
	}

	inline static int32_t get_offset_of_iconIndex_11() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035, ___iconIndex_11)); }
	inline int64_t get_iconIndex_11() const { return ___iconIndex_11; }
	inline int64_t* get_address_of_iconIndex_11() { return &___iconIndex_11; }
	inline void set_iconIndex_11(int64_t value)
	{
		___iconIndex_11 = value;
	}
};

struct ResidenceContentChanger_t3315848035_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ResidenceContentChanger::<>f__switch$map10
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map10_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_12() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t3315848035_StaticFields, ___U3CU3Ef__switchU24map10_12)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map10_12() const { return ___U3CU3Ef__switchU24map10_12; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map10_12() { return &___U3CU3Ef__switchU24map10_12; }
	inline void set_U3CU3Ef__switchU24map10_12(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map10_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map10_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
