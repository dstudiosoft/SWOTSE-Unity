﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Version
struct Version_t3456873960;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Exception
struct Exception_t;
// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t596328627;
// System.Net.HttpListener
struct HttpListener_t988452056;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.Net.HttpListenerRequest
struct HttpListenerRequest_t630699488;
// System.Net.HttpListenerResponse
struct HttpListenerResponse_t3502667045;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.Net.HttpConnection
struct HttpConnection_t269576101;
// System.Net.ICredentials
struct ICredentials_t725721261;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Net.CookieCollection
struct CookieCollection_t3881042616;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1942268960;
// System.Net.ResponseStream
struct ResponseStream_t3810703494;
// System.Net.ServicePoint
struct ServicePoint_t2786966844;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Random
struct Random_t108471755;
// System.Collections.Queue
struct Queue_t3637523393;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Uri
struct Uri_t100236324;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1669436515;
// System.IO.Stream
struct Stream_t1273022909;
// System.Net.WebConnection
struct WebConnection_t3982808322;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Net.HttpWebResponse
struct HttpWebResponse_t3286585418;
// System.Net.FtpWebResponse
struct FtpWebResponse_t3940763575;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Net.IPEndPoint
struct IPEndPoint_t3791887218;
// System.Net.Sockets.Socket
struct Socket_t1119025450;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t932037087;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Net.IPAddress
struct IPAddress_t241777590;
// System.Void
struct Void_t1185182177;
// System.Net.FtpWebRequest
struct FtpWebRequest_t1577818305;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Net.IWebProxy
struct IWebProxy_t688979836;
// System.Net.DownloadDataCompletedEventHandler
struct DownloadDataCompletedEventHandler_t1254360211;
// System.ComponentModel.AsyncCompletedEventHandler
struct AsyncCompletedEventHandler_t3247698412;
// System.Net.DownloadProgressChangedEventHandler
struct DownloadProgressChangedEventHandler_t3270744339;
// System.Net.DownloadStringCompletedEventHandler
struct DownloadStringCompletedEventHandler_t3405971822;
// System.Net.OpenReadCompletedEventHandler
struct OpenReadCompletedEventHandler_t3320248228;
// System.Net.OpenWriteCompletedEventHandler
struct OpenWriteCompletedEventHandler_t1427320362;
// System.Net.UploadDataCompletedEventHandler
struct UploadDataCompletedEventHandler_t1479659898;
// System.Net.UploadFileCompletedEventHandler
struct UploadFileCompletedEventHandler_t3030596794;
// System.Net.UploadProgressChangedEventHandler
struct UploadProgressChangedEventHandler_t3804846296;
// System.Net.UploadStringCompletedEventHandler
struct UploadStringCompletedEventHandler_t3962204127;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t679241653;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.IO.FileStream
struct FileStream_t4292183065;
// System.Net.CookieContainer
struct CookieContainer_t2331592909;
// System.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t2963430373;
// System.Net.AuthenticationSchemeSelector
struct AuthenticationSchemeSelector_t375327801;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t4070033136;
// System.Threading.WaitCallback
struct WaitCallback_t2448485498;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Net.WebConnection/AbortHelper
struct AbortHelper_t1490877826;
// System.Net.WebConnectionData
struct WebConnectionData_t3835660455;
// System.Net.ChunkStream
struct ChunkStream_t2634567336;
// System.Net.NetworkCredential
struct NetworkCredential_t3282608323;
// System.Type
struct Type_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t2970473191;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t3014364904;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Net.EndPointListener
struct EndPointListener_t2984434924;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Net.ListenerPrefix
struct ListenerPrefix_t3570496559;
// System.Net.RequestStream
struct RequestStream_t762880582;
// System.Net.WebResponse
struct WebResponse_t229922639;
// System.IO.StreamReader
struct StreamReader_t4009935899;
// System.Net.IPHostEntry
struct IPHostEntry_t263743900;
// System.Net.FtpAsyncResult
struct FtpAsyncResult_t3265664217;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t713131622;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t1029027275;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3399372417;
// System.Net.HttpContinueDelegate
struct HttpContinueDelegate_t3009151163;
// System.Net.WebConnectionStream
struct WebConnectionStream_t2170064850;
// System.Net.WebAsyncResult
struct WebAsyncResult_t3421962937;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// System.Net.DigestHeaderParser
struct DigestHeaderParser_t341650829;
// System.Net.FileWebResponse
struct FileWebResponse_t544571260;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Net.FileWebRequest
struct FileWebRequest_t591858885;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COOKIEPARSER_T2349142305_H
#define COOKIEPARSER_T2349142305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieParser
struct  CookieParser_t2349142305  : public RuntimeObject
{
public:
	// System.String System.Net.CookieParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.CookieParser::pos
	int32_t ___pos_1;
	// System.Int32 System.Net.CookieParser::length
	int32_t ___length_2;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(CookieParser_t2349142305, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(CookieParser_t2349142305, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(CookieParser_t2349142305, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEPARSER_T2349142305_H
#ifndef HTTPVERSION_T346520293_H
#define HTTPVERSION_T346520293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpVersion
struct  HttpVersion_t346520293  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t346520293_StaticFields
{
public:
	// System.Version System.Net.HttpVersion::Version10
	Version_t3456873960 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t3456873960 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t346520293_StaticFields, ___Version10_0)); }
	inline Version_t3456873960 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t3456873960 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t3456873960 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t346520293_StaticFields, ___Version11_1)); }
	inline Version_t3456873960 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t3456873960 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t3456873960 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T346520293_H
#ifndef HTTPUTILITY_T2559916872_H
#define HTTPUTILITY_T2559916872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpUtility
struct  HttpUtility_t2559916872  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPUTILITY_T2559916872_H
#ifndef LISTENERASYNCRESULT_T871495091_H
#define LISTENERASYNCRESULT_T871495091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ListenerAsyncResult
struct  ListenerAsyncResult_t871495091  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.ListenerAsyncResult::handle
	ManualResetEvent_t451242010 * ___handle_0;
	// System.Boolean System.Net.ListenerAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.ListenerAsyncResult::completed
	bool ___completed_2;
	// System.AsyncCallback System.Net.ListenerAsyncResult::cb
	AsyncCallback_t3962456242 * ___cb_3;
	// System.Object System.Net.ListenerAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Exception System.Net.ListenerAsyncResult::exception
	Exception_t * ___exception_5;
	// System.Net.HttpListenerContext System.Net.ListenerAsyncResult::context
	HttpListenerContext_t424880822 * ___context_6;
	// System.Object System.Net.ListenerAsyncResult::locker
	RuntimeObject * ___locker_7;
	// System.Net.ListenerAsyncResult System.Net.ListenerAsyncResult::forward
	ListenerAsyncResult_t871495091 * ___forward_8;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___handle_0)); }
	inline ManualResetEvent_t451242010 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_t451242010 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_t451242010 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___completed_2)); }
	inline bool get_completed_2() const { return ___completed_2; }
	inline bool* get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(bool value)
	{
		___completed_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___cb_3)); }
	inline AsyncCallback_t3962456242 * get_cb_3() const { return ___cb_3; }
	inline AsyncCallback_t3962456242 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(AsyncCallback_t3962456242 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_exception_5() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___exception_5)); }
	inline Exception_t * get_exception_5() const { return ___exception_5; }
	inline Exception_t ** get_address_of_exception_5() { return &___exception_5; }
	inline void set_exception_5(Exception_t * value)
	{
		___exception_5 = value;
		Il2CppCodeGenWriteBarrier((&___exception_5), value);
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___context_6)); }
	inline HttpListenerContext_t424880822 * get_context_6() const { return ___context_6; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(HttpListenerContext_t424880822 * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier((&___context_6), value);
	}

	inline static int32_t get_offset_of_locker_7() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___locker_7)); }
	inline RuntimeObject * get_locker_7() const { return ___locker_7; }
	inline RuntimeObject ** get_address_of_locker_7() { return &___locker_7; }
	inline void set_locker_7(RuntimeObject * value)
	{
		___locker_7 = value;
		Il2CppCodeGenWriteBarrier((&___locker_7), value);
	}

	inline static int32_t get_offset_of_forward_8() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t871495091, ___forward_8)); }
	inline ListenerAsyncResult_t871495091 * get_forward_8() const { return ___forward_8; }
	inline ListenerAsyncResult_t871495091 ** get_address_of_forward_8() { return &___forward_8; }
	inline void set_forward_8(ListenerAsyncResult_t871495091 * value)
	{
		___forward_8 = value;
		Il2CppCodeGenWriteBarrier((&___forward_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENERASYNCRESULT_T871495091_H
#ifndef LISTENERPREFIX_T3570496559_H
#define LISTENERPREFIX_T3570496559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ListenerPrefix
struct  ListenerPrefix_t3570496559  : public RuntimeObject
{
public:
	// System.String System.Net.ListenerPrefix::original
	String_t* ___original_0;
	// System.String System.Net.ListenerPrefix::host
	String_t* ___host_1;
	// System.UInt16 System.Net.ListenerPrefix::port
	uint16_t ___port_2;
	// System.String System.Net.ListenerPrefix::path
	String_t* ___path_3;
	// System.Boolean System.Net.ListenerPrefix::secure
	bool ___secure_4;
	// System.Net.IPAddress[] System.Net.ListenerPrefix::addresses
	IPAddressU5BU5D_t596328627* ___addresses_5;
	// System.Net.HttpListener System.Net.ListenerPrefix::Listener
	HttpListener_t988452056 * ___Listener_6;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___original_0)); }
	inline String_t* get_original_0() const { return ___original_0; }
	inline String_t** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(String_t* value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___port_2)); }
	inline uint16_t get_port_2() const { return ___port_2; }
	inline uint16_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(uint16_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_secure_4() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___secure_4)); }
	inline bool get_secure_4() const { return ___secure_4; }
	inline bool* get_address_of_secure_4() { return &___secure_4; }
	inline void set_secure_4(bool value)
	{
		___secure_4 = value;
	}

	inline static int32_t get_offset_of_addresses_5() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___addresses_5)); }
	inline IPAddressU5BU5D_t596328627* get_addresses_5() const { return ___addresses_5; }
	inline IPAddressU5BU5D_t596328627** get_address_of_addresses_5() { return &___addresses_5; }
	inline void set_addresses_5(IPAddressU5BU5D_t596328627* value)
	{
		___addresses_5 = value;
		Il2CppCodeGenWriteBarrier((&___addresses_5), value);
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(ListenerPrefix_t3570496559, ___Listener_6)); }
	inline HttpListener_t988452056 * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_t988452056 ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_t988452056 * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENERPREFIX_T3570496559_H
#ifndef IPHOSTENTRY_T263743900_H
#define IPHOSTENTRY_T263743900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPHostEntry
struct  IPHostEntry_t263743900  : public RuntimeObject
{
public:
	// System.Net.IPAddress[] System.Net.IPHostEntry::addressList
	IPAddressU5BU5D_t596328627* ___addressList_0;
	// System.String[] System.Net.IPHostEntry::aliases
	StringU5BU5D_t1281789340* ___aliases_1;
	// System.String System.Net.IPHostEntry::hostName
	String_t* ___hostName_2;

public:
	inline static int32_t get_offset_of_addressList_0() { return static_cast<int32_t>(offsetof(IPHostEntry_t263743900, ___addressList_0)); }
	inline IPAddressU5BU5D_t596328627* get_addressList_0() const { return ___addressList_0; }
	inline IPAddressU5BU5D_t596328627** get_address_of_addressList_0() { return &___addressList_0; }
	inline void set_addressList_0(IPAddressU5BU5D_t596328627* value)
	{
		___addressList_0 = value;
		Il2CppCodeGenWriteBarrier((&___addressList_0), value);
	}

	inline static int32_t get_offset_of_aliases_1() { return static_cast<int32_t>(offsetof(IPHostEntry_t263743900, ___aliases_1)); }
	inline StringU5BU5D_t1281789340* get_aliases_1() const { return ___aliases_1; }
	inline StringU5BU5D_t1281789340** get_address_of_aliases_1() { return &___aliases_1; }
	inline void set_aliases_1(StringU5BU5D_t1281789340* value)
	{
		___aliases_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliases_1), value);
	}

	inline static int32_t get_offset_of_hostName_2() { return static_cast<int32_t>(offsetof(IPHostEntry_t263743900, ___hostName_2)); }
	inline String_t* get_hostName_2() const { return ___hostName_2; }
	inline String_t** get_address_of_hostName_2() { return &___hostName_2; }
	inline void set_hostName_2(String_t* value)
	{
		___hostName_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPHOSTENTRY_T263743900_H
#ifndef IPV6ADDRESS_T2709566769_H
#define IPV6ADDRESS_T2709566769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6Address
struct  IPv6Address_t2709566769  : public RuntimeObject
{
public:
	// System.UInt16[] System.Net.IPv6Address::address
	UInt16U5BU5D_t3326319531* ___address_0;
	// System.Int32 System.Net.IPv6Address::prefixLength
	int32_t ___prefixLength_1;
	// System.Int64 System.Net.IPv6Address::scopeId
	int64_t ___scopeId_2;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6Address_t2709566769, ___address_0)); }
	inline UInt16U5BU5D_t3326319531* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t3326319531** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t3326319531* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_prefixLength_1() { return static_cast<int32_t>(offsetof(IPv6Address_t2709566769, ___prefixLength_1)); }
	inline int32_t get_prefixLength_1() const { return ___prefixLength_1; }
	inline int32_t* get_address_of_prefixLength_1() { return &___prefixLength_1; }
	inline void set_prefixLength_1(int32_t value)
	{
		___prefixLength_1 = value;
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(IPv6Address_t2709566769, ___scopeId_2)); }
	inline int64_t get_scopeId_2() const { return ___scopeId_2; }
	inline int64_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int64_t value)
	{
		___scopeId_2 = value;
	}
};

struct IPv6Address_t2709566769_StaticFields
{
public:
	// System.Net.IPv6Address System.Net.IPv6Address::Loopback
	IPv6Address_t2709566769 * ___Loopback_3;
	// System.Net.IPv6Address System.Net.IPv6Address::Unspecified
	IPv6Address_t2709566769 * ___Unspecified_4;

public:
	inline static int32_t get_offset_of_Loopback_3() { return static_cast<int32_t>(offsetof(IPv6Address_t2709566769_StaticFields, ___Loopback_3)); }
	inline IPv6Address_t2709566769 * get_Loopback_3() const { return ___Loopback_3; }
	inline IPv6Address_t2709566769 ** get_address_of_Loopback_3() { return &___Loopback_3; }
	inline void set_Loopback_3(IPv6Address_t2709566769 * value)
	{
		___Loopback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_3), value);
	}

	inline static int32_t get_offset_of_Unspecified_4() { return static_cast<int32_t>(offsetof(IPv6Address_t2709566769_StaticFields, ___Unspecified_4)); }
	inline IPv6Address_t2709566769 * get_Unspecified_4() const { return ___Unspecified_4; }
	inline IPv6Address_t2709566769 ** get_address_of_Unspecified_4() { return &___Unspecified_4; }
	inline void set_Unspecified_4(IPv6Address_t2709566769 * value)
	{
		___Unspecified_4 = value;
		Il2CppCodeGenWriteBarrier((&___Unspecified_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESS_T2709566769_H
#ifndef HTTPLISTENERCONTEXT_T424880822_H
#define HTTPLISTENERCONTEXT_T424880822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerContext
struct  HttpListenerContext_t424880822  : public RuntimeObject
{
public:
	// System.Net.HttpListenerRequest System.Net.HttpListenerContext::request
	HttpListenerRequest_t630699488 * ___request_0;
	// System.Net.HttpListenerResponse System.Net.HttpListenerContext::response
	HttpListenerResponse_t3502667045 * ___response_1;
	// System.Security.Principal.IPrincipal System.Net.HttpListenerContext::user
	RuntimeObject* ___user_2;
	// System.Net.HttpConnection System.Net.HttpListenerContext::cnc
	HttpConnection_t269576101 * ___cnc_3;
	// System.String System.Net.HttpListenerContext::error
	String_t* ___error_4;
	// System.Int32 System.Net.HttpListenerContext::err_status
	int32_t ___err_status_5;
	// System.Net.HttpListener System.Net.HttpListenerContext::Listener
	HttpListener_t988452056 * ___Listener_6;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___request_0)); }
	inline HttpListenerRequest_t630699488 * get_request_0() const { return ___request_0; }
	inline HttpListenerRequest_t630699488 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpListenerRequest_t630699488 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___response_1)); }
	inline HttpListenerResponse_t3502667045 * get_response_1() const { return ___response_1; }
	inline HttpListenerResponse_t3502667045 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(HttpListenerResponse_t3502667045 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}

	inline static int32_t get_offset_of_user_2() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___user_2)); }
	inline RuntimeObject* get_user_2() const { return ___user_2; }
	inline RuntimeObject** get_address_of_user_2() { return &___user_2; }
	inline void set_user_2(RuntimeObject* value)
	{
		___user_2 = value;
		Il2CppCodeGenWriteBarrier((&___user_2), value);
	}

	inline static int32_t get_offset_of_cnc_3() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___cnc_3)); }
	inline HttpConnection_t269576101 * get_cnc_3() const { return ___cnc_3; }
	inline HttpConnection_t269576101 ** get_address_of_cnc_3() { return &___cnc_3; }
	inline void set_cnc_3(HttpConnection_t269576101 * value)
	{
		___cnc_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_3), value);
	}

	inline static int32_t get_offset_of_error_4() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___error_4)); }
	inline String_t* get_error_4() const { return ___error_4; }
	inline String_t** get_address_of_error_4() { return &___error_4; }
	inline void set_error_4(String_t* value)
	{
		___error_4 = value;
		Il2CppCodeGenWriteBarrier((&___error_4), value);
	}

	inline static int32_t get_offset_of_err_status_5() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___err_status_5)); }
	inline int32_t get_err_status_5() const { return ___err_status_5; }
	inline int32_t* get_address_of_err_status_5() { return &___err_status_5; }
	inline void set_err_status_5(int32_t value)
	{
		___err_status_5 = value;
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(HttpListenerContext_t424880822, ___Listener_6)); }
	inline HttpListener_t988452056 * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_t988452056 ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_t988452056 * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERCONTEXT_T424880822_H
#ifndef EMPTYWEBPROXY_T3036161737_H
#define EMPTYWEBPROXY_T3036161737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalProxySelection/EmptyWebProxy
struct  EmptyWebProxy_t3036161737  : public RuntimeObject
{
public:
	// System.Net.ICredentials System.Net.GlobalProxySelection/EmptyWebProxy::credentials
	RuntimeObject* ___credentials_0;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(EmptyWebProxy_t3036161737, ___credentials_0)); }
	inline RuntimeObject* get_credentials_0() const { return ___credentials_0; }
	inline RuntimeObject** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(RuntimeObject* value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYWEBPROXY_T3036161737_H
#ifndef GLOBALPROXYSELECTION_T1166292522_H
#define GLOBALPROXYSELECTION_T1166292522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalProxySelection
struct  GlobalProxySelection_t1166292522  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALPROXYSELECTION_T1166292522_H
#ifndef HTTPLISTENERPREFIXCOLLECTION_T2963430373_H
#define HTTPLISTENERPREFIXCOLLECTION_T2963430373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerPrefixCollection
struct  HttpListenerPrefixCollection_t2963430373  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Net.HttpListenerPrefixCollection::prefixes
	List_1_t3319525431 * ___prefixes_0;
	// System.Net.HttpListener System.Net.HttpListenerPrefixCollection::listener
	HttpListener_t988452056 * ___listener_1;

public:
	inline static int32_t get_offset_of_prefixes_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t2963430373, ___prefixes_0)); }
	inline List_1_t3319525431 * get_prefixes_0() const { return ___prefixes_0; }
	inline List_1_t3319525431 ** get_address_of_prefixes_0() { return &___prefixes_0; }
	inline void set_prefixes_0(List_1_t3319525431 * value)
	{
		___prefixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_0), value);
	}

	inline static int32_t get_offset_of_listener_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t2963430373, ___listener_1)); }
	inline HttpListener_t988452056 * get_listener_1() const { return ___listener_1; }
	inline HttpListener_t988452056 ** get_address_of_listener_1() { return &___listener_1; }
	inline void set_listener_1(HttpListener_t988452056 * value)
	{
		___listener_1 = value;
		Il2CppCodeGenWriteBarrier((&___listener_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIXCOLLECTION_T2963430373_H
#ifndef HTTPSTREAMASYNCRESULT_T1178010344_H
#define HTTPSTREAMASYNCRESULT_T1178010344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStreamAsyncResult
struct  HttpStreamAsyncResult_t1178010344  : public RuntimeObject
{
public:
	// System.Object System.Net.HttpStreamAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.Threading.ManualResetEvent System.Net.HttpStreamAsyncResult::handle
	ManualResetEvent_t451242010 * ___handle_1;
	// System.Boolean System.Net.HttpStreamAsyncResult::completed
	bool ___completed_2;
	// System.Byte[] System.Net.HttpStreamAsyncResult::Buffer
	ByteU5BU5D_t4116647657* ___Buffer_3;
	// System.Int32 System.Net.HttpStreamAsyncResult::Offset
	int32_t ___Offset_4;
	// System.Int32 System.Net.HttpStreamAsyncResult::Count
	int32_t ___Count_5;
	// System.AsyncCallback System.Net.HttpStreamAsyncResult::Callback
	AsyncCallback_t3962456242 * ___Callback_6;
	// System.Object System.Net.HttpStreamAsyncResult::State
	RuntimeObject * ___State_7;
	// System.Int32 System.Net.HttpStreamAsyncResult::SynchRead
	int32_t ___SynchRead_8;
	// System.Exception System.Net.HttpStreamAsyncResult::Error
	Exception_t * ___Error_9;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___handle_1)); }
	inline ManualResetEvent_t451242010 * get_handle_1() const { return ___handle_1; }
	inline ManualResetEvent_t451242010 ** get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(ManualResetEvent_t451242010 * value)
	{
		___handle_1 = value;
		Il2CppCodeGenWriteBarrier((&___handle_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___completed_2)); }
	inline bool get_completed_2() const { return ___completed_2; }
	inline bool* get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(bool value)
	{
		___completed_2 = value;
	}

	inline static int32_t get_offset_of_Buffer_3() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___Buffer_3)); }
	inline ByteU5BU5D_t4116647657* get_Buffer_3() const { return ___Buffer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_Buffer_3() { return &___Buffer_3; }
	inline void set_Buffer_3(ByteU5BU5D_t4116647657* value)
	{
		___Buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_3), value);
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___Offset_4)); }
	inline int32_t get_Offset_4() const { return ___Offset_4; }
	inline int32_t* get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(int32_t value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Count_5() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___Count_5)); }
	inline int32_t get_Count_5() const { return ___Count_5; }
	inline int32_t* get_address_of_Count_5() { return &___Count_5; }
	inline void set_Count_5(int32_t value)
	{
		___Count_5 = value;
	}

	inline static int32_t get_offset_of_Callback_6() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___Callback_6)); }
	inline AsyncCallback_t3962456242 * get_Callback_6() const { return ___Callback_6; }
	inline AsyncCallback_t3962456242 ** get_address_of_Callback_6() { return &___Callback_6; }
	inline void set_Callback_6(AsyncCallback_t3962456242 * value)
	{
		___Callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_6), value);
	}

	inline static int32_t get_offset_of_State_7() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___State_7)); }
	inline RuntimeObject * get_State_7() const { return ___State_7; }
	inline RuntimeObject ** get_address_of_State_7() { return &___State_7; }
	inline void set_State_7(RuntimeObject * value)
	{
		___State_7 = value;
		Il2CppCodeGenWriteBarrier((&___State_7), value);
	}

	inline static int32_t get_offset_of_SynchRead_8() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___SynchRead_8)); }
	inline int32_t get_SynchRead_8() const { return ___SynchRead_8; }
	inline int32_t* get_address_of_SynchRead_8() { return &___SynchRead_8; }
	inline void set_SynchRead_8(int32_t value)
	{
		___SynchRead_8 = value;
	}

	inline static int32_t get_offset_of_Error_9() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t1178010344, ___Error_9)); }
	inline Exception_t * get_Error_9() const { return ___Error_9; }
	inline Exception_t ** get_address_of_Error_9() { return &___Error_9; }
	inline void set_Error_9(Exception_t * value)
	{
		___Error_9 = value;
		Il2CppCodeGenWriteBarrier((&___Error_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTREAMASYNCRESULT_T1178010344_H
#ifndef HTTPREQUESTCREATOR_T1984314013_H
#define HTTPREQUESTCREATOR_T1984314013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpRequestCreator
struct  HttpRequestCreator_t1984314013  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTCREATOR_T1984314013_H
#ifndef HTTPLISTENERRESPONSE_T3502667045_H
#define HTTPLISTENERRESPONSE_T3502667045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerResponse
struct  HttpListenerResponse_t3502667045  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HttpListenerResponse::disposed
	bool ___disposed_0;
	// System.Text.Encoding System.Net.HttpListenerResponse::content_encoding
	Encoding_t1523322056 * ___content_encoding_1;
	// System.Int64 System.Net.HttpListenerResponse::content_length
	int64_t ___content_length_2;
	// System.Boolean System.Net.HttpListenerResponse::cl_set
	bool ___cl_set_3;
	// System.String System.Net.HttpListenerResponse::content_type
	String_t* ___content_type_4;
	// System.Net.CookieCollection System.Net.HttpListenerResponse::cookies
	CookieCollection_t3881042616 * ___cookies_5;
	// System.Net.WebHeaderCollection System.Net.HttpListenerResponse::headers
	WebHeaderCollection_t1942268960 * ___headers_6;
	// System.Boolean System.Net.HttpListenerResponse::keep_alive
	bool ___keep_alive_7;
	// System.Net.ResponseStream System.Net.HttpListenerResponse::output_stream
	ResponseStream_t3810703494 * ___output_stream_8;
	// System.Version System.Net.HttpListenerResponse::version
	Version_t3456873960 * ___version_9;
	// System.String System.Net.HttpListenerResponse::location
	String_t* ___location_10;
	// System.Int32 System.Net.HttpListenerResponse::status_code
	int32_t ___status_code_11;
	// System.String System.Net.HttpListenerResponse::status_description
	String_t* ___status_description_12;
	// System.Boolean System.Net.HttpListenerResponse::chunked
	bool ___chunked_13;
	// System.Net.HttpListenerContext System.Net.HttpListenerResponse::context
	HttpListenerContext_t424880822 * ___context_14;
	// System.Boolean System.Net.HttpListenerResponse::HeadersSent
	bool ___HeadersSent_15;
	// System.Boolean System.Net.HttpListenerResponse::force_close_chunked
	bool ___force_close_chunked_16;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_content_encoding_1() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___content_encoding_1)); }
	inline Encoding_t1523322056 * get_content_encoding_1() const { return ___content_encoding_1; }
	inline Encoding_t1523322056 ** get_address_of_content_encoding_1() { return &___content_encoding_1; }
	inline void set_content_encoding_1(Encoding_t1523322056 * value)
	{
		___content_encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_encoding_1), value);
	}

	inline static int32_t get_offset_of_content_length_2() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___content_length_2)); }
	inline int64_t get_content_length_2() const { return ___content_length_2; }
	inline int64_t* get_address_of_content_length_2() { return &___content_length_2; }
	inline void set_content_length_2(int64_t value)
	{
		___content_length_2 = value;
	}

	inline static int32_t get_offset_of_cl_set_3() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___cl_set_3)); }
	inline bool get_cl_set_3() const { return ___cl_set_3; }
	inline bool* get_address_of_cl_set_3() { return &___cl_set_3; }
	inline void set_cl_set_3(bool value)
	{
		___cl_set_3 = value;
	}

	inline static int32_t get_offset_of_content_type_4() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___content_type_4)); }
	inline String_t* get_content_type_4() const { return ___content_type_4; }
	inline String_t** get_address_of_content_type_4() { return &___content_type_4; }
	inline void set_content_type_4(String_t* value)
	{
		___content_type_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_type_4), value);
	}

	inline static int32_t get_offset_of_cookies_5() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___cookies_5)); }
	inline CookieCollection_t3881042616 * get_cookies_5() const { return ___cookies_5; }
	inline CookieCollection_t3881042616 ** get_address_of_cookies_5() { return &___cookies_5; }
	inline void set_cookies_5(CookieCollection_t3881042616 * value)
	{
		___cookies_5 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_5), value);
	}

	inline static int32_t get_offset_of_headers_6() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___headers_6)); }
	inline WebHeaderCollection_t1942268960 * get_headers_6() const { return ___headers_6; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_6() { return &___headers_6; }
	inline void set_headers_6(WebHeaderCollection_t1942268960 * value)
	{
		___headers_6 = value;
		Il2CppCodeGenWriteBarrier((&___headers_6), value);
	}

	inline static int32_t get_offset_of_keep_alive_7() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___keep_alive_7)); }
	inline bool get_keep_alive_7() const { return ___keep_alive_7; }
	inline bool* get_address_of_keep_alive_7() { return &___keep_alive_7; }
	inline void set_keep_alive_7(bool value)
	{
		___keep_alive_7 = value;
	}

	inline static int32_t get_offset_of_output_stream_8() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___output_stream_8)); }
	inline ResponseStream_t3810703494 * get_output_stream_8() const { return ___output_stream_8; }
	inline ResponseStream_t3810703494 ** get_address_of_output_stream_8() { return &___output_stream_8; }
	inline void set_output_stream_8(ResponseStream_t3810703494 * value)
	{
		___output_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___output_stream_8), value);
	}

	inline static int32_t get_offset_of_version_9() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___version_9)); }
	inline Version_t3456873960 * get_version_9() const { return ___version_9; }
	inline Version_t3456873960 ** get_address_of_version_9() { return &___version_9; }
	inline void set_version_9(Version_t3456873960 * value)
	{
		___version_9 = value;
		Il2CppCodeGenWriteBarrier((&___version_9), value);
	}

	inline static int32_t get_offset_of_location_10() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___location_10)); }
	inline String_t* get_location_10() const { return ___location_10; }
	inline String_t** get_address_of_location_10() { return &___location_10; }
	inline void set_location_10(String_t* value)
	{
		___location_10 = value;
		Il2CppCodeGenWriteBarrier((&___location_10), value);
	}

	inline static int32_t get_offset_of_status_code_11() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___status_code_11)); }
	inline int32_t get_status_code_11() const { return ___status_code_11; }
	inline int32_t* get_address_of_status_code_11() { return &___status_code_11; }
	inline void set_status_code_11(int32_t value)
	{
		___status_code_11 = value;
	}

	inline static int32_t get_offset_of_status_description_12() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___status_description_12)); }
	inline String_t* get_status_description_12() const { return ___status_description_12; }
	inline String_t** get_address_of_status_description_12() { return &___status_description_12; }
	inline void set_status_description_12(String_t* value)
	{
		___status_description_12 = value;
		Il2CppCodeGenWriteBarrier((&___status_description_12), value);
	}

	inline static int32_t get_offset_of_chunked_13() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___chunked_13)); }
	inline bool get_chunked_13() const { return ___chunked_13; }
	inline bool* get_address_of_chunked_13() { return &___chunked_13; }
	inline void set_chunked_13(bool value)
	{
		___chunked_13 = value;
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___context_14)); }
	inline HttpListenerContext_t424880822 * get_context_14() const { return ___context_14; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(HttpListenerContext_t424880822 * value)
	{
		___context_14 = value;
		Il2CppCodeGenWriteBarrier((&___context_14), value);
	}

	inline static int32_t get_offset_of_HeadersSent_15() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___HeadersSent_15)); }
	inline bool get_HeadersSent_15() const { return ___HeadersSent_15; }
	inline bool* get_address_of_HeadersSent_15() { return &___HeadersSent_15; }
	inline void set_HeadersSent_15(bool value)
	{
		___HeadersSent_15 = value;
	}

	inline static int32_t get_offset_of_force_close_chunked_16() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t3502667045, ___force_close_chunked_16)); }
	inline bool get_force_close_chunked_16() const { return ___force_close_chunked_16; }
	inline bool* get_address_of_force_close_chunked_16() { return &___force_close_chunked_16; }
	inline void set_force_close_chunked_16(bool value)
	{
		___force_close_chunked_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERRESPONSE_T3502667045_H
#ifndef MONOHTTPDATE_T4033147742_H
#define MONOHTTPDATE_T4033147742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.MonoHttpDate
struct  MonoHttpDate_t4033147742  : public RuntimeObject
{
public:

public:
};

struct MonoHttpDate_t4033147742_StaticFields
{
public:
	// System.String System.Net.MonoHttpDate::rfc1123_date
	String_t* ___rfc1123_date_0;
	// System.String System.Net.MonoHttpDate::rfc850_date
	String_t* ___rfc850_date_1;
	// System.String System.Net.MonoHttpDate::asctime_date
	String_t* ___asctime_date_2;
	// System.String[] System.Net.MonoHttpDate::formats
	StringU5BU5D_t1281789340* ___formats_3;

public:
	inline static int32_t get_offset_of_rfc1123_date_0() { return static_cast<int32_t>(offsetof(MonoHttpDate_t4033147742_StaticFields, ___rfc1123_date_0)); }
	inline String_t* get_rfc1123_date_0() const { return ___rfc1123_date_0; }
	inline String_t** get_address_of_rfc1123_date_0() { return &___rfc1123_date_0; }
	inline void set_rfc1123_date_0(String_t* value)
	{
		___rfc1123_date_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc1123_date_0), value);
	}

	inline static int32_t get_offset_of_rfc850_date_1() { return static_cast<int32_t>(offsetof(MonoHttpDate_t4033147742_StaticFields, ___rfc850_date_1)); }
	inline String_t* get_rfc850_date_1() const { return ___rfc850_date_1; }
	inline String_t** get_address_of_rfc850_date_1() { return &___rfc850_date_1; }
	inline void set_rfc850_date_1(String_t* value)
	{
		___rfc850_date_1 = value;
		Il2CppCodeGenWriteBarrier((&___rfc850_date_1), value);
	}

	inline static int32_t get_offset_of_asctime_date_2() { return static_cast<int32_t>(offsetof(MonoHttpDate_t4033147742_StaticFields, ___asctime_date_2)); }
	inline String_t* get_asctime_date_2() const { return ___asctime_date_2; }
	inline String_t** get_address_of_asctime_date_2() { return &___asctime_date_2; }
	inline void set_asctime_date_2(String_t* value)
	{
		___asctime_date_2 = value;
		Il2CppCodeGenWriteBarrier((&___asctime_date_2), value);
	}

	inline static int32_t get_offset_of_formats_3() { return static_cast<int32_t>(offsetof(MonoHttpDate_t4033147742_StaticFields, ___formats_3)); }
	inline StringU5BU5D_t1281789340* get_formats_3() const { return ___formats_3; }
	inline StringU5BU5D_t1281789340** get_address_of_formats_3() { return &___formats_3; }
	inline void set_formats_3(StringU5BU5D_t1281789340* value)
	{
		___formats_3 = value;
		Il2CppCodeGenWriteBarrier((&___formats_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOHTTPDATE_T4033147742_H
#ifndef GENERICIDENTITY_T2319019448_H
#define GENERICIDENTITY_T2319019448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.GenericIdentity
struct  GenericIdentity_t2319019448  : public RuntimeObject
{
public:
	// System.String System.Security.Principal.GenericIdentity::m_name
	String_t* ___m_name_0;
	// System.String System.Security.Principal.GenericIdentity::m_type
	String_t* ___m_type_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(GenericIdentity_t2319019448, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_type_1() { return static_cast<int32_t>(offsetof(GenericIdentity_t2319019448, ___m_type_1)); }
	inline String_t* get_m_type_1() const { return ___m_type_1; }
	inline String_t** get_address_of_m_type_1() { return &___m_type_1; }
	inline void set_m_type_1(String_t* value)
	{
		___m_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICIDENTITY_T2319019448_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef WEBCONNECTIONGROUP_T1712379988_H
#define WEBCONNECTIONGROUP_T1712379988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup
struct  WebConnectionGroup_t1712379988  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnectionGroup::sPoint
	ServicePoint_t2786966844 * ___sPoint_0;
	// System.String System.Net.WebConnectionGroup::name
	String_t* ___name_1;
	// System.Collections.ArrayList System.Net.WebConnectionGroup::connections
	ArrayList_t2718874744 * ___connections_2;
	// System.Random System.Net.WebConnectionGroup::rnd
	Random_t108471755 * ___rnd_3;
	// System.Collections.Queue System.Net.WebConnectionGroup::queue
	Queue_t3637523393 * ___queue_4;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1712379988, ___sPoint_0)); }
	inline ServicePoint_t2786966844 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t2786966844 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t2786966844 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1712379988, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1712379988, ___connections_2)); }
	inline ArrayList_t2718874744 * get_connections_2() const { return ___connections_2; }
	inline ArrayList_t2718874744 ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(ArrayList_t2718874744 * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier((&___connections_2), value);
	}

	inline static int32_t get_offset_of_rnd_3() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1712379988, ___rnd_3)); }
	inline Random_t108471755 * get_rnd_3() const { return ___rnd_3; }
	inline Random_t108471755 ** get_address_of_rnd_3() { return &___rnd_3; }
	inline void set_rnd_3(Random_t108471755 * value)
	{
		___rnd_3 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_3), value);
	}

	inline static int32_t get_offset_of_queue_4() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t1712379988, ___queue_4)); }
	inline Queue_t3637523393 * get_queue_4() const { return ___queue_4; }
	inline Queue_t3637523393 ** get_address_of_queue_4() { return &___queue_4; }
	inline void set_queue_4(Queue_t3637523393 * value)
	{
		___queue_4 = value;
		Il2CppCodeGenWriteBarrier((&___queue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONGROUP_T1712379988_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef SPKEY_T3654231119_H
#define SPKEY_T3654231119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/SPKey
struct  SPKey_t3654231119  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePointManager/SPKey::uri
	Uri_t100236324 * ___uri_0;
	// System.Boolean System.Net.ServicePointManager/SPKey::use_connect
	bool ___use_connect_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(SPKey_t3654231119, ___uri_0)); }
	inline Uri_t100236324 * get_uri_0() const { return ___uri_0; }
	inline Uri_t100236324 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t100236324 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_use_connect_1() { return static_cast<int32_t>(offsetof(SPKey_t3654231119, ___use_connect_1)); }
	inline bool get_use_connect_1() const { return ___use_connect_1; }
	inline bool* get_address_of_use_connect_1() { return &___use_connect_1; }
	inline void set_use_connect_1(bool value)
	{
		___use_connect_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPKEY_T3654231119_H
#ifndef NETWORKCREDENTIAL_T3282608323_H
#define NETWORKCREDENTIAL_T3282608323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkCredential
struct  NetworkCredential_t3282608323  : public RuntimeObject
{
public:
	// System.String System.Net.NetworkCredential::userName
	String_t* ___userName_0;
	// System.String System.Net.NetworkCredential::password
	String_t* ___password_1;
	// System.String System.Net.NetworkCredential::domain
	String_t* ___domain_2;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t3282608323, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t3282608323, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_domain_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t3282608323, ___domain_2)); }
	inline String_t* get_domain_2() const { return ___domain_2; }
	inline String_t** get_address_of_domain_2() { return &___domain_2; }
	inline void set_domain_2(String_t* value)
	{
		___domain_2 = value;
		Il2CppCodeGenWriteBarrier((&___domain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T3282608323_H
#ifndef NETCONFIG_T2828594564_H
#define NETCONFIG_T2828594564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetConfig
struct  NetConfig_t2828594564  : public RuntimeObject
{
public:
	// System.Boolean System.Net.NetConfig::ipv6Enabled
	bool ___ipv6Enabled_0;
	// System.Int32 System.Net.NetConfig::MaxResponseHeadersLength
	int32_t ___MaxResponseHeadersLength_1;

public:
	inline static int32_t get_offset_of_ipv6Enabled_0() { return static_cast<int32_t>(offsetof(NetConfig_t2828594564, ___ipv6Enabled_0)); }
	inline bool get_ipv6Enabled_0() const { return ___ipv6Enabled_0; }
	inline bool* get_address_of_ipv6Enabled_0() { return &___ipv6Enabled_0; }
	inline void set_ipv6Enabled_0(bool value)
	{
		___ipv6Enabled_0 = value;
	}

	inline static int32_t get_offset_of_MaxResponseHeadersLength_1() { return static_cast<int32_t>(offsetof(NetConfig_t2828594564, ___MaxResponseHeadersLength_1)); }
	inline int32_t get_MaxResponseHeadersLength_1() const { return ___MaxResponseHeadersLength_1; }
	inline int32_t* get_address_of_MaxResponseHeadersLength_1() { return &___MaxResponseHeadersLength_1; }
	inline void set_MaxResponseHeadersLength_1(int32_t value)
	{
		___MaxResponseHeadersLength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETCONFIG_T2828594564_H
#ifndef SOCKETADDRESS_T3739769427_H
#define SOCKETADDRESS_T3739769427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_t3739769427  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.SocketAddress::data
	ByteU5BU5D_t4116647657* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(SocketAddress_t3739769427, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_T3739769427_H
#ifndef WEBCONNECTIONDATA_T3835660455_H
#define WEBCONNECTIONDATA_T3835660455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionData
struct  WebConnectionData_t3835660455  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest System.Net.WebConnectionData::request
	HttpWebRequest_t1669436515 * ___request_0;
	// System.Int32 System.Net.WebConnectionData::StatusCode
	int32_t ___StatusCode_1;
	// System.String System.Net.WebConnectionData::StatusDescription
	String_t* ___StatusDescription_2;
	// System.Net.WebHeaderCollection System.Net.WebConnectionData::Headers
	WebHeaderCollection_t1942268960 * ___Headers_3;
	// System.Version System.Net.WebConnectionData::Version
	Version_t3456873960 * ___Version_4;
	// System.IO.Stream System.Net.WebConnectionData::stream
	Stream_t1273022909 * ___stream_5;
	// System.String System.Net.WebConnectionData::Challenge
	String_t* ___Challenge_6;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___request_0)); }
	inline HttpWebRequest_t1669436515 * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t1669436515 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_StatusCode_1() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___StatusCode_1)); }
	inline int32_t get_StatusCode_1() const { return ___StatusCode_1; }
	inline int32_t* get_address_of_StatusCode_1() { return &___StatusCode_1; }
	inline void set_StatusCode_1(int32_t value)
	{
		___StatusCode_1 = value;
	}

	inline static int32_t get_offset_of_StatusDescription_2() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___StatusDescription_2)); }
	inline String_t* get_StatusDescription_2() const { return ___StatusDescription_2; }
	inline String_t** get_address_of_StatusDescription_2() { return &___StatusDescription_2; }
	inline void set_StatusDescription_2(String_t* value)
	{
		___StatusDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___StatusDescription_2), value);
	}

	inline static int32_t get_offset_of_Headers_3() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___Headers_3)); }
	inline WebHeaderCollection_t1942268960 * get_Headers_3() const { return ___Headers_3; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_Headers_3() { return &___Headers_3; }
	inline void set_Headers_3(WebHeaderCollection_t1942268960 * value)
	{
		___Headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Headers_3), value);
	}

	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___Version_4)); }
	inline Version_t3456873960 * get_Version_4() const { return ___Version_4; }
	inline Version_t3456873960 ** get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(Version_t3456873960 * value)
	{
		___Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___Version_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_Challenge_6() { return static_cast<int32_t>(offsetof(WebConnectionData_t3835660455, ___Challenge_6)); }
	inline String_t* get_Challenge_6() const { return ___Challenge_6; }
	inline String_t** get_address_of_Challenge_6() { return &___Challenge_6; }
	inline void set_Challenge_6(String_t* value)
	{
		___Challenge_6 = value;
		Il2CppCodeGenWriteBarrier((&___Challenge_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONDATA_T3835660455_H
#ifndef ABORTHELPER_T1490877826_H
#define ABORTHELPER_T1490877826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection/AbortHelper
struct  AbortHelper_t1490877826  : public RuntimeObject
{
public:
	// System.Net.WebConnection System.Net.WebConnection/AbortHelper::Connection
	WebConnection_t3982808322 * ___Connection_0;

public:
	inline static int32_t get_offset_of_Connection_0() { return static_cast<int32_t>(offsetof(AbortHelper_t1490877826, ___Connection_0)); }
	inline WebConnection_t3982808322 * get_Connection_0() const { return ___Connection_0; }
	inline WebConnection_t3982808322 ** get_address_of_Connection_0() { return &___Connection_0; }
	inline void set_Connection_0(WebConnection_t3982808322 * value)
	{
		___Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABORTHELPER_T1490877826_H
#ifndef WEBASYNCRESULT_T3421962937_H
#define WEBASYNCRESULT_T3421962937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebAsyncResult
struct  WebAsyncResult_t3421962937  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.WebAsyncResult::handle
	ManualResetEvent_t451242010 * ___handle_0;
	// System.Boolean System.Net.WebAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.WebAsyncResult::isCompleted
	bool ___isCompleted_2;
	// System.AsyncCallback System.Net.WebAsyncResult::cb
	AsyncCallback_t3962456242 * ___cb_3;
	// System.Object System.Net.WebAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Int32 System.Net.WebAsyncResult::nbytes
	int32_t ___nbytes_5;
	// System.IAsyncResult System.Net.WebAsyncResult::innerAsyncResult
	RuntimeObject* ___innerAsyncResult_6;
	// System.Boolean System.Net.WebAsyncResult::callbackDone
	bool ___callbackDone_7;
	// System.Exception System.Net.WebAsyncResult::exc
	Exception_t * ___exc_8;
	// System.Net.HttpWebResponse System.Net.WebAsyncResult::response
	HttpWebResponse_t3286585418 * ___response_9;
	// System.IO.Stream System.Net.WebAsyncResult::writeStream
	Stream_t1273022909 * ___writeStream_10;
	// System.Byte[] System.Net.WebAsyncResult::buffer
	ByteU5BU5D_t4116647657* ___buffer_11;
	// System.Int32 System.Net.WebAsyncResult::offset
	int32_t ___offset_12;
	// System.Int32 System.Net.WebAsyncResult::size
	int32_t ___size_13;
	// System.Object System.Net.WebAsyncResult::locker
	RuntimeObject * ___locker_14;
	// System.Boolean System.Net.WebAsyncResult::EndCalled
	bool ___EndCalled_15;
	// System.Boolean System.Net.WebAsyncResult::AsyncWriteAll
	bool ___AsyncWriteAll_16;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___handle_0)); }
	inline ManualResetEvent_t451242010 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_t451242010 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_t451242010 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___cb_3)); }
	inline AsyncCallback_t3962456242 * get_cb_3() const { return ___cb_3; }
	inline AsyncCallback_t3962456242 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(AsyncCallback_t3962456242 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_nbytes_5() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___nbytes_5)); }
	inline int32_t get_nbytes_5() const { return ___nbytes_5; }
	inline int32_t* get_address_of_nbytes_5() { return &___nbytes_5; }
	inline void set_nbytes_5(int32_t value)
	{
		___nbytes_5 = value;
	}

	inline static int32_t get_offset_of_innerAsyncResult_6() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___innerAsyncResult_6)); }
	inline RuntimeObject* get_innerAsyncResult_6() const { return ___innerAsyncResult_6; }
	inline RuntimeObject** get_address_of_innerAsyncResult_6() { return &___innerAsyncResult_6; }
	inline void set_innerAsyncResult_6(RuntimeObject* value)
	{
		___innerAsyncResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___innerAsyncResult_6), value);
	}

	inline static int32_t get_offset_of_callbackDone_7() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___callbackDone_7)); }
	inline bool get_callbackDone_7() const { return ___callbackDone_7; }
	inline bool* get_address_of_callbackDone_7() { return &___callbackDone_7; }
	inline void set_callbackDone_7(bool value)
	{
		___callbackDone_7 = value;
	}

	inline static int32_t get_offset_of_exc_8() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___exc_8)); }
	inline Exception_t * get_exc_8() const { return ___exc_8; }
	inline Exception_t ** get_address_of_exc_8() { return &___exc_8; }
	inline void set_exc_8(Exception_t * value)
	{
		___exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___exc_8), value);
	}

	inline static int32_t get_offset_of_response_9() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___response_9)); }
	inline HttpWebResponse_t3286585418 * get_response_9() const { return ___response_9; }
	inline HttpWebResponse_t3286585418 ** get_address_of_response_9() { return &___response_9; }
	inline void set_response_9(HttpWebResponse_t3286585418 * value)
	{
		___response_9 = value;
		Il2CppCodeGenWriteBarrier((&___response_9), value);
	}

	inline static int32_t get_offset_of_writeStream_10() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___writeStream_10)); }
	inline Stream_t1273022909 * get_writeStream_10() const { return ___writeStream_10; }
	inline Stream_t1273022909 ** get_address_of_writeStream_10() { return &___writeStream_10; }
	inline void set_writeStream_10(Stream_t1273022909 * value)
	{
		___writeStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_10), value);
	}

	inline static int32_t get_offset_of_buffer_11() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___buffer_11)); }
	inline ByteU5BU5D_t4116647657* get_buffer_11() const { return ___buffer_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_11() { return &___buffer_11; }
	inline void set_buffer_11(ByteU5BU5D_t4116647657* value)
	{
		___buffer_11 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_11), value);
	}

	inline static int32_t get_offset_of_offset_12() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___offset_12)); }
	inline int32_t get_offset_12() const { return ___offset_12; }
	inline int32_t* get_address_of_offset_12() { return &___offset_12; }
	inline void set_offset_12(int32_t value)
	{
		___offset_12 = value;
	}

	inline static int32_t get_offset_of_size_13() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___size_13)); }
	inline int32_t get_size_13() const { return ___size_13; }
	inline int32_t* get_address_of_size_13() { return &___size_13; }
	inline void set_size_13(int32_t value)
	{
		___size_13 = value;
	}

	inline static int32_t get_offset_of_locker_14() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___locker_14)); }
	inline RuntimeObject * get_locker_14() const { return ___locker_14; }
	inline RuntimeObject ** get_address_of_locker_14() { return &___locker_14; }
	inline void set_locker_14(RuntimeObject * value)
	{
		___locker_14 = value;
		Il2CppCodeGenWriteBarrier((&___locker_14), value);
	}

	inline static int32_t get_offset_of_EndCalled_15() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___EndCalled_15)); }
	inline bool get_EndCalled_15() const { return ___EndCalled_15; }
	inline bool* get_address_of_EndCalled_15() { return &___EndCalled_15; }
	inline void set_EndCalled_15(bool value)
	{
		___EndCalled_15 = value;
	}

	inline static int32_t get_offset_of_AsyncWriteAll_16() { return static_cast<int32_t>(offsetof(WebAsyncResult_t3421962937, ___AsyncWriteAll_16)); }
	inline bool get_AsyncWriteAll_16() const { return ___AsyncWriteAll_16; }
	inline bool* get_address_of_AsyncWriteAll_16() { return &___AsyncWriteAll_16; }
	inline void set_AsyncWriteAll_16(bool value)
	{
		___AsyncWriteAll_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBASYNCRESULT_T3421962937_H
#ifndef FTPASYNCRESULT_T3265664217_H
#define FTPASYNCRESULT_T3265664217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpAsyncResult
struct  FtpAsyncResult_t3265664217  : public RuntimeObject
{
public:
	// System.Net.FtpWebResponse System.Net.FtpAsyncResult::response
	FtpWebResponse_t3940763575 * ___response_0;
	// System.Threading.ManualResetEvent System.Net.FtpAsyncResult::waitHandle
	ManualResetEvent_t451242010 * ___waitHandle_1;
	// System.Exception System.Net.FtpAsyncResult::exception
	Exception_t * ___exception_2;
	// System.AsyncCallback System.Net.FtpAsyncResult::callback
	AsyncCallback_t3962456242 * ___callback_3;
	// System.IO.Stream System.Net.FtpAsyncResult::stream
	Stream_t1273022909 * ___stream_4;
	// System.Object System.Net.FtpAsyncResult::state
	RuntimeObject * ___state_5;
	// System.Boolean System.Net.FtpAsyncResult::completed
	bool ___completed_6;
	// System.Boolean System.Net.FtpAsyncResult::synch
	bool ___synch_7;
	// System.Object System.Net.FtpAsyncResult::locker
	RuntimeObject * ___locker_8;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___response_0)); }
	inline FtpWebResponse_t3940763575 * get_response_0() const { return ___response_0; }
	inline FtpWebResponse_t3940763575 ** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(FtpWebResponse_t3940763575 * value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_waitHandle_1() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___waitHandle_1)); }
	inline ManualResetEvent_t451242010 * get_waitHandle_1() const { return ___waitHandle_1; }
	inline ManualResetEvent_t451242010 ** get_address_of_waitHandle_1() { return &___waitHandle_1; }
	inline void set_waitHandle_1(ManualResetEvent_t451242010 * value)
	{
		___waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&___waitHandle_1), value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___exception_2)); }
	inline Exception_t * get_exception_2() const { return ___exception_2; }
	inline Exception_t ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier((&___exception_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___callback_3)); }
	inline AsyncCallback_t3962456242 * get_callback_3() const { return ___callback_3; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(AsyncCallback_t3962456242 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___stream_4)); }
	inline Stream_t1273022909 * get_stream_4() const { return ___stream_4; }
	inline Stream_t1273022909 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_t1273022909 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___state_5)); }
	inline RuntimeObject * get_state_5() const { return ___state_5; }
	inline RuntimeObject ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(RuntimeObject * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}

	inline static int32_t get_offset_of_synch_7() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___synch_7)); }
	inline bool get_synch_7() const { return ___synch_7; }
	inline bool* get_address_of_synch_7() { return &___synch_7; }
	inline void set_synch_7(bool value)
	{
		___synch_7 = value;
	}

	inline static int32_t get_offset_of_locker_8() { return static_cast<int32_t>(offsetof(FtpAsyncResult_t3265664217, ___locker_8)); }
	inline RuntimeObject * get_locker_8() const { return ___locker_8; }
	inline RuntimeObject ** get_address_of_locker_8() { return &___locker_8; }
	inline void set_locker_8(RuntimeObject * value)
	{
		___locker_8 = value;
		Il2CppCodeGenWriteBarrier((&___locker_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPASYNCRESULT_T3265664217_H
#ifndef DEFAULTCERTIFICATEPOLICY_T3607119947_H
#define DEFAULTCERTIFICATEPOLICY_T3607119947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DefaultCertificatePolicy
struct  DefaultCertificatePolicy_t3607119947  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCERTIFICATEPOLICY_T3607119947_H
#ifndef ENDPOINTMANAGER_T1428684201_H
#define ENDPOINTMANAGER_T1428684201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointManager
struct  EndPointManager_t1428684201  : public RuntimeObject
{
public:

public:
};

struct EndPointManager_t1428684201_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.EndPointManager::ip_to_endpoints
	Hashtable_t1853889766 * ___ip_to_endpoints_0;

public:
	inline static int32_t get_offset_of_ip_to_endpoints_0() { return static_cast<int32_t>(offsetof(EndPointManager_t1428684201_StaticFields, ___ip_to_endpoints_0)); }
	inline Hashtable_t1853889766 * get_ip_to_endpoints_0() const { return ___ip_to_endpoints_0; }
	inline Hashtable_t1853889766 ** get_address_of_ip_to_endpoints_0() { return &___ip_to_endpoints_0; }
	inline void set_ip_to_endpoints_0(Hashtable_t1853889766 * value)
	{
		___ip_to_endpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___ip_to_endpoints_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTMANAGER_T1428684201_H
#ifndef DIGESTHEADERPARSER_T341650829_H
#define DIGESTHEADERPARSER_T341650829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestHeaderParser
struct  DigestHeaderParser_t341650829  : public RuntimeObject
{
public:
	// System.String System.Net.DigestHeaderParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.DigestHeaderParser::length
	int32_t ___length_1;
	// System.Int32 System.Net.DigestHeaderParser::pos
	int32_t ___pos_2;
	// System.String[] System.Net.DigestHeaderParser::values
	StringU5BU5D_t1281789340* ___values_4;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t341650829, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t341650829, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t341650829, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t341650829, ___values_4)); }
	inline StringU5BU5D_t1281789340* get_values_4() const { return ___values_4; }
	inline StringU5BU5D_t1281789340** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StringU5BU5D_t1281789340* value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier((&___values_4), value);
	}
};

struct DigestHeaderParser_t341650829_StaticFields
{
public:
	// System.String[] System.Net.DigestHeaderParser::keywords
	StringU5BU5D_t1281789340* ___keywords_3;

public:
	inline static int32_t get_offset_of_keywords_3() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t341650829_StaticFields, ___keywords_3)); }
	inline StringU5BU5D_t1281789340* get_keywords_3() const { return ___keywords_3; }
	inline StringU5BU5D_t1281789340** get_address_of_keywords_3() { return &___keywords_3; }
	inline void set_keywords_3(StringU5BU5D_t1281789340* value)
	{
		___keywords_3 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTHEADERPARSER_T341650829_H
#ifndef DIGESTCLIENT_T660790528_H
#define DIGESTCLIENT_T660790528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestClient
struct  DigestClient_t660790528  : public RuntimeObject
{
public:

public:
};

struct DigestClient_t660790528_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.DigestClient::cache
	Hashtable_t1853889766 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(DigestClient_t660790528_StaticFields, ___cache_0)); }
	inline Hashtable_t1853889766 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t1853889766 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t1853889766 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTCLIENT_T660790528_H
#ifndef DNS_T384099571_H
#define DNS_T384099571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns
struct  Dns_t384099571  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNS_T384099571_H
#ifndef ENDPOINT_T982345378_H
#define ENDPOINT_T982345378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t982345378  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T982345378_H
#ifndef FILEWEBREQUESTCREATOR_T1781329382_H
#define FILEWEBREQUESTCREATOR_T1781329382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequestCreator
struct  FileWebRequestCreator_t1781329382  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUESTCREATOR_T1781329382_H
#ifndef CREDENTIALCACHEFORHOSTKEY_T3132413229_H
#define CREDENTIALCACHEFORHOSTKEY_T3132413229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialCache/CredentialCacheForHostKey
struct  CredentialCacheForHostKey_t3132413229  : public RuntimeObject
{
public:
	// System.String System.Net.CredentialCache/CredentialCacheForHostKey::host
	String_t* ___host_0;
	// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::port
	int32_t ___port_1;
	// System.String System.Net.CredentialCache/CredentialCacheForHostKey::authType
	String_t* ___authType_2;
	// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(CredentialCacheForHostKey_t3132413229, ___host_0)); }
	inline String_t* get_host_0() const { return ___host_0; }
	inline String_t** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(String_t* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(CredentialCacheForHostKey_t3132413229, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}

	inline static int32_t get_offset_of_authType_2() { return static_cast<int32_t>(offsetof(CredentialCacheForHostKey_t3132413229, ___authType_2)); }
	inline String_t* get_authType_2() const { return ___authType_2; }
	inline String_t** get_address_of_authType_2() { return &___authType_2; }
	inline void set_authType_2(String_t* value)
	{
		___authType_2 = value;
		Il2CppCodeGenWriteBarrier((&___authType_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(CredentialCacheForHostKey_t3132413229, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALCACHEFORHOSTKEY_T3132413229_H
#ifndef FTPREQUESTCREATOR_T2926281497_H
#define FTPREQUESTCREATOR_T2926281497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpRequestCreator
struct  FtpRequestCreator_t2926281497  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPREQUESTCREATOR_T2926281497_H
#ifndef ENDPOINTLISTENER_T2984434924_H
#define ENDPOINTLISTENER_T2984434924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPointListener
struct  EndPointListener_t2984434924  : public RuntimeObject
{
public:
	// System.Net.IPEndPoint System.Net.EndPointListener::endpoint
	IPEndPoint_t3791887218 * ___endpoint_0;
	// System.Net.Sockets.Socket System.Net.EndPointListener::sock
	Socket_t1119025450 * ___sock_1;
	// System.Collections.Hashtable System.Net.EndPointListener::prefixes
	Hashtable_t1853889766 * ___prefixes_2;
	// System.Collections.ArrayList System.Net.EndPointListener::unhandled
	ArrayList_t2718874744 * ___unhandled_3;
	// System.Collections.ArrayList System.Net.EndPointListener::all
	ArrayList_t2718874744 * ___all_4;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Net.EndPointListener::cert
	X509Certificate2_t714049126 * ___cert_5;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Net.EndPointListener::key
	AsymmetricAlgorithm_t932037087 * ___key_6;
	// System.Boolean System.Net.EndPointListener::secure
	bool ___secure_7;

public:
	inline static int32_t get_offset_of_endpoint_0() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___endpoint_0)); }
	inline IPEndPoint_t3791887218 * get_endpoint_0() const { return ___endpoint_0; }
	inline IPEndPoint_t3791887218 ** get_address_of_endpoint_0() { return &___endpoint_0; }
	inline void set_endpoint_0(IPEndPoint_t3791887218 * value)
	{
		___endpoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___endpoint_0), value);
	}

	inline static int32_t get_offset_of_sock_1() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___sock_1)); }
	inline Socket_t1119025450 * get_sock_1() const { return ___sock_1; }
	inline Socket_t1119025450 ** get_address_of_sock_1() { return &___sock_1; }
	inline void set_sock_1(Socket_t1119025450 * value)
	{
		___sock_1 = value;
		Il2CppCodeGenWriteBarrier((&___sock_1), value);
	}

	inline static int32_t get_offset_of_prefixes_2() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___prefixes_2)); }
	inline Hashtable_t1853889766 * get_prefixes_2() const { return ___prefixes_2; }
	inline Hashtable_t1853889766 ** get_address_of_prefixes_2() { return &___prefixes_2; }
	inline void set_prefixes_2(Hashtable_t1853889766 * value)
	{
		___prefixes_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_2), value);
	}

	inline static int32_t get_offset_of_unhandled_3() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___unhandled_3)); }
	inline ArrayList_t2718874744 * get_unhandled_3() const { return ___unhandled_3; }
	inline ArrayList_t2718874744 ** get_address_of_unhandled_3() { return &___unhandled_3; }
	inline void set_unhandled_3(ArrayList_t2718874744 * value)
	{
		___unhandled_3 = value;
		Il2CppCodeGenWriteBarrier((&___unhandled_3), value);
	}

	inline static int32_t get_offset_of_all_4() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___all_4)); }
	inline ArrayList_t2718874744 * get_all_4() const { return ___all_4; }
	inline ArrayList_t2718874744 ** get_address_of_all_4() { return &___all_4; }
	inline void set_all_4(ArrayList_t2718874744 * value)
	{
		___all_4 = value;
		Il2CppCodeGenWriteBarrier((&___all_4), value);
	}

	inline static int32_t get_offset_of_cert_5() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___cert_5)); }
	inline X509Certificate2_t714049126 * get_cert_5() const { return ___cert_5; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_5() { return &___cert_5; }
	inline void set_cert_5(X509Certificate2_t714049126 * value)
	{
		___cert_5 = value;
		Il2CppCodeGenWriteBarrier((&___cert_5), value);
	}

	inline static int32_t get_offset_of_key_6() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___key_6)); }
	inline AsymmetricAlgorithm_t932037087 * get_key_6() const { return ___key_6; }
	inline AsymmetricAlgorithm_t932037087 ** get_address_of_key_6() { return &___key_6; }
	inline void set_key_6(AsymmetricAlgorithm_t932037087 * value)
	{
		___key_6 = value;
		Il2CppCodeGenWriteBarrier((&___key_6), value);
	}

	inline static int32_t get_offset_of_secure_7() { return static_cast<int32_t>(offsetof(EndPointListener_t2984434924, ___secure_7)); }
	inline bool get_secure_7() const { return ___secure_7; }
	inline bool* get_address_of_secure_7() { return &___secure_7; }
	inline void set_secure_7(bool value)
	{
		___secure_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTLISTENER_T2984434924_H
#ifndef WEBCONNECTIONSTREAM_T2170064850_H
#define WEBCONNECTIONSTREAM_T2170064850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream
struct  WebConnectionStream_t2170064850  : public Stream_t1273022909
{
public:
	// System.Boolean System.Net.WebConnectionStream::isRead
	bool ___isRead_2;
	// System.Net.WebConnection System.Net.WebConnectionStream::cnc
	WebConnection_t3982808322 * ___cnc_3;
	// System.Net.HttpWebRequest System.Net.WebConnectionStream::request
	HttpWebRequest_t1669436515 * ___request_4;
	// System.Byte[] System.Net.WebConnectionStream::readBuffer
	ByteU5BU5D_t4116647657* ___readBuffer_5;
	// System.Int32 System.Net.WebConnectionStream::readBufferOffset
	int32_t ___readBufferOffset_6;
	// System.Int32 System.Net.WebConnectionStream::readBufferSize
	int32_t ___readBufferSize_7;
	// System.Int32 System.Net.WebConnectionStream::contentLength
	int32_t ___contentLength_8;
	// System.Int32 System.Net.WebConnectionStream::totalRead
	int32_t ___totalRead_9;
	// System.Int64 System.Net.WebConnectionStream::totalWritten
	int64_t ___totalWritten_10;
	// System.Boolean System.Net.WebConnectionStream::nextReadCalled
	bool ___nextReadCalled_11;
	// System.Int32 System.Net.WebConnectionStream::pendingReads
	int32_t ___pendingReads_12;
	// System.Int32 System.Net.WebConnectionStream::pendingWrites
	int32_t ___pendingWrites_13;
	// System.Threading.ManualResetEvent System.Net.WebConnectionStream::pending
	ManualResetEvent_t451242010 * ___pending_14;
	// System.Boolean System.Net.WebConnectionStream::allowBuffering
	bool ___allowBuffering_15;
	// System.Boolean System.Net.WebConnectionStream::sendChunked
	bool ___sendChunked_16;
	// System.IO.MemoryStream System.Net.WebConnectionStream::writeBuffer
	MemoryStream_t94973147 * ___writeBuffer_17;
	// System.Boolean System.Net.WebConnectionStream::requestWritten
	bool ___requestWritten_18;
	// System.Byte[] System.Net.WebConnectionStream::headers
	ByteU5BU5D_t4116647657* ___headers_19;
	// System.Boolean System.Net.WebConnectionStream::disposed
	bool ___disposed_20;
	// System.Boolean System.Net.WebConnectionStream::headersSent
	bool ___headersSent_21;
	// System.Object System.Net.WebConnectionStream::locker
	RuntimeObject * ___locker_22;
	// System.Boolean System.Net.WebConnectionStream::initRead
	bool ___initRead_23;
	// System.Boolean System.Net.WebConnectionStream::read_eof
	bool ___read_eof_24;
	// System.Boolean System.Net.WebConnectionStream::complete_request_written
	bool ___complete_request_written_25;
	// System.Int32 System.Net.WebConnectionStream::read_timeout
	int32_t ___read_timeout_26;
	// System.Int32 System.Net.WebConnectionStream::write_timeout
	int32_t ___write_timeout_27;

public:
	inline static int32_t get_offset_of_isRead_2() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___isRead_2)); }
	inline bool get_isRead_2() const { return ___isRead_2; }
	inline bool* get_address_of_isRead_2() { return &___isRead_2; }
	inline void set_isRead_2(bool value)
	{
		___isRead_2 = value;
	}

	inline static int32_t get_offset_of_cnc_3() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___cnc_3)); }
	inline WebConnection_t3982808322 * get_cnc_3() const { return ___cnc_3; }
	inline WebConnection_t3982808322 ** get_address_of_cnc_3() { return &___cnc_3; }
	inline void set_cnc_3(WebConnection_t3982808322 * value)
	{
		___cnc_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_3), value);
	}

	inline static int32_t get_offset_of_request_4() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___request_4)); }
	inline HttpWebRequest_t1669436515 * get_request_4() const { return ___request_4; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_4() { return &___request_4; }
	inline void set_request_4(HttpWebRequest_t1669436515 * value)
	{
		___request_4 = value;
		Il2CppCodeGenWriteBarrier((&___request_4), value);
	}

	inline static int32_t get_offset_of_readBuffer_5() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___readBuffer_5)); }
	inline ByteU5BU5D_t4116647657* get_readBuffer_5() const { return ___readBuffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_readBuffer_5() { return &___readBuffer_5; }
	inline void set_readBuffer_5(ByteU5BU5D_t4116647657* value)
	{
		___readBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_5), value);
	}

	inline static int32_t get_offset_of_readBufferOffset_6() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___readBufferOffset_6)); }
	inline int32_t get_readBufferOffset_6() const { return ___readBufferOffset_6; }
	inline int32_t* get_address_of_readBufferOffset_6() { return &___readBufferOffset_6; }
	inline void set_readBufferOffset_6(int32_t value)
	{
		___readBufferOffset_6 = value;
	}

	inline static int32_t get_offset_of_readBufferSize_7() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___readBufferSize_7)); }
	inline int32_t get_readBufferSize_7() const { return ___readBufferSize_7; }
	inline int32_t* get_address_of_readBufferSize_7() { return &___readBufferSize_7; }
	inline void set_readBufferSize_7(int32_t value)
	{
		___readBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_contentLength_8() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___contentLength_8)); }
	inline int32_t get_contentLength_8() const { return ___contentLength_8; }
	inline int32_t* get_address_of_contentLength_8() { return &___contentLength_8; }
	inline void set_contentLength_8(int32_t value)
	{
		___contentLength_8 = value;
	}

	inline static int32_t get_offset_of_totalRead_9() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___totalRead_9)); }
	inline int32_t get_totalRead_9() const { return ___totalRead_9; }
	inline int32_t* get_address_of_totalRead_9() { return &___totalRead_9; }
	inline void set_totalRead_9(int32_t value)
	{
		___totalRead_9 = value;
	}

	inline static int32_t get_offset_of_totalWritten_10() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___totalWritten_10)); }
	inline int64_t get_totalWritten_10() const { return ___totalWritten_10; }
	inline int64_t* get_address_of_totalWritten_10() { return &___totalWritten_10; }
	inline void set_totalWritten_10(int64_t value)
	{
		___totalWritten_10 = value;
	}

	inline static int32_t get_offset_of_nextReadCalled_11() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___nextReadCalled_11)); }
	inline bool get_nextReadCalled_11() const { return ___nextReadCalled_11; }
	inline bool* get_address_of_nextReadCalled_11() { return &___nextReadCalled_11; }
	inline void set_nextReadCalled_11(bool value)
	{
		___nextReadCalled_11 = value;
	}

	inline static int32_t get_offset_of_pendingReads_12() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___pendingReads_12)); }
	inline int32_t get_pendingReads_12() const { return ___pendingReads_12; }
	inline int32_t* get_address_of_pendingReads_12() { return &___pendingReads_12; }
	inline void set_pendingReads_12(int32_t value)
	{
		___pendingReads_12 = value;
	}

	inline static int32_t get_offset_of_pendingWrites_13() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___pendingWrites_13)); }
	inline int32_t get_pendingWrites_13() const { return ___pendingWrites_13; }
	inline int32_t* get_address_of_pendingWrites_13() { return &___pendingWrites_13; }
	inline void set_pendingWrites_13(int32_t value)
	{
		___pendingWrites_13 = value;
	}

	inline static int32_t get_offset_of_pending_14() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___pending_14)); }
	inline ManualResetEvent_t451242010 * get_pending_14() const { return ___pending_14; }
	inline ManualResetEvent_t451242010 ** get_address_of_pending_14() { return &___pending_14; }
	inline void set_pending_14(ManualResetEvent_t451242010 * value)
	{
		___pending_14 = value;
		Il2CppCodeGenWriteBarrier((&___pending_14), value);
	}

	inline static int32_t get_offset_of_allowBuffering_15() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___allowBuffering_15)); }
	inline bool get_allowBuffering_15() const { return ___allowBuffering_15; }
	inline bool* get_address_of_allowBuffering_15() { return &___allowBuffering_15; }
	inline void set_allowBuffering_15(bool value)
	{
		___allowBuffering_15 = value;
	}

	inline static int32_t get_offset_of_sendChunked_16() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___sendChunked_16)); }
	inline bool get_sendChunked_16() const { return ___sendChunked_16; }
	inline bool* get_address_of_sendChunked_16() { return &___sendChunked_16; }
	inline void set_sendChunked_16(bool value)
	{
		___sendChunked_16 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_17() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___writeBuffer_17)); }
	inline MemoryStream_t94973147 * get_writeBuffer_17() const { return ___writeBuffer_17; }
	inline MemoryStream_t94973147 ** get_address_of_writeBuffer_17() { return &___writeBuffer_17; }
	inline void set_writeBuffer_17(MemoryStream_t94973147 * value)
	{
		___writeBuffer_17 = value;
		Il2CppCodeGenWriteBarrier((&___writeBuffer_17), value);
	}

	inline static int32_t get_offset_of_requestWritten_18() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___requestWritten_18)); }
	inline bool get_requestWritten_18() const { return ___requestWritten_18; }
	inline bool* get_address_of_requestWritten_18() { return &___requestWritten_18; }
	inline void set_requestWritten_18(bool value)
	{
		___requestWritten_18 = value;
	}

	inline static int32_t get_offset_of_headers_19() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___headers_19)); }
	inline ByteU5BU5D_t4116647657* get_headers_19() const { return ___headers_19; }
	inline ByteU5BU5D_t4116647657** get_address_of_headers_19() { return &___headers_19; }
	inline void set_headers_19(ByteU5BU5D_t4116647657* value)
	{
		___headers_19 = value;
		Il2CppCodeGenWriteBarrier((&___headers_19), value);
	}

	inline static int32_t get_offset_of_disposed_20() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___disposed_20)); }
	inline bool get_disposed_20() const { return ___disposed_20; }
	inline bool* get_address_of_disposed_20() { return &___disposed_20; }
	inline void set_disposed_20(bool value)
	{
		___disposed_20 = value;
	}

	inline static int32_t get_offset_of_headersSent_21() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___headersSent_21)); }
	inline bool get_headersSent_21() const { return ___headersSent_21; }
	inline bool* get_address_of_headersSent_21() { return &___headersSent_21; }
	inline void set_headersSent_21(bool value)
	{
		___headersSent_21 = value;
	}

	inline static int32_t get_offset_of_locker_22() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___locker_22)); }
	inline RuntimeObject * get_locker_22() const { return ___locker_22; }
	inline RuntimeObject ** get_address_of_locker_22() { return &___locker_22; }
	inline void set_locker_22(RuntimeObject * value)
	{
		___locker_22 = value;
		Il2CppCodeGenWriteBarrier((&___locker_22), value);
	}

	inline static int32_t get_offset_of_initRead_23() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___initRead_23)); }
	inline bool get_initRead_23() const { return ___initRead_23; }
	inline bool* get_address_of_initRead_23() { return &___initRead_23; }
	inline void set_initRead_23(bool value)
	{
		___initRead_23 = value;
	}

	inline static int32_t get_offset_of_read_eof_24() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___read_eof_24)); }
	inline bool get_read_eof_24() const { return ___read_eof_24; }
	inline bool* get_address_of_read_eof_24() { return &___read_eof_24; }
	inline void set_read_eof_24(bool value)
	{
		___read_eof_24 = value;
	}

	inline static int32_t get_offset_of_complete_request_written_25() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___complete_request_written_25)); }
	inline bool get_complete_request_written_25() const { return ___complete_request_written_25; }
	inline bool* get_address_of_complete_request_written_25() { return &___complete_request_written_25; }
	inline void set_complete_request_written_25(bool value)
	{
		___complete_request_written_25 = value;
	}

	inline static int32_t get_offset_of_read_timeout_26() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___read_timeout_26)); }
	inline int32_t get_read_timeout_26() const { return ___read_timeout_26; }
	inline int32_t* get_address_of_read_timeout_26() { return &___read_timeout_26; }
	inline void set_read_timeout_26(int32_t value)
	{
		___read_timeout_26 = value;
	}

	inline static int32_t get_offset_of_write_timeout_27() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850, ___write_timeout_27)); }
	inline int32_t get_write_timeout_27() const { return ___write_timeout_27; }
	inline int32_t* get_address_of_write_timeout_27() { return &___write_timeout_27; }
	inline void set_write_timeout_27(int32_t value)
	{
		___write_timeout_27 = value;
	}
};

struct WebConnectionStream_t2170064850_StaticFields
{
public:
	// System.Byte[] System.Net.WebConnectionStream::crlf
	ByteU5BU5D_t4116647657* ___crlf_1;

public:
	inline static int32_t get_offset_of_crlf_1() { return static_cast<int32_t>(offsetof(WebConnectionStream_t2170064850_StaticFields, ___crlf_1)); }
	inline ByteU5BU5D_t4116647657* get_crlf_1() const { return ___crlf_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_crlf_1() { return &___crlf_1; }
	inline void set_crlf_1(ByteU5BU5D_t4116647657* value)
	{
		___crlf_1 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONSTREAM_T2170064850_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef PROGRESSCHANGEDEVENTARGS_T3227452477_H
#define PROGRESSCHANGEDEVENTARGS_T3227452477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProgressChangedEventArgs
struct  ProgressChangedEventArgs_t3227452477  : public EventArgs_t3591816995
{
public:
	// System.Int32 System.ComponentModel.ProgressChangedEventArgs::progress
	int32_t ___progress_1;
	// System.Object System.ComponentModel.ProgressChangedEventArgs::state
	RuntimeObject * ___state_2;

public:
	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_t3227452477, ___progress_1)); }
	inline int32_t get_progress_1() const { return ___progress_1; }
	inline int32_t* get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(int32_t value)
	{
		___progress_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_t3227452477, ___state_2)); }
	inline RuntimeObject * get_state_2() const { return ___state_2; }
	inline RuntimeObject ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(RuntimeObject * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCHANGEDEVENTARGS_T3227452477_H
#ifndef ASYNCCOMPLETEDEVENTARGS_T1863481821_H
#define ASYNCCOMPLETEDEVENTARGS_T1863481821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncCompletedEventArgs
struct  AsyncCompletedEventArgs_t1863481821  : public EventArgs_t3591816995
{
public:
	// System.Exception System.ComponentModel.AsyncCompletedEventArgs::_error
	Exception_t * ____error_1;
	// System.Boolean System.ComponentModel.AsyncCompletedEventArgs::_cancelled
	bool ____cancelled_2;
	// System.Object System.ComponentModel.AsyncCompletedEventArgs::_userState
	RuntimeObject * ____userState_3;

public:
	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_t1863481821, ____error_1)); }
	inline Exception_t * get__error_1() const { return ____error_1; }
	inline Exception_t ** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(Exception_t * value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier((&____error_1), value);
	}

	inline static int32_t get_offset_of__cancelled_2() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_t1863481821, ____cancelled_2)); }
	inline bool get__cancelled_2() const { return ____cancelled_2; }
	inline bool* get_address_of__cancelled_2() { return &____cancelled_2; }
	inline void set__cancelled_2(bool value)
	{
		____cancelled_2 = value;
	}

	inline static int32_t get_offset_of__userState_3() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_t1863481821, ____userState_3)); }
	inline RuntimeObject * get__userState_3() const { return ____userState_3; }
	inline RuntimeObject ** get_address_of__userState_3() { return &____userState_3; }
	inline void set__userState_3(RuntimeObject * value)
	{
		____userState_3 = value;
		Il2CppCodeGenWriteBarrier((&____userState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOMPLETEDEVENTARGS_T1863481821_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef IPENDPOINT_T3791887218_H
#define IPENDPOINT_T3791887218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t3791887218  : public EndPoint_t982345378
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::address
	IPAddress_t241777590 * ___address_2;
	// System.Int32 System.Net.IPEndPoint::port
	int32_t ___port_3;

public:
	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t3791887218, ___address_2)); }
	inline IPAddress_t241777590 * get_address_2() const { return ___address_2; }
	inline IPAddress_t241777590 ** get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(IPAddress_t241777590 * value)
	{
		___address_2 = value;
		Il2CppCodeGenWriteBarrier((&___address_2), value);
	}

	inline static int32_t get_offset_of_port_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t3791887218, ___port_3)); }
	inline int32_t get_port_3() const { return ___port_3; }
	inline int32_t* get_address_of_port_3() { return &___port_3; }
	inline void set_port_3(int32_t value)
	{
		___port_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_T3791887218_H
#ifndef WEBRESPONSE_T229922639_H
#define WEBRESPONSE_T229922639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t229922639  : public MarshalByRefObject_t2760389100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T229922639_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef RESPONSESTREAM_T3810703494_H
#define RESPONSESTREAM_T3810703494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ResponseStream
struct  ResponseStream_t3810703494  : public Stream_t1273022909
{
public:
	// System.Net.HttpListenerResponse System.Net.ResponseStream::response
	HttpListenerResponse_t3502667045 * ___response_1;
	// System.Boolean System.Net.ResponseStream::ignore_errors
	bool ___ignore_errors_2;
	// System.Boolean System.Net.ResponseStream::disposed
	bool ___disposed_3;
	// System.Boolean System.Net.ResponseStream::trailer_sent
	bool ___trailer_sent_4;
	// System.IO.Stream System.Net.ResponseStream::stream
	Stream_t1273022909 * ___stream_5;

public:
	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494, ___response_1)); }
	inline HttpListenerResponse_t3502667045 * get_response_1() const { return ___response_1; }
	inline HttpListenerResponse_t3502667045 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(HttpListenerResponse_t3502667045 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((&___response_1), value);
	}

	inline static int32_t get_offset_of_ignore_errors_2() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494, ___ignore_errors_2)); }
	inline bool get_ignore_errors_2() const { return ___ignore_errors_2; }
	inline bool* get_address_of_ignore_errors_2() { return &___ignore_errors_2; }
	inline void set_ignore_errors_2(bool value)
	{
		___ignore_errors_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_trailer_sent_4() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494, ___trailer_sent_4)); }
	inline bool get_trailer_sent_4() const { return ___trailer_sent_4; }
	inline bool* get_address_of_trailer_sent_4() { return &___trailer_sent_4; }
	inline void set_trailer_sent_4(bool value)
	{
		___trailer_sent_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494, ___stream_5)); }
	inline Stream_t1273022909 * get_stream_5() const { return ___stream_5; }
	inline Stream_t1273022909 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t1273022909 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}
};

struct ResponseStream_t3810703494_StaticFields
{
public:
	// System.Byte[] System.Net.ResponseStream::crlf
	ByteU5BU5D_t4116647657* ___crlf_6;

public:
	inline static int32_t get_offset_of_crlf_6() { return static_cast<int32_t>(offsetof(ResponseStream_t3810703494_StaticFields, ___crlf_6)); }
	inline ByteU5BU5D_t4116647657* get_crlf_6() const { return ___crlf_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_crlf_6() { return &___crlf_6; }
	inline void set_crlf_6(ByteU5BU5D_t4116647657* value)
	{
		___crlf_6 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTREAM_T3810703494_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef HTTPLISTENERBASICIDENTITY_T3019963659_H
#define HTTPLISTENERBASICIDENTITY_T3019963659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerBasicIdentity
struct  HttpListenerBasicIdentity_t3019963659  : public GenericIdentity_t2319019448
{
public:
	// System.String System.Net.HttpListenerBasicIdentity::password
	String_t* ___password_2;

public:
	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(HttpListenerBasicIdentity_t3019963659, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier((&___password_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERBASICIDENTITY_T3019963659_H
#ifndef REQUESTSTREAM_T762880582_H
#define REQUESTSTREAM_T762880582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.RequestStream
struct  RequestStream_t762880582  : public Stream_t1273022909
{
public:
	// System.Byte[] System.Net.RequestStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_1;
	// System.Int32 System.Net.RequestStream::offset
	int32_t ___offset_2;
	// System.Int32 System.Net.RequestStream::length
	int32_t ___length_3;
	// System.Int64 System.Net.RequestStream::remaining_body
	int64_t ___remaining_body_4;
	// System.Boolean System.Net.RequestStream::disposed
	bool ___disposed_5;
	// System.IO.Stream System.Net.RequestStream::stream
	Stream_t1273022909 * ___stream_6;

public:
	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_buffer_1() const { return ___buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_1), value);
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___offset_2)); }
	inline int32_t get_offset_2() const { return ___offset_2; }
	inline int32_t* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(int32_t value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___length_3)); }
	inline int32_t get_length_3() const { return ___length_3; }
	inline int32_t* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(int32_t value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_remaining_body_4() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___remaining_body_4)); }
	inline int64_t get_remaining_body_4() const { return ___remaining_body_4; }
	inline int64_t* get_address_of_remaining_body_4() { return &___remaining_body_4; }
	inline void set_remaining_body_4(int64_t value)
	{
		___remaining_body_4 = value;
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}

	inline static int32_t get_offset_of_stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___stream_6)); }
	inline Stream_t1273022909 * get_stream_6() const { return ___stream_6; }
	inline Stream_t1273022909 ** get_address_of_stream_6() { return &___stream_6; }
	inline void set_stream_6(Stream_t1273022909 * value)
	{
		___stream_6 = value;
		Il2CppCodeGenWriteBarrier((&___stream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T762880582_H
#ifndef FTPDATASTREAM_T1366729715_H
#define FTPDATASTREAM_T1366729715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream
struct  FtpDataStream_t1366729715  : public Stream_t1273022909
{
public:
	// System.Net.FtpWebRequest System.Net.FtpDataStream::request
	FtpWebRequest_t1577818305 * ___request_1;
	// System.IO.Stream System.Net.FtpDataStream::networkStream
	Stream_t1273022909 * ___networkStream_2;
	// System.Boolean System.Net.FtpDataStream::disposed
	bool ___disposed_3;
	// System.Boolean System.Net.FtpDataStream::isRead
	bool ___isRead_4;
	// System.Int32 System.Net.FtpDataStream::totalRead
	int32_t ___totalRead_5;

public:
	inline static int32_t get_offset_of_request_1() { return static_cast<int32_t>(offsetof(FtpDataStream_t1366729715, ___request_1)); }
	inline FtpWebRequest_t1577818305 * get_request_1() const { return ___request_1; }
	inline FtpWebRequest_t1577818305 ** get_address_of_request_1() { return &___request_1; }
	inline void set_request_1(FtpWebRequest_t1577818305 * value)
	{
		___request_1 = value;
		Il2CppCodeGenWriteBarrier((&___request_1), value);
	}

	inline static int32_t get_offset_of_networkStream_2() { return static_cast<int32_t>(offsetof(FtpDataStream_t1366729715, ___networkStream_2)); }
	inline Stream_t1273022909 * get_networkStream_2() const { return ___networkStream_2; }
	inline Stream_t1273022909 ** get_address_of_networkStream_2() { return &___networkStream_2; }
	inline void set_networkStream_2(Stream_t1273022909 * value)
	{
		___networkStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_2), value);
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(FtpDataStream_t1366729715, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_isRead_4() { return static_cast<int32_t>(offsetof(FtpDataStream_t1366729715, ___isRead_4)); }
	inline bool get_isRead_4() const { return ___isRead_4; }
	inline bool* get_address_of_isRead_4() { return &___isRead_4; }
	inline void set_isRead_4(bool value)
	{
		___isRead_4 = value;
	}

	inline static int32_t get_offset_of_totalRead_5() { return static_cast<int32_t>(offsetof(FtpDataStream_t1366729715, ___totalRead_5)); }
	inline int32_t get_totalRead_5() const { return ___totalRead_5; }
	inline int32_t* get_address_of_totalRead_5() { return &___totalRead_5; }
	inline void set_totalRead_5(int32_t value)
	{
		___totalRead_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPDATASTREAM_T1366729715_H
#ifndef COMPONENT_T3620823400_H
#define COMPONENT_T3620823400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t3620823400  : public MarshalByRefObject_t2760389100
{
public:
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::event_handlers
	EventHandlerList_t1108123056 * ___event_handlers_1;
	// System.ComponentModel.ISite System.ComponentModel.Component::mySite
	RuntimeObject* ___mySite_2;
	// System.Object System.ComponentModel.Component::disposedEvent
	RuntimeObject * ___disposedEvent_3;

public:
	inline static int32_t get_offset_of_event_handlers_1() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___event_handlers_1)); }
	inline EventHandlerList_t1108123056 * get_event_handlers_1() const { return ___event_handlers_1; }
	inline EventHandlerList_t1108123056 ** get_address_of_event_handlers_1() { return &___event_handlers_1; }
	inline void set_event_handlers_1(EventHandlerList_t1108123056 * value)
	{
		___event_handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___event_handlers_1), value);
	}

	inline static int32_t get_offset_of_mySite_2() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___mySite_2)); }
	inline RuntimeObject* get_mySite_2() const { return ___mySite_2; }
	inline RuntimeObject** get_address_of_mySite_2() { return &___mySite_2; }
	inline void set_mySite_2(RuntimeObject* value)
	{
		___mySite_2 = value;
		Il2CppCodeGenWriteBarrier((&___mySite_2), value);
	}

	inline static int32_t get_offset_of_disposedEvent_3() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___disposedEvent_3)); }
	inline RuntimeObject * get_disposedEvent_3() const { return ___disposedEvent_3; }
	inline RuntimeObject ** get_address_of_disposedEvent_3() { return &___disposedEvent_3; }
	inline void set_disposedEvent_3(RuntimeObject * value)
	{
		___disposedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___disposedEvent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3620823400_H
#ifndef UPLOADDATACOMPLETEDEVENTARGS_T3263315353_H
#define UPLOADDATACOMPLETEDEVENTARGS_T3263315353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadDataCompletedEventArgs
struct  UploadDataCompletedEventArgs_t3263315353  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.Byte[] System.Net.UploadDataCompletedEventArgs::result
	ByteU5BU5D_t4116647657* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(UploadDataCompletedEventArgs_t3263315353, ___result_4)); }
	inline ByteU5BU5D_t4116647657* get_result_4() const { return ___result_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(ByteU5BU5D_t4116647657* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADDATACOMPLETEDEVENTARGS_T3263315353_H
#ifndef UPLOADFILECOMPLETEDEVENTARGS_T4191568555_H
#define UPLOADFILECOMPLETEDEVENTARGS_T4191568555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadFileCompletedEventArgs
struct  UploadFileCompletedEventArgs_t4191568555  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.Byte[] System.Net.UploadFileCompletedEventArgs::result
	ByteU5BU5D_t4116647657* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(UploadFileCompletedEventArgs_t4191568555, ___result_4)); }
	inline ByteU5BU5D_t4116647657* get_result_4() const { return ___result_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(ByteU5BU5D_t4116647657* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADFILECOMPLETEDEVENTARGS_T4191568555_H
#ifndef UPLOADPROGRESSCHANGEDEVENTARGS_T1971263489_H
#define UPLOADPROGRESSCHANGEDEVENTARGS_T1971263489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadProgressChangedEventArgs
struct  UploadProgressChangedEventArgs_t1971263489  : public ProgressChangedEventArgs_t3227452477
{
public:
	// System.Int64 System.Net.UploadProgressChangedEventArgs::received
	int64_t ___received_3;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::sent
	int64_t ___sent_4;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::total_recv
	int64_t ___total_recv_5;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::total_send
	int64_t ___total_send_6;

public:
	inline static int32_t get_offset_of_received_3() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_t1971263489, ___received_3)); }
	inline int64_t get_received_3() const { return ___received_3; }
	inline int64_t* get_address_of_received_3() { return &___received_3; }
	inline void set_received_3(int64_t value)
	{
		___received_3 = value;
	}

	inline static int32_t get_offset_of_sent_4() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_t1971263489, ___sent_4)); }
	inline int64_t get_sent_4() const { return ___sent_4; }
	inline int64_t* get_address_of_sent_4() { return &___sent_4; }
	inline void set_sent_4(int64_t value)
	{
		___sent_4 = value;
	}

	inline static int32_t get_offset_of_total_recv_5() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_t1971263489, ___total_recv_5)); }
	inline int64_t get_total_recv_5() const { return ___total_recv_5; }
	inline int64_t* get_address_of_total_recv_5() { return &___total_recv_5; }
	inline void set_total_recv_5(int64_t value)
	{
		___total_recv_5 = value;
	}

	inline static int32_t get_offset_of_total_send_6() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_t1971263489, ___total_send_6)); }
	inline int64_t get_total_send_6() const { return ___total_send_6; }
	inline int64_t* get_address_of_total_send_6() { return &___total_send_6; }
	inline void set_total_send_6(int64_t value)
	{
		___total_send_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPROGRESSCHANGEDEVENTARGS_T1971263489_H
#ifndef READSTATE_T245281014_H
#define READSTATE_T245281014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ReadState
struct  ReadState_t245281014 
{
public:
	// System.Int32 System.Net.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t245281014, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T245281014_H
#ifndef WEBCLIENT_T3506796782_H
#define WEBCLIENT_T3506796782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient
struct  WebClient_t3506796782  : public Component_t3620823400
{
public:
	// System.Net.ICredentials System.Net.WebClient::credentials
	RuntimeObject* ___credentials_6;
	// System.Net.WebHeaderCollection System.Net.WebClient::headers
	WebHeaderCollection_t1942268960 * ___headers_7;
	// System.Net.WebHeaderCollection System.Net.WebClient::responseHeaders
	WebHeaderCollection_t1942268960 * ___responseHeaders_8;
	// System.Uri System.Net.WebClient::baseAddress
	Uri_t100236324 * ___baseAddress_9;
	// System.String System.Net.WebClient::baseString
	String_t* ___baseString_10;
	// System.Collections.Specialized.NameValueCollection System.Net.WebClient::queryString
	NameValueCollection_t407452768 * ___queryString_11;
	// System.Boolean System.Net.WebClient::is_busy
	bool ___is_busy_12;
	// System.Boolean System.Net.WebClient::async
	bool ___async_13;
	// System.Threading.Thread System.Net.WebClient::async_thread
	Thread_t2300836069 * ___async_thread_14;
	// System.Text.Encoding System.Net.WebClient::encoding
	Encoding_t1523322056 * ___encoding_15;
	// System.Net.IWebProxy System.Net.WebClient::proxy
	RuntimeObject* ___proxy_16;
	// System.Net.DownloadDataCompletedEventHandler System.Net.WebClient::DownloadDataCompleted
	DownloadDataCompletedEventHandler_t1254360211 * ___DownloadDataCompleted_17;
	// System.ComponentModel.AsyncCompletedEventHandler System.Net.WebClient::DownloadFileCompleted
	AsyncCompletedEventHandler_t3247698412 * ___DownloadFileCompleted_18;
	// System.Net.DownloadProgressChangedEventHandler System.Net.WebClient::DownloadProgressChanged
	DownloadProgressChangedEventHandler_t3270744339 * ___DownloadProgressChanged_19;
	// System.Net.DownloadStringCompletedEventHandler System.Net.WebClient::DownloadStringCompleted
	DownloadStringCompletedEventHandler_t3405971822 * ___DownloadStringCompleted_20;
	// System.Net.OpenReadCompletedEventHandler System.Net.WebClient::OpenReadCompleted
	OpenReadCompletedEventHandler_t3320248228 * ___OpenReadCompleted_21;
	// System.Net.OpenWriteCompletedEventHandler System.Net.WebClient::OpenWriteCompleted
	OpenWriteCompletedEventHandler_t1427320362 * ___OpenWriteCompleted_22;
	// System.Net.UploadDataCompletedEventHandler System.Net.WebClient::UploadDataCompleted
	UploadDataCompletedEventHandler_t1479659898 * ___UploadDataCompleted_23;
	// System.Net.UploadFileCompletedEventHandler System.Net.WebClient::UploadFileCompleted
	UploadFileCompletedEventHandler_t3030596794 * ___UploadFileCompleted_24;
	// System.Net.UploadProgressChangedEventHandler System.Net.WebClient::UploadProgressChanged
	UploadProgressChangedEventHandler_t3804846296 * ___UploadProgressChanged_25;
	// System.Net.UploadStringCompletedEventHandler System.Net.WebClient::UploadStringCompleted
	UploadStringCompletedEventHandler_t3962204127 * ___UploadStringCompleted_26;
	// System.Net.UploadValuesCompletedEventHandler System.Net.WebClient::UploadValuesCompleted
	UploadValuesCompletedEventHandler_t679241653 * ___UploadValuesCompleted_27;

public:
	inline static int32_t get_offset_of_credentials_6() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___credentials_6)); }
	inline RuntimeObject* get_credentials_6() const { return ___credentials_6; }
	inline RuntimeObject** get_address_of_credentials_6() { return &___credentials_6; }
	inline void set_credentials_6(RuntimeObject* value)
	{
		___credentials_6 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_6), value);
	}

	inline static int32_t get_offset_of_headers_7() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___headers_7)); }
	inline WebHeaderCollection_t1942268960 * get_headers_7() const { return ___headers_7; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_7() { return &___headers_7; }
	inline void set_headers_7(WebHeaderCollection_t1942268960 * value)
	{
		___headers_7 = value;
		Il2CppCodeGenWriteBarrier((&___headers_7), value);
	}

	inline static int32_t get_offset_of_responseHeaders_8() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___responseHeaders_8)); }
	inline WebHeaderCollection_t1942268960 * get_responseHeaders_8() const { return ___responseHeaders_8; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_responseHeaders_8() { return &___responseHeaders_8; }
	inline void set_responseHeaders_8(WebHeaderCollection_t1942268960 * value)
	{
		___responseHeaders_8 = value;
		Il2CppCodeGenWriteBarrier((&___responseHeaders_8), value);
	}

	inline static int32_t get_offset_of_baseAddress_9() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___baseAddress_9)); }
	inline Uri_t100236324 * get_baseAddress_9() const { return ___baseAddress_9; }
	inline Uri_t100236324 ** get_address_of_baseAddress_9() { return &___baseAddress_9; }
	inline void set_baseAddress_9(Uri_t100236324 * value)
	{
		___baseAddress_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseAddress_9), value);
	}

	inline static int32_t get_offset_of_baseString_10() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___baseString_10)); }
	inline String_t* get_baseString_10() const { return ___baseString_10; }
	inline String_t** get_address_of_baseString_10() { return &___baseString_10; }
	inline void set_baseString_10(String_t* value)
	{
		___baseString_10 = value;
		Il2CppCodeGenWriteBarrier((&___baseString_10), value);
	}

	inline static int32_t get_offset_of_queryString_11() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___queryString_11)); }
	inline NameValueCollection_t407452768 * get_queryString_11() const { return ___queryString_11; }
	inline NameValueCollection_t407452768 ** get_address_of_queryString_11() { return &___queryString_11; }
	inline void set_queryString_11(NameValueCollection_t407452768 * value)
	{
		___queryString_11 = value;
		Il2CppCodeGenWriteBarrier((&___queryString_11), value);
	}

	inline static int32_t get_offset_of_is_busy_12() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___is_busy_12)); }
	inline bool get_is_busy_12() const { return ___is_busy_12; }
	inline bool* get_address_of_is_busy_12() { return &___is_busy_12; }
	inline void set_is_busy_12(bool value)
	{
		___is_busy_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_async_thread_14() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___async_thread_14)); }
	inline Thread_t2300836069 * get_async_thread_14() const { return ___async_thread_14; }
	inline Thread_t2300836069 ** get_address_of_async_thread_14() { return &___async_thread_14; }
	inline void set_async_thread_14(Thread_t2300836069 * value)
	{
		___async_thread_14 = value;
		Il2CppCodeGenWriteBarrier((&___async_thread_14), value);
	}

	inline static int32_t get_offset_of_encoding_15() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___encoding_15)); }
	inline Encoding_t1523322056 * get_encoding_15() const { return ___encoding_15; }
	inline Encoding_t1523322056 ** get_address_of_encoding_15() { return &___encoding_15; }
	inline void set_encoding_15(Encoding_t1523322056 * value)
	{
		___encoding_15 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_15), value);
	}

	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___proxy_16)); }
	inline RuntimeObject* get_proxy_16() const { return ___proxy_16; }
	inline RuntimeObject** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(RuntimeObject* value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_16), value);
	}

	inline static int32_t get_offset_of_DownloadDataCompleted_17() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___DownloadDataCompleted_17)); }
	inline DownloadDataCompletedEventHandler_t1254360211 * get_DownloadDataCompleted_17() const { return ___DownloadDataCompleted_17; }
	inline DownloadDataCompletedEventHandler_t1254360211 ** get_address_of_DownloadDataCompleted_17() { return &___DownloadDataCompleted_17; }
	inline void set_DownloadDataCompleted_17(DownloadDataCompletedEventHandler_t1254360211 * value)
	{
		___DownloadDataCompleted_17 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadDataCompleted_17), value);
	}

	inline static int32_t get_offset_of_DownloadFileCompleted_18() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___DownloadFileCompleted_18)); }
	inline AsyncCompletedEventHandler_t3247698412 * get_DownloadFileCompleted_18() const { return ___DownloadFileCompleted_18; }
	inline AsyncCompletedEventHandler_t3247698412 ** get_address_of_DownloadFileCompleted_18() { return &___DownloadFileCompleted_18; }
	inline void set_DownloadFileCompleted_18(AsyncCompletedEventHandler_t3247698412 * value)
	{
		___DownloadFileCompleted_18 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadFileCompleted_18), value);
	}

	inline static int32_t get_offset_of_DownloadProgressChanged_19() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___DownloadProgressChanged_19)); }
	inline DownloadProgressChangedEventHandler_t3270744339 * get_DownloadProgressChanged_19() const { return ___DownloadProgressChanged_19; }
	inline DownloadProgressChangedEventHandler_t3270744339 ** get_address_of_DownloadProgressChanged_19() { return &___DownloadProgressChanged_19; }
	inline void set_DownloadProgressChanged_19(DownloadProgressChangedEventHandler_t3270744339 * value)
	{
		___DownloadProgressChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadProgressChanged_19), value);
	}

	inline static int32_t get_offset_of_DownloadStringCompleted_20() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___DownloadStringCompleted_20)); }
	inline DownloadStringCompletedEventHandler_t3405971822 * get_DownloadStringCompleted_20() const { return ___DownloadStringCompleted_20; }
	inline DownloadStringCompletedEventHandler_t3405971822 ** get_address_of_DownloadStringCompleted_20() { return &___DownloadStringCompleted_20; }
	inline void set_DownloadStringCompleted_20(DownloadStringCompletedEventHandler_t3405971822 * value)
	{
		___DownloadStringCompleted_20 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadStringCompleted_20), value);
	}

	inline static int32_t get_offset_of_OpenReadCompleted_21() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___OpenReadCompleted_21)); }
	inline OpenReadCompletedEventHandler_t3320248228 * get_OpenReadCompleted_21() const { return ___OpenReadCompleted_21; }
	inline OpenReadCompletedEventHandler_t3320248228 ** get_address_of_OpenReadCompleted_21() { return &___OpenReadCompleted_21; }
	inline void set_OpenReadCompleted_21(OpenReadCompletedEventHandler_t3320248228 * value)
	{
		___OpenReadCompleted_21 = value;
		Il2CppCodeGenWriteBarrier((&___OpenReadCompleted_21), value);
	}

	inline static int32_t get_offset_of_OpenWriteCompleted_22() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___OpenWriteCompleted_22)); }
	inline OpenWriteCompletedEventHandler_t1427320362 * get_OpenWriteCompleted_22() const { return ___OpenWriteCompleted_22; }
	inline OpenWriteCompletedEventHandler_t1427320362 ** get_address_of_OpenWriteCompleted_22() { return &___OpenWriteCompleted_22; }
	inline void set_OpenWriteCompleted_22(OpenWriteCompletedEventHandler_t1427320362 * value)
	{
		___OpenWriteCompleted_22 = value;
		Il2CppCodeGenWriteBarrier((&___OpenWriteCompleted_22), value);
	}

	inline static int32_t get_offset_of_UploadDataCompleted_23() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___UploadDataCompleted_23)); }
	inline UploadDataCompletedEventHandler_t1479659898 * get_UploadDataCompleted_23() const { return ___UploadDataCompleted_23; }
	inline UploadDataCompletedEventHandler_t1479659898 ** get_address_of_UploadDataCompleted_23() { return &___UploadDataCompleted_23; }
	inline void set_UploadDataCompleted_23(UploadDataCompletedEventHandler_t1479659898 * value)
	{
		___UploadDataCompleted_23 = value;
		Il2CppCodeGenWriteBarrier((&___UploadDataCompleted_23), value);
	}

	inline static int32_t get_offset_of_UploadFileCompleted_24() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___UploadFileCompleted_24)); }
	inline UploadFileCompletedEventHandler_t3030596794 * get_UploadFileCompleted_24() const { return ___UploadFileCompleted_24; }
	inline UploadFileCompletedEventHandler_t3030596794 ** get_address_of_UploadFileCompleted_24() { return &___UploadFileCompleted_24; }
	inline void set_UploadFileCompleted_24(UploadFileCompletedEventHandler_t3030596794 * value)
	{
		___UploadFileCompleted_24 = value;
		Il2CppCodeGenWriteBarrier((&___UploadFileCompleted_24), value);
	}

	inline static int32_t get_offset_of_UploadProgressChanged_25() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___UploadProgressChanged_25)); }
	inline UploadProgressChangedEventHandler_t3804846296 * get_UploadProgressChanged_25() const { return ___UploadProgressChanged_25; }
	inline UploadProgressChangedEventHandler_t3804846296 ** get_address_of_UploadProgressChanged_25() { return &___UploadProgressChanged_25; }
	inline void set_UploadProgressChanged_25(UploadProgressChangedEventHandler_t3804846296 * value)
	{
		___UploadProgressChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___UploadProgressChanged_25), value);
	}

	inline static int32_t get_offset_of_UploadStringCompleted_26() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___UploadStringCompleted_26)); }
	inline UploadStringCompletedEventHandler_t3962204127 * get_UploadStringCompleted_26() const { return ___UploadStringCompleted_26; }
	inline UploadStringCompletedEventHandler_t3962204127 ** get_address_of_UploadStringCompleted_26() { return &___UploadStringCompleted_26; }
	inline void set_UploadStringCompleted_26(UploadStringCompletedEventHandler_t3962204127 * value)
	{
		___UploadStringCompleted_26 = value;
		Il2CppCodeGenWriteBarrier((&___UploadStringCompleted_26), value);
	}

	inline static int32_t get_offset_of_UploadValuesCompleted_27() { return static_cast<int32_t>(offsetof(WebClient_t3506796782, ___UploadValuesCompleted_27)); }
	inline UploadValuesCompletedEventHandler_t679241653 * get_UploadValuesCompleted_27() const { return ___UploadValuesCompleted_27; }
	inline UploadValuesCompletedEventHandler_t679241653 ** get_address_of_UploadValuesCompleted_27() { return &___UploadValuesCompleted_27; }
	inline void set_UploadValuesCompleted_27(UploadValuesCompletedEventHandler_t679241653 * value)
	{
		___UploadValuesCompleted_27 = value;
		Il2CppCodeGenWriteBarrier((&___UploadValuesCompleted_27), value);
	}
};

struct WebClient_t3506796782_StaticFields
{
public:
	// System.String System.Net.WebClient::urlEncodedCType
	String_t* ___urlEncodedCType_4;
	// System.Byte[] System.Net.WebClient::hexBytes
	ByteU5BU5D_t4116647657* ___hexBytes_5;

public:
	inline static int32_t get_offset_of_urlEncodedCType_4() { return static_cast<int32_t>(offsetof(WebClient_t3506796782_StaticFields, ___urlEncodedCType_4)); }
	inline String_t* get_urlEncodedCType_4() const { return ___urlEncodedCType_4; }
	inline String_t** get_address_of_urlEncodedCType_4() { return &___urlEncodedCType_4; }
	inline void set_urlEncodedCType_4(String_t* value)
	{
		___urlEncodedCType_4 = value;
		Il2CppCodeGenWriteBarrier((&___urlEncodedCType_4), value);
	}

	inline static int32_t get_offset_of_hexBytes_5() { return static_cast<int32_t>(offsetof(WebClient_t3506796782_StaticFields, ___hexBytes_5)); }
	inline ByteU5BU5D_t4116647657* get_hexBytes_5() const { return ___hexBytes_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_hexBytes_5() { return &___hexBytes_5; }
	inline void set_hexBytes_5(ByteU5BU5D_t4116647657* value)
	{
		___hexBytes_5 = value;
		Il2CppCodeGenWriteBarrier((&___hexBytes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCLIENT_T3506796782_H
#ifndef UPLOADSTRINGCOMPLETEDEVENTARGS_T1649479841_H
#define UPLOADSTRINGCOMPLETEDEVENTARGS_T1649479841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadStringCompletedEventArgs
struct  UploadStringCompletedEventArgs_t1649479841  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.String System.Net.UploadStringCompletedEventArgs::result
	String_t* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(UploadStringCompletedEventArgs_t1649479841, ___result_4)); }
	inline String_t* get_result_4() const { return ___result_4; }
	inline String_t** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(String_t* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTRINGCOMPLETEDEVENTARGS_T1649479841_H
#ifndef UPLOADVALUESCOMPLETEDEVENTARGS_T3455240456_H
#define UPLOADVALUESCOMPLETEDEVENTARGS_T3455240456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadValuesCompletedEventArgs
struct  UploadValuesCompletedEventArgs_t3455240456  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.Byte[] System.Net.UploadValuesCompletedEventArgs::result
	ByteU5BU5D_t4116647657* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(UploadValuesCompletedEventArgs_t3455240456, ___result_4)); }
	inline ByteU5BU5D_t4116647657* get_result_4() const { return ___result_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(ByteU5BU5D_t4116647657* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADVALUESCOMPLETEDEVENTARGS_T3455240456_H
#ifndef DECOMPRESSIONMETHODS_T1612219745_H
#define DECOMPRESSIONMETHODS_T1612219745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t1612219745 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DecompressionMethods_t1612219745, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T1612219745_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef FTPSTATUSCODE_T58879933_H
#define FTPSTATUSCODE_T58879933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatusCode
struct  FtpStatusCode_t58879933 
{
public:
	// System.Int32 System.Net.FtpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FtpStatusCode_t58879933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUSCODE_T58879933_H
#ifndef EXTERNALEXCEPTION_T3544951457_H
#define EXTERNALEXCEPTION_T3544951457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t3544951457  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T3544951457_H
#ifndef AUTHENTICATIONLEVEL_T1236753641_H
#define AUTHENTICATIONLEVEL_T1236753641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_t1236753641 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationLevel_t1236753641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_T1236753641_H
#ifndef ADDRESSFAMILY_T2612549059_H
#define ADDRESSFAMILY_T2612549059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t2612549059 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t2612549059, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T2612549059_H
#ifndef AUTHENTICATIONSCHEMES_T3459406435_H
#define AUTHENTICATIONSCHEMES_T3459406435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t3459406435 
{
public:
	// System.Int32 System.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t3459406435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T3459406435_H
#ifndef FILEACCESS_T1659085276_H
#define FILEACCESS_T1659085276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t1659085276 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAccess_t1659085276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T1659085276_H
#ifndef WEBEXCEPTIONSTATUS_T1731416715_H
#define WEBEXCEPTIONSTATUS_T1731416715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t1731416715 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t1731416715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T1731416715_H
#ifndef X509KEYUSAGEFLAGS_T1431795504_H
#define X509KEYUSAGEFLAGS_T1431795504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t1431795504 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t1431795504, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T1431795504_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef TRANSPORTTYPE_T4086112859_H
#define TRANSPORTTYPE_T4086112859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TransportType
struct  TransportType_t4086112859 
{
public:
	// System.Int32 System.Net.TransportType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransportType_t4086112859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPE_T4086112859_H
#ifndef OPENWRITECOMPLETEDEVENTARGS_T217345581_H
#define OPENWRITECOMPLETEDEVENTARGS_T217345581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenWriteCompletedEventArgs
struct  OpenWriteCompletedEventArgs_t217345581  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.IO.Stream System.Net.OpenWriteCompletedEventArgs::result
	Stream_t1273022909 * ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(OpenWriteCompletedEventArgs_t217345581, ___result_4)); }
	inline Stream_t1273022909 * get_result_4() const { return ___result_4; }
	inline Stream_t1273022909 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Stream_t1273022909 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENWRITECOMPLETEDEVENTARGS_T217345581_H
#ifndef INPUTSTATE_T4051538957_H
#define INPUTSTATE_T4051538957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection/InputState
struct  InputState_t4051538957 
{
public:
	// System.Int32 System.Net.HttpConnection/InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t4051538957, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T4051538957_H
#ifndef DOWNLOADSTRINGCOMPLETEDEVENTARGS_T3792188068_H
#define DOWNLOADSTRINGCOMPLETEDEVENTARGS_T3792188068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadStringCompletedEventArgs
struct  DownloadStringCompletedEventArgs_t3792188068  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.String System.Net.DownloadStringCompletedEventArgs::result
	String_t* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(DownloadStringCompletedEventArgs_t3792188068, ___result_4)); }
	inline String_t* get_result_4() const { return ___result_4; }
	inline String_t** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(String_t* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADSTRINGCOMPLETEDEVENTARGS_T3792188068_H
#ifndef OPENREADCOMPLETEDEVENTARGS_T3114398403_H
#define OPENREADCOMPLETEDEVENTARGS_T3114398403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenReadCompletedEventArgs
struct  OpenReadCompletedEventArgs_t3114398403  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.IO.Stream System.Net.OpenReadCompletedEventArgs::result
	Stream_t1273022909 * ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(OpenReadCompletedEventArgs_t3114398403, ___result_4)); }
	inline Stream_t1273022909 * get_result_4() const { return ___result_4; }
	inline Stream_t1273022909 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Stream_t1273022909 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENREADCOMPLETEDEVENTARGS_T3114398403_H
#ifndef NETWORKACCESS_T3596492016_H
#define NETWORKACCESS_T3596492016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkAccess
struct  NetworkAccess_t3596492016 
{
public:
	// System.Int32 System.Net.NetworkAccess::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkAccess_t3596492016, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKACCESS_T3596492016_H
#ifndef LINESTATE_T768597569_H
#define LINESTATE_T768597569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection/LineState
struct  LineState_t768597569 
{
public:
	// System.Int32 System.Net.HttpConnection/LineState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineState_t768597569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_T768597569_H
#ifndef DOWNLOADPROGRESSCHANGEDEVENTARGS_T2828131725_H
#define DOWNLOADPROGRESSCHANGEDEVENTARGS_T2828131725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadProgressChangedEventArgs
struct  DownloadProgressChangedEventArgs_t2828131725  : public ProgressChangedEventArgs_t3227452477
{
public:
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::received
	int64_t ___received_3;
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::total
	int64_t ___total_4;

public:
	inline static int32_t get_offset_of_received_3() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_t2828131725, ___received_3)); }
	inline int64_t get_received_3() const { return ___received_3; }
	inline int64_t* get_address_of_received_3() { return &___received_3; }
	inline void set_received_3(int64_t value)
	{
		___received_3 = value;
	}

	inline static int32_t get_offset_of_total_4() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_t2828131725, ___total_4)); }
	inline int64_t get_total_4() const { return ___total_4; }
	inline int64_t* get_address_of_total_4() { return &___total_4; }
	inline void set_total_4(int64_t value)
	{
		___total_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADPROGRESSCHANGEDEVENTARGS_T2828131725_H
#ifndef HTTPLISTENERREQUEST_T630699488_H
#define HTTPLISTENERREQUEST_T630699488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerRequest
struct  HttpListenerRequest_t630699488  : public RuntimeObject
{
public:
	// System.String[] System.Net.HttpListenerRequest::accept_types
	StringU5BU5D_t1281789340* ___accept_types_0;
	// System.Text.Encoding System.Net.HttpListenerRequest::content_encoding
	Encoding_t1523322056 * ___content_encoding_1;
	// System.Int64 System.Net.HttpListenerRequest::content_length
	int64_t ___content_length_2;
	// System.Boolean System.Net.HttpListenerRequest::cl_set
	bool ___cl_set_3;
	// System.Net.CookieCollection System.Net.HttpListenerRequest::cookies
	CookieCollection_t3881042616 * ___cookies_4;
	// System.Net.WebHeaderCollection System.Net.HttpListenerRequest::headers
	WebHeaderCollection_t1942268960 * ___headers_5;
	// System.String System.Net.HttpListenerRequest::method
	String_t* ___method_6;
	// System.IO.Stream System.Net.HttpListenerRequest::input_stream
	Stream_t1273022909 * ___input_stream_7;
	// System.Version System.Net.HttpListenerRequest::version
	Version_t3456873960 * ___version_8;
	// System.Collections.Specialized.NameValueCollection System.Net.HttpListenerRequest::query_string
	NameValueCollection_t407452768 * ___query_string_9;
	// System.String System.Net.HttpListenerRequest::raw_url
	String_t* ___raw_url_10;
	// System.Guid System.Net.HttpListenerRequest::identifier
	Guid_t  ___identifier_11;
	// System.Uri System.Net.HttpListenerRequest::url
	Uri_t100236324 * ___url_12;
	// System.Uri System.Net.HttpListenerRequest::referrer
	Uri_t100236324 * ___referrer_13;
	// System.String[] System.Net.HttpListenerRequest::user_languages
	StringU5BU5D_t1281789340* ___user_languages_14;
	// System.Net.HttpListenerContext System.Net.HttpListenerRequest::context
	HttpListenerContext_t424880822 * ___context_15;
	// System.Boolean System.Net.HttpListenerRequest::is_chunked
	bool ___is_chunked_16;

public:
	inline static int32_t get_offset_of_accept_types_0() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___accept_types_0)); }
	inline StringU5BU5D_t1281789340* get_accept_types_0() const { return ___accept_types_0; }
	inline StringU5BU5D_t1281789340** get_address_of_accept_types_0() { return &___accept_types_0; }
	inline void set_accept_types_0(StringU5BU5D_t1281789340* value)
	{
		___accept_types_0 = value;
		Il2CppCodeGenWriteBarrier((&___accept_types_0), value);
	}

	inline static int32_t get_offset_of_content_encoding_1() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___content_encoding_1)); }
	inline Encoding_t1523322056 * get_content_encoding_1() const { return ___content_encoding_1; }
	inline Encoding_t1523322056 ** get_address_of_content_encoding_1() { return &___content_encoding_1; }
	inline void set_content_encoding_1(Encoding_t1523322056 * value)
	{
		___content_encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_encoding_1), value);
	}

	inline static int32_t get_offset_of_content_length_2() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___content_length_2)); }
	inline int64_t get_content_length_2() const { return ___content_length_2; }
	inline int64_t* get_address_of_content_length_2() { return &___content_length_2; }
	inline void set_content_length_2(int64_t value)
	{
		___content_length_2 = value;
	}

	inline static int32_t get_offset_of_cl_set_3() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___cl_set_3)); }
	inline bool get_cl_set_3() const { return ___cl_set_3; }
	inline bool* get_address_of_cl_set_3() { return &___cl_set_3; }
	inline void set_cl_set_3(bool value)
	{
		___cl_set_3 = value;
	}

	inline static int32_t get_offset_of_cookies_4() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___cookies_4)); }
	inline CookieCollection_t3881042616 * get_cookies_4() const { return ___cookies_4; }
	inline CookieCollection_t3881042616 ** get_address_of_cookies_4() { return &___cookies_4; }
	inline void set_cookies_4(CookieCollection_t3881042616 * value)
	{
		___cookies_4 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_4), value);
	}

	inline static int32_t get_offset_of_headers_5() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___headers_5)); }
	inline WebHeaderCollection_t1942268960 * get_headers_5() const { return ___headers_5; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_5() { return &___headers_5; }
	inline void set_headers_5(WebHeaderCollection_t1942268960 * value)
	{
		___headers_5 = value;
		Il2CppCodeGenWriteBarrier((&___headers_5), value);
	}

	inline static int32_t get_offset_of_method_6() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___method_6)); }
	inline String_t* get_method_6() const { return ___method_6; }
	inline String_t** get_address_of_method_6() { return &___method_6; }
	inline void set_method_6(String_t* value)
	{
		___method_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_6), value);
	}

	inline static int32_t get_offset_of_input_stream_7() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___input_stream_7)); }
	inline Stream_t1273022909 * get_input_stream_7() const { return ___input_stream_7; }
	inline Stream_t1273022909 ** get_address_of_input_stream_7() { return &___input_stream_7; }
	inline void set_input_stream_7(Stream_t1273022909 * value)
	{
		___input_stream_7 = value;
		Il2CppCodeGenWriteBarrier((&___input_stream_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___version_8)); }
	inline Version_t3456873960 * get_version_8() const { return ___version_8; }
	inline Version_t3456873960 ** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(Version_t3456873960 * value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}

	inline static int32_t get_offset_of_query_string_9() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___query_string_9)); }
	inline NameValueCollection_t407452768 * get_query_string_9() const { return ___query_string_9; }
	inline NameValueCollection_t407452768 ** get_address_of_query_string_9() { return &___query_string_9; }
	inline void set_query_string_9(NameValueCollection_t407452768 * value)
	{
		___query_string_9 = value;
		Il2CppCodeGenWriteBarrier((&___query_string_9), value);
	}

	inline static int32_t get_offset_of_raw_url_10() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___raw_url_10)); }
	inline String_t* get_raw_url_10() const { return ___raw_url_10; }
	inline String_t** get_address_of_raw_url_10() { return &___raw_url_10; }
	inline void set_raw_url_10(String_t* value)
	{
		___raw_url_10 = value;
		Il2CppCodeGenWriteBarrier((&___raw_url_10), value);
	}

	inline static int32_t get_offset_of_identifier_11() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___identifier_11)); }
	inline Guid_t  get_identifier_11() const { return ___identifier_11; }
	inline Guid_t * get_address_of_identifier_11() { return &___identifier_11; }
	inline void set_identifier_11(Guid_t  value)
	{
		___identifier_11 = value;
	}

	inline static int32_t get_offset_of_url_12() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___url_12)); }
	inline Uri_t100236324 * get_url_12() const { return ___url_12; }
	inline Uri_t100236324 ** get_address_of_url_12() { return &___url_12; }
	inline void set_url_12(Uri_t100236324 * value)
	{
		___url_12 = value;
		Il2CppCodeGenWriteBarrier((&___url_12), value);
	}

	inline static int32_t get_offset_of_referrer_13() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___referrer_13)); }
	inline Uri_t100236324 * get_referrer_13() const { return ___referrer_13; }
	inline Uri_t100236324 ** get_address_of_referrer_13() { return &___referrer_13; }
	inline void set_referrer_13(Uri_t100236324 * value)
	{
		___referrer_13 = value;
		Il2CppCodeGenWriteBarrier((&___referrer_13), value);
	}

	inline static int32_t get_offset_of_user_languages_14() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___user_languages_14)); }
	inline StringU5BU5D_t1281789340* get_user_languages_14() const { return ___user_languages_14; }
	inline StringU5BU5D_t1281789340** get_address_of_user_languages_14() { return &___user_languages_14; }
	inline void set_user_languages_14(StringU5BU5D_t1281789340* value)
	{
		___user_languages_14 = value;
		Il2CppCodeGenWriteBarrier((&___user_languages_14), value);
	}

	inline static int32_t get_offset_of_context_15() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___context_15)); }
	inline HttpListenerContext_t424880822 * get_context_15() const { return ___context_15; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_15() { return &___context_15; }
	inline void set_context_15(HttpListenerContext_t424880822 * value)
	{
		___context_15 = value;
		Il2CppCodeGenWriteBarrier((&___context_15), value);
	}

	inline static int32_t get_offset_of_is_chunked_16() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___is_chunked_16)); }
	inline bool get_is_chunked_16() const { return ___is_chunked_16; }
	inline bool* get_address_of_is_chunked_16() { return &___is_chunked_16; }
	inline void set_is_chunked_16(bool value)
	{
		___is_chunked_16 = value;
	}
};

struct HttpListenerRequest_t630699488_StaticFields
{
public:
	// System.Byte[] System.Net.HttpListenerRequest::_100continue
	ByteU5BU5D_t4116647657* ____100continue_17;
	// System.String[] System.Net.HttpListenerRequest::no_body_methods
	StringU5BU5D_t1281789340* ___no_body_methods_18;
	// System.Char[] System.Net.HttpListenerRequest::separators
	CharU5BU5D_t3528271667* ___separators_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.HttpListenerRequest::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_20;

public:
	inline static int32_t get_offset_of__100continue_17() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ____100continue_17)); }
	inline ByteU5BU5D_t4116647657* get__100continue_17() const { return ____100continue_17; }
	inline ByteU5BU5D_t4116647657** get_address_of__100continue_17() { return &____100continue_17; }
	inline void set__100continue_17(ByteU5BU5D_t4116647657* value)
	{
		____100continue_17 = value;
		Il2CppCodeGenWriteBarrier((&____100continue_17), value);
	}

	inline static int32_t get_offset_of_no_body_methods_18() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___no_body_methods_18)); }
	inline StringU5BU5D_t1281789340* get_no_body_methods_18() const { return ___no_body_methods_18; }
	inline StringU5BU5D_t1281789340** get_address_of_no_body_methods_18() { return &___no_body_methods_18; }
	inline void set_no_body_methods_18(StringU5BU5D_t1281789340* value)
	{
		___no_body_methods_18 = value;
		Il2CppCodeGenWriteBarrier((&___no_body_methods_18), value);
	}

	inline static int32_t get_offset_of_separators_19() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___separators_19)); }
	inline CharU5BU5D_t3528271667* get_separators_19() const { return ___separators_19; }
	inline CharU5BU5D_t3528271667** get_address_of_separators_19() { return &___separators_19; }
	inline void set_separators_19(CharU5BU5D_t3528271667* value)
	{
		___separators_19 = value;
		Il2CppCodeGenWriteBarrier((&___separators_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_20() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___U3CU3Ef__switchU24map7_20)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_20() const { return ___U3CU3Ef__switchU24map7_20; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_20() { return &___U3CU3Ef__switchU24map7_20; }
	inline void set_U3CU3Ef__switchU24map7_20(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERREQUEST_T630699488_H
#ifndef REQUESTSTATE_T4091696808_H
#define REQUESTSTATE_T4091696808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest/RequestState
struct  RequestState_t4091696808 
{
public:
	// System.Int32 System.Net.FtpWebRequest/RequestState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequestState_t4091696808, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTATE_T4091696808_H
#ifndef HTTPRESPONSEHEADER_T1526164768_H
#define HTTPRESPONSEHEADER_T1526164768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpResponseHeader
struct  HttpResponseHeader_t1526164768 
{
public:
	// System.Int32 System.Net.HttpResponseHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpResponseHeader_t1526164768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEHEADER_T1526164768_H
#ifndef HTTPSTATUSCODE_T3035121829_H
#define HTTPSTATUSCODE_T3035121829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t3035121829 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t3035121829, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T3035121829_H
#ifndef DOWNLOADDATACOMPLETEDEVENTARGS_T1352343852_H
#define DOWNLOADDATACOMPLETEDEVENTARGS_T1352343852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadDataCompletedEventArgs
struct  DownloadDataCompletedEventArgs_t1352343852  : public AsyncCompletedEventArgs_t1863481821
{
public:
	// System.Byte[] System.Net.DownloadDataCompletedEventArgs::result
	ByteU5BU5D_t4116647657* ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(DownloadDataCompletedEventArgs_t1352343852, ___result_4)); }
	inline ByteU5BU5D_t4116647657* get_result_4() const { return ___result_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(ByteU5BU5D_t4116647657* value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADDATACOMPLETEDEVENTARGS_T1352343852_H
#ifndef FILEWEBRESPONSE_T544571260_H
#define FILEWEBRESPONSE_T544571260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebResponse
struct  FileWebResponse_t544571260  : public WebResponse_t229922639
{
public:
	// System.Uri System.Net.FileWebResponse::responseUri
	Uri_t100236324 * ___responseUri_1;
	// System.IO.FileStream System.Net.FileWebResponse::fileStream
	FileStream_t4292183065 * ___fileStream_2;
	// System.Int64 System.Net.FileWebResponse::contentLength
	int64_t ___contentLength_3;
	// System.Net.WebHeaderCollection System.Net.FileWebResponse::webHeaders
	WebHeaderCollection_t1942268960 * ___webHeaders_4;
	// System.Boolean System.Net.FileWebResponse::disposed
	bool ___disposed_5;

public:
	inline static int32_t get_offset_of_responseUri_1() { return static_cast<int32_t>(offsetof(FileWebResponse_t544571260, ___responseUri_1)); }
	inline Uri_t100236324 * get_responseUri_1() const { return ___responseUri_1; }
	inline Uri_t100236324 ** get_address_of_responseUri_1() { return &___responseUri_1; }
	inline void set_responseUri_1(Uri_t100236324 * value)
	{
		___responseUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseUri_1), value);
	}

	inline static int32_t get_offset_of_fileStream_2() { return static_cast<int32_t>(offsetof(FileWebResponse_t544571260, ___fileStream_2)); }
	inline FileStream_t4292183065 * get_fileStream_2() const { return ___fileStream_2; }
	inline FileStream_t4292183065 ** get_address_of_fileStream_2() { return &___fileStream_2; }
	inline void set_fileStream_2(FileStream_t4292183065 * value)
	{
		___fileStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileStream_2), value);
	}

	inline static int32_t get_offset_of_contentLength_3() { return static_cast<int32_t>(offsetof(FileWebResponse_t544571260, ___contentLength_3)); }
	inline int64_t get_contentLength_3() const { return ___contentLength_3; }
	inline int64_t* get_address_of_contentLength_3() { return &___contentLength_3; }
	inline void set_contentLength_3(int64_t value)
	{
		___contentLength_3 = value;
	}

	inline static int32_t get_offset_of_webHeaders_4() { return static_cast<int32_t>(offsetof(FileWebResponse_t544571260, ___webHeaders_4)); }
	inline WebHeaderCollection_t1942268960 * get_webHeaders_4() const { return ___webHeaders_4; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_webHeaders_4() { return &___webHeaders_4; }
	inline void set_webHeaders_4(WebHeaderCollection_t1942268960 * value)
	{
		___webHeaders_4 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_4), value);
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(FileWebResponse_t544571260, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBRESPONSE_T544571260_H
#ifndef SECURITYPROTOCOLTYPE_T2721465497_H
#define SECURITYPROTOCOLTYPE_T2721465497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t2721465497 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t2721465497, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T2721465497_H
#ifndef HTTPREQUESTHEADER_T4258090762_H
#define HTTPREQUESTHEADER_T4258090762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpRequestHeader
struct  HttpRequestHeader_t4258090762 
{
public:
	// System.Int32 System.Net.HttpRequestHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpRequestHeader_t4258090762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTHEADER_T4258090762_H
#ifndef HTTPWEBRESPONSE_T3286585418_H
#define HTTPWEBRESPONSE_T3286585418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebResponse
struct  HttpWebResponse_t3286585418  : public WebResponse_t229922639
{
public:
	// System.Uri System.Net.HttpWebResponse::uri
	Uri_t100236324 * ___uri_1;
	// System.Net.WebHeaderCollection System.Net.HttpWebResponse::webHeaders
	WebHeaderCollection_t1942268960 * ___webHeaders_2;
	// System.Net.CookieCollection System.Net.HttpWebResponse::cookieCollection
	CookieCollection_t3881042616 * ___cookieCollection_3;
	// System.String System.Net.HttpWebResponse::method
	String_t* ___method_4;
	// System.Version System.Net.HttpWebResponse::version
	Version_t3456873960 * ___version_5;
	// System.Net.HttpStatusCode System.Net.HttpWebResponse::statusCode
	int32_t ___statusCode_6;
	// System.String System.Net.HttpWebResponse::statusDescription
	String_t* ___statusDescription_7;
	// System.Int64 System.Net.HttpWebResponse::contentLength
	int64_t ___contentLength_8;
	// System.String System.Net.HttpWebResponse::contentType
	String_t* ___contentType_9;
	// System.Net.CookieContainer System.Net.HttpWebResponse::cookie_container
	CookieContainer_t2331592909 * ___cookie_container_10;
	// System.Boolean System.Net.HttpWebResponse::disposed
	bool ___disposed_11;
	// System.IO.Stream System.Net.HttpWebResponse::stream
	Stream_t1273022909 * ___stream_12;
	// System.String[] System.Net.HttpWebResponse::cookieExpiresFormats
	StringU5BU5D_t1281789340* ___cookieExpiresFormats_13;

public:
	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___uri_1)); }
	inline Uri_t100236324 * get_uri_1() const { return ___uri_1; }
	inline Uri_t100236324 ** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(Uri_t100236324 * value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_webHeaders_2() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___webHeaders_2)); }
	inline WebHeaderCollection_t1942268960 * get_webHeaders_2() const { return ___webHeaders_2; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_webHeaders_2() { return &___webHeaders_2; }
	inline void set_webHeaders_2(WebHeaderCollection_t1942268960 * value)
	{
		___webHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_2), value);
	}

	inline static int32_t get_offset_of_cookieCollection_3() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___cookieCollection_3)); }
	inline CookieCollection_t3881042616 * get_cookieCollection_3() const { return ___cookieCollection_3; }
	inline CookieCollection_t3881042616 ** get_address_of_cookieCollection_3() { return &___cookieCollection_3; }
	inline void set_cookieCollection_3(CookieCollection_t3881042616 * value)
	{
		___cookieCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___cookieCollection_3), value);
	}

	inline static int32_t get_offset_of_method_4() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___method_4)); }
	inline String_t* get_method_4() const { return ___method_4; }
	inline String_t** get_address_of_method_4() { return &___method_4; }
	inline void set_method_4(String_t* value)
	{
		___method_4 = value;
		Il2CppCodeGenWriteBarrier((&___method_4), value);
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___version_5)); }
	inline Version_t3456873960 * get_version_5() const { return ___version_5; }
	inline Version_t3456873960 ** get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(Version_t3456873960 * value)
	{
		___version_5 = value;
		Il2CppCodeGenWriteBarrier((&___version_5), value);
	}

	inline static int32_t get_offset_of_statusCode_6() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___statusCode_6)); }
	inline int32_t get_statusCode_6() const { return ___statusCode_6; }
	inline int32_t* get_address_of_statusCode_6() { return &___statusCode_6; }
	inline void set_statusCode_6(int32_t value)
	{
		___statusCode_6 = value;
	}

	inline static int32_t get_offset_of_statusDescription_7() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___statusDescription_7)); }
	inline String_t* get_statusDescription_7() const { return ___statusDescription_7; }
	inline String_t** get_address_of_statusDescription_7() { return &___statusDescription_7; }
	inline void set_statusDescription_7(String_t* value)
	{
		___statusDescription_7 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_7), value);
	}

	inline static int32_t get_offset_of_contentLength_8() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___contentLength_8)); }
	inline int64_t get_contentLength_8() const { return ___contentLength_8; }
	inline int64_t* get_address_of_contentLength_8() { return &___contentLength_8; }
	inline void set_contentLength_8(int64_t value)
	{
		___contentLength_8 = value;
	}

	inline static int32_t get_offset_of_contentType_9() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___contentType_9)); }
	inline String_t* get_contentType_9() const { return ___contentType_9; }
	inline String_t** get_address_of_contentType_9() { return &___contentType_9; }
	inline void set_contentType_9(String_t* value)
	{
		___contentType_9 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_9), value);
	}

	inline static int32_t get_offset_of_cookie_container_10() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___cookie_container_10)); }
	inline CookieContainer_t2331592909 * get_cookie_container_10() const { return ___cookie_container_10; }
	inline CookieContainer_t2331592909 ** get_address_of_cookie_container_10() { return &___cookie_container_10; }
	inline void set_cookie_container_10(CookieContainer_t2331592909 * value)
	{
		___cookie_container_10 = value;
		Il2CppCodeGenWriteBarrier((&___cookie_container_10), value);
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_stream_12() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___stream_12)); }
	inline Stream_t1273022909 * get_stream_12() const { return ___stream_12; }
	inline Stream_t1273022909 ** get_address_of_stream_12() { return &___stream_12; }
	inline void set_stream_12(Stream_t1273022909 * value)
	{
		___stream_12 = value;
		Il2CppCodeGenWriteBarrier((&___stream_12), value);
	}

	inline static int32_t get_offset_of_cookieExpiresFormats_13() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418, ___cookieExpiresFormats_13)); }
	inline StringU5BU5D_t1281789340* get_cookieExpiresFormats_13() const { return ___cookieExpiresFormats_13; }
	inline StringU5BU5D_t1281789340** get_address_of_cookieExpiresFormats_13() { return &___cookieExpiresFormats_13; }
	inline void set_cookieExpiresFormats_13(StringU5BU5D_t1281789340* value)
	{
		___cookieExpiresFormats_13 = value;
		Il2CppCodeGenWriteBarrier((&___cookieExpiresFormats_13), value);
	}
};

struct HttpWebResponse_t3286585418_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.HttpWebResponse::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_14() { return static_cast<int32_t>(offsetof(HttpWebResponse_t3286585418_StaticFields, ___U3CU3Ef__switchU24map8_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_14() const { return ___U3CU3Ef__switchU24map8_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_14() { return &___U3CU3Ef__switchU24map8_14; }
	inline void set_U3CU3Ef__switchU24map8_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBRESPONSE_T3286585418_H
#ifndef FILESTREAM_T4292183065_H
#define FILESTREAM_T4292183065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t4292183065  : public Stream_t1273022909
{
public:
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_1;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_2;
	// System.Boolean System.IO.FileStream::async
	bool ___async_3;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_4;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_5;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_6;
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t4116647657* ___buf_7;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_8;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_9;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_10;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_11;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_12;
	// System.String System.IO.FileStream::name
	String_t* ___name_13;
	// System.IntPtr System.IO.FileStream::handle
	intptr_t ___handle_14;

public:
	inline static int32_t get_offset_of_access_1() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___access_1)); }
	inline int32_t get_access_1() const { return ___access_1; }
	inline int32_t* get_address_of_access_1() { return &___access_1; }
	inline void set_access_1(int32_t value)
	{
		___access_1 = value;
	}

	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___owner_2)); }
	inline bool get_owner_2() const { return ___owner_2; }
	inline bool* get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(bool value)
	{
		___owner_2 = value;
	}

	inline static int32_t get_offset_of_async_3() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___async_3)); }
	inline bool get_async_3() const { return ___async_3; }
	inline bool* get_address_of_async_3() { return &___async_3; }
	inline void set_async_3(bool value)
	{
		___async_3 = value;
	}

	inline static int32_t get_offset_of_canseek_4() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___canseek_4)); }
	inline bool get_canseek_4() const { return ___canseek_4; }
	inline bool* get_address_of_canseek_4() { return &___canseek_4; }
	inline void set_canseek_4(bool value)
	{
		___canseek_4 = value;
	}

	inline static int32_t get_offset_of_append_startpos_5() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___append_startpos_5)); }
	inline int64_t get_append_startpos_5() const { return ___append_startpos_5; }
	inline int64_t* get_address_of_append_startpos_5() { return &___append_startpos_5; }
	inline void set_append_startpos_5(int64_t value)
	{
		___append_startpos_5 = value;
	}

	inline static int32_t get_offset_of_anonymous_6() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___anonymous_6)); }
	inline bool get_anonymous_6() const { return ___anonymous_6; }
	inline bool* get_address_of_anonymous_6() { return &___anonymous_6; }
	inline void set_anonymous_6(bool value)
	{
		___anonymous_6 = value;
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_7)); }
	inline ByteU5BU5D_t4116647657* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_t4116647657* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_buf_size_8() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_size_8)); }
	inline int32_t get_buf_size_8() const { return ___buf_size_8; }
	inline int32_t* get_address_of_buf_size_8() { return &___buf_size_8; }
	inline void set_buf_size_8(int32_t value)
	{
		___buf_size_8 = value;
	}

	inline static int32_t get_offset_of_buf_length_9() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_length_9)); }
	inline int32_t get_buf_length_9() const { return ___buf_length_9; }
	inline int32_t* get_address_of_buf_length_9() { return &___buf_length_9; }
	inline void set_buf_length_9(int32_t value)
	{
		___buf_length_9 = value;
	}

	inline static int32_t get_offset_of_buf_offset_10() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_offset_10)); }
	inline int32_t get_buf_offset_10() const { return ___buf_offset_10; }
	inline int32_t* get_address_of_buf_offset_10() { return &___buf_offset_10; }
	inline void set_buf_offset_10(int32_t value)
	{
		___buf_offset_10 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_11() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_dirty_11)); }
	inline bool get_buf_dirty_11() const { return ___buf_dirty_11; }
	inline bool* get_address_of_buf_dirty_11() { return &___buf_dirty_11; }
	inline void set_buf_dirty_11(bool value)
	{
		___buf_dirty_11 = value;
	}

	inline static int32_t get_offset_of_buf_start_12() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_start_12)); }
	inline int64_t get_buf_start_12() const { return ___buf_start_12; }
	inline int64_t* get_address_of_buf_start_12() { return &___buf_start_12; }
	inline void set_buf_start_12(int64_t value)
	{
		___buf_start_12 = value;
	}

	inline static int32_t get_offset_of_name_13() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___name_13)); }
	inline String_t* get_name_13() const { return ___name_13; }
	inline String_t** get_address_of_name_13() { return &___name_13; }
	inline void set_name_13(String_t* value)
	{
		___name_13 = value;
		Il2CppCodeGenWriteBarrier((&___name_13), value);
	}

	inline static int32_t get_offset_of_handle_14() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___handle_14)); }
	inline intptr_t get_handle_14() const { return ___handle_14; }
	inline intptr_t* get_address_of_handle_14() { return &___handle_14; }
	inline void set_handle_14(intptr_t value)
	{
		___handle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_T4292183065_H
#ifndef WIN32EXCEPTION_T3234146298_H
#define WIN32EXCEPTION_T3234146298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_t3234146298  : public ExternalException_t3544951457
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::native_error_code
	int32_t ___native_error_code_11;

public:
	inline static int32_t get_offset_of_native_error_code_11() { return static_cast<int32_t>(offsetof(Win32Exception_t3234146298, ___native_error_code_11)); }
	inline int32_t get_native_error_code_11() const { return ___native_error_code_11; }
	inline int32_t* get_address_of_native_error_code_11() { return &___native_error_code_11; }
	inline void set_native_error_code_11(int32_t value)
	{
		___native_error_code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_T3234146298_H
#ifndef IPADDRESS_T241777590_H
#define IPADDRESS_T241777590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t241777590  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t3326319531* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_11;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t3326319531* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t3326319531** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t3326319531* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_11() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_HashCode_11)); }
	inline int32_t get_m_HashCode_11() const { return ___m_HashCode_11; }
	inline int32_t* get_address_of_m_HashCode_11() { return &___m_HashCode_11; }
	inline void set_m_HashCode_11(int32_t value)
	{
		___m_HashCode_11 = value;
	}
};

struct IPAddress_t241777590_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t241777590 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t241777590 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t241777590 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t241777590 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t241777590 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t241777590 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t241777590 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Any_4)); }
	inline IPAddress_t241777590 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t241777590 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t241777590 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t241777590 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t241777590 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t241777590 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Loopback_6)); }
	inline IPAddress_t241777590 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t241777590 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t241777590 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___None_7)); }
	inline IPAddress_t241777590 * get_None_7() const { return ___None_7; }
	inline IPAddress_t241777590 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t241777590 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t241777590 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t241777590 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t241777590 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t241777590 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t241777590 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t241777590 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t241777590 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t241777590 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t241777590 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T241777590_H
#ifndef HTTPLISTENER_T988452056_H
#define HTTPLISTENER_T988452056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListener
struct  HttpListener_t988452056  : public RuntimeObject
{
public:
	// System.Net.AuthenticationSchemes System.Net.HttpListener::auth_schemes
	int32_t ___auth_schemes_0;
	// System.Net.HttpListenerPrefixCollection System.Net.HttpListener::prefixes
	HttpListenerPrefixCollection_t2963430373 * ___prefixes_1;
	// System.Net.AuthenticationSchemeSelector System.Net.HttpListener::auth_selector
	AuthenticationSchemeSelector_t375327801 * ___auth_selector_2;
	// System.String System.Net.HttpListener::realm
	String_t* ___realm_3;
	// System.Boolean System.Net.HttpListener::ignore_write_exceptions
	bool ___ignore_write_exceptions_4;
	// System.Boolean System.Net.HttpListener::unsafe_ntlm_auth
	bool ___unsafe_ntlm_auth_5;
	// System.Boolean System.Net.HttpListener::listening
	bool ___listening_6;
	// System.Boolean System.Net.HttpListener::disposed
	bool ___disposed_7;
	// System.Collections.Hashtable System.Net.HttpListener::registry
	Hashtable_t1853889766 * ___registry_8;
	// System.Collections.ArrayList System.Net.HttpListener::ctx_queue
	ArrayList_t2718874744 * ___ctx_queue_9;
	// System.Collections.ArrayList System.Net.HttpListener::wait_queue
	ArrayList_t2718874744 * ___wait_queue_10;

public:
	inline static int32_t get_offset_of_auth_schemes_0() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___auth_schemes_0)); }
	inline int32_t get_auth_schemes_0() const { return ___auth_schemes_0; }
	inline int32_t* get_address_of_auth_schemes_0() { return &___auth_schemes_0; }
	inline void set_auth_schemes_0(int32_t value)
	{
		___auth_schemes_0 = value;
	}

	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___prefixes_1)); }
	inline HttpListenerPrefixCollection_t2963430373 * get_prefixes_1() const { return ___prefixes_1; }
	inline HttpListenerPrefixCollection_t2963430373 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(HttpListenerPrefixCollection_t2963430373 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_auth_selector_2() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___auth_selector_2)); }
	inline AuthenticationSchemeSelector_t375327801 * get_auth_selector_2() const { return ___auth_selector_2; }
	inline AuthenticationSchemeSelector_t375327801 ** get_address_of_auth_selector_2() { return &___auth_selector_2; }
	inline void set_auth_selector_2(AuthenticationSchemeSelector_t375327801 * value)
	{
		___auth_selector_2 = value;
		Il2CppCodeGenWriteBarrier((&___auth_selector_2), value);
	}

	inline static int32_t get_offset_of_realm_3() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___realm_3)); }
	inline String_t* get_realm_3() const { return ___realm_3; }
	inline String_t** get_address_of_realm_3() { return &___realm_3; }
	inline void set_realm_3(String_t* value)
	{
		___realm_3 = value;
		Il2CppCodeGenWriteBarrier((&___realm_3), value);
	}

	inline static int32_t get_offset_of_ignore_write_exceptions_4() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___ignore_write_exceptions_4)); }
	inline bool get_ignore_write_exceptions_4() const { return ___ignore_write_exceptions_4; }
	inline bool* get_address_of_ignore_write_exceptions_4() { return &___ignore_write_exceptions_4; }
	inline void set_ignore_write_exceptions_4(bool value)
	{
		___ignore_write_exceptions_4 = value;
	}

	inline static int32_t get_offset_of_unsafe_ntlm_auth_5() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___unsafe_ntlm_auth_5)); }
	inline bool get_unsafe_ntlm_auth_5() const { return ___unsafe_ntlm_auth_5; }
	inline bool* get_address_of_unsafe_ntlm_auth_5() { return &___unsafe_ntlm_auth_5; }
	inline void set_unsafe_ntlm_auth_5(bool value)
	{
		___unsafe_ntlm_auth_5 = value;
	}

	inline static int32_t get_offset_of_listening_6() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___listening_6)); }
	inline bool get_listening_6() const { return ___listening_6; }
	inline bool* get_address_of_listening_6() { return &___listening_6; }
	inline void set_listening_6(bool value)
	{
		___listening_6 = value;
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_registry_8() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___registry_8)); }
	inline Hashtable_t1853889766 * get_registry_8() const { return ___registry_8; }
	inline Hashtable_t1853889766 ** get_address_of_registry_8() { return &___registry_8; }
	inline void set_registry_8(Hashtable_t1853889766 * value)
	{
		___registry_8 = value;
		Il2CppCodeGenWriteBarrier((&___registry_8), value);
	}

	inline static int32_t get_offset_of_ctx_queue_9() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___ctx_queue_9)); }
	inline ArrayList_t2718874744 * get_ctx_queue_9() const { return ___ctx_queue_9; }
	inline ArrayList_t2718874744 ** get_address_of_ctx_queue_9() { return &___ctx_queue_9; }
	inline void set_ctx_queue_9(ArrayList_t2718874744 * value)
	{
		___ctx_queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_queue_9), value);
	}

	inline static int32_t get_offset_of_wait_queue_10() { return static_cast<int32_t>(offsetof(HttpListener_t988452056, ___wait_queue_10)); }
	inline ArrayList_t2718874744 * get_wait_queue_10() const { return ___wait_queue_10; }
	inline ArrayList_t2718874744 ** get_address_of_wait_queue_10() { return &___wait_queue_10; }
	inline void set_wait_queue_10(ArrayList_t2718874744 * value)
	{
		___wait_queue_10 = value;
		Il2CppCodeGenWriteBarrier((&___wait_queue_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENER_T988452056_H
#ifndef WEBREQUEST_T1939381076_H
#define WEBREQUEST_T1939381076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t1939381076  : public MarshalByRefObject_t2760389100
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::authentication_level
	int32_t ___authentication_level_4;

public:
	inline static int32_t get_offset_of_authentication_level_4() { return static_cast<int32_t>(offsetof(WebRequest_t1939381076, ___authentication_level_4)); }
	inline int32_t get_authentication_level_4() const { return ___authentication_level_4; }
	inline int32_t* get_address_of_authentication_level_4() { return &___authentication_level_4; }
	inline void set_authentication_level_4(int32_t value)
	{
		___authentication_level_4 = value;
	}
};

struct WebRequest_t1939381076_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.WebRequest::prefixes
	HybridDictionary_t4070033136 * ___prefixes_1;
	// System.Boolean System.Net.WebRequest::isDefaultWebProxySet
	bool ___isDefaultWebProxySet_2;
	// System.Net.IWebProxy System.Net.WebRequest::defaultWebProxy
	RuntimeObject* ___defaultWebProxy_3;
	// System.Object System.Net.WebRequest::lockobj
	RuntimeObject * ___lockobj_5;

public:
	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(WebRequest_t1939381076_StaticFields, ___prefixes_1)); }
	inline HybridDictionary_t4070033136 * get_prefixes_1() const { return ___prefixes_1; }
	inline HybridDictionary_t4070033136 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(HybridDictionary_t4070033136 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_isDefaultWebProxySet_2() { return static_cast<int32_t>(offsetof(WebRequest_t1939381076_StaticFields, ___isDefaultWebProxySet_2)); }
	inline bool get_isDefaultWebProxySet_2() const { return ___isDefaultWebProxySet_2; }
	inline bool* get_address_of_isDefaultWebProxySet_2() { return &___isDefaultWebProxySet_2; }
	inline void set_isDefaultWebProxySet_2(bool value)
	{
		___isDefaultWebProxySet_2 = value;
	}

	inline static int32_t get_offset_of_defaultWebProxy_3() { return static_cast<int32_t>(offsetof(WebRequest_t1939381076_StaticFields, ___defaultWebProxy_3)); }
	inline RuntimeObject* get_defaultWebProxy_3() const { return ___defaultWebProxy_3; }
	inline RuntimeObject** get_address_of_defaultWebProxy_3() { return &___defaultWebProxy_3; }
	inline void set_defaultWebProxy_3(RuntimeObject* value)
	{
		___defaultWebProxy_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultWebProxy_3), value);
	}

	inline static int32_t get_offset_of_lockobj_5() { return static_cast<int32_t>(offsetof(WebRequest_t1939381076_StaticFields, ___lockobj_5)); }
	inline RuntimeObject * get_lockobj_5() const { return ___lockobj_5; }
	inline RuntimeObject ** get_address_of_lockobj_5() { return &___lockobj_5; }
	inline void set_lockobj_5(RuntimeObject * value)
	{
		___lockobj_5 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T1939381076_H
#ifndef CHAINVALIDATIONHELPER_T320539540_H
#define CHAINVALIDATIONHELPER_T320539540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/ChainValidationHelper
struct  ChainValidationHelper_t320539540  : public RuntimeObject
{
public:
	// System.Object System.Net.ServicePointManager/ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// System.String System.Net.ServicePointManager/ChainValidationHelper::host
	String_t* ___host_1;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t320539540, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t320539540, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}
};

struct ChainValidationHelper_t320539540_StaticFields
{
public:
	// System.Boolean System.Net.ServicePointManager/ChainValidationHelper::is_macosx
	bool ___is_macosx_2;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Net.ServicePointManager/ChainValidationHelper::s_flags
	int32_t ___s_flags_3;

public:
	inline static int32_t get_offset_of_is_macosx_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t320539540_StaticFields, ___is_macosx_2)); }
	inline bool get_is_macosx_2() const { return ___is_macosx_2; }
	inline bool* get_address_of_is_macosx_2() { return &___is_macosx_2; }
	inline void set_is_macosx_2(bool value)
	{
		___is_macosx_2 = value;
	}

	inline static int32_t get_offset_of_s_flags_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t320539540_StaticFields, ___s_flags_3)); }
	inline int32_t get_s_flags_3() const { return ___s_flags_3; }
	inline int32_t* get_address_of_s_flags_3() { return &___s_flags_3; }
	inline void set_s_flags_3(int32_t value)
	{
		___s_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T320539540_H
#ifndef FTPSTATUS_T2376455776_H
#define FTPSTATUS_T2376455776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatus
struct  FtpStatus_t2376455776  : public RuntimeObject
{
public:
	// System.Net.FtpStatusCode System.Net.FtpStatus::statusCode
	int32_t ___statusCode_0;
	// System.String System.Net.FtpStatus::statusDescription
	String_t* ___statusDescription_1;

public:
	inline static int32_t get_offset_of_statusCode_0() { return static_cast<int32_t>(offsetof(FtpStatus_t2376455776, ___statusCode_0)); }
	inline int32_t get_statusCode_0() const { return ___statusCode_0; }
	inline int32_t* get_address_of_statusCode_0() { return &___statusCode_0; }
	inline void set_statusCode_0(int32_t value)
	{
		___statusCode_0 = value;
	}

	inline static int32_t get_offset_of_statusDescription_1() { return static_cast<int32_t>(offsetof(FtpStatus_t2376455776, ___statusDescription_1)); }
	inline String_t* get_statusDescription_1() const { return ___statusDescription_1; }
	inline String_t** get_address_of_statusDescription_1() { return &___statusDescription_1; }
	inline void set_statusDescription_1(String_t* value)
	{
		___statusDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUS_T2376455776_H
#ifndef WEBCONNECTION_T3982808322_H
#define WEBCONNECTION_T3982808322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection
struct  WebConnection_t3982808322  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnection::sPoint
	ServicePoint_t2786966844 * ___sPoint_0;
	// System.IO.Stream System.Net.WebConnection::nstream
	Stream_t1273022909 * ___nstream_1;
	// System.Net.Sockets.Socket System.Net.WebConnection::socket
	Socket_t1119025450 * ___socket_2;
	// System.Object System.Net.WebConnection::socketLock
	RuntimeObject * ___socketLock_3;
	// System.Net.WebExceptionStatus System.Net.WebConnection::status
	int32_t ___status_4;
	// System.Threading.WaitCallback System.Net.WebConnection::initConn
	WaitCallback_t2448485498 * ___initConn_5;
	// System.Boolean System.Net.WebConnection::keepAlive
	bool ___keepAlive_6;
	// System.Byte[] System.Net.WebConnection::buffer
	ByteU5BU5D_t4116647657* ___buffer_7;
	// System.EventHandler System.Net.WebConnection::abortHandler
	EventHandler_t1348719766 * ___abortHandler_9;
	// System.Net.WebConnection/AbortHelper System.Net.WebConnection::abortHelper
	AbortHelper_t1490877826 * ___abortHelper_10;
	// System.Net.ReadState System.Net.WebConnection::readState
	int32_t ___readState_11;
	// System.Net.WebConnectionData System.Net.WebConnection::Data
	WebConnectionData_t3835660455 * ___Data_12;
	// System.Boolean System.Net.WebConnection::chunkedRead
	bool ___chunkedRead_13;
	// System.Net.ChunkStream System.Net.WebConnection::chunkStream
	ChunkStream_t2634567336 * ___chunkStream_14;
	// System.Collections.Queue System.Net.WebConnection::queue
	Queue_t3637523393 * ___queue_15;
	// System.Boolean System.Net.WebConnection::reused
	bool ___reused_16;
	// System.Int32 System.Net.WebConnection::position
	int32_t ___position_17;
	// System.Boolean System.Net.WebConnection::busy
	bool ___busy_18;
	// System.Net.HttpWebRequest System.Net.WebConnection::priority_request
	HttpWebRequest_t1669436515 * ___priority_request_19;
	// System.Net.NetworkCredential System.Net.WebConnection::ntlm_credentials
	NetworkCredential_t3282608323 * ___ntlm_credentials_20;
	// System.Boolean System.Net.WebConnection::ntlm_authenticated
	bool ___ntlm_authenticated_21;
	// System.Boolean System.Net.WebConnection::unsafe_sharing
	bool ___unsafe_sharing_22;
	// System.Boolean System.Net.WebConnection::ssl
	bool ___ssl_23;
	// System.Boolean System.Net.WebConnection::certsAvailable
	bool ___certsAvailable_24;
	// System.Exception System.Net.WebConnection::connect_exception
	Exception_t * ___connect_exception_25;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___sPoint_0)); }
	inline ServicePoint_t2786966844 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t2786966844 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t2786966844 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_nstream_1() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___nstream_1)); }
	inline Stream_t1273022909 * get_nstream_1() const { return ___nstream_1; }
	inline Stream_t1273022909 ** get_address_of_nstream_1() { return &___nstream_1; }
	inline void set_nstream_1(Stream_t1273022909 * value)
	{
		___nstream_1 = value;
		Il2CppCodeGenWriteBarrier((&___nstream_1), value);
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___socket_2)); }
	inline Socket_t1119025450 * get_socket_2() const { return ___socket_2; }
	inline Socket_t1119025450 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t1119025450 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_socketLock_3() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___socketLock_3)); }
	inline RuntimeObject * get_socketLock_3() const { return ___socketLock_3; }
	inline RuntimeObject ** get_address_of_socketLock_3() { return &___socketLock_3; }
	inline void set_socketLock_3(RuntimeObject * value)
	{
		___socketLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___socketLock_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_initConn_5() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___initConn_5)); }
	inline WaitCallback_t2448485498 * get_initConn_5() const { return ___initConn_5; }
	inline WaitCallback_t2448485498 ** get_address_of_initConn_5() { return &___initConn_5; }
	inline void set_initConn_5(WaitCallback_t2448485498 * value)
	{
		___initConn_5 = value;
		Il2CppCodeGenWriteBarrier((&___initConn_5), value);
	}

	inline static int32_t get_offset_of_keepAlive_6() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___keepAlive_6)); }
	inline bool get_keepAlive_6() const { return ___keepAlive_6; }
	inline bool* get_address_of_keepAlive_6() { return &___keepAlive_6; }
	inline void set_keepAlive_6(bool value)
	{
		___keepAlive_6 = value;
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___buffer_7)); }
	inline ByteU5BU5D_t4116647657* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_t4116647657* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_abortHandler_9() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___abortHandler_9)); }
	inline EventHandler_t1348719766 * get_abortHandler_9() const { return ___abortHandler_9; }
	inline EventHandler_t1348719766 ** get_address_of_abortHandler_9() { return &___abortHandler_9; }
	inline void set_abortHandler_9(EventHandler_t1348719766 * value)
	{
		___abortHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_9), value);
	}

	inline static int32_t get_offset_of_abortHelper_10() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___abortHelper_10)); }
	inline AbortHelper_t1490877826 * get_abortHelper_10() const { return ___abortHelper_10; }
	inline AbortHelper_t1490877826 ** get_address_of_abortHelper_10() { return &___abortHelper_10; }
	inline void set_abortHelper_10(AbortHelper_t1490877826 * value)
	{
		___abortHelper_10 = value;
		Il2CppCodeGenWriteBarrier((&___abortHelper_10), value);
	}

	inline static int32_t get_offset_of_readState_11() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___readState_11)); }
	inline int32_t get_readState_11() const { return ___readState_11; }
	inline int32_t* get_address_of_readState_11() { return &___readState_11; }
	inline void set_readState_11(int32_t value)
	{
		___readState_11 = value;
	}

	inline static int32_t get_offset_of_Data_12() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___Data_12)); }
	inline WebConnectionData_t3835660455 * get_Data_12() const { return ___Data_12; }
	inline WebConnectionData_t3835660455 ** get_address_of_Data_12() { return &___Data_12; }
	inline void set_Data_12(WebConnectionData_t3835660455 * value)
	{
		___Data_12 = value;
		Il2CppCodeGenWriteBarrier((&___Data_12), value);
	}

	inline static int32_t get_offset_of_chunkedRead_13() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___chunkedRead_13)); }
	inline bool get_chunkedRead_13() const { return ___chunkedRead_13; }
	inline bool* get_address_of_chunkedRead_13() { return &___chunkedRead_13; }
	inline void set_chunkedRead_13(bool value)
	{
		___chunkedRead_13 = value;
	}

	inline static int32_t get_offset_of_chunkStream_14() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___chunkStream_14)); }
	inline ChunkStream_t2634567336 * get_chunkStream_14() const { return ___chunkStream_14; }
	inline ChunkStream_t2634567336 ** get_address_of_chunkStream_14() { return &___chunkStream_14; }
	inline void set_chunkStream_14(ChunkStream_t2634567336 * value)
	{
		___chunkStream_14 = value;
		Il2CppCodeGenWriteBarrier((&___chunkStream_14), value);
	}

	inline static int32_t get_offset_of_queue_15() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___queue_15)); }
	inline Queue_t3637523393 * get_queue_15() const { return ___queue_15; }
	inline Queue_t3637523393 ** get_address_of_queue_15() { return &___queue_15; }
	inline void set_queue_15(Queue_t3637523393 * value)
	{
		___queue_15 = value;
		Il2CppCodeGenWriteBarrier((&___queue_15), value);
	}

	inline static int32_t get_offset_of_reused_16() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___reused_16)); }
	inline bool get_reused_16() const { return ___reused_16; }
	inline bool* get_address_of_reused_16() { return &___reused_16; }
	inline void set_reused_16(bool value)
	{
		___reused_16 = value;
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___position_17)); }
	inline int32_t get_position_17() const { return ___position_17; }
	inline int32_t* get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(int32_t value)
	{
		___position_17 = value;
	}

	inline static int32_t get_offset_of_busy_18() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___busy_18)); }
	inline bool get_busy_18() const { return ___busy_18; }
	inline bool* get_address_of_busy_18() { return &___busy_18; }
	inline void set_busy_18(bool value)
	{
		___busy_18 = value;
	}

	inline static int32_t get_offset_of_priority_request_19() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___priority_request_19)); }
	inline HttpWebRequest_t1669436515 * get_priority_request_19() const { return ___priority_request_19; }
	inline HttpWebRequest_t1669436515 ** get_address_of_priority_request_19() { return &___priority_request_19; }
	inline void set_priority_request_19(HttpWebRequest_t1669436515 * value)
	{
		___priority_request_19 = value;
		Il2CppCodeGenWriteBarrier((&___priority_request_19), value);
	}

	inline static int32_t get_offset_of_ntlm_credentials_20() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___ntlm_credentials_20)); }
	inline NetworkCredential_t3282608323 * get_ntlm_credentials_20() const { return ___ntlm_credentials_20; }
	inline NetworkCredential_t3282608323 ** get_address_of_ntlm_credentials_20() { return &___ntlm_credentials_20; }
	inline void set_ntlm_credentials_20(NetworkCredential_t3282608323 * value)
	{
		___ntlm_credentials_20 = value;
		Il2CppCodeGenWriteBarrier((&___ntlm_credentials_20), value);
	}

	inline static int32_t get_offset_of_ntlm_authenticated_21() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___ntlm_authenticated_21)); }
	inline bool get_ntlm_authenticated_21() const { return ___ntlm_authenticated_21; }
	inline bool* get_address_of_ntlm_authenticated_21() { return &___ntlm_authenticated_21; }
	inline void set_ntlm_authenticated_21(bool value)
	{
		___ntlm_authenticated_21 = value;
	}

	inline static int32_t get_offset_of_unsafe_sharing_22() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___unsafe_sharing_22)); }
	inline bool get_unsafe_sharing_22() const { return ___unsafe_sharing_22; }
	inline bool* get_address_of_unsafe_sharing_22() { return &___unsafe_sharing_22; }
	inline void set_unsafe_sharing_22(bool value)
	{
		___unsafe_sharing_22 = value;
	}

	inline static int32_t get_offset_of_ssl_23() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___ssl_23)); }
	inline bool get_ssl_23() const { return ___ssl_23; }
	inline bool* get_address_of_ssl_23() { return &___ssl_23; }
	inline void set_ssl_23(bool value)
	{
		___ssl_23 = value;
	}

	inline static int32_t get_offset_of_certsAvailable_24() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___certsAvailable_24)); }
	inline bool get_certsAvailable_24() const { return ___certsAvailable_24; }
	inline bool* get_address_of_certsAvailable_24() { return &___certsAvailable_24; }
	inline void set_certsAvailable_24(bool value)
	{
		___certsAvailable_24 = value;
	}

	inline static int32_t get_offset_of_connect_exception_25() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322, ___connect_exception_25)); }
	inline Exception_t * get_connect_exception_25() const { return ___connect_exception_25; }
	inline Exception_t ** get_address_of_connect_exception_25() { return &___connect_exception_25; }
	inline void set_connect_exception_25(Exception_t * value)
	{
		___connect_exception_25 = value;
		Il2CppCodeGenWriteBarrier((&___connect_exception_25), value);
	}
};

struct WebConnection_t3982808322_StaticFields
{
public:
	// System.AsyncCallback System.Net.WebConnection::readDoneDelegate
	AsyncCallback_t3962456242 * ___readDoneDelegate_8;
	// System.Object System.Net.WebConnection::classLock
	RuntimeObject * ___classLock_26;
	// System.Type System.Net.WebConnection::sslStream
	Type_t * ___sslStream_27;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piClient
	PropertyInfo_t * ___piClient_28;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piServer
	PropertyInfo_t * ___piServer_29;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piTrustFailure
	PropertyInfo_t * ___piTrustFailure_30;
	// System.Reflection.MethodInfo System.Net.WebConnection::method_GetSecurityPolicyFromNonMainThread
	MethodInfo_t * ___method_GetSecurityPolicyFromNonMainThread_31;

public:
	inline static int32_t get_offset_of_readDoneDelegate_8() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___readDoneDelegate_8)); }
	inline AsyncCallback_t3962456242 * get_readDoneDelegate_8() const { return ___readDoneDelegate_8; }
	inline AsyncCallback_t3962456242 ** get_address_of_readDoneDelegate_8() { return &___readDoneDelegate_8; }
	inline void set_readDoneDelegate_8(AsyncCallback_t3962456242 * value)
	{
		___readDoneDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___readDoneDelegate_8), value);
	}

	inline static int32_t get_offset_of_classLock_26() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___classLock_26)); }
	inline RuntimeObject * get_classLock_26() const { return ___classLock_26; }
	inline RuntimeObject ** get_address_of_classLock_26() { return &___classLock_26; }
	inline void set_classLock_26(RuntimeObject * value)
	{
		___classLock_26 = value;
		Il2CppCodeGenWriteBarrier((&___classLock_26), value);
	}

	inline static int32_t get_offset_of_sslStream_27() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___sslStream_27)); }
	inline Type_t * get_sslStream_27() const { return ___sslStream_27; }
	inline Type_t ** get_address_of_sslStream_27() { return &___sslStream_27; }
	inline void set_sslStream_27(Type_t * value)
	{
		___sslStream_27 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_27), value);
	}

	inline static int32_t get_offset_of_piClient_28() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___piClient_28)); }
	inline PropertyInfo_t * get_piClient_28() const { return ___piClient_28; }
	inline PropertyInfo_t ** get_address_of_piClient_28() { return &___piClient_28; }
	inline void set_piClient_28(PropertyInfo_t * value)
	{
		___piClient_28 = value;
		Il2CppCodeGenWriteBarrier((&___piClient_28), value);
	}

	inline static int32_t get_offset_of_piServer_29() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___piServer_29)); }
	inline PropertyInfo_t * get_piServer_29() const { return ___piServer_29; }
	inline PropertyInfo_t ** get_address_of_piServer_29() { return &___piServer_29; }
	inline void set_piServer_29(PropertyInfo_t * value)
	{
		___piServer_29 = value;
		Il2CppCodeGenWriteBarrier((&___piServer_29), value);
	}

	inline static int32_t get_offset_of_piTrustFailure_30() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___piTrustFailure_30)); }
	inline PropertyInfo_t * get_piTrustFailure_30() const { return ___piTrustFailure_30; }
	inline PropertyInfo_t ** get_address_of_piTrustFailure_30() { return &___piTrustFailure_30; }
	inline void set_piTrustFailure_30(PropertyInfo_t * value)
	{
		___piTrustFailure_30 = value;
		Il2CppCodeGenWriteBarrier((&___piTrustFailure_30), value);
	}

	inline static int32_t get_offset_of_method_GetSecurityPolicyFromNonMainThread_31() { return static_cast<int32_t>(offsetof(WebConnection_t3982808322_StaticFields, ___method_GetSecurityPolicyFromNonMainThread_31)); }
	inline MethodInfo_t * get_method_GetSecurityPolicyFromNonMainThread_31() const { return ___method_GetSecurityPolicyFromNonMainThread_31; }
	inline MethodInfo_t ** get_address_of_method_GetSecurityPolicyFromNonMainThread_31() { return &___method_GetSecurityPolicyFromNonMainThread_31; }
	inline void set_method_GetSecurityPolicyFromNonMainThread_31(MethodInfo_t * value)
	{
		___method_GetSecurityPolicyFromNonMainThread_31 = value;
		Il2CppCodeGenWriteBarrier((&___method_GetSecurityPolicyFromNonMainThread_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTION_T3982808322_H
#ifndef SERVICEPOINTMANAGER_T170559685_H
#define SERVICEPOINTMANAGER_T170559685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_t170559685  : public RuntimeObject
{
public:

public:
};

struct ServicePointManager_t170559685_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t4070033136 * ___servicePoints_2;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	RuntimeObject* ___policy_3;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_4;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_5;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_6;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_7;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_8;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_9;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_10;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::server_cert_cb
	RemoteCertificateValidationCallback_t3014364904 * ___server_cert_cb_11;

public:
	inline static int32_t get_offset_of_servicePoints_2() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___servicePoints_2)); }
	inline HybridDictionary_t4070033136 * get_servicePoints_2() const { return ___servicePoints_2; }
	inline HybridDictionary_t4070033136 ** get_address_of_servicePoints_2() { return &___servicePoints_2; }
	inline void set_servicePoints_2(HybridDictionary_t4070033136 * value)
	{
		___servicePoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoints_2), value);
	}

	inline static int32_t get_offset_of_policy_3() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___policy_3)); }
	inline RuntimeObject* get_policy_3() const { return ___policy_3; }
	inline RuntimeObject** get_address_of_policy_3() { return &___policy_3; }
	inline void set_policy_3(RuntimeObject* value)
	{
		___policy_3 = value;
		Il2CppCodeGenWriteBarrier((&___policy_3), value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_4() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___defaultConnectionLimit_4)); }
	inline int32_t get_defaultConnectionLimit_4() const { return ___defaultConnectionLimit_4; }
	inline int32_t* get_address_of_defaultConnectionLimit_4() { return &___defaultConnectionLimit_4; }
	inline void set_defaultConnectionLimit_4(int32_t value)
	{
		___defaultConnectionLimit_4 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_5() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___maxServicePointIdleTime_5)); }
	inline int32_t get_maxServicePointIdleTime_5() const { return ___maxServicePointIdleTime_5; }
	inline int32_t* get_address_of_maxServicePointIdleTime_5() { return &___maxServicePointIdleTime_5; }
	inline void set_maxServicePointIdleTime_5(int32_t value)
	{
		___maxServicePointIdleTime_5 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_6() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___maxServicePoints_6)); }
	inline int32_t get_maxServicePoints_6() const { return ___maxServicePoints_6; }
	inline int32_t* get_address_of_maxServicePoints_6() { return &___maxServicePoints_6; }
	inline void set_maxServicePoints_6(int32_t value)
	{
		___maxServicePoints_6 = value;
	}

	inline static int32_t get_offset_of__checkCRL_7() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ____checkCRL_7)); }
	inline bool get__checkCRL_7() const { return ____checkCRL_7; }
	inline bool* get_address_of__checkCRL_7() { return &____checkCRL_7; }
	inline void set__checkCRL_7(bool value)
	{
		____checkCRL_7 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_8() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ____securityProtocol_8)); }
	inline int32_t get__securityProtocol_8() const { return ____securityProtocol_8; }
	inline int32_t* get_address_of__securityProtocol_8() { return &____securityProtocol_8; }
	inline void set__securityProtocol_8(int32_t value)
	{
		____securityProtocol_8 = value;
	}

	inline static int32_t get_offset_of_expectContinue_9() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___expectContinue_9)); }
	inline bool get_expectContinue_9() const { return ___expectContinue_9; }
	inline bool* get_address_of_expectContinue_9() { return &___expectContinue_9; }
	inline void set_expectContinue_9(bool value)
	{
		___expectContinue_9 = value;
	}

	inline static int32_t get_offset_of_useNagle_10() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___useNagle_10)); }
	inline bool get_useNagle_10() const { return ___useNagle_10; }
	inline bool* get_address_of_useNagle_10() { return &___useNagle_10; }
	inline void set_useNagle_10(bool value)
	{
		___useNagle_10 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_11() { return static_cast<int32_t>(offsetof(ServicePointManager_t170559685_StaticFields, ___server_cert_cb_11)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_server_cert_cb_11() const { return ___server_cert_cb_11; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_server_cert_cb_11() { return &___server_cert_cb_11; }
	inline void set_server_cert_cb_11(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___server_cert_cb_11 = value;
		Il2CppCodeGenWriteBarrier((&___server_cert_cb_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGER_T170559685_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_T4144007430_H
#define PROTOCOLVIOLATIONEXCEPTION_T4144007430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t4144007430  : public InvalidOperationException_t56020091
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_T4144007430_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef HTTPCONNECTION_T269576101_H
#define HTTPCONNECTION_T269576101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection
struct  HttpConnection_t269576101  : public RuntimeObject
{
public:
	// System.Net.Sockets.Socket System.Net.HttpConnection::sock
	Socket_t1119025450 * ___sock_1;
	// System.IO.Stream System.Net.HttpConnection::stream
	Stream_t1273022909 * ___stream_2;
	// System.Net.EndPointListener System.Net.HttpConnection::epl
	EndPointListener_t2984434924 * ___epl_3;
	// System.IO.MemoryStream System.Net.HttpConnection::ms
	MemoryStream_t94973147 * ___ms_4;
	// System.Byte[] System.Net.HttpConnection::buffer
	ByteU5BU5D_t4116647657* ___buffer_5;
	// System.Net.HttpListenerContext System.Net.HttpConnection::context
	HttpListenerContext_t424880822 * ___context_6;
	// System.Text.StringBuilder System.Net.HttpConnection::current_line
	StringBuilder_t * ___current_line_7;
	// System.Net.ListenerPrefix System.Net.HttpConnection::prefix
	ListenerPrefix_t3570496559 * ___prefix_8;
	// System.Net.RequestStream System.Net.HttpConnection::i_stream
	RequestStream_t762880582 * ___i_stream_9;
	// System.Net.ResponseStream System.Net.HttpConnection::o_stream
	ResponseStream_t3810703494 * ___o_stream_10;
	// System.Boolean System.Net.HttpConnection::chunked
	bool ___chunked_11;
	// System.Int32 System.Net.HttpConnection::chunked_uses
	int32_t ___chunked_uses_12;
	// System.Boolean System.Net.HttpConnection::context_bound
	bool ___context_bound_13;
	// System.Boolean System.Net.HttpConnection::secure
	bool ___secure_14;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Net.HttpConnection::key
	AsymmetricAlgorithm_t932037087 * ___key_15;
	// System.Net.HttpConnection/InputState System.Net.HttpConnection::input_state
	int32_t ___input_state_16;
	// System.Net.HttpConnection/LineState System.Net.HttpConnection::line_state
	int32_t ___line_state_17;
	// System.Int32 System.Net.HttpConnection::position
	int32_t ___position_18;

public:
	inline static int32_t get_offset_of_sock_1() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___sock_1)); }
	inline Socket_t1119025450 * get_sock_1() const { return ___sock_1; }
	inline Socket_t1119025450 ** get_address_of_sock_1() { return &___sock_1; }
	inline void set_sock_1(Socket_t1119025450 * value)
	{
		___sock_1 = value;
		Il2CppCodeGenWriteBarrier((&___sock_1), value);
	}

	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___stream_2)); }
	inline Stream_t1273022909 * get_stream_2() const { return ___stream_2; }
	inline Stream_t1273022909 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(Stream_t1273022909 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier((&___stream_2), value);
	}

	inline static int32_t get_offset_of_epl_3() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___epl_3)); }
	inline EndPointListener_t2984434924 * get_epl_3() const { return ___epl_3; }
	inline EndPointListener_t2984434924 ** get_address_of_epl_3() { return &___epl_3; }
	inline void set_epl_3(EndPointListener_t2984434924 * value)
	{
		___epl_3 = value;
		Il2CppCodeGenWriteBarrier((&___epl_3), value);
	}

	inline static int32_t get_offset_of_ms_4() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___ms_4)); }
	inline MemoryStream_t94973147 * get_ms_4() const { return ___ms_4; }
	inline MemoryStream_t94973147 ** get_address_of_ms_4() { return &___ms_4; }
	inline void set_ms_4(MemoryStream_t94973147 * value)
	{
		___ms_4 = value;
		Il2CppCodeGenWriteBarrier((&___ms_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___buffer_5)); }
	inline ByteU5BU5D_t4116647657* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_t4116647657* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___context_6)); }
	inline HttpListenerContext_t424880822 * get_context_6() const { return ___context_6; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(HttpListenerContext_t424880822 * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier((&___context_6), value);
	}

	inline static int32_t get_offset_of_current_line_7() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___current_line_7)); }
	inline StringBuilder_t * get_current_line_7() const { return ___current_line_7; }
	inline StringBuilder_t ** get_address_of_current_line_7() { return &___current_line_7; }
	inline void set_current_line_7(StringBuilder_t * value)
	{
		___current_line_7 = value;
		Il2CppCodeGenWriteBarrier((&___current_line_7), value);
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___prefix_8)); }
	inline ListenerPrefix_t3570496559 * get_prefix_8() const { return ___prefix_8; }
	inline ListenerPrefix_t3570496559 ** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(ListenerPrefix_t3570496559 * value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}

	inline static int32_t get_offset_of_i_stream_9() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___i_stream_9)); }
	inline RequestStream_t762880582 * get_i_stream_9() const { return ___i_stream_9; }
	inline RequestStream_t762880582 ** get_address_of_i_stream_9() { return &___i_stream_9; }
	inline void set_i_stream_9(RequestStream_t762880582 * value)
	{
		___i_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___i_stream_9), value);
	}

	inline static int32_t get_offset_of_o_stream_10() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___o_stream_10)); }
	inline ResponseStream_t3810703494 * get_o_stream_10() const { return ___o_stream_10; }
	inline ResponseStream_t3810703494 ** get_address_of_o_stream_10() { return &___o_stream_10; }
	inline void set_o_stream_10(ResponseStream_t3810703494 * value)
	{
		___o_stream_10 = value;
		Il2CppCodeGenWriteBarrier((&___o_stream_10), value);
	}

	inline static int32_t get_offset_of_chunked_11() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___chunked_11)); }
	inline bool get_chunked_11() const { return ___chunked_11; }
	inline bool* get_address_of_chunked_11() { return &___chunked_11; }
	inline void set_chunked_11(bool value)
	{
		___chunked_11 = value;
	}

	inline static int32_t get_offset_of_chunked_uses_12() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___chunked_uses_12)); }
	inline int32_t get_chunked_uses_12() const { return ___chunked_uses_12; }
	inline int32_t* get_address_of_chunked_uses_12() { return &___chunked_uses_12; }
	inline void set_chunked_uses_12(int32_t value)
	{
		___chunked_uses_12 = value;
	}

	inline static int32_t get_offset_of_context_bound_13() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___context_bound_13)); }
	inline bool get_context_bound_13() const { return ___context_bound_13; }
	inline bool* get_address_of_context_bound_13() { return &___context_bound_13; }
	inline void set_context_bound_13(bool value)
	{
		___context_bound_13 = value;
	}

	inline static int32_t get_offset_of_secure_14() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___secure_14)); }
	inline bool get_secure_14() const { return ___secure_14; }
	inline bool* get_address_of_secure_14() { return &___secure_14; }
	inline void set_secure_14(bool value)
	{
		___secure_14 = value;
	}

	inline static int32_t get_offset_of_key_15() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___key_15)); }
	inline AsymmetricAlgorithm_t932037087 * get_key_15() const { return ___key_15; }
	inline AsymmetricAlgorithm_t932037087 ** get_address_of_key_15() { return &___key_15; }
	inline void set_key_15(AsymmetricAlgorithm_t932037087 * value)
	{
		___key_15 = value;
		Il2CppCodeGenWriteBarrier((&___key_15), value);
	}

	inline static int32_t get_offset_of_input_state_16() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___input_state_16)); }
	inline int32_t get_input_state_16() const { return ___input_state_16; }
	inline int32_t* get_address_of_input_state_16() { return &___input_state_16; }
	inline void set_input_state_16(int32_t value)
	{
		___input_state_16 = value;
	}

	inline static int32_t get_offset_of_line_state_17() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___line_state_17)); }
	inline int32_t get_line_state_17() const { return ___line_state_17; }
	inline int32_t* get_address_of_line_state_17() { return &___line_state_17; }
	inline void set_line_state_17(int32_t value)
	{
		___line_state_17 = value;
	}

	inline static int32_t get_offset_of_position_18() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___position_18)); }
	inline int32_t get_position_18() const { return ___position_18; }
	inline int32_t* get_address_of_position_18() { return &___position_18; }
	inline void set_position_18(int32_t value)
	{
		___position_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_T269576101_H
#ifndef WEBEXCEPTION_T3237156354_H
#define WEBEXCEPTION_T3237156354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_t3237156354  : public InvalidOperationException_t56020091
{
public:
	// System.Net.WebResponse System.Net.WebException::response
	WebResponse_t229922639 * ___response_12;
	// System.Net.WebExceptionStatus System.Net.WebException::status
	int32_t ___status_13;

public:
	inline static int32_t get_offset_of_response_12() { return static_cast<int32_t>(offsetof(WebException_t3237156354, ___response_12)); }
	inline WebResponse_t229922639 * get_response_12() const { return ___response_12; }
	inline WebResponse_t229922639 ** get_address_of_response_12() { return &___response_12; }
	inline void set_response_12(WebResponse_t229922639 * value)
	{
		___response_12 = value;
		Il2CppCodeGenWriteBarrier((&___response_12), value);
	}

	inline static int32_t get_offset_of_status_13() { return static_cast<int32_t>(offsetof(WebException_t3237156354, ___status_13)); }
	inline int32_t get_status_13() const { return ___status_13; }
	inline int32_t* get_address_of_status_13() { return &___status_13; }
	inline void set_status_13(int32_t value)
	{
		___status_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_T3237156354_H
#ifndef FTPWEBREQUEST_T1577818305_H
#define FTPWEBREQUEST_T1577818305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t1577818305  : public WebRequest_t1939381076
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t100236324 * ___requestUri_19;
	// System.String System.Net.FtpWebRequest::file_name
	String_t* ___file_name_20;
	// System.Net.ServicePoint System.Net.FtpWebRequest::servicePoint
	ServicePoint_t2786966844 * ___servicePoint_21;
	// System.IO.Stream System.Net.FtpWebRequest::origDataStream
	Stream_t1273022909 * ___origDataStream_22;
	// System.IO.Stream System.Net.FtpWebRequest::dataStream
	Stream_t1273022909 * ___dataStream_23;
	// System.IO.Stream System.Net.FtpWebRequest::controlStream
	Stream_t1273022909 * ___controlStream_24;
	// System.IO.StreamReader System.Net.FtpWebRequest::controlReader
	StreamReader_t4009935899 * ___controlReader_25;
	// System.Net.NetworkCredential System.Net.FtpWebRequest::credentials
	NetworkCredential_t3282608323 * ___credentials_26;
	// System.Net.IPHostEntry System.Net.FtpWebRequest::hostEntry
	IPHostEntry_t263743900 * ___hostEntry_27;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::localEndPoint
	IPEndPoint_t3791887218 * ___localEndPoint_28;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	RuntimeObject* ___proxy_29;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_30;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_31;
	// System.Int64 System.Net.FtpWebRequest::offset
	int64_t ___offset_32;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_33;
	// System.Boolean System.Net.FtpWebRequest::enableSsl
	bool ___enableSsl_34;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_35;
	// System.Boolean System.Net.FtpWebRequest::keepAlive
	bool ___keepAlive_36;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_37;
	// System.String System.Net.FtpWebRequest::renameTo
	String_t* ___renameTo_38;
	// System.Object System.Net.FtpWebRequest::locker
	RuntimeObject * ___locker_39;
	// System.Net.FtpWebRequest/RequestState System.Net.FtpWebRequest::requestState
	int32_t ___requestState_40;
	// System.Net.FtpAsyncResult System.Net.FtpWebRequest::asyncResult
	FtpAsyncResult_t3265664217 * ___asyncResult_41;
	// System.Net.FtpWebResponse System.Net.FtpWebRequest::ftpResponse
	FtpWebResponse_t3940763575 * ___ftpResponse_42;
	// System.IO.Stream System.Net.FtpWebRequest::requestStream
	Stream_t1273022909 * ___requestStream_43;
	// System.String System.Net.FtpWebRequest::initial_path
	String_t* ___initial_path_44;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::callback
	RemoteCertificateValidationCallback_t3014364904 * ___callback_46;

public:
	inline static int32_t get_offset_of_requestUri_19() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___requestUri_19)); }
	inline Uri_t100236324 * get_requestUri_19() const { return ___requestUri_19; }
	inline Uri_t100236324 ** get_address_of_requestUri_19() { return &___requestUri_19; }
	inline void set_requestUri_19(Uri_t100236324 * value)
	{
		___requestUri_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_19), value);
	}

	inline static int32_t get_offset_of_file_name_20() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___file_name_20)); }
	inline String_t* get_file_name_20() const { return ___file_name_20; }
	inline String_t** get_address_of_file_name_20() { return &___file_name_20; }
	inline void set_file_name_20(String_t* value)
	{
		___file_name_20 = value;
		Il2CppCodeGenWriteBarrier((&___file_name_20), value);
	}

	inline static int32_t get_offset_of_servicePoint_21() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___servicePoint_21)); }
	inline ServicePoint_t2786966844 * get_servicePoint_21() const { return ___servicePoint_21; }
	inline ServicePoint_t2786966844 ** get_address_of_servicePoint_21() { return &___servicePoint_21; }
	inline void set_servicePoint_21(ServicePoint_t2786966844 * value)
	{
		___servicePoint_21 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_21), value);
	}

	inline static int32_t get_offset_of_origDataStream_22() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___origDataStream_22)); }
	inline Stream_t1273022909 * get_origDataStream_22() const { return ___origDataStream_22; }
	inline Stream_t1273022909 ** get_address_of_origDataStream_22() { return &___origDataStream_22; }
	inline void set_origDataStream_22(Stream_t1273022909 * value)
	{
		___origDataStream_22 = value;
		Il2CppCodeGenWriteBarrier((&___origDataStream_22), value);
	}

	inline static int32_t get_offset_of_dataStream_23() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___dataStream_23)); }
	inline Stream_t1273022909 * get_dataStream_23() const { return ___dataStream_23; }
	inline Stream_t1273022909 ** get_address_of_dataStream_23() { return &___dataStream_23; }
	inline void set_dataStream_23(Stream_t1273022909 * value)
	{
		___dataStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___dataStream_23), value);
	}

	inline static int32_t get_offset_of_controlStream_24() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___controlStream_24)); }
	inline Stream_t1273022909 * get_controlStream_24() const { return ___controlStream_24; }
	inline Stream_t1273022909 ** get_address_of_controlStream_24() { return &___controlStream_24; }
	inline void set_controlStream_24(Stream_t1273022909 * value)
	{
		___controlStream_24 = value;
		Il2CppCodeGenWriteBarrier((&___controlStream_24), value);
	}

	inline static int32_t get_offset_of_controlReader_25() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___controlReader_25)); }
	inline StreamReader_t4009935899 * get_controlReader_25() const { return ___controlReader_25; }
	inline StreamReader_t4009935899 ** get_address_of_controlReader_25() { return &___controlReader_25; }
	inline void set_controlReader_25(StreamReader_t4009935899 * value)
	{
		___controlReader_25 = value;
		Il2CppCodeGenWriteBarrier((&___controlReader_25), value);
	}

	inline static int32_t get_offset_of_credentials_26() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___credentials_26)); }
	inline NetworkCredential_t3282608323 * get_credentials_26() const { return ___credentials_26; }
	inline NetworkCredential_t3282608323 ** get_address_of_credentials_26() { return &___credentials_26; }
	inline void set_credentials_26(NetworkCredential_t3282608323 * value)
	{
		___credentials_26 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_26), value);
	}

	inline static int32_t get_offset_of_hostEntry_27() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___hostEntry_27)); }
	inline IPHostEntry_t263743900 * get_hostEntry_27() const { return ___hostEntry_27; }
	inline IPHostEntry_t263743900 ** get_address_of_hostEntry_27() { return &___hostEntry_27; }
	inline void set_hostEntry_27(IPHostEntry_t263743900 * value)
	{
		___hostEntry_27 = value;
		Il2CppCodeGenWriteBarrier((&___hostEntry_27), value);
	}

	inline static int32_t get_offset_of_localEndPoint_28() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___localEndPoint_28)); }
	inline IPEndPoint_t3791887218 * get_localEndPoint_28() const { return ___localEndPoint_28; }
	inline IPEndPoint_t3791887218 ** get_address_of_localEndPoint_28() { return &___localEndPoint_28; }
	inline void set_localEndPoint_28(IPEndPoint_t3791887218 * value)
	{
		___localEndPoint_28 = value;
		Il2CppCodeGenWriteBarrier((&___localEndPoint_28), value);
	}

	inline static int32_t get_offset_of_proxy_29() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___proxy_29)); }
	inline RuntimeObject* get_proxy_29() const { return ___proxy_29; }
	inline RuntimeObject** get_address_of_proxy_29() { return &___proxy_29; }
	inline void set_proxy_29(RuntimeObject* value)
	{
		___proxy_29 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_29), value);
	}

	inline static int32_t get_offset_of_timeout_30() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___timeout_30)); }
	inline int32_t get_timeout_30() const { return ___timeout_30; }
	inline int32_t* get_address_of_timeout_30() { return &___timeout_30; }
	inline void set_timeout_30(int32_t value)
	{
		___timeout_30 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_31() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___rwTimeout_31)); }
	inline int32_t get_rwTimeout_31() const { return ___rwTimeout_31; }
	inline int32_t* get_address_of_rwTimeout_31() { return &___rwTimeout_31; }
	inline void set_rwTimeout_31(int32_t value)
	{
		___rwTimeout_31 = value;
	}

	inline static int32_t get_offset_of_offset_32() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___offset_32)); }
	inline int64_t get_offset_32() const { return ___offset_32; }
	inline int64_t* get_address_of_offset_32() { return &___offset_32; }
	inline void set_offset_32(int64_t value)
	{
		___offset_32 = value;
	}

	inline static int32_t get_offset_of_binary_33() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___binary_33)); }
	inline bool get_binary_33() const { return ___binary_33; }
	inline bool* get_address_of_binary_33() { return &___binary_33; }
	inline void set_binary_33(bool value)
	{
		___binary_33 = value;
	}

	inline static int32_t get_offset_of_enableSsl_34() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___enableSsl_34)); }
	inline bool get_enableSsl_34() const { return ___enableSsl_34; }
	inline bool* get_address_of_enableSsl_34() { return &___enableSsl_34; }
	inline void set_enableSsl_34(bool value)
	{
		___enableSsl_34 = value;
	}

	inline static int32_t get_offset_of_usePassive_35() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___usePassive_35)); }
	inline bool get_usePassive_35() const { return ___usePassive_35; }
	inline bool* get_address_of_usePassive_35() { return &___usePassive_35; }
	inline void set_usePassive_35(bool value)
	{
		___usePassive_35 = value;
	}

	inline static int32_t get_offset_of_keepAlive_36() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___keepAlive_36)); }
	inline bool get_keepAlive_36() const { return ___keepAlive_36; }
	inline bool* get_address_of_keepAlive_36() { return &___keepAlive_36; }
	inline void set_keepAlive_36(bool value)
	{
		___keepAlive_36 = value;
	}

	inline static int32_t get_offset_of_method_37() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___method_37)); }
	inline String_t* get_method_37() const { return ___method_37; }
	inline String_t** get_address_of_method_37() { return &___method_37; }
	inline void set_method_37(String_t* value)
	{
		___method_37 = value;
		Il2CppCodeGenWriteBarrier((&___method_37), value);
	}

	inline static int32_t get_offset_of_renameTo_38() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___renameTo_38)); }
	inline String_t* get_renameTo_38() const { return ___renameTo_38; }
	inline String_t** get_address_of_renameTo_38() { return &___renameTo_38; }
	inline void set_renameTo_38(String_t* value)
	{
		___renameTo_38 = value;
		Il2CppCodeGenWriteBarrier((&___renameTo_38), value);
	}

	inline static int32_t get_offset_of_locker_39() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___locker_39)); }
	inline RuntimeObject * get_locker_39() const { return ___locker_39; }
	inline RuntimeObject ** get_address_of_locker_39() { return &___locker_39; }
	inline void set_locker_39(RuntimeObject * value)
	{
		___locker_39 = value;
		Il2CppCodeGenWriteBarrier((&___locker_39), value);
	}

	inline static int32_t get_offset_of_requestState_40() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___requestState_40)); }
	inline int32_t get_requestState_40() const { return ___requestState_40; }
	inline int32_t* get_address_of_requestState_40() { return &___requestState_40; }
	inline void set_requestState_40(int32_t value)
	{
		___requestState_40 = value;
	}

	inline static int32_t get_offset_of_asyncResult_41() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___asyncResult_41)); }
	inline FtpAsyncResult_t3265664217 * get_asyncResult_41() const { return ___asyncResult_41; }
	inline FtpAsyncResult_t3265664217 ** get_address_of_asyncResult_41() { return &___asyncResult_41; }
	inline void set_asyncResult_41(FtpAsyncResult_t3265664217 * value)
	{
		___asyncResult_41 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_41), value);
	}

	inline static int32_t get_offset_of_ftpResponse_42() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___ftpResponse_42)); }
	inline FtpWebResponse_t3940763575 * get_ftpResponse_42() const { return ___ftpResponse_42; }
	inline FtpWebResponse_t3940763575 ** get_address_of_ftpResponse_42() { return &___ftpResponse_42; }
	inline void set_ftpResponse_42(FtpWebResponse_t3940763575 * value)
	{
		___ftpResponse_42 = value;
		Il2CppCodeGenWriteBarrier((&___ftpResponse_42), value);
	}

	inline static int32_t get_offset_of_requestStream_43() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___requestStream_43)); }
	inline Stream_t1273022909 * get_requestStream_43() const { return ___requestStream_43; }
	inline Stream_t1273022909 ** get_address_of_requestStream_43() { return &___requestStream_43; }
	inline void set_requestStream_43(Stream_t1273022909 * value)
	{
		___requestStream_43 = value;
		Il2CppCodeGenWriteBarrier((&___requestStream_43), value);
	}

	inline static int32_t get_offset_of_initial_path_44() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___initial_path_44)); }
	inline String_t* get_initial_path_44() const { return ___initial_path_44; }
	inline String_t** get_address_of_initial_path_44() { return &___initial_path_44; }
	inline void set_initial_path_44(String_t* value)
	{
		___initial_path_44 = value;
		Il2CppCodeGenWriteBarrier((&___initial_path_44), value);
	}

	inline static int32_t get_offset_of_callback_46() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305, ___callback_46)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_callback_46() const { return ___callback_46; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_callback_46() { return &___callback_46; }
	inline void set_callback_46(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___callback_46 = value;
		Il2CppCodeGenWriteBarrier((&___callback_46), value);
	}
};

struct FtpWebRequest_t1577818305_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t1281789340* ___supportedCommands_45;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.FtpWebRequest::<>f__am$cache1C
	RemoteCertificateValidationCallback_t3014364904 * ___U3CU3Ef__amU24cache1C_47;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.FtpWebRequest::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_48;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.FtpWebRequest::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_49;

public:
	inline static int32_t get_offset_of_supportedCommands_45() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305_StaticFields, ___supportedCommands_45)); }
	inline StringU5BU5D_t1281789340* get_supportedCommands_45() const { return ___supportedCommands_45; }
	inline StringU5BU5D_t1281789340** get_address_of_supportedCommands_45() { return &___supportedCommands_45; }
	inline void set_supportedCommands_45(StringU5BU5D_t1281789340* value)
	{
		___supportedCommands_45 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCommands_45), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_47() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305_StaticFields, ___U3CU3Ef__amU24cache1C_47)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_U3CU3Ef__amU24cache1C_47() const { return ___U3CU3Ef__amU24cache1C_47; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_U3CU3Ef__amU24cache1C_47() { return &___U3CU3Ef__amU24cache1C_47; }
	inline void set_U3CU3Ef__amU24cache1C_47(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___U3CU3Ef__amU24cache1C_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1C_47), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_48() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305_StaticFields, ___U3CU3Ef__switchU24map5_48)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_48() const { return ___U3CU3Ef__switchU24map5_48; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_48() { return &___U3CU3Ef__switchU24map5_48; }
	inline void set_U3CU3Ef__switchU24map5_48(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_48), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_49() { return static_cast<int32_t>(offsetof(FtpWebRequest_t1577818305_StaticFields, ___U3CU3Ef__switchU24map6_49)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_49() const { return ___U3CU3Ef__switchU24map6_49; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_49() { return &___U3CU3Ef__switchU24map6_49; }
	inline void set_U3CU3Ef__switchU24map6_49(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBREQUEST_T1577818305_H
#ifndef FTPWEBRESPONSE_T3940763575_H
#define FTPWEBRESPONSE_T3940763575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebResponse
struct  FtpWebResponse_t3940763575  : public WebResponse_t229922639
{
public:
	// System.IO.Stream System.Net.FtpWebResponse::stream
	Stream_t1273022909 * ___stream_1;
	// System.Uri System.Net.FtpWebResponse::uri
	Uri_t100236324 * ___uri_2;
	// System.Net.FtpStatusCode System.Net.FtpWebResponse::statusCode
	int32_t ___statusCode_3;
	// System.DateTime System.Net.FtpWebResponse::lastModified
	DateTime_t3738529785  ___lastModified_4;
	// System.String System.Net.FtpWebResponse::bannerMessage
	String_t* ___bannerMessage_5;
	// System.String System.Net.FtpWebResponse::welcomeMessage
	String_t* ___welcomeMessage_6;
	// System.String System.Net.FtpWebResponse::exitMessage
	String_t* ___exitMessage_7;
	// System.String System.Net.FtpWebResponse::statusDescription
	String_t* ___statusDescription_8;
	// System.String System.Net.FtpWebResponse::method
	String_t* ___method_9;
	// System.Boolean System.Net.FtpWebResponse::disposed
	bool ___disposed_10;
	// System.Net.FtpWebRequest System.Net.FtpWebResponse::request
	FtpWebRequest_t1577818305 * ___request_11;
	// System.Int64 System.Net.FtpWebResponse::contentLength
	int64_t ___contentLength_12;

public:
	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___uri_2)); }
	inline Uri_t100236324 * get_uri_2() const { return ___uri_2; }
	inline Uri_t100236324 ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t100236324 * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_statusCode_3() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___statusCode_3)); }
	inline int32_t get_statusCode_3() const { return ___statusCode_3; }
	inline int32_t* get_address_of_statusCode_3() { return &___statusCode_3; }
	inline void set_statusCode_3(int32_t value)
	{
		___statusCode_3 = value;
	}

	inline static int32_t get_offset_of_lastModified_4() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___lastModified_4)); }
	inline DateTime_t3738529785  get_lastModified_4() const { return ___lastModified_4; }
	inline DateTime_t3738529785 * get_address_of_lastModified_4() { return &___lastModified_4; }
	inline void set_lastModified_4(DateTime_t3738529785  value)
	{
		___lastModified_4 = value;
	}

	inline static int32_t get_offset_of_bannerMessage_5() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___bannerMessage_5)); }
	inline String_t* get_bannerMessage_5() const { return ___bannerMessage_5; }
	inline String_t** get_address_of_bannerMessage_5() { return &___bannerMessage_5; }
	inline void set_bannerMessage_5(String_t* value)
	{
		___bannerMessage_5 = value;
		Il2CppCodeGenWriteBarrier((&___bannerMessage_5), value);
	}

	inline static int32_t get_offset_of_welcomeMessage_6() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___welcomeMessage_6)); }
	inline String_t* get_welcomeMessage_6() const { return ___welcomeMessage_6; }
	inline String_t** get_address_of_welcomeMessage_6() { return &___welcomeMessage_6; }
	inline void set_welcomeMessage_6(String_t* value)
	{
		___welcomeMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___welcomeMessage_6), value);
	}

	inline static int32_t get_offset_of_exitMessage_7() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___exitMessage_7)); }
	inline String_t* get_exitMessage_7() const { return ___exitMessage_7; }
	inline String_t** get_address_of_exitMessage_7() { return &___exitMessage_7; }
	inline void set_exitMessage_7(String_t* value)
	{
		___exitMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___exitMessage_7), value);
	}

	inline static int32_t get_offset_of_statusDescription_8() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___statusDescription_8)); }
	inline String_t* get_statusDescription_8() const { return ___statusDescription_8; }
	inline String_t** get_address_of_statusDescription_8() { return &___statusDescription_8; }
	inline void set_statusDescription_8(String_t* value)
	{
		___statusDescription_8 = value;
		Il2CppCodeGenWriteBarrier((&___statusDescription_8), value);
	}

	inline static int32_t get_offset_of_method_9() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___method_9)); }
	inline String_t* get_method_9() const { return ___method_9; }
	inline String_t** get_address_of_method_9() { return &___method_9; }
	inline void set_method_9(String_t* value)
	{
		___method_9 = value;
		Il2CppCodeGenWriteBarrier((&___method_9), value);
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_request_11() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___request_11)); }
	inline FtpWebRequest_t1577818305 * get_request_11() const { return ___request_11; }
	inline FtpWebRequest_t1577818305 ** get_address_of_request_11() { return &___request_11; }
	inline void set_request_11(FtpWebRequest_t1577818305 * value)
	{
		___request_11 = value;
		Il2CppCodeGenWriteBarrier((&___request_11), value);
	}

	inline static int32_t get_offset_of_contentLength_12() { return static_cast<int32_t>(offsetof(FtpWebResponse_t3940763575, ___contentLength_12)); }
	inline int64_t get_contentLength_12() const { return ___contentLength_12; }
	inline int64_t* get_address_of_contentLength_12() { return &___contentLength_12; }
	inline void set_contentLength_12(int64_t value)
	{
		___contentLength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPWEBRESPONSE_T3940763575_H
#ifndef READDELEGATE_T4266946825_H
#define READDELEGATE_T4266946825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/ReadDelegate
struct  ReadDelegate_t4266946825  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_T4266946825_H
#ifndef HTTPLISTENEREXCEPTION_T1795897741_H
#define HTTPLISTENEREXCEPTION_T1795897741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerException
struct  HttpListenerException_t1795897741  : public Win32Exception_t3234146298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENEREXCEPTION_T1795897741_H
#ifndef WRITEDELEGATE_T2016697242_H
#define WRITEDELEGATE_T2016697242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream/WriteDelegate
struct  WriteDelegate_t2016697242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_T2016697242_H
#ifndef GETHOSTENTRYNAMECALLBACK_T131649245_H
#define GETHOSTENTRYNAMECALLBACK_T131649245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns/GetHostEntryNameCallback
struct  GetHostEntryNameCallback_t131649245  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTENTRYNAMECALLBACK_T131649245_H
#ifndef RESOLVECALLBACK_T3109944030_H
#define RESOLVECALLBACK_T3109944030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns/ResolveCallback
struct  ResolveCallback_t3109944030  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVECALLBACK_T3109944030_H
#ifndef GETHOSTBYNAMECALLBACK_T3190253673_H
#define GETHOSTBYNAMECALLBACK_T3190253673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns/GetHostByNameCallback
struct  GetHostByNameCallback_t3190253673  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTBYNAMECALLBACK_T3190253673_H
#ifndef GETHOSTADDRESSESCALLBACK_T559770561_H
#define GETHOSTADDRESSESCALLBACK_T559770561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns/GetHostAddressesCallback
struct  GetHostAddressesCallback_t559770561  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTADDRESSESCALLBACK_T559770561_H
#ifndef SERVICEPOINT_T2786966844_H
#define SERVICEPOINT_T2786966844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_t2786966844  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_t100236324 * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t3738529785  ___idleSince_4;
	// System.Version System.Net.ServicePoint::protocolVersion
	Version_t3456873960 * ___protocolVersion_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::certificate
	X509Certificate_t713131622 * ___certificate_6;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::clientCertificate
	X509Certificate_t713131622 * ___clientCertificate_7;
	// System.Net.IPHostEntry System.Net.ServicePoint::host
	IPHostEntry_t263743900 * ___host_8;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_9;
	// System.Collections.Hashtable System.Net.ServicePoint::groups
	Hashtable_t1853889766 * ___groups_10;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_11;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_12;
	// System.Object System.Net.ServicePoint::locker
	RuntimeObject * ___locker_13;
	// System.Object System.Net.ServicePoint::hostE
	RuntimeObject * ___hostE_14;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_15;
	// System.Net.BindIPEndPoint System.Net.ServicePoint::endPointCallback
	BindIPEndPoint_t1029027275 * ___endPointCallback_16;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___uri_0)); }
	inline Uri_t100236324 * get_uri_0() const { return ___uri_0; }
	inline Uri_t100236324 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t100236324 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___idleSince_4)); }
	inline DateTime_t3738529785  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t3738529785 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t3738529785  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_protocolVersion_5() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___protocolVersion_5)); }
	inline Version_t3456873960 * get_protocolVersion_5() const { return ___protocolVersion_5; }
	inline Version_t3456873960 ** get_address_of_protocolVersion_5() { return &___protocolVersion_5; }
	inline void set_protocolVersion_5(Version_t3456873960 * value)
	{
		___protocolVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___protocolVersion_5), value);
	}

	inline static int32_t get_offset_of_certificate_6() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___certificate_6)); }
	inline X509Certificate_t713131622 * get_certificate_6() const { return ___certificate_6; }
	inline X509Certificate_t713131622 ** get_address_of_certificate_6() { return &___certificate_6; }
	inline void set_certificate_6(X509Certificate_t713131622 * value)
	{
		___certificate_6 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_6), value);
	}

	inline static int32_t get_offset_of_clientCertificate_7() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___clientCertificate_7)); }
	inline X509Certificate_t713131622 * get_clientCertificate_7() const { return ___clientCertificate_7; }
	inline X509Certificate_t713131622 ** get_address_of_clientCertificate_7() { return &___clientCertificate_7; }
	inline void set_clientCertificate_7(X509Certificate_t713131622 * value)
	{
		___clientCertificate_7 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_7), value);
	}

	inline static int32_t get_offset_of_host_8() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___host_8)); }
	inline IPHostEntry_t263743900 * get_host_8() const { return ___host_8; }
	inline IPHostEntry_t263743900 ** get_address_of_host_8() { return &___host_8; }
	inline void set_host_8(IPHostEntry_t263743900 * value)
	{
		___host_8 = value;
		Il2CppCodeGenWriteBarrier((&___host_8), value);
	}

	inline static int32_t get_offset_of_usesProxy_9() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___usesProxy_9)); }
	inline bool get_usesProxy_9() const { return ___usesProxy_9; }
	inline bool* get_address_of_usesProxy_9() { return &___usesProxy_9; }
	inline void set_usesProxy_9(bool value)
	{
		___usesProxy_9 = value;
	}

	inline static int32_t get_offset_of_groups_10() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___groups_10)); }
	inline Hashtable_t1853889766 * get_groups_10() const { return ___groups_10; }
	inline Hashtable_t1853889766 ** get_address_of_groups_10() { return &___groups_10; }
	inline void set_groups_10(Hashtable_t1853889766 * value)
	{
		___groups_10 = value;
		Il2CppCodeGenWriteBarrier((&___groups_10), value);
	}

	inline static int32_t get_offset_of_sendContinue_11() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___sendContinue_11)); }
	inline bool get_sendContinue_11() const { return ___sendContinue_11; }
	inline bool* get_address_of_sendContinue_11() { return &___sendContinue_11; }
	inline void set_sendContinue_11(bool value)
	{
		___sendContinue_11 = value;
	}

	inline static int32_t get_offset_of_useConnect_12() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___useConnect_12)); }
	inline bool get_useConnect_12() const { return ___useConnect_12; }
	inline bool* get_address_of_useConnect_12() { return &___useConnect_12; }
	inline void set_useConnect_12(bool value)
	{
		___useConnect_12 = value;
	}

	inline static int32_t get_offset_of_locker_13() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___locker_13)); }
	inline RuntimeObject * get_locker_13() const { return ___locker_13; }
	inline RuntimeObject ** get_address_of_locker_13() { return &___locker_13; }
	inline void set_locker_13(RuntimeObject * value)
	{
		___locker_13 = value;
		Il2CppCodeGenWriteBarrier((&___locker_13), value);
	}

	inline static int32_t get_offset_of_hostE_14() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___hostE_14)); }
	inline RuntimeObject * get_hostE_14() const { return ___hostE_14; }
	inline RuntimeObject ** get_address_of_hostE_14() { return &___hostE_14; }
	inline void set_hostE_14(RuntimeObject * value)
	{
		___hostE_14 = value;
		Il2CppCodeGenWriteBarrier((&___hostE_14), value);
	}

	inline static int32_t get_offset_of_useNagle_15() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___useNagle_15)); }
	inline bool get_useNagle_15() const { return ___useNagle_15; }
	inline bool* get_address_of_useNagle_15() { return &___useNagle_15; }
	inline void set_useNagle_15(bool value)
	{
		___useNagle_15 = value;
	}

	inline static int32_t get_offset_of_endPointCallback_16() { return static_cast<int32_t>(offsetof(ServicePoint_t2786966844, ___endPointCallback_16)); }
	inline BindIPEndPoint_t1029027275 * get_endPointCallback_16() const { return ___endPointCallback_16; }
	inline BindIPEndPoint_t1029027275 ** get_address_of_endPointCallback_16() { return &___endPointCallback_16; }
	inline void set_endPointCallback_16(BindIPEndPoint_t1029027275 * value)
	{
		___endPointCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___endPointCallback_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINT_T2786966844_H
#ifndef GETHOSTENTRYIPCALLBACK_T2549854718_H
#define GETHOSTENTRYIPCALLBACK_T2549854718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns/GetHostEntryIPCallback
struct  GetHostEntryIPCallback_t2549854718  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTENTRYIPCALLBACK_T2549854718_H
#ifndef HTTPWEBREQUEST_T1669436515_H
#define HTTPWEBREQUEST_T1669436515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest
struct  HttpWebRequest_t1669436515  : public WebRequest_t1939381076
{
public:
	// System.Uri System.Net.HttpWebRequest::requestUri
	Uri_t100236324 * ___requestUri_6;
	// System.Uri System.Net.HttpWebRequest::actualUri
	Uri_t100236324 * ___actualUri_7;
	// System.Boolean System.Net.HttpWebRequest::hostChanged
	bool ___hostChanged_8;
	// System.Boolean System.Net.HttpWebRequest::allowAutoRedirect
	bool ___allowAutoRedirect_9;
	// System.Boolean System.Net.HttpWebRequest::allowBuffering
	bool ___allowBuffering_10;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.HttpWebRequest::certificates
	X509CertificateCollection_t3399372417 * ___certificates_11;
	// System.String System.Net.HttpWebRequest::connectionGroup
	String_t* ___connectionGroup_12;
	// System.Int64 System.Net.HttpWebRequest::contentLength
	int64_t ___contentLength_13;
	// System.Net.HttpContinueDelegate System.Net.HttpWebRequest::continueDelegate
	HttpContinueDelegate_t3009151163 * ___continueDelegate_14;
	// System.Net.CookieContainer System.Net.HttpWebRequest::cookieContainer
	CookieContainer_t2331592909 * ___cookieContainer_15;
	// System.Net.ICredentials System.Net.HttpWebRequest::credentials
	RuntimeObject* ___credentials_16;
	// System.Boolean System.Net.HttpWebRequest::haveResponse
	bool ___haveResponse_17;
	// System.Boolean System.Net.HttpWebRequest::haveRequest
	bool ___haveRequest_18;
	// System.Boolean System.Net.HttpWebRequest::requestSent
	bool ___requestSent_19;
	// System.Net.WebHeaderCollection System.Net.HttpWebRequest::webHeaders
	WebHeaderCollection_t1942268960 * ___webHeaders_20;
	// System.Boolean System.Net.HttpWebRequest::keepAlive
	bool ___keepAlive_21;
	// System.Int32 System.Net.HttpWebRequest::maxAutoRedirect
	int32_t ___maxAutoRedirect_22;
	// System.String System.Net.HttpWebRequest::mediaType
	String_t* ___mediaType_23;
	// System.String System.Net.HttpWebRequest::method
	String_t* ___method_24;
	// System.String System.Net.HttpWebRequest::initialMethod
	String_t* ___initialMethod_25;
	// System.Boolean System.Net.HttpWebRequest::pipelined
	bool ___pipelined_26;
	// System.Boolean System.Net.HttpWebRequest::preAuthenticate
	bool ___preAuthenticate_27;
	// System.Boolean System.Net.HttpWebRequest::usedPreAuth
	bool ___usedPreAuth_28;
	// System.Version System.Net.HttpWebRequest::version
	Version_t3456873960 * ___version_29;
	// System.Version System.Net.HttpWebRequest::actualVersion
	Version_t3456873960 * ___actualVersion_30;
	// System.Net.IWebProxy System.Net.HttpWebRequest::proxy
	RuntimeObject* ___proxy_31;
	// System.Boolean System.Net.HttpWebRequest::sendChunked
	bool ___sendChunked_32;
	// System.Net.ServicePoint System.Net.HttpWebRequest::servicePoint
	ServicePoint_t2786966844 * ___servicePoint_33;
	// System.Int32 System.Net.HttpWebRequest::timeout
	int32_t ___timeout_34;
	// System.Net.WebConnectionStream System.Net.HttpWebRequest::writeStream
	WebConnectionStream_t2170064850 * ___writeStream_35;
	// System.Net.HttpWebResponse System.Net.HttpWebRequest::webResponse
	HttpWebResponse_t3286585418 * ___webResponse_36;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncWrite
	WebAsyncResult_t3421962937 * ___asyncWrite_37;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncRead
	WebAsyncResult_t3421962937 * ___asyncRead_38;
	// System.EventHandler System.Net.HttpWebRequest::abortHandler
	EventHandler_t1348719766 * ___abortHandler_39;
	// System.Int32 System.Net.HttpWebRequest::aborted
	int32_t ___aborted_40;
	// System.Boolean System.Net.HttpWebRequest::gotRequestStream
	bool ___gotRequestStream_41;
	// System.Int32 System.Net.HttpWebRequest::redirects
	int32_t ___redirects_42;
	// System.Boolean System.Net.HttpWebRequest::expectContinue
	bool ___expectContinue_43;
	// System.Boolean System.Net.HttpWebRequest::authCompleted
	bool ___authCompleted_44;
	// System.Byte[] System.Net.HttpWebRequest::bodyBuffer
	ByteU5BU5D_t4116647657* ___bodyBuffer_45;
	// System.Int32 System.Net.HttpWebRequest::bodyBufferLength
	int32_t ___bodyBufferLength_46;
	// System.Boolean System.Net.HttpWebRequest::getResponseCalled
	bool ___getResponseCalled_47;
	// System.Exception System.Net.HttpWebRequest::saved_exc
	Exception_t * ___saved_exc_48;
	// System.Object System.Net.HttpWebRequest::locker
	RuntimeObject * ___locker_49;
	// System.Boolean System.Net.HttpWebRequest::is_ntlm_auth
	bool ___is_ntlm_auth_50;
	// System.Boolean System.Net.HttpWebRequest::finished_reading
	bool ___finished_reading_51;
	// System.Net.WebConnection System.Net.HttpWebRequest::WebConnection
	WebConnection_t3982808322 * ___WebConnection_52;
	// System.Net.DecompressionMethods System.Net.HttpWebRequest::auto_decomp
	int32_t ___auto_decomp_53;
	// System.Int32 System.Net.HttpWebRequest::maxResponseHeadersLength
	int32_t ___maxResponseHeadersLength_54;
	// System.Int32 System.Net.HttpWebRequest::readWriteTimeout
	int32_t ___readWriteTimeout_56;
	// System.Boolean System.Net.HttpWebRequest::unsafe_auth_blah
	bool ___unsafe_auth_blah_57;

public:
	inline static int32_t get_offset_of_requestUri_6() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___requestUri_6)); }
	inline Uri_t100236324 * get_requestUri_6() const { return ___requestUri_6; }
	inline Uri_t100236324 ** get_address_of_requestUri_6() { return &___requestUri_6; }
	inline void set_requestUri_6(Uri_t100236324 * value)
	{
		___requestUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestUri_6), value);
	}

	inline static int32_t get_offset_of_actualUri_7() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___actualUri_7)); }
	inline Uri_t100236324 * get_actualUri_7() const { return ___actualUri_7; }
	inline Uri_t100236324 ** get_address_of_actualUri_7() { return &___actualUri_7; }
	inline void set_actualUri_7(Uri_t100236324 * value)
	{
		___actualUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___actualUri_7), value);
	}

	inline static int32_t get_offset_of_hostChanged_8() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___hostChanged_8)); }
	inline bool get_hostChanged_8() const { return ___hostChanged_8; }
	inline bool* get_address_of_hostChanged_8() { return &___hostChanged_8; }
	inline void set_hostChanged_8(bool value)
	{
		___hostChanged_8 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_9() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___allowAutoRedirect_9)); }
	inline bool get_allowAutoRedirect_9() const { return ___allowAutoRedirect_9; }
	inline bool* get_address_of_allowAutoRedirect_9() { return &___allowAutoRedirect_9; }
	inline void set_allowAutoRedirect_9(bool value)
	{
		___allowAutoRedirect_9 = value;
	}

	inline static int32_t get_offset_of_allowBuffering_10() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___allowBuffering_10)); }
	inline bool get_allowBuffering_10() const { return ___allowBuffering_10; }
	inline bool* get_address_of_allowBuffering_10() { return &___allowBuffering_10; }
	inline void set_allowBuffering_10(bool value)
	{
		___allowBuffering_10 = value;
	}

	inline static int32_t get_offset_of_certificates_11() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___certificates_11)); }
	inline X509CertificateCollection_t3399372417 * get_certificates_11() const { return ___certificates_11; }
	inline X509CertificateCollection_t3399372417 ** get_address_of_certificates_11() { return &___certificates_11; }
	inline void set_certificates_11(X509CertificateCollection_t3399372417 * value)
	{
		___certificates_11 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_11), value);
	}

	inline static int32_t get_offset_of_connectionGroup_12() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___connectionGroup_12)); }
	inline String_t* get_connectionGroup_12() const { return ___connectionGroup_12; }
	inline String_t** get_address_of_connectionGroup_12() { return &___connectionGroup_12; }
	inline void set_connectionGroup_12(String_t* value)
	{
		___connectionGroup_12 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_12), value);
	}

	inline static int32_t get_offset_of_contentLength_13() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___contentLength_13)); }
	inline int64_t get_contentLength_13() const { return ___contentLength_13; }
	inline int64_t* get_address_of_contentLength_13() { return &___contentLength_13; }
	inline void set_contentLength_13(int64_t value)
	{
		___contentLength_13 = value;
	}

	inline static int32_t get_offset_of_continueDelegate_14() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___continueDelegate_14)); }
	inline HttpContinueDelegate_t3009151163 * get_continueDelegate_14() const { return ___continueDelegate_14; }
	inline HttpContinueDelegate_t3009151163 ** get_address_of_continueDelegate_14() { return &___continueDelegate_14; }
	inline void set_continueDelegate_14(HttpContinueDelegate_t3009151163 * value)
	{
		___continueDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((&___continueDelegate_14), value);
	}

	inline static int32_t get_offset_of_cookieContainer_15() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___cookieContainer_15)); }
	inline CookieContainer_t2331592909 * get_cookieContainer_15() const { return ___cookieContainer_15; }
	inline CookieContainer_t2331592909 ** get_address_of_cookieContainer_15() { return &___cookieContainer_15; }
	inline void set_cookieContainer_15(CookieContainer_t2331592909 * value)
	{
		___cookieContainer_15 = value;
		Il2CppCodeGenWriteBarrier((&___cookieContainer_15), value);
	}

	inline static int32_t get_offset_of_credentials_16() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___credentials_16)); }
	inline RuntimeObject* get_credentials_16() const { return ___credentials_16; }
	inline RuntimeObject** get_address_of_credentials_16() { return &___credentials_16; }
	inline void set_credentials_16(RuntimeObject* value)
	{
		___credentials_16 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_16), value);
	}

	inline static int32_t get_offset_of_haveResponse_17() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___haveResponse_17)); }
	inline bool get_haveResponse_17() const { return ___haveResponse_17; }
	inline bool* get_address_of_haveResponse_17() { return &___haveResponse_17; }
	inline void set_haveResponse_17(bool value)
	{
		___haveResponse_17 = value;
	}

	inline static int32_t get_offset_of_haveRequest_18() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___haveRequest_18)); }
	inline bool get_haveRequest_18() const { return ___haveRequest_18; }
	inline bool* get_address_of_haveRequest_18() { return &___haveRequest_18; }
	inline void set_haveRequest_18(bool value)
	{
		___haveRequest_18 = value;
	}

	inline static int32_t get_offset_of_requestSent_19() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___requestSent_19)); }
	inline bool get_requestSent_19() const { return ___requestSent_19; }
	inline bool* get_address_of_requestSent_19() { return &___requestSent_19; }
	inline void set_requestSent_19(bool value)
	{
		___requestSent_19 = value;
	}

	inline static int32_t get_offset_of_webHeaders_20() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___webHeaders_20)); }
	inline WebHeaderCollection_t1942268960 * get_webHeaders_20() const { return ___webHeaders_20; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_webHeaders_20() { return &___webHeaders_20; }
	inline void set_webHeaders_20(WebHeaderCollection_t1942268960 * value)
	{
		___webHeaders_20 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_20), value);
	}

	inline static int32_t get_offset_of_keepAlive_21() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___keepAlive_21)); }
	inline bool get_keepAlive_21() const { return ___keepAlive_21; }
	inline bool* get_address_of_keepAlive_21() { return &___keepAlive_21; }
	inline void set_keepAlive_21(bool value)
	{
		___keepAlive_21 = value;
	}

	inline static int32_t get_offset_of_maxAutoRedirect_22() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___maxAutoRedirect_22)); }
	inline int32_t get_maxAutoRedirect_22() const { return ___maxAutoRedirect_22; }
	inline int32_t* get_address_of_maxAutoRedirect_22() { return &___maxAutoRedirect_22; }
	inline void set_maxAutoRedirect_22(int32_t value)
	{
		___maxAutoRedirect_22 = value;
	}

	inline static int32_t get_offset_of_mediaType_23() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___mediaType_23)); }
	inline String_t* get_mediaType_23() const { return ___mediaType_23; }
	inline String_t** get_address_of_mediaType_23() { return &___mediaType_23; }
	inline void set_mediaType_23(String_t* value)
	{
		___mediaType_23 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_23), value);
	}

	inline static int32_t get_offset_of_method_24() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___method_24)); }
	inline String_t* get_method_24() const { return ___method_24; }
	inline String_t** get_address_of_method_24() { return &___method_24; }
	inline void set_method_24(String_t* value)
	{
		___method_24 = value;
		Il2CppCodeGenWriteBarrier((&___method_24), value);
	}

	inline static int32_t get_offset_of_initialMethod_25() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___initialMethod_25)); }
	inline String_t* get_initialMethod_25() const { return ___initialMethod_25; }
	inline String_t** get_address_of_initialMethod_25() { return &___initialMethod_25; }
	inline void set_initialMethod_25(String_t* value)
	{
		___initialMethod_25 = value;
		Il2CppCodeGenWriteBarrier((&___initialMethod_25), value);
	}

	inline static int32_t get_offset_of_pipelined_26() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___pipelined_26)); }
	inline bool get_pipelined_26() const { return ___pipelined_26; }
	inline bool* get_address_of_pipelined_26() { return &___pipelined_26; }
	inline void set_pipelined_26(bool value)
	{
		___pipelined_26 = value;
	}

	inline static int32_t get_offset_of_preAuthenticate_27() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___preAuthenticate_27)); }
	inline bool get_preAuthenticate_27() const { return ___preAuthenticate_27; }
	inline bool* get_address_of_preAuthenticate_27() { return &___preAuthenticate_27; }
	inline void set_preAuthenticate_27(bool value)
	{
		___preAuthenticate_27 = value;
	}

	inline static int32_t get_offset_of_usedPreAuth_28() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___usedPreAuth_28)); }
	inline bool get_usedPreAuth_28() const { return ___usedPreAuth_28; }
	inline bool* get_address_of_usedPreAuth_28() { return &___usedPreAuth_28; }
	inline void set_usedPreAuth_28(bool value)
	{
		___usedPreAuth_28 = value;
	}

	inline static int32_t get_offset_of_version_29() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___version_29)); }
	inline Version_t3456873960 * get_version_29() const { return ___version_29; }
	inline Version_t3456873960 ** get_address_of_version_29() { return &___version_29; }
	inline void set_version_29(Version_t3456873960 * value)
	{
		___version_29 = value;
		Il2CppCodeGenWriteBarrier((&___version_29), value);
	}

	inline static int32_t get_offset_of_actualVersion_30() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___actualVersion_30)); }
	inline Version_t3456873960 * get_actualVersion_30() const { return ___actualVersion_30; }
	inline Version_t3456873960 ** get_address_of_actualVersion_30() { return &___actualVersion_30; }
	inline void set_actualVersion_30(Version_t3456873960 * value)
	{
		___actualVersion_30 = value;
		Il2CppCodeGenWriteBarrier((&___actualVersion_30), value);
	}

	inline static int32_t get_offset_of_proxy_31() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___proxy_31)); }
	inline RuntimeObject* get_proxy_31() const { return ___proxy_31; }
	inline RuntimeObject** get_address_of_proxy_31() { return &___proxy_31; }
	inline void set_proxy_31(RuntimeObject* value)
	{
		___proxy_31 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_31), value);
	}

	inline static int32_t get_offset_of_sendChunked_32() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___sendChunked_32)); }
	inline bool get_sendChunked_32() const { return ___sendChunked_32; }
	inline bool* get_address_of_sendChunked_32() { return &___sendChunked_32; }
	inline void set_sendChunked_32(bool value)
	{
		___sendChunked_32 = value;
	}

	inline static int32_t get_offset_of_servicePoint_33() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___servicePoint_33)); }
	inline ServicePoint_t2786966844 * get_servicePoint_33() const { return ___servicePoint_33; }
	inline ServicePoint_t2786966844 ** get_address_of_servicePoint_33() { return &___servicePoint_33; }
	inline void set_servicePoint_33(ServicePoint_t2786966844 * value)
	{
		___servicePoint_33 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoint_33), value);
	}

	inline static int32_t get_offset_of_timeout_34() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___timeout_34)); }
	inline int32_t get_timeout_34() const { return ___timeout_34; }
	inline int32_t* get_address_of_timeout_34() { return &___timeout_34; }
	inline void set_timeout_34(int32_t value)
	{
		___timeout_34 = value;
	}

	inline static int32_t get_offset_of_writeStream_35() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___writeStream_35)); }
	inline WebConnectionStream_t2170064850 * get_writeStream_35() const { return ___writeStream_35; }
	inline WebConnectionStream_t2170064850 ** get_address_of_writeStream_35() { return &___writeStream_35; }
	inline void set_writeStream_35(WebConnectionStream_t2170064850 * value)
	{
		___writeStream_35 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_35), value);
	}

	inline static int32_t get_offset_of_webResponse_36() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___webResponse_36)); }
	inline HttpWebResponse_t3286585418 * get_webResponse_36() const { return ___webResponse_36; }
	inline HttpWebResponse_t3286585418 ** get_address_of_webResponse_36() { return &___webResponse_36; }
	inline void set_webResponse_36(HttpWebResponse_t3286585418 * value)
	{
		___webResponse_36 = value;
		Il2CppCodeGenWriteBarrier((&___webResponse_36), value);
	}

	inline static int32_t get_offset_of_asyncWrite_37() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___asyncWrite_37)); }
	inline WebAsyncResult_t3421962937 * get_asyncWrite_37() const { return ___asyncWrite_37; }
	inline WebAsyncResult_t3421962937 ** get_address_of_asyncWrite_37() { return &___asyncWrite_37; }
	inline void set_asyncWrite_37(WebAsyncResult_t3421962937 * value)
	{
		___asyncWrite_37 = value;
		Il2CppCodeGenWriteBarrier((&___asyncWrite_37), value);
	}

	inline static int32_t get_offset_of_asyncRead_38() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___asyncRead_38)); }
	inline WebAsyncResult_t3421962937 * get_asyncRead_38() const { return ___asyncRead_38; }
	inline WebAsyncResult_t3421962937 ** get_address_of_asyncRead_38() { return &___asyncRead_38; }
	inline void set_asyncRead_38(WebAsyncResult_t3421962937 * value)
	{
		___asyncRead_38 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRead_38), value);
	}

	inline static int32_t get_offset_of_abortHandler_39() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___abortHandler_39)); }
	inline EventHandler_t1348719766 * get_abortHandler_39() const { return ___abortHandler_39; }
	inline EventHandler_t1348719766 ** get_address_of_abortHandler_39() { return &___abortHandler_39; }
	inline void set_abortHandler_39(EventHandler_t1348719766 * value)
	{
		___abortHandler_39 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_39), value);
	}

	inline static int32_t get_offset_of_aborted_40() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___aborted_40)); }
	inline int32_t get_aborted_40() const { return ___aborted_40; }
	inline int32_t* get_address_of_aborted_40() { return &___aborted_40; }
	inline void set_aborted_40(int32_t value)
	{
		___aborted_40 = value;
	}

	inline static int32_t get_offset_of_gotRequestStream_41() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___gotRequestStream_41)); }
	inline bool get_gotRequestStream_41() const { return ___gotRequestStream_41; }
	inline bool* get_address_of_gotRequestStream_41() { return &___gotRequestStream_41; }
	inline void set_gotRequestStream_41(bool value)
	{
		___gotRequestStream_41 = value;
	}

	inline static int32_t get_offset_of_redirects_42() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___redirects_42)); }
	inline int32_t get_redirects_42() const { return ___redirects_42; }
	inline int32_t* get_address_of_redirects_42() { return &___redirects_42; }
	inline void set_redirects_42(int32_t value)
	{
		___redirects_42 = value;
	}

	inline static int32_t get_offset_of_expectContinue_43() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___expectContinue_43)); }
	inline bool get_expectContinue_43() const { return ___expectContinue_43; }
	inline bool* get_address_of_expectContinue_43() { return &___expectContinue_43; }
	inline void set_expectContinue_43(bool value)
	{
		___expectContinue_43 = value;
	}

	inline static int32_t get_offset_of_authCompleted_44() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___authCompleted_44)); }
	inline bool get_authCompleted_44() const { return ___authCompleted_44; }
	inline bool* get_address_of_authCompleted_44() { return &___authCompleted_44; }
	inline void set_authCompleted_44(bool value)
	{
		___authCompleted_44 = value;
	}

	inline static int32_t get_offset_of_bodyBuffer_45() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___bodyBuffer_45)); }
	inline ByteU5BU5D_t4116647657* get_bodyBuffer_45() const { return ___bodyBuffer_45; }
	inline ByteU5BU5D_t4116647657** get_address_of_bodyBuffer_45() { return &___bodyBuffer_45; }
	inline void set_bodyBuffer_45(ByteU5BU5D_t4116647657* value)
	{
		___bodyBuffer_45 = value;
		Il2CppCodeGenWriteBarrier((&___bodyBuffer_45), value);
	}

	inline static int32_t get_offset_of_bodyBufferLength_46() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___bodyBufferLength_46)); }
	inline int32_t get_bodyBufferLength_46() const { return ___bodyBufferLength_46; }
	inline int32_t* get_address_of_bodyBufferLength_46() { return &___bodyBufferLength_46; }
	inline void set_bodyBufferLength_46(int32_t value)
	{
		___bodyBufferLength_46 = value;
	}

	inline static int32_t get_offset_of_getResponseCalled_47() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___getResponseCalled_47)); }
	inline bool get_getResponseCalled_47() const { return ___getResponseCalled_47; }
	inline bool* get_address_of_getResponseCalled_47() { return &___getResponseCalled_47; }
	inline void set_getResponseCalled_47(bool value)
	{
		___getResponseCalled_47 = value;
	}

	inline static int32_t get_offset_of_saved_exc_48() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___saved_exc_48)); }
	inline Exception_t * get_saved_exc_48() const { return ___saved_exc_48; }
	inline Exception_t ** get_address_of_saved_exc_48() { return &___saved_exc_48; }
	inline void set_saved_exc_48(Exception_t * value)
	{
		___saved_exc_48 = value;
		Il2CppCodeGenWriteBarrier((&___saved_exc_48), value);
	}

	inline static int32_t get_offset_of_locker_49() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___locker_49)); }
	inline RuntimeObject * get_locker_49() const { return ___locker_49; }
	inline RuntimeObject ** get_address_of_locker_49() { return &___locker_49; }
	inline void set_locker_49(RuntimeObject * value)
	{
		___locker_49 = value;
		Il2CppCodeGenWriteBarrier((&___locker_49), value);
	}

	inline static int32_t get_offset_of_is_ntlm_auth_50() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___is_ntlm_auth_50)); }
	inline bool get_is_ntlm_auth_50() const { return ___is_ntlm_auth_50; }
	inline bool* get_address_of_is_ntlm_auth_50() { return &___is_ntlm_auth_50; }
	inline void set_is_ntlm_auth_50(bool value)
	{
		___is_ntlm_auth_50 = value;
	}

	inline static int32_t get_offset_of_finished_reading_51() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___finished_reading_51)); }
	inline bool get_finished_reading_51() const { return ___finished_reading_51; }
	inline bool* get_address_of_finished_reading_51() { return &___finished_reading_51; }
	inline void set_finished_reading_51(bool value)
	{
		___finished_reading_51 = value;
	}

	inline static int32_t get_offset_of_WebConnection_52() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___WebConnection_52)); }
	inline WebConnection_t3982808322 * get_WebConnection_52() const { return ___WebConnection_52; }
	inline WebConnection_t3982808322 ** get_address_of_WebConnection_52() { return &___WebConnection_52; }
	inline void set_WebConnection_52(WebConnection_t3982808322 * value)
	{
		___WebConnection_52 = value;
		Il2CppCodeGenWriteBarrier((&___WebConnection_52), value);
	}

	inline static int32_t get_offset_of_auto_decomp_53() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___auto_decomp_53)); }
	inline int32_t get_auto_decomp_53() const { return ___auto_decomp_53; }
	inline int32_t* get_address_of_auto_decomp_53() { return &___auto_decomp_53; }
	inline void set_auto_decomp_53(int32_t value)
	{
		___auto_decomp_53 = value;
	}

	inline static int32_t get_offset_of_maxResponseHeadersLength_54() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___maxResponseHeadersLength_54)); }
	inline int32_t get_maxResponseHeadersLength_54() const { return ___maxResponseHeadersLength_54; }
	inline int32_t* get_address_of_maxResponseHeadersLength_54() { return &___maxResponseHeadersLength_54; }
	inline void set_maxResponseHeadersLength_54(int32_t value)
	{
		___maxResponseHeadersLength_54 = value;
	}

	inline static int32_t get_offset_of_readWriteTimeout_56() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___readWriteTimeout_56)); }
	inline int32_t get_readWriteTimeout_56() const { return ___readWriteTimeout_56; }
	inline int32_t* get_address_of_readWriteTimeout_56() { return &___readWriteTimeout_56; }
	inline void set_readWriteTimeout_56(int32_t value)
	{
		___readWriteTimeout_56 = value;
	}

	inline static int32_t get_offset_of_unsafe_auth_blah_57() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515, ___unsafe_auth_blah_57)); }
	inline bool get_unsafe_auth_blah_57() const { return ___unsafe_auth_blah_57; }
	inline bool* get_address_of_unsafe_auth_blah_57() { return &___unsafe_auth_blah_57; }
	inline void set_unsafe_auth_blah_57(bool value)
	{
		___unsafe_auth_blah_57 = value;
	}
};

struct HttpWebRequest_t1669436515_StaticFields
{
public:
	// System.Int32 System.Net.HttpWebRequest::defaultMaxResponseHeadersLength
	int32_t ___defaultMaxResponseHeadersLength_55;

public:
	inline static int32_t get_offset_of_defaultMaxResponseHeadersLength_55() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1669436515_StaticFields, ___defaultMaxResponseHeadersLength_55)); }
	inline int32_t get_defaultMaxResponseHeadersLength_55() const { return ___defaultMaxResponseHeadersLength_55; }
	inline int32_t* get_address_of_defaultMaxResponseHeadersLength_55() { return &___defaultMaxResponseHeadersLength_55; }
	inline void set_defaultMaxResponseHeadersLength_55(int32_t value)
	{
		___defaultMaxResponseHeadersLength_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUEST_T1669436515_H
#ifndef DIGESTSESSION_T1433627624_H
#define DIGESTSESSION_T1433627624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestSession
struct  DigestSession_t1433627624  : public RuntimeObject
{
public:
	// System.DateTime System.Net.DigestSession::lastUse
	DateTime_t3738529785  ___lastUse_1;
	// System.Int32 System.Net.DigestSession::_nc
	int32_t ____nc_2;
	// System.Security.Cryptography.HashAlgorithm System.Net.DigestSession::hash
	HashAlgorithm_t1432317219 * ___hash_3;
	// System.Net.DigestHeaderParser System.Net.DigestSession::parser
	DigestHeaderParser_t341650829 * ___parser_4;
	// System.String System.Net.DigestSession::_cnonce
	String_t* ____cnonce_5;

public:
	inline static int32_t get_offset_of_lastUse_1() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624, ___lastUse_1)); }
	inline DateTime_t3738529785  get_lastUse_1() const { return ___lastUse_1; }
	inline DateTime_t3738529785 * get_address_of_lastUse_1() { return &___lastUse_1; }
	inline void set_lastUse_1(DateTime_t3738529785  value)
	{
		___lastUse_1 = value;
	}

	inline static int32_t get_offset_of__nc_2() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624, ____nc_2)); }
	inline int32_t get__nc_2() const { return ____nc_2; }
	inline int32_t* get_address_of__nc_2() { return &____nc_2; }
	inline void set__nc_2(int32_t value)
	{
		____nc_2 = value;
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624, ___hash_3)); }
	inline HashAlgorithm_t1432317219 * get_hash_3() const { return ___hash_3; }
	inline HashAlgorithm_t1432317219 ** get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(HashAlgorithm_t1432317219 * value)
	{
		___hash_3 = value;
		Il2CppCodeGenWriteBarrier((&___hash_3), value);
	}

	inline static int32_t get_offset_of_parser_4() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624, ___parser_4)); }
	inline DigestHeaderParser_t341650829 * get_parser_4() const { return ___parser_4; }
	inline DigestHeaderParser_t341650829 ** get_address_of_parser_4() { return &___parser_4; }
	inline void set_parser_4(DigestHeaderParser_t341650829 * value)
	{
		___parser_4 = value;
		Il2CppCodeGenWriteBarrier((&___parser_4), value);
	}

	inline static int32_t get_offset_of__cnonce_5() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624, ____cnonce_5)); }
	inline String_t* get__cnonce_5() const { return ____cnonce_5; }
	inline String_t** get_address_of__cnonce_5() { return &____cnonce_5; }
	inline void set__cnonce_5(String_t* value)
	{
		____cnonce_5 = value;
		Il2CppCodeGenWriteBarrier((&____cnonce_5), value);
	}
};

struct DigestSession_t1433627624_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator System.Net.DigestSession::rng
	RandomNumberGenerator_t386037858 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(DigestSession_t1433627624_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t386037858 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t386037858 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSESSION_T1433627624_H
#ifndef GETRESPONSECALLBACK_T2326689408_H
#define GETRESPONSECALLBACK_T2326689408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest/GetResponseCallback
struct  GetResponseCallback_t2326689408  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRESPONSECALLBACK_T2326689408_H
#ifndef FILEWEBREQUEST_T591858885_H
#define FILEWEBREQUEST_T591858885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest
struct  FileWebRequest_t591858885  : public WebRequest_t1939381076
{
public:
	// System.Uri System.Net.FileWebRequest::uri
	Uri_t100236324 * ___uri_6;
	// System.Net.WebHeaderCollection System.Net.FileWebRequest::webHeaders
	WebHeaderCollection_t1942268960 * ___webHeaders_7;
	// System.Net.ICredentials System.Net.FileWebRequest::credentials
	RuntimeObject* ___credentials_8;
	// System.String System.Net.FileWebRequest::connectionGroup
	String_t* ___connectionGroup_9;
	// System.Int64 System.Net.FileWebRequest::contentLength
	int64_t ___contentLength_10;
	// System.IO.FileAccess System.Net.FileWebRequest::fileAccess
	int32_t ___fileAccess_11;
	// System.String System.Net.FileWebRequest::method
	String_t* ___method_12;
	// System.Net.IWebProxy System.Net.FileWebRequest::proxy
	RuntimeObject* ___proxy_13;
	// System.Boolean System.Net.FileWebRequest::preAuthenticate
	bool ___preAuthenticate_14;
	// System.Int32 System.Net.FileWebRequest::timeout
	int32_t ___timeout_15;
	// System.IO.Stream System.Net.FileWebRequest::requestStream
	Stream_t1273022909 * ___requestStream_16;
	// System.Net.FileWebResponse System.Net.FileWebRequest::webResponse
	FileWebResponse_t544571260 * ___webResponse_17;
	// System.Threading.AutoResetEvent System.Net.FileWebRequest::requestEndEvent
	AutoResetEvent_t1333520283 * ___requestEndEvent_18;
	// System.Boolean System.Net.FileWebRequest::requesting
	bool ___requesting_19;
	// System.Boolean System.Net.FileWebRequest::asyncResponding
	bool ___asyncResponding_20;

public:
	inline static int32_t get_offset_of_uri_6() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___uri_6)); }
	inline Uri_t100236324 * get_uri_6() const { return ___uri_6; }
	inline Uri_t100236324 ** get_address_of_uri_6() { return &___uri_6; }
	inline void set_uri_6(Uri_t100236324 * value)
	{
		___uri_6 = value;
		Il2CppCodeGenWriteBarrier((&___uri_6), value);
	}

	inline static int32_t get_offset_of_webHeaders_7() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___webHeaders_7)); }
	inline WebHeaderCollection_t1942268960 * get_webHeaders_7() const { return ___webHeaders_7; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_webHeaders_7() { return &___webHeaders_7; }
	inline void set_webHeaders_7(WebHeaderCollection_t1942268960 * value)
	{
		___webHeaders_7 = value;
		Il2CppCodeGenWriteBarrier((&___webHeaders_7), value);
	}

	inline static int32_t get_offset_of_credentials_8() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___credentials_8)); }
	inline RuntimeObject* get_credentials_8() const { return ___credentials_8; }
	inline RuntimeObject** get_address_of_credentials_8() { return &___credentials_8; }
	inline void set_credentials_8(RuntimeObject* value)
	{
		___credentials_8 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_8), value);
	}

	inline static int32_t get_offset_of_connectionGroup_9() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___connectionGroup_9)); }
	inline String_t* get_connectionGroup_9() const { return ___connectionGroup_9; }
	inline String_t** get_address_of_connectionGroup_9() { return &___connectionGroup_9; }
	inline void set_connectionGroup_9(String_t* value)
	{
		___connectionGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroup_9), value);
	}

	inline static int32_t get_offset_of_contentLength_10() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___contentLength_10)); }
	inline int64_t get_contentLength_10() const { return ___contentLength_10; }
	inline int64_t* get_address_of_contentLength_10() { return &___contentLength_10; }
	inline void set_contentLength_10(int64_t value)
	{
		___contentLength_10 = value;
	}

	inline static int32_t get_offset_of_fileAccess_11() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___fileAccess_11)); }
	inline int32_t get_fileAccess_11() const { return ___fileAccess_11; }
	inline int32_t* get_address_of_fileAccess_11() { return &___fileAccess_11; }
	inline void set_fileAccess_11(int32_t value)
	{
		___fileAccess_11 = value;
	}

	inline static int32_t get_offset_of_method_12() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___method_12)); }
	inline String_t* get_method_12() const { return ___method_12; }
	inline String_t** get_address_of_method_12() { return &___method_12; }
	inline void set_method_12(String_t* value)
	{
		___method_12 = value;
		Il2CppCodeGenWriteBarrier((&___method_12), value);
	}

	inline static int32_t get_offset_of_proxy_13() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___proxy_13)); }
	inline RuntimeObject* get_proxy_13() const { return ___proxy_13; }
	inline RuntimeObject** get_address_of_proxy_13() { return &___proxy_13; }
	inline void set_proxy_13(RuntimeObject* value)
	{
		___proxy_13 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_13), value);
	}

	inline static int32_t get_offset_of_preAuthenticate_14() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___preAuthenticate_14)); }
	inline bool get_preAuthenticate_14() const { return ___preAuthenticate_14; }
	inline bool* get_address_of_preAuthenticate_14() { return &___preAuthenticate_14; }
	inline void set_preAuthenticate_14(bool value)
	{
		___preAuthenticate_14 = value;
	}

	inline static int32_t get_offset_of_timeout_15() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___timeout_15)); }
	inline int32_t get_timeout_15() const { return ___timeout_15; }
	inline int32_t* get_address_of_timeout_15() { return &___timeout_15; }
	inline void set_timeout_15(int32_t value)
	{
		___timeout_15 = value;
	}

	inline static int32_t get_offset_of_requestStream_16() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___requestStream_16)); }
	inline Stream_t1273022909 * get_requestStream_16() const { return ___requestStream_16; }
	inline Stream_t1273022909 ** get_address_of_requestStream_16() { return &___requestStream_16; }
	inline void set_requestStream_16(Stream_t1273022909 * value)
	{
		___requestStream_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestStream_16), value);
	}

	inline static int32_t get_offset_of_webResponse_17() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___webResponse_17)); }
	inline FileWebResponse_t544571260 * get_webResponse_17() const { return ___webResponse_17; }
	inline FileWebResponse_t544571260 ** get_address_of_webResponse_17() { return &___webResponse_17; }
	inline void set_webResponse_17(FileWebResponse_t544571260 * value)
	{
		___webResponse_17 = value;
		Il2CppCodeGenWriteBarrier((&___webResponse_17), value);
	}

	inline static int32_t get_offset_of_requestEndEvent_18() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___requestEndEvent_18)); }
	inline AutoResetEvent_t1333520283 * get_requestEndEvent_18() const { return ___requestEndEvent_18; }
	inline AutoResetEvent_t1333520283 ** get_address_of_requestEndEvent_18() { return &___requestEndEvent_18; }
	inline void set_requestEndEvent_18(AutoResetEvent_t1333520283 * value)
	{
		___requestEndEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestEndEvent_18), value);
	}

	inline static int32_t get_offset_of_requesting_19() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___requesting_19)); }
	inline bool get_requesting_19() const { return ___requesting_19; }
	inline bool* get_address_of_requesting_19() { return &___requesting_19; }
	inline void set_requesting_19(bool value)
	{
		___requesting_19 = value;
	}

	inline static int32_t get_offset_of_asyncResponding_20() { return static_cast<int32_t>(offsetof(FileWebRequest_t591858885, ___asyncResponding_20)); }
	inline bool get_asyncResponding_20() const { return ___asyncResponding_20; }
	inline bool* get_address_of_asyncResponding_20() { return &___asyncResponding_20; }
	inline void set_asyncResponding_20(bool value)
	{
		___asyncResponding_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUEST_T591858885_H
#ifndef FILEWEBSTREAM_T586107972_H
#define FILEWEBSTREAM_T586107972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest/FileWebStream
struct  FileWebStream_t586107972  : public FileStream_t4292183065
{
public:
	// System.Net.FileWebRequest System.Net.FileWebRequest/FileWebStream::webRequest
	FileWebRequest_t591858885 * ___webRequest_15;

public:
	inline static int32_t get_offset_of_webRequest_15() { return static_cast<int32_t>(offsetof(FileWebStream_t586107972, ___webRequest_15)); }
	inline FileWebRequest_t591858885 * get_webRequest_15() const { return ___webRequest_15; }
	inline FileWebRequest_t591858885 ** get_address_of_webRequest_15() { return &___webRequest_15; }
	inline void set_webRequest_15(FileWebRequest_t591858885 * value)
	{
		___webRequest_15 = value;
		Il2CppCodeGenWriteBarrier((&___webRequest_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBSTREAM_T586107972_H
#ifndef GETREQUESTSTREAMCALLBACK_T1154006112_H
#define GETREQUESTSTREAMCALLBACK_T1154006112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest/GetRequestStreamCallback
struct  GetRequestStreamCallback_t1154006112  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETREQUESTSTREAMCALLBACK_T1154006112_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (CredentialCacheForHostKey_t3132413229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1500[4] = 
{
	CredentialCacheForHostKey_t3132413229::get_offset_of_host_0(),
	CredentialCacheForHostKey_t3132413229::get_offset_of_port_1(),
	CredentialCacheForHostKey_t3132413229::get_offset_of_authType_2(),
	CredentialCacheForHostKey_t3132413229::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (DecompressionMethods_t1612219745)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1501[4] = 
{
	DecompressionMethods_t1612219745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (DefaultCertificatePolicy_t3607119947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (DigestHeaderParser_t341650829), -1, sizeof(DigestHeaderParser_t341650829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1503[5] = 
{
	DigestHeaderParser_t341650829::get_offset_of_header_0(),
	DigestHeaderParser_t341650829::get_offset_of_length_1(),
	DigestHeaderParser_t341650829::get_offset_of_pos_2(),
	DigestHeaderParser_t341650829_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t341650829::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (DigestSession_t1433627624), -1, sizeof(DigestSession_t1433627624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1504[6] = 
{
	DigestSession_t1433627624_StaticFields::get_offset_of_rng_0(),
	DigestSession_t1433627624::get_offset_of_lastUse_1(),
	DigestSession_t1433627624::get_offset_of__nc_2(),
	DigestSession_t1433627624::get_offset_of_hash_3(),
	DigestSession_t1433627624::get_offset_of_parser_4(),
	DigestSession_t1433627624::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (DigestClient_t660790528), -1, sizeof(DigestClient_t660790528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1505[1] = 
{
	DigestClient_t660790528_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (Dns_t384099571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (GetHostByNameCallback_t3190253673), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (ResolveCallback_t3109944030), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (GetHostEntryNameCallback_t131649245), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (GetHostEntryIPCallback_t2549854718), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (GetHostAddressesCallback_t559770561), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (DownloadDataCompletedEventArgs_t1352343852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[1] = 
{
	DownloadDataCompletedEventArgs_t1352343852::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (DownloadProgressChangedEventArgs_t2828131725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[2] = 
{
	DownloadProgressChangedEventArgs_t2828131725::get_offset_of_received_3(),
	DownloadProgressChangedEventArgs_t2828131725::get_offset_of_total_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (DownloadStringCompletedEventArgs_t3792188068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[1] = 
{
	DownloadStringCompletedEventArgs_t3792188068::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (EndPoint_t982345378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (EndPointListener_t2984434924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[8] = 
{
	EndPointListener_t2984434924::get_offset_of_endpoint_0(),
	EndPointListener_t2984434924::get_offset_of_sock_1(),
	EndPointListener_t2984434924::get_offset_of_prefixes_2(),
	EndPointListener_t2984434924::get_offset_of_unhandled_3(),
	EndPointListener_t2984434924::get_offset_of_all_4(),
	EndPointListener_t2984434924::get_offset_of_cert_5(),
	EndPointListener_t2984434924::get_offset_of_key_6(),
	EndPointListener_t2984434924::get_offset_of_secure_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (EndPointManager_t1428684201), -1, sizeof(EndPointManager_t1428684201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	EndPointManager_t1428684201_StaticFields::get_offset_of_ip_to_endpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (FileWebRequest_t591858885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[15] = 
{
	FileWebRequest_t591858885::get_offset_of_uri_6(),
	FileWebRequest_t591858885::get_offset_of_webHeaders_7(),
	FileWebRequest_t591858885::get_offset_of_credentials_8(),
	FileWebRequest_t591858885::get_offset_of_connectionGroup_9(),
	FileWebRequest_t591858885::get_offset_of_contentLength_10(),
	FileWebRequest_t591858885::get_offset_of_fileAccess_11(),
	FileWebRequest_t591858885::get_offset_of_method_12(),
	FileWebRequest_t591858885::get_offset_of_proxy_13(),
	FileWebRequest_t591858885::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t591858885::get_offset_of_timeout_15(),
	FileWebRequest_t591858885::get_offset_of_requestStream_16(),
	FileWebRequest_t591858885::get_offset_of_webResponse_17(),
	FileWebRequest_t591858885::get_offset_of_requestEndEvent_18(),
	FileWebRequest_t591858885::get_offset_of_requesting_19(),
	FileWebRequest_t591858885::get_offset_of_asyncResponding_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (FileWebStream_t586107972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	FileWebStream_t586107972::get_offset_of_webRequest_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (GetRequestStreamCallback_t1154006112), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (GetResponseCallback_t2326689408), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (FileWebRequestCreator_t1781329382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (FileWebResponse_t544571260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[5] = 
{
	FileWebResponse_t544571260::get_offset_of_responseUri_1(),
	FileWebResponse_t544571260::get_offset_of_fileStream_2(),
	FileWebResponse_t544571260::get_offset_of_contentLength_3(),
	FileWebResponse_t544571260::get_offset_of_webHeaders_4(),
	FileWebResponse_t544571260::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (FtpAsyncResult_t3265664217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[9] = 
{
	FtpAsyncResult_t3265664217::get_offset_of_response_0(),
	FtpAsyncResult_t3265664217::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t3265664217::get_offset_of_exception_2(),
	FtpAsyncResult_t3265664217::get_offset_of_callback_3(),
	FtpAsyncResult_t3265664217::get_offset_of_stream_4(),
	FtpAsyncResult_t3265664217::get_offset_of_state_5(),
	FtpAsyncResult_t3265664217::get_offset_of_completed_6(),
	FtpAsyncResult_t3265664217::get_offset_of_synch_7(),
	FtpAsyncResult_t3265664217::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (FtpDataStream_t1366729715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[5] = 
{
	FtpDataStream_t1366729715::get_offset_of_request_1(),
	FtpDataStream_t1366729715::get_offset_of_networkStream_2(),
	FtpDataStream_t1366729715::get_offset_of_disposed_3(),
	FtpDataStream_t1366729715::get_offset_of_isRead_4(),
	FtpDataStream_t1366729715::get_offset_of_totalRead_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (WriteDelegate_t2016697242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (ReadDelegate_t4266946825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (FtpRequestCreator_t2926281497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (FtpStatus_t2376455776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[2] = 
{
	FtpStatus_t2376455776::get_offset_of_statusCode_0(),
	FtpStatus_t2376455776::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (FtpStatusCode_t58879933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1530[38] = 
{
	FtpStatusCode_t58879933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (FtpWebRequest_t1577818305), -1, sizeof(FtpWebRequest_t1577818305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1531[44] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FtpWebRequest_t1577818305::get_offset_of_requestUri_19(),
	FtpWebRequest_t1577818305::get_offset_of_file_name_20(),
	FtpWebRequest_t1577818305::get_offset_of_servicePoint_21(),
	FtpWebRequest_t1577818305::get_offset_of_origDataStream_22(),
	FtpWebRequest_t1577818305::get_offset_of_dataStream_23(),
	FtpWebRequest_t1577818305::get_offset_of_controlStream_24(),
	FtpWebRequest_t1577818305::get_offset_of_controlReader_25(),
	FtpWebRequest_t1577818305::get_offset_of_credentials_26(),
	FtpWebRequest_t1577818305::get_offset_of_hostEntry_27(),
	FtpWebRequest_t1577818305::get_offset_of_localEndPoint_28(),
	FtpWebRequest_t1577818305::get_offset_of_proxy_29(),
	FtpWebRequest_t1577818305::get_offset_of_timeout_30(),
	FtpWebRequest_t1577818305::get_offset_of_rwTimeout_31(),
	FtpWebRequest_t1577818305::get_offset_of_offset_32(),
	FtpWebRequest_t1577818305::get_offset_of_binary_33(),
	FtpWebRequest_t1577818305::get_offset_of_enableSsl_34(),
	FtpWebRequest_t1577818305::get_offset_of_usePassive_35(),
	FtpWebRequest_t1577818305::get_offset_of_keepAlive_36(),
	FtpWebRequest_t1577818305::get_offset_of_method_37(),
	FtpWebRequest_t1577818305::get_offset_of_renameTo_38(),
	FtpWebRequest_t1577818305::get_offset_of_locker_39(),
	FtpWebRequest_t1577818305::get_offset_of_requestState_40(),
	FtpWebRequest_t1577818305::get_offset_of_asyncResult_41(),
	FtpWebRequest_t1577818305::get_offset_of_ftpResponse_42(),
	FtpWebRequest_t1577818305::get_offset_of_requestStream_43(),
	FtpWebRequest_t1577818305::get_offset_of_initial_path_44(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_supportedCommands_45(),
	FtpWebRequest_t1577818305::get_offset_of_callback_46(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_47(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_48(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (RequestState_t4091696808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1532[10] = 
{
	RequestState_t4091696808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (FtpWebResponse_t3940763575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[12] = 
{
	FtpWebResponse_t3940763575::get_offset_of_stream_1(),
	FtpWebResponse_t3940763575::get_offset_of_uri_2(),
	FtpWebResponse_t3940763575::get_offset_of_statusCode_3(),
	FtpWebResponse_t3940763575::get_offset_of_lastModified_4(),
	FtpWebResponse_t3940763575::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t3940763575::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t3940763575::get_offset_of_exitMessage_7(),
	FtpWebResponse_t3940763575::get_offset_of_statusDescription_8(),
	FtpWebResponse_t3940763575::get_offset_of_method_9(),
	FtpWebResponse_t3940763575::get_offset_of_disposed_10(),
	FtpWebResponse_t3940763575::get_offset_of_request_11(),
	FtpWebResponse_t3940763575::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (GlobalProxySelection_t1166292522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (EmptyWebProxy_t3036161737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1535[1] = 
{
	EmptyWebProxy_t3036161737::get_offset_of_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (HttpConnection_t269576101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1536[19] = 
{
	0,
	HttpConnection_t269576101::get_offset_of_sock_1(),
	HttpConnection_t269576101::get_offset_of_stream_2(),
	HttpConnection_t269576101::get_offset_of_epl_3(),
	HttpConnection_t269576101::get_offset_of_ms_4(),
	HttpConnection_t269576101::get_offset_of_buffer_5(),
	HttpConnection_t269576101::get_offset_of_context_6(),
	HttpConnection_t269576101::get_offset_of_current_line_7(),
	HttpConnection_t269576101::get_offset_of_prefix_8(),
	HttpConnection_t269576101::get_offset_of_i_stream_9(),
	HttpConnection_t269576101::get_offset_of_o_stream_10(),
	HttpConnection_t269576101::get_offset_of_chunked_11(),
	HttpConnection_t269576101::get_offset_of_chunked_uses_12(),
	HttpConnection_t269576101::get_offset_of_context_bound_13(),
	HttpConnection_t269576101::get_offset_of_secure_14(),
	HttpConnection_t269576101::get_offset_of_key_15(),
	HttpConnection_t269576101::get_offset_of_input_state_16(),
	HttpConnection_t269576101::get_offset_of_line_state_17(),
	HttpConnection_t269576101::get_offset_of_position_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (InputState_t4051538957)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1537[3] = 
{
	InputState_t4051538957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (LineState_t768597569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1538[4] = 
{
	LineState_t768597569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (HttpListener_t988452056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[11] = 
{
	HttpListener_t988452056::get_offset_of_auth_schemes_0(),
	HttpListener_t988452056::get_offset_of_prefixes_1(),
	HttpListener_t988452056::get_offset_of_auth_selector_2(),
	HttpListener_t988452056::get_offset_of_realm_3(),
	HttpListener_t988452056::get_offset_of_ignore_write_exceptions_4(),
	HttpListener_t988452056::get_offset_of_unsafe_ntlm_auth_5(),
	HttpListener_t988452056::get_offset_of_listening_6(),
	HttpListener_t988452056::get_offset_of_disposed_7(),
	HttpListener_t988452056::get_offset_of_registry_8(),
	HttpListener_t988452056::get_offset_of_ctx_queue_9(),
	HttpListener_t988452056::get_offset_of_wait_queue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (HttpListenerBasicIdentity_t3019963659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1540[1] = 
{
	HttpListenerBasicIdentity_t3019963659::get_offset_of_password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (HttpListenerContext_t424880822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[7] = 
{
	HttpListenerContext_t424880822::get_offset_of_request_0(),
	HttpListenerContext_t424880822::get_offset_of_response_1(),
	HttpListenerContext_t424880822::get_offset_of_user_2(),
	HttpListenerContext_t424880822::get_offset_of_cnc_3(),
	HttpListenerContext_t424880822::get_offset_of_error_4(),
	HttpListenerContext_t424880822::get_offset_of_err_status_5(),
	HttpListenerContext_t424880822::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (HttpListenerException_t1795897741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (HttpListenerPrefixCollection_t2963430373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[2] = 
{
	HttpListenerPrefixCollection_t2963430373::get_offset_of_prefixes_0(),
	HttpListenerPrefixCollection_t2963430373::get_offset_of_listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (HttpListenerRequest_t630699488), -1, sizeof(HttpListenerRequest_t630699488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1544[21] = 
{
	HttpListenerRequest_t630699488::get_offset_of_accept_types_0(),
	HttpListenerRequest_t630699488::get_offset_of_content_encoding_1(),
	HttpListenerRequest_t630699488::get_offset_of_content_length_2(),
	HttpListenerRequest_t630699488::get_offset_of_cl_set_3(),
	HttpListenerRequest_t630699488::get_offset_of_cookies_4(),
	HttpListenerRequest_t630699488::get_offset_of_headers_5(),
	HttpListenerRequest_t630699488::get_offset_of_method_6(),
	HttpListenerRequest_t630699488::get_offset_of_input_stream_7(),
	HttpListenerRequest_t630699488::get_offset_of_version_8(),
	HttpListenerRequest_t630699488::get_offset_of_query_string_9(),
	HttpListenerRequest_t630699488::get_offset_of_raw_url_10(),
	HttpListenerRequest_t630699488::get_offset_of_identifier_11(),
	HttpListenerRequest_t630699488::get_offset_of_url_12(),
	HttpListenerRequest_t630699488::get_offset_of_referrer_13(),
	HttpListenerRequest_t630699488::get_offset_of_user_languages_14(),
	HttpListenerRequest_t630699488::get_offset_of_context_15(),
	HttpListenerRequest_t630699488::get_offset_of_is_chunked_16(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of__100continue_17(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_no_body_methods_18(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_separators_19(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (HttpListenerResponse_t3502667045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[17] = 
{
	HttpListenerResponse_t3502667045::get_offset_of_disposed_0(),
	HttpListenerResponse_t3502667045::get_offset_of_content_encoding_1(),
	HttpListenerResponse_t3502667045::get_offset_of_content_length_2(),
	HttpListenerResponse_t3502667045::get_offset_of_cl_set_3(),
	HttpListenerResponse_t3502667045::get_offset_of_content_type_4(),
	HttpListenerResponse_t3502667045::get_offset_of_cookies_5(),
	HttpListenerResponse_t3502667045::get_offset_of_headers_6(),
	HttpListenerResponse_t3502667045::get_offset_of_keep_alive_7(),
	HttpListenerResponse_t3502667045::get_offset_of_output_stream_8(),
	HttpListenerResponse_t3502667045::get_offset_of_version_9(),
	HttpListenerResponse_t3502667045::get_offset_of_location_10(),
	HttpListenerResponse_t3502667045::get_offset_of_status_code_11(),
	HttpListenerResponse_t3502667045::get_offset_of_status_description_12(),
	HttpListenerResponse_t3502667045::get_offset_of_chunked_13(),
	HttpListenerResponse_t3502667045::get_offset_of_context_14(),
	HttpListenerResponse_t3502667045::get_offset_of_HeadersSent_15(),
	HttpListenerResponse_t3502667045::get_offset_of_force_close_chunked_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (HttpRequestCreator_t1984314013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (HttpRequestHeader_t4258090762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1547[42] = 
{
	HttpRequestHeader_t4258090762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (HttpResponseHeader_t1526164768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1548[31] = 
{
	HttpResponseHeader_t1526164768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (HttpStatusCode_t3035121829)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1549[47] = 
{
	HttpStatusCode_t3035121829::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (HttpStreamAsyncResult_t1178010344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[10] = 
{
	HttpStreamAsyncResult_t1178010344::get_offset_of_locker_0(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_handle_1(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_completed_2(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Buffer_3(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Offset_4(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Count_5(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Callback_6(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_State_7(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_SynchRead_8(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Error_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (HttpUtility_t2559916872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (HttpVersion_t346520293), -1, sizeof(HttpVersion_t346520293_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1552[2] = 
{
	HttpVersion_t346520293_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t346520293_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (HttpWebRequest_t1669436515), -1, sizeof(HttpWebRequest_t1669436515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1553[52] = 
{
	HttpWebRequest_t1669436515::get_offset_of_requestUri_6(),
	HttpWebRequest_t1669436515::get_offset_of_actualUri_7(),
	HttpWebRequest_t1669436515::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1669436515::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1669436515::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1669436515::get_offset_of_certificates_11(),
	HttpWebRequest_t1669436515::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1669436515::get_offset_of_contentLength_13(),
	HttpWebRequest_t1669436515::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t1669436515::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t1669436515::get_offset_of_credentials_16(),
	HttpWebRequest_t1669436515::get_offset_of_haveResponse_17(),
	HttpWebRequest_t1669436515::get_offset_of_haveRequest_18(),
	HttpWebRequest_t1669436515::get_offset_of_requestSent_19(),
	HttpWebRequest_t1669436515::get_offset_of_webHeaders_20(),
	HttpWebRequest_t1669436515::get_offset_of_keepAlive_21(),
	HttpWebRequest_t1669436515::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t1669436515::get_offset_of_mediaType_23(),
	HttpWebRequest_t1669436515::get_offset_of_method_24(),
	HttpWebRequest_t1669436515::get_offset_of_initialMethod_25(),
	HttpWebRequest_t1669436515::get_offset_of_pipelined_26(),
	HttpWebRequest_t1669436515::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t1669436515::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t1669436515::get_offset_of_version_29(),
	HttpWebRequest_t1669436515::get_offset_of_actualVersion_30(),
	HttpWebRequest_t1669436515::get_offset_of_proxy_31(),
	HttpWebRequest_t1669436515::get_offset_of_sendChunked_32(),
	HttpWebRequest_t1669436515::get_offset_of_servicePoint_33(),
	HttpWebRequest_t1669436515::get_offset_of_timeout_34(),
	HttpWebRequest_t1669436515::get_offset_of_writeStream_35(),
	HttpWebRequest_t1669436515::get_offset_of_webResponse_36(),
	HttpWebRequest_t1669436515::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t1669436515::get_offset_of_asyncRead_38(),
	HttpWebRequest_t1669436515::get_offset_of_abortHandler_39(),
	HttpWebRequest_t1669436515::get_offset_of_aborted_40(),
	HttpWebRequest_t1669436515::get_offset_of_gotRequestStream_41(),
	HttpWebRequest_t1669436515::get_offset_of_redirects_42(),
	HttpWebRequest_t1669436515::get_offset_of_expectContinue_43(),
	HttpWebRequest_t1669436515::get_offset_of_authCompleted_44(),
	HttpWebRequest_t1669436515::get_offset_of_bodyBuffer_45(),
	HttpWebRequest_t1669436515::get_offset_of_bodyBufferLength_46(),
	HttpWebRequest_t1669436515::get_offset_of_getResponseCalled_47(),
	HttpWebRequest_t1669436515::get_offset_of_saved_exc_48(),
	HttpWebRequest_t1669436515::get_offset_of_locker_49(),
	HttpWebRequest_t1669436515::get_offset_of_is_ntlm_auth_50(),
	HttpWebRequest_t1669436515::get_offset_of_finished_reading_51(),
	HttpWebRequest_t1669436515::get_offset_of_WebConnection_52(),
	HttpWebRequest_t1669436515::get_offset_of_auto_decomp_53(),
	HttpWebRequest_t1669436515::get_offset_of_maxResponseHeadersLength_54(),
	HttpWebRequest_t1669436515_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_55(),
	HttpWebRequest_t1669436515::get_offset_of_readWriteTimeout_56(),
	HttpWebRequest_t1669436515::get_offset_of_unsafe_auth_blah_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (HttpWebResponse_t3286585418), -1, sizeof(HttpWebResponse_t3286585418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1554[14] = 
{
	HttpWebResponse_t3286585418::get_offset_of_uri_1(),
	HttpWebResponse_t3286585418::get_offset_of_webHeaders_2(),
	HttpWebResponse_t3286585418::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t3286585418::get_offset_of_method_4(),
	HttpWebResponse_t3286585418::get_offset_of_version_5(),
	HttpWebResponse_t3286585418::get_offset_of_statusCode_6(),
	HttpWebResponse_t3286585418::get_offset_of_statusDescription_7(),
	HttpWebResponse_t3286585418::get_offset_of_contentLength_8(),
	HttpWebResponse_t3286585418::get_offset_of_contentType_9(),
	HttpWebResponse_t3286585418::get_offset_of_cookie_container_10(),
	HttpWebResponse_t3286585418::get_offset_of_disposed_11(),
	HttpWebResponse_t3286585418::get_offset_of_stream_12(),
	HttpWebResponse_t3286585418::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t3286585418_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (CookieParser_t2349142305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[3] = 
{
	CookieParser_t2349142305::get_offset_of_header_0(),
	CookieParser_t2349142305::get_offset_of_pos_1(),
	CookieParser_t2349142305::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (IPAddress_t241777590), -1, sizeof(IPAddress_t241777590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1561[12] = 
{
	IPAddress_t241777590::get_offset_of_m_Address_0(),
	IPAddress_t241777590::get_offset_of_m_Family_1(),
	IPAddress_t241777590::get_offset_of_m_Numbers_2(),
	IPAddress_t241777590::get_offset_of_m_ScopeId_3(),
	IPAddress_t241777590_StaticFields::get_offset_of_Any_4(),
	IPAddress_t241777590_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t241777590_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t241777590_StaticFields::get_offset_of_None_7(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6None_10(),
	IPAddress_t241777590::get_offset_of_m_HashCode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (IPEndPoint_t3791887218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1562[4] = 
{
	0,
	0,
	IPEndPoint_t3791887218::get_offset_of_address_2(),
	IPEndPoint_t3791887218::get_offset_of_port_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (IPHostEntry_t263743900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1563[3] = 
{
	IPHostEntry_t263743900::get_offset_of_addressList_0(),
	IPHostEntry_t263743900::get_offset_of_aliases_1(),
	IPHostEntry_t263743900::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (IPv6Address_t2709566769), -1, sizeof(IPv6Address_t2709566769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1564[5] = 
{
	IPv6Address_t2709566769::get_offset_of_address_0(),
	IPv6Address_t2709566769::get_offset_of_prefixLength_1(),
	IPv6Address_t2709566769::get_offset_of_scopeId_2(),
	IPv6Address_t2709566769_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2709566769_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (ListenerAsyncResult_t871495091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[9] = 
{
	ListenerAsyncResult_t871495091::get_offset_of_handle_0(),
	ListenerAsyncResult_t871495091::get_offset_of_synch_1(),
	ListenerAsyncResult_t871495091::get_offset_of_completed_2(),
	ListenerAsyncResult_t871495091::get_offset_of_cb_3(),
	ListenerAsyncResult_t871495091::get_offset_of_state_4(),
	ListenerAsyncResult_t871495091::get_offset_of_exception_5(),
	ListenerAsyncResult_t871495091::get_offset_of_context_6(),
	ListenerAsyncResult_t871495091::get_offset_of_locker_7(),
	ListenerAsyncResult_t871495091::get_offset_of_forward_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (ListenerPrefix_t3570496559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[7] = 
{
	ListenerPrefix_t3570496559::get_offset_of_original_0(),
	ListenerPrefix_t3570496559::get_offset_of_host_1(),
	ListenerPrefix_t3570496559::get_offset_of_port_2(),
	ListenerPrefix_t3570496559::get_offset_of_path_3(),
	ListenerPrefix_t3570496559::get_offset_of_secure_4(),
	ListenerPrefix_t3570496559::get_offset_of_addresses_5(),
	ListenerPrefix_t3570496559::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (MonoHttpDate_t4033147742), -1, sizeof(MonoHttpDate_t4033147742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1570[4] = 
{
	MonoHttpDate_t4033147742_StaticFields::get_offset_of_rfc1123_date_0(),
	MonoHttpDate_t4033147742_StaticFields::get_offset_of_rfc850_date_1(),
	MonoHttpDate_t4033147742_StaticFields::get_offset_of_asctime_date_2(),
	MonoHttpDate_t4033147742_StaticFields::get_offset_of_formats_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (NetConfig_t2828594564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[2] = 
{
	NetConfig_t2828594564::get_offset_of_ipv6Enabled_0(),
	NetConfig_t2828594564::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (NetworkAccess_t3596492016)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1572[3] = 
{
	NetworkAccess_t3596492016::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (NetworkCredential_t3282608323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[3] = 
{
	NetworkCredential_t3282608323::get_offset_of_userName_0(),
	NetworkCredential_t3282608323::get_offset_of_password_1(),
	NetworkCredential_t3282608323::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (OpenReadCompletedEventArgs_t3114398403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[1] = 
{
	OpenReadCompletedEventArgs_t3114398403::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (OpenWriteCompletedEventArgs_t217345581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[1] = 
{
	OpenWriteCompletedEventArgs_t217345581::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (ProtocolViolationException_t4144007430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (RequestStream_t762880582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[6] = 
{
	RequestStream_t762880582::get_offset_of_buffer_1(),
	RequestStream_t762880582::get_offset_of_offset_2(),
	RequestStream_t762880582::get_offset_of_length_3(),
	RequestStream_t762880582::get_offset_of_remaining_body_4(),
	RequestStream_t762880582::get_offset_of_disposed_5(),
	RequestStream_t762880582::get_offset_of_stream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (ResponseStream_t3810703494), -1, sizeof(ResponseStream_t3810703494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1578[6] = 
{
	ResponseStream_t3810703494::get_offset_of_response_1(),
	ResponseStream_t3810703494::get_offset_of_ignore_errors_2(),
	ResponseStream_t3810703494::get_offset_of_disposed_3(),
	ResponseStream_t3810703494::get_offset_of_trailer_sent_4(),
	ResponseStream_t3810703494::get_offset_of_stream_5(),
	ResponseStream_t3810703494_StaticFields::get_offset_of_crlf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (SecurityProtocolType_t2721465497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1579[3] = 
{
	SecurityProtocolType_t2721465497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (ServicePoint_t2786966844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[17] = 
{
	ServicePoint_t2786966844::get_offset_of_uri_0(),
	ServicePoint_t2786966844::get_offset_of_connectionLimit_1(),
	ServicePoint_t2786966844::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2786966844::get_offset_of_currentConnections_3(),
	ServicePoint_t2786966844::get_offset_of_idleSince_4(),
	ServicePoint_t2786966844::get_offset_of_protocolVersion_5(),
	ServicePoint_t2786966844::get_offset_of_certificate_6(),
	ServicePoint_t2786966844::get_offset_of_clientCertificate_7(),
	ServicePoint_t2786966844::get_offset_of_host_8(),
	ServicePoint_t2786966844::get_offset_of_usesProxy_9(),
	ServicePoint_t2786966844::get_offset_of_groups_10(),
	ServicePoint_t2786966844::get_offset_of_sendContinue_11(),
	ServicePoint_t2786966844::get_offset_of_useConnect_12(),
	ServicePoint_t2786966844::get_offset_of_locker_13(),
	ServicePoint_t2786966844::get_offset_of_hostE_14(),
	ServicePoint_t2786966844::get_offset_of_useNagle_15(),
	ServicePoint_t2786966844::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (ServicePointManager_t170559685), -1, sizeof(ServicePointManager_t170559685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1581[12] = 
{
	0,
	0,
	ServicePointManager_t170559685_StaticFields::get_offset_of_servicePoints_2(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_policy_3(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_defaultConnectionLimit_4(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_maxServicePointIdleTime_5(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_maxServicePoints_6(),
	ServicePointManager_t170559685_StaticFields::get_offset_of__checkCRL_7(),
	ServicePointManager_t170559685_StaticFields::get_offset_of__securityProtocol_8(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_expectContinue_9(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_useNagle_10(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_server_cert_cb_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (SPKey_t3654231119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[2] = 
{
	SPKey_t3654231119::get_offset_of_uri_0(),
	SPKey_t3654231119::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (ChainValidationHelper_t320539540), -1, sizeof(ChainValidationHelper_t320539540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1583[4] = 
{
	ChainValidationHelper_t320539540::get_offset_of_sender_0(),
	ChainValidationHelper_t320539540::get_offset_of_host_1(),
	ChainValidationHelper_t320539540_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t320539540_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (SocketAddress_t3739769427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[1] = 
{
	SocketAddress_t3739769427::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (TransportType_t4086112859)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1585[6] = 
{
	TransportType_t4086112859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (UploadDataCompletedEventArgs_t3263315353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[1] = 
{
	UploadDataCompletedEventArgs_t3263315353::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (UploadFileCompletedEventArgs_t4191568555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	UploadFileCompletedEventArgs_t4191568555::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (UploadProgressChangedEventArgs_t1971263489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[4] = 
{
	UploadProgressChangedEventArgs_t1971263489::get_offset_of_received_3(),
	UploadProgressChangedEventArgs_t1971263489::get_offset_of_sent_4(),
	UploadProgressChangedEventArgs_t1971263489::get_offset_of_total_recv_5(),
	UploadProgressChangedEventArgs_t1971263489::get_offset_of_total_send_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (UploadStringCompletedEventArgs_t1649479841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	UploadStringCompletedEventArgs_t1649479841::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (UploadValuesCompletedEventArgs_t3455240456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	UploadValuesCompletedEventArgs_t3455240456::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (WebAsyncResult_t3421962937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[17] = 
{
	WebAsyncResult_t3421962937::get_offset_of_handle_0(),
	WebAsyncResult_t3421962937::get_offset_of_synch_1(),
	WebAsyncResult_t3421962937::get_offset_of_isCompleted_2(),
	WebAsyncResult_t3421962937::get_offset_of_cb_3(),
	WebAsyncResult_t3421962937::get_offset_of_state_4(),
	WebAsyncResult_t3421962937::get_offset_of_nbytes_5(),
	WebAsyncResult_t3421962937::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t3421962937::get_offset_of_callbackDone_7(),
	WebAsyncResult_t3421962937::get_offset_of_exc_8(),
	WebAsyncResult_t3421962937::get_offset_of_response_9(),
	WebAsyncResult_t3421962937::get_offset_of_writeStream_10(),
	WebAsyncResult_t3421962937::get_offset_of_buffer_11(),
	WebAsyncResult_t3421962937::get_offset_of_offset_12(),
	WebAsyncResult_t3421962937::get_offset_of_size_13(),
	WebAsyncResult_t3421962937::get_offset_of_locker_14(),
	WebAsyncResult_t3421962937::get_offset_of_EndCalled_15(),
	WebAsyncResult_t3421962937::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (WebClient_t3506796782), -1, sizeof(WebClient_t3506796782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1592[24] = 
{
	WebClient_t3506796782_StaticFields::get_offset_of_urlEncodedCType_4(),
	WebClient_t3506796782_StaticFields::get_offset_of_hexBytes_5(),
	WebClient_t3506796782::get_offset_of_credentials_6(),
	WebClient_t3506796782::get_offset_of_headers_7(),
	WebClient_t3506796782::get_offset_of_responseHeaders_8(),
	WebClient_t3506796782::get_offset_of_baseAddress_9(),
	WebClient_t3506796782::get_offset_of_baseString_10(),
	WebClient_t3506796782::get_offset_of_queryString_11(),
	WebClient_t3506796782::get_offset_of_is_busy_12(),
	WebClient_t3506796782::get_offset_of_async_13(),
	WebClient_t3506796782::get_offset_of_async_thread_14(),
	WebClient_t3506796782::get_offset_of_encoding_15(),
	WebClient_t3506796782::get_offset_of_proxy_16(),
	WebClient_t3506796782::get_offset_of_DownloadDataCompleted_17(),
	WebClient_t3506796782::get_offset_of_DownloadFileCompleted_18(),
	WebClient_t3506796782::get_offset_of_DownloadProgressChanged_19(),
	WebClient_t3506796782::get_offset_of_DownloadStringCompleted_20(),
	WebClient_t3506796782::get_offset_of_OpenReadCompleted_21(),
	WebClient_t3506796782::get_offset_of_OpenWriteCompleted_22(),
	WebClient_t3506796782::get_offset_of_UploadDataCompleted_23(),
	WebClient_t3506796782::get_offset_of_UploadFileCompleted_24(),
	WebClient_t3506796782::get_offset_of_UploadProgressChanged_25(),
	WebClient_t3506796782::get_offset_of_UploadStringCompleted_26(),
	WebClient_t3506796782::get_offset_of_UploadValuesCompleted_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (ReadState_t245281014)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1593[5] = 
{
	ReadState_t245281014::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (WebConnection_t3982808322), -1, sizeof(WebConnection_t3982808322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1594[32] = 
{
	WebConnection_t3982808322::get_offset_of_sPoint_0(),
	WebConnection_t3982808322::get_offset_of_nstream_1(),
	WebConnection_t3982808322::get_offset_of_socket_2(),
	WebConnection_t3982808322::get_offset_of_socketLock_3(),
	WebConnection_t3982808322::get_offset_of_status_4(),
	WebConnection_t3982808322::get_offset_of_initConn_5(),
	WebConnection_t3982808322::get_offset_of_keepAlive_6(),
	WebConnection_t3982808322::get_offset_of_buffer_7(),
	WebConnection_t3982808322_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t3982808322::get_offset_of_abortHandler_9(),
	WebConnection_t3982808322::get_offset_of_abortHelper_10(),
	WebConnection_t3982808322::get_offset_of_readState_11(),
	WebConnection_t3982808322::get_offset_of_Data_12(),
	WebConnection_t3982808322::get_offset_of_chunkedRead_13(),
	WebConnection_t3982808322::get_offset_of_chunkStream_14(),
	WebConnection_t3982808322::get_offset_of_queue_15(),
	WebConnection_t3982808322::get_offset_of_reused_16(),
	WebConnection_t3982808322::get_offset_of_position_17(),
	WebConnection_t3982808322::get_offset_of_busy_18(),
	WebConnection_t3982808322::get_offset_of_priority_request_19(),
	WebConnection_t3982808322::get_offset_of_ntlm_credentials_20(),
	WebConnection_t3982808322::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t3982808322::get_offset_of_unsafe_sharing_22(),
	WebConnection_t3982808322::get_offset_of_ssl_23(),
	WebConnection_t3982808322::get_offset_of_certsAvailable_24(),
	WebConnection_t3982808322::get_offset_of_connect_exception_25(),
	WebConnection_t3982808322_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t3982808322_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t3982808322_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (AbortHelper_t1490877826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[1] = 
{
	AbortHelper_t1490877826::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (WebConnectionData_t3835660455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[7] = 
{
	WebConnectionData_t3835660455::get_offset_of_request_0(),
	WebConnectionData_t3835660455::get_offset_of_StatusCode_1(),
	WebConnectionData_t3835660455::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3835660455::get_offset_of_Headers_3(),
	WebConnectionData_t3835660455::get_offset_of_Version_4(),
	WebConnectionData_t3835660455::get_offset_of_stream_5(),
	WebConnectionData_t3835660455::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (WebConnectionGroup_t1712379988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[5] = 
{
	WebConnectionGroup_t1712379988::get_offset_of_sPoint_0(),
	WebConnectionGroup_t1712379988::get_offset_of_name_1(),
	WebConnectionGroup_t1712379988::get_offset_of_connections_2(),
	WebConnectionGroup_t1712379988::get_offset_of_rnd_3(),
	WebConnectionGroup_t1712379988::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (WebConnectionStream_t2170064850), -1, sizeof(WebConnectionStream_t2170064850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1598[27] = 
{
	WebConnectionStream_t2170064850_StaticFields::get_offset_of_crlf_1(),
	WebConnectionStream_t2170064850::get_offset_of_isRead_2(),
	WebConnectionStream_t2170064850::get_offset_of_cnc_3(),
	WebConnectionStream_t2170064850::get_offset_of_request_4(),
	WebConnectionStream_t2170064850::get_offset_of_readBuffer_5(),
	WebConnectionStream_t2170064850::get_offset_of_readBufferOffset_6(),
	WebConnectionStream_t2170064850::get_offset_of_readBufferSize_7(),
	WebConnectionStream_t2170064850::get_offset_of_contentLength_8(),
	WebConnectionStream_t2170064850::get_offset_of_totalRead_9(),
	WebConnectionStream_t2170064850::get_offset_of_totalWritten_10(),
	WebConnectionStream_t2170064850::get_offset_of_nextReadCalled_11(),
	WebConnectionStream_t2170064850::get_offset_of_pendingReads_12(),
	WebConnectionStream_t2170064850::get_offset_of_pendingWrites_13(),
	WebConnectionStream_t2170064850::get_offset_of_pending_14(),
	WebConnectionStream_t2170064850::get_offset_of_allowBuffering_15(),
	WebConnectionStream_t2170064850::get_offset_of_sendChunked_16(),
	WebConnectionStream_t2170064850::get_offset_of_writeBuffer_17(),
	WebConnectionStream_t2170064850::get_offset_of_requestWritten_18(),
	WebConnectionStream_t2170064850::get_offset_of_headers_19(),
	WebConnectionStream_t2170064850::get_offset_of_disposed_20(),
	WebConnectionStream_t2170064850::get_offset_of_headersSent_21(),
	WebConnectionStream_t2170064850::get_offset_of_locker_22(),
	WebConnectionStream_t2170064850::get_offset_of_initRead_23(),
	WebConnectionStream_t2170064850::get_offset_of_read_eof_24(),
	WebConnectionStream_t2170064850::get_offset_of_complete_request_written_25(),
	WebConnectionStream_t2170064850::get_offset_of_read_timeout_26(),
	WebConnectionStream_t2170064850::get_offset_of_write_timeout_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (WebException_t3237156354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[2] = 
{
	WebException_t3237156354::get_offset_of_response_12(),
	WebException_t3237156354::get_offset_of_status_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
