﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Object>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4108461761(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2563479586 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Object>::Invoke(T)
#define Func_2_Invoke_m429581751(__this, ___arg10, method) ((  Il2CppObject * (*) (Func_2_t2563479586 *, Task_1_t1872448422 *, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3038165720(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2563479586 *, Task_1_t1872448422 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Object>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1564926033(__this, ___result0, method) ((  Il2CppObject * (*) (Func_2_t2563479586 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
