﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceAlliancesLine
struct AllianceAlliancesLine_t4177759463;
// AllianceAlliancesTableController
struct AllianceAlliancesTableController_t831003683;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AllianceAlliancesTableController831003683.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void AllianceAlliancesLine::.ctor()
extern "C"  void AllianceAlliancesLine__ctor_m420263266 (AllianceAlliancesLine_t4177759463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetOwner(AllianceAlliancesTableController)
extern "C"  void AllianceAlliancesLine_SetOwner_m640495598 (AllianceAlliancesLine_t4177759463 * __this, AllianceAlliancesTableController_t831003683 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetAllianceName(System.String)
extern "C"  void AllianceAlliancesLine_SetAllianceName_m1197295046 (AllianceAlliancesLine_t4177759463 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetFounder(System.String)
extern "C"  void AllianceAlliancesLine_SetFounder_m3211765539 (AllianceAlliancesLine_t4177759463 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetLeader(System.String)
extern "C"  void AllianceAlliancesLine_SetLeader_m781984349 (AllianceAlliancesLine_t4177759463 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetExperience(System.Int64)
extern "C"  void AllianceAlliancesLine_SetExperience_m832595230 (AllianceAlliancesLine_t4177759463 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetRank(System.Int64)
extern "C"  void AllianceAlliancesLine_SetRank_m4080616984 (AllianceAlliancesLine_t4177759463 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetMembers(System.Int64)
extern "C"  void AllianceAlliancesLine_SetMembers_m2783561589 (AllianceAlliancesLine_t4177759463 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetAllianceId(System.Int64)
extern "C"  void AllianceAlliancesLine_SetAllianceId_m2087082210 (AllianceAlliancesLine_t4177759463 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SetJoinable(System.Boolean)
extern "C"  void AllianceAlliancesLine_SetJoinable_m4048077379 (AllianceAlliancesLine_t4177759463 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::SendJoinRequest()
extern "C"  void AllianceAlliancesLine_SendJoinRequest_m3145406309 (AllianceAlliancesLine_t4177759463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesLine::JoinRequestSent(System.Object,System.String)
extern "C"  void AllianceAlliancesLine_JoinRequestSent_m1737079769 (AllianceAlliancesLine_t4177759463 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
