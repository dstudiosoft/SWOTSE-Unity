﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3929486352(__this, ___l0, method) ((  void (*) (Enumerator_t4158447185 *, List_1_t328750215 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1761190162(__this, method) ((  void (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2590746668(__this, method) ((  Il2CppObject * (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::Dispose()
#define Enumerator_Dispose_m1648091127(__this, method) ((  void (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::VerifyState()
#define Enumerator_VerifyState_m1728794402(__this, method) ((  void (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::MoveNext()
#define Enumerator_MoveNext_m481745874(__this, method) ((  bool (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TouchScript.TouchPoint>::get_Current()
#define Enumerator_get_Current_m1553955267(__this, method) ((  TouchPoint_t959629083 * (*) (Enumerator_t4158447185 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
