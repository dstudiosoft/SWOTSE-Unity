﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsModel
struct  UnitsModel_t1926818124  : public Il2CppObject
{
public:
	// System.Int64 UnitsModel::city
	int64_t ___city_0;
	// System.Int64 UnitsModel::workers_count
	int64_t ___workers_count_1;
	// System.Int64 UnitsModel::spy_count
	int64_t ___spy_count_2;
	// System.Int64 UnitsModel::swards_man_count
	int64_t ___swards_man_count_3;
	// System.Int64 UnitsModel::spear_man_count
	int64_t ___spear_man_count_4;
	// System.Int64 UnitsModel::pike_man_count
	int64_t ___pike_man_count_5;
	// System.Int64 UnitsModel::scout_rider_count
	int64_t ___scout_rider_count_6;
	// System.Int64 UnitsModel::light_cavalry_count
	int64_t ___light_cavalry_count_7;
	// System.Int64 UnitsModel::heavy_cavalry_count
	int64_t ___heavy_cavalry_count_8;
	// System.Int64 UnitsModel::archer_count
	int64_t ___archer_count_9;
	// System.Int64 UnitsModel::archer_rider_count
	int64_t ___archer_rider_count_10;
	// System.Int64 UnitsModel::holly_man_count
	int64_t ___holly_man_count_11;
	// System.Int64 UnitsModel::wagon_count
	int64_t ___wagon_count_12;
	// System.Int64 UnitsModel::trebuchet_count
	int64_t ___trebuchet_count_13;
	// System.Int64 UnitsModel::siege_towers_count
	int64_t ___siege_towers_count_14;
	// System.Int64 UnitsModel::battering_ram_count
	int64_t ___battering_ram_count_15;
	// System.Int64 UnitsModel::ballista_count
	int64_t ___ballista_count_16;
	// System.String UnitsModel::army_status
	String_t* ___army_status_17;

public:
	inline static int32_t get_offset_of_city_0() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___city_0)); }
	inline int64_t get_city_0() const { return ___city_0; }
	inline int64_t* get_address_of_city_0() { return &___city_0; }
	inline void set_city_0(int64_t value)
	{
		___city_0 = value;
	}

	inline static int32_t get_offset_of_workers_count_1() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___workers_count_1)); }
	inline int64_t get_workers_count_1() const { return ___workers_count_1; }
	inline int64_t* get_address_of_workers_count_1() { return &___workers_count_1; }
	inline void set_workers_count_1(int64_t value)
	{
		___workers_count_1 = value;
	}

	inline static int32_t get_offset_of_spy_count_2() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___spy_count_2)); }
	inline int64_t get_spy_count_2() const { return ___spy_count_2; }
	inline int64_t* get_address_of_spy_count_2() { return &___spy_count_2; }
	inline void set_spy_count_2(int64_t value)
	{
		___spy_count_2 = value;
	}

	inline static int32_t get_offset_of_swards_man_count_3() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___swards_man_count_3)); }
	inline int64_t get_swards_man_count_3() const { return ___swards_man_count_3; }
	inline int64_t* get_address_of_swards_man_count_3() { return &___swards_man_count_3; }
	inline void set_swards_man_count_3(int64_t value)
	{
		___swards_man_count_3 = value;
	}

	inline static int32_t get_offset_of_spear_man_count_4() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___spear_man_count_4)); }
	inline int64_t get_spear_man_count_4() const { return ___spear_man_count_4; }
	inline int64_t* get_address_of_spear_man_count_4() { return &___spear_man_count_4; }
	inline void set_spear_man_count_4(int64_t value)
	{
		___spear_man_count_4 = value;
	}

	inline static int32_t get_offset_of_pike_man_count_5() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___pike_man_count_5)); }
	inline int64_t get_pike_man_count_5() const { return ___pike_man_count_5; }
	inline int64_t* get_address_of_pike_man_count_5() { return &___pike_man_count_5; }
	inline void set_pike_man_count_5(int64_t value)
	{
		___pike_man_count_5 = value;
	}

	inline static int32_t get_offset_of_scout_rider_count_6() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___scout_rider_count_6)); }
	inline int64_t get_scout_rider_count_6() const { return ___scout_rider_count_6; }
	inline int64_t* get_address_of_scout_rider_count_6() { return &___scout_rider_count_6; }
	inline void set_scout_rider_count_6(int64_t value)
	{
		___scout_rider_count_6 = value;
	}

	inline static int32_t get_offset_of_light_cavalry_count_7() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___light_cavalry_count_7)); }
	inline int64_t get_light_cavalry_count_7() const { return ___light_cavalry_count_7; }
	inline int64_t* get_address_of_light_cavalry_count_7() { return &___light_cavalry_count_7; }
	inline void set_light_cavalry_count_7(int64_t value)
	{
		___light_cavalry_count_7 = value;
	}

	inline static int32_t get_offset_of_heavy_cavalry_count_8() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___heavy_cavalry_count_8)); }
	inline int64_t get_heavy_cavalry_count_8() const { return ___heavy_cavalry_count_8; }
	inline int64_t* get_address_of_heavy_cavalry_count_8() { return &___heavy_cavalry_count_8; }
	inline void set_heavy_cavalry_count_8(int64_t value)
	{
		___heavy_cavalry_count_8 = value;
	}

	inline static int32_t get_offset_of_archer_count_9() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___archer_count_9)); }
	inline int64_t get_archer_count_9() const { return ___archer_count_9; }
	inline int64_t* get_address_of_archer_count_9() { return &___archer_count_9; }
	inline void set_archer_count_9(int64_t value)
	{
		___archer_count_9 = value;
	}

	inline static int32_t get_offset_of_archer_rider_count_10() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___archer_rider_count_10)); }
	inline int64_t get_archer_rider_count_10() const { return ___archer_rider_count_10; }
	inline int64_t* get_address_of_archer_rider_count_10() { return &___archer_rider_count_10; }
	inline void set_archer_rider_count_10(int64_t value)
	{
		___archer_rider_count_10 = value;
	}

	inline static int32_t get_offset_of_holly_man_count_11() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___holly_man_count_11)); }
	inline int64_t get_holly_man_count_11() const { return ___holly_man_count_11; }
	inline int64_t* get_address_of_holly_man_count_11() { return &___holly_man_count_11; }
	inline void set_holly_man_count_11(int64_t value)
	{
		___holly_man_count_11 = value;
	}

	inline static int32_t get_offset_of_wagon_count_12() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___wagon_count_12)); }
	inline int64_t get_wagon_count_12() const { return ___wagon_count_12; }
	inline int64_t* get_address_of_wagon_count_12() { return &___wagon_count_12; }
	inline void set_wagon_count_12(int64_t value)
	{
		___wagon_count_12 = value;
	}

	inline static int32_t get_offset_of_trebuchet_count_13() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___trebuchet_count_13)); }
	inline int64_t get_trebuchet_count_13() const { return ___trebuchet_count_13; }
	inline int64_t* get_address_of_trebuchet_count_13() { return &___trebuchet_count_13; }
	inline void set_trebuchet_count_13(int64_t value)
	{
		___trebuchet_count_13 = value;
	}

	inline static int32_t get_offset_of_siege_towers_count_14() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___siege_towers_count_14)); }
	inline int64_t get_siege_towers_count_14() const { return ___siege_towers_count_14; }
	inline int64_t* get_address_of_siege_towers_count_14() { return &___siege_towers_count_14; }
	inline void set_siege_towers_count_14(int64_t value)
	{
		___siege_towers_count_14 = value;
	}

	inline static int32_t get_offset_of_battering_ram_count_15() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___battering_ram_count_15)); }
	inline int64_t get_battering_ram_count_15() const { return ___battering_ram_count_15; }
	inline int64_t* get_address_of_battering_ram_count_15() { return &___battering_ram_count_15; }
	inline void set_battering_ram_count_15(int64_t value)
	{
		___battering_ram_count_15 = value;
	}

	inline static int32_t get_offset_of_ballista_count_16() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___ballista_count_16)); }
	inline int64_t get_ballista_count_16() const { return ___ballista_count_16; }
	inline int64_t* get_address_of_ballista_count_16() { return &___ballista_count_16; }
	inline void set_ballista_count_16(int64_t value)
	{
		___ballista_count_16 = value;
	}

	inline static int32_t get_offset_of_army_status_17() { return static_cast<int32_t>(offsetof(UnitsModel_t1926818124, ___army_status_17)); }
	inline String_t* get_army_status_17() const { return ___army_status_17; }
	inline String_t** get_address_of_army_status_17() { return &___army_status_17; }
	inline void set_army_status_17(String_t* value)
	{
		___army_status_17 = value;
		Il2CppCodeGenWriteBarrier(&___army_status_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
