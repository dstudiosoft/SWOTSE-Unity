﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextReverse
struct TextReverse_t2630752647;

#include "codegen/il2cpp-codegen.h"

// System.Void TextReverse::.ctor()
extern "C"  void TextReverse__ctor_m4114172610 (TextReverse_t2630752647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextReverse::Start()
extern "C"  void TextReverse_Start_m2820420366 (TextReverse_t2630752647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
