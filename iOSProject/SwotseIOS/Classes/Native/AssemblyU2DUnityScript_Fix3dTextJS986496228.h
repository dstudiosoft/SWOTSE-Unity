﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fix3dTextJS
struct  Fix3dTextJS_t986496228  : public MonoBehaviour_t1158329972
{
public:
	// System.String Fix3dTextJS::text
	String_t* ___text_2;
	// System.Boolean Fix3dTextJS::tashkeel
	bool ___tashkeel_3;
	// System.Boolean Fix3dTextJS::hinduNumbers
	bool ___hinduNumbers_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t986496228, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_tashkeel_3() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t986496228, ___tashkeel_3)); }
	inline bool get_tashkeel_3() const { return ___tashkeel_3; }
	inline bool* get_address_of_tashkeel_3() { return &___tashkeel_3; }
	inline void set_tashkeel_3(bool value)
	{
		___tashkeel_3 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_4() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t986496228, ___hinduNumbers_4)); }
	inline bool get_hinduNumbers_4() const { return ___hinduNumbers_4; }
	inline bool* get_address_of_hinduNumbers_4() { return &___hinduNumbers_4; }
	inline void set_hinduNumbers_4(bool value)
	{
		___hinduNumbers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
