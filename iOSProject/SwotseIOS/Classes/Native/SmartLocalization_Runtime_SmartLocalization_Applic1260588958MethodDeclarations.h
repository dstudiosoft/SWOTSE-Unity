﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SystemLanguage2428294669.h"

// System.String SmartLocalization.ApplicationExtensions::GetSystemLanguage()
extern "C"  String_t* ApplicationExtensions_GetSystemLanguage_m2293450982 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.ApplicationExtensions::GetStringValueOfSystemLanguage(UnityEngine.SystemLanguage)
extern "C"  String_t* ApplicationExtensions_GetStringValueOfSystemLanguage_m4031816973 (Il2CppObject * __this /* static, unused */, int32_t ___systemLanguage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
