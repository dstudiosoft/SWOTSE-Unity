﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityItemLine
struct  CityItemLine_t2575739974  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image CityItemLine::cityImage
	Image_t2042527209 * ___cityImage_2;
	// UnityEngine.UI.Image CityItemLine::backGround
	Image_t2042527209 * ___backGround_3;
	// UnityEngine.UI.Text CityItemLine::coords
	Text_t356221433 * ___coords_4;
	// System.Int64 CityItemLine::cityId
	int64_t ___cityId_5;
	// System.Boolean CityItemLine::currentCity
	bool ___currentCity_6;

public:
	inline static int32_t get_offset_of_cityImage_2() { return static_cast<int32_t>(offsetof(CityItemLine_t2575739974, ___cityImage_2)); }
	inline Image_t2042527209 * get_cityImage_2() const { return ___cityImage_2; }
	inline Image_t2042527209 ** get_address_of_cityImage_2() { return &___cityImage_2; }
	inline void set_cityImage_2(Image_t2042527209 * value)
	{
		___cityImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityImage_2, value);
	}

	inline static int32_t get_offset_of_backGround_3() { return static_cast<int32_t>(offsetof(CityItemLine_t2575739974, ___backGround_3)); }
	inline Image_t2042527209 * get_backGround_3() const { return ___backGround_3; }
	inline Image_t2042527209 ** get_address_of_backGround_3() { return &___backGround_3; }
	inline void set_backGround_3(Image_t2042527209 * value)
	{
		___backGround_3 = value;
		Il2CppCodeGenWriteBarrier(&___backGround_3, value);
	}

	inline static int32_t get_offset_of_coords_4() { return static_cast<int32_t>(offsetof(CityItemLine_t2575739974, ___coords_4)); }
	inline Text_t356221433 * get_coords_4() const { return ___coords_4; }
	inline Text_t356221433 ** get_address_of_coords_4() { return &___coords_4; }
	inline void set_coords_4(Text_t356221433 * value)
	{
		___coords_4 = value;
		Il2CppCodeGenWriteBarrier(&___coords_4, value);
	}

	inline static int32_t get_offset_of_cityId_5() { return static_cast<int32_t>(offsetof(CityItemLine_t2575739974, ___cityId_5)); }
	inline int64_t get_cityId_5() const { return ___cityId_5; }
	inline int64_t* get_address_of_cityId_5() { return &___cityId_5; }
	inline void set_cityId_5(int64_t value)
	{
		___cityId_5 = value;
	}

	inline static int32_t get_offset_of_currentCity_6() { return static_cast<int32_t>(offsetof(CityItemLine_t2575739974, ___currentCity_6)); }
	inline bool get_currentCity_6() const { return ___currentCity_6; }
	inline bool* get_address_of_currentCity_6() { return &___currentCity_6; }
	inline void set_currentCity_6(bool value)
	{
		___currentCity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
