﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DismissContentManager
struct DismissContentManager_t1983743404;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DismissContentManager::.ctor()
extern "C"  void DismissContentManager__ctor_m907928191 (DismissContentManager_t1983743404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DismissContentManager::OnEnable()
extern "C"  void DismissContentManager_OnEnable_m135511347 (DismissContentManager_t1983743404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DismissContentManager::FixedUpdate()
extern "C"  void DismissContentManager_FixedUpdate_m2863705828 (DismissContentManager_t1983743404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DismissContentManager::Dismiss()
extern "C"  void DismissContentManager_Dismiss_m3903071785 (DismissContentManager_t1983743404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DismissContentManager::UnitsDismissed(System.Object,System.String)
extern "C"  void DismissContentManager_UnitsDismissed_m153186453 (DismissContentManager_t1983743404 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
