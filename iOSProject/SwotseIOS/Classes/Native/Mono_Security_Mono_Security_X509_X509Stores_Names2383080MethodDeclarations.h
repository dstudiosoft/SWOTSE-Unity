﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Stores/Names
struct Names_t2383080;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509Stores/Names::.ctor()
extern "C"  void Names__ctor_m3328214384 (Names_t2383080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
