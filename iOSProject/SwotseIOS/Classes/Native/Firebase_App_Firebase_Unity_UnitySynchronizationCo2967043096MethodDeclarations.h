﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4
struct U3CSendU3Ec__AnonStorey4_t2967043096;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey4__ctor_m284691415 (U3CSendU3Ec__AnonStorey4_t2967043096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
