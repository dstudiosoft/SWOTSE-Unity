﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2101210652(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4080619382 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2256312060(__this, method) ((  void (*) (InternalEnumerator_1_t4080619382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3108823696(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4080619382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::Dispose()
#define InternalEnumerator_1_Dispose_m4224656251(__this, method) ((  void (*) (InternalEnumerator_1_t4080619382 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1577824660(__this, method) ((  bool (*) (InternalEnumerator_1_t4080619382 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.Oid>::get_Current()
#define InternalEnumerator_1_get_Current_m772696747(__this, method) ((  Oid_t3221867120 * (*) (InternalEnumerator_1_t4080619382 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
