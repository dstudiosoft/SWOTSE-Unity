﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportItemsLine
struct  BattleReportItemsLine_t301030678  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text BattleReportItemsLine::itemName
	Text_t356221433 * ___itemName_2;
	// UnityEngine.UI.Text BattleReportItemsLine::itemPrice
	Text_t356221433 * ___itemPrice_3;
	// UnityEngine.UI.Text BattleReportItemsLine::itemCount
	Text_t356221433 * ___itemCount_4;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t301030678, ___itemName_2)); }
	inline Text_t356221433 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t356221433 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t356221433 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_2, value);
	}

	inline static int32_t get_offset_of_itemPrice_3() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t301030678, ___itemPrice_3)); }
	inline Text_t356221433 * get_itemPrice_3() const { return ___itemPrice_3; }
	inline Text_t356221433 ** get_address_of_itemPrice_3() { return &___itemPrice_3; }
	inline void set_itemPrice_3(Text_t356221433 * value)
	{
		___itemPrice_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrice_3, value);
	}

	inline static int32_t get_offset_of_itemCount_4() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t301030678, ___itemCount_4)); }
	inline Text_t356221433 * get_itemCount_4() const { return ___itemCount_4; }
	inline Text_t356221433 ** get_address_of_itemCount_4() { return &___itemCount_4; }
	inline void set_itemCount_4(Text_t356221433 * value)
	{
		___itemCount_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemCount_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
