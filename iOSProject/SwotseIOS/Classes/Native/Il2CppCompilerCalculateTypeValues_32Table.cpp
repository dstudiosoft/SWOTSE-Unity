﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Text.StringBuilder
struct StringBuilder_t;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.IDisposable
struct IDisposable_t3640265483;
// JSONObject
struct JSONObject_t1339445639;
// System.String
struct String_t;
// GoogleMobileAds.Common.ICustomNativeTemplateClient
struct ICustomNativeTemplateClient_t2241739572;
// GoogleMobileAds.Common.IInterstitialClient
struct IInterstitialClient_t3845106718;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>
struct EventHandler_1_t780121155;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t2280021157;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// WebSocket
struct WebSocket_t1645401340;
// AllianceChat
struct AllianceChat_t1523197209;
// GoogleMobileAds.Common.IBannerClient
struct IBannerClient_t2365802935;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_t1628180368;
// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>
struct EventHandler_1_t1283979565;
// GoogleMobileAds.Common.IRewardBasedVideoAdClient
struct IRewardBasedVideoAdClient_t3292701488;
// GoogleMobileAds.Common.IMobileAdsClient
struct IMobileAdsClient_t673744088;
// GoogleMobileAds.Common.INativeExpressAdClient
struct INativeExpressAdClient_t1600490704;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t2725803170;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t445075330;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// GoogleMobileAds.Common.IAdLoaderClient
struct IAdLoaderClient_t2960292220;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>>
struct Dictionary_2_t856064493;
// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType>
struct HashSet_1_t324370440;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// EchoTest
struct EchoTest_t2567908100;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// GoogleMobileAds.Api.CustomNativeTemplateAd
struct CustomNativeTemplateAd_t3403582769;
// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidReceiveAdCallback
struct GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075;
// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback
struct GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880;
// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback
struct GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154;
// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidDismissScreenCallback
struct GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787;
// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillLeaveApplicationCallback
struct GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168;
// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback
struct GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418;
// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback
struct GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback
struct GADUAdViewDidReceiveAdCallback_t2543294242;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback
struct GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback
struct GADUAdViewWillPresentScreenCallback_t2057580186;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback
struct GADUAdViewDidDismissScreenCallback_t972393216;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback
struct GADUAdViewWillLeaveApplicationCallback_t3323587265;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback
struct GADURewardBasedVideoAdDidReceiveAdCallback_t462486315;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback
struct GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback
struct GADURewardBasedVideoAdDidOpenCallback_t3638490629;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback
struct GADURewardBasedVideoAdDidStartCallback_t2792276088;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback
struct GADURewardBasedVideoAdDidCloseCallback_t623082069;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback
struct GADURewardBasedVideoAdDidRewardCallback_t990863796;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback
struct GADUInterstitialDidReceiveAdCallback_t821971233;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback
struct GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback
struct GADUInterstitialWillPresentScreenCallback_t539653454;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback
struct GADUInterstitialDidDismissScreenCallback_t1339081348;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback
struct GADUInterstitialWillLeaveApplicationCallback_t1816935820;
// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>
struct Action_2_t1070808194;
// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick
struct GADUNativeCustomTemplateDidReceiveClick_t350204406;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t2811520381;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct List_1_t3723909906;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2584574554;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// ConfirmationEvent
struct ConfirmationEvent_t890979749;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// IReloadable
struct IReloadable_t494058401;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745558_H
#define U3CMODULEU3E_T692745558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745558 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745558_H
#ifndef U3CPRINTASYNCU3EC__ITERATOR1_T4186082390_H
#define U3CPRINTASYNCU3EC__ITERATOR1_T4186082390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/<PrintAsync>c__Iterator1
struct  U3CPrintAsyncU3Ec__Iterator1_t4186082390  : public RuntimeObject
{
public:
	// System.Text.StringBuilder JSONObject/<PrintAsync>c__Iterator1::<builder>__0
	StringBuilder_t * ___U3CbuilderU3E__0_0;
	// System.Boolean JSONObject/<PrintAsync>c__Iterator1::pretty
	bool ___pretty_1;
	// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator1::$locvar0
	RuntimeObject* ___U24locvar0_2;
	// System.Collections.IEnumerable JSONObject/<PrintAsync>c__Iterator1::<e>__1
	RuntimeObject* ___U3CeU3E__1_3;
	// System.IDisposable JSONObject/<PrintAsync>c__Iterator1::$locvar1
	RuntimeObject* ___U24locvar1_4;
	// JSONObject JSONObject/<PrintAsync>c__Iterator1::$this
	JSONObject_t1339445639 * ___U24this_5;
	// System.String JSONObject/<PrintAsync>c__Iterator1::$current
	String_t* ___U24current_6;
	// System.Boolean JSONObject/<PrintAsync>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 JSONObject/<PrintAsync>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CbuilderU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U3CbuilderU3E__0_0)); }
	inline StringBuilder_t * get_U3CbuilderU3E__0_0() const { return ___U3CbuilderU3E__0_0; }
	inline StringBuilder_t ** get_address_of_U3CbuilderU3E__0_0() { return &___U3CbuilderU3E__0_0; }
	inline void set_U3CbuilderU3E__0_0(StringBuilder_t * value)
	{
		___U3CbuilderU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuilderU3E__0_0), value);
	}

	inline static int32_t get_offset_of_pretty_1() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___pretty_1)); }
	inline bool get_pretty_1() const { return ___pretty_1; }
	inline bool* get_address_of_pretty_1() { return &___pretty_1; }
	inline void set_pretty_1(bool value)
	{
		___pretty_1 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24locvar0_2)); }
	inline RuntimeObject* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline RuntimeObject** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(RuntimeObject* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_2), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U3CeU3E__1_3)); }
	inline RuntimeObject* get_U3CeU3E__1_3() const { return ___U3CeU3E__1_3; }
	inline RuntimeObject** get_address_of_U3CeU3E__1_3() { return &___U3CeU3E__1_3; }
	inline void set_U3CeU3E__1_3(RuntimeObject* value)
	{
		___U3CeU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24locvar1_4)); }
	inline RuntimeObject* get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline RuntimeObject** get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(RuntimeObject* value)
	{
		___U24locvar1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24this_5)); }
	inline JSONObject_t1339445639 * get_U24this_5() const { return ___U24this_5; }
	inline JSONObject_t1339445639 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(JSONObject_t1339445639 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24current_6)); }
	inline String_t* get_U24current_6() const { return ___U24current_6; }
	inline String_t** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(String_t* value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CPrintAsyncU3Ec__Iterator1_t4186082390, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRINTASYNCU3EC__ITERATOR1_T4186082390_H
#ifndef CUSTOMNATIVETEMPLATEAD_T3403582769_H
#define CUSTOMNATIVETEMPLATEAD_T3403582769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeTemplateAd
struct  CustomNativeTemplateAd_t3403582769  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.ICustomNativeTemplateClient GoogleMobileAds.Api.CustomNativeTemplateAd::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(CustomNativeTemplateAd_t3403582769, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVETEMPLATEAD_T3403582769_H
#ifndef INTERSTITIALAD_T207380487_H
#define INTERSTITIALAD_T207380487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.InterstitialAd
struct  InterstitialAd_t207380487  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.Api.InterstitialAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdLoaded_1)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdOpening_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALAD_T207380487_H
#ifndef U3CBAKEASYNCU3EC__ITERATOR0_T4027967272_H
#define U3CBAKEASYNCU3EC__ITERATOR0_T4027967272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/<BakeAsync>c__Iterator0
struct  U3CBakeAsyncU3Ec__Iterator0_t4027967272  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<BakeAsync>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// System.String JSONObject/<BakeAsync>c__Iterator0::<s>__1
	String_t* ___U3CsU3E__1_1;
	// JSONObject JSONObject/<BakeAsync>c__Iterator0::$this
	JSONObject_t1339445639 * ___U24this_2;
	// System.Object JSONObject/<BakeAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean JSONObject/<BakeAsync>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 JSONObject/<BakeAsync>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U3CsU3E__1_1)); }
	inline String_t* get_U3CsU3E__1_1() const { return ___U3CsU3E__1_1; }
	inline String_t** get_address_of_U3CsU3E__1_1() { return &___U3CsU3E__1_1; }
	inline void set_U3CsU3E__1_1(String_t* value)
	{
		___U3CsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U24this_2)); }
	inline JSONObject_t1339445639 * get_U24this_2() const { return ___U24this_2; }
	inline JSONObject_t1339445639 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONObject_t1339445639 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBakeAsyncU3Ec__Iterator0_t4027967272, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBAKEASYNCU3EC__ITERATOR0_T4027967272_H
#ifndef JSONTEMPLATES_T2141175947_H
#define JSONTEMPLATES_T2141175947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONTemplates
struct  JSONTemplates_t2141175947  : public RuntimeObject
{
public:

public:
};

struct JSONTemplates_t2141175947_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Object> JSONTemplates::touched
	HashSet_1_t1645055638 * ___touched_0;

public:
	inline static int32_t get_offset_of_touched_0() { return static_cast<int32_t>(offsetof(JSONTemplates_t2141175947_StaticFields, ___touched_0)); }
	inline HashSet_1_t1645055638 * get_touched_0() const { return ___touched_0; }
	inline HashSet_1_t1645055638 ** get_address_of_touched_0() { return &___touched_0; }
	inline void set_touched_0(HashSet_1_t1645055638 * value)
	{
		___touched_0 = value;
		Il2CppCodeGenWriteBarrier((&___touched_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEMPLATES_T2141175947_H
#ifndef U3CBEGINCHATU3EC__ITERATOR0_T1806601407_H
#define U3CBEGINCHATU3EC__ITERATOR0_T1806601407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceChat/<BeginChat>c__Iterator0
struct  U3CBeginChatU3Ec__Iterator0_t1806601407  : public RuntimeObject
{
public:
	// WebSocket AllianceChat/<BeginChat>c__Iterator0::<w>__0
	WebSocket_t1645401340 * ___U3CwU3E__0_0;
	// JSONObject AllianceChat/<BeginChat>c__Iterator0::<initial>__0
	JSONObject_t1339445639 * ___U3CinitialU3E__0_1;
	// System.String AllianceChat/<BeginChat>c__Iterator0::<reply>__1
	String_t* ___U3CreplyU3E__1_2;
	// AllianceChat AllianceChat/<BeginChat>c__Iterator0::$this
	AllianceChat_t1523197209 * ___U24this_3;
	// System.Object AllianceChat/<BeginChat>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceChat/<BeginChat>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceChat/<BeginChat>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U3CwU3E__0_0)); }
	inline WebSocket_t1645401340 * get_U3CwU3E__0_0() const { return ___U3CwU3E__0_0; }
	inline WebSocket_t1645401340 ** get_address_of_U3CwU3E__0_0() { return &___U3CwU3E__0_0; }
	inline void set_U3CwU3E__0_0(WebSocket_t1645401340 * value)
	{
		___U3CwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CinitialU3E__0_1() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U3CinitialU3E__0_1)); }
	inline JSONObject_t1339445639 * get_U3CinitialU3E__0_1() const { return ___U3CinitialU3E__0_1; }
	inline JSONObject_t1339445639 ** get_address_of_U3CinitialU3E__0_1() { return &___U3CinitialU3E__0_1; }
	inline void set_U3CinitialU3E__0_1(JSONObject_t1339445639 * value)
	{
		___U3CinitialU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinitialU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CreplyU3E__1_2() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U3CreplyU3E__1_2)); }
	inline String_t* get_U3CreplyU3E__1_2() const { return ___U3CreplyU3E__1_2; }
	inline String_t** get_address_of_U3CreplyU3E__1_2() { return &___U3CreplyU3E__1_2; }
	inline void set_U3CreplyU3E__1_2(String_t* value)
	{
		___U3CreplyU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreplyU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U24this_3)); }
	inline AllianceChat_t1523197209 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceChat_t1523197209 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceChat_t1523197209 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t1806601407, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINCHATU3EC__ITERATOR0_T1806601407_H
#ifndef BANNERVIEW_T2480907735_H
#define BANNERVIEW_T2480907735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.BannerView
struct  BannerView_t2480907735  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IBannerClient GoogleMobileAds.Api.BannerView::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.BannerView::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdLoaded_1)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdOpening_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERVIEW_T2480907735_H
#ifndef ADSIZE_T2717023072_H
#define ADSIZE_T2717023072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdSize
struct  AdSize_t2717023072  : public RuntimeObject
{
public:
	// System.Boolean GoogleMobileAds.Api.AdSize::isSmartBanner
	bool ___isSmartBanner_0;
	// System.Int32 GoogleMobileAds.Api.AdSize::width
	int32_t ___width_1;
	// System.Int32 GoogleMobileAds.Api.AdSize::height
	int32_t ___height_2;

public:
	inline static int32_t get_offset_of_isSmartBanner_0() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___isSmartBanner_0)); }
	inline bool get_isSmartBanner_0() const { return ___isSmartBanner_0; }
	inline bool* get_address_of_isSmartBanner_0() { return &___isSmartBanner_0; }
	inline void set_isSmartBanner_0(bool value)
	{
		___isSmartBanner_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}
};

struct AdSize_t2717023072_StaticFields
{
public:
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Banner
	AdSize_t2717023072 * ___Banner_3;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::MediumRectangle
	AdSize_t2717023072 * ___MediumRectangle_4;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::IABBanner
	AdSize_t2717023072 * ___IABBanner_5;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Leaderboard
	AdSize_t2717023072 * ___Leaderboard_6;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::SmartBanner
	AdSize_t2717023072 * ___SmartBanner_7;
	// System.Int32 GoogleMobileAds.Api.AdSize::FullWidth
	int32_t ___FullWidth_8;

public:
	inline static int32_t get_offset_of_Banner_3() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___Banner_3)); }
	inline AdSize_t2717023072 * get_Banner_3() const { return ___Banner_3; }
	inline AdSize_t2717023072 ** get_address_of_Banner_3() { return &___Banner_3; }
	inline void set_Banner_3(AdSize_t2717023072 * value)
	{
		___Banner_3 = value;
		Il2CppCodeGenWriteBarrier((&___Banner_3), value);
	}

	inline static int32_t get_offset_of_MediumRectangle_4() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___MediumRectangle_4)); }
	inline AdSize_t2717023072 * get_MediumRectangle_4() const { return ___MediumRectangle_4; }
	inline AdSize_t2717023072 ** get_address_of_MediumRectangle_4() { return &___MediumRectangle_4; }
	inline void set_MediumRectangle_4(AdSize_t2717023072 * value)
	{
		___MediumRectangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___MediumRectangle_4), value);
	}

	inline static int32_t get_offset_of_IABBanner_5() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___IABBanner_5)); }
	inline AdSize_t2717023072 * get_IABBanner_5() const { return ___IABBanner_5; }
	inline AdSize_t2717023072 ** get_address_of_IABBanner_5() { return &___IABBanner_5; }
	inline void set_IABBanner_5(AdSize_t2717023072 * value)
	{
		___IABBanner_5 = value;
		Il2CppCodeGenWriteBarrier((&___IABBanner_5), value);
	}

	inline static int32_t get_offset_of_Leaderboard_6() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___Leaderboard_6)); }
	inline AdSize_t2717023072 * get_Leaderboard_6() const { return ___Leaderboard_6; }
	inline AdSize_t2717023072 ** get_address_of_Leaderboard_6() { return &___Leaderboard_6; }
	inline void set_Leaderboard_6(AdSize_t2717023072 * value)
	{
		___Leaderboard_6 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboard_6), value);
	}

	inline static int32_t get_offset_of_SmartBanner_7() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___SmartBanner_7)); }
	inline AdSize_t2717023072 * get_SmartBanner_7() const { return ___SmartBanner_7; }
	inline AdSize_t2717023072 ** get_address_of_SmartBanner_7() { return &___SmartBanner_7; }
	inline void set_SmartBanner_7(AdSize_t2717023072 * value)
	{
		___SmartBanner_7 = value;
		Il2CppCodeGenWriteBarrier((&___SmartBanner_7), value);
	}

	inline static int32_t get_offset_of_FullWidth_8() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___FullWidth_8)); }
	inline int32_t get_FullWidth_8() const { return ___FullWidth_8; }
	inline int32_t* get_address_of_FullWidth_8() { return &___FullWidth_8; }
	inline void set_FullWidth_8(int32_t value)
	{
		___FullWidth_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSIZE_T2717023072_H
#ifndef MEDIATIONEXTRAS_T2251835164_H
#define MEDIATIONEXTRAS_T2251835164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Mediation.MediationExtras
struct  MediationExtras_t2251835164  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MediationExtras_t2251835164, ___U3CExtrasU3Ek__BackingField_0)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_0() const { return ___U3CExtrasU3Ek__BackingField_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_0() { return &___U3CExtrasU3Ek__BackingField_0; }
	inline void set_U3CExtrasU3Ek__BackingField_0(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATIONEXTRAS_T2251835164_H
#ifndef UTILS_T3548761374_H
#define UTILS_T3548761374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.Utils
struct  Utils_t3548761374  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T3548761374_H
#ifndef DUMMYCLIENT_T519661512_H
#define DUMMYCLIENT_T519661512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.DummyClient
struct  DummyClient_t519661512  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Common.DummyClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Common.DummyClient::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.Common.DummyClient::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_7;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdLoaded_0)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_2() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdOpening_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_2() const { return ___OnAdOpening_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_2() { return &___OnAdOpening_2; }
	inline void set_OnAdOpening_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_2), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_3() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdStarted_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_3() const { return ___OnAdStarted_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_3() { return &___OnAdStarted_3; }
	inline void set_OnAdStarted_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_5() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdRewarded_5)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_5() const { return ___OnAdRewarded_5; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_5() { return &___OnAdRewarded_5; }
	inline void set_OnAdRewarded_5(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_7() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnCustomNativeTemplateAdLoaded_7)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_7() const { return ___OnCustomNativeTemplateAdLoaded_7; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_7() { return &___OnCustomNativeTemplateAdLoaded_7; }
	inline void set_OnCustomNativeTemplateAdLoaded_7(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYCLIENT_T519661512_H
#ifndef MOBILEADSCLIENT_T1008075298_H
#define MOBILEADSCLIENT_T1008075298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.MobileAdsClient
struct  MobileAdsClient_t1008075298  : public RuntimeObject
{
public:

public:
};

struct MobileAdsClient_t1008075298_StaticFields
{
public:
	// GoogleMobileAds.iOS.MobileAdsClient GoogleMobileAds.iOS.MobileAdsClient::instance
	MobileAdsClient_t1008075298 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(MobileAdsClient_t1008075298_StaticFields, ___instance_0)); }
	inline MobileAdsClient_t1008075298 * get_instance_0() const { return ___instance_0; }
	inline MobileAdsClient_t1008075298 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(MobileAdsClient_t1008075298 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADSCLIENT_T1008075298_H
#ifndef GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#define GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.GoogleMobileAdsClientFactory
struct  GoogleMobileAdsClientFactory_t3556675256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#ifndef REWARDBASEDVIDEOAD_T2288675867_H
#define REWARDBASEDVIDEOAD_T2288675867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.RewardBasedVideoAd
struct  RewardBasedVideoAd_t2288675867  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.Api.RewardBasedVideoAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_8;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdStarted_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_5), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdClosed_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_6), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdRewarded_7)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_7), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_8), value);
	}
};

struct RewardBasedVideoAd_t2288675867_StaticFields
{
public:
	// GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::instance
	RewardBasedVideoAd_t2288675867 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867_StaticFields, ___instance_1)); }
	inline RewardBasedVideoAd_t2288675867 * get_instance_1() const { return ___instance_1; }
	inline RewardBasedVideoAd_t2288675867 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(RewardBasedVideoAd_t2288675867 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDBASEDVIDEOAD_T2288675867_H
#ifndef MOBILEADS_T1050225196_H
#define MOBILEADS_T1050225196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.MobileAds
struct  MobileAds_t1050225196  : public RuntimeObject
{
public:

public:
};

struct MobileAds_t1050225196_StaticFields
{
public:
	// GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.Api.MobileAds::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(MobileAds_t1050225196_StaticFields, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADS_T1050225196_H
#ifndef EXTERNS_T92207873_H
#define EXTERNS_T92207873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.Externs
struct  Externs_t92207873  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNS_T92207873_H
#ifndef UTILS_T143735646_H
#define UTILS_T143735646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.Utils
struct  Utils_t143735646  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T143735646_H
#ifndef NATIVEEXPRESSADVIEW_T3596908119_H
#define NATIVEEXPRESSADVIEW_T3596908119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.NativeExpressAdView
struct  NativeExpressAdView_t3596908119  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.INativeExpressAdClient GoogleMobileAds.Api.NativeExpressAdView::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.NativeExpressAdView::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.NativeExpressAdView::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.NativeExpressAdView::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.NativeExpressAdView::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.NativeExpressAdView::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___OnAdLoaded_1)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___OnAdOpening_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(NativeExpressAdView_t3596908119, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEEXPRESSADVIEW_T3596908119_H
#ifndef U3CSENDBUYREQUESTU3EC__ITERATOR0_T3818994470_H
#define U3CSENDBUYREQUESTU3EC__ITERATOR0_T3818994470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyShillingsScript/<SendBuyRequest>c__Iterator0
struct  U3CSendBuyRequestU3Ec__Iterator0_t3818994470  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuyShillingsScript/<SendBuyRequest>c__Iterator0::<requestForm>__0
	WWWForm_t4064702195 * ___U3CrequestFormU3E__0_0;
	// System.String BuyShillingsScript/<SendBuyRequest>c__Iterator0::count
	String_t* ___count_1;
	// UnityEngine.Networking.UnityWebRequest BuyShillingsScript/<SendBuyRequest>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object BuyShillingsScript/<SendBuyRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BuyShillingsScript/<SendBuyRequest>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 BuyShillingsScript/<SendBuyRequest>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___count_1)); }
	inline String_t* get_count_1() const { return ___count_1; }
	inline String_t** get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(String_t* value)
	{
		___count_1 = value;
		Il2CppCodeGenWriteBarrier((&___count_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSendBuyRequestU3Ec__Iterator0_t3818994470, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDBUYREQUESTU3EC__ITERATOR0_T3818994470_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#define U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0
struct  U3CActivateU3Ec__Iterator0_t2664723090  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#define U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1
struct  U3CDeactivateU3Ec__Iterator1_t730025274  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifndef U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#define U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2
struct  U3CReloadLevelU3Ec__Iterator2_t2784493974  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifndef WAYPOINTLIST_T2584574554_H
#define WAYPOINTLIST_T2584574554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2584574554  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t445075330 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t807237628* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___circuit_0)); }
	inline WaypointCircuit_t445075330 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t445075330 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___items_1)); }
	inline TransformU5BU5D_t807237628* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t807237628** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t807237628* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2584574554_H
#ifndef ADLOADER_T3255226653_H
#define ADLOADER_T3255226653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader
struct  AdLoader_t3255226653  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IAdLoaderClient GoogleMobileAds.Api.AdLoader::adLoaderClient
	RuntimeObject* ___adLoaderClient_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.AdLoader::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.Api.AdLoader::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t856064493 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;
	// System.String GoogleMobileAds.Api.AdLoader::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_4;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader::<AdTypes>k__BackingField
	HashSet_1_t324370440 * ___U3CAdTypesU3Ek__BackingField_5;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader::<TemplateIds>k__BackingField
	HashSet_1_t412400163 * ___U3CTemplateIdsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_adLoaderClient_0() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___adLoaderClient_0)); }
	inline RuntimeObject* get_adLoaderClient_0() const { return ___adLoaderClient_0; }
	inline RuntimeObject** get_address_of_adLoaderClient_0() { return &___adLoaderClient_0; }
	inline void set_adLoaderClient_0(RuntimeObject* value)
	{
		___adLoaderClient_0 = value;
		Il2CppCodeGenWriteBarrier((&___adLoaderClient_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_2() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___OnCustomNativeTemplateAdLoaded_2)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_2() const { return ___OnCustomNativeTemplateAdLoaded_2; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_2() { return &___OnCustomNativeTemplateAdLoaded_2; }
	inline void set_OnCustomNativeTemplateAdLoaded_2(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t856064493 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t856064493 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t856064493 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CAdUnitIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_4() const { return ___U3CAdUnitIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_4() { return &___U3CAdUnitIdU3Ek__BackingField_4; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CAdTypesU3Ek__BackingField_5)); }
	inline HashSet_1_t324370440 * get_U3CAdTypesU3Ek__BackingField_5() const { return ___U3CAdTypesU3Ek__BackingField_5; }
	inline HashSet_1_t324370440 ** get_address_of_U3CAdTypesU3Ek__BackingField_5() { return &___U3CAdTypesU3Ek__BackingField_5; }
	inline void set_U3CAdTypesU3Ek__BackingField_5(HashSet_1_t324370440 * value)
	{
		___U3CAdTypesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CTemplateIdsU3Ek__BackingField_6)); }
	inline HashSet_1_t412400163 * get_U3CTemplateIdsU3Ek__BackingField_6() const { return ___U3CTemplateIdsU3Ek__BackingField_6; }
	inline HashSet_1_t412400163 ** get_address_of_U3CTemplateIdsU3Ek__BackingField_6() { return &___U3CTemplateIdsU3Ek__BackingField_6; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_6(HashSet_1_t412400163 * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOADER_T3255226653_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3655621217_H
#define U3CSTARTU3EC__ITERATOR0_T3655621217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3655621217  : public RuntimeObject
{
public:
	// WebSocket EchoTest/<Start>c__Iterator0::<w>__0
	WebSocket_t1645401340 * ___U3CwU3E__0_0;
	// System.Int32 EchoTest/<Start>c__Iterator0::<i>__0
	int32_t ___U3CiU3E__0_1;
	// System.String EchoTest/<Start>c__Iterator0::<reply>__1
	String_t* ___U3CreplyU3E__1_2;
	// EchoTest EchoTest/<Start>c__Iterator0::$this
	EchoTest_t2567908100 * ___U24this_3;
	// System.Object EchoTest/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean EchoTest/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 EchoTest/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U3CwU3E__0_0)); }
	inline WebSocket_t1645401340 * get_U3CwU3E__0_0() const { return ___U3CwU3E__0_0; }
	inline WebSocket_t1645401340 ** get_address_of_U3CwU3E__0_0() { return &___U3CwU3E__0_0; }
	inline void set_U3CwU3E__0_0(WebSocket_t1645401340 * value)
	{
		___U3CwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CreplyU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U3CreplyU3E__1_2)); }
	inline String_t* get_U3CreplyU3E__1_2() const { return ___U3CreplyU3E__1_2; }
	inline String_t** get_address_of_U3CreplyU3E__1_2() { return &___U3CreplyU3E__1_2; }
	inline void set_U3CreplyU3E__1_2(String_t* value)
	{
		___U3CreplyU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreplyU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U24this_3)); }
	inline EchoTest_t2567908100 * get_U24this_3() const { return ___U24this_3; }
	inline EchoTest_t2567908100 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(EchoTest_t2567908100 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3655621217, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3655621217_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BUILDER_T2638244817_H
#define BUILDER_T2638244817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader/Builder
struct  Builder_t2638244817  : public RuntimeObject
{
public:
	// System.String GoogleMobileAds.Api.AdLoader/Builder::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader/Builder::<AdTypes>k__BackingField
	HashSet_1_t324370440 * ___U3CAdTypesU3Ek__BackingField_1;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader/Builder::<TemplateIds>k__BackingField
	HashSet_1_t412400163 * ___U3CTemplateIdsU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader/Builder::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t856064493 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CAdUnitIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_0() const { return ___U3CAdUnitIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_0() { return &___U3CAdUnitIdU3Ek__BackingField_0; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CAdTypesU3Ek__BackingField_1)); }
	inline HashSet_1_t324370440 * get_U3CAdTypesU3Ek__BackingField_1() const { return ___U3CAdTypesU3Ek__BackingField_1; }
	inline HashSet_1_t324370440 ** get_address_of_U3CAdTypesU3Ek__BackingField_1() { return &___U3CAdTypesU3Ek__BackingField_1; }
	inline void set_U3CAdTypesU3Ek__BackingField_1(HashSet_1_t324370440 * value)
	{
		___U3CAdTypesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CTemplateIdsU3Ek__BackingField_2)); }
	inline HashSet_1_t412400163 * get_U3CTemplateIdsU3Ek__BackingField_2() const { return ___U3CTemplateIdsU3Ek__BackingField_2; }
	inline HashSet_1_t412400163 ** get_address_of_U3CTemplateIdsU3Ek__BackingField_2() { return &___U3CTemplateIdsU3Ek__BackingField_2; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_2(HashSet_1_t412400163 * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t856064493 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t856064493 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t856064493 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2638244817_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#define MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t3472581009  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef REWARD_T3704020935_H
#define REWARD_T3704020935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Reward
struct  Reward_t3704020935  : public EventArgs_t3591816995
{
public:
	// System.String GoogleMobileAds.Api.Reward::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Double GoogleMobileAds.Api.Reward::<Amount>k__BackingField
	double ___U3CAmountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Reward_t3704020935, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAmountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Reward_t3704020935, ___U3CAmountU3Ek__BackingField_2)); }
	inline double get_U3CAmountU3Ek__BackingField_2() const { return ___U3CAmountU3Ek__BackingField_2; }
	inline double* get_address_of_U3CAmountU3Ek__BackingField_2() { return &___U3CAmountU3Ek__BackingField_2; }
	inline void set_U3CAmountU3Ek__BackingField_2(double value)
	{
		___U3CAmountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARD_T3704020935_H
#ifndef NATIVEADTYPES_T3925888818_H
#define NATIVEADTYPES_T3925888818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeAdTypes
struct  NativeAdTypes_t3925888818 
{
public:
	// System.Int32 GoogleMobileAds.iOS.NativeAdTypes::CustomTemplateAd
	int32_t ___CustomTemplateAd_0;

public:
	inline static int32_t get_offset_of_CustomTemplateAd_0() { return static_cast<int32_t>(offsetof(NativeAdTypes_t3925888818, ___CustomTemplateAd_0)); }
	inline int32_t get_CustomTemplateAd_0() const { return ___CustomTemplateAd_0; }
	inline int32_t* get_address_of_CustomTemplateAd_0() { return &___CustomTemplateAd_0; }
	inline void set_CustomTemplateAd_0(int32_t value)
	{
		___CustomTemplateAd_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADTYPES_T3925888818_H
#ifndef CUSTOMNATIVEEVENTARGS_T3359820132_H
#define CUSTOMNATIVEEVENTARGS_T3359820132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeEventArgs
struct  CustomNativeEventArgs_t3359820132  : public EventArgs_t3591816995
{
public:
	// GoogleMobileAds.Api.CustomNativeTemplateAd GoogleMobileAds.Api.CustomNativeEventArgs::<nativeAd>k__BackingField
	CustomNativeTemplateAd_t3403582769 * ___U3CnativeAdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnativeAdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomNativeEventArgs_t3359820132, ___U3CnativeAdU3Ek__BackingField_1)); }
	inline CustomNativeTemplateAd_t3403582769 * get_U3CnativeAdU3Ek__BackingField_1() const { return ___U3CnativeAdU3Ek__BackingField_1; }
	inline CustomNativeTemplateAd_t3403582769 ** get_address_of_U3CnativeAdU3Ek__BackingField_1() { return &___U3CnativeAdU3Ek__BackingField_1; }
	inline void set_U3CnativeAdU3Ek__BackingField_1(CustomNativeTemplateAd_t3403582769 * value)
	{
		___U3CnativeAdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnativeAdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVEEVENTARGS_T3359820132_H
#ifndef ADFAILEDTOLOADEVENTARGS_T2855961722_H
#define ADFAILEDTOLOADEVENTARGS_T2855961722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdFailedToLoadEventArgs
struct  AdFailedToLoadEventArgs_t2855961722  : public EventArgs_t3591816995
{
public:
	// System.String GoogleMobileAds.Api.AdFailedToLoadEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdFailedToLoadEventArgs_t2855961722, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADFAILEDTOLOADEVENTARGS_T2855961722_H
#ifndef NATIVEEXPRESSADCLIENT_T806157356_H
#define NATIVEEXPRESSADCLIENT_T806157356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient
struct  NativeExpressAdClient_t806157356  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.NativeExpressAdClient::nativeExpressAdViewPtr
	intptr_t ___nativeExpressAdViewPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.NativeExpressAdClient::nativeExpressAdClientPtr
	intptr_t ___nativeExpressAdClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.NativeExpressAdClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.NativeExpressAdClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.NativeExpressAdClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.NativeExpressAdClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.NativeExpressAdClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;

public:
	inline static int32_t get_offset_of_nativeExpressAdViewPtr_0() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___nativeExpressAdViewPtr_0)); }
	inline intptr_t get_nativeExpressAdViewPtr_0() const { return ___nativeExpressAdViewPtr_0; }
	inline intptr_t* get_address_of_nativeExpressAdViewPtr_0() { return &___nativeExpressAdViewPtr_0; }
	inline void set_nativeExpressAdViewPtr_0(intptr_t value)
	{
		___nativeExpressAdViewPtr_0 = value;
	}

	inline static int32_t get_offset_of_nativeExpressAdClientPtr_1() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___nativeExpressAdClientPtr_1)); }
	inline intptr_t get_nativeExpressAdClientPtr_1() const { return ___nativeExpressAdClientPtr_1; }
	inline intptr_t* get_address_of_nativeExpressAdClientPtr_1() { return &___nativeExpressAdClientPtr_1; }
	inline void set_nativeExpressAdClientPtr_1(intptr_t value)
	{
		___nativeExpressAdClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___OnAdClosed_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}
};

struct NativeExpressAdClient_t806157356_StaticFields
{
public:
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidReceiveAdCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache0
	GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * ___U3CU3Ef__mgU24cache0_7;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache1
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * ___U3CU3Ef__mgU24cache1_8;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache2
	GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * ___U3CU3Ef__mgU24cache2_9;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidDismissScreenCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache3
	GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * ___U3CU3Ef__mgU24cache3_10;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache4
	GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * ___U3CU3Ef__mgU24cache4_11;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidReceiveAdCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache5
	GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * ___U3CU3Ef__mgU24cache5_12;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache6
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * ___U3CU3Ef__mgU24cache6_13;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache7
	GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * ___U3CU3Ef__mgU24cache7_14;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidDismissScreenCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache8
	GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * ___U3CU3Ef__mgU24cache8_15;
	// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.NativeExpressAdClient::<>f__mg$cache9
	GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * ___U3CU3Ef__mgU24cache9_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_11() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache4_11)); }
	inline GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * get_U3CU3Ef__mgU24cache4_11() const { return ___U3CU3Ef__mgU24cache4_11; }
	inline GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 ** get_address_of_U3CU3Ef__mgU24cache4_11() { return &___U3CU3Ef__mgU24cache4_11; }
	inline void set_U3CU3Ef__mgU24cache4_11(GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * value)
	{
		___U3CU3Ef__mgU24cache4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_12() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache5_12)); }
	inline GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * get_U3CU3Ef__mgU24cache5_12() const { return ___U3CU3Ef__mgU24cache5_12; }
	inline GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 ** get_address_of_U3CU3Ef__mgU24cache5_12() { return &___U3CU3Ef__mgU24cache5_12; }
	inline void set_U3CU3Ef__mgU24cache5_12(GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075 * value)
	{
		___U3CU3Ef__mgU24cache5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_13() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache6_13)); }
	inline GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * get_U3CU3Ef__mgU24cache6_13() const { return ___U3CU3Ef__mgU24cache6_13; }
	inline GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 ** get_address_of_U3CU3Ef__mgU24cache6_13() { return &___U3CU3Ef__mgU24cache6_13; }
	inline void set_U3CU3Ef__mgU24cache6_13(GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880 * value)
	{
		___U3CU3Ef__mgU24cache6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_14() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache7_14)); }
	inline GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * get_U3CU3Ef__mgU24cache7_14() const { return ___U3CU3Ef__mgU24cache7_14; }
	inline GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 ** get_address_of_U3CU3Ef__mgU24cache7_14() { return &___U3CU3Ef__mgU24cache7_14; }
	inline void set_U3CU3Ef__mgU24cache7_14(GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154 * value)
	{
		___U3CU3Ef__mgU24cache7_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_15() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache8_15)); }
	inline GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * get_U3CU3Ef__mgU24cache8_15() const { return ___U3CU3Ef__mgU24cache8_15; }
	inline GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 ** get_address_of_U3CU3Ef__mgU24cache8_15() { return &___U3CU3Ef__mgU24cache8_15; }
	inline void set_U3CU3Ef__mgU24cache8_15(GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787 * value)
	{
		___U3CU3Ef__mgU24cache8_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_16() { return static_cast<int32_t>(offsetof(NativeExpressAdClient_t806157356_StaticFields, ___U3CU3Ef__mgU24cache9_16)); }
	inline GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * get_U3CU3Ef__mgU24cache9_16() const { return ___U3CU3Ef__mgU24cache9_16; }
	inline GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 ** get_address_of_U3CU3Ef__mgU24cache9_16() { return &___U3CU3Ef__mgU24cache9_16; }
	inline void set_U3CU3Ef__mgU24cache9_16(GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168 * value)
	{
		___U3CU3Ef__mgU24cache9_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEEXPRESSADCLIENT_T806157356_H
#ifndef NATIVEADTYPE_T1759420966_H
#define NATIVEADTYPE_T1759420966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.NativeAdType
struct  NativeAdType_t1759420966 
{
public:
	// System.Int32 GoogleMobileAds.Api.NativeAdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NativeAdType_t1759420966, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADTYPE_T1759420966_H
#ifndef ADLOADERCLIENT_T2216398974_H
#define ADLOADERCLIENT_T2216398974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient
struct  AdLoaderClient_t2216398974  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.AdLoaderClient::adLoaderPtr
	intptr_t ___adLoaderPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.AdLoaderClient::adLoaderClientPtr
	intptr_t ___adLoaderClientPtr_1;
	// GoogleMobileAds.iOS.NativeAdTypes GoogleMobileAds.iOS.AdLoaderClient::adTypes
	NativeAdTypes_t3925888818  ___adTypes_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.iOS.AdLoaderClient::customNativeTemplateCallbacks
	Dictionary_2_t856064493 * ___customNativeTemplateCallbacks_3;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.iOS.AdLoaderClient::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_4;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.AdLoaderClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_5;

public:
	inline static int32_t get_offset_of_adLoaderPtr_0() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adLoaderPtr_0)); }
	inline intptr_t get_adLoaderPtr_0() const { return ___adLoaderPtr_0; }
	inline intptr_t* get_address_of_adLoaderPtr_0() { return &___adLoaderPtr_0; }
	inline void set_adLoaderPtr_0(intptr_t value)
	{
		___adLoaderPtr_0 = value;
	}

	inline static int32_t get_offset_of_adLoaderClientPtr_1() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adLoaderClientPtr_1)); }
	inline intptr_t get_adLoaderClientPtr_1() const { return ___adLoaderClientPtr_1; }
	inline intptr_t* get_address_of_adLoaderClientPtr_1() { return &___adLoaderClientPtr_1; }
	inline void set_adLoaderClientPtr_1(intptr_t value)
	{
		___adLoaderClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_adTypes_2() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adTypes_2)); }
	inline NativeAdTypes_t3925888818  get_adTypes_2() const { return ___adTypes_2; }
	inline NativeAdTypes_t3925888818 * get_address_of_adTypes_2() { return &___adTypes_2; }
	inline void set_adTypes_2(NativeAdTypes_t3925888818  value)
	{
		___adTypes_2 = value;
	}

	inline static int32_t get_offset_of_customNativeTemplateCallbacks_3() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___customNativeTemplateCallbacks_3)); }
	inline Dictionary_2_t856064493 * get_customNativeTemplateCallbacks_3() const { return ___customNativeTemplateCallbacks_3; }
	inline Dictionary_2_t856064493 ** get_address_of_customNativeTemplateCallbacks_3() { return &___customNativeTemplateCallbacks_3; }
	inline void set_customNativeTemplateCallbacks_3(Dictionary_2_t856064493 * value)
	{
		___customNativeTemplateCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___customNativeTemplateCallbacks_3), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_4() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___OnCustomNativeTemplateAdLoaded_4)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_4() const { return ___OnCustomNativeTemplateAdLoaded_4; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_4() { return &___OnCustomNativeTemplateAdLoaded_4; }
	inline void set_OnCustomNativeTemplateAdLoaded_4(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_4), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_5() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___OnAdFailedToLoad_5)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_5() const { return ___OnAdFailedToLoad_5; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_5() { return &___OnAdFailedToLoad_5; }
	inline void set_OnAdFailedToLoad_5(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_5), value);
	}
};

struct AdLoaderClient_t2216398974_StaticFields
{
public:
	// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback GoogleMobileAds.iOS.AdLoaderClient::<>f__mg$cache0
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * ___U3CU3Ef__mgU24cache0_6;
	// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.AdLoaderClient::<>f__mg$cache1
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * ___U3CU3Ef__mgU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOADERCLIENT_T2216398974_H
#ifndef BANNERCLIENT_T2577994961_H
#define BANNERCLIENT_T2577994961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient
struct  BannerClient_t2577994961  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.BannerClient::bannerViewPtr
	intptr_t ___bannerViewPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.BannerClient::bannerClientPtr
	intptr_t ___bannerClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.BannerClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;

public:
	inline static int32_t get_offset_of_bannerViewPtr_0() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___bannerViewPtr_0)); }
	inline intptr_t get_bannerViewPtr_0() const { return ___bannerViewPtr_0; }
	inline intptr_t* get_address_of_bannerViewPtr_0() { return &___bannerViewPtr_0; }
	inline void set_bannerViewPtr_0(intptr_t value)
	{
		___bannerViewPtr_0 = value;
	}

	inline static int32_t get_offset_of_bannerClientPtr_1() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___bannerClientPtr_1)); }
	inline intptr_t get_bannerClientPtr_1() const { return ___bannerClientPtr_1; }
	inline intptr_t* get_address_of_bannerClientPtr_1() { return &___bannerClientPtr_1; }
	inline void set_bannerClientPtr_1(intptr_t value)
	{
		___bannerClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdClosed_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}
};

struct BannerClient_t2577994961_StaticFields
{
public:
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache0
	GADUAdViewDidReceiveAdCallback_t2543294242 * ___U3CU3Ef__mgU24cache0_7;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache1
	GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * ___U3CU3Ef__mgU24cache1_8;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache2
	GADUAdViewWillPresentScreenCallback_t2057580186 * ___U3CU3Ef__mgU24cache2_9;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache3
	GADUAdViewDidDismissScreenCallback_t972393216 * ___U3CU3Ef__mgU24cache3_10;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache4
	GADUAdViewWillLeaveApplicationCallback_t3323587265 * ___U3CU3Ef__mgU24cache4_11;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache5
	GADUAdViewDidReceiveAdCallback_t2543294242 * ___U3CU3Ef__mgU24cache5_12;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache6
	GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * ___U3CU3Ef__mgU24cache6_13;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache7
	GADUAdViewWillPresentScreenCallback_t2057580186 * ___U3CU3Ef__mgU24cache7_14;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache8
	GADUAdViewDidDismissScreenCallback_t972393216 * ___U3CU3Ef__mgU24cache8_15;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache9
	GADUAdViewWillLeaveApplicationCallback_t3323587265 * ___U3CU3Ef__mgU24cache9_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(GADUAdViewDidReceiveAdCallback_t2543294242 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(GADUAdViewWillPresentScreenCallback_t2057580186 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline GADUAdViewDidDismissScreenCallback_t972393216 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline GADUAdViewDidDismissScreenCallback_t972393216 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(GADUAdViewDidDismissScreenCallback_t972393216 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_11() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache4_11)); }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 * get_U3CU3Ef__mgU24cache4_11() const { return ___U3CU3Ef__mgU24cache4_11; }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 ** get_address_of_U3CU3Ef__mgU24cache4_11() { return &___U3CU3Ef__mgU24cache4_11; }
	inline void set_U3CU3Ef__mgU24cache4_11(GADUAdViewWillLeaveApplicationCallback_t3323587265 * value)
	{
		___U3CU3Ef__mgU24cache4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_12() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache5_12)); }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 * get_U3CU3Ef__mgU24cache5_12() const { return ___U3CU3Ef__mgU24cache5_12; }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 ** get_address_of_U3CU3Ef__mgU24cache5_12() { return &___U3CU3Ef__mgU24cache5_12; }
	inline void set_U3CU3Ef__mgU24cache5_12(GADUAdViewDidReceiveAdCallback_t2543294242 * value)
	{
		___U3CU3Ef__mgU24cache5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_13() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache6_13)); }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * get_U3CU3Ef__mgU24cache6_13() const { return ___U3CU3Ef__mgU24cache6_13; }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 ** get_address_of_U3CU3Ef__mgU24cache6_13() { return &___U3CU3Ef__mgU24cache6_13; }
	inline void set_U3CU3Ef__mgU24cache6_13(GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * value)
	{
		___U3CU3Ef__mgU24cache6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_14() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache7_14)); }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 * get_U3CU3Ef__mgU24cache7_14() const { return ___U3CU3Ef__mgU24cache7_14; }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 ** get_address_of_U3CU3Ef__mgU24cache7_14() { return &___U3CU3Ef__mgU24cache7_14; }
	inline void set_U3CU3Ef__mgU24cache7_14(GADUAdViewWillPresentScreenCallback_t2057580186 * value)
	{
		___U3CU3Ef__mgU24cache7_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_15() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache8_15)); }
	inline GADUAdViewDidDismissScreenCallback_t972393216 * get_U3CU3Ef__mgU24cache8_15() const { return ___U3CU3Ef__mgU24cache8_15; }
	inline GADUAdViewDidDismissScreenCallback_t972393216 ** get_address_of_U3CU3Ef__mgU24cache8_15() { return &___U3CU3Ef__mgU24cache8_15; }
	inline void set_U3CU3Ef__mgU24cache8_15(GADUAdViewDidDismissScreenCallback_t972393216 * value)
	{
		___U3CU3Ef__mgU24cache8_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_16() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache9_16)); }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 * get_U3CU3Ef__mgU24cache9_16() const { return ___U3CU3Ef__mgU24cache9_16; }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 ** get_address_of_U3CU3Ef__mgU24cache9_16() { return &___U3CU3Ef__mgU24cache9_16; }
	inline void set_U3CU3Ef__mgU24cache9_16(GADUAdViewWillLeaveApplicationCallback_t3323587265 * value)
	{
		___U3CU3Ef__mgU24cache9_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERCLIENT_T2577994961_H
#ifndef ADPOSITION_T3254734156_H
#define ADPOSITION_T3254734156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdPosition
struct  AdPosition_t3254734156 
{
public:
	// System.Int32 GoogleMobileAds.Api.AdPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdPosition_t3254734156, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPOSITION_T3254734156_H
#ifndef REWARDBASEDVIDEOADCLIENT_T745716004_H
#define REWARDBASEDVIDEOADCLIENT_T745716004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient
struct  RewardBasedVideoAdClient_t745716004  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::rewardBasedVideoAdPtr
	intptr_t ___rewardBasedVideoAdPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::rewardBasedVideoAdClientPtr
	intptr_t ___rewardBasedVideoAdClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_8;

public:
	inline static int32_t get_offset_of_rewardBasedVideoAdPtr_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___rewardBasedVideoAdPtr_0)); }
	inline intptr_t get_rewardBasedVideoAdPtr_0() const { return ___rewardBasedVideoAdPtr_0; }
	inline intptr_t* get_address_of_rewardBasedVideoAdPtr_0() { return &___rewardBasedVideoAdPtr_0; }
	inline void set_rewardBasedVideoAdPtr_0(intptr_t value)
	{
		___rewardBasedVideoAdPtr_0 = value;
	}

	inline static int32_t get_offset_of_rewardBasedVideoAdClientPtr_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___rewardBasedVideoAdClientPtr_1)); }
	inline intptr_t get_rewardBasedVideoAdClientPtr_1() const { return ___rewardBasedVideoAdClientPtr_1; }
	inline intptr_t* get_address_of_rewardBasedVideoAdClientPtr_1() { return &___rewardBasedVideoAdClientPtr_1; }
	inline void set_rewardBasedVideoAdClientPtr_1(intptr_t value)
	{
		___rewardBasedVideoAdClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdStarted_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_5), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdClosed_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_6), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdRewarded_7)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_7), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_8), value);
	}
};

struct RewardBasedVideoAdClient_t745716004_StaticFields
{
public:
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache0
	GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * ___U3CU3Ef__mgU24cache0_9;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache1
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * ___U3CU3Ef__mgU24cache1_10;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache2
	GADURewardBasedVideoAdDidOpenCallback_t3638490629 * ___U3CU3Ef__mgU24cache2_11;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache3
	GADURewardBasedVideoAdDidStartCallback_t2792276088 * ___U3CU3Ef__mgU24cache3_12;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache4
	GADURewardBasedVideoAdDidCloseCallback_t623082069 * ___U3CU3Ef__mgU24cache4_13;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache5
	GADURewardBasedVideoAdDidRewardCallback_t990863796 * ___U3CU3Ef__mgU24cache5_14;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache6
	GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * ___U3CU3Ef__mgU24cache6_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_10() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache1_10)); }
	inline GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * get_U3CU3Ef__mgU24cache1_10() const { return ___U3CU3Ef__mgU24cache1_10; }
	inline GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 ** get_address_of_U3CU3Ef__mgU24cache1_10() { return &___U3CU3Ef__mgU24cache1_10; }
	inline void set_U3CU3Ef__mgU24cache1_10(GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * value)
	{
		___U3CU3Ef__mgU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_11() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache2_11)); }
	inline GADURewardBasedVideoAdDidOpenCallback_t3638490629 * get_U3CU3Ef__mgU24cache2_11() const { return ___U3CU3Ef__mgU24cache2_11; }
	inline GADURewardBasedVideoAdDidOpenCallback_t3638490629 ** get_address_of_U3CU3Ef__mgU24cache2_11() { return &___U3CU3Ef__mgU24cache2_11; }
	inline void set_U3CU3Ef__mgU24cache2_11(GADURewardBasedVideoAdDidOpenCallback_t3638490629 * value)
	{
		___U3CU3Ef__mgU24cache2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_12() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache3_12)); }
	inline GADURewardBasedVideoAdDidStartCallback_t2792276088 * get_U3CU3Ef__mgU24cache3_12() const { return ___U3CU3Ef__mgU24cache3_12; }
	inline GADURewardBasedVideoAdDidStartCallback_t2792276088 ** get_address_of_U3CU3Ef__mgU24cache3_12() { return &___U3CU3Ef__mgU24cache3_12; }
	inline void set_U3CU3Ef__mgU24cache3_12(GADURewardBasedVideoAdDidStartCallback_t2792276088 * value)
	{
		___U3CU3Ef__mgU24cache3_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_13() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache4_13)); }
	inline GADURewardBasedVideoAdDidCloseCallback_t623082069 * get_U3CU3Ef__mgU24cache4_13() const { return ___U3CU3Ef__mgU24cache4_13; }
	inline GADURewardBasedVideoAdDidCloseCallback_t623082069 ** get_address_of_U3CU3Ef__mgU24cache4_13() { return &___U3CU3Ef__mgU24cache4_13; }
	inline void set_U3CU3Ef__mgU24cache4_13(GADURewardBasedVideoAdDidCloseCallback_t623082069 * value)
	{
		___U3CU3Ef__mgU24cache4_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_14() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache5_14)); }
	inline GADURewardBasedVideoAdDidRewardCallback_t990863796 * get_U3CU3Ef__mgU24cache5_14() const { return ___U3CU3Ef__mgU24cache5_14; }
	inline GADURewardBasedVideoAdDidRewardCallback_t990863796 ** get_address_of_U3CU3Ef__mgU24cache5_14() { return &___U3CU3Ef__mgU24cache5_14; }
	inline void set_U3CU3Ef__mgU24cache5_14(GADURewardBasedVideoAdDidRewardCallback_t990863796 * value)
	{
		___U3CU3Ef__mgU24cache5_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_15() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache6_15)); }
	inline GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * get_U3CU3Ef__mgU24cache6_15() const { return ___U3CU3Ef__mgU24cache6_15; }
	inline GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 ** get_address_of_U3CU3Ef__mgU24cache6_15() { return &___U3CU3Ef__mgU24cache6_15; }
	inline void set_U3CU3Ef__mgU24cache6_15(GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * value)
	{
		___U3CU3Ef__mgU24cache6_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDBASEDVIDEOADCLIENT_T745716004_H
#ifndef TYPE_T1992804461_H
#define TYPE_T1992804461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/Type
struct  Type_t1992804461 
{
public:
	// System.Int32 JSONObject/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1992804461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1992804461_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GENDER_T1633829762_H
#define GENDER_T1633829762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Gender
struct  Gender_t1633829762 
{
public:
	// System.Int32 GoogleMobileAds.Api.Gender::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Gender_t1633829762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDER_T1633829762_H
#ifndef INTERSTITIALCLIENT_T301873194_H
#define INTERSTITIALCLIENT_T301873194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient
struct  InterstitialClient_t301873194  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.InterstitialClient::interstitialPtr
	intptr_t ___interstitialPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.InterstitialClient::interstitialClientPtr
	intptr_t ___interstitialClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;

public:
	inline static int32_t get_offset_of_interstitialPtr_0() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___interstitialPtr_0)); }
	inline intptr_t get_interstitialPtr_0() const { return ___interstitialPtr_0; }
	inline intptr_t* get_address_of_interstitialPtr_0() { return &___interstitialPtr_0; }
	inline void set_interstitialPtr_0(intptr_t value)
	{
		___interstitialPtr_0 = value;
	}

	inline static int32_t get_offset_of_interstitialClientPtr_1() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___interstitialClientPtr_1)); }
	inline intptr_t get_interstitialClientPtr_1() const { return ___interstitialClientPtr_1; }
	inline intptr_t* get_address_of_interstitialClientPtr_1() { return &___interstitialClientPtr_1; }
	inline void set_interstitialClientPtr_1(intptr_t value)
	{
		___interstitialClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdClosed_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}
};

struct InterstitialClient_t301873194_StaticFields
{
public:
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache0
	GADUInterstitialDidReceiveAdCallback_t821971233 * ___U3CU3Ef__mgU24cache0_7;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache1
	GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * ___U3CU3Ef__mgU24cache1_8;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache2
	GADUInterstitialWillPresentScreenCallback_t539653454 * ___U3CU3Ef__mgU24cache2_9;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache3
	GADUInterstitialDidDismissScreenCallback_t1339081348 * ___U3CU3Ef__mgU24cache3_10;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache4
	GADUInterstitialWillLeaveApplicationCallback_t1816935820 * ___U3CU3Ef__mgU24cache4_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline GADUInterstitialDidReceiveAdCallback_t821971233 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline GADUInterstitialDidReceiveAdCallback_t821971233 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(GADUInterstitialDidReceiveAdCallback_t821971233 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline GADUInterstitialWillPresentScreenCallback_t539653454 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline GADUInterstitialWillPresentScreenCallback_t539653454 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(GADUInterstitialWillPresentScreenCallback_t539653454 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline GADUInterstitialDidDismissScreenCallback_t1339081348 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline GADUInterstitialDidDismissScreenCallback_t1339081348 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(GADUInterstitialDidDismissScreenCallback_t1339081348 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_11() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache4_11)); }
	inline GADUInterstitialWillLeaveApplicationCallback_t1816935820 * get_U3CU3Ef__mgU24cache4_11() const { return ___U3CU3Ef__mgU24cache4_11; }
	inline GADUInterstitialWillLeaveApplicationCallback_t1816935820 ** get_address_of_U3CU3Ef__mgU24cache4_11() { return &___U3CU3Ef__mgU24cache4_11; }
	inline void set_U3CU3Ef__mgU24cache4_11(GADUInterstitialWillLeaveApplicationCallback_t1816935820 * value)
	{
		___U3CU3Ef__mgU24cache4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALCLIENT_T301873194_H
#ifndef CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#define CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.CustomNativeTemplateClient
struct  CustomNativeTemplateClient_t296756194  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.CustomNativeTemplateClient::customNativeAdPtr
	intptr_t ___customNativeAdPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.CustomNativeTemplateClient::customNativeTemplateAdClientPtr
	intptr_t ___customNativeTemplateAdClientPtr_1;
	// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String> GoogleMobileAds.iOS.CustomNativeTemplateClient::clickHandler
	Action_2_t1070808194 * ___clickHandler_2;

public:
	inline static int32_t get_offset_of_customNativeAdPtr_0() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___customNativeAdPtr_0)); }
	inline intptr_t get_customNativeAdPtr_0() const { return ___customNativeAdPtr_0; }
	inline intptr_t* get_address_of_customNativeAdPtr_0() { return &___customNativeAdPtr_0; }
	inline void set_customNativeAdPtr_0(intptr_t value)
	{
		___customNativeAdPtr_0 = value;
	}

	inline static int32_t get_offset_of_customNativeTemplateAdClientPtr_1() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___customNativeTemplateAdClientPtr_1)); }
	inline intptr_t get_customNativeTemplateAdClientPtr_1() const { return ___customNativeTemplateAdClientPtr_1; }
	inline intptr_t* get_address_of_customNativeTemplateAdClientPtr_1() { return &___customNativeTemplateAdClientPtr_1; }
	inline void set_customNativeTemplateAdClientPtr_1(intptr_t value)
	{
		___customNativeTemplateAdClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_clickHandler_2() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___clickHandler_2)); }
	inline Action_2_t1070808194 * get_clickHandler_2() const { return ___clickHandler_2; }
	inline Action_2_t1070808194 ** get_address_of_clickHandler_2() { return &___clickHandler_2; }
	inline void set_clickHandler_2(Action_2_t1070808194 * value)
	{
		___clickHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___clickHandler_2), value);
	}
};

struct CustomNativeTemplateClient_t296756194_StaticFields
{
public:
	// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick GoogleMobileAds.iOS.CustomNativeTemplateClient::<>f__mg$cache0
	GADUNativeCustomTemplateDidReceiveClick_t350204406 * ___U3CU3Ef__mgU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline GADUNativeCustomTemplateDidReceiveClick_t350204406 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline GADUNativeCustomTemplateDidReceiveClick_t350204406 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(GADUNativeCustomTemplateDidReceiveClick_t350204406 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ROUTEPOINT_T3880028948_H
#define ROUTEPOINT_T3880028948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t3880028948 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t3722313464  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___direction_1)); }
	inline Vector3_t3722313464  get_direction_1() const { return ___direction_1; }
	inline Vector3_t3722313464 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t3722313464  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T3880028948_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef PROGRESSSTYLE_T3254572979_H
#define PROGRESSSTYLE_T3254572979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_t3254572979 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProgressStyle_t3254572979, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_T3254572979_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef JSONOBJECT_T1339445639_H
#define JSONOBJECT_T1339445639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject
struct  JSONObject_t1339445639  : public RuntimeObject
{
public:
	// JSONObject/Type JSONObject::type
	int32_t ___type_6;
	// System.Collections.Generic.List`1<JSONObject> JSONObject::list
	List_1_t2811520381 * ___list_7;
	// System.Collections.Generic.List`1<System.String> JSONObject::keys
	List_1_t3319525431 * ___keys_8;
	// System.String JSONObject::str
	String_t* ___str_9;
	// System.Single JSONObject::n
	float ___n_10;
	// System.Boolean JSONObject::useInt
	bool ___useInt_11;
	// System.Int64 JSONObject::i
	int64_t ___i_12;
	// System.Boolean JSONObject::b
	bool ___b_13;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_list_7() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___list_7)); }
	inline List_1_t2811520381 * get_list_7() const { return ___list_7; }
	inline List_1_t2811520381 ** get_address_of_list_7() { return &___list_7; }
	inline void set_list_7(List_1_t2811520381 * value)
	{
		___list_7 = value;
		Il2CppCodeGenWriteBarrier((&___list_7), value);
	}

	inline static int32_t get_offset_of_keys_8() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___keys_8)); }
	inline List_1_t3319525431 * get_keys_8() const { return ___keys_8; }
	inline List_1_t3319525431 ** get_address_of_keys_8() { return &___keys_8; }
	inline void set_keys_8(List_1_t3319525431 * value)
	{
		___keys_8 = value;
		Il2CppCodeGenWriteBarrier((&___keys_8), value);
	}

	inline static int32_t get_offset_of_str_9() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___str_9)); }
	inline String_t* get_str_9() const { return ___str_9; }
	inline String_t** get_address_of_str_9() { return &___str_9; }
	inline void set_str_9(String_t* value)
	{
		___str_9 = value;
		Il2CppCodeGenWriteBarrier((&___str_9), value);
	}

	inline static int32_t get_offset_of_n_10() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___n_10)); }
	inline float get_n_10() const { return ___n_10; }
	inline float* get_address_of_n_10() { return &___n_10; }
	inline void set_n_10(float value)
	{
		___n_10 = value;
	}

	inline static int32_t get_offset_of_useInt_11() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___useInt_11)); }
	inline bool get_useInt_11() const { return ___useInt_11; }
	inline bool* get_address_of_useInt_11() { return &___useInt_11; }
	inline void set_useInt_11(bool value)
	{
		___useInt_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___i_12)); }
	inline int64_t get_i_12() const { return ___i_12; }
	inline int64_t* get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(int64_t value)
	{
		___i_12 = value;
	}

	inline static int32_t get_offset_of_b_13() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___b_13)); }
	inline bool get_b_13() const { return ___b_13; }
	inline bool* get_address_of_b_13() { return &___b_13; }
	inline void set_b_13(bool value)
	{
		___b_13 = value;
	}
};

struct JSONObject_t1339445639_StaticFields
{
public:
	// System.Char[] JSONObject::WHITESPACE
	CharU5BU5D_t3528271667* ___WHITESPACE_5;
	// System.Diagnostics.Stopwatch JSONObject::printWatch
	Stopwatch_t305734070 * ___printWatch_15;

public:
	inline static int32_t get_offset_of_WHITESPACE_5() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639_StaticFields, ___WHITESPACE_5)); }
	inline CharU5BU5D_t3528271667* get_WHITESPACE_5() const { return ___WHITESPACE_5; }
	inline CharU5BU5D_t3528271667** get_address_of_WHITESPACE_5() { return &___WHITESPACE_5; }
	inline void set_WHITESPACE_5(CharU5BU5D_t3528271667* value)
	{
		___WHITESPACE_5 = value;
		Il2CppCodeGenWriteBarrier((&___WHITESPACE_5), value);
	}

	inline static int32_t get_offset_of_printWatch_15() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639_StaticFields, ___printWatch_15)); }
	inline Stopwatch_t305734070 * get_printWatch_15() const { return ___printWatch_15; }
	inline Stopwatch_t305734070 ** get_address_of_printWatch_15() { return &___printWatch_15; }
	inline void set_printWatch_15(Stopwatch_t305734070 * value)
	{
		___printWatch_15 = value;
		Il2CppCodeGenWriteBarrier((&___printWatch_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T1339445639_H
#ifndef NULLABLE_1_T3356391844_H
#define NULLABLE_1_T3356391844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<GoogleMobileAds.Api.Gender>
struct  Nullable_1_t3356391844 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3356391844, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3356391844, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3356391844_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef U3CSTRINGIFYASYNCU3EC__ITERATOR2_T3547434334_H
#define U3CSTRINGIFYASYNCU3EC__ITERATOR2_T3547434334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/<StringifyAsync>c__Iterator2
struct  U3CStringifyAsyncU3Ec__Iterator2_t3547434334  : public RuntimeObject
{
public:
	// System.Int32 JSONObject/<StringifyAsync>c__Iterator2::depth
	int32_t ___depth_0;
	// JSONObject/Type JSONObject/<StringifyAsync>c__Iterator2::$locvar0
	int32_t ___U24locvar0_1;
	// System.Text.StringBuilder JSONObject/<StringifyAsync>c__Iterator2::builder
	StringBuilder_t * ___builder_2;
	// System.Boolean JSONObject/<StringifyAsync>c__Iterator2::pretty
	bool ___pretty_3;
	// System.Int32 JSONObject/<StringifyAsync>c__Iterator2::<i>__1
	int32_t ___U3CiU3E__1_4;
	// System.String JSONObject/<StringifyAsync>c__Iterator2::<key>__2
	String_t* ___U3CkeyU3E__2_5;
	// JSONObject JSONObject/<StringifyAsync>c__Iterator2::<obj>__2
	JSONObject_t1339445639 * ___U3CobjU3E__2_6;
	// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator2::$locvar1
	RuntimeObject* ___U24locvar1_7;
	// System.Collections.IEnumerable JSONObject/<StringifyAsync>c__Iterator2::<e>__3
	RuntimeObject* ___U3CeU3E__3_8;
	// System.IDisposable JSONObject/<StringifyAsync>c__Iterator2::$locvar2
	RuntimeObject* ___U24locvar2_9;
	// System.Int32 JSONObject/<StringifyAsync>c__Iterator2::<i>__4
	int32_t ___U3CiU3E__4_10;
	// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator2::$locvar3
	RuntimeObject* ___U24locvar3_11;
	// System.Collections.IEnumerable JSONObject/<StringifyAsync>c__Iterator2::<e>__5
	RuntimeObject* ___U3CeU3E__5_12;
	// System.IDisposable JSONObject/<StringifyAsync>c__Iterator2::$locvar4
	RuntimeObject* ___U24locvar4_13;
	// JSONObject JSONObject/<StringifyAsync>c__Iterator2::$this
	JSONObject_t1339445639 * ___U24this_14;
	// System.Object JSONObject/<StringifyAsync>c__Iterator2::$current
	RuntimeObject * ___U24current_15;
	// System.Boolean JSONObject/<StringifyAsync>c__Iterator2::$disposing
	bool ___U24disposing_16;
	// System.Int32 JSONObject/<StringifyAsync>c__Iterator2::<$>depth
	int32_t ___U3CU24U3Edepth_17;
	// System.Int32 JSONObject/<StringifyAsync>c__Iterator2::$PC
	int32_t ___U24PC_18;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24locvar0_1)); }
	inline int32_t get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline int32_t* get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(int32_t value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_builder_2() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___builder_2)); }
	inline StringBuilder_t * get_builder_2() const { return ___builder_2; }
	inline StringBuilder_t ** get_address_of_builder_2() { return &___builder_2; }
	inline void set_builder_2(StringBuilder_t * value)
	{
		___builder_2 = value;
		Il2CppCodeGenWriteBarrier((&___builder_2), value);
	}

	inline static int32_t get_offset_of_pretty_3() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___pretty_3)); }
	inline bool get_pretty_3() const { return ___pretty_3; }
	inline bool* get_address_of_pretty_3() { return &___pretty_3; }
	inline void set_pretty_3(bool value)
	{
		___pretty_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_4() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CiU3E__1_4)); }
	inline int32_t get_U3CiU3E__1_4() const { return ___U3CiU3E__1_4; }
	inline int32_t* get_address_of_U3CiU3E__1_4() { return &___U3CiU3E__1_4; }
	inline void set_U3CiU3E__1_4(int32_t value)
	{
		___U3CiU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CkeyU3E__2_5() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CkeyU3E__2_5)); }
	inline String_t* get_U3CkeyU3E__2_5() const { return ___U3CkeyU3E__2_5; }
	inline String_t** get_address_of_U3CkeyU3E__2_5() { return &___U3CkeyU3E__2_5; }
	inline void set_U3CkeyU3E__2_5(String_t* value)
	{
		___U3CkeyU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_6() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CobjU3E__2_6)); }
	inline JSONObject_t1339445639 * get_U3CobjU3E__2_6() const { return ___U3CobjU3E__2_6; }
	inline JSONObject_t1339445639 ** get_address_of_U3CobjU3E__2_6() { return &___U3CobjU3E__2_6; }
	inline void set_U3CobjU3E__2_6(JSONObject_t1339445639 * value)
	{
		___U3CobjU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U24locvar1_7() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24locvar1_7)); }
	inline RuntimeObject* get_U24locvar1_7() const { return ___U24locvar1_7; }
	inline RuntimeObject** get_address_of_U24locvar1_7() { return &___U24locvar1_7; }
	inline void set_U24locvar1_7(RuntimeObject* value)
	{
		___U24locvar1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_7), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__3_8() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CeU3E__3_8)); }
	inline RuntimeObject* get_U3CeU3E__3_8() const { return ___U3CeU3E__3_8; }
	inline RuntimeObject** get_address_of_U3CeU3E__3_8() { return &___U3CeU3E__3_8; }
	inline void set_U3CeU3E__3_8(RuntimeObject* value)
	{
		___U3CeU3E__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E__3_8), value);
	}

	inline static int32_t get_offset_of_U24locvar2_9() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24locvar2_9)); }
	inline RuntimeObject* get_U24locvar2_9() const { return ___U24locvar2_9; }
	inline RuntimeObject** get_address_of_U24locvar2_9() { return &___U24locvar2_9; }
	inline void set_U24locvar2_9(RuntimeObject* value)
	{
		___U24locvar2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_9), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__4_10() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CiU3E__4_10)); }
	inline int32_t get_U3CiU3E__4_10() const { return ___U3CiU3E__4_10; }
	inline int32_t* get_address_of_U3CiU3E__4_10() { return &___U3CiU3E__4_10; }
	inline void set_U3CiU3E__4_10(int32_t value)
	{
		___U3CiU3E__4_10 = value;
	}

	inline static int32_t get_offset_of_U24locvar3_11() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24locvar3_11)); }
	inline RuntimeObject* get_U24locvar3_11() const { return ___U24locvar3_11; }
	inline RuntimeObject** get_address_of_U24locvar3_11() { return &___U24locvar3_11; }
	inline void set_U24locvar3_11(RuntimeObject* value)
	{
		___U24locvar3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_11), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__5_12() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CeU3E__5_12)); }
	inline RuntimeObject* get_U3CeU3E__5_12() const { return ___U3CeU3E__5_12; }
	inline RuntimeObject** get_address_of_U3CeU3E__5_12() { return &___U3CeU3E__5_12; }
	inline void set_U3CeU3E__5_12(RuntimeObject* value)
	{
		___U3CeU3E__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E__5_12), value);
	}

	inline static int32_t get_offset_of_U24locvar4_13() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24locvar4_13)); }
	inline RuntimeObject* get_U24locvar4_13() const { return ___U24locvar4_13; }
	inline RuntimeObject** get_address_of_U24locvar4_13() { return &___U24locvar4_13; }
	inline void set_U24locvar4_13(RuntimeObject* value)
	{
		___U24locvar4_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar4_13), value);
	}

	inline static int32_t get_offset_of_U24this_14() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24this_14)); }
	inline JSONObject_t1339445639 * get_U24this_14() const { return ___U24this_14; }
	inline JSONObject_t1339445639 ** get_address_of_U24this_14() { return &___U24this_14; }
	inline void set_U24this_14(JSONObject_t1339445639 * value)
	{
		___U24this_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_14), value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24current_15)); }
	inline RuntimeObject * get_U24current_15() const { return ___U24current_15; }
	inline RuntimeObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(RuntimeObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_15), value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Edepth_17() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U3CU24U3Edepth_17)); }
	inline int32_t get_U3CU24U3Edepth_17() const { return ___U3CU24U3Edepth_17; }
	inline int32_t* get_address_of_U3CU24U3Edepth_17() { return &___U3CU24U3Edepth_17; }
	inline void set_U3CU24U3Edepth_17(int32_t value)
	{
		___U3CU24U3Edepth_17 = value;
	}

	inline static int32_t get_offset_of_U24PC_18() { return static_cast<int32_t>(offsetof(U3CStringifyAsyncU3Ec__Iterator2_t3547434334, ___U24PC_18)); }
	inline int32_t get_U24PC_18() const { return ___U24PC_18; }
	inline int32_t* get_address_of_U24PC_18() { return &___U24PC_18; }
	inline void set_U24PC_18(int32_t value)
	{
		___U24PC_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTRINGIFYASYNCU3EC__ITERATOR2_T3547434334_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#define GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback
struct  GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#ifndef ADDJSONCONTENTS_T64304480_H
#define ADDJSONCONTENTS_T64304480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/AddJSONContents
struct  AddJSONContents_t64304480  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDJSONCONTENTS_T64304480_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GETFIELDRESPONSE_T4099938239_H
#define GETFIELDRESPONSE_T4099938239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/GetFieldResponse
struct  GetFieldResponse_t4099938239  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFIELDRESPONSE_T4099938239_H
#ifndef FIELDNOTFOUND_T2620845099_H
#define FIELDNOTFOUND_T2620845099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/FieldNotFound
struct  FieldNotFound_t2620845099  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDNOTFOUND_T2620845099_H
#ifndef GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#define GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct  GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#ifndef GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#define GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback
struct  GADUInterstitialWillPresentScreenCallback_t539653454  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#ifndef GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#define GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback
struct  GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#ifndef GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#define GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback
struct  GADUInterstitialDidReceiveAdCallback_t821971233  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#ifndef GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#define GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback
struct  GADUInterstitialDidDismissScreenCallback_t1339081348  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#ifndef GADUNATIVEEXPRESSADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T1854757880_H
#define GADUNATIVEEXPRESSADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T1854757880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback
struct  GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVEEXPRESSADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T1854757880_H
#ifndef GADUNATIVEEXPRESSADVIEWDIDRECEIVEADCALLBACK_T1287948075_H
#define GADUNATIVEEXPRESSADVIEWDIDRECEIVEADCALLBACK_T1287948075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidReceiveAdCallback
struct  GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVEEXPRESSADVIEWDIDRECEIVEADCALLBACK_T1287948075_H
#ifndef GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#define GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback
struct  GADUInterstitialWillLeaveApplicationCallback_t1816935820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#ifndef GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#define GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback
struct  GADUAdViewWillPresentScreenCallback_t2057580186  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#ifndef GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#define GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback
struct  GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#ifndef GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#define GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback
struct  GADUAdViewDidReceiveAdCallback_t2543294242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#ifndef GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#define GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback
struct  GADUAdViewDidDismissScreenCallback_t972393216  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#ifndef GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#define GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback
struct  GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#ifndef GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#define GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick
struct  GADUNativeCustomTemplateDidReceiveClick_t350204406  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#ifndef GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#define GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback
struct  GADUAdViewWillLeaveApplicationCallback_t3323587265  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#ifndef GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#define GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback
struct  GADURewardBasedVideoAdDidReceiveAdCallback_t462486315  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#ifndef GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#define GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback
struct  GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#ifndef GADUNATIVEEXPRESSADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3688860168_H
#define GADUNATIVEEXPRESSADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3688860168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillLeaveApplicationCallback
struct  GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVEEXPRESSADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3688860168_H
#ifndef GADUNATIVEEXPRESSADVIEWWILLPRESENTSCREENCALLBACK_T1947415154_H
#define GADUNATIVEEXPRESSADVIEWWILLPRESENTSCREENCALLBACK_T1947415154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback
struct  GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVEEXPRESSADVIEWWILLPRESENTSCREENCALLBACK_T1947415154_H
#ifndef GADUNATIVEEXPRESSADVIEWDIDDISMISSSCREENCALLBACK_T2327772787_H
#define GADUNATIVEEXPRESSADVIEWDIDDISMISSSCREENCALLBACK_T2327772787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidDismissScreenCallback
struct  GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVEEXPRESSADVIEWDIDDISMISSSCREENCALLBACK_T2327772787_H
#ifndef GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#define GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback
struct  GADURewardBasedVideoAdDidOpenCallback_t3638490629  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#ifndef GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#define GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback
struct  GADURewardBasedVideoAdDidCloseCallback_t623082069  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#ifndef GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#define GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback
struct  GADURewardBasedVideoAdDidRewardCallback_t990863796  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#ifndef GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#define GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback
struct  GADURewardBasedVideoAdDidStartCallback_t2792276088  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#ifndef BUILDER_T3490962960_H
#define BUILDER_T3490962960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest/Builder
struct  Builder_t3490962960  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest/Builder::<TestDevices>k__BackingField
	List_1_t3319525431 * ___U3CTestDevicesU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest/Builder::<Keywords>k__BackingField
	HashSet_1_t412400163 * ___U3CKeywordsU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest/Builder::<Birthday>k__BackingField
	Nullable_1_t1166124571  ___U3CBirthdayU3Ek__BackingField_2;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest/Builder::<Gender>k__BackingField
	Nullable_1_t3356391844  ___U3CGenderU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest/Builder::<ChildDirectedTreatmentTag>k__BackingField
	Nullable_1_t1819850047  ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest/Builder::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest/Builder::<MediationExtras>k__BackingField
	List_1_t3723909906 * ___U3CMediationExtrasU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CTestDevicesU3Ek__BackingField_0)); }
	inline List_1_t3319525431 * get_U3CTestDevicesU3Ek__BackingField_0() const { return ___U3CTestDevicesU3Ek__BackingField_0; }
	inline List_1_t3319525431 ** get_address_of_U3CTestDevicesU3Ek__BackingField_0() { return &___U3CTestDevicesU3Ek__BackingField_0; }
	inline void set_U3CTestDevicesU3Ek__BackingField_0(List_1_t3319525431 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CKeywordsU3Ek__BackingField_1)); }
	inline HashSet_1_t412400163 * get_U3CKeywordsU3Ek__BackingField_1() const { return ___U3CKeywordsU3Ek__BackingField_1; }
	inline HashSet_1_t412400163 ** get_address_of_U3CKeywordsU3Ek__BackingField_1() { return &___U3CKeywordsU3Ek__BackingField_1; }
	inline void set_U3CKeywordsU3Ek__BackingField_1(HashSet_1_t412400163 * value)
	{
		___U3CKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CBirthdayU3Ek__BackingField_2)); }
	inline Nullable_1_t1166124571  get_U3CBirthdayU3Ek__BackingField_2() const { return ___U3CBirthdayU3Ek__BackingField_2; }
	inline Nullable_1_t1166124571 * get_address_of_U3CBirthdayU3Ek__BackingField_2() { return &___U3CBirthdayU3Ek__BackingField_2; }
	inline void set_U3CBirthdayU3Ek__BackingField_2(Nullable_1_t1166124571  value)
	{
		___U3CBirthdayU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CGenderU3Ek__BackingField_3)); }
	inline Nullable_1_t3356391844  get_U3CGenderU3Ek__BackingField_3() const { return ___U3CGenderU3Ek__BackingField_3; }
	inline Nullable_1_t3356391844 * get_address_of_U3CGenderU3Ek__BackingField_3() { return &___U3CGenderU3Ek__BackingField_3; }
	inline void set_U3CGenderU3Ek__BackingField_3(Nullable_1_t3356391844  value)
	{
		___U3CGenderU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4)); }
	inline Nullable_1_t1819850047  get_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() const { return ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline Nullable_1_t1819850047 * get_address_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return &___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline void set_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(Nullable_1_t1819850047  value)
	{
		___U3CChildDirectedTreatmentTagU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CExtrasU3Ek__BackingField_5)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_5() const { return ___U3CExtrasU3Ek__BackingField_5; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_5() { return &___U3CExtrasU3Ek__BackingField_5; }
	inline void set_U3CExtrasU3Ek__BackingField_5(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CMediationExtrasU3Ek__BackingField_6)); }
	inline List_1_t3723909906 * get_U3CMediationExtrasU3Ek__BackingField_6() const { return ___U3CMediationExtrasU3Ek__BackingField_6; }
	inline List_1_t3723909906 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_6() { return &___U3CMediationExtrasU3Ek__BackingField_6; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_6(List_1_t3723909906 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T3490962960_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ADREQUEST_T1573687800_H
#define ADREQUEST_T1573687800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest
struct  AdRequest_t1573687800  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::<TestDevices>k__BackingField
	List_1_t3319525431 * ___U3CTestDevicesU3Ek__BackingField_2;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::<Keywords>k__BackingField
	HashSet_1_t412400163 * ___U3CKeywordsU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::<Birthday>k__BackingField
	Nullable_1_t1166124571  ___U3CBirthdayU3Ek__BackingField_4;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::<Gender>k__BackingField
	Nullable_1_t3356391844  ___U3CGenderU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::<TagForChildDirectedTreatment>k__BackingField
	Nullable_1_t1819850047  ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::<MediationExtras>k__BackingField
	List_1_t3723909906 * ___U3CMediationExtrasU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CTestDevicesU3Ek__BackingField_2)); }
	inline List_1_t3319525431 * get_U3CTestDevicesU3Ek__BackingField_2() const { return ___U3CTestDevicesU3Ek__BackingField_2; }
	inline List_1_t3319525431 ** get_address_of_U3CTestDevicesU3Ek__BackingField_2() { return &___U3CTestDevicesU3Ek__BackingField_2; }
	inline void set_U3CTestDevicesU3Ek__BackingField_2(List_1_t3319525431 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CKeywordsU3Ek__BackingField_3)); }
	inline HashSet_1_t412400163 * get_U3CKeywordsU3Ek__BackingField_3() const { return ___U3CKeywordsU3Ek__BackingField_3; }
	inline HashSet_1_t412400163 ** get_address_of_U3CKeywordsU3Ek__BackingField_3() { return &___U3CKeywordsU3Ek__BackingField_3; }
	inline void set_U3CKeywordsU3Ek__BackingField_3(HashSet_1_t412400163 * value)
	{
		___U3CKeywordsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CBirthdayU3Ek__BackingField_4)); }
	inline Nullable_1_t1166124571  get_U3CBirthdayU3Ek__BackingField_4() const { return ___U3CBirthdayU3Ek__BackingField_4; }
	inline Nullable_1_t1166124571 * get_address_of_U3CBirthdayU3Ek__BackingField_4() { return &___U3CBirthdayU3Ek__BackingField_4; }
	inline void set_U3CBirthdayU3Ek__BackingField_4(Nullable_1_t1166124571  value)
	{
		___U3CBirthdayU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CGenderU3Ek__BackingField_5)); }
	inline Nullable_1_t3356391844  get_U3CGenderU3Ek__BackingField_5() const { return ___U3CGenderU3Ek__BackingField_5; }
	inline Nullable_1_t3356391844 * get_address_of_U3CGenderU3Ek__BackingField_5() { return &___U3CGenderU3Ek__BackingField_5; }
	inline void set_U3CGenderU3Ek__BackingField_5(Nullable_1_t3356391844  value)
	{
		___U3CGenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6)); }
	inline Nullable_1_t1819850047  get_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() const { return ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline Nullable_1_t1819850047 * get_address_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return &___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline void set_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(Nullable_1_t1819850047  value)
	{
		___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CExtrasU3Ek__BackingField_7)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_7() const { return ___U3CExtrasU3Ek__BackingField_7; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_7() { return &___U3CExtrasU3Ek__BackingField_7; }
	inline void set_U3CExtrasU3Ek__BackingField_7(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CMediationExtrasU3Ek__BackingField_8)); }
	inline List_1_t3723909906 * get_U3CMediationExtrasU3Ek__BackingField_8() const { return ___U3CMediationExtrasU3Ek__BackingField_8; }
	inline List_1_t3723909906 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_8() { return &___U3CMediationExtrasU3Ek__BackingField_8; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_8(List_1_t3723909906 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADREQUEST_T1573687800_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3438860414_H
#define TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t3438860414  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_2;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_3;

public:
	inline static int32_t get_offset_of_m_TimeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_TimeOut_2)); }
	inline float get_m_TimeOut_2() const { return ___m_TimeOut_2; }
	inline float* get_address_of_m_TimeOut_2() { return &___m_TimeOut_2; }
	inline void set_m_TimeOut_2(float value)
	{
		___m_TimeOut_2 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_DetachChildren_3)); }
	inline bool get_m_DetachChildren_3() const { return ___m_DetachChildren_3; }
	inline bool* get_address_of_m_DetachChildren_3() { return &___m_DetachChildren_3; }
	inline void set_m_DetachChildren_3(bool value)
	{
		___m_DetachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifndef WAYPOINTCIRCUIT_T445075330_H
#define WAYPOINTCIRCUIT_T445075330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t445075330  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2584574554 * ___waypointList_2;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_3;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_4;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t1718750761* ___points_5;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1444911251* ___distances_6;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_7;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_8;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_9;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_12;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_13;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t3722313464  ___P0_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t3722313464  ___P1_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t3722313464  ___P2_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t3722313464  ___P3_17;

public:
	inline static int32_t get_offset_of_waypointList_2() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___waypointList_2)); }
	inline WaypointList_t2584574554 * get_waypointList_2() const { return ___waypointList_2; }
	inline WaypointList_t2584574554 ** get_address_of_waypointList_2() { return &___waypointList_2; }
	inline void set_waypointList_2(WaypointList_t2584574554 * value)
	{
		___waypointList_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_2), value);
	}

	inline static int32_t get_offset_of_smoothRoute_3() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___smoothRoute_3)); }
	inline bool get_smoothRoute_3() const { return ___smoothRoute_3; }
	inline bool* get_address_of_smoothRoute_3() { return &___smoothRoute_3; }
	inline void set_smoothRoute_3(bool value)
	{
		___smoothRoute_3 = value;
	}

	inline static int32_t get_offset_of_numPoints_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___numPoints_4)); }
	inline int32_t get_numPoints_4() const { return ___numPoints_4; }
	inline int32_t* get_address_of_numPoints_4() { return &___numPoints_4; }
	inline void set_numPoints_4(int32_t value)
	{
		___numPoints_4 = value;
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___points_5)); }
	inline Vector3U5BU5D_t1718750761* get_points_5() const { return ___points_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(Vector3U5BU5D_t1718750761* value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((&___points_5), value);
	}

	inline static int32_t get_offset_of_distances_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___distances_6)); }
	inline SingleU5BU5D_t1444911251* get_distances_6() const { return ___distances_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_6() { return &___distances_6; }
	inline void set_distances_6(SingleU5BU5D_t1444911251* value)
	{
		___distances_6 = value;
		Il2CppCodeGenWriteBarrier((&___distances_6), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___editorVisualisationSubsteps_7)); }
	inline float get_editorVisualisationSubsteps_7() const { return ___editorVisualisationSubsteps_7; }
	inline float* get_address_of_editorVisualisationSubsteps_7() { return &___editorVisualisationSubsteps_7; }
	inline void set_editorVisualisationSubsteps_7(float value)
	{
		___editorVisualisationSubsteps_7 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___U3CLengthU3Ek__BackingField_8)); }
	inline float get_U3CLengthU3Ek__BackingField_8() const { return ___U3CLengthU3Ek__BackingField_8; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_8() { return &___U3CLengthU3Ek__BackingField_8; }
	inline void set_U3CLengthU3Ek__BackingField_8(float value)
	{
		___U3CLengthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_p0n_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p0n_9)); }
	inline int32_t get_p0n_9() const { return ___p0n_9; }
	inline int32_t* get_address_of_p0n_9() { return &___p0n_9; }
	inline void set_p0n_9(int32_t value)
	{
		___p0n_9 = value;
	}

	inline static int32_t get_offset_of_p1n_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p1n_10)); }
	inline int32_t get_p1n_10() const { return ___p1n_10; }
	inline int32_t* get_address_of_p1n_10() { return &___p1n_10; }
	inline void set_p1n_10(int32_t value)
	{
		___p1n_10 = value;
	}

	inline static int32_t get_offset_of_p2n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p2n_11)); }
	inline int32_t get_p2n_11() const { return ___p2n_11; }
	inline int32_t* get_address_of_p2n_11() { return &___p2n_11; }
	inline void set_p2n_11(int32_t value)
	{
		___p2n_11 = value;
	}

	inline static int32_t get_offset_of_p3n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p3n_12)); }
	inline int32_t get_p3n_12() const { return ___p3n_12; }
	inline int32_t* get_address_of_p3n_12() { return &___p3n_12; }
	inline void set_p3n_12(int32_t value)
	{
		___p3n_12 = value;
	}

	inline static int32_t get_offset_of_i_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___i_13)); }
	inline float get_i_13() const { return ___i_13; }
	inline float* get_address_of_i_13() { return &___i_13; }
	inline void set_i_13(float value)
	{
		___i_13 = value;
	}

	inline static int32_t get_offset_of_P0_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P0_14)); }
	inline Vector3_t3722313464  get_P0_14() const { return ___P0_14; }
	inline Vector3_t3722313464 * get_address_of_P0_14() { return &___P0_14; }
	inline void set_P0_14(Vector3_t3722313464  value)
	{
		___P0_14 = value;
	}

	inline static int32_t get_offset_of_P1_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P1_15)); }
	inline Vector3_t3722313464  get_P1_15() const { return ___P1_15; }
	inline Vector3_t3722313464 * get_address_of_P1_15() { return &___P1_15; }
	inline void set_P1_15(Vector3_t3722313464  value)
	{
		___P1_15 = value;
	}

	inline static int32_t get_offset_of_P2_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P2_16)); }
	inline Vector3_t3722313464  get_P2_16() const { return ___P2_16; }
	inline Vector3_t3722313464 * get_address_of_P2_16() { return &___P2_16; }
	inline void set_P2_16(Vector3_t3722313464  value)
	{
		___P2_16 = value;
	}

	inline static int32_t get_offset_of_P3_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P3_17)); }
	inline Vector3_t3722313464  get_P3_17() const { return ___P3_17; }
	inline Vector3_t3722313464 * get_address_of_P3_17() { return &___P3_17; }
	inline void set_P3_17(Vector3_t3722313464  value)
	{
		___P3_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T445075330_H
#ifndef WAYPOINTPROGRESSTRACKER_T1841386251_H
#define WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_t1841386251  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_t445075330 * ___circuit_2;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_3;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_6;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_8;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CtargetPointU3Ek__BackingField_9;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CspeedPointU3Ek__BackingField_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CprogressPointU3Ek__BackingField_11;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_t3600365921 * ___target_12;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_t3722313464  ___lastPosition_15;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_16;

public:
	inline static int32_t get_offset_of_circuit_2() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___circuit_2)); }
	inline WaypointCircuit_t445075330 * get_circuit_2() const { return ___circuit_2; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_2() { return &___circuit_2; }
	inline void set_circuit_2(WaypointCircuit_t445075330 * value)
	{
		___circuit_2 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_2), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_3() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetOffset_3)); }
	inline float get_lookAheadForTargetOffset_3() const { return ___lookAheadForTargetOffset_3; }
	inline float* get_address_of_lookAheadForTargetOffset_3() { return &___lookAheadForTargetOffset_3; }
	inline void set_lookAheadForTargetOffset_3(float value)
	{
		___lookAheadForTargetOffset_3 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetFactor_4)); }
	inline float get_lookAheadForTargetFactor_4() const { return ___lookAheadForTargetFactor_4; }
	inline float* get_address_of_lookAheadForTargetFactor_4() { return &___lookAheadForTargetFactor_4; }
	inline void set_lookAheadForTargetFactor_4(float value)
	{
		___lookAheadForTargetFactor_4 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedOffset_5)); }
	inline float get_lookAheadForSpeedOffset_5() const { return ___lookAheadForSpeedOffset_5; }
	inline float* get_address_of_lookAheadForSpeedOffset_5() { return &___lookAheadForSpeedOffset_5; }
	inline void set_lookAheadForSpeedOffset_5(float value)
	{
		___lookAheadForSpeedOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedFactor_6)); }
	inline float get_lookAheadForSpeedFactor_6() const { return ___lookAheadForSpeedFactor_6; }
	inline float* get_address_of_lookAheadForSpeedFactor_6() { return &___lookAheadForSpeedFactor_6; }
	inline void set_lookAheadForSpeedFactor_6(float value)
	{
		___lookAheadForSpeedFactor_6 = value;
	}

	inline static int32_t get_offset_of_progressStyle_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressStyle_7)); }
	inline int32_t get_progressStyle_7() const { return ___progressStyle_7; }
	inline int32_t* get_address_of_progressStyle_7() { return &___progressStyle_7; }
	inline void set_progressStyle_7(int32_t value)
	{
		___progressStyle_7 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___pointToPointThreshold_8)); }
	inline float get_pointToPointThreshold_8() const { return ___pointToPointThreshold_8; }
	inline float* get_address_of_pointToPointThreshold_8() { return &___pointToPointThreshold_8; }
	inline void set_pointToPointThreshold_8(float value)
	{
		___pointToPointThreshold_8 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CtargetPointU3Ek__BackingField_9)); }
	inline RoutePoint_t3880028948  get_U3CtargetPointU3Ek__BackingField_9() const { return ___U3CtargetPointU3Ek__BackingField_9; }
	inline RoutePoint_t3880028948 * get_address_of_U3CtargetPointU3Ek__BackingField_9() { return &___U3CtargetPointU3Ek__BackingField_9; }
	inline void set_U3CtargetPointU3Ek__BackingField_9(RoutePoint_t3880028948  value)
	{
		___U3CtargetPointU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CspeedPointU3Ek__BackingField_10)); }
	inline RoutePoint_t3880028948  get_U3CspeedPointU3Ek__BackingField_10() const { return ___U3CspeedPointU3Ek__BackingField_10; }
	inline RoutePoint_t3880028948 * get_address_of_U3CspeedPointU3Ek__BackingField_10() { return &___U3CspeedPointU3Ek__BackingField_10; }
	inline void set_U3CspeedPointU3Ek__BackingField_10(RoutePoint_t3880028948  value)
	{
		___U3CspeedPointU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CprogressPointU3Ek__BackingField_11)); }
	inline RoutePoint_t3880028948  get_U3CprogressPointU3Ek__BackingField_11() const { return ___U3CprogressPointU3Ek__BackingField_11; }
	inline RoutePoint_t3880028948 * get_address_of_U3CprogressPointU3Ek__BackingField_11() { return &___U3CprogressPointU3Ek__BackingField_11; }
	inline void set_U3CprogressPointU3Ek__BackingField_11(RoutePoint_t3880028948  value)
	{
		___U3CprogressPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_target_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___target_12)); }
	inline Transform_t3600365921 * get_target_12() const { return ___target_12; }
	inline Transform_t3600365921 ** get_address_of_target_12() { return &___target_12; }
	inline void set_target_12(Transform_t3600365921 * value)
	{
		___target_12 = value;
		Il2CppCodeGenWriteBarrier((&___target_12), value);
	}

	inline static int32_t get_offset_of_progressDistance_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressDistance_13)); }
	inline float get_progressDistance_13() const { return ___progressDistance_13; }
	inline float* get_address_of_progressDistance_13() { return &___progressDistance_13; }
	inline void set_progressDistance_13(float value)
	{
		___progressDistance_13 = value;
	}

	inline static int32_t get_offset_of_progressNum_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressNum_14)); }
	inline int32_t get_progressNum_14() const { return ___progressNum_14; }
	inline int32_t* get_address_of_progressNum_14() { return &___progressNum_14; }
	inline void set_progressNum_14(int32_t value)
	{
		___progressNum_14 = value;
	}

	inline static int32_t get_offset_of_lastPosition_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lastPosition_15)); }
	inline Vector3_t3722313464  get_lastPosition_15() const { return ___lastPosition_15; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_15() { return &___lastPosition_15; }
	inline void set_lastPosition_15(Vector3_t3722313464  value)
	{
		___lastPosition_15 = value;
	}

	inline static int32_t get_offset_of_speed_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___speed_16)); }
	inline float get_speed_16() const { return ___speed_16; }
	inline float* get_address_of_speed_16() { return &___speed_16; }
	inline void set_speed_16(float value)
	{
		___speed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifndef BUYSHILLINGSSCRIPT_T2693355682_H
#define BUYSHILLINGSSCRIPT_T2693355682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyShillingsScript
struct  BuyShillingsScript_t2693355682  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ToggleGroup BuyShillingsScript::choisesGroup
	ToggleGroup_t123837990 * ___choisesGroup_2;

public:
	inline static int32_t get_offset_of_choisesGroup_2() { return static_cast<int32_t>(offsetof(BuyShillingsScript_t2693355682, ___choisesGroup_2)); }
	inline ToggleGroup_t123837990 * get_choisesGroup_2() const { return ___choisesGroup_2; }
	inline ToggleGroup_t123837990 ** get_address_of_choisesGroup_2() { return &___choisesGroup_2; }
	inline void set_choisesGroup_2(ToggleGroup_t123837990 * value)
	{
		___choisesGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&___choisesGroup_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYSHILLINGSSCRIPT_T2693355682_H
#ifndef ARABICTEXT_T1864787693_H
#define ARABICTEXT_T1864787693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicText
struct  ArabicText_t1864787693  : public MonoBehaviour_t3962482529
{
public:
	// System.String ArabicText::Text
	String_t* ___Text_2;
	// UnityEngine.UI.InputField ArabicText::RefrenceInput
	InputField_t3762917431 * ___RefrenceInput_3;
	// System.Boolean ArabicText::ShowTashkeel
	bool ___ShowTashkeel_4;
	// System.Boolean ArabicText::UseHinduNumbers
	bool ___UseHinduNumbers_5;
	// UnityEngine.UI.Text ArabicText::txt
	Text_t1901882714 * ___txt_6;
	// System.String ArabicText::OldText
	String_t* ___OldText_7;
	// System.Int32 ArabicText::OldFontSize
	int32_t ___OldFontSize_8;
	// UnityEngine.RectTransform ArabicText::rectTransform
	RectTransform_t3704657025 * ___rectTransform_9;
	// UnityEngine.Vector2 ArabicText::OldDeltaSize
	Vector2_t2156229523  ___OldDeltaSize_10;
	// System.Boolean ArabicText::OldEnabled
	bool ___OldEnabled_11;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> ArabicText::OldRectTransformParents
	List_1_t881764471 * ___OldRectTransformParents_12;
	// UnityEngine.Vector2 ArabicText::OldScreenRect
	Vector2_t2156229523  ___OldScreenRect_13;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___Text_2)); }
	inline String_t* get_Text_2() const { return ___Text_2; }
	inline String_t** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(String_t* value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier((&___Text_2), value);
	}

	inline static int32_t get_offset_of_RefrenceInput_3() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___RefrenceInput_3)); }
	inline InputField_t3762917431 * get_RefrenceInput_3() const { return ___RefrenceInput_3; }
	inline InputField_t3762917431 ** get_address_of_RefrenceInput_3() { return &___RefrenceInput_3; }
	inline void set_RefrenceInput_3(InputField_t3762917431 * value)
	{
		___RefrenceInput_3 = value;
		Il2CppCodeGenWriteBarrier((&___RefrenceInput_3), value);
	}

	inline static int32_t get_offset_of_ShowTashkeel_4() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___ShowTashkeel_4)); }
	inline bool get_ShowTashkeel_4() const { return ___ShowTashkeel_4; }
	inline bool* get_address_of_ShowTashkeel_4() { return &___ShowTashkeel_4; }
	inline void set_ShowTashkeel_4(bool value)
	{
		___ShowTashkeel_4 = value;
	}

	inline static int32_t get_offset_of_UseHinduNumbers_5() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___UseHinduNumbers_5)); }
	inline bool get_UseHinduNumbers_5() const { return ___UseHinduNumbers_5; }
	inline bool* get_address_of_UseHinduNumbers_5() { return &___UseHinduNumbers_5; }
	inline void set_UseHinduNumbers_5(bool value)
	{
		___UseHinduNumbers_5 = value;
	}

	inline static int32_t get_offset_of_txt_6() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___txt_6)); }
	inline Text_t1901882714 * get_txt_6() const { return ___txt_6; }
	inline Text_t1901882714 ** get_address_of_txt_6() { return &___txt_6; }
	inline void set_txt_6(Text_t1901882714 * value)
	{
		___txt_6 = value;
		Il2CppCodeGenWriteBarrier((&___txt_6), value);
	}

	inline static int32_t get_offset_of_OldText_7() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldText_7)); }
	inline String_t* get_OldText_7() const { return ___OldText_7; }
	inline String_t** get_address_of_OldText_7() { return &___OldText_7; }
	inline void set_OldText_7(String_t* value)
	{
		___OldText_7 = value;
		Il2CppCodeGenWriteBarrier((&___OldText_7), value);
	}

	inline static int32_t get_offset_of_OldFontSize_8() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldFontSize_8)); }
	inline int32_t get_OldFontSize_8() const { return ___OldFontSize_8; }
	inline int32_t* get_address_of_OldFontSize_8() { return &___OldFontSize_8; }
	inline void set_OldFontSize_8(int32_t value)
	{
		___OldFontSize_8 = value;
	}

	inline static int32_t get_offset_of_rectTransform_9() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___rectTransform_9)); }
	inline RectTransform_t3704657025 * get_rectTransform_9() const { return ___rectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_9() { return &___rectTransform_9; }
	inline void set_rectTransform_9(RectTransform_t3704657025 * value)
	{
		___rectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_9), value);
	}

	inline static int32_t get_offset_of_OldDeltaSize_10() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldDeltaSize_10)); }
	inline Vector2_t2156229523  get_OldDeltaSize_10() const { return ___OldDeltaSize_10; }
	inline Vector2_t2156229523 * get_address_of_OldDeltaSize_10() { return &___OldDeltaSize_10; }
	inline void set_OldDeltaSize_10(Vector2_t2156229523  value)
	{
		___OldDeltaSize_10 = value;
	}

	inline static int32_t get_offset_of_OldEnabled_11() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldEnabled_11)); }
	inline bool get_OldEnabled_11() const { return ___OldEnabled_11; }
	inline bool* get_address_of_OldEnabled_11() { return &___OldEnabled_11; }
	inline void set_OldEnabled_11(bool value)
	{
		___OldEnabled_11 = value;
	}

	inline static int32_t get_offset_of_OldRectTransformParents_12() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldRectTransformParents_12)); }
	inline List_1_t881764471 * get_OldRectTransformParents_12() const { return ___OldRectTransformParents_12; }
	inline List_1_t881764471 ** get_address_of_OldRectTransformParents_12() { return &___OldRectTransformParents_12; }
	inline void set_OldRectTransformParents_12(List_1_t881764471 * value)
	{
		___OldRectTransformParents_12 = value;
		Il2CppCodeGenWriteBarrier((&___OldRectTransformParents_12), value);
	}

	inline static int32_t get_offset_of_OldScreenRect_13() { return static_cast<int32_t>(offsetof(ArabicText_t1864787693, ___OldScreenRect_13)); }
	inline Vector2_t2156229523  get_OldScreenRect_13() const { return ___OldScreenRect_13; }
	inline Vector2_t2156229523 * get_address_of_OldScreenRect_13() { return &___OldScreenRect_13; }
	inline void set_OldScreenRect_13(Vector2_t2156229523  value)
	{
		___OldScreenRect_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICTEXT_T1864787693_H
#ifndef ADVIDEOSCONTENTMANAGER_T2057286990_H
#define ADVIDEOSCONTENTMANAGER_T2057286990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdVideosContentManager
struct  AdVideosContentManager_t2057286990  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button AdVideosContentManager::foodButton
	Button_t4055032469 * ___foodButton_2;
	// UnityEngine.UI.Button AdVideosContentManager::woodButton
	Button_t4055032469 * ___woodButton_3;
	// UnityEngine.UI.Button AdVideosContentManager::stoneButton
	Button_t4055032469 * ___stoneButton_4;
	// UnityEngine.UI.Button AdVideosContentManager::ironButton
	Button_t4055032469 * ___ironButton_5;
	// UnityEngine.UI.Button AdVideosContentManager::silverButton
	Button_t4055032469 * ___silverButton_6;
	// UnityEngine.UI.Button AdVideosContentManager::goldButton
	Button_t4055032469 * ___goldButton_7;
	// UnityEngine.UI.Button AdVideosContentManager::copperButton
	Button_t4055032469 * ___copperButton_8;
	// UnityEngine.UI.Text AdVideosContentManager::foodTime
	Text_t1901882714 * ___foodTime_9;
	// UnityEngine.UI.Text AdVideosContentManager::woodTime
	Text_t1901882714 * ___woodTime_10;
	// UnityEngine.UI.Text AdVideosContentManager::stoneTime
	Text_t1901882714 * ___stoneTime_11;
	// UnityEngine.UI.Text AdVideosContentManager::ironTime
	Text_t1901882714 * ___ironTime_12;
	// UnityEngine.UI.Text AdVideosContentManager::silverTime
	Text_t1901882714 * ___silverTime_13;
	// UnityEngine.UI.Text AdVideosContentManager::goldTime
	Text_t1901882714 * ___goldTime_14;
	// UnityEngine.UI.Text AdVideosContentManager::copperTime
	Text_t1901882714 * ___copperTime_15;
	// System.Boolean AdVideosContentManager::isDownloaded
	bool ___isDownloaded_16;

public:
	inline static int32_t get_offset_of_foodButton_2() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___foodButton_2)); }
	inline Button_t4055032469 * get_foodButton_2() const { return ___foodButton_2; }
	inline Button_t4055032469 ** get_address_of_foodButton_2() { return &___foodButton_2; }
	inline void set_foodButton_2(Button_t4055032469 * value)
	{
		___foodButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___foodButton_2), value);
	}

	inline static int32_t get_offset_of_woodButton_3() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___woodButton_3)); }
	inline Button_t4055032469 * get_woodButton_3() const { return ___woodButton_3; }
	inline Button_t4055032469 ** get_address_of_woodButton_3() { return &___woodButton_3; }
	inline void set_woodButton_3(Button_t4055032469 * value)
	{
		___woodButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___woodButton_3), value);
	}

	inline static int32_t get_offset_of_stoneButton_4() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___stoneButton_4)); }
	inline Button_t4055032469 * get_stoneButton_4() const { return ___stoneButton_4; }
	inline Button_t4055032469 ** get_address_of_stoneButton_4() { return &___stoneButton_4; }
	inline void set_stoneButton_4(Button_t4055032469 * value)
	{
		___stoneButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___stoneButton_4), value);
	}

	inline static int32_t get_offset_of_ironButton_5() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___ironButton_5)); }
	inline Button_t4055032469 * get_ironButton_5() const { return ___ironButton_5; }
	inline Button_t4055032469 ** get_address_of_ironButton_5() { return &___ironButton_5; }
	inline void set_ironButton_5(Button_t4055032469 * value)
	{
		___ironButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___ironButton_5), value);
	}

	inline static int32_t get_offset_of_silverButton_6() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___silverButton_6)); }
	inline Button_t4055032469 * get_silverButton_6() const { return ___silverButton_6; }
	inline Button_t4055032469 ** get_address_of_silverButton_6() { return &___silverButton_6; }
	inline void set_silverButton_6(Button_t4055032469 * value)
	{
		___silverButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___silverButton_6), value);
	}

	inline static int32_t get_offset_of_goldButton_7() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___goldButton_7)); }
	inline Button_t4055032469 * get_goldButton_7() const { return ___goldButton_7; }
	inline Button_t4055032469 ** get_address_of_goldButton_7() { return &___goldButton_7; }
	inline void set_goldButton_7(Button_t4055032469 * value)
	{
		___goldButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___goldButton_7), value);
	}

	inline static int32_t get_offset_of_copperButton_8() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___copperButton_8)); }
	inline Button_t4055032469 * get_copperButton_8() const { return ___copperButton_8; }
	inline Button_t4055032469 ** get_address_of_copperButton_8() { return &___copperButton_8; }
	inline void set_copperButton_8(Button_t4055032469 * value)
	{
		___copperButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___copperButton_8), value);
	}

	inline static int32_t get_offset_of_foodTime_9() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___foodTime_9)); }
	inline Text_t1901882714 * get_foodTime_9() const { return ___foodTime_9; }
	inline Text_t1901882714 ** get_address_of_foodTime_9() { return &___foodTime_9; }
	inline void set_foodTime_9(Text_t1901882714 * value)
	{
		___foodTime_9 = value;
		Il2CppCodeGenWriteBarrier((&___foodTime_9), value);
	}

	inline static int32_t get_offset_of_woodTime_10() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___woodTime_10)); }
	inline Text_t1901882714 * get_woodTime_10() const { return ___woodTime_10; }
	inline Text_t1901882714 ** get_address_of_woodTime_10() { return &___woodTime_10; }
	inline void set_woodTime_10(Text_t1901882714 * value)
	{
		___woodTime_10 = value;
		Il2CppCodeGenWriteBarrier((&___woodTime_10), value);
	}

	inline static int32_t get_offset_of_stoneTime_11() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___stoneTime_11)); }
	inline Text_t1901882714 * get_stoneTime_11() const { return ___stoneTime_11; }
	inline Text_t1901882714 ** get_address_of_stoneTime_11() { return &___stoneTime_11; }
	inline void set_stoneTime_11(Text_t1901882714 * value)
	{
		___stoneTime_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoneTime_11), value);
	}

	inline static int32_t get_offset_of_ironTime_12() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___ironTime_12)); }
	inline Text_t1901882714 * get_ironTime_12() const { return ___ironTime_12; }
	inline Text_t1901882714 ** get_address_of_ironTime_12() { return &___ironTime_12; }
	inline void set_ironTime_12(Text_t1901882714 * value)
	{
		___ironTime_12 = value;
		Il2CppCodeGenWriteBarrier((&___ironTime_12), value);
	}

	inline static int32_t get_offset_of_silverTime_13() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___silverTime_13)); }
	inline Text_t1901882714 * get_silverTime_13() const { return ___silverTime_13; }
	inline Text_t1901882714 ** get_address_of_silverTime_13() { return &___silverTime_13; }
	inline void set_silverTime_13(Text_t1901882714 * value)
	{
		___silverTime_13 = value;
		Il2CppCodeGenWriteBarrier((&___silverTime_13), value);
	}

	inline static int32_t get_offset_of_goldTime_14() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___goldTime_14)); }
	inline Text_t1901882714 * get_goldTime_14() const { return ___goldTime_14; }
	inline Text_t1901882714 ** get_address_of_goldTime_14() { return &___goldTime_14; }
	inline void set_goldTime_14(Text_t1901882714 * value)
	{
		___goldTime_14 = value;
		Il2CppCodeGenWriteBarrier((&___goldTime_14), value);
	}

	inline static int32_t get_offset_of_copperTime_15() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___copperTime_15)); }
	inline Text_t1901882714 * get_copperTime_15() const { return ___copperTime_15; }
	inline Text_t1901882714 ** get_address_of_copperTime_15() { return &___copperTime_15; }
	inline void set_copperTime_15(Text_t1901882714 * value)
	{
		___copperTime_15 = value;
		Il2CppCodeGenWriteBarrier((&___copperTime_15), value);
	}

	inline static int32_t get_offset_of_isDownloaded_16() { return static_cast<int32_t>(offsetof(AdVideosContentManager_t2057286990, ___isDownloaded_16)); }
	inline bool get_isDownloaded_16() const { return ___isDownloaded_16; }
	inline bool* get_address_of_isDownloaded_16() { return &___isDownloaded_16; }
	inline void set_isDownloaded_16(bool value)
	{
		___isDownloaded_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVIDEOSCONTENTMANAGER_T2057286990_H
#ifndef ALLIANCECONTENTCHANGER_T2140085977_H
#define ALLIANCECONTENTCHANGER_T2140085977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceContentChanger
struct  AllianceContentChanger_t2140085977  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject AllianceContentChanger::informationSection
	GameObject_t1113636619 * ___informationSection_2;
	// UnityEngine.GameObject AllianceContentChanger::contentSection
	GameObject_t1113636619 * ___contentSection_3;
	// UnityEngine.UI.Text AllianceContentChanger::contentLabel
	Text_t1901882714 * ___contentLabel_4;
	// UnityEngine.GameObject AllianceContentChanger::membersContent
	GameObject_t1113636619 * ___membersContent_5;
	// UnityEngine.GameObject AllianceContentChanger::reportsContent
	GameObject_t1113636619 * ___reportsContent_6;
	// UnityEngine.GameObject AllianceContentChanger::eventsContent
	GameObject_t1113636619 * ___eventsContent_7;
	// UnityEngine.GameObject AllianceContentChanger::alliancesContent
	GameObject_t1113636619 * ___alliancesContent_8;
	// UnityEngine.GameObject AllianceContentChanger::inviteContent
	GameObject_t1113636619 * ___inviteContent_9;
	// UnityEngine.GameObject AllianceContentChanger::applicationsContent
	GameObject_t1113636619 * ___applicationsContent_10;
	// UnityEngine.GameObject AllianceContentChanger::permissionsContent
	GameObject_t1113636619 * ___permissionsContent_11;
	// UnityEngine.GameObject AllianceContentChanger::guidelinesContent
	GameObject_t1113636619 * ___guidelinesContent_12;
	// UnityEngine.GameObject AllianceContentChanger::messageContent
	GameObject_t1113636619 * ___messageContent_13;
	// UnityEngine.GameObject[] AllianceContentChanger::contents
	GameObjectU5BU5D_t3328599146* ___contents_14;
	// ConfirmationEvent AllianceContentChanger::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_15;

public:
	inline static int32_t get_offset_of_informationSection_2() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___informationSection_2)); }
	inline GameObject_t1113636619 * get_informationSection_2() const { return ___informationSection_2; }
	inline GameObject_t1113636619 ** get_address_of_informationSection_2() { return &___informationSection_2; }
	inline void set_informationSection_2(GameObject_t1113636619 * value)
	{
		___informationSection_2 = value;
		Il2CppCodeGenWriteBarrier((&___informationSection_2), value);
	}

	inline static int32_t get_offset_of_contentSection_3() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___contentSection_3)); }
	inline GameObject_t1113636619 * get_contentSection_3() const { return ___contentSection_3; }
	inline GameObject_t1113636619 ** get_address_of_contentSection_3() { return &___contentSection_3; }
	inline void set_contentSection_3(GameObject_t1113636619 * value)
	{
		___contentSection_3 = value;
		Il2CppCodeGenWriteBarrier((&___contentSection_3), value);
	}

	inline static int32_t get_offset_of_contentLabel_4() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___contentLabel_4)); }
	inline Text_t1901882714 * get_contentLabel_4() const { return ___contentLabel_4; }
	inline Text_t1901882714 ** get_address_of_contentLabel_4() { return &___contentLabel_4; }
	inline void set_contentLabel_4(Text_t1901882714 * value)
	{
		___contentLabel_4 = value;
		Il2CppCodeGenWriteBarrier((&___contentLabel_4), value);
	}

	inline static int32_t get_offset_of_membersContent_5() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___membersContent_5)); }
	inline GameObject_t1113636619 * get_membersContent_5() const { return ___membersContent_5; }
	inline GameObject_t1113636619 ** get_address_of_membersContent_5() { return &___membersContent_5; }
	inline void set_membersContent_5(GameObject_t1113636619 * value)
	{
		___membersContent_5 = value;
		Il2CppCodeGenWriteBarrier((&___membersContent_5), value);
	}

	inline static int32_t get_offset_of_reportsContent_6() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___reportsContent_6)); }
	inline GameObject_t1113636619 * get_reportsContent_6() const { return ___reportsContent_6; }
	inline GameObject_t1113636619 ** get_address_of_reportsContent_6() { return &___reportsContent_6; }
	inline void set_reportsContent_6(GameObject_t1113636619 * value)
	{
		___reportsContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___reportsContent_6), value);
	}

	inline static int32_t get_offset_of_eventsContent_7() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___eventsContent_7)); }
	inline GameObject_t1113636619 * get_eventsContent_7() const { return ___eventsContent_7; }
	inline GameObject_t1113636619 ** get_address_of_eventsContent_7() { return &___eventsContent_7; }
	inline void set_eventsContent_7(GameObject_t1113636619 * value)
	{
		___eventsContent_7 = value;
		Il2CppCodeGenWriteBarrier((&___eventsContent_7), value);
	}

	inline static int32_t get_offset_of_alliancesContent_8() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___alliancesContent_8)); }
	inline GameObject_t1113636619 * get_alliancesContent_8() const { return ___alliancesContent_8; }
	inline GameObject_t1113636619 ** get_address_of_alliancesContent_8() { return &___alliancesContent_8; }
	inline void set_alliancesContent_8(GameObject_t1113636619 * value)
	{
		___alliancesContent_8 = value;
		Il2CppCodeGenWriteBarrier((&___alliancesContent_8), value);
	}

	inline static int32_t get_offset_of_inviteContent_9() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___inviteContent_9)); }
	inline GameObject_t1113636619 * get_inviteContent_9() const { return ___inviteContent_9; }
	inline GameObject_t1113636619 ** get_address_of_inviteContent_9() { return &___inviteContent_9; }
	inline void set_inviteContent_9(GameObject_t1113636619 * value)
	{
		___inviteContent_9 = value;
		Il2CppCodeGenWriteBarrier((&___inviteContent_9), value);
	}

	inline static int32_t get_offset_of_applicationsContent_10() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___applicationsContent_10)); }
	inline GameObject_t1113636619 * get_applicationsContent_10() const { return ___applicationsContent_10; }
	inline GameObject_t1113636619 ** get_address_of_applicationsContent_10() { return &___applicationsContent_10; }
	inline void set_applicationsContent_10(GameObject_t1113636619 * value)
	{
		___applicationsContent_10 = value;
		Il2CppCodeGenWriteBarrier((&___applicationsContent_10), value);
	}

	inline static int32_t get_offset_of_permissionsContent_11() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___permissionsContent_11)); }
	inline GameObject_t1113636619 * get_permissionsContent_11() const { return ___permissionsContent_11; }
	inline GameObject_t1113636619 ** get_address_of_permissionsContent_11() { return &___permissionsContent_11; }
	inline void set_permissionsContent_11(GameObject_t1113636619 * value)
	{
		___permissionsContent_11 = value;
		Il2CppCodeGenWriteBarrier((&___permissionsContent_11), value);
	}

	inline static int32_t get_offset_of_guidelinesContent_12() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___guidelinesContent_12)); }
	inline GameObject_t1113636619 * get_guidelinesContent_12() const { return ___guidelinesContent_12; }
	inline GameObject_t1113636619 ** get_address_of_guidelinesContent_12() { return &___guidelinesContent_12; }
	inline void set_guidelinesContent_12(GameObject_t1113636619 * value)
	{
		___guidelinesContent_12 = value;
		Il2CppCodeGenWriteBarrier((&___guidelinesContent_12), value);
	}

	inline static int32_t get_offset_of_messageContent_13() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___messageContent_13)); }
	inline GameObject_t1113636619 * get_messageContent_13() const { return ___messageContent_13; }
	inline GameObject_t1113636619 ** get_address_of_messageContent_13() { return &___messageContent_13; }
	inline void set_messageContent_13(GameObject_t1113636619 * value)
	{
		___messageContent_13 = value;
		Il2CppCodeGenWriteBarrier((&___messageContent_13), value);
	}

	inline static int32_t get_offset_of_contents_14() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___contents_14)); }
	inline GameObjectU5BU5D_t3328599146* get_contents_14() const { return ___contents_14; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_contents_14() { return &___contents_14; }
	inline void set_contents_14(GameObjectU5BU5D_t3328599146* value)
	{
		___contents_14 = value;
		Il2CppCodeGenWriteBarrier((&___contents_14), value);
	}

	inline static int32_t get_offset_of_confirmed_15() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977, ___confirmed_15)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_15() const { return ___confirmed_15; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_15() { return &___confirmed_15; }
	inline void set_confirmed_15(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_15 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_15), value);
	}
};

struct AllianceContentChanger_t2140085977_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AllianceContentChanger::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_16() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t2140085977_StaticFields, ___U3CU3Ef__switchU24map0_16)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_16() const { return ___U3CU3Ef__switchU24map0_16; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_16() { return &___U3CU3Ef__switchU24map0_16; }
	inline void set_U3CU3Ef__switchU24map0_16(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCECONTENTCHANGER_T2140085977_H
#ifndef ALLIANCEGUIDELINEMANAGER_T1490259725_H
#define ALLIANCEGUIDELINEMANAGER_T1490259725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceGuidelineManager
struct  AllianceGuidelineManager_t1490259725  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField AllianceGuidelineManager::guidelineField
	InputField_t3762917431 * ___guidelineField_2;

public:
	inline static int32_t get_offset_of_guidelineField_2() { return static_cast<int32_t>(offsetof(AllianceGuidelineManager_t1490259725, ___guidelineField_2)); }
	inline InputField_t3762917431 * get_guidelineField_2() const { return ___guidelineField_2; }
	inline InputField_t3762917431 ** get_address_of_guidelineField_2() { return &___guidelineField_2; }
	inline void set_guidelineField_2(InputField_t3762917431 * value)
	{
		___guidelineField_2 = value;
		Il2CppCodeGenWriteBarrier((&___guidelineField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEGUIDELINEMANAGER_T1490259725_H
#ifndef CONSTRUCTIONPITMANAGER_T848510622_H
#define CONSTRUCTIONPITMANAGER_T848510622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstructionPitManager
struct  ConstructionPitManager_t848510622  : public MonoBehaviour_t3962482529
{
public:
	// System.Int64 ConstructionPitManager::pitId
	int64_t ___pitId_2;
	// UnityEngine.GameObject ConstructionPitManager::building
	GameObject_t1113636619 * ___building_3;
	// System.String ConstructionPitManager::type
	String_t* ___type_4;

public:
	inline static int32_t get_offset_of_pitId_2() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t848510622, ___pitId_2)); }
	inline int64_t get_pitId_2() const { return ___pitId_2; }
	inline int64_t* get_address_of_pitId_2() { return &___pitId_2; }
	inline void set_pitId_2(int64_t value)
	{
		___pitId_2 = value;
	}

	inline static int32_t get_offset_of_building_3() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t848510622, ___building_3)); }
	inline GameObject_t1113636619 * get_building_3() const { return ___building_3; }
	inline GameObject_t1113636619 ** get_address_of_building_3() { return &___building_3; }
	inline void set_building_3(GameObject_t1113636619 * value)
	{
		___building_3 = value;
		Il2CppCodeGenWriteBarrier((&___building_3), value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t848510622, ___type_4)); }
	inline String_t* get_type_4() const { return ___type_4; }
	inline String_t** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(String_t* value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier((&___type_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTIONPITMANAGER_T848510622_H
#ifndef ACTIONMANAGER_T2430268190_H
#define ACTIONMANAGER_T2430268190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager
struct  ActionManager_t2430268190  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ActionManager::constructionPanel
	GameObject_t1113636619 * ___constructionPanel_2;
	// UnityEngine.GameObject ActionManager::marchesPanel
	GameObject_t1113636619 * ___marchesPanel_3;
	// UnityEngine.GameObject ActionManager::itemsPanel
	GameObject_t1113636619 * ___itemsPanel_4;
	// UnityEngine.GameObject ActionManager::unitsPanel
	GameObject_t1113636619 * ___unitsPanel_5;

public:
	inline static int32_t get_offset_of_constructionPanel_2() { return static_cast<int32_t>(offsetof(ActionManager_t2430268190, ___constructionPanel_2)); }
	inline GameObject_t1113636619 * get_constructionPanel_2() const { return ___constructionPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_constructionPanel_2() { return &___constructionPanel_2; }
	inline void set_constructionPanel_2(GameObject_t1113636619 * value)
	{
		___constructionPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___constructionPanel_2), value);
	}

	inline static int32_t get_offset_of_marchesPanel_3() { return static_cast<int32_t>(offsetof(ActionManager_t2430268190, ___marchesPanel_3)); }
	inline GameObject_t1113636619 * get_marchesPanel_3() const { return ___marchesPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_marchesPanel_3() { return &___marchesPanel_3; }
	inline void set_marchesPanel_3(GameObject_t1113636619 * value)
	{
		___marchesPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___marchesPanel_3), value);
	}

	inline static int32_t get_offset_of_itemsPanel_4() { return static_cast<int32_t>(offsetof(ActionManager_t2430268190, ___itemsPanel_4)); }
	inline GameObject_t1113636619 * get_itemsPanel_4() const { return ___itemsPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_itemsPanel_4() { return &___itemsPanel_4; }
	inline void set_itemsPanel_4(GameObject_t1113636619 * value)
	{
		___itemsPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemsPanel_4), value);
	}

	inline static int32_t get_offset_of_unitsPanel_5() { return static_cast<int32_t>(offsetof(ActionManager_t2430268190, ___unitsPanel_5)); }
	inline GameObject_t1113636619 * get_unitsPanel_5() const { return ___unitsPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_unitsPanel_5() { return &___unitsPanel_5; }
	inline void set_unitsPanel_5(GameObject_t1113636619 * value)
	{
		___unitsPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___unitsPanel_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONMANAGER_T2430268190_H
#ifndef ADVANCEDTELEPORTCONTENTMANAGER_T2722302342_H
#define ADVANCEDTELEPORTCONTENTMANAGER_T2722302342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdvancedTeleportContentManager
struct  AdvancedTeleportContentManager_t2722302342  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField AdvancedTeleportContentManager::xCoord
	InputField_t3762917431 * ___xCoord_2;
	// UnityEngine.UI.InputField AdvancedTeleportContentManager::yCoord
	InputField_t3762917431 * ___yCoord_3;
	// System.String AdvancedTeleportContentManager::item_id
	String_t* ___item_id_4;
	// IReloadable AdvancedTeleportContentManager::owner
	RuntimeObject* ___owner_5;

public:
	inline static int32_t get_offset_of_xCoord_2() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2722302342, ___xCoord_2)); }
	inline InputField_t3762917431 * get_xCoord_2() const { return ___xCoord_2; }
	inline InputField_t3762917431 ** get_address_of_xCoord_2() { return &___xCoord_2; }
	inline void set_xCoord_2(InputField_t3762917431 * value)
	{
		___xCoord_2 = value;
		Il2CppCodeGenWriteBarrier((&___xCoord_2), value);
	}

	inline static int32_t get_offset_of_yCoord_3() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2722302342, ___yCoord_3)); }
	inline InputField_t3762917431 * get_yCoord_3() const { return ___yCoord_3; }
	inline InputField_t3762917431 ** get_address_of_yCoord_3() { return &___yCoord_3; }
	inline void set_yCoord_3(InputField_t3762917431 * value)
	{
		___yCoord_3 = value;
		Il2CppCodeGenWriteBarrier((&___yCoord_3), value);
	}

	inline static int32_t get_offset_of_item_id_4() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2722302342, ___item_id_4)); }
	inline String_t* get_item_id_4() const { return ___item_id_4; }
	inline String_t** get_address_of_item_id_4() { return &___item_id_4; }
	inline void set_item_id_4(String_t* value)
	{
		___item_id_4 = value;
		Il2CppCodeGenWriteBarrier((&___item_id_4), value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2722302342, ___owner_5)); }
	inline RuntimeObject* get_owner_5() const { return ___owner_5; }
	inline RuntimeObject** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(RuntimeObject* value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDTELEPORTCONTENTMANAGER_T2722302342_H
#ifndef FIXGUITEXTCS_T1419384029_H
#define FIXGUITEXTCS_T1419384029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixGUITextCS
struct  FixGUITextCS_t1419384029  : public MonoBehaviour_t3962482529
{
public:
	// System.String FixGUITextCS::text
	String_t* ___text_2;
	// System.Boolean FixGUITextCS::tashkeel
	bool ___tashkeel_3;
	// System.Boolean FixGUITextCS::hinduNumbers
	bool ___hinduNumbers_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(FixGUITextCS_t1419384029, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_tashkeel_3() { return static_cast<int32_t>(offsetof(FixGUITextCS_t1419384029, ___tashkeel_3)); }
	inline bool get_tashkeel_3() const { return ___tashkeel_3; }
	inline bool* get_address_of_tashkeel_3() { return &___tashkeel_3; }
	inline void set_tashkeel_3(bool value)
	{
		___tashkeel_3 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_4() { return static_cast<int32_t>(offsetof(FixGUITextCS_t1419384029, ___hinduNumbers_4)); }
	inline bool get_hinduNumbers_4() const { return ___hinduNumbers_4; }
	inline bool* get_address_of_hinduNumbers_4() { return &___hinduNumbers_4; }
	inline void set_hinduNumbers_4(bool value)
	{
		___hinduNumbers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXGUITEXTCS_T1419384029_H
#ifndef FIX3DTEXTCS_T2812269764_H
#define FIX3DTEXTCS_T2812269764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fix3dTextCS
struct  Fix3dTextCS_t2812269764  : public MonoBehaviour_t3962482529
{
public:
	// System.String Fix3dTextCS::text
	String_t* ___text_2;
	// System.Boolean Fix3dTextCS::tashkeel
	bool ___tashkeel_3;
	// System.Boolean Fix3dTextCS::hinduNumbers
	bool ___hinduNumbers_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t2812269764, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_tashkeel_3() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t2812269764, ___tashkeel_3)); }
	inline bool get_tashkeel_3() const { return ___tashkeel_3; }
	inline bool* get_address_of_tashkeel_3() { return &___tashkeel_3; }
	inline void set_tashkeel_3(bool value)
	{
		___tashkeel_3 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_4() { return static_cast<int32_t>(offsetof(Fix3dTextCS_t2812269764, ___hinduNumbers_4)); }
	inline bool get_hinduNumbers_4() const { return ___hinduNumbers_4; }
	inline bool* get_address_of_hinduNumbers_4() { return &___hinduNumbers_4; }
	inline void set_hinduNumbers_4(bool value)
	{
		___hinduNumbers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIX3DTEXTCS_T2812269764_H
#ifndef ALLIANCECHAT_T1523197209_H
#define ALLIANCECHAT_T1523197209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceChat
struct  AllianceChat_t1523197209  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text AllianceChat::chatWindow
	Text_t1901882714 * ___chatWindow_2;
	// UnityEngine.UI.InputField AllianceChat::input
	InputField_t3762917431 * ___input_3;
	// UnityEngine.UI.Scrollbar AllianceChat::Scroll
	Scrollbar_t1494447233 * ___Scroll_4;
	// System.String AllianceChat::MessageBuffer
	String_t* ___MessageBuffer_5;
	// System.Boolean AllianceChat::chatWorking
	bool ___chatWorking_6;
	// System.Boolean AllianceChat::readyToChat
	bool ___readyToChat_7;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___chatWindow_2)); }
	inline Text_t1901882714 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t1901882714 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t1901882714 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier((&___chatWindow_2), value);
	}

	inline static int32_t get_offset_of_input_3() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___input_3)); }
	inline InputField_t3762917431 * get_input_3() const { return ___input_3; }
	inline InputField_t3762917431 ** get_address_of_input_3() { return &___input_3; }
	inline void set_input_3(InputField_t3762917431 * value)
	{
		___input_3 = value;
		Il2CppCodeGenWriteBarrier((&___input_3), value);
	}

	inline static int32_t get_offset_of_Scroll_4() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___Scroll_4)); }
	inline Scrollbar_t1494447233 * get_Scroll_4() const { return ___Scroll_4; }
	inline Scrollbar_t1494447233 ** get_address_of_Scroll_4() { return &___Scroll_4; }
	inline void set_Scroll_4(Scrollbar_t1494447233 * value)
	{
		___Scroll_4 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll_4), value);
	}

	inline static int32_t get_offset_of_MessageBuffer_5() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___MessageBuffer_5)); }
	inline String_t* get_MessageBuffer_5() const { return ___MessageBuffer_5; }
	inline String_t** get_address_of_MessageBuffer_5() { return &___MessageBuffer_5; }
	inline void set_MessageBuffer_5(String_t* value)
	{
		___MessageBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___MessageBuffer_5), value);
	}

	inline static int32_t get_offset_of_chatWorking_6() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___chatWorking_6)); }
	inline bool get_chatWorking_6() const { return ___chatWorking_6; }
	inline bool* get_address_of_chatWorking_6() { return &___chatWorking_6; }
	inline void set_chatWorking_6(bool value)
	{
		___chatWorking_6 = value;
	}

	inline static int32_t get_offset_of_readyToChat_7() { return static_cast<int32_t>(offsetof(AllianceChat_t1523197209, ___readyToChat_7)); }
	inline bool get_readyToChat_7() const { return ___readyToChat_7; }
	inline bool* get_address_of_readyToChat_7() { return &___readyToChat_7; }
	inline void set_readyToChat_7(bool value)
	{
		___readyToChat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCECHAT_T1523197209_H
#ifndef TOGGLEBUTTONTEXT_T3880526902_H
#define TOGGLEBUTTONTEXT_T3880526902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleButtonText
struct  ToggleButtonText_t3880526902  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ToggleButtonText::text
	GameObject_t1113636619 * ___text_2;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(ToggleButtonText_t3880526902, ___text_2)); }
	inline GameObject_t1113636619 * get_text_2() const { return ___text_2; }
	inline GameObject_t1113636619 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(GameObject_t1113636619 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEBUTTONTEXT_T3880526902_H
#ifndef ECHOTEST_T2567908100_H
#define ECHOTEST_T2567908100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest
struct  EchoTest_t2567908100  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text EchoTest::chatWindow
	Text_t1901882714 * ___chatWindow_2;
	// UnityEngine.UI.InputField EchoTest::Input
	InputField_t3762917431 * ___Input_3;
	// System.String EchoTest::MessageBuffer
	String_t* ___MessageBuffer_4;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(EchoTest_t2567908100, ___chatWindow_2)); }
	inline Text_t1901882714 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t1901882714 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t1901882714 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier((&___chatWindow_2), value);
	}

	inline static int32_t get_offset_of_Input_3() { return static_cast<int32_t>(offsetof(EchoTest_t2567908100, ___Input_3)); }
	inline InputField_t3762917431 * get_Input_3() const { return ___Input_3; }
	inline InputField_t3762917431 ** get_address_of_Input_3() { return &___Input_3; }
	inline void set_Input_3(InputField_t3762917431 * value)
	{
		___Input_3 = value;
		Il2CppCodeGenWriteBarrier((&___Input_3), value);
	}

	inline static int32_t get_offset_of_MessageBuffer_4() { return static_cast<int32_t>(offsetof(EchoTest_t2567908100, ___MessageBuffer_4)); }
	inline String_t* get_MessageBuffer_4() const { return ___MessageBuffer_4; }
	inline String_t** get_address_of_MessageBuffer_4() { return &___MessageBuffer_4; }
	inline void set_MessageBuffer_4(String_t* value)
	{
		___MessageBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___MessageBuffer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECHOTEST_T2567908100_H
#ifndef SETARABICTEXTEXAMPLE_T565476978_H
#define SETARABICTEXTEXAMPLE_T565476978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetArabicTextExample
struct  SetArabicTextExample_t565476978  : public MonoBehaviour_t3962482529
{
public:
	// System.String SetArabicTextExample::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SetArabicTextExample_t565476978, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETARABICTEXTEXAMPLE_T565476978_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (U3CActivateU3Ec__Iterator0_t2664723090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[4] = 
{
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (U3CDeactivateU3Ec__Iterator1_t730025274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t2784493974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (TimedObjectDestructor_t3438860414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[2] = 
{
	TimedObjectDestructor_t3438860414::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t3438860414::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (WaypointCircuit_t445075330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[16] = 
{
	WaypointCircuit_t445075330::get_offset_of_waypointList_2(),
	WaypointCircuit_t445075330::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t445075330::get_offset_of_numPoints_4(),
	WaypointCircuit_t445075330::get_offset_of_points_5(),
	WaypointCircuit_t445075330::get_offset_of_distances_6(),
	WaypointCircuit_t445075330::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t445075330::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t445075330::get_offset_of_p0n_9(),
	WaypointCircuit_t445075330::get_offset_of_p1n_10(),
	WaypointCircuit_t445075330::get_offset_of_p2n_11(),
	WaypointCircuit_t445075330::get_offset_of_p3n_12(),
	WaypointCircuit_t445075330::get_offset_of_i_13(),
	WaypointCircuit_t445075330::get_offset_of_P0_14(),
	WaypointCircuit_t445075330::get_offset_of_P1_15(),
	WaypointCircuit_t445075330::get_offset_of_P2_16(),
	WaypointCircuit_t445075330::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (WaypointList_t2584574554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3205[2] = 
{
	WaypointList_t2584574554::get_offset_of_circuit_0(),
	WaypointList_t2584574554::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (RoutePoint_t3880028948)+ sizeof (RuntimeObject), sizeof(RoutePoint_t3880028948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3206[2] = 
{
	RoutePoint_t3880028948::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t3880028948::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (WaypointProgressTracker_t1841386251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[15] = 
{
	WaypointProgressTracker_t1841386251::get_offset_of_circuit_2(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetOffset_3(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetFactor_4(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedOffset_5(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedFactor_6(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressStyle_7(),
	WaypointProgressTracker_t1841386251::get_offset_of_pointToPointThreshold_8(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CtargetPointU3Ek__BackingField_9(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CspeedPointU3Ek__BackingField_10(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CprogressPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t1841386251::get_offset_of_target_12(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressDistance_13(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressNum_14(),
	WaypointProgressTracker_t1841386251::get_offset_of_lastPosition_15(),
	WaypointProgressTracker_t1841386251::get_offset_of_speed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (ProgressStyle_t3254572979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3208[3] = 
{
	ProgressStyle_t3254572979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (U3CModuleU3E_t692745558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (ArabicText_t1864787693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[12] = 
{
	ArabicText_t1864787693::get_offset_of_Text_2(),
	ArabicText_t1864787693::get_offset_of_RefrenceInput_3(),
	ArabicText_t1864787693::get_offset_of_ShowTashkeel_4(),
	ArabicText_t1864787693::get_offset_of_UseHinduNumbers_5(),
	ArabicText_t1864787693::get_offset_of_txt_6(),
	ArabicText_t1864787693::get_offset_of_OldText_7(),
	ArabicText_t1864787693::get_offset_of_OldFontSize_8(),
	ArabicText_t1864787693::get_offset_of_rectTransform_9(),
	ArabicText_t1864787693::get_offset_of_OldDeltaSize_10(),
	ArabicText_t1864787693::get_offset_of_OldEnabled_11(),
	ArabicText_t1864787693::get_offset_of_OldRectTransformParents_12(),
	ArabicText_t1864787693::get_offset_of_OldScreenRect_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (Fix3dTextCS_t2812269764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3211[3] = 
{
	Fix3dTextCS_t2812269764::get_offset_of_text_2(),
	Fix3dTextCS_t2812269764::get_offset_of_tashkeel_3(),
	Fix3dTextCS_t2812269764::get_offset_of_hinduNumbers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (FixGUITextCS_t1419384029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[3] = 
{
	FixGUITextCS_t1419384029::get_offset_of_text_2(),
	FixGUITextCS_t1419384029::get_offset_of_tashkeel_3(),
	FixGUITextCS_t1419384029::get_offset_of_hinduNumbers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (SetArabicTextExample_t565476978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[1] = 
{
	SetArabicTextExample_t565476978::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (EchoTest_t2567908100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[3] = 
{
	EchoTest_t2567908100::get_offset_of_chatWindow_2(),
	EchoTest_t2567908100::get_offset_of_Input_3(),
	EchoTest_t2567908100::get_offset_of_MessageBuffer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (U3CStartU3Ec__Iterator0_t3655621217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[7] = 
{
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U3CwU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U3CiU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U3CreplyU3E__1_2(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3655621217::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (AdFailedToLoadEventArgs_t2855961722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[1] = 
{
	AdFailedToLoadEventArgs_t2855961722::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (NativeAdType_t1759420966)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3217[2] = 
{
	NativeAdType_t1759420966::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (AdLoader_t3255226653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[7] = 
{
	AdLoader_t3255226653::get_offset_of_adLoaderClient_0(),
	AdLoader_t3255226653::get_offset_of_OnAdFailedToLoad_1(),
	AdLoader_t3255226653::get_offset_of_OnCustomNativeTemplateAdLoaded_2(),
	AdLoader_t3255226653::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
	AdLoader_t3255226653::get_offset_of_U3CAdUnitIdU3Ek__BackingField_4(),
	AdLoader_t3255226653::get_offset_of_U3CAdTypesU3Ek__BackingField_5(),
	AdLoader_t3255226653::get_offset_of_U3CTemplateIdsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (Builder_t2638244817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[4] = 
{
	Builder_t2638244817::get_offset_of_U3CAdUnitIdU3Ek__BackingField_0(),
	Builder_t2638244817::get_offset_of_U3CAdTypesU3Ek__BackingField_1(),
	Builder_t2638244817::get_offset_of_U3CTemplateIdsU3Ek__BackingField_2(),
	Builder_t2638244817::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (AdPosition_t3254734156)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3220[8] = 
{
	AdPosition_t3254734156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (AdRequest_t1573687800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3221[9] = 
{
	0,
	0,
	AdRequest_t1573687800::get_offset_of_U3CTestDevicesU3Ek__BackingField_2(),
	AdRequest_t1573687800::get_offset_of_U3CKeywordsU3Ek__BackingField_3(),
	AdRequest_t1573687800::get_offset_of_U3CBirthdayU3Ek__BackingField_4(),
	AdRequest_t1573687800::get_offset_of_U3CGenderU3Ek__BackingField_5(),
	AdRequest_t1573687800::get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(),
	AdRequest_t1573687800::get_offset_of_U3CExtrasU3Ek__BackingField_7(),
	AdRequest_t1573687800::get_offset_of_U3CMediationExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (Builder_t3490962960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[7] = 
{
	Builder_t3490962960::get_offset_of_U3CTestDevicesU3Ek__BackingField_0(),
	Builder_t3490962960::get_offset_of_U3CKeywordsU3Ek__BackingField_1(),
	Builder_t3490962960::get_offset_of_U3CBirthdayU3Ek__BackingField_2(),
	Builder_t3490962960::get_offset_of_U3CGenderU3Ek__BackingField_3(),
	Builder_t3490962960::get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(),
	Builder_t3490962960::get_offset_of_U3CExtrasU3Ek__BackingField_5(),
	Builder_t3490962960::get_offset_of_U3CMediationExtrasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (AdSize_t2717023072), -1, sizeof(AdSize_t2717023072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3223[9] = 
{
	AdSize_t2717023072::get_offset_of_isSmartBanner_0(),
	AdSize_t2717023072::get_offset_of_width_1(),
	AdSize_t2717023072::get_offset_of_height_2(),
	AdSize_t2717023072_StaticFields::get_offset_of_Banner_3(),
	AdSize_t2717023072_StaticFields::get_offset_of_MediumRectangle_4(),
	AdSize_t2717023072_StaticFields::get_offset_of_IABBanner_5(),
	AdSize_t2717023072_StaticFields::get_offset_of_Leaderboard_6(),
	AdSize_t2717023072_StaticFields::get_offset_of_SmartBanner_7(),
	AdSize_t2717023072_StaticFields::get_offset_of_FullWidth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (BannerView_t2480907735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[6] = 
{
	BannerView_t2480907735::get_offset_of_client_0(),
	BannerView_t2480907735::get_offset_of_OnAdLoaded_1(),
	BannerView_t2480907735::get_offset_of_OnAdFailedToLoad_2(),
	BannerView_t2480907735::get_offset_of_OnAdOpening_3(),
	BannerView_t2480907735::get_offset_of_OnAdClosed_4(),
	BannerView_t2480907735::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (CustomNativeEventArgs_t3359820132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[1] = 
{
	CustomNativeEventArgs_t3359820132::get_offset_of_U3CnativeAdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (CustomNativeTemplateAd_t3403582769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	CustomNativeTemplateAd_t3403582769::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (Gender_t1633829762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3227[4] = 
{
	Gender_t1633829762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (InterstitialAd_t207380487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[6] = 
{
	InterstitialAd_t207380487::get_offset_of_client_0(),
	InterstitialAd_t207380487::get_offset_of_OnAdLoaded_1(),
	InterstitialAd_t207380487::get_offset_of_OnAdFailedToLoad_2(),
	InterstitialAd_t207380487::get_offset_of_OnAdOpening_3(),
	InterstitialAd_t207380487::get_offset_of_OnAdClosed_4(),
	InterstitialAd_t207380487::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (MediationExtras_t2251835164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[1] = 
{
	MediationExtras_t2251835164::get_offset_of_U3CExtrasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (MobileAds_t1050225196), -1, sizeof(MobileAds_t1050225196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3230[1] = 
{
	MobileAds_t1050225196_StaticFields::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (NativeExpressAdView_t3596908119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[6] = 
{
	NativeExpressAdView_t3596908119::get_offset_of_client_0(),
	NativeExpressAdView_t3596908119::get_offset_of_OnAdLoaded_1(),
	NativeExpressAdView_t3596908119::get_offset_of_OnAdFailedToLoad_2(),
	NativeExpressAdView_t3596908119::get_offset_of_OnAdOpening_3(),
	NativeExpressAdView_t3596908119::get_offset_of_OnAdClosed_4(),
	NativeExpressAdView_t3596908119::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (Reward_t3704020935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[2] = 
{
	Reward_t3704020935::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Reward_t3704020935::get_offset_of_U3CAmountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (RewardBasedVideoAd_t2288675867), -1, sizeof(RewardBasedVideoAd_t2288675867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3233[9] = 
{
	RewardBasedVideoAd_t2288675867::get_offset_of_client_0(),
	RewardBasedVideoAd_t2288675867_StaticFields::get_offset_of_instance_1(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdLeavingApplication_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (DummyClient_t519661512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[8] = 
{
	DummyClient_t519661512::get_offset_of_OnAdLoaded_0(),
	DummyClient_t519661512::get_offset_of_OnAdFailedToLoad_1(),
	DummyClient_t519661512::get_offset_of_OnAdOpening_2(),
	DummyClient_t519661512::get_offset_of_OnAdStarted_3(),
	DummyClient_t519661512::get_offset_of_OnAdClosed_4(),
	DummyClient_t519661512::get_offset_of_OnAdRewarded_5(),
	DummyClient_t519661512::get_offset_of_OnAdLeavingApplication_6(),
	DummyClient_t519661512::get_offset_of_OnCustomNativeTemplateAdLoaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (Utils_t3548761374), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (GoogleMobileAdsClientFactory_t3556675256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (NativeAdTypes_t3925888818)+ sizeof (RuntimeObject), sizeof(NativeAdTypes_t3925888818 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3244[1] = 
{
	NativeAdTypes_t3925888818::get_offset_of_CustomTemplateAd_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (AdLoaderClient_t2216398974), -1, sizeof(AdLoaderClient_t2216398974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3245[8] = 
{
	AdLoaderClient_t2216398974::get_offset_of_adLoaderPtr_0(),
	AdLoaderClient_t2216398974::get_offset_of_adLoaderClientPtr_1(),
	AdLoaderClient_t2216398974::get_offset_of_adTypes_2(),
	AdLoaderClient_t2216398974::get_offset_of_customNativeTemplateCallbacks_3(),
	AdLoaderClient_t2216398974::get_offset_of_OnCustomNativeTemplateAdLoaded_4(),
	AdLoaderClient_t2216398974::get_offset_of_OnAdFailedToLoad_5(),
	AdLoaderClient_t2216398974_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	AdLoaderClient_t2216398974_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (BannerClient_t2577994961), -1, sizeof(BannerClient_t2577994961_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3248[17] = 
{
	BannerClient_t2577994961::get_offset_of_bannerViewPtr_0(),
	BannerClient_t2577994961::get_offset_of_bannerClientPtr_1(),
	BannerClient_t2577994961::get_offset_of_OnAdLoaded_2(),
	BannerClient_t2577994961::get_offset_of_OnAdFailedToLoad_3(),
	BannerClient_t2577994961::get_offset_of_OnAdOpening_4(),
	BannerClient_t2577994961::get_offset_of_OnAdClosed_5(),
	BannerClient_t2577994961::get_offset_of_OnAdLeavingApplication_6(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (GADUAdViewDidReceiveAdCallback_t2543294242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (GADUAdViewWillPresentScreenCallback_t2057580186), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (GADUAdViewDidDismissScreenCallback_t972393216), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (GADUAdViewWillLeaveApplicationCallback_t3323587265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (CustomNativeTemplateClient_t296756194), -1, sizeof(CustomNativeTemplateClient_t296756194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3254[4] = 
{
	CustomNativeTemplateClient_t296756194::get_offset_of_customNativeAdPtr_0(),
	CustomNativeTemplateClient_t296756194::get_offset_of_customNativeTemplateAdClientPtr_1(),
	CustomNativeTemplateClient_t296756194::get_offset_of_clickHandler_2(),
	CustomNativeTemplateClient_t296756194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (GADUNativeCustomTemplateDidReceiveClick_t350204406), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (Externs_t92207873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (InterstitialClient_t301873194), -1, sizeof(InterstitialClient_t301873194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3257[12] = 
{
	InterstitialClient_t301873194::get_offset_of_interstitialPtr_0(),
	InterstitialClient_t301873194::get_offset_of_interstitialClientPtr_1(),
	InterstitialClient_t301873194::get_offset_of_OnAdLoaded_2(),
	InterstitialClient_t301873194::get_offset_of_OnAdFailedToLoad_3(),
	InterstitialClient_t301873194::get_offset_of_OnAdOpening_4(),
	InterstitialClient_t301873194::get_offset_of_OnAdClosed_5(),
	InterstitialClient_t301873194::get_offset_of_OnAdLeavingApplication_6(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (GADUInterstitialDidReceiveAdCallback_t821971233), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (GADUInterstitialWillPresentScreenCallback_t539653454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (GADUInterstitialDidDismissScreenCallback_t1339081348), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (GADUInterstitialWillLeaveApplicationCallback_t1816935820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (MobileAdsClient_t1008075298), -1, sizeof(MobileAdsClient_t1008075298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3263[1] = 
{
	MobileAdsClient_t1008075298_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (MonoPInvokeCallbackAttribute_t3472581009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (NativeExpressAdClient_t806157356), -1, sizeof(NativeExpressAdClient_t806157356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3265[17] = 
{
	NativeExpressAdClient_t806157356::get_offset_of_nativeExpressAdViewPtr_0(),
	NativeExpressAdClient_t806157356::get_offset_of_nativeExpressAdClientPtr_1(),
	NativeExpressAdClient_t806157356::get_offset_of_OnAdLoaded_2(),
	NativeExpressAdClient_t806157356::get_offset_of_OnAdFailedToLoad_3(),
	NativeExpressAdClient_t806157356::get_offset_of_OnAdOpening_4(),
	NativeExpressAdClient_t806157356::get_offset_of_OnAdClosed_5(),
	NativeExpressAdClient_t806157356::get_offset_of_OnAdLeavingApplication_6(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	NativeExpressAdClient_t806157356_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (RewardBasedVideoAdClient_t745716004), -1, sizeof(RewardBasedVideoAdClient_t745716004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3271[16] = 
{
	RewardBasedVideoAdClient_t745716004::get_offset_of_rewardBasedVideoAdPtr_0(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_rewardBasedVideoAdClientPtr_1(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_10(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_11(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_12(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_13(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_14(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (GADURewardBasedVideoAdDidReceiveAdCallback_t462486315), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (GADURewardBasedVideoAdDidOpenCallback_t3638490629), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (GADURewardBasedVideoAdDidStartCallback_t2792276088), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (GADURewardBasedVideoAdDidCloseCallback_t623082069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (GADURewardBasedVideoAdDidRewardCallback_t990863796), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (Utils_t143735646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (JSONObject_t1339445639), -1, sizeof(JSONObject_t1339445639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[16] = 
{
	0,
	0,
	0,
	0,
	0,
	JSONObject_t1339445639_StaticFields::get_offset_of_WHITESPACE_5(),
	JSONObject_t1339445639::get_offset_of_type_6(),
	JSONObject_t1339445639::get_offset_of_list_7(),
	JSONObject_t1339445639::get_offset_of_keys_8(),
	JSONObject_t1339445639::get_offset_of_str_9(),
	JSONObject_t1339445639::get_offset_of_n_10(),
	JSONObject_t1339445639::get_offset_of_useInt_11(),
	JSONObject_t1339445639::get_offset_of_i_12(),
	JSONObject_t1339445639::get_offset_of_b_13(),
	0,
	JSONObject_t1339445639_StaticFields::get_offset_of_printWatch_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (Type_t1992804461)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3281[8] = 
{
	Type_t1992804461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (AddJSONContents_t64304480), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (FieldNotFound_t2620845099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (GetFieldResponse_t4099938239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (U3CBakeAsyncU3Ec__Iterator0_t4027967272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[6] = 
{
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24locvar0_0(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U3CsU3E__1_1(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24this_2(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24disposing_4(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (U3CPrintAsyncU3Ec__Iterator1_t4186082390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[9] = 
{
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24locvar0_2(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U3CeU3E__1_3(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24locvar1_4(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24this_5(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24current_6(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24disposing_7(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (U3CStringifyAsyncU3Ec__Iterator2_t3547434334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[19] = 
{
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar0_1(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_builder_2(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_pretty_3(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CiU3E__1_4(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CkeyU3E__2_5(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CobjU3E__2_6(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar1_7(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CeU3E__3_8(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar2_9(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CiU3E__4_10(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar3_11(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CeU3E__5_12(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar4_13(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24this_14(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24current_15(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24disposing_16(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (JSONTemplates_t2141175947), -1, sizeof(JSONTemplates_t2141175947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3288[1] = 
{
	JSONTemplates_t2141175947_StaticFields::get_offset_of_touched_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (AllianceChat_t1523197209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[6] = 
{
	AllianceChat_t1523197209::get_offset_of_chatWindow_2(),
	AllianceChat_t1523197209::get_offset_of_input_3(),
	AllianceChat_t1523197209::get_offset_of_Scroll_4(),
	AllianceChat_t1523197209::get_offset_of_MessageBuffer_5(),
	AllianceChat_t1523197209::get_offset_of_chatWorking_6(),
	AllianceChat_t1523197209::get_offset_of_readyToChat_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (U3CBeginChatU3Ec__Iterator0_t1806601407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[7] = 
{
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U3CwU3E__0_0(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U3CinitialU3E__0_1(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U3CreplyU3E__1_2(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U24this_3(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U24current_4(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U24disposing_5(),
	U3CBeginChatU3Ec__Iterator0_t1806601407::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (ToggleButtonText_t3880526902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[1] = 
{
	ToggleButtonText_t3880526902::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (BuyShillingsScript_t2693355682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[1] = 
{
	BuyShillingsScript_t2693355682::get_offset_of_choisesGroup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (U3CSendBuyRequestU3Ec__Iterator0_t3818994470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[6] = 
{
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_U3CrequestFormU3E__0_0(),
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_count_1(),
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_U3CrequestU3E__0_2(),
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_U24current_3(),
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_U24disposing_4(),
	U3CSendBuyRequestU3Ec__Iterator0_t3818994470::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (ConstructionPitManager_t848510622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[3] = 
{
	ConstructionPitManager_t848510622::get_offset_of_pitId_2(),
	ConstructionPitManager_t848510622::get_offset_of_building_3(),
	ConstructionPitManager_t848510622::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (ActionManager_t2430268190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[4] = 
{
	ActionManager_t2430268190::get_offset_of_constructionPanel_2(),
	ActionManager_t2430268190::get_offset_of_marchesPanel_3(),
	ActionManager_t2430268190::get_offset_of_itemsPanel_4(),
	ActionManager_t2430268190::get_offset_of_unitsPanel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (AdvancedTeleportContentManager_t2722302342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[4] = 
{
	AdvancedTeleportContentManager_t2722302342::get_offset_of_xCoord_2(),
	AdvancedTeleportContentManager_t2722302342::get_offset_of_yCoord_3(),
	AdvancedTeleportContentManager_t2722302342::get_offset_of_item_id_4(),
	AdvancedTeleportContentManager_t2722302342::get_offset_of_owner_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (AdVideosContentManager_t2057286990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3297[15] = 
{
	AdVideosContentManager_t2057286990::get_offset_of_foodButton_2(),
	AdVideosContentManager_t2057286990::get_offset_of_woodButton_3(),
	AdVideosContentManager_t2057286990::get_offset_of_stoneButton_4(),
	AdVideosContentManager_t2057286990::get_offset_of_ironButton_5(),
	AdVideosContentManager_t2057286990::get_offset_of_silverButton_6(),
	AdVideosContentManager_t2057286990::get_offset_of_goldButton_7(),
	AdVideosContentManager_t2057286990::get_offset_of_copperButton_8(),
	AdVideosContentManager_t2057286990::get_offset_of_foodTime_9(),
	AdVideosContentManager_t2057286990::get_offset_of_woodTime_10(),
	AdVideosContentManager_t2057286990::get_offset_of_stoneTime_11(),
	AdVideosContentManager_t2057286990::get_offset_of_ironTime_12(),
	AdVideosContentManager_t2057286990::get_offset_of_silverTime_13(),
	AdVideosContentManager_t2057286990::get_offset_of_goldTime_14(),
	AdVideosContentManager_t2057286990::get_offset_of_copperTime_15(),
	AdVideosContentManager_t2057286990::get_offset_of_isDownloaded_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (AllianceContentChanger_t2140085977), -1, sizeof(AllianceContentChanger_t2140085977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3298[15] = 
{
	AllianceContentChanger_t2140085977::get_offset_of_informationSection_2(),
	AllianceContentChanger_t2140085977::get_offset_of_contentSection_3(),
	AllianceContentChanger_t2140085977::get_offset_of_contentLabel_4(),
	AllianceContentChanger_t2140085977::get_offset_of_membersContent_5(),
	AllianceContentChanger_t2140085977::get_offset_of_reportsContent_6(),
	AllianceContentChanger_t2140085977::get_offset_of_eventsContent_7(),
	AllianceContentChanger_t2140085977::get_offset_of_alliancesContent_8(),
	AllianceContentChanger_t2140085977::get_offset_of_inviteContent_9(),
	AllianceContentChanger_t2140085977::get_offset_of_applicationsContent_10(),
	AllianceContentChanger_t2140085977::get_offset_of_permissionsContent_11(),
	AllianceContentChanger_t2140085977::get_offset_of_guidelinesContent_12(),
	AllianceContentChanger_t2140085977::get_offset_of_messageContent_13(),
	AllianceContentChanger_t2140085977::get_offset_of_contents_14(),
	AllianceContentChanger_t2140085977::get_offset_of_confirmed_15(),
	AllianceContentChanger_t2140085977_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (AllianceGuidelineManager_t1490259725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[1] = 
{
	AllianceGuidelineManager_t1490259725::get_offset_of_guidelineField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
