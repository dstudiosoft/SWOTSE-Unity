﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RankIconUpdater
struct RankIconUpdater_t3495770272;

#include "codegen/il2cpp-codegen.h"

// System.Void RankIconUpdater::.ctor()
extern "C"  void RankIconUpdater__ctor_m287779465 (RankIconUpdater_t3495770272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RankIconUpdater::OnEnable()
extern "C"  void RankIconUpdater_OnEnable_m1728003313 (RankIconUpdater_t3495770272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RankIconUpdater::FixedUpdate()
extern "C"  void RankIconUpdater_FixedUpdate_m2903178200 (RankIconUpdater_t3495770272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
