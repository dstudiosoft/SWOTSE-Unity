﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebClient
struct WebClient_t1432723993;
// System.Net.DownloadDataCompletedEventHandler
struct DownloadDataCompletedEventHandler_t4142697621;
// System.ComponentModel.AsyncCompletedEventHandler
struct AsyncCompletedEventHandler_t626974191;
// System.Net.DownloadProgressChangedEventHandler
struct DownloadProgressChangedEventHandler_t3683154451;
// System.Net.DownloadStringCompletedEventHandler
struct DownloadStringCompletedEventHandler_t1433233068;
// System.Net.OpenReadCompletedEventHandler
struct OpenReadCompletedEventHandler_t2999865175;
// System.Net.OpenWriteCompletedEventHandler
struct OpenWriteCompletedEventHandler_t1007953034;
// System.Net.UploadDataCompletedEventHandler
struct UploadDataCompletedEventHandler_t1548853072;
// System.Net.UploadFileCompletedEventHandler
struct UploadFileCompletedEventHandler_t3651750422;
// System.Net.UploadProgressChangedEventHandler
struct UploadProgressChangedEventHandler_t4227408420;
// System.Net.UploadStringCompletedEventHandler
struct UploadStringCompletedEventHandler_t495871123;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t563858374;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Uri
struct Uri_t19570940;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t3255436806;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.Net.DownloadDataCompletedEventArgs
struct DownloadDataCompletedEventArgs_t2512484440;
// System.ComponentModel.AsyncCompletedEventArgs
struct AsyncCompletedEventArgs_t83270938;
// System.Net.DownloadProgressChangedEventArgs
struct DownloadProgressChangedEventArgs_t3269688412;
// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t661276395;
// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_t3588930308;
// System.Net.OpenWriteCompletedEventArgs
struct OpenWriteCompletedEventArgs_t2397124231;
// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t2368064285;
// System.Net.UploadFileCompletedEventArgs
struct UploadFileCompletedEventArgs_t674814687;
// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_t4213021259;
// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t3898926532;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;
// System.Net.WebResponse
struct WebResponse_t1895226051;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_DownloadDataCompletedEventHandle4142697621.h"
#include "System_System_ComponentModel_AsyncCompletedEventHan626974191.h"
#include "System_System_Net_DownloadProgressChangedEventHand3683154451.h"
#include "System_System_Net_DownloadStringCompletedEventHand1433233068.h"
#include "System_System_Net_OpenReadCompletedEventHandler2999865175.h"
#include "System_System_Net_OpenWriteCompletedEventHandler1007953034.h"
#include "System_System_Net_UploadDataCompletedEventHandler1548853072.h"
#include "System_System_Net_UploadFileCompletedEventHandler3651750422.h"
#include "System_System_Net_UploadProgressChangedEventHandle4227408420.h"
#include "System_System_Net_UploadStringCompletedEventHandler495871123.h"
#include "System_System_Net_UploadValuesCompletedEventHandler563858374.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_Cache_RequestCachePolicy2663429579.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_Net_DownloadDataCompletedEventArgs2512484440.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_Net_DownloadProgressChangedEventArgs3269688412.h"
#include "System_System_Net_DownloadStringCompletedEventArgs661276395.h"
#include "System_System_Net_OpenReadCompletedEventArgs3588930308.h"
#include "System_System_Net_OpenWriteCompletedEventArgs2397124231.h"
#include "System_System_Net_UploadDataCompletedEventArgs2368064285.h"
#include "System_System_Net_UploadFileCompletedEventArgs674814687.h"
#include "System_System_Net_UploadProgressChangedEventArgs4213021259.h"
#include "System_System_Net_UploadStringCompletedEventArgs3898926532.h"
#include "System_System_Net_UploadValuesCompletedEventArgs3564452537.h"
#include "System_System_Net_WebRequest1365124353.h"

// System.Void System.Net.WebClient::.ctor()
extern "C"  void WebClient__ctor_m660733025 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::.cctor()
extern "C"  void WebClient__cctor_m196213842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadDataCompleted(System.Net.DownloadDataCompletedEventHandler)
extern "C"  void WebClient_add_DownloadDataCompleted_m2181557517 (WebClient_t1432723993 * __this, DownloadDataCompletedEventHandler_t4142697621 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadDataCompleted(System.Net.DownloadDataCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadDataCompleted_m3781813688 (WebClient_t1432723993 * __this, DownloadDataCompletedEventHandler_t4142697621 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadFileCompleted(System.ComponentModel.AsyncCompletedEventHandler)
extern "C"  void WebClient_add_DownloadFileCompleted_m2009126476 (WebClient_t1432723993 * __this, AsyncCompletedEventHandler_t626974191 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadFileCompleted(System.ComponentModel.AsyncCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadFileCompleted_m1452087229 (WebClient_t1432723993 * __this, AsyncCompletedEventHandler_t626974191 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadProgressChanged(System.Net.DownloadProgressChangedEventHandler)
extern "C"  void WebClient_add_DownloadProgressChanged_m2741836737 (WebClient_t1432723993 * __this, DownloadProgressChangedEventHandler_t3683154451 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadProgressChanged(System.Net.DownloadProgressChangedEventHandler)
extern "C"  void WebClient_remove_DownloadProgressChanged_m2584783280 (WebClient_t1432723993 * __this, DownloadProgressChangedEventHandler_t3683154451 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadStringCompleted(System.Net.DownloadStringCompletedEventHandler)
extern "C"  void WebClient_add_DownloadStringCompleted_m1726441145 (WebClient_t1432723993 * __this, DownloadStringCompletedEventHandler_t1433233068 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadStringCompleted(System.Net.DownloadStringCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadStringCompleted_m392365770 (WebClient_t1432723993 * __this, DownloadStringCompletedEventHandler_t1433233068 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_OpenReadCompleted(System.Net.OpenReadCompletedEventHandler)
extern "C"  void WebClient_add_OpenReadCompleted_m2835067041 (WebClient_t1432723993 * __this, OpenReadCompletedEventHandler_t2999865175 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_OpenReadCompleted(System.Net.OpenReadCompletedEventHandler)
extern "C"  void WebClient_remove_OpenReadCompleted_m144434576 (WebClient_t1432723993 * __this, OpenReadCompletedEventHandler_t2999865175 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_OpenWriteCompleted(System.Net.OpenWriteCompletedEventHandler)
extern "C"  void WebClient_add_OpenWriteCompleted_m910146717 (WebClient_t1432723993 * __this, OpenWriteCompletedEventHandler_t1007953034 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_OpenWriteCompleted(System.Net.OpenWriteCompletedEventHandler)
extern "C"  void WebClient_remove_OpenWriteCompleted_m1630429034 (WebClient_t1432723993 * __this, OpenWriteCompletedEventHandler_t1007953034 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadDataCompleted(System.Net.UploadDataCompletedEventHandler)
extern "C"  void WebClient_add_UploadDataCompleted_m2107828589 (WebClient_t1432723993 * __this, UploadDataCompletedEventHandler_t1548853072 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadDataCompleted(System.Net.UploadDataCompletedEventHandler)
extern "C"  void WebClient_remove_UploadDataCompleted_m3153846786 (WebClient_t1432723993 * __this, UploadDataCompletedEventHandler_t1548853072 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadFileCompleted(System.Net.UploadFileCompletedEventHandler)
extern "C"  void WebClient_add_UploadFileCompleted_m3014241513 (WebClient_t1432723993 * __this, UploadFileCompletedEventHandler_t3651750422 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadFileCompleted(System.Net.UploadFileCompletedEventHandler)
extern "C"  void WebClient_remove_UploadFileCompleted_m1354088722 (WebClient_t1432723993 * __this, UploadFileCompletedEventHandler_t3651750422 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadProgressChanged(System.Net.UploadProgressChangedEventHandler)
extern "C"  void WebClient_add_UploadProgressChanged_m592563609 (WebClient_t1432723993 * __this, UploadProgressChangedEventHandler_t4227408420 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadProgressChanged(System.Net.UploadProgressChangedEventHandler)
extern "C"  void WebClient_remove_UploadProgressChanged_m1482493482 (WebClient_t1432723993 * __this, UploadProgressChangedEventHandler_t4227408420 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadStringCompleted(System.Net.UploadStringCompletedEventHandler)
extern "C"  void WebClient_add_UploadStringCompleted_m1867246209 (WebClient_t1432723993 * __this, UploadStringCompletedEventHandler_t495871123 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadStringCompleted(System.Net.UploadStringCompletedEventHandler)
extern "C"  void WebClient_remove_UploadStringCompleted_m1867539248 (WebClient_t1432723993 * __this, UploadStringCompletedEventHandler_t495871123 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_add_UploadValuesCompleted_m3981342317 (WebClient_t1432723993 * __this, UploadValuesCompletedEventHandler_t563858374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_remove_UploadValuesCompleted_m4255774786 (WebClient_t1432723993 * __this, UploadValuesCompletedEventHandler_t563858374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::get_BaseAddress()
extern "C"  String_t* WebClient_get_BaseAddress_m1336470916 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_BaseAddress(System.String)
extern "C"  void WebClient_set_BaseAddress_m2611786153 (WebClient_t1432723993 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.WebClient::GetMustImplement()
extern "C"  Exception_t1927440687 * WebClient_GetMustImplement_m2193213122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Cache.RequestCachePolicy System.Net.WebClient::get_CachePolicy()
extern "C"  RequestCachePolicy_t2663429579 * WebClient_get_CachePolicy_m1581154802 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_CachePolicy(System.Net.Cache.RequestCachePolicy)
extern "C"  void WebClient_set_CachePolicy_m869912549 (WebClient_t1432723993 * __this, RequestCachePolicy_t2663429579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_UseDefaultCredentials()
extern "C"  bool WebClient_get_UseDefaultCredentials_m2741998118 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_UseDefaultCredentials(System.Boolean)
extern "C"  void WebClient_set_UseDefaultCredentials_m3200692751 (WebClient_t1432723993 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.WebClient::get_Credentials()
extern "C"  Il2CppObject * WebClient_get_Credentials_m2768182512 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Credentials(System.Net.ICredentials)
extern "C"  void WebClient_set_Credentials_m398044183 (WebClient_t1432723993 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebClient::get_Headers()
extern "C"  WebHeaderCollection_t3028142837 * WebClient_get_Headers_m2426120036 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Headers(System.Net.WebHeaderCollection)
extern "C"  void WebClient_set_Headers_m660701835 (WebClient_t1432723993 * __this, WebHeaderCollection_t3028142837 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Net.WebClient::get_QueryString()
extern "C"  NameValueCollection_t3047564564 * WebClient_get_QueryString_m2258445755 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_QueryString(System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_set_QueryString_m3492106480 (WebClient_t1432723993 * __this, NameValueCollection_t3047564564 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebClient::get_ResponseHeaders()
extern "C"  WebHeaderCollection_t3028142837 * WebClient_get_ResponseHeaders_m868751503 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Net.WebClient::get_Encoding()
extern "C"  Encoding_t663144255 * WebClient_get_Encoding_m2903625799 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Encoding(System.Text.Encoding)
extern "C"  void WebClient_set_Encoding_m3013612344 (WebClient_t1432723993 * __this, Encoding_t663144255 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebClient::get_Proxy()
extern "C"  Il2CppObject * WebClient_get_Proxy_m903524692 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Proxy(System.Net.IWebProxy)
extern "C"  void WebClient_set_Proxy_m3170122023 (WebClient_t1432723993 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_IsBusy()
extern "C"  bool WebClient_get_IsBusy_m1483534531 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CheckBusy()
extern "C"  void WebClient_CheckBusy_m3268863214 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::SetBusy()
extern "C"  void WebClient_SetBusy_m3197543546 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadData(System.String)
extern "C"  ByteU5BU5D_t3397334013* WebClient_DownloadData_m4117305359 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadData(System.Uri)
extern "C"  ByteU5BU5D_t3397334013* WebClient_DownloadData_m3099526144 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadDataCore(System.Uri,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_DownloadDataCore_m3229215021 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFile(System.String,System.String)
extern "C"  void WebClient_DownloadFile_m4150655671 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFile(System.Uri,System.String)
extern "C"  void WebClient_DownloadFile_m2408974818 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileCore(System.Uri,System.String,System.Object)
extern "C"  void WebClient_DownloadFileCore_m397348773 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenRead(System.String)
extern "C"  Stream_t3255436806 * WebClient_OpenRead_m2427089269 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenRead(System.Uri)
extern "C"  Stream_t3255436806 * WebClient_OpenRead_m747301558 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.String)
extern "C"  Stream_t3255436806 * WebClient_OpenWrite_m1126624624 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.String,System.String)
extern "C"  Stream_t3255436806 * WebClient_OpenWrite_m2602951472 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.Uri)
extern "C"  Stream_t3255436806 * WebClient_OpenWrite_m161606301 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.Uri,System.String)
extern "C"  Stream_t3255436806 * WebClient_OpenWrite_m731378795 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DetermineMethod(System.Uri,System.String,System.Boolean)
extern "C"  String_t* WebClient_DetermineMethod_m2758189946 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.String,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadData_m1942617791 (WebClient_t1432723993 * __this, String_t* ___address0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.String,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadData_m494242113 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___method1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.Uri,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadData_m3206964324 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.Uri,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadData_m2919246968 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadDataCore(System.Uri,System.String,System.Byte[],System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadDataCore_m2013412491 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, ByteU5BU5D_t3397334013* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.String,System.String)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadFile_m2928983230 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.Uri,System.String)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadFile_m3868156371 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.String,System.String,System.String)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadFile_m1705302750 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.Uri,System.String,System.String)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadFile_m1935101409 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFileCore(System.Uri,System.String,System.String,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadFileCore_m436676574 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___fileName2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValues_m2097758147 (WebClient_t1432723993 * __this, String_t* ___address0, NameValueCollection_t3047564564 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.String,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValues_m3336588449 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.Uri,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValues_m3123306670 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, NameValueCollection_t3047564564 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.Uri,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValues_m3103273722 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValuesCore(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValuesCore_m1361004655 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, String_t* ___method1, NameValueCollection_t3047564564 * ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DownloadString(System.String)
extern "C"  String_t* WebClient_DownloadString_m1757636243 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DownloadString(System.Uri)
extern "C"  String_t* WebClient_DownloadString_m2224253444 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m2048343500 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.String,System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m3935565856 (WebClient_t1432723993 * __this, String_t* ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.Uri,System.String)
extern "C"  String_t* WebClient_UploadString_m1632885553 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.Uri,System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m2030690399 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::CreateUri(System.String)
extern "C"  Uri_t19570940 * WebClient_CreateUri_m255501139 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::CreateUri(System.Uri)
extern "C"  Uri_t19570940 * WebClient_CreateUri_m411985792 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::GetQueryString(System.Boolean)
extern "C"  String_t* WebClient_GetQueryString_m1589491618 (WebClient_t1432723993 * __this, bool ___add_qmark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::MakeUri(System.String)
extern "C"  Uri_t19570940 * WebClient_MakeUri_m159089413 (WebClient_t1432723993 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_SetupRequest_m3399367600 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri,System.String,System.Boolean)
extern "C"  WebRequest_t1365124353 * WebClient_SetupRequest_m1206909435 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::ReadAll(System.IO.Stream,System.Int32,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_ReadAll_m1147384516 (WebClient_t1432723993 * __this, Stream_t3255436806 * ___stream0, int32_t ___length1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UrlEncode(System.String)
extern "C"  String_t* WebClient_UrlEncode_m2772509467 (WebClient_t1432723993 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UrlEncodeAndWrite(System.IO.Stream,System.Byte[])
extern "C"  void WebClient_UrlEncodeAndWrite_m3388978810 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CancelAsync()
extern "C"  void WebClient_CancelAsync_m3394329165 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CompleteAsync()
extern "C"  void WebClient_CompleteAsync_m1173274810 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadDataAsync(System.Uri)
extern "C"  void WebClient_DownloadDataAsync_m1960773512 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadDataAsync(System.Uri,System.Object)
extern "C"  void WebClient_DownloadDataAsync_m2914366350 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileAsync(System.Uri,System.String)
extern "C"  void WebClient_DownloadFileAsync_m299188038 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileAsync(System.Uri,System.String,System.Object)
extern "C"  void WebClient_DownloadFileAsync_m367122348 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadStringAsync(System.Uri)
extern "C"  void WebClient_DownloadStringAsync_m1656466799 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadStringAsync(System.Uri,System.Object)
extern "C"  void WebClient_DownloadStringAsync_m3576107017 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenReadAsync(System.Uri)
extern "C"  void WebClient_OpenReadAsync_m1444300656 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenReadAsync(System.Uri,System.Object)
extern "C"  void WebClient_OpenReadAsync_m780618858 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri)
extern "C"  void WebClient_OpenWriteAsync_m4186891423 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri,System.String)
extern "C"  void WebClient_OpenWriteAsync_m1873763509 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri,System.String,System.Object)
extern "C"  void WebClient_OpenWriteAsync_m970689651 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.Byte[])
extern "C"  void WebClient_UploadDataAsync_m3902155958 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.String,System.Byte[])
extern "C"  void WebClient_UploadDataAsync_m165143414 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.String,System.Byte[],System.Object)
extern "C"  void WebClient_UploadDataAsync_m1922067916 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, ByteU5BU5D_t3397334013* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String)
extern "C"  void WebClient_UploadFileAsync_m3935614465 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String,System.String)
extern "C"  void WebClient_UploadFileAsync_m3102309151 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String,System.String,System.Object)
extern "C"  void WebClient_UploadFileAsync_m1720499365 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___fileName2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String)
extern "C"  void WebClient_UploadStringAsync_m3268514816 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String,System.String)
extern "C"  void WebClient_UploadStringAsync_m2356510292 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String,System.String,System.Object)
extern "C"  void WebClient_UploadStringAsync_m2019149466 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, String_t* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_UploadValuesAsync_m3233038012 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, NameValueCollection_t3047564564 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_UploadValuesAsync_m1764200028 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  void WebClient_UploadValuesAsync_m3820210094 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___values2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadDataCompleted(System.Net.DownloadDataCompletedEventArgs)
extern "C"  void WebClient_OnDownloadDataCompleted_m2250972001 (WebClient_t1432723993 * __this, DownloadDataCompletedEventArgs_t2512484440 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadFileCompleted(System.ComponentModel.AsyncCompletedEventArgs)
extern "C"  void WebClient_OnDownloadFileCompleted_m699159956 (WebClient_t1432723993 * __this, AsyncCompletedEventArgs_t83270938 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadProgressChanged(System.Net.DownloadProgressChangedEventArgs)
extern "C"  void WebClient_OnDownloadProgressChanged_m2431565293 (WebClient_t1432723993 * __this, DownloadProgressChangedEventArgs_t3269688412 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadStringCompleted(System.Net.DownloadStringCompletedEventArgs)
extern "C"  void WebClient_OnDownloadStringCompleted_m2360518317 (WebClient_t1432723993 * __this, DownloadStringCompletedEventArgs_t661276395 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnOpenReadCompleted(System.Net.OpenReadCompletedEventArgs)
extern "C"  void WebClient_OnOpenReadCompleted_m1678544973 (WebClient_t1432723993 * __this, OpenReadCompletedEventArgs_t3588930308 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnOpenWriteCompleted(System.Net.OpenWriteCompletedEventArgs)
extern "C"  void WebClient_OnOpenWriteCompleted_m3872805193 (WebClient_t1432723993 * __this, OpenWriteCompletedEventArgs_t2397124231 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadDataCompleted(System.Net.UploadDataCompletedEventArgs)
extern "C"  void WebClient_OnUploadDataCompleted_m2629599097 (WebClient_t1432723993 * __this, UploadDataCompletedEventArgs_t2368064285 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadFileCompleted(System.Net.UploadFileCompletedEventArgs)
extern "C"  void WebClient_OnUploadFileCompleted_m778445853 (WebClient_t1432723993 * __this, UploadFileCompletedEventArgs_t674814687 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadProgressChanged(System.Net.UploadProgressChangedEventArgs)
extern "C"  void WebClient_OnUploadProgressChanged_m2914536141 (WebClient_t1432723993 * __this, UploadProgressChangedEventArgs_t4213021259 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadStringCompleted(System.Net.UploadStringCompletedEventArgs)
extern "C"  void WebClient_OnUploadStringCompleted_m3239230573 (WebClient_t1432723993 * __this, UploadStringCompletedEventArgs_t3898926532 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadValuesCompleted(System.Net.UploadValuesCompletedEventArgs)
extern "C"  void WebClient_OnUploadValuesCompleted_m1965360313 (WebClient_t1432723993 * __this, UploadValuesCompletedEventArgs_t3564452537 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest,System.IAsyncResult)
extern "C"  WebResponse_t1895226051 * WebClient_GetWebResponse_m3546791604 (WebClient_t1432723993 * __this, WebRequest_t1365124353 * ___request0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::GetWebRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_GetWebRequest_m3663064103 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest)
extern "C"  WebResponse_t1895226051 * WebClient_GetWebResponse_m247274309 (WebClient_t1432723993 * __this, WebRequest_t1365124353 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadDataAsync>m__C(System.Object)
extern "C"  void WebClient_U3CDownloadDataAsyncU3Em__C_m2205733421 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadFileAsync>m__D(System.Object)
extern "C"  void WebClient_U3CDownloadFileAsyncU3Em__D_m3811773900 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadStringAsync>m__E(System.Object)
extern "C"  void WebClient_U3CDownloadStringAsyncU3Em__E_m997727100 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<OpenReadAsync>m__F(System.Object)
extern "C"  void WebClient_U3COpenReadAsyncU3Em__F_m607935152 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<OpenWriteAsync>m__10(System.Object)
extern "C"  void WebClient_U3COpenWriteAsyncU3Em__10_m172383792 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadDataAsync>m__11(System.Object)
extern "C"  void WebClient_U3CUploadDataAsyncU3Em__11_m812488329 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadFileAsync>m__12(System.Object)
extern "C"  void WebClient_U3CUploadFileAsyncU3Em__12_m1925727598 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadStringAsync>m__13(System.Object)
extern "C"  void WebClient_U3CUploadStringAsyncU3Em__13_m3910504356 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadValuesAsync>m__14(System.Object)
extern "C"  void WebClient_U3CUploadValuesAsyncU3Em__14_m1338409780 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
