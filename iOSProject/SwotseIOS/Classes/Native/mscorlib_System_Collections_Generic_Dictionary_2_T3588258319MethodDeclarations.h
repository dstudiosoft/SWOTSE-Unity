﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,Firebase.FirebaseApp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m791883432(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3588258319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1237139657_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,Firebase.FirebaseApp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1008953016(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3588258319 *, IntPtr_t, FirebaseApp_t210707726 *, const MethodInfo*))Transform_1_Invoke_m2675095773_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,Firebase.FirebaseApp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2754626255(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3588258319 *, IntPtr_t, FirebaseApp_t210707726 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4270324442_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,Firebase.FirebaseApp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3952798230(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3588258319 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m4121732147_gshared)(__this, ___result0, method)
