﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BattleReportItemsLine
struct BattleReportItemsLine_t301030678;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<CityTreasureModel>
struct List_1_t877182185;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportItemsTableController
struct  BattleReportItemsTableController_t2343543308  : public MonoBehaviour_t1158329972
{
public:
	// BattleReportItemsLine BattleReportItemsTableController::m_cellPrefab
	BattleReportItemsLine_t301030678 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportItemsTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportItemsTableController::items
	List_1_t877182185 * ___items_4;
	// System.Int32 BattleReportItemsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t2343543308, ___m_cellPrefab_2)); }
	inline BattleReportItemsLine_t301030678 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportItemsLine_t301030678 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportItemsLine_t301030678 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t2343543308, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_items_4() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t2343543308, ___items_4)); }
	inline List_1_t877182185 * get_items_4() const { return ___items_4; }
	inline List_1_t877182185 ** get_address_of_items_4() { return &___items_4; }
	inline void set_items_4(List_1_t877182185 * value)
	{
		___items_4 = value;
		Il2CppCodeGenWriteBarrier(&___items_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t2343543308, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
