﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.IAppConfigExtensions
struct IAppConfigExtensions_t273226812;
// Firebase.Platform.IAuthService
struct IAuthService_t3650048058;
// Firebase.Platform.ICertificateService
struct ICertificateService_t1393461683;
// Firebase.Platform.IClockService
struct IClockService_t3133746056;
// Firebase.Platform.IHttpFactoryService
struct IHttpFactoryService_t3971217380;
// Firebase.Platform.ILoggingService
struct ILoggingService_t3100627017;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Platform.Services::.cctor()
extern "C"  void Services__cctor_m877168546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.IAppConfigExtensions Firebase.Platform.Services::get_AppConfig()
extern "C"  Il2CppObject * Services_get_AppConfig_m3501617754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_AppConfig(Firebase.Platform.IAppConfigExtensions)
extern "C"  void Services_set_AppConfig_m3406389729 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_Auth(Firebase.Platform.IAuthService)
extern "C"  void Services_set_Auth_m3841026076 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.ICertificateService Firebase.Platform.Services::get_RootCerts()
extern "C"  Il2CppObject * Services_get_RootCerts_m3976237201 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_RootCerts(Firebase.Platform.ICertificateService)
extern "C"  void Services_set_RootCerts_m497540332 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_Clock(Firebase.Platform.IClockService)
extern "C"  void Services_set_Clock_m2964054414 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_HttpFactory(Firebase.Platform.IHttpFactoryService)
extern "C"  void Services_set_HttpFactory_m1883043390 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.ILoggingService Firebase.Platform.Services::get_Logging()
extern "C"  Il2CppObject * Services_get_Logging_m2195103441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Services::set_Logging(Firebase.Platform.ILoggingService)
extern "C"  void Services_set_Logging_m1595824188 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
