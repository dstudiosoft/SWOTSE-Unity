﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.String
struct String_t;
// SmartLocalization.ChangeLanguageEventHandler
struct ChangeLanguageEventHandler_t2032193146;
// SmartLocalization.LanguageManager
struct LanguageManager_t2767934455;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// SmartLocalization.LanguageDataHandler
struct LanguageDataHandler_t3889722316;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t2617245831;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t1555065447;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2060094783;
// SmartLocalization.ILocalizedAssetLoader
struct ILocalizedAssetLoader_t2808879256;
// SmartLocalization.RuntimeLocalizedAssetLoader
struct RuntimeLocalizedAssetLoader_t316402138;
// SmartLocalization.LocalizedObject
struct LocalizedObject_t1409469237;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>
struct KeyCollection_t1940831260;
// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t878650876;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>
struct List_1_t2881543979;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct List_1_t3532169525;
// System.Predicate`1<SmartLocalization.SmartCultureInfo>
struct Predicate_1_t2885388907;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// SmartLocalization.SmartCultureInfoCollection
struct SmartCultureInfoCollection_t1809038765;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0
struct U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546;
// SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1
struct U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798;
// SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2
struct U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582;
// SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3
struct U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.IO.TextReader
struct TextReader_t283511965;
// SmartLocalization.LocalizedAudioSource
struct LocalizedAudioSource_t267215401;
// System.Delegate
struct Delegate_t1188392813;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// SmartLocalization.LocalizedGUIText
struct LocalizedGUIText_t2415330885;
// UnityEngine.GUIText
struct GUIText_t402233326;
// SmartLocalization.LocalizedGUITexture
struct LocalizedGUITexture_t2717994319;
// UnityEngine.GUITexture
struct GUITexture_t951903601;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t132201056;
// SmartLocalization.LocalizedObject[]
struct LocalizedObjectU5BU5D_t3770507352;
// SmartLocalization.SmartCultureInfo[]
struct SmartCultureInfoU5BU5D_t204387046;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.Collections.Generic.RBTree
struct RBTree_t4095273678;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>
struct NodeHelper_t332667217;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>
struct Stack_1_t2901053741;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;

extern String_t* _stringLiteral3612373897;
extern String_t* _stringLiteral3954355219;
extern String_t* _stringLiteral1179142834;
extern String_t* _stringLiteral3366331569;
extern String_t* _stringLiteral1005565038;
extern String_t* _stringLiteral4067969849;
extern String_t* _stringLiteral2851651844;
extern String_t* _stringLiteral3952115132;
extern String_t* _stringLiteral3897511319;
extern String_t* _stringLiteral486981520;
extern String_t* _stringLiteral3500168455;
extern String_t* _stringLiteral884659934;
extern String_t* _stringLiteral431833368;
extern String_t* _stringLiteral17267921;
extern String_t* _stringLiteral2748939550;
extern String_t* _stringLiteral442750400;
extern String_t* _stringLiteral3552476487;
extern String_t* _stringLiteral1109991769;
extern String_t* _stringLiteral4285556778;
extern String_t* _stringLiteral4280934045;
extern String_t* _stringLiteral2396719944;
extern String_t* _stringLiteral3344213735;
extern String_t* _stringLiteral2392163715;
extern String_t* _stringLiteral3212171020;
extern String_t* _stringLiteral2595179563;
extern String_t* _stringLiteral500347555;
extern String_t* _stringLiteral2136083539;
extern String_t* _stringLiteral2730649301;
extern String_t* _stringLiteral3129577868;
extern String_t* _stringLiteral3901952298;
extern String_t* _stringLiteral3236904726;
extern String_t* _stringLiteral1203471839;
extern String_t* _stringLiteral760857587;
extern String_t* _stringLiteral577400165;
extern String_t* _stringLiteral2458762727;
extern String_t* _stringLiteral2677988819;
extern String_t* _stringLiteral387968785;
extern String_t* _stringLiteral4168042063;
extern String_t* _stringLiteral1708475082;
extern String_t* _stringLiteral2854237347;
extern String_t* _stringLiteral933504174;
extern const uint32_t ApplicationExtensions_GetStringValueOfSystemLanguage_m570700328_MetadataUsageId;
extern RuntimeClass* SortedDictionary_2_t2617245831_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SortedDictionary_2__ctor_m964777687_RuntimeMethod_var;
extern const uint32_t LanguageDataHandler__ctor_m1941805053_MetadataUsageId;
extern RuntimeClass* RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var;
extern const uint32_t LanguageDataHandler_get_AssetLoader_m2232983836_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SortedDictionary_2_get_Count_m634161304_RuntimeMethod_var;
extern String_t* _stringLiteral1798215199;
extern const uint32_t LanguageDataHandler_Load_m1742461671_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SortedDictionary_2_GetEnumerator_m3922890767_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3473425924_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var;
extern const RuntimeMethod* SortedDictionary_2_ContainsKey_m1995604387_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m52858533_RuntimeMethod_var;
extern const RuntimeMethod* SortedDictionary_2_set_Item_m3916906723_RuntimeMethod_var;
extern const RuntimeMethod* SortedDictionary_2_Add_m1412840442_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1285001833_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m235344939_RuntimeMethod_var;
extern String_t* _stringLiteral982267627;
extern String_t* _stringLiteral2945251091;
extern const uint32_t LanguageDataHandler_Append_m248544474_MetadataUsageId;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const uint32_t LanguageDataHandler_GetKeysWithinCategory_m2182585259_MetadataUsageId;
extern const RuntimeMethod* SortedDictionary_2_get_Keys_m3652780046_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m928983596_RuntimeMethod_var;
extern const uint32_t LanguageDataHandler_GetAllKeys_m1125042489_MetadataUsageId;
extern const RuntimeMethod* SortedDictionary_2_TryGetValue_m3861572157_RuntimeMethod_var;
extern const uint32_t LanguageDataHandler_GetLocalizedObject_m3611374437_MetadataUsageId;
extern String_t* _stringLiteral2354339495;
extern String_t* _stringLiteral1513108754;
extern const uint32_t LanguageDataHandler_GetTextValue_m495737691_MetadataUsageId;
extern RuntimeClass* LanguageParser_t1152791527_il2cpp_TypeInfo_var;
extern const uint32_t LanguageDataHandler_LoadLanguageDictionary_m2285733254_MetadataUsageId;
extern const uint32_t LanguageDataHandler_CheckLanguageOverrideCode_m1870029841_MetadataUsageId;
extern RuntimeClass* LanguageDataHandler_t3889722316_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3454842811;
extern const uint32_t LanguageManager__ctor_m3510348828_MetadataUsageId;
extern RuntimeClass* LanguageManager_t2767934455_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisLanguageManager_t2767934455_m1920039193_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisLanguageManager_t2767934455_m4185499804_RuntimeMethod_var;
extern String_t* _stringLiteral2583110339;
extern const uint32_t LanguageManager_get_Instance_m861325331_MetadataUsageId;
extern const uint32_t LanguageManager_SetDontDestroyOnLoad_m13667428_MetadataUsageId;
extern const uint32_t LanguageManager_get_HasInstance_m2564662803_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3729392029_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3162138650_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2276455407_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m3111619026_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1509931054_RuntimeMethod_var;
extern const uint32_t LanguageManager_OnAfterDeserialize_m3239827748_MetadataUsageId;
extern RuntimeClass* List_1_t2881543979_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m981406146_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2873794602_RuntimeMethod_var;
extern const uint32_t LanguageManager_OnBeforeSerialize_m2833749936_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3302800229_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3665229156_RuntimeMethod_var;
extern const uint32_t LanguageManager_get_RawTextDatabase_m1351981778_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2960725483_RuntimeMethod_var;
extern const uint32_t LanguageManager_get_NumberOfSupportedLanguages_m2642276512_MetadataUsageId;
extern RuntimeClass* Predicate_1_t2885388907_il2cpp_TypeInfo_var;
extern const RuntimeMethod* LanguageManager_U3CAwakeU3Em__0_m3454044807_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m2314931459_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Find_m4070095342_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1520579522_RuntimeMethod_var;
extern String_t* _stringLiteral1133921496;
extern String_t* _stringLiteral3186498657;
extern String_t* _stringLiteral857763534;
extern String_t* _stringLiteral2952380563;
extern const uint32_t LanguageManager_Awake_m2745406980_MetadataUsageId;
extern const uint32_t LanguageManager_OnApplicationQuit_m2266523370_MetadataUsageId;
extern RuntimeClass* LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var;
extern RuntimeClass* TextAsset_t3022178571_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral272499010;
extern const uint32_t LanguageManager_LoadAvailableCultures_m3449409331_MetadataUsageId;
extern String_t* _stringLiteral2081855419;
extern const uint32_t LanguageManager_ChangeLanguage_m2374647000_MetadataUsageId;
extern String_t* _stringLiteral2379534151;
extern String_t* _stringLiteral1085397908;
extern String_t* _stringLiteral67993702;
extern const uint32_t LanguageManager_LoadLanguage_m577556272_MetadataUsageId;
extern const uint32_t LanguageManager_GetSupportedSystemLanguageCode_m593164813_MetadataUsageId;
extern RuntimeClass* U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3311227881_RuntimeMethod_var;
extern const uint32_t LanguageManager_GetDeviceCultureIfSupported_m2865750724_MetadataUsageId;
extern RuntimeClass* U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m4210711579_RuntimeMethod_var;
extern String_t* _stringLiteral2890855871;
extern const uint32_t LanguageManager_IsCultureSupported_m294811961_MetadataUsageId;
extern RuntimeClass* U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m3271210525_RuntimeMethod_var;
extern const uint32_t LanguageManager_IsLanguageSupportedEnglishName_m3736923875_MetadataUsageId;
extern RuntimeClass* U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m2012133178_RuntimeMethod_var;
extern const uint32_t LanguageManager_GetCultureInfo_m1993048463_MetadataUsageId;
extern String_t* _stringLiteral112791673;
extern const uint32_t LanguageManager_GetSupportedLanguages_m3762701334_MetadataUsageId;
extern const RuntimeMethod* LanguageDataHandler_GetAsset_TisAudioClip_t3680889665_m950255953_RuntimeMethod_var;
extern const uint32_t LanguageManager_GetAudioClip_m1838735899_MetadataUsageId;
extern const RuntimeMethod* LanguageDataHandler_GetAsset_TisGameObject_t1113636619_m521626920_RuntimeMethod_var;
extern const uint32_t LanguageManager_GetPrefab_m980355803_MetadataUsageId;
extern const RuntimeMethod* LanguageDataHandler_GetAsset_TisTexture_t3661962703_m2393095502_RuntimeMethod_var;
extern const uint32_t LanguageManager_GetTexture_m1160349625_MetadataUsageId;
extern const uint32_t LanguageManager_U3CAwakeU3Em__0_m3454044807_MetadataUsageId;
extern const uint32_t U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m2012133178_MetadataUsageId;
extern const uint32_t U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3311227881_MetadataUsageId;
extern const uint32_t U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m4210711579_MetadataUsageId;
extern const uint32_t U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m3271210525_MetadataUsageId;
extern RuntimeClass* StringReader_t3465604688_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2361950774;
extern String_t* _stringLiteral2596964267;
extern const uint32_t LanguageParser_LoadLanguage_m379871964_MetadataUsageId;
extern String_t* _stringLiteral2037252866;
extern const uint32_t LanguageParser_ReadElements_m1808351402_MetadataUsageId;
extern RuntimeClass* LocalizedObject_t1409469237_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral62725243;
extern String_t* _stringLiteral3493618073;
extern String_t* _stringLiteral550189811;
extern const uint32_t LanguageParser_ReadData_m1441422953_MetadataUsageId;
extern String_t* _stringLiteral3614473796;
extern const uint32_t LanguageParser__cctor_m2820239222_MetadataUsageId;
extern String_t* _stringLiteral3452614530;
extern const uint32_t LanguageRuntimeData_LanguageFilePath_m1120529596_MetadataUsageId;
extern const uint32_t LanguageRuntimeData_AvailableCulturesFilePath_m3409607084_MetadataUsageId;
extern String_t* _stringLiteral3452614529;
extern const uint32_t LanguageRuntimeData_AudioFilesFolderPath_m4027902490_MetadataUsageId;
extern const uint32_t LanguageRuntimeData_TexturesFolderPath_m4143263055_MetadataUsageId;
extern const uint32_t LanguageRuntimeData_PrefabsFolderPath_m1749946394_MetadataUsageId;
extern String_t* _stringLiteral2813389331;
extern String_t* _stringLiteral2143291852;
extern String_t* _stringLiteral2652779316;
extern String_t* _stringLiteral2506181452;
extern String_t* _stringLiteral3727959427;
extern const uint32_t LanguageRuntimeData__cctor_m1578531035_MetadataUsageId;
extern String_t* _stringLiteral2911642733;
extern const uint32_t LocalizedAudioSource__ctor_m4153904623_MetadataUsageId;
extern RuntimeClass* ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var;
extern const RuntimeMethod* LocalizedAudioSource_OnChangeLanguage_m3163652545_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t3935305588_m286538782_RuntimeMethod_var;
extern const uint32_t LocalizedAudioSource_Start_m1039871505_MetadataUsageId;
extern const uint32_t LocalizedAudioSource_OnDestroy_m2449684192_MetadataUsageId;
extern const uint32_t LocalizedAudioSource_OnChangeLanguage_m3163652545_MetadataUsageId;
extern const uint32_t LocalizedGUIText__ctor_m373607288_MetadataUsageId;
extern const RuntimeMethod* LocalizedGUIText_OnChangeLanguage_m236516013_RuntimeMethod_var;
extern const uint32_t LocalizedGUIText_Start_m3774386156_MetadataUsageId;
extern const uint32_t LocalizedGUIText_OnDestroy_m2973192016_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGUIText_t402233326_m1476680223_RuntimeMethod_var;
extern const uint32_t LocalizedGUIText_OnChangeLanguage_m236516013_MetadataUsageId;
extern const uint32_t LocalizedGUITexture__ctor_m1822977010_MetadataUsageId;
extern const RuntimeMethod* LocalizedGUITexture_OnChangeLanguage_m794962917_RuntimeMethod_var;
extern const uint32_t LocalizedGUITexture_Start_m886336810_MetadataUsageId;
extern const uint32_t LocalizedGUITexture_OnDestroy_m3033520291_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGUITexture_t951903601_m1070565238_RuntimeMethod_var;
extern const uint32_t LocalizedGUITexture_OnChangeLanguage_m794962917_MetadataUsageId;
extern const uint32_t LocalizedObject_set_OverrideObjectLanguageCode_m2380448063_MetadataUsageId;
extern String_t* _stringLiteral3577837154;
extern String_t* _stringLiteral1494336486;
extern String_t* _stringLiteral3888186860;
extern String_t* _stringLiteral605123219;
extern String_t* _stringLiteral2226978419;
extern const uint32_t LocalizedObject_GetLocalizedObjectType_m2843596427_MetadataUsageId;
extern String_t* _stringLiteral1233953362;
extern const uint32_t LocalizedObject_GetCleanKey_m2035573102_MetadataUsageId;
extern String_t* _stringLiteral1700742349;
extern const uint32_t LocalizedObject_GetLocalizedObjectTypeStringValue_m652933876_MetadataUsageId;
extern String_t* _stringLiteral2023866285;
extern String_t* _stringLiteral3452614643;
extern const uint32_t LocalizedObject__cctor_m1037640625_MetadataUsageId;
extern const uint32_t RuntimeLocalizedAssetLoader_GetAssetFolderPath_m2028859358_MetadataUsageId;
extern const RuntimeType* GameObject_t1113636619_0_0_0_var;
extern const RuntimeType* AudioClip_t3680889665_0_0_0_var;
extern const RuntimeType* Texture_t3661962703_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeLocalizedAssetLoader__cctor_m930639038_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1567056791;
extern const uint32_t SmartCultureInfo_ToString_m3185995941_MetadataUsageId;
extern RuntimeClass* List_1_t3532169525_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3177292912_RuntimeMethod_var;
extern const uint32_t SmartCultureInfoCollection__ctor_m2569938504_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m3606668418_RuntimeMethod_var;
extern String_t* _stringLiteral2057740977;
extern const uint32_t SmartCultureInfoCollection_AddCultureInfo_m3890672140_MetadataUsageId;
extern RuntimeClass* SmartCultureInfoCollection_t1809038765_il2cpp_TypeInfo_var;
extern const uint32_t SmartCultureInfoCollectionDeserializer_Deserialize_m3273799526_MetadataUsageId;
extern String_t* _stringLiteral889386244;
extern const uint32_t SmartCultureInfoCollectionDeserializer_ReadElements_m2123939648_MetadataUsageId;
extern RuntimeClass* SmartCultureInfo_t2060094783_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral335559975;
extern String_t* _stringLiteral333543461;
extern String_t* _stringLiteral508791134;
extern String_t* _stringLiteral3671028101;
extern const uint32_t SmartCultureInfoCollectionDeserializer_ReadData_m1923819025_MetadataUsageId;

struct ObjectU5BU5D_t2843939325;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef LANGUAGERUNTIMEDATA_T3907746602_H
#define LANGUAGERUNTIMEDATA_T3907746602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageRuntimeData
struct  LanguageRuntimeData_t3907746602  : public RuntimeObject
{
public:

public:
};

struct LanguageRuntimeData_t3907746602_StaticFields
{
public:
	// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFileName
	String_t* ___AvailableCulturesFileName_0;
	// System.String SmartLocalization.LanguageRuntimeData::rootLanguageName
	String_t* ___rootLanguageName_1;
	// System.String SmartLocalization.LanguageRuntimeData::AudioFileFolderName
	String_t* ___AudioFileFolderName_2;
	// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderName
	String_t* ___TexturesFolderName_3;
	// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderName
	String_t* ___PrefabsFolderName_4;

public:
	inline static int32_t get_offset_of_AvailableCulturesFileName_0() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___AvailableCulturesFileName_0)); }
	inline String_t* get_AvailableCulturesFileName_0() const { return ___AvailableCulturesFileName_0; }
	inline String_t** get_address_of_AvailableCulturesFileName_0() { return &___AvailableCulturesFileName_0; }
	inline void set_AvailableCulturesFileName_0(String_t* value)
	{
		___AvailableCulturesFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableCulturesFileName_0), value);
	}

	inline static int32_t get_offset_of_rootLanguageName_1() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___rootLanguageName_1)); }
	inline String_t* get_rootLanguageName_1() const { return ___rootLanguageName_1; }
	inline String_t** get_address_of_rootLanguageName_1() { return &___rootLanguageName_1; }
	inline void set_rootLanguageName_1(String_t* value)
	{
		___rootLanguageName_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootLanguageName_1), value);
	}

	inline static int32_t get_offset_of_AudioFileFolderName_2() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___AudioFileFolderName_2)); }
	inline String_t* get_AudioFileFolderName_2() const { return ___AudioFileFolderName_2; }
	inline String_t** get_address_of_AudioFileFolderName_2() { return &___AudioFileFolderName_2; }
	inline void set_AudioFileFolderName_2(String_t* value)
	{
		___AudioFileFolderName_2 = value;
		Il2CppCodeGenWriteBarrier((&___AudioFileFolderName_2), value);
	}

	inline static int32_t get_offset_of_TexturesFolderName_3() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___TexturesFolderName_3)); }
	inline String_t* get_TexturesFolderName_3() const { return ___TexturesFolderName_3; }
	inline String_t** get_address_of_TexturesFolderName_3() { return &___TexturesFolderName_3; }
	inline void set_TexturesFolderName_3(String_t* value)
	{
		___TexturesFolderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___TexturesFolderName_3), value);
	}

	inline static int32_t get_offset_of_PrefabsFolderName_4() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t3907746602_StaticFields, ___PrefabsFolderName_4)); }
	inline String_t* get_PrefabsFolderName_4() const { return ___PrefabsFolderName_4; }
	inline String_t** get_address_of_PrefabsFolderName_4() { return &___PrefabsFolderName_4; }
	inline void set_PrefabsFolderName_4(String_t* value)
	{
		___PrefabsFolderName_4 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabsFolderName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGERUNTIMEDATA_T3907746602_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1632706988_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t132201056 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t132201056 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t132201056 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t132201056 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef LIST_1_T2881543979_H
#define LIST_1_T2881543979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>
struct  List_1_t2881543979  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	LocalizedObjectU5BU5D_t3770507352* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2881543979, ____items_1)); }
	inline LocalizedObjectU5BU5D_t3770507352* get__items_1() const { return ____items_1; }
	inline LocalizedObjectU5BU5D_t3770507352** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(LocalizedObjectU5BU5D_t3770507352* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2881543979, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2881543979, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2881543979_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	LocalizedObjectU5BU5D_t3770507352* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2881543979_StaticFields, ___EmptyArray_4)); }
	inline LocalizedObjectU5BU5D_t3770507352* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline LocalizedObjectU5BU5D_t3770507352** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(LocalizedObjectU5BU5D_t3770507352* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2881543979_H
#ifndef LANGUAGEPARSER_T1152791527_H
#define LANGUAGEPARSER_T1152791527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageParser
struct  LanguageParser_t1152791527  : public RuntimeObject
{
public:

public:
};

struct LanguageParser_t1152791527_StaticFields
{
public:
	// System.String SmartLocalization.LanguageParser::xmlHeader
	String_t* ___xmlHeader_0;

public:
	inline static int32_t get_offset_of_xmlHeader_0() { return static_cast<int32_t>(offsetof(LanguageParser_t1152791527_StaticFields, ___xmlHeader_0)); }
	inline String_t* get_xmlHeader_0() const { return ___xmlHeader_0; }
	inline String_t** get_address_of_xmlHeader_0() { return &___xmlHeader_0; }
	inline void set_xmlHeader_0(String_t* value)
	{
		___xmlHeader_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlHeader_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEPARSER_T1152791527_H
#ifndef SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#define SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfoCollectionDeserializer
struct  SmartCultureInfoCollectionDeserializer_t1510284870  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFOCOLLECTIONDESERIALIZER_T1510284870_H
#ifndef KEYCOLLECTION_T1940831260_H
#define KEYCOLLECTION_T1940831260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>
struct  KeyCollection_t1940831260  : public RuntimeObject
{
public:
	// System.Collections.Generic.SortedDictionary`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection::_dic
	SortedDictionary_2_t2617245831 * ____dic_0;

public:
	inline static int32_t get_offset_of__dic_0() { return static_cast<int32_t>(offsetof(KeyCollection_t1940831260, ____dic_0)); }
	inline SortedDictionary_2_t2617245831 * get__dic_0() const { return ____dic_0; }
	inline SortedDictionary_2_t2617245831 ** get_address_of__dic_0() { return &____dic_0; }
	inline void set__dic_0(SortedDictionary_2_t2617245831 * value)
	{
		____dic_0 = value;
		Il2CppCodeGenWriteBarrier((&____dic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTION_T1940831260_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef SMARTCULTUREINFOCOLLECTION_T1809038765_H
#define SMARTCULTUREINFOCOLLECTION_T1809038765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfoCollection
struct  SmartCultureInfoCollection_t1809038765  : public RuntimeObject
{
public:
	// System.Int32 SmartLocalization.SmartCultureInfoCollection::version
	int32_t ___version_1;
	// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.SmartCultureInfoCollection::cultureInfos
	List_1_t3532169525 * ___cultureInfos_2;

public:
	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(SmartCultureInfoCollection_t1809038765, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_cultureInfos_2() { return static_cast<int32_t>(offsetof(SmartCultureInfoCollection_t1809038765, ___cultureInfos_2)); }
	inline List_1_t3532169525 * get_cultureInfos_2() const { return ___cultureInfos_2; }
	inline List_1_t3532169525 ** get_address_of_cultureInfos_2() { return &___cultureInfos_2; }
	inline void set_cultureInfos_2(List_1_t3532169525 * value)
	{
		___cultureInfos_2 = value;
		Il2CppCodeGenWriteBarrier((&___cultureInfos_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFOCOLLECTION_T1809038765_H
#ifndef U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#define U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1
struct  U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::languageCode
	String_t* ___languageCode_0;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISCULTURESUPPORTEDU3EC__ANONSTOREY1_T4172074798_H
#ifndef U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#define U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2
struct  U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::englishName
	String_t* ___englishName_0;

public:
	inline static int32_t get_offset_of_englishName_0() { return static_cast<int32_t>(offsetof(U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582, ___englishName_0)); }
	inline String_t* get_englishName_0() const { return ___englishName_0; }
	inline String_t** get_address_of_englishName_0() { return &___englishName_0; }
	inline void set_englishName_0(String_t* value)
	{
		___englishName_0 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISLANGUAGESUPPORTEDENGLISHNAMEU3EC__ANONSTOREY2_T4081837582_H
#ifndef U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#define U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3
struct  U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::languageCode
	String_t* ___languageCode_0;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCULTUREINFOU3EC__ANONSTOREY3_T3382773473_H
#ifndef U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#define U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0
struct  U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546  : public RuntimeObject
{
public:
	// System.String SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::englishName
	String_t* ___englishName_0;

public:
	inline static int32_t get_offset_of_englishName_0() { return static_cast<int32_t>(offsetof(U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546, ___englishName_0)); }
	inline String_t* get_englishName_0() const { return ___englishName_0; }
	inline String_t** get_address_of_englishName_0() { return &___englishName_0; }
	inline void set_englishName_0(String_t* value)
	{
		___englishName_0 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDEVICECULTUREIFSUPPORTEDU3EC__ANONSTOREY0_T3664339546_H
#ifndef LIST_1_T3532169525_H
#define LIST_1_T3532169525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct  List_1_t3532169525  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SmartCultureInfoU5BU5D_t204387046* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3532169525, ____items_1)); }
	inline SmartCultureInfoU5BU5D_t204387046* get__items_1() const { return ____items_1; }
	inline SmartCultureInfoU5BU5D_t204387046** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SmartCultureInfoU5BU5D_t204387046* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3532169525, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3532169525, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3532169525_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	SmartCultureInfoU5BU5D_t204387046* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3532169525_StaticFields, ___EmptyArray_4)); }
	inline SmartCultureInfoU5BU5D_t204387046* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline SmartCultureInfoU5BU5D_t204387046** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(SmartCultureInfoU5BU5D_t204387046* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3532169525_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_0)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_1)); }
	inline XmlReaderSettings_t2186285234 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t2186285234 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef TEXTREADER_T283511965_H
#define TEXTREADER_T283511965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t283511965  : public RuntimeObject
{
public:

public:
};

struct TextReader_t283511965_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t283511965 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t283511965_StaticFields, ___Null_0)); }
	inline TextReader_t283511965 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t283511965 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t283511965 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T283511965_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef LANGUAGEDATAHANDLER_T3889722316_H
#define LANGUAGEDATAHANDLER_T3889722316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageDataHandler
struct  LanguageDataHandler_t3889722316  : public RuntimeObject
{
public:
	// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::loadedValuesDictionary
	SortedDictionary_2_t2617245831 * ___loadedValuesDictionary_0;
	// System.Boolean SmartLocalization.LanguageDataHandler::verboseLogging
	bool ___verboseLogging_1;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::loadedCultureInfo
	SmartCultureInfo_t2060094783 * ___loadedCultureInfo_2;
	// SmartLocalization.ILocalizedAssetLoader SmartLocalization.LanguageDataHandler::assetLoader
	RuntimeObject* ___assetLoader_3;

public:
	inline static int32_t get_offset_of_loadedValuesDictionary_0() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___loadedValuesDictionary_0)); }
	inline SortedDictionary_2_t2617245831 * get_loadedValuesDictionary_0() const { return ___loadedValuesDictionary_0; }
	inline SortedDictionary_2_t2617245831 ** get_address_of_loadedValuesDictionary_0() { return &___loadedValuesDictionary_0; }
	inline void set_loadedValuesDictionary_0(SortedDictionary_2_t2617245831 * value)
	{
		___loadedValuesDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadedValuesDictionary_0), value);
	}

	inline static int32_t get_offset_of_verboseLogging_1() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___verboseLogging_1)); }
	inline bool get_verboseLogging_1() const { return ___verboseLogging_1; }
	inline bool* get_address_of_verboseLogging_1() { return &___verboseLogging_1; }
	inline void set_verboseLogging_1(bool value)
	{
		___verboseLogging_1 = value;
	}

	inline static int32_t get_offset_of_loadedCultureInfo_2() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___loadedCultureInfo_2)); }
	inline SmartCultureInfo_t2060094783 * get_loadedCultureInfo_2() const { return ___loadedCultureInfo_2; }
	inline SmartCultureInfo_t2060094783 ** get_address_of_loadedCultureInfo_2() { return &___loadedCultureInfo_2; }
	inline void set_loadedCultureInfo_2(SmartCultureInfo_t2060094783 * value)
	{
		___loadedCultureInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___loadedCultureInfo_2), value);
	}

	inline static int32_t get_offset_of_assetLoader_3() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3889722316, ___assetLoader_3)); }
	inline RuntimeObject* get_assetLoader_3() const { return ___assetLoader_3; }
	inline RuntimeObject** get_address_of_assetLoader_3() { return &___assetLoader_3; }
	inline void set_assetLoader_3(RuntimeObject* value)
	{
		___assetLoader_3 = value;
		Il2CppCodeGenWriteBarrier((&___assetLoader_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATAHANDLER_T3889722316_H
#ifndef SORTEDDICTIONARY_2_T2617245831_H
#define SORTEDDICTIONARY_2_T2617245831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct  SortedDictionary_2_t2617245831  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t4095273678 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t332667217 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t2617245831, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t2617245831, ___hlp_1)); }
	inline NodeHelper_t332667217 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t332667217 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t332667217 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T2617245831_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef SMARTCULTUREINFO_T2060094783_H
#define SMARTCULTUREINFO_T2060094783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.SmartCultureInfo
struct  SmartCultureInfo_t2060094783  : public RuntimeObject
{
public:
	// System.String SmartLocalization.SmartCultureInfo::languageCode
	String_t* ___languageCode_0;
	// System.String SmartLocalization.SmartCultureInfo::englishName
	String_t* ___englishName_1;
	// System.String SmartLocalization.SmartCultureInfo::nativeName
	String_t* ___nativeName_2;
	// System.Boolean SmartLocalization.SmartCultureInfo::isRightToLeft
	bool ___isRightToLeft_3;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_0), value);
	}

	inline static int32_t get_offset_of_englishName_1() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___englishName_1)); }
	inline String_t* get_englishName_1() const { return ___englishName_1; }
	inline String_t** get_address_of_englishName_1() { return &___englishName_1; }
	inline void set_englishName_1(String_t* value)
	{
		___englishName_1 = value;
		Il2CppCodeGenWriteBarrier((&___englishName_1), value);
	}

	inline static int32_t get_offset_of_nativeName_2() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___nativeName_2)); }
	inline String_t* get_nativeName_2() const { return ___nativeName_2; }
	inline String_t** get_address_of_nativeName_2() { return &___nativeName_2; }
	inline void set_nativeName_2(String_t* value)
	{
		___nativeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___nativeName_2), value);
	}

	inline static int32_t get_offset_of_isRightToLeft_3() { return static_cast<int32_t>(offsetof(SmartCultureInfo_t2060094783, ___isRightToLeft_3)); }
	inline bool get_isRightToLeft_3() const { return ___isRightToLeft_3; }
	inline bool* get_address_of_isRightToLeft_3() { return &___isRightToLeft_3; }
	inline void set_isRightToLeft_3(bool value)
	{
		___isRightToLeft_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTCULTUREINFO_T2060094783_H
#ifndef APPLICATIONEXTENSIONS_T792742538_H
#define APPLICATIONEXTENSIONS_T792742538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.ApplicationExtensions
struct  ApplicationExtensions_t792742538  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXTENSIONS_T792742538_H
#ifndef RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#define RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.RuntimeLocalizedAssetLoader
struct  RuntimeLocalizedAssetLoader_t316402138  : public RuntimeObject
{
public:

public:
};

struct RuntimeLocalizedAssetLoader_t316402138_StaticFields
{
public:
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::GameObjectType
	Type_t * ___GameObjectType_0;
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::AudioClipType
	Type_t * ___AudioClipType_1;
	// System.Type SmartLocalization.RuntimeLocalizedAssetLoader::TextureType
	Type_t * ___TextureType_2;

public:
	inline static int32_t get_offset_of_GameObjectType_0() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___GameObjectType_0)); }
	inline Type_t * get_GameObjectType_0() const { return ___GameObjectType_0; }
	inline Type_t ** get_address_of_GameObjectType_0() { return &___GameObjectType_0; }
	inline void set_GameObjectType_0(Type_t * value)
	{
		___GameObjectType_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectType_0), value);
	}

	inline static int32_t get_offset_of_AudioClipType_1() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___AudioClipType_1)); }
	inline Type_t * get_AudioClipType_1() const { return ___AudioClipType_1; }
	inline Type_t ** get_address_of_AudioClipType_1() { return &___AudioClipType_1; }
	inline void set_AudioClipType_1(Type_t * value)
	{
		___AudioClipType_1 = value;
		Il2CppCodeGenWriteBarrier((&___AudioClipType_1), value);
	}

	inline static int32_t get_offset_of_TextureType_2() { return static_cast<int32_t>(offsetof(RuntimeLocalizedAssetLoader_t316402138_StaticFields, ___TextureType_2)); }
	inline Type_t * get_TextureType_2() const { return ___TextureType_2; }
	inline Type_t ** get_address_of_TextureType_2() { return &___TextureType_2; }
	inline void set_TextureType_2(Type_t * value)
	{
		___TextureType_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextureType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMELOCALIZEDASSETLOADER_T316402138_H
#ifndef KEYVALUEPAIR_2_T3592397703_H
#define KEYVALUEPAIR_2_T3592397703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,SmartLocalization.LocalizedObject>
struct  KeyValuePair_2_t3592397703 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	LocalizedObject_t1409469237 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3592397703, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3592397703, ___value_1)); }
	inline LocalizedObject_t1409469237 * get_value_1() const { return ___value_1; }
	inline LocalizedObject_t1409469237 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(LocalizedObject_t1409469237 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3592397703_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NODEENUMERATOR_T2183475207_H
#define NODEENUMERATOR_T2183475207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.RBTree/NodeEnumerator
struct  NodeEnumerator_t2183475207 
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.RBTree/NodeEnumerator::tree
	RBTree_t4095273678 * ___tree_0;
	// System.UInt32 System.Collections.Generic.RBTree/NodeEnumerator::version
	uint32_t ___version_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node> System.Collections.Generic.RBTree/NodeEnumerator::pennants
	Stack_1_t2901053741 * ___pennants_2;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___tree_0)); }
	inline RBTree_t4095273678 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t4095273678 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t4095273678 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___version_1)); }
	inline uint32_t get_version_1() const { return ___version_1; }
	inline uint32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_pennants_2() { return static_cast<int32_t>(offsetof(NodeEnumerator_t2183475207, ___pennants_2)); }
	inline Stack_1_t2901053741 * get_pennants_2() const { return ___pennants_2; }
	inline Stack_1_t2901053741 ** get_address_of_pennants_2() { return &___pennants_2; }
	inline void set_pennants_2(Stack_1_t2901053741 * value)
	{
		___pennants_2 = value;
		Il2CppCodeGenWriteBarrier((&___pennants_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t2183475207_marshaled_pinvoke
{
	RBTree_t4095273678 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t2901053741 * ___pennants_2;
};
// Native definition for COM marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t2183475207_marshaled_com
{
	RBTree_t4095273678 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t2901053741 * ___pennants_2;
};
#endif // NODEENUMERATOR_T2183475207_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef STRINGREADER_T3465604688_H
#define STRINGREADER_T3465604688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StringReader
struct  StringReader_t3465604688  : public TextReader_t283511965
{
public:
	// System.String System.IO.StringReader::source
	String_t* ___source_1;
	// System.Int32 System.IO.StringReader::nextChar
	int32_t ___nextChar_2;
	// System.Int32 System.IO.StringReader::sourceLength
	int32_t ___sourceLength_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(StringReader_t3465604688, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_nextChar_2() { return static_cast<int32_t>(offsetof(StringReader_t3465604688, ___nextChar_2)); }
	inline int32_t get_nextChar_2() const { return ___nextChar_2; }
	inline int32_t* get_address_of_nextChar_2() { return &___nextChar_2; }
	inline void set_nextChar_2(int32_t value)
	{
		___nextChar_2 = value;
	}

	inline static int32_t get_offset_of_sourceLength_3() { return static_cast<int32_t>(offsetof(StringReader_t3465604688, ___sourceLength_3)); }
	inline int32_t get_sourceLength_3() const { return ___sourceLength_3; }
	inline int32_t* get_address_of_sourceLength_3() { return &___sourceLength_3; }
	inline void set_sourceLength_3(int32_t value)
	{
		___sourceLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREADER_T3465604688_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef LOCALIZEDOBJECTTYPE_T858129287_H
#define LOCALIZEDOBJECTTYPE_T858129287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedObjectType
struct  LocalizedObjectType_t858129287 
{
public:
	// System.Int32 SmartLocalization.LocalizedObjectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LocalizedObjectType_t858129287, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDOBJECTTYPE_T858129287_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef XMLNODETYPE_T1672767151_H
#define XMLNODETYPE_T1672767151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t1672767151 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t1672767151, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T1672767151_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SYSTEMLANGUAGE_T949212163_H
#define SYSTEMLANGUAGE_T949212163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_t949212163 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemLanguage_t949212163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_T949212163_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef ENUMERATOR_T3055753277_H
#define ENUMERATOR_T3055753277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t3055753277 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t2183475207  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t2530217319  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t3055753277, ___host_0)); }
	inline NodeEnumerator_t2183475207  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t2183475207 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t2183475207  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t3055753277, ___current_1)); }
	inline KeyValuePair_2_t2530217319  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t2530217319  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3055753277_H
#ifndef ENUMERATOR_T4117933661_H
#define ENUMERATOR_T4117933661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>
struct  Enumerator_t4117933661 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t2183475207  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t3592397703  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t4117933661, ___host_0)); }
	inline NodeEnumerator_t2183475207  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t2183475207 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t2183475207  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t4117933661, ___current_1)); }
	inline KeyValuePair_2_t3592397703  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t3592397703 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t3592397703  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4117933661_H
#ifndef LOCALIZEDOBJECT_T1409469237_H
#define LOCALIZEDOBJECT_T1409469237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedObject
struct  LocalizedObject_t1409469237  : public RuntimeObject
{
public:
	// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::objectType
	int32_t ___objectType_2;
	// System.String SmartLocalization.LocalizedObject::textValue
	String_t* ___textValue_3;
	// UnityEngine.GameObject SmartLocalization.LocalizedObject::thisGameObject
	GameObject_t1113636619 * ___thisGameObject_4;
	// UnityEngine.AudioClip SmartLocalization.LocalizedObject::thisAudioClip
	AudioClip_t3680889665 * ___thisAudioClip_5;
	// UnityEngine.Texture SmartLocalization.LocalizedObject::thisTexture
	Texture_t3661962703 * ___thisTexture_6;
	// UnityEngine.TextAsset SmartLocalization.LocalizedObject::thisTextAsset
	TextAsset_t3022178571 * ___thisTextAsset_7;
	// System.Boolean SmartLocalization.LocalizedObject::overrideLocalizedObject
	bool ___overrideLocalizedObject_8;
	// System.String SmartLocalization.LocalizedObject::overrideObjectLanguageCode
	String_t* ___overrideObjectLanguageCode_9;

public:
	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___objectType_2)); }
	inline int32_t get_objectType_2() const { return ___objectType_2; }
	inline int32_t* get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(int32_t value)
	{
		___objectType_2 = value;
	}

	inline static int32_t get_offset_of_textValue_3() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___textValue_3)); }
	inline String_t* get_textValue_3() const { return ___textValue_3; }
	inline String_t** get_address_of_textValue_3() { return &___textValue_3; }
	inline void set_textValue_3(String_t* value)
	{
		___textValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_3), value);
	}

	inline static int32_t get_offset_of_thisGameObject_4() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisGameObject_4)); }
	inline GameObject_t1113636619 * get_thisGameObject_4() const { return ___thisGameObject_4; }
	inline GameObject_t1113636619 ** get_address_of_thisGameObject_4() { return &___thisGameObject_4; }
	inline void set_thisGameObject_4(GameObject_t1113636619 * value)
	{
		___thisGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisGameObject_4), value);
	}

	inline static int32_t get_offset_of_thisAudioClip_5() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisAudioClip_5)); }
	inline AudioClip_t3680889665 * get_thisAudioClip_5() const { return ___thisAudioClip_5; }
	inline AudioClip_t3680889665 ** get_address_of_thisAudioClip_5() { return &___thisAudioClip_5; }
	inline void set_thisAudioClip_5(AudioClip_t3680889665 * value)
	{
		___thisAudioClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___thisAudioClip_5), value);
	}

	inline static int32_t get_offset_of_thisTexture_6() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisTexture_6)); }
	inline Texture_t3661962703 * get_thisTexture_6() const { return ___thisTexture_6; }
	inline Texture_t3661962703 ** get_address_of_thisTexture_6() { return &___thisTexture_6; }
	inline void set_thisTexture_6(Texture_t3661962703 * value)
	{
		___thisTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___thisTexture_6), value);
	}

	inline static int32_t get_offset_of_thisTextAsset_7() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___thisTextAsset_7)); }
	inline TextAsset_t3022178571 * get_thisTextAsset_7() const { return ___thisTextAsset_7; }
	inline TextAsset_t3022178571 ** get_address_of_thisTextAsset_7() { return &___thisTextAsset_7; }
	inline void set_thisTextAsset_7(TextAsset_t3022178571 * value)
	{
		___thisTextAsset_7 = value;
		Il2CppCodeGenWriteBarrier((&___thisTextAsset_7), value);
	}

	inline static int32_t get_offset_of_overrideLocalizedObject_8() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___overrideLocalizedObject_8)); }
	inline bool get_overrideLocalizedObject_8() const { return ___overrideLocalizedObject_8; }
	inline bool* get_address_of_overrideLocalizedObject_8() { return &___overrideLocalizedObject_8; }
	inline void set_overrideLocalizedObject_8(bool value)
	{
		___overrideLocalizedObject_8 = value;
	}

	inline static int32_t get_offset_of_overrideObjectLanguageCode_9() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237, ___overrideObjectLanguageCode_9)); }
	inline String_t* get_overrideObjectLanguageCode_9() const { return ___overrideObjectLanguageCode_9; }
	inline String_t** get_address_of_overrideObjectLanguageCode_9() { return &___overrideObjectLanguageCode_9; }
	inline void set_overrideObjectLanguageCode_9(String_t* value)
	{
		___overrideObjectLanguageCode_9 = value;
		Il2CppCodeGenWriteBarrier((&___overrideObjectLanguageCode_9), value);
	}
};

struct LocalizedObject_t1409469237_StaticFields
{
public:
	// System.String SmartLocalization.LocalizedObject::keyTypeIdentifier
	String_t* ___keyTypeIdentifier_0;
	// System.String SmartLocalization.LocalizedObject::endBracket
	String_t* ___endBracket_1;

public:
	inline static int32_t get_offset_of_keyTypeIdentifier_0() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237_StaticFields, ___keyTypeIdentifier_0)); }
	inline String_t* get_keyTypeIdentifier_0() const { return ___keyTypeIdentifier_0; }
	inline String_t** get_address_of_keyTypeIdentifier_0() { return &___keyTypeIdentifier_0; }
	inline void set_keyTypeIdentifier_0(String_t* value)
	{
		___keyTypeIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyTypeIdentifier_0), value);
	}

	inline static int32_t get_offset_of_endBracket_1() { return static_cast<int32_t>(offsetof(LocalizedObject_t1409469237_StaticFields, ___endBracket_1)); }
	inline String_t* get_endBracket_1() const { return ___endBracket_1; }
	inline String_t** get_address_of_endBracket_1() { return &___endBracket_1; }
	inline void set_endBracket_1(String_t* value)
	{
		___endBracket_1 = value;
		Il2CppCodeGenWriteBarrier((&___endBracket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDOBJECT_T1409469237_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TEXTASSET_T3022178571_H
#define TEXTASSET_T3022178571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAsset
struct  TextAsset_t3022178571  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTASSET_T3022178571_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#define CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.ChangeLanguageEventHandler
struct  ChangeLanguageEventHandler_t2032193146  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGELANGUAGEEVENTHANDLER_T2032193146_H
#ifndef PREDICATE_1_T2885388907_H
#define PREDICATE_1_T2885388907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<SmartLocalization.SmartCultureInfo>
struct  Predicate_1_t2885388907  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2885388907_H
#ifndef GUIELEMENT_T3567083079_H
#define GUIELEMENT_T3567083079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t3567083079  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T3567083079_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public Behaviour_t1437897464
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GUITEXT_T402233326_H
#define GUITEXT_T402233326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIText
struct  GUIText_t402233326  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T402233326_H
#ifndef LOCALIZEDAUDIOSOURCE_T267215401_H
#define LOCALIZEDAUDIOSOURCE_T267215401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedAudioSource
struct  LocalizedAudioSource_t267215401  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedAudioSource::localizedKey
	String_t* ___localizedKey_2;
	// UnityEngine.AudioClip SmartLocalization.LocalizedAudioSource::audioClip
	AudioClip_t3680889665 * ___audioClip_3;
	// UnityEngine.AudioSource SmartLocalization.LocalizedAudioSource::audioSource
	AudioSource_t3935305588 * ___audioSource_4;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}

	inline static int32_t get_offset_of_audioClip_3() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___audioClip_3)); }
	inline AudioClip_t3680889665 * get_audioClip_3() const { return ___audioClip_3; }
	inline AudioClip_t3680889665 ** get_address_of_audioClip_3() { return &___audioClip_3; }
	inline void set_audioClip_3(AudioClip_t3680889665 * value)
	{
		___audioClip_3 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_3), value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(LocalizedAudioSource_t267215401, ___audioSource_4)); }
	inline AudioSource_t3935305588 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t3935305588 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDAUDIOSOURCE_T267215401_H
#ifndef LOCALIZEDGUITEXTURE_T2717994319_H
#define LOCALIZEDGUITEXTURE_T2717994319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedGUITexture
struct  LocalizedGUITexture_t2717994319  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedGUITexture::localizedKey
	String_t* ___localizedKey_2;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedGUITexture_t2717994319, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDGUITEXTURE_T2717994319_H
#ifndef GUITEXTURE_T951903601_H
#define GUITEXTURE_T951903601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUITexture
struct  GUITexture_t951903601  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXTURE_T951903601_H
#ifndef LANGUAGEMANAGER_T2767934455_H
#define LANGUAGEMANAGER_T2767934455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager
struct  LanguageManager_t2767934455  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::serializedKeys
	List_1_t3319525431 * ___serializedKeys_6;
	// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject> SmartLocalization.LanguageManager::serializedValues
	List_1_t2881543979 * ___serializedValues_7;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::serializedCulture
	SmartCultureInfo_t2060094783 * ___serializedCulture_8;
	// SmartLocalization.ChangeLanguageEventHandler SmartLocalization.LanguageManager::OnChangeLanguage
	ChangeLanguageEventHandler_t2032193146 * ___OnChangeLanguage_9;
	// System.String SmartLocalization.LanguageManager::defaultLanguage
	String_t* ___defaultLanguage_10;
	// SmartLocalization.SmartCultureInfoCollection SmartLocalization.LanguageManager::availableLanguages
	SmartCultureInfoCollection_t1809038765 * ___availableLanguages_11;
	// SmartLocalization.LanguageDataHandler SmartLocalization.LanguageManager::languageDataHandler
	LanguageDataHandler_t3889722316 * ___languageDataHandler_12;

public:
	inline static int32_t get_offset_of_serializedKeys_6() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedKeys_6)); }
	inline List_1_t3319525431 * get_serializedKeys_6() const { return ___serializedKeys_6; }
	inline List_1_t3319525431 ** get_address_of_serializedKeys_6() { return &___serializedKeys_6; }
	inline void set_serializedKeys_6(List_1_t3319525431 * value)
	{
		___serializedKeys_6 = value;
		Il2CppCodeGenWriteBarrier((&___serializedKeys_6), value);
	}

	inline static int32_t get_offset_of_serializedValues_7() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedValues_7)); }
	inline List_1_t2881543979 * get_serializedValues_7() const { return ___serializedValues_7; }
	inline List_1_t2881543979 ** get_address_of_serializedValues_7() { return &___serializedValues_7; }
	inline void set_serializedValues_7(List_1_t2881543979 * value)
	{
		___serializedValues_7 = value;
		Il2CppCodeGenWriteBarrier((&___serializedValues_7), value);
	}

	inline static int32_t get_offset_of_serializedCulture_8() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___serializedCulture_8)); }
	inline SmartCultureInfo_t2060094783 * get_serializedCulture_8() const { return ___serializedCulture_8; }
	inline SmartCultureInfo_t2060094783 ** get_address_of_serializedCulture_8() { return &___serializedCulture_8; }
	inline void set_serializedCulture_8(SmartCultureInfo_t2060094783 * value)
	{
		___serializedCulture_8 = value;
		Il2CppCodeGenWriteBarrier((&___serializedCulture_8), value);
	}

	inline static int32_t get_offset_of_OnChangeLanguage_9() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___OnChangeLanguage_9)); }
	inline ChangeLanguageEventHandler_t2032193146 * get_OnChangeLanguage_9() const { return ___OnChangeLanguage_9; }
	inline ChangeLanguageEventHandler_t2032193146 ** get_address_of_OnChangeLanguage_9() { return &___OnChangeLanguage_9; }
	inline void set_OnChangeLanguage_9(ChangeLanguageEventHandler_t2032193146 * value)
	{
		___OnChangeLanguage_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnChangeLanguage_9), value);
	}

	inline static int32_t get_offset_of_defaultLanguage_10() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___defaultLanguage_10)); }
	inline String_t* get_defaultLanguage_10() const { return ___defaultLanguage_10; }
	inline String_t** get_address_of_defaultLanguage_10() { return &___defaultLanguage_10; }
	inline void set_defaultLanguage_10(String_t* value)
	{
		___defaultLanguage_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultLanguage_10), value);
	}

	inline static int32_t get_offset_of_availableLanguages_11() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___availableLanguages_11)); }
	inline SmartCultureInfoCollection_t1809038765 * get_availableLanguages_11() const { return ___availableLanguages_11; }
	inline SmartCultureInfoCollection_t1809038765 ** get_address_of_availableLanguages_11() { return &___availableLanguages_11; }
	inline void set_availableLanguages_11(SmartCultureInfoCollection_t1809038765 * value)
	{
		___availableLanguages_11 = value;
		Il2CppCodeGenWriteBarrier((&___availableLanguages_11), value);
	}

	inline static int32_t get_offset_of_languageDataHandler_12() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455, ___languageDataHandler_12)); }
	inline LanguageDataHandler_t3889722316 * get_languageDataHandler_12() const { return ___languageDataHandler_12; }
	inline LanguageDataHandler_t3889722316 ** get_address_of_languageDataHandler_12() { return &___languageDataHandler_12; }
	inline void set_languageDataHandler_12(LanguageDataHandler_t3889722316 * value)
	{
		___languageDataHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___languageDataHandler_12), value);
	}
};

struct LanguageManager_t2767934455_StaticFields
{
public:
	// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::instance
	LanguageManager_t2767934455 * ___instance_2;
	// System.Boolean SmartLocalization.LanguageManager::IsQuitting
	bool ___IsQuitting_3;
	// System.Boolean SmartLocalization.LanguageManager::DontDestroyOnLoadToggle
	bool ___DontDestroyOnLoadToggle_4;
	// System.Boolean SmartLocalization.LanguageManager::DidSetDontDestroyOnLoad
	bool ___DidSetDontDestroyOnLoad_5;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___instance_2)); }
	inline LanguageManager_t2767934455 * get_instance_2() const { return ___instance_2; }
	inline LanguageManager_t2767934455 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LanguageManager_t2767934455 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_IsQuitting_3() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___IsQuitting_3)); }
	inline bool get_IsQuitting_3() const { return ___IsQuitting_3; }
	inline bool* get_address_of_IsQuitting_3() { return &___IsQuitting_3; }
	inline void set_IsQuitting_3(bool value)
	{
		___IsQuitting_3 = value;
	}

	inline static int32_t get_offset_of_DontDestroyOnLoadToggle_4() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___DontDestroyOnLoadToggle_4)); }
	inline bool get_DontDestroyOnLoadToggle_4() const { return ___DontDestroyOnLoadToggle_4; }
	inline bool* get_address_of_DontDestroyOnLoadToggle_4() { return &___DontDestroyOnLoadToggle_4; }
	inline void set_DontDestroyOnLoadToggle_4(bool value)
	{
		___DontDestroyOnLoadToggle_4 = value;
	}

	inline static int32_t get_offset_of_DidSetDontDestroyOnLoad_5() { return static_cast<int32_t>(offsetof(LanguageManager_t2767934455_StaticFields, ___DidSetDontDestroyOnLoad_5)); }
	inline bool get_DidSetDontDestroyOnLoad_5() const { return ___DidSetDontDestroyOnLoad_5; }
	inline bool* get_address_of_DidSetDontDestroyOnLoad_5() { return &___DidSetDontDestroyOnLoad_5; }
	inline void set_DidSetDontDestroyOnLoad_5(bool value)
	{
		___DidSetDontDestroyOnLoad_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEMANAGER_T2767934455_H
#ifndef LOCALIZEDGUITEXT_T2415330885_H
#define LOCALIZEDGUITEXT_T2415330885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedGUIText
struct  LocalizedGUIText_t2415330885  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.LocalizedGUIText::localizedKey
	String_t* ___localizedKey_2;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedGUIText_t2415330885, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDGUITEXT_T2415330885_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m194478212_gshared (SortedDictionary_2_t1555065447 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m174096224_gshared (SortedDictionary_2_t1555065447 * __this, const RuntimeMethod* method);
// System.Collections.Generic.SortedDictionary`2/Enumerator<!0,!1> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3055753277  SortedDictionary_2_GetEnumerator_m1049615467_gshared (SortedDictionary_2_t1555065447 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2530217319  Enumerator_get_Current_m1646926595_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1328507389_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool SortedDictionary_2_ContainsKey_m1824333297_gshared (SortedDictionary_2_t1555065447 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void SortedDictionary_2_set_Item_m3616019246_gshared (SortedDictionary_2_t1555065447 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void SortedDictionary_2_Add_m2722215429_gshared (SortedDictionary_2_t1555065447 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2228859771_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2851724990_gshared (Enumerator_t3055753277 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.SortedDictionary`2/KeyCollection<!0,!1> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  KeyCollection_t878650876 * SortedDictionary_2_get_Keys_m4038202490_gshared (SortedDictionary_2_t1555065447 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1__ctor_m1328752868_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool SortedDictionary_2_TryGetValue_m1285207460_gshared (SortedDictionary_2_t1555065447 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m220242405_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3804808189_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m327447107_gshared (Predicate_1_t3905400288 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<!0>)
extern "C"  RuntimeObject * List_1_Find_m2048854920_gshared (List_1_t257213610 * __this, Predicate_1_t3905400288 * p0, const RuntimeMethod* method);
// T SmartLocalization.LanguageDataHandler::GetAsset<System.Object>(System.String)
extern "C"  RuntimeObject * LanguageDataHandler_GetAsset_TisRuntimeObject_m3985613790_gshared (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);

// UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
extern "C"  int32_t Application_get_systemLanguage_m3110370732 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.ApplicationExtensions::GetStringValueOfSystemLanguage(UnityEngine.SystemLanguage)
extern "C"  String_t* ApplicationExtensions_GetStringValueOfSystemLanguage_m570700328 (RuntimeObject * __this /* static, unused */, int32_t ___systemLanguage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.ChangeLanguageEventHandler::Invoke(SmartLocalization.LanguageManager)
extern "C"  void ChangeLanguageEventHandler_Invoke_m109768076 (ChangeLanguageEventHandler_t2032193146 * __this, LanguageManager_t2767934455 * ___thisLanguageManager0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::.ctor()
#define SortedDictionary_2__ctor_m964777687(__this, method) ((  void (*) (SortedDictionary_2_t2617245831 *, const RuntimeMethod*))SortedDictionary_2__ctor_m194478212_gshared)(__this, method)
// System.Void SmartLocalization.RuntimeLocalizedAssetLoader::.ctor()
extern "C"  void RuntimeLocalizedAssetLoader__ctor_m3777454129 (RuntimeLocalizedAssetLoader_t316402138 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::LoadLanguageDictionary(System.String)
extern "C"  SortedDictionary_2_t2617245831 * LanguageDataHandler_LoadLanguageDictionary_m2285733254 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::get_Count()
#define SortedDictionary_2_get_Count_m634161304(__this, method) ((  int32_t (*) (SortedDictionary_2_t2617245831 *, const RuntimeMethod*))SortedDictionary_2_get_Count_m174096224_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2/Enumerator<!0,!1> System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::GetEnumerator()
#define SortedDictionary_2_GetEnumerator_m3922890767(__this, method) ((  Enumerator_t4117933661  (*) (SortedDictionary_2_t2617245831 *, const RuntimeMethod*))SortedDictionary_2_GetEnumerator_m1049615467_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::get_Current()
#define Enumerator_get_Current_m3473425924(__this, method) ((  KeyValuePair_2_t3592397703  (*) (Enumerator_t4117933661 *, const RuntimeMethod*))Enumerator_get_Current_m1646926595_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,SmartLocalization.LocalizedObject>::get_Key()
#define KeyValuePair_2_get_Key_m2085155650(__this, method) ((  String_t* (*) (KeyValuePair_2_t3592397703 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1328507389_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::ContainsKey(!0)
#define SortedDictionary_2_ContainsKey_m1995604387(__this, p0, method) ((  bool (*) (SortedDictionary_2_t2617245831 *, String_t*, const RuntimeMethod*))SortedDictionary_2_ContainsKey_m1824333297_gshared)(__this, p0, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,SmartLocalization.LocalizedObject>::get_Value()
#define KeyValuePair_2_get_Value_m52858533(__this, method) ((  LocalizedObject_t1409469237 * (*) (KeyValuePair_2_t3592397703 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::set_Item(!0,!1)
#define SortedDictionary_2_set_Item_m3916906723(__this, p0, p1, method) ((  void (*) (SortedDictionary_2_t2617245831 *, String_t*, LocalizedObject_t1409469237 *, const RuntimeMethod*))SortedDictionary_2_set_Item_m3616019246_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::Add(!0,!1)
#define SortedDictionary_2_Add_m1412840442(__this, p0, p1, method) ((  void (*) (SortedDictionary_2_t2617245831 *, String_t*, LocalizedObject_t1409469237 *, const RuntimeMethod*))SortedDictionary_2_Add_m2722215429_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::MoveNext()
#define Enumerator_MoveNext_m1285001833(__this, method) ((  bool (*) (Enumerator_t4117933661 *, const RuntimeMethod*))Enumerator_MoveNext_m2228859771_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::Dispose()
#define Enumerator_Dispose_m235344939(__this, method) ((  void (*) (Enumerator_t4117933661 *, const RuntimeMethod*))Enumerator_Dispose_m2851724990_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m706204246(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1759067526 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m1685793073(__this, p0, method) ((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<!0,!1> System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::get_Keys()
#define SortedDictionary_2_get_Keys_m3652780046(__this, method) ((  KeyCollection_t1940831260 * (*) (SortedDictionary_2_t2617245831 *, const RuntimeMethod*))SortedDictionary_2_get_Keys_m4038202490_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m928983596(__this, p0, method) ((  void (*) (List_1_t3319525431 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>::TryGetValue(!0,!1&)
#define SortedDictionary_2_TryGetValue_m3861572157(__this, p0, p1, method) ((  bool (*) (SortedDictionary_2_t2617245831 *, String_t*, LocalizedObject_t1409469237 **, const RuntimeMethod*))SortedDictionary_2_TryGetValue_m1285207460_gshared)(__this, p0, p1, method)
// SmartLocalization.LocalizedObject SmartLocalization.LanguageDataHandler::GetLocalizedObject(System.String)
extern "C"  LocalizedObject_t1409469237 * LanguageDataHandler_GetLocalizedObject_m3611374437 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::get_TextValue()
extern "C"  String_t* LocalizedObject_get_TextValue_m2788713862 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::get_VerboseLogging()
extern "C"  bool LanguageDataHandler_get_VerboseLogging_m516950596 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageParser::LoadLanguage(System.String)
extern "C"  SortedDictionary_2_t2617245831 * LanguageParser_LoadLanguage_m379871964 (RuntimeObject * __this /* static, unused */, String_t* ___languageDataInResX0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LocalizedObject::get_OverrideLocalizedObject()
extern "C"  bool LocalizedObject_get_OverrideLocalizedObject_m2345827120 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::get_OverrideObjectLanguageCode()
extern "C"  String_t* LocalizedObject_get_OverrideObjectLanguageCode_m2176233279 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::.ctor()
extern "C"  void LanguageDataHandler__ctor_m1941805053 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m2150679437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<SmartLocalization.LanguageManager>()
#define Object_FindObjectOfType_TisLanguageManager_t2767934455_m1920039193(__this /* static, unused */, method) ((  LanguageManager_t2767934455 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m220242405_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m3707688467 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<SmartLocalization.LanguageManager>()
#define GameObject_AddComponent_TisLanguageManager_t2767934455_m4185499804(__this, method) ((  LanguageManager_t2767934455 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3804808189_gshared)(__this, method)
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedValuesDictionary(System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageDataHandler_set_LoadedValuesDictionary_m2801856716 (LanguageDataHandler_t3889722316 * __this, SortedDictionary_2_t2617245831 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::get_LoadedValuesDictionary()
extern "C"  SortedDictionary_2_t2617245831 * LanguageDataHandler_get_LoadedValuesDictionary_m2388809926 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m3729392029(__this, p0, method) ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>::get_Item(System.Int32)
#define List_1_get_Item_m3162138650(__this, p0, method) ((  LocalizedObject_t1409469237 * (*) (List_1_t2881543979 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m2276455407(__this, method) ((  int32_t (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedCulture(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageDataHandler_set_LoadedCulture_m3772607010 (LanguageDataHandler_t3889722316 * __this, SmartCultureInfo_t2060094783 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m3111619026(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>::Clear()
#define List_1_Clear_m1509931054(__this, method) ((  void (*) (List_1_t2881543979 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>::.ctor()
#define List_1__ctor_m981406146(__this, method) ((  void (*) (List_1_t2881543979 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>::Add(!0)
#define List_1_Add_m2873794602(__this, p0, method) ((  void (*) (List_1_t2881543979 *, LocalizedObject_t1409469237 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::get_CurrentlyLoadedCulture()
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_get_CurrentlyLoadedCulture_m1071669230 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m3302800229(__this, method) ((  void (*) (Dictionary_2_t1632706988 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m3665229156(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>::get_Count()
#define List_1_get_Count_m2960725483(__this, method) ((  int32_t (*) (List_1_t3532169525 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::get_LoadedCulture()
extern "C"  SmartCultureInfo_t2060094783 * LanguageDataHandler_get_LoadedCulture_m384579762 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::set_VerboseLogging(System.Boolean)
extern "C"  void LanguageDataHandler_set_VerboseLogging_m2066869259 (LanguageDataHandler_t3889722316 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::get_VerboseLogging()
extern "C"  bool LanguageManager_get_VerboseLogging_m1685643384 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::LoadAvailableCultures()
extern "C"  bool LanguageManager_LoadAvailableCultures_m3449409331 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Predicate`1<SmartLocalization.SmartCultureInfo>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2314931459(__this, p0, p1, method) ((  void (*) (Predicate_1_t2885388907 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method)
// !0 System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>::Find(System.Predicate`1<!0>)
#define List_1_Find_m4070095342(__this, p0, method) ((  SmartCultureInfo_t2060094783 * (*) (List_1_t3532169525 *, Predicate_1_t2885388907 *, const RuntimeMethod*))List_1_Find_m2048854920_gshared)(__this, p0, method)
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageManager_ChangeLanguage_m1970109620 (LanguageManager_t2767934455 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>::get_Item(System.Int32)
#define List_1_get_Item_m1520579522(__this, p0, method) ((  SmartCultureInfo_t2060094783 * (*) (List_1_t3532169525 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFilePath()
extern "C"  String_t* LanguageRuntimeData_AvailableCulturesFilePath_m3409607084 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t631007953 * Resources_Load_m3880010804 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfoCollection SmartLocalization.SmartCultureInfoCollection::Deserialize(UnityEngine.TextAsset)
extern "C"  SmartCultureInfoCollection_t1809038765 * SmartCultureInfoCollection_Deserialize_m2953505266 (RuntimeObject * __this /* static, unused */, TextAsset_t3022178571 * ___xmlFile0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetAllKeys()
extern "C"  List_1_t3319525431 * LanguageDataHandler_GetAllKeys_m1125042489 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(System.String)
extern "C"  void LanguageManager_ChangeLanguage_m2374647000 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::LanguageFilePath(System.String)
extern "C"  String_t* LanguageRuntimeData_LanguageFilePath_m1120529596 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m2027878391 (TextAsset_t3022178571 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::LoadLanguage(System.String,System.String)
extern "C"  bool LanguageManager_LoadLanguage_m577556272 (LanguageManager_t2767934455 * __this, String_t* ___languageData0, String_t* ___languageCode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::Append(System.String)
extern "C"  bool LanguageDataHandler_Append_m248544474 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetCultureInfo(System.String)
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_GetCultureInfo_m1993048463 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::Load(System.String)
extern "C"  bool LanguageDataHandler_Load_m1742461671 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsCultureSupported(System.String)
extern "C"  bool LanguageManager_IsCultureSupported_m294811961 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupported(System.String)
extern "C"  bool LanguageManager_IsLanguageSupported_m2070434101 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetDeviceCultureIfSupported()
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_GetDeviceCultureIfSupported_m2865750724 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::.ctor()
extern "C"  void U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0__ctor_m958998401 (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageManager::GetSystemLanguageEnglishName()
extern "C"  String_t* LanguageManager_GetSystemLanguageEnglishName_m3716429587 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::.ctor()
extern "C"  void U3CIsCultureSupportedU3Ec__AnonStorey1__ctor_m1195679156 (U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::.ctor()
extern "C"  void U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2__ctor_m119487228 (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::.ctor()
extern "C"  void U3CGetCultureInfoU3Ec__AnonStorey3__ctor_m3147796296 (U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.ApplicationExtensions::GetSystemLanguage()
extern "C"  String_t* ApplicationExtensions_GetSystemLanguage_m1620959893 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetKeysWithinCategory(System.String)
extern "C"  List_1_t3319525431 * LanguageDataHandler_GetKeysWithinCategory_m2182585259 (LanguageDataHandler_t3889722316 * __this, String_t* ___category0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageDataHandler::GetTextValue(System.String)
extern "C"  String_t* LanguageDataHandler_GetTextValue_m495737691 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T SmartLocalization.LanguageDataHandler::GetAsset<UnityEngine.AudioClip>(System.String)
#define LanguageDataHandler_GetAsset_TisAudioClip_t3680889665_m950255953(__this, ___key0, method) ((  AudioClip_t3680889665 * (*) (LanguageDataHandler_t3889722316 *, String_t*, const RuntimeMethod*))LanguageDataHandler_GetAsset_TisRuntimeObject_m3985613790_gshared)(__this, ___key0, method)
// T SmartLocalization.LanguageDataHandler::GetAsset<UnityEngine.GameObject>(System.String)
#define LanguageDataHandler_GetAsset_TisGameObject_t1113636619_m521626920(__this, ___key0, method) ((  GameObject_t1113636619 * (*) (LanguageDataHandler_t3889722316 *, String_t*, const RuntimeMethod*))LanguageDataHandler_GetAsset_TisRuntimeObject_m3985613790_gshared)(__this, ___key0, method)
// T SmartLocalization.LanguageDataHandler::GetAsset<UnityEngine.Texture>(System.String)
#define LanguageDataHandler_GetAsset_TisTexture_t3661962703_m2393095502(__this, ___key0, method) ((  Texture_t3661962703 * (*) (LanguageDataHandler_t3889722316 *, String_t*, const RuntimeMethod*))LanguageDataHandler_GetAsset_TisRuntimeObject_m3985613790_gshared)(__this, ___key0, method)
// System.Boolean SmartLocalization.LanguageDataHandler::HasKey(System.String)
extern "C"  bool LanguageDataHandler_HasKey_m2190396545 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower()
extern "C"  String_t* String_ToLower_m2029374922 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m1977622757 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StringReader::.ctor(System.String)
extern "C"  void StringReader__ctor_m126993932 (StringReader_t3465604688 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlReader System.Xml.XmlReader::Create(System.IO.TextReader)
extern "C"  XmlReader_t3121518892 * XmlReader_Create_m2125883682 (RuntimeObject * __this /* static, unused */, TextReader_t283511965 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageParser::ReadElements(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadElements_m1808351402 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SortedDictionary_2_t2617245831 * ___loadedLanguageDictionary1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageParser::ReadData(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadData_m1441422953 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SortedDictionary_2_t2617245831 * ___loadedLanguageDictionary1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::.ctor()
extern "C"  void LocalizedObject__ctor_m1798526539 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::GetLocalizedObjectType(System.String)
extern "C"  int32_t LocalizedObject_GetLocalizedObjectType_m2843596427 (RuntimeObject * __this /* static, unused */, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ObjectType(SmartLocalization.LocalizedObjectType)
extern "C"  void LocalizedObject_set_ObjectType_m3384992281 (LocalizedObject_t1409469237 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_TextValue(System.String)
extern "C"  void LocalizedObject_set_TextValue_m13296749 (LocalizedObject_t1409469237 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::get_ObjectType()
extern "C"  int32_t LocalizedObject_get_ObjectType_m1776010124 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_OverrideLocalizedObject(System.Boolean)
extern "C"  void LocalizedObject_set_OverrideLocalizedObject_m3073665204 (LocalizedObject_t1409469237 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_OverrideObjectLanguageCode(System.String)
extern "C"  void LocalizedObject_set_OverrideObjectLanguageCode_m2380448063 (LocalizedObject_t1409469237 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::GetCleanKey(System.String,SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetCleanKey_m2035573102 (RuntimeObject * __this /* static, unused */, String_t* ___key0, int32_t ___objectType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::get_Instance()
extern "C"  LanguageManager_t2767934455 * LanguageManager_get_Instance_m861325331 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.ChangeLanguageEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ChangeLanguageEventHandler__ctor_m417283477 (ChangeLanguageEventHandler_t2032193146 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t3935305588_m286538782(__this, method) ((  AudioSource_t3935305588 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void SmartLocalization.LocalizedAudioSource::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedAudioSource_OnChangeLanguage_m3163652545 (LocalizedAudioSource_t267215401 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::get_HasInstance()
extern "C"  bool LanguageManager_get_HasInstance_m2564662803 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SmartLocalization.LanguageManager::GetAudioClip(System.String)
extern "C"  AudioClip_t3680889665 * LanguageManager_GetAudioClip_m1838735899 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m31653938 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUIText::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUIText_OnChangeLanguage_m236516013 (LocalizedGUIText_t2415330885 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t402233326_m1476680223(__this, method) ((  GUIText_t402233326 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String SmartLocalization.LanguageManager::GetTextValue(System.String)
extern "C"  String_t* LanguageManager_GetTextValue_m2899262939 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m2265981083 (GUIText_t402233326 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUITexture::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUITexture_OnChangeLanguage_m794962917 (LocalizedGUITexture_t2717994319 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
#define Component_GetComponent_TisGUITexture_t951903601_m1070565238(__this, method) ((  GUITexture_t951903601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Texture SmartLocalization.LanguageManager::GetTexture(System.String)
extern "C"  Texture_t3661962703 * LanguageManager_GetTexture_m1160349625 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C"  void GUITexture_set_texture_m1693041041 (GUITexture_t951903601 * __this, Texture_t3661962703 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisAudioClip(UnityEngine.AudioClip)
extern "C"  void LocalizedObject_set_ThisAudioClip_m2196333741 (LocalizedObject_t1409469237 * __this, AudioClip_t3680889665 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisTexture(UnityEngine.Texture)
extern "C"  void LocalizedObject_set_ThisTexture_m1264717189 (LocalizedObject_t1409469237 * __this, Texture_t3661962703 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisGameObject(UnityEngine.GameObject)
extern "C"  void LocalizedObject_set_ThisGameObject_m1674655102 (LocalizedObject_t1409469237 * __this, GameObject_t1113636619 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::GetLocalizedObjectTypeStringValue(SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetLocalizedObjectTypeStringValue_m652933876 (RuntimeObject * __this /* static, unused */, int32_t ___objectType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_PrefabsFolderPath_m1749946394 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::AudioFilesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_AudioFilesFolderPath_m4027902490 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_TexturesFolderPath_m4143263055 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m2664721875 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m630303134 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>::.ctor()
#define List_1__ctor_m3177292912(__this, method) ((  void (*) (List_1_t3532169525 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>::Add(!0)
#define List_1_Add_m3606668418(__this, p0, method) ((  void (*) (List_1_t3532169525 *, SmartCultureInfo_t2060094783 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// SmartLocalization.SmartCultureInfoCollection SmartLocalization.SmartCultureInfoCollectionDeserializer::Deserialize(UnityEngine.TextAsset)
extern "C"  SmartCultureInfoCollection_t1809038765 * SmartCultureInfoCollectionDeserializer_Deserialize_m3273799526 (RuntimeObject * __this /* static, unused */, TextAsset_t3022178571 * ___xmlFile0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.SmartCultureInfoCollection::.ctor()
extern "C"  void SmartCultureInfoCollection__ctor_m2569938504 (SmartCultureInfoCollection_t1809038765 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.SmartCultureInfoCollectionDeserializer::ReadElements(System.Xml.XmlReader,SmartLocalization.SmartCultureInfoCollection)
extern "C"  void SmartCultureInfoCollectionDeserializer_ReadElements_m2123939648 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SmartCultureInfoCollection_t1809038765 * ___newCollection1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.SmartCultureInfoCollectionDeserializer::ReadData(System.Xml.XmlReader,SmartLocalization.SmartCultureInfoCollection)
extern "C"  void SmartCultureInfoCollectionDeserializer_ReadData_m1923819025 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SmartCultureInfoCollection_t1809038765 * ___newCollection1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.SmartCultureInfo::.ctor(System.String,System.String,System.String,System.Boolean)
extern "C"  void SmartCultureInfo__ctor_m2022110236 (SmartCultureInfo_t2060094783 * __this, String_t* ___languageCode0, String_t* ___englishName1, String_t* ___nativeName2, bool ___isRightToLeft3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.SmartCultureInfoCollection::AddCultureInfo(SmartLocalization.SmartCultureInfo)
extern "C"  void SmartCultureInfoCollection_AddCultureInfo_m3890672140 (SmartCultureInfoCollection_t1809038765 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SmartLocalization.ApplicationExtensions::GetSystemLanguage()
extern "C"  String_t* ApplicationExtensions_GetSystemLanguage_m1620959893 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Application_get_systemLanguage_m3110370732(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ApplicationExtensions_GetStringValueOfSystemLanguage_m570700328(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String SmartLocalization.ApplicationExtensions::GetStringValueOfSystemLanguage(UnityEngine.SystemLanguage)
extern "C"  String_t* ApplicationExtensions_GetStringValueOfSystemLanguage_m570700328 (RuntimeObject * __this /* static, unused */, int32_t ___systemLanguage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ApplicationExtensions_GetStringValueOfSystemLanguage_m570700328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___systemLanguage0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_00b7;
			}
			case 1:
			{
				goto IL_00bd;
			}
			case 2:
			{
				goto IL_00c3;
			}
			case 3:
			{
				goto IL_00c9;
			}
			case 4:
			{
				goto IL_00cf;
			}
			case 5:
			{
				goto IL_00d5;
			}
			case 6:
			{
				goto IL_00db;
			}
			case 7:
			{
				goto IL_00e1;
			}
			case 8:
			{
				goto IL_00e7;
			}
			case 9:
			{
				goto IL_00ed;
			}
			case 10:
			{
				goto IL_00f3;
			}
			case 11:
			{
				goto IL_00f9;
			}
			case 12:
			{
				goto IL_00ff;
			}
			case 13:
			{
				goto IL_0105;
			}
			case 14:
			{
				goto IL_010b;
			}
			case 15:
			{
				goto IL_0111;
			}
			case 16:
			{
				goto IL_0117;
			}
			case 17:
			{
				goto IL_011d;
			}
			case 18:
			{
				goto IL_0123;
			}
			case 19:
			{
				goto IL_0129;
			}
			case 20:
			{
				goto IL_012f;
			}
			case 21:
			{
				goto IL_0135;
			}
			case 22:
			{
				goto IL_013b;
			}
			case 23:
			{
				goto IL_0141;
			}
			case 24:
			{
				goto IL_0147;
			}
			case 25:
			{
				goto IL_014d;
			}
			case 26:
			{
				goto IL_0153;
			}
			case 27:
			{
				goto IL_0159;
			}
			case 28:
			{
				goto IL_015f;
			}
			case 29:
			{
				goto IL_0165;
			}
			case 30:
			{
				goto IL_016b;
			}
			case 31:
			{
				goto IL_0171;
			}
			case 32:
			{
				goto IL_0177;
			}
			case 33:
			{
				goto IL_017d;
			}
			case 34:
			{
				goto IL_0183;
			}
			case 35:
			{
				goto IL_0189;
			}
			case 36:
			{
				goto IL_018f;
			}
			case 37:
			{
				goto IL_0195;
			}
			case 38:
			{
				goto IL_019b;
			}
			case 39:
			{
				goto IL_01a7;
			}
			case 40:
			{
				goto IL_01ad;
			}
			case 41:
			{
				goto IL_01ad;
			}
			case 42:
			{
				goto IL_01a1;
			}
		}
	}
	{
		goto IL_01ad;
	}

IL_00b7:
	{
		return _stringLiteral3612373897;
	}

IL_00bd:
	{
		return _stringLiteral3954355219;
	}

IL_00c3:
	{
		return _stringLiteral1179142834;
	}

IL_00c9:
	{
		return _stringLiteral3366331569;
	}

IL_00cf:
	{
		return _stringLiteral1005565038;
	}

IL_00d5:
	{
		return _stringLiteral4067969849;
	}

IL_00db:
	{
		return _stringLiteral2851651844;
	}

IL_00e1:
	{
		return _stringLiteral3952115132;
	}

IL_00e7:
	{
		return _stringLiteral3897511319;
	}

IL_00ed:
	{
		return _stringLiteral486981520;
	}

IL_00f3:
	{
		return _stringLiteral3500168455;
	}

IL_00f9:
	{
		return _stringLiteral884659934;
	}

IL_00ff:
	{
		return _stringLiteral431833368;
	}

IL_0105:
	{
		return _stringLiteral17267921;
	}

IL_010b:
	{
		return _stringLiteral2748939550;
	}

IL_0111:
	{
		return _stringLiteral442750400;
	}

IL_0117:
	{
		return _stringLiteral3552476487;
	}

IL_011d:
	{
		return _stringLiteral1109991769;
	}

IL_0123:
	{
		return _stringLiteral4285556778;
	}

IL_0129:
	{
		return _stringLiteral4280934045;
	}

IL_012f:
	{
		return _stringLiteral2396719944;
	}

IL_0135:
	{
		return _stringLiteral3344213735;
	}

IL_013b:
	{
		return _stringLiteral2392163715;
	}

IL_0141:
	{
		return _stringLiteral3212171020;
	}

IL_0147:
	{
		return _stringLiteral2595179563;
	}

IL_014d:
	{
		return _stringLiteral500347555;
	}

IL_0153:
	{
		return _stringLiteral2136083539;
	}

IL_0159:
	{
		return _stringLiteral2730649301;
	}

IL_015f:
	{
		return _stringLiteral3129577868;
	}

IL_0165:
	{
		return _stringLiteral3901952298;
	}

IL_016b:
	{
		return _stringLiteral3236904726;
	}

IL_0171:
	{
		return _stringLiteral1203471839;
	}

IL_0177:
	{
		return _stringLiteral760857587;
	}

IL_017d:
	{
		return _stringLiteral577400165;
	}

IL_0183:
	{
		return _stringLiteral2458762727;
	}

IL_0189:
	{
		return _stringLiteral2677988819;
	}

IL_018f:
	{
		return _stringLiteral387968785;
	}

IL_0195:
	{
		return _stringLiteral4168042063;
	}

IL_019b:
	{
		return _stringLiteral1708475082;
	}

IL_01a1:
	{
		return _stringLiteral2854237347;
	}

IL_01a7:
	{
		return _stringLiteral933504174;
	}

IL_01ad:
	{
		return _stringLiteral2854237347;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.ChangeLanguageEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ChangeLanguageEventHandler__ctor_m417283477 (ChangeLanguageEventHandler_t2032193146 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SmartLocalization.ChangeLanguageEventHandler::Invoke(SmartLocalization.LanguageManager)
extern "C"  void ChangeLanguageEventHandler_Invoke_m109768076 (ChangeLanguageEventHandler_t2032193146 * __this, LanguageManager_t2767934455 * ___thisLanguageManager0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ChangeLanguageEventHandler_Invoke_m109768076((ChangeLanguageEventHandler_t2032193146 *)__this->get_prev_9(), ___thisLanguageManager0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, LanguageManager_t2767934455 *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, ___thisLanguageManager0, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, LanguageManager_t2767934455 *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___thisLanguageManager0, targetMethod);
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			typedef void (*FunctionPointerType) (void*, LanguageManager_t2767934455 *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, ___thisLanguageManager0, targetMethod);
		}
		else
		{
			// open
			typedef void (*FunctionPointerType) (LanguageManager_t2767934455 *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(___thisLanguageManager0, targetMethod);
		}
	}
}
// System.IAsyncResult SmartLocalization.ChangeLanguageEventHandler::BeginInvoke(SmartLocalization.LanguageManager,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ChangeLanguageEventHandler_BeginInvoke_m3559598960 (ChangeLanguageEventHandler_t2032193146 * __this, LanguageManager_t2767934455 * ___thisLanguageManager0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___thisLanguageManager0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void SmartLocalization.ChangeLanguageEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ChangeLanguageEventHandler_EndInvoke_m761827169 (ChangeLanguageEventHandler_t2032193146 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageDataHandler::.ctor()
extern "C"  void LanguageDataHandler__ctor_m1941805053 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler__ctor_m1941805053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		SortedDictionary_2_t2617245831 * L_0 = (SortedDictionary_2_t2617245831 *)il2cpp_codegen_object_new(SortedDictionary_2_t2617245831_il2cpp_TypeInfo_var);
		SortedDictionary_2__ctor_m964777687(L_0, /*hidden argument*/SortedDictionary_2__ctor_m964777687_RuntimeMethod_var);
		__this->set_loadedValuesDictionary_0(L_0);
		return;
	}
}
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::get_LoadedCulture()
extern "C"  SmartCultureInfo_t2060094783 * LanguageDataHandler_get_LoadedCulture_m384579762 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = __this->get_loadedCultureInfo_2();
		return L_0;
	}
}
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedCulture(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageDataHandler_set_LoadedCulture_m3772607010 (LanguageDataHandler_t3889722316 * __this, SmartCultureInfo_t2060094783 * ___value0, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = ___value0;
		__this->set_loadedCultureInfo_2(L_0);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageDataHandler::get_VerboseLogging()
extern "C"  bool LanguageDataHandler_get_VerboseLogging_m516950596 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_verboseLogging_1();
		return L_0;
	}
}
// System.Void SmartLocalization.LanguageDataHandler::set_VerboseLogging(System.Boolean)
extern "C"  void LanguageDataHandler_set_VerboseLogging_m2066869259 (LanguageDataHandler_t3889722316 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_verboseLogging_1(L_0);
		return;
	}
}
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::get_LoadedValuesDictionary()
extern "C"  SortedDictionary_2_t2617245831 * LanguageDataHandler_get_LoadedValuesDictionary_m2388809926 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	{
		SortedDictionary_2_t2617245831 * L_0 = __this->get_loadedValuesDictionary_0();
		return L_0;
	}
}
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedValuesDictionary(System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageDataHandler_set_LoadedValuesDictionary_m2801856716 (LanguageDataHandler_t3889722316 * __this, SortedDictionary_2_t2617245831 * ___value0, const RuntimeMethod* method)
{
	{
		SortedDictionary_2_t2617245831 * L_0 = ___value0;
		__this->set_loadedValuesDictionary_0(L_0);
		return;
	}
}
// SmartLocalization.ILocalizedAssetLoader SmartLocalization.LanguageDataHandler::get_AssetLoader()
extern "C"  RuntimeObject* LanguageDataHandler_get_AssetLoader_m2232983836 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_get_AssetLoader_m2232983836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_assetLoader_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RuntimeLocalizedAssetLoader_t316402138 * L_1 = (RuntimeLocalizedAssetLoader_t316402138 *)il2cpp_codegen_object_new(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var);
		RuntimeLocalizedAssetLoader__ctor_m3777454129(L_1, /*hidden argument*/NULL);
		__this->set_assetLoader_3(L_1);
	}

IL_0016:
	{
		RuntimeObject* L_2 = __this->get_assetLoader_3();
		return L_2;
	}
}
// System.Boolean SmartLocalization.LanguageDataHandler::Load(System.String)
extern "C"  bool LanguageDataHandler_Load_m1742461671 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_Load_m1742461671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SortedDictionary_2_t2617245831 * V_0 = NULL;
	{
		String_t* L_0 = ___resxData0;
		SortedDictionary_2_t2617245831 * L_1 = LanguageDataHandler_LoadLanguageDictionary_m2285733254(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SortedDictionary_2_t2617245831 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = SortedDictionary_2_get_Count_m634161304(L_3, /*hidden argument*/SortedDictionary_2_get_Count_m634161304_RuntimeMethod_var);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_5 = V_0;
		__this->set_loadedValuesDictionary_0(L_5);
		bool L_6 = __this->get_verboseLogging_1();
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1798215199, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
// System.Boolean SmartLocalization.LanguageDataHandler::Append(System.String)
extern "C"  bool LanguageDataHandler_Append_m248544474 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_Append_m248544474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SortedDictionary_2_t2617245831 * V_0 = NULL;
	KeyValuePair_2_t3592397703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t4117933661  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___resxData0;
		SortedDictionary_2_t2617245831 * L_1 = LanguageDataHandler_LoadLanguageDictionary_m2285733254(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SortedDictionary_2_t2617245831 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_00c7;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = SortedDictionary_2_get_Count_m634161304(L_3, /*hidden argument*/SortedDictionary_2_get_Count_m634161304_RuntimeMethod_var);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00c7;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_5 = V_0;
		NullCheck(L_5);
		Enumerator_t4117933661  L_6 = SortedDictionary_2_GetEnumerator_m3922890767(L_5, /*hidden argument*/SortedDictionary_2_GetEnumerator_m3922890767_RuntimeMethod_var);
		V_2 = L_6;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007c;
		}

IL_0026:
		{
			KeyValuePair_2_t3592397703  L_7 = Enumerator_get_Current_m3473425924((&V_2), /*hidden argument*/Enumerator_get_Current_m3473425924_RuntimeMethod_var);
			V_1 = L_7;
			SortedDictionary_2_t2617245831 * L_8 = __this->get_loadedValuesDictionary_0();
			String_t* L_9 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			NullCheck(L_8);
			bool L_10 = SortedDictionary_2_ContainsKey_m1995604387(L_8, L_9, /*hidden argument*/SortedDictionary_2_ContainsKey_m1995604387_RuntimeMethod_var);
			if (!L_10)
			{
				goto IL_0063;
			}
		}

IL_0045:
		{
			SortedDictionary_2_t2617245831 * L_11 = __this->get_loadedValuesDictionary_0();
			String_t* L_12 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			LocalizedObject_t1409469237 * L_13 = KeyValuePair_2_get_Value_m52858533((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m52858533_RuntimeMethod_var);
			NullCheck(L_11);
			SortedDictionary_2_set_Item_m3916906723(L_11, L_12, L_13, /*hidden argument*/SortedDictionary_2_set_Item_m3916906723_RuntimeMethod_var);
			goto IL_007c;
		}

IL_0063:
		{
			SortedDictionary_2_t2617245831 * L_14 = __this->get_loadedValuesDictionary_0();
			String_t* L_15 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			LocalizedObject_t1409469237 * L_16 = KeyValuePair_2_get_Value_m52858533((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m52858533_RuntimeMethod_var);
			NullCheck(L_14);
			SortedDictionary_2_Add_m1412840442(L_14, L_15, L_16, /*hidden argument*/SortedDictionary_2_Add_m1412840442_RuntimeMethod_var);
		}

IL_007c:
		{
			bool L_17 = Enumerator_MoveNext_m1285001833((&V_2), /*hidden argument*/Enumerator_MoveNext_m1285001833_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_0026;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0x9B, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m235344939((&V_2), /*hidden argument*/Enumerator_Dispose_m235344939_RuntimeMethod_var);
		IL2CPP_END_FINALLY(141)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_009b:
	{
		bool L_18 = __this->get_verboseLogging_1();
		if (!L_18)
		{
			goto IL_00c5;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = SortedDictionary_2_get_Count_m634161304(L_19, /*hidden argument*/SortedDictionary_2_get_Count_m634161304_RuntimeMethod_var);
		int32_t L_21 = L_20;
		RuntimeObject * L_22 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral982267627, L_22, _stringLiteral2945251091, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return (bool)1;
	}

IL_00c7:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetKeysWithinCategory(System.String)
extern "C"  List_1_t3319525431 * LanguageDataHandler_GetKeysWithinCategory_m2182585259 (LanguageDataHandler_t3889722316 * __this, String_t* ___category0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_GetKeysWithinCategory_m2182585259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3319525431 * V_0 = NULL;
	KeyValuePair_2_t3592397703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t4117933661  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___category0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		SortedDictionary_2_t2617245831 * L_2 = __this->get_loadedValuesDictionary_0();
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_0016:
	{
		List_1_t3319525431 * L_3 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_3, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		return L_3;
	}

IL_001c:
	{
		List_1_t3319525431 * L_4 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_4, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		V_0 = L_4;
		SortedDictionary_2_t2617245831 * L_5 = __this->get_loadedValuesDictionary_0();
		NullCheck(L_5);
		Enumerator_t4117933661  L_6 = SortedDictionary_2_GetEnumerator_m3922890767(L_5, /*hidden argument*/SortedDictionary_2_GetEnumerator_m3922890767_RuntimeMethod_var);
		V_2 = L_6;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005a;
		}

IL_0033:
		{
			KeyValuePair_2_t3592397703  L_7 = Enumerator_get_Current_m3473425924((&V_2), /*hidden argument*/Enumerator_get_Current_m3473425924_RuntimeMethod_var);
			V_1 = L_7;
			String_t* L_8 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			String_t* L_9 = ___category0;
			NullCheck(L_8);
			bool L_10 = String_StartsWith_m1759067526(L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_005a;
			}
		}

IL_004d:
		{
			List_1_t3319525431 * L_11 = V_0;
			String_t* L_12 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			NullCheck(L_11);
			List_1_Add_m1685793073(L_11, L_12, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		}

IL_005a:
		{
			bool L_13 = Enumerator_MoveNext_m1285001833((&V_2), /*hidden argument*/Enumerator_MoveNext_m1285001833_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0033;
			}
		}

IL_0066:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m235344939((&V_2), /*hidden argument*/Enumerator_Dispose_m235344939_RuntimeMethod_var);
		IL2CPP_END_FINALLY(107)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0079:
	{
		List_1_t3319525431 * L_14 = V_0;
		return L_14;
	}
}
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetAllKeys()
extern "C"  List_1_t3319525431 * LanguageDataHandler_GetAllKeys_m1125042489 (LanguageDataHandler_t3889722316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_GetAllKeys_m1125042489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortedDictionary_2_t2617245831 * L_0 = __this->get_loadedValuesDictionary_0();
		NullCheck(L_0);
		KeyCollection_t1940831260 * L_1 = SortedDictionary_2_get_Keys_m3652780046(L_0, /*hidden argument*/SortedDictionary_2_get_Keys_m3652780046_RuntimeMethod_var);
		List_1_t3319525431 * L_2 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m928983596(L_2, L_1, /*hidden argument*/List_1__ctor_m928983596_RuntimeMethod_var);
		return L_2;
	}
}
// SmartLocalization.LocalizedObject SmartLocalization.LanguageDataHandler::GetLocalizedObject(System.String)
extern "C"  LocalizedObject_t1409469237 * LanguageDataHandler_GetLocalizedObject_m3611374437 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_GetLocalizedObject_m3611374437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LocalizedObject_t1409469237 * V_0 = NULL;
	{
		SortedDictionary_2_t2617245831 * L_0 = __this->get_loadedValuesDictionary_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		SortedDictionary_2_TryGetValue_m3861572157(L_0, L_1, (&V_0), /*hidden argument*/SortedDictionary_2_TryGetValue_m3861572157_RuntimeMethod_var);
		LocalizedObject_t1409469237 * L_2 = V_0;
		return L_2;
	}
}
// System.String SmartLocalization.LanguageDataHandler::GetTextValue(System.String)
extern "C"  String_t* LanguageDataHandler_GetTextValue_m495737691 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_GetTextValue_m495737691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LocalizedObject_t1409469237 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		LocalizedObject_t1409469237 * L_1 = LanguageDataHandler_GetLocalizedObject_m3611374437(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		LocalizedObject_t1409469237 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		LocalizedObject_t1409469237 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = LocalizedObject_get_TextValue_m2788713862(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		bool L_5 = LanguageDataHandler_get_VerboseLogging_m516950596(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2354339495, L_6, _stringLiteral1513108754, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return (String_t*)NULL;
	}
}
// System.Boolean SmartLocalization.LanguageDataHandler::HasKey(System.String)
extern "C"  bool LanguageDataHandler_HasKey_m2190396545 (LanguageDataHandler_t3889722316 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___key0;
		LocalizedObject_t1409469237 * L_1 = LanguageDataHandler_GetLocalizedObject_m3611374437(__this, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((RuntimeObject*)(LocalizedObject_t1409469237 *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::LoadLanguageDictionary(System.String)
extern "C"  SortedDictionary_2_t2617245831 * LanguageDataHandler_LoadLanguageDictionary_m2285733254 (LanguageDataHandler_t3889722316 * __this, String_t* ___resxData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_LoadLanguageDictionary_m2285733254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___resxData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (SortedDictionary_2_t2617245831 *)NULL;
	}

IL_000d:
	{
		String_t* L_2 = ___resxData0;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageParser_t1152791527_il2cpp_TypeInfo_var);
		SortedDictionary_2_t2617245831 * L_3 = LanguageParser_LoadLanguage_m379871964(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String SmartLocalization.LanguageDataHandler::CheckLanguageOverrideCode(SmartLocalization.LocalizedObject)
extern "C"  String_t* LanguageDataHandler_CheckLanguageOverrideCode_m1870029841 (LanguageDataHandler_t3889722316 * __this, LocalizedObject_t1409469237 * ___localizedObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageDataHandler_CheckLanguageOverrideCode_m1870029841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		LocalizedObject_t1409469237 * L_0 = ___localizedObject0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		SmartCultureInfo_t2060094783 * L_1 = __this->get_loadedCultureInfo_2();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_languageCode_0();
		return L_2;
	}

IL_0012:
	{
		LocalizedObject_t1409469237 * L_3 = ___localizedObject0;
		NullCheck(L_3);
		bool L_4 = LocalizedObject_get_OverrideLocalizedObject_m2345827120(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		LocalizedObject_t1409469237 * L_5 = ___localizedObject0;
		NullCheck(L_5);
		String_t* L_6 = LocalizedObject_get_OverrideObjectLanguageCode_m2176233279(L_5, /*hidden argument*/NULL);
		G_B5_0 = L_6;
		goto IL_0033;
	}

IL_0028:
	{
		SmartCultureInfo_t2060094783 * L_7 = __this->get_loadedCultureInfo_2();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_languageCode_0();
		G_B5_0 = L_8;
	}

IL_0033:
	{
		V_0 = G_B5_0;
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004b;
		}
	}
	{
		SmartCultureInfo_t2060094783 * L_11 = __this->get_loadedCultureInfo_2();
		NullCheck(L_11);
		String_t* L_12 = L_11->get_languageCode_0();
		V_0 = L_12;
	}

IL_004b:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageManager::.ctor()
extern "C"  void LanguageManager__ctor_m3510348828 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager__ctor_m3510348828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_defaultLanguage_10(_stringLiteral3454842811);
		LanguageDataHandler_t3889722316 * L_0 = (LanguageDataHandler_t3889722316 *)il2cpp_codegen_object_new(LanguageDataHandler_t3889722316_il2cpp_TypeInfo_var);
		LanguageDataHandler__ctor_m1941805053(L_0, /*hidden argument*/NULL);
		__this->set_languageDataHandler_12(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::get_Instance()
extern "C"  LanguageManager_t2767934455 * LanguageManager_get_Instance_m861325331 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_get_Instance_m861325331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		int32_t L_0 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)7))))
		{
			goto IL_002f;
		}
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_2 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_4 = Object_FindObjectOfType_TisLanguageManager_t2767934455_m1920039193(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLanguageManager_t2767934455_m1920039193_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_instance_2(L_4);
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_5 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0089;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_7 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_IsQuitting_3();
		if (L_7)
		{
			goto IL_0089;
		}
	}
	{
		GameObject_t1113636619 * L_8 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m3707688467(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObject_t1113636619 * L_9 = V_0;
		NullCheck(L_9);
		LanguageManager_t2767934455 * L_10 = GameObject_AddComponent_TisLanguageManager_t2767934455_m4185499804(L_9, /*hidden argument*/GameObject_AddComponent_TisLanguageManager_t2767934455_m4185499804_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_instance_2(L_10);
		GameObject_t1113636619 * L_11 = V_0;
		NullCheck(L_11);
		Object_set_name_m291480324(L_11, _stringLiteral2583110339, /*hidden argument*/NULL);
		bool L_12 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_DidSetDontDestroyOnLoad_5();
		if (L_12)
		{
			goto IL_0089;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_13 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_DontDestroyOnLoadToggle_4();
		if (!L_13)
		{
			goto IL_0089;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_14 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_DidSetDontDestroyOnLoad_5((bool)1);
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_15 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		return L_15;
	}
}
// System.Void SmartLocalization.LanguageManager::SetDontDestroyOnLoad()
extern "C"  void LanguageManager_SetDontDestroyOnLoad_m13667428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_SetDontDestroyOnLoad_m13667428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_DontDestroyOnLoadToggle_4((bool)1);
		LanguageManager_t2767934455 * L_0 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_2 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_DidSetDontDestroyOnLoad_5();
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_3 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_DidSetDontDestroyOnLoad_5((bool)1);
	}

IL_0030:
	{
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager::get_HasInstance()
extern "C"  bool LanguageManager_get_HasInstance_m2564662803 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_get_HasInstance_m2564662803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_0 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SmartLocalization.LanguageManager::OnAfterDeserialize()
extern "C"  void LanguageManager_OnAfterDeserialize_m3239827748 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_OnAfterDeserialize_m3239827748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t3319525431 * L_0 = __this->get_serializedKeys_6();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		LanguageDataHandler_t3889722316 * L_1 = __this->get_languageDataHandler_12();
		SortedDictionary_2_t2617245831 * L_2 = (SortedDictionary_2_t2617245831 *)il2cpp_codegen_object_new(SortedDictionary_2_t2617245831_il2cpp_TypeInfo_var);
		SortedDictionary_2__ctor_m964777687(L_2, /*hidden argument*/SortedDictionary_2__ctor_m964777687_RuntimeMethod_var);
		NullCheck(L_1);
		LanguageDataHandler_set_LoadedValuesDictionary_m2801856716(L_1, L_2, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_004f;
	}

IL_0023:
	{
		LanguageDataHandler_t3889722316 * L_3 = __this->get_languageDataHandler_12();
		NullCheck(L_3);
		SortedDictionary_2_t2617245831 * L_4 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_3, /*hidden argument*/NULL);
		List_1_t3319525431 * L_5 = __this->get_serializedKeys_6();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = List_1_get_Item_m3729392029(L_5, L_6, /*hidden argument*/List_1_get_Item_m3729392029_RuntimeMethod_var);
		List_1_t2881543979 * L_8 = __this->get_serializedValues_7();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		LocalizedObject_t1409469237 * L_10 = List_1_get_Item_m3162138650(L_8, L_9, /*hidden argument*/List_1_get_Item_m3162138650_RuntimeMethod_var);
		NullCheck(L_4);
		SortedDictionary_2_Add_m1412840442(L_4, L_7, L_10, /*hidden argument*/SortedDictionary_2_Add_m1412840442_RuntimeMethod_var);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_004f:
	{
		int32_t L_12 = V_0;
		List_1_t3319525431 * L_13 = __this->get_serializedKeys_6();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2276455407(L_13, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0023;
		}
	}
	{
		LanguageDataHandler_t3889722316 * L_15 = __this->get_languageDataHandler_12();
		SmartCultureInfo_t2060094783 * L_16 = __this->get_serializedCulture_8();
		NullCheck(L_15);
		LanguageDataHandler_set_LoadedCulture_m3772607010(L_15, L_16, /*hidden argument*/NULL);
		List_1_t3319525431 * L_17 = __this->get_serializedKeys_6();
		NullCheck(L_17);
		List_1_Clear_m3111619026(L_17, /*hidden argument*/List_1_Clear_m3111619026_RuntimeMethod_var);
		List_1_t2881543979 * L_18 = __this->get_serializedValues_7();
		NullCheck(L_18);
		List_1_Clear_m1509931054(L_18, /*hidden argument*/List_1_Clear_m1509931054_RuntimeMethod_var);
		__this->set_serializedCulture_8((SmartCultureInfo_t2060094783 *)NULL);
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::OnBeforeSerialize()
extern "C"  void LanguageManager_OnBeforeSerialize_m2833749936 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_OnBeforeSerialize_m2833749936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3592397703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t4117933661  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3319525431 * L_0 = __this->get_serializedKeys_6();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t3319525431 * L_1 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_1, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_serializedKeys_6(L_1);
	}

IL_0016:
	{
		List_1_t2881543979 * L_2 = __this->get_serializedValues_7();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t2881543979 * L_3 = (List_1_t2881543979 *)il2cpp_codegen_object_new(List_1_t2881543979_il2cpp_TypeInfo_var);
		List_1__ctor_m981406146(L_3, /*hidden argument*/List_1__ctor_m981406146_RuntimeMethod_var);
		__this->set_serializedValues_7(L_3);
	}

IL_002c:
	{
		List_1_t3319525431 * L_4 = __this->get_serializedKeys_6();
		NullCheck(L_4);
		List_1_Clear_m3111619026(L_4, /*hidden argument*/List_1_Clear_m3111619026_RuntimeMethod_var);
		List_1_t2881543979 * L_5 = __this->get_serializedValues_7();
		NullCheck(L_5);
		List_1_Clear_m1509931054(L_5, /*hidden argument*/List_1_Clear_m1509931054_RuntimeMethod_var);
		LanguageDataHandler_t3889722316 * L_6 = __this->get_languageDataHandler_12();
		NullCheck(L_6);
		SortedDictionary_2_t2617245831 * L_7 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0053;
		}
	}
	{
		return;
	}

IL_0053:
	{
		LanguageDataHandler_t3889722316 * L_8 = __this->get_languageDataHandler_12();
		NullCheck(L_8);
		SortedDictionary_2_t2617245831 * L_9 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Enumerator_t4117933661  L_10 = SortedDictionary_2_GetEnumerator_m3922890767(L_9, /*hidden argument*/SortedDictionary_2_GetEnumerator_m3922890767_RuntimeMethod_var);
		V_1 = L_10;
	}

IL_0064:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0095;
		}

IL_0069:
		{
			KeyValuePair_2_t3592397703  L_11 = Enumerator_get_Current_m3473425924((&V_1), /*hidden argument*/Enumerator_get_Current_m3473425924_RuntimeMethod_var);
			V_0 = L_11;
			List_1_t3319525431 * L_12 = __this->get_serializedKeys_6();
			String_t* L_13 = KeyValuePair_2_get_Key_m2085155650((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			NullCheck(L_12);
			List_1_Add_m1685793073(L_12, L_13, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			List_1_t2881543979 * L_14 = __this->get_serializedValues_7();
			LocalizedObject_t1409469237 * L_15 = KeyValuePair_2_get_Value_m52858533((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m52858533_RuntimeMethod_var);
			NullCheck(L_14);
			List_1_Add_m2873794602(L_14, L_15, /*hidden argument*/List_1_Add_m2873794602_RuntimeMethod_var);
		}

IL_0095:
		{
			bool L_16 = Enumerator_MoveNext_m1285001833((&V_1), /*hidden argument*/Enumerator_MoveNext_m1285001833_RuntimeMethod_var);
			if (L_16)
			{
				goto IL_0069;
			}
		}

IL_00a1:
		{
			IL2CPP_LEAVE(0xB4, FINALLY_00a6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a6;
	}

FINALLY_00a6:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m235344939((&V_1), /*hidden argument*/Enumerator_Dispose_m235344939_RuntimeMethod_var);
		IL2CPP_END_FINALLY(166)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(166)
	{
		IL2CPP_JUMP_TBL(0xB4, IL_00b4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00b4:
	{
		SmartCultureInfo_t2060094783 * L_17 = LanguageManager_get_CurrentlyLoadedCulture_m1071669230(__this, /*hidden argument*/NULL);
		__this->set_serializedCulture_8(L_17);
		return;
	}
}
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageManager::get_LanguageDatabase()
extern "C"  SortedDictionary_2_t2617245831 * LanguageManager_get_LanguageDatabase_m1854744181 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		NullCheck(L_0);
		SortedDictionary_2_t2617245831 * L_1 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> SmartLocalization.LanguageManager::get_RawTextDatabase()
extern "C"  Dictionary_2_t1632706988 * LanguageManager_get_RawTextDatabase_m1351981778 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_get_RawTextDatabase_m1351981778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1632706988 * V_0 = NULL;
	KeyValuePair_2_t3592397703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t4117933661  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		NullCheck(L_0);
		SortedDictionary_2_t2617245831 * L_1 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (Dictionary_2_t1632706988 *)NULL;
	}

IL_0012:
	{
		Dictionary_2_t1632706988 * L_2 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_2, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_0 = L_2;
		LanguageDataHandler_t3889722316 * L_3 = __this->get_languageDataHandler_12();
		NullCheck(L_3);
		SortedDictionary_2_t2617245831 * L_4 = LanguageDataHandler_get_LoadedValuesDictionary_m2388809926(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Enumerator_t4117933661  L_5 = SortedDictionary_2_GetEnumerator_m3922890767(L_4, /*hidden argument*/SortedDictionary_2_GetEnumerator_m3922890767_RuntimeMethod_var);
		V_2 = L_5;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_002e:
		{
			KeyValuePair_2_t3592397703  L_6 = Enumerator_get_Current_m3473425924((&V_2), /*hidden argument*/Enumerator_get_Current_m3473425924_RuntimeMethod_var);
			V_1 = L_6;
			Dictionary_2_t1632706988 * L_7 = V_0;
			String_t* L_8 = KeyValuePair_2_get_Key_m2085155650((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2085155650_RuntimeMethod_var);
			LocalizedObject_t1409469237 * L_9 = KeyValuePair_2_get_Value_m52858533((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m52858533_RuntimeMethod_var);
			NullCheck(L_9);
			String_t* L_10 = LocalizedObject_get_TextValue_m2788713862(L_9, /*hidden argument*/NULL);
			NullCheck(L_7);
			Dictionary_2_Add_m3665229156(L_7, L_8, L_10, /*hidden argument*/Dictionary_2_Add_m3665229156_RuntimeMethod_var);
		}

IL_004f:
		{
			bool L_11 = Enumerator_MoveNext_m1285001833((&V_2), /*hidden argument*/Enumerator_MoveNext_m1285001833_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_002e;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x6E, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m235344939((&V_2), /*hidden argument*/Enumerator_Dispose_m235344939_RuntimeMethod_var);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006e:
	{
		Dictionary_2_t1632706988 * L_12 = V_0;
		return L_12;
	}
}
// System.Int32 SmartLocalization.LanguageManager::get_NumberOfSupportedLanguages()
extern "C"  int32_t LanguageManager_get_NumberOfSupportedLanguages_m2642276512 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_get_NumberOfSupportedLanguages_m2642276512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfoCollection_t1809038765 * L_0 = __this->get_availableLanguages_11();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		SmartCultureInfoCollection_t1809038765 * L_1 = __this->get_availableLanguages_11();
		NullCheck(L_1);
		List_1_t3532169525 * L_2 = L_1->get_cultureInfos_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2960725483(L_2, /*hidden argument*/List_1_get_Count_m2960725483_RuntimeMethod_var);
		return L_3;
	}
}
// System.String SmartLocalization.LanguageManager::get_LoadedLanguage()
extern "C"  String_t* LanguageManager_get_LoadedLanguage_m545115699 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = LanguageManager_get_CurrentlyLoadedCulture_m1071669230(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		return L_1;
	}
}
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::get_CurrentlyLoadedCulture()
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_get_CurrentlyLoadedCulture_m1071669230 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		NullCheck(L_0);
		SmartCultureInfo_t2060094783 * L_1 = LanguageDataHandler_get_LoadedCulture_m384579762(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean SmartLocalization.LanguageManager::get_VerboseLogging()
extern "C"  bool LanguageManager_get_VerboseLogging_m1685643384 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		NullCheck(L_0);
		bool L_1 = LanguageDataHandler_get_VerboseLogging_m516950596(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SmartLocalization.LanguageManager::set_VerboseLogging(System.Boolean)
extern "C"  void LanguageManager_set_VerboseLogging_m2829385089 (LanguageManager_t2767934455 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		bool L_1 = ___value0;
		NullCheck(L_0);
		LanguageDataHandler_set_VerboseLogging_m2066869259(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::Awake()
extern "C"  void LanguageManager_Awake_m2745406980 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_Awake_m2745406980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SmartCultureInfo_t2060094783 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_0 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_instance_2(__this);
		goto IL_0047;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_2 = ((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		bool L_4 = LanguageManager_get_VerboseLogging_m1685643384(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral1133921496, /*hidden argument*/NULL);
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}

IL_0047:
	{
		bool L_5 = LanguageManager_LoadAvailableCultures_m3449409331(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00f3;
		}
	}
	{
		bool L_6 = LanguageManager_get_VerboseLogging_m1685643384(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3186498657, /*hidden argument*/NULL);
	}

IL_0067:
	{
		SmartCultureInfoCollection_t1809038765 * L_7 = __this->get_availableLanguages_11();
		NullCheck(L_7);
		List_1_t3532169525 * L_8 = L_7->get_cultureInfos_2();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m2960725483(L_8, /*hidden argument*/List_1_get_Count_m2960725483_RuntimeMethod_var);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_00e4;
		}
	}
	{
		SmartCultureInfoCollection_t1809038765 * L_10 = __this->get_availableLanguages_11();
		NullCheck(L_10);
		List_1_t3532169525 * L_11 = L_10->get_cultureInfos_2();
		intptr_t L_12 = (intptr_t)LanguageManager_U3CAwakeU3Em__0_m3454044807_RuntimeMethod_var;
		Predicate_1_t2885388907 * L_13 = (Predicate_1_t2885388907 *)il2cpp_codegen_object_new(Predicate_1_t2885388907_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2314931459(L_13, __this, L_12, /*hidden argument*/Predicate_1__ctor_m2314931459_RuntimeMethod_var);
		NullCheck(L_11);
		SmartCultureInfo_t2060094783 * L_14 = List_1_Find_m4070095342(L_11, L_13, /*hidden argument*/List_1_Find_m4070095342_RuntimeMethod_var);
		V_0 = L_14;
		SmartCultureInfo_t2060094783 * L_15 = V_0;
		if (!L_15)
		{
			goto IL_00ac;
		}
	}
	{
		SmartCultureInfo_t2060094783 * L_16 = V_0;
		LanguageManager_ChangeLanguage_m1970109620(__this, L_16, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00ac:
	{
		SmartCultureInfoCollection_t1809038765 * L_17 = __this->get_availableLanguages_11();
		NullCheck(L_17);
		List_1_t3532169525 * L_18 = L_17->get_cultureInfos_2();
		NullCheck(L_18);
		SmartCultureInfo_t2060094783 * L_19 = List_1_get_Item_m1520579522(L_18, 0, /*hidden argument*/List_1_get_Item_m1520579522_RuntimeMethod_var);
		LanguageManager_ChangeLanguage_m1970109620(__this, L_19, /*hidden argument*/NULL);
		SmartCultureInfoCollection_t1809038765 * L_20 = __this->get_availableLanguages_11();
		NullCheck(L_20);
		List_1_t3532169525 * L_21 = L_20->get_cultureInfos_2();
		NullCheck(L_21);
		SmartCultureInfo_t2060094783 * L_22 = List_1_get_Item_m1520579522(L_21, 0, /*hidden argument*/List_1_get_Item_m1520579522_RuntimeMethod_var);
		NullCheck(L_22);
		String_t* L_23 = L_22->get_languageCode_0();
		__this->set_defaultLanguage_10(L_23);
	}

IL_00df:
	{
		goto IL_00ee;
	}

IL_00e4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral857763534, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		goto IL_00fd;
	}

IL_00f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2952380563, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::OnDestroy()
extern "C"  void LanguageManager_OnDestroy_m1141869004 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		__this->set_OnChangeLanguage_9((ChangeLanguageEventHandler_t2032193146 *)NULL);
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::OnApplicationQuit()
extern "C"  void LanguageManager_OnApplicationQuit_m2266523370 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_OnApplicationQuit_m2266523370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		((LanguageManager_t2767934455_StaticFields*)il2cpp_codegen_static_fields_for(LanguageManager_t2767934455_il2cpp_TypeInfo_var))->set_IsQuitting_3((bool)1);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager::LoadAvailableCultures()
extern "C"  bool LanguageManager_LoadAvailableCultures_m3449409331 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_LoadAvailableCultures_m3449409331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextAsset_t3022178571 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_0 = LanguageRuntimeData_AvailableCulturesFilePath_m3409607084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t631007953 * L_1 = Resources_Load_m3880010804(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((TextAsset_t3022178571 *)IsInstClass((RuntimeObject*)L_1, TextAsset_t3022178571_il2cpp_TypeInfo_var));
		TextAsset_t3022178571 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral272499010, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0028:
	{
		TextAsset_t3022178571 * L_4 = V_0;
		SmartCultureInfoCollection_t1809038765 * L_5 = SmartCultureInfoCollection_Deserialize_m2953505266(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_availableLanguages_11(L_5);
		return (bool)1;
	}
}
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::GetAllKeys()
extern "C"  List_1_t3319525431 * LanguageManager_GetAllKeys_m4099376538 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = LanguageDataHandler_GetAllKeys_m1125042489(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageManager_ChangeLanguage_m1970109620 (LanguageManager_t2767934455 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = ___cultureInfo0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		LanguageManager_ChangeLanguage_m2374647000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(System.String)
extern "C"  void LanguageManager_ChangeLanguage_m2374647000 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_ChangeLanguage_m2374647000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextAsset_t3022178571 * V_0 = NULL;
	{
		String_t* L_0 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_1 = LanguageRuntimeData_LanguageFilePath_m1120529596(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Object_t631007953 * L_2 = Resources_Load_m3880010804(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((TextAsset_t3022178571 *)IsInstClass((RuntimeObject*)L_2, TextAsset_t3022178571_il2cpp_TypeInfo_var));
		TextAsset_t3022178571 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_5 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2081855419, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		TextAsset_t3022178571 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = TextAsset_get_text_m2027878391(L_7, /*hidden argument*/NULL);
		String_t* L_9 = ___languageCode0;
		LanguageManager_LoadLanguage_m577556272(__this, L_8, L_9, /*hidden argument*/NULL);
		ChangeLanguageEventHandler_t2032193146 * L_10 = __this->get_OnChangeLanguage_9();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		ChangeLanguageEventHandler_t2032193146 * L_11 = __this->get_OnChangeLanguage_9();
		NullCheck(L_11);
		ChangeLanguageEventHandler_Invoke_m109768076(L_11, __this, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void SmartLocalization.LanguageManager::ChangeLanguageWithData(System.String,System.String)
extern "C"  void LanguageManager_ChangeLanguageWithData_m112952847 (LanguageManager_t2767934455 * __this, String_t* ___languageDataInResX0, String_t* ___languageCode1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___languageDataInResX0;
		String_t* L_1 = ___languageCode1;
		bool L_2 = LanguageManager_LoadLanguage_m577556272(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		ChangeLanguageEventHandler_t2032193146 * L_3 = __this->get_OnChangeLanguage_9();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		ChangeLanguageEventHandler_t2032193146 * L_4 = __this->get_OnChangeLanguage_9();
		NullCheck(L_4);
		ChangeLanguageEventHandler_Invoke_m109768076(L_4, __this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager::AppendLanguageWithTextData(System.String)
extern "C"  bool LanguageManager_AppendLanguageWithTextData_m493888092 (LanguageManager_t2767934455 * __this, String_t* ___languageDataInResX0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___languageDataInResX0;
		NullCheck(L_0);
		bool L_2 = LanguageDataHandler_Append_m248544474(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean SmartLocalization.LanguageManager::LoadLanguage(System.String,System.String)
extern "C"  bool LanguageManager_LoadLanguage_m577556272 (LanguageManager_t2767934455 * __this, String_t* ___languageData0, String_t* ___languageCode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_LoadLanguage_m577556272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SmartCultureInfo_t2060094783 * V_0 = NULL;
	{
		String_t* L_0 = ___languageData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2379534151, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0017:
	{
		String_t* L_2 = ___languageCode1;
		SmartCultureInfo_t2060094783 * L_3 = LanguageManager_GetCultureInfo_m1993048463(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		SmartCultureInfo_t2060094783 * L_4 = V_0;
		if (L_4)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_5 = ___languageCode1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1085397908, L_5, _stringLiteral67993702, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_003c:
	{
		LanguageDataHandler_t3889722316 * L_7 = __this->get_languageDataHandler_12();
		String_t* L_8 = ___languageData0;
		NullCheck(L_7);
		bool L_9 = LanguageDataHandler_Load_m1742461671(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		LanguageDataHandler_t3889722316 * L_10 = __this->get_languageDataHandler_12();
		SmartCultureInfo_t2060094783 * L_11 = V_0;
		NullCheck(L_10);
		LanguageDataHandler_set_LoadedCulture_m3772607010(L_10, L_11, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005b:
	{
		return (bool)0;
	}
}
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupported(System.String)
extern "C"  bool LanguageManager_IsLanguageSupported_m2070434101 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___languageCode0;
		bool L_1 = LanguageManager_IsCultureSupported_m294811961(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupported(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_IsLanguageSupported_m443277966 (LanguageManager_t2767934455 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = ___cultureInfo0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		bool L_2 = LanguageManager_IsLanguageSupported_m2070434101(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetSupportedSystemLanguage()
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_GetSupportedSystemLanguage_m48836997 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = LanguageManager_GetDeviceCultureIfSupported_m2865750724(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String SmartLocalization.LanguageManager::GetSupportedSystemLanguageCode()
extern "C"  String_t* LanguageManager_GetSupportedSystemLanguageCode_m593164813 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetSupportedSystemLanguageCode_m593164813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SmartCultureInfo_t2060094783 * V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		SmartCultureInfo_t2060094783 * L_0 = LanguageManager_GetDeviceCultureIfSupported_m2865750724(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SmartCultureInfo_t2060094783 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		SmartCultureInfo_t2060094783 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_languageCode_0();
		G_B3_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_4;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetDeviceCultureIfSupported()
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_GetDeviceCultureIfSupported_m2865750724 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetDeviceCultureIfSupported_m2865750724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * V_0 = NULL;
	{
		U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * L_0 = (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 *)il2cpp_codegen_object_new(U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546_il2cpp_TypeInfo_var);
		U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0__ctor_m958998401(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SmartCultureInfoCollection_t1809038765 * L_1 = __this->get_availableLanguages_11();
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return (SmartCultureInfo_t2060094783 *)NULL;
	}

IL_0013:
	{
		U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * L_2 = V_0;
		String_t* L_3 = LanguageManager_GetSystemLanguageEnglishName_m3716429587(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_englishName_0(L_3);
		SmartCultureInfoCollection_t1809038765 * L_4 = __this->get_availableLanguages_11();
		NullCheck(L_4);
		List_1_t3532169525 * L_5 = L_4->get_cultureInfos_2();
		U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3311227881_RuntimeMethod_var;
		Predicate_1_t2885388907 * L_8 = (Predicate_1_t2885388907 *)il2cpp_codegen_object_new(Predicate_1_t2885388907_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2314931459(L_8, L_6, L_7, /*hidden argument*/Predicate_1__ctor_m2314931459_RuntimeMethod_var);
		NullCheck(L_5);
		SmartCultureInfo_t2060094783 * L_9 = List_1_Find_m4070095342(L_5, L_8, /*hidden argument*/List_1_Find_m4070095342_RuntimeMethod_var);
		return L_9;
	}
}
// System.Boolean SmartLocalization.LanguageManager::IsCultureSupported(System.String)
extern "C"  bool LanguageManager_IsCultureSupported_m294811961 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_IsCultureSupported_m294811961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * V_0 = NULL;
	{
		U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * L_0 = (U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 *)il2cpp_codegen_object_new(U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798_il2cpp_TypeInfo_var);
		U3CIsCultureSupportedU3Ec__AnonStorey1__ctor_m1195679156(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * L_1 = V_0;
		String_t* L_2 = ___languageCode0;
		NullCheck(L_1);
		L_1->set_languageCode_0(L_2);
		SmartCultureInfoCollection_t1809038765 * L_3 = __this->get_availableLanguages_11();
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2890855871, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0024:
	{
		SmartCultureInfoCollection_t1809038765 * L_4 = __this->get_availableLanguages_11();
		NullCheck(L_4);
		List_1_t3532169525 * L_5 = L_4->get_cultureInfos_2();
		U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m4210711579_RuntimeMethod_var;
		Predicate_1_t2885388907 * L_8 = (Predicate_1_t2885388907 *)il2cpp_codegen_object_new(Predicate_1_t2885388907_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2314931459(L_8, L_6, L_7, /*hidden argument*/Predicate_1__ctor_m2314931459_RuntimeMethod_var);
		NullCheck(L_5);
		SmartCultureInfo_t2060094783 * L_9 = List_1_Find_m4070095342(L_5, L_8, /*hidden argument*/List_1_Find_m4070095342_RuntimeMethod_var);
		return (bool)((((int32_t)((((RuntimeObject*)(SmartCultureInfo_t2060094783 *)L_9) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean SmartLocalization.LanguageManager::IsCultureSupported(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_IsCultureSupported_m4019498667 (LanguageManager_t2767934455 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method)
{
	{
		SmartCultureInfo_t2060094783 * L_0 = ___cultureInfo0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		bool L_2 = LanguageManager_IsCultureSupported_m294811961(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupportedEnglishName(System.String)
extern "C"  bool LanguageManager_IsLanguageSupportedEnglishName_m3736923875 (LanguageManager_t2767934455 * __this, String_t* ___englishName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_IsLanguageSupportedEnglishName_m3736923875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * V_0 = NULL;
	{
		U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * L_0 = (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 *)il2cpp_codegen_object_new(U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582_il2cpp_TypeInfo_var);
		U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2__ctor_m119487228(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * L_1 = V_0;
		String_t* L_2 = ___englishName0;
		NullCheck(L_1);
		L_1->set_englishName_0(L_2);
		SmartCultureInfoCollection_t1809038765 * L_3 = __this->get_availableLanguages_11();
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2890855871, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0024:
	{
		SmartCultureInfoCollection_t1809038765 * L_4 = __this->get_availableLanguages_11();
		NullCheck(L_4);
		List_1_t3532169525 * L_5 = L_4->get_cultureInfos_2();
		U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m3271210525_RuntimeMethod_var;
		Predicate_1_t2885388907 * L_8 = (Predicate_1_t2885388907 *)il2cpp_codegen_object_new(Predicate_1_t2885388907_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2314931459(L_8, L_6, L_7, /*hidden argument*/Predicate_1__ctor_m2314931459_RuntimeMethod_var);
		NullCheck(L_5);
		SmartCultureInfo_t2060094783 * L_9 = List_1_Find_m4070095342(L_5, L_8, /*hidden argument*/List_1_Find_m4070095342_RuntimeMethod_var);
		return (bool)((((int32_t)((((RuntimeObject*)(SmartCultureInfo_t2060094783 *)L_9) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetCultureInfo(System.String)
extern "C"  SmartCultureInfo_t2060094783 * LanguageManager_GetCultureInfo_m1993048463 (LanguageManager_t2767934455 * __this, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetCultureInfo_m1993048463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * V_0 = NULL;
	{
		U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * L_0 = (U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 *)il2cpp_codegen_object_new(U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473_il2cpp_TypeInfo_var);
		U3CGetCultureInfoU3Ec__AnonStorey3__ctor_m3147796296(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * L_1 = V_0;
		String_t* L_2 = ___languageCode0;
		NullCheck(L_1);
		L_1->set_languageCode_0(L_2);
		SmartCultureInfoCollection_t1809038765 * L_3 = __this->get_availableLanguages_11();
		NullCheck(L_3);
		List_1_t3532169525 * L_4 = L_3->get_cultureInfos_2();
		U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * L_5 = V_0;
		intptr_t L_6 = (intptr_t)U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m2012133178_RuntimeMethod_var;
		Predicate_1_t2885388907 * L_7 = (Predicate_1_t2885388907 *)il2cpp_codegen_object_new(Predicate_1_t2885388907_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2314931459(L_7, L_5, L_6, /*hidden argument*/Predicate_1__ctor_m2314931459_RuntimeMethod_var);
		NullCheck(L_4);
		SmartCultureInfo_t2060094783 * L_8 = List_1_Find_m4070095342(L_4, L_7, /*hidden argument*/List_1_Find_m4070095342_RuntimeMethod_var);
		return L_8;
	}
}
// System.String SmartLocalization.LanguageManager::GetSystemLanguageEnglishName()
extern "C"  String_t* LanguageManager_GetSystemLanguageEnglishName_m3716429587 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ApplicationExtensions_GetSystemLanguage_m1620959893(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.LanguageManager::GetSupportedLanguages()
extern "C"  List_1_t3532169525 * LanguageManager_GetSupportedLanguages_m3762701334 (LanguageManager_t2767934455 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetSupportedLanguages_m3762701334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfoCollection_t1809038765 * L_0 = __this->get_availableLanguages_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral112791673, /*hidden argument*/NULL);
		return (List_1_t3532169525 *)NULL;
	}

IL_0017:
	{
		SmartCultureInfoCollection_t1809038765 * L_1 = __this->get_availableLanguages_11();
		NullCheck(L_1);
		List_1_t3532169525 * L_2 = L_1->get_cultureInfos_2();
		return L_2;
	}
}
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::GetKeysWithinCategory(System.String)
extern "C"  List_1_t3319525431 * LanguageManager_GetKeysWithinCategory_m979429038 (LanguageManager_t2767934455 * __this, String_t* ___category0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___category0;
		NullCheck(L_0);
		List_1_t3319525431 * L_2 = LanguageDataHandler_GetKeysWithinCategory_m2182585259(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SmartLocalization.LanguageManager::GetTextValue(System.String)
extern "C"  String_t* LanguageManager_GetTextValue_m2899262939 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		String_t* L_2 = LanguageDataHandler_GetTextValue_m495737691(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AudioClip SmartLocalization.LanguageManager::GetAudioClip(System.String)
extern "C"  AudioClip_t3680889665 * LanguageManager_GetAudioClip_m1838735899 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetAudioClip_m1838735899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		AudioClip_t3680889665 * L_2 = LanguageDataHandler_GetAsset_TisAudioClip_t3680889665_m950255953(L_0, L_1, /*hidden argument*/LanguageDataHandler_GetAsset_TisAudioClip_t3680889665_m950255953_RuntimeMethod_var);
		return L_2;
	}
}
// UnityEngine.GameObject SmartLocalization.LanguageManager::GetPrefab(System.String)
extern "C"  GameObject_t1113636619 * LanguageManager_GetPrefab_m980355803 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetPrefab_m980355803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_2 = LanguageDataHandler_GetAsset_TisGameObject_t1113636619_m521626920(L_0, L_1, /*hidden argument*/LanguageDataHandler_GetAsset_TisGameObject_t1113636619_m521626920_RuntimeMethod_var);
		return L_2;
	}
}
// UnityEngine.Texture SmartLocalization.LanguageManager::GetTexture(System.String)
extern "C"  Texture_t3661962703 * LanguageManager_GetTexture_m1160349625 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_GetTexture_m1160349625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		Texture_t3661962703 * L_2 = LanguageDataHandler_GetAsset_TisTexture_t3661962703_m2393095502(L_0, L_1, /*hidden argument*/LanguageDataHandler_GetAsset_TisTexture_t3661962703_m2393095502_RuntimeMethod_var);
		return L_2;
	}
}
// System.Boolean SmartLocalization.LanguageManager::HasKey(System.String)
extern "C"  bool LanguageManager_HasKey_m36224425 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = LanguageDataHandler_HasKey_m2190396545(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// SmartLocalization.LocalizedObject SmartLocalization.LanguageManager::GetLocalizedObject(System.String)
extern "C"  LocalizedObject_t1409469237 * LanguageManager_GetLocalizedObject_m2598250065 (LanguageManager_t2767934455 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		LanguageDataHandler_t3889722316 * L_0 = __this->get_languageDataHandler_12();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		LocalizedObject_t1409469237 * L_2 = LanguageDataHandler_GetLocalizedObject_m3611374437(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SmartLocalization.LanguageManager::.cctor()
extern "C"  void LanguageManager__cctor_m3799102090 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager::<Awake>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_U3CAwakeU3Em__0_m3454044807 (LanguageManager_t2767934455 * __this, SmartCultureInfo_t2060094783 * ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageManager_U3CAwakeU3Em__0_m3454044807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___info0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		String_t* L_2 = __this->get_defaultLanguage_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::.ctor()
extern "C"  void U3CGetCultureInfoU3Ec__AnonStorey3__ctor_m3147796296 (U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m2012133178 (U3CGetCultureInfoU3Ec__AnonStorey3_t3382773473 * __this, SmartCultureInfo_t2060094783 * ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m2012133178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___info0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		String_t* L_2 = __this->get_languageCode_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::.ctor()
extern "C"  void U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0__ctor_m958998401 (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3311227881 (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t3664339546 * __this, SmartCultureInfo_t2060094783 * ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3311227881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___info0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_englishName_1();
		NullCheck(L_1);
		String_t* L_2 = String_ToLower_m2029374922(L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_englishName_0();
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2029374922(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::.ctor()
extern "C"  void U3CIsCultureSupportedU3Ec__AnonStorey1__ctor_m1195679156 (U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m4210711579 (U3CIsCultureSupportedU3Ec__AnonStorey1_t4172074798 * __this, SmartCultureInfo_t2060094783 * ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m4210711579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___info0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_languageCode_0();
		String_t* L_2 = __this->get_languageCode_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::.ctor()
extern "C"  void U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2__ctor_m119487228 (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m3271210525 (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t4081837582 * __this, SmartCultureInfo_t2060094783 * ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m3271210525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___info0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_englishName_1();
		NullCheck(L_1);
		String_t* L_2 = String_ToLower_m2029374922(L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_englishName_0();
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2029374922(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageParser::LoadLanguage(System.String)
extern "C"  SortedDictionary_2_t2617245831 * LanguageParser_LoadLanguage_m379871964 (RuntimeObject * __this /* static, unused */, String_t* ___languageDataInResX0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageParser_LoadLanguage_m379871964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SortedDictionary_2_t2617245831 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	StringReader_t3465604688 * V_4 = NULL;
	XmlReader_t3121518892 * V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___languageDataInResX0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___languageDataInResX0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2361950774, /*hidden argument*/NULL);
		return (SortedDictionary_2_t2617245831 *)NULL;
	}

IL_0022:
	{
		SortedDictionary_2_t2617245831 * L_4 = (SortedDictionary_2_t2617245831 *)il2cpp_codegen_object_new(SortedDictionary_2_t2617245831_il2cpp_TypeInfo_var);
		SortedDictionary_2__ctor_m964777687(L_4, /*hidden argument*/SortedDictionary_2__ctor_m964777687_RuntimeMethod_var);
		V_0 = L_4;
		String_t* L_5 = ___languageDataInResX0;
		V_1 = L_5;
		String_t* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = String_IndexOf_m1977622757(L_6, _stringLiteral2596964267, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)((int32_t)13)));
		String_t* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2848979100(L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageParser_t1152791527_il2cpp_TypeInfo_var);
		String_t* L_12 = ((LanguageParser_t1152791527_StaticFields*)il2cpp_codegen_static_fields_for(LanguageParser_t1152791527_il2cpp_TypeInfo_var))->get_xmlHeader_0();
		String_t* L_13 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3937257545(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		String_t* L_15 = V_3;
		StringReader_t3465604688 * L_16 = (StringReader_t3465604688 *)il2cpp_codegen_object_new(StringReader_t3465604688_il2cpp_TypeInfo_var);
		StringReader__ctor_m126993932(L_16, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
	}

IL_0057:
	try
	{ // begin try (depth: 1)
		{
			StringReader_t3465604688 * L_17 = V_4;
			XmlReader_t3121518892 * L_18 = XmlReader_Create_m2125883682(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			V_5 = L_18;
		}

IL_0060:
		try
		{ // begin try (depth: 2)
			XmlReader_t3121518892 * L_19 = V_5;
			SortedDictionary_2_t2617245831 * L_20 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(LanguageParser_t1152791527_il2cpp_TypeInfo_var);
			LanguageParser_ReadElements_m1808351402(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x7C, FINALLY_006d);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_006d;
		}

FINALLY_006d:
		{ // begin finally (depth: 2)
			{
				XmlReader_t3121518892 * L_21 = V_5;
				if (!L_21)
				{
					goto IL_007b;
				}
			}

IL_0074:
			{
				XmlReader_t3121518892 * L_22 = V_5;
				NullCheck(L_22);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_22);
			}

IL_007b:
			{
				IL2CPP_END_FINALLY(109)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(109)
		{
			IL2CPP_JUMP_TBL(0x7C, IL_007c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_007c:
		{
			IL2CPP_LEAVE(0x90, FINALLY_0081);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0081;
	}

FINALLY_0081:
	{ // begin finally (depth: 1)
		{
			StringReader_t3465604688 * L_23 = V_4;
			if (!L_23)
			{
				goto IL_008f;
			}
		}

IL_0088:
		{
			StringReader_t3465604688 * L_24 = V_4;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_24);
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(129)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(129)
	{
		IL2CPP_JUMP_TBL(0x90, IL_0090)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0090:
	{
		SortedDictionary_2_t2617245831 * L_25 = V_0;
		return L_25;
	}
}
// System.Void SmartLocalization.LanguageParser::ReadElements(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadElements_m1808351402 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SortedDictionary_2_t2617245831 * ___loadedLanguageDictionary1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageParser_ReadElements_m1808351402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_002d;
	}

IL_0005:
	{
		XmlReader_t3121518892 * L_0 = ___reader0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		XmlReader_t3121518892 * L_2 = ___reader0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlReader::get_Name() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral2037252866, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		XmlReader_t3121518892 * L_5 = ___reader0;
		SortedDictionary_2_t2617245831 * L_6 = ___loadedLanguageDictionary1;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageParser_t1152791527_il2cpp_TypeInfo_var);
		LanguageParser_ReadData_m1441422953(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		XmlReader_t3121518892 * L_7 = ___reader0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(40 /* System.Boolean System.Xml.XmlReader::Read() */, L_7);
		if (L_8)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void SmartLocalization.LanguageParser::ReadData(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadData_m1441422953 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SortedDictionary_2_t2617245831 * ___loadedLanguageDictionary1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageParser_ReadData_m1441422953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	LocalizedObject_t1409469237 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_1 = L_1;
		XmlReader_t3121518892 * L_2 = ___reader0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean System.Xml.XmlReader::get_HasAttributes() */, L_2);
		if (!L_3)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0038;
	}

IL_001c:
	{
		XmlReader_t3121518892 * L_4 = ___reader0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlReader::get_Name() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral62725243, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		XmlReader_t3121518892 * L_7 = ___reader0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Xml.XmlReader::get_Value() */, L_7);
		V_0 = L_8;
	}

IL_0038:
	{
		XmlReader_t3121518892 * L_9 = ___reader0;
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XmlReader::MoveToNextAttribute() */, L_9);
		if (L_10)
		{
			goto IL_001c;
		}
	}

IL_0043:
	{
		XmlReader_t3121518892 * L_11 = ___reader0;
		NullCheck(L_11);
		VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean System.Xml.XmlReader::MoveToElement() */, L_11);
		XmlReader_t3121518892 * L_12 = ___reader0;
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker1< bool, String_t* >::Invoke(45 /* System.Boolean System.Xml.XmlReader::ReadToDescendant(System.String) */, L_12, _stringLiteral3493618073);
		if (!L_13)
		{
			goto IL_0071;
		}
	}

IL_005a:
	{
		XmlReader_t3121518892 * L_14 = ___reader0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(48 /* System.String System.Xml.XmlReader::ReadElementContentAsString() */, L_14);
		V_1 = L_15;
		XmlReader_t3121518892 * L_16 = ___reader0;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlReader::ReadToNextSibling(System.String) */, L_16, _stringLiteral3493618073);
		if (L_17)
		{
			goto IL_005a;
		}
	}

IL_0071:
	{
		LocalizedObject_t1409469237 * L_18 = (LocalizedObject_t1409469237 *)il2cpp_codegen_object_new(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		LocalizedObject__ctor_m1798526539(L_18, /*hidden argument*/NULL);
		V_2 = L_18;
		LocalizedObject_t1409469237 * L_19 = V_2;
		String_t* L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		int32_t L_21 = LocalizedObject_GetLocalizedObjectType_m2843596427(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		LocalizedObject_set_ObjectType_m3384992281(L_19, L_21, /*hidden argument*/NULL);
		LocalizedObject_t1409469237 * L_22 = V_2;
		String_t* L_23 = V_1;
		NullCheck(L_22);
		LocalizedObject_set_TextValue_m13296749(L_22, L_23, /*hidden argument*/NULL);
		LocalizedObject_t1409469237 * L_24 = V_2;
		NullCheck(L_24);
		int32_t L_25 = LocalizedObject_get_ObjectType_m1776010124(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		LocalizedObject_t1409469237 * L_26 = V_2;
		NullCheck(L_26);
		String_t* L_27 = LocalizedObject_get_TextValue_m2788713862(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00d7;
		}
	}
	{
		LocalizedObject_t1409469237 * L_28 = V_2;
		NullCheck(L_28);
		String_t* L_29 = LocalizedObject_get_TextValue_m2788713862(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m1759067526(L_29, _stringLiteral550189811, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00d7;
		}
	}
	{
		LocalizedObject_t1409469237 * L_31 = V_2;
		NullCheck(L_31);
		LocalizedObject_set_OverrideLocalizedObject_m3073665204(L_31, (bool)1, /*hidden argument*/NULL);
		LocalizedObject_t1409469237 * L_32 = V_2;
		LocalizedObject_t1409469237 * L_33 = V_2;
		NullCheck(L_33);
		String_t* L_34 = LocalizedObject_get_TextValue_m2788713862(L_33, /*hidden argument*/NULL);
		NullCheck(_stringLiteral550189811);
		int32_t L_35 = String_get_Length_m3847582255(_stringLiteral550189811, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_36 = String_Substring_m2848979100(L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_32);
		LocalizedObject_set_OverrideObjectLanguageCode_m2380448063(L_32, L_36, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		SortedDictionary_2_t2617245831 * L_37 = ___loadedLanguageDictionary1;
		String_t* L_38 = V_0;
		LocalizedObject_t1409469237 * L_39 = V_2;
		NullCheck(L_39);
		int32_t L_40 = LocalizedObject_get_ObjectType_m1776010124(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_41 = LocalizedObject_GetCleanKey_m2035573102(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		LocalizedObject_t1409469237 * L_42 = V_2;
		NullCheck(L_37);
		SortedDictionary_2_Add_m1412840442(L_37, L_41, L_42, /*hidden argument*/SortedDictionary_2_Add_m1412840442_RuntimeMethod_var);
		return;
	}
}
// System.Void SmartLocalization.LanguageParser::.cctor()
extern "C"  void LanguageParser__cctor_m2820239222 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageParser__cctor_m2820239222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LanguageParser_t1152791527_StaticFields*)il2cpp_codegen_static_fields_for(LanguageParser_t1152791527_il2cpp_TypeInfo_var))->set_xmlHeader_0(_stringLiteral3614473796);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SmartLocalization.LanguageRuntimeData::LanguageFilePath(System.String)
extern "C"  String_t* LanguageRuntimeData_LanguageFilePath_m1120529596 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData_LanguageFilePath_m1120529596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->get_rootLanguageName_1();
		String_t* L_1 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, _stringLiteral3452614530, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFilePath()
extern "C"  String_t* LanguageRuntimeData_AvailableCulturesFilePath_m3409607084 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData_AvailableCulturesFilePath_m3409607084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->get_AvailableCulturesFileName_0();
		return L_0;
	}
}
// System.String SmartLocalization.LanguageRuntimeData::AudioFilesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_AudioFilesFolderPath_m4027902490 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData_AudioFilesFolderPath_m4027902490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_1 = ((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->get_AudioFileFolderName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, _stringLiteral3452614529, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_TexturesFolderPath_m4143263055 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData_TexturesFolderPath_m4143263055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_1 = ((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->get_TexturesFolderName_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, _stringLiteral3452614529, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_PrefabsFolderPath_m1749946394 (RuntimeObject * __this /* static, unused */, String_t* ___languageCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData_PrefabsFolderPath_m1749946394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___languageCode0;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_1 = ((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->get_PrefabsFolderName_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, _stringLiteral3452614529, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SmartLocalization.LanguageRuntimeData::.cctor()
extern "C"  void LanguageRuntimeData__cctor_m1578531035 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LanguageRuntimeData__cctor_m1578531035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->set_AvailableCulturesFileName_0(_stringLiteral2813389331);
		((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->set_rootLanguageName_1(_stringLiteral2143291852);
		((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->set_AudioFileFolderName_2(_stringLiteral2652779316);
		((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->set_TexturesFolderName_3(_stringLiteral2506181452);
		((LanguageRuntimeData_t3907746602_StaticFields*)il2cpp_codegen_static_fields_for(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var))->set_PrefabsFolderName_4(_stringLiteral3727959427);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LocalizedAudioSource::.ctor()
extern "C"  void LocalizedAudioSource__ctor_m4153904623 (LocalizedAudioSource_t267215401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedAudioSource__ctor_m4153904623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_localizedKey_2(_stringLiteral2911642733);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedAudioSource::Start()
extern "C"  void LocalizedAudioSource_Start_m1039871505 (LocalizedAudioSource_t267215401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedAudioSource_Start_m1039871505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LanguageManager_t2767934455 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_0 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		LanguageManager_t2767934455 * L_1 = V_0;
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedAudioSource_OnChangeLanguage_m3163652545_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
		AudioSource_t3935305588 * L_7 = Component_GetComponent_TisAudioSource_t3935305588_m286538782(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m286538782_RuntimeMethod_var);
		__this->set_audioSource_4(L_7);
		LanguageManager_t2767934455 * L_8 = V_0;
		LocalizedAudioSource_OnChangeLanguage_m3163652545(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedAudioSource::OnDestroy()
extern "C"  void LocalizedAudioSource_OnDestroy_m2449684192 (LocalizedAudioSource_t267215401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedAudioSource_OnDestroy_m2449684192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_0 = LanguageManager_get_HasInstance_m2564662803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_1 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedAudioSource_OnChangeLanguage_m3163652545_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		return;
	}
}
// System.Void SmartLocalization.LocalizedAudioSource::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedAudioSource_OnChangeLanguage_m3163652545 (LocalizedAudioSource_t267215401 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedAudioSource_OnChangeLanguage_m3163652545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LanguageManager_t2767934455 * L_0 = ___languageManager0;
		String_t* L_1 = __this->get_localizedKey_2();
		NullCheck(L_0);
		AudioClip_t3680889665 * L_2 = LanguageManager_GetAudioClip_m1838735899(L_0, L_1, /*hidden argument*/NULL);
		__this->set_audioClip_3(L_2);
		AudioSource_t3935305588 * L_3 = __this->get_audioSource_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		AudioSource_t3935305588 * L_5 = __this->get_audioSource_4();
		AudioClip_t3680889665 * L_6 = __this->get_audioClip_3();
		NullCheck(L_5);
		AudioSource_set_clip_m31653938(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LocalizedGUIText::.ctor()
extern "C"  void LocalizedGUIText__ctor_m373607288 (LocalizedGUIText_t2415330885 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUIText__ctor_m373607288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_localizedKey_2(_stringLiteral2911642733);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUIText::Start()
extern "C"  void LocalizedGUIText_Start_m3774386156 (LocalizedGUIText_t2415330885 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUIText_Start_m3774386156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LanguageManager_t2767934455 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_0 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		LanguageManager_t2767934455 * L_1 = V_0;
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedGUIText_OnChangeLanguage_m236516013_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
		LanguageManager_t2767934455 * L_7 = V_0;
		LocalizedGUIText_OnChangeLanguage_m236516013(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUIText::OnDestroy()
extern "C"  void LocalizedGUIText_OnDestroy_m2973192016 (LocalizedGUIText_t2415330885 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUIText_OnDestroy_m2973192016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_0 = LanguageManager_get_HasInstance_m2564662803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_1 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedGUIText_OnChangeLanguage_m236516013_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUIText::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUIText_OnChangeLanguage_m236516013 (LocalizedGUIText_t2415330885 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUIText_OnChangeLanguage_m236516013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t402233326 * L_0 = Component_GetComponent_TisGUIText_t402233326_m1476680223(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t402233326_m1476680223_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_1 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_localizedKey_2();
		NullCheck(L_1);
		String_t* L_3 = LanguageManager_GetTextValue_m2899262939(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIText_set_text_m2265981083(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LocalizedGUITexture::.ctor()
extern "C"  void LocalizedGUITexture__ctor_m1822977010 (LocalizedGUITexture_t2717994319 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUITexture__ctor_m1822977010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_localizedKey_2(_stringLiteral2911642733);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUITexture::Start()
extern "C"  void LocalizedGUITexture_Start_m886336810 (LocalizedGUITexture_t2717994319 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUITexture_Start_m886336810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LanguageManager_t2767934455 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_0 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		LanguageManager_t2767934455 * L_1 = V_0;
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedGUITexture_OnChangeLanguage_m794962917_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
		LanguageManager_t2767934455 * L_7 = V_0;
		LocalizedGUITexture_OnChangeLanguage_m794962917(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUITexture::OnDestroy()
extern "C"  void LocalizedGUITexture_OnDestroy_m3033520291 (LocalizedGUITexture_t2717994319 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUITexture_OnDestroy_m3033520291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		bool L_0 = LanguageManager_get_HasInstance_m2564662803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_1 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		LanguageManager_t2767934455 * L_2 = L_1;
		NullCheck(L_2);
		ChangeLanguageEventHandler_t2032193146 * L_3 = L_2->get_OnChangeLanguage_9();
		intptr_t L_4 = (intptr_t)LocalizedGUITexture_OnChangeLanguage_m794962917_RuntimeMethod_var;
		ChangeLanguageEventHandler_t2032193146 * L_5 = (ChangeLanguageEventHandler_t2032193146 *)il2cpp_codegen_object_new(ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var);
		ChangeLanguageEventHandler__ctor_m417283477(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_OnChangeLanguage_9(((ChangeLanguageEventHandler_t2032193146 *)CastclassSealed((RuntimeObject*)L_6, ChangeLanguageEventHandler_t2032193146_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		return;
	}
}
// System.Void SmartLocalization.LocalizedGUITexture::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUITexture_OnChangeLanguage_m794962917 (LocalizedGUITexture_t2717994319 * __this, LanguageManager_t2767934455 * ___languageManager0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedGUITexture_OnChangeLanguage_m794962917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUITexture_t951903601 * L_0 = Component_GetComponent_TisGUITexture_t951903601_m1070565238(__this, /*hidden argument*/Component_GetComponent_TisGUITexture_t951903601_m1070565238_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LanguageManager_t2767934455_il2cpp_TypeInfo_var);
		LanguageManager_t2767934455 * L_1 = LanguageManager_get_Instance_m861325331(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_localizedKey_2();
		NullCheck(L_1);
		Texture_t3661962703 * L_3 = LanguageManager_GetTexture_m1160349625(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUITexture_set_texture_m1693041041(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.LocalizedObject::.ctor()
extern "C"  void LocalizedObject__ctor_m1798526539 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::get_ObjectType()
extern "C"  int32_t LocalizedObject_get_ObjectType_m1776010124 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_objectType_2();
		return L_0;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_ObjectType(SmartLocalization.LocalizedObjectType)
extern "C"  void LocalizedObject_set_ObjectType_m3384992281 (LocalizedObject_t1409469237 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_objectType_2(L_0);
		return;
	}
}
// System.String SmartLocalization.LocalizedObject::get_TextValue()
extern "C"  String_t* LocalizedObject_get_TextValue_m2788713862 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_textValue_3();
		return L_0;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_TextValue(System.String)
extern "C"  void LocalizedObject_set_TextValue_m13296749 (LocalizedObject_t1409469237 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_textValue_3(L_0);
		return;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_ThisGameObject(UnityEngine.GameObject)
extern "C"  void LocalizedObject_set_ThisGameObject_m1674655102 (LocalizedObject_t1409469237 * __this, GameObject_t1113636619 * ___value0, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = ___value0;
		__this->set_thisGameObject_4(L_0);
		return;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_ThisAudioClip(UnityEngine.AudioClip)
extern "C"  void LocalizedObject_set_ThisAudioClip_m2196333741 (LocalizedObject_t1409469237 * __this, AudioClip_t3680889665 * ___value0, const RuntimeMethod* method)
{
	{
		AudioClip_t3680889665 * L_0 = ___value0;
		__this->set_thisAudioClip_5(L_0);
		return;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_ThisTexture(UnityEngine.Texture)
extern "C"  void LocalizedObject_set_ThisTexture_m1264717189 (LocalizedObject_t1409469237 * __this, Texture_t3661962703 * ___value0, const RuntimeMethod* method)
{
	{
		Texture_t3661962703 * L_0 = ___value0;
		__this->set_thisTexture_6(L_0);
		return;
	}
}
// System.Boolean SmartLocalization.LocalizedObject::get_OverrideLocalizedObject()
extern "C"  bool LocalizedObject_get_OverrideLocalizedObject_m2345827120 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_overrideLocalizedObject_8();
		return L_0;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_OverrideLocalizedObject(System.Boolean)
extern "C"  void LocalizedObject_set_OverrideLocalizedObject_m3073665204 (LocalizedObject_t1409469237 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_overrideLocalizedObject_8(L_0);
		bool L_1 = __this->get_overrideLocalizedObject_8();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_overrideObjectLanguageCode_9((String_t*)NULL);
		goto IL_003a;
	}

IL_001e:
	{
		LocalizedObject_set_ThisAudioClip_m2196333741(__this, (AudioClip_t3680889665 *)NULL, /*hidden argument*/NULL);
		LocalizedObject_set_ThisTexture_m1264717189(__this, (Texture_t3661962703 *)NULL, /*hidden argument*/NULL);
		LocalizedObject_set_ThisGameObject_m1674655102(__this, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
		LocalizedObject_set_ThisTexture_m1264717189(__this, (Texture_t3661962703 *)NULL, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.String SmartLocalization.LocalizedObject::get_OverrideObjectLanguageCode()
extern "C"  String_t* LocalizedObject_get_OverrideObjectLanguageCode_m2176233279 (LocalizedObject_t1409469237 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_overrideObjectLanguageCode_9();
		return L_0;
	}
}
// System.Void SmartLocalization.LocalizedObject::set_OverrideObjectLanguageCode(System.String)
extern "C"  void LocalizedObject_set_OverrideObjectLanguageCode_m2380448063 (LocalizedObject_t1409469237 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedObject_set_OverrideObjectLanguageCode_m2380448063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		__this->set_overrideObjectLanguageCode_9(L_0);
		bool L_1 = __this->get_overrideLocalizedObject_8();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_2 = __this->get_overrideObjectLanguageCode_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral550189811, L_2, /*hidden argument*/NULL);
		__this->set_textValue_3(L_3);
	}

IL_0028:
	{
		return;
	}
}
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::GetLocalizedObjectType(System.String)
extern "C"  int32_t LocalizedObject_GetLocalizedObjectType_m2843596427 (RuntimeObject * __this /* static, unused */, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedObject_GetLocalizedObjectType_m2843596427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_1 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_keyTypeIdentifier_0();
		NullCheck(L_0);
		bool L_2 = String_StartsWith_m1759067526(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_008a;
		}
	}
	{
		String_t* L_3 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_4 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_keyTypeIdentifier_0();
		String_t* L_5 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_endBracket_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, L_4, _stringLiteral3577837154, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_7 = String_StartsWith_m1759067526(L_3, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0031;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0031:
	{
		String_t* L_8 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_9 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_keyTypeIdentifier_0();
		String_t* L_10 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_endBracket_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3755062657(NULL /*static, unused*/, L_9, _stringLiteral1494336486, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_12 = String_StartsWith_m1759067526(L_8, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0052:
	{
		String_t* L_13 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_14 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_keyTypeIdentifier_0();
		String_t* L_15 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_endBracket_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3755062657(NULL /*static, unused*/, L_14, _stringLiteral3888186860, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_17 = String_StartsWith_m1759067526(L_13, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0073;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0073:
	{
		String_t* L_18 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral605123219, L_18, _stringLiteral2226978419, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return (int32_t)(0);
	}

IL_008a:
	{
		return (int32_t)(0);
	}
}
// System.String SmartLocalization.LocalizedObject::GetCleanKey(System.String,SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetCleanKey_m2035573102 (RuntimeObject * __this /* static, unused */, String_t* ___key0, int32_t ___objectType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedObject_GetCleanKey_m2035573102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LocalizedObject_t1409469237_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_keyTypeIdentifier_0();
		int32_t L_1 = ___objectType1;
		String_t* L_2 = LocalizedObject_GetLocalizedObjectTypeStringValue_m652933876(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->get_endBracket_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = ___objectType1;
		if (L_6)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_7 = ___key0;
		return L_7;
	}

IL_0023:
	{
		int32_t L_8 = ___objectType1;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_9 = ___objectType1;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_10 = ___objectType1;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0040;
		}
	}

IL_0038:
	{
		String_t* L_11 = ___key0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m2848979100(L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		int32_t L_14 = ___objectType1;
		int32_t L_15 = ((int32_t)L_14);
		RuntimeObject * L_16 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1233953362, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		String_t* L_18 = ___key0;
		return L_18;
	}
}
// System.String SmartLocalization.LocalizedObject::GetLocalizedObjectTypeStringValue(SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetLocalizedObjectTypeStringValue_m652933876 (RuntimeObject * __this /* static, unused */, int32_t ___objectType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedObject_GetLocalizedObjectTypeStringValue_m652933876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___objectType0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_001b;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0033;
	}

IL_001b:
	{
		return _stringLiteral3577837154;
	}

IL_0021:
	{
		return _stringLiteral1494336486;
	}

IL_0027:
	{
		return _stringLiteral1700742349;
	}

IL_002d:
	{
		return _stringLiteral3888186860;
	}

IL_0033:
	{
		return _stringLiteral1700742349;
	}
}
// System.Void SmartLocalization.LocalizedObject::.cctor()
extern "C"  void LocalizedObject__cctor_m1037640625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalizedObject__cctor_m1037640625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->set_keyTypeIdentifier_0(_stringLiteral2023866285);
		((LocalizedObject_t1409469237_StaticFields*)il2cpp_codegen_static_fields_for(LocalizedObject_t1409469237_il2cpp_TypeInfo_var))->set_endBracket_1(_stringLiteral3452614643);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.RuntimeLocalizedAssetLoader::.ctor()
extern "C"  void RuntimeLocalizedAssetLoader__ctor_m3777454129 (RuntimeLocalizedAssetLoader_t316402138 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SmartLocalization.RuntimeLocalizedAssetLoader::GetAssetFolderPath(System.Type,System.String)
extern "C"  String_t* RuntimeLocalizedAssetLoader_GetAssetFolderPath_m2028859358 (RuntimeLocalizedAssetLoader_t316402138 * __this, Type_t * ___assetType0, String_t* ___languageCode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeLocalizedAssetLoader_GetAssetFolderPath_m2028859358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___assetType0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var);
		Type_t * L_1 = ((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->get_GameObjectType_0();
		if ((!(((RuntimeObject*)(Type_t *)L_0) == ((RuntimeObject*)(Type_t *)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___languageCode1;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_3 = LanguageRuntimeData_PrefabsFolderPath_m1749946394(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Type_t * L_4 = ___assetType0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var);
		Type_t * L_5 = ((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->get_AudioClipType_1();
		if ((!(((RuntimeObject*)(Type_t *)L_4) == ((RuntimeObject*)(Type_t *)L_5))))
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_6 = ___languageCode1;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_7 = LanguageRuntimeData_AudioFilesFolderPath_m4027902490(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0024:
	{
		Type_t * L_8 = ___assetType0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var);
		Type_t * L_9 = ((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->get_TextureType_2();
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_10 = ___languageCode1;
		IL2CPP_RUNTIME_CLASS_INIT(LanguageRuntimeData_t3907746602_il2cpp_TypeInfo_var);
		String_t* L_11 = LanguageRuntimeData_TexturesFolderPath_m4143263055(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_12;
	}
}
// System.Void SmartLocalization.RuntimeLocalizedAssetLoader::.cctor()
extern "C"  void RuntimeLocalizedAssetLoader__cctor_m930639038 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeLocalizedAssetLoader__cctor_m930639038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (GameObject_t1113636619_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->set_GameObjectType_0(L_1);
		RuntimeTypeHandle_t3027515415  L_2 = { reinterpret_cast<intptr_t> (AudioClip_t3680889665_0_0_0_var) };
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->set_AudioClipType_1(L_3);
		RuntimeTypeHandle_t3027515415  L_4 = { reinterpret_cast<intptr_t> (Texture_t3661962703_0_0_0_var) };
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		((RuntimeLocalizedAssetLoader_t316402138_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeLocalizedAssetLoader_t316402138_il2cpp_TypeInfo_var))->set_TextureType_2(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.SmartCultureInfo::.ctor()
extern "C"  void SmartCultureInfo__ctor_m3046517418 (SmartCultureInfo_t2060094783 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.SmartCultureInfo::.ctor(System.String,System.String,System.String,System.Boolean)
extern "C"  void SmartCultureInfo__ctor_m2022110236 (SmartCultureInfo_t2060094783 * __this, String_t* ___languageCode0, String_t* ___englishName1, String_t* ___nativeName2, bool ___isRightToLeft3, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___languageCode0;
		__this->set_languageCode_0(L_0);
		String_t* L_1 = ___englishName1;
		__this->set_englishName_1(L_1);
		String_t* L_2 = ___nativeName2;
		__this->set_nativeName_2(L_2);
		bool L_3 = ___isRightToLeft3;
		__this->set_isRightToLeft_3(L_3);
		return;
	}
}
// System.String SmartLocalization.SmartCultureInfo::ToString()
extern "C"  String_t* SmartCultureInfo_ToString_m3185995941 (SmartCultureInfo_t2060094783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfo_ToString_m3185995941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_1 = __this->get_languageCode_0();
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_1);
		ObjectU5BU5D_t2843939325* L_2 = L_0;
		String_t* L_3 = __this->get_englishName_1();
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_2;
		String_t* L_5 = __this->get_nativeName_2();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_4;
		bool* L_7 = __this->get_address_of_isRightToLeft_3();
		String_t* L_8 = Boolean_ToString_m2664721875(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral1567056791, L_6, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmartLocalization.SmartCultureInfoCollection::.ctor()
extern "C"  void SmartCultureInfoCollection__ctor_m2569938504 (SmartCultureInfoCollection_t1809038765 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfoCollection__ctor_m2569938504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3532169525 * L_0 = (List_1_t3532169525 *)il2cpp_codegen_object_new(List_1_t3532169525_il2cpp_TypeInfo_var);
		List_1__ctor_m3177292912(L_0, /*hidden argument*/List_1__ctor_m3177292912_RuntimeMethod_var);
		__this->set_cultureInfos_2(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmartLocalization.SmartCultureInfoCollection::AddCultureInfo(SmartLocalization.SmartCultureInfo)
extern "C"  void SmartCultureInfoCollection_AddCultureInfo_m3890672140 (SmartCultureInfoCollection_t1809038765 * __this, SmartCultureInfo_t2060094783 * ___cultureInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfoCollection_AddCultureInfo_m3890672140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SmartCultureInfo_t2060094783 * L_0 = ___cultureInfo0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2057740977, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		List_1_t3532169525 * L_1 = __this->get_cultureInfos_2();
		SmartCultureInfo_t2060094783 * L_2 = ___cultureInfo0;
		NullCheck(L_1);
		List_1_Add_m3606668418(L_1, L_2, /*hidden argument*/List_1_Add_m3606668418_RuntimeMethod_var);
		return;
	}
}
// SmartLocalization.SmartCultureInfoCollection SmartLocalization.SmartCultureInfoCollection::Deserialize(UnityEngine.TextAsset)
extern "C"  SmartCultureInfoCollection_t1809038765 * SmartCultureInfoCollection_Deserialize_m2953505266 (RuntimeObject * __this /* static, unused */, TextAsset_t3022178571 * ___xmlFile0, const RuntimeMethod* method)
{
	{
		TextAsset_t3022178571 * L_0 = ___xmlFile0;
		SmartCultureInfoCollection_t1809038765 * L_1 = SmartCultureInfoCollectionDeserializer_Deserialize_m3273799526(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// SmartLocalization.SmartCultureInfoCollection SmartLocalization.SmartCultureInfoCollectionDeserializer::Deserialize(UnityEngine.TextAsset)
extern "C"  SmartCultureInfoCollection_t1809038765 * SmartCultureInfoCollectionDeserializer_Deserialize_m3273799526 (RuntimeObject * __this /* static, unused */, TextAsset_t3022178571 * ___xmlFile0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfoCollectionDeserializer_Deserialize_m3273799526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SmartCultureInfoCollection_t1809038765 * V_0 = NULL;
	StringReader_t3465604688 * V_1 = NULL;
	XmlReader_t3121518892 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TextAsset_t3022178571 * L_0 = ___xmlFile0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (SmartCultureInfoCollection_t1809038765 *)NULL;
	}

IL_000e:
	{
		SmartCultureInfoCollection_t1809038765 * L_2 = (SmartCultureInfoCollection_t1809038765 *)il2cpp_codegen_object_new(SmartCultureInfoCollection_t1809038765_il2cpp_TypeInfo_var);
		SmartCultureInfoCollection__ctor_m2569938504(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		TextAsset_t3022178571 * L_3 = ___xmlFile0;
		NullCheck(L_3);
		String_t* L_4 = TextAsset_get_text_m2027878391(L_3, /*hidden argument*/NULL);
		StringReader_t3465604688 * L_5 = (StringReader_t3465604688 *)il2cpp_codegen_object_new(StringReader_t3465604688_il2cpp_TypeInfo_var);
		StringReader__ctor_m126993932(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			StringReader_t3465604688 * L_6 = V_1;
			XmlReader_t3121518892 * L_7 = XmlReader_Create_m2125883682(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
		}

IL_0027:
		try
		{ // begin try (depth: 2)
			XmlReader_t3121518892 * L_8 = V_2;
			SmartCultureInfoCollection_t1809038765 * L_9 = V_0;
			SmartCultureInfoCollectionDeserializer_ReadElements_m2123939648(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x40, FINALLY_0033);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0033;
		}

FINALLY_0033:
		{ // begin finally (depth: 2)
			{
				XmlReader_t3121518892 * L_10 = V_2;
				if (!L_10)
				{
					goto IL_003f;
				}
			}

IL_0039:
			{
				XmlReader_t3121518892 * L_11 = V_2;
				NullCheck(L_11);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_11);
			}

IL_003f:
			{
				IL2CPP_END_FINALLY(51)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(51)
		{
			IL2CPP_JUMP_TBL(0x40, IL_0040)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			StringReader_t3465604688 * L_12 = V_1;
			if (!L_12)
			{
				goto IL_0051;
			}
		}

IL_004b:
		{
			StringReader_t3465604688 * L_13 = V_1;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_13);
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0052:
	{
		SmartCultureInfoCollection_t1809038765 * L_14 = V_0;
		return L_14;
	}
}
// System.Void SmartLocalization.SmartCultureInfoCollectionDeserializer::ReadElements(System.Xml.XmlReader,SmartLocalization.SmartCultureInfoCollection)
extern "C"  void SmartCultureInfoCollectionDeserializer_ReadElements_m2123939648 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SmartCultureInfoCollection_t1809038765 * ___newCollection1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfoCollectionDeserializer_ReadElements_m2123939648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_002d;
	}

IL_0005:
	{
		XmlReader_t3121518892 * L_0 = ___reader0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		XmlReader_t3121518892 * L_2 = ___reader0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlReader::get_Name() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral889386244, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		XmlReader_t3121518892 * L_5 = ___reader0;
		SmartCultureInfoCollection_t1809038765 * L_6 = ___newCollection1;
		SmartCultureInfoCollectionDeserializer_ReadData_m1923819025(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		XmlReader_t3121518892 * L_7 = ___reader0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(40 /* System.Boolean System.Xml.XmlReader::Read() */, L_7);
		if (L_8)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void SmartLocalization.SmartCultureInfoCollectionDeserializer::ReadData(System.Xml.XmlReader,SmartLocalization.SmartCultureInfoCollection)
extern "C"  void SmartCultureInfoCollectionDeserializer_ReadData_m1923819025 (RuntimeObject * __this /* static, unused */, XmlReader_t3121518892 * ___reader0, SmartCultureInfoCollection_t1809038765 * ___newCollection1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartCultureInfoCollectionDeserializer_ReadData_m1923819025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_1 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_2 = L_2;
		V_3 = (bool)0;
		XmlReader_t3121518892 * L_3 = ___reader0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker1< bool, String_t* >::Invoke(45 /* System.Boolean System.Xml.XmlReader::ReadToDescendant(System.String) */, L_3, _stringLiteral335559975);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		XmlReader_t3121518892 * L_5 = ___reader0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(48 /* System.String System.Xml.XmlReader::ReadElementContentAsString() */, L_5);
		V_0 = L_6;
	}

IL_002b:
	{
		XmlReader_t3121518892 * L_7 = ___reader0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlReader::ReadToNextSibling(System.String) */, L_7, _stringLiteral333543461);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		XmlReader_t3121518892 * L_9 = ___reader0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(48 /* System.String System.Xml.XmlReader::ReadElementContentAsString() */, L_9);
		V_1 = L_10;
	}

IL_0042:
	{
		XmlReader_t3121518892 * L_11 = ___reader0;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlReader::ReadToNextSibling(System.String) */, L_11, _stringLiteral508791134);
		if (!L_12)
		{
			goto IL_0059;
		}
	}
	{
		XmlReader_t3121518892 * L_13 = ___reader0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(48 /* System.String System.Xml.XmlReader::ReadElementContentAsString() */, L_13);
		V_2 = L_14;
	}

IL_0059:
	{
		XmlReader_t3121518892 * L_15 = ___reader0;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlReader::ReadToNextSibling(System.String) */, L_15, _stringLiteral3671028101);
		if (!L_16)
		{
			goto IL_0070;
		}
	}
	{
		XmlReader_t3121518892 * L_17 = ___reader0;
		NullCheck(L_17);
		bool L_18 = VirtFuncInvoker0< bool >::Invoke(47 /* System.Boolean System.Xml.XmlReader::ReadElementContentAsBoolean() */, L_17);
		V_3 = L_18;
	}

IL_0070:
	{
		SmartCultureInfoCollection_t1809038765 * L_19 = ___newCollection1;
		String_t* L_20 = V_0;
		String_t* L_21 = V_1;
		String_t* L_22 = V_2;
		bool L_23 = V_3;
		SmartCultureInfo_t2060094783 * L_24 = (SmartCultureInfo_t2060094783 *)il2cpp_codegen_object_new(SmartCultureInfo_t2060094783_il2cpp_TypeInfo_var);
		SmartCultureInfo__ctor_m2022110236(L_24, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		SmartCultureInfoCollection_AddCultureInfo_m3890672140(L_19, L_24, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
