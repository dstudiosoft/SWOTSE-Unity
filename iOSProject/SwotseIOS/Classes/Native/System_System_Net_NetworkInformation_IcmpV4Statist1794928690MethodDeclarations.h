﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IcmpV4Statistics
struct IcmpV4Statistics_t1794928690;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IcmpV4Statistics::.ctor()
extern "C"  void IcmpV4Statistics__ctor_m695427374 (IcmpV4Statistics_t1794928690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
