﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// BarbarianRecruitManager
struct BarbarianRecruitManager_t1320026565;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6
struct  U3CGetBarbariansArmyU3Ec__Iterator6_t634682975  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// JSONObject BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::<obj>__1
	JSONObject_t1971882247 * ___U3CobjU3E__1_1;
	// System.Int32 BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::$current
	Il2CppObject * ___U24current_3;
	// BarbarianRecruitManager BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::<>f__this
	BarbarianRecruitManager_t1320026565 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator6_t634682975, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator6_t634682975, ___U3CobjU3E__1_1)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__1_1() const { return ___U3CobjU3E__1_1; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__1_1() { return &___U3CobjU3E__1_1; }
	inline void set_U3CobjU3E__1_1(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator6_t634682975, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator6_t634682975, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator6_t634682975, ___U3CU3Ef__this_4)); }
	inline BarbarianRecruitManager_t1320026565 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline BarbarianRecruitManager_t1320026565 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(BarbarianRecruitManager_t1320026565 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
