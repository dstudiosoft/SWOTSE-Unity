﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GuestHouseTableLine
struct GuestHouseTableLine_t1665954750;
// GuestFeastingTableController
struct GuestFeastingTableController_t60500861;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GuestFeastingTableController60500861.h"
#include "AssemblyU2DCSharp_ArmyGeneralModel609759248.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GuestHouseTableLine::.ctor()
extern "C"  void GuestHouseTableLine__ctor_m727457463 (GuestHouseTableLine_t1665954750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetOwner(GuestFeastingTableController)
extern "C"  void GuestHouseTableLine_SetOwner_m2993963095 (GuestHouseTableLine_t1665954750 * __this, GuestFeastingTableController_t60500861 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetGeneralModel(ArmyGeneralModel)
extern "C"  void GuestHouseTableLine_SetGeneralModel_m3171241104 (GuestHouseTableLine_t1665954750 * __this, ArmyGeneralModel_t609759248 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetKnightName(System.String)
extern "C"  void GuestHouseTableLine_SetKnightName_m970272559 (GuestHouseTableLine_t1665954750 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetPolitics(System.Int64)
extern "C"  void GuestHouseTableLine_SetPolitics_m3998369070 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetMarches(System.Int64)
extern "C"  void GuestHouseTableLine_SetMarches_m1305542754 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetSpeed(System.Int64)
extern "C"  void GuestHouseTableLine_SetSpeed_m286892810 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetLoyalty(System.Int64)
extern "C"  void GuestHouseTableLine_SetLoyalty_m1331815765 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetLevel(System.Int64)
extern "C"  void GuestHouseTableLine_SetLevel_m593106605 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetSalary(System.Int64)
extern "C"  void GuestHouseTableLine_SetSalary_m3890402851 (GuestHouseTableLine_t1665954750 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetStatus(System.String)
extern "C"  void GuestHouseTableLine_SetStatus_m3921866851 (GuestHouseTableLine_t1665954750 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetActionText(System.String)
extern "C"  void GuestHouseTableLine_SetActionText_m4097517514 (GuestHouseTableLine_t1665954750 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::RecruitDismissKnight()
extern "C"  void GuestHouseTableLine_RecruitDismissKnight_m4283080810 (GuestHouseTableLine_t1665954750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::SetPressable(System.Boolean)
extern "C"  void GuestHouseTableLine_SetPressable_m3519830059 (GuestHouseTableLine_t1665954750 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestHouseTableLine::OpenGeneralInfo()
extern "C"  void GuestHouseTableLine_OpenGeneralInfo_m2324644903 (GuestHouseTableLine_t1665954750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
