﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Ping/IcmpMessage
struct IcmpMessage_t482998463;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_IPStatus1105460603.h"

// System.Void System.Net.NetworkInformation.Ping/IcmpMessage::.ctor(System.Byte[],System.Int32,System.Int32)
extern "C"  void IcmpMessage__ctor_m621024584 (IcmpMessage_t482998463 * __this, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping/IcmpMessage::.ctor(System.Byte,System.Byte,System.Int16,System.Int16,System.Byte[])
extern "C"  void IcmpMessage__ctor_m1671930572 (IcmpMessage_t482998463 * __this, uint8_t ___type0, uint8_t ___code1, int16_t ___identifier2, int16_t ___sequence3, ByteU5BU5D_t3397334013* ___data4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Net.NetworkInformation.Ping/IcmpMessage::get_Type()
extern "C"  uint8_t IcmpMessage_get_Type_m4132751270 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Net.NetworkInformation.Ping/IcmpMessage::get_Code()
extern "C"  uint8_t IcmpMessage_get_Code_m969897705 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Net.NetworkInformation.Ping/IcmpMessage::get_Identifier()
extern "C"  uint8_t IcmpMessage_get_Identifier_m2325173215 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Net.NetworkInformation.Ping/IcmpMessage::get_Sequence()
extern "C"  uint8_t IcmpMessage_get_Sequence_m369625479 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.NetworkInformation.Ping/IcmpMessage::get_Data()
extern "C"  ByteU5BU5D_t3397334013* IcmpMessage_get_Data_m672939050 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.NetworkInformation.Ping/IcmpMessage::GetBytes()
extern "C"  ByteU5BU5D_t3397334013* IcmpMessage_GetBytes_m3013486824 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Net.NetworkInformation.Ping/IcmpMessage::ComputeChecksum(System.Byte[])
extern "C"  uint16_t IcmpMessage_ComputeChecksum_m565187029 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPStatus System.Net.NetworkInformation.Ping/IcmpMessage::get_IPStatus()
extern "C"  int32_t IcmpMessage_get_IPStatus_m2686790847 (IcmpMessage_t482998463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
