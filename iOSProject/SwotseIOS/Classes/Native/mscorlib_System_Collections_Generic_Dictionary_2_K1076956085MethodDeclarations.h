﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct KeyCollection_t1076956085;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1282961752.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m842306302_gshared (KeyCollection_t1076956085 * __this, Dictionary_2_t2888425610 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m842306302(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1076956085 *, Dictionary_2_t2888425610 *, const MethodInfo*))KeyCollection__ctor_m842306302_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3907074996_gshared (KeyCollection_t1076956085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3907074996(__this, ___item0, method) ((  void (*) (KeyCollection_t1076956085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3907074996_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2765266779_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2765266779(__this, method) ((  void (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2765266779_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3208381696_gshared (KeyCollection_t1076956085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3208381696(__this, ___item0, method) ((  bool (*) (KeyCollection_t1076956085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3208381696_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2555335107_gshared (KeyCollection_t1076956085 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2555335107(__this, ___item0, method) ((  bool (*) (KeyCollection_t1076956085 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2555335107_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1642380961_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1642380961(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1642380961_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m293891217_gshared (KeyCollection_t1076956085 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m293891217(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1076956085 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m293891217_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2970364720_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2970364720(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2970364720_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m955917807_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m955917807(__this, method) ((  bool (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m955917807_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1359816365_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1359816365(__this, method) ((  bool (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1359816365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1045288725_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1045288725(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1045288725_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m989455923_gshared (KeyCollection_t1076956085 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m989455923(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1076956085 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m989455923_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::GetEnumerator()
extern "C"  Enumerator_t1282961752  KeyCollection_GetEnumerator_m1866178416_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1866178416(__this, method) ((  Enumerator_t1282961752  (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_GetEnumerator_m1866178416_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3456875241_gshared (KeyCollection_t1076956085 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3456875241(__this, method) ((  int32_t (*) (KeyCollection_t1076956085 *, const MethodInfo*))KeyCollection_get_Count_m3456875241_gshared)(__this, method)
