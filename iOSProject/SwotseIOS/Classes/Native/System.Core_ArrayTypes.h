﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Action
struct Action_t3226471752;
// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>
struct Action_2_t1212770125;

#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li116960033.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_System_Action_2_gen1212770125.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3564775402.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li204904209.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li247561424.h"

#pragma once
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3597933550  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t865133271  m_Items[1];

public:
	inline Link_t865133271  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t865133271 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t865133271  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.IClippable>[]
struct LinkU5BU5D_t222250812  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t116960033  m_Items[1];

public:
	inline Link_t116960033  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t116960033 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t116960033  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t87223449  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3226471752 * m_Items[1];

public:
	inline Action_t3226471752 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3226471752 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>[]
struct Action_2U5BU5D_t2539395296  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_2_t1212770125 * m_Items[1];

public:
	inline Action_2_t1212770125 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_2_t1212770125 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_2_t1212770125 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<GoogleMobileAds.Api.NativeAdType>[]
struct LinkU5BU5D_t2756065583  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3564775402  m_Items[1];

public:
	inline Link_t3564775402  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t3564775402 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t3564775402  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct LinkU5BU5D_t1625685388  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t204904209  m_Items[1];

public:
	inline Link_t204904209  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t204904209 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t204904209  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Int32>[]
struct LinkU5BU5D_t3013699057  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t247561424  m_Items[1];

public:
	inline Link_t247561424  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t247561424 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t247561424  value)
	{
		m_Items[index] = value;
	}
};
