﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketBuyTableLine
struct MarketBuyTableLine_t1445592330;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketBuyTableLine::.ctor()
extern "C"  void MarketBuyTableLine__ctor_m1696359169 (MarketBuyTableLine_t1445592330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketBuyTableLine::SetQuantity(System.Int64)
extern "C"  void MarketBuyTableLine_SetQuantity_m383398048 (MarketBuyTableLine_t1445592330 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketBuyTableLine::SetUnitPrice(System.Int64)
extern "C"  void MarketBuyTableLine_SetUnitPrice_m1437919794 (MarketBuyTableLine_t1445592330 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
