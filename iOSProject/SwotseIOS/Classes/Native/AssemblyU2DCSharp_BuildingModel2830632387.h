﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingModel
struct  BuildingModel_t2830632387  : public Il2CppObject
{
public:
	// System.Int64 BuildingModel::id
	int64_t ___id_0;
	// System.String BuildingModel::building
	String_t* ___building_1;
	// System.Int64 BuildingModel::level
	int64_t ___level_2;
	// System.Int64 BuildingModel::pit_id
	int64_t ___pit_id_3;
	// System.Int64 BuildingModel::city
	int64_t ___city_4;
	// System.String BuildingModel::status
	String_t* ___status_5;
	// System.Int64 BuildingModel::population
	int64_t ___population_6;
	// System.String BuildingModel::location
	String_t* ___location_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_building_1() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___building_1)); }
	inline String_t* get_building_1() const { return ___building_1; }
	inline String_t** get_address_of_building_1() { return &___building_1; }
	inline void set_building_1(String_t* value)
	{
		___building_1 = value;
		Il2CppCodeGenWriteBarrier(&___building_1, value);
	}

	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___level_2)); }
	inline int64_t get_level_2() const { return ___level_2; }
	inline int64_t* get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(int64_t value)
	{
		___level_2 = value;
	}

	inline static int32_t get_offset_of_pit_id_3() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___pit_id_3)); }
	inline int64_t get_pit_id_3() const { return ___pit_id_3; }
	inline int64_t* get_address_of_pit_id_3() { return &___pit_id_3; }
	inline void set_pit_id_3(int64_t value)
	{
		___pit_id_3 = value;
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___city_4)); }
	inline int64_t get_city_4() const { return ___city_4; }
	inline int64_t* get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(int64_t value)
	{
		___city_4 = value;
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___status_5)); }
	inline String_t* get_status_5() const { return ___status_5; }
	inline String_t** get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(String_t* value)
	{
		___status_5 = value;
		Il2CppCodeGenWriteBarrier(&___status_5, value);
	}

	inline static int32_t get_offset_of_population_6() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___population_6)); }
	inline int64_t get_population_6() const { return ___population_6; }
	inline int64_t* get_address_of_population_6() { return &___population_6; }
	inline void set_population_6(int64_t value)
	{
		___population_6 = value;
	}

	inline static int32_t get_offset_of_location_7() { return static_cast<int32_t>(offsetof(BuildingModel_t2830632387, ___location_7)); }
	inline String_t* get_location_7() const { return ___location_7; }
	inline String_t** get_address_of_location_7() { return &___location_7; }
	inline void set_location_7(String_t* value)
	{
		___location_7 = value;
		Il2CppCodeGenWriteBarrier(&___location_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
