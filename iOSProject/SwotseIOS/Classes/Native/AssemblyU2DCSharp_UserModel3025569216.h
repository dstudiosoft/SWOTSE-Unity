﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AllianceModel
struct AllianceModel_t1834891198;
// MyCityModel
struct MyCityModel_t1736786178;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserModel
struct  UserModel_t3025569216  : public Il2CppObject
{
public:
	// System.Int64 UserModel::id
	int64_t ___id_0;
	// System.String UserModel::username
	String_t* ___username_1;
	// System.String UserModel::status
	String_t* ___status_2;
	// System.String UserModel::image_name
	String_t* ___image_name_3;
	// System.String UserModel::rank
	String_t* ___rank_4;
	// System.Int64 UserModel::emperor_id
	int64_t ___emperor_id_5;
	// System.String UserModel::email
	String_t* ___email_6;
	// System.Int64 UserModel::experience
	int64_t ___experience_7;
	// System.String UserModel::alliance_rank
	String_t* ___alliance_rank_8;
	// System.DateTime UserModel::last_login
	DateTime_t693205669  ___last_login_9;
	// System.Int64 UserModel::city_count
	int64_t ___city_count_10;
	// AllianceModel UserModel::alliance
	AllianceModel_t1834891198 * ___alliance_11;
	// MyCityModel UserModel::capital
	MyCityModel_t1736786178 * ___capital_12;
	// System.Int64 UserModel::shillings
	int64_t ___shillings_13;
	// System.String UserModel::language
	String_t* ___language_14;
	// System.String UserModel::flag
	String_t* ___flag_15;
	// System.Int64 UserModel::players_wins
	int64_t ___players_wins_16;
	// System.Int64 UserModel::players_losses
	int64_t ___players_losses_17;
	// System.Int64 UserModel::barbarians_wins
	int64_t ___barbarians_wins_18;
	// System.Int64 UserModel::barbarians_losses
	int64_t ___barbarians_losses_19;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier(&___username_1, value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier(&___status_2, value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier(&___image_name_3, value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___rank_4)); }
	inline String_t* get_rank_4() const { return ___rank_4; }
	inline String_t** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(String_t* value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier(&___rank_4, value);
	}

	inline static int32_t get_offset_of_emperor_id_5() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___emperor_id_5)); }
	inline int64_t get_emperor_id_5() const { return ___emperor_id_5; }
	inline int64_t* get_address_of_emperor_id_5() { return &___emperor_id_5; }
	inline void set_emperor_id_5(int64_t value)
	{
		___emperor_id_5 = value;
	}

	inline static int32_t get_offset_of_email_6() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___email_6)); }
	inline String_t* get_email_6() const { return ___email_6; }
	inline String_t** get_address_of_email_6() { return &___email_6; }
	inline void set_email_6(String_t* value)
	{
		___email_6 = value;
		Il2CppCodeGenWriteBarrier(&___email_6, value);
	}

	inline static int32_t get_offset_of_experience_7() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___experience_7)); }
	inline int64_t get_experience_7() const { return ___experience_7; }
	inline int64_t* get_address_of_experience_7() { return &___experience_7; }
	inline void set_experience_7(int64_t value)
	{
		___experience_7 = value;
	}

	inline static int32_t get_offset_of_alliance_rank_8() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___alliance_rank_8)); }
	inline String_t* get_alliance_rank_8() const { return ___alliance_rank_8; }
	inline String_t** get_address_of_alliance_rank_8() { return &___alliance_rank_8; }
	inline void set_alliance_rank_8(String_t* value)
	{
		___alliance_rank_8 = value;
		Il2CppCodeGenWriteBarrier(&___alliance_rank_8, value);
	}

	inline static int32_t get_offset_of_last_login_9() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___last_login_9)); }
	inline DateTime_t693205669  get_last_login_9() const { return ___last_login_9; }
	inline DateTime_t693205669 * get_address_of_last_login_9() { return &___last_login_9; }
	inline void set_last_login_9(DateTime_t693205669  value)
	{
		___last_login_9 = value;
	}

	inline static int32_t get_offset_of_city_count_10() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___city_count_10)); }
	inline int64_t get_city_count_10() const { return ___city_count_10; }
	inline int64_t* get_address_of_city_count_10() { return &___city_count_10; }
	inline void set_city_count_10(int64_t value)
	{
		___city_count_10 = value;
	}

	inline static int32_t get_offset_of_alliance_11() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___alliance_11)); }
	inline AllianceModel_t1834891198 * get_alliance_11() const { return ___alliance_11; }
	inline AllianceModel_t1834891198 ** get_address_of_alliance_11() { return &___alliance_11; }
	inline void set_alliance_11(AllianceModel_t1834891198 * value)
	{
		___alliance_11 = value;
		Il2CppCodeGenWriteBarrier(&___alliance_11, value);
	}

	inline static int32_t get_offset_of_capital_12() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___capital_12)); }
	inline MyCityModel_t1736786178 * get_capital_12() const { return ___capital_12; }
	inline MyCityModel_t1736786178 ** get_address_of_capital_12() { return &___capital_12; }
	inline void set_capital_12(MyCityModel_t1736786178 * value)
	{
		___capital_12 = value;
		Il2CppCodeGenWriteBarrier(&___capital_12, value);
	}

	inline static int32_t get_offset_of_shillings_13() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___shillings_13)); }
	inline int64_t get_shillings_13() const { return ___shillings_13; }
	inline int64_t* get_address_of_shillings_13() { return &___shillings_13; }
	inline void set_shillings_13(int64_t value)
	{
		___shillings_13 = value;
	}

	inline static int32_t get_offset_of_language_14() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___language_14)); }
	inline String_t* get_language_14() const { return ___language_14; }
	inline String_t** get_address_of_language_14() { return &___language_14; }
	inline void set_language_14(String_t* value)
	{
		___language_14 = value;
		Il2CppCodeGenWriteBarrier(&___language_14, value);
	}

	inline static int32_t get_offset_of_flag_15() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___flag_15)); }
	inline String_t* get_flag_15() const { return ___flag_15; }
	inline String_t** get_address_of_flag_15() { return &___flag_15; }
	inline void set_flag_15(String_t* value)
	{
		___flag_15 = value;
		Il2CppCodeGenWriteBarrier(&___flag_15, value);
	}

	inline static int32_t get_offset_of_players_wins_16() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___players_wins_16)); }
	inline int64_t get_players_wins_16() const { return ___players_wins_16; }
	inline int64_t* get_address_of_players_wins_16() { return &___players_wins_16; }
	inline void set_players_wins_16(int64_t value)
	{
		___players_wins_16 = value;
	}

	inline static int32_t get_offset_of_players_losses_17() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___players_losses_17)); }
	inline int64_t get_players_losses_17() const { return ___players_losses_17; }
	inline int64_t* get_address_of_players_losses_17() { return &___players_losses_17; }
	inline void set_players_losses_17(int64_t value)
	{
		___players_losses_17 = value;
	}

	inline static int32_t get_offset_of_barbarians_wins_18() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___barbarians_wins_18)); }
	inline int64_t get_barbarians_wins_18() const { return ___barbarians_wins_18; }
	inline int64_t* get_address_of_barbarians_wins_18() { return &___barbarians_wins_18; }
	inline void set_barbarians_wins_18(int64_t value)
	{
		___barbarians_wins_18 = value;
	}

	inline static int32_t get_offset_of_barbarians_losses_19() { return static_cast<int32_t>(offsetof(UserModel_t3025569216, ___barbarians_losses_19)); }
	inline int64_t get_barbarians_losses_19() const { return ___barbarians_losses_19; }
	inline int64_t* get_address_of_barbarians_losses_19() { return &___barbarians_losses_19; }
	inline void set_barbarians_losses_19(int64_t value)
	{
		___barbarians_losses_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
