﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportHeader
struct BattleReportHeader_t2798103525;

#include "codegen/il2cpp-codegen.h"

// System.Void BattleReportHeader::.ctor()
extern "C"  void BattleReportHeader__ctor_m1862963164 (BattleReportHeader_t2798103525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
