﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "OSCsharp_OSCsharp_Data_OscPacket504761797.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t2764280154  : public OscPacket_t504761797
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_20;

public:
	inline static int32_t get_offset_of_typeTag_20() { return static_cast<int32_t>(offsetof(OscMessage_t2764280154, ___typeTag_20)); }
	inline String_t* get_typeTag_20() const { return ___typeTag_20; }
	inline String_t** get_address_of_typeTag_20() { return &___typeTag_20; }
	inline void set_typeTag_20(String_t* value)
	{
		___typeTag_20 = value;
		Il2CppCodeGenWriteBarrier(&___typeTag_20, value);
	}
};

struct OscMessage_t2764280154_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OSCsharp.Data.OscMessage::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_21() { return static_cast<int32_t>(offsetof(OscMessage_t2764280154_StaticFields, ___U3CU3Ef__switchU24map0_21)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_21() const { return ___U3CU3Ef__switchU24map0_21; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_21() { return &___U3CU3Ef__switchU24map0_21; }
	inline void set_U3CU3Ef__switchU24map0_21(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
