﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor()
#define Dictionary_2__ctor_m2836329251(__this, method) ((  void (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2__ctor_m2974019358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3327355077(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m1779115180(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2602799901_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3049183(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2125486988 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3640981791(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3143729840_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3414302478(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2125486988 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2391180541_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m906293649(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2125486988 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3231638736(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m673000885_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3886240978(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1552474645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3739239333(__this, method) ((  bool (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m286716188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3853769566(__this, method) ((  bool (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m114053137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m794890296(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2523130893(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1515577492(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m166014324(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3836953505(__this, ___key0, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3534553858(__this, method) ((  bool (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3377830860(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1041452384(__this, method) ((  bool (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3533230809(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2125486988 *, KeyValuePair_2_t4177799506 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m173927915(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2125486988 *, KeyValuePair_2_t4177799506 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2347825093(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2125486988 *, KeyValuePair_2U5BU5D_t4207492391*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2520949926(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2125486988 *, KeyValuePair_2_t4177799506 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2785101654(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m261611203(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4036672546(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m237610861(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::get_Count()
#define Dictionary_2_get_Count_m590645354(__this, method) ((  int32_t (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::get_Item(TKey)
#define Dictionary_2_get_Item_m433554697(__this, ___key0, method) ((  FirebaseApp_t210707726 * (*) (Dictionary_2_t2125486988 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1617273894(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2125486988 *, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_set_Item_m782556999_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m4245395986(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2125486988 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1011958163(__this, ___size0, method) ((  void (*) (Dictionary_2_t2125486988 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1452855017(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2354981071(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4177799506  (*) (Il2CppObject * /* static, unused */, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m522791695(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m380340335(__this /* static, unused */, ___key0, ___value1, method) ((  FirebaseApp_t210707726 * (*) (Il2CppObject * /* static, unused */, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m4172791640(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2125486988 *, KeyValuePair_2U5BU5D_t4207492391*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Resize()
#define Dictionary_2_Resize_m783737252(__this, method) ((  void (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Add(TKey,TValue)
#define Dictionary_2_Add_m1237617443(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2125486988 *, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Clear()
#define Dictionary_2_Clear_m3658164290(__this, method) ((  void (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m956646201(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2125486988 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2082978705(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2125486988 *, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m2455988378(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2125486988 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m677357292(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Remove(TKey)
#define Dictionary_2_Remove_m2831909098(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2125486988 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m3167158394(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2125486988 *, String_t*, FirebaseApp_t210707726 **, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::get_Keys()
#define Dictionary_2_get_Keys_m2658215313(__this, method) ((  KeyCollection_t314017463 * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::get_Values()
#define Dictionary_2_get_Values_m373261557(__this, method) ((  ValueCollection_t828546831 * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m2844834290(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m560093362(__this, ___value0, method) ((  FirebaseApp_t210707726 * (*) (Dictionary_2_t2125486988 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3547904272(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2125486988 *, KeyValuePair_2_t4177799506 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m912646743(__this, method) ((  Enumerator_t3445511690  (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1209439792(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, String_t*, FirebaseApp_t210707726 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
