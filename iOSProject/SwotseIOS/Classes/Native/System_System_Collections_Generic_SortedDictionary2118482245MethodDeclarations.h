﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_589319634MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper__ctor_m946609732(__this, ___cmp0, method) ((  void (*) (NodeHelper_t2118482245 *, Il2CppObject*, const MethodInfo*))NodeHelper__ctor_m787982996_gshared)(__this, ___cmp0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>::.cctor()
#define NodeHelper__cctor_m600721912(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NodeHelper__cctor_m1473232840_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>::Compare(TKey,System.Collections.Generic.RBTree/Node)
#define NodeHelper_Compare_m840275839(__this, ___key0, ___node1, method) ((  int32_t (*) (NodeHelper_t2118482245 *, String_t*, Node_t2499136326 *, const MethodInfo*))NodeHelper_Compare_m1850031039_gshared)(__this, ___key0, ___node1, method)
// System.Collections.Generic.RBTree/Node System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>::CreateNode(TKey)
#define NodeHelper_CreateNode_m717413821(__this, ___key0, method) ((  Node_t2499136326 * (*) (NodeHelper_t2118482245 *, String_t*, const MethodInfo*))NodeHelper_CreateNode_m449733853_gshared)(__this, ___key0, method)
// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,SmartLocalization.LocalizedObject>::GetHelper(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper_GetHelper_m4231945516(__this /* static, unused */, ___cmp0, method) ((  NodeHelper_t2118482245 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))NodeHelper_GetHelper_m3439204716_gshared)(__this /* static, unused */, ___cmp0, method)
