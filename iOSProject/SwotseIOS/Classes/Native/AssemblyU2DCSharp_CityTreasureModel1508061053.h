﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ShopItemModel
struct ShopItemModel_t96241056;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityTreasureModel
struct  CityTreasureModel_t1508061053  : public Il2CppObject
{
public:
	// System.String CityTreasureModel::id
	String_t* ___id_0;
	// System.Int64 CityTreasureModel::city
	int64_t ___city_1;
	// ShopItemModel CityTreasureModel::item_constant
	ShopItemModel_t96241056 * ___item_constant_2;
	// System.Int64 CityTreasureModel::count
	int64_t ___count_3;
	// System.Int64 CityTreasureModel::price
	int64_t ___price_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1508061053, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_city_1() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1508061053, ___city_1)); }
	inline int64_t get_city_1() const { return ___city_1; }
	inline int64_t* get_address_of_city_1() { return &___city_1; }
	inline void set_city_1(int64_t value)
	{
		___city_1 = value;
	}

	inline static int32_t get_offset_of_item_constant_2() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1508061053, ___item_constant_2)); }
	inline ShopItemModel_t96241056 * get_item_constant_2() const { return ___item_constant_2; }
	inline ShopItemModel_t96241056 ** get_address_of_item_constant_2() { return &___item_constant_2; }
	inline void set_item_constant_2(ShopItemModel_t96241056 * value)
	{
		___item_constant_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_constant_2, value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1508061053, ___count_3)); }
	inline int64_t get_count_3() const { return ___count_3; }
	inline int64_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int64_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_price_4() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1508061053, ___price_4)); }
	inline int64_t get_price_4() const { return ___price_4; }
	inline int64_t* get_address_of_price_4() { return &___price_4; }
	inline void set_price_4(int64_t value)
	{
		___price_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
