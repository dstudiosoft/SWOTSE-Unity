﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceInviteLine
struct AllianceInviteLine_t3911898758;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<UserModel>
struct List_1_t2394690348;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInviteTableController
struct  AllianceInviteTableController_t989323868  : public MonoBehaviour_t1158329972
{
public:
	// AllianceInviteLine AllianceInviteTableController::m_cellPrefab
	AllianceInviteLine_t3911898758 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceInviteTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.Generic.List`1<UserModel> AllianceInviteTableController::allUsers
	List_1_t2394690348 * ___allUsers_4;
	// System.Int32 AllianceInviteTableController::m_numRows
	int32_t ___m_numRows_5;
	// System.String AllianceInviteTableController::dateFormat
	String_t* ___dateFormat_6;
	// SimpleEvent AllianceInviteTableController::onGetAllUsers
	SimpleEvent_t1509017202 * ___onGetAllUsers_7;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___m_cellPrefab_2)); }
	inline AllianceInviteLine_t3911898758 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceInviteLine_t3911898758 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceInviteLine_t3911898758 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_allUsers_4() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___allUsers_4)); }
	inline List_1_t2394690348 * get_allUsers_4() const { return ___allUsers_4; }
	inline List_1_t2394690348 ** get_address_of_allUsers_4() { return &___allUsers_4; }
	inline void set_allUsers_4(List_1_t2394690348 * value)
	{
		___allUsers_4 = value;
		Il2CppCodeGenWriteBarrier(&___allUsers_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}

	inline static int32_t get_offset_of_dateFormat_6() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___dateFormat_6)); }
	inline String_t* get_dateFormat_6() const { return ___dateFormat_6; }
	inline String_t** get_address_of_dateFormat_6() { return &___dateFormat_6; }
	inline void set_dateFormat_6(String_t* value)
	{
		___dateFormat_6 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_6, value);
	}

	inline static int32_t get_offset_of_onGetAllUsers_7() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t989323868, ___onGetAllUsers_7)); }
	inline SimpleEvent_t1509017202 * get_onGetAllUsers_7() const { return ___onGetAllUsers_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetAllUsers_7() { return &___onGetAllUsers_7; }
	inline void set_onGetAllUsers_7(SimpleEvent_t1509017202 * value)
	{
		___onGetAllUsers_7 = value;
		Il2CppCodeGenWriteBarrier(&___onGetAllUsers_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
