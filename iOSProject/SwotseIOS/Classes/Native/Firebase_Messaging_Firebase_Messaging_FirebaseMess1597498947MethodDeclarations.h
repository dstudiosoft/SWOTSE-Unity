﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_t1597498947;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Messaging.FirebaseMessaging/Listener::.ctor()
extern "C"  void Listener__ctor_m2655013895 (Listener_t1597498947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::Create()
extern "C"  Listener_t1597498947 * Listener_Create_m3044555873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Destroy()
extern "C"  void Listener_Destroy_m1408005397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Finalize()
extern "C"  void Listener_Finalize_m4084179437 (Listener_t1597498947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Dispose()
extern "C"  void Listener_Dispose_m2961180058 (Listener_t1597498947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener::MessageReceivedDelegateMethod(System.IntPtr)
extern "C"  int32_t Listener_MessageReceivedDelegateMethod_m28038043 (Il2CppObject * __this /* static, unused */, IntPtr_t ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_Listener_MessageReceivedDelegateMethod_m28038043(intptr_t ___message0);
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::TokenReceivedDelegateMethod(System.String)
extern "C"  void Listener_TokenReceivedDelegateMethod_m1462504193 (Il2CppObject * __this /* static, unused */, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Listener_TokenReceivedDelegateMethod_m1462504193(char* ___token0);
