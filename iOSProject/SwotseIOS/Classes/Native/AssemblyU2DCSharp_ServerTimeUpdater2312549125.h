﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ServerTimeUpdater
struct ServerTimeUpdater_t2312549125;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerTimeUpdater
struct  ServerTimeUpdater_t2312549125  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ServerTimeUpdater::currentTime
	Text_t356221433 * ___currentTime_3;
	// System.DateTime ServerTimeUpdater::currentDate
	DateTime_t693205669  ___currentDate_4;

public:
	inline static int32_t get_offset_of_currentTime_3() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t2312549125, ___currentTime_3)); }
	inline Text_t356221433 * get_currentTime_3() const { return ___currentTime_3; }
	inline Text_t356221433 ** get_address_of_currentTime_3() { return &___currentTime_3; }
	inline void set_currentTime_3(Text_t356221433 * value)
	{
		___currentTime_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentTime_3, value);
	}

	inline static int32_t get_offset_of_currentDate_4() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t2312549125, ___currentDate_4)); }
	inline DateTime_t693205669  get_currentDate_4() const { return ___currentDate_4; }
	inline DateTime_t693205669 * get_address_of_currentDate_4() { return &___currentDate_4; }
	inline void set_currentDate_4(DateTime_t693205669  value)
	{
		___currentDate_4 = value;
	}
};

struct ServerTimeUpdater_t2312549125_StaticFields
{
public:
	// ServerTimeUpdater ServerTimeUpdater::instance
	ServerTimeUpdater_t2312549125 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t2312549125_StaticFields, ___instance_2)); }
	inline ServerTimeUpdater_t2312549125 * get_instance_2() const { return ___instance_2; }
	inline ServerTimeUpdater_t2312549125 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ServerTimeUpdater_t2312549125 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
