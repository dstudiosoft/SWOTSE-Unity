﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FutureString/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t1819656562;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Firebase.FutureString/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIG_CompletionDelegate__ctor_m3991997779 (SWIG_CompletionDelegate_t1819656562 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureString/SWIG_CompletionDelegate::Invoke(System.Int32)
extern "C"  void SWIG_CompletionDelegate_Invoke_m2502924064 (SWIG_CompletionDelegate_t1819656562 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Firebase.FutureString/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SWIG_CompletionDelegate_BeginInvoke_m1619116057 (SWIG_CompletionDelegate_t1819656562 * __this, int32_t ___index0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureString/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SWIG_CompletionDelegate_EndInvoke_m2088723553 (SWIG_CompletionDelegate_t1819656562 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
