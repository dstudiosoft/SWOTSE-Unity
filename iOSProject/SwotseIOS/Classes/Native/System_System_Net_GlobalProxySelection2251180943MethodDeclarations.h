﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.GlobalProxySelection
struct GlobalProxySelection_t2251180943;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.GlobalProxySelection::.ctor()
extern "C"  void GlobalProxySelection__ctor_m502199831 (GlobalProxySelection_t2251180943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.GlobalProxySelection::get_Select()
extern "C"  Il2CppObject * GlobalProxySelection_get_Select_m1637611912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.GlobalProxySelection::set_Select(System.Net.IWebProxy)
extern "C"  void GlobalProxySelection_set_Select_m1392742401 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.GlobalProxySelection::GetEmptyWebProxy()
extern "C"  Il2CppObject * GlobalProxySelection_GetEmptyWebProxy_m3615532046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
