﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResourceManager
struct ResourceManager_t4136494783;
// UnitsManager
struct UnitsManager_t3465258182;
// BuildingsManager
struct BuildingsManager_t1345368432;
// TreasureManager
struct TreasureManager_t1131816636;
// MyCityModel
struct MyCityModel_t1736786178;
// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel>
struct Dictionary_2_t2726304580;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager
struct  CityManager_t1148847038  : public Il2CppObject
{
public:
	// ResourceManager CityManager::resourceManager
	ResourceManager_t4136494783 * ___resourceManager_0;
	// UnitsManager CityManager::unitsManager
	UnitsManager_t3465258182 * ___unitsManager_1;
	// BuildingsManager CityManager::buildingsManager
	BuildingsManager_t1345368432 * ___buildingsManager_2;
	// TreasureManager CityManager::treasureManager
	TreasureManager_t1131816636 * ___treasureManager_3;
	// MyCityModel CityManager::currentCity
	MyCityModel_t1736786178 * ___currentCity_4;
	// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel> CityManager::myCities
	Dictionary_2_t2726304580 * ___myCities_5;
	// SimpleEvent CityManager::onCityBuilt
	SimpleEvent_t1509017202 * ___onCityBuilt_6;
	// SimpleEvent CityManager::onGotNewCity
	SimpleEvent_t1509017202 * ___onGotNewCity_7;
	// SimpleEvent CityManager::onCityNameAndIconChanged
	SimpleEvent_t1509017202 * ___onCityNameAndIconChanged_8;
	// SimpleEvent CityManager::cityChangerUpdateImage
	SimpleEvent_t1509017202 * ___cityChangerUpdateImage_9;
	// SimpleEvent CityManager::gotAllowTroopsState
	SimpleEvent_t1509017202 * ___gotAllowTroopsState_10;
	// SimpleEvent CityManager::onTaxUpdated
	SimpleEvent_t1509017202 * ___onTaxUpdated_11;

public:
	inline static int32_t get_offset_of_resourceManager_0() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___resourceManager_0)); }
	inline ResourceManager_t4136494783 * get_resourceManager_0() const { return ___resourceManager_0; }
	inline ResourceManager_t4136494783 ** get_address_of_resourceManager_0() { return &___resourceManager_0; }
	inline void set_resourceManager_0(ResourceManager_t4136494783 * value)
	{
		___resourceManager_0 = value;
		Il2CppCodeGenWriteBarrier(&___resourceManager_0, value);
	}

	inline static int32_t get_offset_of_unitsManager_1() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___unitsManager_1)); }
	inline UnitsManager_t3465258182 * get_unitsManager_1() const { return ___unitsManager_1; }
	inline UnitsManager_t3465258182 ** get_address_of_unitsManager_1() { return &___unitsManager_1; }
	inline void set_unitsManager_1(UnitsManager_t3465258182 * value)
	{
		___unitsManager_1 = value;
		Il2CppCodeGenWriteBarrier(&___unitsManager_1, value);
	}

	inline static int32_t get_offset_of_buildingsManager_2() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___buildingsManager_2)); }
	inline BuildingsManager_t1345368432 * get_buildingsManager_2() const { return ___buildingsManager_2; }
	inline BuildingsManager_t1345368432 ** get_address_of_buildingsManager_2() { return &___buildingsManager_2; }
	inline void set_buildingsManager_2(BuildingsManager_t1345368432 * value)
	{
		___buildingsManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsManager_2, value);
	}

	inline static int32_t get_offset_of_treasureManager_3() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___treasureManager_3)); }
	inline TreasureManager_t1131816636 * get_treasureManager_3() const { return ___treasureManager_3; }
	inline TreasureManager_t1131816636 ** get_address_of_treasureManager_3() { return &___treasureManager_3; }
	inline void set_treasureManager_3(TreasureManager_t1131816636 * value)
	{
		___treasureManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___treasureManager_3, value);
	}

	inline static int32_t get_offset_of_currentCity_4() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___currentCity_4)); }
	inline MyCityModel_t1736786178 * get_currentCity_4() const { return ___currentCity_4; }
	inline MyCityModel_t1736786178 ** get_address_of_currentCity_4() { return &___currentCity_4; }
	inline void set_currentCity_4(MyCityModel_t1736786178 * value)
	{
		___currentCity_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentCity_4, value);
	}

	inline static int32_t get_offset_of_myCities_5() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___myCities_5)); }
	inline Dictionary_2_t2726304580 * get_myCities_5() const { return ___myCities_5; }
	inline Dictionary_2_t2726304580 ** get_address_of_myCities_5() { return &___myCities_5; }
	inline void set_myCities_5(Dictionary_2_t2726304580 * value)
	{
		___myCities_5 = value;
		Il2CppCodeGenWriteBarrier(&___myCities_5, value);
	}

	inline static int32_t get_offset_of_onCityBuilt_6() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___onCityBuilt_6)); }
	inline SimpleEvent_t1509017202 * get_onCityBuilt_6() const { return ___onCityBuilt_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onCityBuilt_6() { return &___onCityBuilt_6; }
	inline void set_onCityBuilt_6(SimpleEvent_t1509017202 * value)
	{
		___onCityBuilt_6 = value;
		Il2CppCodeGenWriteBarrier(&___onCityBuilt_6, value);
	}

	inline static int32_t get_offset_of_onGotNewCity_7() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___onGotNewCity_7)); }
	inline SimpleEvent_t1509017202 * get_onGotNewCity_7() const { return ___onGotNewCity_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGotNewCity_7() { return &___onGotNewCity_7; }
	inline void set_onGotNewCity_7(SimpleEvent_t1509017202 * value)
	{
		___onGotNewCity_7 = value;
		Il2CppCodeGenWriteBarrier(&___onGotNewCity_7, value);
	}

	inline static int32_t get_offset_of_onCityNameAndIconChanged_8() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___onCityNameAndIconChanged_8)); }
	inline SimpleEvent_t1509017202 * get_onCityNameAndIconChanged_8() const { return ___onCityNameAndIconChanged_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onCityNameAndIconChanged_8() { return &___onCityNameAndIconChanged_8; }
	inline void set_onCityNameAndIconChanged_8(SimpleEvent_t1509017202 * value)
	{
		___onCityNameAndIconChanged_8 = value;
		Il2CppCodeGenWriteBarrier(&___onCityNameAndIconChanged_8, value);
	}

	inline static int32_t get_offset_of_cityChangerUpdateImage_9() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___cityChangerUpdateImage_9)); }
	inline SimpleEvent_t1509017202 * get_cityChangerUpdateImage_9() const { return ___cityChangerUpdateImage_9; }
	inline SimpleEvent_t1509017202 ** get_address_of_cityChangerUpdateImage_9() { return &___cityChangerUpdateImage_9; }
	inline void set_cityChangerUpdateImage_9(SimpleEvent_t1509017202 * value)
	{
		___cityChangerUpdateImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityChangerUpdateImage_9, value);
	}

	inline static int32_t get_offset_of_gotAllowTroopsState_10() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___gotAllowTroopsState_10)); }
	inline SimpleEvent_t1509017202 * get_gotAllowTroopsState_10() const { return ___gotAllowTroopsState_10; }
	inline SimpleEvent_t1509017202 ** get_address_of_gotAllowTroopsState_10() { return &___gotAllowTroopsState_10; }
	inline void set_gotAllowTroopsState_10(SimpleEvent_t1509017202 * value)
	{
		___gotAllowTroopsState_10 = value;
		Il2CppCodeGenWriteBarrier(&___gotAllowTroopsState_10, value);
	}

	inline static int32_t get_offset_of_onTaxUpdated_11() { return static_cast<int32_t>(offsetof(CityManager_t1148847038, ___onTaxUpdated_11)); }
	inline SimpleEvent_t1509017202 * get_onTaxUpdated_11() const { return ___onTaxUpdated_11; }
	inline SimpleEvent_t1509017202 ** get_address_of_onTaxUpdated_11() { return &___onTaxUpdated_11; }
	inline void set_onTaxUpdated_11(SimpleEvent_t1509017202 * value)
	{
		___onTaxUpdated_11 = value;
		Il2CppCodeGenWriteBarrier(&___onTaxUpdated_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
