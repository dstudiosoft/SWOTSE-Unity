﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>
struct EqualityComparer_1_t2454235246;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m956103590_gshared (EqualityComparer_1_t2454235246 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m956103590(__this, method) ((  void (*) (EqualityComparer_1_t2454235246 *, const MethodInfo*))EqualityComparer_1__ctor_m956103590_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2966618823_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2966618823(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2966618823_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1788863987_gshared (EqualityComparer_1_t2454235246 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1788863987(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2454235246 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1788863987_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2451619445_gshared (EqualityComparer_1_t2454235246 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2451619445(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2454235246 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2451619445_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<TouchScript.Gestures.UI.UIGesture/TouchData>::get_Default()
extern "C"  EqualityComparer_1_t2454235246 * EqualityComparer_1_get_Default_m2545903194_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2545903194(__this /* static, unused */, method) ((  EqualityComparer_1_t2454235246 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2545903194_gshared)(__this /* static, unused */, method)
