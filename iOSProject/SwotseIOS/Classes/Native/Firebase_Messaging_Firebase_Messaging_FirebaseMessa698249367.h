﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t2996655534;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1711098632;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE
struct  FirebaseMessagingPINVOKE_t698249367  : public Il2CppObject
{
public:

public:
};

struct FirebaseMessagingPINVOKE_t698249367_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t2996655534 * ___swigExceptionHelper_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigStringHelper
	SWIGStringHelper_t1711098632 * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t698249367_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t2996655534 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t2996655534 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t2996655534 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier(&___swigExceptionHelper_0, value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t698249367_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t1711098632 * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t1711098632 ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t1711098632 * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier(&___swigStringHelper_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
