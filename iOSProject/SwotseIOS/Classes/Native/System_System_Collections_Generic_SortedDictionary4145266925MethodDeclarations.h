﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t4145266925;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary3796243578.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1640811614_gshared (KeyCollection_t4145266925 * __this, SortedDictionary_2_t3931121312 * ___dic0, const MethodInfo* method);
#define KeyCollection__ctor_m1640811614(__this, ___dic0, method) ((  void (*) (KeyCollection_t4145266925 *, SortedDictionary_2_t3931121312 *, const MethodInfo*))KeyCollection__ctor_m1640811614_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125209017_gshared (KeyCollection_t4145266925 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125209017(__this, ___item0, method) ((  void (*) (KeyCollection_t4145266925 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125209017_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4124574780_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4124574780(__this, method) ((  void (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4124574780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3936183375_gshared (KeyCollection_t4145266925 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3936183375(__this, ___item0, method) ((  bool (*) (KeyCollection_t4145266925 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3936183375_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1016326398_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1016326398(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1016326398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m421859978_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m421859978(__this, method) ((  bool (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m421859978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3986283392_gshared (KeyCollection_t4145266925 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3986283392(__this, ___item0, method) ((  bool (*) (KeyCollection_t4145266925 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3986283392_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2542066674_gshared (KeyCollection_t4145266925 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2542066674(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4145266925 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2542066674_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m694373310_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m694373310(__this, method) ((  bool (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m694373310_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2770359586_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2770359586(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2770359586_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1072197233_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1072197233(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1072197233_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2465470732_gshared (KeyCollection_t4145266925 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2465470732(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t4145266925 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2465470732_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1152988478_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1152988478(__this, method) ((  int32_t (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_get_Count_m1152988478_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3796243578  KeyCollection_GetEnumerator_m1078324954_gshared (KeyCollection_t4145266925 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1078324954(__this, method) ((  Enumerator_t3796243578  (*) (KeyCollection_t4145266925 *, const MethodInfo*))KeyCollection_GetEnumerator_m1078324954_gshared)(__this, method)
