﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.TouchManagerInstance
struct TouchManagerInstance_t2629118981;
// System.EventHandler
struct EventHandler_t277755526;
// System.EventHandler`1<TouchScript.TouchEventArgs>
struct EventHandler_1_t509234338;
// TouchScript.Devices.Display.IDisplayDevice
struct IDisplayDevice_t2646363379;
// System.Collections.Generic.IList`1<TouchScript.Layers.TouchLayer>
struct IList_1_t3176380579;
// System.Collections.Generic.IList`1<TouchScript.InputSources.IInputSource>
struct IList_1_t3807500939;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// TouchScript.InputSources.IInputSource
struct IInputSource_t3266560338;
// UnityEngine.Transform
struct Transform_t3275118058;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t328750215;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer2635439978.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.TouchManagerInstance::.ctor()
extern "C"  void TouchManagerInstance__ctor_m1040953106 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::.cctor()
extern "C"  void TouchManagerInstance__cctor_m4192131667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_FrameStarted(System.EventHandler)
extern "C"  void TouchManagerInstance_add_FrameStarted_m1015120101 (TouchManagerInstance_t2629118981 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_FrameStarted(System.EventHandler)
extern "C"  void TouchManagerInstance_remove_FrameStarted_m165930970 (TouchManagerInstance_t2629118981 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_FrameFinished(System.EventHandler)
extern "C"  void TouchManagerInstance_add_FrameFinished_m3297081630 (TouchManagerInstance_t2629118981 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_FrameFinished(System.EventHandler)
extern "C"  void TouchManagerInstance_remove_FrameFinished_m1647132625 (TouchManagerInstance_t2629118981 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_TouchesBegan(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_add_TouchesBegan_m486819500 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_TouchesBegan(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_remove_TouchesBegan_m526558631 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_TouchesMoved(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_add_TouchesMoved_m2987505174 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_TouchesMoved(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_remove_TouchesMoved_m1482742573 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_TouchesEnded(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_add_TouchesEnded_m2834125513 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_TouchesEnded(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_remove_TouchesEnded_m479541490 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::add_TouchesCancelled(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_add_TouchesCancelled_m749004930 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::remove_TouchesCancelled(System.EventHandler`1<TouchScript.TouchEventArgs>)
extern "C"  void TouchManagerInstance_remove_TouchesCancelled_m989050095 (TouchManagerInstance_t2629118981 * __this, EventHandler_1_t509234338 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchManagerInstance TouchScript.TouchManagerInstance::get_Instance()
extern "C"  TouchManagerInstance_t2629118981 * TouchManagerInstance_get_Instance_m3878126648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManagerInstance::get_DisplayDevice()
extern "C"  Il2CppObject * TouchManagerInstance_get_DisplayDevice_m1491316288 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
extern "C"  void TouchManagerInstance_set_DisplayDevice_m519774249 (TouchManagerInstance_t2629118981 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.TouchManagerInstance::get_DPI()
extern "C"  float TouchManagerInstance_get_DPI_m1641935312 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::get_ShouldCreateCameraLayer()
extern "C"  bool TouchManagerInstance_get_ShouldCreateCameraLayer_m1303868182 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::set_ShouldCreateCameraLayer(System.Boolean)
extern "C"  void TouchManagerInstance_set_ShouldCreateCameraLayer_m1359473997 (TouchManagerInstance_t2629118981 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::get_ShouldCreateStandardInput()
extern "C"  bool TouchManagerInstance_get_ShouldCreateStandardInput_m232729485 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::set_ShouldCreateStandardInput(System.Boolean)
extern "C"  void TouchManagerInstance_set_ShouldCreateStandardInput_m443885386 (TouchManagerInstance_t2629118981 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManagerInstance::get_Layers()
extern "C"  Il2CppObject* TouchManagerInstance_get_Layers_m3315425989 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.InputSources.IInputSource> TouchScript.TouchManagerInstance::get_Inputs()
extern "C"  Il2CppObject* TouchManagerInstance_get_Inputs_m4235890670 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.TouchManagerInstance::get_DotsPerCentimeter()
extern "C"  float TouchManagerInstance_get_DotsPerCentimeter_m979173194 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.TouchManagerInstance::get_NumberOfTouches()
extern "C"  int32_t TouchManagerInstance_get_NumberOfTouches_m1963617308 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::get_ActiveTouches()
extern "C"  Il2CppObject* TouchManagerInstance_get_ActiveTouches_m3099082747 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::AddLayer(TouchScript.Layers.TouchLayer,System.Int32,System.Boolean)
extern "C"  bool TouchManagerInstance_AddLayer_m2365734176 (TouchManagerInstance_t2629118981 * __this, TouchLayer_t2635439978 * ___layer0, int32_t ___index1, bool ___addIfExists2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::RemoveLayer(TouchScript.Layers.TouchLayer)
extern "C"  bool TouchManagerInstance_RemoveLayer_m2991410817 (TouchManagerInstance_t2629118981 * __this, TouchLayer_t2635439978 * ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::ChangeLayerIndex(System.Int32,System.Int32)
extern "C"  void TouchManagerInstance_ChangeLayerIndex_m2175687047 (TouchManagerInstance_t2629118981 * __this, int32_t ___at0, int32_t ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::AddInput(TouchScript.InputSources.IInputSource)
extern "C"  bool TouchManagerInstance_AddInput_m4270423107 (TouchManagerInstance_t2629118981 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::RemoveInput(TouchScript.InputSources.IInputSource)
extern "C"  bool TouchManagerInstance_RemoveInput_m2656308524 (TouchManagerInstance_t2629118981 * __this, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform TouchScript.TouchManagerInstance::GetHitTarget(UnityEngine.Vector2)
extern "C"  Transform_t3275118058 * TouchManagerInstance_GetHitTarget_m296054122 (TouchManagerInstance_t2629118981 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::GetHitTarget(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  bool TouchManagerInstance_GetHitTarget_m2413355339 (TouchManagerInstance_t2629118981 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::GetHitTarget(UnityEngine.Vector2,TouchScript.Hit.TouchHit&,TouchScript.Layers.TouchLayer&)
extern "C"  bool TouchManagerInstance_GetHitTarget_m2834584169 (TouchManagerInstance_t2629118981 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, TouchLayer_t2635439978 ** ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::CancelTouch(System.Int32,System.Boolean)
extern "C"  void TouchManagerInstance_CancelTouch_m3547684505 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::CancelTouch(System.Int32)
extern "C"  void TouchManagerInstance_CancelTouch_m968894266 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.TouchManagerInstance::INTERNAL_BeginTouch(UnityEngine.Vector2,TouchScript.InputSources.IInputSource)
extern "C"  TouchPoint_t959629083 * TouchManagerInstance_INTERNAL_BeginTouch_m2601024342 (TouchManagerInstance_t2629118981 * __this, Vector2_t2243707579  ___position0, Il2CppObject * ___input1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.TouchManagerInstance::INTERNAL_BeginTouch(UnityEngine.Vector2,TouchScript.InputSources.IInputSource,TouchScript.Tags)
extern "C"  TouchPoint_t959629083 * TouchManagerInstance_INTERNAL_BeginTouch_m2119312865 (TouchManagerInstance_t2629118981 * __this, Vector2_t2243707579  ___position0, Il2CppObject * ___input1, Tags_t1265380163 * ___tags2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::INTERNAL_UpdateTouch(System.Int32)
extern "C"  void TouchManagerInstance_INTERNAL_UpdateTouch_m2225431485 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::INTERNAL_MoveTouch(System.Int32,UnityEngine.Vector2)
extern "C"  void TouchManagerInstance_INTERNAL_MoveTouch_m3826839585 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, Vector2_t2243707579  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::INTERNAL_EndTouch(System.Int32)
extern "C"  void TouchManagerInstance_INTERNAL_EndTouch_m1938849627 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::INTERNAL_CancelTouch(System.Int32)
extern "C"  void TouchManagerInstance_INTERNAL_CancelTouch_m311631898 (TouchManagerInstance_t2629118981 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::Awake()
extern "C"  void TouchManagerInstance_Awake_m411776221 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::OnLevelWasLoaded(System.Int32)
extern "C"  void TouchManagerInstance_OnLevelWasLoaded_m974166966 (TouchManagerInstance_t2629118981 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchScript.TouchManagerInstance::lateAwake()
extern "C"  Il2CppObject * TouchManagerInstance_lateAwake_m3477628619 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::Update()
extern "C"  void TouchManagerInstance_Update_m2236618753 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::OnApplicationQuit()
extern "C"  void TouchManagerInstance_OnApplicationQuit_m864355400 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateDPI()
extern "C"  void TouchManagerInstance_updateDPI_m2112328602 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateLayers()
extern "C"  void TouchManagerInstance_updateLayers_m953546345 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::createCameraLayer()
extern "C"  void TouchManagerInstance_createCameraLayer_m954116398 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::createTouchInput()
extern "C"  void TouchManagerInstance_createTouchInput_m3313427147 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateInputs()
extern "C"  void TouchManagerInstance_updateInputs_m3890644892 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateBegan(System.Collections.Generic.List`1<TouchScript.TouchPoint>)
extern "C"  void TouchManagerInstance_updateBegan_m1792964133 (TouchManagerInstance_t2629118981 * __this, List_1_t328750215 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateUpdated(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void TouchManagerInstance_updateUpdated_m2550597931 (TouchManagerInstance_t2629118981 * __this, List_1_t1440998580 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateEnded(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void TouchManagerInstance_updateEnded_m1818978950 (TouchManagerInstance_t2629118981 * __this, List_1_t1440998580 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateCancelled(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void TouchManagerInstance_updateCancelled_m2223650473 (TouchManagerInstance_t2629118981 * __this, List_1_t1440998580 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::updateTouches()
extern "C"  void TouchManagerInstance_updateTouches_m3657616818 (TouchManagerInstance_t2629118981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::<touchPointPool>m__20(TouchScript.TouchPoint)
extern "C"  void TouchManagerInstance_U3CtouchPointPoolU3Em__20_m3078137197 (Il2CppObject * __this /* static, unused */, TouchPoint_t959629083 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::<touchPointListPool>m__21()
extern "C"  List_1_t328750215 * TouchManagerInstance_U3CtouchPointListPoolU3Em__21_m2296680311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::<touchPointListPool>m__22(System.Collections.Generic.List`1<TouchScript.TouchPoint>)
extern "C"  void TouchManagerInstance_U3CtouchPointListPoolU3Em__22_m2921475235 (Il2CppObject * __this /* static, unused */, List_1_t328750215 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> TouchScript.TouchManagerInstance::<intListPool>m__23()
extern "C"  List_1_t1440998580 * TouchManagerInstance_U3CintListPoolU3Em__23_m685371327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManagerInstance::<intListPool>m__24(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void TouchManagerInstance_U3CintListPoolU3Em__24_m768917863 (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManagerInstance::<updateLayers>m__28(TouchScript.Layers.TouchLayer)
extern "C"  bool TouchManagerInstance_U3CupdateLayersU3Em__28_m1050952002 (Il2CppObject * __this /* static, unused */, TouchLayer_t2635439978 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
