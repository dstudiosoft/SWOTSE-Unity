﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Authenticode.PrivateKey
struct PrivateKey_t1785233108;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Security.Cryptography.RSA
struct RSA_t3719518354;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.Authenticode.PrivateKey::.ctor(System.Byte[],System.String)
extern "C"  void PrivateKey__ctor_m2779802083 (PrivateKey_t1785233108 * __this, ByteU5BU5D_t3397334013* ___data0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Authenticode.PrivateKey::get_RSA()
extern "C"  RSA_t3719518354 * PrivateKey_get_RSA_m2584711367 (PrivateKey_t1785233108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.PrivateKey::DeriveKey(System.Byte[],System.String)
extern "C"  ByteU5BU5D_t3397334013* PrivateKey_DeriveKey_m502282257 (PrivateKey_t1785233108 * __this, ByteU5BU5D_t3397334013* ___salt0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.PrivateKey::Decode(System.Byte[],System.String)
extern "C"  bool PrivateKey_Decode_m2735712995 (PrivateKey_t1785233108 * __this, ByteU5BU5D_t3397334013* ___pvk0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Authenticode.PrivateKey Mono.Security.Authenticode.PrivateKey::CreateFromFile(System.String)
extern "C"  PrivateKey_t1785233108 * PrivateKey_CreateFromFile_m2068416645 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Authenticode.PrivateKey Mono.Security.Authenticode.PrivateKey::CreateFromFile(System.String,System.String)
extern "C"  PrivateKey_t1785233108 * PrivateKey_CreateFromFile_m2403436807 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
