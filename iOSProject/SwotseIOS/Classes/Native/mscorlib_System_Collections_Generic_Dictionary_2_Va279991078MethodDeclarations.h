﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va279991078.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m578713165_gshared (Enumerator_t279991078 * __this, Dictionary_2_t2888425610 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m578713165(__this, ___host0, method) ((  void (*) (Enumerator_t279991078 *, Dictionary_2_t2888425610 *, const MethodInfo*))Enumerator__ctor_m578713165_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2324302712_gshared (Enumerator_t279991078 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2324302712(__this, method) ((  Il2CppObject * (*) (Enumerator_t279991078 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2324302712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2872104894_gshared (Enumerator_t279991078 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2872104894(__this, method) ((  void (*) (Enumerator_t279991078 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2872104894_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Dispose()
extern "C"  void Enumerator_Dispose_m1477292453_gshared (Enumerator_t279991078 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1477292453(__this, method) ((  void (*) (Enumerator_t279991078 *, const MethodInfo*))Enumerator_Dispose_m1477292453_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3454829658_gshared (Enumerator_t279991078 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3454829658(__this, method) ((  bool (*) (Enumerator_t279991078 *, const MethodInfo*))Enumerator_MoveNext_m3454829658_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Current()
extern "C"  TouchData_t3880599975  Enumerator_get_Current_m4212626840_gshared (Enumerator_t279991078 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4212626840(__this, method) ((  TouchData_t3880599975  (*) (Enumerator_t279991078 *, const MethodInfo*))Enumerator_get_Current_m4212626840_gshared)(__this, method)
