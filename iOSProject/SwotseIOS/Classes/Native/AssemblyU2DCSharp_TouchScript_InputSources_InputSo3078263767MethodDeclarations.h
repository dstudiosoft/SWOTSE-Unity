﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.InputSource
struct InputSource_t3078263767;
// TouchScript.InputSources.ICoordinatesRemapper
struct ICoordinatesRemapper_t2162613424;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// TouchScript.Tags
struct Tags_t1265380163;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"

// System.Void TouchScript.InputSources.InputSource::.ctor()
extern "C"  void InputSource__ctor_m876230838 (InputSource_t3078263767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputSource::get_CoordinatesRemapper()
extern "C"  Il2CppObject * InputSource_get_CoordinatesRemapper_m457308327 (InputSource_t3078263767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::set_CoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern "C"  void InputSource_set_CoordinatesRemapper_m3454756984 (InputSource_t3078263767 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::UpdateInput()
extern "C"  void InputSource_UpdateInput_m602977329 (InputSource_t3078263767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  void InputSource_CancelTouch_m1004016495 (InputSource_t3078263767 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::OnEnable()
extern "C"  void InputSource_OnEnable_m866025202 (InputSource_t3078263767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::OnDisable()
extern "C"  void InputSource_OnDisable_m4088493999 (InputSource_t3078263767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.InputSources.InputSource::beginTouch(UnityEngine.Vector2,TouchScript.Tags,System.Boolean)
extern "C"  TouchPoint_t959629083 * InputSource_beginTouch_m3362699974 (InputSource_t3078263767 * __this, Vector2_t2243707579  ___position0, Tags_t1265380163 * ___tags1, bool ___canRemap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::updateTouch(System.Int32)
extern "C"  void InputSource_updateTouch_m1851591771 (InputSource_t3078263767 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::moveTouch(System.Int32,UnityEngine.Vector2)
extern "C"  void InputSource_moveTouch_m2777582399 (InputSource_t3078263767 * __this, int32_t ___id0, Vector2_t2243707579  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::endTouch(System.Int32)
extern "C"  void InputSource_endTouch_m4112874165 (InputSource_t3078263767 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputSource::cancelTouch(System.Int32)
extern "C"  void InputSource_cancelTouch_m47973866 (InputSource_t3078263767 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
