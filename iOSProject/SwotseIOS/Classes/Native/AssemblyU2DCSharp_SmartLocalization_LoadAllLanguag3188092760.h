﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct List_1_t1730846869;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LoadAllLanguages
struct  LoadAllLanguages_t3188092760  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> SmartLocalization.LoadAllLanguages::currentLanguageKeys
	List_1_t1398341365 * ___currentLanguageKeys_2;
	// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.LoadAllLanguages::availableLanguages
	List_1_t1730846869 * ___availableLanguages_3;
	// SmartLocalization.LanguageManager SmartLocalization.LoadAllLanguages::languageManager
	LanguageManager_t132697751 * ___languageManager_4;
	// UnityEngine.Vector2 SmartLocalization.LoadAllLanguages::valuesScrollPosition
	Vector2_t2243707579  ___valuesScrollPosition_5;
	// UnityEngine.Vector2 SmartLocalization.LoadAllLanguages::languagesScrollPosition
	Vector2_t2243707579  ___languagesScrollPosition_6;

public:
	inline static int32_t get_offset_of_currentLanguageKeys_2() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t3188092760, ___currentLanguageKeys_2)); }
	inline List_1_t1398341365 * get_currentLanguageKeys_2() const { return ___currentLanguageKeys_2; }
	inline List_1_t1398341365 ** get_address_of_currentLanguageKeys_2() { return &___currentLanguageKeys_2; }
	inline void set_currentLanguageKeys_2(List_1_t1398341365 * value)
	{
		___currentLanguageKeys_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentLanguageKeys_2, value);
	}

	inline static int32_t get_offset_of_availableLanguages_3() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t3188092760, ___availableLanguages_3)); }
	inline List_1_t1730846869 * get_availableLanguages_3() const { return ___availableLanguages_3; }
	inline List_1_t1730846869 ** get_address_of_availableLanguages_3() { return &___availableLanguages_3; }
	inline void set_availableLanguages_3(List_1_t1730846869 * value)
	{
		___availableLanguages_3 = value;
		Il2CppCodeGenWriteBarrier(&___availableLanguages_3, value);
	}

	inline static int32_t get_offset_of_languageManager_4() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t3188092760, ___languageManager_4)); }
	inline LanguageManager_t132697751 * get_languageManager_4() const { return ___languageManager_4; }
	inline LanguageManager_t132697751 ** get_address_of_languageManager_4() { return &___languageManager_4; }
	inline void set_languageManager_4(LanguageManager_t132697751 * value)
	{
		___languageManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___languageManager_4, value);
	}

	inline static int32_t get_offset_of_valuesScrollPosition_5() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t3188092760, ___valuesScrollPosition_5)); }
	inline Vector2_t2243707579  get_valuesScrollPosition_5() const { return ___valuesScrollPosition_5; }
	inline Vector2_t2243707579 * get_address_of_valuesScrollPosition_5() { return &___valuesScrollPosition_5; }
	inline void set_valuesScrollPosition_5(Vector2_t2243707579  value)
	{
		___valuesScrollPosition_5 = value;
	}

	inline static int32_t get_offset_of_languagesScrollPosition_6() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t3188092760, ___languagesScrollPosition_6)); }
	inline Vector2_t2243707579  get_languagesScrollPosition_6() const { return ___languagesScrollPosition_6; }
	inline Vector2_t2243707579 * get_address_of_languagesScrollPosition_6() { return &___languagesScrollPosition_6; }
	inline void set_languagesScrollPosition_6(Vector2_t2243707579  value)
	{
		___languagesScrollPosition_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
