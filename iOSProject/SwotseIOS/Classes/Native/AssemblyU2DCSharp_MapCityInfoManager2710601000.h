﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// WorldFieldModel
struct WorldFieldModel_t3469935653;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCityInfoManager
struct  MapCityInfoManager_t2710601000  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MapCityInfoManager::rank
	Text_t356221433 * ___rank_2;
	// UnityEngine.UI.Text MapCityInfoManager::experience
	Text_t356221433 * ___experience_3;
	// UnityEngine.UI.Text MapCityInfoManager::region
	Text_t356221433 * ___region_4;
	// UnityEngine.UI.Text MapCityInfoManager::coords
	Text_t356221433 * ___coords_5;
	// UnityEngine.UI.Text MapCityInfoManager::alliance
	Text_t356221433 * ___alliance_6;
	// UnityEngine.UI.Text MapCityInfoManager::status
	Text_t356221433 * ___status_7;
	// UnityEngine.UI.Text MapCityInfoManager::cityName
	Text_t356221433 * ___cityName_8;
	// UnityEngine.UI.Text MapCityInfoManager::playerName
	Text_t356221433 * ___playerName_9;
	// UnityEngine.UI.Text MapCityInfoManager::occupationLabel
	Text_t356221433 * ___occupationLabel_10;
	// UnityEngine.GameObject MapCityInfoManager::marchBtn
	GameObject_t1756533147 * ___marchBtn_11;
	// UnityEngine.GameObject MapCityInfoManager::revoltBtn
	GameObject_t1756533147 * ___revoltBtn_12;
	// UnityEngine.GameObject MapCityInfoManager::nominateBtn
	GameObject_t1756533147 * ___nominateBtn_13;
	// UnityEngine.GameObject MapCityInfoManager::shopBtn
	GameObject_t1756533147 * ___shopBtn_14;
	// UnityEngine.GameObject MapCityInfoManager::giftBtn
	GameObject_t1756533147 * ___giftBtn_15;
	// UnityEngine.UI.Image MapCityInfoManager::playerImage
	Image_t2042527209 * ___playerImage_16;
	// UnityEngine.UI.Image MapCityInfoManager::cityImage
	Image_t2042527209 * ___cityImage_17;
	// WorldFieldModel MapCityInfoManager::field
	WorldFieldModel_t3469935653 * ___field_18;

public:
	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___rank_2)); }
	inline Text_t356221433 * get_rank_2() const { return ___rank_2; }
	inline Text_t356221433 ** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(Text_t356221433 * value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier(&___rank_2, value);
	}

	inline static int32_t get_offset_of_experience_3() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___experience_3)); }
	inline Text_t356221433 * get_experience_3() const { return ___experience_3; }
	inline Text_t356221433 ** get_address_of_experience_3() { return &___experience_3; }
	inline void set_experience_3(Text_t356221433 * value)
	{
		___experience_3 = value;
		Il2CppCodeGenWriteBarrier(&___experience_3, value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___region_4)); }
	inline Text_t356221433 * get_region_4() const { return ___region_4; }
	inline Text_t356221433 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t356221433 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier(&___region_4, value);
	}

	inline static int32_t get_offset_of_coords_5() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___coords_5)); }
	inline Text_t356221433 * get_coords_5() const { return ___coords_5; }
	inline Text_t356221433 ** get_address_of_coords_5() { return &___coords_5; }
	inline void set_coords_5(Text_t356221433 * value)
	{
		___coords_5 = value;
		Il2CppCodeGenWriteBarrier(&___coords_5, value);
	}

	inline static int32_t get_offset_of_alliance_6() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___alliance_6)); }
	inline Text_t356221433 * get_alliance_6() const { return ___alliance_6; }
	inline Text_t356221433 ** get_address_of_alliance_6() { return &___alliance_6; }
	inline void set_alliance_6(Text_t356221433 * value)
	{
		___alliance_6 = value;
		Il2CppCodeGenWriteBarrier(&___alliance_6, value);
	}

	inline static int32_t get_offset_of_status_7() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___status_7)); }
	inline Text_t356221433 * get_status_7() const { return ___status_7; }
	inline Text_t356221433 ** get_address_of_status_7() { return &___status_7; }
	inline void set_status_7(Text_t356221433 * value)
	{
		___status_7 = value;
		Il2CppCodeGenWriteBarrier(&___status_7, value);
	}

	inline static int32_t get_offset_of_cityName_8() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___cityName_8)); }
	inline Text_t356221433 * get_cityName_8() const { return ___cityName_8; }
	inline Text_t356221433 ** get_address_of_cityName_8() { return &___cityName_8; }
	inline void set_cityName_8(Text_t356221433 * value)
	{
		___cityName_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_8, value);
	}

	inline static int32_t get_offset_of_playerName_9() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___playerName_9)); }
	inline Text_t356221433 * get_playerName_9() const { return ___playerName_9; }
	inline Text_t356221433 ** get_address_of_playerName_9() { return &___playerName_9; }
	inline void set_playerName_9(Text_t356221433 * value)
	{
		___playerName_9 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_9, value);
	}

	inline static int32_t get_offset_of_occupationLabel_10() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___occupationLabel_10)); }
	inline Text_t356221433 * get_occupationLabel_10() const { return ___occupationLabel_10; }
	inline Text_t356221433 ** get_address_of_occupationLabel_10() { return &___occupationLabel_10; }
	inline void set_occupationLabel_10(Text_t356221433 * value)
	{
		___occupationLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___occupationLabel_10, value);
	}

	inline static int32_t get_offset_of_marchBtn_11() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___marchBtn_11)); }
	inline GameObject_t1756533147 * get_marchBtn_11() const { return ___marchBtn_11; }
	inline GameObject_t1756533147 ** get_address_of_marchBtn_11() { return &___marchBtn_11; }
	inline void set_marchBtn_11(GameObject_t1756533147 * value)
	{
		___marchBtn_11 = value;
		Il2CppCodeGenWriteBarrier(&___marchBtn_11, value);
	}

	inline static int32_t get_offset_of_revoltBtn_12() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___revoltBtn_12)); }
	inline GameObject_t1756533147 * get_revoltBtn_12() const { return ___revoltBtn_12; }
	inline GameObject_t1756533147 ** get_address_of_revoltBtn_12() { return &___revoltBtn_12; }
	inline void set_revoltBtn_12(GameObject_t1756533147 * value)
	{
		___revoltBtn_12 = value;
		Il2CppCodeGenWriteBarrier(&___revoltBtn_12, value);
	}

	inline static int32_t get_offset_of_nominateBtn_13() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___nominateBtn_13)); }
	inline GameObject_t1756533147 * get_nominateBtn_13() const { return ___nominateBtn_13; }
	inline GameObject_t1756533147 ** get_address_of_nominateBtn_13() { return &___nominateBtn_13; }
	inline void set_nominateBtn_13(GameObject_t1756533147 * value)
	{
		___nominateBtn_13 = value;
		Il2CppCodeGenWriteBarrier(&___nominateBtn_13, value);
	}

	inline static int32_t get_offset_of_shopBtn_14() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___shopBtn_14)); }
	inline GameObject_t1756533147 * get_shopBtn_14() const { return ___shopBtn_14; }
	inline GameObject_t1756533147 ** get_address_of_shopBtn_14() { return &___shopBtn_14; }
	inline void set_shopBtn_14(GameObject_t1756533147 * value)
	{
		___shopBtn_14 = value;
		Il2CppCodeGenWriteBarrier(&___shopBtn_14, value);
	}

	inline static int32_t get_offset_of_giftBtn_15() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___giftBtn_15)); }
	inline GameObject_t1756533147 * get_giftBtn_15() const { return ___giftBtn_15; }
	inline GameObject_t1756533147 ** get_address_of_giftBtn_15() { return &___giftBtn_15; }
	inline void set_giftBtn_15(GameObject_t1756533147 * value)
	{
		___giftBtn_15 = value;
		Il2CppCodeGenWriteBarrier(&___giftBtn_15, value);
	}

	inline static int32_t get_offset_of_playerImage_16() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___playerImage_16)); }
	inline Image_t2042527209 * get_playerImage_16() const { return ___playerImage_16; }
	inline Image_t2042527209 ** get_address_of_playerImage_16() { return &___playerImage_16; }
	inline void set_playerImage_16(Image_t2042527209 * value)
	{
		___playerImage_16 = value;
		Il2CppCodeGenWriteBarrier(&___playerImage_16, value);
	}

	inline static int32_t get_offset_of_cityImage_17() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___cityImage_17)); }
	inline Image_t2042527209 * get_cityImage_17() const { return ___cityImage_17; }
	inline Image_t2042527209 ** get_address_of_cityImage_17() { return &___cityImage_17; }
	inline void set_cityImage_17(Image_t2042527209 * value)
	{
		___cityImage_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityImage_17, value);
	}

	inline static int32_t get_offset_of_field_18() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t2710601000, ___field_18)); }
	inline WorldFieldModel_t3469935653 * get_field_18() const { return ___field_18; }
	inline WorldFieldModel_t3469935653 ** get_address_of_field_18() { return &___field_18; }
	inline void set_field_18(WorldFieldModel_t3469935653 * value)
	{
		___field_18 = value;
		Il2CppCodeGenWriteBarrier(&___field_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
