﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.String
struct String_t;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.Socket
struct Socket_t3821512045;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_Sockets_Socket3821512045.h"

// System.Void System.Net.Sockets.UdpClient::.ctor()
extern "C"  void UdpClient__ctor_m1976824576 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Net.Sockets.AddressFamily)
extern "C"  void UdpClient__ctor_m3797562890 (UdpClient_t1278197702 * __this, int32_t ___family0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Int32)
extern "C"  void UdpClient__ctor_m2819550893 (UdpClient_t1278197702 * __this, int32_t ___port0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Net.IPEndPoint)
extern "C"  void UdpClient__ctor_m881476380 (UdpClient_t1278197702 * __this, IPEndPoint_t2615413766 * ___localEP0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Int32,System.Net.Sockets.AddressFamily)
extern "C"  void UdpClient__ctor_m103334171 (UdpClient_t1278197702 * __this, int32_t ___port0, int32_t ___family1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.String,System.Int32)
extern "C"  void UdpClient__ctor_m1279882271 (UdpClient_t1278197702 * __this, String_t* ___hostname0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::System.IDisposable.Dispose()
extern "C"  void UdpClient_System_IDisposable_Dispose_m3664529459 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::InitSocket(System.Net.EndPoint)
extern "C"  void UdpClient_InitSocket_m3011230328 (UdpClient_t1278197702 * __this, EndPoint_t4156119363 * ___localEP0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Close()
extern "C"  void UdpClient_Close_m4158802552 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::DoConnect(System.Net.IPEndPoint)
extern "C"  void UdpClient_DoConnect_m3716433165 (UdpClient_t1278197702 * __this, IPEndPoint_t2615413766 * ___endPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Connect(System.Net.IPEndPoint)
extern "C"  void UdpClient_Connect_m515177034 (UdpClient_t1278197702 * __this, IPEndPoint_t2615413766 * ___endPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Connect(System.Net.IPAddress,System.Int32)
extern "C"  void UdpClient_Connect_m1641905240 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___addr0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Connect(System.String,System.Int32)
extern "C"  void UdpClient_Connect_m691295425 (UdpClient_t1278197702 * __this, String_t* ___hostname0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::DropMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_DropMulticastGroup_m1415384411 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___multicastAddr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::DropMulticastGroup(System.Net.IPAddress,System.Int32)
extern "C"  void UdpClient_DropMulticastGroup_m1007426188 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___multicastAddr0, int32_t ___ifindex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_JoinMulticastGroup_m3983752416 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___multicastAddr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Int32,System.Net.IPAddress)
extern "C"  void UdpClient_JoinMulticastGroup_m100310405 (UdpClient_t1278197702 * __this, int32_t ___ifindex0, IPAddress_t1399971723 * ___multicastAddr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Net.IPAddress,System.Int32)
extern "C"  void UdpClient_JoinMulticastGroup_m3096058245 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___multicastAddr0, int32_t ___timeToLive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Net.IPAddress,System.Net.IPAddress)
extern "C"  void UdpClient_JoinMulticastGroup_m265148353 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * ___multicastAddr0, IPAddress_t1399971723 * ___localAddress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.UdpClient::Receive(System.Net.IPEndPoint&)
extern "C"  ByteU5BU5D_t3397334013* UdpClient_Receive_m1064940409 (UdpClient_t1278197702 * __this, IPEndPoint_t2615413766 ** ___remoteEP0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::DoSend(System.Byte[],System.Int32,System.Net.IPEndPoint)
extern "C"  int32_t UdpClient_DoSend_m3464086495 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___dgram0, int32_t ___bytes1, IPEndPoint_t2615413766 * ___endPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::Send(System.Byte[],System.Int32)
extern "C"  int32_t UdpClient_Send_m1494798914 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___dgram0, int32_t ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::Send(System.Byte[],System.Int32,System.Net.IPEndPoint)
extern "C"  int32_t UdpClient_Send_m3460719652 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___dgram0, int32_t ___bytes1, IPEndPoint_t2615413766 * ___endPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::Send(System.Byte[],System.Int32,System.String,System.Int32)
extern "C"  int32_t UdpClient_Send_m2178198043 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___dgram0, int32_t ___bytes1, String_t* ___hostname2, int32_t ___port3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.UdpClient::CutArray(System.Byte[],System.Int32)
extern "C"  ByteU5BU5D_t3397334013* UdpClient_CutArray_m3570065513 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___orig0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::DoBeginSend(System.Byte[],System.Int32,System.Net.IPEndPoint,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UdpClient_DoBeginSend_m13411394 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___datagram0, int32_t ___bytes1, IPEndPoint_t2615413766 * ___endPoint2, AsyncCallback_t163412349 * ___requestCallback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginSend(System.Byte[],System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UdpClient_BeginSend_m287746597 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___datagram0, int32_t ___bytes1, AsyncCallback_t163412349 * ___requestCallback2, Il2CppObject * ___state3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginSend(System.Byte[],System.Int32,System.Net.IPEndPoint,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UdpClient_BeginSend_m2593542257 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___datagram0, int32_t ___bytes1, IPEndPoint_t2615413766 * ___endPoint2, AsyncCallback_t163412349 * ___requestCallback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginSend(System.Byte[],System.Int32,System.String,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UdpClient_BeginSend_m1472494034 (UdpClient_t1278197702 * __this, ByteU5BU5D_t3397334013* ___datagram0, int32_t ___bytes1, String_t* ___hostname2, int32_t ___port3, AsyncCallback_t163412349 * ___requestCallback4, Il2CppObject * ___state5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::EndSend(System.IAsyncResult)
extern "C"  int32_t UdpClient_EndSend_m789380296 (UdpClient_t1278197702 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginReceive(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UdpClient_BeginReceive_m3219495788 (UdpClient_t1278197702 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.UdpClient::EndReceive(System.IAsyncResult,System.Net.IPEndPoint&)
extern "C"  ByteU5BU5D_t3397334013* UdpClient_EndReceive_m1059013247 (UdpClient_t1278197702 * __this, Il2CppObject * ___asyncResult0, IPEndPoint_t2615413766 ** ___remoteEP1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.UdpClient::get_Active()
extern "C"  bool UdpClient_get_Active_m1319652965 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_Active(System.Boolean)
extern "C"  void UdpClient_set_Active_m2170165176 (UdpClient_t1278197702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket System.Net.Sockets.UdpClient::get_Client()
extern "C"  Socket_t3821512045 * UdpClient_get_Client_m4293315388 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_Client(System.Net.Sockets.Socket)
extern "C"  void UdpClient_set_Client_m3629597759 (UdpClient_t1278197702 * __this, Socket_t3821512045 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.UdpClient::get_Available()
extern "C"  int32_t UdpClient_get_Available_m352590560 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.UdpClient::get_DontFragment()
extern "C"  bool UdpClient_get_DontFragment_m3458513272 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_DontFragment(System.Boolean)
extern "C"  void UdpClient_set_DontFragment_m1487417727 (UdpClient_t1278197702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.UdpClient::get_EnableBroadcast()
extern "C"  bool UdpClient_get_EnableBroadcast_m642459323 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_EnableBroadcast(System.Boolean)
extern "C"  void UdpClient_set_EnableBroadcast_m1365689764 (UdpClient_t1278197702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.UdpClient::get_ExclusiveAddressUse()
extern "C"  bool UdpClient_get_ExclusiveAddressUse_m1557760044 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_ExclusiveAddressUse(System.Boolean)
extern "C"  void UdpClient_set_ExclusiveAddressUse_m3122656589 (UdpClient_t1278197702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.UdpClient::get_MulticastLoopback()
extern "C"  bool UdpClient_get_MulticastLoopback_m1657617812 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_MulticastLoopback(System.Boolean)
extern "C"  void UdpClient_set_MulticastLoopback_m3592888825 (UdpClient_t1278197702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Net.Sockets.UdpClient::get_Ttl()
extern "C"  int16_t UdpClient_get_Ttl_m3677237045 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_Ttl(System.Int16)
extern "C"  void UdpClient_set_Ttl_m2635444808 (UdpClient_t1278197702 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Dispose(System.Boolean)
extern "C"  void UdpClient_Dispose_m2051482172 (UdpClient_t1278197702 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Finalize()
extern "C"  void UdpClient_Finalize_m3567066892 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::CheckDisposed()
extern "C"  void UdpClient_CheckDisposed_m705034929 (UdpClient_t1278197702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
