﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<PrintAsync>c__Iterator2
struct U3CPrintAsyncU3Ec__Iterator2_t716304654;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<PrintAsync>c__Iterator2::.ctor()
extern "C"  void U3CPrintAsyncU3Ec__Iterator2__ctor_m912913703 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject/<PrintAsync>c__Iterator2::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m3728394825 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<PrintAsync>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1028168001 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m2067350732 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<PrintAsync>c__Iterator2::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m4268902500 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<PrintAsync>c__Iterator2::MoveNext()
extern "C"  bool U3CPrintAsyncU3Ec__Iterator2_MoveNext_m836203705 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<PrintAsync>c__Iterator2::Dispose()
extern "C"  void U3CPrintAsyncU3Ec__Iterator2_Dispose_m466863592 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<PrintAsync>c__Iterator2::Reset()
extern "C"  void U3CPrintAsyncU3Ec__Iterator2_Reset_m1792243642 (U3CPrintAsyncU3Ec__Iterator2_t716304654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
