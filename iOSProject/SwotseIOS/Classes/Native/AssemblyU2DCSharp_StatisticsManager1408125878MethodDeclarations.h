﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatisticsManager
struct StatisticsManager_t1408125878;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void StatisticsManager::.ctor()
extern "C"  void StatisticsManager__ctor_m3693090569 (StatisticsManager_t1408125878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatisticsManager::OnEnable()
extern "C"  void StatisticsManager_OnEnable_m2468835281 (StatisticsManager_t1408125878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatisticsManager::FixedUpdate()
extern "C"  void StatisticsManager_FixedUpdate_m3008974778 (StatisticsManager_t1408125878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatisticsManager::closePanel(System.String)
extern "C"  void StatisticsManager_closePanel_m1398488235 (StatisticsManager_t1408125878 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
