﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/ResolveCallback
struct ResolveCallback_t3862612227;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.Dns/ResolveCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ResolveCallback__ctor_m2455807187 (ResolveCallback_t3862612227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/ResolveCallback::Invoke(System.String)
extern "C"  IPHostEntry_t994738509 * ResolveCallback_Invoke_m3392889217 (ResolveCallback_t3862612227 * __this, String_t* ___hostName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Dns/ResolveCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResolveCallback_BeginInvoke_m2459201748 (ResolveCallback_t3862612227 * __this, String_t* ___hostName0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/ResolveCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t994738509 * ResolveCallback_EndInvoke_m558111749 (ResolveCallback_t3862612227 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
