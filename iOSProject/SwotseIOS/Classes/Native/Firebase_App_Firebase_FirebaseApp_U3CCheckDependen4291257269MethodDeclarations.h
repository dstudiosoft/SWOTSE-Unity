﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey5
struct U3CCheckDependenciesU3Ec__AnonStorey5_t4291257269;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey5::.ctor()
extern "C"  void U3CCheckDependenciesU3Ec__AnonStorey5__ctor_m2814250388 (U3CCheckDependenciesU3Ec__AnonStorey5_t4291257269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey5::<>m__0()
extern "C"  void U3CCheckDependenciesU3Ec__AnonStorey5_U3CU3Em__0_m2781875745 (U3CCheckDependenciesU3Ec__AnonStorey5_t4291257269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
