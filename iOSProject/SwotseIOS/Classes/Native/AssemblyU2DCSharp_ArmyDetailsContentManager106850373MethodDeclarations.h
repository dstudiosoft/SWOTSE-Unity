﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArmyDetailsContentManager
struct ArmyDetailsContentManager_t106850373;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ArmyDetailsContentManager::.ctor()
extern "C"  void ArmyDetailsContentManager__ctor_m100085508 (ArmyDetailsContentManager_t106850373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArmyDetailsContentManager::Start()
extern "C"  void ArmyDetailsContentManager_Start_m2944528012 (ArmyDetailsContentManager_t106850373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArmyDetailsContentManager::ReturnArmy()
extern "C"  void ArmyDetailsContentManager_ReturnArmy_m2236833153 (ArmyDetailsContentManager_t106850373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArmyDetailsContentManager::ArmyReturnSet(System.Object,System.String)
extern "C"  void ArmyDetailsContentManager_ArmyReturnSet_m1421734195 (ArmyDetailsContentManager_t106850373 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
