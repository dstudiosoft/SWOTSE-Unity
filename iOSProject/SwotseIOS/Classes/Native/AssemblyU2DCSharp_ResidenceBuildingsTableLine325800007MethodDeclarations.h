﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceBuildingsTableLine
struct ResidenceBuildingsTableLine_t325800007;
// ResidenceBuildingsTableController
struct ResidenceBuildingsTableController_t3217399565;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResidenceBuildingsTableControlle3217399565.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResidenceBuildingsTableLine::.ctor()
extern "C"  void ResidenceBuildingsTableLine__ctor_m4191346468 (ResidenceBuildingsTableLine_t325800007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetOwner(ResidenceBuildingsTableController)
extern "C"  void ResidenceBuildingsTableLine_SetOwner_m1311879176 (ResidenceBuildingsTableLine_t325800007 * __this, ResidenceBuildingsTableController_t3217399565 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetBuildingName(System.String)
extern "C"  void ResidenceBuildingsTableLine_SetBuildingName_m2033852939 (ResidenceBuildingsTableLine_t325800007 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetLevel(System.Int64)
extern "C"  void ResidenceBuildingsTableLine_SetLevel_m3360552988 (ResidenceBuildingsTableLine_t325800007 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetStatus(System.String)
extern "C"  void ResidenceBuildingsTableLine_SetStatus_m3833894664 (ResidenceBuildingsTableLine_t325800007 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetPitId(System.Int64)
extern "C"  void ResidenceBuildingsTableLine_SetPitId_m1963773642 (ResidenceBuildingsTableLine_t325800007 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::SetIcon(System.String)
extern "C"  void ResidenceBuildingsTableLine_SetIcon_m2570688025 (ResidenceBuildingsTableLine_t325800007 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableLine::OpenBuildingWindow()
extern "C"  void ResidenceBuildingsTableLine_OpenBuildingWindow_m57030318 (ResidenceBuildingsTableLine_t325800007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
