﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String SmartLocalization.LanguageRuntimeData::LanguageFilePath(System.String)
extern "C"  String_t* LanguageRuntimeData_LanguageFilePath_m962612918 (Il2CppObject * __this /* static, unused */, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFilePath()
extern "C"  String_t* LanguageRuntimeData_AvailableCulturesFilePath_m1674437834 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::AudioFilesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_AudioFilesFolderPath_m2499437907 (Il2CppObject * __this /* static, unused */, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_TexturesFolderPath_m2077609754 (Il2CppObject * __this /* static, unused */, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderPath(System.String)
extern "C"  String_t* LanguageRuntimeData_PrefabsFolderPath_m4293288429 (Il2CppObject * __this /* static, unused */, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageRuntimeData::.cctor()
extern "C"  void LanguageRuntimeData__cctor_m613625231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
