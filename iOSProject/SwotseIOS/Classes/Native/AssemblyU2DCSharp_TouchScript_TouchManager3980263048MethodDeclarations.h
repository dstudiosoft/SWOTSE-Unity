﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.TouchManager
struct TouchManager_t3980263048;
// TouchScript.ITouchManager
struct ITouchManager_t2552034033;
// TouchScript.Devices.Display.IDisplayDevice
struct IDisplayDevice_t2646363379;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// TouchScript.TouchEventArgs
struct TouchEventArgs_t1917927166;
// System.EventArgs
struct EventArgs_t3289624707;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchManager_Message1628650752.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_TouchScript_TouchEventArgs1917927166.h"
#include "mscorlib_System_EventArgs3289624707.h"

// System.Void TouchScript.TouchManager::.ctor()
extern "C"  void TouchManager__ctor_m2313113093 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::.cctor()
extern "C"  void TouchManager__cctor_m2551550966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.ITouchManager TouchScript.TouchManager::get_Instance()
extern "C"  Il2CppObject * TouchManager_get_Instance_m3038552737 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManager::get_DisplayDevice()
extern "C"  Il2CppObject * TouchManager_get_DisplayDevice_m299787511 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
extern "C"  void TouchManager_set_DisplayDevice_m1338182972 (TouchManager_t3980263048 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManager::get_ShouldCreateCameraLayer()
extern "C"  bool TouchManager_get_ShouldCreateCameraLayer_m3690778879 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_ShouldCreateCameraLayer(System.Boolean)
extern "C"  void TouchManager_set_ShouldCreateCameraLayer_m3743099984 (TouchManager_t3980263048 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManager::get_ShouldCreateStandardInput()
extern "C"  bool TouchManager_get_ShouldCreateStandardInput_m3659875252 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_ShouldCreateStandardInput(System.Boolean)
extern "C"  void TouchManager_set_ShouldCreateStandardInput_m3595118167 (TouchManager_t3980263048 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManager::get_UseSendMessage()
extern "C"  bool TouchManager_get_UseSendMessage_m3191970908 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_UseSendMessage(System.Boolean)
extern "C"  void TouchManager_set_UseSendMessage_m1096575199 (TouchManager_t3980263048 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchManager/MessageType TouchScript.TouchManager::get_SendMessageEvents()
extern "C"  int32_t TouchManager_get_SendMessageEvents_m256970969 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_SendMessageEvents(TouchScript.TouchManager/MessageType)
extern "C"  void TouchManager_set_SendMessageEvents_m1812499280 (TouchManager_t3980263048 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TouchScript.TouchManager::get_SendMessageTarget()
extern "C"  GameObject_t1756533147 * TouchManager_get_SendMessageTarget_m126511337 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::set_SendMessageTarget(UnityEngine.GameObject)
extern "C"  void TouchManager_set_SendMessageTarget_m1438009350 (TouchManager_t3980263048 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchManager::IsInvalidPosition(UnityEngine.Vector2)
extern "C"  bool TouchManager_IsInvalidPosition_m3237080995 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::Awake()
extern "C"  void TouchManager_Awake_m424697024 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::OnEnable()
extern "C"  void TouchManager_OnEnable_m1591834885 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::OnDisable()
extern "C"  void TouchManager_OnDisable_m2046803728 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::updateSubscription()
extern "C"  void TouchManager_updateSubscription_m4226836499 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::removeSubscriptions()
extern "C"  void TouchManager_removeSubscriptions_m2026794903 (TouchManager_t3980263048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::touchesBeganHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchManager_touchesBeganHandler_m368779193 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::touchesMovedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchManager_touchesMovedHandler_m2659954751 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::touchesEndedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchManager_touchesEndedHandler_m3368006524 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::touchesCancelledHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchManager_touchesCancelledHandler_m2847849197 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::frameStartedHandler(System.Object,System.EventArgs)
extern "C"  void TouchManager_frameStartedHandler_m2734077407 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchManager::frameFinishedHandler(System.Object,System.EventArgs)
extern "C"  void TouchManager_frameFinishedHandler_m1108599098 (TouchManager_t3980263048 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
