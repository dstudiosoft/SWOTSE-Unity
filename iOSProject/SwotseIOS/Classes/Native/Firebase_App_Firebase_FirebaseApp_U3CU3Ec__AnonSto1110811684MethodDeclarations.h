﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<>c__AnonStorey3
struct U3CU3Ec__AnonStorey3_t1110811684;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/<>c__AnonStorey3::.ctor()
extern "C"  void U3CU3Ec__AnonStorey3__ctor_m3189176579 (U3CU3Ec__AnonStorey3_t1110811684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<>c__AnonStorey3::<>m__0()
extern "C"  void U3CU3Ec__AnonStorey3_U3CU3Em__0_m2624841966 (U3CU3Ec__AnonStorey3_t1110811684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
