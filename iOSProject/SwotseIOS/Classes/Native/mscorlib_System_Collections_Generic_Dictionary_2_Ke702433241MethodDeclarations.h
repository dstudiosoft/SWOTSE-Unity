﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>
struct KeyCollection_t702433241;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t4274551732;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke908438908.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m323559081_gshared (KeyCollection_t702433241 * __this, Dictionary_2_t2513902766 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m323559081(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t702433241 *, Dictionary_2_t2513902766 *, const MethodInfo*))KeyCollection__ctor_m323559081_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331(__this, ___item0, method) ((  void (*) (KeyCollection_t702433241 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144(__this, method) ((  void (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m164383913_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m164383913(__this, ___item0, method) ((  bool (*) (KeyCollection_t702433241 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m164383913_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472(__this, ___item0, method) ((  bool (*) (KeyCollection_t702433241 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m228846932_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m228846932(__this, method) ((  Il2CppObject* (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m228846932_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m748083542_gshared (KeyCollection_t702433241 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m748083542(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t702433241 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m748083542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1236899363_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1236899363(__this, method) ((  Il2CppObject * (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1236899363_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m547843110_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m547843110(__this, method) ((  bool (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m547843110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4009966266_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4009966266(__this, method) ((  bool (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4009966266_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068(__this, method) ((  Il2CppObject * (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4123737228_gshared (KeyCollection_t702433241 * __this, IntPtrU5BU5D_t169632028* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4123737228(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t702433241 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4123737228_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t908438908  KeyCollection_GetEnumerator_m2489792873_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2489792873(__this, method) ((  Enumerator_t908438908  (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_GetEnumerator_m2489792873_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2256401266_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2256401266(__this, method) ((  int32_t (*) (KeyCollection_t702433241 *, const MethodInfo*))KeyCollection_get_Count_m2256401266_gshared)(__this, method)
