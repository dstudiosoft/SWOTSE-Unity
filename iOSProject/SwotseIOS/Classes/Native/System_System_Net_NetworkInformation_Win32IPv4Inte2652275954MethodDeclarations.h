﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPv4InterfaceStatistics
struct Win32IPv4InterfaceStatistics_t2652275954;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"

// System.Void System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::.ctor(System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPv4InterfaceStatistics__ctor_m2916876138 (Win32IPv4InterfaceStatistics_t2652275954 * __this, Win32_MIB_IFROW_t4215928996  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_BytesReceived()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_BytesReceived_m3678151446 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_BytesSent()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_BytesSent_m2353998263 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_IncomingPacketsDiscarded()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_IncomingPacketsDiscarded_m1067883588 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_IncomingPacketsWithErrors()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_IncomingPacketsWithErrors_m3212885416 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_IncomingUnknownProtocolPackets()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_IncomingUnknownProtocolPackets_m2143908429 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_NonUnicastPacketsReceived()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_NonUnicastPacketsReceived_m2765845752 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_NonUnicastPacketsSent()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_NonUnicastPacketsSent_m1557969429 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_OutgoingPacketsDiscarded()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_OutgoingPacketsDiscarded_m3713638782 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_OutgoingPacketsWithErrors()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_OutgoingPacketsWithErrors_m685160634 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_OutputQueueLength()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_OutputQueueLength_m2330033006 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_UnicastPacketsReceived()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_UnicastPacketsReceived_m3349911229 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::get_UnicastPacketsSent()
extern "C"  int64_t Win32IPv4InterfaceStatistics_get_UnicastPacketsSent_m2788835916 (Win32IPv4InterfaceStatistics_t2652275954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
