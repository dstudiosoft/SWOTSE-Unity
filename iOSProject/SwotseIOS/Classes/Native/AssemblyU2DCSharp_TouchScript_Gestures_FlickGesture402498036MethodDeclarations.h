﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.FlickGesture
struct FlickGesture_t402498036;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_FlickGestur1726119045.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void TouchScript.Gestures.FlickGesture::.ctor()
extern "C"  void FlickGesture__ctor_m4018797781 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::add_Flicked(System.EventHandler`1<System.EventArgs>)
extern "C"  void FlickGesture_add_Flicked_m4207897645 (FlickGesture_t402498036 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::remove_Flicked(System.EventHandler`1<System.EventArgs>)
extern "C"  void FlickGesture_remove_Flicked_m1953303588 (FlickGesture_t402498036 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.FlickGesture::get_FlickTime()
extern "C"  float FlickGesture_get_FlickTime_m4705422 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_FlickTime(System.Single)
extern "C"  void FlickGesture_set_FlickTime_m171560299 (FlickGesture_t402498036 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.FlickGesture::get_MinDistance()
extern "C"  float FlickGesture_get_MinDistance_m471751215 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_MinDistance(System.Single)
extern "C"  void FlickGesture_set_MinDistance_m984060678 (FlickGesture_t402498036 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.FlickGesture::get_MovementThreshold()
extern "C"  float FlickGesture_get_MovementThreshold_m1000631544 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_MovementThreshold(System.Single)
extern "C"  void FlickGesture_set_MovementThreshold_m612080833 (FlickGesture_t402498036 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.FlickGesture/GestureDirection TouchScript.Gestures.FlickGesture::get_Direction()
extern "C"  int32_t FlickGesture_get_Direction_m2692618237 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_Direction(TouchScript.Gestures.FlickGesture/GestureDirection)
extern "C"  void FlickGesture_set_Direction_m183406470 (FlickGesture_t402498036 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::get_ScreenFlickVector()
extern "C"  Vector2_t2243707579  FlickGesture_get_ScreenFlickVector_m1388074061 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_ScreenFlickVector(UnityEngine.Vector2)
extern "C"  void FlickGesture_set_ScreenFlickVector_m144829726 (FlickGesture_t402498036 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.FlickGesture::get_ScreenFlickTime()
extern "C"  float FlickGesture_get_ScreenFlickTime_m4153514618 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::set_ScreenFlickTime(System.Single)
extern "C"  void FlickGesture_set_ScreenFlickTime_m2145158503 (FlickGesture_t402498036 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::LateUpdate()
extern "C"  void FlickGesture_LateUpdate_m1490939708 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void FlickGesture_touchesBegan_m3929224635 (FlickGesture_t402498036 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void FlickGesture_touchesMoved_m1246810125 (FlickGesture_t402498036 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void FlickGesture_touchesEnded_m1893652670 (FlickGesture_t402498036 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::onRecognized()
extern "C"  void FlickGesture_onRecognized_m3938567024 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.FlickGesture::reset()
extern "C"  void FlickGesture_reset_m1396612746 (FlickGesture_t402498036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
