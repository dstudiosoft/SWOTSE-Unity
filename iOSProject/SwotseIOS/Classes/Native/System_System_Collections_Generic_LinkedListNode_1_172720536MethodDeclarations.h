﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m2784846651(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t172720536 *, LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, const MethodInfo*))LinkedListNode_1__ctor_m231316848_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m2942822495(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t172720536 *, LinkedList_1_t1581322852 *, TableViewCell_t1276614623 *, LinkedListNode_1_t172720536 *, LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedListNode_1__ctor_m127450634_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::Detach()
#define LinkedListNode_1_Detach_m1240866511(__this, method) ((  void (*) (LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedListNode_1_Detach_m3110429670_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_List()
#define LinkedListNode_1_get_List_m3960387183(__this, method) ((  LinkedList_1_t1581322852 * (*) (LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedListNode_1_get_List_m2861065156_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_Previous()
#define LinkedListNode_1_get_Previous_m279258422(__this, method) ((  LinkedListNode_1_t172720536 * (*) (LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedListNode_1_get_Previous_m1837352519_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_Value()
#define LinkedListNode_1_get_Value_m3101072349(__this, method) ((  TableViewCell_t1276614623 * (*) (LinkedListNode_1_t172720536 *, const MethodInfo*))LinkedListNode_1_get_Value_m201900116_gshared)(__this, method)
