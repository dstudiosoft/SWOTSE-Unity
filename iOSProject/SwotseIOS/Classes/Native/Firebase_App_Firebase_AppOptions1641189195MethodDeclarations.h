﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppOptions
struct AppOptions_t1641189195;
// Firebase.AppOptionsInternal
struct AppOptionsInternal_t2434721572;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_AppOptionsInternal2434721572.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.AppOptions::.ctor()
extern "C"  void AppOptions__ctor_m3557761911 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::.ctor(Firebase.AppOptionsInternal)
extern "C"  void AppOptions__ctor_m154365616 (AppOptions_t1641189195 * __this, AppOptionsInternal_t2434721572 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::Dispose()
extern "C"  void AppOptions_Dispose_m287632940 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Firebase.AppOptions::get_DatabaseUrl()
extern "C"  Uri_t19570940 * AppOptions_get_DatabaseUrl_m241783396 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_DatabaseUrl(System.Uri)
extern "C"  void AppOptions_set_DatabaseUrl_m2720457657 (AppOptions_t1641189195 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::get_AppId()
extern "C"  String_t* AppOptions_get_AppId_m3389908543 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_AppId(System.String)
extern "C"  void AppOptions_set_AppId_m2396046502 (AppOptions_t1641189195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::get_ApiKey()
extern "C"  String_t* AppOptions_get_ApiKey_m2804760840 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_ApiKey(System.String)
extern "C"  void AppOptions_set_ApiKey_m3570896115 (AppOptions_t1641189195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::get_MessageSenderId()
extern "C"  String_t* AppOptions_get_MessageSenderId_m3254576708 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_MessageSenderId(System.String)
extern "C"  void AppOptions_set_MessageSenderId_m1713529105 (AppOptions_t1641189195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::get_StorageBucket()
extern "C"  String_t* AppOptions_get_StorageBucket_m2042381236 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_StorageBucket(System.String)
extern "C"  void AppOptions_set_StorageBucket_m4267975131 (AppOptions_t1641189195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::get_ProjectId()
extern "C"  String_t* AppOptions_get_ProjectId_m458797751 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::set_ProjectId(System.String)
extern "C"  void AppOptions_set_ProjectId_m4290255736 (AppOptions_t1641189195 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.AppOptionsInternal Firebase.AppOptions::ConvertToInternal()
extern "C"  AppOptionsInternal_t2434721572 * AppOptions_ConvertToInternal_m2052621818 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
