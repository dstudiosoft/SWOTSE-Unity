﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateBuildingContentManager
struct UpdateBuildingContentManager_t3210716219;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UpdateBuildingContentManager::.ctor()
extern "C"  void UpdateBuildingContentManager__ctor_m1525121880 (UpdateBuildingContentManager_t3210716219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateBuildingContentManager::OnEnable()
extern "C"  void UpdateBuildingContentManager_OnEnable_m524072124 (UpdateBuildingContentManager_t3210716219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateBuildingContentManager::FixedUpdate()
extern "C"  void UpdateBuildingContentManager_FixedUpdate_m1704486329 (UpdateBuildingContentManager_t3210716219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateBuildingContentManager::PerformAction()
extern "C"  void UpdateBuildingContentManager_PerformAction_m1723948045 (UpdateBuildingContentManager_t3210716219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateBuildingContentManager::ActionPeformed(System.Object,System.String)
extern "C"  void UpdateBuildingContentManager_ActionPeformed_m1900755564 (UpdateBuildingContentManager_t3210716219 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
