﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DontDestroyMe
struct DontDestroyMe_t3466757697;

#include "codegen/il2cpp-codegen.h"

// System.Void DontDestroyMe::.ctor()
extern "C"  void DontDestroyMe__ctor_m1675196156 (DontDestroyMe_t3466757697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DontDestroyMe::Start()
extern "C"  void DontDestroyMe_Start_m4067459036 (DontDestroyMe_t3466757697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
