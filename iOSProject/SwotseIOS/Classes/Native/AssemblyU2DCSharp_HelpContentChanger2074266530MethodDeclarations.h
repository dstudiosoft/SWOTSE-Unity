﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HelpContentChanger
struct HelpContentChanger_t2074266530;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HelpContentChanger::.ctor()
extern "C"  void HelpContentChanger__ctor_m1892085491 (HelpContentChanger_t2074266530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpContentChanger::Start()
extern "C"  void HelpContentChanger_Start_m232258735 (HelpContentChanger_t2074266530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpContentChanger::closeAllPanels()
extern "C"  void HelpContentChanger_closeAllPanels_m139610153 (HelpContentChanger_t2074266530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpContentChanger::resetAllSprites()
extern "C"  void HelpContentChanger_resetAllSprites_m3847679401 (HelpContentChanger_t2074266530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpContentChanger::openPanel(System.String)
extern "C"  void HelpContentChanger_openPanel_m3953463339 (HelpContentChanger_t2074266530 * __this, String_t* ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
