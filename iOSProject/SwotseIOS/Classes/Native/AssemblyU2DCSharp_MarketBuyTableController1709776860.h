﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MarketManager
struct MarketManager_t2047363881;
// MarketBuyTableLine
struct MarketBuyTableLine_t1445592330;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketBuyTableController
struct  MarketBuyTableController_t1709776860  : public MonoBehaviour_t1158329972
{
public:
	// MarketManager MarketBuyTableController::owner
	MarketManager_t2047363881 * ___owner_2;
	// MarketBuyTableLine MarketBuyTableController::m_cellPrefab
	MarketBuyTableLine_t1445592330 * ___m_cellPrefab_3;
	// Tacticsoft.TableView MarketBuyTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_4;
	// System.Collections.ArrayList MarketBuyTableController::lots
	ArrayList_t4252133567 * ___lots_5;
	// System.Int32 MarketBuyTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t1709776860, ___owner_2)); }
	inline MarketManager_t2047363881 * get_owner_2() const { return ___owner_2; }
	inline MarketManager_t2047363881 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(MarketManager_t2047363881 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t1709776860, ___m_cellPrefab_3)); }
	inline MarketBuyTableLine_t1445592330 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline MarketBuyTableLine_t1445592330 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(MarketBuyTableLine_t1445592330 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t1709776860, ___m_tableView_4)); }
	inline TableView_t3179510217 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t3179510217 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_4, value);
	}

	inline static int32_t get_offset_of_lots_5() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t1709776860, ___lots_5)); }
	inline ArrayList_t4252133567 * get_lots_5() const { return ___lots_5; }
	inline ArrayList_t4252133567 ** get_address_of_lots_5() { return &___lots_5; }
	inline void set_lots_5(ArrayList_t4252133567 * value)
	{
		___lots_5 = value;
		Il2CppCodeGenWriteBarrier(&___lots_5, value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t1709776860, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
