﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FinancialContentChanger
struct FinancialContentChanger_t2753898366;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// FinancialTableLine
struct FinancialTableLine_t2609388451;
// Tacticsoft.TableView
struct TableView_t3179510217;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinanceTableController
struct  FinanceTableController_t1083841530  : public MonoBehaviour_t1158329972
{
public:
	// FinancialContentChanger FinanceTableController::owner
	FinancialContentChanger_t2753898366 * ___owner_2;
	// System.Collections.ArrayList FinanceTableController::currentSourceArray
	ArrayList_t4252133567 * ___currentSourceArray_3;
	// FinancialTableLine FinanceTableController::m_cellPrefab
	FinancialTableLine_t2609388451 * ___m_cellPrefab_4;
	// Tacticsoft.TableView FinanceTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_5;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(FinanceTableController_t1083841530, ___owner_2)); }
	inline FinancialContentChanger_t2753898366 * get_owner_2() const { return ___owner_2; }
	inline FinancialContentChanger_t2753898366 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(FinancialContentChanger_t2753898366 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}

	inline static int32_t get_offset_of_currentSourceArray_3() { return static_cast<int32_t>(offsetof(FinanceTableController_t1083841530, ___currentSourceArray_3)); }
	inline ArrayList_t4252133567 * get_currentSourceArray_3() const { return ___currentSourceArray_3; }
	inline ArrayList_t4252133567 ** get_address_of_currentSourceArray_3() { return &___currentSourceArray_3; }
	inline void set_currentSourceArray_3(ArrayList_t4252133567 * value)
	{
		___currentSourceArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentSourceArray_3, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_4() { return static_cast<int32_t>(offsetof(FinanceTableController_t1083841530, ___m_cellPrefab_4)); }
	inline FinancialTableLine_t2609388451 * get_m_cellPrefab_4() const { return ___m_cellPrefab_4; }
	inline FinancialTableLine_t2609388451 ** get_address_of_m_cellPrefab_4() { return &___m_cellPrefab_4; }
	inline void set_m_cellPrefab_4(FinancialTableLine_t2609388451 * value)
	{
		___m_cellPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_4, value);
	}

	inline static int32_t get_offset_of_m_tableView_5() { return static_cast<int32_t>(offsetof(FinanceTableController_t1083841530, ___m_tableView_5)); }
	inline TableView_t3179510217 * get_m_tableView_5() const { return ___m_tableView_5; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_5() { return &___m_tableView_5; }
	inline void set_m_tableView_5(TableView_t3179510217 * value)
	{
		___m_tableView_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
