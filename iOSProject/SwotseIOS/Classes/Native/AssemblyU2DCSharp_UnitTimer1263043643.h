﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitTimer
struct  UnitTimer_t1263043643  : public Il2CppObject
{
public:
	// System.Int64 UnitTimer::id
	int64_t ___id_0;
	// System.String UnitTimer::unit
	String_t* ___unit_1;
	// System.Int64 UnitTimer::event_id
	int64_t ___event_id_2;
	// System.DateTime UnitTimer::start_time
	DateTime_t693205669  ___start_time_3;
	// System.DateTime UnitTimer::finish_time
	DateTime_t693205669  ___finish_time_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UnitTimer_t1263043643, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(UnitTimer_t1263043643, ___unit_1)); }
	inline String_t* get_unit_1() const { return ___unit_1; }
	inline String_t** get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(String_t* value)
	{
		___unit_1 = value;
		Il2CppCodeGenWriteBarrier(&___unit_1, value);
	}

	inline static int32_t get_offset_of_event_id_2() { return static_cast<int32_t>(offsetof(UnitTimer_t1263043643, ___event_id_2)); }
	inline int64_t get_event_id_2() const { return ___event_id_2; }
	inline int64_t* get_address_of_event_id_2() { return &___event_id_2; }
	inline void set_event_id_2(int64_t value)
	{
		___event_id_2 = value;
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(UnitTimer_t1263043643, ___start_time_3)); }
	inline DateTime_t693205669  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t693205669 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t693205669  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(UnitTimer_t1263043643, ___finish_time_4)); }
	inline DateTime_t693205669  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t693205669 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t693205669  value)
	{
		___finish_time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
