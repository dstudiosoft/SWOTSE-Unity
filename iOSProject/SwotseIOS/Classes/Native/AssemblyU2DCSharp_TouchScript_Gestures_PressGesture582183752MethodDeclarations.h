﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.PressGesture
struct PressGesture_t582183752;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture2352305985.h"

// System.Void TouchScript.Gestures.PressGesture::.ctor()
extern "C"  void PressGesture__ctor_m4070326877 (PressGesture_t582183752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PressGesture::add_Pressed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PressGesture_add_Pressed_m1759971757 (PressGesture_t582183752 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PressGesture::remove_Pressed(System.EventHandler`1<System.EventArgs>)
extern "C"  void PressGesture_remove_Pressed_m889898292 (PressGesture_t582183752 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.PressGesture::get_IgnoreChildren()
extern "C"  bool PressGesture_get_IgnoreChildren_m1301742151 (PressGesture_t582183752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PressGesture::set_IgnoreChildren(System.Boolean)
extern "C"  void PressGesture_set_IgnoreChildren_m882609684 (PressGesture_t582183752 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.PressGesture::ShouldReceiveTouch(TouchScript.TouchPoint)
extern "C"  bool PressGesture_ShouldReceiveTouch_m2709033015 (PressGesture_t582183752 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.PressGesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern "C"  bool PressGesture_CanPreventGesture_m3254370709 (PressGesture_t582183752 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.PressGesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern "C"  bool PressGesture_CanBePreventedByGesture_m3072670792 (PressGesture_t582183752 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PressGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void PressGesture_touchesBegan_m3641336591 (PressGesture_t582183752 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.PressGesture::onRecognized()
extern "C"  void PressGesture_onRecognized_m105209032 (PressGesture_t582183752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
