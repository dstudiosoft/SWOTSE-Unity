﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509StoreManager
struct X509StoreManager_t1740460067;
// Mono.Security.X509.X509Stores
struct X509Stores_t3001420399;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472866;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509StoreManager::.ctor()
extern "C"  void X509StoreManager__ctor_m1746925563 (X509StoreManager_t1740460067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
extern "C"  X509Stores_t3001420399 * X509StoreManager_get_CurrentUser_m569194374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
extern "C"  X509Stores_t3001420399 * X509StoreManager_get_LocalMachine_m2451390246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_IntermediateCACertificates()
extern "C"  X509CertificateCollection_t3592472866 * X509StoreManager_get_IntermediateCACertificates_m580809864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_IntermediateCACrls()
extern "C"  ArrayList_t4252133567 * X509StoreManager_get_IntermediateCACrls_m1480593123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
extern "C"  X509CertificateCollection_t3592472866 * X509StoreManager_get_TrustedRootCertificates_m122138510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_TrustedRootCACrls()
extern "C"  ArrayList_t4252133567 * X509StoreManager_get_TrustedRootCACrls_m695533273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_UntrustedCertificates()
extern "C"  X509CertificateCollection_t3592472866 * X509StoreManager_get_UntrustedCertificates_m2003978593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
