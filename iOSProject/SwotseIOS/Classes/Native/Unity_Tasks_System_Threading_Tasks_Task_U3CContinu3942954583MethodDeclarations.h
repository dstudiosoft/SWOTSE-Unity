﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Int32>
struct U3CContinueWithU3Ec__AnonStorey0_1_t3942954583;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Int32>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1__ctor_m887260433_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1__ctor_m887260433(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1__ctor_m887260433_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Int32>::<>m__0()
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m3180729216_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m3180729216(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m3180729216_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Int32>::<>m__1(System.Threading.Tasks.Task)
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m4146175749_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 * __this, Task_t1843236107 * ___t0, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m4146175749(__this, ___t0, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t3942954583 *, Task_t1843236107 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m4146175749_gshared)(__this, ___t0, method)
