﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnityHttpFactoryService
struct UnityHttpFactoryService_t22354146;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Unity.UnityHttpFactoryService::.ctor()
extern "C"  void UnityHttpFactoryService__ctor_m1460875245 (UnityHttpFactoryService_t22354146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Unity.UnityHttpFactoryService Firebase.Unity.UnityHttpFactoryService::get_Instance()
extern "C"  UnityHttpFactoryService_t22354146 * UnityHttpFactoryService_get_Instance_m1254201494 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnityHttpFactoryService::.cctor()
extern "C"  void UnityHttpFactoryService__cctor_m1725644162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
