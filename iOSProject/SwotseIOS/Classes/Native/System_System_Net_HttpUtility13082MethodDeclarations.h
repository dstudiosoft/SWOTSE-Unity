﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpUtility
struct HttpUtility_t13082;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Text.Encoding
struct Encoding_t663144255;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Text_Encoding663144255.h"

// System.Void System.Net.HttpUtility::.ctor()
extern "C"  void HttpUtility__ctor_m3055050124 (HttpUtility_t13082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpUtility::UrlDecode(System.String)
extern "C"  String_t* HttpUtility_UrlDecode_m776658698 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.Net.HttpUtility::GetChars(System.IO.MemoryStream,System.Text.Encoding)
extern "C"  CharU5BU5D_t1328083999* HttpUtility_GetChars_m4034563568 (Il2CppObject * __this /* static, unused */, MemoryStream_t743994179 * ___b0, Encoding_t663144255 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern "C"  String_t* HttpUtility_UrlDecode_m3776032611 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t663144255 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
