﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_g2042738142MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Int32,TouchScript.Utils.ObjectPool`1/UnityFunc`1<T,T>,UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m1397208164(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method) ((  void (*) (ObjectPool_1_t3542042081 *, int32_t, UnityFunc_1_t1583593973 *, UnityAction_1_t1260371689 *, UnityAction_1_t1260371689 *, const MethodInfo*))ObjectPool_1__ctor_m3649530527_gshared)(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_CountAll()
#define ObjectPool_1_get_CountAll_m3349137844(__this, method) ((  int32_t (*) (ObjectPool_1_t3542042081 *, const MethodInfo*))ObjectPool_1_get_CountAll_m1681014435_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::set_CountAll(System.Int32)
#define ObjectPool_1_set_CountAll_m3257460757(__this, ___value0, method) ((  void (*) (ObjectPool_1_t3542042081 *, int32_t, const MethodInfo*))ObjectPool_1_set_CountAll_m122827726_gshared)(__this, ___value0, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_CountActive()
#define ObjectPool_1_get_CountActive_m4138740603(__this, method) ((  int32_t (*) (ObjectPool_1_t3542042081 *, const MethodInfo*))ObjectPool_1_get_CountActive_m3063653714_gshared)(__this, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_CountInactive()
#define ObjectPool_1_get_CountInactive_m1942054038(__this, method) ((  int32_t (*) (ObjectPool_1_t3542042081 *, const MethodInfo*))ObjectPool_1_get_CountInactive_m1369084401_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::WarmUp(System.Int32)
#define ObjectPool_1_WarmUp_m2636577276(__this, ___count0, method) ((  void (*) (ObjectPool_1_t3542042081 *, int32_t, const MethodInfo*))ObjectPool_1_WarmUp_m1349419031_gshared)(__this, ___count0, method)
// T TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::Get()
#define ObjectPool_1_Get_m2586648460(__this, method) ((  TouchProxyBase_t4188753234 * (*) (ObjectPool_1_t3542042081 *, const MethodInfo*))ObjectPool_1_Get_m2843288903_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::Release(T)
#define ObjectPool_1_Release_m785900094(__this, ___element0, method) ((  void (*) (ObjectPool_1_t3542042081 *, TouchProxyBase_t4188753234 *, const MethodInfo*))ObjectPool_1_Release_m1491729219_gshared)(__this, ___element0, method)
