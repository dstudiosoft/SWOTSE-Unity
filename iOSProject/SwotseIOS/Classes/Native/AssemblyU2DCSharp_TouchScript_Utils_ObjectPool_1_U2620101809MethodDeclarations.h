﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_U1709969761MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.TouchPoint,TouchScript.TouchPoint>::.ctor(System.Object,System.IntPtr)
#define UnityFunc_1__ctor_m1845531859(__this, ___object0, ___method1, method) ((  void (*) (UnityFunc_1_t2620101809 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityFunc_1__ctor_m3483258383_gshared)(__this, ___object0, ___method1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.TouchPoint,TouchScript.TouchPoint>::Invoke()
#define UnityFunc_1_Invoke_m1647678620(__this, method) ((  TouchPoint_t959629083 * (*) (UnityFunc_1_t2620101809 *, const MethodInfo*))UnityFunc_1_Invoke_m4133775042_gshared)(__this, method)
// System.IAsyncResult TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.TouchPoint,TouchScript.TouchPoint>::BeginInvoke(System.AsyncCallback,System.Object)
#define UnityFunc_1_BeginInvoke_m3125893540(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (UnityFunc_1_t2620101809 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_BeginInvoke_m3032171082_gshared)(__this, ___callback0, ___object1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.TouchPoint,TouchScript.TouchPoint>::EndInvoke(System.IAsyncResult)
#define UnityFunc_1_EndInvoke_m2422385468(__this, ___result0, method) ((  TouchPoint_t959629083 * (*) (UnityFunc_1_t2620101809 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_EndInvoke_m455851222_gshared)(__this, ___result0, method)
