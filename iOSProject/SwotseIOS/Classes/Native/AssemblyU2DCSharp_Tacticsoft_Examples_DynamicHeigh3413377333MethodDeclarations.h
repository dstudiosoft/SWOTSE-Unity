﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.DynamicHeightTableViewController
struct DynamicHeightTableViewController_t3413377333;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void Tacticsoft.Examples.DynamicHeightTableViewController::.ctor()
extern "C"  void DynamicHeightTableViewController__ctor_m1659807647 (DynamicHeightTableViewController_t3413377333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightTableViewController::Start()
extern "C"  void DynamicHeightTableViewController_Start_m1992092451 (DynamicHeightTableViewController_t3413377333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightTableViewController::SendBeer()
extern "C"  void DynamicHeightTableViewController_SendBeer_m1628318739 (DynamicHeightTableViewController_t3413377333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.Examples.DynamicHeightTableViewController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t DynamicHeightTableViewController_GetNumberOfRowsForTableView_m1044939979 (DynamicHeightTableViewController_t3413377333 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.Examples.DynamicHeightTableViewController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float DynamicHeightTableViewController_GetHeightForRowInTableView_m270933179 (DynamicHeightTableViewController_t3413377333 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.Examples.DynamicHeightTableViewController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * DynamicHeightTableViewController_GetCellForRowInTableView_m3574370544 (DynamicHeightTableViewController_t3413377333 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.Examples.DynamicHeightTableViewController::GetHeightOfRow(System.Int32)
extern "C"  float DynamicHeightTableViewController_GetHeightOfRow_m3634291444 (DynamicHeightTableViewController_t3413377333 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightTableViewController::OnCellHeightChanged(System.Int32,System.Single)
extern "C"  void DynamicHeightTableViewController_OnCellHeightChanged_m637963689 (DynamicHeightTableViewController_t3413377333 * __this, int32_t ___row0, float ___newHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
