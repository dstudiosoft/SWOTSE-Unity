﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/DestroyDelegate
struct DestroyDelegate_t3635929227;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Firebase.FirebaseApp/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m984049708 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/DestroyDelegate::Invoke()
extern "C"  void DestroyDelegate_Invoke_m3785470502 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Firebase.FirebaseApp/DestroyDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DestroyDelegate_BeginInvoke_m3002495555 (DestroyDelegate_t3635929227 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DestroyDelegate_EndInvoke_m1272627102 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
