﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

// UnityEngine.Vector3 TouchScript.Utils.ProjectionUtils::CameraToPlaneProjection(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Plane)
extern "C"  Vector3_t2243707580  ProjectionUtils_CameraToPlaneProjection_m873903950 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___position0, Camera_t189460977 * ___camera1, Plane_t3727654732  ___projectionPlane2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Utils.ProjectionUtils::ScreenToPlaneProjection(UnityEngine.Vector2,UnityEngine.Plane)
extern "C"  Vector3_t2243707580  ProjectionUtils_ScreenToPlaneProjection_m3928252099 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___position0, Plane_t3727654732  ___projectionPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
