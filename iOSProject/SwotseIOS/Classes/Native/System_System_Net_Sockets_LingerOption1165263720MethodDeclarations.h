﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.LingerOption
struct LingerOption_t1165263720;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.Sockets.LingerOption::.ctor(System.Boolean,System.Int32)
extern "C"  void LingerOption__ctor_m1123371952 (LingerOption_t1165263720 * __this, bool ___enable0, int32_t ___secs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.LingerOption::get_Enabled()
extern "C"  bool LingerOption_get_Enabled_m3521799762 (LingerOption_t1165263720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.LingerOption::set_Enabled(System.Boolean)
extern "C"  void LingerOption_set_Enabled_m2480431159 (LingerOption_t1165263720 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.LingerOption::get_LingerTime()
extern "C"  int32_t LingerOption_get_LingerTime_m2199088629 (LingerOption_t1165263720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.LingerOption::set_LingerTime(System.Int32)
extern "C"  void LingerOption_set_LingerTime_m3689194172 (LingerOption_t1165263720 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
