﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// BattleReportModel
struct BattleReportModel_t2786384729;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportContentManager
struct  BattleReportContentManager_t4271590472  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BattleReportContentManager::attackColonizeDestroySection
	GameObject_t1756533147 * ___attackColonizeDestroySection_2;
	// UnityEngine.GameObject BattleReportContentManager::convertSection
	GameObject_t1756533147 * ___convertSection_3;
	// UnityEngine.GameObject BattleReportContentManager::scoutSection
	GameObject_t1756533147 * ___scoutSection_4;
	// UnityEngine.GameObject BattleReportContentManager::reinforceTransportSection
	GameObject_t1756533147 * ___reinforceTransportSection_5;
	// System.Int64 BattleReportContentManager::reportId
	int64_t ___reportId_6;
	// BattleReportModel BattleReportContentManager::report
	BattleReportModel_t2786384729 * ___report_7;
	// System.String BattleReportContentManager::dateFormat
	String_t* ___dateFormat_8;
	// SimpleEvent BattleReportContentManager::onGetBattleReport
	SimpleEvent_t1509017202 * ___onGetBattleReport_9;

public:
	inline static int32_t get_offset_of_attackColonizeDestroySection_2() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___attackColonizeDestroySection_2)); }
	inline GameObject_t1756533147 * get_attackColonizeDestroySection_2() const { return ___attackColonizeDestroySection_2; }
	inline GameObject_t1756533147 ** get_address_of_attackColonizeDestroySection_2() { return &___attackColonizeDestroySection_2; }
	inline void set_attackColonizeDestroySection_2(GameObject_t1756533147 * value)
	{
		___attackColonizeDestroySection_2 = value;
		Il2CppCodeGenWriteBarrier(&___attackColonizeDestroySection_2, value);
	}

	inline static int32_t get_offset_of_convertSection_3() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___convertSection_3)); }
	inline GameObject_t1756533147 * get_convertSection_3() const { return ___convertSection_3; }
	inline GameObject_t1756533147 ** get_address_of_convertSection_3() { return &___convertSection_3; }
	inline void set_convertSection_3(GameObject_t1756533147 * value)
	{
		___convertSection_3 = value;
		Il2CppCodeGenWriteBarrier(&___convertSection_3, value);
	}

	inline static int32_t get_offset_of_scoutSection_4() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___scoutSection_4)); }
	inline GameObject_t1756533147 * get_scoutSection_4() const { return ___scoutSection_4; }
	inline GameObject_t1756533147 ** get_address_of_scoutSection_4() { return &___scoutSection_4; }
	inline void set_scoutSection_4(GameObject_t1756533147 * value)
	{
		___scoutSection_4 = value;
		Il2CppCodeGenWriteBarrier(&___scoutSection_4, value);
	}

	inline static int32_t get_offset_of_reinforceTransportSection_5() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___reinforceTransportSection_5)); }
	inline GameObject_t1756533147 * get_reinforceTransportSection_5() const { return ___reinforceTransportSection_5; }
	inline GameObject_t1756533147 ** get_address_of_reinforceTransportSection_5() { return &___reinforceTransportSection_5; }
	inline void set_reinforceTransportSection_5(GameObject_t1756533147 * value)
	{
		___reinforceTransportSection_5 = value;
		Il2CppCodeGenWriteBarrier(&___reinforceTransportSection_5, value);
	}

	inline static int32_t get_offset_of_reportId_6() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___reportId_6)); }
	inline int64_t get_reportId_6() const { return ___reportId_6; }
	inline int64_t* get_address_of_reportId_6() { return &___reportId_6; }
	inline void set_reportId_6(int64_t value)
	{
		___reportId_6 = value;
	}

	inline static int32_t get_offset_of_report_7() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___report_7)); }
	inline BattleReportModel_t2786384729 * get_report_7() const { return ___report_7; }
	inline BattleReportModel_t2786384729 ** get_address_of_report_7() { return &___report_7; }
	inline void set_report_7(BattleReportModel_t2786384729 * value)
	{
		___report_7 = value;
		Il2CppCodeGenWriteBarrier(&___report_7, value);
	}

	inline static int32_t get_offset_of_dateFormat_8() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___dateFormat_8)); }
	inline String_t* get_dateFormat_8() const { return ___dateFormat_8; }
	inline String_t** get_address_of_dateFormat_8() { return &___dateFormat_8; }
	inline void set_dateFormat_8(String_t* value)
	{
		___dateFormat_8 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_8, value);
	}

	inline static int32_t get_offset_of_onGetBattleReport_9() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472, ___onGetBattleReport_9)); }
	inline SimpleEvent_t1509017202 * get_onGetBattleReport_9() const { return ___onGetBattleReport_9; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetBattleReport_9() { return &___onGetBattleReport_9; }
	inline void set_onGetBattleReport_9(SimpleEvent_t1509017202 * value)
	{
		___onGetBattleReport_9 = value;
		Il2CppCodeGenWriteBarrier(&___onGetBattleReport_9, value);
	}
};

struct BattleReportContentManager_t4271590472_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BattleReportContentManager::<>f__switch$map5
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map5_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BattleReportContentManager::<>f__switch$map6
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map6_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_10() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472_StaticFields, ___U3CU3Ef__switchU24map5_10)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map5_10() const { return ___U3CU3Ef__switchU24map5_10; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map5_10() { return &___U3CU3Ef__switchU24map5_10; }
	inline void set_U3CU3Ef__switchU24map5_10(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map5_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_11() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t4271590472_StaticFields, ___U3CU3Ef__switchU24map6_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map6_11() const { return ___U3CU3Ef__switchU24map6_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map6_11() { return &___U3CU3Ef__switchU24map6_11; }
	inline void set_U3CU3Ef__switchU24map6_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map6_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
