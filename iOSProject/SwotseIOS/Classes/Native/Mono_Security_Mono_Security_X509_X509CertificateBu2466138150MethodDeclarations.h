﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509CertificateBuilder
struct X509CertificateBuilder_t2466138150;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1640144840;
// Mono.Security.ASN1
struct ASN1_t924533536;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"

// System.Void Mono.Security.X509.X509CertificateBuilder::.ctor()
extern "C"  void X509CertificateBuilder__ctor_m3432754591 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::.ctor(System.Byte)
extern "C"  void X509CertificateBuilder__ctor_m4175378656 (X509CertificateBuilder_t2466138150 * __this, uint8_t ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X509CertificateBuilder::get_Version()
extern "C"  uint8_t X509CertificateBuilder_get_Version_m3134853154 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_Version(System.Byte)
extern "C"  void X509CertificateBuilder_set_Version_m701612885 (X509CertificateBuilder_t2466138150 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_SerialNumber()
extern "C"  ByteU5BU5D_t3397334013* X509CertificateBuilder_get_SerialNumber_m2477392433 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SerialNumber(System.Byte[])
extern "C"  void X509CertificateBuilder_set_SerialNumber_m635890100 (X509CertificateBuilder_t2466138150 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509CertificateBuilder::get_IssuerName()
extern "C"  String_t* X509CertificateBuilder_get_IssuerName_m2831697119 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_IssuerName(System.String)
extern "C"  void X509CertificateBuilder_set_IssuerName_m1148371314 (X509CertificateBuilder_t2466138150 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509CertificateBuilder::get_NotBefore()
extern "C"  DateTime_t693205669  X509CertificateBuilder_get_NotBefore_m503705483 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_NotBefore(System.DateTime)
extern "C"  void X509CertificateBuilder_set_NotBefore_m1524827446 (X509CertificateBuilder_t2466138150 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509CertificateBuilder::get_NotAfter()
extern "C"  DateTime_t693205669  X509CertificateBuilder_get_NotAfter_m3578064138 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_NotAfter(System.DateTime)
extern "C"  void X509CertificateBuilder_set_NotAfter_m3187600633 (X509CertificateBuilder_t2466138150 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509CertificateBuilder::get_SubjectName()
extern "C"  String_t* X509CertificateBuilder_get_SubjectName_m3792892108 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectName(System.String)
extern "C"  void X509CertificateBuilder_set_SubjectName_m197298087 (X509CertificateBuilder_t2466138150 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.X509.X509CertificateBuilder::get_SubjectPublicKey()
extern "C"  AsymmetricAlgorithm_t784058677 * X509CertificateBuilder_get_SubjectPublicKey_m3240612949 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectPublicKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  void X509CertificateBuilder_set_SubjectPublicKey_m910502052 (X509CertificateBuilder_t2466138150 * __this, AsymmetricAlgorithm_t784058677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_IssuerUniqueId()
extern "C"  ByteU5BU5D_t3397334013* X509CertificateBuilder_get_IssuerUniqueId_m698464699 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_IssuerUniqueId(System.Byte[])
extern "C"  void X509CertificateBuilder_set_IssuerUniqueId_m2131838316 (X509CertificateBuilder_t2466138150 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_SubjectUniqueId()
extern "C"  ByteU5BU5D_t3397334013* X509CertificateBuilder_get_SubjectUniqueId_m3834323084 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectUniqueId(System.Byte[])
extern "C"  void X509CertificateBuilder_set_SubjectUniqueId_m1805392107 (X509CertificateBuilder_t2466138150 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509CertificateBuilder::get_Extensions()
extern "C"  X509ExtensionCollection_t1640144840 * X509CertificateBuilder_get_Extensions_m249949015 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509CertificateBuilder::SubjectPublicKeyInfo()
extern "C"  ASN1_t924533536 * X509CertificateBuilder_SubjectPublicKeyInfo_m1617276418 (X509CertificateBuilder_t2466138150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::UniqueIdentifier(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* X509CertificateBuilder_UniqueIdentifier_m1783744316 (X509CertificateBuilder_t2466138150 * __this, ByteU5BU5D_t3397334013* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509CertificateBuilder::ToBeSigned(System.String)
extern "C"  ASN1_t924533536 * X509CertificateBuilder_ToBeSigned_m3306919328 (X509CertificateBuilder_t2466138150 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
