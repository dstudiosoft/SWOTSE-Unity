﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConvertReportManager
struct ConvertReportManager_t1059314932;

#include "codegen/il2cpp-codegen.h"

// System.Void ConvertReportManager::.ctor()
extern "C"  void ConvertReportManager__ctor_m809759757 (ConvertReportManager_t1059314932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConvertReportManager::OnEnable()
extern "C"  void ConvertReportManager_OnEnable_m1187593141 (ConvertReportManager_t1059314932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConvertReportManager::CloseReport()
extern "C"  void ConvertReportManager_CloseReport_m1037753057 (ConvertReportManager_t1059314932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
