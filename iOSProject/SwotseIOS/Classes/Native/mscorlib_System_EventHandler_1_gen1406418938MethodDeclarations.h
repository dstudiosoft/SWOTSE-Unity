﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler_1_gen1280756467MethodDeclarations.h"

// System.Void System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m1592654722(__this, ___object0, ___method1, method) ((  void (*) (EventHandler_1_t1406418938 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventHandler_1__ctor_m805401670_gshared)(__this, ___object0, ___method1, method)
// System.Void System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs>::Invoke(System.Object,TEventArgs)
#define EventHandler_1_Invoke_m4219498199(__this, ___sender0, ___e1, method) ((  void (*) (EventHandler_1_t1406418938 *, Il2CppObject *, SocketAsyncEventArgs_t2815111766 *, const MethodInfo*))EventHandler_1_Invoke_m3162899003_gshared)(__this, ___sender0, ___e1, method)
// System.IAsyncResult System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs>::BeginInvoke(System.Object,TEventArgs,System.AsyncCallback,System.Object)
#define EventHandler_1_BeginInvoke_m3474249240(__this, ___sender0, ___e1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (EventHandler_1_t1406418938 *, Il2CppObject *, SocketAsyncEventArgs_t2815111766 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))EventHandler_1_BeginInvoke_m2005697352_gshared)(__this, ___sender0, ___e1, ___callback2, ___object3, method)
// System.Void System.EventHandler`1<System.Net.Sockets.SocketAsyncEventArgs>::EndInvoke(System.IAsyncResult)
#define EventHandler_1_EndInvoke_m3703276600(__this, ___result0, method) ((  void (*) (EventHandler_1_t1406418938 *, Il2CppObject *, const MethodInfo*))EventHandler_1_EndInvoke_m487063176_gshared)(__this, ___result0, method)
