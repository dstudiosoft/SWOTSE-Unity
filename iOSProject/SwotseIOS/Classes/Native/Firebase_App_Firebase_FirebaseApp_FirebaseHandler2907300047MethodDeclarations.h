﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/FirebaseHandler
struct FirebaseHandler_t2907300047;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.EventHandler`1<Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs>
struct EventHandler_1_t3924069827;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.FirebaseApp/FirebaseHandler::.ctor()
extern "C"  void FirebaseHandler__ctor_m1949563562 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::add_Updated(System.EventHandler`1<System.EventArgs>)
extern "C"  void FirebaseHandler_add_Updated_m2090237375 (FirebaseHandler_t2907300047 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::remove_Updated(System.EventHandler`1<System.EventArgs>)
extern "C"  void FirebaseHandler_remove_Updated_m1888673004 (FirebaseHandler_t2907300047 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::add_ApplicationFocusChanged(System.EventHandler`1<Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs>)
extern "C"  void FirebaseHandler_add_ApplicationFocusChanged_m4035155415 (FirebaseHandler_t2907300047 * __this, EventHandler_1_t3924069827 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::remove_ApplicationFocusChanged(System.EventHandler`1<Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs>)
extern "C"  void FirebaseHandler_remove_ApplicationFocusChanged_m716004702 (FirebaseHandler_t2907300047 * __this, EventHandler_1_t3924069827 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp/FirebaseHandler Firebase.FirebaseApp/FirebaseHandler::get_DefaultInstance()
extern "C"  FirebaseHandler_t2907300047 * FirebaseHandler_get_DefaultInstance_m1177823201 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::CreatePartialOnMainThread()
extern "C"  void FirebaseHandler_CreatePartialOnMainThread_m3340449767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::Create()
extern "C"  void FirebaseHandler_Create_m3248912600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void FirebaseHandler_LogMessage_m1734897910 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseHandler_LogMessage_m1734897910(int32_t ___log_level0, char* ___message1);
// System.Void Firebase.FirebaseApp/FirebaseHandler::Update()
extern "C"  void FirebaseHandler_Update_m164025069 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::OnApplicationFocus(System.Boolean)
extern "C"  void FirebaseHandler_OnApplicationFocus_m717688886 (FirebaseHandler_t2907300047 * __this, bool ___hasFocus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::OnDestroy()
extern "C"  void FirebaseHandler_OnDestroy_m1330888223 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::.cctor()
extern "C"  void FirebaseHandler__cctor_m901621883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::<CreatePartialOnMainThread>m__0()
extern "C"  void FirebaseHandler_U3CCreatePartialOnMainThreadU3Em__0_m36356336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
