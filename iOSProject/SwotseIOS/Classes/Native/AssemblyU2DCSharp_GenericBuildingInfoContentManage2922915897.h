﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericBuildingInfoContentManager
struct  GenericBuildingInfoContentManager_t2922915897  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingName
	Text_t356221433 * ___buildingName_2;
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingLevel
	Text_t356221433 * ___buildingLevel_3;
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingDescription
	Text_t356221433 * ___buildingDescription_4;
	// UnityEngine.GameObject GenericBuildingInfoContentManager::demolishBtn
	GameObject_t1756533147 * ___demolishBtn_5;
	// ConfirmationEvent GenericBuildingInfoContentManager::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_8;

public:
	inline static int32_t get_offset_of_buildingName_2() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897, ___buildingName_2)); }
	inline Text_t356221433 * get_buildingName_2() const { return ___buildingName_2; }
	inline Text_t356221433 ** get_address_of_buildingName_2() { return &___buildingName_2; }
	inline void set_buildingName_2(Text_t356221433 * value)
	{
		___buildingName_2 = value;
		Il2CppCodeGenWriteBarrier(&___buildingName_2, value);
	}

	inline static int32_t get_offset_of_buildingLevel_3() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897, ___buildingLevel_3)); }
	inline Text_t356221433 * get_buildingLevel_3() const { return ___buildingLevel_3; }
	inline Text_t356221433 ** get_address_of_buildingLevel_3() { return &___buildingLevel_3; }
	inline void set_buildingLevel_3(Text_t356221433 * value)
	{
		___buildingLevel_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildingLevel_3, value);
	}

	inline static int32_t get_offset_of_buildingDescription_4() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897, ___buildingDescription_4)); }
	inline Text_t356221433 * get_buildingDescription_4() const { return ___buildingDescription_4; }
	inline Text_t356221433 ** get_address_of_buildingDescription_4() { return &___buildingDescription_4; }
	inline void set_buildingDescription_4(Text_t356221433 * value)
	{
		___buildingDescription_4 = value;
		Il2CppCodeGenWriteBarrier(&___buildingDescription_4, value);
	}

	inline static int32_t get_offset_of_demolishBtn_5() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897, ___demolishBtn_5)); }
	inline GameObject_t1756533147 * get_demolishBtn_5() const { return ___demolishBtn_5; }
	inline GameObject_t1756533147 ** get_address_of_demolishBtn_5() { return &___demolishBtn_5; }
	inline void set_demolishBtn_5(GameObject_t1756533147 * value)
	{
		___demolishBtn_5 = value;
		Il2CppCodeGenWriteBarrier(&___demolishBtn_5, value);
	}

	inline static int32_t get_offset_of_confirmed_8() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897, ___confirmed_8)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_8() const { return ___confirmed_8; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_8() { return &___confirmed_8; }
	inline void set_confirmed_8(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_8 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_8, value);
	}
};

struct GenericBuildingInfoContentManager_t2922915897_StaticFields
{
public:
	// System.String GenericBuildingInfoContentManager::type
	String_t* ___type_6;
	// System.Int64 GenericBuildingInfoContentManager::pitId
	int64_t ___pitId_7;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897_StaticFields, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier(&___type_6, value);
	}

	inline static int32_t get_offset_of_pitId_7() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t2922915897_StaticFields, ___pitId_7)); }
	inline int64_t get_pitId_7() const { return ___pitId_7; }
	inline int64_t* get_address_of_pitId_7() { return &___pitId_7; }
	inline void set_pitId_7(int64_t value)
	{
		___pitId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
