﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceCitiesTableLine
struct ResidenceCitiesTableLine_t1138332587;
// ResidenceCitiesTableController
struct ResidenceCitiesTableController_t1287619937;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResidenceCitiesTableController1287619937.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResidenceCitiesTableLine::.ctor()
extern "C"  void ResidenceCitiesTableLine__ctor_m2607012458 (ResidenceCitiesTableLine_t1138332587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetOwner(ResidenceCitiesTableController)
extern "C"  void ResidenceCitiesTableLine_SetOwner_m4120477976 (ResidenceCitiesTableLine_t1138332587 * __this, ResidenceCitiesTableController_t1287619937 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetCityName(System.String)
extern "C"  void ResidenceCitiesTableLine_SetCityName_m4058635376 (ResidenceCitiesTableLine_t1138332587 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetMayerLevel(System.Int64)
extern "C"  void ResidenceCitiesTableLine_SetMayerLevel_m328465810 (ResidenceCitiesTableLine_t1138332587 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetCoords(System.String)
extern "C"  void ResidenceCitiesTableLine_SetCoords_m1060317250 (ResidenceCitiesTableLine_t1138332587 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetRegion(System.String)
extern "C"  void ResidenceCitiesTableLine_SetRegion_m2033996808 (ResidenceCitiesTableLine_t1138332587 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetFieldsCount(System.Int64)
extern "C"  void ResidenceCitiesTableLine_SetFieldsCount_m4029392716 (ResidenceCitiesTableLine_t1138332587 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceCitiesTableLine::SetValleysCount(System.Int64)
extern "C"  void ResidenceCitiesTableLine_SetValleysCount_m2221085781 (ResidenceCitiesTableLine_t1138332587 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
