﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Image
struct Image_t2042527209;
// IReloadable
struct IReloadable_t861412162;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t2823857299;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t1789388632;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityDeedContentManager
struct  CityDeedContentManager_t3098988935  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text CityDeedContentManager::totalBuildings
	Text_t356221433 * ___totalBuildings_2;
	// UnityEngine.UI.Text CityDeedContentManager::buildingsHighestLevel
	Text_t356221433 * ___buildingsHighestLevel_3;
	// UnityEngine.UI.Text CityDeedContentManager::totalFields
	Text_t356221433 * ___totalFields_4;
	// UnityEngine.UI.Text CityDeedContentManager::fieldsHighestLevel
	Text_t356221433 * ___fieldsHighestLevel_5;
	// UnityEngine.UI.Text CityDeedContentManager::totalUnits
	Text_t356221433 * ___totalUnits_6;
	// UnityEngine.UI.Dropdown CityDeedContentManager::citiesDropdown
	Dropdown_t1985816271 * ___citiesDropdown_7;
	// UnityEngine.UI.Text CityDeedContentManager::cityName
	Text_t356221433 * ___cityName_8;
	// UnityEngine.UI.Text CityDeedContentManager::cityCoords
	Text_t356221433 * ___cityCoords_9;
	// UnityEngine.UI.InputField CityDeedContentManager::cityPrice
	InputField_t1631627530 * ___cityPrice_10;
	// UnityEngine.UI.InputField CityDeedContentManager::buyerName
	InputField_t1631627530 * ___buyerName_11;
	// UnityEngine.UI.InputField CityDeedContentManager::buyerEmail
	InputField_t1631627530 * ___buyerEmail_12;
	// UnityEngine.UI.Image CityDeedContentManager::cityIcon
	Image_t2042527209 * ___cityIcon_13;
	// IReloadable CityDeedContentManager::owner
	Il2CppObject * ___owner_14;
	// System.String CityDeedContentManager::item_id
	String_t* ___item_id_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> CityDeedContentManager::nameToId
	Dictionary_2_t2823857299 * ___nameToId_16;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> CityDeedContentManager::citiesList
	List_1_t1789388632 * ___citiesList_17;

public:
	inline static int32_t get_offset_of_totalBuildings_2() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___totalBuildings_2)); }
	inline Text_t356221433 * get_totalBuildings_2() const { return ___totalBuildings_2; }
	inline Text_t356221433 ** get_address_of_totalBuildings_2() { return &___totalBuildings_2; }
	inline void set_totalBuildings_2(Text_t356221433 * value)
	{
		___totalBuildings_2 = value;
		Il2CppCodeGenWriteBarrier(&___totalBuildings_2, value);
	}

	inline static int32_t get_offset_of_buildingsHighestLevel_3() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___buildingsHighestLevel_3)); }
	inline Text_t356221433 * get_buildingsHighestLevel_3() const { return ___buildingsHighestLevel_3; }
	inline Text_t356221433 ** get_address_of_buildingsHighestLevel_3() { return &___buildingsHighestLevel_3; }
	inline void set_buildingsHighestLevel_3(Text_t356221433 * value)
	{
		___buildingsHighestLevel_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsHighestLevel_3, value);
	}

	inline static int32_t get_offset_of_totalFields_4() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___totalFields_4)); }
	inline Text_t356221433 * get_totalFields_4() const { return ___totalFields_4; }
	inline Text_t356221433 ** get_address_of_totalFields_4() { return &___totalFields_4; }
	inline void set_totalFields_4(Text_t356221433 * value)
	{
		___totalFields_4 = value;
		Il2CppCodeGenWriteBarrier(&___totalFields_4, value);
	}

	inline static int32_t get_offset_of_fieldsHighestLevel_5() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___fieldsHighestLevel_5)); }
	inline Text_t356221433 * get_fieldsHighestLevel_5() const { return ___fieldsHighestLevel_5; }
	inline Text_t356221433 ** get_address_of_fieldsHighestLevel_5() { return &___fieldsHighestLevel_5; }
	inline void set_fieldsHighestLevel_5(Text_t356221433 * value)
	{
		___fieldsHighestLevel_5 = value;
		Il2CppCodeGenWriteBarrier(&___fieldsHighestLevel_5, value);
	}

	inline static int32_t get_offset_of_totalUnits_6() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___totalUnits_6)); }
	inline Text_t356221433 * get_totalUnits_6() const { return ___totalUnits_6; }
	inline Text_t356221433 ** get_address_of_totalUnits_6() { return &___totalUnits_6; }
	inline void set_totalUnits_6(Text_t356221433 * value)
	{
		___totalUnits_6 = value;
		Il2CppCodeGenWriteBarrier(&___totalUnits_6, value);
	}

	inline static int32_t get_offset_of_citiesDropdown_7() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___citiesDropdown_7)); }
	inline Dropdown_t1985816271 * get_citiesDropdown_7() const { return ___citiesDropdown_7; }
	inline Dropdown_t1985816271 ** get_address_of_citiesDropdown_7() { return &___citiesDropdown_7; }
	inline void set_citiesDropdown_7(Dropdown_t1985816271 * value)
	{
		___citiesDropdown_7 = value;
		Il2CppCodeGenWriteBarrier(&___citiesDropdown_7, value);
	}

	inline static int32_t get_offset_of_cityName_8() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___cityName_8)); }
	inline Text_t356221433 * get_cityName_8() const { return ___cityName_8; }
	inline Text_t356221433 ** get_address_of_cityName_8() { return &___cityName_8; }
	inline void set_cityName_8(Text_t356221433 * value)
	{
		___cityName_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_8, value);
	}

	inline static int32_t get_offset_of_cityCoords_9() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___cityCoords_9)); }
	inline Text_t356221433 * get_cityCoords_9() const { return ___cityCoords_9; }
	inline Text_t356221433 ** get_address_of_cityCoords_9() { return &___cityCoords_9; }
	inline void set_cityCoords_9(Text_t356221433 * value)
	{
		___cityCoords_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityCoords_9, value);
	}

	inline static int32_t get_offset_of_cityPrice_10() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___cityPrice_10)); }
	inline InputField_t1631627530 * get_cityPrice_10() const { return ___cityPrice_10; }
	inline InputField_t1631627530 ** get_address_of_cityPrice_10() { return &___cityPrice_10; }
	inline void set_cityPrice_10(InputField_t1631627530 * value)
	{
		___cityPrice_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityPrice_10, value);
	}

	inline static int32_t get_offset_of_buyerName_11() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___buyerName_11)); }
	inline InputField_t1631627530 * get_buyerName_11() const { return ___buyerName_11; }
	inline InputField_t1631627530 ** get_address_of_buyerName_11() { return &___buyerName_11; }
	inline void set_buyerName_11(InputField_t1631627530 * value)
	{
		___buyerName_11 = value;
		Il2CppCodeGenWriteBarrier(&___buyerName_11, value);
	}

	inline static int32_t get_offset_of_buyerEmail_12() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___buyerEmail_12)); }
	inline InputField_t1631627530 * get_buyerEmail_12() const { return ___buyerEmail_12; }
	inline InputField_t1631627530 ** get_address_of_buyerEmail_12() { return &___buyerEmail_12; }
	inline void set_buyerEmail_12(InputField_t1631627530 * value)
	{
		___buyerEmail_12 = value;
		Il2CppCodeGenWriteBarrier(&___buyerEmail_12, value);
	}

	inline static int32_t get_offset_of_cityIcon_13() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___cityIcon_13)); }
	inline Image_t2042527209 * get_cityIcon_13() const { return ___cityIcon_13; }
	inline Image_t2042527209 ** get_address_of_cityIcon_13() { return &___cityIcon_13; }
	inline void set_cityIcon_13(Image_t2042527209 * value)
	{
		___cityIcon_13 = value;
		Il2CppCodeGenWriteBarrier(&___cityIcon_13, value);
	}

	inline static int32_t get_offset_of_owner_14() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___owner_14)); }
	inline Il2CppObject * get_owner_14() const { return ___owner_14; }
	inline Il2CppObject ** get_address_of_owner_14() { return &___owner_14; }
	inline void set_owner_14(Il2CppObject * value)
	{
		___owner_14 = value;
		Il2CppCodeGenWriteBarrier(&___owner_14, value);
	}

	inline static int32_t get_offset_of_item_id_15() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___item_id_15)); }
	inline String_t* get_item_id_15() const { return ___item_id_15; }
	inline String_t** get_address_of_item_id_15() { return &___item_id_15; }
	inline void set_item_id_15(String_t* value)
	{
		___item_id_15 = value;
		Il2CppCodeGenWriteBarrier(&___item_id_15, value);
	}

	inline static int32_t get_offset_of_nameToId_16() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___nameToId_16)); }
	inline Dictionary_2_t2823857299 * get_nameToId_16() const { return ___nameToId_16; }
	inline Dictionary_2_t2823857299 ** get_address_of_nameToId_16() { return &___nameToId_16; }
	inline void set_nameToId_16(Dictionary_2_t2823857299 * value)
	{
		___nameToId_16 = value;
		Il2CppCodeGenWriteBarrier(&___nameToId_16, value);
	}

	inline static int32_t get_offset_of_citiesList_17() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t3098988935, ___citiesList_17)); }
	inline List_1_t1789388632 * get_citiesList_17() const { return ___citiesList_17; }
	inline List_1_t1789388632 ** get_address_of_citiesList_17() { return &___citiesList_17; }
	inline void set_citiesList_17(List_1_t1789388632 * value)
	{
		___citiesList_17 = value;
		Il2CppCodeGenWriteBarrier(&___citiesList_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
