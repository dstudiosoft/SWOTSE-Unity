﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BattleReportUnitsLine
struct BattleReportUnitsLine_t392298293;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportUnitTableController
struct  BattleReportUnitTableController_t2722587206  : public MonoBehaviour_t1158329972
{
public:
	// BattleReportUnitsLine BattleReportUnitTableController::m_cellPrefab
	BattleReportUnitsLine_t392298293 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportUnitTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.Generic.List`1<System.String> BattleReportUnitTableController::unitNames
	List_1_t1398341365 * ___unitNames_4;
	// System.Collections.Generic.List`1<System.Int64> BattleReportUnitTableController::unitBeforeCount
	List_1_t278199169 * ___unitBeforeCount_5;
	// System.Collections.Generic.List`1<System.Int64> BattleReportUnitTableController::unitAfterCount
	List_1_t278199169 * ___unitAfterCount_6;
	// System.Int32 BattleReportUnitTableController::m_numRows
	int32_t ___m_numRows_7;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___m_cellPrefab_2)); }
	inline BattleReportUnitsLine_t392298293 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportUnitsLine_t392298293 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportUnitsLine_t392298293 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_unitNames_4() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___unitNames_4)); }
	inline List_1_t1398341365 * get_unitNames_4() const { return ___unitNames_4; }
	inline List_1_t1398341365 ** get_address_of_unitNames_4() { return &___unitNames_4; }
	inline void set_unitNames_4(List_1_t1398341365 * value)
	{
		___unitNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___unitNames_4, value);
	}

	inline static int32_t get_offset_of_unitBeforeCount_5() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___unitBeforeCount_5)); }
	inline List_1_t278199169 * get_unitBeforeCount_5() const { return ___unitBeforeCount_5; }
	inline List_1_t278199169 ** get_address_of_unitBeforeCount_5() { return &___unitBeforeCount_5; }
	inline void set_unitBeforeCount_5(List_1_t278199169 * value)
	{
		___unitBeforeCount_5 = value;
		Il2CppCodeGenWriteBarrier(&___unitBeforeCount_5, value);
	}

	inline static int32_t get_offset_of_unitAfterCount_6() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___unitAfterCount_6)); }
	inline List_1_t278199169 * get_unitAfterCount_6() const { return ___unitAfterCount_6; }
	inline List_1_t278199169 ** get_address_of_unitAfterCount_6() { return &___unitAfterCount_6; }
	inline void set_unitAfterCount_6(List_1_t278199169 * value)
	{
		___unitAfterCount_6 = value;
		Il2CppCodeGenWriteBarrier(&___unitAfterCount_6, value);
	}

	inline static int32_t get_offset_of_m_numRows_7() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t2722587206, ___m_numRows_7)); }
	inline int32_t get_m_numRows_7() const { return ___m_numRows_7; }
	inline int32_t* get_address_of_m_numRows_7() { return &___m_numRows_7; }
	inline void set_m_numRows_7(int32_t value)
	{
		___m_numRows_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
