﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager/<RemoveLots>c__Iterator11
struct U3CRemoveLotsU3Ec__Iterator11_t3546016335;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketManager/<RemoveLots>c__Iterator11::.ctor()
extern "C"  void U3CRemoveLotsU3Ec__Iterator11__ctor_m3242673564 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<RemoveLots>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRemoveLotsU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2734246762 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<RemoveLots>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRemoveLotsU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3908712018 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketManager/<RemoveLots>c__Iterator11::MoveNext()
extern "C"  bool U3CRemoveLotsU3Ec__Iterator11_MoveNext_m3854292268 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<RemoveLots>c__Iterator11::Dispose()
extern "C"  void U3CRemoveLotsU3Ec__Iterator11_Dispose_m1444375737 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<RemoveLots>c__Iterator11::Reset()
extern "C"  void U3CRemoveLotsU3Ec__Iterator11_Reset_m549588055 (U3CRemoveLotsU3Ec__Iterator11_t3546016335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
