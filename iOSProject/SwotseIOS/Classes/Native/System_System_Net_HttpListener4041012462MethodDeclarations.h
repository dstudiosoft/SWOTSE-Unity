﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListener
struct HttpListener_t4041012462;
// System.Net.AuthenticationSchemeSelector
struct AuthenticationSchemeSelector_t1076949500;
// System.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t3802522018;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// System.Net.HttpListenerContext
struct HttpListenerContext_t506453093;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_AuthenticationSchemes4122484730.h"
#include "System_System_Net_AuthenticationSchemeSelector1076949500.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_HttpListenerContext506453093.h"

// System.Void System.Net.HttpListener::.ctor()
extern "C"  void HttpListener__ctor_m2768420302 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::System.IDisposable.Dispose()
extern "C"  void HttpListener_System_IDisposable_Dispose_m3984623805 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.HttpListener::get_AuthenticationSchemes()
extern "C"  int32_t HttpListener_get_AuthenticationSchemes_m2255840006 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_AuthenticationSchemes(System.Net.AuthenticationSchemes)
extern "C"  void HttpListener_set_AuthenticationSchemes_m2607712757 (HttpListener_t4041012462 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemeSelector System.Net.HttpListener::get_AuthenticationSchemeSelectorDelegate()
extern "C"  AuthenticationSchemeSelector_t1076949500 * HttpListener_get_AuthenticationSchemeSelectorDelegate_m1400849885 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_AuthenticationSchemeSelectorDelegate(System.Net.AuthenticationSchemeSelector)
extern "C"  void HttpListener_set_AuthenticationSchemeSelectorDelegate_m804050402 (HttpListener_t4041012462 * __this, AuthenticationSchemeSelector_t1076949500 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IgnoreWriteExceptions()
extern "C"  bool HttpListener_get_IgnoreWriteExceptions_m2598605034 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_IgnoreWriteExceptions(System.Boolean)
extern "C"  void HttpListener_set_IgnoreWriteExceptions_m3445825865 (HttpListener_t4041012462 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IsListening()
extern "C"  bool HttpListener_get_IsListening_m1072963192 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IsSupported()
extern "C"  bool HttpListener_get_IsSupported_m567834559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerPrefixCollection System.Net.HttpListener::get_Prefixes()
extern "C"  HttpListenerPrefixCollection_t3802522018 * HttpListener_get_Prefixes_m3957162648 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListener::get_Realm()
extern "C"  String_t* HttpListener_get_Realm_m3145716557 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_Realm(System.String)
extern "C"  void HttpListener_set_Realm_m4177855924 (HttpListener_t4041012462 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_UnsafeConnectionNtlmAuthentication()
extern "C"  bool HttpListener_get_UnsafeConnectionNtlmAuthentication_m2942948242 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_UnsafeConnectionNtlmAuthentication(System.Boolean)
extern "C"  void HttpListener_set_UnsafeConnectionNtlmAuthentication_m1168212719 (HttpListener_t4041012462 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Abort()
extern "C"  void HttpListener_Abort_m4066214020 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Close()
extern "C"  void HttpListener_Close_m2813904186 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Close(System.Boolean)
extern "C"  void HttpListener_Close_m657648631 (HttpListener_t4041012462 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Cleanup(System.Boolean)
extern "C"  void HttpListener_Cleanup_m4217512685 (HttpListener_t4041012462 * __this, bool ___close_existing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.HttpListener::BeginGetContext(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HttpListener_BeginGetContext_m3161358140 (HttpListener_t4041012462 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::EndGetContext(System.IAsyncResult)
extern "C"  HttpListenerContext_t506453093 * HttpListener_EndGetContext_m3255663925 (HttpListener_t4041012462 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.HttpListener::SelectAuthenticationScheme(System.Net.HttpListenerContext)
extern "C"  int32_t HttpListener_SelectAuthenticationScheme_m2311333015 (HttpListener_t4041012462 * __this, HttpListenerContext_t506453093 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::GetContext()
extern "C"  HttpListenerContext_t506453093 * HttpListener_GetContext_m2516726631 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Start()
extern "C"  void HttpListener_Start_m3496646746 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Stop()
extern "C"  void HttpListener_Stop_m671977552 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::CheckDisposed()
extern "C"  void HttpListener_CheckDisposed_m419131111 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::GetContextFromQueue()
extern "C"  HttpListenerContext_t506453093 * HttpListener_GetContextFromQueue_m579363446 (HttpListener_t4041012462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::RegisterContext(System.Net.HttpListenerContext)
extern "C"  void HttpListener_RegisterContext_m2770996447 (HttpListener_t4041012462 * __this, HttpListenerContext_t506453093 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::UnregisterContext(System.Net.HttpListenerContext)
extern "C"  void HttpListener_UnregisterContext_m499150332 (HttpListener_t4041012462 * __this, HttpListenerContext_t506453093 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
