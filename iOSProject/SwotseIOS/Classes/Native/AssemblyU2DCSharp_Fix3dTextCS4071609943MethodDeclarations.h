﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fix3dTextCS
struct Fix3dTextCS_t4071609943;

#include "codegen/il2cpp-codegen.h"

// System.Void Fix3dTextCS::.ctor()
extern "C"  void Fix3dTextCS__ctor_m454219816 (Fix3dTextCS_t4071609943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fix3dTextCS::Start()
extern "C"  void Fix3dTextCS_Start_m2921540316 (Fix3dTextCS_t4071609943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fix3dTextCS::Update()
extern "C"  void Fix3dTextCS_Update_m1504886371 (Fix3dTextCS_t4071609943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
