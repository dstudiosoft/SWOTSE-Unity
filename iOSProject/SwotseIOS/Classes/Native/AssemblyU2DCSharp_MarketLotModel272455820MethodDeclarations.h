﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketLotModel
struct MarketLotModel_t272455820;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketLotModel::.ctor()
extern "C"  void MarketLotModel__ctor_m1011354879 (MarketLotModel_t272455820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
