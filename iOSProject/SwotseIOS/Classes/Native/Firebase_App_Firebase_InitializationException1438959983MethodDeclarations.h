﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.InitializationException
struct InitializationException_t1438959983;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_InitResult105293995.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult)
extern "C"  void InitializationException__ctor_m6220901 (InitializationException_t1438959983 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern "C"  void InitializationException__ctor_m1927152695 (InitializationException_t1438959983 * __this, int32_t ___result0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String,System.Exception)
extern "C"  void InitializationException__ctor_m2920435359 (InitializationException_t1438959983 * __this, int32_t ___result0, String_t* ___message1, Exception_t1927440687 * ___inner2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.InitResult Firebase.InitializationException::get_InitResult()
extern "C"  int32_t InitializationException_get_InitResult_m2014061296 (InitializationException_t1438959983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern "C"  void InitializationException_set_InitResult_m394481415 (InitializationException_t1438959983 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
