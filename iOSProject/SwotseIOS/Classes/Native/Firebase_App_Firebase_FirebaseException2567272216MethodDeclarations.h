﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseException
struct FirebaseException_t2567272216;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Firebase.FirebaseException::.ctor()
extern "C"  void FirebaseException__ctor_m1926807088 (FirebaseException_t2567272216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseException::.ctor(System.Int32)
extern "C"  void FirebaseException__ctor_m394143869 (FirebaseException_t2567272216 * __this, int32_t ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern "C"  void FirebaseException__ctor_m3734642555 (FirebaseException_t2567272216 * __this, int32_t ___errorCode0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String,System.Exception)
extern "C"  void FirebaseException__ctor_m2912326555 (FirebaseException_t2567272216 * __this, int32_t ___errorCode0, String_t* ___message1, Exception_t1927440687 * ___inner2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.FirebaseException::get_ErrorCode()
extern "C"  int32_t FirebaseException_get_ErrorCode_m586267042 (FirebaseException_t2567272216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern "C"  void FirebaseException_set_ErrorCode_m1261295849 (FirebaseException_t2567272216 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
