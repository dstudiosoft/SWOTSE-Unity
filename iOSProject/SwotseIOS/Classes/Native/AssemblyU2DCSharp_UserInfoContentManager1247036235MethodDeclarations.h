﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserInfoContentManager
struct UserInfoContentManager_t1247036235;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UserInfoContentManager::.ctor()
extern "C"  void UserInfoContentManager__ctor_m1946139858 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::OnEnable()
extern "C"  void UserInfoContentManager_OnEnable_m3701424182 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::SwitchIcon(System.Boolean)
extern "C"  void UserInfoContentManager_SwitchIcon_m2586988920 (UserInfoContentManager_t1247036235 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::SaveUserData()
extern "C"  void UserInfoContentManager_SaveUserData_m1658027966 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::ChangeMemberPosition()
extern "C"  void UserInfoContentManager_ChangeMemberPosition_m3054271265 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::MessageMember()
extern "C"  void UserInfoContentManager_MessageMember_m1205657433 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::ExcludeMember()
extern "C"  void UserInfoContentManager_ExcludeMember_m2242781530 (UserInfoContentManager_t1247036235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserInfoContentManager::MemberExcluded(System.Object,System.String)
extern "C"  void UserInfoContentManager_MemberExcluded_m23753700 (UserInfoContentManager_t1247036235 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
