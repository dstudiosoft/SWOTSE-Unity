﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t3751268748;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3285998422.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m556430178_gshared (Enumerator_t3285998422 * __this, List_1_t3751268748 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m556430178(__this, ___l0, method) ((  void (*) (Enumerator_t3285998422 *, List_1_t3751268748 *, const MethodInfo*))Enumerator__ctor_m556430178_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m45028544_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m45028544(__this, method) ((  void (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m45028544_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2925206422_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2925206422(__this, method) ((  Il2CppObject * (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2925206422_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::Dispose()
extern "C"  void Enumerator_Dispose_m3090990731_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3090990731(__this, method) ((  void (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_Dispose_m3090990731_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4204174256_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4204174256(__this, method) ((  void (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_VerifyState_m4204174256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m792634832_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m792634832(__this, method) ((  bool (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_MoveNext_m792634832_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit>::get_Current()
extern "C"  RaycastHit_t87180320  Enumerator_get_Current_m3741798607_gshared (Enumerator_t3285998422 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3741798607(__this, method) ((  RaycastHit_t87180320  (*) (Enumerator_t3285998422 *, const MethodInfo*))Enumerator_get_Current_m3741798607_gshared)(__this, method)
