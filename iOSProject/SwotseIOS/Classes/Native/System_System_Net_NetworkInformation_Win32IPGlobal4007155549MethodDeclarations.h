﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW
struct Win32_MIB_TCP6ROW_t4007155549;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.NetworkInformation.TcpConnectionInformation
struct TcpConnectionInformation_t3392050187;
struct Win32_MIB_TCP6ROW_t4007155549_marshaled_pinvoke;
struct Win32_MIB_TCP6ROW_t4007155549_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW::.ctor()
extern "C"  void Win32_MIB_TCP6ROW__ctor_m1089573993 (Win32_MIB_TCP6ROW_t4007155549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW::get_LocalEndPoint()
extern "C"  IPEndPoint_t2615413766 * Win32_MIB_TCP6ROW_get_LocalEndPoint_m1499831341 (Win32_MIB_TCP6ROW_t4007155549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW::get_RemoteEndPoint()
extern "C"  IPEndPoint_t2615413766 * Win32_MIB_TCP6ROW_get_RemoteEndPoint_m1504756156 (Win32_MIB_TCP6ROW_t4007155549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.TcpConnectionInformation System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW::get_TcpInfo()
extern "C"  TcpConnectionInformation_t3392050187 * Win32_MIB_TCP6ROW_get_TcpInfo_m3318259821 (Win32_MIB_TCP6ROW_t4007155549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Win32_MIB_TCP6ROW_t4007155549;
struct Win32_MIB_TCP6ROW_t4007155549_marshaled_pinvoke;

extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_pinvoke(const Win32_MIB_TCP6ROW_t4007155549& unmarshaled, Win32_MIB_TCP6ROW_t4007155549_marshaled_pinvoke& marshaled);
extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_pinvoke_back(const Win32_MIB_TCP6ROW_t4007155549_marshaled_pinvoke& marshaled, Win32_MIB_TCP6ROW_t4007155549& unmarshaled);
extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_pinvoke_cleanup(Win32_MIB_TCP6ROW_t4007155549_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Win32_MIB_TCP6ROW_t4007155549;
struct Win32_MIB_TCP6ROW_t4007155549_marshaled_com;

extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_com(const Win32_MIB_TCP6ROW_t4007155549& unmarshaled, Win32_MIB_TCP6ROW_t4007155549_marshaled_com& marshaled);
extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_com_back(const Win32_MIB_TCP6ROW_t4007155549_marshaled_com& marshaled, Win32_MIB_TCP6ROW_t4007155549& unmarshaled);
extern "C" void Win32_MIB_TCP6ROW_t4007155549_marshal_com_cleanup(Win32_MIB_TCP6ROW_t4007155549_marshaled_com& marshaled);
