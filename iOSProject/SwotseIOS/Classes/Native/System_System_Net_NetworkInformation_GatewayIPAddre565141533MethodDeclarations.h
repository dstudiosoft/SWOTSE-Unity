﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.GatewayIPAddressInformationImpl
struct GatewayIPAddressInformationImpl_t565141533;
// System.Net.IPAddress
struct IPAddress_t1399971723;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"

// System.Void System.Net.NetworkInformation.GatewayIPAddressInformationImpl::.ctor(System.Net.IPAddress)
extern "C"  void GatewayIPAddressInformationImpl__ctor_m1701918754 (GatewayIPAddressInformationImpl_t565141533 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.GatewayIPAddressInformationImpl::get_Address()
extern "C"  IPAddress_t1399971723 * GatewayIPAddressInformationImpl_get_Address_m664028476 (GatewayIPAddressInformationImpl_t565141533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
