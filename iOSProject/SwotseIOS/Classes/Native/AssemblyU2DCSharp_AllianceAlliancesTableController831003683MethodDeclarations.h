﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceAlliancesTableController
struct AllianceAlliancesTableController_t831003683;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void AllianceAlliancesTableController::.ctor()
extern "C"  void AllianceAlliancesTableController__ctor_m616775430 (AllianceAlliancesTableController_t831003683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceAlliancesTableController::Start()
extern "C"  void AllianceAlliancesTableController_Start_m2427604066 (AllianceAlliancesTableController_t831003683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllianceAlliancesTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t AllianceAlliancesTableController_GetNumberOfRowsForTableView_m3921823824 (AllianceAlliancesTableController_t831003683 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AllianceAlliancesTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float AllianceAlliancesTableController_GetHeightForRowInTableView_m2974277844 (AllianceAlliancesTableController_t831003683 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell AllianceAlliancesTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * AllianceAlliancesTableController_GetCellForRowInTableView_m1532396491 (AllianceAlliancesTableController_t831003683 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
