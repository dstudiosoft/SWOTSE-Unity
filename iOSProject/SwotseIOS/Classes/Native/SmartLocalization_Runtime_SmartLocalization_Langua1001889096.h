﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageRuntimeData
struct  LanguageRuntimeData_t1001889096  : public Il2CppObject
{
public:

public:
};

struct LanguageRuntimeData_t1001889096_StaticFields
{
public:
	// System.String SmartLocalization.LanguageRuntimeData::AvailableCulturesFileName
	String_t* ___AvailableCulturesFileName_0;
	// System.String SmartLocalization.LanguageRuntimeData::rootLanguageName
	String_t* ___rootLanguageName_1;
	// System.String SmartLocalization.LanguageRuntimeData::AudioFileFolderName
	String_t* ___AudioFileFolderName_2;
	// System.String SmartLocalization.LanguageRuntimeData::TexturesFolderName
	String_t* ___TexturesFolderName_3;
	// System.String SmartLocalization.LanguageRuntimeData::PrefabsFolderName
	String_t* ___PrefabsFolderName_4;

public:
	inline static int32_t get_offset_of_AvailableCulturesFileName_0() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t1001889096_StaticFields, ___AvailableCulturesFileName_0)); }
	inline String_t* get_AvailableCulturesFileName_0() const { return ___AvailableCulturesFileName_0; }
	inline String_t** get_address_of_AvailableCulturesFileName_0() { return &___AvailableCulturesFileName_0; }
	inline void set_AvailableCulturesFileName_0(String_t* value)
	{
		___AvailableCulturesFileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___AvailableCulturesFileName_0, value);
	}

	inline static int32_t get_offset_of_rootLanguageName_1() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t1001889096_StaticFields, ___rootLanguageName_1)); }
	inline String_t* get_rootLanguageName_1() const { return ___rootLanguageName_1; }
	inline String_t** get_address_of_rootLanguageName_1() { return &___rootLanguageName_1; }
	inline void set_rootLanguageName_1(String_t* value)
	{
		___rootLanguageName_1 = value;
		Il2CppCodeGenWriteBarrier(&___rootLanguageName_1, value);
	}

	inline static int32_t get_offset_of_AudioFileFolderName_2() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t1001889096_StaticFields, ___AudioFileFolderName_2)); }
	inline String_t* get_AudioFileFolderName_2() const { return ___AudioFileFolderName_2; }
	inline String_t** get_address_of_AudioFileFolderName_2() { return &___AudioFileFolderName_2; }
	inline void set_AudioFileFolderName_2(String_t* value)
	{
		___AudioFileFolderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___AudioFileFolderName_2, value);
	}

	inline static int32_t get_offset_of_TexturesFolderName_3() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t1001889096_StaticFields, ___TexturesFolderName_3)); }
	inline String_t* get_TexturesFolderName_3() const { return ___TexturesFolderName_3; }
	inline String_t** get_address_of_TexturesFolderName_3() { return &___TexturesFolderName_3; }
	inline void set_TexturesFolderName_3(String_t* value)
	{
		___TexturesFolderName_3 = value;
		Il2CppCodeGenWriteBarrier(&___TexturesFolderName_3, value);
	}

	inline static int32_t get_offset_of_PrefabsFolderName_4() { return static_cast<int32_t>(offsetof(LanguageRuntimeData_t1001889096_StaticFields, ___PrefabsFolderName_4)); }
	inline String_t* get_PrefabsFolderName_4() const { return ___PrefabsFolderName_4; }
	inline String_t** get_address_of_PrefabsFolderName_4() { return &___PrefabsFolderName_4; }
	inline void set_PrefabsFolderName_4(String_t* value)
	{
		___PrefabsFolderName_4 = value;
		Il2CppCodeGenWriteBarrier(&___PrefabsFolderName_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
