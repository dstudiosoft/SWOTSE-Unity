﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_t3588930308;
// System.IO.Stream
struct Stream_t3255436806;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.OpenReadCompletedEventArgs::.ctor(System.IO.Stream,System.Exception,System.Boolean,System.Object)
extern "C"  void OpenReadCompletedEventArgs__ctor_m2536263158 (OpenReadCompletedEventArgs_t3588930308 * __this, Stream_t3255436806 * ___result0, Exception_t1927440687 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.OpenReadCompletedEventArgs::get_Result()
extern "C"  Stream_t3255436806 * OpenReadCompletedEventArgs_get_Result_m2629244720 (OpenReadCompletedEventArgs_t3588930308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
