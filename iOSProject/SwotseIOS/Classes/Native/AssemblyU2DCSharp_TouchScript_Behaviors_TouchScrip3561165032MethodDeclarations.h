﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Behaviors.TouchScriptInputModule
struct TouchScriptInputModule_t3561165032;
// System.String
struct String_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// System.Object
struct Il2CppObject;
// TouchScript.TouchEventArgs
struct TouchEventArgs_t1917927166;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_TouchScript_TouchEventArgs1917927166.h"

// System.Void TouchScript.Behaviors.TouchScriptInputModule::.ctor()
extern "C"  void TouchScriptInputModule__ctor_m888560660 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Behaviors.TouchScriptInputModule::get_HorizontalAxis()
extern "C"  String_t* TouchScriptInputModule_get_HorizontalAxis_m3776383785 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::set_HorizontalAxis(System.String)
extern "C"  void TouchScriptInputModule_set_HorizontalAxis_m2102167492 (TouchScriptInputModule_t3561165032 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Behaviors.TouchScriptInputModule::get_VerticalAxis()
extern "C"  String_t* TouchScriptInputModule_get_VerticalAxis_m2257907765 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::set_VerticalAxis(System.String)
extern "C"  void TouchScriptInputModule_set_VerticalAxis_m1108427546 (TouchScriptInputModule_t3561165032 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Behaviors.TouchScriptInputModule::get_SubmitButton()
extern "C"  String_t* TouchScriptInputModule_get_SubmitButton_m3793401266 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::set_SubmitButton(System.String)
extern "C"  void TouchScriptInputModule_set_SubmitButton_m3167870969 (TouchScriptInputModule_t3561165032 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Behaviors.TouchScriptInputModule::get_CancelButton()
extern "C"  String_t* TouchScriptInputModule_get_CancelButton_m1202776236 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::set_CancelButton(System.String)
extern "C"  void TouchScriptInputModule_set_CancelButton_m2711543923 (TouchScriptInputModule_t3561165032 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::IsModuleSupported()
extern "C"  bool TouchScriptInputModule_IsModuleSupported_m1768391116 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::ShouldActivateModule()
extern "C"  bool TouchScriptInputModule_ShouldActivateModule_m1407783138 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::IsPointerOverGameObject(System.Int32)
extern "C"  bool TouchScriptInputModule_IsPointerOverGameObject_m2057335037 (TouchScriptInputModule_t3561165032 * __this, int32_t ___pointerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::ActivateModule()
extern "C"  void TouchScriptInputModule_ActivateModule_m767953947 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::DeactivateModule()
extern "C"  void TouchScriptInputModule_DeactivateModule_m1627589448 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::Process()
extern "C"  void TouchScriptInputModule_Process_m193394843 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Behaviors.TouchScriptInputModule::ToString()
extern "C"  String_t* TouchScriptInputModule_ToString_m3518776287 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::raycastPointer(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchScriptInputModule_raycastPointer_m1866617394 (TouchScriptInputModule_t3561165032 * __this, PointerEventData_t1599784723 * ___pointerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerEventData TouchScript.Behaviors.TouchScriptInputModule::initPointerData(TouchScript.TouchPoint)
extern "C"  PointerEventData_t1599784723 * TouchScriptInputModule_initPointerData_m458105847 (TouchScriptInputModule_t3561165032 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::injectPointer(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchScriptInputModule_injectPointer_m466633090 (TouchScriptInputModule_t3561165032 * __this, PointerEventData_t1599784723 * ___pointerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerEventData TouchScript.Behaviors.TouchScriptInputModule::updatePointerData(TouchScript.TouchPoint)
extern "C"  PointerEventData_t1599784723 * TouchScriptInputModule_updatePointerData_m1794857396 (TouchScriptInputModule_t3561165032 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::movePointer(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchScriptInputModule_movePointer_m438468814 (TouchScriptInputModule_t3561165032 * __this, PointerEventData_t1599784723 * ___pointerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::endPointer(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchScriptInputModule_endPointer_m351791112 (TouchScriptInputModule_t3561165032 * __this, PointerEventData_t1599784723 * ___pointerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::getPointerData(System.Int32,UnityEngine.EventSystems.PointerEventData&,System.Boolean)
extern "C"  bool TouchScriptInputModule_getPointerData_m2811224277 (TouchScriptInputModule_t3561165032 * __this, int32_t ___id0, PointerEventData_t1599784723 ** ___data1, bool ___create2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::removePointerData(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchScriptInputModule_removePointerData_m831712905 (TouchScriptInputModule_t3561165032 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerEventData TouchScript.Behaviors.TouchScriptInputModule::getLastPointerEventData(System.Int32)
extern "C"  PointerEventData_t1599784723 * TouchScriptInputModule_getLastPointerEventData_m3669664097 (TouchScriptInputModule_t3561165032 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::clearSelection()
extern "C"  void TouchScriptInputModule_clearSelection_m1401941945 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::deselectIfSelectionChanged(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData)
extern "C"  void TouchScriptInputModule_deselectIfSelectionChanged_m2926465204 (TouchScriptInputModule_t3561165032 * __this, GameObject_t1756533147 * ___currentOverGo0, BaseEventData_t2681005625 * ___pointerEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::processBegan(TouchScript.TouchPoint)
extern "C"  void TouchScriptInputModule_processBegan_m818949001 (TouchScriptInputModule_t3561165032 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::processMove(TouchScript.TouchPoint)
extern "C"  void TouchScriptInputModule_processMove_m3064172419 (TouchScriptInputModule_t3561165032 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::processEnded(TouchScript.TouchPoint)
extern "C"  void TouchScriptInputModule_processEnded_m1488387638 (TouchScriptInputModule_t3561165032 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::allowMoveEventProcessing(System.Single)
extern "C"  bool TouchScriptInputModule_allowMoveEventProcessing_m4118875172 (TouchScriptInputModule_t3561165032 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::shouldStartDrag(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  bool TouchScriptInputModule_shouldStartDrag_m781340967 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___pressPos0, Vector2_t2243707579  ___currentPos1, float ___threshold2, bool ___useDragThreshold3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::sendSubmitEventToSelectedObject()
extern "C"  bool TouchScriptInputModule_sendSubmitEventToSelectedObject_m1085494747 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Behaviors.TouchScriptInputModule::getRawMoveVector()
extern "C"  Vector2_t2243707579  TouchScriptInputModule_getRawMoveVector_m3294488417 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::sendMoveEventToSelectedObject()
extern "C"  bool TouchScriptInputModule_sendMoveEventToSelectedObject_m1804757180 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.TouchScriptInputModule::sendUpdateEventToSelectedObject()
extern "C"  bool TouchScriptInputModule_sendUpdateEventToSelectedObject_m4187618820 (TouchScriptInputModule_t3561165032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::touchesBeganHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchScriptInputModule_touchesBeganHandler_m1686860296 (TouchScriptInputModule_t3561165032 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::touchesMovedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchScriptInputModule_touchesMovedHandler_m1842205806 (TouchScriptInputModule_t3561165032 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::touchesEndedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchScriptInputModule_touchesEndedHandler_m2660161453 (TouchScriptInputModule_t3561165032 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.TouchScriptInputModule::touchesCancelledHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchScriptInputModule_touchesCancelledHandler_m162648070 (TouchScriptInputModule_t3561165032 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
