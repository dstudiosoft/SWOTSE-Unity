﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyShillingsScript/<SendBuyRequest>c__Iterator5
struct U3CSendBuyRequestU3Ec__Iterator5_t109805522;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BuyShillingsScript/<SendBuyRequest>c__Iterator5::.ctor()
extern "C"  void U3CSendBuyRequestU3Ec__Iterator5__ctor_m3270251065 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BuyShillingsScript/<SendBuyRequest>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendBuyRequestU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1378331011 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BuyShillingsScript/<SendBuyRequest>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendBuyRequestU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m564530603 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuyShillingsScript/<SendBuyRequest>c__Iterator5::MoveNext()
extern "C"  bool U3CSendBuyRequestU3Ec__Iterator5_MoveNext_m776542039 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyShillingsScript/<SendBuyRequest>c__Iterator5::Dispose()
extern "C"  void U3CSendBuyRequestU3Ec__Iterator5_Dispose_m1350732636 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyShillingsScript/<SendBuyRequest>c__Iterator5::Reset()
extern "C"  void U3CSendBuyRequestU3Ec__Iterator5_Reset_m3976361822 (U3CSendBuyRequestU3Ec__Iterator5_t109805522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
