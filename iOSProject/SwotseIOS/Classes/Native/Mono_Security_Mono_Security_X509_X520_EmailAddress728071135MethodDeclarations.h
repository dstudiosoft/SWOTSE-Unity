﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/EmailAddress
struct EmailAddress_t728071135;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/EmailAddress::.ctor()
extern "C"  void EmailAddress__ctor_m470048069 (EmailAddress_t728071135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
