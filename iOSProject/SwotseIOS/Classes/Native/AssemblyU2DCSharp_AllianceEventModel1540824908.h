﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventModel
struct  AllianceEventModel_t1540824908  : public Il2CppObject
{
public:
	// System.Int64 AllianceEventModel::id
	int64_t ___id_0;
	// System.Int64 AllianceEventModel::alliance
	int64_t ___alliance_1;
	// System.String AllianceEventModel::message
	String_t* ___message_2;
	// System.DateTime AllianceEventModel::date
	DateTime_t693205669  ___date_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1540824908, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_alliance_1() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1540824908, ___alliance_1)); }
	inline int64_t get_alliance_1() const { return ___alliance_1; }
	inline int64_t* get_address_of_alliance_1() { return &___alliance_1; }
	inline void set_alliance_1(int64_t value)
	{
		___alliance_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1540824908, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1540824908, ___date_3)); }
	inline DateTime_t693205669  get_date_3() const { return ___date_3; }
	inline DateTime_t693205669 * get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DateTime_t693205669  value)
	{
		___date_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
