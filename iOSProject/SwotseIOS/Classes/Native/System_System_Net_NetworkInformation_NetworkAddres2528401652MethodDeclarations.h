﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.NetworkAddressChangedEventHandler
struct NetworkAddressChangedEventHandler_t2528401652;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.NetworkInformation.NetworkAddressChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void NetworkAddressChangedEventHandler__ctor_m3179012430 (NetworkAddressChangedEventHandler_t2528401652 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkAddressChangedEventHandler::Invoke(System.Object,System.EventArgs)
extern "C"  void NetworkAddressChangedEventHandler_Invoke_m4011478512 (NetworkAddressChangedEventHandler_t2528401652 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.NetworkInformation.NetworkAddressChangedEventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NetworkAddressChangedEventHandler_BeginInvoke_m3012450555 (NetworkAddressChangedEventHandler_t2528401652 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkAddressChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void NetworkAddressChangedEventHandler_EndInvoke_m474115540 (NetworkAddressChangedEventHandler_t2528401652 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
