﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Hit.Untouchable
struct Untouchable_t2967803006;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_HitTest_ObjectHi3057876522.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"

// System.Void TouchScript.Hit.Untouchable::.ctor()
extern "C"  void Untouchable__ctor_m616149214 (Untouchable_t2967803006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.HitTest/ObjectHitResult TouchScript.Hit.Untouchable::IsHit(TouchScript.Hit.TouchHit)
extern "C"  int32_t Untouchable_IsHit_m2617595853 (Untouchable_t2967803006 * __this, TouchHit_t4186847494  ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
