﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t2823857299;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager
struct  LoginManager_t973619992  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> LoginManager::emperorsNameToId
	Dictionary_2_t2823857299 * ___emperorsNameToId_2;
	// System.Boolean LoginManager::gotBuildingConstants
	bool ___gotBuildingConstants_3;
	// System.Boolean LoginManager::gotUnitConstants
	bool ___gotUnitConstants_4;
	// System.Boolean LoginManager::gotTutorialPages
	bool ___gotTutorialPages_5;
	// System.Boolean LoginManager::gotPlayerInfo
	bool ___gotPlayerInfo_6;
	// System.Boolean LoginManager::gotPlayerCities
	bool ___gotPlayerCities_7;
	// System.Boolean LoginManager::gotCityResources
	bool ___gotCityResources_8;
	// System.Boolean LoginManager::gotCityBuildings
	bool ___gotCityBuildings_9;
	// System.Boolean LoginManager::gotCityFields
	bool ___gotCityFields_10;
	// System.Boolean LoginManager::gotCityValleys
	bool ___gotCityValleys_11;
	// System.Boolean LoginManager::gotCityUnits
	bool ___gotCityUnits_12;
	// System.Boolean LoginManager::gotCityGenerals
	bool ___gotCityGenerals_13;
	// System.Boolean LoginManager::gotCityTreasures
	bool ___gotCityTreasures_14;
	// SimpleEvent LoginManager::onGetEmperors
	SimpleEvent_t1509017202 * ___onGetEmperors_15;
	// SimpleEvent LoginManager::onRegister
	SimpleEvent_t1509017202 * ___onRegister_16;
	// SimpleEvent LoginManager::onLogin
	SimpleEvent_t1509017202 * ___onLogin_17;

public:
	inline static int32_t get_offset_of_emperorsNameToId_2() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___emperorsNameToId_2)); }
	inline Dictionary_2_t2823857299 * get_emperorsNameToId_2() const { return ___emperorsNameToId_2; }
	inline Dictionary_2_t2823857299 ** get_address_of_emperorsNameToId_2() { return &___emperorsNameToId_2; }
	inline void set_emperorsNameToId_2(Dictionary_2_t2823857299 * value)
	{
		___emperorsNameToId_2 = value;
		Il2CppCodeGenWriteBarrier(&___emperorsNameToId_2, value);
	}

	inline static int32_t get_offset_of_gotBuildingConstants_3() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotBuildingConstants_3)); }
	inline bool get_gotBuildingConstants_3() const { return ___gotBuildingConstants_3; }
	inline bool* get_address_of_gotBuildingConstants_3() { return &___gotBuildingConstants_3; }
	inline void set_gotBuildingConstants_3(bool value)
	{
		___gotBuildingConstants_3 = value;
	}

	inline static int32_t get_offset_of_gotUnitConstants_4() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotUnitConstants_4)); }
	inline bool get_gotUnitConstants_4() const { return ___gotUnitConstants_4; }
	inline bool* get_address_of_gotUnitConstants_4() { return &___gotUnitConstants_4; }
	inline void set_gotUnitConstants_4(bool value)
	{
		___gotUnitConstants_4 = value;
	}

	inline static int32_t get_offset_of_gotTutorialPages_5() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotTutorialPages_5)); }
	inline bool get_gotTutorialPages_5() const { return ___gotTutorialPages_5; }
	inline bool* get_address_of_gotTutorialPages_5() { return &___gotTutorialPages_5; }
	inline void set_gotTutorialPages_5(bool value)
	{
		___gotTutorialPages_5 = value;
	}

	inline static int32_t get_offset_of_gotPlayerInfo_6() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotPlayerInfo_6)); }
	inline bool get_gotPlayerInfo_6() const { return ___gotPlayerInfo_6; }
	inline bool* get_address_of_gotPlayerInfo_6() { return &___gotPlayerInfo_6; }
	inline void set_gotPlayerInfo_6(bool value)
	{
		___gotPlayerInfo_6 = value;
	}

	inline static int32_t get_offset_of_gotPlayerCities_7() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotPlayerCities_7)); }
	inline bool get_gotPlayerCities_7() const { return ___gotPlayerCities_7; }
	inline bool* get_address_of_gotPlayerCities_7() { return &___gotPlayerCities_7; }
	inline void set_gotPlayerCities_7(bool value)
	{
		___gotPlayerCities_7 = value;
	}

	inline static int32_t get_offset_of_gotCityResources_8() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityResources_8)); }
	inline bool get_gotCityResources_8() const { return ___gotCityResources_8; }
	inline bool* get_address_of_gotCityResources_8() { return &___gotCityResources_8; }
	inline void set_gotCityResources_8(bool value)
	{
		___gotCityResources_8 = value;
	}

	inline static int32_t get_offset_of_gotCityBuildings_9() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityBuildings_9)); }
	inline bool get_gotCityBuildings_9() const { return ___gotCityBuildings_9; }
	inline bool* get_address_of_gotCityBuildings_9() { return &___gotCityBuildings_9; }
	inline void set_gotCityBuildings_9(bool value)
	{
		___gotCityBuildings_9 = value;
	}

	inline static int32_t get_offset_of_gotCityFields_10() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityFields_10)); }
	inline bool get_gotCityFields_10() const { return ___gotCityFields_10; }
	inline bool* get_address_of_gotCityFields_10() { return &___gotCityFields_10; }
	inline void set_gotCityFields_10(bool value)
	{
		___gotCityFields_10 = value;
	}

	inline static int32_t get_offset_of_gotCityValleys_11() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityValleys_11)); }
	inline bool get_gotCityValleys_11() const { return ___gotCityValleys_11; }
	inline bool* get_address_of_gotCityValleys_11() { return &___gotCityValleys_11; }
	inline void set_gotCityValleys_11(bool value)
	{
		___gotCityValleys_11 = value;
	}

	inline static int32_t get_offset_of_gotCityUnits_12() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityUnits_12)); }
	inline bool get_gotCityUnits_12() const { return ___gotCityUnits_12; }
	inline bool* get_address_of_gotCityUnits_12() { return &___gotCityUnits_12; }
	inline void set_gotCityUnits_12(bool value)
	{
		___gotCityUnits_12 = value;
	}

	inline static int32_t get_offset_of_gotCityGenerals_13() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityGenerals_13)); }
	inline bool get_gotCityGenerals_13() const { return ___gotCityGenerals_13; }
	inline bool* get_address_of_gotCityGenerals_13() { return &___gotCityGenerals_13; }
	inline void set_gotCityGenerals_13(bool value)
	{
		___gotCityGenerals_13 = value;
	}

	inline static int32_t get_offset_of_gotCityTreasures_14() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___gotCityTreasures_14)); }
	inline bool get_gotCityTreasures_14() const { return ___gotCityTreasures_14; }
	inline bool* get_address_of_gotCityTreasures_14() { return &___gotCityTreasures_14; }
	inline void set_gotCityTreasures_14(bool value)
	{
		___gotCityTreasures_14 = value;
	}

	inline static int32_t get_offset_of_onGetEmperors_15() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___onGetEmperors_15)); }
	inline SimpleEvent_t1509017202 * get_onGetEmperors_15() const { return ___onGetEmperors_15; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetEmperors_15() { return &___onGetEmperors_15; }
	inline void set_onGetEmperors_15(SimpleEvent_t1509017202 * value)
	{
		___onGetEmperors_15 = value;
		Il2CppCodeGenWriteBarrier(&___onGetEmperors_15, value);
	}

	inline static int32_t get_offset_of_onRegister_16() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___onRegister_16)); }
	inline SimpleEvent_t1509017202 * get_onRegister_16() const { return ___onRegister_16; }
	inline SimpleEvent_t1509017202 ** get_address_of_onRegister_16() { return &___onRegister_16; }
	inline void set_onRegister_16(SimpleEvent_t1509017202 * value)
	{
		___onRegister_16 = value;
		Il2CppCodeGenWriteBarrier(&___onRegister_16, value);
	}

	inline static int32_t get_offset_of_onLogin_17() { return static_cast<int32_t>(offsetof(LoginManager_t973619992, ___onLogin_17)); }
	inline SimpleEvent_t1509017202 * get_onLogin_17() const { return ___onLogin_17; }
	inline SimpleEvent_t1509017202 ** get_address_of_onLogin_17() { return &___onLogin_17; }
	inline void set_onLogin_17(SimpleEvent_t1509017202 * value)
	{
		___onLogin_17 = value;
		Il2CppCodeGenWriteBarrier(&___onLogin_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
