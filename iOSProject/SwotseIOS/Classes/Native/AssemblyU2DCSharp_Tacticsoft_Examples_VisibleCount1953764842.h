﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.VisibleCounterCell
struct  VisibleCounterCell_t1953764842  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text Tacticsoft.Examples.VisibleCounterCell::m_rowNumberText
	Text_t356221433 * ___m_rowNumberText_2;
	// UnityEngine.UI.Text Tacticsoft.Examples.VisibleCounterCell::m_visibleCountText
	Text_t356221433 * ___m_visibleCountText_3;
	// System.Int32 Tacticsoft.Examples.VisibleCounterCell::m_numTimesBecameVisible
	int32_t ___m_numTimesBecameVisible_4;

public:
	inline static int32_t get_offset_of_m_rowNumberText_2() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1953764842, ___m_rowNumberText_2)); }
	inline Text_t356221433 * get_m_rowNumberText_2() const { return ___m_rowNumberText_2; }
	inline Text_t356221433 ** get_address_of_m_rowNumberText_2() { return &___m_rowNumberText_2; }
	inline void set_m_rowNumberText_2(Text_t356221433 * value)
	{
		___m_rowNumberText_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_rowNumberText_2, value);
	}

	inline static int32_t get_offset_of_m_visibleCountText_3() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1953764842, ___m_visibleCountText_3)); }
	inline Text_t356221433 * get_m_visibleCountText_3() const { return ___m_visibleCountText_3; }
	inline Text_t356221433 ** get_address_of_m_visibleCountText_3() { return &___m_visibleCountText_3; }
	inline void set_m_visibleCountText_3(Text_t356221433 * value)
	{
		___m_visibleCountText_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_visibleCountText_3, value);
	}

	inline static int32_t get_offset_of_m_numTimesBecameVisible_4() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1953764842, ___m_numTimesBecameVisible_4)); }
	inline int32_t get_m_numTimesBecameVisible_4() const { return ___m_numTimesBecameVisible_4; }
	inline int32_t* get_address_of_m_numTimesBecameVisible_4() { return &___m_numTimesBecameVisible_4; }
	inline void set_m_numTimesBecameVisible_4(int32_t value)
	{
		___m_numTimesBecameVisible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
