﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen444384941.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"

// System.Void System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3807065410_gshared (InternalEnumerator_1_t444384941 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3807065410(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t444384941 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3807065410_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m160408858_gshared (InternalEnumerator_1_t444384941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m160408858(__this, method) ((  void (*) (InternalEnumerator_1_t444384941 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m160408858_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2096446724_gshared (InternalEnumerator_1_t444384941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2096446724(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t444384941 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2096446724_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2119333167_gshared (InternalEnumerator_1_t444384941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2119333167(__this, method) ((  void (*) (InternalEnumerator_1_t444384941 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2119333167_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1663738282_gshared (InternalEnumerator_1_t444384941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1663738282(__this, method) ((  bool (*) (InternalEnumerator_1_t444384941 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1663738282_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>::get_Current()
extern "C"  TouchData_t3880599975  InternalEnumerator_1_get_Current_m1882051307_gshared (InternalEnumerator_1_t444384941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1882051307(__this, method) ((  TouchData_t3880599975  (*) (InternalEnumerator_1_t444384941 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1882051307_gshared)(__this, method)
