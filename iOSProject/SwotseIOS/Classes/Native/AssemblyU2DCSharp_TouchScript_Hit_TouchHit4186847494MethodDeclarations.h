﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Transform
struct Transform_t3275118058;
// TouchScript.Hit.TouchHit
struct TouchHit_t4186847494;
struct TouchHit_t4186847494_marshaled_pinvoke;
struct TouchHit_t4186847494_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit_TouchHi1472696400.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TouchScript.Hit.TouchHit::.ctor(UnityEngine.Transform)
extern "C"  void TouchHit__ctor_m3123762395 (TouchHit_t4186847494 * __this, Transform_t3275118058 * ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Hit.TouchHit::.ctor(UnityEngine.RaycastHit)
extern "C"  void TouchHit__ctor_m3004682461 (TouchHit_t4186847494 * __this, RaycastHit_t87180320  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Hit.TouchHit::.ctor(UnityEngine.RaycastHit2D)
extern "C"  void TouchHit__ctor_m4140996379 (TouchHit_t4186847494 * __this, RaycastHit2D_t4063908774  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Hit.TouchHit::.ctor(UnityEngine.EventSystems.RaycastResult)
extern "C"  void TouchHit__ctor_m2844760857 (TouchHit_t4186847494 * __this, RaycastResult_t21186376  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.TouchHit/TouchHitType TouchScript.Hit.TouchHit::get_Type()
extern "C"  int32_t TouchHit_get_Type_m2712823272 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform TouchScript.Hit.TouchHit::get_Transform()
extern "C"  Transform_t3275118058 * TouchHit_get_Transform_m1705867317 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit TouchScript.Hit.TouchHit::get_RaycastHit()
extern "C"  RaycastHit_t87180320  TouchHit_get_RaycastHit_m1767946069 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D TouchScript.Hit.TouchHit::get_RaycastHit2D()
extern "C"  RaycastHit2D_t4063908774  TouchHit_get_RaycastHit2D_m3167789297 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.RaycastResult TouchScript.Hit.TouchHit::get_RaycastResult()
extern "C"  RaycastResult_t21186376  TouchHit_get_RaycastResult_m3813359189 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Hit.TouchHit::get_Point()
extern "C"  Vector3_t2243707580  TouchHit_get_Point_m3121832411 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Hit.TouchHit::get_Normal()
extern "C"  Vector3_t2243707580  TouchHit_get_Normal_m3894785068 (TouchHit_t4186847494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TouchHit_t4186847494;
struct TouchHit_t4186847494_marshaled_pinvoke;

extern "C" void TouchHit_t4186847494_marshal_pinvoke(const TouchHit_t4186847494& unmarshaled, TouchHit_t4186847494_marshaled_pinvoke& marshaled);
extern "C" void TouchHit_t4186847494_marshal_pinvoke_back(const TouchHit_t4186847494_marshaled_pinvoke& marshaled, TouchHit_t4186847494& unmarshaled);
extern "C" void TouchHit_t4186847494_marshal_pinvoke_cleanup(TouchHit_t4186847494_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TouchHit_t4186847494;
struct TouchHit_t4186847494_marshaled_com;

extern "C" void TouchHit_t4186847494_marshal_com(const TouchHit_t4186847494& unmarshaled, TouchHit_t4186847494_marshaled_com& marshaled);
extern "C" void TouchHit_t4186847494_marshal_com_back(const TouchHit_t4186847494_marshaled_com& marshaled, TouchHit_t4186847494& unmarshaled);
extern "C" void TouchHit_t4186847494_marshal_com_cleanup(TouchHit_t4186847494_marshaled_com& marshaled);
