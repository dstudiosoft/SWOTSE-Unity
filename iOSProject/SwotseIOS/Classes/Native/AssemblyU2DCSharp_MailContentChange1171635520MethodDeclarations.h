﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailContentChange
struct MailContentChange_t1171635520;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailContentChange::.ctor()
extern "C"  void MailContentChange__ctor_m2316585085 (MailContentChange_t1171635520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailContentChange::OnEnable()
extern "C"  void MailContentChange_OnEnable_m259155557 (MailContentChange_t1171635520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailContentChange::openPanel(System.String)
extern "C"  void MailContentChange_openPanel_m3427098581 (MailContentChange_t1171635520 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailContentChange::disablePanels()
extern "C"  void MailContentChange_disablePanels_m1422548816 (MailContentChange_t1171635520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailContentChange::OpenNewMessage()
extern "C"  void MailContentChange_OpenNewMessage_m2574120942 (MailContentChange_t1171635520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
