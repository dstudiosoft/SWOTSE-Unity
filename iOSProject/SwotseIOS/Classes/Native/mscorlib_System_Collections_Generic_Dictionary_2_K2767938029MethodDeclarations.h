﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1941751998(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2767938029 *, Dictionary_2_t284440258 *, const MethodInfo*))KeyCollection__ctor_m3555561037_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2512085608(__this, ___item0, method) ((  void (*) (KeyCollection_t2767938029 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3811841551(__this, method) ((  void (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3735357152(__this, ___item0, method) ((  bool (*) (KeyCollection_t2767938029 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m81519087(__this, ___item0, method) ((  bool (*) (KeyCollection_t2767938029 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3035055381(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4156462009(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2767938029 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2745552464(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m660305027(__this, method) ((  bool (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3160249557(__this, method) ((  bool (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2218965241(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1360826807(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2767938029 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4056130090_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3316481394(__this, method) ((  Enumerator_t2973943696  (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_GetEnumerator_m627495629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Tacticsoft.TableViewCell>::get_Count()
#define KeyCollection_get_Count_m1055853869(__this, method) ((  int32_t (*) (KeyCollection_t2767938029 *, const MethodInfo*))KeyCollection_get_Count_m2942903136_gshared)(__this, method)
