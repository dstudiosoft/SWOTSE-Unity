﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPv6InterfaceProperties
struct Win32IPv6InterfaceProperties_t2993999558;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"

// System.Void System.Net.NetworkInformation.Win32IPv6InterfaceProperties::.ctor(System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPv6InterfaceProperties__ctor_m1650543462 (Win32IPv6InterfaceProperties_t2993999558 * __this, Win32_MIB_IFROW_t4215928996  ___mib0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPv6InterfaceProperties::get_Index()
extern "C"  int32_t Win32IPv6InterfaceProperties_get_Index_m2791256761 (Win32IPv6InterfaceProperties_t2993999558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPv6InterfaceProperties::get_Mtu()
extern "C"  int32_t Win32IPv6InterfaceProperties_get_Mtu_m3818533903 (Win32IPv6InterfaceProperties_t2993999558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
