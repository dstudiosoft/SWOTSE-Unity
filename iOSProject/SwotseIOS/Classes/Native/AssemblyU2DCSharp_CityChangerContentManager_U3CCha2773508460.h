﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoginManager
struct LoginManager_t973619992;
// CityItemLine
struct CityItemLine_t2575739974;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TestWorldChunksManager
struct TestWorldChunksManager_t3581849513;
// System.Object
struct Il2CppObject;
// CityChangerContentManager
struct CityChangerContentManager_t4168518203;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityChangerContentManager/<ChangeCity>c__Iterator8
struct  U3CChangeCityU3Ec__Iterator8_t2773508460  : public Il2CppObject
{
public:
	// LoginManager CityChangerContentManager/<ChangeCity>c__Iterator8::<loginManager>__0
	LoginManager_t973619992 * ___U3CloginManagerU3E__0_0;
	// System.Int32 CityChangerContentManager/<ChangeCity>c__Iterator8::<i>__1
	int32_t ___U3CiU3E__1_1;
	// CityItemLine CityChangerContentManager/<ChangeCity>c__Iterator8::<line>__2
	CityItemLine_t2575739974 * ___U3ClineU3E__2_2;
	// System.Int64 CityChangerContentManager/<ChangeCity>c__Iterator8::cityId
	int64_t ___cityId_3;
	// System.Int32 CityChangerContentManager/<ChangeCity>c__Iterator8::<i>__3
	int32_t ___U3CiU3E__3_4;
	// CityItemLine CityChangerContentManager/<ChangeCity>c__Iterator8::<line>__4
	CityItemLine_t2575739974 * ___U3ClineU3E__4_5;
	// UnityEngine.GameObject CityChangerContentManager/<ChangeCity>c__Iterator8::<world>__5
	GameObject_t1756533147 * ___U3CworldU3E__5_6;
	// UnityEngine.GameObject CityChangerContentManager/<ChangeCity>c__Iterator8::<world>__6
	GameObject_t1756533147 * ___U3CworldU3E__6_7;
	// TestWorldChunksManager CityChangerContentManager/<ChangeCity>c__Iterator8::<worldManager>__7
	TestWorldChunksManager_t3581849513 * ___U3CworldManagerU3E__7_8;
	// System.Int32 CityChangerContentManager/<ChangeCity>c__Iterator8::$PC
	int32_t ___U24PC_9;
	// System.Object CityChangerContentManager/<ChangeCity>c__Iterator8::$current
	Il2CppObject * ___U24current_10;
	// System.Int64 CityChangerContentManager/<ChangeCity>c__Iterator8::<$>cityId
	int64_t ___U3CU24U3EcityId_11;
	// CityChangerContentManager CityChangerContentManager/<ChangeCity>c__Iterator8::<>f__this
	CityChangerContentManager_t4168518203 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CloginManagerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CloginManagerU3E__0_0)); }
	inline LoginManager_t973619992 * get_U3CloginManagerU3E__0_0() const { return ___U3CloginManagerU3E__0_0; }
	inline LoginManager_t973619992 ** get_address_of_U3CloginManagerU3E__0_0() { return &___U3CloginManagerU3E__0_0; }
	inline void set_U3CloginManagerU3E__0_0(LoginManager_t973619992 * value)
	{
		___U3CloginManagerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloginManagerU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3ClineU3E__2_2() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3ClineU3E__2_2)); }
	inline CityItemLine_t2575739974 * get_U3ClineU3E__2_2() const { return ___U3ClineU3E__2_2; }
	inline CityItemLine_t2575739974 ** get_address_of_U3ClineU3E__2_2() { return &___U3ClineU3E__2_2; }
	inline void set_U3ClineU3E__2_2(CityItemLine_t2575739974 * value)
	{
		___U3ClineU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClineU3E__2_2, value);
	}

	inline static int32_t get_offset_of_cityId_3() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___cityId_3)); }
	inline int64_t get_cityId_3() const { return ___cityId_3; }
	inline int64_t* get_address_of_cityId_3() { return &___cityId_3; }
	inline void set_cityId_3(int64_t value)
	{
		___cityId_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_4() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CiU3E__3_4)); }
	inline int32_t get_U3CiU3E__3_4() const { return ___U3CiU3E__3_4; }
	inline int32_t* get_address_of_U3CiU3E__3_4() { return &___U3CiU3E__3_4; }
	inline void set_U3CiU3E__3_4(int32_t value)
	{
		___U3CiU3E__3_4 = value;
	}

	inline static int32_t get_offset_of_U3ClineU3E__4_5() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3ClineU3E__4_5)); }
	inline CityItemLine_t2575739974 * get_U3ClineU3E__4_5() const { return ___U3ClineU3E__4_5; }
	inline CityItemLine_t2575739974 ** get_address_of_U3ClineU3E__4_5() { return &___U3ClineU3E__4_5; }
	inline void set_U3ClineU3E__4_5(CityItemLine_t2575739974 * value)
	{
		___U3ClineU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClineU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CworldU3E__5_6() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CworldU3E__5_6)); }
	inline GameObject_t1756533147 * get_U3CworldU3E__5_6() const { return ___U3CworldU3E__5_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CworldU3E__5_6() { return &___U3CworldU3E__5_6; }
	inline void set_U3CworldU3E__5_6(GameObject_t1756533147 * value)
	{
		___U3CworldU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CworldU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CworldU3E__6_7() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CworldU3E__6_7)); }
	inline GameObject_t1756533147 * get_U3CworldU3E__6_7() const { return ___U3CworldU3E__6_7; }
	inline GameObject_t1756533147 ** get_address_of_U3CworldU3E__6_7() { return &___U3CworldU3E__6_7; }
	inline void set_U3CworldU3E__6_7(GameObject_t1756533147 * value)
	{
		___U3CworldU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CworldU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U3CworldManagerU3E__7_8() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CworldManagerU3E__7_8)); }
	inline TestWorldChunksManager_t3581849513 * get_U3CworldManagerU3E__7_8() const { return ___U3CworldManagerU3E__7_8; }
	inline TestWorldChunksManager_t3581849513 ** get_address_of_U3CworldManagerU3E__7_8() { return &___U3CworldManagerU3E__7_8; }
	inline void set_U3CworldManagerU3E__7_8(TestWorldChunksManager_t3581849513 * value)
	{
		___U3CworldManagerU3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CworldManagerU3E__7_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcityId_11() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CU24U3EcityId_11)); }
	inline int64_t get_U3CU24U3EcityId_11() const { return ___U3CU24U3EcityId_11; }
	inline int64_t* get_address_of_U3CU24U3EcityId_11() { return &___U3CU24U3EcityId_11; }
	inline void set_U3CU24U3EcityId_11(int64_t value)
	{
		___U3CU24U3EcityId_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator8_t2773508460, ___U3CU3Ef__this_12)); }
	inline CityChangerContentManager_t4168518203 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline CityChangerContentManager_t4168518203 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(CityChangerContentManager_t4168518203 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
