﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor()
#define Dictionary_2__ctor_m3422731920(__this, method) ((  void (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2__ctor_m3997847064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m338284510(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2284756127_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m3767799473(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2286074618_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Int32)
#define Dictionary_2__ctor_m153087422(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3111963761_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m366013310(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m878210833_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1042244585(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m289325280_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m3269933232(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t284440258 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m965168575_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m571852665(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4245826226_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3766120137(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3427730524_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m837366882(__this, method) ((  bool (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1135994675_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3997443341(__this, method) ((  bool (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2156058800_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2773990503(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2945412702_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m176140524(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m941667911_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m2657557865(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3189569330_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m2518587285(__this, ___key0, method) ((  bool (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3937948050_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2869881728(__this, ___key0, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3199539467_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4035908867(__this, method) ((  bool (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m304009368_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m824437443(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2487129350_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2732766113(__this, method) ((  bool (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1111602362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m109013778(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t284440258 *, KeyValuePair_2_t2336752776 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1043757703_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3120801654(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t284440258 *, KeyValuePair_2_t2336752776 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1927335261_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2140060670(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t284440258 *, KeyValuePair_2U5BU5D_t105508953*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3678641635_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1691084759(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t284440258 *, KeyValuePair_2_t2336752776 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m181279132_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m929046963(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1985034736_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3935231128(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3830548821_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2223166889(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m631947640_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m254890694(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1284065099_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::get_Count()
#define Dictionary_2_get_Count_m1411215946(__this, method) ((  int32_t (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_get_Count_m2168147420_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::get_Item(TKey)
#define Dictionary_2_get_Item_m34701014(__this, ___key0, method) ((  TableViewCell_t1276614623 * (*) (Dictionary_2_t284440258 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m4277290203_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1720436193(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_set_Item_m3121864719_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m3498696359(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3666073812_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m989159866(__this, ___size0, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3810830177_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1514659032(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1541945891_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m823335810(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2336752776  (*) (Il2CppObject * /* static, unused */, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_make_pair_m90480045_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m3387316996(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_pick_key_m761174441_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1477654820(__this /* static, unused */, ___key0, ___value1, method) ((  TableViewCell_t1276614623 * (*) (Il2CppObject * /* static, unused */, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_pick_value_m353965321_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1288587331(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t284440258 *, KeyValuePair_2U5BU5D_t105508953*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1956977846_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::Resize()
#define Dictionary_2_Resize_m335508465(__this, method) ((  void (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_Resize_m2532139610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::Add(TKey,TValue)
#define Dictionary_2_Add_m701488458(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t284440258 *, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_Add_m2839642701_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::Clear()
#define Dictionary_2_Clear_m2365148650(__this, method) ((  void (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_Clear_m899854001_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3852974182(__this, ___key0, method) ((  bool (*) (Dictionary_2_t284440258 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m255952723_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2750685094(__this, ___value0, method) ((  bool (*) (Dictionary_2_t284440258 *, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_ContainsValue_m392092147_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m4018646111(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t284440258 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m233109612_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3683440199(__this, ___sender0, method) ((  void (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2092139626_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::Remove(TKey)
#define Dictionary_2_Remove_m1245078367(__this, ___key0, method) ((  bool (*) (Dictionary_2_t284440258 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m602713029_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m2745456133(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t284440258 *, int32_t, TableViewCell_t1276614623 **, const MethodInfo*))Dictionary_2_TryGetValue_m3187691483_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::get_Keys()
#define Dictionary_2_get_Keys_m1434050682(__this, method) ((  KeyCollection_t2767938029 * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_get_Keys_m1900997095_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::get_Values()
#define Dictionary_2_get_Values_m4209308690(__this, method) ((  ValueCollection_t3282467397 * (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_get_Values_m372946023_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m2828801449(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2900575080_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2095027521(__this, ___value0, method) ((  TableViewCell_t1276614623 * (*) (Dictionary_2_t284440258 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m14471464_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m2572592383(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t284440258 *, KeyValuePair_2_t2336752776 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m790970878_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m658501298(__this, method) ((  Enumerator_t1604464960  (*) (Dictionary_2_t284440258 *, const MethodInfo*))Dictionary_2_GetEnumerator_m706253773_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m955135785(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, TableViewCell_t1276614623 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m741309042_gshared)(__this /* static, unused */, ___key0, ___value1, method)
