﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// FinancialContentChanger
struct FinancialContentChanger_t2753898366;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger/<CancelOfferRequest>c__IteratorD
struct  U3CCancelOfferRequestU3Ec__IteratorD_t2972413101  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm FinancialContentChanger/<CancelOfferRequest>c__IteratorD::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// System.String FinancialContentChanger/<CancelOfferRequest>c__IteratorD::id
	String_t* ___id_1;
	// UnityEngine.Networking.UnityWebRequest FinancialContentChanger/<CancelOfferRequest>c__IteratorD::<request>__1
	UnityWebRequest_t254341728 * ___U3CrequestU3E__1_2;
	// JSONObject FinancialContentChanger/<CancelOfferRequest>c__IteratorD::<response>__2
	JSONObject_t1971882247 * ___U3CresponseU3E__2_3;
	// System.Int32 FinancialContentChanger/<CancelOfferRequest>c__IteratorD::$PC
	int32_t ___U24PC_4;
	// System.Object FinancialContentChanger/<CancelOfferRequest>c__IteratorD::$current
	Il2CppObject * ___U24current_5;
	// System.String FinancialContentChanger/<CancelOfferRequest>c__IteratorD::<$>id
	String_t* ___U3CU24U3Eid_6;
	// FinancialContentChanger FinancialContentChanger/<CancelOfferRequest>c__IteratorD::<>f__this
	FinancialContentChanger_t2753898366 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U3CrequestU3E__1_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__1_2() const { return ___U3CrequestU3E__1_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__1_2() { return &___U3CrequestU3E__1_2; }
	inline void set_U3CrequestU3E__1_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__2_3() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U3CresponseU3E__2_3)); }
	inline JSONObject_t1971882247 * get_U3CresponseU3E__2_3() const { return ___U3CresponseU3E__2_3; }
	inline JSONObject_t1971882247 ** get_address_of_U3CresponseU3E__2_3() { return &___U3CresponseU3E__2_3; }
	inline void set_U3CresponseU3E__2_3(JSONObject_t1971882247 * value)
	{
		___U3CresponseU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_6() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U3CU24U3Eid_6)); }
	inline String_t* get_U3CU24U3Eid_6() const { return ___U3CU24U3Eid_6; }
	inline String_t** get_address_of_U3CU24U3Eid_6() { return &___U3CU24U3Eid_6; }
	inline void set_U3CU24U3Eid_6(String_t* value)
	{
		___U3CU24U3Eid_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__IteratorD_t2972413101, ___U3CU3Ef__this_7)); }
	inline FinancialContentChanger_t2753898366 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline FinancialContentChanger_t2753898366 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(FinancialContentChanger_t2753898366 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
