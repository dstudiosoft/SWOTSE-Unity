﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// TouchScript.Layers.UILayer
struct UILayer_t1746714259;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t2345966477;
// TouchScript.Gestures.LongPressGesture
struct LongPressGesture_t3651199768;
// TouchScript.Gestures.TapGesture
struct TapGesture_t1114317921;
// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>
struct Func_4_t3126574096;
// System.Action`2<System.Int32,UnityEngine.Vector2>
struct Action_2_t3382406540;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// TouchScript.Tags
struct Tags_t3560929397;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1074268497;
// WebSocket
struct WebSocket_t1645401340;
// WorldChat
struct WorldChat_t753147188;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// WorldChunkManager
struct WorldChunkManager_t1513314516;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t1334725428;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t2689211518;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// TouchScript.TouchPoint
struct TouchPoint_t873891735;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t811567916;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Version
struct Version_t3456873960;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>
struct List_1_t2806800170;
// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>
struct EventHandler_1_t2939568409;
// TouchScript.Layers.ILayerDelegate
struct ILayerDelegate_t2167973062;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2315592447;
// System.Collections.Generic.List`1<TouchScript.Gestures.ITransformGesture>
struct List_1_t3798361940;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct List_1_t3532169525;
// SmartLocalization.LanguageManager
struct LanguageManager_t2767934455;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// WorldFieldModel[0...,0...]
struct WorldFieldModelU5BU2CU5D_t237916709;
// SimpleEvent
struct SimpleEvent_t129249603;
// WorldFieldModel
struct WorldFieldModel_t2417974361;
// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct TouchProxyBase_t1980850257;
// TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>
struct ObjectPool_1_t2604094430;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>
struct Dictionary_2_t869563588;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t765533606;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1245047080;
// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>>
struct Action_2_t3198859162;
// System.Action`1<UnityEngine.Transform>
struct Action_1_t3772833516;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t1656671064;
// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t518150435;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>
struct ObjectPool_1_t1868291253;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct ObjectPool_1_t2969210650;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>>
struct ObjectPool_1_t1400717540;
// TestWorldChunksManager
struct TestWorldChunksManager_t1200850834;
// TouchScript.InputSources.ICoordinatesRemapper
struct ICoordinatesRemapper_t3288837556;
// TouchScript.TouchManagerInstance
struct TouchManagerInstance_t3252675660;
// ActionManager
struct ActionManager_t2430268190;
// AllianceContentChanger
struct AllianceContentChanger_t2140085977;
// TreasureManager
struct TreasureManager_t110095690;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// TouchScript.InputSources.InputHandlers.MouseHandler
struct MouseHandler_t2265731789;
// TouchScript.InputSources.InputHandlers.TouchHandler
struct TouchHandler_t684722057;
// System.Collections.Generic.List`1<TouchScript.Hit.HitTest>
struct List_1_t2630392082;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>
struct Dictionary_2_t1501986344;
// System.EventHandler`1<TouchScript.TouchEventArgs>
struct EventHandler_1_t2376944037;
// System.EventHandler
struct EventHandler_t1348719766;
// TouchScript.Devices.Display.IDisplayDevice
struct IDisplayDevice_t3094031949;
// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource>
struct List_1_t967242533;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.TouchPoint>
struct Dictionary_2_t4057572362;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// TouchScript.Utils.ObjectPool`1<TouchScript.TouchPoint>
struct ObjectPool_1_t1497135908;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_t751297372;
// System.Predicate`1<TouchScript.Layers.TouchLayer>
struct Predicate_1_t2160019552;
// System.Collections.Generic.List`1<TouchScript.InputSources.TuioObjectMapping>
struct List_1_t3907028109;
// TUIOsharp.TuioServer
struct TuioServer_t3158170151;
// TUIOsharp.DataProcessors.CursorProcessor
struct CursorProcessor_t3936223090;
// TUIOsharp.DataProcessors.ObjectProcessor
struct ObjectProcessor_t2877610401;
// TUIOsharp.DataProcessors.BlobProcessor
struct BlobProcessor_t2126871202;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>
struct Dictionary_2_t2551302653;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>
struct Dictionary_2_t1664370694;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioObject,TouchScript.TouchPoint>
struct Dictionary_2_t1027656984;
// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>
struct EventHandler_1_t766498446;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// TouchScript.ITouchManager
struct ITouchManager_t2123341201;
// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint>
struct ReadOnlyCollection_1_t2086468022;
// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>
struct TimedSequence_1_t1745006843;
// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t955425494;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t2528076708;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>
struct List_1_t3751656731;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t1693108292;
// TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>
struct TimedSequence_1_t3027344631;
// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>
struct EventHandler_1_t238911699;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1227816588_H
#define U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1227816588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_MoveTouch>c__AnonStorey1
struct  U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1227816588  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_MoveTouch>c__AnonStorey1::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1227816588, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1227816588_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T770193313_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T770193313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.UILayer/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t770193313  : public RuntimeObject
{
public:
	// TouchScript.Layers.UILayer TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$this
	UILayer_t1746714259 * ___U24this_0;
	// System.Object TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t770193313, ___U24this_0)); }
	inline UILayer_t1746714259 * get_U24this_0() const { return ___U24this_0; }
	inline UILayer_t1746714259 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UILayer_t1746714259 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t770193313, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t770193313, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t770193313, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T770193313_H
#ifndef TAGS_T3560929397_H
#define TAGS_T3560929397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Tags
struct  Tags_t3560929397  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> TouchScript.Tags::tagList
	List_1_t3319525431 * ___tagList_5;
	// System.Collections.Generic.HashSet`1<System.String> TouchScript.Tags::tags
	HashSet_1_t412400163 * ___tags_6;
	// System.String TouchScript.Tags::stringValue
	String_t* ___stringValue_7;

public:
	inline static int32_t get_offset_of_tagList_5() { return static_cast<int32_t>(offsetof(Tags_t3560929397, ___tagList_5)); }
	inline List_1_t3319525431 * get_tagList_5() const { return ___tagList_5; }
	inline List_1_t3319525431 ** get_address_of_tagList_5() { return &___tagList_5; }
	inline void set_tagList_5(List_1_t3319525431 * value)
	{
		___tagList_5 = value;
		Il2CppCodeGenWriteBarrier((&___tagList_5), value);
	}

	inline static int32_t get_offset_of_tags_6() { return static_cast<int32_t>(offsetof(Tags_t3560929397, ___tags_6)); }
	inline HashSet_1_t412400163 * get_tags_6() const { return ___tags_6; }
	inline HashSet_1_t412400163 ** get_address_of_tags_6() { return &___tags_6; }
	inline void set_tags_6(HashSet_1_t412400163 * value)
	{
		___tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___tags_6), value);
	}

	inline static int32_t get_offset_of_stringValue_7() { return static_cast<int32_t>(offsetof(Tags_t3560929397, ___stringValue_7)); }
	inline String_t* get_stringValue_7() const { return ___stringValue_7; }
	inline String_t** get_address_of_stringValue_7() { return &___stringValue_7; }
	inline void set_stringValue_7(String_t* value)
	{
		___stringValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_7), value);
	}
};

struct Tags_t3560929397_StaticFields
{
public:
	// TouchScript.Tags TouchScript.Tags::EMPTY
	Tags_t3560929397 * ___EMPTY_4;

public:
	inline static int32_t get_offset_of_EMPTY_4() { return static_cast<int32_t>(offsetof(Tags_t3560929397_StaticFields, ___EMPTY_4)); }
	inline Tags_t3560929397 * get_EMPTY_4() const { return ___EMPTY_4; }
	inline Tags_t3560929397 ** get_address_of_EMPTY_4() { return &___EMPTY_4; }
	inline void set_EMPTY_4(Tags_t3560929397 * value)
	{
		___EMPTY_4 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGS_T3560929397_H
#ifndef TUIOOBJECTMAPPING_T2434953367_H
#define TUIOOBJECTMAPPING_T2434953367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioObjectMapping
struct  TuioObjectMapping_t2434953367  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.InputSources.TuioObjectMapping::Id
	int32_t ___Id_0;
	// System.String TouchScript.InputSources.TuioObjectMapping::Tag
	String_t* ___Tag_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TuioObjectMapping_t2434953367, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Tag_1() { return static_cast<int32_t>(offsetof(TuioObjectMapping_t2434953367, ___Tag_1)); }
	inline String_t* get_Tag_1() const { return ___Tag_1; }
	inline String_t** get_address_of_Tag_1() { return &___Tag_1; }
	inline void set_Tag_1(String_t* value)
	{
		___Tag_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tag_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTMAPPING_T2434953367_H
#ifndef U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T2290274025_H
#define U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T2290274025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_EndTouch>c__AnonStorey2
struct  U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t2290274025  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_EndTouch>c__AnonStorey2::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t2290274025, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T2290274025_H
#ifndef CLUSTERS_T2177422968_H
#define CLUSTERS_T2177422968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Clusters.Clusters
struct  Clusters_t2177422968  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::points
	List_1_t2345966477 * ___points_2;
	// System.Boolean TouchScript.Clusters.Clusters::dirty
	bool ___dirty_3;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::cluster1
	List_1_t2345966477 * ___cluster1_4;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::cluster2
	List_1_t2345966477 * ___cluster2_5;
	// System.Single TouchScript.Clusters.Clusters::minPointDistance
	float ___minPointDistance_6;
	// System.Single TouchScript.Clusters.Clusters::minPointDistanceSqr
	float ___minPointDistanceSqr_7;
	// System.Boolean TouchScript.Clusters.Clusters::hasClusters
	bool ___hasClusters_8;

public:
	inline static int32_t get_offset_of_points_2() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___points_2)); }
	inline List_1_t2345966477 * get_points_2() const { return ___points_2; }
	inline List_1_t2345966477 ** get_address_of_points_2() { return &___points_2; }
	inline void set_points_2(List_1_t2345966477 * value)
	{
		___points_2 = value;
		Il2CppCodeGenWriteBarrier((&___points_2), value);
	}

	inline static int32_t get_offset_of_dirty_3() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___dirty_3)); }
	inline bool get_dirty_3() const { return ___dirty_3; }
	inline bool* get_address_of_dirty_3() { return &___dirty_3; }
	inline void set_dirty_3(bool value)
	{
		___dirty_3 = value;
	}

	inline static int32_t get_offset_of_cluster1_4() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___cluster1_4)); }
	inline List_1_t2345966477 * get_cluster1_4() const { return ___cluster1_4; }
	inline List_1_t2345966477 ** get_address_of_cluster1_4() { return &___cluster1_4; }
	inline void set_cluster1_4(List_1_t2345966477 * value)
	{
		___cluster1_4 = value;
		Il2CppCodeGenWriteBarrier((&___cluster1_4), value);
	}

	inline static int32_t get_offset_of_cluster2_5() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___cluster2_5)); }
	inline List_1_t2345966477 * get_cluster2_5() const { return ___cluster2_5; }
	inline List_1_t2345966477 ** get_address_of_cluster2_5() { return &___cluster2_5; }
	inline void set_cluster2_5(List_1_t2345966477 * value)
	{
		___cluster2_5 = value;
		Il2CppCodeGenWriteBarrier((&___cluster2_5), value);
	}

	inline static int32_t get_offset_of_minPointDistance_6() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___minPointDistance_6)); }
	inline float get_minPointDistance_6() const { return ___minPointDistance_6; }
	inline float* get_address_of_minPointDistance_6() { return &___minPointDistance_6; }
	inline void set_minPointDistance_6(float value)
	{
		___minPointDistance_6 = value;
	}

	inline static int32_t get_offset_of_minPointDistanceSqr_7() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___minPointDistanceSqr_7)); }
	inline float get_minPointDistanceSqr_7() const { return ___minPointDistanceSqr_7; }
	inline float* get_address_of_minPointDistanceSqr_7() { return &___minPointDistanceSqr_7; }
	inline void set_minPointDistanceSqr_7(float value)
	{
		___minPointDistanceSqr_7 = value;
	}

	inline static int32_t get_offset_of_hasClusters_8() { return static_cast<int32_t>(offsetof(Clusters_t2177422968, ___hasClusters_8)); }
	inline bool get_hasClusters_8() const { return ___hasClusters_8; }
	inline bool* get_address_of_hasClusters_8() { return &___hasClusters_8; }
	inline void set_hasClusters_8(bool value)
	{
		___hasClusters_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERS_T2177422968_H
#ifndef U3CWAITU3EC__ITERATOR0_T3912298409_H
#define U3CWAITU3EC__ITERATOR0_T3912298409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0
struct  U3CwaitU3Ec__Iterator0_t3912298409  : public RuntimeObject
{
public:
	// System.Single TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_0;
	// TouchScript.Gestures.LongPressGesture TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$this
	LongPressGesture_t3651199768 * ___U24this_1;
	// System.Object TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3912298409, ___U3CtargetTimeU3E__0_0)); }
	inline float get_U3CtargetTimeU3E__0_0() const { return ___U3CtargetTimeU3E__0_0; }
	inline float* get_address_of_U3CtargetTimeU3E__0_0() { return &___U3CtargetTimeU3E__0_0; }
	inline void set_U3CtargetTimeU3E__0_0(float value)
	{
		___U3CtargetTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3912298409, ___U24this_1)); }
	inline LongPressGesture_t3651199768 * get_U24this_1() const { return ___U24this_1; }
	inline LongPressGesture_t3651199768 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LongPressGesture_t3651199768 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3912298409, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3912298409, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3912298409, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3EC__ITERATOR0_T3912298409_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef U3CWAITU3EC__ITERATOR0_T2840560335_H
#define U3CWAITU3EC__ITERATOR0_T2840560335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TapGesture/<wait>c__Iterator0
struct  U3CwaitU3Ec__Iterator0_t2840560335  : public RuntimeObject
{
public:
	// System.Single TouchScript.Gestures.TapGesture/<wait>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_0;
	// TouchScript.Gestures.TapGesture TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$this
	TapGesture_t1114317921 * ___U24this_1;
	// System.Object TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t2840560335, ___U3CtargetTimeU3E__0_0)); }
	inline float get_U3CtargetTimeU3E__0_0() const { return ___U3CtargetTimeU3E__0_0; }
	inline float* get_address_of_U3CtargetTimeU3E__0_0() { return &___U3CtargetTimeU3E__0_0; }
	inline void set_U3CtargetTimeU3E__0_0(float value)
	{
		___U3CtargetTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t2840560335, ___U24this_1)); }
	inline TapGesture_t1114317921 * get_U24this_1() const { return ___U24this_1; }
	inline TapGesture_t1114317921 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TapGesture_t1114317921 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t2840560335, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t2840560335, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t2840560335, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3EC__ITERATOR0_T2840560335_H
#ifndef TOUCHHANDLER_T684722057_H
#define TOUCHHANDLER_T684722057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.TouchHandler
struct  TouchHandler_t684722057  : public RuntimeObject
{
public:
	// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint> TouchScript.InputSources.InputHandlers.TouchHandler::beginTouch
	Func_4_t3126574096 * ___beginTouch_0;
	// System.Action`2<System.Int32,UnityEngine.Vector2> TouchScript.InputSources.InputHandlers.TouchHandler::moveTouch
	Action_2_t3382406540 * ___moveTouch_1;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.TouchHandler::endTouch
	Action_1_t3123413348 * ___endTouch_2;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.TouchHandler::cancelTouch
	Action_1_t3123413348 * ___cancelTouch_3;
	// TouchScript.Tags TouchScript.InputSources.InputHandlers.TouchHandler::tags
	Tags_t3560929397 * ___tags_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState> TouchScript.InputSources.InputHandlers.TouchHandler::systemToInternalId
	Dictionary_2_t1074268497 * ___systemToInternalId_5;
	// System.Int32 TouchScript.InputSources.InputHandlers.TouchHandler::touchesNum
	int32_t ___touchesNum_6;

public:
	inline static int32_t get_offset_of_beginTouch_0() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___beginTouch_0)); }
	inline Func_4_t3126574096 * get_beginTouch_0() const { return ___beginTouch_0; }
	inline Func_4_t3126574096 ** get_address_of_beginTouch_0() { return &___beginTouch_0; }
	inline void set_beginTouch_0(Func_4_t3126574096 * value)
	{
		___beginTouch_0 = value;
		Il2CppCodeGenWriteBarrier((&___beginTouch_0), value);
	}

	inline static int32_t get_offset_of_moveTouch_1() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___moveTouch_1)); }
	inline Action_2_t3382406540 * get_moveTouch_1() const { return ___moveTouch_1; }
	inline Action_2_t3382406540 ** get_address_of_moveTouch_1() { return &___moveTouch_1; }
	inline void set_moveTouch_1(Action_2_t3382406540 * value)
	{
		___moveTouch_1 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouch_1), value);
	}

	inline static int32_t get_offset_of_endTouch_2() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___endTouch_2)); }
	inline Action_1_t3123413348 * get_endTouch_2() const { return ___endTouch_2; }
	inline Action_1_t3123413348 ** get_address_of_endTouch_2() { return &___endTouch_2; }
	inline void set_endTouch_2(Action_1_t3123413348 * value)
	{
		___endTouch_2 = value;
		Il2CppCodeGenWriteBarrier((&___endTouch_2), value);
	}

	inline static int32_t get_offset_of_cancelTouch_3() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___cancelTouch_3)); }
	inline Action_1_t3123413348 * get_cancelTouch_3() const { return ___cancelTouch_3; }
	inline Action_1_t3123413348 ** get_address_of_cancelTouch_3() { return &___cancelTouch_3; }
	inline void set_cancelTouch_3(Action_1_t3123413348 * value)
	{
		___cancelTouch_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelTouch_3), value);
	}

	inline static int32_t get_offset_of_tags_4() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___tags_4)); }
	inline Tags_t3560929397 * get_tags_4() const { return ___tags_4; }
	inline Tags_t3560929397 ** get_address_of_tags_4() { return &___tags_4; }
	inline void set_tags_4(Tags_t3560929397 * value)
	{
		___tags_4 = value;
		Il2CppCodeGenWriteBarrier((&___tags_4), value);
	}

	inline static int32_t get_offset_of_systemToInternalId_5() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___systemToInternalId_5)); }
	inline Dictionary_2_t1074268497 * get_systemToInternalId_5() const { return ___systemToInternalId_5; }
	inline Dictionary_2_t1074268497 ** get_address_of_systemToInternalId_5() { return &___systemToInternalId_5; }
	inline void set_systemToInternalId_5(Dictionary_2_t1074268497 * value)
	{
		___systemToInternalId_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemToInternalId_5), value);
	}

	inline static int32_t get_offset_of_touchesNum_6() { return static_cast<int32_t>(offsetof(TouchHandler_t684722057, ___touchesNum_6)); }
	inline int32_t get_touchesNum_6() const { return ___touchesNum_6; }
	inline int32_t* get_address_of_touchesNum_6() { return &___touchesNum_6; }
	inline void set_touchesNum_6(int32_t value)
	{
		___touchesNum_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHANDLER_T684722057_H
#ifndef U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T214618119_H
#define U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T214618119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_CancelTouch>c__AnonStorey3
struct  U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t214618119  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_CancelTouch>c__AnonStorey3::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t214618119, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T214618119_H
#ifndef U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
#define U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChat/<BeginChat>c__Iterator0
struct  U3CBeginChatU3Ec__Iterator0_t121862130  : public RuntimeObject
{
public:
	// WebSocket WorldChat/<BeginChat>c__Iterator0::<w>__0
	WebSocket_t1645401340 * ___U3CwU3E__0_0;
	// System.String WorldChat/<BeginChat>c__Iterator0::<reply>__1
	String_t* ___U3CreplyU3E__1_1;
	// WorldChat WorldChat/<BeginChat>c__Iterator0::$this
	WorldChat_t753147188 * ___U24this_2;
	// System.Object WorldChat/<BeginChat>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WorldChat/<BeginChat>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WorldChat/<BeginChat>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U3CwU3E__0_0)); }
	inline WebSocket_t1645401340 * get_U3CwU3E__0_0() const { return ___U3CwU3E__0_0; }
	inline WebSocket_t1645401340 ** get_address_of_U3CwU3E__0_0() { return &___U3CwU3E__0_0; }
	inline void set_U3CwU3E__0_0(WebSocket_t1645401340 * value)
	{
		___U3CwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CreplyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U3CreplyU3E__1_1)); }
	inline String_t* get_U3CreplyU3E__1_1() const { return ___U3CreplyU3E__1_1; }
	inline String_t** get_address_of_U3CreplyU3E__1_1() { return &___U3CreplyU3E__1_1; }
	inline void set_U3CreplyU3E__1_1(String_t* value)
	{
		___U3CreplyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreplyU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24this_2)); }
	inline WorldChat_t753147188 * get_U24this_2() const { return ___U24this_2; }
	inline WorldChat_t753147188 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WorldChat_t753147188 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
#ifndef U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#define U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChunkManager/<GetChunkInfo>c__Iterator0
struct  U3CGetChunkInfoU3Ec__Iterator0_t549570326  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WorldChunkManager/<GetChunkInfo>c__Iterator0::<unitForm>__0
	WWWForm_t4064702195 * ___U3CunitFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest WorldChunkManager/<GetChunkInfo>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// WorldChunkManager WorldChunkManager/<GetChunkInfo>c__Iterator0::$this
	WorldChunkManager_t1513314516 * ___U24this_2;
	// System.Object WorldChunkManager/<GetChunkInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WorldChunkManager/<GetChunkInfo>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WorldChunkManager/<GetChunkInfo>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CunitFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U3CunitFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CunitFormU3E__0_0() const { return ___U3CunitFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CunitFormU3E__0_0() { return &___U3CunitFormU3E__0_0; }
	inline void set_U3CunitFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CunitFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunitFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24this_2)); }
	inline WorldChunkManager_t1513314516 * get_U24this_2() const { return ___U24this_2; }
	inline WorldChunkManager_t1513314516 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WorldChunkManager_t1513314516 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T4200040672_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T4200040672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t4200040672  : public RuntimeObject
{
public:
	// TouchScript.Layers.TouchLayer TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$this
	TouchLayer_t1334725428 * ___U24this_0;
	// System.Object TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t4200040672, ___U24this_0)); }
	inline TouchLayer_t1334725428 * get_U24this_0() const { return ___U24this_0; }
	inline TouchLayer_t1334725428 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TouchLayer_t1334725428 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t4200040672, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t4200040672, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t4200040672, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T4200040672_H
#ifndef PROJECTIONPARAMS_T2315592447_H
#define PROJECTIONPARAMS_T2315592447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.ProjectionParams
struct  ProjectionParams_t2315592447  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONPARAMS_T2315592447_H
#ifndef TOUCHEVENTARGS_T157817308_H
#define TOUCHEVENTARGS_T157817308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchEventArgs
struct  TouchEventArgs_t157817308  : public EventArgs_t3591816995
{
public:
	// System.Collections.Generic.IList`1<TouchScript.TouchPoint> TouchScript.TouchEventArgs::<Touches>k__BackingField
	RuntimeObject* ___U3CTouchesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchEventArgs_t157817308, ___U3CTouchesU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CTouchesU3Ek__BackingField_1() const { return ___U3CTouchesU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CTouchesU3Ek__BackingField_1() { return &___U3CTouchesU3Ek__BackingField_1; }
	inline void set_U3CTouchesU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CTouchesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchesU3Ek__BackingField_1), value);
	}
};

struct TouchEventArgs_t157817308_StaticFields
{
public:
	// TouchScript.TouchEventArgs TouchScript.TouchEventArgs::instance
	TouchEventArgs_t157817308 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(TouchEventArgs_t157817308_StaticFields, ___instance_2)); }
	inline TouchEventArgs_t157817308 * get_instance_2() const { return ___instance_2; }
	inline TouchEventArgs_t157817308 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TouchEventArgs_t157817308 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTARGS_T157817308_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUMERATOR_T2017297076_H
#define ENUMERATOR_T2017297076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2017297076 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t128053199 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___l_0)); }
	inline List_1_t128053199 * get_l_0() const { return ___l_0; }
	inline List_1_t128053199 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t128053199 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2017297076_H
#ifndef METAGESTUREEVENTARGS_T2314752266_H
#define METAGESTUREEVENTARGS_T2314752266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.MetaGestureEventArgs
struct  MetaGestureEventArgs_t2314752266  : public EventArgs_t3591816995
{
public:
	// TouchScript.TouchPoint TouchScript.Gestures.MetaGestureEventArgs::<Touch>k__BackingField
	TouchPoint_t873891735 * ___U3CTouchU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaGestureEventArgs_t2314752266, ___U3CTouchU3Ek__BackingField_1)); }
	inline TouchPoint_t873891735 * get_U3CTouchU3Ek__BackingField_1() const { return ___U3CTouchU3Ek__BackingField_1; }
	inline TouchPoint_t873891735 ** get_address_of_U3CTouchU3Ek__BackingField_1() { return &___U3CTouchU3Ek__BackingField_1; }
	inline void set_U3CTouchU3Ek__BackingField_1(TouchPoint_t873891735 * value)
	{
		___U3CTouchU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METAGESTUREEVENTARGS_T2314752266_H
#ifndef TOUCHLAYEREVENTARGS_T720441680_H
#define TOUCHLAYEREVENTARGS_T720441680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayerEventArgs
struct  TouchLayerEventArgs_t720441680  : public EventArgs_t3591816995
{
public:
	// TouchScript.TouchPoint TouchScript.Layers.TouchLayerEventArgs::<Touch>k__BackingField
	TouchPoint_t873891735 * ___U3CTouchU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchLayerEventArgs_t720441680, ___U3CTouchU3Ek__BackingField_1)); }
	inline TouchPoint_t873891735 * get_U3CTouchU3Ek__BackingField_1() const { return ___U3CTouchU3Ek__BackingField_1; }
	inline TouchPoint_t873891735 ** get_address_of_U3CTouchU3Ek__BackingField_1() { return &___U3CTouchU3Ek__BackingField_1; }
	inline void set_U3CTouchU3Ek__BackingField_1(TouchPoint_t873891735 * value)
	{
		___U3CTouchU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHLAYEREVENTARGS_T720441680_H
#ifndef TOUCHDATA_T2804394961_H
#define TOUCHDATA_T2804394961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.UI.UIGesture/TouchData
struct  TouchData_t2804394961 
{
public:
	// System.Boolean TouchScript.Gestures.UI.UIGesture/TouchData::OnTarget
	bool ___OnTarget_0;
	// UnityEngine.EventSystems.PointerEventData TouchScript.Gestures.UI.UIGesture/TouchData::Data
	PointerEventData_t3807901092 * ___Data_1;

public:
	inline static int32_t get_offset_of_OnTarget_0() { return static_cast<int32_t>(offsetof(TouchData_t2804394961, ___OnTarget_0)); }
	inline bool get_OnTarget_0() const { return ___OnTarget_0; }
	inline bool* get_address_of_OnTarget_0() { return &___OnTarget_0; }
	inline void set_OnTarget_0(bool value)
	{
		___OnTarget_0 = value;
	}

	inline static int32_t get_offset_of_Data_1() { return static_cast<int32_t>(offsetof(TouchData_t2804394961, ___Data_1)); }
	inline PointerEventData_t3807901092 * get_Data_1() const { return ___Data_1; }
	inline PointerEventData_t3807901092 ** get_address_of_Data_1() { return &___Data_1; }
	inline void set_Data_1(PointerEventData_t3807901092 * value)
	{
		___Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Gestures.UI.UIGesture/TouchData
struct TouchData_t2804394961_marshaled_pinvoke
{
	int32_t ___OnTarget_0;
	PointerEventData_t3807901092 * ___Data_1;
};
// Native definition for COM marshalling of TouchScript.Gestures.UI.UIGesture/TouchData
struct TouchData_t2804394961_marshaled_com
{
	int32_t ___OnTarget_0;
	PointerEventData_t3807901092 * ___Data_1;
};
#endif // TOUCHDATA_T2804394961_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CAMERAPROJECTIONPARAMS_T1295449896_H
#define CAMERAPROJECTIONPARAMS_T1295449896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraProjectionParams
struct  CameraProjectionParams_t1295449896  : public ProjectionParams_t2315592447
{
public:
	// UnityEngine.Camera TouchScript.Layers.CameraProjectionParams::camera
	Camera_t4157153871 * ___camera_0;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(CameraProjectionParams_t1295449896, ___camera_0)); }
	inline Camera_t4157153871 * get_camera_0() const { return ___camera_0; }
	inline Camera_t4157153871 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t4157153871 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPROJECTIONPARAMS_T1295449896_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef PROJECTIONTYPE_T3454177328_H
#define PROJECTIONTYPE_T3454177328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TransformGesture/ProjectionType
struct  ProjectionType_t3454177328 
{
public:
	// System.Int32 TouchScript.Gestures.TransformGesture/ProjectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProjectionType_t3454177328, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONTYPE_T3454177328_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef MESSAGETYPE_T3141418335_H
#define MESSAGETYPE_T3141418335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager/MessageType
struct  MessageType_t3141418335 
{
public:
	// System.Int32 TouchScript.TouchManager/MessageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MessageType_t3141418335, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPE_T3141418335_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef U3CSORTHITSU3EC__ANONSTOREY0_T1648932270_H
#define U3CSORTHITSU3EC__ANONSTOREY0_T1648932270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer/<sortHits>c__AnonStorey0
struct  U3CsortHitsU3Ec__AnonStorey0_t1648932270  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 TouchScript.Layers.CameraLayer/<sortHits>c__AnonStorey0::cameraPos
	Vector3_t3722313464  ___cameraPos_0;

public:
	inline static int32_t get_offset_of_cameraPos_0() { return static_cast<int32_t>(offsetof(U3CsortHitsU3Ec__AnonStorey0_t1648932270, ___cameraPos_0)); }
	inline Vector3_t3722313464  get_cameraPos_0() const { return ___cameraPos_0; }
	inline Vector3_t3722313464 * get_address_of_cameraPos_0() { return &___cameraPos_0; }
	inline void set_cameraPos_0(Vector3_t3722313464  value)
	{
		___cameraPos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSORTHITSU3EC__ANONSTOREY0_T1648932270_H
#ifndef LAYERHITRESULT_T807160396_H
#define LAYERHITRESULT_T807160396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer/LayerHitResult
struct  LayerHitResult_t807160396 
{
public:
	// System.Int32 TouchScript.Layers.TouchLayer/LayerHitResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerHitResult_t807160396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERHITRESULT_T807160396_H
#ifndef LAYERTYPE_T3061072963_H
#define LAYERTYPE_T3061072963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.FullscreenLayer/LayerType
struct  LayerType_t3061072963 
{
public:
	// System.Int32 TouchScript.Layers.FullscreenLayer/LayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerType_t3061072963, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERTYPE_T3061072963_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef WINDOWS7TOUCHAPITYPE_T460929017_H
#define WINDOWS7TOUCHAPITYPE_T460929017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput/Windows7TouchAPIType
struct  Windows7TouchAPIType_t460929017 
{
public:
	// System.Int32 TouchScript.InputSources.StandardInput/Windows7TouchAPIType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Windows7TouchAPIType_t460929017, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWS7TOUCHAPITYPE_T460929017_H
#ifndef WINDOWS8TOUCHAPITYPE_T4063000358_H
#define WINDOWS8TOUCHAPITYPE_T4063000358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput/Windows8TouchAPIType
struct  Windows8TouchAPIType_t4063000358 
{
public:
	// System.Int32 TouchScript.InputSources.StandardInput/Windows8TouchAPIType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Windows8TouchAPIType_t4063000358, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWS8TOUCHAPITYPE_T4063000358_H
#ifndef OBJECTHITRESULT_T1915067883_H
#define OBJECTHITRESULT_T1915067883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.HitTest/ObjectHitResult
struct  ObjectHitResult_t1915067883 
{
public:
	// System.Int32 TouchScript.Hit.HitTest/ObjectHitResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectHitResult_t1915067883, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTHITRESULT_T1915067883_H
#ifndef U3CWRAPMULTILINEU3EC__ITERATOR0_T191716052_H
#define U3CWRAPMULTILINEU3EC__ITERATOR0_T191716052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextLocalizer/<WrapMultiline>c__Iterator0
struct  U3CWrapMultilineU3Ec__Iterator0_t191716052  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> TextLocalizer/<WrapMultiline>c__Iterator0::<lineBreakIndices>__0
	List_1_t128053199 * ___U3ClineBreakIndicesU3E__0_0;
	// System.String TextLocalizer/<WrapMultiline>c__Iterator0::textToWrap
	String_t* ___textToWrap_1;
	// UnityEngine.UI.Text TextLocalizer/<WrapMultiline>c__Iterator0::reciever
	Text_t1901882714 * ___reciever_2;
	// System.Char[] TextLocalizer/<WrapMultiline>c__Iterator0::<chars>__0
	CharU5BU5D_t3528271667* ___U3CcharsU3E__0_3;
	// System.Collections.Generic.List`1<System.Char> TextLocalizer/<WrapMultiline>c__Iterator0::<charsList>__0
	List_1_t811567916 * ___U3CcharsListU3E__0_4;
	// System.Collections.Generic.List`1/Enumerator<System.Int32> TextLocalizer/<WrapMultiline>c__Iterator0::$locvar0
	Enumerator_t2017297076  ___U24locvar0_5;
	// System.String TextLocalizer/<WrapMultiline>c__Iterator0::<rebuilded>__0
	String_t* ___U3CrebuildedU3E__0_6;
	// System.Object TextLocalizer/<WrapMultiline>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TextLocalizer/<WrapMultiline>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TextLocalizer/<WrapMultiline>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3ClineBreakIndicesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U3ClineBreakIndicesU3E__0_0)); }
	inline List_1_t128053199 * get_U3ClineBreakIndicesU3E__0_0() const { return ___U3ClineBreakIndicesU3E__0_0; }
	inline List_1_t128053199 ** get_address_of_U3ClineBreakIndicesU3E__0_0() { return &___U3ClineBreakIndicesU3E__0_0; }
	inline void set_U3ClineBreakIndicesU3E__0_0(List_1_t128053199 * value)
	{
		___U3ClineBreakIndicesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineBreakIndicesU3E__0_0), value);
	}

	inline static int32_t get_offset_of_textToWrap_1() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___textToWrap_1)); }
	inline String_t* get_textToWrap_1() const { return ___textToWrap_1; }
	inline String_t** get_address_of_textToWrap_1() { return &___textToWrap_1; }
	inline void set_textToWrap_1(String_t* value)
	{
		___textToWrap_1 = value;
		Il2CppCodeGenWriteBarrier((&___textToWrap_1), value);
	}

	inline static int32_t get_offset_of_reciever_2() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___reciever_2)); }
	inline Text_t1901882714 * get_reciever_2() const { return ___reciever_2; }
	inline Text_t1901882714 ** get_address_of_reciever_2() { return &___reciever_2; }
	inline void set_reciever_2(Text_t1901882714 * value)
	{
		___reciever_2 = value;
		Il2CppCodeGenWriteBarrier((&___reciever_2), value);
	}

	inline static int32_t get_offset_of_U3CcharsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U3CcharsU3E__0_3)); }
	inline CharU5BU5D_t3528271667* get_U3CcharsU3E__0_3() const { return ___U3CcharsU3E__0_3; }
	inline CharU5BU5D_t3528271667** get_address_of_U3CcharsU3E__0_3() { return &___U3CcharsU3E__0_3; }
	inline void set_U3CcharsU3E__0_3(CharU5BU5D_t3528271667* value)
	{
		___U3CcharsU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharsU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcharsListU3E__0_4() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U3CcharsListU3E__0_4)); }
	inline List_1_t811567916 * get_U3CcharsListU3E__0_4() const { return ___U3CcharsListU3E__0_4; }
	inline List_1_t811567916 ** get_address_of_U3CcharsListU3E__0_4() { return &___U3CcharsListU3E__0_4; }
	inline void set_U3CcharsListU3E__0_4(List_1_t811567916 * value)
	{
		___U3CcharsListU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharsListU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24locvar0_5() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U24locvar0_5)); }
	inline Enumerator_t2017297076  get_U24locvar0_5() const { return ___U24locvar0_5; }
	inline Enumerator_t2017297076 * get_address_of_U24locvar0_5() { return &___U24locvar0_5; }
	inline void set_U24locvar0_5(Enumerator_t2017297076  value)
	{
		___U24locvar0_5 = value;
	}

	inline static int32_t get_offset_of_U3CrebuildedU3E__0_6() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U3CrebuildedU3E__0_6)); }
	inline String_t* get_U3CrebuildedU3E__0_6() const { return ___U3CrebuildedU3E__0_6; }
	inline String_t** get_address_of_U3CrebuildedU3E__0_6() { return &___U3CrebuildedU3E__0_6; }
	inline void set_U3CrebuildedU3E__0_6(String_t* value)
	{
		___U3CrebuildedU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrebuildedU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CWrapMultilineU3Ec__Iterator0_t191716052, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRAPMULTILINEU3EC__ITERATOR0_T191716052_H
#ifndef MOUSEHANDLER_T2265731789_H
#define MOUSEHANDLER_T2265731789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.MouseHandler
struct  MouseHandler_t2265731789  : public RuntimeObject
{
public:
	// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint> TouchScript.InputSources.InputHandlers.MouseHandler::beginTouch
	Func_4_t3126574096 * ___beginTouch_0;
	// System.Action`2<System.Int32,UnityEngine.Vector2> TouchScript.InputSources.InputHandlers.MouseHandler::moveTouch
	Action_2_t3382406540 * ___moveTouch_1;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.MouseHandler::endTouch
	Action_1_t3123413348 * ___endTouch_2;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.MouseHandler::cancelTouch
	Action_1_t3123413348 * ___cancelTouch_3;
	// TouchScript.Tags TouchScript.InputSources.InputHandlers.MouseHandler::tags
	Tags_t3560929397 * ___tags_4;
	// System.Int32 TouchScript.InputSources.InputHandlers.MouseHandler::mousePointId
	int32_t ___mousePointId_5;
	// System.Int32 TouchScript.InputSources.InputHandlers.MouseHandler::fakeMousePointId
	int32_t ___fakeMousePointId_6;
	// UnityEngine.Vector3 TouchScript.InputSources.InputHandlers.MouseHandler::mousePointPos
	Vector3_t3722313464  ___mousePointPos_7;

public:
	inline static int32_t get_offset_of_beginTouch_0() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___beginTouch_0)); }
	inline Func_4_t3126574096 * get_beginTouch_0() const { return ___beginTouch_0; }
	inline Func_4_t3126574096 ** get_address_of_beginTouch_0() { return &___beginTouch_0; }
	inline void set_beginTouch_0(Func_4_t3126574096 * value)
	{
		___beginTouch_0 = value;
		Il2CppCodeGenWriteBarrier((&___beginTouch_0), value);
	}

	inline static int32_t get_offset_of_moveTouch_1() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___moveTouch_1)); }
	inline Action_2_t3382406540 * get_moveTouch_1() const { return ___moveTouch_1; }
	inline Action_2_t3382406540 ** get_address_of_moveTouch_1() { return &___moveTouch_1; }
	inline void set_moveTouch_1(Action_2_t3382406540 * value)
	{
		___moveTouch_1 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouch_1), value);
	}

	inline static int32_t get_offset_of_endTouch_2() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___endTouch_2)); }
	inline Action_1_t3123413348 * get_endTouch_2() const { return ___endTouch_2; }
	inline Action_1_t3123413348 ** get_address_of_endTouch_2() { return &___endTouch_2; }
	inline void set_endTouch_2(Action_1_t3123413348 * value)
	{
		___endTouch_2 = value;
		Il2CppCodeGenWriteBarrier((&___endTouch_2), value);
	}

	inline static int32_t get_offset_of_cancelTouch_3() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___cancelTouch_3)); }
	inline Action_1_t3123413348 * get_cancelTouch_3() const { return ___cancelTouch_3; }
	inline Action_1_t3123413348 ** get_address_of_cancelTouch_3() { return &___cancelTouch_3; }
	inline void set_cancelTouch_3(Action_1_t3123413348 * value)
	{
		___cancelTouch_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelTouch_3), value);
	}

	inline static int32_t get_offset_of_tags_4() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___tags_4)); }
	inline Tags_t3560929397 * get_tags_4() const { return ___tags_4; }
	inline Tags_t3560929397 ** get_address_of_tags_4() { return &___tags_4; }
	inline void set_tags_4(Tags_t3560929397 * value)
	{
		___tags_4 = value;
		Il2CppCodeGenWriteBarrier((&___tags_4), value);
	}

	inline static int32_t get_offset_of_mousePointId_5() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___mousePointId_5)); }
	inline int32_t get_mousePointId_5() const { return ___mousePointId_5; }
	inline int32_t* get_address_of_mousePointId_5() { return &___mousePointId_5; }
	inline void set_mousePointId_5(int32_t value)
	{
		___mousePointId_5 = value;
	}

	inline static int32_t get_offset_of_fakeMousePointId_6() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___fakeMousePointId_6)); }
	inline int32_t get_fakeMousePointId_6() const { return ___fakeMousePointId_6; }
	inline int32_t* get_address_of_fakeMousePointId_6() { return &___fakeMousePointId_6; }
	inline void set_fakeMousePointId_6(int32_t value)
	{
		___fakeMousePointId_6 = value;
	}

	inline static int32_t get_offset_of_mousePointPos_7() { return static_cast<int32_t>(offsetof(MouseHandler_t2265731789, ___mousePointPos_7)); }
	inline Vector3_t3722313464  get_mousePointPos_7() const { return ___mousePointPos_7; }
	inline Vector3_t3722313464 * get_address_of_mousePointPos_7() { return &___mousePointPos_7; }
	inline void set_mousePointPos_7(Vector3_t3722313464  value)
	{
		___mousePointPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEHANDLER_T2265731789_H
#ifndef TOUCHHITTYPE_T1165203098_H
#define TOUCHHITTYPE_T1165203098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit/TouchHitType
struct  TouchHitType_t1165203098 
{
public:
	// System.Int32 TouchScript.Hit.TouchHit/TouchHitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchHitType_t1165203098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHITTYPE_T1165203098_H
#ifndef GESTUREDIRECTION_T4143464802_H
#define GESTUREDIRECTION_T4143464802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.FlickGesture/GestureDirection
struct  GestureDirection_t4143464802 
{
public:
	// System.Int32 TouchScript.Gestures.FlickGesture/GestureDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureDirection_t4143464802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREDIRECTION_T4143464802_H
#ifndef GESTURESTATE_T1922660398_H
#define GESTURESTATE_T1922660398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/GestureState
struct  GestureState_t1922660398 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/GestureState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureState_t1922660398, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURESTATE_T1922660398_H
#ifndef TOUCHESNUMSTATE_T401176838_H
#define TOUCHESNUMSTATE_T401176838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/TouchesNumState
struct  TouchesNumState_t401176838 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/TouchesNumState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchesNumState_t401176838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHESNUMSTATE_T401176838_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef TRANSFORMTYPE_T264424356_H
#define TRANSFORMTYPE_T264424356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType
struct  TransformType_t264424356 
{
public:
	// System.Int32 TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformType_t264424356, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMTYPE_T264424356_H
#ifndef MOVEDIRECTION_T1216237838_H
#define MOVEDIRECTION_T1216237838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1216237838 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1216237838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1216237838_H
#ifndef TRANSFORMTYPE_T3234482893_H
#define TRANSFORMTYPE_T3234482893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase/TransformType
struct  TransformType_t3234482893 
{
public:
	// System.Int32 TouchScript.Gestures.Base.TransformGestureBase/TransformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformType_t3234482893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMTYPE_T3234482893_H
#ifndef INPUTTYPE_T1161111304_H
#define INPUTTYPE_T1161111304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioInput/InputType
struct  InputType_t1161111304 
{
public:
	// System.Int32 TouchScript.InputSources.TuioInput/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t1161111304, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1161111304_H
#ifndef MESSAGENAME_T1313736650_H
#define MESSAGENAME_T1313736650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager/MessageName
struct  MessageName_t1313736650 
{
public:
	// System.Int32 TouchScript.TouchManager/MessageName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MessageName_t1313736650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGENAME_T1313736650_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef PROJECTIONTYPE_T4274413497_H
#define PROJECTIONTYPE_T4274413497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PinnedTransformGesture/ProjectionType
struct  ProjectionType_t4274413497 
{
public:
	// System.Int32 TouchScript.Gestures.PinnedTransformGesture/ProjectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProjectionType_t4274413497, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONTYPE_T4274413497_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef TOUCHSTATE_T2185555166_H
#define TOUCHSTATE_T2185555166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.TouchHandler/TouchState
struct  TouchState_t2185555166 
{
public:
	// System.Int32 TouchScript.InputSources.InputHandlers.TouchHandler/TouchState::Id
	int32_t ___Id_0;
	// UnityEngine.TouchPhase TouchScript.InputSources.InputHandlers.TouchHandler/TouchState::Phase
	int32_t ___Phase_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TouchState_t2185555166, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Phase_1() { return static_cast<int32_t>(offsetof(TouchState_t2185555166, ___Phase_1)); }
	inline int32_t get_Phase_1() const { return ___Phase_1; }
	inline int32_t* get_address_of_Phase_1() { return &___Phase_1; }
	inline void set_Phase_1(int32_t value)
	{
		___Phase_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTATE_T2185555166_H
#ifndef CANVASPROJECTIONPARAMS_T3073699597_H
#define CANVASPROJECTIONPARAMS_T3073699597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CanvasProjectionParams
struct  CanvasProjectionParams_t3073699597  : public ProjectionParams_t2315592447
{
public:
	// UnityEngine.Canvas TouchScript.Layers.CanvasProjectionParams::canvas
	Canvas_t3310196443 * ___canvas_0;
	// UnityEngine.RectTransform TouchScript.Layers.CanvasProjectionParams::rect
	RectTransform_t3704657025 * ___rect_1;
	// UnityEngine.RenderMode TouchScript.Layers.CanvasProjectionParams::mode
	int32_t ___mode_2;
	// UnityEngine.Camera TouchScript.Layers.CanvasProjectionParams::camera
	Camera_t4157153871 * ___camera_3;

public:
	inline static int32_t get_offset_of_canvas_0() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t3073699597, ___canvas_0)); }
	inline Canvas_t3310196443 * get_canvas_0() const { return ___canvas_0; }
	inline Canvas_t3310196443 ** get_address_of_canvas_0() { return &___canvas_0; }
	inline void set_canvas_0(Canvas_t3310196443 * value)
	{
		___canvas_0 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_0), value);
	}

	inline static int32_t get_offset_of_rect_1() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t3073699597, ___rect_1)); }
	inline RectTransform_t3704657025 * get_rect_1() const { return ___rect_1; }
	inline RectTransform_t3704657025 ** get_address_of_rect_1() { return &___rect_1; }
	inline void set_rect_1(RectTransform_t3704657025 * value)
	{
		___rect_1 = value;
		Il2CppCodeGenWriteBarrier((&___rect_1), value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t3073699597, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_camera_3() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t3073699597, ___camera_3)); }
	inline Camera_t4157153871 * get_camera_3() const { return ___camera_3; }
	inline Camera_t4157153871 ** get_address_of_camera_3() { return &___camera_3; }
	inline void set_camera_3(Camera_t4157153871 * value)
	{
		___camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___camera_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASPROJECTIONPARAMS_T3073699597_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef GESTURESTATECHANGEEVENTARGS_T2842339013_H
#define GESTURESTATECHANGEEVENTARGS_T2842339013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.GestureStateChangeEventArgs
struct  GestureStateChangeEventArgs_t2842339013  : public EventArgs_t3591816995
{
public:
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_1;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GestureStateChangeEventArgs_t2842339013, ___U3CPreviousStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_1() const { return ___U3CPreviousStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_1() { return &___U3CPreviousStateU3Ek__BackingField_1; }
	inline void set_U3CPreviousStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GestureStateChangeEventArgs_t2842339013, ___U3CStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURESTATECHANGEEVENTARGS_T2842339013_H
#ifndef TOUCHHIT_T1165636103_H
#define TOUCHHIT_T1165636103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit
struct  TouchHit_t1165636103 
{
public:
	// TouchScript.Hit.TouchHit/TouchHitType TouchScript.Hit.TouchHit::type
	int32_t ___type_0;
	// UnityEngine.Transform TouchScript.Hit.TouchHit::transform
	Transform_t3600365921 * ___transform_1;
	// UnityEngine.RaycastHit TouchScript.Hit.TouchHit::raycastHit
	RaycastHit_t1056001966  ___raycastHit_2;
	// UnityEngine.RaycastHit2D TouchScript.Hit.TouchHit::raycastHit2D
	RaycastHit2D_t2279581989  ___raycastHit2D_3;
	// UnityEngine.EventSystems.RaycastResult TouchScript.Hit.TouchHit::raycastResult
	RaycastResult_t3360306849  ___raycastResult_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___transform_1)); }
	inline Transform_t3600365921 * get_transform_1() const { return ___transform_1; }
	inline Transform_t3600365921 ** get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Transform_t3600365921 * value)
	{
		___transform_1 = value;
		Il2CppCodeGenWriteBarrier((&___transform_1), value);
	}

	inline static int32_t get_offset_of_raycastHit_2() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastHit_2)); }
	inline RaycastHit_t1056001966  get_raycastHit_2() const { return ___raycastHit_2; }
	inline RaycastHit_t1056001966 * get_address_of_raycastHit_2() { return &___raycastHit_2; }
	inline void set_raycastHit_2(RaycastHit_t1056001966  value)
	{
		___raycastHit_2 = value;
	}

	inline static int32_t get_offset_of_raycastHit2D_3() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastHit2D_3)); }
	inline RaycastHit2D_t2279581989  get_raycastHit2D_3() const { return ___raycastHit2D_3; }
	inline RaycastHit2D_t2279581989 * get_address_of_raycastHit2D_3() { return &___raycastHit2D_3; }
	inline void set_raycastHit2D_3(RaycastHit2D_t2279581989  value)
	{
		___raycastHit2D_3 = value;
	}

	inline static int32_t get_offset_of_raycastResult_4() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastResult_4)); }
	inline RaycastResult_t3360306849  get_raycastResult_4() const { return ___raycastResult_4; }
	inline RaycastResult_t3360306849 * get_address_of_raycastResult_4() { return &___raycastResult_4; }
	inline void set_raycastResult_4(RaycastResult_t3360306849  value)
	{
		___raycastResult_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t1165636103_marshaled_pinvoke
{
	int32_t ___type_0;
	Transform_t3600365921 * ___transform_1;
	RaycastHit_t1056001966_marshaled_pinvoke ___raycastHit_2;
	RaycastHit2D_t2279581989_marshaled_pinvoke ___raycastHit2D_3;
	RaycastResult_t3360306849_marshaled_pinvoke ___raycastResult_4;
};
// Native definition for COM marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t1165636103_marshaled_com
{
	int32_t ___type_0;
	Transform_t3600365921 * ___transform_1;
	RaycastHit_t1056001966_marshaled_com ___raycastHit_2;
	RaycastHit2D_t2279581989_marshaled_com ___raycastHit2D_3;
	RaycastResult_t3360306849_marshaled_com ___raycastResult_4;
};
#endif // TOUCHHIT_T1165636103_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef DISPLAYDEVICE_T2568977856_H
#define DISPLAYDEVICE_T2568977856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Devices.Display.DisplayDevice
struct  DisplayDevice_t2568977856  : public ScriptableObject_t2528358522
{
public:
	// System.String TouchScript.Devices.Display.DisplayDevice::name
	String_t* ___name_2;
	// System.Single TouchScript.Devices.Display.DisplayDevice::dpi
	float ___dpi_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DisplayDevice_t2568977856, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_dpi_3() { return static_cast<int32_t>(offsetof(DisplayDevice_t2568977856, ___dpi_3)); }
	inline float get_dpi_3() const { return ___dpi_3; }
	inline float* get_address_of_dpi_3() { return &___dpi_3; }
	inline void set_dpi_3(float value)
	{
		___dpi_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYDEVICE_T2568977856_H
#ifndef GENERICDISPLAYDEVICE_T1376900854_H
#define GENERICDISPLAYDEVICE_T1376900854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Devices.Display.GenericDisplayDevice
struct  GenericDisplayDevice_t1376900854  : public DisplayDevice_t2568977856
{
public:

public:
};

struct GenericDisplayDevice_t1376900854_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> TouchScript.Devices.Display.GenericDisplayDevice::isLaptop
	Nullable_1_t1819850047  ___isLaptop_4;

public:
	inline static int32_t get_offset_of_isLaptop_4() { return static_cast<int32_t>(offsetof(GenericDisplayDevice_t1376900854_StaticFields, ___isLaptop_4)); }
	inline Nullable_1_t1819850047  get_isLaptop_4() const { return ___isLaptop_4; }
	inline Nullable_1_t1819850047 * get_address_of_isLaptop_4() { return &___isLaptop_4; }
	inline void set_isLaptop_4(Nullable_1_t1819850047  value)
	{
		___isLaptop_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICDISPLAYDEVICE_T1376900854_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TOUCHMANAGER_T1655870378_H
#define TOUCHMANAGER_T1655870378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager
struct  TouchManager_t1655870378  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Object TouchScript.TouchManager::displayDevice
	Object_t631007953 * ___displayDevice_6;
	// System.Boolean TouchScript.TouchManager::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_7;
	// System.Boolean TouchScript.TouchManager::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_8;
	// System.Boolean TouchScript.TouchManager::useSendMessage
	bool ___useSendMessage_9;
	// TouchScript.TouchManager/MessageType TouchScript.TouchManager::sendMessageEvents
	int32_t ___sendMessageEvents_10;
	// UnityEngine.GameObject TouchScript.TouchManager::sendMessageTarget
	GameObject_t1113636619 * ___sendMessageTarget_11;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManager::layers
	List_1_t2806800170 * ___layers_12;

public:
	inline static int32_t get_offset_of_displayDevice_6() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___displayDevice_6)); }
	inline Object_t631007953 * get_displayDevice_6() const { return ___displayDevice_6; }
	inline Object_t631007953 ** get_address_of_displayDevice_6() { return &___displayDevice_6; }
	inline void set_displayDevice_6(Object_t631007953 * value)
	{
		___displayDevice_6 = value;
		Il2CppCodeGenWriteBarrier((&___displayDevice_6), value);
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_7() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___shouldCreateCameraLayer_7)); }
	inline bool get_shouldCreateCameraLayer_7() const { return ___shouldCreateCameraLayer_7; }
	inline bool* get_address_of_shouldCreateCameraLayer_7() { return &___shouldCreateCameraLayer_7; }
	inline void set_shouldCreateCameraLayer_7(bool value)
	{
		___shouldCreateCameraLayer_7 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_8() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___shouldCreateStandardInput_8)); }
	inline bool get_shouldCreateStandardInput_8() const { return ___shouldCreateStandardInput_8; }
	inline bool* get_address_of_shouldCreateStandardInput_8() { return &___shouldCreateStandardInput_8; }
	inline void set_shouldCreateStandardInput_8(bool value)
	{
		___shouldCreateStandardInput_8 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_9() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___useSendMessage_9)); }
	inline bool get_useSendMessage_9() const { return ___useSendMessage_9; }
	inline bool* get_address_of_useSendMessage_9() { return &___useSendMessage_9; }
	inline void set_useSendMessage_9(bool value)
	{
		___useSendMessage_9 = value;
	}

	inline static int32_t get_offset_of_sendMessageEvents_10() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___sendMessageEvents_10)); }
	inline int32_t get_sendMessageEvents_10() const { return ___sendMessageEvents_10; }
	inline int32_t* get_address_of_sendMessageEvents_10() { return &___sendMessageEvents_10; }
	inline void set_sendMessageEvents_10(int32_t value)
	{
		___sendMessageEvents_10 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_11() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___sendMessageTarget_11)); }
	inline GameObject_t1113636619 * get_sendMessageTarget_11() const { return ___sendMessageTarget_11; }
	inline GameObject_t1113636619 ** get_address_of_sendMessageTarget_11() { return &___sendMessageTarget_11; }
	inline void set_sendMessageTarget_11(GameObject_t1113636619 * value)
	{
		___sendMessageTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_11), value);
	}

	inline static int32_t get_offset_of_layers_12() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378, ___layers_12)); }
	inline List_1_t2806800170 * get_layers_12() const { return ___layers_12; }
	inline List_1_t2806800170 ** get_address_of_layers_12() { return &___layers_12; }
	inline void set_layers_12(List_1_t2806800170 * value)
	{
		___layers_12 = value;
		Il2CppCodeGenWriteBarrier((&___layers_12), value);
	}
};

struct TouchManager_t1655870378_StaticFields
{
public:
	// UnityEngine.Vector2 TouchScript.TouchManager::INVALID_POSITION
	Vector2_t2156229523  ___INVALID_POSITION_4;
	// System.Version TouchScript.TouchManager::VERSION
	Version_t3456873960 * ___VERSION_5;

public:
	inline static int32_t get_offset_of_INVALID_POSITION_4() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378_StaticFields, ___INVALID_POSITION_4)); }
	inline Vector2_t2156229523  get_INVALID_POSITION_4() const { return ___INVALID_POSITION_4; }
	inline Vector2_t2156229523 * get_address_of_INVALID_POSITION_4() { return &___INVALID_POSITION_4; }
	inline void set_INVALID_POSITION_4(Vector2_t2156229523  value)
	{
		___INVALID_POSITION_4 = value;
	}

	inline static int32_t get_offset_of_VERSION_5() { return static_cast<int32_t>(offsetof(TouchManager_t1655870378_StaticFields, ___VERSION_5)); }
	inline Version_t3456873960 * get_VERSION_5() const { return ___VERSION_5; }
	inline Version_t3456873960 ** get_address_of_VERSION_5() { return &___VERSION_5; }
	inline void set_VERSION_5(Version_t3456873960 * value)
	{
		___VERSION_5 = value;
		Il2CppCodeGenWriteBarrier((&___VERSION_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHMANAGER_T1655870378_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TOUCHLAYER_T1334725428_H
#define TOUCHLAYER_T1334725428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer
struct  TouchLayer_t1334725428  : public MonoBehaviour_t3962482529
{
public:
	// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs> TouchScript.Layers.TouchLayer::touchBeganInvoker
	EventHandler_1_t2939568409 * ___touchBeganInvoker_2;
	// System.String TouchScript.Layers.TouchLayer::Name
	String_t* ___Name_3;
	// TouchScript.Layers.ILayerDelegate TouchScript.Layers.TouchLayer::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_4;
	// TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::layerProjectionParams
	ProjectionParams_t2315592447 * ___layerProjectionParams_5;

public:
	inline static int32_t get_offset_of_touchBeganInvoker_2() { return static_cast<int32_t>(offsetof(TouchLayer_t1334725428, ___touchBeganInvoker_2)); }
	inline EventHandler_1_t2939568409 * get_touchBeganInvoker_2() const { return ___touchBeganInvoker_2; }
	inline EventHandler_1_t2939568409 ** get_address_of_touchBeganInvoker_2() { return &___touchBeganInvoker_2; }
	inline void set_touchBeganInvoker_2(EventHandler_1_t2939568409 * value)
	{
		___touchBeganInvoker_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchBeganInvoker_2), value);
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(TouchLayer_t1334725428, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TouchLayer_t1334725428, ___U3CDelegateU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_4() const { return ___U3CDelegateU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_4() { return &___U3CDelegateU3Ek__BackingField_4; }
	inline void set_U3CDelegateU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_layerProjectionParams_5() { return static_cast<int32_t>(offsetof(TouchLayer_t1334725428, ___layerProjectionParams_5)); }
	inline ProjectionParams_t2315592447 * get_layerProjectionParams_5() const { return ___layerProjectionParams_5; }
	inline ProjectionParams_t2315592447 ** get_address_of_layerProjectionParams_5() { return &___layerProjectionParams_5; }
	inline void set_layerProjectionParams_5(ProjectionParams_t2315592447 * value)
	{
		___layerProjectionParams_5 = value;
		Il2CppCodeGenWriteBarrier((&___layerProjectionParams_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHLAYER_T1334725428_H
#ifndef TRANSFORMER_T1886620421_H
#define TRANSFORMER_T1886620421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Transformer
struct  Transformer_t1886620421  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TouchScript.Behaviors.Transformer::cachedTransform
	Transform_t3600365921 * ___cachedTransform_2;
	// System.Collections.Generic.List`1<TouchScript.Gestures.ITransformGesture> TouchScript.Behaviors.Transformer::gestures
	List_1_t3798361940 * ___gestures_3;

public:
	inline static int32_t get_offset_of_cachedTransform_2() { return static_cast<int32_t>(offsetof(Transformer_t1886620421, ___cachedTransform_2)); }
	inline Transform_t3600365921 * get_cachedTransform_2() const { return ___cachedTransform_2; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_2() { return &___cachedTransform_2; }
	inline void set_cachedTransform_2(Transform_t3600365921 * value)
	{
		___cachedTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_2), value);
	}

	inline static int32_t get_offset_of_gestures_3() { return static_cast<int32_t>(offsetof(Transformer_t1886620421, ___gestures_3)); }
	inline List_1_t3798361940 * get_gestures_3() const { return ___gestures_3; }
	inline List_1_t3798361940 ** get_address_of_gestures_3() { return &___gestures_3; }
	inline void set_gestures_3(List_1_t3798361940 * value)
	{
		___gestures_3 = value;
		Il2CppCodeGenWriteBarrier((&___gestures_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMER_T1886620421_H
#ifndef ZOOMMANAGER_T89017121_H
#define ZOOMMANAGER_T89017121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomManager
struct  ZoomManager_t89017121  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ZoomManager::mainCamera
	Camera_t4157153871 * ___mainCamera_2;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(ZoomManager_t89017121, ___mainCamera_2)); }
	inline Camera_t4157153871 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Camera_t4157153871 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMMANAGER_T89017121_H
#ifndef LOADALLLANGUAGES_T2349347345_H
#define LOADALLLANGUAGES_T2349347345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LoadAllLanguages
struct  LoadAllLanguages_t2349347345  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> SmartLocalization.LoadAllLanguages::currentLanguageKeys
	List_1_t3319525431 * ___currentLanguageKeys_2;
	// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.LoadAllLanguages::availableLanguages
	List_1_t3532169525 * ___availableLanguages_3;
	// SmartLocalization.LanguageManager SmartLocalization.LoadAllLanguages::languageManager
	LanguageManager_t2767934455 * ___languageManager_4;
	// UnityEngine.Vector2 SmartLocalization.LoadAllLanguages::valuesScrollPosition
	Vector2_t2156229523  ___valuesScrollPosition_5;
	// UnityEngine.Vector2 SmartLocalization.LoadAllLanguages::languagesScrollPosition
	Vector2_t2156229523  ___languagesScrollPosition_6;

public:
	inline static int32_t get_offset_of_currentLanguageKeys_2() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t2349347345, ___currentLanguageKeys_2)); }
	inline List_1_t3319525431 * get_currentLanguageKeys_2() const { return ___currentLanguageKeys_2; }
	inline List_1_t3319525431 ** get_address_of_currentLanguageKeys_2() { return &___currentLanguageKeys_2; }
	inline void set_currentLanguageKeys_2(List_1_t3319525431 * value)
	{
		___currentLanguageKeys_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentLanguageKeys_2), value);
	}

	inline static int32_t get_offset_of_availableLanguages_3() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t2349347345, ___availableLanguages_3)); }
	inline List_1_t3532169525 * get_availableLanguages_3() const { return ___availableLanguages_3; }
	inline List_1_t3532169525 ** get_address_of_availableLanguages_3() { return &___availableLanguages_3; }
	inline void set_availableLanguages_3(List_1_t3532169525 * value)
	{
		___availableLanguages_3 = value;
		Il2CppCodeGenWriteBarrier((&___availableLanguages_3), value);
	}

	inline static int32_t get_offset_of_languageManager_4() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t2349347345, ___languageManager_4)); }
	inline LanguageManager_t2767934455 * get_languageManager_4() const { return ___languageManager_4; }
	inline LanguageManager_t2767934455 ** get_address_of_languageManager_4() { return &___languageManager_4; }
	inline void set_languageManager_4(LanguageManager_t2767934455 * value)
	{
		___languageManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___languageManager_4), value);
	}

	inline static int32_t get_offset_of_valuesScrollPosition_5() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t2349347345, ___valuesScrollPosition_5)); }
	inline Vector2_t2156229523  get_valuesScrollPosition_5() const { return ___valuesScrollPosition_5; }
	inline Vector2_t2156229523 * get_address_of_valuesScrollPosition_5() { return &___valuesScrollPosition_5; }
	inline void set_valuesScrollPosition_5(Vector2_t2156229523  value)
	{
		___valuesScrollPosition_5 = value;
	}

	inline static int32_t get_offset_of_languagesScrollPosition_6() { return static_cast<int32_t>(offsetof(LoadAllLanguages_t2349347345, ___languagesScrollPosition_6)); }
	inline Vector2_t2156229523  get_languagesScrollPosition_6() const { return ___languagesScrollPosition_6; }
	inline Vector2_t2156229523 * get_address_of_languagesScrollPosition_6() { return &___languagesScrollPosition_6; }
	inline void set_languagesScrollPosition_6(Vector2_t2156229523  value)
	{
		___languagesScrollPosition_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADALLLANGUAGES_T2349347345_H
#ifndef WORLDCHAT_T753147188_H
#define WORLDCHAT_T753147188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChat
struct  WorldChat_t753147188  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text WorldChat::chatWindow
	Text_t1901882714 * ___chatWindow_2;
	// UnityEngine.UI.InputField WorldChat::input
	InputField_t3762917431 * ___input_3;
	// UnityEngine.UI.Scrollbar WorldChat::Scroll
	Scrollbar_t1494447233 * ___Scroll_4;
	// System.String WorldChat::MessageBuffer
	String_t* ___MessageBuffer_5;
	// System.Boolean WorldChat::chatWorking
	bool ___chatWorking_6;
	// System.Boolean WorldChat::readyToChat
	bool ___readyToChat_7;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___chatWindow_2)); }
	inline Text_t1901882714 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t1901882714 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t1901882714 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier((&___chatWindow_2), value);
	}

	inline static int32_t get_offset_of_input_3() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___input_3)); }
	inline InputField_t3762917431 * get_input_3() const { return ___input_3; }
	inline InputField_t3762917431 ** get_address_of_input_3() { return &___input_3; }
	inline void set_input_3(InputField_t3762917431 * value)
	{
		___input_3 = value;
		Il2CppCodeGenWriteBarrier((&___input_3), value);
	}

	inline static int32_t get_offset_of_Scroll_4() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___Scroll_4)); }
	inline Scrollbar_t1494447233 * get_Scroll_4() const { return ___Scroll_4; }
	inline Scrollbar_t1494447233 ** get_address_of_Scroll_4() { return &___Scroll_4; }
	inline void set_Scroll_4(Scrollbar_t1494447233 * value)
	{
		___Scroll_4 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll_4), value);
	}

	inline static int32_t get_offset_of_MessageBuffer_5() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___MessageBuffer_5)); }
	inline String_t* get_MessageBuffer_5() const { return ___MessageBuffer_5; }
	inline String_t** get_address_of_MessageBuffer_5() { return &___MessageBuffer_5; }
	inline void set_MessageBuffer_5(String_t* value)
	{
		___MessageBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___MessageBuffer_5), value);
	}

	inline static int32_t get_offset_of_chatWorking_6() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___chatWorking_6)); }
	inline bool get_chatWorking_6() const { return ___chatWorking_6; }
	inline bool* get_address_of_chatWorking_6() { return &___chatWorking_6; }
	inline void set_chatWorking_6(bool value)
	{
		___chatWorking_6 = value;
	}

	inline static int32_t get_offset_of_readyToChat_7() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___readyToChat_7)); }
	inline bool get_readyToChat_7() const { return ___readyToChat_7; }
	inline bool* get_address_of_readyToChat_7() { return &___readyToChat_7; }
	inline void set_readyToChat_7(bool value)
	{
		___readyToChat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCHAT_T753147188_H
#ifndef WORLDCHUNKMANAGER_T1513314516_H
#define WORLDCHUNKMANAGER_T1513314516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChunkManager
struct  WorldChunkManager_t1513314516  : public MonoBehaviour_t3962482529
{
public:
	// System.Int64 WorldChunkManager::startX
	int64_t ___startX_2;
	// System.Int64 WorldChunkManager::startY
	int64_t ___startY_3;
	// WorldFieldModel[0...,0...] WorldChunkManager::chunkFields
	WorldFieldModelU5BU2CU5D_t237916709* ___chunkFields_4;
	// SimpleEvent WorldChunkManager::onGetChunkInfo
	SimpleEvent_t129249603 * ___onGetChunkInfo_5;

public:
	inline static int32_t get_offset_of_startX_2() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___startX_2)); }
	inline int64_t get_startX_2() const { return ___startX_2; }
	inline int64_t* get_address_of_startX_2() { return &___startX_2; }
	inline void set_startX_2(int64_t value)
	{
		___startX_2 = value;
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___startY_3)); }
	inline int64_t get_startY_3() const { return ___startY_3; }
	inline int64_t* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(int64_t value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_chunkFields_4() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___chunkFields_4)); }
	inline WorldFieldModelU5BU2CU5D_t237916709* get_chunkFields_4() const { return ___chunkFields_4; }
	inline WorldFieldModelU5BU2CU5D_t237916709** get_address_of_chunkFields_4() { return &___chunkFields_4; }
	inline void set_chunkFields_4(WorldFieldModelU5BU2CU5D_t237916709* value)
	{
		___chunkFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___chunkFields_4), value);
	}

	inline static int32_t get_offset_of_onGetChunkInfo_5() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___onGetChunkInfo_5)); }
	inline SimpleEvent_t129249603 * get_onGetChunkInfo_5() const { return ___onGetChunkInfo_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetChunkInfo_5() { return &___onGetChunkInfo_5; }
	inline void set_onGetChunkInfo_5(SimpleEvent_t129249603 * value)
	{
		___onGetChunkInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___onGetChunkInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCHUNKMANAGER_T1513314516_H
#ifndef WORLDFIELDMANAGER_T2312070818_H
#define WORLDFIELDMANAGER_T2312070818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldManager
struct  WorldFieldManager_t2312070818  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 WorldFieldManager::internalPitId
	int32_t ___internalPitId_2;
	// UnityEngine.GameObject WorldFieldManager::fieldObject
	GameObject_t1113636619 * ___fieldObject_3;
	// UnityEngine.GameObject WorldFieldManager::flag
	GameObject_t1113636619 * ___flag_4;
	// UnityEngine.GameObject WorldFieldManager::occupationIcon
	GameObject_t1113636619 * ___occupationIcon_5;
	// WorldFieldModel WorldFieldManager::fieldInfo
	WorldFieldModel_t2417974361 * ___fieldInfo_6;

public:
	inline static int32_t get_offset_of_internalPitId_2() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___internalPitId_2)); }
	inline int32_t get_internalPitId_2() const { return ___internalPitId_2; }
	inline int32_t* get_address_of_internalPitId_2() { return &___internalPitId_2; }
	inline void set_internalPitId_2(int32_t value)
	{
		___internalPitId_2 = value;
	}

	inline static int32_t get_offset_of_fieldObject_3() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___fieldObject_3)); }
	inline GameObject_t1113636619 * get_fieldObject_3() const { return ___fieldObject_3; }
	inline GameObject_t1113636619 ** get_address_of_fieldObject_3() { return &___fieldObject_3; }
	inline void set_fieldObject_3(GameObject_t1113636619 * value)
	{
		___fieldObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___fieldObject_3), value);
	}

	inline static int32_t get_offset_of_flag_4() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___flag_4)); }
	inline GameObject_t1113636619 * get_flag_4() const { return ___flag_4; }
	inline GameObject_t1113636619 ** get_address_of_flag_4() { return &___flag_4; }
	inline void set_flag_4(GameObject_t1113636619 * value)
	{
		___flag_4 = value;
		Il2CppCodeGenWriteBarrier((&___flag_4), value);
	}

	inline static int32_t get_offset_of_occupationIcon_5() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___occupationIcon_5)); }
	inline GameObject_t1113636619 * get_occupationIcon_5() const { return ___occupationIcon_5; }
	inline GameObject_t1113636619 ** get_address_of_occupationIcon_5() { return &___occupationIcon_5; }
	inline void set_occupationIcon_5(GameObject_t1113636619 * value)
	{
		___occupationIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___occupationIcon_5), value);
	}

	inline static int32_t get_offset_of_fieldInfo_6() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___fieldInfo_6)); }
	inline WorldFieldModel_t2417974361 * get_fieldInfo_6() const { return ___fieldInfo_6; }
	inline WorldFieldModel_t2417974361 ** get_address_of_fieldInfo_6() { return &___fieldInfo_6; }
	inline void set_fieldInfo_6(WorldFieldModel_t2417974361 * value)
	{
		___fieldInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDFIELDMANAGER_T2312070818_H
#ifndef LOCALIZEDTEXT_T875466957_H
#define LOCALIZEDTEXT_T875466957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.Editor.LocalizedText
struct  LocalizedText_t875466957  : public MonoBehaviour_t3962482529
{
public:
	// System.String SmartLocalization.Editor.LocalizedText::localizedKey
	String_t* ___localizedKey_2;
	// UnityEngine.UI.Text SmartLocalization.Editor.LocalizedText::textObject
	Text_t1901882714 * ___textObject_3;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedText_t875466957, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___localizedKey_2), value);
	}

	inline static int32_t get_offset_of_textObject_3() { return static_cast<int32_t>(offsetof(LocalizedText_t875466957, ___textObject_3)); }
	inline Text_t1901882714 * get_textObject_3() const { return ___textObject_3; }
	inline Text_t1901882714 ** get_address_of_textObject_3() { return &___textObject_3; }
	inline void set_textObject_3(Text_t1901882714 * value)
	{
		___textObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___textObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDTEXT_T875466957_H
#ifndef DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#define DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.DebuggableMonoBehaviour
struct  DebuggableMonoBehaviour_t3692909384  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#ifndef TOUCHVISUALIZER_T699015915_H
#define TOUCHVISUALIZER_T699015915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchVisualizer
struct  TouchVisualizer_t699015915  : public MonoBehaviour_t3962482529
{
public:
	// TouchScript.Behaviors.Visualizer.TouchProxyBase TouchScript.Behaviors.Visualizer.TouchVisualizer::touchProxy
	TouchProxyBase_t1980850257 * ___touchProxy_2;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::showTouchId
	bool ___showTouchId_3;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::showTags
	bool ___showTags_4;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::useDPI
	bool ___useDPI_5;
	// System.Single TouchScript.Behaviors.Visualizer.TouchVisualizer::touchSize
	float ___touchSize_6;
	// System.Int32 TouchScript.Behaviors.Visualizer.TouchVisualizer::defaultSize
	int32_t ___defaultSize_7;
	// UnityEngine.RectTransform TouchScript.Behaviors.Visualizer.TouchVisualizer::rect
	RectTransform_t3704657025 * ___rect_8;
	// TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase> TouchScript.Behaviors.Visualizer.TouchVisualizer::pool
	ObjectPool_1_t2604094430 * ___pool_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase> TouchScript.Behaviors.Visualizer.TouchVisualizer::proxies
	Dictionary_2_t869563588 * ___proxies_10;

public:
	inline static int32_t get_offset_of_touchProxy_2() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___touchProxy_2)); }
	inline TouchProxyBase_t1980850257 * get_touchProxy_2() const { return ___touchProxy_2; }
	inline TouchProxyBase_t1980850257 ** get_address_of_touchProxy_2() { return &___touchProxy_2; }
	inline void set_touchProxy_2(TouchProxyBase_t1980850257 * value)
	{
		___touchProxy_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchProxy_2), value);
	}

	inline static int32_t get_offset_of_showTouchId_3() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___showTouchId_3)); }
	inline bool get_showTouchId_3() const { return ___showTouchId_3; }
	inline bool* get_address_of_showTouchId_3() { return &___showTouchId_3; }
	inline void set_showTouchId_3(bool value)
	{
		___showTouchId_3 = value;
	}

	inline static int32_t get_offset_of_showTags_4() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___showTags_4)); }
	inline bool get_showTags_4() const { return ___showTags_4; }
	inline bool* get_address_of_showTags_4() { return &___showTags_4; }
	inline void set_showTags_4(bool value)
	{
		___showTags_4 = value;
	}

	inline static int32_t get_offset_of_useDPI_5() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___useDPI_5)); }
	inline bool get_useDPI_5() const { return ___useDPI_5; }
	inline bool* get_address_of_useDPI_5() { return &___useDPI_5; }
	inline void set_useDPI_5(bool value)
	{
		___useDPI_5 = value;
	}

	inline static int32_t get_offset_of_touchSize_6() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___touchSize_6)); }
	inline float get_touchSize_6() const { return ___touchSize_6; }
	inline float* get_address_of_touchSize_6() { return &___touchSize_6; }
	inline void set_touchSize_6(float value)
	{
		___touchSize_6 = value;
	}

	inline static int32_t get_offset_of_defaultSize_7() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___defaultSize_7)); }
	inline int32_t get_defaultSize_7() const { return ___defaultSize_7; }
	inline int32_t* get_address_of_defaultSize_7() { return &___defaultSize_7; }
	inline void set_defaultSize_7(int32_t value)
	{
		___defaultSize_7 = value;
	}

	inline static int32_t get_offset_of_rect_8() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___rect_8)); }
	inline RectTransform_t3704657025 * get_rect_8() const { return ___rect_8; }
	inline RectTransform_t3704657025 ** get_address_of_rect_8() { return &___rect_8; }
	inline void set_rect_8(RectTransform_t3704657025 * value)
	{
		___rect_8 = value;
		Il2CppCodeGenWriteBarrier((&___rect_8), value);
	}

	inline static int32_t get_offset_of_pool_9() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___pool_9)); }
	inline ObjectPool_1_t2604094430 * get_pool_9() const { return ___pool_9; }
	inline ObjectPool_1_t2604094430 ** get_address_of_pool_9() { return &___pool_9; }
	inline void set_pool_9(ObjectPool_1_t2604094430 * value)
	{
		___pool_9 = value;
		Il2CppCodeGenWriteBarrier((&___pool_9), value);
	}

	inline static int32_t get_offset_of_proxies_10() { return static_cast<int32_t>(offsetof(TouchVisualizer_t699015915, ___proxies_10)); }
	inline Dictionary_2_t869563588 * get_proxies_10() const { return ___proxies_10; }
	inline Dictionary_2_t869563588 ** get_address_of_proxies_10() { return &___proxies_10; }
	inline void set_proxies_10(Dictionary_2_t869563588 * value)
	{
		___proxies_10 = value;
		Il2CppCodeGenWriteBarrier((&___proxies_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHVISUALIZER_T699015915_H
#ifndef TOUCHPROXYBASE_T1980850257_H
#define TOUCHPROXYBASE_T1980850257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct  TouchProxyBase_t1980850257  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::<ShowTouchId>k__BackingField
	bool ___U3CShowTouchIdU3Ek__BackingField_2;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::<ShowTags>k__BackingField
	bool ___U3CShowTagsU3Ek__BackingField_3;
	// UnityEngine.RectTransform TouchScript.Behaviors.Visualizer.TouchProxyBase::rect
	RectTransform_t3704657025 * ___rect_4;
	// System.Int32 TouchScript.Behaviors.Visualizer.TouchProxyBase::size
	int32_t ___size_5;

public:
	inline static int32_t get_offset_of_U3CShowTouchIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchProxyBase_t1980850257, ___U3CShowTouchIdU3Ek__BackingField_2)); }
	inline bool get_U3CShowTouchIdU3Ek__BackingField_2() const { return ___U3CShowTouchIdU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CShowTouchIdU3Ek__BackingField_2() { return &___U3CShowTouchIdU3Ek__BackingField_2; }
	inline void set_U3CShowTouchIdU3Ek__BackingField_2(bool value)
	{
		___U3CShowTouchIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CShowTagsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchProxyBase_t1980850257, ___U3CShowTagsU3Ek__BackingField_3)); }
	inline bool get_U3CShowTagsU3Ek__BackingField_3() const { return ___U3CShowTagsU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CShowTagsU3Ek__BackingField_3() { return &___U3CShowTagsU3Ek__BackingField_3; }
	inline void set_U3CShowTagsU3Ek__BackingField_3(bool value)
	{
		___U3CShowTagsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_rect_4() { return static_cast<int32_t>(offsetof(TouchProxyBase_t1980850257, ___rect_4)); }
	inline RectTransform_t3704657025 * get_rect_4() const { return ___rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_rect_4() { return &___rect_4; }
	inline void set_rect_4(RectTransform_t3704657025 * value)
	{
		___rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___rect_4), value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(TouchProxyBase_t1980850257, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPROXYBASE_T1980850257_H
#ifndef TEXTREVERSE_T1080962664_H
#define TEXTREVERSE_T1080962664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextReverse
struct  TextReverse_t1080962664  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREVERSE_T1080962664_H
#ifndef GESTUREMANAGERINSTANCE_T955425494_H
#define GESTUREMANAGERINSTANCE_T955425494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.GestureManagerInstance
struct  GestureManagerInstance_t955425494  : public MonoBehaviour_t3962482529
{
public:
	// TouchScript.IGestureDelegate TouchScript.GestureManagerInstance::<GlobalGestureDelegate>k__BackingField
	RuntimeObject* ___U3CGlobalGestureDelegateU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::gesturesToReset
	List_1_t1245047080 * ___gesturesToReset_5;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateBegan
	Action_2_t3198859162 * ____updateBegan_6;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateMoved
	Action_2_t3198859162 * ____updateMoved_7;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateEnded
	Action_2_t3198859162 * ____updateEnded_8;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateCancelled
	Action_2_t3198859162 * ____updateCancelled_9;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTarget
	Action_1_t3772833516 * ____processTarget_10;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTargetBegan
	Action_1_t3772833516 * ____processTargetBegan_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::targetTouches
	Dictionary_2_t1656671064 * ___targetTouches_12;
	// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::gestureTouches
	Dictionary_2_t518150435 * ___gestureTouches_13;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::activeGestures
	List_1_t1245047080 * ___activeGestures_14;

public:
	inline static int32_t get_offset_of_U3CGlobalGestureDelegateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ___U3CGlobalGestureDelegateU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CGlobalGestureDelegateU3Ek__BackingField_2() const { return ___U3CGlobalGestureDelegateU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CGlobalGestureDelegateU3Ek__BackingField_2() { return &___U3CGlobalGestureDelegateU3Ek__BackingField_2; }
	inline void set_U3CGlobalGestureDelegateU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CGlobalGestureDelegateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGlobalGestureDelegateU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_gesturesToReset_5() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ___gesturesToReset_5)); }
	inline List_1_t1245047080 * get_gesturesToReset_5() const { return ___gesturesToReset_5; }
	inline List_1_t1245047080 ** get_address_of_gesturesToReset_5() { return &___gesturesToReset_5; }
	inline void set_gesturesToReset_5(List_1_t1245047080 * value)
	{
		___gesturesToReset_5 = value;
		Il2CppCodeGenWriteBarrier((&___gesturesToReset_5), value);
	}

	inline static int32_t get_offset_of__updateBegan_6() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____updateBegan_6)); }
	inline Action_2_t3198859162 * get__updateBegan_6() const { return ____updateBegan_6; }
	inline Action_2_t3198859162 ** get_address_of__updateBegan_6() { return &____updateBegan_6; }
	inline void set__updateBegan_6(Action_2_t3198859162 * value)
	{
		____updateBegan_6 = value;
		Il2CppCodeGenWriteBarrier((&____updateBegan_6), value);
	}

	inline static int32_t get_offset_of__updateMoved_7() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____updateMoved_7)); }
	inline Action_2_t3198859162 * get__updateMoved_7() const { return ____updateMoved_7; }
	inline Action_2_t3198859162 ** get_address_of__updateMoved_7() { return &____updateMoved_7; }
	inline void set__updateMoved_7(Action_2_t3198859162 * value)
	{
		____updateMoved_7 = value;
		Il2CppCodeGenWriteBarrier((&____updateMoved_7), value);
	}

	inline static int32_t get_offset_of__updateEnded_8() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____updateEnded_8)); }
	inline Action_2_t3198859162 * get__updateEnded_8() const { return ____updateEnded_8; }
	inline Action_2_t3198859162 ** get_address_of__updateEnded_8() { return &____updateEnded_8; }
	inline void set__updateEnded_8(Action_2_t3198859162 * value)
	{
		____updateEnded_8 = value;
		Il2CppCodeGenWriteBarrier((&____updateEnded_8), value);
	}

	inline static int32_t get_offset_of__updateCancelled_9() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____updateCancelled_9)); }
	inline Action_2_t3198859162 * get__updateCancelled_9() const { return ____updateCancelled_9; }
	inline Action_2_t3198859162 ** get_address_of__updateCancelled_9() { return &____updateCancelled_9; }
	inline void set__updateCancelled_9(Action_2_t3198859162 * value)
	{
		____updateCancelled_9 = value;
		Il2CppCodeGenWriteBarrier((&____updateCancelled_9), value);
	}

	inline static int32_t get_offset_of__processTarget_10() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____processTarget_10)); }
	inline Action_1_t3772833516 * get__processTarget_10() const { return ____processTarget_10; }
	inline Action_1_t3772833516 ** get_address_of__processTarget_10() { return &____processTarget_10; }
	inline void set__processTarget_10(Action_1_t3772833516 * value)
	{
		____processTarget_10 = value;
		Il2CppCodeGenWriteBarrier((&____processTarget_10), value);
	}

	inline static int32_t get_offset_of__processTargetBegan_11() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ____processTargetBegan_11)); }
	inline Action_1_t3772833516 * get__processTargetBegan_11() const { return ____processTargetBegan_11; }
	inline Action_1_t3772833516 ** get_address_of__processTargetBegan_11() { return &____processTargetBegan_11; }
	inline void set__processTargetBegan_11(Action_1_t3772833516 * value)
	{
		____processTargetBegan_11 = value;
		Il2CppCodeGenWriteBarrier((&____processTargetBegan_11), value);
	}

	inline static int32_t get_offset_of_targetTouches_12() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ___targetTouches_12)); }
	inline Dictionary_2_t1656671064 * get_targetTouches_12() const { return ___targetTouches_12; }
	inline Dictionary_2_t1656671064 ** get_address_of_targetTouches_12() { return &___targetTouches_12; }
	inline void set_targetTouches_12(Dictionary_2_t1656671064 * value)
	{
		___targetTouches_12 = value;
		Il2CppCodeGenWriteBarrier((&___targetTouches_12), value);
	}

	inline static int32_t get_offset_of_gestureTouches_13() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ___gestureTouches_13)); }
	inline Dictionary_2_t518150435 * get_gestureTouches_13() const { return ___gestureTouches_13; }
	inline Dictionary_2_t518150435 ** get_address_of_gestureTouches_13() { return &___gestureTouches_13; }
	inline void set_gestureTouches_13(Dictionary_2_t518150435 * value)
	{
		___gestureTouches_13 = value;
		Il2CppCodeGenWriteBarrier((&___gestureTouches_13), value);
	}

	inline static int32_t get_offset_of_activeGestures_14() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494, ___activeGestures_14)); }
	inline List_1_t1245047080 * get_activeGestures_14() const { return ___activeGestures_14; }
	inline List_1_t1245047080 ** get_address_of_activeGestures_14() { return &___activeGestures_14; }
	inline void set_activeGestures_14(List_1_t1245047080 * value)
	{
		___activeGestures_14 = value;
		Il2CppCodeGenWriteBarrier((&___activeGestures_14), value);
	}
};

struct GestureManagerInstance_t955425494_StaticFields
{
public:
	// TouchScript.GestureManagerInstance TouchScript.GestureManagerInstance::instance
	GestureManagerInstance_t955425494 * ___instance_3;
	// System.Boolean TouchScript.GestureManagerInstance::shuttingDown
	bool ___shuttingDown_4;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>> TouchScript.GestureManagerInstance::gestureListPool
	ObjectPool_1_t1868291253 * ___gestureListPool_15;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::touchListPool
	ObjectPool_1_t2969210650 * ___touchListPool_16;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>> TouchScript.GestureManagerInstance::transformListPool
	ObjectPool_1_t1400717540 * ___transformListPool_17;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494_StaticFields, ___instance_3)); }
	inline GestureManagerInstance_t955425494 * get_instance_3() const { return ___instance_3; }
	inline GestureManagerInstance_t955425494 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(GestureManagerInstance_t955425494 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_shuttingDown_4() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494_StaticFields, ___shuttingDown_4)); }
	inline bool get_shuttingDown_4() const { return ___shuttingDown_4; }
	inline bool* get_address_of_shuttingDown_4() { return &___shuttingDown_4; }
	inline void set_shuttingDown_4(bool value)
	{
		___shuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_gestureListPool_15() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494_StaticFields, ___gestureListPool_15)); }
	inline ObjectPool_1_t1868291253 * get_gestureListPool_15() const { return ___gestureListPool_15; }
	inline ObjectPool_1_t1868291253 ** get_address_of_gestureListPool_15() { return &___gestureListPool_15; }
	inline void set_gestureListPool_15(ObjectPool_1_t1868291253 * value)
	{
		___gestureListPool_15 = value;
		Il2CppCodeGenWriteBarrier((&___gestureListPool_15), value);
	}

	inline static int32_t get_offset_of_touchListPool_16() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494_StaticFields, ___touchListPool_16)); }
	inline ObjectPool_1_t2969210650 * get_touchListPool_16() const { return ___touchListPool_16; }
	inline ObjectPool_1_t2969210650 ** get_address_of_touchListPool_16() { return &___touchListPool_16; }
	inline void set_touchListPool_16(ObjectPool_1_t2969210650 * value)
	{
		___touchListPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchListPool_16), value);
	}

	inline static int32_t get_offset_of_transformListPool_17() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t955425494_StaticFields, ___transformListPool_17)); }
	inline ObjectPool_1_t1400717540 * get_transformListPool_17() const { return ___transformListPool_17; }
	inline ObjectPool_1_t1400717540 ** get_address_of_transformListPool_17() { return &___transformListPool_17; }
	inline void set_transformListPool_17(ObjectPool_1_t1400717540 * value)
	{
		___transformListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___transformListPool_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGERINSTANCE_T955425494_H
#ifndef GESTUREMANAGER_T1744876654_H
#define GESTUREMANAGER_T1744876654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.GestureManager
struct  GestureManager_t1744876654  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGER_T1744876654_H
#ifndef HITTEST_T1158317340_H
#define HITTEST_T1158317340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.HitTest
struct  HitTest_t1158317340  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTEST_T1158317340_H
#ifndef WORLDMAPCOORDSCHANGER_T692571169_H
#define WORLDMAPCOORDSCHANGER_T692571169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapCoordsChanger
struct  WorldMapCoordsChanger_t692571169  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField WorldMapCoordsChanger::xInput
	InputField_t3762917431 * ___xInput_2;
	// UnityEngine.UI.InputField WorldMapCoordsChanger::yInput
	InputField_t3762917431 * ___yInput_3;
	// TestWorldChunksManager WorldMapCoordsChanger::chunksManager
	TestWorldChunksManager_t1200850834 * ___chunksManager_4;

public:
	inline static int32_t get_offset_of_xInput_2() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___xInput_2)); }
	inline InputField_t3762917431 * get_xInput_2() const { return ___xInput_2; }
	inline InputField_t3762917431 ** get_address_of_xInput_2() { return &___xInput_2; }
	inline void set_xInput_2(InputField_t3762917431 * value)
	{
		___xInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___xInput_2), value);
	}

	inline static int32_t get_offset_of_yInput_3() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___yInput_3)); }
	inline InputField_t3762917431 * get_yInput_3() const { return ___yInput_3; }
	inline InputField_t3762917431 ** get_address_of_yInput_3() { return &___yInput_3; }
	inline void set_yInput_3(InputField_t3762917431 * value)
	{
		___yInput_3 = value;
		Il2CppCodeGenWriteBarrier((&___yInput_3), value);
	}

	inline static int32_t get_offset_of_chunksManager_4() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___chunksManager_4)); }
	inline TestWorldChunksManager_t1200850834 * get_chunksManager_4() const { return ___chunksManager_4; }
	inline TestWorldChunksManager_t1200850834 ** get_address_of_chunksManager_4() { return &___chunksManager_4; }
	inline void set_chunksManager_4(TestWorldChunksManager_t1200850834 * value)
	{
		___chunksManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___chunksManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDMAPCOORDSCHANGER_T692571169_H
#ifndef INPUTSOURCE_T979757727_H
#define INPUTSOURCE_T979757727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputSource
struct  InputSource_t979757727  : public MonoBehaviour_t3962482529
{
public:
	// TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputSource::<CoordinatesRemapper>k__BackingField
	RuntimeObject* ___U3CCoordinatesRemapperU3Ek__BackingField_2;
	// System.Boolean TouchScript.InputSources.InputSource::advancedProps
	bool ___advancedProps_3;
	// TouchScript.TouchManagerInstance TouchScript.InputSources.InputSource::manager
	TouchManagerInstance_t3252675660 * ___manager_4;

public:
	inline static int32_t get_offset_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputSource_t979757727, ___U3CCoordinatesRemapperU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CCoordinatesRemapperU3Ek__BackingField_2() const { return ___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return &___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline void set_U3CCoordinatesRemapperU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CCoordinatesRemapperU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCoordinatesRemapperU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_advancedProps_3() { return static_cast<int32_t>(offsetof(InputSource_t979757727, ___advancedProps_3)); }
	inline bool get_advancedProps_3() const { return ___advancedProps_3; }
	inline bool* get_address_of_advancedProps_3() { return &___advancedProps_3; }
	inline void set_advancedProps_3(bool value)
	{
		___advancedProps_3 = value;
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(InputSource_t979757727, ___manager_4)); }
	inline TouchManagerInstance_t3252675660 * get_manager_4() const { return ___manager_4; }
	inline TouchManagerInstance_t3252675660 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(TouchManagerInstance_t3252675660 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T979757727_H
#ifndef WINDOWINSTANCEMANAGER_T3234774687_H
#define WINDOWINSTANCEMANAGER_T3234774687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowInstanceManager
struct  WindowInstanceManager_t3234774687  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean WindowInstanceManager::windowOpened
	bool ___windowOpened_3;
	// System.String WindowInstanceManager::shillingsHtml
	String_t* ___shillingsHtml_4;
	// UnityEngine.GameObject WindowInstanceManager::BuildingConstructionWindow
	GameObject_t1113636619 * ___BuildingConstructionWindow_5;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindow
	GameObject_t1113636619 * ___BuildingInformationWindow_6;
	// UnityEngine.GameObject WindowInstanceManager::ReportContainerWindow
	GameObject_t1113636619 * ___ReportContainerWindow_7;
	// UnityEngine.GameObject WindowInstanceManager::ShopWindow
	GameObject_t1113636619 * ___ShopWindow_8;
	// UnityEngine.GameObject WindowInstanceManager::MailWindow
	GameObject_t1113636619 * ___MailWindow_9;
	// UnityEngine.GameObject WindowInstanceManager::AlliancesWindow
	GameObject_t1113636619 * ___AlliancesWindow_10;
	// UnityEngine.GameObject WindowInstanceManager::UserInfoWindow
	GameObject_t1113636619 * ___UserInfoWindow_11;
	// UnityEngine.GameObject WindowInstanceManager::FinanceWindow
	GameObject_t1113636619 * ___FinanceWindow_12;
	// UnityEngine.GameObject WindowInstanceManager::HelpWindow
	GameObject_t1113636619 * ___HelpWindow_13;
	// UnityEngine.GameObject WindowInstanceManager::NewMailWindow
	GameObject_t1113636619 * ___NewMailWindow_14;
	// UnityEngine.GameObject WindowInstanceManager::MessageWindow
	GameObject_t1113636619 * ___MessageWindow_15;
	// UnityEngine.GameObject WindowInstanceManager::MessageAllianceMemberWindow
	GameObject_t1113636619 * ___MessageAllianceMemberWindow_16;
	// UnityEngine.GameObject WindowInstanceManager::EventsWindow
	GameObject_t1113636619 * ___EventsWindow_17;
	// UnityEngine.GameObject WindowInstanceManager::AllianceInfoWindow
	GameObject_t1113636619 * ___AllianceInfoWindow_18;
	// UnityEngine.GameObject WindowInstanceManager::UnitDetailsWindow
	GameObject_t1113636619 * ___UnitDetailsWindow_19;
	// UnityEngine.GameObject WindowInstanceManager::CityInfoWindow
	GameObject_t1113636619 * ___CityInfoWindow_20;
	// UnityEngine.GameObject WindowInstanceManager::ValleyInfoWindow
	GameObject_t1113636619 * ___ValleyInfoWindow_21;
	// UnityEngine.GameObject WindowInstanceManager::DispatchWindow
	GameObject_t1113636619 * ___DispatchWindow_22;
	// UnityEngine.GameObject WindowInstanceManager::RecruitBarbariansWindow
	GameObject_t1113636619 * ___RecruitBarbariansWindow_23;
	// UnityEngine.GameObject WindowInstanceManager::NotificationWindow
	GameObject_t1113636619 * ___NotificationWindow_24;
	// UnityEngine.GameObject WindowInstanceManager::LoginProgressBar
	GameObject_t1113636619 * ___LoginProgressBar_25;
	// UnityEngine.GameObject WindowInstanceManager::KnightInfoWindow
	GameObject_t1113636619 * ___KnightInfoWindow_26;
	// UnityEngine.GameObject WindowInstanceManager::BuyItemWindow
	GameObject_t1113636619 * ___BuyItemWindow_27;
	// UnityEngine.GameObject WindowInstanceManager::SellItemWindow
	GameObject_t1113636619 * ___SellItemWindow_28;
	// UnityEngine.GameObject WindowInstanceManager::SpeedUpWindow
	GameObject_t1113636619 * ___SpeedUpWindow_29;
	// UnityEngine.GameObject WindowInstanceManager::UpgradeBuildingWindow
	GameObject_t1113636619 * ___UpgradeBuildingWindow_30;
	// UnityEngine.GameObject WindowInstanceManager::AdvencedTeleportWindow
	GameObject_t1113636619 * ___AdvencedTeleportWindow_31;
	// UnityEngine.GameObject WindowInstanceManager::CityDeedWindow
	GameObject_t1113636619 * ___CityDeedWindow_32;
	// UnityEngine.GameObject WindowInstanceManager::BuyShillingsWindow
	GameObject_t1113636619 * ___BuyShillingsWindow_33;
	// UnityEngine.GameObject WindowInstanceManager::ChangeLanguageWindow
	GameObject_t1113636619 * ___ChangeLanguageWindow_34;
	// UnityEngine.GameObject WindowInstanceManager::DismissWindow
	GameObject_t1113636619 * ___DismissWindow_35;
	// UnityEngine.GameObject WindowInstanceManager::ChangePasswordWindow
	GameObject_t1113636619 * ___ChangePasswordWindow_36;
	// UnityEngine.GameObject WindowInstanceManager::TributesWindow
	GameObject_t1113636619 * ___TributesWindow_37;
	// UnityEngine.GameObject WindowInstanceManager::UpdateBuildingWindow
	GameObject_t1113636619 * ___UpdateBuildingWindow_38;
	// UnityEngine.GameObject WindowInstanceManager::BattleLogWindow
	GameObject_t1113636619 * ___BattleLogWindow_39;
	// UnityEngine.GameObject WindowInstanceManager::BuildNewCityWindow
	GameObject_t1113636619 * ___BuildNewCityWindow_40;
	// UnityEngine.GameObject WindowInstanceManager::ArmyDetailsWIndow
	GameObject_t1113636619 * ___ArmyDetailsWIndow_41;
	// UnityEngine.GameObject WindowInstanceManager::NewAllianceNameWindow
	GameObject_t1113636619 * ___NewAllianceNameWindow_42;
	// UnityEngine.GameObject WindowInstanceManager::ConfirmationWindow
	GameObject_t1113636619 * ___ConfirmationWindow_43;
	// UnityEngine.GameObject WindowInstanceManager::ShieldTimeWindow
	GameObject_t1113636619 * ___ShieldTimeWindow_44;
	// UnityEngine.GameObject WindowInstanceManager::TutorialWindow
	GameObject_t1113636619 * ___TutorialWindow_45;
	// UnityEngine.GameObject WindowInstanceManager::VideoAdsWindow
	GameObject_t1113636619 * ___VideoAdsWindow_46;
	// UnityEngine.GameObject WindowInstanceManager::BuildingConstructionWindowInstance
	GameObject_t1113636619 * ___BuildingConstructionWindowInstance_47;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindowInstance
	GameObject_t1113636619 * ___BuildingInformationWindowInstance_48;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindowInstance2
	GameObject_t1113636619 * ___BuildingInformationWindowInstance2_49;
	// UnityEngine.GameObject WindowInstanceManager::ReportContainerWindowInstance
	GameObject_t1113636619 * ___ReportContainerWindowInstance_50;
	// UnityEngine.GameObject WindowInstanceManager::ShopWindowInstance
	GameObject_t1113636619 * ___ShopWindowInstance_51;
	// UnityEngine.GameObject WindowInstanceManager::MailWindowInstance
	GameObject_t1113636619 * ___MailWindowInstance_52;
	// UnityEngine.GameObject WindowInstanceManager::AlliancesWindowInstance
	GameObject_t1113636619 * ___AlliancesWindowInstance_53;
	// UnityEngine.GameObject WindowInstanceManager::UserInfoWindowInstance
	GameObject_t1113636619 * ___UserInfoWindowInstance_54;
	// UnityEngine.GameObject WindowInstanceManager::FinanceWindowInstance
	GameObject_t1113636619 * ___FinanceWindowInstance_55;
	// UnityEngine.GameObject WindowInstanceManager::HelpWindowInstance
	GameObject_t1113636619 * ___HelpWindowInstance_56;
	// UnityEngine.GameObject WindowInstanceManager::NewMailWindowInstance
	GameObject_t1113636619 * ___NewMailWindowInstance_57;
	// UnityEngine.GameObject WindowInstanceManager::MessageWindowInstance
	GameObject_t1113636619 * ___MessageWindowInstance_58;
	// UnityEngine.GameObject WindowInstanceManager::MessageAllianceMemberWindowInstance
	GameObject_t1113636619 * ___MessageAllianceMemberWindowInstance_59;
	// UnityEngine.GameObject WindowInstanceManager::EventsWindowInstance
	GameObject_t1113636619 * ___EventsWindowInstance_60;
	// UnityEngine.GameObject WindowInstanceManager::AllianceInfoWindowInstance
	GameObject_t1113636619 * ___AllianceInfoWindowInstance_61;
	// UnityEngine.GameObject WindowInstanceManager::UnitDetailsWindowInstance
	GameObject_t1113636619 * ___UnitDetailsWindowInstance_62;
	// UnityEngine.GameObject WindowInstanceManager::CityInfoWindowInstance
	GameObject_t1113636619 * ___CityInfoWindowInstance_63;
	// UnityEngine.GameObject WindowInstanceManager::DispatchWindowInstance
	GameObject_t1113636619 * ___DispatchWindowInstance_64;
	// UnityEngine.GameObject WindowInstanceManager::RecruitBarbariansWindowInstance
	GameObject_t1113636619 * ___RecruitBarbariansWindowInstance_65;
	// UnityEngine.GameObject WindowInstanceManager::ValleyInfoWindowInstance
	GameObject_t1113636619 * ___ValleyInfoWindowInstance_66;
	// UnityEngine.GameObject WindowInstanceManager::NotificationWindowInstance
	GameObject_t1113636619 * ___NotificationWindowInstance_67;
	// UnityEngine.GameObject WindowInstanceManager::LoginProgressBarInstance
	GameObject_t1113636619 * ___LoginProgressBarInstance_68;
	// UnityEngine.GameObject WindowInstanceManager::KnightInfoWindowInstance
	GameObject_t1113636619 * ___KnightInfoWindowInstance_69;
	// UnityEngine.GameObject WindowInstanceManager::BuyItemWindowInstance
	GameObject_t1113636619 * ___BuyItemWindowInstance_70;
	// UnityEngine.GameObject WindowInstanceManager::SellItemWindowInstance
	GameObject_t1113636619 * ___SellItemWindowInstance_71;
	// UnityEngine.GameObject WindowInstanceManager::SpeedUpWindowInstance
	GameObject_t1113636619 * ___SpeedUpWindowInstance_72;
	// UnityEngine.GameObject WindowInstanceManager::UpgradeBuildingWindowInstance
	GameObject_t1113636619 * ___UpgradeBuildingWindowInstance_73;
	// UnityEngine.GameObject WindowInstanceManager::AdvencedTeleportWindowInstance
	GameObject_t1113636619 * ___AdvencedTeleportWindowInstance_74;
	// UnityEngine.GameObject WindowInstanceManager::CityDeedWindowInstance
	GameObject_t1113636619 * ___CityDeedWindowInstance_75;
	// UnityEngine.GameObject WindowInstanceManager::BuyShillingsWindowInstance
	GameObject_t1113636619 * ___BuyShillingsWindowInstance_76;
	// UnityEngine.GameObject WindowInstanceManager::ChangeLanguageWindowInstance
	GameObject_t1113636619 * ___ChangeLanguageWindowInstance_77;
	// UnityEngine.GameObject WindowInstanceManager::DismissWindowInstance
	GameObject_t1113636619 * ___DismissWindowInstance_78;
	// UnityEngine.GameObject WindowInstanceManager::ChangePasswordWindowInstance
	GameObject_t1113636619 * ___ChangePasswordWindowInstance_79;
	// UnityEngine.GameObject WindowInstanceManager::TributesWindowInstance
	GameObject_t1113636619 * ___TributesWindowInstance_80;
	// UnityEngine.GameObject WindowInstanceManager::UpdateBuildingWindowInstance
	GameObject_t1113636619 * ___UpdateBuildingWindowInstance_81;
	// UnityEngine.GameObject WindowInstanceManager::BattleLogWindowInstance
	GameObject_t1113636619 * ___BattleLogWindowInstance_82;
	// UnityEngine.GameObject WindowInstanceManager::BuildNewCityWindowInstance
	GameObject_t1113636619 * ___BuildNewCityWindowInstance_83;
	// UnityEngine.GameObject WindowInstanceManager::ArmyDetailsWindowInstance
	GameObject_t1113636619 * ___ArmyDetailsWindowInstance_84;
	// UnityEngine.GameObject WindowInstanceManager::ShillingsWebViewWindowInstance
	GameObject_t1113636619 * ___ShillingsWebViewWindowInstance_85;
	// UnityEngine.GameObject WindowInstanceManager::NewAllianceNameWindowInstance
	GameObject_t1113636619 * ___NewAllianceNameWindowInstance_86;
	// UnityEngine.GameObject WindowInstanceManager::ConfirmationWindowInstance
	GameObject_t1113636619 * ___ConfirmationWindowInstance_87;
	// UnityEngine.GameObject WindowInstanceManager::ShieldTimeWindowInstance
	GameObject_t1113636619 * ___ShieldTimeWindowInstance_88;
	// UnityEngine.GameObject WindowInstanceManager::TutorialWindowInstance
	GameObject_t1113636619 * ___TutorialWindowInstance_89;
	// UnityEngine.GameObject WindowInstanceManager::VideoAdsWindowInstance
	GameObject_t1113636619 * ___VideoAdsWindowInstance_90;
	// UnityEngine.GameObject WindowInstanceManager::overviewPanel
	GameObject_t1113636619 * ___overviewPanel_91;
	// UnityEngine.GameObject WindowInstanceManager::statisticsPanel
	GameObject_t1113636619 * ___statisticsPanel_92;
	// UnityEngine.GameObject WindowInstanceManager::actionPanel
	GameObject_t1113636619 * ___actionPanel_93;
	// ActionManager WindowInstanceManager::actionManager
	ActionManager_t2430268190 * ___actionManager_94;
	// AllianceContentChanger WindowInstanceManager::allianceManager
	AllianceContentChanger_t2140085977 * ___allianceManager_95;
	// TreasureManager WindowInstanceManager::treasureManager
	TreasureManager_t110095690 * ___treasureManager_96;
	// UnityEngine.GameObject WindowInstanceManager::oldDesignPanel
	GameObject_t1113636619 * ___oldDesignPanel_97;
	// UnityEngine.GameObject WindowInstanceManager::newDesignPanel
	GameObject_t1113636619 * ___newDesignPanel_98;
	// UnityEngine.GameObject WindowInstanceManager::viewPanel
	GameObject_t1113636619 * ___viewPanel_99;
	// UnityEngine.GameObject WindowInstanceManager::itemPanel
	GameObject_t1113636619 * ___itemPanel_100;
	// UnityEngine.GameObject WindowInstanceManager::citiesPanel
	GameObject_t1113636619 * ___citiesPanel_101;
	// UnityEngine.UI.Image WindowInstanceManager::viewButton
	Image_t2670269651 * ___viewButton_102;
	// UnityEngine.UI.Image WindowInstanceManager::itemButton
	Image_t2670269651 * ___itemButton_103;
	// UnityEngine.UI.Image WindowInstanceManager::voiceButton
	Image_t2670269651 * ___voiceButton_104;
	// UnityEngine.Sprite WindowInstanceManager::viewClosedSprite
	Sprite_t280657092 * ___viewClosedSprite_105;
	// UnityEngine.Sprite WindowInstanceManager::viewOpenedSprite
	Sprite_t280657092 * ___viewOpenedSprite_106;
	// UnityEngine.Sprite WindowInstanceManager::itemClosedSprite
	Sprite_t280657092 * ___itemClosedSprite_107;
	// UnityEngine.Sprite WindowInstanceManager::itemOpenedSprite
	Sprite_t280657092 * ___itemOpenedSprite_108;
	// UnityEngine.Sprite WindowInstanceManager::voiceOnSprite
	Sprite_t280657092 * ___voiceOnSprite_109;
	// UnityEngine.Sprite WindowInstanceManager::voiceOffSprite
	Sprite_t280657092 * ___voiceOffSprite_110;
	// UnityEngine.GameObject WindowInstanceManager::UI
	GameObject_t1113636619 * ___UI_111;
	// UnityEngine.GameObject WindowInstanceManager::Shell
	GameObject_t1113636619 * ___Shell_112;

public:
	inline static int32_t get_offset_of_windowOpened_3() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___windowOpened_3)); }
	inline bool get_windowOpened_3() const { return ___windowOpened_3; }
	inline bool* get_address_of_windowOpened_3() { return &___windowOpened_3; }
	inline void set_windowOpened_3(bool value)
	{
		___windowOpened_3 = value;
	}

	inline static int32_t get_offset_of_shillingsHtml_4() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___shillingsHtml_4)); }
	inline String_t* get_shillingsHtml_4() const { return ___shillingsHtml_4; }
	inline String_t** get_address_of_shillingsHtml_4() { return &___shillingsHtml_4; }
	inline void set_shillingsHtml_4(String_t* value)
	{
		___shillingsHtml_4 = value;
		Il2CppCodeGenWriteBarrier((&___shillingsHtml_4), value);
	}

	inline static int32_t get_offset_of_BuildingConstructionWindow_5() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingConstructionWindow_5)); }
	inline GameObject_t1113636619 * get_BuildingConstructionWindow_5() const { return ___BuildingConstructionWindow_5; }
	inline GameObject_t1113636619 ** get_address_of_BuildingConstructionWindow_5() { return &___BuildingConstructionWindow_5; }
	inline void set_BuildingConstructionWindow_5(GameObject_t1113636619 * value)
	{
		___BuildingConstructionWindow_5 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingConstructionWindow_5), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindow_6() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindow_6)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindow_6() const { return ___BuildingInformationWindow_6; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindow_6() { return &___BuildingInformationWindow_6; }
	inline void set_BuildingInformationWindow_6(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindow_6 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindow_6), value);
	}

	inline static int32_t get_offset_of_ReportContainerWindow_7() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ReportContainerWindow_7)); }
	inline GameObject_t1113636619 * get_ReportContainerWindow_7() const { return ___ReportContainerWindow_7; }
	inline GameObject_t1113636619 ** get_address_of_ReportContainerWindow_7() { return &___ReportContainerWindow_7; }
	inline void set_ReportContainerWindow_7(GameObject_t1113636619 * value)
	{
		___ReportContainerWindow_7 = value;
		Il2CppCodeGenWriteBarrier((&___ReportContainerWindow_7), value);
	}

	inline static int32_t get_offset_of_ShopWindow_8() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShopWindow_8)); }
	inline GameObject_t1113636619 * get_ShopWindow_8() const { return ___ShopWindow_8; }
	inline GameObject_t1113636619 ** get_address_of_ShopWindow_8() { return &___ShopWindow_8; }
	inline void set_ShopWindow_8(GameObject_t1113636619 * value)
	{
		___ShopWindow_8 = value;
		Il2CppCodeGenWriteBarrier((&___ShopWindow_8), value);
	}

	inline static int32_t get_offset_of_MailWindow_9() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MailWindow_9)); }
	inline GameObject_t1113636619 * get_MailWindow_9() const { return ___MailWindow_9; }
	inline GameObject_t1113636619 ** get_address_of_MailWindow_9() { return &___MailWindow_9; }
	inline void set_MailWindow_9(GameObject_t1113636619 * value)
	{
		___MailWindow_9 = value;
		Il2CppCodeGenWriteBarrier((&___MailWindow_9), value);
	}

	inline static int32_t get_offset_of_AlliancesWindow_10() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AlliancesWindow_10)); }
	inline GameObject_t1113636619 * get_AlliancesWindow_10() const { return ___AlliancesWindow_10; }
	inline GameObject_t1113636619 ** get_address_of_AlliancesWindow_10() { return &___AlliancesWindow_10; }
	inline void set_AlliancesWindow_10(GameObject_t1113636619 * value)
	{
		___AlliancesWindow_10 = value;
		Il2CppCodeGenWriteBarrier((&___AlliancesWindow_10), value);
	}

	inline static int32_t get_offset_of_UserInfoWindow_11() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UserInfoWindow_11)); }
	inline GameObject_t1113636619 * get_UserInfoWindow_11() const { return ___UserInfoWindow_11; }
	inline GameObject_t1113636619 ** get_address_of_UserInfoWindow_11() { return &___UserInfoWindow_11; }
	inline void set_UserInfoWindow_11(GameObject_t1113636619 * value)
	{
		___UserInfoWindow_11 = value;
		Il2CppCodeGenWriteBarrier((&___UserInfoWindow_11), value);
	}

	inline static int32_t get_offset_of_FinanceWindow_12() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___FinanceWindow_12)); }
	inline GameObject_t1113636619 * get_FinanceWindow_12() const { return ___FinanceWindow_12; }
	inline GameObject_t1113636619 ** get_address_of_FinanceWindow_12() { return &___FinanceWindow_12; }
	inline void set_FinanceWindow_12(GameObject_t1113636619 * value)
	{
		___FinanceWindow_12 = value;
		Il2CppCodeGenWriteBarrier((&___FinanceWindow_12), value);
	}

	inline static int32_t get_offset_of_HelpWindow_13() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___HelpWindow_13)); }
	inline GameObject_t1113636619 * get_HelpWindow_13() const { return ___HelpWindow_13; }
	inline GameObject_t1113636619 ** get_address_of_HelpWindow_13() { return &___HelpWindow_13; }
	inline void set_HelpWindow_13(GameObject_t1113636619 * value)
	{
		___HelpWindow_13 = value;
		Il2CppCodeGenWriteBarrier((&___HelpWindow_13), value);
	}

	inline static int32_t get_offset_of_NewMailWindow_14() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewMailWindow_14)); }
	inline GameObject_t1113636619 * get_NewMailWindow_14() const { return ___NewMailWindow_14; }
	inline GameObject_t1113636619 ** get_address_of_NewMailWindow_14() { return &___NewMailWindow_14; }
	inline void set_NewMailWindow_14(GameObject_t1113636619 * value)
	{
		___NewMailWindow_14 = value;
		Il2CppCodeGenWriteBarrier((&___NewMailWindow_14), value);
	}

	inline static int32_t get_offset_of_MessageWindow_15() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageWindow_15)); }
	inline GameObject_t1113636619 * get_MessageWindow_15() const { return ___MessageWindow_15; }
	inline GameObject_t1113636619 ** get_address_of_MessageWindow_15() { return &___MessageWindow_15; }
	inline void set_MessageWindow_15(GameObject_t1113636619 * value)
	{
		___MessageWindow_15 = value;
		Il2CppCodeGenWriteBarrier((&___MessageWindow_15), value);
	}

	inline static int32_t get_offset_of_MessageAllianceMemberWindow_16() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageAllianceMemberWindow_16)); }
	inline GameObject_t1113636619 * get_MessageAllianceMemberWindow_16() const { return ___MessageAllianceMemberWindow_16; }
	inline GameObject_t1113636619 ** get_address_of_MessageAllianceMemberWindow_16() { return &___MessageAllianceMemberWindow_16; }
	inline void set_MessageAllianceMemberWindow_16(GameObject_t1113636619 * value)
	{
		___MessageAllianceMemberWindow_16 = value;
		Il2CppCodeGenWriteBarrier((&___MessageAllianceMemberWindow_16), value);
	}

	inline static int32_t get_offset_of_EventsWindow_17() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___EventsWindow_17)); }
	inline GameObject_t1113636619 * get_EventsWindow_17() const { return ___EventsWindow_17; }
	inline GameObject_t1113636619 ** get_address_of_EventsWindow_17() { return &___EventsWindow_17; }
	inline void set_EventsWindow_17(GameObject_t1113636619 * value)
	{
		___EventsWindow_17 = value;
		Il2CppCodeGenWriteBarrier((&___EventsWindow_17), value);
	}

	inline static int32_t get_offset_of_AllianceInfoWindow_18() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AllianceInfoWindow_18)); }
	inline GameObject_t1113636619 * get_AllianceInfoWindow_18() const { return ___AllianceInfoWindow_18; }
	inline GameObject_t1113636619 ** get_address_of_AllianceInfoWindow_18() { return &___AllianceInfoWindow_18; }
	inline void set_AllianceInfoWindow_18(GameObject_t1113636619 * value)
	{
		___AllianceInfoWindow_18 = value;
		Il2CppCodeGenWriteBarrier((&___AllianceInfoWindow_18), value);
	}

	inline static int32_t get_offset_of_UnitDetailsWindow_19() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UnitDetailsWindow_19)); }
	inline GameObject_t1113636619 * get_UnitDetailsWindow_19() const { return ___UnitDetailsWindow_19; }
	inline GameObject_t1113636619 ** get_address_of_UnitDetailsWindow_19() { return &___UnitDetailsWindow_19; }
	inline void set_UnitDetailsWindow_19(GameObject_t1113636619 * value)
	{
		___UnitDetailsWindow_19 = value;
		Il2CppCodeGenWriteBarrier((&___UnitDetailsWindow_19), value);
	}

	inline static int32_t get_offset_of_CityInfoWindow_20() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityInfoWindow_20)); }
	inline GameObject_t1113636619 * get_CityInfoWindow_20() const { return ___CityInfoWindow_20; }
	inline GameObject_t1113636619 ** get_address_of_CityInfoWindow_20() { return &___CityInfoWindow_20; }
	inline void set_CityInfoWindow_20(GameObject_t1113636619 * value)
	{
		___CityInfoWindow_20 = value;
		Il2CppCodeGenWriteBarrier((&___CityInfoWindow_20), value);
	}

	inline static int32_t get_offset_of_ValleyInfoWindow_21() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ValleyInfoWindow_21)); }
	inline GameObject_t1113636619 * get_ValleyInfoWindow_21() const { return ___ValleyInfoWindow_21; }
	inline GameObject_t1113636619 ** get_address_of_ValleyInfoWindow_21() { return &___ValleyInfoWindow_21; }
	inline void set_ValleyInfoWindow_21(GameObject_t1113636619 * value)
	{
		___ValleyInfoWindow_21 = value;
		Il2CppCodeGenWriteBarrier((&___ValleyInfoWindow_21), value);
	}

	inline static int32_t get_offset_of_DispatchWindow_22() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DispatchWindow_22)); }
	inline GameObject_t1113636619 * get_DispatchWindow_22() const { return ___DispatchWindow_22; }
	inline GameObject_t1113636619 ** get_address_of_DispatchWindow_22() { return &___DispatchWindow_22; }
	inline void set_DispatchWindow_22(GameObject_t1113636619 * value)
	{
		___DispatchWindow_22 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchWindow_22), value);
	}

	inline static int32_t get_offset_of_RecruitBarbariansWindow_23() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___RecruitBarbariansWindow_23)); }
	inline GameObject_t1113636619 * get_RecruitBarbariansWindow_23() const { return ___RecruitBarbariansWindow_23; }
	inline GameObject_t1113636619 ** get_address_of_RecruitBarbariansWindow_23() { return &___RecruitBarbariansWindow_23; }
	inline void set_RecruitBarbariansWindow_23(GameObject_t1113636619 * value)
	{
		___RecruitBarbariansWindow_23 = value;
		Il2CppCodeGenWriteBarrier((&___RecruitBarbariansWindow_23), value);
	}

	inline static int32_t get_offset_of_NotificationWindow_24() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NotificationWindow_24)); }
	inline GameObject_t1113636619 * get_NotificationWindow_24() const { return ___NotificationWindow_24; }
	inline GameObject_t1113636619 ** get_address_of_NotificationWindow_24() { return &___NotificationWindow_24; }
	inline void set_NotificationWindow_24(GameObject_t1113636619 * value)
	{
		___NotificationWindow_24 = value;
		Il2CppCodeGenWriteBarrier((&___NotificationWindow_24), value);
	}

	inline static int32_t get_offset_of_LoginProgressBar_25() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___LoginProgressBar_25)); }
	inline GameObject_t1113636619 * get_LoginProgressBar_25() const { return ___LoginProgressBar_25; }
	inline GameObject_t1113636619 ** get_address_of_LoginProgressBar_25() { return &___LoginProgressBar_25; }
	inline void set_LoginProgressBar_25(GameObject_t1113636619 * value)
	{
		___LoginProgressBar_25 = value;
		Il2CppCodeGenWriteBarrier((&___LoginProgressBar_25), value);
	}

	inline static int32_t get_offset_of_KnightInfoWindow_26() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___KnightInfoWindow_26)); }
	inline GameObject_t1113636619 * get_KnightInfoWindow_26() const { return ___KnightInfoWindow_26; }
	inline GameObject_t1113636619 ** get_address_of_KnightInfoWindow_26() { return &___KnightInfoWindow_26; }
	inline void set_KnightInfoWindow_26(GameObject_t1113636619 * value)
	{
		___KnightInfoWindow_26 = value;
		Il2CppCodeGenWriteBarrier((&___KnightInfoWindow_26), value);
	}

	inline static int32_t get_offset_of_BuyItemWindow_27() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyItemWindow_27)); }
	inline GameObject_t1113636619 * get_BuyItemWindow_27() const { return ___BuyItemWindow_27; }
	inline GameObject_t1113636619 ** get_address_of_BuyItemWindow_27() { return &___BuyItemWindow_27; }
	inline void set_BuyItemWindow_27(GameObject_t1113636619 * value)
	{
		___BuyItemWindow_27 = value;
		Il2CppCodeGenWriteBarrier((&___BuyItemWindow_27), value);
	}

	inline static int32_t get_offset_of_SellItemWindow_28() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SellItemWindow_28)); }
	inline GameObject_t1113636619 * get_SellItemWindow_28() const { return ___SellItemWindow_28; }
	inline GameObject_t1113636619 ** get_address_of_SellItemWindow_28() { return &___SellItemWindow_28; }
	inline void set_SellItemWindow_28(GameObject_t1113636619 * value)
	{
		___SellItemWindow_28 = value;
		Il2CppCodeGenWriteBarrier((&___SellItemWindow_28), value);
	}

	inline static int32_t get_offset_of_SpeedUpWindow_29() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SpeedUpWindow_29)); }
	inline GameObject_t1113636619 * get_SpeedUpWindow_29() const { return ___SpeedUpWindow_29; }
	inline GameObject_t1113636619 ** get_address_of_SpeedUpWindow_29() { return &___SpeedUpWindow_29; }
	inline void set_SpeedUpWindow_29(GameObject_t1113636619 * value)
	{
		___SpeedUpWindow_29 = value;
		Il2CppCodeGenWriteBarrier((&___SpeedUpWindow_29), value);
	}

	inline static int32_t get_offset_of_UpgradeBuildingWindow_30() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpgradeBuildingWindow_30)); }
	inline GameObject_t1113636619 * get_UpgradeBuildingWindow_30() const { return ___UpgradeBuildingWindow_30; }
	inline GameObject_t1113636619 ** get_address_of_UpgradeBuildingWindow_30() { return &___UpgradeBuildingWindow_30; }
	inline void set_UpgradeBuildingWindow_30(GameObject_t1113636619 * value)
	{
		___UpgradeBuildingWindow_30 = value;
		Il2CppCodeGenWriteBarrier((&___UpgradeBuildingWindow_30), value);
	}

	inline static int32_t get_offset_of_AdvencedTeleportWindow_31() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AdvencedTeleportWindow_31)); }
	inline GameObject_t1113636619 * get_AdvencedTeleportWindow_31() const { return ___AdvencedTeleportWindow_31; }
	inline GameObject_t1113636619 ** get_address_of_AdvencedTeleportWindow_31() { return &___AdvencedTeleportWindow_31; }
	inline void set_AdvencedTeleportWindow_31(GameObject_t1113636619 * value)
	{
		___AdvencedTeleportWindow_31 = value;
		Il2CppCodeGenWriteBarrier((&___AdvencedTeleportWindow_31), value);
	}

	inline static int32_t get_offset_of_CityDeedWindow_32() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityDeedWindow_32)); }
	inline GameObject_t1113636619 * get_CityDeedWindow_32() const { return ___CityDeedWindow_32; }
	inline GameObject_t1113636619 ** get_address_of_CityDeedWindow_32() { return &___CityDeedWindow_32; }
	inline void set_CityDeedWindow_32(GameObject_t1113636619 * value)
	{
		___CityDeedWindow_32 = value;
		Il2CppCodeGenWriteBarrier((&___CityDeedWindow_32), value);
	}

	inline static int32_t get_offset_of_BuyShillingsWindow_33() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyShillingsWindow_33)); }
	inline GameObject_t1113636619 * get_BuyShillingsWindow_33() const { return ___BuyShillingsWindow_33; }
	inline GameObject_t1113636619 ** get_address_of_BuyShillingsWindow_33() { return &___BuyShillingsWindow_33; }
	inline void set_BuyShillingsWindow_33(GameObject_t1113636619 * value)
	{
		___BuyShillingsWindow_33 = value;
		Il2CppCodeGenWriteBarrier((&___BuyShillingsWindow_33), value);
	}

	inline static int32_t get_offset_of_ChangeLanguageWindow_34() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangeLanguageWindow_34)); }
	inline GameObject_t1113636619 * get_ChangeLanguageWindow_34() const { return ___ChangeLanguageWindow_34; }
	inline GameObject_t1113636619 ** get_address_of_ChangeLanguageWindow_34() { return &___ChangeLanguageWindow_34; }
	inline void set_ChangeLanguageWindow_34(GameObject_t1113636619 * value)
	{
		___ChangeLanguageWindow_34 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeLanguageWindow_34), value);
	}

	inline static int32_t get_offset_of_DismissWindow_35() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DismissWindow_35)); }
	inline GameObject_t1113636619 * get_DismissWindow_35() const { return ___DismissWindow_35; }
	inline GameObject_t1113636619 ** get_address_of_DismissWindow_35() { return &___DismissWindow_35; }
	inline void set_DismissWindow_35(GameObject_t1113636619 * value)
	{
		___DismissWindow_35 = value;
		Il2CppCodeGenWriteBarrier((&___DismissWindow_35), value);
	}

	inline static int32_t get_offset_of_ChangePasswordWindow_36() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangePasswordWindow_36)); }
	inline GameObject_t1113636619 * get_ChangePasswordWindow_36() const { return ___ChangePasswordWindow_36; }
	inline GameObject_t1113636619 ** get_address_of_ChangePasswordWindow_36() { return &___ChangePasswordWindow_36; }
	inline void set_ChangePasswordWindow_36(GameObject_t1113636619 * value)
	{
		___ChangePasswordWindow_36 = value;
		Il2CppCodeGenWriteBarrier((&___ChangePasswordWindow_36), value);
	}

	inline static int32_t get_offset_of_TributesWindow_37() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TributesWindow_37)); }
	inline GameObject_t1113636619 * get_TributesWindow_37() const { return ___TributesWindow_37; }
	inline GameObject_t1113636619 ** get_address_of_TributesWindow_37() { return &___TributesWindow_37; }
	inline void set_TributesWindow_37(GameObject_t1113636619 * value)
	{
		___TributesWindow_37 = value;
		Il2CppCodeGenWriteBarrier((&___TributesWindow_37), value);
	}

	inline static int32_t get_offset_of_UpdateBuildingWindow_38() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpdateBuildingWindow_38)); }
	inline GameObject_t1113636619 * get_UpdateBuildingWindow_38() const { return ___UpdateBuildingWindow_38; }
	inline GameObject_t1113636619 ** get_address_of_UpdateBuildingWindow_38() { return &___UpdateBuildingWindow_38; }
	inline void set_UpdateBuildingWindow_38(GameObject_t1113636619 * value)
	{
		___UpdateBuildingWindow_38 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateBuildingWindow_38), value);
	}

	inline static int32_t get_offset_of_BattleLogWindow_39() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BattleLogWindow_39)); }
	inline GameObject_t1113636619 * get_BattleLogWindow_39() const { return ___BattleLogWindow_39; }
	inline GameObject_t1113636619 ** get_address_of_BattleLogWindow_39() { return &___BattleLogWindow_39; }
	inline void set_BattleLogWindow_39(GameObject_t1113636619 * value)
	{
		___BattleLogWindow_39 = value;
		Il2CppCodeGenWriteBarrier((&___BattleLogWindow_39), value);
	}

	inline static int32_t get_offset_of_BuildNewCityWindow_40() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildNewCityWindow_40)); }
	inline GameObject_t1113636619 * get_BuildNewCityWindow_40() const { return ___BuildNewCityWindow_40; }
	inline GameObject_t1113636619 ** get_address_of_BuildNewCityWindow_40() { return &___BuildNewCityWindow_40; }
	inline void set_BuildNewCityWindow_40(GameObject_t1113636619 * value)
	{
		___BuildNewCityWindow_40 = value;
		Il2CppCodeGenWriteBarrier((&___BuildNewCityWindow_40), value);
	}

	inline static int32_t get_offset_of_ArmyDetailsWIndow_41() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ArmyDetailsWIndow_41)); }
	inline GameObject_t1113636619 * get_ArmyDetailsWIndow_41() const { return ___ArmyDetailsWIndow_41; }
	inline GameObject_t1113636619 ** get_address_of_ArmyDetailsWIndow_41() { return &___ArmyDetailsWIndow_41; }
	inline void set_ArmyDetailsWIndow_41(GameObject_t1113636619 * value)
	{
		___ArmyDetailsWIndow_41 = value;
		Il2CppCodeGenWriteBarrier((&___ArmyDetailsWIndow_41), value);
	}

	inline static int32_t get_offset_of_NewAllianceNameWindow_42() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewAllianceNameWindow_42)); }
	inline GameObject_t1113636619 * get_NewAllianceNameWindow_42() const { return ___NewAllianceNameWindow_42; }
	inline GameObject_t1113636619 ** get_address_of_NewAllianceNameWindow_42() { return &___NewAllianceNameWindow_42; }
	inline void set_NewAllianceNameWindow_42(GameObject_t1113636619 * value)
	{
		___NewAllianceNameWindow_42 = value;
		Il2CppCodeGenWriteBarrier((&___NewAllianceNameWindow_42), value);
	}

	inline static int32_t get_offset_of_ConfirmationWindow_43() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ConfirmationWindow_43)); }
	inline GameObject_t1113636619 * get_ConfirmationWindow_43() const { return ___ConfirmationWindow_43; }
	inline GameObject_t1113636619 ** get_address_of_ConfirmationWindow_43() { return &___ConfirmationWindow_43; }
	inline void set_ConfirmationWindow_43(GameObject_t1113636619 * value)
	{
		___ConfirmationWindow_43 = value;
		Il2CppCodeGenWriteBarrier((&___ConfirmationWindow_43), value);
	}

	inline static int32_t get_offset_of_ShieldTimeWindow_44() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShieldTimeWindow_44)); }
	inline GameObject_t1113636619 * get_ShieldTimeWindow_44() const { return ___ShieldTimeWindow_44; }
	inline GameObject_t1113636619 ** get_address_of_ShieldTimeWindow_44() { return &___ShieldTimeWindow_44; }
	inline void set_ShieldTimeWindow_44(GameObject_t1113636619 * value)
	{
		___ShieldTimeWindow_44 = value;
		Il2CppCodeGenWriteBarrier((&___ShieldTimeWindow_44), value);
	}

	inline static int32_t get_offset_of_TutorialWindow_45() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TutorialWindow_45)); }
	inline GameObject_t1113636619 * get_TutorialWindow_45() const { return ___TutorialWindow_45; }
	inline GameObject_t1113636619 ** get_address_of_TutorialWindow_45() { return &___TutorialWindow_45; }
	inline void set_TutorialWindow_45(GameObject_t1113636619 * value)
	{
		___TutorialWindow_45 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialWindow_45), value);
	}

	inline static int32_t get_offset_of_VideoAdsWindow_46() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___VideoAdsWindow_46)); }
	inline GameObject_t1113636619 * get_VideoAdsWindow_46() const { return ___VideoAdsWindow_46; }
	inline GameObject_t1113636619 ** get_address_of_VideoAdsWindow_46() { return &___VideoAdsWindow_46; }
	inline void set_VideoAdsWindow_46(GameObject_t1113636619 * value)
	{
		___VideoAdsWindow_46 = value;
		Il2CppCodeGenWriteBarrier((&___VideoAdsWindow_46), value);
	}

	inline static int32_t get_offset_of_BuildingConstructionWindowInstance_47() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingConstructionWindowInstance_47)); }
	inline GameObject_t1113636619 * get_BuildingConstructionWindowInstance_47() const { return ___BuildingConstructionWindowInstance_47; }
	inline GameObject_t1113636619 ** get_address_of_BuildingConstructionWindowInstance_47() { return &___BuildingConstructionWindowInstance_47; }
	inline void set_BuildingConstructionWindowInstance_47(GameObject_t1113636619 * value)
	{
		___BuildingConstructionWindowInstance_47 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingConstructionWindowInstance_47), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindowInstance_48() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindowInstance_48)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindowInstance_48() const { return ___BuildingInformationWindowInstance_48; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindowInstance_48() { return &___BuildingInformationWindowInstance_48; }
	inline void set_BuildingInformationWindowInstance_48(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindowInstance_48 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindowInstance_48), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindowInstance2_49() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindowInstance2_49)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindowInstance2_49() const { return ___BuildingInformationWindowInstance2_49; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindowInstance2_49() { return &___BuildingInformationWindowInstance2_49; }
	inline void set_BuildingInformationWindowInstance2_49(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindowInstance2_49 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindowInstance2_49), value);
	}

	inline static int32_t get_offset_of_ReportContainerWindowInstance_50() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ReportContainerWindowInstance_50)); }
	inline GameObject_t1113636619 * get_ReportContainerWindowInstance_50() const { return ___ReportContainerWindowInstance_50; }
	inline GameObject_t1113636619 ** get_address_of_ReportContainerWindowInstance_50() { return &___ReportContainerWindowInstance_50; }
	inline void set_ReportContainerWindowInstance_50(GameObject_t1113636619 * value)
	{
		___ReportContainerWindowInstance_50 = value;
		Il2CppCodeGenWriteBarrier((&___ReportContainerWindowInstance_50), value);
	}

	inline static int32_t get_offset_of_ShopWindowInstance_51() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShopWindowInstance_51)); }
	inline GameObject_t1113636619 * get_ShopWindowInstance_51() const { return ___ShopWindowInstance_51; }
	inline GameObject_t1113636619 ** get_address_of_ShopWindowInstance_51() { return &___ShopWindowInstance_51; }
	inline void set_ShopWindowInstance_51(GameObject_t1113636619 * value)
	{
		___ShopWindowInstance_51 = value;
		Il2CppCodeGenWriteBarrier((&___ShopWindowInstance_51), value);
	}

	inline static int32_t get_offset_of_MailWindowInstance_52() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MailWindowInstance_52)); }
	inline GameObject_t1113636619 * get_MailWindowInstance_52() const { return ___MailWindowInstance_52; }
	inline GameObject_t1113636619 ** get_address_of_MailWindowInstance_52() { return &___MailWindowInstance_52; }
	inline void set_MailWindowInstance_52(GameObject_t1113636619 * value)
	{
		___MailWindowInstance_52 = value;
		Il2CppCodeGenWriteBarrier((&___MailWindowInstance_52), value);
	}

	inline static int32_t get_offset_of_AlliancesWindowInstance_53() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AlliancesWindowInstance_53)); }
	inline GameObject_t1113636619 * get_AlliancesWindowInstance_53() const { return ___AlliancesWindowInstance_53; }
	inline GameObject_t1113636619 ** get_address_of_AlliancesWindowInstance_53() { return &___AlliancesWindowInstance_53; }
	inline void set_AlliancesWindowInstance_53(GameObject_t1113636619 * value)
	{
		___AlliancesWindowInstance_53 = value;
		Il2CppCodeGenWriteBarrier((&___AlliancesWindowInstance_53), value);
	}

	inline static int32_t get_offset_of_UserInfoWindowInstance_54() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UserInfoWindowInstance_54)); }
	inline GameObject_t1113636619 * get_UserInfoWindowInstance_54() const { return ___UserInfoWindowInstance_54; }
	inline GameObject_t1113636619 ** get_address_of_UserInfoWindowInstance_54() { return &___UserInfoWindowInstance_54; }
	inline void set_UserInfoWindowInstance_54(GameObject_t1113636619 * value)
	{
		___UserInfoWindowInstance_54 = value;
		Il2CppCodeGenWriteBarrier((&___UserInfoWindowInstance_54), value);
	}

	inline static int32_t get_offset_of_FinanceWindowInstance_55() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___FinanceWindowInstance_55)); }
	inline GameObject_t1113636619 * get_FinanceWindowInstance_55() const { return ___FinanceWindowInstance_55; }
	inline GameObject_t1113636619 ** get_address_of_FinanceWindowInstance_55() { return &___FinanceWindowInstance_55; }
	inline void set_FinanceWindowInstance_55(GameObject_t1113636619 * value)
	{
		___FinanceWindowInstance_55 = value;
		Il2CppCodeGenWriteBarrier((&___FinanceWindowInstance_55), value);
	}

	inline static int32_t get_offset_of_HelpWindowInstance_56() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___HelpWindowInstance_56)); }
	inline GameObject_t1113636619 * get_HelpWindowInstance_56() const { return ___HelpWindowInstance_56; }
	inline GameObject_t1113636619 ** get_address_of_HelpWindowInstance_56() { return &___HelpWindowInstance_56; }
	inline void set_HelpWindowInstance_56(GameObject_t1113636619 * value)
	{
		___HelpWindowInstance_56 = value;
		Il2CppCodeGenWriteBarrier((&___HelpWindowInstance_56), value);
	}

	inline static int32_t get_offset_of_NewMailWindowInstance_57() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewMailWindowInstance_57)); }
	inline GameObject_t1113636619 * get_NewMailWindowInstance_57() const { return ___NewMailWindowInstance_57; }
	inline GameObject_t1113636619 ** get_address_of_NewMailWindowInstance_57() { return &___NewMailWindowInstance_57; }
	inline void set_NewMailWindowInstance_57(GameObject_t1113636619 * value)
	{
		___NewMailWindowInstance_57 = value;
		Il2CppCodeGenWriteBarrier((&___NewMailWindowInstance_57), value);
	}

	inline static int32_t get_offset_of_MessageWindowInstance_58() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageWindowInstance_58)); }
	inline GameObject_t1113636619 * get_MessageWindowInstance_58() const { return ___MessageWindowInstance_58; }
	inline GameObject_t1113636619 ** get_address_of_MessageWindowInstance_58() { return &___MessageWindowInstance_58; }
	inline void set_MessageWindowInstance_58(GameObject_t1113636619 * value)
	{
		___MessageWindowInstance_58 = value;
		Il2CppCodeGenWriteBarrier((&___MessageWindowInstance_58), value);
	}

	inline static int32_t get_offset_of_MessageAllianceMemberWindowInstance_59() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageAllianceMemberWindowInstance_59)); }
	inline GameObject_t1113636619 * get_MessageAllianceMemberWindowInstance_59() const { return ___MessageAllianceMemberWindowInstance_59; }
	inline GameObject_t1113636619 ** get_address_of_MessageAllianceMemberWindowInstance_59() { return &___MessageAllianceMemberWindowInstance_59; }
	inline void set_MessageAllianceMemberWindowInstance_59(GameObject_t1113636619 * value)
	{
		___MessageAllianceMemberWindowInstance_59 = value;
		Il2CppCodeGenWriteBarrier((&___MessageAllianceMemberWindowInstance_59), value);
	}

	inline static int32_t get_offset_of_EventsWindowInstance_60() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___EventsWindowInstance_60)); }
	inline GameObject_t1113636619 * get_EventsWindowInstance_60() const { return ___EventsWindowInstance_60; }
	inline GameObject_t1113636619 ** get_address_of_EventsWindowInstance_60() { return &___EventsWindowInstance_60; }
	inline void set_EventsWindowInstance_60(GameObject_t1113636619 * value)
	{
		___EventsWindowInstance_60 = value;
		Il2CppCodeGenWriteBarrier((&___EventsWindowInstance_60), value);
	}

	inline static int32_t get_offset_of_AllianceInfoWindowInstance_61() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AllianceInfoWindowInstance_61)); }
	inline GameObject_t1113636619 * get_AllianceInfoWindowInstance_61() const { return ___AllianceInfoWindowInstance_61; }
	inline GameObject_t1113636619 ** get_address_of_AllianceInfoWindowInstance_61() { return &___AllianceInfoWindowInstance_61; }
	inline void set_AllianceInfoWindowInstance_61(GameObject_t1113636619 * value)
	{
		___AllianceInfoWindowInstance_61 = value;
		Il2CppCodeGenWriteBarrier((&___AllianceInfoWindowInstance_61), value);
	}

	inline static int32_t get_offset_of_UnitDetailsWindowInstance_62() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UnitDetailsWindowInstance_62)); }
	inline GameObject_t1113636619 * get_UnitDetailsWindowInstance_62() const { return ___UnitDetailsWindowInstance_62; }
	inline GameObject_t1113636619 ** get_address_of_UnitDetailsWindowInstance_62() { return &___UnitDetailsWindowInstance_62; }
	inline void set_UnitDetailsWindowInstance_62(GameObject_t1113636619 * value)
	{
		___UnitDetailsWindowInstance_62 = value;
		Il2CppCodeGenWriteBarrier((&___UnitDetailsWindowInstance_62), value);
	}

	inline static int32_t get_offset_of_CityInfoWindowInstance_63() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityInfoWindowInstance_63)); }
	inline GameObject_t1113636619 * get_CityInfoWindowInstance_63() const { return ___CityInfoWindowInstance_63; }
	inline GameObject_t1113636619 ** get_address_of_CityInfoWindowInstance_63() { return &___CityInfoWindowInstance_63; }
	inline void set_CityInfoWindowInstance_63(GameObject_t1113636619 * value)
	{
		___CityInfoWindowInstance_63 = value;
		Il2CppCodeGenWriteBarrier((&___CityInfoWindowInstance_63), value);
	}

	inline static int32_t get_offset_of_DispatchWindowInstance_64() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DispatchWindowInstance_64)); }
	inline GameObject_t1113636619 * get_DispatchWindowInstance_64() const { return ___DispatchWindowInstance_64; }
	inline GameObject_t1113636619 ** get_address_of_DispatchWindowInstance_64() { return &___DispatchWindowInstance_64; }
	inline void set_DispatchWindowInstance_64(GameObject_t1113636619 * value)
	{
		___DispatchWindowInstance_64 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchWindowInstance_64), value);
	}

	inline static int32_t get_offset_of_RecruitBarbariansWindowInstance_65() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___RecruitBarbariansWindowInstance_65)); }
	inline GameObject_t1113636619 * get_RecruitBarbariansWindowInstance_65() const { return ___RecruitBarbariansWindowInstance_65; }
	inline GameObject_t1113636619 ** get_address_of_RecruitBarbariansWindowInstance_65() { return &___RecruitBarbariansWindowInstance_65; }
	inline void set_RecruitBarbariansWindowInstance_65(GameObject_t1113636619 * value)
	{
		___RecruitBarbariansWindowInstance_65 = value;
		Il2CppCodeGenWriteBarrier((&___RecruitBarbariansWindowInstance_65), value);
	}

	inline static int32_t get_offset_of_ValleyInfoWindowInstance_66() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ValleyInfoWindowInstance_66)); }
	inline GameObject_t1113636619 * get_ValleyInfoWindowInstance_66() const { return ___ValleyInfoWindowInstance_66; }
	inline GameObject_t1113636619 ** get_address_of_ValleyInfoWindowInstance_66() { return &___ValleyInfoWindowInstance_66; }
	inline void set_ValleyInfoWindowInstance_66(GameObject_t1113636619 * value)
	{
		___ValleyInfoWindowInstance_66 = value;
		Il2CppCodeGenWriteBarrier((&___ValleyInfoWindowInstance_66), value);
	}

	inline static int32_t get_offset_of_NotificationWindowInstance_67() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NotificationWindowInstance_67)); }
	inline GameObject_t1113636619 * get_NotificationWindowInstance_67() const { return ___NotificationWindowInstance_67; }
	inline GameObject_t1113636619 ** get_address_of_NotificationWindowInstance_67() { return &___NotificationWindowInstance_67; }
	inline void set_NotificationWindowInstance_67(GameObject_t1113636619 * value)
	{
		___NotificationWindowInstance_67 = value;
		Il2CppCodeGenWriteBarrier((&___NotificationWindowInstance_67), value);
	}

	inline static int32_t get_offset_of_LoginProgressBarInstance_68() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___LoginProgressBarInstance_68)); }
	inline GameObject_t1113636619 * get_LoginProgressBarInstance_68() const { return ___LoginProgressBarInstance_68; }
	inline GameObject_t1113636619 ** get_address_of_LoginProgressBarInstance_68() { return &___LoginProgressBarInstance_68; }
	inline void set_LoginProgressBarInstance_68(GameObject_t1113636619 * value)
	{
		___LoginProgressBarInstance_68 = value;
		Il2CppCodeGenWriteBarrier((&___LoginProgressBarInstance_68), value);
	}

	inline static int32_t get_offset_of_KnightInfoWindowInstance_69() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___KnightInfoWindowInstance_69)); }
	inline GameObject_t1113636619 * get_KnightInfoWindowInstance_69() const { return ___KnightInfoWindowInstance_69; }
	inline GameObject_t1113636619 ** get_address_of_KnightInfoWindowInstance_69() { return &___KnightInfoWindowInstance_69; }
	inline void set_KnightInfoWindowInstance_69(GameObject_t1113636619 * value)
	{
		___KnightInfoWindowInstance_69 = value;
		Il2CppCodeGenWriteBarrier((&___KnightInfoWindowInstance_69), value);
	}

	inline static int32_t get_offset_of_BuyItemWindowInstance_70() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyItemWindowInstance_70)); }
	inline GameObject_t1113636619 * get_BuyItemWindowInstance_70() const { return ___BuyItemWindowInstance_70; }
	inline GameObject_t1113636619 ** get_address_of_BuyItemWindowInstance_70() { return &___BuyItemWindowInstance_70; }
	inline void set_BuyItemWindowInstance_70(GameObject_t1113636619 * value)
	{
		___BuyItemWindowInstance_70 = value;
		Il2CppCodeGenWriteBarrier((&___BuyItemWindowInstance_70), value);
	}

	inline static int32_t get_offset_of_SellItemWindowInstance_71() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SellItemWindowInstance_71)); }
	inline GameObject_t1113636619 * get_SellItemWindowInstance_71() const { return ___SellItemWindowInstance_71; }
	inline GameObject_t1113636619 ** get_address_of_SellItemWindowInstance_71() { return &___SellItemWindowInstance_71; }
	inline void set_SellItemWindowInstance_71(GameObject_t1113636619 * value)
	{
		___SellItemWindowInstance_71 = value;
		Il2CppCodeGenWriteBarrier((&___SellItemWindowInstance_71), value);
	}

	inline static int32_t get_offset_of_SpeedUpWindowInstance_72() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SpeedUpWindowInstance_72)); }
	inline GameObject_t1113636619 * get_SpeedUpWindowInstance_72() const { return ___SpeedUpWindowInstance_72; }
	inline GameObject_t1113636619 ** get_address_of_SpeedUpWindowInstance_72() { return &___SpeedUpWindowInstance_72; }
	inline void set_SpeedUpWindowInstance_72(GameObject_t1113636619 * value)
	{
		___SpeedUpWindowInstance_72 = value;
		Il2CppCodeGenWriteBarrier((&___SpeedUpWindowInstance_72), value);
	}

	inline static int32_t get_offset_of_UpgradeBuildingWindowInstance_73() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpgradeBuildingWindowInstance_73)); }
	inline GameObject_t1113636619 * get_UpgradeBuildingWindowInstance_73() const { return ___UpgradeBuildingWindowInstance_73; }
	inline GameObject_t1113636619 ** get_address_of_UpgradeBuildingWindowInstance_73() { return &___UpgradeBuildingWindowInstance_73; }
	inline void set_UpgradeBuildingWindowInstance_73(GameObject_t1113636619 * value)
	{
		___UpgradeBuildingWindowInstance_73 = value;
		Il2CppCodeGenWriteBarrier((&___UpgradeBuildingWindowInstance_73), value);
	}

	inline static int32_t get_offset_of_AdvencedTeleportWindowInstance_74() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AdvencedTeleportWindowInstance_74)); }
	inline GameObject_t1113636619 * get_AdvencedTeleportWindowInstance_74() const { return ___AdvencedTeleportWindowInstance_74; }
	inline GameObject_t1113636619 ** get_address_of_AdvencedTeleportWindowInstance_74() { return &___AdvencedTeleportWindowInstance_74; }
	inline void set_AdvencedTeleportWindowInstance_74(GameObject_t1113636619 * value)
	{
		___AdvencedTeleportWindowInstance_74 = value;
		Il2CppCodeGenWriteBarrier((&___AdvencedTeleportWindowInstance_74), value);
	}

	inline static int32_t get_offset_of_CityDeedWindowInstance_75() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityDeedWindowInstance_75)); }
	inline GameObject_t1113636619 * get_CityDeedWindowInstance_75() const { return ___CityDeedWindowInstance_75; }
	inline GameObject_t1113636619 ** get_address_of_CityDeedWindowInstance_75() { return &___CityDeedWindowInstance_75; }
	inline void set_CityDeedWindowInstance_75(GameObject_t1113636619 * value)
	{
		___CityDeedWindowInstance_75 = value;
		Il2CppCodeGenWriteBarrier((&___CityDeedWindowInstance_75), value);
	}

	inline static int32_t get_offset_of_BuyShillingsWindowInstance_76() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyShillingsWindowInstance_76)); }
	inline GameObject_t1113636619 * get_BuyShillingsWindowInstance_76() const { return ___BuyShillingsWindowInstance_76; }
	inline GameObject_t1113636619 ** get_address_of_BuyShillingsWindowInstance_76() { return &___BuyShillingsWindowInstance_76; }
	inline void set_BuyShillingsWindowInstance_76(GameObject_t1113636619 * value)
	{
		___BuyShillingsWindowInstance_76 = value;
		Il2CppCodeGenWriteBarrier((&___BuyShillingsWindowInstance_76), value);
	}

	inline static int32_t get_offset_of_ChangeLanguageWindowInstance_77() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangeLanguageWindowInstance_77)); }
	inline GameObject_t1113636619 * get_ChangeLanguageWindowInstance_77() const { return ___ChangeLanguageWindowInstance_77; }
	inline GameObject_t1113636619 ** get_address_of_ChangeLanguageWindowInstance_77() { return &___ChangeLanguageWindowInstance_77; }
	inline void set_ChangeLanguageWindowInstance_77(GameObject_t1113636619 * value)
	{
		___ChangeLanguageWindowInstance_77 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeLanguageWindowInstance_77), value);
	}

	inline static int32_t get_offset_of_DismissWindowInstance_78() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DismissWindowInstance_78)); }
	inline GameObject_t1113636619 * get_DismissWindowInstance_78() const { return ___DismissWindowInstance_78; }
	inline GameObject_t1113636619 ** get_address_of_DismissWindowInstance_78() { return &___DismissWindowInstance_78; }
	inline void set_DismissWindowInstance_78(GameObject_t1113636619 * value)
	{
		___DismissWindowInstance_78 = value;
		Il2CppCodeGenWriteBarrier((&___DismissWindowInstance_78), value);
	}

	inline static int32_t get_offset_of_ChangePasswordWindowInstance_79() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangePasswordWindowInstance_79)); }
	inline GameObject_t1113636619 * get_ChangePasswordWindowInstance_79() const { return ___ChangePasswordWindowInstance_79; }
	inline GameObject_t1113636619 ** get_address_of_ChangePasswordWindowInstance_79() { return &___ChangePasswordWindowInstance_79; }
	inline void set_ChangePasswordWindowInstance_79(GameObject_t1113636619 * value)
	{
		___ChangePasswordWindowInstance_79 = value;
		Il2CppCodeGenWriteBarrier((&___ChangePasswordWindowInstance_79), value);
	}

	inline static int32_t get_offset_of_TributesWindowInstance_80() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TributesWindowInstance_80)); }
	inline GameObject_t1113636619 * get_TributesWindowInstance_80() const { return ___TributesWindowInstance_80; }
	inline GameObject_t1113636619 ** get_address_of_TributesWindowInstance_80() { return &___TributesWindowInstance_80; }
	inline void set_TributesWindowInstance_80(GameObject_t1113636619 * value)
	{
		___TributesWindowInstance_80 = value;
		Il2CppCodeGenWriteBarrier((&___TributesWindowInstance_80), value);
	}

	inline static int32_t get_offset_of_UpdateBuildingWindowInstance_81() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpdateBuildingWindowInstance_81)); }
	inline GameObject_t1113636619 * get_UpdateBuildingWindowInstance_81() const { return ___UpdateBuildingWindowInstance_81; }
	inline GameObject_t1113636619 ** get_address_of_UpdateBuildingWindowInstance_81() { return &___UpdateBuildingWindowInstance_81; }
	inline void set_UpdateBuildingWindowInstance_81(GameObject_t1113636619 * value)
	{
		___UpdateBuildingWindowInstance_81 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateBuildingWindowInstance_81), value);
	}

	inline static int32_t get_offset_of_BattleLogWindowInstance_82() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BattleLogWindowInstance_82)); }
	inline GameObject_t1113636619 * get_BattleLogWindowInstance_82() const { return ___BattleLogWindowInstance_82; }
	inline GameObject_t1113636619 ** get_address_of_BattleLogWindowInstance_82() { return &___BattleLogWindowInstance_82; }
	inline void set_BattleLogWindowInstance_82(GameObject_t1113636619 * value)
	{
		___BattleLogWindowInstance_82 = value;
		Il2CppCodeGenWriteBarrier((&___BattleLogWindowInstance_82), value);
	}

	inline static int32_t get_offset_of_BuildNewCityWindowInstance_83() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildNewCityWindowInstance_83)); }
	inline GameObject_t1113636619 * get_BuildNewCityWindowInstance_83() const { return ___BuildNewCityWindowInstance_83; }
	inline GameObject_t1113636619 ** get_address_of_BuildNewCityWindowInstance_83() { return &___BuildNewCityWindowInstance_83; }
	inline void set_BuildNewCityWindowInstance_83(GameObject_t1113636619 * value)
	{
		___BuildNewCityWindowInstance_83 = value;
		Il2CppCodeGenWriteBarrier((&___BuildNewCityWindowInstance_83), value);
	}

	inline static int32_t get_offset_of_ArmyDetailsWindowInstance_84() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ArmyDetailsWindowInstance_84)); }
	inline GameObject_t1113636619 * get_ArmyDetailsWindowInstance_84() const { return ___ArmyDetailsWindowInstance_84; }
	inline GameObject_t1113636619 ** get_address_of_ArmyDetailsWindowInstance_84() { return &___ArmyDetailsWindowInstance_84; }
	inline void set_ArmyDetailsWindowInstance_84(GameObject_t1113636619 * value)
	{
		___ArmyDetailsWindowInstance_84 = value;
		Il2CppCodeGenWriteBarrier((&___ArmyDetailsWindowInstance_84), value);
	}

	inline static int32_t get_offset_of_ShillingsWebViewWindowInstance_85() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShillingsWebViewWindowInstance_85)); }
	inline GameObject_t1113636619 * get_ShillingsWebViewWindowInstance_85() const { return ___ShillingsWebViewWindowInstance_85; }
	inline GameObject_t1113636619 ** get_address_of_ShillingsWebViewWindowInstance_85() { return &___ShillingsWebViewWindowInstance_85; }
	inline void set_ShillingsWebViewWindowInstance_85(GameObject_t1113636619 * value)
	{
		___ShillingsWebViewWindowInstance_85 = value;
		Il2CppCodeGenWriteBarrier((&___ShillingsWebViewWindowInstance_85), value);
	}

	inline static int32_t get_offset_of_NewAllianceNameWindowInstance_86() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewAllianceNameWindowInstance_86)); }
	inline GameObject_t1113636619 * get_NewAllianceNameWindowInstance_86() const { return ___NewAllianceNameWindowInstance_86; }
	inline GameObject_t1113636619 ** get_address_of_NewAllianceNameWindowInstance_86() { return &___NewAllianceNameWindowInstance_86; }
	inline void set_NewAllianceNameWindowInstance_86(GameObject_t1113636619 * value)
	{
		___NewAllianceNameWindowInstance_86 = value;
		Il2CppCodeGenWriteBarrier((&___NewAllianceNameWindowInstance_86), value);
	}

	inline static int32_t get_offset_of_ConfirmationWindowInstance_87() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ConfirmationWindowInstance_87)); }
	inline GameObject_t1113636619 * get_ConfirmationWindowInstance_87() const { return ___ConfirmationWindowInstance_87; }
	inline GameObject_t1113636619 ** get_address_of_ConfirmationWindowInstance_87() { return &___ConfirmationWindowInstance_87; }
	inline void set_ConfirmationWindowInstance_87(GameObject_t1113636619 * value)
	{
		___ConfirmationWindowInstance_87 = value;
		Il2CppCodeGenWriteBarrier((&___ConfirmationWindowInstance_87), value);
	}

	inline static int32_t get_offset_of_ShieldTimeWindowInstance_88() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShieldTimeWindowInstance_88)); }
	inline GameObject_t1113636619 * get_ShieldTimeWindowInstance_88() const { return ___ShieldTimeWindowInstance_88; }
	inline GameObject_t1113636619 ** get_address_of_ShieldTimeWindowInstance_88() { return &___ShieldTimeWindowInstance_88; }
	inline void set_ShieldTimeWindowInstance_88(GameObject_t1113636619 * value)
	{
		___ShieldTimeWindowInstance_88 = value;
		Il2CppCodeGenWriteBarrier((&___ShieldTimeWindowInstance_88), value);
	}

	inline static int32_t get_offset_of_TutorialWindowInstance_89() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TutorialWindowInstance_89)); }
	inline GameObject_t1113636619 * get_TutorialWindowInstance_89() const { return ___TutorialWindowInstance_89; }
	inline GameObject_t1113636619 ** get_address_of_TutorialWindowInstance_89() { return &___TutorialWindowInstance_89; }
	inline void set_TutorialWindowInstance_89(GameObject_t1113636619 * value)
	{
		___TutorialWindowInstance_89 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialWindowInstance_89), value);
	}

	inline static int32_t get_offset_of_VideoAdsWindowInstance_90() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___VideoAdsWindowInstance_90)); }
	inline GameObject_t1113636619 * get_VideoAdsWindowInstance_90() const { return ___VideoAdsWindowInstance_90; }
	inline GameObject_t1113636619 ** get_address_of_VideoAdsWindowInstance_90() { return &___VideoAdsWindowInstance_90; }
	inline void set_VideoAdsWindowInstance_90(GameObject_t1113636619 * value)
	{
		___VideoAdsWindowInstance_90 = value;
		Il2CppCodeGenWriteBarrier((&___VideoAdsWindowInstance_90), value);
	}

	inline static int32_t get_offset_of_overviewPanel_91() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___overviewPanel_91)); }
	inline GameObject_t1113636619 * get_overviewPanel_91() const { return ___overviewPanel_91; }
	inline GameObject_t1113636619 ** get_address_of_overviewPanel_91() { return &___overviewPanel_91; }
	inline void set_overviewPanel_91(GameObject_t1113636619 * value)
	{
		___overviewPanel_91 = value;
		Il2CppCodeGenWriteBarrier((&___overviewPanel_91), value);
	}

	inline static int32_t get_offset_of_statisticsPanel_92() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___statisticsPanel_92)); }
	inline GameObject_t1113636619 * get_statisticsPanel_92() const { return ___statisticsPanel_92; }
	inline GameObject_t1113636619 ** get_address_of_statisticsPanel_92() { return &___statisticsPanel_92; }
	inline void set_statisticsPanel_92(GameObject_t1113636619 * value)
	{
		___statisticsPanel_92 = value;
		Il2CppCodeGenWriteBarrier((&___statisticsPanel_92), value);
	}

	inline static int32_t get_offset_of_actionPanel_93() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___actionPanel_93)); }
	inline GameObject_t1113636619 * get_actionPanel_93() const { return ___actionPanel_93; }
	inline GameObject_t1113636619 ** get_address_of_actionPanel_93() { return &___actionPanel_93; }
	inline void set_actionPanel_93(GameObject_t1113636619 * value)
	{
		___actionPanel_93 = value;
		Il2CppCodeGenWriteBarrier((&___actionPanel_93), value);
	}

	inline static int32_t get_offset_of_actionManager_94() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___actionManager_94)); }
	inline ActionManager_t2430268190 * get_actionManager_94() const { return ___actionManager_94; }
	inline ActionManager_t2430268190 ** get_address_of_actionManager_94() { return &___actionManager_94; }
	inline void set_actionManager_94(ActionManager_t2430268190 * value)
	{
		___actionManager_94 = value;
		Il2CppCodeGenWriteBarrier((&___actionManager_94), value);
	}

	inline static int32_t get_offset_of_allianceManager_95() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___allianceManager_95)); }
	inline AllianceContentChanger_t2140085977 * get_allianceManager_95() const { return ___allianceManager_95; }
	inline AllianceContentChanger_t2140085977 ** get_address_of_allianceManager_95() { return &___allianceManager_95; }
	inline void set_allianceManager_95(AllianceContentChanger_t2140085977 * value)
	{
		___allianceManager_95 = value;
		Il2CppCodeGenWriteBarrier((&___allianceManager_95), value);
	}

	inline static int32_t get_offset_of_treasureManager_96() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___treasureManager_96)); }
	inline TreasureManager_t110095690 * get_treasureManager_96() const { return ___treasureManager_96; }
	inline TreasureManager_t110095690 ** get_address_of_treasureManager_96() { return &___treasureManager_96; }
	inline void set_treasureManager_96(TreasureManager_t110095690 * value)
	{
		___treasureManager_96 = value;
		Il2CppCodeGenWriteBarrier((&___treasureManager_96), value);
	}

	inline static int32_t get_offset_of_oldDesignPanel_97() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___oldDesignPanel_97)); }
	inline GameObject_t1113636619 * get_oldDesignPanel_97() const { return ___oldDesignPanel_97; }
	inline GameObject_t1113636619 ** get_address_of_oldDesignPanel_97() { return &___oldDesignPanel_97; }
	inline void set_oldDesignPanel_97(GameObject_t1113636619 * value)
	{
		___oldDesignPanel_97 = value;
		Il2CppCodeGenWriteBarrier((&___oldDesignPanel_97), value);
	}

	inline static int32_t get_offset_of_newDesignPanel_98() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___newDesignPanel_98)); }
	inline GameObject_t1113636619 * get_newDesignPanel_98() const { return ___newDesignPanel_98; }
	inline GameObject_t1113636619 ** get_address_of_newDesignPanel_98() { return &___newDesignPanel_98; }
	inline void set_newDesignPanel_98(GameObject_t1113636619 * value)
	{
		___newDesignPanel_98 = value;
		Il2CppCodeGenWriteBarrier((&___newDesignPanel_98), value);
	}

	inline static int32_t get_offset_of_viewPanel_99() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewPanel_99)); }
	inline GameObject_t1113636619 * get_viewPanel_99() const { return ___viewPanel_99; }
	inline GameObject_t1113636619 ** get_address_of_viewPanel_99() { return &___viewPanel_99; }
	inline void set_viewPanel_99(GameObject_t1113636619 * value)
	{
		___viewPanel_99 = value;
		Il2CppCodeGenWriteBarrier((&___viewPanel_99), value);
	}

	inline static int32_t get_offset_of_itemPanel_100() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemPanel_100)); }
	inline GameObject_t1113636619 * get_itemPanel_100() const { return ___itemPanel_100; }
	inline GameObject_t1113636619 ** get_address_of_itemPanel_100() { return &___itemPanel_100; }
	inline void set_itemPanel_100(GameObject_t1113636619 * value)
	{
		___itemPanel_100 = value;
		Il2CppCodeGenWriteBarrier((&___itemPanel_100), value);
	}

	inline static int32_t get_offset_of_citiesPanel_101() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___citiesPanel_101)); }
	inline GameObject_t1113636619 * get_citiesPanel_101() const { return ___citiesPanel_101; }
	inline GameObject_t1113636619 ** get_address_of_citiesPanel_101() { return &___citiesPanel_101; }
	inline void set_citiesPanel_101(GameObject_t1113636619 * value)
	{
		___citiesPanel_101 = value;
		Il2CppCodeGenWriteBarrier((&___citiesPanel_101), value);
	}

	inline static int32_t get_offset_of_viewButton_102() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewButton_102)); }
	inline Image_t2670269651 * get_viewButton_102() const { return ___viewButton_102; }
	inline Image_t2670269651 ** get_address_of_viewButton_102() { return &___viewButton_102; }
	inline void set_viewButton_102(Image_t2670269651 * value)
	{
		___viewButton_102 = value;
		Il2CppCodeGenWriteBarrier((&___viewButton_102), value);
	}

	inline static int32_t get_offset_of_itemButton_103() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemButton_103)); }
	inline Image_t2670269651 * get_itemButton_103() const { return ___itemButton_103; }
	inline Image_t2670269651 ** get_address_of_itemButton_103() { return &___itemButton_103; }
	inline void set_itemButton_103(Image_t2670269651 * value)
	{
		___itemButton_103 = value;
		Il2CppCodeGenWriteBarrier((&___itemButton_103), value);
	}

	inline static int32_t get_offset_of_voiceButton_104() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceButton_104)); }
	inline Image_t2670269651 * get_voiceButton_104() const { return ___voiceButton_104; }
	inline Image_t2670269651 ** get_address_of_voiceButton_104() { return &___voiceButton_104; }
	inline void set_voiceButton_104(Image_t2670269651 * value)
	{
		___voiceButton_104 = value;
		Il2CppCodeGenWriteBarrier((&___voiceButton_104), value);
	}

	inline static int32_t get_offset_of_viewClosedSprite_105() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewClosedSprite_105)); }
	inline Sprite_t280657092 * get_viewClosedSprite_105() const { return ___viewClosedSprite_105; }
	inline Sprite_t280657092 ** get_address_of_viewClosedSprite_105() { return &___viewClosedSprite_105; }
	inline void set_viewClosedSprite_105(Sprite_t280657092 * value)
	{
		___viewClosedSprite_105 = value;
		Il2CppCodeGenWriteBarrier((&___viewClosedSprite_105), value);
	}

	inline static int32_t get_offset_of_viewOpenedSprite_106() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewOpenedSprite_106)); }
	inline Sprite_t280657092 * get_viewOpenedSprite_106() const { return ___viewOpenedSprite_106; }
	inline Sprite_t280657092 ** get_address_of_viewOpenedSprite_106() { return &___viewOpenedSprite_106; }
	inline void set_viewOpenedSprite_106(Sprite_t280657092 * value)
	{
		___viewOpenedSprite_106 = value;
		Il2CppCodeGenWriteBarrier((&___viewOpenedSprite_106), value);
	}

	inline static int32_t get_offset_of_itemClosedSprite_107() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemClosedSprite_107)); }
	inline Sprite_t280657092 * get_itemClosedSprite_107() const { return ___itemClosedSprite_107; }
	inline Sprite_t280657092 ** get_address_of_itemClosedSprite_107() { return &___itemClosedSprite_107; }
	inline void set_itemClosedSprite_107(Sprite_t280657092 * value)
	{
		___itemClosedSprite_107 = value;
		Il2CppCodeGenWriteBarrier((&___itemClosedSprite_107), value);
	}

	inline static int32_t get_offset_of_itemOpenedSprite_108() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemOpenedSprite_108)); }
	inline Sprite_t280657092 * get_itemOpenedSprite_108() const { return ___itemOpenedSprite_108; }
	inline Sprite_t280657092 ** get_address_of_itemOpenedSprite_108() { return &___itemOpenedSprite_108; }
	inline void set_itemOpenedSprite_108(Sprite_t280657092 * value)
	{
		___itemOpenedSprite_108 = value;
		Il2CppCodeGenWriteBarrier((&___itemOpenedSprite_108), value);
	}

	inline static int32_t get_offset_of_voiceOnSprite_109() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceOnSprite_109)); }
	inline Sprite_t280657092 * get_voiceOnSprite_109() const { return ___voiceOnSprite_109; }
	inline Sprite_t280657092 ** get_address_of_voiceOnSprite_109() { return &___voiceOnSprite_109; }
	inline void set_voiceOnSprite_109(Sprite_t280657092 * value)
	{
		___voiceOnSprite_109 = value;
		Il2CppCodeGenWriteBarrier((&___voiceOnSprite_109), value);
	}

	inline static int32_t get_offset_of_voiceOffSprite_110() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceOffSprite_110)); }
	inline Sprite_t280657092 * get_voiceOffSprite_110() const { return ___voiceOffSprite_110; }
	inline Sprite_t280657092 ** get_address_of_voiceOffSprite_110() { return &___voiceOffSprite_110; }
	inline void set_voiceOffSprite_110(Sprite_t280657092 * value)
	{
		___voiceOffSprite_110 = value;
		Il2CppCodeGenWriteBarrier((&___voiceOffSprite_110), value);
	}

	inline static int32_t get_offset_of_UI_111() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UI_111)); }
	inline GameObject_t1113636619 * get_UI_111() const { return ___UI_111; }
	inline GameObject_t1113636619 ** get_address_of_UI_111() { return &___UI_111; }
	inline void set_UI_111(GameObject_t1113636619 * value)
	{
		___UI_111 = value;
		Il2CppCodeGenWriteBarrier((&___UI_111), value);
	}

	inline static int32_t get_offset_of_Shell_112() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___Shell_112)); }
	inline GameObject_t1113636619 * get_Shell_112() const { return ___Shell_112; }
	inline GameObject_t1113636619 ** get_address_of_Shell_112() { return &___Shell_112; }
	inline void set_Shell_112(GameObject_t1113636619 * value)
	{
		___Shell_112 = value;
		Il2CppCodeGenWriteBarrier((&___Shell_112), value);
	}
};

struct WindowInstanceManager_t3234774687_StaticFields
{
public:
	// WindowInstanceManager WindowInstanceManager::instance
	WindowInstanceManager_t3234774687 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687_StaticFields, ___instance_2)); }
	inline WindowInstanceManager_t3234774687 * get_instance_2() const { return ___instance_2; }
	inline WindowInstanceManager_t3234774687 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(WindowInstanceManager_t3234774687 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWINSTANCEMANAGER_T3234774687_H
#ifndef TEXTLOCALIZER_T4021138871_H
#define TEXTLOCALIZER_T4021138871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextLocalizer
struct  TextLocalizer_t4021138871  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TextLocalizer::uppercase
	bool ___uppercase_2;
	// System.String TextLocalizer::key
	String_t* ___key_3;
	// System.String TextLocalizer::appendText
	String_t* ___appendText_4;

public:
	inline static int32_t get_offset_of_uppercase_2() { return static_cast<int32_t>(offsetof(TextLocalizer_t4021138871, ___uppercase_2)); }
	inline bool get_uppercase_2() const { return ___uppercase_2; }
	inline bool* get_address_of_uppercase_2() { return &___uppercase_2; }
	inline void set_uppercase_2(bool value)
	{
		___uppercase_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(TextLocalizer_t4021138871, ___key_3)); }
	inline String_t* get_key_3() const { return ___key_3; }
	inline String_t** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(String_t* value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier((&___key_3), value);
	}

	inline static int32_t get_offset_of_appendText_4() { return static_cast<int32_t>(offsetof(TextLocalizer_t4021138871, ___appendText_4)); }
	inline String_t* get_appendText_4() const { return ___appendText_4; }
	inline String_t** get_address_of_appendText_4() { return &___appendText_4; }
	inline void set_appendText_4(String_t* value)
	{
		___appendText_4 = value;
		Il2CppCodeGenWriteBarrier((&___appendText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTLOCALIZER_T4021138871_H
#ifndef VALLEYINFOMANAGER_T2327472454_H
#define VALLEYINFOMANAGER_T2327472454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ValleyInfoManager
struct  ValleyInfoManager_t2327472454  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ValleyInfoManager::valleyName
	Text_t1901882714 * ___valleyName_2;
	// UnityEngine.UI.Text ValleyInfoManager::product
	Text_t1901882714 * ___product_3;
	// UnityEngine.UI.Text ValleyInfoManager::coords
	Text_t1901882714 * ___coords_4;
	// UnityEngine.UI.Image ValleyInfoManager::valleyImage
	Image_t2670269651 * ___valleyImage_5;
	// UnityEngine.GameObject ValleyInfoManager::marchBtn
	GameObject_t1113636619 * ___marchBtn_6;
	// UnityEngine.GameObject ValleyInfoManager::buildCityBtn
	GameObject_t1113636619 * ___buildCityBtn_7;
	// UnityEngine.GameObject ValleyInfoManager::recruitBtn
	GameObject_t1113636619 * ___recruitBtn_8;
	// UnityEngine.UI.Image[] ValleyInfoManager::tabs
	ImageU5BU5D_t2439009922* ___tabs_9;
	// UnityEngine.Sprite ValleyInfoManager::tabSelectedSprite
	Sprite_t280657092 * ___tabSelectedSprite_10;
	// UnityEngine.Sprite ValleyInfoManager::tabUnselectedSprite
	Sprite_t280657092 * ___tabUnselectedSprite_11;
	// WorldFieldModel ValleyInfoManager::field
	WorldFieldModel_t2417974361 * ___field_12;

public:
	inline static int32_t get_offset_of_valleyName_2() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___valleyName_2)); }
	inline Text_t1901882714 * get_valleyName_2() const { return ___valleyName_2; }
	inline Text_t1901882714 ** get_address_of_valleyName_2() { return &___valleyName_2; }
	inline void set_valleyName_2(Text_t1901882714 * value)
	{
		___valleyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___valleyName_2), value);
	}

	inline static int32_t get_offset_of_product_3() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___product_3)); }
	inline Text_t1901882714 * get_product_3() const { return ___product_3; }
	inline Text_t1901882714 ** get_address_of_product_3() { return &___product_3; }
	inline void set_product_3(Text_t1901882714 * value)
	{
		___product_3 = value;
		Il2CppCodeGenWriteBarrier((&___product_3), value);
	}

	inline static int32_t get_offset_of_coords_4() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___coords_4)); }
	inline Text_t1901882714 * get_coords_4() const { return ___coords_4; }
	inline Text_t1901882714 ** get_address_of_coords_4() { return &___coords_4; }
	inline void set_coords_4(Text_t1901882714 * value)
	{
		___coords_4 = value;
		Il2CppCodeGenWriteBarrier((&___coords_4), value);
	}

	inline static int32_t get_offset_of_valleyImage_5() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___valleyImage_5)); }
	inline Image_t2670269651 * get_valleyImage_5() const { return ___valleyImage_5; }
	inline Image_t2670269651 ** get_address_of_valleyImage_5() { return &___valleyImage_5; }
	inline void set_valleyImage_5(Image_t2670269651 * value)
	{
		___valleyImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___valleyImage_5), value);
	}

	inline static int32_t get_offset_of_marchBtn_6() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___marchBtn_6)); }
	inline GameObject_t1113636619 * get_marchBtn_6() const { return ___marchBtn_6; }
	inline GameObject_t1113636619 ** get_address_of_marchBtn_6() { return &___marchBtn_6; }
	inline void set_marchBtn_6(GameObject_t1113636619 * value)
	{
		___marchBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&___marchBtn_6), value);
	}

	inline static int32_t get_offset_of_buildCityBtn_7() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___buildCityBtn_7)); }
	inline GameObject_t1113636619 * get_buildCityBtn_7() const { return ___buildCityBtn_7; }
	inline GameObject_t1113636619 ** get_address_of_buildCityBtn_7() { return &___buildCityBtn_7; }
	inline void set_buildCityBtn_7(GameObject_t1113636619 * value)
	{
		___buildCityBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___buildCityBtn_7), value);
	}

	inline static int32_t get_offset_of_recruitBtn_8() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___recruitBtn_8)); }
	inline GameObject_t1113636619 * get_recruitBtn_8() const { return ___recruitBtn_8; }
	inline GameObject_t1113636619 ** get_address_of_recruitBtn_8() { return &___recruitBtn_8; }
	inline void set_recruitBtn_8(GameObject_t1113636619 * value)
	{
		___recruitBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___recruitBtn_8), value);
	}

	inline static int32_t get_offset_of_tabs_9() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___tabs_9)); }
	inline ImageU5BU5D_t2439009922* get_tabs_9() const { return ___tabs_9; }
	inline ImageU5BU5D_t2439009922** get_address_of_tabs_9() { return &___tabs_9; }
	inline void set_tabs_9(ImageU5BU5D_t2439009922* value)
	{
		___tabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_9), value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_10() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___tabSelectedSprite_10)); }
	inline Sprite_t280657092 * get_tabSelectedSprite_10() const { return ___tabSelectedSprite_10; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedSprite_10() { return &___tabSelectedSprite_10; }
	inline void set_tabSelectedSprite_10(Sprite_t280657092 * value)
	{
		___tabSelectedSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedSprite_10), value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_11() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___tabUnselectedSprite_11)); }
	inline Sprite_t280657092 * get_tabUnselectedSprite_11() const { return ___tabUnselectedSprite_11; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedSprite_11() { return &___tabUnselectedSprite_11; }
	inline void set_tabUnselectedSprite_11(Sprite_t280657092 * value)
	{
		___tabUnselectedSprite_11 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedSprite_11), value);
	}

	inline static int32_t get_offset_of_field_12() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t2327472454, ___field_12)); }
	inline WorldFieldModel_t2417974361 * get_field_12() const { return ___field_12; }
	inline WorldFieldModel_t2417974361 ** get_address_of_field_12() { return &___field_12; }
	inline void set_field_12(WorldFieldModel_t2417974361 * value)
	{
		___field_12 = value;
		Il2CppCodeGenWriteBarrier((&___field_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALLEYINFOMANAGER_T2327472454_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef STANDARDINPUT_T3469524447_H
#define STANDARDINPUT_T3469524447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput
struct  StandardInput_t3469524447  : public InputSource_t979757727
{
public:
	// TouchScript.Tags TouchScript.InputSources.StandardInput::TouchTags
	Tags_t3560929397 * ___TouchTags_5;
	// TouchScript.Tags TouchScript.InputSources.StandardInput::MouseTags
	Tags_t3560929397 * ___MouseTags_6;
	// TouchScript.Tags TouchScript.InputSources.StandardInput::PenTags
	Tags_t3560929397 * ___PenTags_7;
	// TouchScript.InputSources.StandardInput/Windows8TouchAPIType TouchScript.InputSources.StandardInput::Windows8Touch
	int32_t ___Windows8Touch_8;
	// TouchScript.InputSources.StandardInput/Windows7TouchAPIType TouchScript.InputSources.StandardInput::Windows7Touch
	int32_t ___Windows7Touch_9;
	// System.Boolean TouchScript.InputSources.StandardInput::WebPlayerTouch
	bool ___WebPlayerTouch_10;
	// System.Boolean TouchScript.InputSources.StandardInput::WebGLTouch
	bool ___WebGLTouch_11;
	// System.Boolean TouchScript.InputSources.StandardInput::Windows8Mouse
	bool ___Windows8Mouse_12;
	// System.Boolean TouchScript.InputSources.StandardInput::Windows7Mouse
	bool ___Windows7Mouse_13;
	// System.Boolean TouchScript.InputSources.StandardInput::UniversalWindowsMouse
	bool ___UniversalWindowsMouse_14;
	// TouchScript.InputSources.InputHandlers.MouseHandler TouchScript.InputSources.StandardInput::mouseHandler
	MouseHandler_t2265731789 * ___mouseHandler_15;
	// TouchScript.InputSources.InputHandlers.TouchHandler TouchScript.InputSources.StandardInput::touchHandler
	TouchHandler_t684722057 * ___touchHandler_16;

public:
	inline static int32_t get_offset_of_TouchTags_5() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___TouchTags_5)); }
	inline Tags_t3560929397 * get_TouchTags_5() const { return ___TouchTags_5; }
	inline Tags_t3560929397 ** get_address_of_TouchTags_5() { return &___TouchTags_5; }
	inline void set_TouchTags_5(Tags_t3560929397 * value)
	{
		___TouchTags_5 = value;
		Il2CppCodeGenWriteBarrier((&___TouchTags_5), value);
	}

	inline static int32_t get_offset_of_MouseTags_6() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___MouseTags_6)); }
	inline Tags_t3560929397 * get_MouseTags_6() const { return ___MouseTags_6; }
	inline Tags_t3560929397 ** get_address_of_MouseTags_6() { return &___MouseTags_6; }
	inline void set_MouseTags_6(Tags_t3560929397 * value)
	{
		___MouseTags_6 = value;
		Il2CppCodeGenWriteBarrier((&___MouseTags_6), value);
	}

	inline static int32_t get_offset_of_PenTags_7() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___PenTags_7)); }
	inline Tags_t3560929397 * get_PenTags_7() const { return ___PenTags_7; }
	inline Tags_t3560929397 ** get_address_of_PenTags_7() { return &___PenTags_7; }
	inline void set_PenTags_7(Tags_t3560929397 * value)
	{
		___PenTags_7 = value;
		Il2CppCodeGenWriteBarrier((&___PenTags_7), value);
	}

	inline static int32_t get_offset_of_Windows8Touch_8() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___Windows8Touch_8)); }
	inline int32_t get_Windows8Touch_8() const { return ___Windows8Touch_8; }
	inline int32_t* get_address_of_Windows8Touch_8() { return &___Windows8Touch_8; }
	inline void set_Windows8Touch_8(int32_t value)
	{
		___Windows8Touch_8 = value;
	}

	inline static int32_t get_offset_of_Windows7Touch_9() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___Windows7Touch_9)); }
	inline int32_t get_Windows7Touch_9() const { return ___Windows7Touch_9; }
	inline int32_t* get_address_of_Windows7Touch_9() { return &___Windows7Touch_9; }
	inline void set_Windows7Touch_9(int32_t value)
	{
		___Windows7Touch_9 = value;
	}

	inline static int32_t get_offset_of_WebPlayerTouch_10() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___WebPlayerTouch_10)); }
	inline bool get_WebPlayerTouch_10() const { return ___WebPlayerTouch_10; }
	inline bool* get_address_of_WebPlayerTouch_10() { return &___WebPlayerTouch_10; }
	inline void set_WebPlayerTouch_10(bool value)
	{
		___WebPlayerTouch_10 = value;
	}

	inline static int32_t get_offset_of_WebGLTouch_11() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___WebGLTouch_11)); }
	inline bool get_WebGLTouch_11() const { return ___WebGLTouch_11; }
	inline bool* get_address_of_WebGLTouch_11() { return &___WebGLTouch_11; }
	inline void set_WebGLTouch_11(bool value)
	{
		___WebGLTouch_11 = value;
	}

	inline static int32_t get_offset_of_Windows8Mouse_12() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___Windows8Mouse_12)); }
	inline bool get_Windows8Mouse_12() const { return ___Windows8Mouse_12; }
	inline bool* get_address_of_Windows8Mouse_12() { return &___Windows8Mouse_12; }
	inline void set_Windows8Mouse_12(bool value)
	{
		___Windows8Mouse_12 = value;
	}

	inline static int32_t get_offset_of_Windows7Mouse_13() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___Windows7Mouse_13)); }
	inline bool get_Windows7Mouse_13() const { return ___Windows7Mouse_13; }
	inline bool* get_address_of_Windows7Mouse_13() { return &___Windows7Mouse_13; }
	inline void set_Windows7Mouse_13(bool value)
	{
		___Windows7Mouse_13 = value;
	}

	inline static int32_t get_offset_of_UniversalWindowsMouse_14() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___UniversalWindowsMouse_14)); }
	inline bool get_UniversalWindowsMouse_14() const { return ___UniversalWindowsMouse_14; }
	inline bool* get_address_of_UniversalWindowsMouse_14() { return &___UniversalWindowsMouse_14; }
	inline void set_UniversalWindowsMouse_14(bool value)
	{
		___UniversalWindowsMouse_14 = value;
	}

	inline static int32_t get_offset_of_mouseHandler_15() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___mouseHandler_15)); }
	inline MouseHandler_t2265731789 * get_mouseHandler_15() const { return ___mouseHandler_15; }
	inline MouseHandler_t2265731789 ** get_address_of_mouseHandler_15() { return &___mouseHandler_15; }
	inline void set_mouseHandler_15(MouseHandler_t2265731789 * value)
	{
		___mouseHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___mouseHandler_15), value);
	}

	inline static int32_t get_offset_of_touchHandler_16() { return static_cast<int32_t>(offsetof(StandardInput_t3469524447, ___touchHandler_16)); }
	inline TouchHandler_t684722057 * get_touchHandler_16() const { return ___touchHandler_16; }
	inline TouchHandler_t684722057 ** get_address_of_touchHandler_16() { return &___touchHandler_16; }
	inline void set_touchHandler_16(TouchHandler_t684722057 * value)
	{
		___touchHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDINPUT_T3469524447_H
#ifndef FULLSCREENLAYER_T254587142_H
#define FULLSCREENLAYER_T254587142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.FullscreenLayer
struct  FullscreenLayer_t254587142  : public TouchLayer_t1334725428
{
public:
	// TouchScript.Layers.FullscreenLayer/LayerType TouchScript.Layers.FullscreenLayer::type
	int32_t ___type_6;
	// UnityEngine.Camera TouchScript.Layers.FullscreenLayer::_camera
	Camera_t4157153871 * ____camera_7;
	// UnityEngine.Transform TouchScript.Layers.FullscreenLayer::cameraTransform
	Transform_t3600365921 * ___cameraTransform_8;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.FullscreenLayer::tmpHitTestList
	List_1_t2630392082 * ___tmpHitTestList_9;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(FullscreenLayer_t254587142, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of__camera_7() { return static_cast<int32_t>(offsetof(FullscreenLayer_t254587142, ____camera_7)); }
	inline Camera_t4157153871 * get__camera_7() const { return ____camera_7; }
	inline Camera_t4157153871 ** get_address_of__camera_7() { return &____camera_7; }
	inline void set__camera_7(Camera_t4157153871 * value)
	{
		____camera_7 = value;
		Il2CppCodeGenWriteBarrier((&____camera_7), value);
	}

	inline static int32_t get_offset_of_cameraTransform_8() { return static_cast<int32_t>(offsetof(FullscreenLayer_t254587142, ___cameraTransform_8)); }
	inline Transform_t3600365921 * get_cameraTransform_8() const { return ___cameraTransform_8; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_8() { return &___cameraTransform_8; }
	inline void set_cameraTransform_8(Transform_t3600365921 * value)
	{
		___cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_9() { return static_cast<int32_t>(offsetof(FullscreenLayer_t254587142, ___tmpHitTestList_9)); }
	inline List_1_t2630392082 * get_tmpHitTestList_9() const { return ___tmpHitTestList_9; }
	inline List_1_t2630392082 ** get_address_of_tmpHitTestList_9() { return &___tmpHitTestList_9; }
	inline void set_tmpHitTestList_9(List_1_t2630392082 * value)
	{
		___tmpHitTestList_9 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENLAYER_T254587142_H
#ifndef UNTOUCHABLE_T746303506_H
#define UNTOUCHABLE_T746303506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.Untouchable
struct  Untouchable_t746303506  : public HitTest_t1158317340
{
public:
	// System.Boolean TouchScript.Hit.Untouchable::DiscardTouch
	bool ___DiscardTouch_2;

public:
	inline static int32_t get_offset_of_DiscardTouch_2() { return static_cast<int32_t>(offsetof(Untouchable_t746303506, ___DiscardTouch_2)); }
	inline bool get_DiscardTouch_2() const { return ___DiscardTouch_2; }
	inline bool* get_address_of_DiscardTouch_2() { return &___DiscardTouch_2; }
	inline void set_DiscardTouch_2(bool value)
	{
		___DiscardTouch_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNTOUCHABLE_T746303506_H
#ifndef TOUCHPROXY_T686879209_H
#define TOUCHPROXY_T686879209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchProxy
struct  TouchProxy_t686879209  : public TouchProxyBase_t1980850257
{
public:
	// UnityEngine.UI.Text TouchScript.Behaviors.Visualizer.TouchProxy::Text
	Text_t1901882714 * ___Text_6;
	// System.Text.StringBuilder TouchScript.Behaviors.Visualizer.TouchProxy::stringBuilder
	StringBuilder_t * ___stringBuilder_7;

public:
	inline static int32_t get_offset_of_Text_6() { return static_cast<int32_t>(offsetof(TouchProxy_t686879209, ___Text_6)); }
	inline Text_t1901882714 * get_Text_6() const { return ___Text_6; }
	inline Text_t1901882714 ** get_address_of_Text_6() { return &___Text_6; }
	inline void set_Text_6(Text_t1901882714 * value)
	{
		___Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___Text_6), value);
	}

	inline static int32_t get_offset_of_stringBuilder_7() { return static_cast<int32_t>(offsetof(TouchProxy_t686879209, ___stringBuilder_7)); }
	inline StringBuilder_t * get_stringBuilder_7() const { return ___stringBuilder_7; }
	inline StringBuilder_t ** get_address_of_stringBuilder_7() { return &___stringBuilder_7; }
	inline void set_stringBuilder_7(StringBuilder_t * value)
	{
		___stringBuilder_7 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPROXY_T686879209_H
#ifndef CAMERALAYERBASE_T3093103062_H
#define CAMERALAYERBASE_T3093103062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayerBase
struct  CameraLayerBase_t3093103062  : public TouchLayer_t1334725428
{
public:
	// UnityEngine.LayerMask TouchScript.Layers.CameraLayerBase::layerMask
	LayerMask_t3493934918  ___layerMask_6;
	// UnityEngine.Camera TouchScript.Layers.CameraLayerBase::_camera
	Camera_t4157153871 * ____camera_7;

public:
	inline static int32_t get_offset_of_layerMask_6() { return static_cast<int32_t>(offsetof(CameraLayerBase_t3093103062, ___layerMask_6)); }
	inline LayerMask_t3493934918  get_layerMask_6() const { return ___layerMask_6; }
	inline LayerMask_t3493934918 * get_address_of_layerMask_6() { return &___layerMask_6; }
	inline void set_layerMask_6(LayerMask_t3493934918  value)
	{
		___layerMask_6 = value;
	}

	inline static int32_t get_offset_of__camera_7() { return static_cast<int32_t>(offsetof(CameraLayerBase_t3093103062, ____camera_7)); }
	inline Camera_t4157153871 * get__camera_7() const { return ____camera_7; }
	inline Camera_t4157153871 ** get_address_of__camera_7() { return &____camera_7; }
	inline void set__camera_7(Camera_t4157153871 * value)
	{
		____camera_7 = value;
		Il2CppCodeGenWriteBarrier((&____camera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYERBASE_T3093103062_H
#ifndef UILAYER_T1746714259_H
#define UILAYER_T1746714259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.UILayer
struct  UILayer_t1746714259  : public TouchLayer_t1334725428
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> TouchScript.Layers.UILayer::raycastResultCache
	List_1_t537414295 * ___raycastResultCache_7;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.UILayer::tmpHitTestList
	List_1_t2630392082 * ___tmpHitTestList_8;
	// UnityEngine.EventSystems.PointerEventData TouchScript.Layers.UILayer::pointerDataCache
	PointerEventData_t3807901092 * ___pointerDataCache_9;
	// UnityEngine.EventSystems.EventSystem TouchScript.Layers.UILayer::eventSystem
	EventSystem_t1003666588 * ___eventSystem_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams> TouchScript.Layers.UILayer::projectionParamsCache
	Dictionary_2_t1501986344 * ___projectionParamsCache_11;

public:
	inline static int32_t get_offset_of_raycastResultCache_7() { return static_cast<int32_t>(offsetof(UILayer_t1746714259, ___raycastResultCache_7)); }
	inline List_1_t537414295 * get_raycastResultCache_7() const { return ___raycastResultCache_7; }
	inline List_1_t537414295 ** get_address_of_raycastResultCache_7() { return &___raycastResultCache_7; }
	inline void set_raycastResultCache_7(List_1_t537414295 * value)
	{
		___raycastResultCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResultCache_7), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_8() { return static_cast<int32_t>(offsetof(UILayer_t1746714259, ___tmpHitTestList_8)); }
	inline List_1_t2630392082 * get_tmpHitTestList_8() const { return ___tmpHitTestList_8; }
	inline List_1_t2630392082 ** get_address_of_tmpHitTestList_8() { return &___tmpHitTestList_8; }
	inline void set_tmpHitTestList_8(List_1_t2630392082 * value)
	{
		___tmpHitTestList_8 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_8), value);
	}

	inline static int32_t get_offset_of_pointerDataCache_9() { return static_cast<int32_t>(offsetof(UILayer_t1746714259, ___pointerDataCache_9)); }
	inline PointerEventData_t3807901092 * get_pointerDataCache_9() const { return ___pointerDataCache_9; }
	inline PointerEventData_t3807901092 ** get_address_of_pointerDataCache_9() { return &___pointerDataCache_9; }
	inline void set_pointerDataCache_9(PointerEventData_t3807901092 * value)
	{
		___pointerDataCache_9 = value;
		Il2CppCodeGenWriteBarrier((&___pointerDataCache_9), value);
	}

	inline static int32_t get_offset_of_eventSystem_10() { return static_cast<int32_t>(offsetof(UILayer_t1746714259, ___eventSystem_10)); }
	inline EventSystem_t1003666588 * get_eventSystem_10() const { return ___eventSystem_10; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_10() { return &___eventSystem_10; }
	inline void set_eventSystem_10(EventSystem_t1003666588 * value)
	{
		___eventSystem_10 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_10), value);
	}

	inline static int32_t get_offset_of_projectionParamsCache_11() { return static_cast<int32_t>(offsetof(UILayer_t1746714259, ___projectionParamsCache_11)); }
	inline Dictionary_2_t1501986344 * get_projectionParamsCache_11() const { return ___projectionParamsCache_11; }
	inline Dictionary_2_t1501986344 ** get_address_of_projectionParamsCache_11() { return &___projectionParamsCache_11; }
	inline void set_projectionParamsCache_11(Dictionary_2_t1501986344 * value)
	{
		___projectionParamsCache_11 = value;
		Il2CppCodeGenWriteBarrier((&___projectionParamsCache_11), value);
	}
};

struct UILayer_t1746714259_StaticFields
{
public:
	// TouchScript.Layers.UILayer TouchScript.Layers.UILayer::instance
	UILayer_t1746714259 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(UILayer_t1746714259_StaticFields, ___instance_6)); }
	inline UILayer_t1746714259 * get_instance_6() const { return ___instance_6; }
	inline UILayer_t1746714259 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(UILayer_t1746714259 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILAYER_T1746714259_H
#ifndef MOBILEINPUT_T4061688459_H
#define MOBILEINPUT_T4061688459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.MobileInput
struct  MobileInput_t4061688459  : public InputSource_t979757727
{
public:
	// System.Boolean TouchScript.InputSources.MobileInput::DisableOnNonTouchPlatforms
	bool ___DisableOnNonTouchPlatforms_5;
	// TouchScript.Tags TouchScript.InputSources.MobileInput::Tags
	Tags_t3560929397 * ___Tags_6;
	// TouchScript.InputSources.InputHandlers.TouchHandler TouchScript.InputSources.MobileInput::touchHandler
	TouchHandler_t684722057 * ___touchHandler_7;

public:
	inline static int32_t get_offset_of_DisableOnNonTouchPlatforms_5() { return static_cast<int32_t>(offsetof(MobileInput_t4061688459, ___DisableOnNonTouchPlatforms_5)); }
	inline bool get_DisableOnNonTouchPlatforms_5() const { return ___DisableOnNonTouchPlatforms_5; }
	inline bool* get_address_of_DisableOnNonTouchPlatforms_5() { return &___DisableOnNonTouchPlatforms_5; }
	inline void set_DisableOnNonTouchPlatforms_5(bool value)
	{
		___DisableOnNonTouchPlatforms_5 = value;
	}

	inline static int32_t get_offset_of_Tags_6() { return static_cast<int32_t>(offsetof(MobileInput_t4061688459, ___Tags_6)); }
	inline Tags_t3560929397 * get_Tags_6() const { return ___Tags_6; }
	inline Tags_t3560929397 ** get_address_of_Tags_6() { return &___Tags_6; }
	inline void set_Tags_6(Tags_t3560929397 * value)
	{
		___Tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_6), value);
	}

	inline static int32_t get_offset_of_touchHandler_7() { return static_cast<int32_t>(offsetof(MobileInput_t4061688459, ___touchHandler_7)); }
	inline TouchHandler_t684722057 * get_touchHandler_7() const { return ___touchHandler_7; }
	inline TouchHandler_t684722057 ** get_address_of_touchHandler_7() { return &___touchHandler_7; }
	inline void set_touchHandler_7(TouchHandler_t684722057 * value)
	{
		___touchHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T4061688459_H
#ifndef TOUCHMANAGERINSTANCE_T3252675660_H
#define TOUCHMANAGERINSTANCE_T3252675660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance
struct  TouchManagerInstance_t3252675660  : public DebuggableMonoBehaviour_t3692909384
{
public:
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesBeganInvoker
	EventHandler_1_t2376944037 * ___touchesBeganInvoker_2;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesMovedInvoker
	EventHandler_1_t2376944037 * ___touchesMovedInvoker_3;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesEndedInvoker
	EventHandler_1_t2376944037 * ___touchesEndedInvoker_4;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesCancelledInvoker
	EventHandler_1_t2376944037 * ___touchesCancelledInvoker_5;
	// System.EventHandler TouchScript.TouchManagerInstance::frameStartedInvoker
	EventHandler_t1348719766 * ___frameStartedInvoker_6;
	// System.EventHandler TouchScript.TouchManagerInstance::frameFinishedInvoker
	EventHandler_t1348719766 * ___frameFinishedInvoker_7;
	// System.Boolean TouchScript.TouchManagerInstance::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_10;
	// System.Boolean TouchScript.TouchManagerInstance::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_11;
	// TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManagerInstance::displayDevice
	RuntimeObject* ___displayDevice_12;
	// System.Single TouchScript.TouchManagerInstance::dpi
	float ___dpi_13;
	// System.Single TouchScript.TouchManagerInstance::dotsPerCentimeter
	float ___dotsPerCentimeter_14;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManagerInstance::layers
	List_1_t2806800170 * ___layers_15;
	// System.Int32 TouchScript.TouchManagerInstance::layerCount
	int32_t ___layerCount_16;
	// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource> TouchScript.TouchManagerInstance::inputs
	List_1_t967242533 * ___inputs_17;
	// System.Int32 TouchScript.TouchManagerInstance::inputCount
	int32_t ___inputCount_18;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touches
	List_1_t2345966477 * ___touches_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.TouchPoint> TouchScript.TouchManagerInstance::idToTouch
	Dictionary_2_t4057572362 * ___idToTouch_20;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touchesBegan
	List_1_t2345966477 * ___touchesBegan_21;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesUpdated
	HashSet_1_t1515895227 * ___touchesUpdated_22;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesEnded
	HashSet_1_t1515895227 * ___touchesEnded_23;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesCancelled
	HashSet_1_t1515895227 * ___touchesCancelled_24;
	// System.Int32 TouchScript.TouchManagerInstance::nextTouchId
	int32_t ___nextTouchId_28;
	// System.Object TouchScript.TouchManagerInstance::touchLock
	RuntimeObject * ___touchLock_29;

public:
	inline static int32_t get_offset_of_touchesBeganInvoker_2() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesBeganInvoker_2)); }
	inline EventHandler_1_t2376944037 * get_touchesBeganInvoker_2() const { return ___touchesBeganInvoker_2; }
	inline EventHandler_1_t2376944037 ** get_address_of_touchesBeganInvoker_2() { return &___touchesBeganInvoker_2; }
	inline void set_touchesBeganInvoker_2(EventHandler_1_t2376944037 * value)
	{
		___touchesBeganInvoker_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBeganInvoker_2), value);
	}

	inline static int32_t get_offset_of_touchesMovedInvoker_3() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesMovedInvoker_3)); }
	inline EventHandler_1_t2376944037 * get_touchesMovedInvoker_3() const { return ___touchesMovedInvoker_3; }
	inline EventHandler_1_t2376944037 ** get_address_of_touchesMovedInvoker_3() { return &___touchesMovedInvoker_3; }
	inline void set_touchesMovedInvoker_3(EventHandler_1_t2376944037 * value)
	{
		___touchesMovedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___touchesMovedInvoker_3), value);
	}

	inline static int32_t get_offset_of_touchesEndedInvoker_4() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesEndedInvoker_4)); }
	inline EventHandler_1_t2376944037 * get_touchesEndedInvoker_4() const { return ___touchesEndedInvoker_4; }
	inline EventHandler_1_t2376944037 ** get_address_of_touchesEndedInvoker_4() { return &___touchesEndedInvoker_4; }
	inline void set_touchesEndedInvoker_4(EventHandler_1_t2376944037 * value)
	{
		___touchesEndedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEndedInvoker_4), value);
	}

	inline static int32_t get_offset_of_touchesCancelledInvoker_5() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesCancelledInvoker_5)); }
	inline EventHandler_1_t2376944037 * get_touchesCancelledInvoker_5() const { return ___touchesCancelledInvoker_5; }
	inline EventHandler_1_t2376944037 ** get_address_of_touchesCancelledInvoker_5() { return &___touchesCancelledInvoker_5; }
	inline void set_touchesCancelledInvoker_5(EventHandler_1_t2376944037 * value)
	{
		___touchesCancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((&___touchesCancelledInvoker_5), value);
	}

	inline static int32_t get_offset_of_frameStartedInvoker_6() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___frameStartedInvoker_6)); }
	inline EventHandler_t1348719766 * get_frameStartedInvoker_6() const { return ___frameStartedInvoker_6; }
	inline EventHandler_t1348719766 ** get_address_of_frameStartedInvoker_6() { return &___frameStartedInvoker_6; }
	inline void set_frameStartedInvoker_6(EventHandler_t1348719766 * value)
	{
		___frameStartedInvoker_6 = value;
		Il2CppCodeGenWriteBarrier((&___frameStartedInvoker_6), value);
	}

	inline static int32_t get_offset_of_frameFinishedInvoker_7() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___frameFinishedInvoker_7)); }
	inline EventHandler_t1348719766 * get_frameFinishedInvoker_7() const { return ___frameFinishedInvoker_7; }
	inline EventHandler_t1348719766 ** get_address_of_frameFinishedInvoker_7() { return &___frameFinishedInvoker_7; }
	inline void set_frameFinishedInvoker_7(EventHandler_t1348719766 * value)
	{
		___frameFinishedInvoker_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameFinishedInvoker_7), value);
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_10() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___shouldCreateCameraLayer_10)); }
	inline bool get_shouldCreateCameraLayer_10() const { return ___shouldCreateCameraLayer_10; }
	inline bool* get_address_of_shouldCreateCameraLayer_10() { return &___shouldCreateCameraLayer_10; }
	inline void set_shouldCreateCameraLayer_10(bool value)
	{
		___shouldCreateCameraLayer_10 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_11() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___shouldCreateStandardInput_11)); }
	inline bool get_shouldCreateStandardInput_11() const { return ___shouldCreateStandardInput_11; }
	inline bool* get_address_of_shouldCreateStandardInput_11() { return &___shouldCreateStandardInput_11; }
	inline void set_shouldCreateStandardInput_11(bool value)
	{
		___shouldCreateStandardInput_11 = value;
	}

	inline static int32_t get_offset_of_displayDevice_12() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___displayDevice_12)); }
	inline RuntimeObject* get_displayDevice_12() const { return ___displayDevice_12; }
	inline RuntimeObject** get_address_of_displayDevice_12() { return &___displayDevice_12; }
	inline void set_displayDevice_12(RuntimeObject* value)
	{
		___displayDevice_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayDevice_12), value);
	}

	inline static int32_t get_offset_of_dpi_13() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___dpi_13)); }
	inline float get_dpi_13() const { return ___dpi_13; }
	inline float* get_address_of_dpi_13() { return &___dpi_13; }
	inline void set_dpi_13(float value)
	{
		___dpi_13 = value;
	}

	inline static int32_t get_offset_of_dotsPerCentimeter_14() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___dotsPerCentimeter_14)); }
	inline float get_dotsPerCentimeter_14() const { return ___dotsPerCentimeter_14; }
	inline float* get_address_of_dotsPerCentimeter_14() { return &___dotsPerCentimeter_14; }
	inline void set_dotsPerCentimeter_14(float value)
	{
		___dotsPerCentimeter_14 = value;
	}

	inline static int32_t get_offset_of_layers_15() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___layers_15)); }
	inline List_1_t2806800170 * get_layers_15() const { return ___layers_15; }
	inline List_1_t2806800170 ** get_address_of_layers_15() { return &___layers_15; }
	inline void set_layers_15(List_1_t2806800170 * value)
	{
		___layers_15 = value;
		Il2CppCodeGenWriteBarrier((&___layers_15), value);
	}

	inline static int32_t get_offset_of_layerCount_16() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___layerCount_16)); }
	inline int32_t get_layerCount_16() const { return ___layerCount_16; }
	inline int32_t* get_address_of_layerCount_16() { return &___layerCount_16; }
	inline void set_layerCount_16(int32_t value)
	{
		___layerCount_16 = value;
	}

	inline static int32_t get_offset_of_inputs_17() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___inputs_17)); }
	inline List_1_t967242533 * get_inputs_17() const { return ___inputs_17; }
	inline List_1_t967242533 ** get_address_of_inputs_17() { return &___inputs_17; }
	inline void set_inputs_17(List_1_t967242533 * value)
	{
		___inputs_17 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_17), value);
	}

	inline static int32_t get_offset_of_inputCount_18() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___inputCount_18)); }
	inline int32_t get_inputCount_18() const { return ___inputCount_18; }
	inline int32_t* get_address_of_inputCount_18() { return &___inputCount_18; }
	inline void set_inputCount_18(int32_t value)
	{
		___inputCount_18 = value;
	}

	inline static int32_t get_offset_of_touches_19() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touches_19)); }
	inline List_1_t2345966477 * get_touches_19() const { return ___touches_19; }
	inline List_1_t2345966477 ** get_address_of_touches_19() { return &___touches_19; }
	inline void set_touches_19(List_1_t2345966477 * value)
	{
		___touches_19 = value;
		Il2CppCodeGenWriteBarrier((&___touches_19), value);
	}

	inline static int32_t get_offset_of_idToTouch_20() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___idToTouch_20)); }
	inline Dictionary_2_t4057572362 * get_idToTouch_20() const { return ___idToTouch_20; }
	inline Dictionary_2_t4057572362 ** get_address_of_idToTouch_20() { return &___idToTouch_20; }
	inline void set_idToTouch_20(Dictionary_2_t4057572362 * value)
	{
		___idToTouch_20 = value;
		Il2CppCodeGenWriteBarrier((&___idToTouch_20), value);
	}

	inline static int32_t get_offset_of_touchesBegan_21() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesBegan_21)); }
	inline List_1_t2345966477 * get_touchesBegan_21() const { return ___touchesBegan_21; }
	inline List_1_t2345966477 ** get_address_of_touchesBegan_21() { return &___touchesBegan_21; }
	inline void set_touchesBegan_21(List_1_t2345966477 * value)
	{
		___touchesBegan_21 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBegan_21), value);
	}

	inline static int32_t get_offset_of_touchesUpdated_22() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesUpdated_22)); }
	inline HashSet_1_t1515895227 * get_touchesUpdated_22() const { return ___touchesUpdated_22; }
	inline HashSet_1_t1515895227 ** get_address_of_touchesUpdated_22() { return &___touchesUpdated_22; }
	inline void set_touchesUpdated_22(HashSet_1_t1515895227 * value)
	{
		___touchesUpdated_22 = value;
		Il2CppCodeGenWriteBarrier((&___touchesUpdated_22), value);
	}

	inline static int32_t get_offset_of_touchesEnded_23() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesEnded_23)); }
	inline HashSet_1_t1515895227 * get_touchesEnded_23() const { return ___touchesEnded_23; }
	inline HashSet_1_t1515895227 ** get_address_of_touchesEnded_23() { return &___touchesEnded_23; }
	inline void set_touchesEnded_23(HashSet_1_t1515895227 * value)
	{
		___touchesEnded_23 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEnded_23), value);
	}

	inline static int32_t get_offset_of_touchesCancelled_24() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchesCancelled_24)); }
	inline HashSet_1_t1515895227 * get_touchesCancelled_24() const { return ___touchesCancelled_24; }
	inline HashSet_1_t1515895227 ** get_address_of_touchesCancelled_24() { return &___touchesCancelled_24; }
	inline void set_touchesCancelled_24(HashSet_1_t1515895227 * value)
	{
		___touchesCancelled_24 = value;
		Il2CppCodeGenWriteBarrier((&___touchesCancelled_24), value);
	}

	inline static int32_t get_offset_of_nextTouchId_28() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___nextTouchId_28)); }
	inline int32_t get_nextTouchId_28() const { return ___nextTouchId_28; }
	inline int32_t* get_address_of_nextTouchId_28() { return &___nextTouchId_28; }
	inline void set_nextTouchId_28(int32_t value)
	{
		___nextTouchId_28 = value;
	}

	inline static int32_t get_offset_of_touchLock_29() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660, ___touchLock_29)); }
	inline RuntimeObject * get_touchLock_29() const { return ___touchLock_29; }
	inline RuntimeObject ** get_address_of_touchLock_29() { return &___touchLock_29; }
	inline void set_touchLock_29(RuntimeObject * value)
	{
		___touchLock_29 = value;
		Il2CppCodeGenWriteBarrier((&___touchLock_29), value);
	}
};

struct TouchManagerInstance_t3252675660_StaticFields
{
public:
	// System.Boolean TouchScript.TouchManagerInstance::shuttingDown
	bool ___shuttingDown_8;
	// TouchScript.TouchManagerInstance TouchScript.TouchManagerInstance::instance
	TouchManagerInstance_t3252675660 * ___instance_9;
	// TouchScript.Utils.ObjectPool`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touchPointPool
	ObjectPool_1_t1497135908 * ___touchPointPool_25;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.TouchManagerInstance::touchPointListPool
	ObjectPool_1_t2969210650 * ___touchPointListPool_26;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>> TouchScript.TouchManagerInstance::intListPool
	ObjectPool_1_t751297372 * ___intListPool_27;
	// System.Predicate`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManagerInstance::<>f__am$cache0
	Predicate_1_t2160019552 * ___U3CU3Ef__amU24cache0_30;

public:
	inline static int32_t get_offset_of_shuttingDown_8() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___shuttingDown_8)); }
	inline bool get_shuttingDown_8() const { return ___shuttingDown_8; }
	inline bool* get_address_of_shuttingDown_8() { return &___shuttingDown_8; }
	inline void set_shuttingDown_8(bool value)
	{
		___shuttingDown_8 = value;
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___instance_9)); }
	inline TouchManagerInstance_t3252675660 * get_instance_9() const { return ___instance_9; }
	inline TouchManagerInstance_t3252675660 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(TouchManagerInstance_t3252675660 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}

	inline static int32_t get_offset_of_touchPointPool_25() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___touchPointPool_25)); }
	inline ObjectPool_1_t1497135908 * get_touchPointPool_25() const { return ___touchPointPool_25; }
	inline ObjectPool_1_t1497135908 ** get_address_of_touchPointPool_25() { return &___touchPointPool_25; }
	inline void set_touchPointPool_25(ObjectPool_1_t1497135908 * value)
	{
		___touchPointPool_25 = value;
		Il2CppCodeGenWriteBarrier((&___touchPointPool_25), value);
	}

	inline static int32_t get_offset_of_touchPointListPool_26() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___touchPointListPool_26)); }
	inline ObjectPool_1_t2969210650 * get_touchPointListPool_26() const { return ___touchPointListPool_26; }
	inline ObjectPool_1_t2969210650 ** get_address_of_touchPointListPool_26() { return &___touchPointListPool_26; }
	inline void set_touchPointListPool_26(ObjectPool_1_t2969210650 * value)
	{
		___touchPointListPool_26 = value;
		Il2CppCodeGenWriteBarrier((&___touchPointListPool_26), value);
	}

	inline static int32_t get_offset_of_intListPool_27() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___intListPool_27)); }
	inline ObjectPool_1_t751297372 * get_intListPool_27() const { return ___intListPool_27; }
	inline ObjectPool_1_t751297372 ** get_address_of_intListPool_27() { return &___intListPool_27; }
	inline void set_intListPool_27(ObjectPool_1_t751297372 * value)
	{
		___intListPool_27 = value;
		Il2CppCodeGenWriteBarrier((&___intListPool_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_30() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t3252675660_StaticFields, ___U3CU3Ef__amU24cache0_30)); }
	inline Predicate_1_t2160019552 * get_U3CU3Ef__amU24cache0_30() const { return ___U3CU3Ef__amU24cache0_30; }
	inline Predicate_1_t2160019552 ** get_address_of_U3CU3Ef__amU24cache0_30() { return &___U3CU3Ef__amU24cache0_30; }
	inline void set_U3CU3Ef__amU24cache0_30(Predicate_1_t2160019552 * value)
	{
		___U3CU3Ef__amU24cache0_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHMANAGERINSTANCE_T3252675660_H
#ifndef TUIOINPUT_T3522336661_H
#define TUIOINPUT_T3522336661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioInput
struct  TuioInput_t3522336661  : public InputSource_t979757727
{
public:
	// System.Int32 TouchScript.InputSources.TuioInput::tuioPort
	int32_t ___tuioPort_6;
	// TouchScript.InputSources.TuioInput/InputType TouchScript.InputSources.TuioInput::supportedInputs
	int32_t ___supportedInputs_7;
	// System.Collections.Generic.List`1<TouchScript.InputSources.TuioObjectMapping> TouchScript.InputSources.TuioInput::tuioObjectMappings
	List_1_t3907028109 * ___tuioObjectMappings_8;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::cursorTags
	Tags_t3560929397 * ___cursorTags_9;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::blobTags
	Tags_t3560929397 * ___blobTags_10;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::objectTags
	Tags_t3560929397 * ___objectTags_11;
	// TUIOsharp.TuioServer TouchScript.InputSources.TuioInput::server
	TuioServer_t3158170151 * ___server_12;
	// TUIOsharp.DataProcessors.CursorProcessor TouchScript.InputSources.TuioInput::cursorProcessor
	CursorProcessor_t3936223090 * ___cursorProcessor_13;
	// TUIOsharp.DataProcessors.ObjectProcessor TouchScript.InputSources.TuioInput::objectProcessor
	ObjectProcessor_t2877610401 * ___objectProcessor_14;
	// TUIOsharp.DataProcessors.BlobProcessor TouchScript.InputSources.TuioInput::blobProcessor
	BlobProcessor_t2126871202 * ___blobProcessor_15;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::cursorToInternalId
	Dictionary_2_t2551302653 * ___cursorToInternalId_16;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::blobToInternalId
	Dictionary_2_t1664370694 * ___blobToInternalId_17;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioObject,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::objectToInternalId
	Dictionary_2_t1027656984 * ___objectToInternalId_18;
	// System.Int32 TouchScript.InputSources.TuioInput::screenWidth
	int32_t ___screenWidth_19;
	// System.Int32 TouchScript.InputSources.TuioInput::screenHeight
	int32_t ___screenHeight_20;

public:
	inline static int32_t get_offset_of_tuioPort_6() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___tuioPort_6)); }
	inline int32_t get_tuioPort_6() const { return ___tuioPort_6; }
	inline int32_t* get_address_of_tuioPort_6() { return &___tuioPort_6; }
	inline void set_tuioPort_6(int32_t value)
	{
		___tuioPort_6 = value;
	}

	inline static int32_t get_offset_of_supportedInputs_7() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___supportedInputs_7)); }
	inline int32_t get_supportedInputs_7() const { return ___supportedInputs_7; }
	inline int32_t* get_address_of_supportedInputs_7() { return &___supportedInputs_7; }
	inline void set_supportedInputs_7(int32_t value)
	{
		___supportedInputs_7 = value;
	}

	inline static int32_t get_offset_of_tuioObjectMappings_8() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___tuioObjectMappings_8)); }
	inline List_1_t3907028109 * get_tuioObjectMappings_8() const { return ___tuioObjectMappings_8; }
	inline List_1_t3907028109 ** get_address_of_tuioObjectMappings_8() { return &___tuioObjectMappings_8; }
	inline void set_tuioObjectMappings_8(List_1_t3907028109 * value)
	{
		___tuioObjectMappings_8 = value;
		Il2CppCodeGenWriteBarrier((&___tuioObjectMappings_8), value);
	}

	inline static int32_t get_offset_of_cursorTags_9() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___cursorTags_9)); }
	inline Tags_t3560929397 * get_cursorTags_9() const { return ___cursorTags_9; }
	inline Tags_t3560929397 ** get_address_of_cursorTags_9() { return &___cursorTags_9; }
	inline void set_cursorTags_9(Tags_t3560929397 * value)
	{
		___cursorTags_9 = value;
		Il2CppCodeGenWriteBarrier((&___cursorTags_9), value);
	}

	inline static int32_t get_offset_of_blobTags_10() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___blobTags_10)); }
	inline Tags_t3560929397 * get_blobTags_10() const { return ___blobTags_10; }
	inline Tags_t3560929397 ** get_address_of_blobTags_10() { return &___blobTags_10; }
	inline void set_blobTags_10(Tags_t3560929397 * value)
	{
		___blobTags_10 = value;
		Il2CppCodeGenWriteBarrier((&___blobTags_10), value);
	}

	inline static int32_t get_offset_of_objectTags_11() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___objectTags_11)); }
	inline Tags_t3560929397 * get_objectTags_11() const { return ___objectTags_11; }
	inline Tags_t3560929397 ** get_address_of_objectTags_11() { return &___objectTags_11; }
	inline void set_objectTags_11(Tags_t3560929397 * value)
	{
		___objectTags_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectTags_11), value);
	}

	inline static int32_t get_offset_of_server_12() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___server_12)); }
	inline TuioServer_t3158170151 * get_server_12() const { return ___server_12; }
	inline TuioServer_t3158170151 ** get_address_of_server_12() { return &___server_12; }
	inline void set_server_12(TuioServer_t3158170151 * value)
	{
		___server_12 = value;
		Il2CppCodeGenWriteBarrier((&___server_12), value);
	}

	inline static int32_t get_offset_of_cursorProcessor_13() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___cursorProcessor_13)); }
	inline CursorProcessor_t3936223090 * get_cursorProcessor_13() const { return ___cursorProcessor_13; }
	inline CursorProcessor_t3936223090 ** get_address_of_cursorProcessor_13() { return &___cursorProcessor_13; }
	inline void set_cursorProcessor_13(CursorProcessor_t3936223090 * value)
	{
		___cursorProcessor_13 = value;
		Il2CppCodeGenWriteBarrier((&___cursorProcessor_13), value);
	}

	inline static int32_t get_offset_of_objectProcessor_14() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___objectProcessor_14)); }
	inline ObjectProcessor_t2877610401 * get_objectProcessor_14() const { return ___objectProcessor_14; }
	inline ObjectProcessor_t2877610401 ** get_address_of_objectProcessor_14() { return &___objectProcessor_14; }
	inline void set_objectProcessor_14(ObjectProcessor_t2877610401 * value)
	{
		___objectProcessor_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectProcessor_14), value);
	}

	inline static int32_t get_offset_of_blobProcessor_15() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___blobProcessor_15)); }
	inline BlobProcessor_t2126871202 * get_blobProcessor_15() const { return ___blobProcessor_15; }
	inline BlobProcessor_t2126871202 ** get_address_of_blobProcessor_15() { return &___blobProcessor_15; }
	inline void set_blobProcessor_15(BlobProcessor_t2126871202 * value)
	{
		___blobProcessor_15 = value;
		Il2CppCodeGenWriteBarrier((&___blobProcessor_15), value);
	}

	inline static int32_t get_offset_of_cursorToInternalId_16() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___cursorToInternalId_16)); }
	inline Dictionary_2_t2551302653 * get_cursorToInternalId_16() const { return ___cursorToInternalId_16; }
	inline Dictionary_2_t2551302653 ** get_address_of_cursorToInternalId_16() { return &___cursorToInternalId_16; }
	inline void set_cursorToInternalId_16(Dictionary_2_t2551302653 * value)
	{
		___cursorToInternalId_16 = value;
		Il2CppCodeGenWriteBarrier((&___cursorToInternalId_16), value);
	}

	inline static int32_t get_offset_of_blobToInternalId_17() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___blobToInternalId_17)); }
	inline Dictionary_2_t1664370694 * get_blobToInternalId_17() const { return ___blobToInternalId_17; }
	inline Dictionary_2_t1664370694 ** get_address_of_blobToInternalId_17() { return &___blobToInternalId_17; }
	inline void set_blobToInternalId_17(Dictionary_2_t1664370694 * value)
	{
		___blobToInternalId_17 = value;
		Il2CppCodeGenWriteBarrier((&___blobToInternalId_17), value);
	}

	inline static int32_t get_offset_of_objectToInternalId_18() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___objectToInternalId_18)); }
	inline Dictionary_2_t1027656984 * get_objectToInternalId_18() const { return ___objectToInternalId_18; }
	inline Dictionary_2_t1027656984 ** get_address_of_objectToInternalId_18() { return &___objectToInternalId_18; }
	inline void set_objectToInternalId_18(Dictionary_2_t1027656984 * value)
	{
		___objectToInternalId_18 = value;
		Il2CppCodeGenWriteBarrier((&___objectToInternalId_18), value);
	}

	inline static int32_t get_offset_of_screenWidth_19() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___screenWidth_19)); }
	inline int32_t get_screenWidth_19() const { return ___screenWidth_19; }
	inline int32_t* get_address_of_screenWidth_19() { return &___screenWidth_19; }
	inline void set_screenWidth_19(int32_t value)
	{
		___screenWidth_19 = value;
	}

	inline static int32_t get_offset_of_screenHeight_20() { return static_cast<int32_t>(offsetof(TuioInput_t3522336661, ___screenHeight_20)); }
	inline int32_t get_screenHeight_20() const { return ___screenHeight_20; }
	inline int32_t* get_address_of_screenHeight_20() { return &___screenHeight_20; }
	inline void set_screenHeight_20(int32_t value)
	{
		___screenHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOINPUT_T3522336661_H
#ifndef GESTURE_T4067939634_H
#define GESTURE_T4067939634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture
struct  Gesture_t4067939634  : public DebuggableMonoBehaviour_t3692909384
{
public:
	// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs> TouchScript.Gestures.Gesture::stateChangedInvoker
	EventHandler_1_t766498446 * ___stateChangedInvoker_4;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Gesture::cancelledInvoker
	EventHandler_1_t1515976428 * ___cancelledInvoker_5;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_6;
	// TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_7;
	// TouchScript.ITouchManager TouchScript.Gestures.Gesture::<touchManager>k__BackingField
	RuntimeObject* ___U3CtouchManagerU3Ek__BackingField_8;
	// TouchScript.Gestures.Gesture/TouchesNumState TouchScript.Gestures.Gesture::<touchesNumState>k__BackingField
	int32_t ___U3CtouchesNumStateU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::activeTouches
	List_1_t2345966477 * ___activeTouches_10;
	// UnityEngine.Transform TouchScript.Gestures.Gesture::cachedTransform
	Transform_t3600365921 * ___cachedTransform_11;
	// System.Boolean TouchScript.Gestures.Gesture::advancedProps
	bool ___advancedProps_12;
	// System.Int32 TouchScript.Gestures.Gesture::minTouches
	int32_t ___minTouches_13;
	// System.Int32 TouchScript.Gestures.Gesture::maxTouches
	int32_t ___maxTouches_14;
	// System.Boolean TouchScript.Gestures.Gesture::combineTouches
	bool ___combineTouches_15;
	// System.Single TouchScript.Gestures.Gesture::combineTouchesInterval
	float ___combineTouchesInterval_16;
	// System.Boolean TouchScript.Gestures.Gesture::useSendMessage
	bool ___useSendMessage_17;
	// System.Boolean TouchScript.Gestures.Gesture::sendStateChangeMessages
	bool ___sendStateChangeMessages_18;
	// UnityEngine.GameObject TouchScript.Gestures.Gesture::sendMessageTarget
	GameObject_t1113636619 * ___sendMessageTarget_19;
	// TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::requireGestureToFail
	Gesture_t4067939634 * ___requireGestureToFail_20;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Gestures.Gesture::friendlyGestures
	List_1_t1245047080 * ___friendlyGestures_21;
	// System.Int32 TouchScript.Gestures.Gesture::numTouches
	int32_t ___numTouches_22;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.Gesture::layer
	TouchLayer_t1334725428 * ___layer_23;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::readonlyActiveTouches
	ReadOnlyCollection_1_t2086468022 * ___readonlyActiveTouches_24;
	// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::touchSequence
	TimedSequence_1_t1745006843 * ___touchSequence_25;
	// TouchScript.GestureManagerInstance TouchScript.Gestures.Gesture::gestureManagerInstance
	GestureManagerInstance_t955425494 * ___gestureManagerInstance_26;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::delayedStateChange
	int32_t ___delayedStateChange_27;
	// System.Boolean TouchScript.Gestures.Gesture::requiredGestureFailed
	bool ___requiredGestureFailed_28;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::state
	int32_t ___state_29;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedScreenPosition
	Vector2_t2156229523  ___cachedScreenPosition_30;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedPreviousScreenPosition
	Vector2_t2156229523  ___cachedPreviousScreenPosition_31;

public:
	inline static int32_t get_offset_of_stateChangedInvoker_4() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___stateChangedInvoker_4)); }
	inline EventHandler_1_t766498446 * get_stateChangedInvoker_4() const { return ___stateChangedInvoker_4; }
	inline EventHandler_1_t766498446 ** get_address_of_stateChangedInvoker_4() { return &___stateChangedInvoker_4; }
	inline void set_stateChangedInvoker_4(EventHandler_1_t766498446 * value)
	{
		___stateChangedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((&___stateChangedInvoker_4), value);
	}

	inline static int32_t get_offset_of_cancelledInvoker_5() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cancelledInvoker_5)); }
	inline EventHandler_1_t1515976428 * get_cancelledInvoker_5() const { return ___cancelledInvoker_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_cancelledInvoker_5() { return &___cancelledInvoker_5; }
	inline void set_cancelledInvoker_5(EventHandler_1_t1515976428 * value)
	{
		___cancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((&___cancelledInvoker_5), value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CPreviousStateU3Ek__BackingField_6)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_6() const { return ___U3CPreviousStateU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_6() { return &___U3CPreviousStateU3Ek__BackingField_6; }
	inline void set_U3CPreviousStateU3Ek__BackingField_6(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CDelegateU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_7() const { return ___U3CDelegateU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_7() { return &___U3CDelegateU3Ek__BackingField_7; }
	inline void set_U3CDelegateU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CtouchManagerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CtouchManagerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CtouchManagerU3Ek__BackingField_8() const { return ___U3CtouchManagerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CtouchManagerU3Ek__BackingField_8() { return &___U3CtouchManagerU3Ek__BackingField_8; }
	inline void set_U3CtouchManagerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CtouchManagerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchManagerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CtouchesNumStateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CtouchesNumStateU3Ek__BackingField_9)); }
	inline int32_t get_U3CtouchesNumStateU3Ek__BackingField_9() const { return ___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CtouchesNumStateU3Ek__BackingField_9() { return &___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline void set_U3CtouchesNumStateU3Ek__BackingField_9(int32_t value)
	{
		___U3CtouchesNumStateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_activeTouches_10() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___activeTouches_10)); }
	inline List_1_t2345966477 * get_activeTouches_10() const { return ___activeTouches_10; }
	inline List_1_t2345966477 ** get_address_of_activeTouches_10() { return &___activeTouches_10; }
	inline void set_activeTouches_10(List_1_t2345966477 * value)
	{
		___activeTouches_10 = value;
		Il2CppCodeGenWriteBarrier((&___activeTouches_10), value);
	}

	inline static int32_t get_offset_of_cachedTransform_11() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedTransform_11)); }
	inline Transform_t3600365921 * get_cachedTransform_11() const { return ___cachedTransform_11; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_11() { return &___cachedTransform_11; }
	inline void set_cachedTransform_11(Transform_t3600365921 * value)
	{
		___cachedTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_11), value);
	}

	inline static int32_t get_offset_of_advancedProps_12() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___advancedProps_12)); }
	inline bool get_advancedProps_12() const { return ___advancedProps_12; }
	inline bool* get_address_of_advancedProps_12() { return &___advancedProps_12; }
	inline void set_advancedProps_12(bool value)
	{
		___advancedProps_12 = value;
	}

	inline static int32_t get_offset_of_minTouches_13() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___minTouches_13)); }
	inline int32_t get_minTouches_13() const { return ___minTouches_13; }
	inline int32_t* get_address_of_minTouches_13() { return &___minTouches_13; }
	inline void set_minTouches_13(int32_t value)
	{
		___minTouches_13 = value;
	}

	inline static int32_t get_offset_of_maxTouches_14() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___maxTouches_14)); }
	inline int32_t get_maxTouches_14() const { return ___maxTouches_14; }
	inline int32_t* get_address_of_maxTouches_14() { return &___maxTouches_14; }
	inline void set_maxTouches_14(int32_t value)
	{
		___maxTouches_14 = value;
	}

	inline static int32_t get_offset_of_combineTouches_15() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___combineTouches_15)); }
	inline bool get_combineTouches_15() const { return ___combineTouches_15; }
	inline bool* get_address_of_combineTouches_15() { return &___combineTouches_15; }
	inline void set_combineTouches_15(bool value)
	{
		___combineTouches_15 = value;
	}

	inline static int32_t get_offset_of_combineTouchesInterval_16() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___combineTouchesInterval_16)); }
	inline float get_combineTouchesInterval_16() const { return ___combineTouchesInterval_16; }
	inline float* get_address_of_combineTouchesInterval_16() { return &___combineTouchesInterval_16; }
	inline void set_combineTouchesInterval_16(float value)
	{
		___combineTouchesInterval_16 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_17() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___useSendMessage_17)); }
	inline bool get_useSendMessage_17() const { return ___useSendMessage_17; }
	inline bool* get_address_of_useSendMessage_17() { return &___useSendMessage_17; }
	inline void set_useSendMessage_17(bool value)
	{
		___useSendMessage_17 = value;
	}

	inline static int32_t get_offset_of_sendStateChangeMessages_18() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___sendStateChangeMessages_18)); }
	inline bool get_sendStateChangeMessages_18() const { return ___sendStateChangeMessages_18; }
	inline bool* get_address_of_sendStateChangeMessages_18() { return &___sendStateChangeMessages_18; }
	inline void set_sendStateChangeMessages_18(bool value)
	{
		___sendStateChangeMessages_18 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_19() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___sendMessageTarget_19)); }
	inline GameObject_t1113636619 * get_sendMessageTarget_19() const { return ___sendMessageTarget_19; }
	inline GameObject_t1113636619 ** get_address_of_sendMessageTarget_19() { return &___sendMessageTarget_19; }
	inline void set_sendMessageTarget_19(GameObject_t1113636619 * value)
	{
		___sendMessageTarget_19 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_19), value);
	}

	inline static int32_t get_offset_of_requireGestureToFail_20() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___requireGestureToFail_20)); }
	inline Gesture_t4067939634 * get_requireGestureToFail_20() const { return ___requireGestureToFail_20; }
	inline Gesture_t4067939634 ** get_address_of_requireGestureToFail_20() { return &___requireGestureToFail_20; }
	inline void set_requireGestureToFail_20(Gesture_t4067939634 * value)
	{
		___requireGestureToFail_20 = value;
		Il2CppCodeGenWriteBarrier((&___requireGestureToFail_20), value);
	}

	inline static int32_t get_offset_of_friendlyGestures_21() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___friendlyGestures_21)); }
	inline List_1_t1245047080 * get_friendlyGestures_21() const { return ___friendlyGestures_21; }
	inline List_1_t1245047080 ** get_address_of_friendlyGestures_21() { return &___friendlyGestures_21; }
	inline void set_friendlyGestures_21(List_1_t1245047080 * value)
	{
		___friendlyGestures_21 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyGestures_21), value);
	}

	inline static int32_t get_offset_of_numTouches_22() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___numTouches_22)); }
	inline int32_t get_numTouches_22() const { return ___numTouches_22; }
	inline int32_t* get_address_of_numTouches_22() { return &___numTouches_22; }
	inline void set_numTouches_22(int32_t value)
	{
		___numTouches_22 = value;
	}

	inline static int32_t get_offset_of_layer_23() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___layer_23)); }
	inline TouchLayer_t1334725428 * get_layer_23() const { return ___layer_23; }
	inline TouchLayer_t1334725428 ** get_address_of_layer_23() { return &___layer_23; }
	inline void set_layer_23(TouchLayer_t1334725428 * value)
	{
		___layer_23 = value;
		Il2CppCodeGenWriteBarrier((&___layer_23), value);
	}

	inline static int32_t get_offset_of_readonlyActiveTouches_24() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___readonlyActiveTouches_24)); }
	inline ReadOnlyCollection_1_t2086468022 * get_readonlyActiveTouches_24() const { return ___readonlyActiveTouches_24; }
	inline ReadOnlyCollection_1_t2086468022 ** get_address_of_readonlyActiveTouches_24() { return &___readonlyActiveTouches_24; }
	inline void set_readonlyActiveTouches_24(ReadOnlyCollection_1_t2086468022 * value)
	{
		___readonlyActiveTouches_24 = value;
		Il2CppCodeGenWriteBarrier((&___readonlyActiveTouches_24), value);
	}

	inline static int32_t get_offset_of_touchSequence_25() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___touchSequence_25)); }
	inline TimedSequence_1_t1745006843 * get_touchSequence_25() const { return ___touchSequence_25; }
	inline TimedSequence_1_t1745006843 ** get_address_of_touchSequence_25() { return &___touchSequence_25; }
	inline void set_touchSequence_25(TimedSequence_1_t1745006843 * value)
	{
		___touchSequence_25 = value;
		Il2CppCodeGenWriteBarrier((&___touchSequence_25), value);
	}

	inline static int32_t get_offset_of_gestureManagerInstance_26() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___gestureManagerInstance_26)); }
	inline GestureManagerInstance_t955425494 * get_gestureManagerInstance_26() const { return ___gestureManagerInstance_26; }
	inline GestureManagerInstance_t955425494 ** get_address_of_gestureManagerInstance_26() { return &___gestureManagerInstance_26; }
	inline void set_gestureManagerInstance_26(GestureManagerInstance_t955425494 * value)
	{
		___gestureManagerInstance_26 = value;
		Il2CppCodeGenWriteBarrier((&___gestureManagerInstance_26), value);
	}

	inline static int32_t get_offset_of_delayedStateChange_27() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___delayedStateChange_27)); }
	inline int32_t get_delayedStateChange_27() const { return ___delayedStateChange_27; }
	inline int32_t* get_address_of_delayedStateChange_27() { return &___delayedStateChange_27; }
	inline void set_delayedStateChange_27(int32_t value)
	{
		___delayedStateChange_27 = value;
	}

	inline static int32_t get_offset_of_requiredGestureFailed_28() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___requiredGestureFailed_28)); }
	inline bool get_requiredGestureFailed_28() const { return ___requiredGestureFailed_28; }
	inline bool* get_address_of_requiredGestureFailed_28() { return &___requiredGestureFailed_28; }
	inline void set_requiredGestureFailed_28(bool value)
	{
		___requiredGestureFailed_28 = value;
	}

	inline static int32_t get_offset_of_state_29() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___state_29)); }
	inline int32_t get_state_29() const { return ___state_29; }
	inline int32_t* get_address_of_state_29() { return &___state_29; }
	inline void set_state_29(int32_t value)
	{
		___state_29 = value;
	}

	inline static int32_t get_offset_of_cachedScreenPosition_30() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedScreenPosition_30)); }
	inline Vector2_t2156229523  get_cachedScreenPosition_30() const { return ___cachedScreenPosition_30; }
	inline Vector2_t2156229523 * get_address_of_cachedScreenPosition_30() { return &___cachedScreenPosition_30; }
	inline void set_cachedScreenPosition_30(Vector2_t2156229523  value)
	{
		___cachedScreenPosition_30 = value;
	}

	inline static int32_t get_offset_of_cachedPreviousScreenPosition_31() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedPreviousScreenPosition_31)); }
	inline Vector2_t2156229523  get_cachedPreviousScreenPosition_31() const { return ___cachedPreviousScreenPosition_31; }
	inline Vector2_t2156229523 * get_address_of_cachedPreviousScreenPosition_31() { return &___cachedPreviousScreenPosition_31; }
	inline void set_cachedPreviousScreenPosition_31(Vector2_t2156229523  value)
	{
		___cachedPreviousScreenPosition_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_T4067939634_H
#ifndef MOUSEINPUT_T985431514_H
#define MOUSEINPUT_T985431514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.MouseInput
struct  MouseInput_t985431514  : public InputSource_t979757727
{
public:
	// System.Boolean TouchScript.InputSources.MouseInput::DisableOnMobilePlatforms
	bool ___DisableOnMobilePlatforms_5;
	// TouchScript.Tags TouchScript.InputSources.MouseInput::Tags
	Tags_t3560929397 * ___Tags_6;
	// TouchScript.InputSources.InputHandlers.MouseHandler TouchScript.InputSources.MouseInput::mouseHandler
	MouseHandler_t2265731789 * ___mouseHandler_7;

public:
	inline static int32_t get_offset_of_DisableOnMobilePlatforms_5() { return static_cast<int32_t>(offsetof(MouseInput_t985431514, ___DisableOnMobilePlatforms_5)); }
	inline bool get_DisableOnMobilePlatforms_5() const { return ___DisableOnMobilePlatforms_5; }
	inline bool* get_address_of_DisableOnMobilePlatforms_5() { return &___DisableOnMobilePlatforms_5; }
	inline void set_DisableOnMobilePlatforms_5(bool value)
	{
		___DisableOnMobilePlatforms_5 = value;
	}

	inline static int32_t get_offset_of_Tags_6() { return static_cast<int32_t>(offsetof(MouseInput_t985431514, ___Tags_6)); }
	inline Tags_t3560929397 * get_Tags_6() const { return ___Tags_6; }
	inline Tags_t3560929397 ** get_address_of_Tags_6() { return &___Tags_6; }
	inline void set_Tags_6(Tags_t3560929397 * value)
	{
		___Tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_6), value);
	}

	inline static int32_t get_offset_of_mouseHandler_7() { return static_cast<int32_t>(offsetof(MouseInput_t985431514, ___mouseHandler_7)); }
	inline MouseHandler_t2265731789 * get_mouseHandler_7() const { return ___mouseHandler_7; }
	inline MouseHandler_t2265731789 ** get_address_of_mouseHandler_7() { return &___mouseHandler_7; }
	inline void set_mouseHandler_7(MouseHandler_t2265731789 * value)
	{
		___mouseHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___mouseHandler_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEINPUT_T985431514_H
#ifndef TOUCHSCRIPTINPUTMODULE_T2128209366_H
#define TOUCHSCRIPTINPUTMODULE_T2128209366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.TouchScriptInputModule
struct  TouchScriptInputModule_t2128209366  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> TouchScript.Behaviors.TouchScriptInputModule::pointerEvents
	Dictionary_2_t2696614423 * ___pointerEvents_8;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::horizontalAxis
	String_t* ___horizontalAxis_9;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::verticalAxis
	String_t* ___verticalAxis_10;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::submitButton
	String_t* ___submitButton_11;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::cancelButton
	String_t* ___cancelButton_12;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::inputActionsPerSecond
	float ___inputActionsPerSecond_13;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::repeatDelay
	float ___repeatDelay_14;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::nextActionTime
	float ___nextActionTime_15;
	// UnityEngine.EventSystems.MoveDirection TouchScript.Behaviors.TouchScriptInputModule::lastMoveDirection
	int32_t ___lastMoveDirection_16;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::lastMoveStartTime
	float ___lastMoveStartTime_17;

public:
	inline static int32_t get_offset_of_pointerEvents_8() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___pointerEvents_8)); }
	inline Dictionary_2_t2696614423 * get_pointerEvents_8() const { return ___pointerEvents_8; }
	inline Dictionary_2_t2696614423 ** get_address_of_pointerEvents_8() { return &___pointerEvents_8; }
	inline void set_pointerEvents_8(Dictionary_2_t2696614423 * value)
	{
		___pointerEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointerEvents_8), value);
	}

	inline static int32_t get_offset_of_horizontalAxis_9() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___horizontalAxis_9)); }
	inline String_t* get_horizontalAxis_9() const { return ___horizontalAxis_9; }
	inline String_t** get_address_of_horizontalAxis_9() { return &___horizontalAxis_9; }
	inline void set_horizontalAxis_9(String_t* value)
	{
		___horizontalAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_9), value);
	}

	inline static int32_t get_offset_of_verticalAxis_10() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___verticalAxis_10)); }
	inline String_t* get_verticalAxis_10() const { return ___verticalAxis_10; }
	inline String_t** get_address_of_verticalAxis_10() { return &___verticalAxis_10; }
	inline void set_verticalAxis_10(String_t* value)
	{
		___verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_10), value);
	}

	inline static int32_t get_offset_of_submitButton_11() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___submitButton_11)); }
	inline String_t* get_submitButton_11() const { return ___submitButton_11; }
	inline String_t** get_address_of_submitButton_11() { return &___submitButton_11; }
	inline void set_submitButton_11(String_t* value)
	{
		___submitButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___submitButton_11), value);
	}

	inline static int32_t get_offset_of_cancelButton_12() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___cancelButton_12)); }
	inline String_t* get_cancelButton_12() const { return ___cancelButton_12; }
	inline String_t** get_address_of_cancelButton_12() { return &___cancelButton_12; }
	inline void set_cancelButton_12(String_t* value)
	{
		___cancelButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_12), value);
	}

	inline static int32_t get_offset_of_inputActionsPerSecond_13() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___inputActionsPerSecond_13)); }
	inline float get_inputActionsPerSecond_13() const { return ___inputActionsPerSecond_13; }
	inline float* get_address_of_inputActionsPerSecond_13() { return &___inputActionsPerSecond_13; }
	inline void set_inputActionsPerSecond_13(float value)
	{
		___inputActionsPerSecond_13 = value;
	}

	inline static int32_t get_offset_of_repeatDelay_14() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___repeatDelay_14)); }
	inline float get_repeatDelay_14() const { return ___repeatDelay_14; }
	inline float* get_address_of_repeatDelay_14() { return &___repeatDelay_14; }
	inline void set_repeatDelay_14(float value)
	{
		___repeatDelay_14 = value;
	}

	inline static int32_t get_offset_of_nextActionTime_15() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___nextActionTime_15)); }
	inline float get_nextActionTime_15() const { return ___nextActionTime_15; }
	inline float* get_address_of_nextActionTime_15() { return &___nextActionTime_15; }
	inline void set_nextActionTime_15(float value)
	{
		___nextActionTime_15 = value;
	}

	inline static int32_t get_offset_of_lastMoveDirection_16() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___lastMoveDirection_16)); }
	inline int32_t get_lastMoveDirection_16() const { return ___lastMoveDirection_16; }
	inline int32_t* get_address_of_lastMoveDirection_16() { return &___lastMoveDirection_16; }
	inline void set_lastMoveDirection_16(int32_t value)
	{
		___lastMoveDirection_16 = value;
	}

	inline static int32_t get_offset_of_lastMoveStartTime_17() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t2128209366, ___lastMoveStartTime_17)); }
	inline float get_lastMoveStartTime_17() const { return ___lastMoveStartTime_17; }
	inline float* get_address_of_lastMoveStartTime_17() { return &___lastMoveStartTime_17; }
	inline void set_lastMoveStartTime_17(float value)
	{
		___lastMoveStartTime_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCRIPTINPUTMODULE_T2128209366_H
#ifndef CAMERALAYER_T2201591754_H
#define CAMERALAYER_T2201591754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer
struct  CameraLayer_t2201591754  : public CameraLayerBase_t3093103062
{
public:
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit> TouchScript.Layers.CameraLayer::sortedHits
	List_1_t2528076708 * ___sortedHits_8;
	// UnityEngine.Transform TouchScript.Layers.CameraLayer::cachedTransform
	Transform_t3600365921 * ___cachedTransform_9;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.CameraLayer::tmpHitTestList
	List_1_t2630392082 * ___tmpHitTestList_10;

public:
	inline static int32_t get_offset_of_sortedHits_8() { return static_cast<int32_t>(offsetof(CameraLayer_t2201591754, ___sortedHits_8)); }
	inline List_1_t2528076708 * get_sortedHits_8() const { return ___sortedHits_8; }
	inline List_1_t2528076708 ** get_address_of_sortedHits_8() { return &___sortedHits_8; }
	inline void set_sortedHits_8(List_1_t2528076708 * value)
	{
		___sortedHits_8 = value;
		Il2CppCodeGenWriteBarrier((&___sortedHits_8), value);
	}

	inline static int32_t get_offset_of_cachedTransform_9() { return static_cast<int32_t>(offsetof(CameraLayer_t2201591754, ___cachedTransform_9)); }
	inline Transform_t3600365921 * get_cachedTransform_9() const { return ___cachedTransform_9; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_9() { return &___cachedTransform_9; }
	inline void set_cachedTransform_9(Transform_t3600365921 * value)
	{
		___cachedTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_9), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_10() { return static_cast<int32_t>(offsetof(CameraLayer_t2201591754, ___tmpHitTestList_10)); }
	inline List_1_t2630392082 * get_tmpHitTestList_10() const { return ___tmpHitTestList_10; }
	inline List_1_t2630392082 ** get_address_of_tmpHitTestList_10() { return &___tmpHitTestList_10; }
	inline void set_tmpHitTestList_10(List_1_t2630392082 * value)
	{
		___tmpHitTestList_10 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYER_T2201591754_H
#ifndef RELEASEGESTURE_T1591249992_H
#define RELEASEGESTURE_T1591249992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.ReleaseGesture
struct  ReleaseGesture_t1591249992  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.ReleaseGesture::releasedInvoker
	EventHandler_1_t1515976428 * ___releasedInvoker_33;
	// System.Boolean TouchScript.Gestures.ReleaseGesture::ignoreChildren
	bool ___ignoreChildren_34;

public:
	inline static int32_t get_offset_of_releasedInvoker_33() { return static_cast<int32_t>(offsetof(ReleaseGesture_t1591249992, ___releasedInvoker_33)); }
	inline EventHandler_1_t1515976428 * get_releasedInvoker_33() const { return ___releasedInvoker_33; }
	inline EventHandler_1_t1515976428 ** get_address_of_releasedInvoker_33() { return &___releasedInvoker_33; }
	inline void set_releasedInvoker_33(EventHandler_1_t1515976428 * value)
	{
		___releasedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___releasedInvoker_33), value);
	}

	inline static int32_t get_offset_of_ignoreChildren_34() { return static_cast<int32_t>(offsetof(ReleaseGesture_t1591249992, ___ignoreChildren_34)); }
	inline bool get_ignoreChildren_34() const { return ___ignoreChildren_34; }
	inline bool* get_address_of_ignoreChildren_34() { return &___ignoreChildren_34; }
	inline void set_ignoreChildren_34(bool value)
	{
		___ignoreChildren_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELEASEGESTURE_T1591249992_H
#ifndef PRESSGESTURE_T44453096_H
#define PRESSGESTURE_T44453096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PressGesture
struct  PressGesture_t44453096  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.PressGesture::pressedInvoker
	EventHandler_1_t1515976428 * ___pressedInvoker_33;
	// System.Boolean TouchScript.Gestures.PressGesture::ignoreChildren
	bool ___ignoreChildren_34;

public:
	inline static int32_t get_offset_of_pressedInvoker_33() { return static_cast<int32_t>(offsetof(PressGesture_t44453096, ___pressedInvoker_33)); }
	inline EventHandler_1_t1515976428 * get_pressedInvoker_33() const { return ___pressedInvoker_33; }
	inline EventHandler_1_t1515976428 ** get_address_of_pressedInvoker_33() { return &___pressedInvoker_33; }
	inline void set_pressedInvoker_33(EventHandler_1_t1515976428 * value)
	{
		___pressedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___pressedInvoker_33), value);
	}

	inline static int32_t get_offset_of_ignoreChildren_34() { return static_cast<int32_t>(offsetof(PressGesture_t44453096, ___ignoreChildren_34)); }
	inline bool get_ignoreChildren_34() const { return ___ignoreChildren_34; }
	inline bool* get_address_of_ignoreChildren_34() { return &___ignoreChildren_34; }
	inline void set_ignoreChildren_34(bool value)
	{
		___ignoreChildren_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSGESTURE_T44453096_H
#ifndef TAPGESTURE_T1114317921_H
#define TAPGESTURE_T1114317921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TapGesture
struct  TapGesture_t1114317921  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.TapGesture::tappedInvoker
	EventHandler_1_t1515976428 * ___tappedInvoker_33;
	// System.Int32 TouchScript.Gestures.TapGesture::numberOfTapsRequired
	int32_t ___numberOfTapsRequired_34;
	// System.Single TouchScript.Gestures.TapGesture::timeLimit
	float ___timeLimit_35;
	// System.Single TouchScript.Gestures.TapGesture::distanceLimit
	float ___distanceLimit_36;
	// System.Single TouchScript.Gestures.TapGesture::distanceLimitInPixelsSquared
	float ___distanceLimitInPixelsSquared_37;
	// System.Boolean TouchScript.Gestures.TapGesture::isActive
	bool ___isActive_38;
	// System.Int32 TouchScript.Gestures.TapGesture::tapsDone
	int32_t ___tapsDone_39;
	// UnityEngine.Vector2 TouchScript.Gestures.TapGesture::startPosition
	Vector2_t2156229523  ___startPosition_40;
	// UnityEngine.Vector2 TouchScript.Gestures.TapGesture::totalMovement
	Vector2_t2156229523  ___totalMovement_41;

public:
	inline static int32_t get_offset_of_tappedInvoker_33() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___tappedInvoker_33)); }
	inline EventHandler_1_t1515976428 * get_tappedInvoker_33() const { return ___tappedInvoker_33; }
	inline EventHandler_1_t1515976428 ** get_address_of_tappedInvoker_33() { return &___tappedInvoker_33; }
	inline void set_tappedInvoker_33(EventHandler_1_t1515976428 * value)
	{
		___tappedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___tappedInvoker_33), value);
	}

	inline static int32_t get_offset_of_numberOfTapsRequired_34() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___numberOfTapsRequired_34)); }
	inline int32_t get_numberOfTapsRequired_34() const { return ___numberOfTapsRequired_34; }
	inline int32_t* get_address_of_numberOfTapsRequired_34() { return &___numberOfTapsRequired_34; }
	inline void set_numberOfTapsRequired_34(int32_t value)
	{
		___numberOfTapsRequired_34 = value;
	}

	inline static int32_t get_offset_of_timeLimit_35() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___timeLimit_35)); }
	inline float get_timeLimit_35() const { return ___timeLimit_35; }
	inline float* get_address_of_timeLimit_35() { return &___timeLimit_35; }
	inline void set_timeLimit_35(float value)
	{
		___timeLimit_35 = value;
	}

	inline static int32_t get_offset_of_distanceLimit_36() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___distanceLimit_36)); }
	inline float get_distanceLimit_36() const { return ___distanceLimit_36; }
	inline float* get_address_of_distanceLimit_36() { return &___distanceLimit_36; }
	inline void set_distanceLimit_36(float value)
	{
		___distanceLimit_36 = value;
	}

	inline static int32_t get_offset_of_distanceLimitInPixelsSquared_37() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___distanceLimitInPixelsSquared_37)); }
	inline float get_distanceLimitInPixelsSquared_37() const { return ___distanceLimitInPixelsSquared_37; }
	inline float* get_address_of_distanceLimitInPixelsSquared_37() { return &___distanceLimitInPixelsSquared_37; }
	inline void set_distanceLimitInPixelsSquared_37(float value)
	{
		___distanceLimitInPixelsSquared_37 = value;
	}

	inline static int32_t get_offset_of_isActive_38() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___isActive_38)); }
	inline bool get_isActive_38() const { return ___isActive_38; }
	inline bool* get_address_of_isActive_38() { return &___isActive_38; }
	inline void set_isActive_38(bool value)
	{
		___isActive_38 = value;
	}

	inline static int32_t get_offset_of_tapsDone_39() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___tapsDone_39)); }
	inline int32_t get_tapsDone_39() const { return ___tapsDone_39; }
	inline int32_t* get_address_of_tapsDone_39() { return &___tapsDone_39; }
	inline void set_tapsDone_39(int32_t value)
	{
		___tapsDone_39 = value;
	}

	inline static int32_t get_offset_of_startPosition_40() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___startPosition_40)); }
	inline Vector2_t2156229523  get_startPosition_40() const { return ___startPosition_40; }
	inline Vector2_t2156229523 * get_address_of_startPosition_40() { return &___startPosition_40; }
	inline void set_startPosition_40(Vector2_t2156229523  value)
	{
		___startPosition_40 = value;
	}

	inline static int32_t get_offset_of_totalMovement_41() { return static_cast<int32_t>(offsetof(TapGesture_t1114317921, ___totalMovement_41)); }
	inline Vector2_t2156229523  get_totalMovement_41() const { return ___totalMovement_41; }
	inline Vector2_t2156229523 * get_address_of_totalMovement_41() { return &___totalMovement_41; }
	inline void set_totalMovement_41(Vector2_t2156229523  value)
	{
		___totalMovement_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURE_T1114317921_H
#ifndef CAMERALAYER2D_T2236291724_H
#define CAMERALAYER2D_T2236291724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer2D
struct  CameraLayer2D_t2236291724  : public CameraLayerBase_t3093103062
{
public:
	// System.Int32[] TouchScript.Layers.CameraLayer2D::layerIds
	Int32U5BU5D_t385246372* ___layerIds_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TouchScript.Layers.CameraLayer2D::layerById
	Dictionary_2_t1839659084 * ___layerById_9;
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D> TouchScript.Layers.CameraLayer2D::sortedHits
	List_1_t3751656731 * ___sortedHits_10;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.CameraLayer2D::tmpHitTestList
	List_1_t2630392082 * ___tmpHitTestList_11;

public:
	inline static int32_t get_offset_of_layerIds_8() { return static_cast<int32_t>(offsetof(CameraLayer2D_t2236291724, ___layerIds_8)); }
	inline Int32U5BU5D_t385246372* get_layerIds_8() const { return ___layerIds_8; }
	inline Int32U5BU5D_t385246372** get_address_of_layerIds_8() { return &___layerIds_8; }
	inline void set_layerIds_8(Int32U5BU5D_t385246372* value)
	{
		___layerIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerIds_8), value);
	}

	inline static int32_t get_offset_of_layerById_9() { return static_cast<int32_t>(offsetof(CameraLayer2D_t2236291724, ___layerById_9)); }
	inline Dictionary_2_t1839659084 * get_layerById_9() const { return ___layerById_9; }
	inline Dictionary_2_t1839659084 ** get_address_of_layerById_9() { return &___layerById_9; }
	inline void set_layerById_9(Dictionary_2_t1839659084 * value)
	{
		___layerById_9 = value;
		Il2CppCodeGenWriteBarrier((&___layerById_9), value);
	}

	inline static int32_t get_offset_of_sortedHits_10() { return static_cast<int32_t>(offsetof(CameraLayer2D_t2236291724, ___sortedHits_10)); }
	inline List_1_t3751656731 * get_sortedHits_10() const { return ___sortedHits_10; }
	inline List_1_t3751656731 ** get_address_of_sortedHits_10() { return &___sortedHits_10; }
	inline void set_sortedHits_10(List_1_t3751656731 * value)
	{
		___sortedHits_10 = value;
		Il2CppCodeGenWriteBarrier((&___sortedHits_10), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_11() { return static_cast<int32_t>(offsetof(CameraLayer2D_t2236291724, ___tmpHitTestList_11)); }
	inline List_1_t2630392082 * get_tmpHitTestList_11() const { return ___tmpHitTestList_11; }
	inline List_1_t2630392082 ** get_address_of_tmpHitTestList_11() { return &___tmpHitTestList_11; }
	inline void set_tmpHitTestList_11(List_1_t2630392082 * value)
	{
		___tmpHitTestList_11 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYER2D_T2236291724_H
#ifndef UIGESTURE_T4257215238_H
#define UIGESTURE_T4257215238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.UI.UIGesture
struct  UIGesture_t4257215238  : public Gesture_t4067939634
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData> TouchScript.Gestures.UI.UIGesture::pointerData
	Dictionary_2_t1693108292 * ___pointerData_32;

public:
	inline static int32_t get_offset_of_pointerData_32() { return static_cast<int32_t>(offsetof(UIGesture_t4257215238, ___pointerData_32)); }
	inline Dictionary_2_t1693108292 * get_pointerData_32() const { return ___pointerData_32; }
	inline Dictionary_2_t1693108292 ** get_address_of_pointerData_32() { return &___pointerData_32; }
	inline void set_pointerData_32(Dictionary_2_t1693108292 * value)
	{
		___pointerData_32 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGESTURE_T4257215238_H
#ifndef FLICKGESTURE_T2013564523_H
#define FLICKGESTURE_T2013564523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.FlickGesture
struct  FlickGesture_t2013564523  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.FlickGesture::flickedInvoker
	EventHandler_1_t1515976428 * ___flickedInvoker_33;
	// UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::<ScreenFlickVector>k__BackingField
	Vector2_t2156229523  ___U3CScreenFlickVectorU3Ek__BackingField_34;
	// System.Single TouchScript.Gestures.FlickGesture::<ScreenFlickTime>k__BackingField
	float ___U3CScreenFlickTimeU3Ek__BackingField_35;
	// System.Single TouchScript.Gestures.FlickGesture::flickTime
	float ___flickTime_36;
	// System.Single TouchScript.Gestures.FlickGesture::minDistance
	float ___minDistance_37;
	// System.Single TouchScript.Gestures.FlickGesture::movementThreshold
	float ___movementThreshold_38;
	// TouchScript.Gestures.FlickGesture/GestureDirection TouchScript.Gestures.FlickGesture::direction
	int32_t ___direction_39;
	// System.Boolean TouchScript.Gestures.FlickGesture::moving
	bool ___moving_40;
	// UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::movementBuffer
	Vector2_t2156229523  ___movementBuffer_41;
	// System.Boolean TouchScript.Gestures.FlickGesture::isActive
	bool ___isActive_42;
	// TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2> TouchScript.Gestures.FlickGesture::deltaSequence
	TimedSequence_1_t3027344631 * ___deltaSequence_43;

public:
	inline static int32_t get_offset_of_flickedInvoker_33() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___flickedInvoker_33)); }
	inline EventHandler_1_t1515976428 * get_flickedInvoker_33() const { return ___flickedInvoker_33; }
	inline EventHandler_1_t1515976428 ** get_address_of_flickedInvoker_33() { return &___flickedInvoker_33; }
	inline void set_flickedInvoker_33(EventHandler_1_t1515976428 * value)
	{
		___flickedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___flickedInvoker_33), value);
	}

	inline static int32_t get_offset_of_U3CScreenFlickVectorU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___U3CScreenFlickVectorU3Ek__BackingField_34)); }
	inline Vector2_t2156229523  get_U3CScreenFlickVectorU3Ek__BackingField_34() const { return ___U3CScreenFlickVectorU3Ek__BackingField_34; }
	inline Vector2_t2156229523 * get_address_of_U3CScreenFlickVectorU3Ek__BackingField_34() { return &___U3CScreenFlickVectorU3Ek__BackingField_34; }
	inline void set_U3CScreenFlickVectorU3Ek__BackingField_34(Vector2_t2156229523  value)
	{
		___U3CScreenFlickVectorU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CScreenFlickTimeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___U3CScreenFlickTimeU3Ek__BackingField_35)); }
	inline float get_U3CScreenFlickTimeU3Ek__BackingField_35() const { return ___U3CScreenFlickTimeU3Ek__BackingField_35; }
	inline float* get_address_of_U3CScreenFlickTimeU3Ek__BackingField_35() { return &___U3CScreenFlickTimeU3Ek__BackingField_35; }
	inline void set_U3CScreenFlickTimeU3Ek__BackingField_35(float value)
	{
		___U3CScreenFlickTimeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_flickTime_36() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___flickTime_36)); }
	inline float get_flickTime_36() const { return ___flickTime_36; }
	inline float* get_address_of_flickTime_36() { return &___flickTime_36; }
	inline void set_flickTime_36(float value)
	{
		___flickTime_36 = value;
	}

	inline static int32_t get_offset_of_minDistance_37() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___minDistance_37)); }
	inline float get_minDistance_37() const { return ___minDistance_37; }
	inline float* get_address_of_minDistance_37() { return &___minDistance_37; }
	inline void set_minDistance_37(float value)
	{
		___minDistance_37 = value;
	}

	inline static int32_t get_offset_of_movementThreshold_38() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___movementThreshold_38)); }
	inline float get_movementThreshold_38() const { return ___movementThreshold_38; }
	inline float* get_address_of_movementThreshold_38() { return &___movementThreshold_38; }
	inline void set_movementThreshold_38(float value)
	{
		___movementThreshold_38 = value;
	}

	inline static int32_t get_offset_of_direction_39() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___direction_39)); }
	inline int32_t get_direction_39() const { return ___direction_39; }
	inline int32_t* get_address_of_direction_39() { return &___direction_39; }
	inline void set_direction_39(int32_t value)
	{
		___direction_39 = value;
	}

	inline static int32_t get_offset_of_moving_40() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___moving_40)); }
	inline bool get_moving_40() const { return ___moving_40; }
	inline bool* get_address_of_moving_40() { return &___moving_40; }
	inline void set_moving_40(bool value)
	{
		___moving_40 = value;
	}

	inline static int32_t get_offset_of_movementBuffer_41() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___movementBuffer_41)); }
	inline Vector2_t2156229523  get_movementBuffer_41() const { return ___movementBuffer_41; }
	inline Vector2_t2156229523 * get_address_of_movementBuffer_41() { return &___movementBuffer_41; }
	inline void set_movementBuffer_41(Vector2_t2156229523  value)
	{
		___movementBuffer_41 = value;
	}

	inline static int32_t get_offset_of_isActive_42() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___isActive_42)); }
	inline bool get_isActive_42() const { return ___isActive_42; }
	inline bool* get_address_of_isActive_42() { return &___isActive_42; }
	inline void set_isActive_42(bool value)
	{
		___isActive_42 = value;
	}

	inline static int32_t get_offset_of_deltaSequence_43() { return static_cast<int32_t>(offsetof(FlickGesture_t2013564523, ___deltaSequence_43)); }
	inline TimedSequence_1_t3027344631 * get_deltaSequence_43() const { return ___deltaSequence_43; }
	inline TimedSequence_1_t3027344631 ** get_address_of_deltaSequence_43() { return &___deltaSequence_43; }
	inline void set_deltaSequence_43(TimedSequence_1_t3027344631 * value)
	{
		___deltaSequence_43 = value;
		Il2CppCodeGenWriteBarrier((&___deltaSequence_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLICKGESTURE_T2013564523_H
#ifndef TRANSFORMGESTUREBASE_T2879339428_H
#define TRANSFORMGESTUREBASE_T2879339428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase
struct  TransformGestureBase_t2879339428  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformStartedInvoker
	EventHandler_1_t1515976428 * ___transformStartedInvoker_35;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformedInvoker
	EventHandler_1_t1515976428 * ___transformedInvoker_36;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformCompletedInvoker
	EventHandler_1_t1515976428 * ___transformCompletedInvoker_37;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistance
	float ___minScreenPointsPixelDistance_38;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistanceSquared
	float ___minScreenPointsPixelDistanceSquared_39;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThreshold
	float ___screenTransformPixelThreshold_40;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThresholdSquared
	float ___screenTransformPixelThresholdSquared_41;
	// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::deltaPosition
	Vector3_t3722313464  ___deltaPosition_42;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaRotation
	float ___deltaRotation_43;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaScale
	float ___deltaScale_44;
	// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::screenPixelTranslationBuffer
	Vector2_t2156229523  ___screenPixelTranslationBuffer_45;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelRotationBuffer
	float ___screenPixelRotationBuffer_46;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::angleBuffer
	float ___angleBuffer_47;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelScalingBuffer
	float ___screenPixelScalingBuffer_48;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::scaleBuffer
	float ___scaleBuffer_49;
	// System.Boolean TouchScript.Gestures.Base.TransformGestureBase::isTransforming
	bool ___isTransforming_50;
	// TouchScript.Gestures.Base.TransformGestureBase/TransformType TouchScript.Gestures.Base.TransformGestureBase::type
	int32_t ___type_51;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsDistance
	float ___minScreenPointsDistance_52;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformThreshold
	float ___screenTransformThreshold_53;

public:
	inline static int32_t get_offset_of_transformStartedInvoker_35() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformStartedInvoker_35)); }
	inline EventHandler_1_t1515976428 * get_transformStartedInvoker_35() const { return ___transformStartedInvoker_35; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformStartedInvoker_35() { return &___transformStartedInvoker_35; }
	inline void set_transformStartedInvoker_35(EventHandler_1_t1515976428 * value)
	{
		___transformStartedInvoker_35 = value;
		Il2CppCodeGenWriteBarrier((&___transformStartedInvoker_35), value);
	}

	inline static int32_t get_offset_of_transformedInvoker_36() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformedInvoker_36)); }
	inline EventHandler_1_t1515976428 * get_transformedInvoker_36() const { return ___transformedInvoker_36; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformedInvoker_36() { return &___transformedInvoker_36; }
	inline void set_transformedInvoker_36(EventHandler_1_t1515976428 * value)
	{
		___transformedInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___transformedInvoker_36), value);
	}

	inline static int32_t get_offset_of_transformCompletedInvoker_37() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformCompletedInvoker_37)); }
	inline EventHandler_1_t1515976428 * get_transformCompletedInvoker_37() const { return ___transformCompletedInvoker_37; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformCompletedInvoker_37() { return &___transformCompletedInvoker_37; }
	inline void set_transformCompletedInvoker_37(EventHandler_1_t1515976428 * value)
	{
		___transformCompletedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___transformCompletedInvoker_37), value);
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistance_38() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsPixelDistance_38)); }
	inline float get_minScreenPointsPixelDistance_38() const { return ___minScreenPointsPixelDistance_38; }
	inline float* get_address_of_minScreenPointsPixelDistance_38() { return &___minScreenPointsPixelDistance_38; }
	inline void set_minScreenPointsPixelDistance_38(float value)
	{
		___minScreenPointsPixelDistance_38 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistanceSquared_39() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsPixelDistanceSquared_39)); }
	inline float get_minScreenPointsPixelDistanceSquared_39() const { return ___minScreenPointsPixelDistanceSquared_39; }
	inline float* get_address_of_minScreenPointsPixelDistanceSquared_39() { return &___minScreenPointsPixelDistanceSquared_39; }
	inline void set_minScreenPointsPixelDistanceSquared_39(float value)
	{
		___minScreenPointsPixelDistanceSquared_39 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThreshold_40() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformPixelThreshold_40)); }
	inline float get_screenTransformPixelThreshold_40() const { return ___screenTransformPixelThreshold_40; }
	inline float* get_address_of_screenTransformPixelThreshold_40() { return &___screenTransformPixelThreshold_40; }
	inline void set_screenTransformPixelThreshold_40(float value)
	{
		___screenTransformPixelThreshold_40 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThresholdSquared_41() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformPixelThresholdSquared_41)); }
	inline float get_screenTransformPixelThresholdSquared_41() const { return ___screenTransformPixelThresholdSquared_41; }
	inline float* get_address_of_screenTransformPixelThresholdSquared_41() { return &___screenTransformPixelThresholdSquared_41; }
	inline void set_screenTransformPixelThresholdSquared_41(float value)
	{
		___screenTransformPixelThresholdSquared_41 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_42() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaPosition_42)); }
	inline Vector3_t3722313464  get_deltaPosition_42() const { return ___deltaPosition_42; }
	inline Vector3_t3722313464 * get_address_of_deltaPosition_42() { return &___deltaPosition_42; }
	inline void set_deltaPosition_42(Vector3_t3722313464  value)
	{
		___deltaPosition_42 = value;
	}

	inline static int32_t get_offset_of_deltaRotation_43() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaRotation_43)); }
	inline float get_deltaRotation_43() const { return ___deltaRotation_43; }
	inline float* get_address_of_deltaRotation_43() { return &___deltaRotation_43; }
	inline void set_deltaRotation_43(float value)
	{
		___deltaRotation_43 = value;
	}

	inline static int32_t get_offset_of_deltaScale_44() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaScale_44)); }
	inline float get_deltaScale_44() const { return ___deltaScale_44; }
	inline float* get_address_of_deltaScale_44() { return &___deltaScale_44; }
	inline void set_deltaScale_44(float value)
	{
		___deltaScale_44 = value;
	}

	inline static int32_t get_offset_of_screenPixelTranslationBuffer_45() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelTranslationBuffer_45)); }
	inline Vector2_t2156229523  get_screenPixelTranslationBuffer_45() const { return ___screenPixelTranslationBuffer_45; }
	inline Vector2_t2156229523 * get_address_of_screenPixelTranslationBuffer_45() { return &___screenPixelTranslationBuffer_45; }
	inline void set_screenPixelTranslationBuffer_45(Vector2_t2156229523  value)
	{
		___screenPixelTranslationBuffer_45 = value;
	}

	inline static int32_t get_offset_of_screenPixelRotationBuffer_46() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelRotationBuffer_46)); }
	inline float get_screenPixelRotationBuffer_46() const { return ___screenPixelRotationBuffer_46; }
	inline float* get_address_of_screenPixelRotationBuffer_46() { return &___screenPixelRotationBuffer_46; }
	inline void set_screenPixelRotationBuffer_46(float value)
	{
		___screenPixelRotationBuffer_46 = value;
	}

	inline static int32_t get_offset_of_angleBuffer_47() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___angleBuffer_47)); }
	inline float get_angleBuffer_47() const { return ___angleBuffer_47; }
	inline float* get_address_of_angleBuffer_47() { return &___angleBuffer_47; }
	inline void set_angleBuffer_47(float value)
	{
		___angleBuffer_47 = value;
	}

	inline static int32_t get_offset_of_screenPixelScalingBuffer_48() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelScalingBuffer_48)); }
	inline float get_screenPixelScalingBuffer_48() const { return ___screenPixelScalingBuffer_48; }
	inline float* get_address_of_screenPixelScalingBuffer_48() { return &___screenPixelScalingBuffer_48; }
	inline void set_screenPixelScalingBuffer_48(float value)
	{
		___screenPixelScalingBuffer_48 = value;
	}

	inline static int32_t get_offset_of_scaleBuffer_49() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___scaleBuffer_49)); }
	inline float get_scaleBuffer_49() const { return ___scaleBuffer_49; }
	inline float* get_address_of_scaleBuffer_49() { return &___scaleBuffer_49; }
	inline void set_scaleBuffer_49(float value)
	{
		___scaleBuffer_49 = value;
	}

	inline static int32_t get_offset_of_isTransforming_50() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___isTransforming_50)); }
	inline bool get_isTransforming_50() const { return ___isTransforming_50; }
	inline bool* get_address_of_isTransforming_50() { return &___isTransforming_50; }
	inline void set_isTransforming_50(bool value)
	{
		___isTransforming_50 = value;
	}

	inline static int32_t get_offset_of_type_51() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___type_51)); }
	inline int32_t get_type_51() const { return ___type_51; }
	inline int32_t* get_address_of_type_51() { return &___type_51; }
	inline void set_type_51(int32_t value)
	{
		___type_51 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsDistance_52() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsDistance_52)); }
	inline float get_minScreenPointsDistance_52() const { return ___minScreenPointsDistance_52; }
	inline float* get_address_of_minScreenPointsDistance_52() { return &___minScreenPointsDistance_52; }
	inline void set_minScreenPointsDistance_52(float value)
	{
		___minScreenPointsDistance_52 = value;
	}

	inline static int32_t get_offset_of_screenTransformThreshold_53() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformThreshold_53)); }
	inline float get_screenTransformThreshold_53() const { return ___screenTransformThreshold_53; }
	inline float* get_address_of_screenTransformThreshold_53() { return &___screenTransformThreshold_53; }
	inline void set_screenTransformThreshold_53(float value)
	{
		___screenTransformThreshold_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMGESTUREBASE_T2879339428_H
#ifndef LONGPRESSGESTURE_T3651199768_H
#define LONGPRESSGESTURE_T3651199768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.LongPressGesture
struct  LongPressGesture_t3651199768  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.LongPressGesture::longPressedInvoker
	EventHandler_1_t1515976428 * ___longPressedInvoker_33;
	// System.Single TouchScript.Gestures.LongPressGesture::timeToPress
	float ___timeToPress_34;
	// System.Single TouchScript.Gestures.LongPressGesture::distanceLimit
	float ___distanceLimit_35;
	// System.Single TouchScript.Gestures.LongPressGesture::distanceLimitInPixelsSquared
	float ___distanceLimitInPixelsSquared_36;
	// UnityEngine.Vector2 TouchScript.Gestures.LongPressGesture::totalMovement
	Vector2_t2156229523  ___totalMovement_37;

public:
	inline static int32_t get_offset_of_longPressedInvoker_33() { return static_cast<int32_t>(offsetof(LongPressGesture_t3651199768, ___longPressedInvoker_33)); }
	inline EventHandler_1_t1515976428 * get_longPressedInvoker_33() const { return ___longPressedInvoker_33; }
	inline EventHandler_1_t1515976428 ** get_address_of_longPressedInvoker_33() { return &___longPressedInvoker_33; }
	inline void set_longPressedInvoker_33(EventHandler_1_t1515976428 * value)
	{
		___longPressedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___longPressedInvoker_33), value);
	}

	inline static int32_t get_offset_of_timeToPress_34() { return static_cast<int32_t>(offsetof(LongPressGesture_t3651199768, ___timeToPress_34)); }
	inline float get_timeToPress_34() const { return ___timeToPress_34; }
	inline float* get_address_of_timeToPress_34() { return &___timeToPress_34; }
	inline void set_timeToPress_34(float value)
	{
		___timeToPress_34 = value;
	}

	inline static int32_t get_offset_of_distanceLimit_35() { return static_cast<int32_t>(offsetof(LongPressGesture_t3651199768, ___distanceLimit_35)); }
	inline float get_distanceLimit_35() const { return ___distanceLimit_35; }
	inline float* get_address_of_distanceLimit_35() { return &___distanceLimit_35; }
	inline void set_distanceLimit_35(float value)
	{
		___distanceLimit_35 = value;
	}

	inline static int32_t get_offset_of_distanceLimitInPixelsSquared_36() { return static_cast<int32_t>(offsetof(LongPressGesture_t3651199768, ___distanceLimitInPixelsSquared_36)); }
	inline float get_distanceLimitInPixelsSquared_36() const { return ___distanceLimitInPixelsSquared_36; }
	inline float* get_address_of_distanceLimitInPixelsSquared_36() { return &___distanceLimitInPixelsSquared_36; }
	inline void set_distanceLimitInPixelsSquared_36(float value)
	{
		___distanceLimitInPixelsSquared_36 = value;
	}

	inline static int32_t get_offset_of_totalMovement_37() { return static_cast<int32_t>(offsetof(LongPressGesture_t3651199768, ___totalMovement_37)); }
	inline Vector2_t2156229523  get_totalMovement_37() const { return ___totalMovement_37; }
	inline Vector2_t2156229523 * get_address_of_totalMovement_37() { return &___totalMovement_37; }
	inline void set_totalMovement_37(Vector2_t2156229523  value)
	{
		___totalMovement_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPRESSGESTURE_T3651199768_H
#ifndef PINNEDTRASFORMGESTUREBASE_T533205172_H
#define PINNEDTRASFORMGESTUREBASE_T533205172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.PinnedTrasformGestureBase
struct  PinnedTrasformGestureBase_t533205172  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformStartedInvoker
	EventHandler_1_t1515976428 * ___transformStartedInvoker_35;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformedInvoker
	EventHandler_1_t1515976428 * ___transformedInvoker_36;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformCompletedInvoker
	EventHandler_1_t1515976428 * ___transformCompletedInvoker_37;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformPixelThreshold
	float ___screenTransformPixelThreshold_38;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformPixelThresholdSquared
	float ___screenTransformPixelThresholdSquared_39;
	// UnityEngine.Collider TouchScript.Gestures.Base.PinnedTrasformGestureBase::cachedCollider
	Collider_t1773347010 * ___cachedCollider_40;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::deltaRotation
	float ___deltaRotation_41;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::deltaScale
	float ___deltaScale_42;
	// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelTranslationBuffer
	Vector2_t2156229523  ___screenPixelTranslationBuffer_43;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelRotationBuffer
	float ___screenPixelRotationBuffer_44;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::angleBuffer
	float ___angleBuffer_45;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelScalingBuffer
	float ___screenPixelScalingBuffer_46;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::scaleBuffer
	float ___scaleBuffer_47;
	// System.Boolean TouchScript.Gestures.Base.PinnedTrasformGestureBase::isTransforming
	bool ___isTransforming_48;
	// TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType TouchScript.Gestures.Base.PinnedTrasformGestureBase::type
	int32_t ___type_49;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformThreshold
	float ___screenTransformThreshold_50;

public:
	inline static int32_t get_offset_of_transformStartedInvoker_35() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___transformStartedInvoker_35)); }
	inline EventHandler_1_t1515976428 * get_transformStartedInvoker_35() const { return ___transformStartedInvoker_35; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformStartedInvoker_35() { return &___transformStartedInvoker_35; }
	inline void set_transformStartedInvoker_35(EventHandler_1_t1515976428 * value)
	{
		___transformStartedInvoker_35 = value;
		Il2CppCodeGenWriteBarrier((&___transformStartedInvoker_35), value);
	}

	inline static int32_t get_offset_of_transformedInvoker_36() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___transformedInvoker_36)); }
	inline EventHandler_1_t1515976428 * get_transformedInvoker_36() const { return ___transformedInvoker_36; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformedInvoker_36() { return &___transformedInvoker_36; }
	inline void set_transformedInvoker_36(EventHandler_1_t1515976428 * value)
	{
		___transformedInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___transformedInvoker_36), value);
	}

	inline static int32_t get_offset_of_transformCompletedInvoker_37() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___transformCompletedInvoker_37)); }
	inline EventHandler_1_t1515976428 * get_transformCompletedInvoker_37() const { return ___transformCompletedInvoker_37; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformCompletedInvoker_37() { return &___transformCompletedInvoker_37; }
	inline void set_transformCompletedInvoker_37(EventHandler_1_t1515976428 * value)
	{
		___transformCompletedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___transformCompletedInvoker_37), value);
	}

	inline static int32_t get_offset_of_screenTransformPixelThreshold_38() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenTransformPixelThreshold_38)); }
	inline float get_screenTransformPixelThreshold_38() const { return ___screenTransformPixelThreshold_38; }
	inline float* get_address_of_screenTransformPixelThreshold_38() { return &___screenTransformPixelThreshold_38; }
	inline void set_screenTransformPixelThreshold_38(float value)
	{
		___screenTransformPixelThreshold_38 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThresholdSquared_39() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenTransformPixelThresholdSquared_39)); }
	inline float get_screenTransformPixelThresholdSquared_39() const { return ___screenTransformPixelThresholdSquared_39; }
	inline float* get_address_of_screenTransformPixelThresholdSquared_39() { return &___screenTransformPixelThresholdSquared_39; }
	inline void set_screenTransformPixelThresholdSquared_39(float value)
	{
		___screenTransformPixelThresholdSquared_39 = value;
	}

	inline static int32_t get_offset_of_cachedCollider_40() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___cachedCollider_40)); }
	inline Collider_t1773347010 * get_cachedCollider_40() const { return ___cachedCollider_40; }
	inline Collider_t1773347010 ** get_address_of_cachedCollider_40() { return &___cachedCollider_40; }
	inline void set_cachedCollider_40(Collider_t1773347010 * value)
	{
		___cachedCollider_40 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCollider_40), value);
	}

	inline static int32_t get_offset_of_deltaRotation_41() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___deltaRotation_41)); }
	inline float get_deltaRotation_41() const { return ___deltaRotation_41; }
	inline float* get_address_of_deltaRotation_41() { return &___deltaRotation_41; }
	inline void set_deltaRotation_41(float value)
	{
		___deltaRotation_41 = value;
	}

	inline static int32_t get_offset_of_deltaScale_42() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___deltaScale_42)); }
	inline float get_deltaScale_42() const { return ___deltaScale_42; }
	inline float* get_address_of_deltaScale_42() { return &___deltaScale_42; }
	inline void set_deltaScale_42(float value)
	{
		___deltaScale_42 = value;
	}

	inline static int32_t get_offset_of_screenPixelTranslationBuffer_43() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenPixelTranslationBuffer_43)); }
	inline Vector2_t2156229523  get_screenPixelTranslationBuffer_43() const { return ___screenPixelTranslationBuffer_43; }
	inline Vector2_t2156229523 * get_address_of_screenPixelTranslationBuffer_43() { return &___screenPixelTranslationBuffer_43; }
	inline void set_screenPixelTranslationBuffer_43(Vector2_t2156229523  value)
	{
		___screenPixelTranslationBuffer_43 = value;
	}

	inline static int32_t get_offset_of_screenPixelRotationBuffer_44() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenPixelRotationBuffer_44)); }
	inline float get_screenPixelRotationBuffer_44() const { return ___screenPixelRotationBuffer_44; }
	inline float* get_address_of_screenPixelRotationBuffer_44() { return &___screenPixelRotationBuffer_44; }
	inline void set_screenPixelRotationBuffer_44(float value)
	{
		___screenPixelRotationBuffer_44 = value;
	}

	inline static int32_t get_offset_of_angleBuffer_45() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___angleBuffer_45)); }
	inline float get_angleBuffer_45() const { return ___angleBuffer_45; }
	inline float* get_address_of_angleBuffer_45() { return &___angleBuffer_45; }
	inline void set_angleBuffer_45(float value)
	{
		___angleBuffer_45 = value;
	}

	inline static int32_t get_offset_of_screenPixelScalingBuffer_46() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenPixelScalingBuffer_46)); }
	inline float get_screenPixelScalingBuffer_46() const { return ___screenPixelScalingBuffer_46; }
	inline float* get_address_of_screenPixelScalingBuffer_46() { return &___screenPixelScalingBuffer_46; }
	inline void set_screenPixelScalingBuffer_46(float value)
	{
		___screenPixelScalingBuffer_46 = value;
	}

	inline static int32_t get_offset_of_scaleBuffer_47() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___scaleBuffer_47)); }
	inline float get_scaleBuffer_47() const { return ___scaleBuffer_47; }
	inline float* get_address_of_scaleBuffer_47() { return &___scaleBuffer_47; }
	inline void set_scaleBuffer_47(float value)
	{
		___scaleBuffer_47 = value;
	}

	inline static int32_t get_offset_of_isTransforming_48() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___isTransforming_48)); }
	inline bool get_isTransforming_48() const { return ___isTransforming_48; }
	inline bool* get_address_of_isTransforming_48() { return &___isTransforming_48; }
	inline void set_isTransforming_48(bool value)
	{
		___isTransforming_48 = value;
	}

	inline static int32_t get_offset_of_type_49() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___type_49)); }
	inline int32_t get_type_49() const { return ___type_49; }
	inline int32_t* get_address_of_type_49() { return &___type_49; }
	inline void set_type_49(int32_t value)
	{
		___type_49 = value;
	}

	inline static int32_t get_offset_of_screenTransformThreshold_50() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t533205172, ___screenTransformThreshold_50)); }
	inline float get_screenTransformThreshold_50() const { return ___screenTransformThreshold_50; }
	inline float* get_address_of_screenTransformThreshold_50() { return &___screenTransformThreshold_50; }
	inline void set_screenTransformThreshold_50(float value)
	{
		___screenTransformThreshold_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINNEDTRASFORMGESTUREBASE_T533205172_H
#ifndef METAGESTURE_T1757796975_H
#define METAGESTURE_T1757796975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.MetaGesture
struct  MetaGesture_t1757796975  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchBeganInvoker
	EventHandler_1_t238911699 * ___touchBeganInvoker_36;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchMovedInvoker
	EventHandler_1_t238911699 * ___touchMovedInvoker_37;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchEndedInvoker
	EventHandler_1_t238911699 * ___touchEndedInvoker_38;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchCancelledInvoker
	EventHandler_1_t238911699 * ___touchCancelledInvoker_39;

public:
	inline static int32_t get_offset_of_touchBeganInvoker_36() { return static_cast<int32_t>(offsetof(MetaGesture_t1757796975, ___touchBeganInvoker_36)); }
	inline EventHandler_1_t238911699 * get_touchBeganInvoker_36() const { return ___touchBeganInvoker_36; }
	inline EventHandler_1_t238911699 ** get_address_of_touchBeganInvoker_36() { return &___touchBeganInvoker_36; }
	inline void set_touchBeganInvoker_36(EventHandler_1_t238911699 * value)
	{
		___touchBeganInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___touchBeganInvoker_36), value);
	}

	inline static int32_t get_offset_of_touchMovedInvoker_37() { return static_cast<int32_t>(offsetof(MetaGesture_t1757796975, ___touchMovedInvoker_37)); }
	inline EventHandler_1_t238911699 * get_touchMovedInvoker_37() const { return ___touchMovedInvoker_37; }
	inline EventHandler_1_t238911699 ** get_address_of_touchMovedInvoker_37() { return &___touchMovedInvoker_37; }
	inline void set_touchMovedInvoker_37(EventHandler_1_t238911699 * value)
	{
		___touchMovedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___touchMovedInvoker_37), value);
	}

	inline static int32_t get_offset_of_touchEndedInvoker_38() { return static_cast<int32_t>(offsetof(MetaGesture_t1757796975, ___touchEndedInvoker_38)); }
	inline EventHandler_1_t238911699 * get_touchEndedInvoker_38() const { return ___touchEndedInvoker_38; }
	inline EventHandler_1_t238911699 ** get_address_of_touchEndedInvoker_38() { return &___touchEndedInvoker_38; }
	inline void set_touchEndedInvoker_38(EventHandler_1_t238911699 * value)
	{
		___touchEndedInvoker_38 = value;
		Il2CppCodeGenWriteBarrier((&___touchEndedInvoker_38), value);
	}

	inline static int32_t get_offset_of_touchCancelledInvoker_39() { return static_cast<int32_t>(offsetof(MetaGesture_t1757796975, ___touchCancelledInvoker_39)); }
	inline EventHandler_1_t238911699 * get_touchCancelledInvoker_39() const { return ___touchCancelledInvoker_39; }
	inline EventHandler_1_t238911699 ** get_address_of_touchCancelledInvoker_39() { return &___touchCancelledInvoker_39; }
	inline void set_touchCancelledInvoker_39(EventHandler_1_t238911699 * value)
	{
		___touchCancelledInvoker_39 = value;
		Il2CppCodeGenWriteBarrier((&___touchCancelledInvoker_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METAGESTURE_T1757796975_H
#ifndef PINNEDTRANSFORMGESTURE_T823299556_H
#define PINNEDTRANSFORMGESTURE_T823299556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PinnedTransformGesture
struct  PinnedTransformGesture_t823299556  : public PinnedTrasformGestureBase_t533205172
{
public:
	// TouchScript.Gestures.PinnedTransformGesture/ProjectionType TouchScript.Gestures.PinnedTransformGesture::projection
	int32_t ___projection_51;
	// UnityEngine.Vector3 TouchScript.Gestures.PinnedTransformGesture::projectionPlaneNormal
	Vector3_t3722313464  ___projectionPlaneNormal_52;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.PinnedTransformGesture::projectionLayer
	TouchLayer_t1334725428 * ___projectionLayer_53;
	// UnityEngine.Plane TouchScript.Gestures.PinnedTransformGesture::transformPlane
	Plane_t1000493321  ___transformPlane_54;

public:
	inline static int32_t get_offset_of_projection_51() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t823299556, ___projection_51)); }
	inline int32_t get_projection_51() const { return ___projection_51; }
	inline int32_t* get_address_of_projection_51() { return &___projection_51; }
	inline void set_projection_51(int32_t value)
	{
		___projection_51 = value;
	}

	inline static int32_t get_offset_of_projectionPlaneNormal_52() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t823299556, ___projectionPlaneNormal_52)); }
	inline Vector3_t3722313464  get_projectionPlaneNormal_52() const { return ___projectionPlaneNormal_52; }
	inline Vector3_t3722313464 * get_address_of_projectionPlaneNormal_52() { return &___projectionPlaneNormal_52; }
	inline void set_projectionPlaneNormal_52(Vector3_t3722313464  value)
	{
		___projectionPlaneNormal_52 = value;
	}

	inline static int32_t get_offset_of_projectionLayer_53() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t823299556, ___projectionLayer_53)); }
	inline TouchLayer_t1334725428 * get_projectionLayer_53() const { return ___projectionLayer_53; }
	inline TouchLayer_t1334725428 ** get_address_of_projectionLayer_53() { return &___projectionLayer_53; }
	inline void set_projectionLayer_53(TouchLayer_t1334725428 * value)
	{
		___projectionLayer_53 = value;
		Il2CppCodeGenWriteBarrier((&___projectionLayer_53), value);
	}

	inline static int32_t get_offset_of_transformPlane_54() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t823299556, ___transformPlane_54)); }
	inline Plane_t1000493321  get_transformPlane_54() const { return ___transformPlane_54; }
	inline Plane_t1000493321 * get_address_of_transformPlane_54() { return &___transformPlane_54; }
	inline void set_transformPlane_54(Plane_t1000493321  value)
	{
		___transformPlane_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINNEDTRANSFORMGESTURE_T823299556_H
#ifndef SCREENTRANSFORMGESTURE_T2511156087_H
#define SCREENTRANSFORMGESTURE_T2511156087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.ScreenTransformGesture
struct  ScreenTransformGesture_t2511156087  : public TransformGestureBase_t2879339428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENTRANSFORMGESTURE_T2511156087_H
#ifndef TRANSFORMGESTURE_T1059674377_H
#define TRANSFORMGESTURE_T1059674377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TransformGesture
struct  TransformGesture_t1059674377  : public TransformGestureBase_t2879339428
{
public:
	// TouchScript.Gestures.TransformGesture/ProjectionType TouchScript.Gestures.TransformGesture::projection
	int32_t ___projection_54;
	// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::projectionPlaneNormal
	Vector3_t3722313464  ___projectionPlaneNormal_55;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.TransformGesture::projectionLayer
	TouchLayer_t1334725428 * ___projectionLayer_56;
	// UnityEngine.Plane TouchScript.Gestures.TransformGesture::transformPlane
	Plane_t1000493321  ___transformPlane_57;

public:
	inline static int32_t get_offset_of_projection_54() { return static_cast<int32_t>(offsetof(TransformGesture_t1059674377, ___projection_54)); }
	inline int32_t get_projection_54() const { return ___projection_54; }
	inline int32_t* get_address_of_projection_54() { return &___projection_54; }
	inline void set_projection_54(int32_t value)
	{
		___projection_54 = value;
	}

	inline static int32_t get_offset_of_projectionPlaneNormal_55() { return static_cast<int32_t>(offsetof(TransformGesture_t1059674377, ___projectionPlaneNormal_55)); }
	inline Vector3_t3722313464  get_projectionPlaneNormal_55() const { return ___projectionPlaneNormal_55; }
	inline Vector3_t3722313464 * get_address_of_projectionPlaneNormal_55() { return &___projectionPlaneNormal_55; }
	inline void set_projectionPlaneNormal_55(Vector3_t3722313464  value)
	{
		___projectionPlaneNormal_55 = value;
	}

	inline static int32_t get_offset_of_projectionLayer_56() { return static_cast<int32_t>(offsetof(TransformGesture_t1059674377, ___projectionLayer_56)); }
	inline TouchLayer_t1334725428 * get_projectionLayer_56() const { return ___projectionLayer_56; }
	inline TouchLayer_t1334725428 ** get_address_of_projectionLayer_56() { return &___projectionLayer_56; }
	inline void set_projectionLayer_56(TouchLayer_t1334725428 * value)
	{
		___projectionLayer_56 = value;
		Il2CppCodeGenWriteBarrier((&___projectionLayer_56), value);
	}

	inline static int32_t get_offset_of_transformPlane_57() { return static_cast<int32_t>(offsetof(TransformGesture_t1059674377, ___transformPlane_57)); }
	inline Plane_t1000493321  get_transformPlane_57() const { return ___transformPlane_57; }
	inline Plane_t1000493321 * get_address_of_transformPlane_57() { return &___transformPlane_57; }
	inline void set_transformPlane_57(Plane_t1000493321  value)
	{
		___transformPlane_57 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMGESTURE_T1059674377_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (TextLocalizer_t4021138871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[3] = 
{
	TextLocalizer_t4021138871::get_offset_of_uppercase_2(),
	TextLocalizer_t4021138871::get_offset_of_key_3(),
	TextLocalizer_t4021138871::get_offset_of_appendText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (U3CWrapMultilineU3Ec__Iterator0_t191716052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[10] = 
{
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U3ClineBreakIndicesU3E__0_0(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_textToWrap_1(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_reciever_2(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U3CcharsU3E__0_3(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U3CcharsListU3E__0_4(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U24locvar0_5(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U3CrebuildedU3E__0_6(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U24current_7(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U24disposing_8(),
	U3CWrapMultilineU3Ec__Iterator0_t191716052::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (ValleyInfoManager_t2327472454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[11] = 
{
	ValleyInfoManager_t2327472454::get_offset_of_valleyName_2(),
	ValleyInfoManager_t2327472454::get_offset_of_product_3(),
	ValleyInfoManager_t2327472454::get_offset_of_coords_4(),
	ValleyInfoManager_t2327472454::get_offset_of_valleyImage_5(),
	ValleyInfoManager_t2327472454::get_offset_of_marchBtn_6(),
	ValleyInfoManager_t2327472454::get_offset_of_buildCityBtn_7(),
	ValleyInfoManager_t2327472454::get_offset_of_recruitBtn_8(),
	ValleyInfoManager_t2327472454::get_offset_of_tabs_9(),
	ValleyInfoManager_t2327472454::get_offset_of_tabSelectedSprite_10(),
	ValleyInfoManager_t2327472454::get_offset_of_tabUnselectedSprite_11(),
	ValleyInfoManager_t2327472454::get_offset_of_field_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (WindowInstanceManager_t3234774687), -1, sizeof(WindowInstanceManager_t3234774687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3603[111] = 
{
	WindowInstanceManager_t3234774687_StaticFields::get_offset_of_instance_2(),
	WindowInstanceManager_t3234774687::get_offset_of_windowOpened_3(),
	WindowInstanceManager_t3234774687::get_offset_of_shillingsHtml_4(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildingConstructionWindow_5(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildingInformationWindow_6(),
	WindowInstanceManager_t3234774687::get_offset_of_ReportContainerWindow_7(),
	WindowInstanceManager_t3234774687::get_offset_of_ShopWindow_8(),
	WindowInstanceManager_t3234774687::get_offset_of_MailWindow_9(),
	WindowInstanceManager_t3234774687::get_offset_of_AlliancesWindow_10(),
	WindowInstanceManager_t3234774687::get_offset_of_UserInfoWindow_11(),
	WindowInstanceManager_t3234774687::get_offset_of_FinanceWindow_12(),
	WindowInstanceManager_t3234774687::get_offset_of_HelpWindow_13(),
	WindowInstanceManager_t3234774687::get_offset_of_NewMailWindow_14(),
	WindowInstanceManager_t3234774687::get_offset_of_MessageWindow_15(),
	WindowInstanceManager_t3234774687::get_offset_of_MessageAllianceMemberWindow_16(),
	WindowInstanceManager_t3234774687::get_offset_of_EventsWindow_17(),
	WindowInstanceManager_t3234774687::get_offset_of_AllianceInfoWindow_18(),
	WindowInstanceManager_t3234774687::get_offset_of_UnitDetailsWindow_19(),
	WindowInstanceManager_t3234774687::get_offset_of_CityInfoWindow_20(),
	WindowInstanceManager_t3234774687::get_offset_of_ValleyInfoWindow_21(),
	WindowInstanceManager_t3234774687::get_offset_of_DispatchWindow_22(),
	WindowInstanceManager_t3234774687::get_offset_of_RecruitBarbariansWindow_23(),
	WindowInstanceManager_t3234774687::get_offset_of_NotificationWindow_24(),
	WindowInstanceManager_t3234774687::get_offset_of_LoginProgressBar_25(),
	WindowInstanceManager_t3234774687::get_offset_of_KnightInfoWindow_26(),
	WindowInstanceManager_t3234774687::get_offset_of_BuyItemWindow_27(),
	WindowInstanceManager_t3234774687::get_offset_of_SellItemWindow_28(),
	WindowInstanceManager_t3234774687::get_offset_of_SpeedUpWindow_29(),
	WindowInstanceManager_t3234774687::get_offset_of_UpgradeBuildingWindow_30(),
	WindowInstanceManager_t3234774687::get_offset_of_AdvencedTeleportWindow_31(),
	WindowInstanceManager_t3234774687::get_offset_of_CityDeedWindow_32(),
	WindowInstanceManager_t3234774687::get_offset_of_BuyShillingsWindow_33(),
	WindowInstanceManager_t3234774687::get_offset_of_ChangeLanguageWindow_34(),
	WindowInstanceManager_t3234774687::get_offset_of_DismissWindow_35(),
	WindowInstanceManager_t3234774687::get_offset_of_ChangePasswordWindow_36(),
	WindowInstanceManager_t3234774687::get_offset_of_TributesWindow_37(),
	WindowInstanceManager_t3234774687::get_offset_of_UpdateBuildingWindow_38(),
	WindowInstanceManager_t3234774687::get_offset_of_BattleLogWindow_39(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildNewCityWindow_40(),
	WindowInstanceManager_t3234774687::get_offset_of_ArmyDetailsWIndow_41(),
	WindowInstanceManager_t3234774687::get_offset_of_NewAllianceNameWindow_42(),
	WindowInstanceManager_t3234774687::get_offset_of_ConfirmationWindow_43(),
	WindowInstanceManager_t3234774687::get_offset_of_ShieldTimeWindow_44(),
	WindowInstanceManager_t3234774687::get_offset_of_TutorialWindow_45(),
	WindowInstanceManager_t3234774687::get_offset_of_VideoAdsWindow_46(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildingConstructionWindowInstance_47(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildingInformationWindowInstance_48(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildingInformationWindowInstance2_49(),
	WindowInstanceManager_t3234774687::get_offset_of_ReportContainerWindowInstance_50(),
	WindowInstanceManager_t3234774687::get_offset_of_ShopWindowInstance_51(),
	WindowInstanceManager_t3234774687::get_offset_of_MailWindowInstance_52(),
	WindowInstanceManager_t3234774687::get_offset_of_AlliancesWindowInstance_53(),
	WindowInstanceManager_t3234774687::get_offset_of_UserInfoWindowInstance_54(),
	WindowInstanceManager_t3234774687::get_offset_of_FinanceWindowInstance_55(),
	WindowInstanceManager_t3234774687::get_offset_of_HelpWindowInstance_56(),
	WindowInstanceManager_t3234774687::get_offset_of_NewMailWindowInstance_57(),
	WindowInstanceManager_t3234774687::get_offset_of_MessageWindowInstance_58(),
	WindowInstanceManager_t3234774687::get_offset_of_MessageAllianceMemberWindowInstance_59(),
	WindowInstanceManager_t3234774687::get_offset_of_EventsWindowInstance_60(),
	WindowInstanceManager_t3234774687::get_offset_of_AllianceInfoWindowInstance_61(),
	WindowInstanceManager_t3234774687::get_offset_of_UnitDetailsWindowInstance_62(),
	WindowInstanceManager_t3234774687::get_offset_of_CityInfoWindowInstance_63(),
	WindowInstanceManager_t3234774687::get_offset_of_DispatchWindowInstance_64(),
	WindowInstanceManager_t3234774687::get_offset_of_RecruitBarbariansWindowInstance_65(),
	WindowInstanceManager_t3234774687::get_offset_of_ValleyInfoWindowInstance_66(),
	WindowInstanceManager_t3234774687::get_offset_of_NotificationWindowInstance_67(),
	WindowInstanceManager_t3234774687::get_offset_of_LoginProgressBarInstance_68(),
	WindowInstanceManager_t3234774687::get_offset_of_KnightInfoWindowInstance_69(),
	WindowInstanceManager_t3234774687::get_offset_of_BuyItemWindowInstance_70(),
	WindowInstanceManager_t3234774687::get_offset_of_SellItemWindowInstance_71(),
	WindowInstanceManager_t3234774687::get_offset_of_SpeedUpWindowInstance_72(),
	WindowInstanceManager_t3234774687::get_offset_of_UpgradeBuildingWindowInstance_73(),
	WindowInstanceManager_t3234774687::get_offset_of_AdvencedTeleportWindowInstance_74(),
	WindowInstanceManager_t3234774687::get_offset_of_CityDeedWindowInstance_75(),
	WindowInstanceManager_t3234774687::get_offset_of_BuyShillingsWindowInstance_76(),
	WindowInstanceManager_t3234774687::get_offset_of_ChangeLanguageWindowInstance_77(),
	WindowInstanceManager_t3234774687::get_offset_of_DismissWindowInstance_78(),
	WindowInstanceManager_t3234774687::get_offset_of_ChangePasswordWindowInstance_79(),
	WindowInstanceManager_t3234774687::get_offset_of_TributesWindowInstance_80(),
	WindowInstanceManager_t3234774687::get_offset_of_UpdateBuildingWindowInstance_81(),
	WindowInstanceManager_t3234774687::get_offset_of_BattleLogWindowInstance_82(),
	WindowInstanceManager_t3234774687::get_offset_of_BuildNewCityWindowInstance_83(),
	WindowInstanceManager_t3234774687::get_offset_of_ArmyDetailsWindowInstance_84(),
	WindowInstanceManager_t3234774687::get_offset_of_ShillingsWebViewWindowInstance_85(),
	WindowInstanceManager_t3234774687::get_offset_of_NewAllianceNameWindowInstance_86(),
	WindowInstanceManager_t3234774687::get_offset_of_ConfirmationWindowInstance_87(),
	WindowInstanceManager_t3234774687::get_offset_of_ShieldTimeWindowInstance_88(),
	WindowInstanceManager_t3234774687::get_offset_of_TutorialWindowInstance_89(),
	WindowInstanceManager_t3234774687::get_offset_of_VideoAdsWindowInstance_90(),
	WindowInstanceManager_t3234774687::get_offset_of_overviewPanel_91(),
	WindowInstanceManager_t3234774687::get_offset_of_statisticsPanel_92(),
	WindowInstanceManager_t3234774687::get_offset_of_actionPanel_93(),
	WindowInstanceManager_t3234774687::get_offset_of_actionManager_94(),
	WindowInstanceManager_t3234774687::get_offset_of_allianceManager_95(),
	WindowInstanceManager_t3234774687::get_offset_of_treasureManager_96(),
	WindowInstanceManager_t3234774687::get_offset_of_oldDesignPanel_97(),
	WindowInstanceManager_t3234774687::get_offset_of_newDesignPanel_98(),
	WindowInstanceManager_t3234774687::get_offset_of_viewPanel_99(),
	WindowInstanceManager_t3234774687::get_offset_of_itemPanel_100(),
	WindowInstanceManager_t3234774687::get_offset_of_citiesPanel_101(),
	WindowInstanceManager_t3234774687::get_offset_of_viewButton_102(),
	WindowInstanceManager_t3234774687::get_offset_of_itemButton_103(),
	WindowInstanceManager_t3234774687::get_offset_of_voiceButton_104(),
	WindowInstanceManager_t3234774687::get_offset_of_viewClosedSprite_105(),
	WindowInstanceManager_t3234774687::get_offset_of_viewOpenedSprite_106(),
	WindowInstanceManager_t3234774687::get_offset_of_itemClosedSprite_107(),
	WindowInstanceManager_t3234774687::get_offset_of_itemOpenedSprite_108(),
	WindowInstanceManager_t3234774687::get_offset_of_voiceOnSprite_109(),
	WindowInstanceManager_t3234774687::get_offset_of_voiceOffSprite_110(),
	WindowInstanceManager_t3234774687::get_offset_of_UI_111(),
	WindowInstanceManager_t3234774687::get_offset_of_Shell_112(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (WorldChat_t753147188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3604[6] = 
{
	WorldChat_t753147188::get_offset_of_chatWindow_2(),
	WorldChat_t753147188::get_offset_of_input_3(),
	WorldChat_t753147188::get_offset_of_Scroll_4(),
	WorldChat_t753147188::get_offset_of_MessageBuffer_5(),
	WorldChat_t753147188::get_offset_of_chatWorking_6(),
	WorldChat_t753147188::get_offset_of_readyToChat_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (U3CBeginChatU3Ec__Iterator0_t121862130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[6] = 
{
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U3CwU3E__0_0(),
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U3CreplyU3E__1_1(),
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U24this_2(),
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U24current_3(),
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U24disposing_4(),
	U3CBeginChatU3Ec__Iterator0_t121862130::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (WorldChunkManager_t1513314516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3606[4] = 
{
	WorldChunkManager_t1513314516::get_offset_of_startX_2(),
	WorldChunkManager_t1513314516::get_offset_of_startY_3(),
	WorldChunkManager_t1513314516::get_offset_of_chunkFields_4(),
	WorldChunkManager_t1513314516::get_offset_of_onGetChunkInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (U3CGetChunkInfoU3Ec__Iterator0_t549570326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3607[6] = 
{
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U3CunitFormU3E__0_0(),
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U3CrequestU3E__0_1(),
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U24this_2(),
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U24current_3(),
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U24disposing_4(),
	U3CGetChunkInfoU3Ec__Iterator0_t549570326::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (WorldFieldManager_t2312070818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[5] = 
{
	WorldFieldManager_t2312070818::get_offset_of_internalPitId_2(),
	WorldFieldManager_t2312070818::get_offset_of_fieldObject_3(),
	WorldFieldManager_t2312070818::get_offset_of_flag_4(),
	WorldFieldManager_t2312070818::get_offset_of_occupationIcon_5(),
	WorldFieldManager_t2312070818::get_offset_of_fieldInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (WorldMapCoordsChanger_t692571169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3609[3] = 
{
	WorldMapCoordsChanger_t692571169::get_offset_of_xInput_2(),
	WorldMapCoordsChanger_t692571169::get_offset_of_yInput_3(),
	WorldMapCoordsChanger_t692571169::get_offset_of_chunksManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (ZoomManager_t89017121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[1] = 
{
	ZoomManager_t89017121::get_offset_of_mainCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (LoadAllLanguages_t2349347345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3611[5] = 
{
	LoadAllLanguages_t2349347345::get_offset_of_currentLanguageKeys_2(),
	LoadAllLanguages_t2349347345::get_offset_of_availableLanguages_3(),
	LoadAllLanguages_t2349347345::get_offset_of_languageManager_4(),
	LoadAllLanguages_t2349347345::get_offset_of_valuesScrollPosition_5(),
	LoadAllLanguages_t2349347345::get_offset_of_languagesScrollPosition_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (LocalizedText_t875466957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[2] = 
{
	LocalizedText_t875466957::get_offset_of_localizedKey_2(),
	LocalizedText_t875466957::get_offset_of_textObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (TextReverse_t1080962664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (TuioInput_t3522336661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[16] = 
{
	0,
	TuioInput_t3522336661::get_offset_of_tuioPort_6(),
	TuioInput_t3522336661::get_offset_of_supportedInputs_7(),
	TuioInput_t3522336661::get_offset_of_tuioObjectMappings_8(),
	TuioInput_t3522336661::get_offset_of_cursorTags_9(),
	TuioInput_t3522336661::get_offset_of_blobTags_10(),
	TuioInput_t3522336661::get_offset_of_objectTags_11(),
	TuioInput_t3522336661::get_offset_of_server_12(),
	TuioInput_t3522336661::get_offset_of_cursorProcessor_13(),
	TuioInput_t3522336661::get_offset_of_objectProcessor_14(),
	TuioInput_t3522336661::get_offset_of_blobProcessor_15(),
	TuioInput_t3522336661::get_offset_of_cursorToInternalId_16(),
	TuioInput_t3522336661::get_offset_of_blobToInternalId_17(),
	TuioInput_t3522336661::get_offset_of_objectToInternalId_18(),
	TuioInput_t3522336661::get_offset_of_screenWidth_19(),
	TuioInput_t3522336661::get_offset_of_screenHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (InputType_t1161111304)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3615[4] = 
{
	InputType_t1161111304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (TuioObjectMapping_t2434953367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[2] = 
{
	TuioObjectMapping_t2434953367::get_offset_of_Id_0(),
	TuioObjectMapping_t2434953367::get_offset_of_Tag_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (TouchScriptInputModule_t2128209366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[10] = 
{
	TouchScriptInputModule_t2128209366::get_offset_of_pointerEvents_8(),
	TouchScriptInputModule_t2128209366::get_offset_of_horizontalAxis_9(),
	TouchScriptInputModule_t2128209366::get_offset_of_verticalAxis_10(),
	TouchScriptInputModule_t2128209366::get_offset_of_submitButton_11(),
	TouchScriptInputModule_t2128209366::get_offset_of_cancelButton_12(),
	TouchScriptInputModule_t2128209366::get_offset_of_inputActionsPerSecond_13(),
	TouchScriptInputModule_t2128209366::get_offset_of_repeatDelay_14(),
	TouchScriptInputModule_t2128209366::get_offset_of_nextActionTime_15(),
	TouchScriptInputModule_t2128209366::get_offset_of_lastMoveDirection_16(),
	TouchScriptInputModule_t2128209366::get_offset_of_lastMoveStartTime_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (Transformer_t1886620421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[2] = 
{
	Transformer_t1886620421::get_offset_of_cachedTransform_2(),
	Transformer_t1886620421::get_offset_of_gestures_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (TouchProxy_t686879209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[2] = 
{
	TouchProxy_t686879209::get_offset_of_Text_6(),
	TouchProxy_t686879209::get_offset_of_stringBuilder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (TouchProxyBase_t1980850257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[4] = 
{
	TouchProxyBase_t1980850257::get_offset_of_U3CShowTouchIdU3Ek__BackingField_2(),
	TouchProxyBase_t1980850257::get_offset_of_U3CShowTagsU3Ek__BackingField_3(),
	TouchProxyBase_t1980850257::get_offset_of_rect_4(),
	TouchProxyBase_t1980850257::get_offset_of_size_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (TouchVisualizer_t699015915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[9] = 
{
	TouchVisualizer_t699015915::get_offset_of_touchProxy_2(),
	TouchVisualizer_t699015915::get_offset_of_showTouchId_3(),
	TouchVisualizer_t699015915::get_offset_of_showTags_4(),
	TouchVisualizer_t699015915::get_offset_of_useDPI_5(),
	TouchVisualizer_t699015915::get_offset_of_touchSize_6(),
	TouchVisualizer_t699015915::get_offset_of_defaultSize_7(),
	TouchVisualizer_t699015915::get_offset_of_rect_8(),
	TouchVisualizer_t699015915::get_offset_of_pool_9(),
	TouchVisualizer_t699015915::get_offset_of_proxies_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (Clusters_t2177422968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[9] = 
{
	0,
	0,
	Clusters_t2177422968::get_offset_of_points_2(),
	Clusters_t2177422968::get_offset_of_dirty_3(),
	Clusters_t2177422968::get_offset_of_cluster1_4(),
	Clusters_t2177422968::get_offset_of_cluster2_5(),
	Clusters_t2177422968::get_offset_of_minPointDistance_6(),
	Clusters_t2177422968::get_offset_of_minPointDistanceSqr_7(),
	Clusters_t2177422968::get_offset_of_hasClusters_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (DebuggableMonoBehaviour_t3692909384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (DisplayDevice_t2568977856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[2] = 
{
	DisplayDevice_t2568977856::get_offset_of_name_2(),
	DisplayDevice_t2568977856::get_offset_of_dpi_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (GenericDisplayDevice_t1376900854), -1, sizeof(GenericDisplayDevice_t1376900854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3625[1] = 
{
	GenericDisplayDevice_t1376900854_StaticFields::get_offset_of_isLaptop_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (GestureManager_t1744876654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (GestureManagerInstance_t955425494), -1, sizeof(GestureManagerInstance_t955425494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3628[16] = 
{
	GestureManagerInstance_t955425494::get_offset_of_U3CGlobalGestureDelegateU3Ek__BackingField_2(),
	GestureManagerInstance_t955425494_StaticFields::get_offset_of_instance_3(),
	GestureManagerInstance_t955425494_StaticFields::get_offset_of_shuttingDown_4(),
	GestureManagerInstance_t955425494::get_offset_of_gesturesToReset_5(),
	GestureManagerInstance_t955425494::get_offset_of__updateBegan_6(),
	GestureManagerInstance_t955425494::get_offset_of__updateMoved_7(),
	GestureManagerInstance_t955425494::get_offset_of__updateEnded_8(),
	GestureManagerInstance_t955425494::get_offset_of__updateCancelled_9(),
	GestureManagerInstance_t955425494::get_offset_of__processTarget_10(),
	GestureManagerInstance_t955425494::get_offset_of__processTargetBegan_11(),
	GestureManagerInstance_t955425494::get_offset_of_targetTouches_12(),
	GestureManagerInstance_t955425494::get_offset_of_gestureTouches_13(),
	GestureManagerInstance_t955425494::get_offset_of_activeGestures_14(),
	GestureManagerInstance_t955425494_StaticFields::get_offset_of_gestureListPool_15(),
	GestureManagerInstance_t955425494_StaticFields::get_offset_of_touchListPool_16(),
	GestureManagerInstance_t955425494_StaticFields::get_offset_of_transformListPool_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (PinnedTrasformGestureBase_t533205172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[19] = 
{
	0,
	0,
	0,
	PinnedTrasformGestureBase_t533205172::get_offset_of_transformStartedInvoker_35(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_transformedInvoker_36(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_transformCompletedInvoker_37(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenTransformPixelThreshold_38(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenTransformPixelThresholdSquared_39(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_cachedCollider_40(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_deltaRotation_41(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_deltaScale_42(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenPixelTranslationBuffer_43(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenPixelRotationBuffer_44(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_angleBuffer_45(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenPixelScalingBuffer_46(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_scaleBuffer_47(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_isTransforming_48(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_type_49(),
	PinnedTrasformGestureBase_t533205172::get_offset_of_screenTransformThreshold_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (TransformType_t264424356)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3630[3] = 
{
	TransformType_t264424356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (TransformGestureBase_t2879339428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[22] = 
{
	0,
	0,
	0,
	TransformGestureBase_t2879339428::get_offset_of_transformStartedInvoker_35(),
	TransformGestureBase_t2879339428::get_offset_of_transformedInvoker_36(),
	TransformGestureBase_t2879339428::get_offset_of_transformCompletedInvoker_37(),
	TransformGestureBase_t2879339428::get_offset_of_minScreenPointsPixelDistance_38(),
	TransformGestureBase_t2879339428::get_offset_of_minScreenPointsPixelDistanceSquared_39(),
	TransformGestureBase_t2879339428::get_offset_of_screenTransformPixelThreshold_40(),
	TransformGestureBase_t2879339428::get_offset_of_screenTransformPixelThresholdSquared_41(),
	TransformGestureBase_t2879339428::get_offset_of_deltaPosition_42(),
	TransformGestureBase_t2879339428::get_offset_of_deltaRotation_43(),
	TransformGestureBase_t2879339428::get_offset_of_deltaScale_44(),
	TransformGestureBase_t2879339428::get_offset_of_screenPixelTranslationBuffer_45(),
	TransformGestureBase_t2879339428::get_offset_of_screenPixelRotationBuffer_46(),
	TransformGestureBase_t2879339428::get_offset_of_angleBuffer_47(),
	TransformGestureBase_t2879339428::get_offset_of_screenPixelScalingBuffer_48(),
	TransformGestureBase_t2879339428::get_offset_of_scaleBuffer_49(),
	TransformGestureBase_t2879339428::get_offset_of_isTransforming_50(),
	TransformGestureBase_t2879339428::get_offset_of_type_51(),
	TransformGestureBase_t2879339428::get_offset_of_minScreenPointsDistance_52(),
	TransformGestureBase_t2879339428::get_offset_of_screenTransformThreshold_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (TransformType_t3234482893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3632[4] = 
{
	TransformType_t3234482893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (FlickGesture_t2013564523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[12] = 
{
	0,
	FlickGesture_t2013564523::get_offset_of_flickedInvoker_33(),
	FlickGesture_t2013564523::get_offset_of_U3CScreenFlickVectorU3Ek__BackingField_34(),
	FlickGesture_t2013564523::get_offset_of_U3CScreenFlickTimeU3Ek__BackingField_35(),
	FlickGesture_t2013564523::get_offset_of_flickTime_36(),
	FlickGesture_t2013564523::get_offset_of_minDistance_37(),
	FlickGesture_t2013564523::get_offset_of_movementThreshold_38(),
	FlickGesture_t2013564523::get_offset_of_direction_39(),
	FlickGesture_t2013564523::get_offset_of_moving_40(),
	FlickGesture_t2013564523::get_offset_of_movementBuffer_41(),
	FlickGesture_t2013564523::get_offset_of_isActive_42(),
	FlickGesture_t2013564523::get_offset_of_deltaSequence_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (GestureDirection_t4143464802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3634[4] = 
{
	GestureDirection_t4143464802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (Gesture_t4067939634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[30] = 
{
	0,
	0,
	Gesture_t4067939634::get_offset_of_stateChangedInvoker_4(),
	Gesture_t4067939634::get_offset_of_cancelledInvoker_5(),
	Gesture_t4067939634::get_offset_of_U3CPreviousStateU3Ek__BackingField_6(),
	Gesture_t4067939634::get_offset_of_U3CDelegateU3Ek__BackingField_7(),
	Gesture_t4067939634::get_offset_of_U3CtouchManagerU3Ek__BackingField_8(),
	Gesture_t4067939634::get_offset_of_U3CtouchesNumStateU3Ek__BackingField_9(),
	Gesture_t4067939634::get_offset_of_activeTouches_10(),
	Gesture_t4067939634::get_offset_of_cachedTransform_11(),
	Gesture_t4067939634::get_offset_of_advancedProps_12(),
	Gesture_t4067939634::get_offset_of_minTouches_13(),
	Gesture_t4067939634::get_offset_of_maxTouches_14(),
	Gesture_t4067939634::get_offset_of_combineTouches_15(),
	Gesture_t4067939634::get_offset_of_combineTouchesInterval_16(),
	Gesture_t4067939634::get_offset_of_useSendMessage_17(),
	Gesture_t4067939634::get_offset_of_sendStateChangeMessages_18(),
	Gesture_t4067939634::get_offset_of_sendMessageTarget_19(),
	Gesture_t4067939634::get_offset_of_requireGestureToFail_20(),
	Gesture_t4067939634::get_offset_of_friendlyGestures_21(),
	Gesture_t4067939634::get_offset_of_numTouches_22(),
	Gesture_t4067939634::get_offset_of_layer_23(),
	Gesture_t4067939634::get_offset_of_readonlyActiveTouches_24(),
	Gesture_t4067939634::get_offset_of_touchSequence_25(),
	Gesture_t4067939634::get_offset_of_gestureManagerInstance_26(),
	Gesture_t4067939634::get_offset_of_delayedStateChange_27(),
	Gesture_t4067939634::get_offset_of_requiredGestureFailed_28(),
	Gesture_t4067939634::get_offset_of_state_29(),
	Gesture_t4067939634::get_offset_of_cachedScreenPosition_30(),
	Gesture_t4067939634::get_offset_of_cachedPreviousScreenPosition_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (GestureState_t1922660398)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3636[8] = 
{
	GestureState_t1922660398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (TouchesNumState_t401176838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3637[7] = 
{
	TouchesNumState_t401176838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (GestureStateChangeEventArgs_t2842339013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[2] = 
{
	GestureStateChangeEventArgs_t2842339013::get_offset_of_U3CPreviousStateU3Ek__BackingField_1(),
	GestureStateChangeEventArgs_t2842339013::get_offset_of_U3CStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (LongPressGesture_t3651199768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[6] = 
{
	0,
	LongPressGesture_t3651199768::get_offset_of_longPressedInvoker_33(),
	LongPressGesture_t3651199768::get_offset_of_timeToPress_34(),
	LongPressGesture_t3651199768::get_offset_of_distanceLimit_35(),
	LongPressGesture_t3651199768::get_offset_of_distanceLimitInPixelsSquared_36(),
	LongPressGesture_t3651199768::get_offset_of_totalMovement_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (U3CwaitU3Ec__Iterator0_t3912298409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[5] = 
{
	U3CwaitU3Ec__Iterator0_t3912298409::get_offset_of_U3CtargetTimeU3E__0_0(),
	U3CwaitU3Ec__Iterator0_t3912298409::get_offset_of_U24this_1(),
	U3CwaitU3Ec__Iterator0_t3912298409::get_offset_of_U24current_2(),
	U3CwaitU3Ec__Iterator0_t3912298409::get_offset_of_U24disposing_3(),
	U3CwaitU3Ec__Iterator0_t3912298409::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (MetaGesture_t1757796975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[8] = 
{
	0,
	0,
	0,
	0,
	MetaGesture_t1757796975::get_offset_of_touchBeganInvoker_36(),
	MetaGesture_t1757796975::get_offset_of_touchMovedInvoker_37(),
	MetaGesture_t1757796975::get_offset_of_touchEndedInvoker_38(),
	MetaGesture_t1757796975::get_offset_of_touchCancelledInvoker_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (MetaGestureEventArgs_t2314752266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[1] = 
{
	MetaGestureEventArgs_t2314752266::get_offset_of_U3CTouchU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (PinnedTransformGesture_t823299556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[4] = 
{
	PinnedTransformGesture_t823299556::get_offset_of_projection_51(),
	PinnedTransformGesture_t823299556::get_offset_of_projectionPlaneNormal_52(),
	PinnedTransformGesture_t823299556::get_offset_of_projectionLayer_53(),
	PinnedTransformGesture_t823299556::get_offset_of_transformPlane_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (ProjectionType_t4274413497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3646[4] = 
{
	ProjectionType_t4274413497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (PressGesture_t44453096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[3] = 
{
	0,
	PressGesture_t44453096::get_offset_of_pressedInvoker_33(),
	PressGesture_t44453096::get_offset_of_ignoreChildren_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (ReleaseGesture_t1591249992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[3] = 
{
	0,
	ReleaseGesture_t1591249992::get_offset_of_releasedInvoker_33(),
	ReleaseGesture_t1591249992::get_offset_of_ignoreChildren_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (ScreenTransformGesture_t2511156087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (TapGesture_t1114317921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[10] = 
{
	0,
	TapGesture_t1114317921::get_offset_of_tappedInvoker_33(),
	TapGesture_t1114317921::get_offset_of_numberOfTapsRequired_34(),
	TapGesture_t1114317921::get_offset_of_timeLimit_35(),
	TapGesture_t1114317921::get_offset_of_distanceLimit_36(),
	TapGesture_t1114317921::get_offset_of_distanceLimitInPixelsSquared_37(),
	TapGesture_t1114317921::get_offset_of_isActive_38(),
	TapGesture_t1114317921::get_offset_of_tapsDone_39(),
	TapGesture_t1114317921::get_offset_of_startPosition_40(),
	TapGesture_t1114317921::get_offset_of_totalMovement_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (U3CwaitU3Ec__Iterator0_t2840560335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[5] = 
{
	U3CwaitU3Ec__Iterator0_t2840560335::get_offset_of_U3CtargetTimeU3E__0_0(),
	U3CwaitU3Ec__Iterator0_t2840560335::get_offset_of_U24this_1(),
	U3CwaitU3Ec__Iterator0_t2840560335::get_offset_of_U24current_2(),
	U3CwaitU3Ec__Iterator0_t2840560335::get_offset_of_U24disposing_3(),
	U3CwaitU3Ec__Iterator0_t2840560335::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (TransformGesture_t1059674377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[4] = 
{
	TransformGesture_t1059674377::get_offset_of_projection_54(),
	TransformGesture_t1059674377::get_offset_of_projectionPlaneNormal_55(),
	TransformGesture_t1059674377::get_offset_of_projectionLayer_56(),
	TransformGesture_t1059674377::get_offset_of_transformPlane_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (ProjectionType_t3454177328)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3653[4] = 
{
	ProjectionType_t3454177328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (UIGesture_t4257215238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[1] = 
{
	UIGesture_t4257215238::get_offset_of_pointerData_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (TouchData_t2804394961)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[2] = 
{
	TouchData_t2804394961::get_offset_of_OnTarget_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t2804394961::get_offset_of_Data_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (HitTest_t1158317340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (ObjectHitResult_t1915067883)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3657[5] = 
{
	ObjectHitResult_t1915067883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (TouchHit_t1165636103)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[5] = 
{
	TouchHit_t1165636103::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t1165636103::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t1165636103::get_offset_of_raycastHit_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t1165636103::get_offset_of_raycastHit2D_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t1165636103::get_offset_of_raycastResult_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (TouchHitType_t1165203098)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3659[4] = 
{
	TouchHitType_t1165203098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (Untouchable_t746303506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[1] = 
{
	Untouchable_t746303506::get_offset_of_DiscardTouch_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (MouseHandler_t2265731789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[8] = 
{
	MouseHandler_t2265731789::get_offset_of_beginTouch_0(),
	MouseHandler_t2265731789::get_offset_of_moveTouch_1(),
	MouseHandler_t2265731789::get_offset_of_endTouch_2(),
	MouseHandler_t2265731789::get_offset_of_cancelTouch_3(),
	MouseHandler_t2265731789::get_offset_of_tags_4(),
	MouseHandler_t2265731789::get_offset_of_mousePointId_5(),
	MouseHandler_t2265731789::get_offset_of_fakeMousePointId_6(),
	MouseHandler_t2265731789::get_offset_of_mousePointPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (TouchHandler_t684722057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[7] = 
{
	TouchHandler_t684722057::get_offset_of_beginTouch_0(),
	TouchHandler_t684722057::get_offset_of_moveTouch_1(),
	TouchHandler_t684722057::get_offset_of_endTouch_2(),
	TouchHandler_t684722057::get_offset_of_cancelTouch_3(),
	TouchHandler_t684722057::get_offset_of_tags_4(),
	TouchHandler_t684722057::get_offset_of_systemToInternalId_5(),
	TouchHandler_t684722057::get_offset_of_touchesNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (TouchState_t2185555166)+ sizeof (RuntimeObject), sizeof(TouchState_t2185555166 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3667[2] = 
{
	TouchState_t2185555166::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchState_t2185555166::get_offset_of_Phase_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (InputSource_t979757727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[3] = 
{
	InputSource_t979757727::get_offset_of_U3CCoordinatesRemapperU3Ek__BackingField_2(),
	InputSource_t979757727::get_offset_of_advancedProps_3(),
	InputSource_t979757727::get_offset_of_manager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (MobileInput_t4061688459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[3] = 
{
	MobileInput_t4061688459::get_offset_of_DisableOnNonTouchPlatforms_5(),
	MobileInput_t4061688459::get_offset_of_Tags_6(),
	MobileInput_t4061688459::get_offset_of_touchHandler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (MouseInput_t985431514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[3] = 
{
	MouseInput_t985431514::get_offset_of_DisableOnMobilePlatforms_5(),
	MouseInput_t985431514::get_offset_of_Tags_6(),
	MouseInput_t985431514::get_offset_of_mouseHandler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (StandardInput_t3469524447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[12] = 
{
	StandardInput_t3469524447::get_offset_of_TouchTags_5(),
	StandardInput_t3469524447::get_offset_of_MouseTags_6(),
	StandardInput_t3469524447::get_offset_of_PenTags_7(),
	StandardInput_t3469524447::get_offset_of_Windows8Touch_8(),
	StandardInput_t3469524447::get_offset_of_Windows7Touch_9(),
	StandardInput_t3469524447::get_offset_of_WebPlayerTouch_10(),
	StandardInput_t3469524447::get_offset_of_WebGLTouch_11(),
	StandardInput_t3469524447::get_offset_of_Windows8Mouse_12(),
	StandardInput_t3469524447::get_offset_of_Windows7Mouse_13(),
	StandardInput_t3469524447::get_offset_of_UniversalWindowsMouse_14(),
	StandardInput_t3469524447::get_offset_of_mouseHandler_15(),
	StandardInput_t3469524447::get_offset_of_touchHandler_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (Windows8TouchAPIType_t4063000358)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3672[5] = 
{
	Windows8TouchAPIType_t4063000358::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (Windows7TouchAPIType_t460929017)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3673[4] = 
{
	Windows7TouchAPIType_t460929017::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (TouchEventArgs_t157817308), -1, sizeof(TouchEventArgs_t157817308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3675[2] = 
{
	TouchEventArgs_t157817308::get_offset_of_U3CTouchesU3Ek__BackingField_1(),
	TouchEventArgs_t157817308_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (CameraLayer_t2201591754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[3] = 
{
	CameraLayer_t2201591754::get_offset_of_sortedHits_8(),
	CameraLayer_t2201591754::get_offset_of_cachedTransform_9(),
	CameraLayer_t2201591754::get_offset_of_tmpHitTestList_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (U3CsortHitsU3Ec__AnonStorey0_t1648932270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[1] = 
{
	U3CsortHitsU3Ec__AnonStorey0_t1648932270::get_offset_of_cameraPos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (CameraLayer2D_t2236291724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[4] = 
{
	CameraLayer2D_t2236291724::get_offset_of_layerIds_8(),
	CameraLayer2D_t2236291724::get_offset_of_layerById_9(),
	CameraLayer2D_t2236291724::get_offset_of_sortedHits_10(),
	CameraLayer2D_t2236291724::get_offset_of_tmpHitTestList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (CameraLayerBase_t3093103062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[2] = 
{
	CameraLayerBase_t3093103062::get_offset_of_layerMask_6(),
	CameraLayerBase_t3093103062::get_offset_of__camera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (FullscreenLayer_t254587142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[4] = 
{
	FullscreenLayer_t254587142::get_offset_of_type_6(),
	FullscreenLayer_t254587142::get_offset_of__camera_7(),
	FullscreenLayer_t254587142::get_offset_of_cameraTransform_8(),
	FullscreenLayer_t254587142::get_offset_of_tmpHitTestList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (LayerType_t3061072963)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3681[4] = 
{
	LayerType_t3061072963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (ProjectionParams_t2315592447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (CameraProjectionParams_t1295449896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[1] = 
{
	CameraProjectionParams_t1295449896::get_offset_of_camera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (CanvasProjectionParams_t3073699597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[4] = 
{
	CanvasProjectionParams_t3073699597::get_offset_of_canvas_0(),
	CanvasProjectionParams_t3073699597::get_offset_of_rect_1(),
	CanvasProjectionParams_t3073699597::get_offset_of_mode_2(),
	CanvasProjectionParams_t3073699597::get_offset_of_camera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (TouchLayer_t1334725428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3686[4] = 
{
	TouchLayer_t1334725428::get_offset_of_touchBeganInvoker_2(),
	TouchLayer_t1334725428::get_offset_of_Name_3(),
	TouchLayer_t1334725428::get_offset_of_U3CDelegateU3Ek__BackingField_4(),
	TouchLayer_t1334725428::get_offset_of_layerProjectionParams_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (LayerHitResult_t807160396)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3687[4] = 
{
	LayerHitResult_t807160396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t4200040672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t4200040672::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t4200040672::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t4200040672::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t4200040672::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (TouchLayerEventArgs_t720441680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[1] = 
{
	TouchLayerEventArgs_t720441680::get_offset_of_U3CTouchU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (UILayer_t1746714259), -1, sizeof(UILayer_t1746714259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3690[6] = 
{
	UILayer_t1746714259_StaticFields::get_offset_of_instance_6(),
	UILayer_t1746714259::get_offset_of_raycastResultCache_7(),
	UILayer_t1746714259::get_offset_of_tmpHitTestList_8(),
	UILayer_t1746714259::get_offset_of_pointerDataCache_9(),
	UILayer_t1746714259::get_offset_of_eventSystem_10(),
	UILayer_t1746714259::get_offset_of_projectionParamsCache_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t770193313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t770193313::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t770193313::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t770193313::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t770193313::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (Tags_t3560929397), -1, sizeof(Tags_t3560929397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3692[8] = 
{
	0,
	0,
	0,
	0,
	Tags_t3560929397_StaticFields::get_offset_of_EMPTY_4(),
	Tags_t3560929397::get_offset_of_tagList_5(),
	Tags_t3560929397::get_offset_of_tags_6(),
	Tags_t3560929397::get_offset_of_stringValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (TouchManager_t1655870378), -1, sizeof(TouchManager_t1655870378_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3693[11] = 
{
	0,
	0,
	TouchManager_t1655870378_StaticFields::get_offset_of_INVALID_POSITION_4(),
	TouchManager_t1655870378_StaticFields::get_offset_of_VERSION_5(),
	TouchManager_t1655870378::get_offset_of_displayDevice_6(),
	TouchManager_t1655870378::get_offset_of_shouldCreateCameraLayer_7(),
	TouchManager_t1655870378::get_offset_of_shouldCreateStandardInput_8(),
	TouchManager_t1655870378::get_offset_of_useSendMessage_9(),
	TouchManager_t1655870378::get_offset_of_sendMessageEvents_10(),
	TouchManager_t1655870378::get_offset_of_sendMessageTarget_11(),
	TouchManager_t1655870378::get_offset_of_layers_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (MessageType_t3141418335)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3694[7] = 
{
	MessageType_t3141418335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (MessageName_t1313736650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3695[7] = 
{
	MessageName_t1313736650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (TouchManagerInstance_t3252675660), -1, sizeof(TouchManagerInstance_t3252675660_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3696[29] = 
{
	TouchManagerInstance_t3252675660::get_offset_of_touchesBeganInvoker_2(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesMovedInvoker_3(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesEndedInvoker_4(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesCancelledInvoker_5(),
	TouchManagerInstance_t3252675660::get_offset_of_frameStartedInvoker_6(),
	TouchManagerInstance_t3252675660::get_offset_of_frameFinishedInvoker_7(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_shuttingDown_8(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_instance_9(),
	TouchManagerInstance_t3252675660::get_offset_of_shouldCreateCameraLayer_10(),
	TouchManagerInstance_t3252675660::get_offset_of_shouldCreateStandardInput_11(),
	TouchManagerInstance_t3252675660::get_offset_of_displayDevice_12(),
	TouchManagerInstance_t3252675660::get_offset_of_dpi_13(),
	TouchManagerInstance_t3252675660::get_offset_of_dotsPerCentimeter_14(),
	TouchManagerInstance_t3252675660::get_offset_of_layers_15(),
	TouchManagerInstance_t3252675660::get_offset_of_layerCount_16(),
	TouchManagerInstance_t3252675660::get_offset_of_inputs_17(),
	TouchManagerInstance_t3252675660::get_offset_of_inputCount_18(),
	TouchManagerInstance_t3252675660::get_offset_of_touches_19(),
	TouchManagerInstance_t3252675660::get_offset_of_idToTouch_20(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesBegan_21(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesUpdated_22(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesEnded_23(),
	TouchManagerInstance_t3252675660::get_offset_of_touchesCancelled_24(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_touchPointPool_25(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_touchPointListPool_26(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_intListPool_27(),
	TouchManagerInstance_t3252675660::get_offset_of_nextTouchId_28(),
	TouchManagerInstance_t3252675660::get_offset_of_touchLock_29(),
	TouchManagerInstance_t3252675660_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1227816588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[1] = 
{
	U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1227816588::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t2290274025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[1] = 
{
	U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t2290274025::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t214618119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[1] = 
{
	U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t214618119::get_offset_of_id_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
