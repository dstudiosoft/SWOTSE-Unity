﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// MarketManager
struct MarketManager_t2047363881;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<AddLot>c__Iterator10
struct  U3CAddLotU3Ec__Iterator10_t2138302172  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm MarketManager/<AddLot>c__Iterator10::<addForm>__0
	WWWForm_t3950226929 * ___U3CaddFormU3E__0_0;
	// System.Int64 MarketManager/<AddLot>c__Iterator10::resourceCount
	int64_t ___resourceCount_1;
	// System.Int64 MarketManager/<AddLot>c__Iterator10::unitPrice
	int64_t ___unitPrice_2;
	// UnityEngine.Networking.UnityWebRequest MarketManager/<AddLot>c__Iterator10::<request>__1
	UnityWebRequest_t254341728 * ___U3CrequestU3E__1_3;
	// JSONObject MarketManager/<AddLot>c__Iterator10::<obj>__2
	JSONObject_t1971882247 * ___U3CobjU3E__2_4;
	// System.Int32 MarketManager/<AddLot>c__Iterator10::$PC
	int32_t ___U24PC_5;
	// System.Object MarketManager/<AddLot>c__Iterator10::$current
	Il2CppObject * ___U24current_6;
	// System.Int64 MarketManager/<AddLot>c__Iterator10::<$>resourceCount
	int64_t ___U3CU24U3EresourceCount_7;
	// System.Int64 MarketManager/<AddLot>c__Iterator10::<$>unitPrice
	int64_t ___U3CU24U3EunitPrice_8;
	// MarketManager MarketManager/<AddLot>c__Iterator10::<>f__this
	MarketManager_t2047363881 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CaddFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CaddFormU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CaddFormU3E__0_0() const { return ___U3CaddFormU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CaddFormU3E__0_0() { return &___U3CaddFormU3E__0_0; }
	inline void set_U3CaddFormU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CaddFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaddFormU3E__0_0, value);
	}

	inline static int32_t get_offset_of_resourceCount_1() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___resourceCount_1)); }
	inline int64_t get_resourceCount_1() const { return ___resourceCount_1; }
	inline int64_t* get_address_of_resourceCount_1() { return &___resourceCount_1; }
	inline void set_resourceCount_1(int64_t value)
	{
		___resourceCount_1 = value;
	}

	inline static int32_t get_offset_of_unitPrice_2() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___unitPrice_2)); }
	inline int64_t get_unitPrice_2() const { return ___unitPrice_2; }
	inline int64_t* get_address_of_unitPrice_2() { return &___unitPrice_2; }
	inline void set_unitPrice_2(int64_t value)
	{
		___unitPrice_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CrequestU3E__1_3)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__1_3() const { return ___U3CrequestU3E__1_3; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__1_3() { return &___U3CrequestU3E__1_3; }
	inline void set_U3CrequestU3E__1_3(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CobjU3E__2_4)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__2_4() const { return ___U3CobjU3E__2_4; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__2_4() { return &___U3CobjU3E__2_4; }
	inline void set_U3CobjU3E__2_4(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EresourceCount_7() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CU24U3EresourceCount_7)); }
	inline int64_t get_U3CU24U3EresourceCount_7() const { return ___U3CU24U3EresourceCount_7; }
	inline int64_t* get_address_of_U3CU24U3EresourceCount_7() { return &___U3CU24U3EresourceCount_7; }
	inline void set_U3CU24U3EresourceCount_7(int64_t value)
	{
		___U3CU24U3EresourceCount_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EunitPrice_8() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CU24U3EunitPrice_8)); }
	inline int64_t get_U3CU24U3EunitPrice_8() const { return ___U3CU24U3EunitPrice_8; }
	inline int64_t* get_address_of_U3CU24U3EunitPrice_8() { return &___U3CU24U3EunitPrice_8; }
	inline void set_U3CU24U3EunitPrice_8(int64_t value)
	{
		___U3CU24U3EunitPrice_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator10_t2138302172, ___U3CU3Ef__this_9)); }
	inline MarketManager_t2047363881 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline MarketManager_t2047363881 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(MarketManager_t2047363881 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
