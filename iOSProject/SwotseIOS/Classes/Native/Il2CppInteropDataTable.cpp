﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2148130204_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const RuntimeType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const RuntimeType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t384495010_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t4135868527_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const RuntimeType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const RuntimeType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoResource_t4103430009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t4103430009_0_0_0;
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoWin32Resource_t1904229483_0_0_0;
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t484390987_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t2872965302_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const RuntimeType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const RuntimeType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1728406613_0_0_0;
extern "C" void SNIP_t4156255223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SNIP_t4156255223_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1422462475_0_0_0;
extern "C" void AsyncFlowControl_t153243767_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncFlowControl_t153243767_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncFlowControl_t153243767_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncFlowControl_t153243767_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const RuntimeType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3640485471_0_0_0;
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NodeEnumerator_t2183475207_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1188682440();
extern const RuntimeType AsyncReadHandler_t1188682440_0_0_0;
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcessAsyncReader_t337580163_0_0_0;
extern "C" void ProcInfo_t2917059746_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcInfo_t2917059746_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t893206259();
extern const RuntimeType ReadMethod_t893206259_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624();
extern const RuntimeType UnmanagedReadOrWrite_t876388624_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t2538911768();
extern const RuntimeType WriteMethod_t2538911768_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2469437439();
extern const RuntimeType ReadDelegate_t2469437439_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1613340939();
extern const RuntimeType WriteDelegate_t1613340939_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4266946825();
extern const RuntimeType ReadDelegate_t4266946825_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2016697242();
extern const RuntimeType WriteDelegate_t2016697242_0_0_0;
extern "C" void ifaddrs_t271199369_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t271199369_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t271199369_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ifaddrs_t271199369_0_0_0;
extern "C" void in6_addr_t3611791508_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t3611791508_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t3611791508_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType in6_addr_t3611791508_0_0_0;
extern "C" void ifaddrs_t2169824096_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t2169824096_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t2169824096_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ifaddrs_t2169824096_0_0_0;
extern "C" void in6_addr_t1417766092_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t1417766092_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t1417766092_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType in6_addr_t1417766092_0_0_0;
extern "C" void sockaddr_dl_t1317779094_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_dl_t1317779094_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_dl_t1317779094_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_dl_t1317779094_0_0_0;
extern "C" void sockaddr_in6_t2080844659_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t2080844659_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t2080844659_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_in6_t2080844659_0_0_0;
extern "C" void sockaddr_in6_t2790242023_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t2790242023_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t2790242023_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_in6_t2790242023_0_0_0;
extern "C" void sockaddr_ll_t3978606313_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_ll_t3978606313_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_ll_t3978606313_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_ll_t3978606313_0_0_0;
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_FIXED_INFO_t1299345856_0_0_0;
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_ADAPTER_ADDRESSES_t3463526328_0_0_0;
extern "C" void Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_ADAPTER_INFO_t882755512_0_0_0;
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_ADDR_STRING_t1213417184_0_0_0;
extern "C" void Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_PER_ADAPTER_INFO_t4002330115_0_0_0;
extern "C" void Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_ICMP_EX_t2444676487_0_0_0;
extern "C" void Win32_MIB_IFROW_t851471770_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_IFROW_t851471770_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_IFROW_t851471770_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_IFROW_t851471770_0_0_0;
extern "C" void Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIBICMPSTATS_EX_t3467025201_0_0_0;
extern "C" void Win32_SOCKADDR_t2504501424_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_SOCKADDR_t2504501424_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_SOCKADDR_t2504501424_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_SOCKADDR_t2504501424_0_0_0;
extern "C" void Win32_IN6_ADDR_t1894425855_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IN6_ADDR_t1894425855_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IN6_ADDR_t1894425855_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IN6_ADDR_t1894425855_0_0_0;
extern "C" void Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_TCP6ROW_t1885213672_0_0_0;
extern "C" void Win32_MIB_TCPROW_t2573267565_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_TCPROW_t2573267565_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_TCPROW_t2573267565_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_TCPROW_t2573267565_0_0_0;
extern "C" void Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_UDP6ROW_t1883444167_0_0_0;
extern "C" void Win32_MIB_UDPROW_t2570449486_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_UDPROW_t2570449486_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_UDPROW_t2570449486_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_UDPROW_t2570449486_0_0_0;
extern "C" void IPPacketInformation_t1358721607_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IPPacketInformation_t1358721607_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IPPacketInformation_t1358721607_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IPPacketInformation_t1358721607_0_0_0;
extern "C" void DelegatePInvokeWrapper_SendFileHandler_t4071383562();
extern const RuntimeType SendFileHandler_t4071383562_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t1521370843();
extern const RuntimeType SocketAsyncCall_t1521370843_0_0_0;
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t2080034863_0_0_0;
extern "C" void SocketInformation_t870404749_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketInformation_t870404749_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketInformation_t870404749_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketInformation_t870404749_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const RuntimeType CostDelegate_t1722821004_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t722425697_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const RuntimeType Action_t1264377477_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3046754366_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const RuntimeType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const RuntimeType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t3119663542_0_0_0;
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t699759206_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1445031843_0_0_0;
extern "C" void OrderBlock_t1585977831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OrderBlock_t1585977831_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3829159415_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124();
extern const RuntimeType CSSMeasureFunc_t1554030124_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const RuntimeType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const RuntimeType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const RuntimeType UnityAction_t3245792599_0_0_0;
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t547604379_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3067099924_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t631007953_0_0_0;
extern "C" void PlayableBinding_t354260709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t354260709_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1369453676_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3229609740_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1199777556_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279();
extern const RuntimeType RequestAtlasCallback_t3100554279_0_0_0;
extern "C" void WorkRequest_t1354518612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1354518612_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1699091251_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t403091072_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const RuntimeType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const RuntimeType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const RuntimeType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const RuntimeType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3211863866_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t2928496527_0_0_0;
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t463507806_0_0_0;
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequestAsyncOperation_t3852015985_0_0_0;
extern "C" void UploadHandler_t2993558019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t2993558019_0_0_0;
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t25761545_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t2465339518_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2719720026_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const RuntimeType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const RuntimeType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1397964415_0_0_0;
extern "C" void SliderHandler_t1154919399_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SliderHandler_t1154919399_0_0_0;
extern "C" void EmissionModule_t311448003_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmissionModule_t311448003_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmissionModule_t311448003_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmissionModule_t311448003_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t3805203441_0_0_0;
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t2279581989_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t4262080450_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t240592346_0_0_0;
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t1056001966_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const RuntimeType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3163629820();
extern const RuntimeType SessionStateChanged_t3163629820_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const RuntimeType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void CancellationToken_t784455623_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationToken_t784455623_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationToken_t784455623_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CancellationToken_t784455623_0_0_0;
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CancellationTokenRegistration_t2813424904_0_0_0;
extern "C" void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1533780051();
extern const RuntimeType ExceptionArgumentDelegate_t1533780051_0_0_0;
extern "C" void DelegatePInvokeWrapper_ExceptionDelegate_t467131861();
extern const RuntimeType ExceptionDelegate_t467131861_0_0_0;
extern "C" void DelegatePInvokeWrapper_SWIGStringDelegate_t3256258918();
extern const RuntimeType SWIGStringDelegate_t3256258918_0_0_0;
extern "C" void DelegatePInvokeWrapper_DestroyDelegate_t2525580959();
extern const RuntimeType DestroyDelegate_t2525580959_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogMessageDelegate_t657899700();
extern const RuntimeType LogMessageDelegate_t657899700_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t2707960735();
extern const RuntimeType Action_t2707960735_0_0_0;
extern "C" void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t3186567461();
extern const RuntimeType SWIG_CompletionDelegate_t3186567461_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1244810226();
extern const RuntimeType Action_t1244810226_0_0_0;
extern "C" void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t2224793779();
extern const RuntimeType SWIG_CompletionDelegate_t2224793779_0_0_0;
extern "C" void TagName_t2891256255_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TagName_t2891256255_0_0_0;
extern "C" void QNameValueType_t615604793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType QNameValueType_t615604793_0_0_0;
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringArrayValueType_t3147326440_0_0_0;
extern "C" void StringValueType_t261329552_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringValueType_t261329552_0_0_0;
extern "C" void UriValueType_t2866347695_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriValueType_t2866347695_0_0_0;
extern "C" void NsDecl_t3938094415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsDecl_t3938094415_0_0_0;
extern "C" void NsScope_t3958624705_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsScope_t3958624705_0_0_0;
extern "C" void DelegatePInvokeWrapper_CharGetter_t1703763694();
extern const RuntimeType CharGetter_t1703763694_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const RuntimeType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3049316579_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1362986479_0_0_0;
extern "C" void DelegatePInvokeWrapper_MessageReceivedDelegate_t2474724020();
extern const RuntimeType MessageReceivedDelegate_t2474724020_0_0_0;
extern "C" void DelegatePInvokeWrapper_TokenReceivedDelegate_t1016457320();
extern const RuntimeType TokenReceivedDelegate_t1016457320_0_0_0;
extern "C" void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1079801895();
extern const RuntimeType ExceptionArgumentDelegate_t1079801895_0_0_0;
extern "C" void DelegatePInvokeWrapper_ExceptionDelegate_t1699519678();
extern const RuntimeType ExceptionDelegate_t1699519678_0_0_0;
extern "C" void DelegatePInvokeWrapper_SWIGStringDelegate_t3107596264();
extern const RuntimeType SWIGStringDelegate_t3107596264_0_0_0;
extern "C" void DelegatePInvokeWrapper_ConfirmationEvent_t890979749();
extern const RuntimeType ConfirmationEvent_t890979749_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432();
extern const RuntimeType GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418();
extern const RuntimeType GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidDismissScreenCallback_t972393216();
extern const RuntimeType GADUAdViewDidDismissScreenCallback_t972393216_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547();
extern const RuntimeType GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidReceiveAdCallback_t2543294242();
extern const RuntimeType GADUAdViewDidReceiveAdCallback_t2543294242_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewWillLeaveApplicationCallback_t3323587265();
extern const RuntimeType GADUAdViewWillLeaveApplicationCallback_t3323587265_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewWillPresentScreenCallback_t2057580186();
extern const RuntimeType GADUAdViewWillPresentScreenCallback_t2057580186_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeCustomTemplateDidReceiveClick_t350204406();
extern const RuntimeType GADUNativeCustomTemplateDidReceiveClick_t350204406_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidDismissScreenCallback_t1339081348();
extern const RuntimeType GADUInterstitialDidDismissScreenCallback_t1339081348_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714();
extern const RuntimeType GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidReceiveAdCallback_t821971233();
extern const RuntimeType GADUInterstitialDidReceiveAdCallback_t821971233_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialWillLeaveApplicationCallback_t1816935820();
extern const RuntimeType GADUInterstitialWillLeaveApplicationCallback_t1816935820_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialWillPresentScreenCallback_t539653454();
extern const RuntimeType GADUInterstitialWillPresentScreenCallback_t539653454_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787();
extern const RuntimeType GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880();
extern const RuntimeType GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075();
extern const RuntimeType GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168();
extern const RuntimeType GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154();
extern const RuntimeType GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCloseCallback_t623082069();
extern const RuntimeType GADURewardBasedVideoAdDidCloseCallback_t623082069_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788();
extern const RuntimeType GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidOpenCallback_t3638490629();
extern const RuntimeType GADURewardBasedVideoAdDidOpenCallback_t3638490629_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidReceiveAdCallback_t462486315();
extern const RuntimeType GADURewardBasedVideoAdDidReceiveAdCallback_t462486315_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidRewardCallback_t990863796();
extern const RuntimeType GADURewardBasedVideoAdDidRewardCallback_t990863796_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidStartCallback_t2792276088();
extern const RuntimeType GADURewardBasedVideoAdDidStartCallback_t2792276088_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531();
extern const RuntimeType GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531_0_0_0;
extern "C" void DelegatePInvokeWrapper_FieldNotFound_t2620845099();
extern const RuntimeType FieldNotFound_t2620845099_0_0_0;
extern "C" void TouchData_t2804394961_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TouchData_t2804394961_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TouchData_t2804394961_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TouchData_t2804394961_0_0_0;
extern "C" void TouchHit_t1165636103_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TouchHit_t1165636103_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TouchHit_t1165636103_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TouchHit_t1165636103_0_0_0;
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UniWebViewMessage_t2441068380_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[206] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t4103430009_marshal_pinvoke, MonoResource_t4103430009_marshal_pinvoke_back, MonoResource_t4103430009_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t4103430009_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t1904229483_marshal_pinvoke, MonoWin32Resource_t1904229483_marshal_pinvoke_back, MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t1904229483_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, RefEmitPermissionSet_t484390987_marshal_pinvoke, RefEmitPermissionSet_t484390987_marshal_pinvoke_back, RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t484390987_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SNIP_t4156255223_marshal_pinvoke, SNIP_t4156255223_marshal_pinvoke_back, SNIP_t4156255223_marshal_pinvoke_cleanup, NULL, NULL, &SNIP_t4156255223_0_0_0 } /* System.Security.Permissions.StrongNameIdentityPermission/SNIP */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ NULL, AsyncFlowControl_t153243767_marshal_pinvoke, AsyncFlowControl_t153243767_marshal_pinvoke_back, AsyncFlowControl_t153243767_marshal_pinvoke_cleanup, NULL, NULL, &AsyncFlowControl_t153243767_0_0_0 } /* System.Threading.AsyncFlowControl */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ NULL, NodeEnumerator_t2183475207_marshal_pinvoke, NodeEnumerator_t2183475207_marshal_pinvoke_back, NodeEnumerator_t2183475207_marshal_pinvoke_cleanup, NULL, NULL, &NodeEnumerator_t2183475207_0_0_0 } /* System.Collections.Generic.RBTree/NodeEnumerator */,
	{ DelegatePInvokeWrapper_AsyncReadHandler_t1188682440, NULL, NULL, NULL, NULL, NULL, &AsyncReadHandler_t1188682440_0_0_0 } /* System.Diagnostics.Process/AsyncReadHandler */,
	{ NULL, ProcessAsyncReader_t337580163_marshal_pinvoke, ProcessAsyncReader_t337580163_marshal_pinvoke_back, ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup, NULL, NULL, &ProcessAsyncReader_t337580163_0_0_0 } /* System.Diagnostics.Process/ProcessAsyncReader */,
	{ NULL, ProcInfo_t2917059746_marshal_pinvoke, ProcInfo_t2917059746_marshal_pinvoke_back, ProcInfo_t2917059746_marshal_pinvoke_cleanup, NULL, NULL, &ProcInfo_t2917059746_0_0_0 } /* System.Diagnostics.Process/ProcInfo */,
	{ DelegatePInvokeWrapper_ReadMethod_t893206259, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t893206259_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t876388624_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t2538911768, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t2538911768_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2469437439, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2469437439_0_0_0 } /* System.IO.MonoSyncFileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1613340939, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1613340939_0_0_0 } /* System.IO.MonoSyncFileStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4266946825, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4266946825_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2016697242, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2016697242_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ NULL, ifaddrs_t271199369_marshal_pinvoke, ifaddrs_t271199369_marshal_pinvoke_back, ifaddrs_t271199369_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t271199369_0_0_0 } /* System.Net.NetworkInformation.ifaddrs */,
	{ NULL, in6_addr_t3611791508_marshal_pinvoke, in6_addr_t3611791508_marshal_pinvoke_back, in6_addr_t3611791508_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t3611791508_0_0_0 } /* System.Net.NetworkInformation.in6_addr */,
	{ NULL, ifaddrs_t2169824096_marshal_pinvoke, ifaddrs_t2169824096_marshal_pinvoke_back, ifaddrs_t2169824096_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t2169824096_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.ifaddrs */,
	{ NULL, in6_addr_t1417766092_marshal_pinvoke, in6_addr_t1417766092_marshal_pinvoke_back, in6_addr_t1417766092_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t1417766092_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.in6_addr */,
	{ NULL, sockaddr_dl_t1317779094_marshal_pinvoke, sockaddr_dl_t1317779094_marshal_pinvoke_back, sockaddr_dl_t1317779094_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_dl_t1317779094_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_dl */,
	{ NULL, sockaddr_in6_t2080844659_marshal_pinvoke, sockaddr_in6_t2080844659_marshal_pinvoke_back, sockaddr_in6_t2080844659_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t2080844659_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_in6 */,
	{ NULL, sockaddr_in6_t2790242023_marshal_pinvoke, sockaddr_in6_t2790242023_marshal_pinvoke_back, sockaddr_in6_t2790242023_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t2790242023_0_0_0 } /* System.Net.NetworkInformation.sockaddr_in6 */,
	{ NULL, sockaddr_ll_t3978606313_marshal_pinvoke, sockaddr_ll_t3978606313_marshal_pinvoke_back, sockaddr_ll_t3978606313_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_ll_t3978606313_0_0_0 } /* System.Net.NetworkInformation.sockaddr_ll */,
	{ NULL, Win32_FIXED_INFO_t1299345856_marshal_pinvoke, Win32_FIXED_INFO_t1299345856_marshal_pinvoke_back, Win32_FIXED_INFO_t1299345856_marshal_pinvoke_cleanup, NULL, NULL, &Win32_FIXED_INFO_t1299345856_0_0_0 } /* System.Net.NetworkInformation.Win32_FIXED_INFO */,
	{ NULL, Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke, Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke_back, Win32_IP_ADAPTER_ADDRESSES_t3463526328_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADAPTER_ADDRESSES_t3463526328_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES */,
	{ NULL, Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke, Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke_back, Win32_IP_ADAPTER_INFO_t882755512_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADAPTER_INFO_t882755512_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADAPTER_INFO */,
	{ NULL, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_back, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADDR_STRING_t1213417184_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADDR_STRING */,
	{ NULL, Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke, Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke_back, Win32_IP_PER_ADAPTER_INFO_t4002330115_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_PER_ADAPTER_INFO_t4002330115_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_PER_ADAPTER_INFO */,
	{ NULL, Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke, Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke_back, Win32_MIB_ICMP_EX_t2444676487_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_ICMP_EX_t2444676487_0_0_0 } /* System.Net.NetworkInformation.Win32_MIB_ICMP_EX */,
	{ NULL, Win32_MIB_IFROW_t851471770_marshal_pinvoke, Win32_MIB_IFROW_t851471770_marshal_pinvoke_back, Win32_MIB_IFROW_t851471770_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_IFROW_t851471770_0_0_0 } /* System.Net.NetworkInformation.Win32_MIB_IFROW */,
	{ NULL, Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke, Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke_back, Win32_MIBICMPSTATS_EX_t3467025201_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIBICMPSTATS_EX_t3467025201_0_0_0 } /* System.Net.NetworkInformation.Win32_MIBICMPSTATS_EX */,
	{ NULL, Win32_SOCKADDR_t2504501424_marshal_pinvoke, Win32_SOCKADDR_t2504501424_marshal_pinvoke_back, Win32_SOCKADDR_t2504501424_marshal_pinvoke_cleanup, NULL, NULL, &Win32_SOCKADDR_t2504501424_0_0_0 } /* System.Net.NetworkInformation.Win32_SOCKADDR */,
	{ NULL, Win32_IN6_ADDR_t1894425855_marshal_pinvoke, Win32_IN6_ADDR_t1894425855_marshal_pinvoke_back, Win32_IN6_ADDR_t1894425855_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IN6_ADDR_t1894425855_0_0_0 } /* System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_IN6_ADDR */,
	{ NULL, Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke, Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke_back, Win32_MIB_TCP6ROW_t1885213672_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_TCP6ROW_t1885213672_0_0_0 } /* System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW */,
	{ NULL, Win32_MIB_TCPROW_t2573267565_marshal_pinvoke, Win32_MIB_TCPROW_t2573267565_marshal_pinvoke_back, Win32_MIB_TCPROW_t2573267565_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_TCPROW_t2573267565_0_0_0 } /* System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCPROW */,
	{ NULL, Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke, Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke_back, Win32_MIB_UDP6ROW_t1883444167_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_UDP6ROW_t1883444167_0_0_0 } /* System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_UDP6ROW */,
	{ NULL, Win32_MIB_UDPROW_t2570449486_marshal_pinvoke, Win32_MIB_UDPROW_t2570449486_marshal_pinvoke_back, Win32_MIB_UDPROW_t2570449486_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_UDPROW_t2570449486_0_0_0 } /* System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_UDPROW */,
	{ NULL, IPPacketInformation_t1358721607_marshal_pinvoke, IPPacketInformation_t1358721607_marshal_pinvoke_back, IPPacketInformation_t1358721607_marshal_pinvoke_cleanup, NULL, NULL, &IPPacketInformation_t1358721607_0_0_0 } /* System.Net.Sockets.IPPacketInformation */,
	{ DelegatePInvokeWrapper_SendFileHandler_t4071383562, NULL, NULL, NULL, NULL, NULL, &SendFileHandler_t4071383562_0_0_0 } /* System.Net.Sockets.Socket/SendFileHandler */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t1521370843, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t1521370843_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t2080034863_marshal_pinvoke, SocketAsyncResult_t2080034863_marshal_pinvoke_back, SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t2080034863_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, SocketInformation_t870404749_marshal_pinvoke, SocketInformation_t870404749_marshal_pinvoke_back, SocketInformation_t870404749_marshal_pinvoke_cleanup, NULL, NULL, &SocketInformation_t870404749_0_0_0 } /* System.Net.Sockets.SocketInformation */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t3119663542_marshal_pinvoke, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t3119663542_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t699759206_marshal_pinvoke, AssetBundleRequest_t699759206_marshal_pinvoke_back, AssetBundleRequest_t699759206_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t699759206_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, OrderBlock_t1585977831_marshal_pinvoke, OrderBlock_t1585977831_marshal_pinvoke_back, OrderBlock_t1585977831_marshal_pinvoke_cleanup, NULL, NULL, &OrderBlock_t1585977831_0_0_0 } /* UnityEngine.BeforeRenderHelper/OrderBlock */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1554030124_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t547604379_marshal_pinvoke, FailedToLoadScriptObject_t547604379_marshal_pinvoke_back, FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t547604379_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t354260709_marshal_pinvoke, PlayableBinding_t354260709_marshal_pinvoke_back, PlayableBinding_t354260709_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t354260709_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3100554279_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1354518612_marshal_pinvoke, WorkRequest_t1354518612_marshal_pinvoke_back, WorkRequest_t1354518612_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1354518612_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t2928496527_marshal_pinvoke, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t2928496527_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t463507806_marshal_pinvoke, UnityWebRequest_t463507806_marshal_pinvoke_back, UnityWebRequest_t463507806_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t463507806_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequestAsyncOperation_t3852015985_0_0_0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation */,
	{ NULL, UploadHandler_t2993558019_marshal_pinvoke, UploadHandler_t2993558019_marshal_pinvoke_back, UploadHandler_t2993558019_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t2993558019_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t25761545_marshal_pinvoke, UploadHandlerRaw_t25761545_marshal_pinvoke_back, UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t25761545_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, SliderHandler_t1154919399_marshal_pinvoke, SliderHandler_t1154919399_marshal_pinvoke_back, SliderHandler_t1154919399_marshal_pinvoke_cleanup, NULL, NULL, &SliderHandler_t1154919399_0_0_0 } /* UnityEngine.SliderHandler */,
	{ NULL, EmissionModule_t311448003_marshal_pinvoke, EmissionModule_t311448003_marshal_pinvoke_back, EmissionModule_t311448003_marshal_pinvoke_cleanup, NULL, NULL, &EmissionModule_t311448003_0_0_0 } /* UnityEngine.ParticleSystem/EmissionModule */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, RaycastHit2D_t2279581989_marshal_pinvoke, RaycastHit2D_t2279581989_marshal_pinvoke_back, RaycastHit2D_t2279581989_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2279581989_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t1056001966_marshal_pinvoke, RaycastHit_t1056001966_marshal_pinvoke_back, RaycastHit_t1056001966_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1056001966_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3163629820, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3163629820_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, CancellationToken_t784455623_marshal_pinvoke, CancellationToken_t784455623_marshal_pinvoke_back, CancellationToken_t784455623_marshal_pinvoke_cleanup, NULL, NULL, &CancellationToken_t784455623_0_0_0 } /* System.Threading.CancellationToken */,
	{ NULL, CancellationTokenRegistration_t2813424904_marshal_pinvoke, CancellationTokenRegistration_t2813424904_marshal_pinvoke_back, CancellationTokenRegistration_t2813424904_marshal_pinvoke_cleanup, NULL, NULL, &CancellationTokenRegistration_t2813424904_0_0_0 } /* System.Threading.CancellationTokenRegistration */,
	{ DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1533780051, NULL, NULL, NULL, NULL, NULL, &ExceptionArgumentDelegate_t1533780051_0_0_0 } /* Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate */,
	{ DelegatePInvokeWrapper_ExceptionDelegate_t467131861, NULL, NULL, NULL, NULL, NULL, &ExceptionDelegate_t467131861_0_0_0 } /* Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate */,
	{ DelegatePInvokeWrapper_SWIGStringDelegate_t3256258918, NULL, NULL, NULL, NULL, NULL, &SWIGStringDelegate_t3256258918_0_0_0 } /* Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate */,
	{ DelegatePInvokeWrapper_DestroyDelegate_t2525580959, NULL, NULL, NULL, NULL, NULL, &DestroyDelegate_t2525580959_0_0_0 } /* Firebase.FirebaseApp/DestroyDelegate */,
	{ DelegatePInvokeWrapper_LogMessageDelegate_t657899700, NULL, NULL, NULL, NULL, NULL, &LogMessageDelegate_t657899700_0_0_0 } /* Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate */,
	{ DelegatePInvokeWrapper_Action_t2707960735, NULL, NULL, NULL, NULL, NULL, &Action_t2707960735_0_0_0 } /* Firebase.FutureString/Action */,
	{ DelegatePInvokeWrapper_SWIG_CompletionDelegate_t3186567461, NULL, NULL, NULL, NULL, NULL, &SWIG_CompletionDelegate_t3186567461_0_0_0 } /* Firebase.FutureString/SWIG_CompletionDelegate */,
	{ DelegatePInvokeWrapper_Action_t1244810226, NULL, NULL, NULL, NULL, NULL, &Action_t1244810226_0_0_0 } /* Firebase.FutureVoid/Action */,
	{ DelegatePInvokeWrapper_SWIG_CompletionDelegate_t2224793779, NULL, NULL, NULL, NULL, NULL, &SWIG_CompletionDelegate_t2224793779_0_0_0 } /* Firebase.FutureVoid/SWIG_CompletionDelegate */,
	{ NULL, TagName_t2891256255_marshal_pinvoke, TagName_t2891256255_marshal_pinvoke_back, TagName_t2891256255_marshal_pinvoke_cleanup, NULL, NULL, &TagName_t2891256255_0_0_0 } /* Mono.Xml2.XmlTextReader/TagName */,
	{ NULL, QNameValueType_t615604793_marshal_pinvoke, QNameValueType_t615604793_marshal_pinvoke_back, QNameValueType_t615604793_marshal_pinvoke_cleanup, NULL, NULL, &QNameValueType_t615604793_0_0_0 } /* System.Xml.Schema.QNameValueType */,
	{ NULL, StringArrayValueType_t3147326440_marshal_pinvoke, StringArrayValueType_t3147326440_marshal_pinvoke_back, StringArrayValueType_t3147326440_marshal_pinvoke_cleanup, NULL, NULL, &StringArrayValueType_t3147326440_0_0_0 } /* System.Xml.Schema.StringArrayValueType */,
	{ NULL, StringValueType_t261329552_marshal_pinvoke, StringValueType_t261329552_marshal_pinvoke_back, StringValueType_t261329552_marshal_pinvoke_cleanup, NULL, NULL, &StringValueType_t261329552_0_0_0 } /* System.Xml.Schema.StringValueType */,
	{ NULL, UriValueType_t2866347695_marshal_pinvoke, UriValueType_t2866347695_marshal_pinvoke_back, UriValueType_t2866347695_marshal_pinvoke_cleanup, NULL, NULL, &UriValueType_t2866347695_0_0_0 } /* System.Xml.Schema.UriValueType */,
	{ NULL, NsDecl_t3938094415_marshal_pinvoke, NsDecl_t3938094415_marshal_pinvoke_back, NsDecl_t3938094415_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t3938094415_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, NsScope_t3958624705_marshal_pinvoke, NsScope_t3958624705_marshal_pinvoke_back, NsScope_t3958624705_marshal_pinvoke_cleanup, NULL, NULL, &NsScope_t3958624705_0_0_0 } /* System.Xml.XmlNamespaceManager/NsScope */,
	{ DelegatePInvokeWrapper_CharGetter_t1703763694, NULL, NULL, NULL, NULL, NULL, &CharGetter_t1703763694_0_0_0 } /* System.Xml.XmlReaderBinarySupport/CharGetter */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ DelegatePInvokeWrapper_MessageReceivedDelegate_t2474724020, NULL, NULL, NULL, NULL, NULL, &MessageReceivedDelegate_t2474724020_0_0_0 } /* Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate */,
	{ DelegatePInvokeWrapper_TokenReceivedDelegate_t1016457320, NULL, NULL, NULL, NULL, NULL, &TokenReceivedDelegate_t1016457320_0_0_0 } /* Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate */,
	{ DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1079801895, NULL, NULL, NULL, NULL, NULL, &ExceptionArgumentDelegate_t1079801895_0_0_0 } /* Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate */,
	{ DelegatePInvokeWrapper_ExceptionDelegate_t1699519678, NULL, NULL, NULL, NULL, NULL, &ExceptionDelegate_t1699519678_0_0_0 } /* Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate */,
	{ DelegatePInvokeWrapper_SWIGStringDelegate_t3107596264, NULL, NULL, NULL, NULL, NULL, &SWIGStringDelegate_t3107596264_0_0_0 } /* Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate */,
	{ DelegatePInvokeWrapper_ConfirmationEvent_t890979749, NULL, NULL, NULL, NULL, NULL, &ConfirmationEvent_t890979749_0_0_0 } /* ConfirmationEvent */,
	{ DelegatePInvokeWrapper_GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432, NULL, NULL, NULL, NULL, NULL, &GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432_0_0_0 } /* GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418, NULL, NULL, NULL, NULL, NULL, &GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418_0_0_0 } /* GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidDismissScreenCallback_t972393216, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidDismissScreenCallback_t972393216_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidReceiveAdCallback_t2543294242, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidReceiveAdCallback_t2543294242_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewWillLeaveApplicationCallback_t3323587265, NULL, NULL, NULL, NULL, NULL, &GADUAdViewWillLeaveApplicationCallback_t3323587265_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewWillPresentScreenCallback_t2057580186, NULL, NULL, NULL, NULL, NULL, &GADUAdViewWillPresentScreenCallback_t2057580186_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback */,
	{ DelegatePInvokeWrapper_GADUNativeCustomTemplateDidReceiveClick_t350204406, NULL, NULL, NULL, NULL, NULL, &GADUNativeCustomTemplateDidReceiveClick_t350204406_0_0_0 } /* GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidDismissScreenCallback_t1339081348, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidDismissScreenCallback_t1339081348_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidReceiveAdCallback_t821971233, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidReceiveAdCallback_t821971233_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialWillLeaveApplicationCallback_t1816935820, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialWillLeaveApplicationCallback_t1816935820_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialWillPresentScreenCallback_t539653454, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialWillPresentScreenCallback_t539653454_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback */,
	{ DelegatePInvokeWrapper_GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787, NULL, NULL, NULL, NULL, NULL, &GADUNativeExpressAdViewDidDismissScreenCallback_t2327772787_0_0_0 } /* GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidDismissScreenCallback */,
	{ DelegatePInvokeWrapper_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880, NULL, NULL, NULL, NULL, NULL, &GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t1854757880_0_0_0 } /* GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075, NULL, NULL, NULL, NULL, NULL, &GADUNativeExpressAdViewDidReceiveAdCallback_t1287948075_0_0_0 } /* GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168, NULL, NULL, NULL, NULL, NULL, &GADUNativeExpressAdViewWillLeaveApplicationCallback_t3688860168_0_0_0 } /* GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154, NULL, NULL, NULL, NULL, NULL, &GADUNativeExpressAdViewWillPresentScreenCallback_t1947415154_0_0_0 } /* GoogleMobileAds.iOS.NativeExpressAdClient/GADUNativeExpressAdViewWillPresentScreenCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCloseCallback_t623082069, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidCloseCallback_t623082069_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidOpenCallback_t3638490629, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidOpenCallback_t3638490629_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidReceiveAdCallback_t462486315, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidReceiveAdCallback_t462486315_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidRewardCallback_t990863796, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidRewardCallback_t990863796_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidStartCallback_t2792276088, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidStartCallback_t2792276088_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_FieldNotFound_t2620845099, NULL, NULL, NULL, NULL, NULL, &FieldNotFound_t2620845099_0_0_0 } /* JSONObject/FieldNotFound */,
	{ NULL, TouchData_t2804394961_marshal_pinvoke, TouchData_t2804394961_marshal_pinvoke_back, TouchData_t2804394961_marshal_pinvoke_cleanup, NULL, NULL, &TouchData_t2804394961_0_0_0 } /* TouchScript.Gestures.UI.UIGesture/TouchData */,
	{ NULL, TouchHit_t1165636103_marshal_pinvoke, TouchHit_t1165636103_marshal_pinvoke_back, TouchHit_t1165636103_marshal_pinvoke_cleanup, NULL, NULL, &TouchHit_t1165636103_0_0_0 } /* TouchScript.Hit.TouchHit */,
	{ NULL, UniWebViewMessage_t2441068380_marshal_pinvoke, UniWebViewMessage_t2441068380_marshal_pinvoke_back, UniWebViewMessage_t2441068380_marshal_pinvoke_cleanup, NULL, NULL, &UniWebViewMessage_t2441068380_0_0_0 } /* UniWebViewMessage */,
	NULL,
};
