﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Ping/<SendAsync>c__AnonStorey6
struct U3CSendAsyncU3Ec__AnonStorey6_t2153546882;
// System.Object
struct Il2CppObject;
// System.ComponentModel.DoWorkEventArgs
struct DoWorkEventArgs_t62745097;
// System.ComponentModel.RunWorkerCompletedEventArgs
struct RunWorkerCompletedEventArgs_t3512304465;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_DoWorkEventArgs62745097.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve3512304465.h"

// System.Void System.Net.NetworkInformation.Ping/<SendAsync>c__AnonStorey6::.ctor()
extern "C"  void U3CSendAsyncU3Ec__AnonStorey6__ctor_m461735020 (U3CSendAsyncU3Ec__AnonStorey6_t2153546882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping/<SendAsync>c__AnonStorey6::<>m__5(System.Object,System.ComponentModel.DoWorkEventArgs)
extern "C"  void U3CSendAsyncU3Ec__AnonStorey6_U3CU3Em__5_m1121268942 (U3CSendAsyncU3Ec__AnonStorey6_t2153546882 * __this, Il2CppObject * ___o0, DoWorkEventArgs_t62745097 * ___ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping/<SendAsync>c__AnonStorey6::<>m__6(System.Object,System.ComponentModel.RunWorkerCompletedEventArgs)
extern "C"  void U3CSendAsyncU3Ec__AnonStorey6_U3CU3Em__6_m2222549381 (U3CSendAsyncU3Ec__AnonStorey6_t2153546882 * __this, Il2CppObject * ___o0, RunWorkerCompletedEventArgs_t3512304465 * ___ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
