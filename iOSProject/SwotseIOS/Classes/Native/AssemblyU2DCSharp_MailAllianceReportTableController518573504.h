﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MailAllianceReportTableLine
struct MailAllianceReportTableLine_t3318082430;
// Tacticsoft.TableView
struct TableView_t3179510217;
// BattleReportHeader[]
struct BattleReportHeaderU5BU5D_t4047042280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailAllianceReportTableController
struct  MailAllianceReportTableController_t518573504  : public MonoBehaviour_t1158329972
{
public:
	// MailAllianceReportTableLine MailAllianceReportTableController::m_cellPrefab
	MailAllianceReportTableLine_t3318082430 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailAllianceReportTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Boolean MailAllianceReportTableController::alliance
	bool ___alliance_4;
	// BattleReportHeader[] MailAllianceReportTableController::allReports
	BattleReportHeaderU5BU5D_t4047042280* ___allReports_5;
	// System.Int32 MailAllianceReportTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t518573504, ___m_cellPrefab_2)); }
	inline MailAllianceReportTableLine_t3318082430 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailAllianceReportTableLine_t3318082430 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailAllianceReportTableLine_t3318082430 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t518573504, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_alliance_4() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t518573504, ___alliance_4)); }
	inline bool get_alliance_4() const { return ___alliance_4; }
	inline bool* get_address_of_alliance_4() { return &___alliance_4; }
	inline void set_alliance_4(bool value)
	{
		___alliance_4 = value;
	}

	inline static int32_t get_offset_of_allReports_5() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t518573504, ___allReports_5)); }
	inline BattleReportHeaderU5BU5D_t4047042280* get_allReports_5() const { return ___allReports_5; }
	inline BattleReportHeaderU5BU5D_t4047042280** get_address_of_allReports_5() { return &___allReports_5; }
	inline void set_allReports_5(BattleReportHeaderU5BU5D_t4047042280* value)
	{
		___allReports_5 = value;
		Il2CppCodeGenWriteBarrier(&___allReports_5, value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t518573504, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
