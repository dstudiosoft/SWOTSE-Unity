﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1504523094.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_645770832.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m325887324_gshared (InternalEnumerator_1_t1504523094 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m325887324(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1504523094 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m325887324_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m351725548_gshared (InternalEnumerator_1_t1504523094 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m351725548(__this, method) ((  void (*) (InternalEnumerator_1_t1504523094 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m351725548_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2775301482_gshared (InternalEnumerator_1_t1504523094 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2775301482(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1504523094 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2775301482_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m971296167_gshared (InternalEnumerator_1_t1504523094 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m971296167(__this, method) ((  void (*) (InternalEnumerator_1_t1504523094 *, const MethodInfo*))InternalEnumerator_1_Dispose_m971296167_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1875266708_gshared (InternalEnumerator_1_t1504523094 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1875266708(__this, method) ((  bool (*) (InternalEnumerator_1_t1504523094 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1875266708_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::get_Current()
extern "C"  KeyValuePair_2_t645770832  InternalEnumerator_1_get_Current_m2949134643_gshared (InternalEnumerator_1_t1504523094 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2949134643(__this, method) ((  KeyValuePair_2_t645770832  (*) (InternalEnumerator_1_t1504523094 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2949134643_gshared)(__this, method)
