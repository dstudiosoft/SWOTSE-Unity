﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandCenterManager
struct CommandCenterManager_t2111964453;

#include "codegen/il2cpp-codegen.h"

// System.Void CommandCenterManager::.ctor()
extern "C"  void CommandCenterManager__ctor_m3241826178 (CommandCenterManager_t2111964453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterManager::OnEnable()
extern "C"  void CommandCenterManager_OnEnable_m929384338 (CommandCenterManager_t2111964453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterManager::FixedUpdate()
extern "C"  void CommandCenterManager_FixedUpdate_m2434892067 (CommandCenterManager_t2111964453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterManager::OpenCenterDispatch()
extern "C"  void CommandCenterManager_OpenCenterDispatch_m4173675083 (CommandCenterManager_t2111964453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
