﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceGuidelineManager
struct AllianceGuidelineManager_t1315580376;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceGuidelineManager::.ctor()
extern "C"  void AllianceGuidelineManager__ctor_m3185830629 (AllianceGuidelineManager_t1315580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceGuidelineManager::Start()
extern "C"  void AllianceGuidelineManager_Start_m1321231205 (AllianceGuidelineManager_t1315580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceGuidelineManager::UpdateGuideline()
extern "C"  void AllianceGuidelineManager_UpdateGuideline_m1388013518 (AllianceGuidelineManager_t1315580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceGuidelineManager::GuideLineUpdated(System.Object,System.String)
extern "C"  void AllianceGuidelineManager_GuideLineUpdated_m2944742894 (AllianceGuidelineManager_t1315580376 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
