﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.OrderedDictionary/OrderedCollection
struct OrderedCollection_t3279434856;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Specialized.OrderedDictionary/OrderedCollection::.ctor(System.Collections.ArrayList,System.Boolean)
extern "C"  void OrderedCollection__ctor_m2236395172 (OrderedCollection_t3279434856 * __this, ArrayList_t4252133567 * ___list0, bool ___isKeyList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.OrderedDictionary/OrderedCollection::get_Count()
extern "C"  int32_t OrderedCollection_get_Count_m3734100286 (OrderedCollection_t3279434856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary/OrderedCollection::get_IsSynchronized()
extern "C"  bool OrderedCollection_get_IsSynchronized_m1343440783 (OrderedCollection_t3279434856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary/OrderedCollection::get_SyncRoot()
extern "C"  Il2CppObject * OrderedCollection_get_SyncRoot_m2831836163 (OrderedCollection_t3279434856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary/OrderedCollection::CopyTo(System.Array,System.Int32)
extern "C"  void OrderedCollection_CopyTo_m2175666327 (OrderedCollection_t3279434856 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.OrderedDictionary/OrderedCollection::GetEnumerator()
extern "C"  Il2CppObject * OrderedCollection_GetEnumerator_m440868970 (OrderedCollection_t3279434856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
