﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator
struct  OrderedEntryCollectionEnumerator_t517177738  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::listEnumerator
	Il2CppObject * ___listEnumerator_0;

public:
	inline static int32_t get_offset_of_listEnumerator_0() { return static_cast<int32_t>(offsetof(OrderedEntryCollectionEnumerator_t517177738, ___listEnumerator_0)); }
	inline Il2CppObject * get_listEnumerator_0() const { return ___listEnumerator_0; }
	inline Il2CppObject ** get_address_of_listEnumerator_0() { return &___listEnumerator_0; }
	inline void set_listEnumerator_0(Il2CppObject * value)
	{
		___listEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier(&___listEnumerator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
