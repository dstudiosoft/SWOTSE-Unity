﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// BuildingModel
struct BuildingModel_t2830632387;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// BuildingConstantModel
struct BuildingConstantModel_t2546650549;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateBuildingContentManager
struct  UpdateBuildingContentManager_t3210716219  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UpdateBuildingContentManager::upgrade
	bool ___upgrade_2;
	// System.String UpdateBuildingContentManager::type
	String_t* ___type_3;
	// System.Int64 UpdateBuildingContentManager::pitId
	int64_t ___pitId_4;
	// UnityEngine.UI.Text UpdateBuildingContentManager::contentLabel
	Text_t356221433 * ___contentLabel_5;
	// UnityEngine.UI.Text UpdateBuildingContentManager::description
	Text_t356221433 * ___description_6;
	// UnityEngine.GameObject UpdateBuildingContentManager::resources
	GameObject_t1756533147 * ___resources_7;
	// UnityEngine.GameObject UpdateBuildingContentManager::downgradeDescription
	GameObject_t1756533147 * ___downgradeDescription_8;
	// UnityEngine.UI.Text UpdateBuildingContentManager::foodNeeded
	Text_t356221433 * ___foodNeeded_9;
	// UnityEngine.UI.Text UpdateBuildingContentManager::woodNeeded
	Text_t356221433 * ___woodNeeded_10;
	// UnityEngine.UI.Text UpdateBuildingContentManager::stoneNeeded
	Text_t356221433 * ___stoneNeeded_11;
	// UnityEngine.UI.Text UpdateBuildingContentManager::ironNeeded
	Text_t356221433 * ___ironNeeded_12;
	// UnityEngine.UI.Text UpdateBuildingContentManager::copperNeeded
	Text_t356221433 * ___copperNeeded_13;
	// UnityEngine.UI.Text UpdateBuildingContentManager::silverNeeded
	Text_t356221433 * ___silverNeeded_14;
	// UnityEngine.UI.Text UpdateBuildingContentManager::goldNeeded
	Text_t356221433 * ___goldNeeded_15;
	// UnityEngine.UI.Text UpdateBuildingContentManager::foodPresent
	Text_t356221433 * ___foodPresent_16;
	// UnityEngine.UI.Text UpdateBuildingContentManager::woodPresent
	Text_t356221433 * ___woodPresent_17;
	// UnityEngine.UI.Text UpdateBuildingContentManager::stonePresent
	Text_t356221433 * ___stonePresent_18;
	// UnityEngine.UI.Text UpdateBuildingContentManager::ironPresent
	Text_t356221433 * ___ironPresent_19;
	// UnityEngine.UI.Text UpdateBuildingContentManager::copperPresent
	Text_t356221433 * ___copperPresent_20;
	// UnityEngine.UI.Text UpdateBuildingContentManager::silverPresent
	Text_t356221433 * ___silverPresent_21;
	// UnityEngine.UI.Text UpdateBuildingContentManager::goldPresent
	Text_t356221433 * ___goldPresent_22;
	// BuildingModel UpdateBuildingContentManager::building
	BuildingModel_t2830632387 * ___building_23;
	// WorldFieldModel UpdateBuildingContentManager::valley
	WorldFieldModel_t3469935653 * ___valley_24;
	// BuildingConstantModel UpdateBuildingContentManager::constant
	BuildingConstantModel_t2546650549 * ___constant_25;
	// ResourcesModel UpdateBuildingContentManager::cityResources
	ResourcesModel_t2978985958 * ___cityResources_26;

public:
	inline static int32_t get_offset_of_upgrade_2() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___upgrade_2)); }
	inline bool get_upgrade_2() const { return ___upgrade_2; }
	inline bool* get_address_of_upgrade_2() { return &___upgrade_2; }
	inline void set_upgrade_2(bool value)
	{
		___upgrade_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___type_3)); }
	inline String_t* get_type_3() const { return ___type_3; }
	inline String_t** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(String_t* value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier(&___type_3, value);
	}

	inline static int32_t get_offset_of_pitId_4() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___pitId_4)); }
	inline int64_t get_pitId_4() const { return ___pitId_4; }
	inline int64_t* get_address_of_pitId_4() { return &___pitId_4; }
	inline void set_pitId_4(int64_t value)
	{
		___pitId_4 = value;
	}

	inline static int32_t get_offset_of_contentLabel_5() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___contentLabel_5)); }
	inline Text_t356221433 * get_contentLabel_5() const { return ___contentLabel_5; }
	inline Text_t356221433 ** get_address_of_contentLabel_5() { return &___contentLabel_5; }
	inline void set_contentLabel_5(Text_t356221433 * value)
	{
		___contentLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___contentLabel_5, value);
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___description_6)); }
	inline Text_t356221433 * get_description_6() const { return ___description_6; }
	inline Text_t356221433 ** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(Text_t356221433 * value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier(&___description_6, value);
	}

	inline static int32_t get_offset_of_resources_7() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___resources_7)); }
	inline GameObject_t1756533147 * get_resources_7() const { return ___resources_7; }
	inline GameObject_t1756533147 ** get_address_of_resources_7() { return &___resources_7; }
	inline void set_resources_7(GameObject_t1756533147 * value)
	{
		___resources_7 = value;
		Il2CppCodeGenWriteBarrier(&___resources_7, value);
	}

	inline static int32_t get_offset_of_downgradeDescription_8() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___downgradeDescription_8)); }
	inline GameObject_t1756533147 * get_downgradeDescription_8() const { return ___downgradeDescription_8; }
	inline GameObject_t1756533147 ** get_address_of_downgradeDescription_8() { return &___downgradeDescription_8; }
	inline void set_downgradeDescription_8(GameObject_t1756533147 * value)
	{
		___downgradeDescription_8 = value;
		Il2CppCodeGenWriteBarrier(&___downgradeDescription_8, value);
	}

	inline static int32_t get_offset_of_foodNeeded_9() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___foodNeeded_9)); }
	inline Text_t356221433 * get_foodNeeded_9() const { return ___foodNeeded_9; }
	inline Text_t356221433 ** get_address_of_foodNeeded_9() { return &___foodNeeded_9; }
	inline void set_foodNeeded_9(Text_t356221433 * value)
	{
		___foodNeeded_9 = value;
		Il2CppCodeGenWriteBarrier(&___foodNeeded_9, value);
	}

	inline static int32_t get_offset_of_woodNeeded_10() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___woodNeeded_10)); }
	inline Text_t356221433 * get_woodNeeded_10() const { return ___woodNeeded_10; }
	inline Text_t356221433 ** get_address_of_woodNeeded_10() { return &___woodNeeded_10; }
	inline void set_woodNeeded_10(Text_t356221433 * value)
	{
		___woodNeeded_10 = value;
		Il2CppCodeGenWriteBarrier(&___woodNeeded_10, value);
	}

	inline static int32_t get_offset_of_stoneNeeded_11() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___stoneNeeded_11)); }
	inline Text_t356221433 * get_stoneNeeded_11() const { return ___stoneNeeded_11; }
	inline Text_t356221433 ** get_address_of_stoneNeeded_11() { return &___stoneNeeded_11; }
	inline void set_stoneNeeded_11(Text_t356221433 * value)
	{
		___stoneNeeded_11 = value;
		Il2CppCodeGenWriteBarrier(&___stoneNeeded_11, value);
	}

	inline static int32_t get_offset_of_ironNeeded_12() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___ironNeeded_12)); }
	inline Text_t356221433 * get_ironNeeded_12() const { return ___ironNeeded_12; }
	inline Text_t356221433 ** get_address_of_ironNeeded_12() { return &___ironNeeded_12; }
	inline void set_ironNeeded_12(Text_t356221433 * value)
	{
		___ironNeeded_12 = value;
		Il2CppCodeGenWriteBarrier(&___ironNeeded_12, value);
	}

	inline static int32_t get_offset_of_copperNeeded_13() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___copperNeeded_13)); }
	inline Text_t356221433 * get_copperNeeded_13() const { return ___copperNeeded_13; }
	inline Text_t356221433 ** get_address_of_copperNeeded_13() { return &___copperNeeded_13; }
	inline void set_copperNeeded_13(Text_t356221433 * value)
	{
		___copperNeeded_13 = value;
		Il2CppCodeGenWriteBarrier(&___copperNeeded_13, value);
	}

	inline static int32_t get_offset_of_silverNeeded_14() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___silverNeeded_14)); }
	inline Text_t356221433 * get_silverNeeded_14() const { return ___silverNeeded_14; }
	inline Text_t356221433 ** get_address_of_silverNeeded_14() { return &___silverNeeded_14; }
	inline void set_silverNeeded_14(Text_t356221433 * value)
	{
		___silverNeeded_14 = value;
		Il2CppCodeGenWriteBarrier(&___silverNeeded_14, value);
	}

	inline static int32_t get_offset_of_goldNeeded_15() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___goldNeeded_15)); }
	inline Text_t356221433 * get_goldNeeded_15() const { return ___goldNeeded_15; }
	inline Text_t356221433 ** get_address_of_goldNeeded_15() { return &___goldNeeded_15; }
	inline void set_goldNeeded_15(Text_t356221433 * value)
	{
		___goldNeeded_15 = value;
		Il2CppCodeGenWriteBarrier(&___goldNeeded_15, value);
	}

	inline static int32_t get_offset_of_foodPresent_16() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___foodPresent_16)); }
	inline Text_t356221433 * get_foodPresent_16() const { return ___foodPresent_16; }
	inline Text_t356221433 ** get_address_of_foodPresent_16() { return &___foodPresent_16; }
	inline void set_foodPresent_16(Text_t356221433 * value)
	{
		___foodPresent_16 = value;
		Il2CppCodeGenWriteBarrier(&___foodPresent_16, value);
	}

	inline static int32_t get_offset_of_woodPresent_17() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___woodPresent_17)); }
	inline Text_t356221433 * get_woodPresent_17() const { return ___woodPresent_17; }
	inline Text_t356221433 ** get_address_of_woodPresent_17() { return &___woodPresent_17; }
	inline void set_woodPresent_17(Text_t356221433 * value)
	{
		___woodPresent_17 = value;
		Il2CppCodeGenWriteBarrier(&___woodPresent_17, value);
	}

	inline static int32_t get_offset_of_stonePresent_18() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___stonePresent_18)); }
	inline Text_t356221433 * get_stonePresent_18() const { return ___stonePresent_18; }
	inline Text_t356221433 ** get_address_of_stonePresent_18() { return &___stonePresent_18; }
	inline void set_stonePresent_18(Text_t356221433 * value)
	{
		___stonePresent_18 = value;
		Il2CppCodeGenWriteBarrier(&___stonePresent_18, value);
	}

	inline static int32_t get_offset_of_ironPresent_19() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___ironPresent_19)); }
	inline Text_t356221433 * get_ironPresent_19() const { return ___ironPresent_19; }
	inline Text_t356221433 ** get_address_of_ironPresent_19() { return &___ironPresent_19; }
	inline void set_ironPresent_19(Text_t356221433 * value)
	{
		___ironPresent_19 = value;
		Il2CppCodeGenWriteBarrier(&___ironPresent_19, value);
	}

	inline static int32_t get_offset_of_copperPresent_20() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___copperPresent_20)); }
	inline Text_t356221433 * get_copperPresent_20() const { return ___copperPresent_20; }
	inline Text_t356221433 ** get_address_of_copperPresent_20() { return &___copperPresent_20; }
	inline void set_copperPresent_20(Text_t356221433 * value)
	{
		___copperPresent_20 = value;
		Il2CppCodeGenWriteBarrier(&___copperPresent_20, value);
	}

	inline static int32_t get_offset_of_silverPresent_21() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___silverPresent_21)); }
	inline Text_t356221433 * get_silverPresent_21() const { return ___silverPresent_21; }
	inline Text_t356221433 ** get_address_of_silverPresent_21() { return &___silverPresent_21; }
	inline void set_silverPresent_21(Text_t356221433 * value)
	{
		___silverPresent_21 = value;
		Il2CppCodeGenWriteBarrier(&___silverPresent_21, value);
	}

	inline static int32_t get_offset_of_goldPresent_22() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___goldPresent_22)); }
	inline Text_t356221433 * get_goldPresent_22() const { return ___goldPresent_22; }
	inline Text_t356221433 ** get_address_of_goldPresent_22() { return &___goldPresent_22; }
	inline void set_goldPresent_22(Text_t356221433 * value)
	{
		___goldPresent_22 = value;
		Il2CppCodeGenWriteBarrier(&___goldPresent_22, value);
	}

	inline static int32_t get_offset_of_building_23() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___building_23)); }
	inline BuildingModel_t2830632387 * get_building_23() const { return ___building_23; }
	inline BuildingModel_t2830632387 ** get_address_of_building_23() { return &___building_23; }
	inline void set_building_23(BuildingModel_t2830632387 * value)
	{
		___building_23 = value;
		Il2CppCodeGenWriteBarrier(&___building_23, value);
	}

	inline static int32_t get_offset_of_valley_24() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___valley_24)); }
	inline WorldFieldModel_t3469935653 * get_valley_24() const { return ___valley_24; }
	inline WorldFieldModel_t3469935653 ** get_address_of_valley_24() { return &___valley_24; }
	inline void set_valley_24(WorldFieldModel_t3469935653 * value)
	{
		___valley_24 = value;
		Il2CppCodeGenWriteBarrier(&___valley_24, value);
	}

	inline static int32_t get_offset_of_constant_25() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___constant_25)); }
	inline BuildingConstantModel_t2546650549 * get_constant_25() const { return ___constant_25; }
	inline BuildingConstantModel_t2546650549 ** get_address_of_constant_25() { return &___constant_25; }
	inline void set_constant_25(BuildingConstantModel_t2546650549 * value)
	{
		___constant_25 = value;
		Il2CppCodeGenWriteBarrier(&___constant_25, value);
	}

	inline static int32_t get_offset_of_cityResources_26() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219, ___cityResources_26)); }
	inline ResourcesModel_t2978985958 * get_cityResources_26() const { return ___cityResources_26; }
	inline ResourcesModel_t2978985958 ** get_address_of_cityResources_26() { return &___cityResources_26; }
	inline void set_cityResources_26(ResourcesModel_t2978985958 * value)
	{
		___cityResources_26 = value;
		Il2CppCodeGenWriteBarrier(&___cityResources_26, value);
	}
};

struct UpdateBuildingContentManager_t3210716219_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UpdateBuildingContentManager::<>f__switch$map18
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map18_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map18_27() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t3210716219_StaticFields, ___U3CU3Ef__switchU24map18_27)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map18_27() const { return ___U3CU3Ef__switchU24map18_27; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map18_27() { return &___U3CU3Ef__switchU24map18_27; }
	inline void set_U3CU3Ef__switchU24map18_27(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map18_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map18_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
