﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.Default.AppConfigExtensions
struct AppConfigExtensions_t2317624261;
// Firebase.Platform.IAppConfigExtensions
struct IAppConfigExtensions_t273226812;
// System.String
struct String_t;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Platform.Default.AppConfigExtensions::.ctor()
extern "C"  void AppConfigExtensions__ctor_m515335031 (AppConfigExtensions_t2317624261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::get_Instance()
extern "C"  Il2CppObject * AppConfigExtensions_get_Instance_m2995693818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Platform.Default.AppConfigExtensions::GetWriteablePath(Firebase.FirebaseApp)
extern "C"  String_t* AppConfigExtensions_GetWriteablePath_m3529270025 (AppConfigExtensions_t2317624261 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Platform.Default.AppConfigExtensions::GetCertPemFile(Firebase.FirebaseApp)
extern "C"  String_t* AppConfigExtensions_GetCertPemFile_m4028732833 (AppConfigExtensions_t2317624261 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Default.AppConfigExtensions::.cctor()
extern "C"  void AppConfigExtensions__cctor_m3015227318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
