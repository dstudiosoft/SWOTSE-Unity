﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// MarketLotModel
struct MarketLotModel_t272455820;
// System.Object
struct Il2CppObject;
// MarketManager
struct MarketManager_t2047363881;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat875733053.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<GetLotsToBuy>c__IteratorE
struct  U3CGetLotsToBuyU3Ec__IteratorE_t2036023919  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest MarketManager/<GetLotsToBuy>c__IteratorE::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// JSONObject MarketManager/<GetLotsToBuy>c__IteratorE::<obj>__1
	JSONObject_t1971882247 * ___U3CobjU3E__1_1;
	// JSONObject MarketManager/<GetLotsToBuy>c__IteratorE::<lotsArray>__2
	JSONObject_t1971882247 * ___U3ClotsArrayU3E__2_2;
	// System.Collections.Generic.List`1/Enumerator<JSONObject> MarketManager/<GetLotsToBuy>c__IteratorE::<$s_38>__3
	Enumerator_t875733053  ___U3CU24s_38U3E__3_3;
	// JSONObject MarketManager/<GetLotsToBuy>c__IteratorE::<lot>__4
	JSONObject_t1971882247 * ___U3ClotU3E__4_4;
	// MarketLotModel MarketManager/<GetLotsToBuy>c__IteratorE::<lotModel>__5
	MarketLotModel_t272455820 * ___U3ClotModelU3E__5_5;
	// System.Int32 MarketManager/<GetLotsToBuy>c__IteratorE::<j>__6
	int32_t ___U3CjU3E__6_6;
	// System.Int32 MarketManager/<GetLotsToBuy>c__IteratorE::<i>__7
	int32_t ___U3CiU3E__7_7;
	// MarketLotModel MarketManager/<GetLotsToBuy>c__IteratorE::<temp>__8
	MarketLotModel_t272455820 * ___U3CtempU3E__8_8;
	// System.Int32 MarketManager/<GetLotsToBuy>c__IteratorE::$PC
	int32_t ___U24PC_9;
	// System.Object MarketManager/<GetLotsToBuy>c__IteratorE::$current
	Il2CppObject * ___U24current_10;
	// MarketManager MarketManager/<GetLotsToBuy>c__IteratorE::<>f__this
	MarketManager_t2047363881 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CobjU3E__1_1)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__1_1() const { return ___U3CobjU3E__1_1; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__1_1() { return &___U3CobjU3E__1_1; }
	inline void set_U3CobjU3E__1_1(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3ClotsArrayU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3ClotsArrayU3E__2_2)); }
	inline JSONObject_t1971882247 * get_U3ClotsArrayU3E__2_2() const { return ___U3ClotsArrayU3E__2_2; }
	inline JSONObject_t1971882247 ** get_address_of_U3ClotsArrayU3E__2_2() { return &___U3ClotsArrayU3E__2_2; }
	inline void set_U3ClotsArrayU3E__2_2(JSONObject_t1971882247 * value)
	{
		___U3ClotsArrayU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClotsArrayU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_38U3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CU24s_38U3E__3_3)); }
	inline Enumerator_t875733053  get_U3CU24s_38U3E__3_3() const { return ___U3CU24s_38U3E__3_3; }
	inline Enumerator_t875733053 * get_address_of_U3CU24s_38U3E__3_3() { return &___U3CU24s_38U3E__3_3; }
	inline void set_U3CU24s_38U3E__3_3(Enumerator_t875733053  value)
	{
		___U3CU24s_38U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3ClotU3E__4_4() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3ClotU3E__4_4)); }
	inline JSONObject_t1971882247 * get_U3ClotU3E__4_4() const { return ___U3ClotU3E__4_4; }
	inline JSONObject_t1971882247 ** get_address_of_U3ClotU3E__4_4() { return &___U3ClotU3E__4_4; }
	inline void set_U3ClotU3E__4_4(JSONObject_t1971882247 * value)
	{
		___U3ClotU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClotU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3ClotModelU3E__5_5() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3ClotModelU3E__5_5)); }
	inline MarketLotModel_t272455820 * get_U3ClotModelU3E__5_5() const { return ___U3ClotModelU3E__5_5; }
	inline MarketLotModel_t272455820 ** get_address_of_U3ClotModelU3E__5_5() { return &___U3ClotModelU3E__5_5; }
	inline void set_U3ClotModelU3E__5_5(MarketLotModel_t272455820 * value)
	{
		___U3ClotModelU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClotModelU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CjU3E__6_6() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CjU3E__6_6)); }
	inline int32_t get_U3CjU3E__6_6() const { return ___U3CjU3E__6_6; }
	inline int32_t* get_address_of_U3CjU3E__6_6() { return &___U3CjU3E__6_6; }
	inline void set_U3CjU3E__6_6(int32_t value)
	{
		___U3CjU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__7_7() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CiU3E__7_7)); }
	inline int32_t get_U3CiU3E__7_7() const { return ___U3CiU3E__7_7; }
	inline int32_t* get_address_of_U3CiU3E__7_7() { return &___U3CiU3E__7_7; }
	inline void set_U3CiU3E__7_7(int32_t value)
	{
		___U3CiU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__8_8() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CtempU3E__8_8)); }
	inline MarketLotModel_t272455820 * get_U3CtempU3E__8_8() const { return ___U3CtempU3E__8_8; }
	inline MarketLotModel_t272455820 ** get_address_of_U3CtempU3E__8_8() { return &___U3CtempU3E__8_8; }
	inline void set_U3CtempU3E__8_8(MarketLotModel_t272455820 * value)
	{
		___U3CtempU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__IteratorE_t2036023919, ___U3CU3Ef__this_11)); }
	inline MarketManager_t2047363881 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline MarketManager_t2047363881 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(MarketManager_t2047363881 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
