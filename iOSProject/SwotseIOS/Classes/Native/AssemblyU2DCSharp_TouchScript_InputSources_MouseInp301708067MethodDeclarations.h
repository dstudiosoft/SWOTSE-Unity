﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.MouseInput
struct MouseInput_t301708067;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.InputSources.MouseInput::.ctor()
extern "C"  void MouseInput__ctor_m1876491266 (MouseInput_t301708067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MouseInput::UpdateInput()
extern "C"  void MouseInput_UpdateInput_m2561416353 (MouseInput_t301708067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MouseInput::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  void MouseInput_CancelTouch_m241471239 (MouseInput_t301708067 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MouseInput::OnEnable()
extern "C"  void MouseInput_OnEnable_m2464723118 (MouseInput_t301708067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MouseInput::OnDisable()
extern "C"  void MouseInput_OnDisable_m145058391 (MouseInput_t301708067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
