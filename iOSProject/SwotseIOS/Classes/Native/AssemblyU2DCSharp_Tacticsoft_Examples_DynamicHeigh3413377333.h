﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.Examples.DynamicHeightCell
struct DynamicHeightCell_t1164024910;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.DynamicHeightTableViewController
struct  DynamicHeightTableViewController_t3413377333  : public MonoBehaviour_t1158329972
{
public:
	// Tacticsoft.Examples.DynamicHeightCell Tacticsoft.Examples.DynamicHeightTableViewController::m_cellPrefab
	DynamicHeightCell_t1164024910 * ___m_cellPrefab_2;
	// Tacticsoft.TableView Tacticsoft.Examples.DynamicHeightTableViewController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Int32 Tacticsoft.Examples.DynamicHeightTableViewController::m_numRows
	int32_t ___m_numRows_4;
	// System.Int32 Tacticsoft.Examples.DynamicHeightTableViewController::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Tacticsoft.Examples.DynamicHeightTableViewController::m_customRowHeights
	Dictionary_2_t1084335567 * ___m_customRowHeights_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t3413377333, ___m_cellPrefab_2)); }
	inline DynamicHeightCell_t1164024910 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline DynamicHeightCell_t1164024910 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(DynamicHeightCell_t1164024910 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t3413377333, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t3413377333, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_5() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t3413377333, ___m_numInstancesCreated_5)); }
	inline int32_t get_m_numInstancesCreated_5() const { return ___m_numInstancesCreated_5; }
	inline int32_t* get_address_of_m_numInstancesCreated_5() { return &___m_numInstancesCreated_5; }
	inline void set_m_numInstancesCreated_5(int32_t value)
	{
		___m_numInstancesCreated_5 = value;
	}

	inline static int32_t get_offset_of_m_customRowHeights_6() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t3413377333, ___m_customRowHeights_6)); }
	inline Dictionary_2_t1084335567 * get_m_customRowHeights_6() const { return ___m_customRowHeights_6; }
	inline Dictionary_2_t1084335567 ** get_address_of_m_customRowHeights_6() { return &___m_customRowHeights_6; }
	inline void set_m_customRowHeights_6(Dictionary_2_t1084335567 * value)
	{
		___m_customRowHeights_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_customRowHeights_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
