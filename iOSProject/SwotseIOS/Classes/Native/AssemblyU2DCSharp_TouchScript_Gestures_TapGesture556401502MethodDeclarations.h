﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.TapGesture
struct TapGesture_t556401502;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Gestures.TapGesture::.ctor()
extern "C"  void TapGesture__ctor_m883660659 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::add_Tapped(System.EventHandler`1<System.EventArgs>)
extern "C"  void TapGesture_add_Tapped_m2373555573 (TapGesture_t556401502 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::remove_Tapped(System.EventHandler`1<System.EventArgs>)
extern "C"  void TapGesture_remove_Tapped_m1334800700 (TapGesture_t556401502 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Gestures.TapGesture::get_NumberOfTapsRequired()
extern "C"  int32_t TapGesture_get_NumberOfTapsRequired_m4162317817 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::set_NumberOfTapsRequired(System.Int32)
extern "C"  void TapGesture_set_NumberOfTapsRequired_m2399184556 (TapGesture_t556401502 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.TapGesture::get_TimeLimit()
extern "C"  float TapGesture_get_TimeLimit_m4248879030 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::set_TimeLimit(System.Single)
extern "C"  void TapGesture_set_TimeLimit_m2681484467 (TapGesture_t556401502 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.TapGesture::get_DistanceLimit()
extern "C"  float TapGesture_get_DistanceLimit_m2907232436 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::set_DistanceLimit(System.Single)
extern "C"  void TapGesture_set_DistanceLimit_m581044385 (TapGesture_t556401502 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::OnEnable()
extern "C"  void TapGesture_OnEnable_m176779175 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TapGesture_touchesBegan_m2815431461 (TapGesture_t556401502 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TapGesture_touchesMoved_m3262678639 (TapGesture_t556401502 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TapGesture_touchesEnded_m1269654420 (TapGesture_t556401502 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::onRecognized()
extern "C"  void TapGesture_onRecognized_m1393016350 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TapGesture::reset()
extern "C"  void TapGesture_reset_m2556454832 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.TapGesture::shouldCacheTouchPosition(TouchScript.TouchPoint)
extern "C"  bool TapGesture_shouldCacheTouchPosition_m792958075 (TapGesture_t556401502 * __this, TouchPoint_t959629083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchScript.Gestures.TapGesture::wait()
extern "C"  Il2CppObject * TapGesture_wait_m1286413414 (TapGesture_t556401502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
