﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewMailPlayerLine
struct NewMailPlayerLine_t3159573886;
// NewMailContentManager
struct NewMailContentManager_t2045651277;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NewMailContentManager2045651277.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NewMailPlayerLine::.ctor()
extern "C"  void NewMailPlayerLine__ctor_m495915211 (NewMailPlayerLine_t3159573886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayerLine::SetManager(NewMailContentManager)
extern "C"  void NewMailPlayerLine_SetManager_m3988953577 (NewMailPlayerLine_t3159573886 * __this, NewMailContentManager_t2045651277 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayerLine::SetPlayerId(System.Int64)
extern "C"  void NewMailPlayerLine_SetPlayerId_m2077126765 (NewMailPlayerLine_t3159573886 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayerLine::SetPlayerName(System.String)
extern "C"  void NewMailPlayerLine_SetPlayerName_m4099380127 (NewMailPlayerLine_t3159573886 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayerLine::SendUserMessage()
extern "C"  void NewMailPlayerLine_SendUserMessage_m3278104813 (NewMailPlayerLine_t3159573886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
