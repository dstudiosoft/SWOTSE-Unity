﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.UriBuilder
struct UriBuilder_t2016461725;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.UriBuilder::.ctor()
extern "C"  void UriBuilder__ctor_m3402167180 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.String)
extern "C"  void UriBuilder__ctor_m2114669814 (UriBuilder_t2016461725 * __this, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.Uri)
extern "C"  void UriBuilder__ctor_m2466436215 (UriBuilder_t2016461725 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.String,System.String)
extern "C"  void UriBuilder__ctor_m679401258 (UriBuilder_t2016461725 * __this, String_t* ___schemeName0, String_t* ___hostName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.String,System.String,System.Int32)
extern "C"  void UriBuilder__ctor_m1344278439 (UriBuilder_t2016461725 * __this, String_t* ___scheme0, String_t* ___host1, int32_t ___portNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.String,System.String,System.Int32,System.String)
extern "C"  void UriBuilder__ctor_m159079961 (UriBuilder_t2016461725 * __this, String_t* ___scheme0, String_t* ___host1, int32_t ___port2, String_t* ___pathValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::.ctor(System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void UriBuilder__ctor_m365341671 (UriBuilder_t2016461725 * __this, String_t* ___scheme0, String_t* ___host1, int32_t ___port2, String_t* ___pathValue3, String_t* ___extraValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Fragment()
extern "C"  String_t* UriBuilder_get_Fragment_m337437088 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Fragment(System.String)
extern "C"  void UriBuilder_set_Fragment_m2608259137 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Host()
extern "C"  String_t* UriBuilder_get_Host_m730544282 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Host(System.String)
extern "C"  void UriBuilder_set_Host_m1829532633 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Password()
extern "C"  String_t* UriBuilder_get_Password_m1900644093 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Password(System.String)
extern "C"  void UriBuilder_set_Password_m2092652892 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Path()
extern "C"  String_t* UriBuilder_get_Path_m4040208335 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Path(System.String)
extern "C"  void UriBuilder_set_Path_m1291193322 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UriBuilder::get_Port()
extern "C"  int32_t UriBuilder_get_Port_m2107260188 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Port(System.Int32)
extern "C"  void UriBuilder_set_Port_m399873979 (UriBuilder_t2016461725 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Query()
extern "C"  String_t* UriBuilder_get_Query_m471431858 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Query(System.String)
extern "C"  void UriBuilder_set_Query_m209196739 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_Scheme()
extern "C"  String_t* UriBuilder_get_Scheme_m1038875007 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_Scheme(System.String)
extern "C"  void UriBuilder_set_Scheme_m920228366 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.UriBuilder::get_Uri()
extern "C"  Uri_t19570940 * UriBuilder_get_Uri_m1365242951 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::get_UserName()
extern "C"  String_t* UriBuilder_get_UserName_m2831706084 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UriBuilder::set_UserName(System.String)
extern "C"  void UriBuilder_set_UserName_m3190783731 (UriBuilder_t2016461725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UriBuilder::Equals(System.Object)
extern "C"  bool UriBuilder_Equals_m1682960663 (UriBuilder_t2016461725 * __this, Il2CppObject * ___rparam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UriBuilder::GetHashCode()
extern "C"  int32_t UriBuilder_GetHashCode_m2846873589 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UriBuilder::ToString()
extern "C"  String_t* UriBuilder_ToString_m2442590737 (UriBuilder_t2016461725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
