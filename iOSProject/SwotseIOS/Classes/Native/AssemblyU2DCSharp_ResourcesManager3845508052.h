﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesManager
struct  ResourcesManager_t3845508052  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ResourcesManager::cityFood
	Text_t356221433 * ___cityFood_2;
	// UnityEngine.UI.Text ResourcesManager::cityWood
	Text_t356221433 * ___cityWood_3;
	// UnityEngine.UI.Text ResourcesManager::cityIron
	Text_t356221433 * ___cityIron_4;
	// UnityEngine.UI.Text ResourcesManager::cityStone
	Text_t356221433 * ___cityStone_5;
	// UnityEngine.UI.Text ResourcesManager::cityCopper
	Text_t356221433 * ___cityCopper_6;
	// UnityEngine.UI.Text ResourcesManager::citySilver
	Text_t356221433 * ___citySilver_7;
	// UnityEngine.UI.Text ResourcesManager::cityGold
	Text_t356221433 * ___cityGold_8;

public:
	inline static int32_t get_offset_of_cityFood_2() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityFood_2)); }
	inline Text_t356221433 * get_cityFood_2() const { return ___cityFood_2; }
	inline Text_t356221433 ** get_address_of_cityFood_2() { return &___cityFood_2; }
	inline void set_cityFood_2(Text_t356221433 * value)
	{
		___cityFood_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityFood_2, value);
	}

	inline static int32_t get_offset_of_cityWood_3() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityWood_3)); }
	inline Text_t356221433 * get_cityWood_3() const { return ___cityWood_3; }
	inline Text_t356221433 ** get_address_of_cityWood_3() { return &___cityWood_3; }
	inline void set_cityWood_3(Text_t356221433 * value)
	{
		___cityWood_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityWood_3, value);
	}

	inline static int32_t get_offset_of_cityIron_4() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityIron_4)); }
	inline Text_t356221433 * get_cityIron_4() const { return ___cityIron_4; }
	inline Text_t356221433 ** get_address_of_cityIron_4() { return &___cityIron_4; }
	inline void set_cityIron_4(Text_t356221433 * value)
	{
		___cityIron_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityIron_4, value);
	}

	inline static int32_t get_offset_of_cityStone_5() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityStone_5)); }
	inline Text_t356221433 * get_cityStone_5() const { return ___cityStone_5; }
	inline Text_t356221433 ** get_address_of_cityStone_5() { return &___cityStone_5; }
	inline void set_cityStone_5(Text_t356221433 * value)
	{
		___cityStone_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityStone_5, value);
	}

	inline static int32_t get_offset_of_cityCopper_6() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityCopper_6)); }
	inline Text_t356221433 * get_cityCopper_6() const { return ___cityCopper_6; }
	inline Text_t356221433 ** get_address_of_cityCopper_6() { return &___cityCopper_6; }
	inline void set_cityCopper_6(Text_t356221433 * value)
	{
		___cityCopper_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityCopper_6, value);
	}

	inline static int32_t get_offset_of_citySilver_7() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___citySilver_7)); }
	inline Text_t356221433 * get_citySilver_7() const { return ___citySilver_7; }
	inline Text_t356221433 ** get_address_of_citySilver_7() { return &___citySilver_7; }
	inline void set_citySilver_7(Text_t356221433 * value)
	{
		___citySilver_7 = value;
		Il2CppCodeGenWriteBarrier(&___citySilver_7, value);
	}

	inline static int32_t get_offset_of_cityGold_8() { return static_cast<int32_t>(offsetof(ResourcesManager_t3845508052, ___cityGold_8)); }
	inline Text_t356221433 * get_cityGold_8() const { return ___cityGold_8; }
	inline Text_t356221433 ** get_address_of_cityGold_8() { return &___cityGold_8; }
	inline void set_cityGold_8(Text_t356221433 * value)
	{
		___cityGold_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityGold_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
