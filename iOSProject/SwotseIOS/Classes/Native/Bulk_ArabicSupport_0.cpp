﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Collections.Generic.List`1<TashkeelLocation>
struct List_1_t828523468;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Char[]
struct CharU5BU5D_t3528271667;
// TashkeelLocation
struct TashkeelLocation_t3651416022;
// System.String[]
struct StringU5BU5D_t1281789340;
// ArabicTable
struct ArabicTable_t2015225755;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t811567916;
// ArabicMapping
struct ArabicMapping_t3619416640;
// System.Collections.Generic.List`1<ArabicMapping>
struct List_1_t796524086;
// ArabicMapping[]
struct ArabicMappingU5BU5D_t981329089;
// TashkeelLocation[]
struct TashkeelLocationU5BU5D_t884365331;
// System.Void
struct Void_t1185182177;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;

extern RuntimeClass* List_1_t828523468_il2cpp_TypeInfo_var;
extern RuntimeClass* TashkeelLocation_t3651416022_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1050387868_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1198340228_RuntimeMethod_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573____U24U24method0x6000009U2D1_0_FieldInfo_var;
extern String_t* _stringLiteral757602046;
extern const uint32_t ArabicFixerTool_RemoveTashkeel_m2173189906_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2618350689_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1991375571_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1427298682_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2831730375_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1442584936_RuntimeMethod_var;
extern const uint32_t ArabicFixerTool_ReturnTashkeel_m1878862093_MetadataUsageId;
extern RuntimeClass* ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t811567916_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1709633523_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4264982211_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2041960438_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m634868510_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2890230691_RuntimeMethod_var;
extern String_t* _stringLiteral3452614528;
extern const uint32_t ArabicFixerTool_FixLine_m2584389017_MetadataUsageId;
extern const uint32_t ArabicFixerTool_IsIgnoredCharacter_m803703993_MetadataUsageId;
extern const uint32_t ArabicFixerTool_IsLeadingLetter_m2995983843_MetadataUsageId;
extern const uint32_t ArabicFixerTool_IsFinishingLetter_m1180940598_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t ArabicFixerTool_IsMiddleLetter_m3068743487_MetadataUsageId;
extern const uint32_t ArabicFixerTool__cctor_m2328499328_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3452614566;
extern const uint32_t ArabicFixer_Fix_m4044602794_MetadataUsageId;
extern RuntimeClass* List_1_t796524086_il2cpp_TypeInfo_var;
extern RuntimeClass* ArabicTable_t2015225755_il2cpp_TypeInfo_var;
extern RuntimeClass* ArabicMapping_t3619416640_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2019409646_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1908758304_RuntimeMethod_var;
extern const uint32_t ArabicTable__ctor_m3313828430_MetadataUsageId;
extern const uint32_t ArabicTable_get_ArabicMapper_m331737571_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m3336618809_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2544204511_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2035344556_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3914768235_RuntimeMethod_var;
extern const uint32_t ArabicTable_Convert_m3785855912_MetadataUsageId;

struct CharU5BU5D_t3528271667;
struct StringU5BU5D_t1281789340;


#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_T796524086_H
#define LIST_1_T796524086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<ArabicMapping>
struct  List_1_t796524086  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ArabicMappingU5BU5D_t981329089* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t796524086, ____items_1)); }
	inline ArabicMappingU5BU5D_t981329089* get__items_1() const { return ____items_1; }
	inline ArabicMappingU5BU5D_t981329089** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ArabicMappingU5BU5D_t981329089* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t796524086, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t796524086, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t796524086_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ArabicMappingU5BU5D_t981329089* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t796524086_StaticFields, ___EmptyArray_4)); }
	inline ArabicMappingU5BU5D_t981329089* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ArabicMappingU5BU5D_t981329089** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ArabicMappingU5BU5D_t981329089* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T796524086_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LIST_1_T811567916_H
#define LIST_1_T811567916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Char>
struct  List_1_t811567916  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharU5BU5D_t3528271667* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____items_1)); }
	inline CharU5BU5D_t3528271667* get__items_1() const { return ____items_1; }
	inline CharU5BU5D_t3528271667** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharU5BU5D_t3528271667* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t811567916, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t811567916_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CharU5BU5D_t3528271667* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t811567916_StaticFields, ___EmptyArray_4)); }
	inline CharU5BU5D_t3528271667* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CharU5BU5D_t3528271667** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CharU5BU5D_t3528271667* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T811567916_H
#ifndef ARABICTABLE_T2015225755_H
#define ARABICTABLE_T2015225755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicTable
struct  ArabicTable_t2015225755  : public RuntimeObject
{
public:

public:
};

struct ArabicTable_t2015225755_StaticFields
{
public:
	// System.Collections.Generic.List`1<ArabicMapping> ArabicTable::mapList
	List_1_t796524086 * ___mapList_0;
	// ArabicTable ArabicTable::arabicMapper
	ArabicTable_t2015225755 * ___arabicMapper_1;

public:
	inline static int32_t get_offset_of_mapList_0() { return static_cast<int32_t>(offsetof(ArabicTable_t2015225755_StaticFields, ___mapList_0)); }
	inline List_1_t796524086 * get_mapList_0() const { return ___mapList_0; }
	inline List_1_t796524086 ** get_address_of_mapList_0() { return &___mapList_0; }
	inline void set_mapList_0(List_1_t796524086 * value)
	{
		___mapList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapList_0), value);
	}

	inline static int32_t get_offset_of_arabicMapper_1() { return static_cast<int32_t>(offsetof(ArabicTable_t2015225755_StaticFields, ___arabicMapper_1)); }
	inline ArabicTable_t2015225755 * get_arabicMapper_1() const { return ___arabicMapper_1; }
	inline ArabicTable_t2015225755 ** get_address_of_arabicMapper_1() { return &___arabicMapper_1; }
	inline void set_arabicMapper_1(ArabicTable_t2015225755 * value)
	{
		___arabicMapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___arabicMapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICTABLE_T2015225755_H
#ifndef ARABICMAPPING_T3619416640_H
#define ARABICMAPPING_T3619416640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicMapping
struct  ArabicMapping_t3619416640  : public RuntimeObject
{
public:
	// System.Int32 ArabicMapping::from
	int32_t ___from_0;
	// System.Int32 ArabicMapping::to
	int32_t ___to_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(ArabicMapping_t3619416640, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_to_1() { return static_cast<int32_t>(offsetof(ArabicMapping_t3619416640, ___to_1)); }
	inline int32_t get_to_1() const { return ___to_1; }
	inline int32_t* get_address_of_to_1() { return &___to_1; }
	inline void set_to_1(int32_t value)
	{
		___to_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICMAPPING_T3619416640_H
#ifndef ARABICFIXER_T565187177_H
#define ARABICFIXER_T565187177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicSupport.ArabicFixer
struct  ArabicFixer_t565187177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXER_T565187177_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ARABICFIXERTOOL_T2520528640_H
#define ARABICFIXERTOOL_T2520528640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicFixerTool
struct  ArabicFixerTool_t2520528640  : public RuntimeObject
{
public:

public:
};

struct ArabicFixerTool_t2520528640_StaticFields
{
public:
	// System.Boolean ArabicFixerTool::showTashkeel
	bool ___showTashkeel_0;
	// System.Boolean ArabicFixerTool::useHinduNumbers
	bool ___useHinduNumbers_1;

public:
	inline static int32_t get_offset_of_showTashkeel_0() { return static_cast<int32_t>(offsetof(ArabicFixerTool_t2520528640_StaticFields, ___showTashkeel_0)); }
	inline bool get_showTashkeel_0() const { return ___showTashkeel_0; }
	inline bool* get_address_of_showTashkeel_0() { return &___showTashkeel_0; }
	inline void set_showTashkeel_0(bool value)
	{
		___showTashkeel_0 = value;
	}

	inline static int32_t get_offset_of_useHinduNumbers_1() { return static_cast<int32_t>(offsetof(ArabicFixerTool_t2520528640_StaticFields, ___useHinduNumbers_1)); }
	inline bool get_useHinduNumbers_1() const { return ___useHinduNumbers_1; }
	inline bool* get_address_of_useHinduNumbers_1() { return &___useHinduNumbers_1; }
	inline void set_useHinduNumbers_1(bool value)
	{
		___useHinduNumbers_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXERTOOL_T2520528640_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T828523468_H
#define LIST_1_T828523468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TashkeelLocation>
struct  List_1_t828523468  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TashkeelLocationU5BU5D_t884365331* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t828523468, ____items_1)); }
	inline TashkeelLocationU5BU5D_t884365331* get__items_1() const { return ____items_1; }
	inline TashkeelLocationU5BU5D_t884365331** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TashkeelLocationU5BU5D_t884365331* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t828523468, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t828523468, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t828523468_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TashkeelLocationU5BU5D_t884365331* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t828523468_StaticFields, ___EmptyArray_4)); }
	inline TashkeelLocationU5BU5D_t884365331* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TashkeelLocationU5BU5D_t884365331** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TashkeelLocationU5BU5D_t884365331* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T828523468_H
#ifndef TASHKEELLOCATION_T3651416022_H
#define TASHKEELLOCATION_T3651416022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TashkeelLocation
struct  TashkeelLocation_t3651416022  : public RuntimeObject
{
public:
	// System.Char TashkeelLocation::tashkeel
	Il2CppChar ___tashkeel_0;
	// System.Int32 TashkeelLocation::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_tashkeel_0() { return static_cast<int32_t>(offsetof(TashkeelLocation_t3651416022, ___tashkeel_0)); }
	inline Il2CppChar get_tashkeel_0() const { return ___tashkeel_0; }
	inline Il2CppChar* get_address_of_tashkeel_0() { return &___tashkeel_0; }
	inline void set_tashkeel_0(Il2CppChar value)
	{
		___tashkeel_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TashkeelLocation_t3651416022, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASHKEELLOCATION_T3651416022_H
#ifndef ENUMERATOR_T2685767963_H
#define ENUMERATOR_T2685767963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<ArabicMapping>
struct  Enumerator_t2685767963 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t796524086 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ArabicMapping_t3619416640 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2685767963, ___l_0)); }
	inline List_1_t796524086 * get_l_0() const { return ___l_0; }
	inline List_1_t796524086 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t796524086 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2685767963, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2685767963, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2685767963, ___current_3)); }
	inline ArabicMapping_t3619416640 * get_current_3() const { return ___current_3; }
	inline ArabicMapping_t3619416640 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ArabicMapping_t3619416640 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2685767963_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#define __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18
struct  __StaticArrayInitTypeSizeU3D18_t270274279 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D18_t270274279__padding[18];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUMERATOR_T2717767345_H
#define ENUMERATOR_T2717767345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<TashkeelLocation>
struct  Enumerator_t2717767345 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t828523468 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	TashkeelLocation_t3651416022 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2717767345, ___l_0)); }
	inline List_1_t828523468 * get_l_0() const { return ___l_0; }
	inline List_1_t828523468 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t828523468 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2717767345, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2717767345, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2717767345, ___current_3)); }
	inline TashkeelLocation_t3651416022 * get_current_3() const { return ___current_3; }
	inline TashkeelLocation_t3651416022 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TashkeelLocation_t3651416022 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2717767345_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}
struct  U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields
{
public:
	// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18 <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}::$$method0x6000009-1
	__StaticArrayInitTypeSizeU3D18_t270274279  ___U24U24method0x6000009U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000009U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields, ___U24U24method0x6000009U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D18_t270274279  get_U24U24method0x6000009U2D1_0() const { return ___U24U24method0x6000009U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D18_t270274279 * get_address_of_U24U24method0x6000009U2D1_0() { return &___U24U24method0x6000009U2D1_0; }
	inline void set_U24U24method0x6000009U2D1_0(__StaticArrayInitTypeSizeU3D18_t270274279  value)
	{
		___U24U24method0x6000009U2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#ifndef STRINGSPLITOPTIONS_T641086070_H
#define STRINGSPLITOPTIONS_T641086070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringSplitOptions
struct  StringSplitOptions_t641086070 
{
public:
	// System.Int32 System.StringSplitOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringSplitOptions_t641086070, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSPLITOPTIONS_T641086070_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
extern "C"  void List_1__ctor_m1709633523_gshared (List_1_t811567916 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Char>::Add(!0)
extern "C"  void List_1_Add_m4264982211_gshared (List_1_t811567916 * __this, Il2CppChar p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count()
extern "C"  int32_t List_1_get_Count_m2041960438_gshared (List_1_t811567916 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32)
extern "C"  Il2CppChar List_1_get_Item_m634868510_gshared (List_1_t811567916 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
extern "C"  void List_1_Clear_m2890230691_gshared (List_1_t811567916 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<TashkeelLocation>::.ctor()
#define List_1__ctor_m1050387868(__this, method) ((  void (*) (List_1_t828523468 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t3528271667* String_ToCharArray_m1492846834 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TashkeelLocation::.ctor(System.Char,System.Int32)
extern "C"  void TashkeelLocation__ctor_m1906920022 (TashkeelLocation_t3651416022 * __this, Il2CppChar ___tashkeel0, int32_t ___position1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TashkeelLocation>::Add(!0)
#define List_1_Add_m1198340228(__this, p0, method) ((  void (*) (List_1_t828523468 *, TashkeelLocation_t3651416022 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3117905507 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1871169219  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TashkeelLocation>::get_Count()
#define List_1_get_Count_m2618350689(__this, method) ((  int32_t (*) (List_1_t828523468 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<TashkeelLocation>::GetEnumerator()
#define List_1_GetEnumerator_m1991375571(__this, method) ((  Enumerator_t2717767345  (*) (List_1_t828523468 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::get_Current()
#define Enumerator_get_Current_m1427298682(__this, method) ((  TashkeelLocation_t3651416022 * (*) (Enumerator_t2717767345 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::MoveNext()
#define Enumerator_MoveNext_m2831730375(__this, method) ((  bool (*) (Enumerator_t2717767345 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::Dispose()
#define Enumerator_Dispose_m1442584936(__this, method) ((  void (*) (Enumerator_t2717767345 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.String ArabicFixerTool::RemoveTashkeel(System.String,System.Collections.Generic.List`1<TashkeelLocation>&)
extern "C"  String_t* ArabicFixerTool_RemoveTashkeel_m2173189906 (RuntimeObject * __this /* static, unused */, String_t* ___str0, List_1_t828523468 ** ___tashkeelLocation1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// ArabicTable ArabicTable::get_ArabicMapper()
extern "C"  ArabicTable_t2015225755 * ArabicTable_get_ArabicMapper_m331737571 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 ArabicTable::Convert(System.Int32)
extern "C"  int32_t ArabicTable_Convert_m3785855912 (ArabicTable_t2015225755 * __this, int32_t ___toBeConverted0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArabicFixerTool::IsIgnoredCharacter(System.Char)
extern "C"  bool ArabicFixerTool_IsIgnoredCharacter_m803703993 (RuntimeObject * __this /* static, unused */, Il2CppChar ___ch0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArabicFixerTool::IsMiddleLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsMiddleLetter_m3068743487 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArabicFixerTool::IsFinishingLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsFinishingLetter_m1180940598 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArabicFixerTool::IsLeadingLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsLeadingLetter_m2995983843 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Int32,System.Int32)
extern "C"  String_t* Convert_ToString_m2142825503 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] ArabicFixerTool::ReturnTashkeel(System.Char[],System.Collections.Generic.List`1<TashkeelLocation>)
extern "C"  CharU5BU5D_t3528271667* ArabicFixerTool_ReturnTashkeel_m1878862093 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, List_1_t828523468 * ___tashkeelLocation1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
#define List_1__ctor_m1709633523(__this, method) ((  void (*) (List_1_t811567916 *, const RuntimeMethod*))List_1__ctor_m1709633523_gshared)(__this, method)
// System.Boolean System.Char::IsPunctuation(System.Char)
extern "C"  bool Char_IsPunctuation_m3984409211 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Char>::Add(!0)
#define List_1_Add_m4264982211(__this, p0, method) ((  void (*) (List_1_t811567916 *, Il2CppChar, const RuntimeMethod*))List_1_Add_m4264982211_gshared)(__this, p0, method)
// System.Boolean System.Char::IsLower(System.Char)
extern "C"  bool Char_IsLower_m3108076820 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Char::IsUpper(System.Char)
extern "C"  bool Char_IsUpper_m3564669513 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Char::IsNumber(System.Char)
extern "C"  bool Char_IsNumber_m2445552278 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Char::IsSymbol(System.Char)
extern "C"  bool Char_IsSymbol_m1780736836 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count()
#define List_1_get_Count_m2041960438(__this, method) ((  int32_t (*) (List_1_t811567916 *, const RuntimeMethod*))List_1_get_Count_m2041960438_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32)
#define List_1_get_Item_m634868510(__this, p0, method) ((  Il2CppChar (*) (List_1_t811567916 *, int32_t, const RuntimeMethod*))List_1_get_Item_m634868510_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
#define List_1_Clear_m2890230691(__this, method) ((  void (*) (List_1_t811567916 *, const RuntimeMethod*))List_1_Clear_m2890230691_gshared)(__this, method)
// System.String System.String::CreateString(System.Char[])
extern "C"  String_t* String_CreateString_m2818852475 (String_t* __this, CharU5BU5D_t3528271667* ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ArabicSupport.ArabicFixer::Fix(System.String,System.Boolean,System.Boolean)
extern "C"  String_t* ArabicFixer_Fix_m4044602794 (RuntimeObject * __this /* static, unused */, String_t* ___str0, bool ___showTashkeel1, bool ___useHinduNumbers2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_NewLine()
extern "C"  String_t* Environment_get_NewLine_m3211016485 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.String[],System.StringSplitOptions)
extern "C"  StringU5BU5D_t1281789340* String_Split_m4013853433 (String_t* __this, StringU5BU5D_t1281789340* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ArabicFixerTool::FixLine(System.String)
extern "C"  String_t* ArabicFixerTool_FixLine_m2584389017 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<ArabicMapping>::.ctor()
#define List_1__ctor_m2019409646(__this, method) ((  void (*) (List_1_t796524086 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void ArabicMapping::.ctor(System.Int32,System.Int32)
extern "C"  void ArabicMapping__ctor_m2823911374 (ArabicMapping_t3619416640 * __this, int32_t ___from0, int32_t ___to1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<ArabicMapping>::Add(!0)
#define List_1_Add_m1908758304(__this, p0, method) ((  void (*) (List_1_t796524086 *, ArabicMapping_t3619416640 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void ArabicTable::.ctor()
extern "C"  void ArabicTable__ctor_m3313828430 (ArabicTable_t2015225755 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<ArabicMapping>::GetEnumerator()
#define List_1_GetEnumerator_m3336618809(__this, method) ((  Enumerator_t2685767963  (*) (List_1_t796524086 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<ArabicMapping>::get_Current()
#define Enumerator_get_Current_m2544204511(__this, method) ((  ArabicMapping_t3619416640 * (*) (Enumerator_t2685767963 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ArabicMapping>::MoveNext()
#define Enumerator_MoveNext_m2035344556(__this, method) ((  bool (*) (Enumerator_t2685767963 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ArabicMapping>::Dispose()
#define Enumerator_Dispose_m3914768235(__this, method) ((  void (*) (Enumerator_t2685767963 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ArabicFixerTool::RemoveTashkeel(System.String,System.Collections.Generic.List`1<TashkeelLocation>&)
extern "C"  String_t* ArabicFixerTool_RemoveTashkeel_m2173189906 (RuntimeObject * __this /* static, unused */, String_t* ___str0, List_1_t828523468 ** ___tashkeelLocation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_RemoveTashkeel_m2173189906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	int32_t V_1 = 0;
	StringU5BU5D_t1281789340* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	bool V_5 = false;
	StringU5BU5D_t1281789340* V_6 = NULL;
	int32_t V_7 = 0;
	{
		List_1_t828523468 ** L_0 = ___tashkeelLocation1;
		List_1_t828523468 * L_1 = (List_1_t828523468 *)il2cpp_codegen_object_new(List_1_t828523468_il2cpp_TypeInfo_var);
		List_1__ctor_m1050387868(L_1, /*hidden argument*/List_1__ctor_m1050387868_RuntimeMethod_var);
		*((RuntimeObject **)(L_0)) = (RuntimeObject *)L_1;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_0), (RuntimeObject *)L_1);
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		CharU5BU5D_t3528271667* L_3 = String_ToCharArray_m1492846834(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0191;
	}

IL_0016:
	{
		CharU5BU5D_t3528271667* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_5 = (bool)((((int32_t)((((int32_t)L_7) == ((int32_t)((int32_t)1611)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_5;
		if (L_8)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t828523468 ** L_9 = ___tashkeelLocation1;
		int32_t L_10 = V_1;
		TashkeelLocation_t3651416022 * L_11 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_11, ((int32_t)1611), L_10, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_9)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_9)), L_11, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_0042:
	{
		CharU5BU5D_t3528271667* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		uint16_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = (bool)((((int32_t)((((int32_t)L_15) == ((int32_t)((int32_t)1612)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_16 = V_5;
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		List_1_t828523468 ** L_17 = ___tashkeelLocation1;
		int32_t L_18 = V_1;
		TashkeelLocation_t3651416022 * L_19 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_19, ((int32_t)1612), L_18, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_17)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_17)), L_19, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_006d:
	{
		CharU5BU5D_t3528271667* L_20 = V_0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		uint16_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_5 = (bool)((((int32_t)((((int32_t)L_23) == ((int32_t)((int32_t)1613)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_24 = V_5;
		if (L_24)
		{
			goto IL_0098;
		}
	}
	{
		List_1_t828523468 ** L_25 = ___tashkeelLocation1;
		int32_t L_26 = V_1;
		TashkeelLocation_t3651416022 * L_27 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_27, ((int32_t)1613), L_26, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_25)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_25)), L_27, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_0098:
	{
		CharU5BU5D_t3528271667* L_28 = V_0;
		int32_t L_29 = V_1;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		uint16_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_5 = (bool)((((int32_t)((((int32_t)L_31) == ((int32_t)((int32_t)1614)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_32 = V_5;
		if (L_32)
		{
			goto IL_00c3;
		}
	}
	{
		List_1_t828523468 ** L_33 = ___tashkeelLocation1;
		int32_t L_34 = V_1;
		TashkeelLocation_t3651416022 * L_35 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_35, ((int32_t)1614), L_34, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_33)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_33)), L_35, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_00c3:
	{
		CharU5BU5D_t3528271667* L_36 = V_0;
		int32_t L_37 = V_1;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		uint16_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		V_5 = (bool)((((int32_t)((((int32_t)L_39) == ((int32_t)((int32_t)1615)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_40 = V_5;
		if (L_40)
		{
			goto IL_00ee;
		}
	}
	{
		List_1_t828523468 ** L_41 = ___tashkeelLocation1;
		int32_t L_42 = V_1;
		TashkeelLocation_t3651416022 * L_43 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_43, ((int32_t)1615), L_42, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_41)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_41)), L_43, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_00ee:
	{
		CharU5BU5D_t3528271667* L_44 = V_0;
		int32_t L_45 = V_1;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		uint16_t L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		V_5 = (bool)((((int32_t)((((int32_t)L_47) == ((int32_t)((int32_t)1616)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_48 = V_5;
		if (L_48)
		{
			goto IL_0116;
		}
	}
	{
		List_1_t828523468 ** L_49 = ___tashkeelLocation1;
		int32_t L_50 = V_1;
		TashkeelLocation_t3651416022 * L_51 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_51, ((int32_t)1616), L_50, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_49)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_49)), L_51, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_0116:
	{
		CharU5BU5D_t3528271667* L_52 = V_0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		uint16_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		V_5 = (bool)((((int32_t)((((int32_t)L_55) == ((int32_t)((int32_t)1617)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_56 = V_5;
		if (L_56)
		{
			goto IL_013e;
		}
	}
	{
		List_1_t828523468 ** L_57 = ___tashkeelLocation1;
		int32_t L_58 = V_1;
		TashkeelLocation_t3651416022 * L_59 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_59, ((int32_t)1617), L_58, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_57)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_57)), L_59, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_013e:
	{
		CharU5BU5D_t3528271667* L_60 = V_0;
		int32_t L_61 = V_1;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		uint16_t L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		V_5 = (bool)((((int32_t)((((int32_t)L_63) == ((int32_t)((int32_t)1618)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_64 = V_5;
		if (L_64)
		{
			goto IL_0166;
		}
	}
	{
		List_1_t828523468 ** L_65 = ___tashkeelLocation1;
		int32_t L_66 = V_1;
		TashkeelLocation_t3651416022 * L_67 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_67, ((int32_t)1618), L_66, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_65)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_65)), L_67, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
		goto IL_018c;
	}

IL_0166:
	{
		CharU5BU5D_t3528271667* L_68 = V_0;
		int32_t L_69 = V_1;
		NullCheck(L_68);
		int32_t L_70 = L_69;
		uint16_t L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		V_5 = (bool)((((int32_t)((((int32_t)L_71) == ((int32_t)((int32_t)1619)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_72 = V_5;
		if (L_72)
		{
			goto IL_018c;
		}
	}
	{
		List_1_t828523468 ** L_73 = ___tashkeelLocation1;
		int32_t L_74 = V_1;
		TashkeelLocation_t3651416022 * L_75 = (TashkeelLocation_t3651416022 *)il2cpp_codegen_object_new(TashkeelLocation_t3651416022_il2cpp_TypeInfo_var);
		TashkeelLocation__ctor_m1906920022(L_75, ((int32_t)1619), L_74, /*hidden argument*/NULL);
		NullCheck((*((List_1_t828523468 **)L_73)));
		List_1_Add_m1198340228((*((List_1_t828523468 **)L_73)), L_75, /*hidden argument*/List_1_Add_m1198340228_RuntimeMethod_var);
	}

IL_018c:
	{
		int32_t L_76 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_76, (int32_t)1));
	}

IL_0191:
	{
		int32_t L_77 = V_1;
		CharU5BU5D_t3528271667* L_78 = V_0;
		NullCheck(L_78);
		V_5 = (bool)((((int32_t)L_77) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_78)->max_length))))))? 1 : 0);
		bool L_79 = V_5;
		if (L_79)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_80 = ___str0;
		CharU5BU5D_t3528271667* L_81 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		RuntimeFieldHandle_t1871169219  L_82 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573____U24U24method0x6000009U2D1_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_81, L_82, /*hidden argument*/NULL);
		NullCheck(L_80);
		StringU5BU5D_t1281789340* L_83 = String_Split_m3646115398(L_80, L_81, /*hidden argument*/NULL);
		V_2 = L_83;
		___str0 = _stringLiteral757602046;
		StringU5BU5D_t1281789340* L_84 = V_2;
		V_6 = L_84;
		V_7 = 0;
		goto IL_01e0;
	}

IL_01c9:
	{
		StringU5BU5D_t1281789340* L_85 = V_6;
		int32_t L_86 = V_7;
		NullCheck(L_85);
		int32_t L_87 = L_86;
		String_t* L_88 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_87));
		V_3 = L_88;
		String_t* L_89 = ___str0;
		String_t* L_90 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = String_Concat_m3937257545(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		___str0 = L_91;
		int32_t L_92 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_92, (int32_t)1));
	}

IL_01e0:
	{
		int32_t L_93 = V_7;
		StringU5BU5D_t1281789340* L_94 = V_6;
		NullCheck(L_94);
		V_5 = (bool)((((int32_t)L_93) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_94)->max_length))))))? 1 : 0);
		bool L_95 = V_5;
		if (L_95)
		{
			goto IL_01c9;
		}
	}
	{
		String_t* L_96 = ___str0;
		V_4 = L_96;
		goto IL_01f3;
	}

IL_01f3:
	{
		String_t* L_97 = V_4;
		return L_97;
	}
}
// System.Char[] ArabicFixerTool::ReturnTashkeel(System.Char[],System.Collections.Generic.List`1<TashkeelLocation>)
extern "C"  CharU5BU5D_t3528271667* ArabicFixerTool_ReturnTashkeel_m1878862093 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, List_1_t828523468 * ___tashkeelLocation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_ReturnTashkeel_m1878862093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	TashkeelLocation_t3651416022 * V_3 = NULL;
	CharU5BU5D_t3528271667* V_4 = NULL;
	Enumerator_t2717767345  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CharU5BU5D_t3528271667* L_0 = ___letters0;
		NullCheck(L_0);
		List_1_t828523468 * L_1 = ___tashkeelLocation1;
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2618350689(L_1, /*hidden argument*/List_1_get_Count_m2618350689_RuntimeMethod_var);
		V_0 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), (int32_t)L_2))));
		V_1 = 0;
		V_2 = 0;
		goto IL_007c;
	}

IL_0017:
	{
		CharU5BU5D_t3528271667* L_3 = V_0;
		int32_t L_4 = V_1;
		CharU5BU5D_t3528271667* L_5 = ___letters0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Il2CppChar)L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		List_1_t828523468 * L_10 = ___tashkeelLocation1;
		NullCheck(L_10);
		Enumerator_t2717767345  L_11 = List_1_GetEnumerator_m1991375571(L_10, /*hidden argument*/List_1_GetEnumerator_m1991375571_RuntimeMethod_var);
		V_5 = L_11;
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0058;
		}

IL_002d:
		{
			TashkeelLocation_t3651416022 * L_12 = Enumerator_get_Current_m1427298682((&V_5), /*hidden argument*/Enumerator_get_Current_m1427298682_RuntimeMethod_var);
			V_3 = L_12;
			TashkeelLocation_t3651416022 * L_13 = V_3;
			NullCheck(L_13);
			int32_t L_14 = L_13->get_position_1();
			int32_t L_15 = V_1;
			V_6 = (bool)((((int32_t)((((int32_t)L_14) == ((int32_t)L_15))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_16 = V_6;
			if (L_16)
			{
				goto IL_0057;
			}
		}

IL_0048:
		{
			CharU5BU5D_t3528271667* L_17 = V_0;
			int32_t L_18 = V_1;
			TashkeelLocation_t3651416022 * L_19 = V_3;
			NullCheck(L_19);
			Il2CppChar L_20 = L_19->get_tashkeel_0();
			NullCheck(L_17);
			(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppChar)L_20);
			int32_t L_21 = V_1;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
		}

IL_0057:
		{
		}

IL_0058:
		{
			bool L_22 = Enumerator_MoveNext_m2831730375((&V_5), /*hidden argument*/Enumerator_MoveNext_m2831730375_RuntimeMethod_var);
			V_6 = L_22;
			bool L_23 = V_6;
			if (L_23)
			{
				goto IL_002d;
			}
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x76, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1442584936((&V_5), /*hidden argument*/Enumerator_Dispose_m1442584936_RuntimeMethod_var);
		IL2CPP_END_FINALLY(103)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0076:
	{
		int32_t L_24 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_007c:
	{
		int32_t L_25 = V_2;
		CharU5BU5D_t3528271667* L_26 = ___letters0;
		NullCheck(L_26);
		V_6 = (bool)((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length))))))? 1 : 0);
		bool L_27 = V_6;
		if (L_27)
		{
			goto IL_0017;
		}
	}
	{
		CharU5BU5D_t3528271667* L_28 = V_0;
		V_4 = L_28;
		goto IL_008d;
	}

IL_008d:
	{
		CharU5BU5D_t3528271667* L_29 = V_4;
		return L_29;
	}
}
// System.String ArabicFixerTool::FixLine(System.String)
extern "C"  String_t* ArabicFixerTool_FixLine_m2584389017 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_FixLine_m2584389017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t828523468 * V_1 = NULL;
	String_t* V_2 = NULL;
	CharU5BU5D_t3528271667* V_3 = NULL;
	CharU5BU5D_t3528271667* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	List_1_t811567916 * V_8 = NULL;
	List_1_t811567916 * V_9 = NULL;
	int32_t V_10 = 0;
	String_t* V_11 = NULL;
	bool V_12 = false;
	int32_t G_B24_0 = 0;
	int32_t G_B67_0 = 0;
	int32_t G_B69_0 = 0;
	int32_t G_B91_0 = 0;
	int32_t G_B93_0 = 0;
	int32_t G_B101_0 = 0;
	int32_t G_B117_0 = 0;
	int32_t G_B119_0 = 0;
	{
		V_0 = _stringLiteral757602046;
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_1 = ArabicFixerTool_RemoveTashkeel_m2173189906(NULL /*static, unused*/, L_0, (&V_1), /*hidden argument*/NULL);
		V_2 = L_1;
		String_t* L_2 = V_2;
		NullCheck(L_2);
		CharU5BU5D_t3528271667* L_3 = String_ToCharArray_m1492846834(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		String_t* L_4 = V_2;
		NullCheck(L_4);
		CharU5BU5D_t3528271667* L_5 = String_ToCharArray_m1492846834(L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		V_5 = 0;
		V_6 = 0;
		goto IL_0042;
	}

IL_0027:
	{
		CharU5BU5D_t3528271667* L_6 = V_3;
		int32_t L_7 = V_6;
		ArabicTable_t2015225755 * L_8 = ArabicTable_get_ArabicMapper_m331737571(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_9 = V_3;
		int32_t L_10 = V_6;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint16_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_8);
		int32_t L_13 = ArabicTable_Convert_m3785855912(L_8, L_12, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Il2CppChar)(((int32_t)((uint16_t)L_13))));
		int32_t L_14 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0042:
	{
		int32_t L_15 = V_6;
		CharU5BU5D_t3528271667* L_16 = V_3;
		NullCheck(L_16);
		V_12 = (bool)((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length))))))? 1 : 0);
		bool L_17 = V_12;
		if (L_17)
		{
			goto IL_0027;
		}
	}
	{
		V_6 = 0;
		goto IL_036f;
	}

IL_0057:
	{
		V_7 = (bool)0;
		int32_t L_18 = V_6;
		V_12 = (bool)((((int32_t)((((int32_t)L_18) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_19 = V_12;
		if (L_19)
		{
			goto IL_006c;
		}
	}
	{
		V_5 = 0;
	}

IL_006c:
	{
		CharU5BU5D_t3528271667* L_20 = V_3;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		uint16_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_12 = (bool)((((int32_t)((((int32_t)L_23) == ((int32_t)((int32_t)57121)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_24 = V_12;
		if (L_24)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_25 = V_5;
		V_5 = L_25;
	}

IL_0084:
	{
		CharU5BU5D_t3528271667* L_26 = V_3;
		int32_t L_27 = V_6;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		uint16_t L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_12 = (bool)((((int32_t)((((int32_t)L_29) == ((int32_t)((int32_t)65245)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_30 = V_12;
		if (L_30)
		{
			goto IL_017d;
		}
	}
	{
		int32_t L_31 = V_6;
		CharU5BU5D_t3528271667* L_32 = V_3;
		NullCheck(L_32);
		V_12 = (bool)((((int32_t)((((int32_t)L_31) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))), (int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_33 = V_12;
		if (L_33)
		{
			goto IL_017c;
		}
	}
	{
		CharU5BU5D_t3528271667* L_34 = V_3;
		int32_t L_35 = V_6;
		NullCheck(L_34);
		int32_t L_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
		uint16_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		V_12 = (bool)((((int32_t)((((int32_t)L_37) == ((int32_t)((int32_t)65159)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_38 = V_12;
		if (L_38)
		{
			goto IL_00e7;
		}
	}
	{
		CharU5BU5D_t3528271667* L_39 = V_3;
		int32_t L_40 = V_6;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (Il2CppChar)((int32_t)65271));
		CharU5BU5D_t3528271667* L_41 = V_4;
		int32_t L_42 = V_6;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1))), (Il2CppChar)((int32_t)65535));
		V_7 = (bool)1;
		goto IL_017b;
	}

IL_00e7:
	{
		CharU5BU5D_t3528271667* L_43 = V_3;
		int32_t L_44 = V_6;
		NullCheck(L_43);
		int32_t L_45 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
		uint16_t L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		V_12 = (bool)((((int32_t)((((int32_t)L_46) == ((int32_t)((int32_t)65165)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_47 = V_12;
		if (L_47)
		{
			goto IL_0119;
		}
	}
	{
		CharU5BU5D_t3528271667* L_48 = V_3;
		int32_t L_49 = V_6;
		NullCheck(L_48);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(L_49), (Il2CppChar)((int32_t)65273));
		CharU5BU5D_t3528271667* L_50 = V_4;
		int32_t L_51 = V_6;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1))), (Il2CppChar)((int32_t)65535));
		V_7 = (bool)1;
		goto IL_017b;
	}

IL_0119:
	{
		CharU5BU5D_t3528271667* L_52 = V_3;
		int32_t L_53 = V_6;
		NullCheck(L_52);
		int32_t L_54 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1));
		uint16_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		V_12 = (bool)((((int32_t)((((int32_t)L_55) == ((int32_t)((int32_t)65155)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_56 = V_12;
		if (L_56)
		{
			goto IL_014b;
		}
	}
	{
		CharU5BU5D_t3528271667* L_57 = V_3;
		int32_t L_58 = V_6;
		NullCheck(L_57);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (Il2CppChar)((int32_t)65269));
		CharU5BU5D_t3528271667* L_59 = V_4;
		int32_t L_60 = V_6;
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_60, (int32_t)1))), (Il2CppChar)((int32_t)65535));
		V_7 = (bool)1;
		goto IL_017b;
	}

IL_014b:
	{
		CharU5BU5D_t3528271667* L_61 = V_3;
		int32_t L_62 = V_6;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)il2cpp_codegen_add((int32_t)L_62, (int32_t)1));
		uint16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		V_12 = (bool)((((int32_t)((((int32_t)L_64) == ((int32_t)((int32_t)65153)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_65 = V_12;
		if (L_65)
		{
			goto IL_017b;
		}
	}
	{
		CharU5BU5D_t3528271667* L_66 = V_3;
		int32_t L_67 = V_6;
		NullCheck(L_66);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (Il2CppChar)((int32_t)65267));
		CharU5BU5D_t3528271667* L_68 = V_4;
		int32_t L_69 = V_6;
		NullCheck(L_68);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_69, (int32_t)1))), (Il2CppChar)((int32_t)65535));
		V_7 = (bool)1;
	}

IL_017b:
	{
	}

IL_017c:
	{
	}

IL_017d:
	{
		CharU5BU5D_t3528271667* L_70 = V_3;
		int32_t L_71 = V_6;
		NullCheck(L_70);
		int32_t L_72 = L_71;
		uint16_t L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_74 = ArabicFixerTool_IsIgnoredCharacter_m803703993(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (L_74)
		{
			goto IL_019a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_75 = V_3;
		int32_t L_76 = V_6;
		NullCheck(L_75);
		int32_t L_77 = L_76;
		uint16_t L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		if ((((int32_t)L_78) == ((int32_t)((int32_t)65))))
		{
			goto IL_019a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_79 = V_3;
		int32_t L_80 = V_6;
		NullCheck(L_79);
		int32_t L_81 = L_80;
		uint16_t L_82 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
		G_B24_0 = ((((int32_t)L_82) == ((int32_t)((int32_t)65)))? 1 : 0);
		goto IL_019b;
	}

IL_019a:
	{
		G_B24_0 = 1;
	}

IL_019b:
	{
		V_12 = (bool)G_B24_0;
		bool L_83 = V_12;
		if (L_83)
		{
			goto IL_01fe;
		}
	}
	{
		CharU5BU5D_t3528271667* L_84 = V_3;
		int32_t L_85 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_86 = ArabicFixerTool_IsMiddleLetter_m3068743487(NULL /*static, unused*/, L_84, L_85, /*hidden argument*/NULL);
		V_12 = (bool)((((int32_t)L_86) == ((int32_t)0))? 1 : 0);
		bool L_87 = V_12;
		if (L_87)
		{
			goto IL_01c1;
		}
	}
	{
		CharU5BU5D_t3528271667* L_88 = V_4;
		int32_t L_89 = V_6;
		CharU5BU5D_t3528271667* L_90 = V_3;
		int32_t L_91 = V_6;
		NullCheck(L_90);
		int32_t L_92 = L_91;
		uint16_t L_93 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		NullCheck(L_88);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_93, (int32_t)3))))));
		goto IL_01fd;
	}

IL_01c1:
	{
		CharU5BU5D_t3528271667* L_94 = V_3;
		int32_t L_95 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_96 = ArabicFixerTool_IsFinishingLetter_m1180940598(NULL /*static, unused*/, L_94, L_95, /*hidden argument*/NULL);
		V_12 = (bool)((((int32_t)L_96) == ((int32_t)0))? 1 : 0);
		bool L_97 = V_12;
		if (L_97)
		{
			goto IL_01e0;
		}
	}
	{
		CharU5BU5D_t3528271667* L_98 = V_4;
		int32_t L_99 = V_6;
		CharU5BU5D_t3528271667* L_100 = V_3;
		int32_t L_101 = V_6;
		NullCheck(L_100);
		int32_t L_102 = L_101;
		uint16_t L_103 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_102));
		NullCheck(L_98);
		(L_98)->SetAt(static_cast<il2cpp_array_size_t>(L_99), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_103, (int32_t)1))))));
		goto IL_01fd;
	}

IL_01e0:
	{
		CharU5BU5D_t3528271667* L_104 = V_3;
		int32_t L_105 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_106 = ArabicFixerTool_IsLeadingLetter_m2995983843(NULL /*static, unused*/, L_104, L_105, /*hidden argument*/NULL);
		V_12 = (bool)((((int32_t)L_106) == ((int32_t)0))? 1 : 0);
		bool L_107 = V_12;
		if (L_107)
		{
			goto IL_01fd;
		}
	}
	{
		CharU5BU5D_t3528271667* L_108 = V_4;
		int32_t L_109 = V_6;
		CharU5BU5D_t3528271667* L_110 = V_3;
		int32_t L_111 = V_6;
		NullCheck(L_110);
		int32_t L_112 = L_111;
		uint16_t L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		NullCheck(L_108);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(L_109), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_113, (int32_t)2))))));
	}

IL_01fd:
	{
	}

IL_01fe:
	{
		String_t* L_114 = V_0;
		CharU5BU5D_t3528271667* L_115 = V_3;
		int32_t L_116 = V_6;
		NullCheck(L_115);
		int32_t L_117 = L_116;
		uint16_t L_118 = (L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_119 = Convert_ToString_m2142825503(NULL /*static, unused*/, L_118, ((int32_t)16), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_120 = String_Concat_m3755062657(NULL /*static, unused*/, L_114, L_119, _stringLiteral3452614528, /*hidden argument*/NULL);
		V_0 = L_120;
		bool L_121 = V_7;
		V_12 = (bool)((((int32_t)L_121) == ((int32_t)0))? 1 : 0);
		bool L_122 = V_12;
		if (L_122)
		{
			goto IL_0226;
		}
	}
	{
		int32_t L_123 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_123, (int32_t)1));
	}

IL_0226:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_124 = ((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->get_useHinduNumbers_1();
		V_12 = (bool)((((int32_t)L_124) == ((int32_t)0))? 1 : 0);
		bool L_125 = V_12;
		if (L_125)
		{
			goto IL_0368;
		}
	}
	{
		CharU5BU5D_t3528271667* L_126 = V_3;
		int32_t L_127 = V_6;
		NullCheck(L_126);
		int32_t L_128 = L_127;
		uint16_t L_129 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
		V_12 = (bool)((((int32_t)((((int32_t)L_129) == ((int32_t)((int32_t)48)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_130 = V_12;
		if (L_130)
		{
			goto IL_0258;
		}
	}
	{
		CharU5BU5D_t3528271667* L_131 = V_4;
		int32_t L_132 = V_6;
		NullCheck(L_131);
		(L_131)->SetAt(static_cast<il2cpp_array_size_t>(L_132), (Il2CppChar)((int32_t)1632));
		goto IL_0367;
	}

IL_0258:
	{
		CharU5BU5D_t3528271667* L_133 = V_3;
		int32_t L_134 = V_6;
		NullCheck(L_133);
		int32_t L_135 = L_134;
		uint16_t L_136 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_135));
		V_12 = (bool)((((int32_t)((((int32_t)L_136) == ((int32_t)((int32_t)49)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_137 = V_12;
		if (L_137)
		{
			goto IL_0278;
		}
	}
	{
		CharU5BU5D_t3528271667* L_138 = V_4;
		int32_t L_139 = V_6;
		NullCheck(L_138);
		(L_138)->SetAt(static_cast<il2cpp_array_size_t>(L_139), (Il2CppChar)((int32_t)1633));
		goto IL_0367;
	}

IL_0278:
	{
		CharU5BU5D_t3528271667* L_140 = V_3;
		int32_t L_141 = V_6;
		NullCheck(L_140);
		int32_t L_142 = L_141;
		uint16_t L_143 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_142));
		V_12 = (bool)((((int32_t)((((int32_t)L_143) == ((int32_t)((int32_t)50)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_144 = V_12;
		if (L_144)
		{
			goto IL_0298;
		}
	}
	{
		CharU5BU5D_t3528271667* L_145 = V_4;
		int32_t L_146 = V_6;
		NullCheck(L_145);
		(L_145)->SetAt(static_cast<il2cpp_array_size_t>(L_146), (Il2CppChar)((int32_t)1634));
		goto IL_0367;
	}

IL_0298:
	{
		CharU5BU5D_t3528271667* L_147 = V_3;
		int32_t L_148 = V_6;
		NullCheck(L_147);
		int32_t L_149 = L_148;
		uint16_t L_150 = (L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149));
		V_12 = (bool)((((int32_t)((((int32_t)L_150) == ((int32_t)((int32_t)51)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_151 = V_12;
		if (L_151)
		{
			goto IL_02b8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_152 = V_4;
		int32_t L_153 = V_6;
		NullCheck(L_152);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(L_153), (Il2CppChar)((int32_t)1635));
		goto IL_0367;
	}

IL_02b8:
	{
		CharU5BU5D_t3528271667* L_154 = V_3;
		int32_t L_155 = V_6;
		NullCheck(L_154);
		int32_t L_156 = L_155;
		uint16_t L_157 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		V_12 = (bool)((((int32_t)((((int32_t)L_157) == ((int32_t)((int32_t)52)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_158 = V_12;
		if (L_158)
		{
			goto IL_02d8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_159 = V_4;
		int32_t L_160 = V_6;
		NullCheck(L_159);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(L_160), (Il2CppChar)((int32_t)1636));
		goto IL_0367;
	}

IL_02d8:
	{
		CharU5BU5D_t3528271667* L_161 = V_3;
		int32_t L_162 = V_6;
		NullCheck(L_161);
		int32_t L_163 = L_162;
		uint16_t L_164 = (L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		V_12 = (bool)((((int32_t)((((int32_t)L_164) == ((int32_t)((int32_t)53)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_165 = V_12;
		if (L_165)
		{
			goto IL_02f5;
		}
	}
	{
		CharU5BU5D_t3528271667* L_166 = V_4;
		int32_t L_167 = V_6;
		NullCheck(L_166);
		(L_166)->SetAt(static_cast<il2cpp_array_size_t>(L_167), (Il2CppChar)((int32_t)1637));
		goto IL_0367;
	}

IL_02f5:
	{
		CharU5BU5D_t3528271667* L_168 = V_3;
		int32_t L_169 = V_6;
		NullCheck(L_168);
		int32_t L_170 = L_169;
		uint16_t L_171 = (L_168)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		V_12 = (bool)((((int32_t)((((int32_t)L_171) == ((int32_t)((int32_t)54)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_172 = V_12;
		if (L_172)
		{
			goto IL_0312;
		}
	}
	{
		CharU5BU5D_t3528271667* L_173 = V_4;
		int32_t L_174 = V_6;
		NullCheck(L_173);
		(L_173)->SetAt(static_cast<il2cpp_array_size_t>(L_174), (Il2CppChar)((int32_t)1638));
		goto IL_0367;
	}

IL_0312:
	{
		CharU5BU5D_t3528271667* L_175 = V_3;
		int32_t L_176 = V_6;
		NullCheck(L_175);
		int32_t L_177 = L_176;
		uint16_t L_178 = (L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_177));
		V_12 = (bool)((((int32_t)((((int32_t)L_178) == ((int32_t)((int32_t)55)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_179 = V_12;
		if (L_179)
		{
			goto IL_032f;
		}
	}
	{
		CharU5BU5D_t3528271667* L_180 = V_4;
		int32_t L_181 = V_6;
		NullCheck(L_180);
		(L_180)->SetAt(static_cast<il2cpp_array_size_t>(L_181), (Il2CppChar)((int32_t)1639));
		goto IL_0367;
	}

IL_032f:
	{
		CharU5BU5D_t3528271667* L_182 = V_3;
		int32_t L_183 = V_6;
		NullCheck(L_182);
		int32_t L_184 = L_183;
		uint16_t L_185 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184));
		V_12 = (bool)((((int32_t)((((int32_t)L_185) == ((int32_t)((int32_t)56)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_186 = V_12;
		if (L_186)
		{
			goto IL_034c;
		}
	}
	{
		CharU5BU5D_t3528271667* L_187 = V_4;
		int32_t L_188 = V_6;
		NullCheck(L_187);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(L_188), (Il2CppChar)((int32_t)1640));
		goto IL_0367;
	}

IL_034c:
	{
		CharU5BU5D_t3528271667* L_189 = V_3;
		int32_t L_190 = V_6;
		NullCheck(L_189);
		int32_t L_191 = L_190;
		uint16_t L_192 = (L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		V_12 = (bool)((((int32_t)((((int32_t)L_192) == ((int32_t)((int32_t)57)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_193 = V_12;
		if (L_193)
		{
			goto IL_0367;
		}
	}
	{
		CharU5BU5D_t3528271667* L_194 = V_4;
		int32_t L_195 = V_6;
		NullCheck(L_194);
		(L_194)->SetAt(static_cast<il2cpp_array_size_t>(L_195), (Il2CppChar)((int32_t)1641));
	}

IL_0367:
	{
	}

IL_0368:
	{
		int32_t L_196 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_196, (int32_t)1));
	}

IL_036f:
	{
		int32_t L_197 = V_6;
		CharU5BU5D_t3528271667* L_198 = V_3;
		NullCheck(L_198);
		V_12 = (bool)((((int32_t)L_197) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_198)->max_length))))))? 1 : 0);
		bool L_199 = V_12;
		if (L_199)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		bool L_200 = ((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->get_showTashkeel_0();
		V_12 = (bool)((((int32_t)L_200) == ((int32_t)0))? 1 : 0);
		bool L_201 = V_12;
		if (L_201)
		{
			goto IL_0397;
		}
	}
	{
		CharU5BU5D_t3528271667* L_202 = V_4;
		List_1_t828523468 * L_203 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_204 = ArabicFixerTool_ReturnTashkeel_m1878862093(NULL /*static, unused*/, L_202, L_203, /*hidden argument*/NULL);
		V_4 = L_204;
	}

IL_0397:
	{
		List_1_t811567916 * L_205 = (List_1_t811567916 *)il2cpp_codegen_object_new(List_1_t811567916_il2cpp_TypeInfo_var);
		List_1__ctor_m1709633523(L_205, /*hidden argument*/List_1__ctor_m1709633523_RuntimeMethod_var);
		V_8 = L_205;
		List_1_t811567916 * L_206 = (List_1_t811567916 *)il2cpp_codegen_object_new(List_1_t811567916_il2cpp_TypeInfo_var);
		List_1__ctor_m1709633523(L_206, /*hidden argument*/List_1__ctor_m1709633523_RuntimeMethod_var);
		V_9 = L_206;
		CharU5BU5D_t3528271667* L_207 = V_4;
		NullCheck(L_207);
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_207)->max_length)))), (int32_t)1));
		goto IL_06c3;
	}

IL_03b2:
	{
		CharU5BU5D_t3528271667* L_208 = V_4;
		int32_t L_209 = V_6;
		NullCheck(L_208);
		int32_t L_210 = L_209;
		uint16_t L_211 = (L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_210));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_212 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_211, /*hidden argument*/NULL);
		if (!L_212)
		{
			goto IL_03f0;
		}
	}
	{
		int32_t L_213 = V_6;
		if ((((int32_t)L_213) <= ((int32_t)0)))
		{
			goto IL_03f0;
		}
	}
	{
		int32_t L_214 = V_6;
		CharU5BU5D_t3528271667* L_215 = V_4;
		NullCheck(L_215);
		if ((((int32_t)L_214) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_215)->max_length)))), (int32_t)1)))))
		{
			goto IL_03f0;
		}
	}
	{
		CharU5BU5D_t3528271667* L_216 = V_4;
		int32_t L_217 = V_6;
		NullCheck(L_216);
		int32_t L_218 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_217, (int32_t)1));
		uint16_t L_219 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_220 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_219, /*hidden argument*/NULL);
		if (L_220)
		{
			goto IL_03ed;
		}
	}
	{
		CharU5BU5D_t3528271667* L_221 = V_4;
		int32_t L_222 = V_6;
		NullCheck(L_221);
		int32_t L_223 = ((int32_t)il2cpp_codegen_add((int32_t)L_222, (int32_t)1));
		uint16_t L_224 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_225 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_224, /*hidden argument*/NULL);
		G_B67_0 = ((((int32_t)L_225) == ((int32_t)0))? 1 : 0);
		goto IL_03ee;
	}

IL_03ed:
	{
		G_B67_0 = 0;
	}

IL_03ee:
	{
		G_B69_0 = G_B67_0;
		goto IL_03f1;
	}

IL_03f0:
	{
		G_B69_0 = 1;
	}

IL_03f1:
	{
		V_12 = (bool)G_B69_0;
		bool L_226 = V_12;
		if (L_226)
		{
			goto IL_0498;
		}
	}
	{
		CharU5BU5D_t3528271667* L_227 = V_4;
		int32_t L_228 = V_6;
		NullCheck(L_227);
		int32_t L_229 = L_228;
		uint16_t L_230 = (L_227)->GetAt(static_cast<il2cpp_array_size_t>(L_229));
		V_12 = (bool)((((int32_t)((((int32_t)L_230) == ((int32_t)((int32_t)40)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_231 = V_12;
		if (L_231)
		{
			goto IL_0419;
		}
	}
	{
		List_1_t811567916 * L_232 = V_8;
		NullCheck(L_232);
		List_1_Add_m4264982211(L_232, ((int32_t)41), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_0492;
	}

IL_0419:
	{
		CharU5BU5D_t3528271667* L_233 = V_4;
		int32_t L_234 = V_6;
		NullCheck(L_233);
		int32_t L_235 = L_234;
		uint16_t L_236 = (L_233)->GetAt(static_cast<il2cpp_array_size_t>(L_235));
		V_12 = (bool)((((int32_t)((((int32_t)L_236) == ((int32_t)((int32_t)41)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_237 = V_12;
		if (L_237)
		{
			goto IL_0437;
		}
	}
	{
		List_1_t811567916 * L_238 = V_8;
		NullCheck(L_238);
		List_1_Add_m4264982211(L_238, ((int32_t)40), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_0492;
	}

IL_0437:
	{
		CharU5BU5D_t3528271667* L_239 = V_4;
		int32_t L_240 = V_6;
		NullCheck(L_239);
		int32_t L_241 = L_240;
		uint16_t L_242 = (L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		V_12 = (bool)((((int32_t)((((int32_t)L_242) == ((int32_t)((int32_t)60)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_243 = V_12;
		if (L_243)
		{
			goto IL_0455;
		}
	}
	{
		List_1_t811567916 * L_244 = V_8;
		NullCheck(L_244);
		List_1_Add_m4264982211(L_244, ((int32_t)62), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_0492;
	}

IL_0455:
	{
		CharU5BU5D_t3528271667* L_245 = V_4;
		int32_t L_246 = V_6;
		NullCheck(L_245);
		int32_t L_247 = L_246;
		uint16_t L_248 = (L_245)->GetAt(static_cast<il2cpp_array_size_t>(L_247));
		V_12 = (bool)((((int32_t)((((int32_t)L_248) == ((int32_t)((int32_t)62)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_249 = V_12;
		if (L_249)
		{
			goto IL_0473;
		}
	}
	{
		List_1_t811567916 * L_250 = V_8;
		NullCheck(L_250);
		List_1_Add_m4264982211(L_250, ((int32_t)60), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_0492;
	}

IL_0473:
	{
		CharU5BU5D_t3528271667* L_251 = V_4;
		int32_t L_252 = V_6;
		NullCheck(L_251);
		int32_t L_253 = L_252;
		uint16_t L_254 = (L_251)->GetAt(static_cast<il2cpp_array_size_t>(L_253));
		V_12 = (bool)((((int32_t)L_254) == ((int32_t)((int32_t)65535)))? 1 : 0);
		bool L_255 = V_12;
		if (L_255)
		{
			goto IL_0492;
		}
	}
	{
		List_1_t811567916 * L_256 = V_8;
		CharU5BU5D_t3528271667* L_257 = V_4;
		int32_t L_258 = V_6;
		NullCheck(L_257);
		int32_t L_259 = L_258;
		uint16_t L_260 = (L_257)->GetAt(static_cast<il2cpp_array_size_t>(L_259));
		NullCheck(L_256);
		List_1_Add_m4264982211(L_256, L_260, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
	}

IL_0492:
	{
		goto IL_06bc;
	}

IL_0498:
	{
		CharU5BU5D_t3528271667* L_261 = V_4;
		int32_t L_262 = V_6;
		NullCheck(L_261);
		int32_t L_263 = L_262;
		uint16_t L_264 = (L_261)->GetAt(static_cast<il2cpp_array_size_t>(L_263));
		if ((!(((uint32_t)L_264) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_050a;
		}
	}
	{
		int32_t L_265 = V_6;
		if ((((int32_t)L_265) <= ((int32_t)0)))
		{
			goto IL_050a;
		}
	}
	{
		int32_t L_266 = V_6;
		CharU5BU5D_t3528271667* L_267 = V_4;
		NullCheck(L_267);
		if ((((int32_t)L_266) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_267)->max_length)))), (int32_t)1)))))
		{
			goto IL_050a;
		}
	}
	{
		CharU5BU5D_t3528271667* L_268 = V_4;
		int32_t L_269 = V_6;
		NullCheck(L_268);
		int32_t L_270 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_269, (int32_t)1));
		uint16_t L_271 = (L_268)->GetAt(static_cast<il2cpp_array_size_t>(L_270));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_272 = Char_IsLower_m3108076820(NULL /*static, unused*/, L_271, /*hidden argument*/NULL);
		if (L_272)
		{
			goto IL_04da;
		}
	}
	{
		CharU5BU5D_t3528271667* L_273 = V_4;
		int32_t L_274 = V_6;
		NullCheck(L_273);
		int32_t L_275 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_274, (int32_t)1));
		uint16_t L_276 = (L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_277 = Char_IsUpper_m3564669513(NULL /*static, unused*/, L_276, /*hidden argument*/NULL);
		if (L_277)
		{
			goto IL_04da;
		}
	}
	{
		CharU5BU5D_t3528271667* L_278 = V_4;
		int32_t L_279 = V_6;
		NullCheck(L_278);
		int32_t L_280 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_279, (int32_t)1));
		uint16_t L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_282 = Char_IsNumber_m2445552278(NULL /*static, unused*/, L_281, /*hidden argument*/NULL);
		if (!L_282)
		{
			goto IL_050a;
		}
	}

IL_04da:
	{
		CharU5BU5D_t3528271667* L_283 = V_4;
		int32_t L_284 = V_6;
		NullCheck(L_283);
		int32_t L_285 = ((int32_t)il2cpp_codegen_add((int32_t)L_284, (int32_t)1));
		uint16_t L_286 = (L_283)->GetAt(static_cast<il2cpp_array_size_t>(L_285));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_287 = Char_IsLower_m3108076820(NULL /*static, unused*/, L_286, /*hidden argument*/NULL);
		if (L_287)
		{
			goto IL_0507;
		}
	}
	{
		CharU5BU5D_t3528271667* L_288 = V_4;
		int32_t L_289 = V_6;
		NullCheck(L_288);
		int32_t L_290 = ((int32_t)il2cpp_codegen_add((int32_t)L_289, (int32_t)1));
		uint16_t L_291 = (L_288)->GetAt(static_cast<il2cpp_array_size_t>(L_290));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_292 = Char_IsUpper_m3564669513(NULL /*static, unused*/, L_291, /*hidden argument*/NULL);
		if (L_292)
		{
			goto IL_0507;
		}
	}
	{
		CharU5BU5D_t3528271667* L_293 = V_4;
		int32_t L_294 = V_6;
		NullCheck(L_293);
		int32_t L_295 = ((int32_t)il2cpp_codegen_add((int32_t)L_294, (int32_t)1));
		uint16_t L_296 = (L_293)->GetAt(static_cast<il2cpp_array_size_t>(L_295));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_297 = Char_IsNumber_m2445552278(NULL /*static, unused*/, L_296, /*hidden argument*/NULL);
		G_B91_0 = ((((int32_t)L_297) == ((int32_t)0))? 1 : 0);
		goto IL_0508;
	}

IL_0507:
	{
		G_B91_0 = 0;
	}

IL_0508:
	{
		G_B93_0 = G_B91_0;
		goto IL_050b;
	}

IL_050a:
	{
		G_B93_0 = 1;
	}

IL_050b:
	{
		V_12 = (bool)G_B93_0;
		bool L_298 = V_12;
		if (L_298)
		{
			goto IL_0525;
		}
	}
	{
		List_1_t811567916 * L_299 = V_9;
		CharU5BU5D_t3528271667* L_300 = V_4;
		int32_t L_301 = V_6;
		NullCheck(L_300);
		int32_t L_302 = L_301;
		uint16_t L_303 = (L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_302));
		NullCheck(L_299);
		List_1_Add_m4264982211(L_299, L_303, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_06bc;
	}

IL_0525:
	{
		CharU5BU5D_t3528271667* L_304 = V_4;
		int32_t L_305 = V_6;
		NullCheck(L_304);
		int32_t L_306 = L_305;
		uint16_t L_307 = (L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_306));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_308 = Char_IsNumber_m2445552278(NULL /*static, unused*/, L_307, /*hidden argument*/NULL);
		if (L_308)
		{
			goto IL_0564;
		}
	}
	{
		CharU5BU5D_t3528271667* L_309 = V_4;
		int32_t L_310 = V_6;
		NullCheck(L_309);
		int32_t L_311 = L_310;
		uint16_t L_312 = (L_309)->GetAt(static_cast<il2cpp_array_size_t>(L_311));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_313 = Char_IsLower_m3108076820(NULL /*static, unused*/, L_312, /*hidden argument*/NULL);
		if (L_313)
		{
			goto IL_0564;
		}
	}
	{
		CharU5BU5D_t3528271667* L_314 = V_4;
		int32_t L_315 = V_6;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		uint16_t L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_318 = Char_IsUpper_m3564669513(NULL /*static, unused*/, L_317, /*hidden argument*/NULL);
		if (L_318)
		{
			goto IL_0564;
		}
	}
	{
		CharU5BU5D_t3528271667* L_319 = V_4;
		int32_t L_320 = V_6;
		NullCheck(L_319);
		int32_t L_321 = L_320;
		uint16_t L_322 = (L_319)->GetAt(static_cast<il2cpp_array_size_t>(L_321));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_323 = Char_IsSymbol_m1780736836(NULL /*static, unused*/, L_322, /*hidden argument*/NULL);
		if (L_323)
		{
			goto IL_0564;
		}
	}
	{
		CharU5BU5D_t3528271667* L_324 = V_4;
		int32_t L_325 = V_6;
		NullCheck(L_324);
		int32_t L_326 = L_325;
		uint16_t L_327 = (L_324)->GetAt(static_cast<il2cpp_array_size_t>(L_326));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_328 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_327, /*hidden argument*/NULL);
		G_B101_0 = ((((int32_t)L_328) == ((int32_t)0))? 1 : 0);
		goto IL_0565;
	}

IL_0564:
	{
		G_B101_0 = 0;
	}

IL_0565:
	{
		V_12 = (bool)G_B101_0;
		bool L_329 = V_12;
		if (L_329)
		{
			goto IL_05fa;
		}
	}
	{
		CharU5BU5D_t3528271667* L_330 = V_4;
		int32_t L_331 = V_6;
		NullCheck(L_330);
		int32_t L_332 = L_331;
		uint16_t L_333 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_332));
		V_12 = (bool)((((int32_t)((((int32_t)L_333) == ((int32_t)((int32_t)40)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_334 = V_12;
		if (L_334)
		{
			goto IL_058d;
		}
	}
	{
		List_1_t811567916 * L_335 = V_9;
		NullCheck(L_335);
		List_1_Add_m4264982211(L_335, ((int32_t)41), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_05f4;
	}

IL_058d:
	{
		CharU5BU5D_t3528271667* L_336 = V_4;
		int32_t L_337 = V_6;
		NullCheck(L_336);
		int32_t L_338 = L_337;
		uint16_t L_339 = (L_336)->GetAt(static_cast<il2cpp_array_size_t>(L_338));
		V_12 = (bool)((((int32_t)((((int32_t)L_339) == ((int32_t)((int32_t)41)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_340 = V_12;
		if (L_340)
		{
			goto IL_05ab;
		}
	}
	{
		List_1_t811567916 * L_341 = V_9;
		NullCheck(L_341);
		List_1_Add_m4264982211(L_341, ((int32_t)40), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_05f4;
	}

IL_05ab:
	{
		CharU5BU5D_t3528271667* L_342 = V_4;
		int32_t L_343 = V_6;
		NullCheck(L_342);
		int32_t L_344 = L_343;
		uint16_t L_345 = (L_342)->GetAt(static_cast<il2cpp_array_size_t>(L_344));
		V_12 = (bool)((((int32_t)((((int32_t)L_345) == ((int32_t)((int32_t)60)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_346 = V_12;
		if (L_346)
		{
			goto IL_05c9;
		}
	}
	{
		List_1_t811567916 * L_347 = V_9;
		NullCheck(L_347);
		List_1_Add_m4264982211(L_347, ((int32_t)62), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_05f4;
	}

IL_05c9:
	{
		CharU5BU5D_t3528271667* L_348 = V_4;
		int32_t L_349 = V_6;
		NullCheck(L_348);
		int32_t L_350 = L_349;
		uint16_t L_351 = (L_348)->GetAt(static_cast<il2cpp_array_size_t>(L_350));
		V_12 = (bool)((((int32_t)((((int32_t)L_351) == ((int32_t)((int32_t)62)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_352 = V_12;
		if (L_352)
		{
			goto IL_05e7;
		}
	}
	{
		List_1_t811567916 * L_353 = V_9;
		NullCheck(L_353);
		List_1_Add_m4264982211(L_353, ((int32_t)60), /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_05f4;
	}

IL_05e7:
	{
		List_1_t811567916 * L_354 = V_9;
		CharU5BU5D_t3528271667* L_355 = V_4;
		int32_t L_356 = V_6;
		NullCheck(L_355);
		int32_t L_357 = L_356;
		uint16_t L_358 = (L_355)->GetAt(static_cast<il2cpp_array_size_t>(L_357));
		NullCheck(L_354);
		List_1_Add_m4264982211(L_354, L_358, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
	}

IL_05f4:
	{
		goto IL_06bc;
	}

IL_05fa:
	{
		CharU5BU5D_t3528271667* L_359 = V_4;
		int32_t L_360 = V_6;
		NullCheck(L_359);
		int32_t L_361 = L_360;
		uint16_t L_362 = (L_359)->GetAt(static_cast<il2cpp_array_size_t>(L_361));
		if ((((int32_t)L_362) < ((int32_t)((int32_t)55296))))
		{
			goto IL_0612;
		}
	}
	{
		CharU5BU5D_t3528271667* L_363 = V_4;
		int32_t L_364 = V_6;
		NullCheck(L_363);
		int32_t L_365 = L_364;
		uint16_t L_366 = (L_363)->GetAt(static_cast<il2cpp_array_size_t>(L_365));
		if ((((int32_t)L_366) <= ((int32_t)((int32_t)56319))))
		{
			goto IL_062f;
		}
	}

IL_0612:
	{
		CharU5BU5D_t3528271667* L_367 = V_4;
		int32_t L_368 = V_6;
		NullCheck(L_367);
		int32_t L_369 = L_368;
		uint16_t L_370 = (L_367)->GetAt(static_cast<il2cpp_array_size_t>(L_369));
		if ((((int32_t)L_370) < ((int32_t)((int32_t)56320))))
		{
			goto IL_062c;
		}
	}
	{
		CharU5BU5D_t3528271667* L_371 = V_4;
		int32_t L_372 = V_6;
		NullCheck(L_371);
		int32_t L_373 = L_372;
		uint16_t L_374 = (L_371)->GetAt(static_cast<il2cpp_array_size_t>(L_373));
		G_B117_0 = ((((int32_t)L_374) > ((int32_t)((int32_t)57343)))? 1 : 0);
		goto IL_062d;
	}

IL_062c:
	{
		G_B117_0 = 1;
	}

IL_062d:
	{
		G_B119_0 = G_B117_0;
		goto IL_0630;
	}

IL_062f:
	{
		G_B119_0 = 0;
	}

IL_0630:
	{
		V_12 = (bool)G_B119_0;
		bool L_375 = V_12;
		if (L_375)
		{
			goto IL_0647;
		}
	}
	{
		List_1_t811567916 * L_376 = V_9;
		CharU5BU5D_t3528271667* L_377 = V_4;
		int32_t L_378 = V_6;
		NullCheck(L_377);
		int32_t L_379 = L_378;
		uint16_t L_380 = (L_377)->GetAt(static_cast<il2cpp_array_size_t>(L_379));
		NullCheck(L_376);
		List_1_Add_m4264982211(L_376, L_380, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		goto IL_06bc;
	}

IL_0647:
	{
		List_1_t811567916 * L_381 = V_9;
		NullCheck(L_381);
		int32_t L_382 = List_1_get_Count_m2041960438(L_381, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		V_12 = (bool)((((int32_t)((((int32_t)L_382) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_383 = V_12;
		if (L_383)
		{
			goto IL_069c;
		}
	}
	{
		V_10 = 0;
		goto IL_0682;
	}

IL_0661:
	{
		List_1_t811567916 * L_384 = V_8;
		List_1_t811567916 * L_385 = V_9;
		List_1_t811567916 * L_386 = V_9;
		NullCheck(L_386);
		int32_t L_387 = List_1_get_Count_m2041960438(L_386, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		int32_t L_388 = V_10;
		NullCheck(L_385);
		Il2CppChar L_389 = List_1_get_Item_m634868510(L_385, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_387, (int32_t)1)), (int32_t)L_388)), /*hidden argument*/List_1_get_Item_m634868510_RuntimeMethod_var);
		NullCheck(L_384);
		List_1_Add_m4264982211(L_384, L_389, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		int32_t L_390 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_390, (int32_t)1));
	}

IL_0682:
	{
		int32_t L_391 = V_10;
		List_1_t811567916 * L_392 = V_9;
		NullCheck(L_392);
		int32_t L_393 = List_1_get_Count_m2041960438(L_392, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		V_12 = (bool)((((int32_t)L_391) < ((int32_t)L_393))? 1 : 0);
		bool L_394 = V_12;
		if (L_394)
		{
			goto IL_0661;
		}
	}
	{
		List_1_t811567916 * L_395 = V_9;
		NullCheck(L_395);
		List_1_Clear_m2890230691(L_395, /*hidden argument*/List_1_Clear_m2890230691_RuntimeMethod_var);
	}

IL_069c:
	{
		CharU5BU5D_t3528271667* L_396 = V_4;
		int32_t L_397 = V_6;
		NullCheck(L_396);
		int32_t L_398 = L_397;
		uint16_t L_399 = (L_396)->GetAt(static_cast<il2cpp_array_size_t>(L_398));
		V_12 = (bool)((((int32_t)L_399) == ((int32_t)((int32_t)65535)))? 1 : 0);
		bool L_400 = V_12;
		if (L_400)
		{
			goto IL_06bb;
		}
	}
	{
		List_1_t811567916 * L_401 = V_8;
		CharU5BU5D_t3528271667* L_402 = V_4;
		int32_t L_403 = V_6;
		NullCheck(L_402);
		int32_t L_404 = L_403;
		uint16_t L_405 = (L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_404));
		NullCheck(L_401);
		List_1_Add_m4264982211(L_401, L_405, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
	}

IL_06bb:
	{
	}

IL_06bc:
	{
		int32_t L_406 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_406, (int32_t)1));
	}

IL_06c3:
	{
		int32_t L_407 = V_6;
		V_12 = (bool)((((int32_t)((((int32_t)L_407) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_408 = V_12;
		if (L_408)
		{
			goto IL_03b2;
		}
	}
	{
		List_1_t811567916 * L_409 = V_9;
		NullCheck(L_409);
		int32_t L_410 = List_1_get_Count_m2041960438(L_409, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		V_12 = (bool)((((int32_t)((((int32_t)L_410) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_411 = V_12;
		if (L_411)
		{
			goto IL_0728;
		}
	}
	{
		V_10 = 0;
		goto IL_070e;
	}

IL_06ed:
	{
		List_1_t811567916 * L_412 = V_8;
		List_1_t811567916 * L_413 = V_9;
		List_1_t811567916 * L_414 = V_9;
		NullCheck(L_414);
		int32_t L_415 = List_1_get_Count_m2041960438(L_414, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		int32_t L_416 = V_10;
		NullCheck(L_413);
		Il2CppChar L_417 = List_1_get_Item_m634868510(L_413, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_415, (int32_t)1)), (int32_t)L_416)), /*hidden argument*/List_1_get_Item_m634868510_RuntimeMethod_var);
		NullCheck(L_412);
		List_1_Add_m4264982211(L_412, L_417, /*hidden argument*/List_1_Add_m4264982211_RuntimeMethod_var);
		int32_t L_418 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_418, (int32_t)1));
	}

IL_070e:
	{
		int32_t L_419 = V_10;
		List_1_t811567916 * L_420 = V_9;
		NullCheck(L_420);
		int32_t L_421 = List_1_get_Count_m2041960438(L_420, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		V_12 = (bool)((((int32_t)L_419) < ((int32_t)L_421))? 1 : 0);
		bool L_422 = V_12;
		if (L_422)
		{
			goto IL_06ed;
		}
	}
	{
		List_1_t811567916 * L_423 = V_9;
		NullCheck(L_423);
		List_1_Clear_m2890230691(L_423, /*hidden argument*/List_1_Clear_m2890230691_RuntimeMethod_var);
	}

IL_0728:
	{
		List_1_t811567916 * L_424 = V_8;
		NullCheck(L_424);
		int32_t L_425 = List_1_get_Count_m2041960438(L_424, /*hidden argument*/List_1_get_Count_m2041960438_RuntimeMethod_var);
		V_4 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)L_425));
		V_6 = 0;
		goto IL_074f;
	}

IL_073b:
	{
		CharU5BU5D_t3528271667* L_426 = V_4;
		int32_t L_427 = V_6;
		List_1_t811567916 * L_428 = V_8;
		int32_t L_429 = V_6;
		NullCheck(L_428);
		Il2CppChar L_430 = List_1_get_Item_m634868510(L_428, L_429, /*hidden argument*/List_1_get_Item_m634868510_RuntimeMethod_var);
		NullCheck(L_426);
		(L_426)->SetAt(static_cast<il2cpp_array_size_t>(L_427), (Il2CppChar)L_430);
		int32_t L_431 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_431, (int32_t)1));
	}

IL_074f:
	{
		int32_t L_432 = V_6;
		CharU5BU5D_t3528271667* L_433 = V_4;
		NullCheck(L_433);
		V_12 = (bool)((((int32_t)L_432) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_433)->max_length))))))? 1 : 0);
		bool L_434 = V_12;
		if (L_434)
		{
			goto IL_073b;
		}
	}
	{
		CharU5BU5D_t3528271667* L_435 = V_4;
		String_t* L_436 = String_CreateString_m2818852475(NULL, L_435, /*hidden argument*/NULL);
		___str0 = L_436;
		String_t* L_437 = ___str0;
		V_11 = L_437;
		goto IL_076b;
	}

IL_076b:
	{
		String_t* L_438 = V_11;
		return L_438;
	}
}
// System.Boolean ArabicFixerTool::IsIgnoredCharacter(System.Char)
extern "C"  bool ArabicFixerTool_IsIgnoredCharacter_m803703993 (RuntimeObject * __this /* static, unused */, Il2CppChar ___ch0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_IsIgnoredCharacter_m803703993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B22_0 = 0;
	{
		Il2CppChar L_0 = ___ch0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppChar L_2 = ___ch0;
		bool L_3 = Char_IsNumber_m2445552278(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppChar L_4 = ___ch0;
		bool L_5 = Char_IsLower_m3108076820(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Il2CppChar L_6 = ___ch0;
		bool L_7 = Char_IsUpper_m3564669513(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Il2CppChar L_8 = ___ch0;
		bool L_9 = Char_IsSymbol_m1780736836(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		Il2CppChar L_10 = ___ch0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)64342))))
		{
			goto IL_0047;
		}
	}
	{
		Il2CppChar L_11 = ___ch0;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)64378))))
		{
			goto IL_0047;
		}
	}
	{
		Il2CppChar L_12 = ___ch0;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)64394))))
		{
			goto IL_0047;
		}
	}
	{
		Il2CppChar L_13 = ___ch0;
		G_B5_0 = ((((int32_t)L_13) == ((int32_t)((int32_t)64402)))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 1;
	}

IL_0048:
	{
		V_5 = (bool)G_B5_0;
		Il2CppChar L_14 = ___ch0;
		if ((((int32_t)L_14) > ((int32_t)((int32_t)65279))))
		{
			goto IL_005f;
		}
	}
	{
		Il2CppChar L_15 = ___ch0;
		G_B8_0 = ((((int32_t)((((int32_t)L_15) < ((int32_t)((int32_t)65136)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B8_0 = 0;
	}

IL_0060:
	{
		V_6 = (bool)G_B8_0;
		bool L_16 = V_6;
		if (L_16)
		{
			goto IL_006a;
		}
	}
	{
		bool L_17 = V_5;
		G_B11_0 = ((int32_t)(L_17));
		goto IL_006b;
	}

IL_006a:
	{
		G_B11_0 = 1;
	}

IL_006b:
	{
		V_7 = (bool)G_B11_0;
		bool L_18 = V_0;
		if (L_18)
		{
			goto IL_009a;
		}
	}
	{
		bool L_19 = V_1;
		if (L_19)
		{
			goto IL_009a;
		}
	}
	{
		bool L_20 = V_2;
		if (L_20)
		{
			goto IL_009a;
		}
	}
	{
		bool L_21 = V_3;
		if (L_21)
		{
			goto IL_009a;
		}
	}
	{
		bool L_22 = V_4;
		if (L_22)
		{
			goto IL_009a;
		}
	}
	{
		bool L_23 = V_7;
		if (!L_23)
		{
			goto IL_009a;
		}
	}
	{
		Il2CppChar L_24 = ___ch0;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)97))))
		{
			goto IL_009a;
		}
	}
	{
		Il2CppChar L_25 = ___ch0;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)62))))
		{
			goto IL_009a;
		}
	}
	{
		Il2CppChar L_26 = ___ch0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)60))))
		{
			goto IL_009a;
		}
	}
	{
		Il2CppChar L_27 = ___ch0;
		G_B22_0 = ((((int32_t)L_27) == ((int32_t)((int32_t)1563)))? 1 : 0);
		goto IL_009b;
	}

IL_009a:
	{
		G_B22_0 = 1;
	}

IL_009b:
	{
		V_8 = (bool)G_B22_0;
		goto IL_009f;
	}

IL_009f:
	{
		bool L_28 = V_8;
		return L_28;
	}
}
// System.Boolean ArabicFixerTool::IsLeadingLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsLeadingLetter_m2995983843 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_IsLeadingLetter_m2995983843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B35_0 = 0;
	{
		int32_t L_0 = ___index1;
		if (!L_0)
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_1 = ___letters0;
		int32_t L_2 = ___index1;
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_5 = ___letters0;
		int32_t L_6 = ___index1;
		NullCheck(L_5);
		int32_t L_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		uint16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((((int32_t)L_8) == ((int32_t)((int32_t)42))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_9 = ___letters0;
		int32_t L_10 = ___index1;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)1));
		uint16_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((((int32_t)L_12) == ((int32_t)((int32_t)65))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_13 = ___letters0;
		int32_t L_14 = ___index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1));
		uint16_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_17 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_18 = ___letters0;
		int32_t L_19 = ___index1;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1));
		uint16_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		if ((((int32_t)L_21) == ((int32_t)((int32_t)62))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_22 = ___letters0;
		int32_t L_23 = ___index1;
		NullCheck(L_22);
		int32_t L_24 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)1));
		uint16_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		if ((((int32_t)L_25) == ((int32_t)((int32_t)60))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_26 = ___letters0;
		int32_t L_27 = ___index1;
		NullCheck(L_26);
		int32_t L_28 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)1));
		uint16_t L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		if ((((int32_t)L_29) == ((int32_t)((int32_t)65165))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_30 = ___letters0;
		int32_t L_31 = ___index1;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1));
		uint16_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		if ((((int32_t)L_33) == ((int32_t)((int32_t)65193))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_34 = ___letters0;
		int32_t L_35 = ___index1;
		NullCheck(L_34);
		int32_t L_36 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)1));
		uint16_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		if ((((int32_t)L_37) == ((int32_t)((int32_t)65195))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_38 = ___letters0;
		int32_t L_39 = ___index1;
		NullCheck(L_38);
		int32_t L_40 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_39, (int32_t)1));
		uint16_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		if ((((int32_t)L_41) == ((int32_t)((int32_t)65197))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_42 = ___letters0;
		int32_t L_43 = ___index1;
		NullCheck(L_42);
		int32_t L_44 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_43, (int32_t)1));
		uint16_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		if ((((int32_t)L_45) == ((int32_t)((int32_t)65199))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_46 = ___letters0;
		int32_t L_47 = ___index1;
		NullCheck(L_46);
		int32_t L_48 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_47, (int32_t)1));
		uint16_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		if ((((int32_t)L_49) == ((int32_t)((int32_t)64394))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_50 = ___letters0;
		int32_t L_51 = ___index1;
		NullCheck(L_50);
		int32_t L_52 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_51, (int32_t)1));
		uint16_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		if ((((int32_t)L_53) == ((int32_t)((int32_t)65263))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_54 = ___letters0;
		int32_t L_55 = ___index1;
		NullCheck(L_54);
		int32_t L_56 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)1));
		uint16_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		if ((((int32_t)L_57) == ((int32_t)((int32_t)65261))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_58 = ___letters0;
		int32_t L_59 = ___index1;
		NullCheck(L_58);
		int32_t L_60 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_59, (int32_t)1));
		uint16_t L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		if ((((int32_t)L_61) == ((int32_t)((int32_t)65153))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_62 = ___letters0;
		int32_t L_63 = ___index1;
		NullCheck(L_62);
		int32_t L_64 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_63, (int32_t)1));
		uint16_t L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		if ((((int32_t)L_65) == ((int32_t)((int32_t)65155))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_66 = ___letters0;
		int32_t L_67 = ___index1;
		NullCheck(L_66);
		int32_t L_68 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_67, (int32_t)1));
		uint16_t L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		if ((((int32_t)L_69) == ((int32_t)((int32_t)65159))))
		{
			goto IL_00e8;
		}
	}
	{
		CharU5BU5D_t3528271667* L_70 = ___letters0;
		int32_t L_71 = ___index1;
		NullCheck(L_70);
		int32_t L_72 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_71, (int32_t)1));
		uint16_t L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		if ((!(((uint32_t)L_73) == ((uint32_t)((int32_t)65157)))))
		{
			goto IL_0184;
		}
	}

IL_00e8:
	{
		CharU5BU5D_t3528271667* L_74 = ___letters0;
		int32_t L_75 = ___index1;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		uint16_t L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		if ((((int32_t)L_77) == ((int32_t)((int32_t)32))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_78 = ___letters0;
		int32_t L_79 = ___index1;
		NullCheck(L_78);
		int32_t L_80 = L_79;
		uint16_t L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		if ((((int32_t)L_81) == ((int32_t)((int32_t)65193))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_82 = ___letters0;
		int32_t L_83 = ___index1;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		uint16_t L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		if ((((int32_t)L_85) == ((int32_t)((int32_t)65195))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_86 = ___letters0;
		int32_t L_87 = ___index1;
		NullCheck(L_86);
		int32_t L_88 = L_87;
		uint16_t L_89 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		if ((((int32_t)L_89) == ((int32_t)((int32_t)65197))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_90 = ___letters0;
		int32_t L_91 = ___index1;
		NullCheck(L_90);
		int32_t L_92 = L_91;
		uint16_t L_93 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		if ((((int32_t)L_93) == ((int32_t)((int32_t)65199))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_94 = ___letters0;
		int32_t L_95 = ___index1;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		uint16_t L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		if ((((int32_t)L_97) == ((int32_t)((int32_t)64394))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_98 = ___letters0;
		int32_t L_99 = ___index1;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		uint16_t L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		if ((((int32_t)L_101) == ((int32_t)((int32_t)65165))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_102 = ___letters0;
		int32_t L_103 = ___index1;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		uint16_t L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		if ((((int32_t)L_105) == ((int32_t)((int32_t)65155))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_106 = ___letters0;
		int32_t L_107 = ___index1;
		NullCheck(L_106);
		int32_t L_108 = L_107;
		uint16_t L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		if ((((int32_t)L_109) == ((int32_t)((int32_t)65159))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_110 = ___letters0;
		int32_t L_111 = ___index1;
		NullCheck(L_110);
		int32_t L_112 = L_111;
		uint16_t L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		if ((((int32_t)L_113) == ((int32_t)((int32_t)65261))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_114 = ___letters0;
		int32_t L_115 = ___index1;
		NullCheck(L_114);
		int32_t L_116 = L_115;
		uint16_t L_117 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116));
		if ((((int32_t)L_117) == ((int32_t)((int32_t)65152))))
		{
			goto IL_0184;
		}
	}
	{
		int32_t L_118 = ___index1;
		CharU5BU5D_t3528271667* L_119 = ___letters0;
		NullCheck(L_119);
		if ((((int32_t)L_118) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_119)->max_length)))), (int32_t)1)))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_120 = ___letters0;
		int32_t L_121 = ___index1;
		NullCheck(L_120);
		int32_t L_122 = ((int32_t)il2cpp_codegen_add((int32_t)L_121, (int32_t)1));
		uint16_t L_123 = (L_120)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		if ((((int32_t)L_123) == ((int32_t)((int32_t)32))))
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_124 = ___letters0;
		int32_t L_125 = ___index1;
		NullCheck(L_124);
		int32_t L_126 = ((int32_t)il2cpp_codegen_add((int32_t)L_125, (int32_t)1));
		uint16_t L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_128 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		if (L_128)
		{
			goto IL_0184;
		}
	}
	{
		CharU5BU5D_t3528271667* L_129 = ___letters0;
		int32_t L_130 = ___index1;
		NullCheck(L_129);
		int32_t L_131 = ((int32_t)il2cpp_codegen_add((int32_t)L_130, (int32_t)1));
		uint16_t L_132 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		G_B35_0 = ((((int32_t)L_132) == ((int32_t)((int32_t)65152)))? 1 : 0);
		goto IL_0185;
	}

IL_0184:
	{
		G_B35_0 = 1;
	}

IL_0185:
	{
		V_1 = (bool)G_B35_0;
		bool L_133 = V_1;
		if (L_133)
		{
			goto IL_018e;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0192;
	}

IL_018e:
	{
		V_0 = (bool)0;
		goto IL_0192;
	}

IL_0192:
	{
		bool L_134 = V_0;
		return L_134;
	}
}
// System.Boolean ArabicFixerTool::IsFinishingLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsFinishingLetter_m1180940598 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_IsFinishingLetter_m1180940598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B24_0 = 0;
	{
		int32_t L_0 = ___index1;
		if (!L_0)
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_1 = ___letters0;
		int32_t L_2 = ___index1;
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_5 = ___letters0;
		int32_t L_6 = ___index1;
		NullCheck(L_5);
		int32_t L_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		uint16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((((int32_t)L_8) == ((int32_t)((int32_t)42))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_9 = ___letters0;
		int32_t L_10 = ___index1;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)1));
		uint16_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((((int32_t)L_12) == ((int32_t)((int32_t)65))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_13 = ___letters0;
		int32_t L_14 = ___index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1));
		uint16_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if ((((int32_t)L_16) == ((int32_t)((int32_t)65193))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_17 = ___letters0;
		int32_t L_18 = ___index1;
		NullCheck(L_17);
		int32_t L_19 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1));
		uint16_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		if ((((int32_t)L_20) == ((int32_t)((int32_t)65195))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_21 = ___letters0;
		int32_t L_22 = ___index1;
		NullCheck(L_21);
		int32_t L_23 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1));
		uint16_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		if ((((int32_t)L_24) == ((int32_t)((int32_t)65197))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_25 = ___letters0;
		int32_t L_26 = ___index1;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)1));
		uint16_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		if ((((int32_t)L_28) == ((int32_t)((int32_t)65199))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_29 = ___letters0;
		int32_t L_30 = ___index1;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)1));
		uint16_t L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		if ((((int32_t)L_32) == ((int32_t)((int32_t)64394))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_33 = ___letters0;
		int32_t L_34 = ___index1;
		NullCheck(L_33);
		int32_t L_35 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_34, (int32_t)1));
		uint16_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		if ((((int32_t)L_36) == ((int32_t)((int32_t)65263))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_37 = ___letters0;
		int32_t L_38 = ___index1;
		NullCheck(L_37);
		int32_t L_39 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_38, (int32_t)1));
		uint16_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		if ((((int32_t)L_40) == ((int32_t)((int32_t)65261))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_41 = ___letters0;
		int32_t L_42 = ___index1;
		NullCheck(L_41);
		int32_t L_43 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1));
		uint16_t L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		if ((((int32_t)L_44) == ((int32_t)((int32_t)65165))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_45 = ___letters0;
		int32_t L_46 = ___index1;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)1));
		uint16_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		if ((((int32_t)L_48) == ((int32_t)((int32_t)65153))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_49 = ___letters0;
		int32_t L_50 = ___index1;
		NullCheck(L_49);
		int32_t L_51 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_50, (int32_t)1));
		uint16_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		if ((((int32_t)L_52) == ((int32_t)((int32_t)65155))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_53 = ___letters0;
		int32_t L_54 = ___index1;
		NullCheck(L_53);
		int32_t L_55 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)1));
		uint16_t L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		if ((((int32_t)L_56) == ((int32_t)((int32_t)65159))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_57 = ___letters0;
		int32_t L_58 = ___index1;
		NullCheck(L_57);
		int32_t L_59 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_58, (int32_t)1));
		uint16_t L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		if ((((int32_t)L_60) == ((int32_t)((int32_t)65157))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_61 = ___letters0;
		int32_t L_62 = ___index1;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_62, (int32_t)1));
		uint16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if ((((int32_t)L_64) == ((int32_t)((int32_t)65152))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_65 = ___letters0;
		int32_t L_66 = ___index1;
		NullCheck(L_65);
		int32_t L_67 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_66, (int32_t)1));
		uint16_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_69 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		if (L_69)
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_70 = ___letters0;
		int32_t L_71 = ___index1;
		NullCheck(L_70);
		int32_t L_72 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_71, (int32_t)1));
		uint16_t L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		if ((((int32_t)L_73) == ((int32_t)((int32_t)62))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_74 = ___letters0;
		int32_t L_75 = ___index1;
		NullCheck(L_74);
		int32_t L_76 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_75, (int32_t)1));
		uint16_t L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		if ((((int32_t)L_77) == ((int32_t)((int32_t)60))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_78 = ___letters0;
		int32_t L_79 = ___index1;
		NullCheck(L_78);
		int32_t L_80 = L_79;
		uint16_t L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		if ((((int32_t)L_81) == ((int32_t)((int32_t)32))))
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_82 = ___index1;
		CharU5BU5D_t3528271667* L_83 = ___letters0;
		NullCheck(L_83);
		if ((((int32_t)L_82) >= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_83)->max_length)))))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3528271667* L_84 = ___letters0;
		int32_t L_85 = ___index1;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		uint16_t L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		G_B24_0 = ((((int32_t)L_87) == ((int32_t)((int32_t)65152)))? 1 : 0);
		goto IL_0111;
	}

IL_0110:
	{
		G_B24_0 = 1;
	}

IL_0111:
	{
		V_1 = (bool)G_B24_0;
		bool L_88 = V_1;
		if (L_88)
		{
			goto IL_011a;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_011e;
	}

IL_011a:
	{
		V_0 = (bool)0;
		goto IL_011e;
	}

IL_011e:
	{
		bool L_89 = V_0;
		return L_89;
	}
}
// System.Boolean ArabicFixerTool::IsMiddleLetter(System.Char[],System.Int32)
extern "C"  bool ArabicFixerTool_IsMiddleLetter_m3068743487 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___letters0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool_IsMiddleLetter_m3068743487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B41_0 = 0;
	{
		int32_t L_0 = ___index1;
		if (!L_0)
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_1 = ___letters0;
		int32_t L_2 = ___index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_5 = ___letters0;
		int32_t L_6 = ___index1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((((int32_t)L_8) == ((int32_t)((int32_t)65165))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_9 = ___letters0;
		int32_t L_10 = ___index1;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint16_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((((int32_t)L_12) == ((int32_t)((int32_t)65193))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_13 = ___letters0;
		int32_t L_14 = ___index1;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint16_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if ((((int32_t)L_16) == ((int32_t)((int32_t)65195))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_17 = ___letters0;
		int32_t L_18 = ___index1;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		uint16_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		if ((((int32_t)L_20) == ((int32_t)((int32_t)65197))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_21 = ___letters0;
		int32_t L_22 = ___index1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		uint16_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		if ((((int32_t)L_24) == ((int32_t)((int32_t)65199))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_25 = ___letters0;
		int32_t L_26 = ___index1;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		uint16_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		if ((((int32_t)L_28) == ((int32_t)((int32_t)64394))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_29 = ___letters0;
		int32_t L_30 = ___index1;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		uint16_t L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		if ((((int32_t)L_32) == ((int32_t)((int32_t)65263))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_33 = ___letters0;
		int32_t L_34 = ___index1;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		uint16_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		if ((((int32_t)L_36) == ((int32_t)((int32_t)65261))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_37 = ___letters0;
		int32_t L_38 = ___index1;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		uint16_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		if ((((int32_t)L_40) == ((int32_t)((int32_t)65153))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_41 = ___letters0;
		int32_t L_42 = ___index1;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		uint16_t L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		if ((((int32_t)L_44) == ((int32_t)((int32_t)65155))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_45 = ___letters0;
		int32_t L_46 = ___index1;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		uint16_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		if ((((int32_t)L_48) == ((int32_t)((int32_t)65159))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_49 = ___letters0;
		int32_t L_50 = ___index1;
		NullCheck(L_49);
		int32_t L_51 = L_50;
		uint16_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		if ((((int32_t)L_52) == ((int32_t)((int32_t)65157))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_53 = ___letters0;
		int32_t L_54 = ___index1;
		NullCheck(L_53);
		int32_t L_55 = L_54;
		uint16_t L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		if ((((int32_t)L_56) == ((int32_t)((int32_t)65152))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_57 = ___letters0;
		int32_t L_58 = ___index1;
		NullCheck(L_57);
		int32_t L_59 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_58, (int32_t)1));
		uint16_t L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		if ((((int32_t)L_60) == ((int32_t)((int32_t)65165))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_61 = ___letters0;
		int32_t L_62 = ___index1;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_62, (int32_t)1));
		uint16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if ((((int32_t)L_64) == ((int32_t)((int32_t)65193))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_65 = ___letters0;
		int32_t L_66 = ___index1;
		NullCheck(L_65);
		int32_t L_67 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_66, (int32_t)1));
		uint16_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		if ((((int32_t)L_68) == ((int32_t)((int32_t)65195))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_69 = ___letters0;
		int32_t L_70 = ___index1;
		NullCheck(L_69);
		int32_t L_71 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_70, (int32_t)1));
		uint16_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		if ((((int32_t)L_72) == ((int32_t)((int32_t)65197))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_73 = ___letters0;
		int32_t L_74 = ___index1;
		NullCheck(L_73);
		int32_t L_75 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_74, (int32_t)1));
		uint16_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		if ((((int32_t)L_76) == ((int32_t)((int32_t)65199))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_77 = ___letters0;
		int32_t L_78 = ___index1;
		NullCheck(L_77);
		int32_t L_79 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_78, (int32_t)1));
		uint16_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		if ((((int32_t)L_80) == ((int32_t)((int32_t)64394))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_81 = ___letters0;
		int32_t L_82 = ___index1;
		NullCheck(L_81);
		int32_t L_83 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_82, (int32_t)1));
		uint16_t L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		if ((((int32_t)L_84) == ((int32_t)((int32_t)65263))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_85 = ___letters0;
		int32_t L_86 = ___index1;
		NullCheck(L_85);
		int32_t L_87 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_86, (int32_t)1));
		uint16_t L_88 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_87));
		if ((((int32_t)L_88) == ((int32_t)((int32_t)65261))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_89 = ___letters0;
		int32_t L_90 = ___index1;
		NullCheck(L_89);
		int32_t L_91 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_90, (int32_t)1));
		uint16_t L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		if ((((int32_t)L_92) == ((int32_t)((int32_t)65153))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_93 = ___letters0;
		int32_t L_94 = ___index1;
		NullCheck(L_93);
		int32_t L_95 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_94, (int32_t)1));
		uint16_t L_96 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		if ((((int32_t)L_96) == ((int32_t)((int32_t)65155))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_97 = ___letters0;
		int32_t L_98 = ___index1;
		NullCheck(L_97);
		int32_t L_99 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_98, (int32_t)1));
		uint16_t L_100 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		if ((((int32_t)L_100) == ((int32_t)((int32_t)65159))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_101 = ___letters0;
		int32_t L_102 = ___index1;
		NullCheck(L_101);
		int32_t L_103 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_102, (int32_t)1));
		uint16_t L_104 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		if ((((int32_t)L_104) == ((int32_t)((int32_t)65157))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_105 = ___letters0;
		int32_t L_106 = ___index1;
		NullCheck(L_105);
		int32_t L_107 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_106, (int32_t)1));
		uint16_t L_108 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107));
		if ((((int32_t)L_108) == ((int32_t)((int32_t)65152))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_109 = ___letters0;
		int32_t L_110 = ___index1;
		NullCheck(L_109);
		int32_t L_111 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_110, (int32_t)1));
		uint16_t L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		if ((((int32_t)L_112) == ((int32_t)((int32_t)62))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_113 = ___letters0;
		int32_t L_114 = ___index1;
		NullCheck(L_113);
		int32_t L_115 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_114, (int32_t)1));
		uint16_t L_116 = (L_113)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		if ((((int32_t)L_116) == ((int32_t)((int32_t)60))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_117 = ___letters0;
		int32_t L_118 = ___index1;
		NullCheck(L_117);
		int32_t L_119 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_118, (int32_t)1));
		uint16_t L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		if ((((int32_t)L_120) == ((int32_t)((int32_t)32))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_121 = ___letters0;
		int32_t L_122 = ___index1;
		NullCheck(L_121);
		int32_t L_123 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_122, (int32_t)1));
		uint16_t L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		if ((((int32_t)L_124) == ((int32_t)((int32_t)42))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_125 = ___letters0;
		int32_t L_126 = ___index1;
		NullCheck(L_125);
		int32_t L_127 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_126, (int32_t)1));
		uint16_t L_128 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_129 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		if (L_129)
		{
			goto IL_01ea;
		}
	}
	{
		int32_t L_130 = ___index1;
		CharU5BU5D_t3528271667* L_131 = ___letters0;
		NullCheck(L_131);
		if ((((int32_t)L_130) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_131)->max_length)))), (int32_t)1)))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_132 = ___letters0;
		int32_t L_133 = ___index1;
		NullCheck(L_132);
		int32_t L_134 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
		uint16_t L_135 = (L_132)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		if ((((int32_t)L_135) == ((int32_t)((int32_t)32))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_136 = ___letters0;
		int32_t L_137 = ___index1;
		NullCheck(L_136);
		int32_t L_138 = ((int32_t)il2cpp_codegen_add((int32_t)L_137, (int32_t)1));
		uint16_t L_139 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		if ((((int32_t)L_139) == ((int32_t)((int32_t)13))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_140 = ___letters0;
		int32_t L_141 = ___index1;
		NullCheck(L_140);
		int32_t L_142 = ((int32_t)il2cpp_codegen_add((int32_t)L_141, (int32_t)1));
		uint16_t L_143 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_142));
		if ((((int32_t)L_143) == ((int32_t)((int32_t)65))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_144 = ___letters0;
		int32_t L_145 = ___index1;
		NullCheck(L_144);
		int32_t L_146 = ((int32_t)il2cpp_codegen_add((int32_t)L_145, (int32_t)1));
		uint16_t L_147 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146));
		if ((((int32_t)L_147) == ((int32_t)((int32_t)62))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_148 = ___letters0;
		int32_t L_149 = ___index1;
		NullCheck(L_148);
		int32_t L_150 = ((int32_t)il2cpp_codegen_add((int32_t)L_149, (int32_t)1));
		uint16_t L_151 = (L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_150));
		if ((((int32_t)L_151) == ((int32_t)((int32_t)62))))
		{
			goto IL_01ea;
		}
	}
	{
		CharU5BU5D_t3528271667* L_152 = ___letters0;
		int32_t L_153 = ___index1;
		NullCheck(L_152);
		int32_t L_154 = ((int32_t)il2cpp_codegen_add((int32_t)L_153, (int32_t)1));
		uint16_t L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		G_B41_0 = ((((int32_t)L_155) == ((int32_t)((int32_t)65152)))? 1 : 0);
		goto IL_01eb;
	}

IL_01ea:
	{
		G_B41_0 = 1;
	}

IL_01eb:
	{
		V_1 = (bool)G_B41_0;
		bool L_156 = V_1;
		if (L_156)
		{
			goto IL_0210;
		}
	}
	{
	}

IL_01f0:
	try
	{ // begin try (depth: 1)
		{
			CharU5BU5D_t3528271667* L_157 = ___letters0;
			int32_t L_158 = ___index1;
			NullCheck(L_157);
			int32_t L_159 = ((int32_t)il2cpp_codegen_add((int32_t)L_158, (int32_t)1));
			uint16_t L_160 = (L_157)->GetAt(static_cast<il2cpp_array_size_t>(L_159));
			IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
			bool L_161 = Char_IsPunctuation_m3984409211(NULL /*static, unused*/, L_160, /*hidden argument*/NULL);
			V_1 = (bool)((((int32_t)L_161) == ((int32_t)0))? 1 : 0);
			bool L_162 = V_1;
			if (L_162)
			{
				goto IL_0206;
			}
		}

IL_0202:
		{
			V_0 = (bool)0;
			goto IL_0214;
		}

IL_0206:
		{
			V_0 = (bool)1;
			goto IL_0214;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_020a;
		throw e;
	}

CATCH_020a:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0214;
	} // end catch (depth: 1)

IL_0210:
	{
		V_0 = (bool)0;
		goto IL_0214;
	}

IL_0214:
	{
		bool L_163 = V_0;
		return L_163;
	}
}
// System.Void ArabicFixerTool::.cctor()
extern "C"  void ArabicFixerTool__cctor_m2328499328 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixerTool__cctor_m2328499328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->set_showTashkeel_0((bool)1);
		((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->set_useHinduNumbers_1((bool)0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArabicMapping::.ctor(System.Int32,System.Int32)
extern "C"  void ArabicMapping__ctor_m2823911374 (ArabicMapping_t3619416640 * __this, int32_t ___from0, int32_t ___to1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___from0;
		__this->set_from_0(L_0);
		int32_t L_1 = ___to1;
		__this->set_to_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ArabicSupport.ArabicFixer::Fix(System.String)
extern "C"  String_t* ArabicFixer_Fix_m3682192890 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___str0;
		String_t* L_1 = ArabicFixer_Fix_m4044602794(NULL /*static, unused*/, L_0, (bool)0, (bool)1, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.String ArabicSupport.ArabicFixer::Fix(System.String,System.Boolean,System.Boolean)
extern "C"  String_t* ArabicFixer_Fix_m4044602794 (RuntimeObject * __this /* static, unused */, String_t* ___str0, bool ___showTashkeel1, bool ___useHinduNumbers2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicFixer_Fix_m4044602794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	StringU5BU5D_t1281789340* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	bool V_5 = false;
	StringU5BU5D_t1281789340* V_6 = NULL;
	{
		bool L_0 = ___showTashkeel1;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->set_showTashkeel_0(L_0);
		bool L_1 = ___useHinduNumbers2;
		((ArabicFixerTool_t2520528640_StaticFields*)il2cpp_codegen_static_fields_for(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var))->set_useHinduNumbers_1(L_1);
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		bool L_3 = String_Contains_m1147431944(L_2, _stringLiteral3452614566, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_5;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_5 = ___str0;
		String_t* L_6 = Environment_get_NewLine_m3211016485(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Replace_m1273907647(L_5, _stringLiteral3452614566, L_6, /*hidden argument*/NULL);
		___str0 = L_7;
	}

IL_0033:
	{
		String_t* L_8 = ___str0;
		String_t* L_9 = Environment_get_NewLine_m3211016485(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = String_Contains_m1147431944(L_8, L_9, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_5;
		if (L_11)
		{
			goto IL_00e4;
		}
	}
	{
		V_6 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)1));
		StringU5BU5D_t1281789340* L_12 = V_6;
		String_t* L_13 = Environment_get_NewLine_m3211016485(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_13);
		StringU5BU5D_t1281789340* L_14 = V_6;
		V_0 = L_14;
		String_t* L_15 = ___str0;
		StringU5BU5D_t1281789340* L_16 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t1281789340* L_17 = String_Split_m4013853433(L_15, L_16, 0, /*hidden argument*/NULL);
		V_1 = L_17;
		StringU5BU5D_t1281789340* L_18 = V_1;
		NullCheck(L_18);
		V_5 = (bool)((((int32_t)((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_19 = V_5;
		if (L_19)
		{
			goto IL_0081;
		}
	}
	{
		String_t* L_20 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_21 = ArabicFixerTool_FixLine_m2584389017(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		goto IL_00ef;
	}

IL_0081:
	{
		StringU5BU5D_t1281789340* L_22 = V_1;
		NullCheck(L_22);
		V_5 = (bool)((((int32_t)((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length))))) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_5;
		if (L_23)
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_24 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_25 = ArabicFixerTool_FixLine_m2584389017(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		goto IL_00ef;
	}

IL_009a:
	{
		StringU5BU5D_t1281789340* L_26 = V_1;
		NullCheck(L_26);
		int32_t L_27 = 0;
		String_t* L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_29 = ArabicFixerTool_FixLine_m2584389017(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		V_3 = 1;
		StringU5BU5D_t1281789340* L_30 = V_1;
		NullCheck(L_30);
		V_5 = (bool)((((int32_t)((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_30)->max_length))))) > ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_31 = V_5;
		if (L_31)
		{
			goto IL_00df;
		}
	}
	{
		goto IL_00d2;
	}

IL_00b8:
	{
		String_t* L_32 = V_2;
		String_t* L_33 = Environment_get_NewLine_m3211016485(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringU5BU5D_t1281789340* L_34 = V_1;
		int32_t L_35 = V_3;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_38 = ArabicFixerTool_FixLine_m2584389017(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m3755062657(NULL /*static, unused*/, L_32, L_33, L_38, /*hidden argument*/NULL);
		V_2 = L_39;
		int32_t L_40 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1));
	}

IL_00d2:
	{
		int32_t L_41 = V_3;
		StringU5BU5D_t1281789340* L_42 = V_1;
		NullCheck(L_42);
		V_5 = (bool)((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_42)->max_length))))))? 1 : 0);
		bool L_43 = V_5;
		if (L_43)
		{
			goto IL_00b8;
		}
	}
	{
	}

IL_00df:
	{
		String_t* L_44 = V_2;
		V_4 = L_44;
		goto IL_00ef;
	}

IL_00e4:
	{
		String_t* L_45 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(ArabicFixerTool_t2520528640_il2cpp_TypeInfo_var);
		String_t* L_46 = ArabicFixerTool_FixLine_m2584389017(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_4 = L_46;
		goto IL_00ef;
	}

IL_00ef:
	{
		String_t* L_47 = V_4;
		return L_47;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArabicTable::.ctor()
extern "C"  void ArabicTable__ctor_m3313828430 (ArabicTable_t2015225755 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicTable__ctor_m3313828430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		List_1_t796524086 * L_0 = (List_1_t796524086 *)il2cpp_codegen_object_new(List_1_t796524086_il2cpp_TypeInfo_var);
		List_1__ctor_m2019409646(L_0, /*hidden argument*/List_1__ctor_m2019409646_RuntimeMethod_var);
		((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->set_mapList_0(L_0);
		List_1_t796524086 * L_1 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_2 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_2, ((int32_t)1569), ((int32_t)65152), /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m1908758304(L_1, L_2, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_3 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_4 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_4, ((int32_t)1575), ((int32_t)65165), /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m1908758304(L_3, L_4, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_5 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_6 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_6, ((int32_t)1571), ((int32_t)65155), /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m1908758304(L_5, L_6, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_7 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_8 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_8, ((int32_t)1572), ((int32_t)65157), /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m1908758304(L_7, L_8, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_9 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_10 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_10, ((int32_t)1573), ((int32_t)65159), /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m1908758304(L_9, L_10, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_11 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_12 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_12, ((int32_t)1609), ((int32_t)65263), /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_m1908758304(L_11, L_12, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_13 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_14 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_14, ((int32_t)1574), ((int32_t)65161), /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_m1908758304(L_13, L_14, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_15 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_16 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_16, ((int32_t)1576), ((int32_t)65167), /*hidden argument*/NULL);
		NullCheck(L_15);
		List_1_Add_m1908758304(L_15, L_16, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_17 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_18 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_18, ((int32_t)1578), ((int32_t)65173), /*hidden argument*/NULL);
		NullCheck(L_17);
		List_1_Add_m1908758304(L_17, L_18, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_19 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_20 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_20, ((int32_t)1579), ((int32_t)65177), /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_Add_m1908758304(L_19, L_20, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_21 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_22 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_22, ((int32_t)1580), ((int32_t)65181), /*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_Add_m1908758304(L_21, L_22, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_23 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_24 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_24, ((int32_t)1581), ((int32_t)65185), /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_m1908758304(L_23, L_24, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_25 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_26 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_26, ((int32_t)1582), ((int32_t)65189), /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_Add_m1908758304(L_25, L_26, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_27 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_28 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_28, ((int32_t)1583), ((int32_t)65193), /*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_m1908758304(L_27, L_28, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_29 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_30 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_30, ((int32_t)1584), ((int32_t)65195), /*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_m1908758304(L_29, L_30, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_31 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_32 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_32, ((int32_t)1585), ((int32_t)65197), /*hidden argument*/NULL);
		NullCheck(L_31);
		List_1_Add_m1908758304(L_31, L_32, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_33 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_34 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_34, ((int32_t)1586), ((int32_t)65199), /*hidden argument*/NULL);
		NullCheck(L_33);
		List_1_Add_m1908758304(L_33, L_34, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_35 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_36 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_36, ((int32_t)1587), ((int32_t)65201), /*hidden argument*/NULL);
		NullCheck(L_35);
		List_1_Add_m1908758304(L_35, L_36, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_37 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_38 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_38, ((int32_t)1588), ((int32_t)65205), /*hidden argument*/NULL);
		NullCheck(L_37);
		List_1_Add_m1908758304(L_37, L_38, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_39 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_40 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_40, ((int32_t)1589), ((int32_t)65209), /*hidden argument*/NULL);
		NullCheck(L_39);
		List_1_Add_m1908758304(L_39, L_40, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_41 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_42 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_42, ((int32_t)1590), ((int32_t)65213), /*hidden argument*/NULL);
		NullCheck(L_41);
		List_1_Add_m1908758304(L_41, L_42, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_43 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_44 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_44, ((int32_t)1591), ((int32_t)65217), /*hidden argument*/NULL);
		NullCheck(L_43);
		List_1_Add_m1908758304(L_43, L_44, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_45 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_46 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_46, ((int32_t)1592), ((int32_t)65221), /*hidden argument*/NULL);
		NullCheck(L_45);
		List_1_Add_m1908758304(L_45, L_46, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_47 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_48 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_48, ((int32_t)1593), ((int32_t)65225), /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_Add_m1908758304(L_47, L_48, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_49 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_50 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_50, ((int32_t)1594), ((int32_t)65229), /*hidden argument*/NULL);
		NullCheck(L_49);
		List_1_Add_m1908758304(L_49, L_50, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_51 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_52 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_52, ((int32_t)1601), ((int32_t)65233), /*hidden argument*/NULL);
		NullCheck(L_51);
		List_1_Add_m1908758304(L_51, L_52, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_53 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_54 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_54, ((int32_t)1602), ((int32_t)65237), /*hidden argument*/NULL);
		NullCheck(L_53);
		List_1_Add_m1908758304(L_53, L_54, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_55 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_56 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_56, ((int32_t)1603), ((int32_t)65241), /*hidden argument*/NULL);
		NullCheck(L_55);
		List_1_Add_m1908758304(L_55, L_56, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_57 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_58 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_58, ((int32_t)1604), ((int32_t)65245), /*hidden argument*/NULL);
		NullCheck(L_57);
		List_1_Add_m1908758304(L_57, L_58, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_59 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_60 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_60, ((int32_t)1605), ((int32_t)65249), /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_Add_m1908758304(L_59, L_60, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_61 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_62 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_62, ((int32_t)1606), ((int32_t)65253), /*hidden argument*/NULL);
		NullCheck(L_61);
		List_1_Add_m1908758304(L_61, L_62, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_63 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_64 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_64, ((int32_t)1607), ((int32_t)65257), /*hidden argument*/NULL);
		NullCheck(L_63);
		List_1_Add_m1908758304(L_63, L_64, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_65 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_66 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_66, ((int32_t)1608), ((int32_t)65261), /*hidden argument*/NULL);
		NullCheck(L_65);
		List_1_Add_m1908758304(L_65, L_66, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_67 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_68 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_68, ((int32_t)1610), ((int32_t)65265), /*hidden argument*/NULL);
		NullCheck(L_67);
		List_1_Add_m1908758304(L_67, L_68, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_69 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_70 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_70, ((int32_t)1570), ((int32_t)65153), /*hidden argument*/NULL);
		NullCheck(L_69);
		List_1_Add_m1908758304(L_69, L_70, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_71 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_72 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_72, ((int32_t)1577), ((int32_t)65171), /*hidden argument*/NULL);
		NullCheck(L_71);
		List_1_Add_m1908758304(L_71, L_72, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_73 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_74 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_74, ((int32_t)1662), ((int32_t)64342), /*hidden argument*/NULL);
		NullCheck(L_73);
		List_1_Add_m1908758304(L_73, L_74, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_75 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_76 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_76, ((int32_t)1670), ((int32_t)64378), /*hidden argument*/NULL);
		NullCheck(L_75);
		List_1_Add_m1908758304(L_75, L_76, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_77 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_78 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_78, ((int32_t)1688), ((int32_t)64394), /*hidden argument*/NULL);
		NullCheck(L_77);
		List_1_Add_m1908758304(L_77, L_78, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		List_1_t796524086 * L_79 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		ArabicMapping_t3619416640 * L_80 = (ArabicMapping_t3619416640 *)il2cpp_codegen_object_new(ArabicMapping_t3619416640_il2cpp_TypeInfo_var);
		ArabicMapping__ctor_m2823911374(L_80, ((int32_t)1711), ((int32_t)64402), /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_Add_m1908758304(L_79, L_80, /*hidden argument*/List_1_Add_m1908758304_RuntimeMethod_var);
		return;
	}
}
// ArabicTable ArabicTable::get_ArabicMapper()
extern "C"  ArabicTable_t2015225755 * ArabicTable_get_ArabicMapper_m331737571 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicTable_get_ArabicMapper_m331737571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArabicTable_t2015225755 * V_0 = NULL;
	bool V_1 = false;
	{
		ArabicTable_t2015225755 * L_0 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_arabicMapper_1();
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(ArabicTable_t2015225755 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		ArabicTable_t2015225755 * L_2 = (ArabicTable_t2015225755 *)il2cpp_codegen_object_new(ArabicTable_t2015225755_il2cpp_TypeInfo_var);
		ArabicTable__ctor_m3313828430(L_2, /*hidden argument*/NULL);
		((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->set_arabicMapper_1(L_2);
	}

IL_001a:
	{
		ArabicTable_t2015225755 * L_3 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_arabicMapper_1();
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		ArabicTable_t2015225755 * L_4 = V_0;
		return L_4;
	}
}
// System.Int32 ArabicTable::Convert(System.Int32)
extern "C"  int32_t ArabicTable_Convert_m3785855912 (ArabicTable_t2015225755 * __this, int32_t ___toBeConverted0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArabicTable_Convert_m3785855912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArabicMapping_t3619416640 * V_0 = NULL;
	int32_t V_1 = 0;
	Enumerator_t2685767963  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t796524086 * L_0 = ((ArabicTable_t2015225755_StaticFields*)il2cpp_codegen_static_fields_for(ArabicTable_t2015225755_il2cpp_TypeInfo_var))->get_mapList_0();
		NullCheck(L_0);
		Enumerator_t2685767963  L_1 = List_1_GetEnumerator_m3336618809(L_0, /*hidden argument*/List_1_GetEnumerator_m3336618809_RuntimeMethod_var);
		V_2 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_000f:
		{
			ArabicMapping_t3619416640 * L_2 = Enumerator_get_Current_m2544204511((&V_2), /*hidden argument*/Enumerator_get_Current_m2544204511_RuntimeMethod_var);
			V_0 = L_2;
			ArabicMapping_t3619416640 * L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = L_3->get_from_0();
			int32_t L_5 = ___toBeConverted0;
			V_3 = (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_6 = V_3;
			if (L_6)
			{
				goto IL_0031;
			}
		}

IL_0027:
		{
			ArabicMapping_t3619416640 * L_7 = V_0;
			NullCheck(L_7);
			int32_t L_8 = L_7->get_to_1();
			V_1 = L_8;
			IL2CPP_LEAVE(0x52, FINALLY_003e);
		}

IL_0031:
		{
			bool L_9 = Enumerator_MoveNext_m2035344556((&V_2), /*hidden argument*/Enumerator_MoveNext_m2035344556_RuntimeMethod_var);
			V_3 = L_9;
			bool L_10 = V_3;
			if (L_10)
			{
				goto IL_000f;
			}
		}

IL_003c:
		{
			IL2CPP_LEAVE(0x4D, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3914768235((&V_2), /*hidden argument*/Enumerator_Dispose_m3914768235_RuntimeMethod_var);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004d:
	{
		int32_t L_11 = ___toBeConverted0;
		V_1 = L_11;
		goto IL_0052;
	}

IL_0052:
	{
		int32_t L_12 = V_1;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TashkeelLocation::.ctor(System.Char,System.Int32)
extern "C"  void TashkeelLocation__ctor_m1906920022 (TashkeelLocation_t3651416022 * __this, Il2CppChar ___tashkeel0, int32_t ___position1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Il2CppChar L_0 = ___tashkeel0;
		__this->set_tashkeel_0(L_0);
		int32_t L_1 = ___position1;
		__this->set_position_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
