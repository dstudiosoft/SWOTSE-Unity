﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackColonizeDestroyReportManager
struct AttackColonizeDestroyReportManager_t499208356;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackColonizeDestroyReportManager::.ctor()
extern "C"  void AttackColonizeDestroyReportManager__ctor_m1006120899 (AttackColonizeDestroyReportManager_t499208356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackColonizeDestroyReportManager::OnEnable()
extern "C"  void AttackColonizeDestroyReportManager_OnEnable_m2458612303 (AttackColonizeDestroyReportManager_t499208356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackColonizeDestroyReportManager::CloseReport()
extern "C"  void AttackColonizeDestroyReportManager_CloseReport_m1453130451 (AttackColonizeDestroyReportManager_t499208356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
