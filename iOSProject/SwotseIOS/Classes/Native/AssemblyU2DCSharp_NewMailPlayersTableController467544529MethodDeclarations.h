﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewMailPlayersTableController
struct NewMailPlayersTableController_t467544529;
// SimpleEvent
struct SimpleEvent_t1509017202;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UserModel
struct UserModel_t3025569216;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_UserModel3025569216.h"

// System.Void NewMailPlayersTableController::.ctor()
extern "C"  void NewMailPlayersTableController__ctor_m1780918414 (NewMailPlayersTableController_t467544529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayersTableController::add_onGetAllUsers(SimpleEvent)
extern "C"  void NewMailPlayersTableController_add_onGetAllUsers_m277396524 (NewMailPlayersTableController_t467544529 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayersTableController::remove_onGetAllUsers(SimpleEvent)
extern "C"  void NewMailPlayersTableController_remove_onGetAllUsers_m588230587 (NewMailPlayersTableController_t467544529 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayersTableController::Start()
extern "C"  void NewMailPlayersTableController_Start_m4224198934 (NewMailPlayersTableController_t467544529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NewMailPlayersTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t NewMailPlayersTableController_GetNumberOfRowsForTableView_m3162257044 (NewMailPlayersTableController_t467544529 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NewMailPlayersTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float NewMailPlayersTableController_GetHeightForRowInTableView_m1079079236 (NewMailPlayersTableController_t467544529 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell NewMailPlayersTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * NewMailPlayersTableController_GetCellForRowInTableView_m120439981 (NewMailPlayersTableController_t467544529 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NewMailPlayersTableController::GetAllUsers()
extern "C"  Il2CppObject * NewMailPlayersTableController_GetAllUsers_m3977747679 (NewMailPlayersTableController_t467544529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayersTableController::GotAllUsers(System.Object,System.String)
extern "C"  void NewMailPlayersTableController_GotAllUsers_m3002632107 (NewMailPlayersTableController_t467544529 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailPlayersTableController::RemoveUser(UserModel)
extern "C"  void NewMailPlayersTableController_RemoveUser_m961722013 (NewMailPlayersTableController_t467544529 * __this, UserModel_t3025569216 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
