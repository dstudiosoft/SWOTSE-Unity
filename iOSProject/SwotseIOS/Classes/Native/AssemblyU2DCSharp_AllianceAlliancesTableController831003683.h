﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceAlliancesLine
struct AllianceAlliancesLine_t4177759463;
// Tacticsoft.TableView
struct TableView_t3179510217;
// AllianceModel[]
struct AllianceModelU5BU5D_t1718348683;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceAlliancesTableController
struct  AllianceAlliancesTableController_t831003683  : public MonoBehaviour_t1158329972
{
public:
	// AllianceAlliancesLine AllianceAlliancesTableController::m_cellPrefab
	AllianceAlliancesLine_t4177759463 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceAlliancesTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// AllianceModel[] AllianceAlliancesTableController::alliances
	AllianceModelU5BU5D_t1718348683* ___alliances_4;
	// System.Int32 AllianceAlliancesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t831003683, ___m_cellPrefab_2)); }
	inline AllianceAlliancesLine_t4177759463 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceAlliancesLine_t4177759463 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceAlliancesLine_t4177759463 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t831003683, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_alliances_4() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t831003683, ___alliances_4)); }
	inline AllianceModelU5BU5D_t1718348683* get_alliances_4() const { return ___alliances_4; }
	inline AllianceModelU5BU5D_t1718348683** get_address_of_alliances_4() { return &___alliances_4; }
	inline void set_alliances_4(AllianceModelU5BU5D_t1718348683* value)
	{
		___alliances_4 = value;
		Il2CppCodeGenWriteBarrier(&___alliances_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t831003683, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
