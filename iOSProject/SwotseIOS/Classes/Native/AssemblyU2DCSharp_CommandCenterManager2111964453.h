﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterManager
struct  CommandCenterManager_t2111964453  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text CommandCenterManager::cityWorker
	Text_t356221433 * ___cityWorker_2;
	// UnityEngine.UI.Text CommandCenterManager::citySpy
	Text_t356221433 * ___citySpy_3;
	// UnityEngine.UI.Text CommandCenterManager::citySwordsman
	Text_t356221433 * ___citySwordsman_4;
	// UnityEngine.UI.Text CommandCenterManager::citySpearman
	Text_t356221433 * ___citySpearman_5;
	// UnityEngine.UI.Text CommandCenterManager::cityPikeman
	Text_t356221433 * ___cityPikeman_6;
	// UnityEngine.UI.Text CommandCenterManager::cityScoutRider
	Text_t356221433 * ___cityScoutRider_7;
	// UnityEngine.UI.Text CommandCenterManager::cityLightCavalry
	Text_t356221433 * ___cityLightCavalry_8;
	// UnityEngine.UI.Text CommandCenterManager::cityHeavyCavalry
	Text_t356221433 * ___cityHeavyCavalry_9;
	// UnityEngine.UI.Text CommandCenterManager::cityArcher
	Text_t356221433 * ___cityArcher_10;
	// UnityEngine.UI.Text CommandCenterManager::cityArcherRider
	Text_t356221433 * ___cityArcherRider_11;
	// UnityEngine.UI.Text CommandCenterManager::cityHollyman
	Text_t356221433 * ___cityHollyman_12;
	// UnityEngine.UI.Text CommandCenterManager::cityWagon
	Text_t356221433 * ___cityWagon_13;
	// UnityEngine.UI.Text CommandCenterManager::cityTrebuchet
	Text_t356221433 * ___cityTrebuchet_14;
	// UnityEngine.UI.Text CommandCenterManager::citySiegeTower
	Text_t356221433 * ___citySiegeTower_15;
	// UnityEngine.UI.Text CommandCenterManager::cityBatteringRam
	Text_t356221433 * ___cityBatteringRam_16;
	// UnityEngine.UI.Text CommandCenterManager::cityBallista
	Text_t356221433 * ___cityBallista_17;

public:
	inline static int32_t get_offset_of_cityWorker_2() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityWorker_2)); }
	inline Text_t356221433 * get_cityWorker_2() const { return ___cityWorker_2; }
	inline Text_t356221433 ** get_address_of_cityWorker_2() { return &___cityWorker_2; }
	inline void set_cityWorker_2(Text_t356221433 * value)
	{
		___cityWorker_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorker_2, value);
	}

	inline static int32_t get_offset_of_citySpy_3() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___citySpy_3)); }
	inline Text_t356221433 * get_citySpy_3() const { return ___citySpy_3; }
	inline Text_t356221433 ** get_address_of_citySpy_3() { return &___citySpy_3; }
	inline void set_citySpy_3(Text_t356221433 * value)
	{
		___citySpy_3 = value;
		Il2CppCodeGenWriteBarrier(&___citySpy_3, value);
	}

	inline static int32_t get_offset_of_citySwordsman_4() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___citySwordsman_4)); }
	inline Text_t356221433 * get_citySwordsman_4() const { return ___citySwordsman_4; }
	inline Text_t356221433 ** get_address_of_citySwordsman_4() { return &___citySwordsman_4; }
	inline void set_citySwordsman_4(Text_t356221433 * value)
	{
		___citySwordsman_4 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsman_4, value);
	}

	inline static int32_t get_offset_of_citySpearman_5() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___citySpearman_5)); }
	inline Text_t356221433 * get_citySpearman_5() const { return ___citySpearman_5; }
	inline Text_t356221433 ** get_address_of_citySpearman_5() { return &___citySpearman_5; }
	inline void set_citySpearman_5(Text_t356221433 * value)
	{
		___citySpearman_5 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearman_5, value);
	}

	inline static int32_t get_offset_of_cityPikeman_6() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityPikeman_6)); }
	inline Text_t356221433 * get_cityPikeman_6() const { return ___cityPikeman_6; }
	inline Text_t356221433 ** get_address_of_cityPikeman_6() { return &___cityPikeman_6; }
	inline void set_cityPikeman_6(Text_t356221433 * value)
	{
		___cityPikeman_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikeman_6, value);
	}

	inline static int32_t get_offset_of_cityScoutRider_7() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityScoutRider_7)); }
	inline Text_t356221433 * get_cityScoutRider_7() const { return ___cityScoutRider_7; }
	inline Text_t356221433 ** get_address_of_cityScoutRider_7() { return &___cityScoutRider_7; }
	inline void set_cityScoutRider_7(Text_t356221433 * value)
	{
		___cityScoutRider_7 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRider_7, value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_8() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityLightCavalry_8)); }
	inline Text_t356221433 * get_cityLightCavalry_8() const { return ___cityLightCavalry_8; }
	inline Text_t356221433 ** get_address_of_cityLightCavalry_8() { return &___cityLightCavalry_8; }
	inline void set_cityLightCavalry_8(Text_t356221433 * value)
	{
		___cityLightCavalry_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalry_8, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_9() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityHeavyCavalry_9)); }
	inline Text_t356221433 * get_cityHeavyCavalry_9() const { return ___cityHeavyCavalry_9; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalry_9() { return &___cityHeavyCavalry_9; }
	inline void set_cityHeavyCavalry_9(Text_t356221433 * value)
	{
		___cityHeavyCavalry_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalry_9, value);
	}

	inline static int32_t get_offset_of_cityArcher_10() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityArcher_10)); }
	inline Text_t356221433 * get_cityArcher_10() const { return ___cityArcher_10; }
	inline Text_t356221433 ** get_address_of_cityArcher_10() { return &___cityArcher_10; }
	inline void set_cityArcher_10(Text_t356221433 * value)
	{
		___cityArcher_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcher_10, value);
	}

	inline static int32_t get_offset_of_cityArcherRider_11() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityArcherRider_11)); }
	inline Text_t356221433 * get_cityArcherRider_11() const { return ___cityArcherRider_11; }
	inline Text_t356221433 ** get_address_of_cityArcherRider_11() { return &___cityArcherRider_11; }
	inline void set_cityArcherRider_11(Text_t356221433 * value)
	{
		___cityArcherRider_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRider_11, value);
	}

	inline static int32_t get_offset_of_cityHollyman_12() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityHollyman_12)); }
	inline Text_t356221433 * get_cityHollyman_12() const { return ___cityHollyman_12; }
	inline Text_t356221433 ** get_address_of_cityHollyman_12() { return &___cityHollyman_12; }
	inline void set_cityHollyman_12(Text_t356221433 * value)
	{
		___cityHollyman_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollyman_12, value);
	}

	inline static int32_t get_offset_of_cityWagon_13() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityWagon_13)); }
	inline Text_t356221433 * get_cityWagon_13() const { return ___cityWagon_13; }
	inline Text_t356221433 ** get_address_of_cityWagon_13() { return &___cityWagon_13; }
	inline void set_cityWagon_13(Text_t356221433 * value)
	{
		___cityWagon_13 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagon_13, value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_14() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityTrebuchet_14)); }
	inline Text_t356221433 * get_cityTrebuchet_14() const { return ___cityTrebuchet_14; }
	inline Text_t356221433 ** get_address_of_cityTrebuchet_14() { return &___cityTrebuchet_14; }
	inline void set_cityTrebuchet_14(Text_t356221433 * value)
	{
		___cityTrebuchet_14 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchet_14, value);
	}

	inline static int32_t get_offset_of_citySiegeTower_15() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___citySiegeTower_15)); }
	inline Text_t356221433 * get_citySiegeTower_15() const { return ___citySiegeTower_15; }
	inline Text_t356221433 ** get_address_of_citySiegeTower_15() { return &___citySiegeTower_15; }
	inline void set_citySiegeTower_15(Text_t356221433 * value)
	{
		___citySiegeTower_15 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTower_15, value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_16() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityBatteringRam_16)); }
	inline Text_t356221433 * get_cityBatteringRam_16() const { return ___cityBatteringRam_16; }
	inline Text_t356221433 ** get_address_of_cityBatteringRam_16() { return &___cityBatteringRam_16; }
	inline void set_cityBatteringRam_16(Text_t356221433 * value)
	{
		___cityBatteringRam_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRam_16, value);
	}

	inline static int32_t get_offset_of_cityBallista_17() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2111964453, ___cityBallista_17)); }
	inline Text_t356221433 * get_cityBallista_17() const { return ___cityBallista_17; }
	inline Text_t356221433 ** get_address_of_cityBallista_17() { return &___cityBallista_17; }
	inline void set_cityBallista_17(Text_t356221433 * value)
	{
		___cityBallista_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallista_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
