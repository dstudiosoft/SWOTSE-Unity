﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// FinanceTableController
struct FinanceTableController_t1083841530;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger
struct  FinancialContentChanger_t2753898366  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text FinancialContentChanger::contentLabel
	Text_t356221433 * ___contentLabel_2;
	// UnityEngine.UI.Text FinancialContentChanger::shillingsCount
	Text_t356221433 * ___shillingsCount_3;
	// UnityEngine.UI.Text FinancialContentChanger::totalPurchases
	Text_t356221433 * ___totalPurchases_4;
	// UnityEngine.UI.Text FinancialContentChanger::totalLabel
	Text_t356221433 * ___totalLabel_5;
	// UnityEngine.UI.Text FinancialContentChanger::tableColumn
	Text_t356221433 * ___tableColumn_6;
	// UnityEngine.Sprite FinancialContentChanger::tabSelectedSprite
	Sprite_t309593783 * ___tabSelectedSprite_7;
	// UnityEngine.Sprite FinancialContentChanger::tabUnselectedSprite
	Sprite_t309593783 * ___tabUnselectedSprite_8;
	// UnityEngine.UI.Image[] FinancialContentChanger::tabs
	ImageU5BU5D_t590162004* ___tabs_9;
	// System.Collections.ArrayList FinancialContentChanger::boughtItems
	ArrayList_t4252133567 * ___boughtItems_10;
	// System.Collections.ArrayList FinancialContentChanger::soldItems
	ArrayList_t4252133567 * ___soldItems_11;
	// System.Collections.ArrayList FinancialContentChanger::offeredItems
	ArrayList_t4252133567 * ___offeredItems_12;
	// FinanceTableController FinancialContentChanger::tableController
	FinanceTableController_t1083841530 * ___tableController_13;
	// System.String FinancialContentChanger::content
	String_t* ___content_14;
	// SimpleEvent FinancialContentChanger::gotOffers
	SimpleEvent_t1509017202 * ___gotOffers_15;
	// SimpleEvent FinancialContentChanger::offerCanceled
	SimpleEvent_t1509017202 * ___offerCanceled_16;

public:
	inline static int32_t get_offset_of_contentLabel_2() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___contentLabel_2)); }
	inline Text_t356221433 * get_contentLabel_2() const { return ___contentLabel_2; }
	inline Text_t356221433 ** get_address_of_contentLabel_2() { return &___contentLabel_2; }
	inline void set_contentLabel_2(Text_t356221433 * value)
	{
		___contentLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentLabel_2, value);
	}

	inline static int32_t get_offset_of_shillingsCount_3() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___shillingsCount_3)); }
	inline Text_t356221433 * get_shillingsCount_3() const { return ___shillingsCount_3; }
	inline Text_t356221433 ** get_address_of_shillingsCount_3() { return &___shillingsCount_3; }
	inline void set_shillingsCount_3(Text_t356221433 * value)
	{
		___shillingsCount_3 = value;
		Il2CppCodeGenWriteBarrier(&___shillingsCount_3, value);
	}

	inline static int32_t get_offset_of_totalPurchases_4() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___totalPurchases_4)); }
	inline Text_t356221433 * get_totalPurchases_4() const { return ___totalPurchases_4; }
	inline Text_t356221433 ** get_address_of_totalPurchases_4() { return &___totalPurchases_4; }
	inline void set_totalPurchases_4(Text_t356221433 * value)
	{
		___totalPurchases_4 = value;
		Il2CppCodeGenWriteBarrier(&___totalPurchases_4, value);
	}

	inline static int32_t get_offset_of_totalLabel_5() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___totalLabel_5)); }
	inline Text_t356221433 * get_totalLabel_5() const { return ___totalLabel_5; }
	inline Text_t356221433 ** get_address_of_totalLabel_5() { return &___totalLabel_5; }
	inline void set_totalLabel_5(Text_t356221433 * value)
	{
		___totalLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___totalLabel_5, value);
	}

	inline static int32_t get_offset_of_tableColumn_6() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___tableColumn_6)); }
	inline Text_t356221433 * get_tableColumn_6() const { return ___tableColumn_6; }
	inline Text_t356221433 ** get_address_of_tableColumn_6() { return &___tableColumn_6; }
	inline void set_tableColumn_6(Text_t356221433 * value)
	{
		___tableColumn_6 = value;
		Il2CppCodeGenWriteBarrier(&___tableColumn_6, value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_7() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___tabSelectedSprite_7)); }
	inline Sprite_t309593783 * get_tabSelectedSprite_7() const { return ___tabSelectedSprite_7; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedSprite_7() { return &___tabSelectedSprite_7; }
	inline void set_tabSelectedSprite_7(Sprite_t309593783 * value)
	{
		___tabSelectedSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedSprite_7, value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_8() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___tabUnselectedSprite_8)); }
	inline Sprite_t309593783 * get_tabUnselectedSprite_8() const { return ___tabUnselectedSprite_8; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedSprite_8() { return &___tabUnselectedSprite_8; }
	inline void set_tabUnselectedSprite_8(Sprite_t309593783 * value)
	{
		___tabUnselectedSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedSprite_8, value);
	}

	inline static int32_t get_offset_of_tabs_9() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___tabs_9)); }
	inline ImageU5BU5D_t590162004* get_tabs_9() const { return ___tabs_9; }
	inline ImageU5BU5D_t590162004** get_address_of_tabs_9() { return &___tabs_9; }
	inline void set_tabs_9(ImageU5BU5D_t590162004* value)
	{
		___tabs_9 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_9, value);
	}

	inline static int32_t get_offset_of_boughtItems_10() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___boughtItems_10)); }
	inline ArrayList_t4252133567 * get_boughtItems_10() const { return ___boughtItems_10; }
	inline ArrayList_t4252133567 ** get_address_of_boughtItems_10() { return &___boughtItems_10; }
	inline void set_boughtItems_10(ArrayList_t4252133567 * value)
	{
		___boughtItems_10 = value;
		Il2CppCodeGenWriteBarrier(&___boughtItems_10, value);
	}

	inline static int32_t get_offset_of_soldItems_11() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___soldItems_11)); }
	inline ArrayList_t4252133567 * get_soldItems_11() const { return ___soldItems_11; }
	inline ArrayList_t4252133567 ** get_address_of_soldItems_11() { return &___soldItems_11; }
	inline void set_soldItems_11(ArrayList_t4252133567 * value)
	{
		___soldItems_11 = value;
		Il2CppCodeGenWriteBarrier(&___soldItems_11, value);
	}

	inline static int32_t get_offset_of_offeredItems_12() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___offeredItems_12)); }
	inline ArrayList_t4252133567 * get_offeredItems_12() const { return ___offeredItems_12; }
	inline ArrayList_t4252133567 ** get_address_of_offeredItems_12() { return &___offeredItems_12; }
	inline void set_offeredItems_12(ArrayList_t4252133567 * value)
	{
		___offeredItems_12 = value;
		Il2CppCodeGenWriteBarrier(&___offeredItems_12, value);
	}

	inline static int32_t get_offset_of_tableController_13() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___tableController_13)); }
	inline FinanceTableController_t1083841530 * get_tableController_13() const { return ___tableController_13; }
	inline FinanceTableController_t1083841530 ** get_address_of_tableController_13() { return &___tableController_13; }
	inline void set_tableController_13(FinanceTableController_t1083841530 * value)
	{
		___tableController_13 = value;
		Il2CppCodeGenWriteBarrier(&___tableController_13, value);
	}

	inline static int32_t get_offset_of_content_14() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___content_14)); }
	inline String_t* get_content_14() const { return ___content_14; }
	inline String_t** get_address_of_content_14() { return &___content_14; }
	inline void set_content_14(String_t* value)
	{
		___content_14 = value;
		Il2CppCodeGenWriteBarrier(&___content_14, value);
	}

	inline static int32_t get_offset_of_gotOffers_15() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___gotOffers_15)); }
	inline SimpleEvent_t1509017202 * get_gotOffers_15() const { return ___gotOffers_15; }
	inline SimpleEvent_t1509017202 ** get_address_of_gotOffers_15() { return &___gotOffers_15; }
	inline void set_gotOffers_15(SimpleEvent_t1509017202 * value)
	{
		___gotOffers_15 = value;
		Il2CppCodeGenWriteBarrier(&___gotOffers_15, value);
	}

	inline static int32_t get_offset_of_offerCanceled_16() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366, ___offerCanceled_16)); }
	inline SimpleEvent_t1509017202 * get_offerCanceled_16() const { return ___offerCanceled_16; }
	inline SimpleEvent_t1509017202 ** get_address_of_offerCanceled_16() { return &___offerCanceled_16; }
	inline void set_offerCanceled_16(SimpleEvent_t1509017202 * value)
	{
		___offerCanceled_16 = value;
		Il2CppCodeGenWriteBarrier(&___offerCanceled_16, value);
	}
};

struct FinancialContentChanger_t2753898366_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> FinancialContentChanger::<>f__switch$map8
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map8_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_17() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t2753898366_StaticFields, ___U3CU3Ef__switchU24map8_17)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map8_17() const { return ___U3CU3Ef__switchU24map8_17; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map8_17() { return &___U3CU3Ef__switchU24map8_17; }
	inline void set_U3CU3Ef__switchU24map8_17(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map8_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
