﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// TouchScript.Gestures.UI.UIGesture/TouchData
struct TouchData_t3880599975;
struct TouchData_t3880599975_marshaled_pinvoke;
struct TouchData_t3880599975_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void TouchScript.Gestures.UI.UIGesture/TouchData::.ctor(System.Boolean,UnityEngine.EventSystems.PointerEventData)
extern "C"  void TouchData__ctor_m323615977 (TouchData_t3880599975 * __this, bool ___onTarget0, PointerEventData_t1599784723 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TouchData_t3880599975;
struct TouchData_t3880599975_marshaled_pinvoke;

extern "C" void TouchData_t3880599975_marshal_pinvoke(const TouchData_t3880599975& unmarshaled, TouchData_t3880599975_marshaled_pinvoke& marshaled);
extern "C" void TouchData_t3880599975_marshal_pinvoke_back(const TouchData_t3880599975_marshaled_pinvoke& marshaled, TouchData_t3880599975& unmarshaled);
extern "C" void TouchData_t3880599975_marshal_pinvoke_cleanup(TouchData_t3880599975_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TouchData_t3880599975;
struct TouchData_t3880599975_marshaled_com;

extern "C" void TouchData_t3880599975_marshal_com(const TouchData_t3880599975& unmarshaled, TouchData_t3880599975_marshaled_com& marshaled);
extern "C" void TouchData_t3880599975_marshal_com_back(const TouchData_t3880599975_marshaled_com& marshaled, TouchData_t3880599975& unmarshaled);
extern "C" void TouchData_t3880599975_marshal_com_cleanup(TouchData_t3880599975_marshaled_com& marshaled);
