﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.HostProtectionException
struct HostProtectionException_t582572462;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Security_Permissions_HostProtectio3100089779.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void System.Security.HostProtectionException::.ctor()
extern "C"  void HostProtectionException__ctor_m4205338195 (HostProtectionException_t582572462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String)
extern "C"  void HostProtectionException__ctor_m104056793 (HostProtectionException_t582572462 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String,System.Exception)
extern "C"  void HostProtectionException__ctor_m3650685009 (HostProtectionException_t582572462 * __this, String_t* ___message0, Exception_t1927440687 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String,System.Security.Permissions.HostProtectionResource,System.Security.Permissions.HostProtectionResource)
extern "C"  void HostProtectionException__ctor_m1191489753 (HostProtectionException_t582572462 * __this, String_t* ___message0, int32_t ___protectedResources1, int32_t ___demandedResources2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HostProtectionException__ctor_m3800622000 (HostProtectionException_t582572462 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::get_DemandedResources()
extern "C"  int32_t HostProtectionException_get_DemandedResources_m3503741822 (HostProtectionException_t582572462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::get_ProtectedResources()
extern "C"  int32_t HostProtectionException_get_ProtectedResources_m467892170 (HostProtectionException_t582572462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HostProtectionException_GetObjectData_m3630109683 (HostProtectionException_t582572462 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.HostProtectionException::ToString()
extern "C"  String_t* HostProtectionException_ToString_m284764948 (HostProtectionException_t582572462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
