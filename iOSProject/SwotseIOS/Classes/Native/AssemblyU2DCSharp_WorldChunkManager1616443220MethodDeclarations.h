﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldChunkManager
struct WorldChunkManager_t1616443220;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void WorldChunkManager::.ctor()
extern "C"  void WorldChunkManager__ctor_m3001480573 (WorldChunkManager_t1616443220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::add_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_add_onGetChunkInfo_m2191456819 (WorldChunkManager_t1616443220 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::remove_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_remove_onGetChunkInfo_m3288486254 (WorldChunkManager_t1616443220 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WorldChunkManager::GetChunkInfo()
extern "C"  Il2CppObject * WorldChunkManager_GetChunkInfo_m297663954 (WorldChunkManager_t1616443220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::InitChunkFields(System.Object,System.String)
extern "C"  void WorldChunkManager_InitChunkFields_m4069471765 (WorldChunkManager_t1616443220 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::ResetFieldsState()
extern "C"  void WorldChunkManager_ResetFieldsState_m3390329600 (WorldChunkManager_t1616443220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
