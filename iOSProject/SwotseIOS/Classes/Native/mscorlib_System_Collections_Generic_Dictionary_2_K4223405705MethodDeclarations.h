﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct KeyCollection_t4223405705;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke134444076.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m266462330_gshared (KeyCollection_t4223405705 * __this, Dictionary_2_t1739907934 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m266462330(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4223405705 *, Dictionary_2_t1739907934 *, const MethodInfo*))KeyCollection__ctor_m266462330_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125106540_gshared (KeyCollection_t4223405705 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125106540(__this, ___item0, method) ((  void (*) (KeyCollection_t4223405705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125106540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3531311795_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3531311795(__this, method) ((  void (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3531311795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1633218164_gshared (KeyCollection_t4223405705 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1633218164(__this, ___item0, method) ((  bool (*) (KeyCollection_t4223405705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1633218164_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m652486395_gshared (KeyCollection_t4223405705 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m652486395(__this, ___item0, method) ((  bool (*) (KeyCollection_t4223405705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m652486395_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17006617_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17006617(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17006617_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1362056133_gshared (KeyCollection_t4223405705 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1362056133(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4223405705 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1362056133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3909030988_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3909030988(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3909030988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2098537743_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2098537743(__this, method) ((  bool (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2098537743_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3635540313_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3635540313(__this, method) ((  bool (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3635540313_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m179421077_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m179421077(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m179421077_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1063993091_gshared (KeyCollection_t4223405705 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1063993091(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4223405705 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1063993091_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::GetEnumerator()
extern "C"  Enumerator_t134444076  KeyCollection_GetEnumerator_m4203070_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m4203070(__this, method) ((  Enumerator_t134444076  (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_GetEnumerator_m4203070_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2395491353_gshared (KeyCollection_t4223405705 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2395491353(__this, method) ((  int32_t (*) (KeyCollection_t4223405705 *, const MethodInfo*))KeyCollection_get_Count_m2395491353_gshared)(__this, method)
