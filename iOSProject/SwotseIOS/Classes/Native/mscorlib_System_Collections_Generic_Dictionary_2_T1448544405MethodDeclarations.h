﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,InviteReportModel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m849214475(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1448544405 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3373533409_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,InviteReportModel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2159252367(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1448544405 *, int64_t, InviteReportModel_t3681483684 *, const MethodInfo*))Transform_1_Invoke_m1299483797_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,InviteReportModel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2195519620(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1448544405 *, int64_t, InviteReportModel_t3681483684 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3448776694_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,InviteReportModel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m393888269(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1448544405 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3006940059_gshared)(__this, ___result0, method)
