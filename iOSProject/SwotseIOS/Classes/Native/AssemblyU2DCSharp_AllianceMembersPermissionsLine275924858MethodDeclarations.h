﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceMembersPermissionsLine
struct AllianceMembersPermissionsLine_t275924858;
// UserModel
struct UserModel_t3025569216;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UserModel3025569216.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceMembersPermissionsLine::.ctor()
extern "C"  void AllianceMembersPermissionsLine__ctor_m775463395 (AllianceMembersPermissionsLine_t275924858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetUser(UserModel)
extern "C"  void AllianceMembersPermissionsLine_SetUser_m3197573560 (AllianceMembersPermissionsLine_t275924858 * __this, UserModel_t3025569216 * ___userValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetMemberName(System.String)
extern "C"  void AllianceMembersPermissionsLine_SetMemberName_m2944575102 (AllianceMembersPermissionsLine_t275924858 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetPosition(System.String)
extern "C"  void AllianceMembersPermissionsLine_SetPosition_m2873282472 (AllianceMembersPermissionsLine_t275924858 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetRank(System.String)
extern "C"  void AllianceMembersPermissionsLine_SetRank_m2910583153 (AllianceMembersPermissionsLine_t275924858 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetExperience(System.Int64)
extern "C"  void AllianceMembersPermissionsLine_SetExperience_m1192135465 (AllianceMembersPermissionsLine_t275924858 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetCitiesCount(System.Int64)
extern "C"  void AllianceMembersPermissionsLine_SetCitiesCount_m758710209 (AllianceMembersPermissionsLine_t275924858 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetCapitalName(System.String)
extern "C"  void AllianceMembersPermissionsLine_SetCapitalName_m3797037684 (AllianceMembersPermissionsLine_t275924858 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::SetLastOnline(System.String)
extern "C"  void AllianceMembersPermissionsLine_SetLastOnline_m657450588 (AllianceMembersPermissionsLine_t275924858 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermissionsLine::ShowMemberInfo()
extern "C"  void AllianceMembersPermissionsLine_ShowMemberInfo_m305972746 (AllianceMembersPermissionsLine_t275924858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
