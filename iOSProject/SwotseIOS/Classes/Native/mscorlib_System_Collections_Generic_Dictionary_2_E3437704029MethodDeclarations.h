﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m457722949(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3437704029 *, Dictionary_2_t2117679327 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3712745426(__this, method) ((  Il2CppObject * (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m675202732(__this, method) ((  void (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m179884791(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2183879432(__this, method) ((  Il2CppObject * (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1332525726(__this, method) ((  Il2CppObject * (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::MoveNext()
#define Enumerator_MoveNext_m295163107(__this, method) ((  bool (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::get_Current()
#define Enumerator_get_Current_m3491019298(__this, method) ((  KeyValuePair_2_t4169991845  (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m89636041(__this, method) ((  TuioCursor_t1850351419 * (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3782886777(__this, method) ((  TouchPoint_t959629083 * (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::Reset()
#define Enumerator_Reset_m1335067183(__this, method) ((  void (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::VerifyState()
#define Enumerator_VerifyState_m2059957148(__this, method) ((  void (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m98834068(__this, method) ((  void (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>::Dispose()
#define Enumerator_Dispose_m1684116093(__this, method) ((  void (*) (Enumerator_t3437704029 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
