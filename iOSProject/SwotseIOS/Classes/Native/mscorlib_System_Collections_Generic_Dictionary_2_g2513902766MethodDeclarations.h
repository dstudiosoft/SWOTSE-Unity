﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t1716693387;
// System.Collections.Generic.IDictionary`2<System.IntPtr,System.Int32>
struct IDictionary_2_t512986187;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2723936573;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>
struct IEnumerator_1_t2041739111;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>
struct KeyCollection_t702433241;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>
struct ValueCollection_t1216962609;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_271247988.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3833927468.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m186159185_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m186159185(__this, method) ((  void (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2__ctor_m186159185_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m698622435_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m698622435(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m698622435_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3293372376_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3293372376(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3293372376_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2084775085_gshared (Dictionary_2_t2513902766 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2084775085(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2513902766 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2084775085_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1216003821_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1216003821(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1216003821_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3715647522_gshared (Dictionary_2_t2513902766 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3715647522(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2513902766 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3715647522_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m805989651_gshared (Dictionary_2_t2513902766 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m805989651(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2513902766 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m805989651_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1041184992_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1041184992(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1041184992_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1760938102_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1760938102(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1760938102_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m27959663_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m27959663(__this, method) ((  bool (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m27959663_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3854915106_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3854915106(__this, method) ((  bool (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3854915106_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1193549048_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1193549048(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1193549048_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m202343163_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m202343163(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m202343163_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m486431808_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m486431808(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m486431808_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1641859472_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1641859472(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1641859472_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1349944503_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1349944503(__this, ___key0, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1349944503_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3353677118_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3353677118(__this, method) ((  bool (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3353677118_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1632914392_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1632914392(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1632914392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m168307736_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m168307736(__this, method) ((  bool (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m168307736_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3585148715_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2_t271247988  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3585148715(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2513902766 *, KeyValuePair_2_t271247988 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3585148715_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1589023041_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2_t271247988  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1589023041(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2513902766 *, KeyValuePair_2_t271247988 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1589023041_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2964885711_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2U5BU5D_t2723936573* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2964885711(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2513902766 *, KeyValuePair_2U5BU5D_t2723936573*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2964885711_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2844689242_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2_t271247988  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2844689242(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2513902766 *, KeyValuePair_2_t271247988 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2844689242_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3497237550_gshared (Dictionary_2_t2513902766 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3497237550(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3497237550_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m160099913_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m160099913(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m160099913_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1633406570_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1633406570(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1633406570_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2433180535_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2433180535(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2433180535_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3735532354_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3735532354(__this, method) ((  int32_t (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_get_Count_m3735532354_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m3823539671_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3823539671(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2513902766 *, IntPtr_t, const MethodInfo*))Dictionary_2_get_Item_m3823539671_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1952206578_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1952206578(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2513902766 *, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1952206578_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1950053162_gshared (Dictionary_2_t2513902766 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1950053162(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2513902766 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1950053162_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2609855733_gshared (Dictionary_2_t2513902766 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2609855733(__this, ___size0, method) ((  void (*) (Dictionary_2_t2513902766 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2609855733_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2683203999_gshared (Dictionary_2_t2513902766 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2683203999(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2683203999_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t271247988  Dictionary_2_make_pair_m3155688233_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3155688233(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t271247988  (*) (Il2CppObject * /* static, unused */, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m3155688233_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::pick_key(TKey,TValue)
extern "C"  IntPtr_t Dictionary_2_pick_key_m2962658517_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2962658517(__this /* static, unused */, ___key0, ___value1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m2962658517_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m3428108981_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3428108981(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m3428108981_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3551625504_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2U5BU5D_t2723936573* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3551625504(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2513902766 *, KeyValuePair_2U5BU5D_t2723936573*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3551625504_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m704132344_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m704132344(__this, method) ((  void (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_Resize_m704132344_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1944108841_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1944108841(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2513902766 *, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_Add_m1944108841_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1614755397_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1614755397(__this, method) ((  void (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_Clear_m1614755397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3317648662_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3317648662(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2513902766 *, IntPtr_t, const MethodInfo*))Dictionary_2_ContainsKey_m3317648662_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m4214728991_gshared (Dictionary_2_t2513902766 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m4214728991(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2513902766 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m4214728991_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1960016498_gshared (Dictionary_2_t2513902766 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1960016498(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2513902766 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1960016498_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m820841708_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m820841708(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m820841708_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m4050138034_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m4050138034(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2513902766 *, IntPtr_t, const MethodInfo*))Dictionary_2_Remove_m4050138034_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4186983950_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4186983950(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2513902766 *, IntPtr_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m4186983950_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Keys()
extern "C"  KeyCollection_t702433241 * Dictionary_2_get_Keys_m2441631547_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2441631547(__this, method) ((  KeyCollection_t702433241 * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_get_Keys_m2441631547_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Values()
extern "C"  ValueCollection_t1216962609 * Dictionary_2_get_Values_m3276982075_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3276982075(__this, method) ((  ValueCollection_t1216962609 * (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_get_Values_m3276982075_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::ToTKey(System.Object)
extern "C"  IntPtr_t Dictionary_2_ToTKey_m2350234490_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2350234490(__this, ___key0, method) ((  IntPtr_t (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2350234490_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m1100686074_gshared (Dictionary_2_t2513902766 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1100686074(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2513902766 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1100686074_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m377389976_gshared (Dictionary_2_t2513902766 * __this, KeyValuePair_2_t271247988  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m377389976(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2513902766 *, KeyValuePair_2_t271247988 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m377389976_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3833927468  Dictionary_2_GetEnumerator_m327749289_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m327749289(__this, method) ((  Enumerator_t3833927468  (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2_GetEnumerator_m327749289_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2192742688_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2192742688(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2192742688_gshared)(__this /* static, unused */, ___key0, ___value1, method)
