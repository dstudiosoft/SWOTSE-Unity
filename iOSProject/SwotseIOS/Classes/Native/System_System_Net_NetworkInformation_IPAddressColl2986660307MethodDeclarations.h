﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IPAddressCollection
struct IPAddressCollection_t2986660307;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Collections.Generic.IEnumerator`1<System.Net.IPAddress>
struct IEnumerator_1_t3170462846;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"

// System.Void System.Net.NetworkInformation.IPAddressCollection::.ctor()
extern "C"  void IPAddressCollection__ctor_m3768432617 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.NetworkInformation.IPAddressCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * IPAddressCollection_System_Collections_IEnumerable_GetEnumerator_m1976498550 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.IPAddressCollection::SetReadOnly()
extern "C"  void IPAddressCollection_SetReadOnly_m3648147413 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.IPAddressCollection::Add(System.Net.IPAddress)
extern "C"  void IPAddressCollection_Add_m1453492041 (IPAddressCollection_t2986660307 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.IPAddressCollection::Clear()
extern "C"  void IPAddressCollection_Clear_m2217404704 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.IPAddressCollection::Contains(System.Net.IPAddress)
extern "C"  bool IPAddressCollection_Contains_m273604859 (IPAddressCollection_t2986660307 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.IPAddressCollection::CopyTo(System.Net.IPAddress[],System.Int32)
extern "C"  void IPAddressCollection_CopyTo_m2396182533 (IPAddressCollection_t2986660307 * __this, IPAddressU5BU5D_t4087230954* ___array0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Net.IPAddress> System.Net.NetworkInformation.IPAddressCollection::GetEnumerator()
extern "C"  Il2CppObject* IPAddressCollection_GetEnumerator_m2726255566 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.IPAddressCollection::Remove(System.Net.IPAddress)
extern "C"  bool IPAddressCollection_Remove_m256110546 (IPAddressCollection_t2986660307 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.IPAddressCollection::get_Count()
extern "C"  int32_t IPAddressCollection_get_Count_m76586813 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.IPAddressCollection::get_IsReadOnly()
extern "C"  bool IPAddressCollection_get_IsReadOnly_m974350634 (IPAddressCollection_t2986660307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.IPAddressCollection::get_Item(System.Int32)
extern "C"  IPAddress_t1399971723 * IPAddressCollection_get_Item_m2566635212 (IPAddressCollection_t2986660307 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
