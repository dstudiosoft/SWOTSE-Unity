﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Exception>::.ctor()
#define List_1__ctor_m749969962(__this, method) ((  void (*) (List_1_t1296561819 *, const MethodInfo*))List_1__ctor_m3040225017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m772107377(__this, ___collection0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::.ctor(System.Int32)
#define List_1__ctor_m1183842875(__this, ___capacity0, method) ((  void (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::.ctor(T[],System.Int32)
#define List_1__ctor_m2893096301(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1296561819 *, ExceptionU5BU5D_t1780857142*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::.cctor()
#define List_1__cctor_m675696873(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Exception>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m808951454(__this, method) ((  Il2CppObject* (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3611002956(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1296561819 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Exception>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m428486095(__this, method) ((  Il2CppObject * (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m981876512(__this, ___item0, method) ((  int32_t (*) (List_1_t1296561819 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m908759262(__this, ___item0, method) ((  bool (*) (List_1_t1296561819 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2680708534(__this, ___item0, method) ((  int32_t (*) (List_1_t1296561819 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3521856783(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1296561819 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1723602983(__this, ___item0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3702551331(__this, method) ((  bool (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4108204392(__this, method) ((  bool (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Exception>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3875317158(__this, method) ((  Il2CppObject * (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3917013207(__this, method) ((  bool (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1659299396(__this, method) ((  bool (*) (List_1_t1296561819 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3586901219(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m4280985110(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1296561819 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Add(T)
#define List_1_Add_m3022301630(__this, ___item0, method) ((  void (*) (List_1_t1296561819 *, Exception_t1927440687 *, const MethodInfo*))List_1_Add_m1194694229_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1803157446(__this, ___newCount0, method) ((  void (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1834164150(__this, ___collection0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2317535062(__this, ___enumerable0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1159066352(__this, ___collection0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Exception>::AsReadOnly()
#define List_1_AsReadOnly_m1382810028(__this, method) ((  ReadOnlyCollection_1_t2113226379 * (*) (List_1_t1296561819 *, const MethodInfo*))List_1_AsReadOnly_m1618299177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Clear()
#define List_1_Clear_m4023450399(__this, method) ((  void (*) (List_1_t1296561819 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::Contains(T)
#define List_1_Contains_m1746790081(__this, ___item0, method) ((  bool (*) (List_1_t1296561819 *, Exception_t1927440687 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2475653191(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1296561819 *, ExceptionU5BU5D_t1780857142*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Exception>::Find(System.Predicate`1<T>)
#define List_1_Find_m3143899825(__this, ___match0, method) ((  Exception_t1927440687 * (*) (List_1_t1296561819 *, Predicate_1_t370410802 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3750415124(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t370410802 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Exception>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3189977704(__this, ___match0, method) ((  List_1_t1296561819 * (*) (List_1_t1296561819 *, Predicate_1_t370410802 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Exception>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m279528360(__this, ___match0, method) ((  List_1_t1296561819 * (*) (List_1_t1296561819 *, Predicate_1_t370410802 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Exception>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3100261512(__this, ___match0, method) ((  List_1_t1296561819 * (*) (List_1_t1296561819 *, Predicate_1_t370410802 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4131563745(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1296561819 *, int32_t, int32_t, Predicate_1_t370410802 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m150899548(__this, ___action0, method) ((  void (*) (List_1_t1296561819 *, Action_1_t1729240069 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Exception>::GetEnumerator()
#define List_1_GetEnumerator_m1547264952(__this, method) ((  Enumerator_t831291493  (*) (List_1_t1296561819 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::IndexOf(T)
#define List_1_IndexOf_m2637003187(__this, ___item0, method) ((  int32_t (*) (List_1_t1296561819 *, Exception_t1927440687 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4037660072(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1296561819 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m175152567(__this, ___index0, method) ((  void (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Insert(System.Int32,T)
#define List_1_Insert_m2092062778(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1296561819 *, int32_t, Exception_t1927440687 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m641634473(__this, ___collection0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Exception>::Remove(T)
#define List_1_Remove_m1614653580(__this, ___item0, method) ((  bool (*) (List_1_t1296561819 *, Exception_t1927440687 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m990539120(__this, ___match0, method) ((  int32_t (*) (List_1_t1296561819 *, Predicate_1_t370410802 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m747247382(__this, ___index0, method) ((  void (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Reverse()
#define List_1_Reverse_m62242286(__this, method) ((  void (*) (List_1_t1296561819 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Sort()
#define List_1_Sort_m653415818(__this, method) ((  void (*) (List_1_t1296561819 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2771142710(__this, ___comparer0, method) ((  void (*) (List_1_t1296561819 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1614623329(__this, ___comparison0, method) ((  void (*) (List_1_t1296561819 *, Comparison_1_t3189179538 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Exception>::ToArray()
#define List_1_ToArray_m4031813501(__this, method) ((  ExceptionU5BU5D_t1780857142* (*) (List_1_t1296561819 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::TrimExcess()
#define List_1_TrimExcess_m3585446787(__this, method) ((  void (*) (List_1_t1296561819 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::get_Capacity()
#define List_1_get_Capacity_m4171070549(__this, method) ((  int32_t (*) (List_1_t1296561819 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m246071894(__this, ___value0, method) ((  void (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Exception>::get_Count()
#define List_1_get_Count_m4198959680(__this, method) ((  int32_t (*) (List_1_t1296561819 *, const MethodInfo*))List_1_get_Count_m1549410684_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Exception>::get_Item(System.Int32)
#define List_1_get_Item_m307111802(__this, ___index0, method) ((  Exception_t1927440687 * (*) (List_1_t1296561819 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Exception>::set_Item(System.Int32,T)
#define List_1_set_Item_m4054693827(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1296561819 *, int32_t, Exception_t1927440687 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
