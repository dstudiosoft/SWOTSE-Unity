﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructionPitManager
struct ConstructionPitManager_t99488877;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingInformationManager
struct  BuildingInformationManager_t1741266979  : public MonoBehaviour_t1158329972
{
public:
	// ConstructionPitManager BuildingInformationManager::pitManager
	ConstructionPitManager_t99488877 * ___pitManager_2;
	// UnityEngine.GameObject BuildingInformationManager::trainingPanel
	GameObject_t1756533147 * ___trainingPanel_3;
	// UnityEngine.GameObject BuildingInformationManager::marketPanel
	GameObject_t1756533147 * ___marketPanel_4;
	// UnityEngine.GameObject BuildingInformationManager::residencePanel
	GameObject_t1756533147 * ___residencePanel_5;
	// UnityEngine.GameObject BuildingInformationManager::guestHousePanel
	GameObject_t1756533147 * ___guestHousePanel_6;
	// UnityEngine.GameObject BuildingInformationManager::feastingHallPanel
	GameObject_t1756533147 * ___feastingHallPanel_7;
	// UnityEngine.GameObject BuildingInformationManager::diplomacyPanel
	GameObject_t1756533147 * ___diplomacyPanel_8;
	// UnityEngine.GameObject BuildingInformationManager::commandCenterPanel
	GameObject_t1756533147 * ___commandCenterPanel_9;
	// UnityEngine.GameObject BuildingInformationManager::archery
	GameObject_t1756533147 * ___archery_10;
	// UnityEngine.GameObject BuildingInformationManager::godhouse
	GameObject_t1756533147 * ___godhouse_11;
	// UnityEngine.GameObject BuildingInformationManager::stable
	GameObject_t1756533147 * ___stable_12;
	// UnityEngine.GameObject BuildingInformationManager::academy
	GameObject_t1756533147 * ___academy_13;
	// UnityEngine.GameObject BuildingInformationManager::engineering
	GameObject_t1756533147 * ___engineering_14;
	// UnityEngine.UI.Text BuildingInformationManager::buildingName
	Text_t356221433 * ___buildingName_15;
	// UnityEngine.UI.Text BuildingInformationManager::buildingDesctiption
	Text_t356221433 * ___buildingDesctiption_16;
	// UnityEngine.UI.Text BuildingInformationManager::buildingLevel
	Text_t356221433 * ___buildingLevel_17;
	// UnityEngine.UI.Image BuildingInformationManager::buildingImage
	Image_t2042527209 * ___buildingImage_18;
	// UnityEngine.UI.Dropdown BuildingInformationManager::taxDropdown
	Dropdown_t1985816271 * ___taxDropdown_19;
	// System.Int64 BuildingInformationManager::taxIndex
	int64_t ___taxIndex_20;
	// System.String BuildingInformationManager::type
	String_t* ___type_21;
	// System.Int64 BuildingInformationManager::world_id
	int64_t ___world_id_22;

public:
	inline static int32_t get_offset_of_pitManager_2() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___pitManager_2)); }
	inline ConstructionPitManager_t99488877 * get_pitManager_2() const { return ___pitManager_2; }
	inline ConstructionPitManager_t99488877 ** get_address_of_pitManager_2() { return &___pitManager_2; }
	inline void set_pitManager_2(ConstructionPitManager_t99488877 * value)
	{
		___pitManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___pitManager_2, value);
	}

	inline static int32_t get_offset_of_trainingPanel_3() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___trainingPanel_3)); }
	inline GameObject_t1756533147 * get_trainingPanel_3() const { return ___trainingPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_trainingPanel_3() { return &___trainingPanel_3; }
	inline void set_trainingPanel_3(GameObject_t1756533147 * value)
	{
		___trainingPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___trainingPanel_3, value);
	}

	inline static int32_t get_offset_of_marketPanel_4() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___marketPanel_4)); }
	inline GameObject_t1756533147 * get_marketPanel_4() const { return ___marketPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_marketPanel_4() { return &___marketPanel_4; }
	inline void set_marketPanel_4(GameObject_t1756533147 * value)
	{
		___marketPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___marketPanel_4, value);
	}

	inline static int32_t get_offset_of_residencePanel_5() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___residencePanel_5)); }
	inline GameObject_t1756533147 * get_residencePanel_5() const { return ___residencePanel_5; }
	inline GameObject_t1756533147 ** get_address_of_residencePanel_5() { return &___residencePanel_5; }
	inline void set_residencePanel_5(GameObject_t1756533147 * value)
	{
		___residencePanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___residencePanel_5, value);
	}

	inline static int32_t get_offset_of_guestHousePanel_6() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___guestHousePanel_6)); }
	inline GameObject_t1756533147 * get_guestHousePanel_6() const { return ___guestHousePanel_6; }
	inline GameObject_t1756533147 ** get_address_of_guestHousePanel_6() { return &___guestHousePanel_6; }
	inline void set_guestHousePanel_6(GameObject_t1756533147 * value)
	{
		___guestHousePanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___guestHousePanel_6, value);
	}

	inline static int32_t get_offset_of_feastingHallPanel_7() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___feastingHallPanel_7)); }
	inline GameObject_t1756533147 * get_feastingHallPanel_7() const { return ___feastingHallPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_feastingHallPanel_7() { return &___feastingHallPanel_7; }
	inline void set_feastingHallPanel_7(GameObject_t1756533147 * value)
	{
		___feastingHallPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___feastingHallPanel_7, value);
	}

	inline static int32_t get_offset_of_diplomacyPanel_8() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___diplomacyPanel_8)); }
	inline GameObject_t1756533147 * get_diplomacyPanel_8() const { return ___diplomacyPanel_8; }
	inline GameObject_t1756533147 ** get_address_of_diplomacyPanel_8() { return &___diplomacyPanel_8; }
	inline void set_diplomacyPanel_8(GameObject_t1756533147 * value)
	{
		___diplomacyPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___diplomacyPanel_8, value);
	}

	inline static int32_t get_offset_of_commandCenterPanel_9() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___commandCenterPanel_9)); }
	inline GameObject_t1756533147 * get_commandCenterPanel_9() const { return ___commandCenterPanel_9; }
	inline GameObject_t1756533147 ** get_address_of_commandCenterPanel_9() { return &___commandCenterPanel_9; }
	inline void set_commandCenterPanel_9(GameObject_t1756533147 * value)
	{
		___commandCenterPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___commandCenterPanel_9, value);
	}

	inline static int32_t get_offset_of_archery_10() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___archery_10)); }
	inline GameObject_t1756533147 * get_archery_10() const { return ___archery_10; }
	inline GameObject_t1756533147 ** get_address_of_archery_10() { return &___archery_10; }
	inline void set_archery_10(GameObject_t1756533147 * value)
	{
		___archery_10 = value;
		Il2CppCodeGenWriteBarrier(&___archery_10, value);
	}

	inline static int32_t get_offset_of_godhouse_11() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___godhouse_11)); }
	inline GameObject_t1756533147 * get_godhouse_11() const { return ___godhouse_11; }
	inline GameObject_t1756533147 ** get_address_of_godhouse_11() { return &___godhouse_11; }
	inline void set_godhouse_11(GameObject_t1756533147 * value)
	{
		___godhouse_11 = value;
		Il2CppCodeGenWriteBarrier(&___godhouse_11, value);
	}

	inline static int32_t get_offset_of_stable_12() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___stable_12)); }
	inline GameObject_t1756533147 * get_stable_12() const { return ___stable_12; }
	inline GameObject_t1756533147 ** get_address_of_stable_12() { return &___stable_12; }
	inline void set_stable_12(GameObject_t1756533147 * value)
	{
		___stable_12 = value;
		Il2CppCodeGenWriteBarrier(&___stable_12, value);
	}

	inline static int32_t get_offset_of_academy_13() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___academy_13)); }
	inline GameObject_t1756533147 * get_academy_13() const { return ___academy_13; }
	inline GameObject_t1756533147 ** get_address_of_academy_13() { return &___academy_13; }
	inline void set_academy_13(GameObject_t1756533147 * value)
	{
		___academy_13 = value;
		Il2CppCodeGenWriteBarrier(&___academy_13, value);
	}

	inline static int32_t get_offset_of_engineering_14() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___engineering_14)); }
	inline GameObject_t1756533147 * get_engineering_14() const { return ___engineering_14; }
	inline GameObject_t1756533147 ** get_address_of_engineering_14() { return &___engineering_14; }
	inline void set_engineering_14(GameObject_t1756533147 * value)
	{
		___engineering_14 = value;
		Il2CppCodeGenWriteBarrier(&___engineering_14, value);
	}

	inline static int32_t get_offset_of_buildingName_15() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___buildingName_15)); }
	inline Text_t356221433 * get_buildingName_15() const { return ___buildingName_15; }
	inline Text_t356221433 ** get_address_of_buildingName_15() { return &___buildingName_15; }
	inline void set_buildingName_15(Text_t356221433 * value)
	{
		___buildingName_15 = value;
		Il2CppCodeGenWriteBarrier(&___buildingName_15, value);
	}

	inline static int32_t get_offset_of_buildingDesctiption_16() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___buildingDesctiption_16)); }
	inline Text_t356221433 * get_buildingDesctiption_16() const { return ___buildingDesctiption_16; }
	inline Text_t356221433 ** get_address_of_buildingDesctiption_16() { return &___buildingDesctiption_16; }
	inline void set_buildingDesctiption_16(Text_t356221433 * value)
	{
		___buildingDesctiption_16 = value;
		Il2CppCodeGenWriteBarrier(&___buildingDesctiption_16, value);
	}

	inline static int32_t get_offset_of_buildingLevel_17() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___buildingLevel_17)); }
	inline Text_t356221433 * get_buildingLevel_17() const { return ___buildingLevel_17; }
	inline Text_t356221433 ** get_address_of_buildingLevel_17() { return &___buildingLevel_17; }
	inline void set_buildingLevel_17(Text_t356221433 * value)
	{
		___buildingLevel_17 = value;
		Il2CppCodeGenWriteBarrier(&___buildingLevel_17, value);
	}

	inline static int32_t get_offset_of_buildingImage_18() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___buildingImage_18)); }
	inline Image_t2042527209 * get_buildingImage_18() const { return ___buildingImage_18; }
	inline Image_t2042527209 ** get_address_of_buildingImage_18() { return &___buildingImage_18; }
	inline void set_buildingImage_18(Image_t2042527209 * value)
	{
		___buildingImage_18 = value;
		Il2CppCodeGenWriteBarrier(&___buildingImage_18, value);
	}

	inline static int32_t get_offset_of_taxDropdown_19() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___taxDropdown_19)); }
	inline Dropdown_t1985816271 * get_taxDropdown_19() const { return ___taxDropdown_19; }
	inline Dropdown_t1985816271 ** get_address_of_taxDropdown_19() { return &___taxDropdown_19; }
	inline void set_taxDropdown_19(Dropdown_t1985816271 * value)
	{
		___taxDropdown_19 = value;
		Il2CppCodeGenWriteBarrier(&___taxDropdown_19, value);
	}

	inline static int32_t get_offset_of_taxIndex_20() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___taxIndex_20)); }
	inline int64_t get_taxIndex_20() const { return ___taxIndex_20; }
	inline int64_t* get_address_of_taxIndex_20() { return &___taxIndex_20; }
	inline void set_taxIndex_20(int64_t value)
	{
		___taxIndex_20 = value;
	}

	inline static int32_t get_offset_of_type_21() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___type_21)); }
	inline String_t* get_type_21() const { return ___type_21; }
	inline String_t** get_address_of_type_21() { return &___type_21; }
	inline void set_type_21(String_t* value)
	{
		___type_21 = value;
		Il2CppCodeGenWriteBarrier(&___type_21, value);
	}

	inline static int32_t get_offset_of_world_id_22() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979, ___world_id_22)); }
	inline int64_t get_world_id_22() const { return ___world_id_22; }
	inline int64_t* get_address_of_world_id_22() { return &___world_id_22; }
	inline void set_world_id_22(int64_t value)
	{
		___world_id_22 = value;
	}
};

struct BuildingInformationManager_t1741266979_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BuildingInformationManager::<>f__switch$map7
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map7_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_23() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t1741266979_StaticFields, ___U3CU3Ef__switchU24map7_23)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map7_23() const { return ___U3CU3Ef__switchU24map7_23; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map7_23() { return &___U3CU3Ef__switchU24map7_23; }
	inline void set_U3CU3Ef__switchU24map7_23(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map7_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map7_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
