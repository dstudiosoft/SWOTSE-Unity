﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>
struct TimedSequence_1_t4021328779;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t2784648180;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t686677694;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::.ctor()
extern "C"  void TimedSequence_1__ctor_m1597661095_gshared (TimedSequence_1_t4021328779 * __this, const MethodInfo* method);
#define TimedSequence_1__ctor_m1597661095(__this, method) ((  void (*) (TimedSequence_1_t4021328779 *, const MethodInfo*))TimedSequence_1__ctor_m1597661095_gshared)(__this, method)
// System.Void TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::Add(T)
extern "C"  void TimedSequence_1_Add_m513031666_gshared (TimedSequence_1_t4021328779 * __this, Vector2_t2243707579  ___element0, const MethodInfo* method);
#define TimedSequence_1_Add_m513031666(__this, ___element0, method) ((  void (*) (TimedSequence_1_t4021328779 *, Vector2_t2243707579 , const MethodInfo*))TimedSequence_1_Add_m513031666_gshared)(__this, ___element0, method)
// System.Void TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::Add(T,System.Single)
extern "C"  void TimedSequence_1_Add_m2986003123_gshared (TimedSequence_1_t4021328779 * __this, Vector2_t2243707579  ___element0, float ___time1, const MethodInfo* method);
#define TimedSequence_1_Add_m2986003123(__this, ___element0, ___time1, method) ((  void (*) (TimedSequence_1_t4021328779 *, Vector2_t2243707579 , float, const MethodInfo*))TimedSequence_1_Add_m2986003123_gshared)(__this, ___element0, ___time1, method)
// System.Void TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::Clear()
extern "C"  void TimedSequence_1_Clear_m3305992206_gshared (TimedSequence_1_t4021328779 * __this, const MethodInfo* method);
#define TimedSequence_1_Clear_m3305992206(__this, method) ((  void (*) (TimedSequence_1_t4021328779 *, const MethodInfo*))TimedSequence_1_Clear_m3305992206_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::FindElementsLaterThan(System.Single)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m1145705187_gshared (TimedSequence_1_t4021328779 * __this, float ___time0, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m1145705187(__this, ___time0, method) ((  Il2CppObject* (*) (TimedSequence_1_t4021328779 *, float, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1145705187_gshared)(__this, ___time0, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::FindElementsLaterThan(System.Single,System.Single&)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m601930982_gshared (TimedSequence_1_t4021328779 * __this, float ___time0, float* ___lastTime1, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m601930982(__this, ___time0, ___lastTime1, method) ((  Il2CppObject* (*) (TimedSequence_1_t4021328779 *, float, float*, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m601930982_gshared)(__this, ___time0, ___lastTime1, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>::FindElementsLaterThan(System.Single,System.Predicate`1<T>)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m1406380918_gshared (TimedSequence_1_t4021328779 * __this, float ___time0, Predicate_1_t686677694 * ___predicate1, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m1406380918(__this, ___time0, ___predicate1, method) ((  Il2CppObject* (*) (TimedSequence_1_t4021328779 *, float, Predicate_1_t686677694 *, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1406380918_gshared)(__this, ___time0, ___predicate1, method)
