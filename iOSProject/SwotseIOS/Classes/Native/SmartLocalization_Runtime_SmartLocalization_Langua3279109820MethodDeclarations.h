﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageDataHandler
struct LanguageDataHandler_t3279109820;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t1165316627;
// SmartLocalization.ILocalizedAssetLoader
struct ILocalizedAssetLoader_t2275178333;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// SmartLocalization.LocalizedObject
struct LocalizedObject_t1895892772;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"
#include "mscorlib_System_String2029220233.h"
#include "SmartLocalization_Runtime_SmartLocalization_Locali1895892772.h"

// System.Void SmartLocalization.LanguageDataHandler::.ctor()
extern "C"  void LanguageDataHandler__ctor_m895538922 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::get_LoadedCulture()
extern "C"  SmartCultureInfo_t2361725737 * LanguageDataHandler_get_LoadedCulture_m3295413164 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedCulture(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageDataHandler_set_LoadedCulture_m415338081 (LanguageDataHandler_t3279109820 * __this, SmartCultureInfo_t2361725737 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::get_VerboseLogging()
extern "C"  bool LanguageDataHandler_get_VerboseLogging_m902204926 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::set_VerboseLogging(System.Boolean)
extern "C"  void LanguageDataHandler_set_VerboseLogging_m1492209955 (LanguageDataHandler_t3279109820 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::get_LoadedValuesDictionary()
extern "C"  SortedDictionary_2_t1165316627 * LanguageDataHandler_get_LoadedValuesDictionary_m4237153133 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageDataHandler::set_LoadedValuesDictionary(System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageDataHandler_set_LoadedValuesDictionary_m1738843566 (LanguageDataHandler_t3279109820 * __this, SortedDictionary_2_t1165316627 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.ILocalizedAssetLoader SmartLocalization.LanguageDataHandler::get_AssetLoader()
extern "C"  Il2CppObject * LanguageDataHandler_get_AssetLoader_m868353928 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::Load(System.String)
extern "C"  bool LanguageDataHandler_Load_m2182883078 (LanguageDataHandler_t3279109820 * __this, String_t* ___resxData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::Append(System.String)
extern "C"  bool LanguageDataHandler_Append_m4107375858 (LanguageDataHandler_t3279109820 * __this, String_t* ___resxData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetKeysWithinCategory(System.String)
extern "C"  List_1_t1398341365 * LanguageDataHandler_GetKeysWithinCategory_m3960296562 (LanguageDataHandler_t3279109820 * __this, String_t* ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageDataHandler::GetAllKeys()
extern "C"  List_1_t1398341365 * LanguageDataHandler_GetAllKeys_m2613598376 (LanguageDataHandler_t3279109820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObject SmartLocalization.LanguageDataHandler::GetLocalizedObject(System.String)
extern "C"  LocalizedObject_t1895892772 * LanguageDataHandler_GetLocalizedObject_m3084583499 (LanguageDataHandler_t3279109820 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageDataHandler::GetTextValue(System.String)
extern "C"  String_t* LanguageDataHandler_GetTextValue_m520202043 (LanguageDataHandler_t3279109820 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageDataHandler::HasKey(System.String)
extern "C"  bool LanguageDataHandler_HasKey_m2666009083 (LanguageDataHandler_t3279109820 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::LoadLanguageDictionary(System.String)
extern "C"  SortedDictionary_2_t1165316627 * LanguageDataHandler_LoadLanguageDictionary_m283339857 (LanguageDataHandler_t3279109820 * __this, String_t* ___resxData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageDataHandler::CheckLanguageOverrideCode(SmartLocalization.LocalizedObject)
extern "C"  String_t* LanguageDataHandler_CheckLanguageOverrideCode_m1956820006 (LanguageDataHandler_t3279109820 * __this, LocalizedObject_t1895892772 * ___localizedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
