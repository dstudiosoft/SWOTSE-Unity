﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3145943116(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3068432259 *, Dictionary_2_t70405120 *, const MethodInfo*))ValueCollection__ctor_m1801851342_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3213293630(__this, ___item0, method) ((  void (*) (ValueCollection_t3068432259 *, List_1_t328750215 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1477647540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m132249883(__this, method) ((  void (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m573646175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m874140354(__this, ___item0, method) ((  bool (*) (ValueCollection_t3068432259 *, List_1_t328750215 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1598273024_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3740525611(__this, ___item0, method) ((  bool (*) (ValueCollection_t3068432259 *, List_1_t328750215 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3764375695_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3071253521(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3036711881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3130419565(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3068432259 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3792551117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3385191630(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1773104428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3322657903(__this, method) ((  bool (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1530798787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3404230073(__this, method) ((  bool (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3044620153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1994956061(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m919209341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1132250459(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3068432259 *, List_1U5BU5D_t2023996798*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m927881183_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2448761654(__this, method) ((  Enumerator_t1756937884  (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_GetEnumerator_m401908452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::get_Count()
#define ValueCollection_get_Count_m2376766945(__this, method) ((  int32_t (*) (ValueCollection_t3068432259 *, const MethodInfo*))ValueCollection_get_Count_m3718352161_gshared)(__this, method)
