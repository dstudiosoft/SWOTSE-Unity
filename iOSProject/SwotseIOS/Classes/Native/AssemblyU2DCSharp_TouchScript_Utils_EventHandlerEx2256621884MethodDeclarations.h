﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Exception
struct Exception_t1927440687;
// System.EventHandler
struct EventHandler_t277755526;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"

// System.Exception TouchScript.Utils.EventHandlerExtensions::InvokeHandleExceptions(System.EventHandler,System.Object,System.EventArgs)
extern "C"  Exception_t1927440687 * EventHandlerExtensions_InvokeHandleExceptions_m656995278 (Il2CppObject * __this /* static, unused */, EventHandler_t277755526 * ___handler0, Il2CppObject * ___sender1, EventArgs_t3289624707 * ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
