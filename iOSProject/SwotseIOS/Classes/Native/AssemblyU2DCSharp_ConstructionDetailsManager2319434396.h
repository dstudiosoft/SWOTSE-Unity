﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructionPitManager
struct ConstructionPitManager_t99488877;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstructionDetailsManager
struct  ConstructionDetailsManager_t2319434396  : public MonoBehaviour_t1158329972
{
public:
	// ConstructionPitManager ConstructionDetailsManager::pitManager
	ConstructionPitManager_t99488877 * ___pitManager_2;
	// UnityEngine.GameObject ConstructionDetailsManager::cityPanel
	GameObject_t1756533147 * ___cityPanel_3;
	// UnityEngine.GameObject ConstructionDetailsManager::fieldsPanel
	GameObject_t1756533147 * ___fieldsPanel_4;
	// UnityEngine.GameObject ConstructionDetailsManager::feastingHall
	GameObject_t1756533147 * ___feastingHall_5;
	// UnityEngine.GameObject ConstructionDetailsManager::market
	GameObject_t1756533147 * ___market_6;
	// UnityEngine.GameObject ConstructionDetailsManager::guestHouse
	GameObject_t1756533147 * ___guestHouse_7;
	// UnityEngine.GameObject ConstructionDetailsManager::storage
	GameObject_t1756533147 * ___storage_8;
	// UnityEngine.GameObject ConstructionDetailsManager::furnace
	GameObject_t1756533147 * ___furnace_9;
	// UnityEngine.GameObject ConstructionDetailsManager::blackSmith
	GameObject_t1756533147 * ___blackSmith_10;
	// UnityEngine.GameObject ConstructionDetailsManager::hospital
	GameObject_t1756533147 * ___hospital_11;
	// UnityEngine.GameObject ConstructionDetailsManager::godHouse
	GameObject_t1756533147 * ___godHouse_12;
	// UnityEngine.GameObject ConstructionDetailsManager::commandCenter
	GameObject_t1756533147 * ___commandCenter_13;
	// UnityEngine.GameObject ConstructionDetailsManager::diplomacy
	GameObject_t1756533147 * ___diplomacy_14;
	// UnityEngine.GameObject ConstructionDetailsManager::trainingGround
	GameObject_t1756533147 * ___trainingGround_15;
	// UnityEngine.GameObject ConstructionDetailsManager::sportArena
	GameObject_t1756533147 * ___sportArena_16;
	// UnityEngine.GameObject ConstructionDetailsManager::univercity
	GameObject_t1756533147 * ___univercity_17;
	// UnityEngine.GameObject ConstructionDetailsManager::militarySchool
	GameObject_t1756533147 * ___militarySchool_18;
	// UnityEngine.GameObject ConstructionDetailsManager::archery
	GameObject_t1756533147 * ___archery_19;
	// UnityEngine.GameObject ConstructionDetailsManager::stable
	GameObject_t1756533147 * ___stable_20;
	// UnityEngine.GameObject ConstructionDetailsManager::engineering
	GameObject_t1756533147 * ___engineering_21;

public:
	inline static int32_t get_offset_of_pitManager_2() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___pitManager_2)); }
	inline ConstructionPitManager_t99488877 * get_pitManager_2() const { return ___pitManager_2; }
	inline ConstructionPitManager_t99488877 ** get_address_of_pitManager_2() { return &___pitManager_2; }
	inline void set_pitManager_2(ConstructionPitManager_t99488877 * value)
	{
		___pitManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___pitManager_2, value);
	}

	inline static int32_t get_offset_of_cityPanel_3() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___cityPanel_3)); }
	inline GameObject_t1756533147 * get_cityPanel_3() const { return ___cityPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_cityPanel_3() { return &___cityPanel_3; }
	inline void set_cityPanel_3(GameObject_t1756533147 * value)
	{
		___cityPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityPanel_3, value);
	}

	inline static int32_t get_offset_of_fieldsPanel_4() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___fieldsPanel_4)); }
	inline GameObject_t1756533147 * get_fieldsPanel_4() const { return ___fieldsPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_fieldsPanel_4() { return &___fieldsPanel_4; }
	inline void set_fieldsPanel_4(GameObject_t1756533147 * value)
	{
		___fieldsPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___fieldsPanel_4, value);
	}

	inline static int32_t get_offset_of_feastingHall_5() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___feastingHall_5)); }
	inline GameObject_t1756533147 * get_feastingHall_5() const { return ___feastingHall_5; }
	inline GameObject_t1756533147 ** get_address_of_feastingHall_5() { return &___feastingHall_5; }
	inline void set_feastingHall_5(GameObject_t1756533147 * value)
	{
		___feastingHall_5 = value;
		Il2CppCodeGenWriteBarrier(&___feastingHall_5, value);
	}

	inline static int32_t get_offset_of_market_6() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___market_6)); }
	inline GameObject_t1756533147 * get_market_6() const { return ___market_6; }
	inline GameObject_t1756533147 ** get_address_of_market_6() { return &___market_6; }
	inline void set_market_6(GameObject_t1756533147 * value)
	{
		___market_6 = value;
		Il2CppCodeGenWriteBarrier(&___market_6, value);
	}

	inline static int32_t get_offset_of_guestHouse_7() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___guestHouse_7)); }
	inline GameObject_t1756533147 * get_guestHouse_7() const { return ___guestHouse_7; }
	inline GameObject_t1756533147 ** get_address_of_guestHouse_7() { return &___guestHouse_7; }
	inline void set_guestHouse_7(GameObject_t1756533147 * value)
	{
		___guestHouse_7 = value;
		Il2CppCodeGenWriteBarrier(&___guestHouse_7, value);
	}

	inline static int32_t get_offset_of_storage_8() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___storage_8)); }
	inline GameObject_t1756533147 * get_storage_8() const { return ___storage_8; }
	inline GameObject_t1756533147 ** get_address_of_storage_8() { return &___storage_8; }
	inline void set_storage_8(GameObject_t1756533147 * value)
	{
		___storage_8 = value;
		Il2CppCodeGenWriteBarrier(&___storage_8, value);
	}

	inline static int32_t get_offset_of_furnace_9() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___furnace_9)); }
	inline GameObject_t1756533147 * get_furnace_9() const { return ___furnace_9; }
	inline GameObject_t1756533147 ** get_address_of_furnace_9() { return &___furnace_9; }
	inline void set_furnace_9(GameObject_t1756533147 * value)
	{
		___furnace_9 = value;
		Il2CppCodeGenWriteBarrier(&___furnace_9, value);
	}

	inline static int32_t get_offset_of_blackSmith_10() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___blackSmith_10)); }
	inline GameObject_t1756533147 * get_blackSmith_10() const { return ___blackSmith_10; }
	inline GameObject_t1756533147 ** get_address_of_blackSmith_10() { return &___blackSmith_10; }
	inline void set_blackSmith_10(GameObject_t1756533147 * value)
	{
		___blackSmith_10 = value;
		Il2CppCodeGenWriteBarrier(&___blackSmith_10, value);
	}

	inline static int32_t get_offset_of_hospital_11() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___hospital_11)); }
	inline GameObject_t1756533147 * get_hospital_11() const { return ___hospital_11; }
	inline GameObject_t1756533147 ** get_address_of_hospital_11() { return &___hospital_11; }
	inline void set_hospital_11(GameObject_t1756533147 * value)
	{
		___hospital_11 = value;
		Il2CppCodeGenWriteBarrier(&___hospital_11, value);
	}

	inline static int32_t get_offset_of_godHouse_12() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___godHouse_12)); }
	inline GameObject_t1756533147 * get_godHouse_12() const { return ___godHouse_12; }
	inline GameObject_t1756533147 ** get_address_of_godHouse_12() { return &___godHouse_12; }
	inline void set_godHouse_12(GameObject_t1756533147 * value)
	{
		___godHouse_12 = value;
		Il2CppCodeGenWriteBarrier(&___godHouse_12, value);
	}

	inline static int32_t get_offset_of_commandCenter_13() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___commandCenter_13)); }
	inline GameObject_t1756533147 * get_commandCenter_13() const { return ___commandCenter_13; }
	inline GameObject_t1756533147 ** get_address_of_commandCenter_13() { return &___commandCenter_13; }
	inline void set_commandCenter_13(GameObject_t1756533147 * value)
	{
		___commandCenter_13 = value;
		Il2CppCodeGenWriteBarrier(&___commandCenter_13, value);
	}

	inline static int32_t get_offset_of_diplomacy_14() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___diplomacy_14)); }
	inline GameObject_t1756533147 * get_diplomacy_14() const { return ___diplomacy_14; }
	inline GameObject_t1756533147 ** get_address_of_diplomacy_14() { return &___diplomacy_14; }
	inline void set_diplomacy_14(GameObject_t1756533147 * value)
	{
		___diplomacy_14 = value;
		Il2CppCodeGenWriteBarrier(&___diplomacy_14, value);
	}

	inline static int32_t get_offset_of_trainingGround_15() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___trainingGround_15)); }
	inline GameObject_t1756533147 * get_trainingGround_15() const { return ___trainingGround_15; }
	inline GameObject_t1756533147 ** get_address_of_trainingGround_15() { return &___trainingGround_15; }
	inline void set_trainingGround_15(GameObject_t1756533147 * value)
	{
		___trainingGround_15 = value;
		Il2CppCodeGenWriteBarrier(&___trainingGround_15, value);
	}

	inline static int32_t get_offset_of_sportArena_16() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___sportArena_16)); }
	inline GameObject_t1756533147 * get_sportArena_16() const { return ___sportArena_16; }
	inline GameObject_t1756533147 ** get_address_of_sportArena_16() { return &___sportArena_16; }
	inline void set_sportArena_16(GameObject_t1756533147 * value)
	{
		___sportArena_16 = value;
		Il2CppCodeGenWriteBarrier(&___sportArena_16, value);
	}

	inline static int32_t get_offset_of_univercity_17() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___univercity_17)); }
	inline GameObject_t1756533147 * get_univercity_17() const { return ___univercity_17; }
	inline GameObject_t1756533147 ** get_address_of_univercity_17() { return &___univercity_17; }
	inline void set_univercity_17(GameObject_t1756533147 * value)
	{
		___univercity_17 = value;
		Il2CppCodeGenWriteBarrier(&___univercity_17, value);
	}

	inline static int32_t get_offset_of_militarySchool_18() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___militarySchool_18)); }
	inline GameObject_t1756533147 * get_militarySchool_18() const { return ___militarySchool_18; }
	inline GameObject_t1756533147 ** get_address_of_militarySchool_18() { return &___militarySchool_18; }
	inline void set_militarySchool_18(GameObject_t1756533147 * value)
	{
		___militarySchool_18 = value;
		Il2CppCodeGenWriteBarrier(&___militarySchool_18, value);
	}

	inline static int32_t get_offset_of_archery_19() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___archery_19)); }
	inline GameObject_t1756533147 * get_archery_19() const { return ___archery_19; }
	inline GameObject_t1756533147 ** get_address_of_archery_19() { return &___archery_19; }
	inline void set_archery_19(GameObject_t1756533147 * value)
	{
		___archery_19 = value;
		Il2CppCodeGenWriteBarrier(&___archery_19, value);
	}

	inline static int32_t get_offset_of_stable_20() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___stable_20)); }
	inline GameObject_t1756533147 * get_stable_20() const { return ___stable_20; }
	inline GameObject_t1756533147 ** get_address_of_stable_20() { return &___stable_20; }
	inline void set_stable_20(GameObject_t1756533147 * value)
	{
		___stable_20 = value;
		Il2CppCodeGenWriteBarrier(&___stable_20, value);
	}

	inline static int32_t get_offset_of_engineering_21() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t2319434396, ___engineering_21)); }
	inline GameObject_t1756533147 * get_engineering_21() const { return ___engineering_21; }
	inline GameObject_t1756533147 ** get_address_of_engineering_21() { return &___engineering_21; }
	inline void set_engineering_21(GameObject_t1756533147 * value)
	{
		___engineering_21 = value;
		Il2CppCodeGenWriteBarrier(&___engineering_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
