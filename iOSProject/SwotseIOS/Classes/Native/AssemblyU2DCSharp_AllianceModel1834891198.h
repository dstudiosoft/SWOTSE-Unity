﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceModel
struct  AllianceModel_t1834891198  : public Il2CppObject
{
public:
	// System.Int64 AllianceModel::id
	int64_t ___id_0;
	// System.String AllianceModel::alliance_name
	String_t* ___alliance_name_1;
	// System.Int64 AllianceModel::founderId
	int64_t ___founderId_2;
	// System.String AllianceModel::founderName
	String_t* ___founderName_3;
	// System.Int64 AllianceModel::leaderId
	int64_t ___leaderId_4;
	// System.String AllianceModel::leaderName
	String_t* ___leaderName_5;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::allies
	List_1_t278199169 * ___allies_6;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::enemies
	List_1_t278199169 * ___enemies_7;
	// System.Int64 AllianceModel::rank
	int64_t ___rank_8;
	// System.Int64 AllianceModel::members_count
	int64_t ___members_count_9;
	// System.Int64 AllianceModel::alliance_experience
	int64_t ___alliance_experience_10;
	// System.String AllianceModel::guideline
	String_t* ___guideline_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_alliance_name_1() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___alliance_name_1)); }
	inline String_t* get_alliance_name_1() const { return ___alliance_name_1; }
	inline String_t** get_address_of_alliance_name_1() { return &___alliance_name_1; }
	inline void set_alliance_name_1(String_t* value)
	{
		___alliance_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___alliance_name_1, value);
	}

	inline static int32_t get_offset_of_founderId_2() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___founderId_2)); }
	inline int64_t get_founderId_2() const { return ___founderId_2; }
	inline int64_t* get_address_of_founderId_2() { return &___founderId_2; }
	inline void set_founderId_2(int64_t value)
	{
		___founderId_2 = value;
	}

	inline static int32_t get_offset_of_founderName_3() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___founderName_3)); }
	inline String_t* get_founderName_3() const { return ___founderName_3; }
	inline String_t** get_address_of_founderName_3() { return &___founderName_3; }
	inline void set_founderName_3(String_t* value)
	{
		___founderName_3 = value;
		Il2CppCodeGenWriteBarrier(&___founderName_3, value);
	}

	inline static int32_t get_offset_of_leaderId_4() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___leaderId_4)); }
	inline int64_t get_leaderId_4() const { return ___leaderId_4; }
	inline int64_t* get_address_of_leaderId_4() { return &___leaderId_4; }
	inline void set_leaderId_4(int64_t value)
	{
		___leaderId_4 = value;
	}

	inline static int32_t get_offset_of_leaderName_5() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___leaderName_5)); }
	inline String_t* get_leaderName_5() const { return ___leaderName_5; }
	inline String_t** get_address_of_leaderName_5() { return &___leaderName_5; }
	inline void set_leaderName_5(String_t* value)
	{
		___leaderName_5 = value;
		Il2CppCodeGenWriteBarrier(&___leaderName_5, value);
	}

	inline static int32_t get_offset_of_allies_6() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___allies_6)); }
	inline List_1_t278199169 * get_allies_6() const { return ___allies_6; }
	inline List_1_t278199169 ** get_address_of_allies_6() { return &___allies_6; }
	inline void set_allies_6(List_1_t278199169 * value)
	{
		___allies_6 = value;
		Il2CppCodeGenWriteBarrier(&___allies_6, value);
	}

	inline static int32_t get_offset_of_enemies_7() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___enemies_7)); }
	inline List_1_t278199169 * get_enemies_7() const { return ___enemies_7; }
	inline List_1_t278199169 ** get_address_of_enemies_7() { return &___enemies_7; }
	inline void set_enemies_7(List_1_t278199169 * value)
	{
		___enemies_7 = value;
		Il2CppCodeGenWriteBarrier(&___enemies_7, value);
	}

	inline static int32_t get_offset_of_rank_8() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___rank_8)); }
	inline int64_t get_rank_8() const { return ___rank_8; }
	inline int64_t* get_address_of_rank_8() { return &___rank_8; }
	inline void set_rank_8(int64_t value)
	{
		___rank_8 = value;
	}

	inline static int32_t get_offset_of_members_count_9() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___members_count_9)); }
	inline int64_t get_members_count_9() const { return ___members_count_9; }
	inline int64_t* get_address_of_members_count_9() { return &___members_count_9; }
	inline void set_members_count_9(int64_t value)
	{
		___members_count_9 = value;
	}

	inline static int32_t get_offset_of_alliance_experience_10() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___alliance_experience_10)); }
	inline int64_t get_alliance_experience_10() const { return ___alliance_experience_10; }
	inline int64_t* get_address_of_alliance_experience_10() { return &___alliance_experience_10; }
	inline void set_alliance_experience_10(int64_t value)
	{
		___alliance_experience_10 = value;
	}

	inline static int32_t get_offset_of_guideline_11() { return static_cast<int32_t>(offsetof(AllianceModel_t1834891198, ___guideline_11)); }
	inline String_t* get_guideline_11() const { return ___guideline_11; }
	inline String_t** get_address_of_guideline_11() { return &___guideline_11; }
	inline void set_guideline_11(String_t* value)
	{
		___guideline_11 = value;
		Il2CppCodeGenWriteBarrier(&___guideline_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
