﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// TUIOsharp.DataProcessors.BlobProcessor
struct BlobProcessor_t2126871202;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct EventHandler_1_t3126626844;
// System.Delegate
struct Delegate_t1188392813;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct Dictionary_2_t2974023424;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct List_1_t1262417539;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// OSCsharp.Data.OscMessage
struct OscMessage_t3323977005;
// OSCsharp.Data.OscPacket
struct OscPacket_t3204151022;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t4085310093;
// TUIOsharp.Entities.TuioEntity
struct TuioEntity_t4229790798;
// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct TuioBlobEventArgs_t907500115;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1004265597;
// TUIOsharp.DataProcessors.CursorProcessor
struct CursorProcessor_t3936223090;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct EventHandler_1_t4113216011;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct Dictionary_2_t1693470997;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct List_1_t4276832408;
// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t2804757666;
// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct TuioCursorEventArgs_t1894089282;
// TUIOsharp.DataProcessors.ObjectProcessor
struct ObjectProcessor_t2877610401;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct EventHandler_1_t2455490493;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct Dictionary_2_t3186476502;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct List_1_t1474870617;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t2795875;
// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct TuioObjectEventArgs_t236363764;
// System.EventArgs
struct EventArgs_t3591816995;
// TUIOsharp.TuioServer
struct TuioServer_t3158170151;
// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct List_1_t2674567522;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t1881969775;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t314378975;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t319478644;
// TUIOsharp.DataProcessors.IDataProcessor
struct IDataProcessor_t1202492780;
// OSCsharp.Utils.ExceptionEventArgs
struct ExceptionEventArgs_t2395319211;
// OSCsharp.Net.OscMessageReceivedEventArgs
struct OscMessageReceivedEventArgs_t2390219542;
// TUIOsharp.Entities.TuioCursor[]
struct TuioCursorU5BU5D_t3173943959;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioCursor,System.Collections.DictionaryEntry>
struct Transform_1_t2778282803;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// TUIOsharp.DataProcessors.IDataProcessor[]
struct IDataProcessorU5BU5D_t1118448485;
// TUIOsharp.Entities.TuioObject[]
struct TuioObjectU5BU5D_t1650298290;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioObject,System.Collections.DictionaryEntry>
struct Transform_1_t1254637134;
// System.Char[]
struct CharU5BU5D_t3528271667;
// TUIOsharp.Entities.TuioBlob[]
struct TuioBlobU5BU5D_t2287012000;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioBlob,System.Collections.DictionaryEntry>
struct Transform_1_t1891350844;
// System.Void
struct Void_t1185182177;
// System.Exception
struct Exception_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct EventHandler_1_t2882212236;
// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct EventHandler_1_t2793618168;
// System.Net.IPAddress
struct IPAddress_t241777590;
// System.Net.IPEndPoint
struct IPEndPoint_t3791887218;
// System.Net.Sockets.UdpClient
struct UdpClient_t967282006;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.IAsyncResult
struct IAsyncResult_t767004451;

extern RuntimeClass* EventHandler_1_t3126626844_il2cpp_TypeInfo_var;
extern const uint32_t BlobProcessor_add_BlobAdded_m3279817448_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobAdded_m1162188030_MetadataUsageId;
extern const uint32_t BlobProcessor_add_BlobUpdated_m4053757175_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobUpdated_m3661772473_MetadataUsageId;
extern const uint32_t BlobProcessor_add_BlobRemoved_m2881795667_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobRemoved_m387963999_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2974023424_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1262417539_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t128053199_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3077278521_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2209108566_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1628857705_RuntimeMethod_var;
extern const uint32_t BlobProcessor__ctor_m1296745391_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t600458651_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t1613291102_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioBlob_t4085310093_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioBlobEventArgs_t907500115_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m4133274017_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m689584216_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m697420525_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2841467664_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4138203631_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m1056191967_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m4250047412_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m3037048099_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1031836987_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1857637826_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1699732805_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1456013675_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m987622744_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1090758517_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m4027566236_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m361000296_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1910578371_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m4020322656_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m911377797_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2154023298_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m4267157696_RuntimeMethod_var;
extern String_t* _stringLiteral3983133878;
extern String_t* _stringLiteral2553217811;
extern String_t* _stringLiteral1569063519;
extern String_t* _stringLiteral4243243339;
extern const uint32_t BlobProcessor_ProcessMessage_m2788254080_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t4113216011_il2cpp_TypeInfo_var;
extern const uint32_t CursorProcessor_add_CursorAdded_m511123241_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorAdded_m4000780957_MetadataUsageId;
extern const uint32_t CursorProcessor_add_CursorUpdated_m1062449922_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorUpdated_m770238256_MetadataUsageId;
extern const uint32_t CursorProcessor_add_CursorRemoved_m365604737_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorRemoved_m234525769_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1693470997_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4276832408_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m4060431757_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m546517191_RuntimeMethod_var;
extern const uint32_t CursorProcessor__ctor_m359159357_MetadataUsageId;
extern RuntimeClass* TuioCursor_t2804757666_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioCursorEventArgs_t1894089282_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2949425533_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3374480747_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m3436297861_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1124426651_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m2249459862_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4072011572_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3655613863_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2851265837_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1501378545_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m4292169967_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m4211285854_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m2655476952_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m2068205222_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m2147751717_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2024113527_RuntimeMethod_var;
extern String_t* _stringLiteral3269165829;
extern const uint32_t CursorProcessor_ProcessMessage_m986564398_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t2455490493_il2cpp_TypeInfo_var;
extern const uint32_t ObjectProcessor_add_ObjectAdded_m954421447_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectAdded_m1729185584_MetadataUsageId;
extern const uint32_t ObjectProcessor_add_ObjectUpdated_m3547798864_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectUpdated_m3667678717_MetadataUsageId;
extern const uint32_t ObjectProcessor_add_ObjectRemoved_m386417278_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectRemoved_m1261782329_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3186476502_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1474870617_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3393625636_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1635335082_RuntimeMethod_var;
extern const uint32_t ObjectProcessor__ctor_m3182407677_MetadataUsageId;
extern RuntimeClass* TuioObject_t2795875_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioObjectEventArgs_t236363764_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3350852866_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3017127986_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m3493560753_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m702359707_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m3626592599_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4089973788_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1218014203_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1437897001_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1972841044_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m3922808659_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m357742037_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m1236698408_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m587101098_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m404394798_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m227439264_RuntimeMethod_var;
extern String_t* _stringLiteral3625986001;
extern const uint32_t ObjectProcessor_ProcessMessage_m1566973063_MetadataUsageId;
extern RuntimeClass* EventArgs_t3591816995_il2cpp_TypeInfo_var;
extern const uint32_t TuioBlobEventArgs__ctor_m1030032409_MetadataUsageId;
extern const uint32_t TuioCursorEventArgs__ctor_m138848966_MetadataUsageId;
extern const uint32_t TuioObjectEventArgs__ctor_m3321218798_MetadataUsageId;
extern RuntimeClass* List_1_t2674567522_il2cpp_TypeInfo_var;
extern RuntimeClass* UDPReceiver_t1881969775_il2cpp_TypeInfo_var;
extern RuntimeClass* EventHandler_1_t314378975_il2cpp_TypeInfo_var;
extern RuntimeClass* EventHandler_1_t319478644_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m211060356_RuntimeMethod_var;
extern const RuntimeMethod* TuioServer_handlerOscMessageReceived_m3605979964_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1__ctor_m871205231_RuntimeMethod_var;
extern const RuntimeMethod* TuioServer_handlerOscErrorOccured_m2827029671_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1__ctor_m951552470_RuntimeMethod_var;
extern const uint32_t TuioServer__ctor_m3200135755_MetadataUsageId;
extern const RuntimeMethod* List_1_Contains_m1021282117_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2077402128_RuntimeMethod_var;
extern const uint32_t TuioServer_AddDataProcessor_m4080391229_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m2487744439_RuntimeMethod_var;
extern const uint32_t TuioServer_RemoveDataProcessor_m2239788743_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3700974216_RuntimeMethod_var;
extern const uint32_t TuioServer_RemoveAllDataProcessors_m3154379542_MetadataUsageId;
extern RuntimeClass* IDataProcessor_t1202492780_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m2207940050_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3750012149_RuntimeMethod_var;
extern const uint32_t TuioServer_processMessage_m1808445210_MetadataUsageId;
extern const RuntimeMethod* EventHandler_1_Invoke_m3384041340_RuntimeMethod_var;
extern const uint32_t TuioServer_handlerOscErrorOccured_m2827029671_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef LIST_1_T4276832408_H
#define LIST_1_T4276832408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct  List_1_t4276832408  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioCursorU5BU5D_t3173943959* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4276832408, ____items_1)); }
	inline TuioCursorU5BU5D_t3173943959* get__items_1() const { return ____items_1; }
	inline TuioCursorU5BU5D_t3173943959** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioCursorU5BU5D_t3173943959* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4276832408, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4276832408, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4276832408_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioCursorU5BU5D_t3173943959* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4276832408_StaticFields, ___EmptyArray_4)); }
	inline TuioCursorU5BU5D_t3173943959* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioCursorU5BU5D_t3173943959** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioCursorU5BU5D_t3173943959* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4276832408_H
#ifndef DICTIONARY_2_T1693470997_H
#define DICTIONARY_2_T1693470997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  Dictionary_2_t1693470997  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t385246372* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioCursorU5BU5D_t3173943959* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___keySlots_6)); }
	inline Int32U5BU5D_t385246372* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t385246372** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t385246372* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___valueSlots_7)); }
	inline TuioCursorU5BU5D_t3173943959* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioCursorU5BU5D_t3173943959** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioCursorU5BU5D_t3173943959* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1693470997_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2778282803 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1693470997_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2778282803 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2778282803 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2778282803 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1693470997_H
#ifndef OSCPACKET_T3204151022_H
#define OSCPACKET_T3204151022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscPacket
struct  OscPacket_t3204151022  : public RuntimeObject
{
public:
	// System.String OSCsharp.Data.OscPacket::address
	String_t* ___address_1;
	// System.Collections.Generic.List`1<System.Object> OSCsharp.Data.OscPacket::data
	List_1_t257213610 * ___data_2;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022, ___data_2)); }
	inline List_1_t257213610 * get_data_2() const { return ___data_2; }
	inline List_1_t257213610 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t257213610 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct OscPacket_t3204151022_StaticFields
{
public:
	// System.Boolean OSCsharp.Data.OscPacket::littleEndianByteOrder
	bool ___littleEndianByteOrder_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OSCsharp.Data.OscPacket::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_3;

public:
	inline static int32_t get_offset_of_littleEndianByteOrder_0() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022_StaticFields, ___littleEndianByteOrder_0)); }
	inline bool get_littleEndianByteOrder_0() const { return ___littleEndianByteOrder_0; }
	inline bool* get_address_of_littleEndianByteOrder_0() { return &___littleEndianByteOrder_0; }
	inline void set_littleEndianByteOrder_0(bool value)
	{
		___littleEndianByteOrder_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_3() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022_StaticFields, ___U3CU3Ef__switchU24map1_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_3() const { return ___U3CU3Ef__switchU24map1_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_3() { return &___U3CU3Ef__switchU24map1_3; }
	inline void set_U3CU3Ef__switchU24map1_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKET_T3204151022_H
#ifndef TUIOENTITY_T4229790798_H
#define TUIOENTITY_T4229790798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioEntity
struct  TuioEntity_t4229790798  : public RuntimeObject
{
public:
	// System.Int32 TUIOsharp.Entities.TuioEntity::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Single TUIOsharp.Entities.TuioEntity::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single TUIOsharp.Entities.TuioEntity::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_3;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_4;
	// System.Single TUIOsharp.Entities.TuioEntity::<Acceleration>k__BackingField
	float ___U3CAccelerationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CVelocityXU3Ek__BackingField_3)); }
	inline float get_U3CVelocityXU3Ek__BackingField_3() const { return ___U3CVelocityXU3Ek__BackingField_3; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_3() { return &___U3CVelocityXU3Ek__BackingField_3; }
	inline void set_U3CVelocityXU3Ek__BackingField_3(float value)
	{
		___U3CVelocityXU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CVelocityYU3Ek__BackingField_4)); }
	inline float get_U3CVelocityYU3Ek__BackingField_4() const { return ___U3CVelocityYU3Ek__BackingField_4; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_4() { return &___U3CVelocityYU3Ek__BackingField_4; }
	inline void set_U3CVelocityYU3Ek__BackingField_4(float value)
	{
		___U3CVelocityYU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CAccelerationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TuioEntity_t4229790798, ___U3CAccelerationU3Ek__BackingField_5)); }
	inline float get_U3CAccelerationU3Ek__BackingField_5() const { return ___U3CAccelerationU3Ek__BackingField_5; }
	inline float* get_address_of_U3CAccelerationU3Ek__BackingField_5() { return &___U3CAccelerationU3Ek__BackingField_5; }
	inline void set_U3CAccelerationU3Ek__BackingField_5(float value)
	{
		___U3CAccelerationU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOENTITY_T4229790798_H
#ifndef CURSORPROCESSOR_T3936223090_H
#define CURSORPROCESSOR_T3936223090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.CursorProcessor
struct  CursorProcessor_t3936223090  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorAdded
	EventHandler_1_t4113216011 * ___CursorAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorUpdated
	EventHandler_1_t4113216011 * ___CursorUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorRemoved
	EventHandler_1_t4113216011 * ___CursorRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.CursorProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::cursors
	Dictionary_2_t1693470997 * ___cursors_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::updatedCursors
	List_1_t4276832408 * ___updatedCursors_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::addedCursors
	List_1_t128053199 * ___addedCursors_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::removedCursors
	List_1_t128053199 * ___removedCursors_7;

public:
	inline static int32_t get_offset_of_CursorAdded_0() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorAdded_0)); }
	inline EventHandler_1_t4113216011 * get_CursorAdded_0() const { return ___CursorAdded_0; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorAdded_0() { return &___CursorAdded_0; }
	inline void set_CursorAdded_0(EventHandler_1_t4113216011 * value)
	{
		___CursorAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___CursorAdded_0), value);
	}

	inline static int32_t get_offset_of_CursorUpdated_1() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorUpdated_1)); }
	inline EventHandler_1_t4113216011 * get_CursorUpdated_1() const { return ___CursorUpdated_1; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorUpdated_1() { return &___CursorUpdated_1; }
	inline void set_CursorUpdated_1(EventHandler_1_t4113216011 * value)
	{
		___CursorUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___CursorUpdated_1), value);
	}

	inline static int32_t get_offset_of_CursorRemoved_2() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___CursorRemoved_2)); }
	inline EventHandler_1_t4113216011 * get_CursorRemoved_2() const { return ___CursorRemoved_2; }
	inline EventHandler_1_t4113216011 ** get_address_of_CursorRemoved_2() { return &___CursorRemoved_2; }
	inline void set_CursorRemoved_2(EventHandler_1_t4113216011 * value)
	{
		___CursorRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_cursors_4() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___cursors_4)); }
	inline Dictionary_2_t1693470997 * get_cursors_4() const { return ___cursors_4; }
	inline Dictionary_2_t1693470997 ** get_address_of_cursors_4() { return &___cursors_4; }
	inline void set_cursors_4(Dictionary_2_t1693470997 * value)
	{
		___cursors_4 = value;
		Il2CppCodeGenWriteBarrier((&___cursors_4), value);
	}

	inline static int32_t get_offset_of_updatedCursors_5() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___updatedCursors_5)); }
	inline List_1_t4276832408 * get_updatedCursors_5() const { return ___updatedCursors_5; }
	inline List_1_t4276832408 ** get_address_of_updatedCursors_5() { return &___updatedCursors_5; }
	inline void set_updatedCursors_5(List_1_t4276832408 * value)
	{
		___updatedCursors_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedCursors_5), value);
	}

	inline static int32_t get_offset_of_addedCursors_6() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___addedCursors_6)); }
	inline List_1_t128053199 * get_addedCursors_6() const { return ___addedCursors_6; }
	inline List_1_t128053199 ** get_address_of_addedCursors_6() { return &___addedCursors_6; }
	inline void set_addedCursors_6(List_1_t128053199 * value)
	{
		___addedCursors_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedCursors_6), value);
	}

	inline static int32_t get_offset_of_removedCursors_7() { return static_cast<int32_t>(offsetof(CursorProcessor_t3936223090, ___removedCursors_7)); }
	inline List_1_t128053199 * get_removedCursors_7() const { return ___removedCursors_7; }
	inline List_1_t128053199 ** get_address_of_removedCursors_7() { return &___removedCursors_7; }
	inline void set_removedCursors_7(List_1_t128053199 * value)
	{
		___removedCursors_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedCursors_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORPROCESSOR_T3936223090_H
#ifndef OBJECTPROCESSOR_T2877610401_H
#define OBJECTPROCESSOR_T2877610401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.ObjectProcessor
struct  ObjectProcessor_t2877610401  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectAdded
	EventHandler_1_t2455490493 * ___ObjectAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectUpdated
	EventHandler_1_t2455490493 * ___ObjectUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectRemoved
	EventHandler_1_t2455490493 * ___ObjectRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.ObjectProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::objects
	Dictionary_2_t3186476502 * ___objects_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::updatedObjects
	List_1_t1474870617 * ___updatedObjects_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::addedObjects
	List_1_t128053199 * ___addedObjects_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::removedObjects
	List_1_t128053199 * ___removedObjects_7;

public:
	inline static int32_t get_offset_of_ObjectAdded_0() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectAdded_0)); }
	inline EventHandler_1_t2455490493 * get_ObjectAdded_0() const { return ___ObjectAdded_0; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectAdded_0() { return &___ObjectAdded_0; }
	inline void set_ObjectAdded_0(EventHandler_1_t2455490493 * value)
	{
		___ObjectAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectAdded_0), value);
	}

	inline static int32_t get_offset_of_ObjectUpdated_1() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectUpdated_1)); }
	inline EventHandler_1_t2455490493 * get_ObjectUpdated_1() const { return ___ObjectUpdated_1; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectUpdated_1() { return &___ObjectUpdated_1; }
	inline void set_ObjectUpdated_1(EventHandler_1_t2455490493 * value)
	{
		___ObjectUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectUpdated_1), value);
	}

	inline static int32_t get_offset_of_ObjectRemoved_2() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___ObjectRemoved_2)); }
	inline EventHandler_1_t2455490493 * get_ObjectRemoved_2() const { return ___ObjectRemoved_2; }
	inline EventHandler_1_t2455490493 ** get_address_of_ObjectRemoved_2() { return &___ObjectRemoved_2; }
	inline void set_ObjectRemoved_2(EventHandler_1_t2455490493 * value)
	{
		___ObjectRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___objects_4)); }
	inline Dictionary_2_t3186476502 * get_objects_4() const { return ___objects_4; }
	inline Dictionary_2_t3186476502 ** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(Dictionary_2_t3186476502 * value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier((&___objects_4), value);
	}

	inline static int32_t get_offset_of_updatedObjects_5() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___updatedObjects_5)); }
	inline List_1_t1474870617 * get_updatedObjects_5() const { return ___updatedObjects_5; }
	inline List_1_t1474870617 ** get_address_of_updatedObjects_5() { return &___updatedObjects_5; }
	inline void set_updatedObjects_5(List_1_t1474870617 * value)
	{
		___updatedObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedObjects_5), value);
	}

	inline static int32_t get_offset_of_addedObjects_6() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___addedObjects_6)); }
	inline List_1_t128053199 * get_addedObjects_6() const { return ___addedObjects_6; }
	inline List_1_t128053199 ** get_address_of_addedObjects_6() { return &___addedObjects_6; }
	inline void set_addedObjects_6(List_1_t128053199 * value)
	{
		___addedObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedObjects_6), value);
	}

	inline static int32_t get_offset_of_removedObjects_7() { return static_cast<int32_t>(offsetof(ObjectProcessor_t2877610401, ___removedObjects_7)); }
	inline List_1_t128053199 * get_removedObjects_7() const { return ___removedObjects_7; }
	inline List_1_t128053199 ** get_address_of_removedObjects_7() { return &___removedObjects_7; }
	inline void set_removedObjects_7(List_1_t128053199 * value)
	{
		___removedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedObjects_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPROCESSOR_T2877610401_H
#ifndef TUIOSERVER_T3158170151_H
#define TUIOSERVER_T3158170151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.TuioServer
struct  TuioServer_t3158170151  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> TUIOsharp.TuioServer::ErrorOccured
	EventHandler_1_t319478644 * ___ErrorOccured_0;
	// System.Int32 TUIOsharp.TuioServer::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_1;
	// OSCsharp.Net.UDPReceiver TUIOsharp.TuioServer::udpReceiver
	UDPReceiver_t1881969775 * ___udpReceiver_2;
	// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor> TUIOsharp.TuioServer::processors
	List_1_t2674567522 * ___processors_3;

public:
	inline static int32_t get_offset_of_ErrorOccured_0() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___ErrorOccured_0)); }
	inline EventHandler_1_t319478644 * get_ErrorOccured_0() const { return ___ErrorOccured_0; }
	inline EventHandler_1_t319478644 ** get_address_of_ErrorOccured_0() { return &___ErrorOccured_0; }
	inline void set_ErrorOccured_0(EventHandler_1_t319478644 * value)
	{
		___ErrorOccured_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_0), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___U3CPortU3Ek__BackingField_1)); }
	inline int32_t get_U3CPortU3Ek__BackingField_1() const { return ___U3CPortU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_1() { return &___U3CPortU3Ek__BackingField_1; }
	inline void set_U3CPortU3Ek__BackingField_1(int32_t value)
	{
		___U3CPortU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_udpReceiver_2() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___udpReceiver_2)); }
	inline UDPReceiver_t1881969775 * get_udpReceiver_2() const { return ___udpReceiver_2; }
	inline UDPReceiver_t1881969775 ** get_address_of_udpReceiver_2() { return &___udpReceiver_2; }
	inline void set_udpReceiver_2(UDPReceiver_t1881969775 * value)
	{
		___udpReceiver_2 = value;
		Il2CppCodeGenWriteBarrier((&___udpReceiver_2), value);
	}

	inline static int32_t get_offset_of_processors_3() { return static_cast<int32_t>(offsetof(TuioServer_t3158170151, ___processors_3)); }
	inline List_1_t2674567522 * get_processors_3() const { return ___processors_3; }
	inline List_1_t2674567522 ** get_address_of_processors_3() { return &___processors_3; }
	inline void set_processors_3(List_1_t2674567522 * value)
	{
		___processors_3 = value;
		Il2CppCodeGenWriteBarrier((&___processors_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOSERVER_T3158170151_H
#ifndef LIST_1_T2674567522_H
#define LIST_1_T2674567522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct  List_1_t2674567522  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IDataProcessorU5BU5D_t1118448485* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2674567522, ____items_1)); }
	inline IDataProcessorU5BU5D_t1118448485* get__items_1() const { return ____items_1; }
	inline IDataProcessorU5BU5D_t1118448485** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IDataProcessorU5BU5D_t1118448485* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2674567522, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2674567522, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2674567522_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	IDataProcessorU5BU5D_t1118448485* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2674567522_StaticFields, ___EmptyArray_4)); }
	inline IDataProcessorU5BU5D_t1118448485* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline IDataProcessorU5BU5D_t1118448485** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(IDataProcessorU5BU5D_t1118448485* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2674567522_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DICTIONARY_2_T3186476502_H
#define DICTIONARY_2_T3186476502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct  Dictionary_2_t3186476502  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t385246372* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioObjectU5BU5D_t1650298290* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___keySlots_6)); }
	inline Int32U5BU5D_t385246372* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t385246372** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t385246372* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___valueSlots_7)); }
	inline TuioObjectU5BU5D_t1650298290* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioObjectU5BU5D_t1650298290** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioObjectU5BU5D_t1650298290* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3186476502_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1254637134 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3186476502_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1254637134 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1254637134 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1254637134 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3186476502_H
#ifndef LIST_1_T1474870617_H
#define LIST_1_T1474870617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct  List_1_t1474870617  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioObjectU5BU5D_t1650298290* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1474870617, ____items_1)); }
	inline TuioObjectU5BU5D_t1650298290* get__items_1() const { return ____items_1; }
	inline TuioObjectU5BU5D_t1650298290** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioObjectU5BU5D_t1650298290* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1474870617, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1474870617, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1474870617_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioObjectU5BU5D_t1650298290* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1474870617_StaticFields, ___EmptyArray_4)); }
	inline TuioObjectU5BU5D_t1650298290* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioObjectU5BU5D_t1650298290** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioObjectU5BU5D_t1650298290* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1474870617_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T128053199_H
#define LIST_1_T128053199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t128053199  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t385246372* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____items_1)); }
	inline Int32U5BU5D_t385246372* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t385246372** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t385246372* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t128053199_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t385246372* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t128053199_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t385246372* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t385246372* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T128053199_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef BLOBPROCESSOR_T2126871202_H
#define BLOBPROCESSOR_T2126871202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.BlobProcessor
struct  BlobProcessor_t2126871202  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobAdded
	EventHandler_1_t3126626844 * ___BlobAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobUpdated
	EventHandler_1_t3126626844 * ___BlobUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobRemoved
	EventHandler_1_t3126626844 * ___BlobRemoved_2;
	// System.Int32 TUIOsharp.DataProcessors.BlobProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::blobs
	Dictionary_2_t2974023424 * ___blobs_4;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::updatedBlobs
	List_1_t1262417539 * ___updatedBlobs_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::addedBlobs
	List_1_t128053199 * ___addedBlobs_6;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::removedBlobs
	List_1_t128053199 * ___removedBlobs_7;

public:
	inline static int32_t get_offset_of_BlobAdded_0() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobAdded_0)); }
	inline EventHandler_1_t3126626844 * get_BlobAdded_0() const { return ___BlobAdded_0; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobAdded_0() { return &___BlobAdded_0; }
	inline void set_BlobAdded_0(EventHandler_1_t3126626844 * value)
	{
		___BlobAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___BlobAdded_0), value);
	}

	inline static int32_t get_offset_of_BlobUpdated_1() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobUpdated_1)); }
	inline EventHandler_1_t3126626844 * get_BlobUpdated_1() const { return ___BlobUpdated_1; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobUpdated_1() { return &___BlobUpdated_1; }
	inline void set_BlobUpdated_1(EventHandler_1_t3126626844 * value)
	{
		___BlobUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___BlobUpdated_1), value);
	}

	inline static int32_t get_offset_of_BlobRemoved_2() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___BlobRemoved_2)); }
	inline EventHandler_1_t3126626844 * get_BlobRemoved_2() const { return ___BlobRemoved_2; }
	inline EventHandler_1_t3126626844 ** get_address_of_BlobRemoved_2() { return &___BlobRemoved_2; }
	inline void set_BlobRemoved_2(EventHandler_1_t3126626844 * value)
	{
		___BlobRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___BlobRemoved_2), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___U3CFrameNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_3() const { return ___U3CFrameNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_3() { return &___U3CFrameNumberU3Ek__BackingField_3; }
	inline void set_U3CFrameNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_blobs_4() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___blobs_4)); }
	inline Dictionary_2_t2974023424 * get_blobs_4() const { return ___blobs_4; }
	inline Dictionary_2_t2974023424 ** get_address_of_blobs_4() { return &___blobs_4; }
	inline void set_blobs_4(Dictionary_2_t2974023424 * value)
	{
		___blobs_4 = value;
		Il2CppCodeGenWriteBarrier((&___blobs_4), value);
	}

	inline static int32_t get_offset_of_updatedBlobs_5() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___updatedBlobs_5)); }
	inline List_1_t1262417539 * get_updatedBlobs_5() const { return ___updatedBlobs_5; }
	inline List_1_t1262417539 ** get_address_of_updatedBlobs_5() { return &___updatedBlobs_5; }
	inline void set_updatedBlobs_5(List_1_t1262417539 * value)
	{
		___updatedBlobs_5 = value;
		Il2CppCodeGenWriteBarrier((&___updatedBlobs_5), value);
	}

	inline static int32_t get_offset_of_addedBlobs_6() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___addedBlobs_6)); }
	inline List_1_t128053199 * get_addedBlobs_6() const { return ___addedBlobs_6; }
	inline List_1_t128053199 ** get_address_of_addedBlobs_6() { return &___addedBlobs_6; }
	inline void set_addedBlobs_6(List_1_t128053199 * value)
	{
		___addedBlobs_6 = value;
		Il2CppCodeGenWriteBarrier((&___addedBlobs_6), value);
	}

	inline static int32_t get_offset_of_removedBlobs_7() { return static_cast<int32_t>(offsetof(BlobProcessor_t2126871202, ___removedBlobs_7)); }
	inline List_1_t128053199 * get_removedBlobs_7() const { return ___removedBlobs_7; }
	inline List_1_t128053199 ** get_address_of_removedBlobs_7() { return &___removedBlobs_7; }
	inline void set_removedBlobs_7(List_1_t128053199 * value)
	{
		___removedBlobs_7 = value;
		Il2CppCodeGenWriteBarrier((&___removedBlobs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOBPROCESSOR_T2126871202_H
#ifndef LIST_1_T1262417539_H
#define LIST_1_T1262417539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct  List_1_t1262417539  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioBlobU5BU5D_t2287012000* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1262417539, ____items_1)); }
	inline TuioBlobU5BU5D_t2287012000* get__items_1() const { return ____items_1; }
	inline TuioBlobU5BU5D_t2287012000** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioBlobU5BU5D_t2287012000* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1262417539, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1262417539, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1262417539_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioBlobU5BU5D_t2287012000* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1262417539_StaticFields, ___EmptyArray_4)); }
	inline TuioBlobU5BU5D_t2287012000* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioBlobU5BU5D_t2287012000** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioBlobU5BU5D_t2287012000* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1262417539_H
#ifndef DICTIONARY_2_T2974023424_H
#define DICTIONARY_2_T2974023424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  Dictionary_2_t2974023424  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t385246372* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioBlobU5BU5D_t2287012000* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___keySlots_6)); }
	inline Int32U5BU5D_t385246372* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t385246372** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t385246372* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___valueSlots_7)); }
	inline TuioBlobU5BU5D_t2287012000* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioBlobU5BU5D_t2287012000** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioBlobU5BU5D_t2287012000* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2974023424_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1891350844 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2974023424_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1891350844 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1891350844 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1891350844 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2974023424_H
#ifndef KEYVALUEPAIR_2_T4091143164_H
#define KEYVALUEPAIR_2_T4091143164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  KeyValuePair_2_t4091143164 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioCursor_t2804757666 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4091143164, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4091143164, ___value_1)); }
	inline TuioCursor_t2804757666 * get_value_1() const { return ___value_1; }
	inline TuioCursor_t2804757666 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioCursor_t2804757666 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4091143164_H
#ifndef OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#define OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscMessageReceivedEventArgs
struct  OscMessageReceivedEventArgs_t2390219542  : public EventArgs_t3591816995
{
public:
	// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::<Message>k__BackingField
	OscMessage_t3323977005 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscMessageReceivedEventArgs_t2390219542, ___U3CMessageU3Ek__BackingField_1)); }
	inline OscMessage_t3323977005 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline OscMessage_t3323977005 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(OscMessage_t3323977005 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#ifndef TUIOCURSOREVENTARGS_T1894089282_H
#define TUIOCURSOREVENTARGS_T1894089282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct  TuioCursorEventArgs_t1894089282  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioCursor TUIOsharp.DataProcessors.TuioCursorEventArgs::Cursor
	TuioCursor_t2804757666 * ___Cursor_1;

public:
	inline static int32_t get_offset_of_Cursor_1() { return static_cast<int32_t>(offsetof(TuioCursorEventArgs_t1894089282, ___Cursor_1)); }
	inline TuioCursor_t2804757666 * get_Cursor_1() const { return ___Cursor_1; }
	inline TuioCursor_t2804757666 ** get_address_of_Cursor_1() { return &___Cursor_1; }
	inline void set_Cursor_1(TuioCursor_t2804757666 * value)
	{
		___Cursor_1 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOREVENTARGS_T1894089282_H
#ifndef KEYVALUEPAIR_2_T1289181373_H
#define KEYVALUEPAIR_2_T1289181373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct  KeyValuePair_2_t1289181373 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioObject_t2795875 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1289181373, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1289181373, ___value_1)); }
	inline TuioObject_t2795875 * get_value_1() const { return ___value_1; }
	inline TuioObject_t2795875 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioObject_t2795875 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1289181373_H
#ifndef TUIOOBJECTEVENTARGS_T236363764_H
#define TUIOOBJECTEVENTARGS_T236363764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct  TuioObjectEventArgs_t236363764  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioObject TUIOsharp.DataProcessors.TuioObjectEventArgs::Object
	TuioObject_t2795875 * ___Object_1;

public:
	inline static int32_t get_offset_of_Object_1() { return static_cast<int32_t>(offsetof(TuioObjectEventArgs_t236363764, ___Object_1)); }
	inline TuioObject_t2795875 * get_Object_1() const { return ___Object_1; }
	inline TuioObject_t2795875 ** get_address_of_Object_1() { return &___Object_1; }
	inline void set_Object_1(TuioObject_t2795875 * value)
	{
		___Object_1 = value;
		Il2CppCodeGenWriteBarrier((&___Object_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTEVENTARGS_T236363764_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef TUIOOBJECT_T2795875_H
#define TUIOOBJECT_T2795875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioObject
struct  TuioObject_t2795875  : public TuioEntity_t4229790798
{
public:
	// System.Int32 TUIOsharp.Entities.TuioObject::<ClassId>k__BackingField
	int32_t ___U3CClassIdU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioObject::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CClassIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CClassIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CClassIdU3Ek__BackingField_6() const { return ___U3CClassIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CClassIdU3Ek__BackingField_6() { return &___U3CClassIdU3Ek__BackingField_6; }
	inline void set_U3CClassIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CClassIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CAngleU3Ek__BackingField_7)); }
	inline float get_U3CAngleU3Ek__BackingField_7() const { return ___U3CAngleU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_7() { return &___U3CAngleU3Ek__BackingField_7; }
	inline void set_U3CAngleU3Ek__BackingField_7(float value)
	{
		___U3CAngleU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CRotationVelocityU3Ek__BackingField_8)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_8() const { return ___U3CRotationVelocityU3Ek__BackingField_8; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_8() { return &___U3CRotationVelocityU3Ek__BackingField_8; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_8(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioObject_t2795875, ___U3CRotationAccelerationU3Ek__BackingField_9)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_9() const { return ___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_9() { return &___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_9(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECT_T2795875_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef TUIOBLOB_T4085310093_H
#define TUIOBLOB_T4085310093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioBlob
struct  TuioBlob_t4085310093  : public TuioEntity_t4229790798
{
public:
	// System.Single TUIOsharp.Entities.TuioBlob::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioBlob::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioBlob::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioBlob::<Area>k__BackingField
	float ___U3CAreaU3Ek__BackingField_9;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_10;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CAngleU3Ek__BackingField_6)); }
	inline float get_U3CAngleU3Ek__BackingField_6() const { return ___U3CAngleU3Ek__BackingField_6; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_6() { return &___U3CAngleU3Ek__BackingField_6; }
	inline void set_U3CAngleU3Ek__BackingField_6(float value)
	{
		___U3CAngleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CWidthU3Ek__BackingField_7)); }
	inline float get_U3CWidthU3Ek__BackingField_7() const { return ___U3CWidthU3Ek__BackingField_7; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_7() { return &___U3CWidthU3Ek__BackingField_7; }
	inline void set_U3CWidthU3Ek__BackingField_7(float value)
	{
		___U3CWidthU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CHeightU3Ek__BackingField_8)); }
	inline float get_U3CHeightU3Ek__BackingField_8() const { return ___U3CHeightU3Ek__BackingField_8; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_8() { return &___U3CHeightU3Ek__BackingField_8; }
	inline void set_U3CHeightU3Ek__BackingField_8(float value)
	{
		___U3CHeightU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CAreaU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CAreaU3Ek__BackingField_9)); }
	inline float get_U3CAreaU3Ek__BackingField_9() const { return ___U3CAreaU3Ek__BackingField_9; }
	inline float* get_address_of_U3CAreaU3Ek__BackingField_9() { return &___U3CAreaU3Ek__BackingField_9; }
	inline void set_U3CAreaU3Ek__BackingField_9(float value)
	{
		___U3CAreaU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CRotationVelocityU3Ek__BackingField_10)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_10() const { return ___U3CRotationVelocityU3Ek__BackingField_10; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_10() { return &___U3CRotationVelocityU3Ek__BackingField_10; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_10(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TuioBlob_t4085310093, ___U3CRotationAccelerationU3Ek__BackingField_11)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_11() const { return ___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_11() { return &___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_11(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOB_T4085310093_H
#ifndef TUIOBLOBEVENTARGS_T907500115_H
#define TUIOBLOBEVENTARGS_T907500115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct  TuioBlobEventArgs_t907500115  : public EventArgs_t3591816995
{
public:
	// TUIOsharp.Entities.TuioBlob TUIOsharp.DataProcessors.TuioBlobEventArgs::Blob
	TuioBlob_t4085310093 * ___Blob_1;

public:
	inline static int32_t get_offset_of_Blob_1() { return static_cast<int32_t>(offsetof(TuioBlobEventArgs_t907500115, ___Blob_1)); }
	inline TuioBlob_t4085310093 * get_Blob_1() const { return ___Blob_1; }
	inline TuioBlob_t4085310093 ** get_address_of_Blob_1() { return &___Blob_1; }
	inline void set_Blob_1(TuioBlob_t4085310093 * value)
	{
		___Blob_1 = value;
		Il2CppCodeGenWriteBarrier((&___Blob_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOBEVENTARGS_T907500115_H
#ifndef KEYVALUEPAIR_2_T1076728295_H
#define KEYVALUEPAIR_2_T1076728295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  KeyValuePair_2_t1076728295 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioBlob_t4085310093 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1076728295, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1076728295, ___value_1)); }
	inline TuioBlob_t4085310093 * get_value_1() const { return ___value_1; }
	inline TuioBlob_t4085310093 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioBlob_t4085310093 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1076728295_H
#ifndef KEYVALUEPAIR_2_T71524366_H
#define KEYVALUEPAIR_2_T71524366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t71524366 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t71524366, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T71524366_H
#ifndef TUIOCURSOR_T2804757666_H
#define TUIOCURSOR_T2804757666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioCursor
struct  TuioCursor_t2804757666  : public TuioEntity_t4229790798
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOR_T2804757666_H
#ifndef EXCEPTIONEVENTARGS_T2395319211_H
#define EXCEPTIONEVENTARGS_T2395319211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.ExceptionEventArgs
struct  ExceptionEventArgs_t2395319211  : public EventArgs_t3591816995
{
public:
	// System.Exception OSCsharp.Utils.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionEventArgs_t2395319211, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T2395319211_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef OSCMESSAGE_T3323977005_H
#define OSCMESSAGE_T3323977005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t3323977005  : public OscPacket_t3204151022
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_20;

public:
	inline static int32_t get_offset_of_typeTag_20() { return static_cast<int32_t>(offsetof(OscMessage_t3323977005, ___typeTag_20)); }
	inline String_t* get_typeTag_20() const { return ___typeTag_20; }
	inline String_t** get_address_of_typeTag_20() { return &___typeTag_20; }
	inline void set_typeTag_20(String_t* value)
	{
		___typeTag_20 = value;
		Il2CppCodeGenWriteBarrier((&___typeTag_20), value);
	}
};

struct OscMessage_t3323977005_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OSCsharp.Data.OscMessage::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_21() { return static_cast<int32_t>(offsetof(OscMessage_t3323977005_StaticFields, ___U3CU3Ef__switchU24map0_21)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_21() const { return ___U3CU3Ef__switchU24map0_21; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_21() { return &___U3CU3Ef__switchU24map0_21; }
	inline void set_U3CU3Ef__switchU24map0_21(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGE_T3323977005_H
#ifndef TRANSMISSIONTYPE_T3895608725_H
#define TRANSMISSIONTYPE_T3895608725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.TransmissionType
struct  TransmissionType_t3895608725 
{
public:
	// System.Int32 OSCsharp.Net.TransmissionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransmissionType_t3895608725, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONTYPE_T3895608725_H
#ifndef ENUMERATOR_T3923002270_H
#define ENUMERATOR_T3923002270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct  Enumerator_t3923002270 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1968819495 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t71524366  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3923002270, ___dictionary_0)); }
	inline Dictionary_2_t1968819495 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1968819495 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1968819495 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3923002270, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3923002270, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3923002270, ___current_3)); }
	inline KeyValuePair_2_t71524366  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t71524366 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t71524366  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3923002270_H
#ifndef ENUMERATOR_T633238903_H
#define ENUMERATOR_T633238903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  Enumerator_t633238903 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2974023424 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1076728295  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t633238903, ___dictionary_0)); }
	inline Dictionary_2_t2974023424 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2974023424 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2974023424 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t633238903, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t633238903, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t633238903, ___current_3)); }
	inline KeyValuePair_2_t1076728295  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1076728295 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1076728295  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T633238903_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ENUMERATOR_T3647653772_H
#define ENUMERATOR_T3647653772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  Enumerator_t3647653772 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1693470997 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t4091143164  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3647653772, ___dictionary_0)); }
	inline Dictionary_2_t1693470997 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1693470997 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1693470997 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3647653772, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3647653772, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3647653772, ___current_3)); }
	inline KeyValuePair_2_t4091143164  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t4091143164 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t4091143164  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3647653772_H
#ifndef ENUMERATOR_T845691981_H
#define ENUMERATOR_T845691981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>
struct  Enumerator_t845691981 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3186476502 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1289181373  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t845691981, ___dictionary_0)); }
	inline Dictionary_2_t3186476502 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3186476502 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3186476502 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t845691981, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t845691981, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t845691981, ___current_3)); }
	inline KeyValuePair_2_t1289181373  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1289181373 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1289181373  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T845691981_H
#ifndef UDPRECEIVER_T1881969775_H
#define UDPRECEIVER_T1881969775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver
struct  UDPReceiver_t1881969775  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs> OSCsharp.Net.UDPReceiver::PacketReceived
	EventHandler_1_t2882212236 * ___PacketReceived_0;
	// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs> OSCsharp.Net.UDPReceiver::BundleReceived
	EventHandler_1_t2793618168 * ___BundleReceived_1;
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> OSCsharp.Net.UDPReceiver::ErrorOccured
	EventHandler_1_t319478644 * ___ErrorOccured_2;
	// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs> OSCsharp.Net.UDPReceiver::messageReceivedInvoker
	EventHandler_1_t314378975 * ___messageReceivedInvoker_3;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<IPAddress>k__BackingField
	IPAddress_t241777590 * ___U3CIPAddressU3Ek__BackingField_4;
	// System.Int32 OSCsharp.Net.UDPReceiver::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_5;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<MulticastAddress>k__BackingField
	IPAddress_t241777590 * ___U3CMulticastAddressU3Ek__BackingField_6;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::<IPEndPoint>k__BackingField
	IPEndPoint_t3791887218 * ___U3CIPEndPointU3Ek__BackingField_7;
	// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::<TransmissionType>k__BackingField
	int32_t ___U3CTransmissionTypeU3Ek__BackingField_8;
	// System.Boolean OSCsharp.Net.UDPReceiver::<ConsumeParsingExceptions>k__BackingField
	bool ___U3CConsumeParsingExceptionsU3Ek__BackingField_9;
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver::udpClient
	UdpClient_t967282006 * ___udpClient_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) OSCsharp.Net.UDPReceiver::acceptingConnections
	bool ___acceptingConnections_11;
	// System.AsyncCallback OSCsharp.Net.UDPReceiver::callback
	AsyncCallback_t3962456242 * ___callback_12;

public:
	inline static int32_t get_offset_of_PacketReceived_0() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___PacketReceived_0)); }
	inline EventHandler_1_t2882212236 * get_PacketReceived_0() const { return ___PacketReceived_0; }
	inline EventHandler_1_t2882212236 ** get_address_of_PacketReceived_0() { return &___PacketReceived_0; }
	inline void set_PacketReceived_0(EventHandler_1_t2882212236 * value)
	{
		___PacketReceived_0 = value;
		Il2CppCodeGenWriteBarrier((&___PacketReceived_0), value);
	}

	inline static int32_t get_offset_of_BundleReceived_1() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___BundleReceived_1)); }
	inline EventHandler_1_t2793618168 * get_BundleReceived_1() const { return ___BundleReceived_1; }
	inline EventHandler_1_t2793618168 ** get_address_of_BundleReceived_1() { return &___BundleReceived_1; }
	inline void set_BundleReceived_1(EventHandler_1_t2793618168 * value)
	{
		___BundleReceived_1 = value;
		Il2CppCodeGenWriteBarrier((&___BundleReceived_1), value);
	}

	inline static int32_t get_offset_of_ErrorOccured_2() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___ErrorOccured_2)); }
	inline EventHandler_1_t319478644 * get_ErrorOccured_2() const { return ___ErrorOccured_2; }
	inline EventHandler_1_t319478644 ** get_address_of_ErrorOccured_2() { return &___ErrorOccured_2; }
	inline void set_ErrorOccured_2(EventHandler_1_t319478644 * value)
	{
		___ErrorOccured_2 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_2), value);
	}

	inline static int32_t get_offset_of_messageReceivedInvoker_3() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___messageReceivedInvoker_3)); }
	inline EventHandler_1_t314378975 * get_messageReceivedInvoker_3() const { return ___messageReceivedInvoker_3; }
	inline EventHandler_1_t314378975 ** get_address_of_messageReceivedInvoker_3() { return &___messageReceivedInvoker_3; }
	inline void set_messageReceivedInvoker_3(EventHandler_1_t314378975 * value)
	{
		___messageReceivedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedInvoker_3), value);
	}

	inline static int32_t get_offset_of_U3CIPAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CIPAddressU3Ek__BackingField_4)); }
	inline IPAddress_t241777590 * get_U3CIPAddressU3Ek__BackingField_4() const { return ___U3CIPAddressU3Ek__BackingField_4; }
	inline IPAddress_t241777590 ** get_address_of_U3CIPAddressU3Ek__BackingField_4() { return &___U3CIPAddressU3Ek__BackingField_4; }
	inline void set_U3CIPAddressU3Ek__BackingField_4(IPAddress_t241777590 * value)
	{
		___U3CIPAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPAddressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CPortU3Ek__BackingField_5)); }
	inline int32_t get_U3CPortU3Ek__BackingField_5() const { return ___U3CPortU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_5() { return &___U3CPortU3Ek__BackingField_5; }
	inline void set_U3CPortU3Ek__BackingField_5(int32_t value)
	{
		___U3CPortU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMulticastAddressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CMulticastAddressU3Ek__BackingField_6)); }
	inline IPAddress_t241777590 * get_U3CMulticastAddressU3Ek__BackingField_6() const { return ___U3CMulticastAddressU3Ek__BackingField_6; }
	inline IPAddress_t241777590 ** get_address_of_U3CMulticastAddressU3Ek__BackingField_6() { return &___U3CMulticastAddressU3Ek__BackingField_6; }
	inline void set_U3CMulticastAddressU3Ek__BackingField_6(IPAddress_t241777590 * value)
	{
		___U3CMulticastAddressU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMulticastAddressU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CIPEndPointU3Ek__BackingField_7)); }
	inline IPEndPoint_t3791887218 * get_U3CIPEndPointU3Ek__BackingField_7() const { return ___U3CIPEndPointU3Ek__BackingField_7; }
	inline IPEndPoint_t3791887218 ** get_address_of_U3CIPEndPointU3Ek__BackingField_7() { return &___U3CIPEndPointU3Ek__BackingField_7; }
	inline void set_U3CIPEndPointU3Ek__BackingField_7(IPEndPoint_t3791887218 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTransmissionTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CTransmissionTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CTransmissionTypeU3Ek__BackingField_8() const { return ___U3CTransmissionTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CTransmissionTypeU3Ek__BackingField_8() { return &___U3CTransmissionTypeU3Ek__BackingField_8; }
	inline void set_U3CTransmissionTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CTransmissionTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CConsumeParsingExceptionsU3Ek__BackingField_9)); }
	inline bool get_U3CConsumeParsingExceptionsU3Ek__BackingField_9() const { return ___U3CConsumeParsingExceptionsU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CConsumeParsingExceptionsU3Ek__BackingField_9() { return &___U3CConsumeParsingExceptionsU3Ek__BackingField_9; }
	inline void set_U3CConsumeParsingExceptionsU3Ek__BackingField_9(bool value)
	{
		___U3CConsumeParsingExceptionsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_udpClient_10() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___udpClient_10)); }
	inline UdpClient_t967282006 * get_udpClient_10() const { return ___udpClient_10; }
	inline UdpClient_t967282006 ** get_address_of_udpClient_10() { return &___udpClient_10; }
	inline void set_udpClient_10(UdpClient_t967282006 * value)
	{
		___udpClient_10 = value;
		Il2CppCodeGenWriteBarrier((&___udpClient_10), value);
	}

	inline static int32_t get_offset_of_acceptingConnections_11() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___acceptingConnections_11)); }
	inline bool get_acceptingConnections_11() const { return ___acceptingConnections_11; }
	inline bool* get_address_of_acceptingConnections_11() { return &___acceptingConnections_11; }
	inline void set_acceptingConnections_11(bool value)
	{
		___acceptingConnections_11 = value;
	}

	inline static int32_t get_offset_of_callback_12() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___callback_12)); }
	inline AsyncCallback_t3962456242 * get_callback_12() const { return ___callback_12; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_12() { return &___callback_12; }
	inline void set_callback_12(AsyncCallback_t3962456242 * value)
	{
		___callback_12 = value;
		Il2CppCodeGenWriteBarrier((&___callback_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPRECEIVER_T1881969775_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef EVENTHANDLER_1_T2455490493_H
#define EVENTHANDLER_1_T2455490493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct  EventHandler_1_t2455490493  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2455490493_H
#ifndef EVENTHANDLER_1_T4113216011_H
#define EVENTHANDLER_1_T4113216011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct  EventHandler_1_t4113216011  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T4113216011_H
#ifndef EVENTHANDLER_1_T3126626844_H
#define EVENTHANDLER_1_T3126626844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct  EventHandler_1_t3126626844  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T3126626844_H
#ifndef EVENTHANDLER_1_T314378975_H
#define EVENTHANDLER_1_T314378975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct  EventHandler_1_t314378975  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T314378975_H
#ifndef EVENTHANDLER_1_T319478644_H
#define EVENTHANDLER_1_T319478644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct  EventHandler_1_t319478644  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T319478644_H


// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m252876485_gshared (Dictionary_2_t1968819495 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1628857705_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3167860287_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m697420525_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3923002270  Dictionary_2_GetEnumerator_m1087370259_gshared (Dictionary_2_t1968819495 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t71524366  Enumerator_get_Current_m3431285658_gshared (Enumerator_t3923002270 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_gshared (KeyValuePair_2_t71524366 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(!0)
extern "C"  bool List_1_Contains_m4250047412_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
extern "C"  bool List_1_Remove_m3037048099_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3398155861_gshared (Enumerator_t3923002270 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m562365603_gshared (Enumerator_t3923002270 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2382293057_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2059424751_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m4124830036_gshared (EventHandler_1_t1004265597 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m361000296_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1910578371_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m107019914_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m4193450060_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m2154023298_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventHandler_1__ctor_m699822512_gshared (EventHandler_1_t1004265597 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m2654125393_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::.ctor()
#define Dictionary_2__ctor_m3077278521(__this, method) ((  void (*) (Dictionary_2_t2974023424 *, const RuntimeMethod*))Dictionary_2__ctor_m252876485_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor()
#define List_1__ctor_m2209108566(__this, method) ((  void (*) (List_1_t1262417539 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1628857705(__this, method) ((  void (*) (List_1_t128053199 *, const RuntimeMethod*))List_1__ctor_m1628857705_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String OSCsharp.Data.OscPacket::get_Address()
extern "C"  String_t* OscPacket_get_Address_m2654232420 (OscPacket_t3204151022 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m3397942478 (OscPacket_t3204151022 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m4133274017(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2974023424 *, int32_t, TuioBlob_t4085310093 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3167860287_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32)
extern "C"  void TuioBlob__ctor_m3494057218 (TuioBlob_t4085310093 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob_Update_m2887677996 (TuioBlob_t4085310093 * __this, float ___x0, float ___y1, float ___angle2, float ___width3, float ___height4, float ___area5, float ___velocityX6, float ___velocityY7, float ___rotationVelocity8, float ___acceleration9, float ___rotationAcceleration10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Add(!0)
#define List_1_Add_m689584216(__this, p0, method) ((  void (*) (List_1_t1262417539 *, TuioBlob_t4085310093 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m697420525(__this, p0, method) ((  void (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_Add_m697420525_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2841467664(__this, method) ((  Enumerator_t633238903  (*) (Dictionary_2_t2974023424 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m1087370259_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Current()
#define Enumerator_get_Current_m4138203631(__this, method) ((  KeyValuePair_2_t1076728295  (*) (Enumerator_t633238903 *, const RuntimeMethod*))Enumerator_get_Current_m3431285658_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Key()
#define KeyValuePair_2_get_Key_m1056191967(__this, method) ((  int32_t (*) (KeyValuePair_2_t1076728295 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1839753989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(!0)
#define List_1_Contains_m4250047412(__this, p0, method) ((  bool (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_Contains_m4250047412_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
#define List_1_Remove_m3037048099(__this, p0, method) ((  bool (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_Remove_m3037048099_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::MoveNext()
#define Enumerator_MoveNext_m1031836987(__this, method) ((  bool (*) (Enumerator_t633238903 *, const RuntimeMethod*))Enumerator_MoveNext_m3398155861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::Dispose()
#define Enumerator_Dispose_m1857637826(__this, method) ((  void (*) (Enumerator_t633238903 *, const RuntimeMethod*))Enumerator_Dispose_m562365603_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.BlobProcessor::set_FrameNumber(System.Int32)
extern "C"  void BlobProcessor_set_FrameNumber_m4211722092 (BlobProcessor_t2126871202 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Count()
#define List_1_get_Count_m1699732805(__this, method) ((  int32_t (*) (List_1_t1262417539 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Item(System.Int32)
#define List_1_get_Item_m1456013675(__this, p0, method) ((  TuioBlob_t4085310093 * (*) (List_1_t1262417539 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 TUIOsharp.Entities.TuioEntity::get_Id()
extern "C"  int32_t TuioEntity_get_Id_m3362535295 (TuioEntity_t4229790798 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m987622744(__this, p0, method) ((  bool (*) (Dictionary_2_t2974023424 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m2382293057_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::Add(!0,!1)
#define Dictionary_2_Add_m1090758517(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2974023424 *, int32_t, TuioBlob_t4085310093 *, const RuntimeMethod*))Dictionary_2_Add_m2059424751_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioBlobEventArgs::.ctor(TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioBlobEventArgs__ctor_m1030032409 (TuioBlobEventArgs_t907500115 * __this, TuioBlob_t4085310093 * ___blob0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m4027566236(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3126626844 *, RuntimeObject *, TuioBlobEventArgs_t907500115 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m361000296(__this, method) ((  int32_t (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_get_Count_m361000296_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m1910578371(__this, p0, method) ((  int32_t (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1910578371_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Item(!0)
#define Dictionary_2_get_Item_m4020322656(__this, p0, method) ((  TuioBlob_t4085310093 * (*) (Dictionary_2_t2974023424 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m107019914_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::Remove(!0)
#define Dictionary_2_Remove_m911377797(__this, p0, method) ((  bool (*) (Dictionary_2_t2974023424 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m4193450060_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
#define List_1_Clear_m2154023298(__this, method) ((  void (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_Clear_m2154023298_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Clear()
#define List_1_Clear_m4267157696(__this, method) ((  void (*) (List_1_t1262417539 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::.ctor()
#define Dictionary_2__ctor_m4060431757(__this, method) ((  void (*) (Dictionary_2_t1693470997 *, const RuntimeMethod*))Dictionary_2__ctor_m252876485_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor()
#define List_1__ctor_m546517191(__this, method) ((  void (*) (List_1_t4276832408 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2949425533(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1693470997 *, int32_t, TuioCursor_t2804757666 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3167860287_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32)
extern "C"  void TuioCursor__ctor_m1466983245 (TuioCursor_t2804757666 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioCursor::Update(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor_Update_m1099848212 (TuioCursor_t2804757666 * __this, float ___x0, float ___y1, float ___velocityX2, float ___velocityY3, float ___acceleration4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Add(!0)
#define List_1_Add_m3374480747(__this, p0, method) ((  void (*) (List_1_t4276832408 *, TuioCursor_t2804757666 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3436297861(__this, method) ((  Enumerator_t3647653772  (*) (Dictionary_2_t1693470997 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m1087370259_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Current()
#define Enumerator_get_Current_m1124426651(__this, method) ((  KeyValuePair_2_t4091143164  (*) (Enumerator_t3647653772 *, const RuntimeMethod*))Enumerator_get_Current_m3431285658_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Key()
#define KeyValuePair_2_get_Key_m2249459862(__this, method) ((  int32_t (*) (KeyValuePair_2_t4091143164 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1839753989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::MoveNext()
#define Enumerator_MoveNext_m4072011572(__this, method) ((  bool (*) (Enumerator_t3647653772 *, const RuntimeMethod*))Enumerator_MoveNext_m3398155861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::Dispose()
#define Enumerator_Dispose_m3655613863(__this, method) ((  void (*) (Enumerator_t3647653772 *, const RuntimeMethod*))Enumerator_Dispose_m562365603_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.CursorProcessor::set_FrameNumber(System.Int32)
extern "C"  void CursorProcessor_set_FrameNumber_m730012964 (CursorProcessor_t3936223090 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Count()
#define List_1_get_Count_m2851265837(__this, method) ((  int32_t (*) (List_1_t4276832408 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Item(System.Int32)
#define List_1_get_Item_m1501378545(__this, p0, method) ((  TuioCursor_t2804757666 * (*) (List_1_t4276832408 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m4292169967(__this, p0, method) ((  bool (*) (Dictionary_2_t1693470997 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m2382293057_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::Add(!0,!1)
#define Dictionary_2_Add_m4211285854(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1693470997 *, int32_t, TuioCursor_t2804757666 *, const RuntimeMethod*))Dictionary_2_Add_m2059424751_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioCursorEventArgs::.ctor(TUIOsharp.Entities.TuioCursor)
extern "C"  void TuioCursorEventArgs__ctor_m138848966 (TuioCursorEventArgs_t1894089282 * __this, TuioCursor_t2804757666 * ___cursor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2655476952(__this, p0, p1, method) ((  void (*) (EventHandler_1_t4113216011 *, RuntimeObject *, TuioCursorEventArgs_t1894089282 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Item(!0)
#define Dictionary_2_get_Item_m2068205222(__this, p0, method) ((  TuioCursor_t2804757666 * (*) (Dictionary_2_t1693470997 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m107019914_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::Remove(!0)
#define Dictionary_2_Remove_m2147751717(__this, p0, method) ((  bool (*) (Dictionary_2_t1693470997 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m4193450060_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Clear()
#define List_1_Clear_m2024113527(__this, method) ((  void (*) (List_1_t4276832408 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::.ctor()
#define Dictionary_2__ctor_m3393625636(__this, method) ((  void (*) (Dictionary_2_t3186476502 *, const RuntimeMethod*))Dictionary_2__ctor_m252876485_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::.ctor()
#define List_1__ctor_m1635335082(__this, method) ((  void (*) (List_1_t1474870617 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3350852866(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t3186476502 *, int32_t, TuioObject_t2795875 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3167860287_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32)
extern "C"  void TuioObject__ctor_m1545049101 (TuioObject_t2795875 * __this, int32_t ___id0, int32_t ___classId1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject_Update_m1076492596 (TuioObject_t2795875 * __this, float ___x0, float ___y1, float ___angle2, float ___velocityX3, float ___velocityY4, float ___rotationVelocity5, float ___acceleration6, float ___rotationAcceleration7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::Add(!0)
#define List_1_Add_m3017127986(__this, p0, method) ((  void (*) (List_1_t1474870617 *, TuioObject_t2795875 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3493560753(__this, method) ((  Enumerator_t845691981  (*) (Dictionary_2_t3186476502 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m1087370259_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::get_Current()
#define Enumerator_get_Current_m702359707(__this, method) ((  KeyValuePair_2_t1289181373  (*) (Enumerator_t845691981 *, const RuntimeMethod*))Enumerator_get_Current_m3431285658_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioObject>::get_Key()
#define KeyValuePair_2_get_Key_m3626592599(__this, method) ((  int32_t (*) (KeyValuePair_2_t1289181373 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1839753989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::MoveNext()
#define Enumerator_MoveNext_m4089973788(__this, method) ((  bool (*) (Enumerator_t845691981 *, const RuntimeMethod*))Enumerator_MoveNext_m3398155861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::Dispose()
#define Enumerator_Dispose_m1218014203(__this, method) ((  void (*) (Enumerator_t845691981 *, const RuntimeMethod*))Enumerator_Dispose_m562365603_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::set_FrameNumber(System.Int32)
extern "C"  void ObjectProcessor_set_FrameNumber_m3146742338 (ObjectProcessor_t2877610401 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::get_Count()
#define List_1_get_Count_m1437897001(__this, method) ((  int32_t (*) (List_1_t1474870617 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::get_Item(System.Int32)
#define List_1_get_Item_m1972841044(__this, p0, method) ((  TuioObject_t2795875 * (*) (List_1_t1474870617 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m3922808659(__this, p0, method) ((  bool (*) (Dictionary_2_t3186476502 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m2382293057_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::Add(!0,!1)
#define Dictionary_2_Add_m357742037(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3186476502 *, int32_t, TuioObject_t2795875 *, const RuntimeMethod*))Dictionary_2_Add_m2059424751_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioObjectEventArgs::.ctor(TUIOsharp.Entities.TuioObject)
extern "C"  void TuioObjectEventArgs__ctor_m3321218798 (TuioObjectEventArgs_t236363764 * __this, TuioObject_t2795875 * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m1236698408(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2455490493 *, RuntimeObject *, TuioObjectEventArgs_t236363764 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::get_Item(!0)
#define Dictionary_2_get_Item_m587101098(__this, p0, method) ((  TuioObject_t2795875 * (*) (Dictionary_2_t3186476502 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m107019914_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::Remove(!0)
#define Dictionary_2_Remove_m404394798(__this, p0, method) ((  bool (*) (Dictionary_2_t3186476502 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m4193450060_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::Clear()
#define List_1_Clear_m227439264(__this, method) ((  void (*) (List_1_t1474870617 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m32674013 (EventArgs_t3591816995 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob__ctor_m2376717690 (TuioBlob_t4085310093 * __this, int32_t ___id0, float ___x1, float ___y2, float ___angle3, float ___width4, float ___height5, float ___area6, float ___velocityX7, float ___velocityY8, float ___rotationVelocity9, float ___acceleration10, float ___rotationAcceleration11, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioEntity__ctor_m3445181350 (TuioEntity_t4229790798 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Angle(System.Single)
extern "C"  void TuioBlob_set_Angle_m3346400918 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Width(System.Single)
extern "C"  void TuioBlob_set_Width_m2177051861 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Height(System.Single)
extern "C"  void TuioBlob_set_Height_m222606430 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Area(System.Single)
extern "C"  void TuioBlob_set_Area_m3038579628 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationVelocity(System.Single)
extern "C"  void TuioBlob_set_RotationVelocity_m854345461 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationAcceleration(System.Single)
extern "C"  void TuioBlob_set_RotationAcceleration_m3695130551 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_X(System.Single)
extern "C"  void TuioEntity_set_X_m3721568381 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Y(System.Single)
extern "C"  void TuioEntity_set_Y_m3176374475 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityX(System.Single)
extern "C"  void TuioEntity_set_VelocityX_m7749035 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityY(System.Single)
extern "C"  void TuioEntity_set_VelocityY_m4294262286 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Acceleration(System.Single)
extern "C"  void TuioEntity_set_Acceleration_m270679629 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor__ctor_m4081665409 (TuioCursor_t2804757666 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Id(System.Int32)
extern "C"  void TuioEntity_set_Id_m2368423484 (TuioEntity_t4229790798 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject__ctor_m2087415962 (TuioObject_t2795875 * __this, int32_t ___id0, int32_t ___classId1, float ___x2, float ___y3, float ___angle4, float ___velocityX5, float ___velocityY6, float ___rotationVelocity7, float ___acceleration8, float ___rotationAcceleration9, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_ClassId(System.Int32)
extern "C"  void TuioObject_set_ClassId_m1315785328 (TuioObject_t2795875 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_Angle(System.Single)
extern "C"  void TuioObject_set_Angle_m3294417457 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_RotationVelocity(System.Single)
extern "C"  void TuioObject_set_RotationVelocity_m3598322238 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_RotationAcceleration(System.Single)
extern "C"  void TuioObject_set_RotationAcceleration_m683552439 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor()
#define List_1__ctor_m211060356(__this, method) ((  void (*) (List_1_t2674567522 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void TUIOsharp.TuioServer::set_Port(System.Int32)
extern "C"  void TuioServer_set_Port_m3406601027 (TuioServer_t3158170151 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 TUIOsharp.TuioServer::get_Port()
extern "C"  int32_t TuioServer_get_Port_m2570413809 (TuioServer_t3158170151 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m588051875 (UDPReceiver_t1881969775 * __this, int32_t p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m871205231(__this, p0, p1, method) ((  void (*) (EventHandler_1_t314378975 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m699822512_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver::add_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_add_MessageReceived_m2005693360 (UDPReceiver_t1881969775 * __this, EventHandler_1_t314378975 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m951552470(__this, p0, p1, method) ((  void (*) (EventHandler_1_t319478644 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m699822512_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver::add_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_add_ErrorOccured_m2838983283 (UDPReceiver_t1881969775 * __this, EventHandler_1_t319478644 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_IsRunning()
extern "C"  bool UDPReceiver_get_IsRunning_m1711451 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Start()
extern "C"  void UDPReceiver_Start_m2568122436 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Stop()
extern "C"  void UDPReceiver_Stop_m2310297977 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Contains(!0)
#define List_1_Contains_m1021282117(__this, p0, method) ((  bool (*) (List_1_t2674567522 *, RuntimeObject*, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Add(!0)
#define List_1_Add_m2077402128(__this, p0, method) ((  void (*) (List_1_t2674567522 *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Remove(!0)
#define List_1_Remove_m2487744439(__this, p0, method) ((  bool (*) (List_1_t2674567522 *, RuntimeObject*, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Clear()
#define List_1_Clear_m3700974216(__this, method) ((  void (*) (List_1_t2674567522 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Count()
#define List_1_get_Count_m2207940050(__this, method) ((  int32_t (*) (List_1_t2674567522 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Item(System.Int32)
#define List_1_get_Item_m3750012149(__this, p0, method) ((  RuntimeObject* (*) (List_1_t2674567522 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3384041340(__this, p0, p1, method) ((  void (*) (EventHandler_1_t319478644 *, RuntimeObject *, ExceptionEventArgs_t2395319211 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::get_Message()
extern "C"  OscMessage_t3323977005 * OscMessageReceivedEventArgs_get_Message_m2617929226 (OscMessageReceivedEventArgs_t2390219542 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.TuioServer::processMessage(OSCsharp.Data.OscMessage)
extern "C"  void TuioServer_processMessage_m1808445210 (TuioServer_t3158170151 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobAdded_m3279817448 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobAdded_m3279817448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobAdded_0();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobAdded_m1162188030 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobAdded_m1162188030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobAdded_0();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobUpdated_m4053757175 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobUpdated_m4053757175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobUpdated_1();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobUpdated_m3661772473 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobUpdated_m3661772473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobUpdated_1();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobRemoved_m2881795667 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobRemoved_m2881795667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobRemoved_2();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobRemoved_m387963999 (BlobProcessor_t2126871202 * __this, EventHandler_1_t3126626844 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobRemoved_m387963999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3126626844 * V_0 = NULL;
	EventHandler_1_t3126626844 * V_1 = NULL;
	{
		EventHandler_1_t3126626844 * L_0 = __this->get_BlobRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3126626844 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3126626844 ** L_2 = __this->get_address_of_BlobRemoved_2();
		EventHandler_1_t3126626844 * L_3 = V_1;
		EventHandler_1_t3126626844 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t3126626844 * L_6 = V_0;
		EventHandler_1_t3126626844 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t3126626844 *>(L_2, ((EventHandler_1_t3126626844 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t3126626844_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t3126626844 * L_8 = V_0;
		EventHandler_1_t3126626844 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3126626844 *)L_8) == ((RuntimeObject*)(EventHandler_1_t3126626844 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::set_FrameNumber(System.Int32)
extern "C"  void BlobProcessor_set_FrameNumber_m4211722092 (BlobProcessor_t2126871202 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::.ctor()
extern "C"  void BlobProcessor__ctor_m1296745391 (BlobProcessor_t2126871202 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor__ctor_m1296745391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2974023424 * L_0 = (Dictionary_2_t2974023424 *)il2cpp_codegen_object_new(Dictionary_2_t2974023424_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3077278521(L_0, /*hidden argument*/Dictionary_2__ctor_m3077278521_RuntimeMethod_var);
		__this->set_blobs_4(L_0);
		List_1_t1262417539 * L_1 = (List_1_t1262417539 *)il2cpp_codegen_object_new(List_1_t1262417539_il2cpp_TypeInfo_var);
		List_1__ctor_m2209108566(L_1, /*hidden argument*/List_1__ctor_m2209108566_RuntimeMethod_var);
		__this->set_updatedBlobs_5(L_1);
		List_1_t128053199 * L_2 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_2, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_addedBlobs_6(L_2);
		List_1_t128053199 * L_3 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_3, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_removedBlobs_7(L_3);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void BlobProcessor_ProcessMessage_m2788254080 (BlobProcessor_t2126871202 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_ProcessMessage_m2788254080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	TuioBlob_t4085310093 * V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	KeyValuePair_2_t1076728295  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Enumerator_t633238903  V_17;
	memset(&V_17, 0, sizeof(V_17));
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	TuioBlob_t4085310093 * V_20 = NULL;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t3323977005 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m2654232420(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, _stringLiteral3983133878, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		OscMessage_t3323977005 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m3397942478(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		if (!L_7)
		{
			goto IL_03d2;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral2553217811, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral1569063519, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_01a3;
		}
	}
	{
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral4243243339, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_025f;
		}
	}
	{
		goto IL_03d2;
	}

IL_0063:
	{
		OscMessage_t3323977005 * L_14 = ___message0;
		NullCheck(L_14);
		RuntimeObject* L_15 = OscPacket_get_Data_m3397942478(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_15);
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)13))))
		{
			goto IL_0076;
		}
	}
	{
		return;
	}

IL_0076:
	{
		OscMessage_t3323977005 * L_17 = ___message0;
		NullCheck(L_17);
		RuntimeObject* L_18 = OscPacket_get_Data_m3397942478(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		RuntimeObject * L_19 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_18, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_19, Int32_t2950945753_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_20 = ___message0;
		NullCheck(L_20);
		RuntimeObject* L_21 = OscPacket_get_Data_m3397942478(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		RuntimeObject * L_22 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_21, 2);
		V_2 = ((*(float*)((float*)UnBox(L_22, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_23 = ___message0;
		NullCheck(L_23);
		RuntimeObject* L_24 = OscPacket_get_Data_m3397942478(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject * L_25 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_24, 3);
		V_3 = ((*(float*)((float*)UnBox(L_25, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_26 = ___message0;
		NullCheck(L_26);
		RuntimeObject* L_27 = OscPacket_get_Data_m3397942478(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		RuntimeObject * L_28 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_27, 4);
		V_4 = ((*(float*)((float*)UnBox(L_28, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_29 = ___message0;
		NullCheck(L_29);
		RuntimeObject* L_30 = OscPacket_get_Data_m3397942478(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		RuntimeObject * L_31 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_30, 5);
		V_5 = ((*(float*)((float*)UnBox(L_31, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_32 = ___message0;
		NullCheck(L_32);
		RuntimeObject* L_33 = OscPacket_get_Data_m3397942478(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		RuntimeObject * L_34 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_33, 6);
		V_6 = ((*(float*)((float*)UnBox(L_34, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_35 = ___message0;
		NullCheck(L_35);
		RuntimeObject* L_36 = OscPacket_get_Data_m3397942478(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		RuntimeObject * L_37 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_36, 7);
		V_7 = ((*(float*)((float*)UnBox(L_37, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_38 = ___message0;
		NullCheck(L_38);
		RuntimeObject* L_39 = OscPacket_get_Data_m3397942478(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		RuntimeObject * L_40 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_39, 8);
		V_8 = ((*(float*)((float*)UnBox(L_40, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_41 = ___message0;
		NullCheck(L_41);
		RuntimeObject* L_42 = OscPacket_get_Data_m3397942478(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		RuntimeObject * L_43 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_42, ((int32_t)9));
		V_9 = ((*(float*)((float*)UnBox(L_43, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_44 = ___message0;
		NullCheck(L_44);
		RuntimeObject* L_45 = OscPacket_get_Data_m3397942478(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		RuntimeObject * L_46 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_45, ((int32_t)10));
		V_10 = ((*(float*)((float*)UnBox(L_46, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_47 = ___message0;
		NullCheck(L_47);
		RuntimeObject* L_48 = OscPacket_get_Data_m3397942478(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		RuntimeObject * L_49 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_48, ((int32_t)11));
		V_11 = ((*(float*)((float*)UnBox(L_49, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_50 = ___message0;
		NullCheck(L_50);
		RuntimeObject* L_51 = OscPacket_get_Data_m3397942478(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		RuntimeObject * L_52 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_51, ((int32_t)12));
		V_12 = ((*(float*)((float*)UnBox(L_52, Single_t1397266774_il2cpp_TypeInfo_var))));
		Dictionary_2_t2974023424 * L_53 = __this->get_blobs_4();
		int32_t L_54 = V_1;
		NullCheck(L_53);
		bool L_55 = Dictionary_2_TryGetValue_m4133274017(L_53, L_54, (&V_13), /*hidden argument*/Dictionary_2_TryGetValue_m4133274017_RuntimeMethod_var);
		if (L_55)
		{
			goto IL_0176;
		}
	}
	{
		int32_t L_56 = V_1;
		TuioBlob_t4085310093 * L_57 = (TuioBlob_t4085310093 *)il2cpp_codegen_object_new(TuioBlob_t4085310093_il2cpp_TypeInfo_var);
		TuioBlob__ctor_m3494057218(L_57, L_56, /*hidden argument*/NULL);
		V_13 = L_57;
	}

IL_0176:
	{
		TuioBlob_t4085310093 * L_58 = V_13;
		float L_59 = V_2;
		float L_60 = V_3;
		float L_61 = V_4;
		float L_62 = V_5;
		float L_63 = V_6;
		float L_64 = V_7;
		float L_65 = V_8;
		float L_66 = V_9;
		float L_67 = V_10;
		float L_68 = V_11;
		float L_69 = V_12;
		NullCheck(L_58);
		TuioBlob_Update_m2887677996(L_58, L_59, L_60, L_61, L_62, L_63, L_64, L_65, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
		List_1_t1262417539 * L_70 = __this->get_updatedBlobs_5();
		TuioBlob_t4085310093 * L_71 = V_13;
		NullCheck(L_70);
		List_1_Add_m689584216(L_70, L_71, /*hidden argument*/List_1_Add_m689584216_RuntimeMethod_var);
		goto IL_03d2;
	}

IL_01a3:
	{
		OscMessage_t3323977005 * L_72 = ___message0;
		NullCheck(L_72);
		RuntimeObject* L_73 = OscPacket_get_Data_m3397942478(L_72, /*hidden argument*/NULL);
		NullCheck(L_73);
		int32_t L_74 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_73);
		V_14 = L_74;
		V_15 = 1;
		goto IL_01db;
	}

IL_01b8:
	{
		List_1_t128053199 * L_75 = __this->get_addedBlobs_6();
		OscMessage_t3323977005 * L_76 = ___message0;
		NullCheck(L_76);
		RuntimeObject* L_77 = OscPacket_get_Data_m3397942478(L_76, /*hidden argument*/NULL);
		int32_t L_78 = V_15;
		NullCheck(L_77);
		RuntimeObject * L_79 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_77, L_78);
		NullCheck(L_75);
		List_1_Add_m697420525(L_75, ((*(int32_t*)((int32_t*)UnBox(L_79, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		int32_t L_80 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
	}

IL_01db:
	{
		int32_t L_81 = V_15;
		int32_t L_82 = V_14;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_01b8;
		}
	}
	{
		Dictionary_2_t2974023424 * L_83 = __this->get_blobs_4();
		NullCheck(L_83);
		Enumerator_t633238903  L_84 = Dictionary_2_GetEnumerator_m2841467664(L_83, /*hidden argument*/Dictionary_2_GetEnumerator_m2841467664_RuntimeMethod_var);
		V_17 = L_84;
	}

IL_01f1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_023b;
		}

IL_01f6:
		{
			KeyValuePair_2_t1076728295  L_85 = Enumerator_get_Current_m4138203631((&V_17), /*hidden argument*/Enumerator_get_Current_m4138203631_RuntimeMethod_var);
			V_16 = L_85;
			List_1_t128053199 * L_86 = __this->get_addedBlobs_6();
			int32_t L_87 = KeyValuePair_2_get_Key_m1056191967((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m1056191967_RuntimeMethod_var);
			NullCheck(L_86);
			bool L_88 = List_1_Contains_m4250047412(L_86, L_87, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
			if (L_88)
			{
				goto IL_0228;
			}
		}

IL_0216:
		{
			List_1_t128053199 * L_89 = __this->get_removedBlobs_7();
			int32_t L_90 = KeyValuePair_2_get_Key_m1056191967((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m1056191967_RuntimeMethod_var);
			NullCheck(L_89);
			List_1_Add_m697420525(L_89, L_90, /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		}

IL_0228:
		{
			List_1_t128053199 * L_91 = __this->get_addedBlobs_6();
			int32_t L_92 = KeyValuePair_2_get_Key_m1056191967((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m1056191967_RuntimeMethod_var);
			NullCheck(L_91);
			List_1_Remove_m3037048099(L_91, L_92, /*hidden argument*/List_1_Remove_m3037048099_RuntimeMethod_var);
		}

IL_023b:
		{
			bool L_93 = Enumerator_MoveNext_m1031836987((&V_17), /*hidden argument*/Enumerator_MoveNext_m1031836987_RuntimeMethod_var);
			if (L_93)
			{
				goto IL_01f6;
			}
		}

IL_0247:
		{
			IL2CPP_LEAVE(0x25A, FINALLY_024c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_024c;
	}

FINALLY_024c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1857637826((&V_17), /*hidden argument*/Enumerator_Dispose_m1857637826_RuntimeMethod_var);
		IL2CPP_END_FINALLY(588)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(588)
	{
		IL2CPP_JUMP_TBL(0x25A, IL_025a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_025a:
	{
		goto IL_03d2;
	}

IL_025f:
	{
		OscMessage_t3323977005 * L_94 = ___message0;
		NullCheck(L_94);
		RuntimeObject* L_95 = OscPacket_get_Data_m3397942478(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		int32_t L_96 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_95);
		if ((((int32_t)L_96) >= ((int32_t)2)))
		{
			goto IL_0271;
		}
	}
	{
		return;
	}

IL_0271:
	{
		OscMessage_t3323977005 * L_97 = ___message0;
		NullCheck(L_97);
		RuntimeObject* L_98 = OscPacket_get_Data_m3397942478(L_97, /*hidden argument*/NULL);
		NullCheck(L_98);
		RuntimeObject * L_99 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_98, 1);
		BlobProcessor_set_FrameNumber_m4211722092(__this, ((*(int32_t*)((int32_t*)UnBox(L_99, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t1262417539 * L_100 = __this->get_updatedBlobs_5();
		NullCheck(L_100);
		int32_t L_101 = List_1_get_Count_m1699732805(L_100, /*hidden argument*/List_1_get_Count_m1699732805_RuntimeMethod_var);
		V_18 = L_101;
		V_19 = 0;
		goto IL_0335;
	}

IL_029d:
	{
		List_1_t1262417539 * L_102 = __this->get_updatedBlobs_5();
		int32_t L_103 = V_19;
		NullCheck(L_102);
		TuioBlob_t4085310093 * L_104 = List_1_get_Item_m1456013675(L_102, L_103, /*hidden argument*/List_1_get_Item_m1456013675_RuntimeMethod_var);
		V_20 = L_104;
		List_1_t128053199 * L_105 = __this->get_addedBlobs_6();
		TuioBlob_t4085310093 * L_106 = V_20;
		NullCheck(L_106);
		int32_t L_107 = TuioEntity_get_Id_m3362535295(L_106, /*hidden argument*/NULL);
		NullCheck(L_105);
		bool L_108 = List_1_Contains_m4250047412(L_105, L_107, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
		if (!L_108)
		{
			goto IL_0311;
		}
	}
	{
		Dictionary_2_t2974023424 * L_109 = __this->get_blobs_4();
		TuioBlob_t4085310093 * L_110 = V_20;
		NullCheck(L_110);
		int32_t L_111 = TuioEntity_get_Id_m3362535295(L_110, /*hidden argument*/NULL);
		NullCheck(L_109);
		bool L_112 = Dictionary_2_ContainsKey_m987622744(L_109, L_111, /*hidden argument*/Dictionary_2_ContainsKey_m987622744_RuntimeMethod_var);
		if (L_112)
		{
			goto IL_0311;
		}
	}
	{
		Dictionary_2_t2974023424 * L_113 = __this->get_blobs_4();
		TuioBlob_t4085310093 * L_114 = V_20;
		NullCheck(L_114);
		int32_t L_115 = TuioEntity_get_Id_m3362535295(L_114, /*hidden argument*/NULL);
		TuioBlob_t4085310093 * L_116 = V_20;
		NullCheck(L_113);
		Dictionary_2_Add_m1090758517(L_113, L_115, L_116, /*hidden argument*/Dictionary_2_Add_m1090758517_RuntimeMethod_var);
		EventHandler_1_t3126626844 * L_117 = __this->get_BlobAdded_0();
		if (!L_117)
		{
			goto IL_030c;
		}
	}
	{
		EventHandler_1_t3126626844 * L_118 = __this->get_BlobAdded_0();
		TuioBlob_t4085310093 * L_119 = V_20;
		TuioBlobEventArgs_t907500115 * L_120 = (TuioBlobEventArgs_t907500115 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t907500115_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m1030032409(L_120, L_119, /*hidden argument*/NULL);
		NullCheck(L_118);
		EventHandler_1_Invoke_m4027566236(L_118, __this, L_120, /*hidden argument*/EventHandler_1_Invoke_m4027566236_RuntimeMethod_var);
	}

IL_030c:
	{
		goto IL_032f;
	}

IL_0311:
	{
		EventHandler_1_t3126626844 * L_121 = __this->get_BlobUpdated_1();
		if (!L_121)
		{
			goto IL_032f;
		}
	}
	{
		EventHandler_1_t3126626844 * L_122 = __this->get_BlobUpdated_1();
		TuioBlob_t4085310093 * L_123 = V_20;
		TuioBlobEventArgs_t907500115 * L_124 = (TuioBlobEventArgs_t907500115 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t907500115_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m1030032409(L_124, L_123, /*hidden argument*/NULL);
		NullCheck(L_122);
		EventHandler_1_Invoke_m4027566236(L_122, __this, L_124, /*hidden argument*/EventHandler_1_Invoke_m4027566236_RuntimeMethod_var);
	}

IL_032f:
	{
		int32_t L_125 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_125, (int32_t)1));
	}

IL_0335:
	{
		int32_t L_126 = V_19;
		int32_t L_127 = V_18;
		if ((((int32_t)L_126) < ((int32_t)L_127)))
		{
			goto IL_029d;
		}
	}
	{
		List_1_t128053199 * L_128 = __this->get_removedBlobs_7();
		NullCheck(L_128);
		int32_t L_129 = List_1_get_Count_m361000296(L_128, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
		V_18 = L_129;
		V_21 = 0;
		goto IL_03a3;
	}

IL_0353:
	{
		List_1_t128053199 * L_130 = __this->get_removedBlobs_7();
		int32_t L_131 = V_21;
		NullCheck(L_130);
		int32_t L_132 = List_1_get_Item_m1910578371(L_130, L_131, /*hidden argument*/List_1_get_Item_m1910578371_RuntimeMethod_var);
		V_22 = L_132;
		Dictionary_2_t2974023424 * L_133 = __this->get_blobs_4();
		int32_t L_134 = V_22;
		NullCheck(L_133);
		TuioBlob_t4085310093 * L_135 = Dictionary_2_get_Item_m4020322656(L_133, L_134, /*hidden argument*/Dictionary_2_get_Item_m4020322656_RuntimeMethod_var);
		V_13 = L_135;
		Dictionary_2_t2974023424 * L_136 = __this->get_blobs_4();
		int32_t L_137 = V_22;
		NullCheck(L_136);
		Dictionary_2_Remove_m911377797(L_136, L_137, /*hidden argument*/Dictionary_2_Remove_m911377797_RuntimeMethod_var);
		EventHandler_1_t3126626844 * L_138 = __this->get_BlobRemoved_2();
		if (!L_138)
		{
			goto IL_039d;
		}
	}
	{
		EventHandler_1_t3126626844 * L_139 = __this->get_BlobRemoved_2();
		TuioBlob_t4085310093 * L_140 = V_13;
		TuioBlobEventArgs_t907500115 * L_141 = (TuioBlobEventArgs_t907500115 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t907500115_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m1030032409(L_141, L_140, /*hidden argument*/NULL);
		NullCheck(L_139);
		EventHandler_1_Invoke_m4027566236(L_139, __this, L_141, /*hidden argument*/EventHandler_1_Invoke_m4027566236_RuntimeMethod_var);
	}

IL_039d:
	{
		int32_t L_142 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_142, (int32_t)1));
	}

IL_03a3:
	{
		int32_t L_143 = V_21;
		int32_t L_144 = V_18;
		if ((((int32_t)L_143) < ((int32_t)L_144)))
		{
			goto IL_0353;
		}
	}
	{
		List_1_t128053199 * L_145 = __this->get_addedBlobs_6();
		NullCheck(L_145);
		List_1_Clear_m2154023298(L_145, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t128053199 * L_146 = __this->get_removedBlobs_7();
		NullCheck(L_146);
		List_1_Clear_m2154023298(L_146, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t1262417539 * L_147 = __this->get_updatedBlobs_5();
		NullCheck(L_147);
		List_1_Clear_m4267157696(L_147, /*hidden argument*/List_1_Clear_m4267157696_RuntimeMethod_var);
		goto IL_03d2;
	}

IL_03d2:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorAdded_m511123241 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorAdded_m511123241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorAdded_0();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorAdded_m4000780957 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorAdded_m4000780957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorAdded_0();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorUpdated_m1062449922 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorUpdated_m1062449922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorUpdated_1();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorUpdated_m770238256 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorUpdated_m770238256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorUpdated_1();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorRemoved_m365604737 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorRemoved_m365604737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorRemoved_2();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorRemoved_m234525769 (CursorProcessor_t3936223090 * __this, EventHandler_1_t4113216011 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorRemoved_m234525769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t4113216011 * V_0 = NULL;
	EventHandler_1_t4113216011 * V_1 = NULL;
	{
		EventHandler_1_t4113216011 * L_0 = __this->get_CursorRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t4113216011 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t4113216011 ** L_2 = __this->get_address_of_CursorRemoved_2();
		EventHandler_1_t4113216011 * L_3 = V_1;
		EventHandler_1_t4113216011 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t4113216011 * L_6 = V_0;
		EventHandler_1_t4113216011 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t4113216011 *>(L_2, ((EventHandler_1_t4113216011 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t4113216011_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t4113216011 * L_8 = V_0;
		EventHandler_1_t4113216011 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t4113216011 *)L_8) == ((RuntimeObject*)(EventHandler_1_t4113216011 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::set_FrameNumber(System.Int32)
extern "C"  void CursorProcessor_set_FrameNumber_m730012964 (CursorProcessor_t3936223090 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::.ctor()
extern "C"  void CursorProcessor__ctor_m359159357 (CursorProcessor_t3936223090 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor__ctor_m359159357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1693470997 * L_0 = (Dictionary_2_t1693470997 *)il2cpp_codegen_object_new(Dictionary_2_t1693470997_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4060431757(L_0, /*hidden argument*/Dictionary_2__ctor_m4060431757_RuntimeMethod_var);
		__this->set_cursors_4(L_0);
		List_1_t4276832408 * L_1 = (List_1_t4276832408 *)il2cpp_codegen_object_new(List_1_t4276832408_il2cpp_TypeInfo_var);
		List_1__ctor_m546517191(L_1, /*hidden argument*/List_1__ctor_m546517191_RuntimeMethod_var);
		__this->set_updatedCursors_5(L_1);
		List_1_t128053199 * L_2 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_2, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_addedCursors_6(L_2);
		List_1_t128053199 * L_3 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_3, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_removedCursors_7(L_3);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void CursorProcessor_ProcessMessage_m986564398 (CursorProcessor_t3936223090 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_ProcessMessage_m986564398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	TuioCursor_t2804757666 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	KeyValuePair_2_t4091143164  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Enumerator_t3647653772  V_11;
	memset(&V_11, 0, sizeof(V_11));
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	TuioCursor_t2804757666 * V_14 = NULL;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t3323977005 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m2654232420(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, _stringLiteral3269165829, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		OscMessage_t3323977005 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m3397942478(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		if (!L_7)
		{
			goto IL_034f;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral2553217811, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral1569063519, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0120;
		}
	}
	{
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral4243243339, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_01dc;
		}
	}
	{
		goto IL_034f;
	}

IL_0063:
	{
		OscMessage_t3323977005 * L_14 = ___message0;
		NullCheck(L_14);
		RuntimeObject* L_15 = OscPacket_get_Data_m3397942478(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_15);
		if ((((int32_t)L_16) >= ((int32_t)7)))
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		OscMessage_t3323977005 * L_17 = ___message0;
		NullCheck(L_17);
		RuntimeObject* L_18 = OscPacket_get_Data_m3397942478(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		RuntimeObject * L_19 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_18, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_19, Int32_t2950945753_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_20 = ___message0;
		NullCheck(L_20);
		RuntimeObject* L_21 = OscPacket_get_Data_m3397942478(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		RuntimeObject * L_22 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_21, 2);
		V_2 = ((*(float*)((float*)UnBox(L_22, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_23 = ___message0;
		NullCheck(L_23);
		RuntimeObject* L_24 = OscPacket_get_Data_m3397942478(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject * L_25 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_24, 3);
		V_3 = ((*(float*)((float*)UnBox(L_25, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_26 = ___message0;
		NullCheck(L_26);
		RuntimeObject* L_27 = OscPacket_get_Data_m3397942478(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		RuntimeObject * L_28 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_27, 4);
		V_4 = ((*(float*)((float*)UnBox(L_28, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_29 = ___message0;
		NullCheck(L_29);
		RuntimeObject* L_30 = OscPacket_get_Data_m3397942478(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		RuntimeObject * L_31 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_30, 5);
		V_5 = ((*(float*)((float*)UnBox(L_31, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_32 = ___message0;
		NullCheck(L_32);
		RuntimeObject* L_33 = OscPacket_get_Data_m3397942478(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		RuntimeObject * L_34 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_33, 6);
		V_6 = ((*(float*)((float*)UnBox(L_34, Single_t1397266774_il2cpp_TypeInfo_var))));
		Dictionary_2_t1693470997 * L_35 = __this->get_cursors_4();
		int32_t L_36 = V_1;
		NullCheck(L_35);
		bool L_37 = Dictionary_2_TryGetValue_m2949425533(L_35, L_36, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m2949425533_RuntimeMethod_var);
		if (L_37)
		{
			goto IL_00ff;
		}
	}
	{
		int32_t L_38 = V_1;
		TuioCursor_t2804757666 * L_39 = (TuioCursor_t2804757666 *)il2cpp_codegen_object_new(TuioCursor_t2804757666_il2cpp_TypeInfo_var);
		TuioCursor__ctor_m1466983245(L_39, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
	}

IL_00ff:
	{
		TuioCursor_t2804757666 * L_40 = V_7;
		float L_41 = V_2;
		float L_42 = V_3;
		float L_43 = V_4;
		float L_44 = V_5;
		float L_45 = V_6;
		NullCheck(L_40);
		TuioCursor_Update_m1099848212(L_40, L_41, L_42, L_43, L_44, L_45, /*hidden argument*/NULL);
		List_1_t4276832408 * L_46 = __this->get_updatedCursors_5();
		TuioCursor_t2804757666 * L_47 = V_7;
		NullCheck(L_46);
		List_1_Add_m3374480747(L_46, L_47, /*hidden argument*/List_1_Add_m3374480747_RuntimeMethod_var);
		goto IL_034f;
	}

IL_0120:
	{
		OscMessage_t3323977005 * L_48 = ___message0;
		NullCheck(L_48);
		RuntimeObject* L_49 = OscPacket_get_Data_m3397942478(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_49);
		V_8 = L_50;
		V_9 = 1;
		goto IL_0158;
	}

IL_0135:
	{
		List_1_t128053199 * L_51 = __this->get_addedCursors_6();
		OscMessage_t3323977005 * L_52 = ___message0;
		NullCheck(L_52);
		RuntimeObject* L_53 = OscPacket_get_Data_m3397942478(L_52, /*hidden argument*/NULL);
		int32_t L_54 = V_9;
		NullCheck(L_53);
		RuntimeObject * L_55 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_53, L_54);
		NullCheck(L_51);
		List_1_Add_m697420525(L_51, ((*(int32_t*)((int32_t*)UnBox(L_55, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		int32_t L_56 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_56, (int32_t)1));
	}

IL_0158:
	{
		int32_t L_57 = V_9;
		int32_t L_58 = V_8;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_0135;
		}
	}
	{
		Dictionary_2_t1693470997 * L_59 = __this->get_cursors_4();
		NullCheck(L_59);
		Enumerator_t3647653772  L_60 = Dictionary_2_GetEnumerator_m3436297861(L_59, /*hidden argument*/Dictionary_2_GetEnumerator_m3436297861_RuntimeMethod_var);
		V_11 = L_60;
	}

IL_016e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b8;
		}

IL_0173:
		{
			KeyValuePair_2_t4091143164  L_61 = Enumerator_get_Current_m1124426651((&V_11), /*hidden argument*/Enumerator_get_Current_m1124426651_RuntimeMethod_var);
			V_10 = L_61;
			List_1_t128053199 * L_62 = __this->get_addedCursors_6();
			int32_t L_63 = KeyValuePair_2_get_Key_m2249459862((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m2249459862_RuntimeMethod_var);
			NullCheck(L_62);
			bool L_64 = List_1_Contains_m4250047412(L_62, L_63, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
			if (L_64)
			{
				goto IL_01a5;
			}
		}

IL_0193:
		{
			List_1_t128053199 * L_65 = __this->get_removedCursors_7();
			int32_t L_66 = KeyValuePair_2_get_Key_m2249459862((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m2249459862_RuntimeMethod_var);
			NullCheck(L_65);
			List_1_Add_m697420525(L_65, L_66, /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		}

IL_01a5:
		{
			List_1_t128053199 * L_67 = __this->get_addedCursors_6();
			int32_t L_68 = KeyValuePair_2_get_Key_m2249459862((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m2249459862_RuntimeMethod_var);
			NullCheck(L_67);
			List_1_Remove_m3037048099(L_67, L_68, /*hidden argument*/List_1_Remove_m3037048099_RuntimeMethod_var);
		}

IL_01b8:
		{
			bool L_69 = Enumerator_MoveNext_m4072011572((&V_11), /*hidden argument*/Enumerator_MoveNext_m4072011572_RuntimeMethod_var);
			if (L_69)
			{
				goto IL_0173;
			}
		}

IL_01c4:
		{
			IL2CPP_LEAVE(0x1D7, FINALLY_01c9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01c9;
	}

FINALLY_01c9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3655613863((&V_11), /*hidden argument*/Enumerator_Dispose_m3655613863_RuntimeMethod_var);
		IL2CPP_END_FINALLY(457)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(457)
	{
		IL2CPP_JUMP_TBL(0x1D7, IL_01d7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01d7:
	{
		goto IL_034f;
	}

IL_01dc:
	{
		OscMessage_t3323977005 * L_70 = ___message0;
		NullCheck(L_70);
		RuntimeObject* L_71 = OscPacket_get_Data_m3397942478(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		int32_t L_72 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_71);
		if ((((int32_t)L_72) >= ((int32_t)2)))
		{
			goto IL_01ee;
		}
	}
	{
		return;
	}

IL_01ee:
	{
		OscMessage_t3323977005 * L_73 = ___message0;
		NullCheck(L_73);
		RuntimeObject* L_74 = OscPacket_get_Data_m3397942478(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		RuntimeObject * L_75 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_74, 1);
		CursorProcessor_set_FrameNumber_m730012964(__this, ((*(int32_t*)((int32_t*)UnBox(L_75, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t4276832408 * L_76 = __this->get_updatedCursors_5();
		NullCheck(L_76);
		int32_t L_77 = List_1_get_Count_m2851265837(L_76, /*hidden argument*/List_1_get_Count_m2851265837_RuntimeMethod_var);
		V_12 = L_77;
		V_13 = 0;
		goto IL_02b2;
	}

IL_021a:
	{
		List_1_t4276832408 * L_78 = __this->get_updatedCursors_5();
		int32_t L_79 = V_13;
		NullCheck(L_78);
		TuioCursor_t2804757666 * L_80 = List_1_get_Item_m1501378545(L_78, L_79, /*hidden argument*/List_1_get_Item_m1501378545_RuntimeMethod_var);
		V_14 = L_80;
		List_1_t128053199 * L_81 = __this->get_addedCursors_6();
		TuioCursor_t2804757666 * L_82 = V_14;
		NullCheck(L_82);
		int32_t L_83 = TuioEntity_get_Id_m3362535295(L_82, /*hidden argument*/NULL);
		NullCheck(L_81);
		bool L_84 = List_1_Contains_m4250047412(L_81, L_83, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
		if (!L_84)
		{
			goto IL_028e;
		}
	}
	{
		Dictionary_2_t1693470997 * L_85 = __this->get_cursors_4();
		TuioCursor_t2804757666 * L_86 = V_14;
		NullCheck(L_86);
		int32_t L_87 = TuioEntity_get_Id_m3362535295(L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		bool L_88 = Dictionary_2_ContainsKey_m4292169967(L_85, L_87, /*hidden argument*/Dictionary_2_ContainsKey_m4292169967_RuntimeMethod_var);
		if (L_88)
		{
			goto IL_028e;
		}
	}
	{
		Dictionary_2_t1693470997 * L_89 = __this->get_cursors_4();
		TuioCursor_t2804757666 * L_90 = V_14;
		NullCheck(L_90);
		int32_t L_91 = TuioEntity_get_Id_m3362535295(L_90, /*hidden argument*/NULL);
		TuioCursor_t2804757666 * L_92 = V_14;
		NullCheck(L_89);
		Dictionary_2_Add_m4211285854(L_89, L_91, L_92, /*hidden argument*/Dictionary_2_Add_m4211285854_RuntimeMethod_var);
		EventHandler_1_t4113216011 * L_93 = __this->get_CursorAdded_0();
		if (!L_93)
		{
			goto IL_0289;
		}
	}
	{
		EventHandler_1_t4113216011 * L_94 = __this->get_CursorAdded_0();
		TuioCursor_t2804757666 * L_95 = V_14;
		TuioCursorEventArgs_t1894089282 * L_96 = (TuioCursorEventArgs_t1894089282 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t1894089282_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m138848966(L_96, L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		EventHandler_1_Invoke_m2655476952(L_94, __this, L_96, /*hidden argument*/EventHandler_1_Invoke_m2655476952_RuntimeMethod_var);
	}

IL_0289:
	{
		goto IL_02ac;
	}

IL_028e:
	{
		EventHandler_1_t4113216011 * L_97 = __this->get_CursorUpdated_1();
		if (!L_97)
		{
			goto IL_02ac;
		}
	}
	{
		EventHandler_1_t4113216011 * L_98 = __this->get_CursorUpdated_1();
		TuioCursor_t2804757666 * L_99 = V_14;
		TuioCursorEventArgs_t1894089282 * L_100 = (TuioCursorEventArgs_t1894089282 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t1894089282_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m138848966(L_100, L_99, /*hidden argument*/NULL);
		NullCheck(L_98);
		EventHandler_1_Invoke_m2655476952(L_98, __this, L_100, /*hidden argument*/EventHandler_1_Invoke_m2655476952_RuntimeMethod_var);
	}

IL_02ac:
	{
		int32_t L_101 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_101, (int32_t)1));
	}

IL_02b2:
	{
		int32_t L_102 = V_13;
		int32_t L_103 = V_12;
		if ((((int32_t)L_102) < ((int32_t)L_103)))
		{
			goto IL_021a;
		}
	}
	{
		List_1_t128053199 * L_104 = __this->get_removedCursors_7();
		NullCheck(L_104);
		int32_t L_105 = List_1_get_Count_m361000296(L_104, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
		V_12 = L_105;
		V_15 = 0;
		goto IL_0320;
	}

IL_02d0:
	{
		List_1_t128053199 * L_106 = __this->get_removedCursors_7();
		int32_t L_107 = V_15;
		NullCheck(L_106);
		int32_t L_108 = List_1_get_Item_m1910578371(L_106, L_107, /*hidden argument*/List_1_get_Item_m1910578371_RuntimeMethod_var);
		V_16 = L_108;
		Dictionary_2_t1693470997 * L_109 = __this->get_cursors_4();
		int32_t L_110 = V_16;
		NullCheck(L_109);
		TuioCursor_t2804757666 * L_111 = Dictionary_2_get_Item_m2068205222(L_109, L_110, /*hidden argument*/Dictionary_2_get_Item_m2068205222_RuntimeMethod_var);
		V_7 = L_111;
		Dictionary_2_t1693470997 * L_112 = __this->get_cursors_4();
		int32_t L_113 = V_16;
		NullCheck(L_112);
		Dictionary_2_Remove_m2147751717(L_112, L_113, /*hidden argument*/Dictionary_2_Remove_m2147751717_RuntimeMethod_var);
		EventHandler_1_t4113216011 * L_114 = __this->get_CursorRemoved_2();
		if (!L_114)
		{
			goto IL_031a;
		}
	}
	{
		EventHandler_1_t4113216011 * L_115 = __this->get_CursorRemoved_2();
		TuioCursor_t2804757666 * L_116 = V_7;
		TuioCursorEventArgs_t1894089282 * L_117 = (TuioCursorEventArgs_t1894089282 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t1894089282_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m138848966(L_117, L_116, /*hidden argument*/NULL);
		NullCheck(L_115);
		EventHandler_1_Invoke_m2655476952(L_115, __this, L_117, /*hidden argument*/EventHandler_1_Invoke_m2655476952_RuntimeMethod_var);
	}

IL_031a:
	{
		int32_t L_118 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_118, (int32_t)1));
	}

IL_0320:
	{
		int32_t L_119 = V_15;
		int32_t L_120 = V_12;
		if ((((int32_t)L_119) < ((int32_t)L_120)))
		{
			goto IL_02d0;
		}
	}
	{
		List_1_t128053199 * L_121 = __this->get_addedCursors_6();
		NullCheck(L_121);
		List_1_Clear_m2154023298(L_121, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t128053199 * L_122 = __this->get_removedCursors_7();
		NullCheck(L_122);
		List_1_Clear_m2154023298(L_122, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t4276832408 * L_123 = __this->get_updatedCursors_5();
		NullCheck(L_123);
		List_1_Clear_m2024113527(L_123, /*hidden argument*/List_1_Clear_m2024113527_RuntimeMethod_var);
		goto IL_034f;
	}

IL_034f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectAdded_m954421447 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectAdded_m954421447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectAdded_0();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectAdded_m1729185584 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectAdded_m1729185584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectAdded_0();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectUpdated_m3547798864 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectUpdated_m3547798864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectUpdated_1();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectUpdated_m3667678717 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectUpdated_m3667678717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectUpdated_1();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectRemoved_m386417278 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectRemoved_m386417278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectRemoved_2();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectRemoved_m1261782329 (ObjectProcessor_t2877610401 * __this, EventHandler_1_t2455490493 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectRemoved_m1261782329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2455490493 * V_0 = NULL;
	EventHandler_1_t2455490493 * V_1 = NULL;
	{
		EventHandler_1_t2455490493 * L_0 = __this->get_ObjectRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2455490493 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2455490493 ** L_2 = __this->get_address_of_ObjectRemoved_2();
		EventHandler_1_t2455490493 * L_3 = V_1;
		EventHandler_1_t2455490493 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t2455490493 * L_6 = V_0;
		EventHandler_1_t2455490493 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t2455490493 *>(L_2, ((EventHandler_1_t2455490493 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t2455490493_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t2455490493 * L_8 = V_0;
		EventHandler_1_t2455490493 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2455490493 *)L_8) == ((RuntimeObject*)(EventHandler_1_t2455490493 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::set_FrameNumber(System.Int32)
extern "C"  void ObjectProcessor_set_FrameNumber_m3146742338 (ObjectProcessor_t2877610401 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::.ctor()
extern "C"  void ObjectProcessor__ctor_m3182407677 (ObjectProcessor_t2877610401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor__ctor_m3182407677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3186476502 * L_0 = (Dictionary_2_t3186476502 *)il2cpp_codegen_object_new(Dictionary_2_t3186476502_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3393625636(L_0, /*hidden argument*/Dictionary_2__ctor_m3393625636_RuntimeMethod_var);
		__this->set_objects_4(L_0);
		List_1_t1474870617 * L_1 = (List_1_t1474870617 *)il2cpp_codegen_object_new(List_1_t1474870617_il2cpp_TypeInfo_var);
		List_1__ctor_m1635335082(L_1, /*hidden argument*/List_1__ctor_m1635335082_RuntimeMethod_var);
		__this->set_updatedObjects_5(L_1);
		List_1_t128053199 * L_2 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_2, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_addedObjects_6(L_2);
		List_1_t128053199 * L_3 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_3, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_removedObjects_7(L_3);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void ObjectProcessor_ProcessMessage_m1566973063 (ObjectProcessor_t2877610401 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_ProcessMessage_m1566973063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	TuioObject_t2795875 * V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	KeyValuePair_2_t1289181373  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Enumerator_t845691981  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	TuioObject_t2795875 * V_18 = NULL;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t3323977005 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m2654232420(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, _stringLiteral3625986001, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		OscMessage_t3323977005 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m3397942478(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		if (!L_7)
		{
			goto IL_03a6;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral2553217811, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral1569063519, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0177;
		}
	}
	{
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral4243243339, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0233;
		}
	}
	{
		goto IL_03a6;
	}

IL_0063:
	{
		OscMessage_t3323977005 * L_14 = ___message0;
		NullCheck(L_14);
		RuntimeObject* L_15 = OscPacket_get_Data_m3397942478(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_15);
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)11))))
		{
			goto IL_0076;
		}
	}
	{
		return;
	}

IL_0076:
	{
		OscMessage_t3323977005 * L_17 = ___message0;
		NullCheck(L_17);
		RuntimeObject* L_18 = OscPacket_get_Data_m3397942478(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		RuntimeObject * L_19 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_18, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_19, Int32_t2950945753_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_20 = ___message0;
		NullCheck(L_20);
		RuntimeObject* L_21 = OscPacket_get_Data_m3397942478(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		RuntimeObject * L_22 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_21, 2);
		V_2 = ((*(int32_t*)((int32_t*)UnBox(L_22, Int32_t2950945753_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_23 = ___message0;
		NullCheck(L_23);
		RuntimeObject* L_24 = OscPacket_get_Data_m3397942478(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject * L_25 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_24, 3);
		V_3 = ((*(float*)((float*)UnBox(L_25, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_26 = ___message0;
		NullCheck(L_26);
		RuntimeObject* L_27 = OscPacket_get_Data_m3397942478(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		RuntimeObject * L_28 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_27, 4);
		V_4 = ((*(float*)((float*)UnBox(L_28, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_29 = ___message0;
		NullCheck(L_29);
		RuntimeObject* L_30 = OscPacket_get_Data_m3397942478(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		RuntimeObject * L_31 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_30, 5);
		V_5 = ((*(float*)((float*)UnBox(L_31, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_32 = ___message0;
		NullCheck(L_32);
		RuntimeObject* L_33 = OscPacket_get_Data_m3397942478(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		RuntimeObject * L_34 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_33, 6);
		V_6 = ((*(float*)((float*)UnBox(L_34, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_35 = ___message0;
		NullCheck(L_35);
		RuntimeObject* L_36 = OscPacket_get_Data_m3397942478(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		RuntimeObject * L_37 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_36, 7);
		V_7 = ((*(float*)((float*)UnBox(L_37, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_38 = ___message0;
		NullCheck(L_38);
		RuntimeObject* L_39 = OscPacket_get_Data_m3397942478(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		RuntimeObject * L_40 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_39, 8);
		V_8 = ((*(float*)((float*)UnBox(L_40, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_41 = ___message0;
		NullCheck(L_41);
		RuntimeObject* L_42 = OscPacket_get_Data_m3397942478(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		RuntimeObject * L_43 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_42, ((int32_t)9));
		V_9 = ((*(float*)((float*)UnBox(L_43, Single_t1397266774_il2cpp_TypeInfo_var))));
		OscMessage_t3323977005 * L_44 = ___message0;
		NullCheck(L_44);
		RuntimeObject* L_45 = OscPacket_get_Data_m3397942478(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		RuntimeObject * L_46 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_45, ((int32_t)10));
		V_10 = ((*(float*)((float*)UnBox(L_46, Single_t1397266774_il2cpp_TypeInfo_var))));
		Dictionary_2_t3186476502 * L_47 = __this->get_objects_4();
		int32_t L_48 = V_1;
		NullCheck(L_47);
		bool L_49 = Dictionary_2_TryGetValue_m3350852866(L_47, L_48, (&V_11), /*hidden argument*/Dictionary_2_TryGetValue_m3350852866_RuntimeMethod_var);
		if (L_49)
		{
			goto IL_014f;
		}
	}
	{
		int32_t L_50 = V_1;
		int32_t L_51 = V_2;
		TuioObject_t2795875 * L_52 = (TuioObject_t2795875 *)il2cpp_codegen_object_new(TuioObject_t2795875_il2cpp_TypeInfo_var);
		TuioObject__ctor_m1545049101(L_52, L_50, L_51, /*hidden argument*/NULL);
		V_11 = L_52;
	}

IL_014f:
	{
		TuioObject_t2795875 * L_53 = V_11;
		float L_54 = V_3;
		float L_55 = V_4;
		float L_56 = V_5;
		float L_57 = V_6;
		float L_58 = V_7;
		float L_59 = V_8;
		float L_60 = V_9;
		float L_61 = V_10;
		NullCheck(L_53);
		TuioObject_Update_m1076492596(L_53, L_54, L_55, L_56, L_57, L_58, L_59, L_60, L_61, /*hidden argument*/NULL);
		List_1_t1474870617 * L_62 = __this->get_updatedObjects_5();
		TuioObject_t2795875 * L_63 = V_11;
		NullCheck(L_62);
		List_1_Add_m3017127986(L_62, L_63, /*hidden argument*/List_1_Add_m3017127986_RuntimeMethod_var);
		goto IL_03a6;
	}

IL_0177:
	{
		OscMessage_t3323977005 * L_64 = ___message0;
		NullCheck(L_64);
		RuntimeObject* L_65 = OscPacket_get_Data_m3397942478(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		int32_t L_66 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_65);
		V_12 = L_66;
		V_13 = 1;
		goto IL_01af;
	}

IL_018c:
	{
		List_1_t128053199 * L_67 = __this->get_addedObjects_6();
		OscMessage_t3323977005 * L_68 = ___message0;
		NullCheck(L_68);
		RuntimeObject* L_69 = OscPacket_get_Data_m3397942478(L_68, /*hidden argument*/NULL);
		int32_t L_70 = V_13;
		NullCheck(L_69);
		RuntimeObject * L_71 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_69, L_70);
		NullCheck(L_67);
		List_1_Add_m697420525(L_67, ((*(int32_t*)((int32_t*)UnBox(L_71, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		int32_t L_72 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_72, (int32_t)1));
	}

IL_01af:
	{
		int32_t L_73 = V_13;
		int32_t L_74 = V_12;
		if ((((int32_t)L_73) < ((int32_t)L_74)))
		{
			goto IL_018c;
		}
	}
	{
		Dictionary_2_t3186476502 * L_75 = __this->get_objects_4();
		NullCheck(L_75);
		Enumerator_t845691981  L_76 = Dictionary_2_GetEnumerator_m3493560753(L_75, /*hidden argument*/Dictionary_2_GetEnumerator_m3493560753_RuntimeMethod_var);
		V_15 = L_76;
	}

IL_01c5:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020f;
		}

IL_01ca:
		{
			KeyValuePair_2_t1289181373  L_77 = Enumerator_get_Current_m702359707((&V_15), /*hidden argument*/Enumerator_get_Current_m702359707_RuntimeMethod_var);
			V_14 = L_77;
			List_1_t128053199 * L_78 = __this->get_addedObjects_6();
			int32_t L_79 = KeyValuePair_2_get_Key_m3626592599((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m3626592599_RuntimeMethod_var);
			NullCheck(L_78);
			bool L_80 = List_1_Contains_m4250047412(L_78, L_79, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
			if (L_80)
			{
				goto IL_01fc;
			}
		}

IL_01ea:
		{
			List_1_t128053199 * L_81 = __this->get_removedObjects_7();
			int32_t L_82 = KeyValuePair_2_get_Key_m3626592599((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m3626592599_RuntimeMethod_var);
			NullCheck(L_81);
			List_1_Add_m697420525(L_81, L_82, /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		}

IL_01fc:
		{
			List_1_t128053199 * L_83 = __this->get_addedObjects_6();
			int32_t L_84 = KeyValuePair_2_get_Key_m3626592599((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m3626592599_RuntimeMethod_var);
			NullCheck(L_83);
			List_1_Remove_m3037048099(L_83, L_84, /*hidden argument*/List_1_Remove_m3037048099_RuntimeMethod_var);
		}

IL_020f:
		{
			bool L_85 = Enumerator_MoveNext_m4089973788((&V_15), /*hidden argument*/Enumerator_MoveNext_m4089973788_RuntimeMethod_var);
			if (L_85)
			{
				goto IL_01ca;
			}
		}

IL_021b:
		{
			IL2CPP_LEAVE(0x22E, FINALLY_0220);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0220;
	}

FINALLY_0220:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1218014203((&V_15), /*hidden argument*/Enumerator_Dispose_m1218014203_RuntimeMethod_var);
		IL2CPP_END_FINALLY(544)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(544)
	{
		IL2CPP_JUMP_TBL(0x22E, IL_022e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_022e:
	{
		goto IL_03a6;
	}

IL_0233:
	{
		OscMessage_t3323977005 * L_86 = ___message0;
		NullCheck(L_86);
		RuntimeObject* L_87 = OscPacket_get_Data_m3397942478(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		int32_t L_88 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_87);
		if ((((int32_t)L_88) >= ((int32_t)2)))
		{
			goto IL_0245;
		}
	}
	{
		return;
	}

IL_0245:
	{
		OscMessage_t3323977005 * L_89 = ___message0;
		NullCheck(L_89);
		RuntimeObject* L_90 = OscPacket_get_Data_m3397942478(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		RuntimeObject * L_91 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_90, 1);
		ObjectProcessor_set_FrameNumber_m3146742338(__this, ((*(int32_t*)((int32_t*)UnBox(L_91, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t1474870617 * L_92 = __this->get_updatedObjects_5();
		NullCheck(L_92);
		int32_t L_93 = List_1_get_Count_m1437897001(L_92, /*hidden argument*/List_1_get_Count_m1437897001_RuntimeMethod_var);
		V_16 = L_93;
		V_17 = 0;
		goto IL_0309;
	}

IL_0271:
	{
		List_1_t1474870617 * L_94 = __this->get_updatedObjects_5();
		int32_t L_95 = V_17;
		NullCheck(L_94);
		TuioObject_t2795875 * L_96 = List_1_get_Item_m1972841044(L_94, L_95, /*hidden argument*/List_1_get_Item_m1972841044_RuntimeMethod_var);
		V_18 = L_96;
		List_1_t128053199 * L_97 = __this->get_addedObjects_6();
		TuioObject_t2795875 * L_98 = V_18;
		NullCheck(L_98);
		int32_t L_99 = TuioEntity_get_Id_m3362535295(L_98, /*hidden argument*/NULL);
		NullCheck(L_97);
		bool L_100 = List_1_Contains_m4250047412(L_97, L_99, /*hidden argument*/List_1_Contains_m4250047412_RuntimeMethod_var);
		if (!L_100)
		{
			goto IL_02e5;
		}
	}
	{
		Dictionary_2_t3186476502 * L_101 = __this->get_objects_4();
		TuioObject_t2795875 * L_102 = V_18;
		NullCheck(L_102);
		int32_t L_103 = TuioEntity_get_Id_m3362535295(L_102, /*hidden argument*/NULL);
		NullCheck(L_101);
		bool L_104 = Dictionary_2_ContainsKey_m3922808659(L_101, L_103, /*hidden argument*/Dictionary_2_ContainsKey_m3922808659_RuntimeMethod_var);
		if (L_104)
		{
			goto IL_02e5;
		}
	}
	{
		Dictionary_2_t3186476502 * L_105 = __this->get_objects_4();
		TuioObject_t2795875 * L_106 = V_18;
		NullCheck(L_106);
		int32_t L_107 = TuioEntity_get_Id_m3362535295(L_106, /*hidden argument*/NULL);
		TuioObject_t2795875 * L_108 = V_18;
		NullCheck(L_105);
		Dictionary_2_Add_m357742037(L_105, L_107, L_108, /*hidden argument*/Dictionary_2_Add_m357742037_RuntimeMethod_var);
		EventHandler_1_t2455490493 * L_109 = __this->get_ObjectAdded_0();
		if (!L_109)
		{
			goto IL_02e0;
		}
	}
	{
		EventHandler_1_t2455490493 * L_110 = __this->get_ObjectAdded_0();
		TuioObject_t2795875 * L_111 = V_18;
		TuioObjectEventArgs_t236363764 * L_112 = (TuioObjectEventArgs_t236363764 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t236363764_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m3321218798(L_112, L_111, /*hidden argument*/NULL);
		NullCheck(L_110);
		EventHandler_1_Invoke_m1236698408(L_110, __this, L_112, /*hidden argument*/EventHandler_1_Invoke_m1236698408_RuntimeMethod_var);
	}

IL_02e0:
	{
		goto IL_0303;
	}

IL_02e5:
	{
		EventHandler_1_t2455490493 * L_113 = __this->get_ObjectUpdated_1();
		if (!L_113)
		{
			goto IL_0303;
		}
	}
	{
		EventHandler_1_t2455490493 * L_114 = __this->get_ObjectUpdated_1();
		TuioObject_t2795875 * L_115 = V_18;
		TuioObjectEventArgs_t236363764 * L_116 = (TuioObjectEventArgs_t236363764 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t236363764_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m3321218798(L_116, L_115, /*hidden argument*/NULL);
		NullCheck(L_114);
		EventHandler_1_Invoke_m1236698408(L_114, __this, L_116, /*hidden argument*/EventHandler_1_Invoke_m1236698408_RuntimeMethod_var);
	}

IL_0303:
	{
		int32_t L_117 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_117, (int32_t)1));
	}

IL_0309:
	{
		int32_t L_118 = V_17;
		int32_t L_119 = V_16;
		if ((((int32_t)L_118) < ((int32_t)L_119)))
		{
			goto IL_0271;
		}
	}
	{
		List_1_t128053199 * L_120 = __this->get_removedObjects_7();
		NullCheck(L_120);
		int32_t L_121 = List_1_get_Count_m361000296(L_120, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
		V_16 = L_121;
		V_19 = 0;
		goto IL_0377;
	}

IL_0327:
	{
		List_1_t128053199 * L_122 = __this->get_removedObjects_7();
		int32_t L_123 = V_19;
		NullCheck(L_122);
		int32_t L_124 = List_1_get_Item_m1910578371(L_122, L_123, /*hidden argument*/List_1_get_Item_m1910578371_RuntimeMethod_var);
		V_20 = L_124;
		Dictionary_2_t3186476502 * L_125 = __this->get_objects_4();
		int32_t L_126 = V_20;
		NullCheck(L_125);
		TuioObject_t2795875 * L_127 = Dictionary_2_get_Item_m587101098(L_125, L_126, /*hidden argument*/Dictionary_2_get_Item_m587101098_RuntimeMethod_var);
		V_11 = L_127;
		Dictionary_2_t3186476502 * L_128 = __this->get_objects_4();
		int32_t L_129 = V_20;
		NullCheck(L_128);
		Dictionary_2_Remove_m404394798(L_128, L_129, /*hidden argument*/Dictionary_2_Remove_m404394798_RuntimeMethod_var);
		EventHandler_1_t2455490493 * L_130 = __this->get_ObjectRemoved_2();
		if (!L_130)
		{
			goto IL_0371;
		}
	}
	{
		EventHandler_1_t2455490493 * L_131 = __this->get_ObjectRemoved_2();
		TuioObject_t2795875 * L_132 = V_11;
		TuioObjectEventArgs_t236363764 * L_133 = (TuioObjectEventArgs_t236363764 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t236363764_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m3321218798(L_133, L_132, /*hidden argument*/NULL);
		NullCheck(L_131);
		EventHandler_1_Invoke_m1236698408(L_131, __this, L_133, /*hidden argument*/EventHandler_1_Invoke_m1236698408_RuntimeMethod_var);
	}

IL_0371:
	{
		int32_t L_134 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_134, (int32_t)1));
	}

IL_0377:
	{
		int32_t L_135 = V_19;
		int32_t L_136 = V_16;
		if ((((int32_t)L_135) < ((int32_t)L_136)))
		{
			goto IL_0327;
		}
	}
	{
		List_1_t128053199 * L_137 = __this->get_addedObjects_6();
		NullCheck(L_137);
		List_1_Clear_m2154023298(L_137, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t128053199 * L_138 = __this->get_removedObjects_7();
		NullCheck(L_138);
		List_1_Clear_m2154023298(L_138, /*hidden argument*/List_1_Clear_m2154023298_RuntimeMethod_var);
		List_1_t1474870617 * L_139 = __this->get_updatedObjects_5();
		NullCheck(L_139);
		List_1_Clear_m227439264(L_139, /*hidden argument*/List_1_Clear_m227439264_RuntimeMethod_var);
		goto IL_03a6;
	}

IL_03a6:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.TuioBlobEventArgs::.ctor(TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioBlobEventArgs__ctor_m1030032409 (TuioBlobEventArgs_t907500115 * __this, TuioBlob_t4085310093 * ___blob0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioBlobEventArgs__ctor_m1030032409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		TuioBlob_t4085310093 * L_0 = ___blob0;
		__this->set_Blob_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.TuioCursorEventArgs::.ctor(TUIOsharp.Entities.TuioCursor)
extern "C"  void TuioCursorEventArgs__ctor_m138848966 (TuioCursorEventArgs_t1894089282 * __this, TuioCursor_t2804757666 * ___cursor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioCursorEventArgs__ctor_m138848966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		TuioCursor_t2804757666 * L_0 = ___cursor0;
		__this->set_Cursor_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.TuioObjectEventArgs::.ctor(TUIOsharp.Entities.TuioObject)
extern "C"  void TuioObjectEventArgs__ctor_m3321218798 (TuioObjectEventArgs_t236363764 * __this, TuioObject_t2795875 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioObjectEventArgs__ctor_m3321218798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		TuioObject_t2795875 * L_0 = ___obj0;
		__this->set_Object_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single TUIOsharp.Entities.TuioBlob::get_Angle()
extern "C"  float TuioBlob_get_Angle_m1062452350 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Angle(System.Single)
extern "C"  void TuioBlob_set_Angle_m3346400918 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Width()
extern "C"  float TuioBlob_get_Width_m2947137917 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CWidthU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Width(System.Single)
extern "C"  void TuioBlob_set_Width_m2177051861 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Height()
extern "C"  float TuioBlob_get_Height_m1153354888 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CHeightU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Height(System.Single)
extern "C"  void TuioBlob_set_Height_m222606430 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Area()
extern "C"  float TuioBlob_get_Area_m2171438420 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAreaU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Area(System.Single)
extern "C"  void TuioBlob_set_Area_m3038579628 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAreaU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_RotationVelocity()
extern "C"  float TuioBlob_get_RotationVelocity_m17636645 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationVelocityU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationVelocity(System.Single)
extern "C"  void TuioBlob_set_RotationVelocity_m854345461 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationVelocityU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_RotationAcceleration()
extern "C"  float TuioBlob_get_RotationAcceleration_m3029732083 (TuioBlob_t4085310093 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationAccelerationU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationAcceleration(System.Single)
extern "C"  void TuioBlob_set_RotationAcceleration_m3695130551 (TuioBlob_t4085310093 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationAccelerationU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32)
extern "C"  void TuioBlob__ctor_m3494057218 (TuioBlob_t4085310093 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		TuioBlob__ctor_m2376717690(__this, L_0, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob__ctor_m2376717690 (TuioBlob_t4085310093 * __this, int32_t ___id0, float ___x1, float ___y2, float ___angle3, float ___width4, float ___height5, float ___area6, float ___velocityX7, float ___velocityY8, float ___rotationVelocity9, float ___acceleration10, float ___rotationAcceleration11, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x1;
		float L_2 = ___y2;
		float L_3 = ___velocityX7;
		float L_4 = ___velocityY8;
		float L_5 = ___acceleration10;
		TuioEntity__ctor_m3445181350(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		float L_6 = ___angle3;
		TuioBlob_set_Angle_m3346400918(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___width4;
		TuioBlob_set_Width_m2177051861(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___height5;
		TuioBlob_set_Height_m222606430(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___area6;
		TuioBlob_set_Area_m3038579628(__this, L_9, /*hidden argument*/NULL);
		float L_10 = ___rotationVelocity9;
		TuioBlob_set_RotationVelocity_m854345461(__this, L_10, /*hidden argument*/NULL);
		float L_11 = ___rotationAcceleration11;
		TuioBlob_set_RotationAcceleration_m3695130551(__this, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob_Update_m2887677996 (TuioBlob_t4085310093 * __this, float ___x0, float ___y1, float ___angle2, float ___width3, float ___height4, float ___area5, float ___velocityX6, float ___velocityY7, float ___rotationVelocity8, float ___acceleration9, float ___rotationAcceleration10, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m3721568381(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3176374475(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___angle2;
		TuioBlob_set_Angle_m3346400918(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___width3;
		TuioBlob_set_Width_m2177051861(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___height4;
		TuioBlob_set_Height_m222606430(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___area5;
		TuioBlob_set_Area_m3038579628(__this, L_5, /*hidden argument*/NULL);
		float L_6 = ___velocityX6;
		TuioEntity_set_VelocityX_m7749035(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___velocityX6;
		TuioEntity_set_VelocityY_m4294262286(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___rotationVelocity8;
		TuioBlob_set_RotationVelocity_m854345461(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___acceleration9;
		TuioEntity_set_Acceleration_m270679629(__this, L_9, /*hidden argument*/NULL);
		float L_10 = ___rotationAcceleration10;
		TuioBlob_set_RotationAcceleration_m3695130551(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32)
extern "C"  void TuioCursor__ctor_m1466983245 (TuioCursor_t2804757666 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		TuioCursor__ctor_m4081665409(__this, L_0, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor__ctor_m4081665409 (TuioCursor_t2804757666 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x1;
		float L_2 = ___y2;
		float L_3 = ___velocityX3;
		float L_4 = ___velocityY4;
		float L_5 = ___acceleration5;
		TuioEntity__ctor_m3445181350(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioCursor::Update(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor_Update_m1099848212 (TuioCursor_t2804757666 * __this, float ___x0, float ___y1, float ___velocityX2, float ___velocityY3, float ___acceleration4, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m3721568381(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3176374475(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___velocityX2;
		TuioEntity_set_VelocityX_m7749035(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX2;
		TuioEntity_set_VelocityY_m4294262286(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___acceleration4;
		TuioEntity_set_Acceleration_m270679629(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TUIOsharp.Entities.TuioEntity::get_Id()
extern "C"  int32_t TuioEntity_get_Id_m3362535295 (TuioEntity_t4229790798 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Id(System.Int32)
extern "C"  void TuioEntity_set_Id_m2368423484 (TuioEntity_t4229790798 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioEntity::get_X()
extern "C"  float TuioEntity_get_X_m2751109317 (TuioEntity_t4229790798 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CXU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_X(System.Single)
extern "C"  void TuioEntity_set_X_m3721568381 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CXU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioEntity::get_Y()
extern "C"  float TuioEntity_get_Y_m794794181 (TuioEntity_t4229790798 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CYU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Y(System.Single)
extern "C"  void TuioEntity_set_Y_m3176374475 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CYU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityX(System.Single)
extern "C"  void TuioEntity_set_VelocityX_m7749035 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CVelocityXU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityY(System.Single)
extern "C"  void TuioEntity_set_VelocityY_m4294262286 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CVelocityYU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Acceleration(System.Single)
extern "C"  void TuioEntity_set_Acceleration_m270679629 (TuioEntity_t4229790798 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAccelerationU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioEntity__ctor_m3445181350 (TuioEntity_t4229790798 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___id0;
		TuioEntity_set_Id_m2368423484(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___x1;
		TuioEntity_set_X_m3721568381(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___y2;
		TuioEntity_set_Y_m3176374475(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX3;
		TuioEntity_set_VelocityX_m7749035(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___velocityY4;
		TuioEntity_set_VelocityY_m4294262286(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___acceleration5;
		TuioEntity_set_Acceleration_m270679629(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TUIOsharp.Entities.TuioObject::get_ClassId()
extern "C"  int32_t TuioObject_get_ClassId_m93535143 (TuioObject_t2795875 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CClassIdU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_ClassId(System.Int32)
extern "C"  void TuioObject_set_ClassId_m1315785328 (TuioObject_t2795875 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CClassIdU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_Angle()
extern "C"  float TuioObject_get_Angle_m1391248714 (TuioObject_t2795875 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_Angle(System.Single)
extern "C"  void TuioObject_set_Angle_m3294417457 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_RotationVelocity()
extern "C"  float TuioObject_get_RotationVelocity_m853201353 (TuioObject_t2795875 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationVelocityU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_RotationVelocity(System.Single)
extern "C"  void TuioObject_set_RotationVelocity_m3598322238 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationVelocityU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_RotationAcceleration()
extern "C"  float TuioObject_get_RotationAcceleration_m1976948343 (TuioObject_t2795875 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationAccelerationU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_RotationAcceleration(System.Single)
extern "C"  void TuioObject_set_RotationAcceleration_m683552439 (TuioObject_t2795875 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationAccelerationU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32)
extern "C"  void TuioObject__ctor_m1545049101 (TuioObject_t2795875 * __this, int32_t ___id0, int32_t ___classId1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = ___classId1;
		TuioObject__ctor_m2087415962(__this, L_0, L_1, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject__ctor_m2087415962 (TuioObject_t2795875 * __this, int32_t ___id0, int32_t ___classId1, float ___x2, float ___y3, float ___angle4, float ___velocityX5, float ___velocityY6, float ___rotationVelocity7, float ___acceleration8, float ___rotationAcceleration9, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x2;
		float L_2 = ___y3;
		float L_3 = ___velocityX5;
		float L_4 = ___velocityY6;
		float L_5 = ___acceleration8;
		TuioEntity__ctor_m3445181350(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___classId1;
		TuioObject_set_ClassId_m1315785328(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___angle4;
		TuioObject_set_Angle_m3294417457(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___rotationVelocity7;
		TuioObject_set_RotationVelocity_m3598322238(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___rotationAcceleration9;
		TuioObject_set_RotationAcceleration_m683552439(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject_Update_m1076492596 (TuioObject_t2795875 * __this, float ___x0, float ___y1, float ___angle2, float ___velocityX3, float ___velocityY4, float ___rotationVelocity5, float ___acceleration6, float ___rotationAcceleration7, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m3721568381(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3176374475(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___angle2;
		TuioObject_set_Angle_m3294417457(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX3;
		TuioEntity_set_VelocityX_m7749035(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___velocityX3;
		TuioEntity_set_VelocityY_m4294262286(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___rotationVelocity5;
		TuioObject_set_RotationVelocity_m3598322238(__this, L_5, /*hidden argument*/NULL);
		float L_6 = ___acceleration6;
		TuioEntity_set_Acceleration_m270679629(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___rotationAcceleration7;
		TuioObject_set_RotationAcceleration_m683552439(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TUIOsharp.TuioServer::get_Port()
extern "C"  int32_t TuioServer_get_Port_m2570413809 (TuioServer_t3158170151 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void TUIOsharp.TuioServer::set_Port(System.Int32)
extern "C"  void TuioServer_set_Port_m3406601027 (TuioServer_t3158170151 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPortU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::.ctor(System.Int32)
extern "C"  void TuioServer__ctor_m3200135755 (TuioServer_t3158170151 * __this, int32_t ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer__ctor_m3200135755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2674567522 * L_0 = (List_1_t2674567522 *)il2cpp_codegen_object_new(List_1_t2674567522_il2cpp_TypeInfo_var);
		List_1__ctor_m211060356(L_0, /*hidden argument*/List_1__ctor_m211060356_RuntimeMethod_var);
		__this->set_processors_3(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___port0;
		TuioServer_set_Port_m3406601027(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = TuioServer_get_Port_m2570413809(__this, /*hidden argument*/NULL);
		UDPReceiver_t1881969775 * L_3 = (UDPReceiver_t1881969775 *)il2cpp_codegen_object_new(UDPReceiver_t1881969775_il2cpp_TypeInfo_var);
		UDPReceiver__ctor_m588051875(L_3, L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_udpReceiver_2(L_3);
		UDPReceiver_t1881969775 * L_4 = __this->get_udpReceiver_2();
		intptr_t L_5 = (intptr_t)TuioServer_handlerOscMessageReceived_m3605979964_RuntimeMethod_var;
		EventHandler_1_t314378975 * L_6 = (EventHandler_1_t314378975 *)il2cpp_codegen_object_new(EventHandler_1_t314378975_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m871205231(L_6, __this, L_5, /*hidden argument*/EventHandler_1__ctor_m871205231_RuntimeMethod_var);
		NullCheck(L_4);
		UDPReceiver_add_MessageReceived_m2005693360(L_4, L_6, /*hidden argument*/NULL);
		UDPReceiver_t1881969775 * L_7 = __this->get_udpReceiver_2();
		intptr_t L_8 = (intptr_t)TuioServer_handlerOscErrorOccured_m2827029671_RuntimeMethod_var;
		EventHandler_1_t319478644 * L_9 = (EventHandler_1_t319478644 *)il2cpp_codegen_object_new(EventHandler_1_t319478644_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m951552470(L_9, __this, L_8, /*hidden argument*/EventHandler_1__ctor_m951552470_RuntimeMethod_var);
		NullCheck(L_7);
		UDPReceiver_add_ErrorOccured_m2838983283(L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::Connect()
extern "C"  void TuioServer_Connect_m2489363144 (TuioServer_t3158170151 * __this, const RuntimeMethod* method)
{
	{
		UDPReceiver_t1881969775 * L_0 = __this->get_udpReceiver_2();
		NullCheck(L_0);
		bool L_1 = UDPReceiver_get_IsRunning_m1711451(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		UDPReceiver_t1881969775 * L_2 = __this->get_udpReceiver_2();
		NullCheck(L_2);
		UDPReceiver_Start_m2568122436(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::Disconnect()
extern "C"  void TuioServer_Disconnect_m2253260659 (TuioServer_t3158170151 * __this, const RuntimeMethod* method)
{
	{
		UDPReceiver_t1881969775 * L_0 = __this->get_udpReceiver_2();
		NullCheck(L_0);
		bool L_1 = UDPReceiver_get_IsRunning_m1711451(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UDPReceiver_t1881969775 * L_2 = __this->get_udpReceiver_2();
		NullCheck(L_2);
		UDPReceiver_Stop_m2310297977(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::AddDataProcessor(TUIOsharp.DataProcessors.IDataProcessor)
extern "C"  void TuioServer_AddDataProcessor_m4080391229 (TuioServer_t3158170151 * __this, RuntimeObject* ___processor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_AddDataProcessor_m4080391229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2674567522 * L_0 = __this->get_processors_3();
		RuntimeObject* L_1 = ___processor0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m1021282117(L_0, L_1, /*hidden argument*/List_1_Contains_m1021282117_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		List_1_t2674567522 * L_3 = __this->get_processors_3();
		RuntimeObject* L_4 = ___processor0;
		NullCheck(L_3);
		List_1_Add_m2077402128(L_3, L_4, /*hidden argument*/List_1_Add_m2077402128_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::RemoveDataProcessor(TUIOsharp.DataProcessors.IDataProcessor)
extern "C"  void TuioServer_RemoveDataProcessor_m2239788743 (TuioServer_t3158170151 * __this, RuntimeObject* ___processor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_RemoveDataProcessor_m2239788743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2674567522 * L_0 = __this->get_processors_3();
		RuntimeObject* L_1 = ___processor0;
		NullCheck(L_0);
		List_1_Remove_m2487744439(L_0, L_1, /*hidden argument*/List_1_Remove_m2487744439_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::RemoveAllDataProcessors()
extern "C"  void TuioServer_RemoveAllDataProcessors_m3154379542 (TuioServer_t3158170151 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_RemoveAllDataProcessors_m3154379542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2674567522 * L_0 = __this->get_processors_3();
		NullCheck(L_0);
		List_1_Clear_m3700974216(L_0, /*hidden argument*/List_1_Clear_m3700974216_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::processMessage(OSCsharp.Data.OscMessage)
extern "C"  void TuioServer_processMessage_m1808445210 (TuioServer_t3158170151 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_processMessage_m1808445210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		OscMessage_t3323977005 * L_0 = ___message0;
		NullCheck(L_0);
		RuntimeObject* L_1 = OscPacket_get_Data_m3397942478(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_1);
		if (L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		List_1_t2674567522 * L_3 = __this->get_processors_3();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2207940050(L_3, /*hidden argument*/List_1_get_Count_m2207940050_RuntimeMethod_var);
		V_0 = L_4;
		V_1 = 0;
		goto IL_003a;
	}

IL_0024:
	{
		List_1_t2674567522 * L_5 = __this->get_processors_3();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		RuntimeObject* L_7 = List_1_get_Item_m3750012149(L_5, L_6, /*hidden argument*/List_1_get_Item_m3750012149_RuntimeMethod_var);
		OscMessage_t3323977005 * L_8 = ___message0;
		NullCheck(L_7);
		InterfaceActionInvoker1< OscMessage_t3323977005 * >::Invoke(0 /* System.Void TUIOsharp.DataProcessors.IDataProcessor::ProcessMessage(OSCsharp.Data.OscMessage) */, IDataProcessor_t1202492780_il2cpp_TypeInfo_var, L_7, L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0024;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::handlerOscErrorOccured(System.Object,OSCsharp.Utils.ExceptionEventArgs)
extern "C"  void TuioServer_handlerOscErrorOccured_m2827029671 (TuioServer_t3158170151 * __this, RuntimeObject * ___sender0, ExceptionEventArgs_t2395319211 * ___exceptionEventArgs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_handlerOscErrorOccured_m2827029671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t319478644 * L_0 = __this->get_ErrorOccured_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		EventHandler_1_t319478644 * L_1 = __this->get_ErrorOccured_0();
		ExceptionEventArgs_t2395319211 * L_2 = ___exceptionEventArgs1;
		NullCheck(L_1);
		EventHandler_1_Invoke_m3384041340(L_1, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m3384041340_RuntimeMethod_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::handlerOscMessageReceived(System.Object,OSCsharp.Net.OscMessageReceivedEventArgs)
extern "C"  void TuioServer_handlerOscMessageReceived_m3605979964 (TuioServer_t3158170151 * __this, RuntimeObject * ___sender0, OscMessageReceivedEventArgs_t2390219542 * ___oscMessageReceivedEventArgs1, const RuntimeMethod* method)
{
	{
		OscMessageReceivedEventArgs_t2390219542 * L_0 = ___oscMessageReceivedEventArgs1;
		NullCheck(L_0);
		OscMessage_t3323977005 * L_1 = OscMessageReceivedEventArgs_get_Message_m2617929226(L_0, /*hidden argument*/NULL);
		TuioServer_processMessage_m1808445210(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
