﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>
struct EventHandler_1_t2091288363;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t4252506175;
// TouchScript.IGestureManager
struct IGestureManager_t4266705231;
// TouchScript.ITouchManager
struct ITouchManager_t2552034033;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Object
struct Il2CppObject;
// TouchScript.Gestures.GestureStateChangeEventArgs
struct GestureStateChangeEventArgs_t3499981191;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture2352305985.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Ges2128095272.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Touc594133898.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_GestureStat3499981191.h"

// System.Void TouchScript.Gestures.Gesture::.ctor()
extern "C"  void Gesture__ctor_m3646659046 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::add_StateChanged(System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>)
extern "C"  void Gesture_add_StateChanged_m3330938286 (Gesture_t2352305985 * __this, EventHandler_1_t2091288363 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::remove_StateChanged(System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>)
extern "C"  void Gesture_remove_StateChanged_m205700585 (Gesture_t2352305985 * __this, EventHandler_1_t2091288363 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::add_Cancelled(System.EventHandler`1<System.EventArgs>)
extern "C"  void Gesture_add_Cancelled_m4179701355 (Gesture_t2352305985 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::remove_Cancelled(System.EventHandler`1<System.EventArgs>)
extern "C"  void Gesture_remove_Cancelled_m1923951876 (Gesture_t2352305985 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Gestures.Gesture::get_MinTouches()
extern "C"  int32_t Gesture_get_MinTouches_m3962988296 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_MinTouches(System.Int32)
extern "C"  void Gesture_set_MinTouches_m1972060503 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Gestures.Gesture::get_MaxTouches()
extern "C"  int32_t Gesture_get_MaxTouches_m2124030162 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_MaxTouches(System.Int32)
extern "C"  void Gesture_set_MaxTouches_m3568751513 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::get_RequireGestureToFail()
extern "C"  Gesture_t2352305985 * Gesture_get_RequireGestureToFail_m2709793748 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_RequireGestureToFail(TouchScript.Gestures.Gesture)
extern "C"  void Gesture_set_RequireGestureToFail_m1188571949 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::get_CombineTouches()
extern "C"  bool Gesture_get_CombineTouches_m1744010373 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_CombineTouches(System.Boolean)
extern "C"  void Gesture_set_CombineTouches_m2299365496 (Gesture_t2352305985 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Gesture::get_CombineTouchesInterval()
extern "C"  float Gesture_get_CombineTouchesInterval_m3575323314 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_CombineTouchesInterval(System.Single)
extern "C"  void Gesture_set_CombineTouchesInterval_m2742199539 (Gesture_t2352305985 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::get_UseSendMessage()
extern "C"  bool Gesture_get_UseSendMessage_m2092904715 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_UseSendMessage(System.Boolean)
extern "C"  void Gesture_set_UseSendMessage_m3387141820 (Gesture_t2352305985 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::get_SendStateChangeMessages()
extern "C"  bool Gesture_get_SendStateChangeMessages_m1770751012 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_SendStateChangeMessages(System.Boolean)
extern "C"  void Gesture_set_SendStateChangeMessages_m2069026349 (Gesture_t2352305985 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TouchScript.Gestures.Gesture::get_SendMessageTarget()
extern "C"  GameObject_t1756533147 * Gesture_get_SendMessageTarget_m749181614 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_SendMessageTarget(UnityEngine.GameObject)
extern "C"  void Gesture_set_SendMessageTarget_m3081811173 (Gesture_t2352305985 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::get_State()
extern "C"  int32_t Gesture_get_State_m2377657021 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_State(TouchScript.Gestures.Gesture/GestureState)
extern "C"  void Gesture_set_State_m3774786926 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::get_PreviousState()
extern "C"  int32_t Gesture_get_PreviousState_m552531468 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_PreviousState(TouchScript.Gestures.Gesture/GestureState)
extern "C"  void Gesture_set_PreviousState_m551698607 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_ScreenPosition()
extern "C"  Vector2_t2243707579  Gesture_get_ScreenPosition_m383142253 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_PreviousScreenPosition()
extern "C"  Vector2_t2243707579  Gesture_get_PreviousScreenPosition_m1105588956 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_NormalizedScreenPosition()
extern "C"  Vector2_t2243707579  Gesture_get_NormalizedScreenPosition_m910011980 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_PreviousNormalizedScreenPosition()
extern "C"  Vector2_t2243707579  Gesture_get_PreviousNormalizedScreenPosition_m1574026301 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::get_ActiveTouches()
extern "C"  Il2CppObject* Gesture_get_ActiveTouches_m2829438389 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Gestures.Gesture::get_NumTouches()
extern "C"  int32_t Gesture_get_NumTouches_m2034998484 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::get_Delegate()
extern "C"  Il2CppObject * Gesture_get_Delegate_m4238350308 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_Delegate(TouchScript.IGestureDelegate)
extern "C"  void Gesture_set_Delegate_m2344335981 (Gesture_t2352305985 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.IGestureManager TouchScript.Gestures.Gesture::get_gestureManager()
extern "C"  Il2CppObject * Gesture_get_gestureManager_m640612765 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.ITouchManager TouchScript.Gestures.Gesture::get_touchManager()
extern "C"  Il2CppObject * Gesture_get_touchManager_m986430345 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_touchManager(TouchScript.ITouchManager)
extern "C"  void Gesture_set_touchManager_m2357745074 (Gesture_t2352305985 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/TouchesNumState TouchScript.Gestures.Gesture::get_touchesNumState()
extern "C"  int32_t Gesture_get_touchesNumState_m3625621304 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::set_touchesNumState(TouchScript.Gestures.Gesture/TouchesNumState)
extern "C"  void Gesture_set_touchesNumState_m1247546725 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::AddFriendlyGesture(TouchScript.Gestures.Gesture)
extern "C"  void Gesture_AddFriendlyGesture_m2648281050 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::IsFriendly(TouchScript.Gestures.Gesture)
extern "C"  bool Gesture_IsFriendly_m612133508 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::GetTargetHitResult()
extern "C"  bool Gesture_GetTargetHitResult_m2521336289 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::GetTargetHitResult(TouchScript.Hit.TouchHit&)
extern "C"  bool Gesture_GetTargetHitResult_m2761067356 (Gesture_t2352305985 * __this, TouchHit_t4186847494 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::GetTargetHitResult(UnityEngine.Vector2)
extern "C"  bool Gesture_GetTargetHitResult_m1755270345 (Gesture_t2352305985 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::GetTargetHitResult(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  bool Gesture_GetTargetHitResult_m2246451094 (Gesture_t2352305985 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::HasTouch(TouchScript.TouchPoint)
extern "C"  bool Gesture_HasTouch_m884999946 (Gesture_t2352305985 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern "C"  bool Gesture_CanPreventGesture_m2642494254 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern "C"  bool Gesture_CanBePreventedByGesture_m722201483 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::ShouldReceiveTouch(TouchScript.TouchPoint)
extern "C"  bool Gesture_ShouldReceiveTouch_m3065245502 (Gesture_t2352305985 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::ShouldBegin()
extern "C"  bool Gesture_ShouldBegin_m63377060 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::Cancel(System.Boolean,System.Boolean)
extern "C"  void Gesture_Cancel_m4216647554 (Gesture_t2352305985 * __this, bool ___cancelTouches0, bool ___returnTouches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::Cancel()
extern "C"  void Gesture_Cancel_m2660254594 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::Awake()
extern "C"  void Gesture_Awake_m4273535963 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::OnEnable()
extern "C"  void Gesture_OnEnable_m2809469018 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::OnDisable()
extern "C"  void Gesture_OnDisable_m2347433855 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::OnDestroy()
extern "C"  void Gesture_OnDestroy_m1871005277 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_SetState(TouchScript.Gestures.Gesture/GestureState)
extern "C"  void Gesture_INTERNAL_SetState_m1733639453 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_Reset()
extern "C"  void Gesture_INTERNAL_Reset_m3886746867 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_TouchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_INTERNAL_TouchesBegan_m2497007526 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_TouchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_INTERNAL_TouchesMoved_m2485146676 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_TouchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_INTERNAL_TouchesEnded_m3450105077 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_TouchesCancelled(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_INTERNAL_TouchesCancelled_m1510693264 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::INTERNAL_RemoveFriendlyGesture(TouchScript.Gestures.Gesture)
extern "C"  void Gesture_INTERNAL_RemoveFriendlyGesture_m1762562461 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::shouldCacheTouchPosition(TouchScript.TouchPoint)
extern "C"  bool Gesture_shouldCacheTouchPosition_m1701345620 (Gesture_t2352305985 * __this, TouchPoint_t959629083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Gesture::setState(TouchScript.Gestures.Gesture/GestureState)
extern "C"  bool Gesture_setState_m20557353 (Gesture_t2352305985 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_touchesBegan_m574832914 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_touchesMoved_m2258263916 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_touchesEnded_m1011866573 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::touchesCancelled(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Gesture_touchesCancelled_m510754036 (Gesture_t2352305985 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::reset()
extern "C"  void Gesture_reset_m2199708423 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onPossible()
extern "C"  void Gesture_onPossible_m774161518 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onBegan()
extern "C"  void Gesture_onBegan_m2825679924 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onChanged()
extern "C"  void Gesture_onChanged_m419206185 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onRecognized()
extern "C"  void Gesture_onRecognized_m3926842631 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onFailed()
extern "C"  void Gesture_onFailed_m2057633400 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::onCancelled()
extern "C"  void Gesture_onCancelled_m2393768066 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::retainTouches()
extern "C"  void Gesture_retainTouches_m2139288866 (Gesture_t2352305985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::releaseTouches(System.Boolean)
extern "C"  void Gesture_releaseTouches_m2003995115 (Gesture_t2352305985 * __this, bool ___cancel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::registerFriendlyGesture(TouchScript.Gestures.Gesture)
extern "C"  void Gesture_registerFriendlyGesture_m532957522 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::unregisterFriendlyGesture(TouchScript.Gestures.Gesture)
extern "C"  void Gesture_unregisterFriendlyGesture_m395046297 (Gesture_t2352305985 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Gesture::requiredToFailGestureStateChangedHandler(System.Object,TouchScript.Gestures.GestureStateChangeEventArgs)
extern "C"  void Gesture_requiredToFailGestureStateChangedHandler_m44980241 (Gesture_t2352305985 * __this, Il2CppObject * ___sender0, GestureStateChangeEventArgs_t3499981191 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
