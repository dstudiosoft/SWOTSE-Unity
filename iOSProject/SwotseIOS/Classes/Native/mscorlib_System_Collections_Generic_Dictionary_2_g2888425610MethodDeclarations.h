﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Collections.Generic.IDictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct IDictionary_2_t887509031;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>[]
struct KeyValuePair_2U5BU5D_t1878179441;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>
struct IEnumerator_1_t2416261955;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct KeyCollection_t1076956085;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct ValueCollection_t1591485453;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_645770832.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4208450312.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor()
extern "C"  void Dictionary_2__ctor_m3431423698_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3431423698(__this, method) ((  void (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2__ctor_m3431423698_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3367545102_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3367545102(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3367545102_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m166216429_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m166216429(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m166216429_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2942040278_gshared (Dictionary_2_t2888425610 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2942040278(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2942040278_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1528067190_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1528067190(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1528067190_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2616755405_gshared (Dictionary_2_t2888425610 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2616755405(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2616755405_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m949862696_gshared (Dictionary_2_t2888425610 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m949862696(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2888425610 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m949862696_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m32467021_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m32467021(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m32467021_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2993549885_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2993549885(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2993549885_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m703953778_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m703953778(__this, method) ((  bool (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m703953778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m552917993_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m552917993(__this, method) ((  bool (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m552917993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m986526671_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m986526671(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m986526671_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m606649572_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m606649572(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m606649572_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2644136485_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2644136485(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2644136485_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1005614585_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1005614585(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1005614585_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1838808400_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1838808400(__this, ___key0, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1838808400_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3430086775_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3430086775(__this, method) ((  bool (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3430086775_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4196780675_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4196780675(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4196780675_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m348664133_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m348664133(__this, method) ((  bool (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m348664133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4093130050_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2_t645770832  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4093130050(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2888425610 *, KeyValuePair_2_t645770832 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4093130050_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3154322526_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2_t645770832  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3154322526(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2888425610 *, KeyValuePair_2_t645770832 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3154322526_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m299027446_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2U5BU5D_t1878179441* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m299027446(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2888425610 *, KeyValuePair_2U5BU5D_t1878179441*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m299027446_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m136319891_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2_t645770832  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m136319891(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2888425610 *, KeyValuePair_2_t645770832 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m136319891_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m516465175_gshared (Dictionary_2_t2888425610 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m516465175(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m516465175_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m675149680_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m675149680(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m675149680_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m766189325_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m766189325(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m766189325_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3704491274_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3704491274(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3704491274_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m4113036927_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m4113036927(__this, method) ((  int32_t (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_get_Count_m4113036927_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Item(TKey)
extern "C"  TouchData_t3880599975  Dictionary_2_get_Item_m2941950270_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2941950270(__this, ___key0, method) ((  TouchData_t3880599975  (*) (Dictionary_2_t2888425610 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2941950270_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1031485957_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1031485957(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_set_Item_m1031485957_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2091388003_gshared (Dictionary_2_t2888425610 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2091388003(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2091388003_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m290320458_gshared (Dictionary_2_t2888425610 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m290320458(__this, ___size0, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m290320458_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m733189416_gshared (Dictionary_2_t2888425610 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m733189416(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m733189416_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t645770832  Dictionary_2_make_pair_m3847932466_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3847932466(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t645770832  (*) (Il2CppObject * /* static, unused */, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_make_pair_m3847932466_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2464580812_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2464580812(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_pick_key_m2464580812_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::pick_value(TKey,TValue)
extern "C"  TouchData_t3880599975  Dictionary_2_pick_value_m624324300_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m624324300(__this /* static, unused */, ___key0, ___value1, method) ((  TouchData_t3880599975  (*) (Il2CppObject * /* static, unused */, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_pick_value_m624324300_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1713302599_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2U5BU5D_t1878179441* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1713302599(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2888425610 *, KeyValuePair_2U5BU5D_t1878179441*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1713302599_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Resize()
extern "C"  void Dictionary_2_Resize_m3235913565_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3235913565(__this, method) ((  void (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_Resize_m3235913565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3731428938_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3731428938(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2888425610 *, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_Add_m3731428938_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Clear()
extern "C"  void Dictionary_2_Clear_m1035650330_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1035650330(__this, method) ((  void (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_Clear_m1035650330_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3126828849_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3126828849(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2888425610 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3126828849_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3678055446_gshared (Dictionary_2_t2888425610 * __this, TouchData_t3880599975  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3678055446(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2888425610 *, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_ContainsValue_m3678055446_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m4196298139_gshared (Dictionary_2_t2888425610 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m4196298139(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2888425610 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m4196298139_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3007173747_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3007173747(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3007173747_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m387063119_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m387063119(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2888425610 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m387063119_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m894247137_gshared (Dictionary_2_t2888425610 * __this, int32_t ___key0, TouchData_t3880599975 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m894247137(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2888425610 *, int32_t, TouchData_t3880599975 *, const MethodInfo*))Dictionary_2_TryGetValue_m894247137_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Keys()
extern "C"  KeyCollection_t1076956085 * Dictionary_2_get_Keys_m2703387422_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2703387422(__this, method) ((  KeyCollection_t1076956085 * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_get_Keys_m2703387422_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Values()
extern "C"  ValueCollection_t1591485453 * Dictionary_2_get_Values_m2642511358_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2642511358(__this, method) ((  ValueCollection_t1591485453 * (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_get_Values_m2642511358_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1284774173_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1284774173(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1284774173_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::ToTValue(System.Object)
extern "C"  TouchData_t3880599975  Dictionary_2_ToTValue_m2279353021_gshared (Dictionary_2_t2888425610 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2279353021(__this, ___value0, method) ((  TouchData_t3880599975  (*) (Dictionary_2_t2888425610 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2279353021_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1906793115_gshared (Dictionary_2_t2888425610 * __this, KeyValuePair_2_t645770832  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1906793115(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2888425610 *, KeyValuePair_2_t645770832 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1906793115_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::GetEnumerator()
extern "C"  Enumerator_t4208450312  Dictionary_2_GetEnumerator_m248250736_gshared (Dictionary_2_t2888425610 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m248250736(__this, method) ((  Enumerator_t4208450312  (*) (Dictionary_2_t2888425610 *, const MethodInfo*))Dictionary_2_GetEnumerator_m248250736_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m611287181_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m611287181(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, TouchData_t3880599975 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m611287181_gshared)(__this /* static, unused */, ___key0, ___value1, method)
