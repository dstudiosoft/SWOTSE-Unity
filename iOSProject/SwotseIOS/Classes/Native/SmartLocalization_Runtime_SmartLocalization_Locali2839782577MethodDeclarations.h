﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LocalizedGUITexture
struct LocalizedGUITexture_t2839782577;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_Languag132697751.h"

// System.Void SmartLocalization.LocalizedGUITexture::.ctor()
extern "C"  void LocalizedGUITexture__ctor_m3281369821 (LocalizedGUITexture_t2839782577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUITexture::Start()
extern "C"  void LocalizedGUITexture_Start_m3382620157 (LocalizedGUITexture_t2839782577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUITexture::OnDestroy()
extern "C"  void LocalizedGUITexture_OnDestroy_m168719764 (LocalizedGUITexture_t2839782577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUITexture::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUITexture_OnChangeLanguage_m2086742077 (LocalizedGUITexture_t2839782577 * __this, LanguageManager_t132697751 * ___languageManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
