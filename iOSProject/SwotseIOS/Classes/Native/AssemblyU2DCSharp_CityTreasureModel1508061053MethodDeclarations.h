﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityTreasureModel
struct CityTreasureModel_t1508061053;

#include "codegen/il2cpp-codegen.h"

// System.Void CityTreasureModel::.ctor()
extern "C"  void CityTreasureModel__ctor_m1360266914 (CityTreasureModel_t1508061053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
