﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3059932636.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23792220452.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2127737671_gshared (Enumerator_t3059932636 * __this, Dictionary_2_t1739907934 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2127737671(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3059932636 *, Dictionary_2_t1739907934 *, const MethodInfo*))Enumerator__ctor_m2127737671_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m754763942_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m754763942(__this, method) ((  Il2CppObject * (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m754763942_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2602457082_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2602457082(__this, method) ((  void (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2602457082_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1259773341_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1259773341(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1259773341_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3497345656_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3497345656(__this, method) ((  Il2CppObject * (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3497345656_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3086646882_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3086646882(__this, method) ((  Il2CppObject * (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3086646882_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4128866617_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4128866617(__this, method) ((  bool (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_MoveNext_m4128866617_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Current()
extern "C"  KeyValuePair_2_t3792220452  Enumerator_get_Current_m4077787232_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4077787232(__this, method) ((  KeyValuePair_2_t3792220452  (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_get_Current_m4077787232_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3275501355_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3275501355(__this, method) ((  int32_t (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_get_CurrentKey_m3275501355_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_CurrentValue()
extern "C"  TouchState_t2732082299  Enumerator_get_CurrentValue_m1425731659_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1425731659(__this, method) ((  TouchState_t2732082299  (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_get_CurrentValue_m1425731659_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Reset()
extern "C"  void Enumerator_Reset_m2007606405_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2007606405(__this, method) ((  void (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_Reset_m2007606405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3982557966_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3982557966(__this, method) ((  void (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_VerifyState_m3982557966_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3311595954_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3311595954(__this, method) ((  void (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_VerifyCurrent_m3311595954_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Dispose()
extern "C"  void Enumerator_Dispose_m1939310555_gshared (Enumerator_t3059932636 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1939310555(__this, method) ((  void (*) (Enumerator_t3059932636 *, const MethodInfo*))Enumerator_Dispose_m1939310555_gshared)(__this, method)
