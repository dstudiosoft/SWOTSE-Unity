﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsManager
struct SettingsManager_t2519859232;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsManager::.ctor()
extern "C"  void SettingsManager__ctor_m1130914037 (SettingsManager_t2519859232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::LogOut()
extern "C"  void SettingsManager_LogOut_m1894533047 (SettingsManager_t2519859232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
