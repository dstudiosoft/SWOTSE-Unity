﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceInviteLine
struct AllianceInviteLine_t3911898758;
// AllianceInviteTableController
struct AllianceInviteTableController_t989323868;
// UserModel
struct UserModel_t3025569216;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AllianceInviteTableController989323868.h"
#include "AssemblyU2DCSharp_UserModel3025569216.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void AllianceInviteLine::.ctor()
extern "C"  void AllianceInviteLine__ctor_m3195887501 (AllianceInviteLine_t3911898758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetOwner(AllianceInviteTableController)
extern "C"  void AllianceInviteLine_SetOwner_m2621412664 (AllianceInviteLine_t3911898758 * __this, AllianceInviteTableController_t989323868 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetUserId(UserModel)
extern "C"  void AllianceInviteLine_SetUserId_m3721518069 (AllianceInviteLine_t3911898758 * __this, UserModel_t3025569216 * ___userValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetMemberName(System.String)
extern "C"  void AllianceInviteLine_SetMemberName_m3935261758 (AllianceInviteLine_t3911898758 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetAllianceName(System.String)
extern "C"  void AllianceInviteLine_SetAllianceName_m1704432439 (AllianceInviteLine_t3911898758 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetRank(System.String)
extern "C"  void AllianceInviteLine_SetRank_m860791271 (AllianceInviteLine_t3911898758 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetExperience(System.Int64)
extern "C"  void AllianceInviteLine_SetExperience_m674659659 (AllianceInviteLine_t3911898758 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetCities(System.Int64)
extern "C"  void AllianceInviteLine_SetCities_m4244054846 (AllianceInviteLine_t3911898758 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetCapitalName(System.String)
extern "C"  void AllianceInviteLine_SetCapitalName_m1565468680 (AllianceInviteLine_t3911898758 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetCapitalCoords(System.Int64,System.Int64)
extern "C"  void AllianceInviteLine_SetCapitalCoords_m122636907 (AllianceInviteLine_t3911898758 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SetLastOnline(System.String)
extern "C"  void AllianceInviteLine_SetLastOnline_m4225701628 (AllianceInviteLine_t3911898758 * __this, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::SendInvite()
extern "C"  void AllianceInviteLine_SendInvite_m1114069058 (AllianceInviteLine_t3911898758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInviteLine::InviteSent(System.Object,System.String)
extern "C"  void AllianceInviteLine_InviteSent_m1323214878 (AllianceInviteLine_t3911898758 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
