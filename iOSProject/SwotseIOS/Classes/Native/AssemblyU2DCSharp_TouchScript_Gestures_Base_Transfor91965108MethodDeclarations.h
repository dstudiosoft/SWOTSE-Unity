﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.Base.TransformGestureBase
struct TransformGestureBase_t91965108;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Base_Transfor94296928.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_ProjectionPar2712959773.h"

// System.Void TouchScript.Gestures.Base.TransformGestureBase::.ctor()
extern "C"  void TransformGestureBase__ctor_m794431458 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_add_TransformStarted_m3496391339 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_remove_TransformStarted_m3744181462 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_add_Transformed_m1742461379 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_remove_Transformed_m2463521938 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_add_TransformCompleted_m2815834727 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGestureBase_remove_TransformCompleted_m2620567266 (TransformGestureBase_t91965108 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Base.TransformGestureBase/TransformType TouchScript.Gestures.Base.TransformGestureBase::get_Type()
extern "C"  int32_t TransformGestureBase_get_Type_m2365796206 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::set_Type(TouchScript.Gestures.Base.TransformGestureBase/TransformType)
extern "C"  void TransformGestureBase_set_Type_m3341946329 (TransformGestureBase_t91965108 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::get_MinScreenPointsDistance()
extern "C"  float TransformGestureBase_get_MinScreenPointsDistance_m252658581 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::set_MinScreenPointsDistance(System.Single)
extern "C"  void TransformGestureBase_set_MinScreenPointsDistance_m3902032758 (TransformGestureBase_t91965108 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::get_ScreenTransformThreshold()
extern "C"  float TransformGestureBase_get_ScreenTransformThreshold_m3452962988 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::set_ScreenTransformThreshold(System.Single)
extern "C"  void TransformGestureBase_set_ScreenTransformThreshold_m4082078199 (TransformGestureBase_t91965108 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::get_DeltaPosition()
extern "C"  Vector3_t2243707580  TransformGestureBase_get_DeltaPosition_m3899573698 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::get_DeltaRotation()
extern "C"  float TransformGestureBase_get_DeltaRotation_m175655455 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::get_DeltaScale()
extern "C"  float TransformGestureBase_get_DeltaScale_m3328095351 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::get_ScreenPosition()
extern "C"  Vector2_t2243707579  TransformGestureBase_get_ScreenPosition_m2698954779 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::get_PreviousScreenPosition()
extern "C"  Vector2_t2243707579  TransformGestureBase_get_PreviousScreenPosition_m1580795116 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::OnEnable()
extern "C"  void TransformGestureBase_OnEnable_m3551360202 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TransformGestureBase_touchesBegan_m3408895766 (TransformGestureBase_t91965108 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TransformGestureBase_touchesMoved_m1608233808 (TransformGestureBase_t91965108 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TransformGestureBase_touchesEnded_m975793471 (TransformGestureBase_t91965108 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::onBegan()
extern "C"  void TransformGestureBase_onBegan_m976397444 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::onChanged()
extern "C"  void TransformGestureBase_onChanged_m619156635 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::onRecognized()
extern "C"  void TransformGestureBase_onRecognized_m3656089561 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::reset()
extern "C"  void TransformGestureBase_reset_m976465313 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float TransformGestureBase_doRotation_m2103328458 (TransformGestureBase_t91965108 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.Base.TransformGestureBase::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float TransformGestureBase_doScaling_m263396795 (TransformGestureBase_t91965108 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  TransformGestureBase_doOnePointTranslation_m2737473309 (TransformGestureBase_t91965108 * __this, Vector2_t2243707579  ___oldScreenPos0, Vector2_t2243707579  ___newScreenPos1, ProjectionParams_t2712959773 * ___projectionParams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  TransformGestureBase_doTwoPointTranslation_m3002328993 (TransformGestureBase_t91965108 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, float ___dR4, float ___dS5, ProjectionParams_t2712959773 * ___projectionParams6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Gestures.Base.TransformGestureBase::getNumPoints()
extern "C"  int32_t TransformGestureBase_getNumPoints_m2604532957 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Base.TransformGestureBase::relevantTouches1(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  bool TransformGestureBase_relevantTouches1_m574720847 (TransformGestureBase_t91965108 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Gestures.Base.TransformGestureBase::relevantTouches2(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  bool TransformGestureBase_relevantTouches2_m2807264782 (TransformGestureBase_t91965108 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::getPointScreenPosition(System.Int32)
extern "C"  Vector2_t2243707579  TransformGestureBase_getPointScreenPosition_m3761784101 (TransformGestureBase_t91965108 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::getPointPreviousScreenPosition(System.Int32)
extern "C"  Vector2_t2243707579  TransformGestureBase_getPointPreviousScreenPosition_m2871926278 (TransformGestureBase_t91965108 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::updateMinScreenPointsDistance()
extern "C"  void TransformGestureBase_updateMinScreenPointsDistance_m3956727531 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.Base.TransformGestureBase::updateScreenTransformThreshold()
extern "C"  void TransformGestureBase_updateScreenTransformThreshold_m4286192822 (TransformGestureBase_t91965108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
