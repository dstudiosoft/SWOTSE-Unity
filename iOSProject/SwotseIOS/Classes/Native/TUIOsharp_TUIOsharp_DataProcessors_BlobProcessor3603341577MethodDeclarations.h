﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TUIOsharp.DataProcessors.BlobProcessor
struct BlobProcessor_t3603341577;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct EventHandler_1_t2154285351;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscMessage2764280154.h"

// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobAdded_m1362271275 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobAdded_m3134592944 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobUpdated_m1642149482 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobUpdated_m1870432989 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobRemoved_m486758865 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobRemoved_m1953649818 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::set_FrameNumber(System.Int32)
extern "C"  void BlobProcessor_set_FrameNumber_m1366367337 (BlobProcessor_t3603341577 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::.ctor()
extern "C"  void BlobProcessor__ctor_m1736416811 (BlobProcessor_t3603341577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.BlobProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void BlobProcessor_ProcessMessage_m3196822674 (BlobProcessor_t3603341577 * __this, OscMessage_t2764280154 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
