﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32_MIB_ICMP_EX
struct Win32_MIB_ICMP_EX_t2748725486;
struct Win32_MIB_ICMP_EX_t2748725486_marshaled_pinvoke;
struct Win32_MIB_ICMP_EX_t2748725486_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Win32_MIB_ICMP_EX_t2748725486;
struct Win32_MIB_ICMP_EX_t2748725486_marshaled_pinvoke;

extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_pinvoke(const Win32_MIB_ICMP_EX_t2748725486& unmarshaled, Win32_MIB_ICMP_EX_t2748725486_marshaled_pinvoke& marshaled);
extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_pinvoke_back(const Win32_MIB_ICMP_EX_t2748725486_marshaled_pinvoke& marshaled, Win32_MIB_ICMP_EX_t2748725486& unmarshaled);
extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_pinvoke_cleanup(Win32_MIB_ICMP_EX_t2748725486_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Win32_MIB_ICMP_EX_t2748725486;
struct Win32_MIB_ICMP_EX_t2748725486_marshaled_com;

extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_com(const Win32_MIB_ICMP_EX_t2748725486& unmarshaled, Win32_MIB_ICMP_EX_t2748725486_marshaled_com& marshaled);
extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_com_back(const Win32_MIB_ICMP_EX_t2748725486_marshaled_com& marshaled, Win32_MIB_ICMP_EX_t2748725486& unmarshaled);
extern "C" void Win32_MIB_ICMP_EX_t2748725486_marshal_com_cleanup(Win32_MIB_ICMP_EX_t2748725486_marshaled_com& marshaled);
