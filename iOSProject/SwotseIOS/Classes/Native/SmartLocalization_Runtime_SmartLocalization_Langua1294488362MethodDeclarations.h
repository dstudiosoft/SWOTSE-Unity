﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2
struct U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t1294488362;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"

// System.Void SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::.ctor()
extern "C"  void U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2__ctor_m51981622 (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t1294488362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager/<IsLanguageSupportedEnglishName>c__AnonStorey2::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_U3CU3Em__0_m4247736856 (U3CIsLanguageSupportedEnglishNameU3Ec__AnonStorey2_t1294488362 * __this, SmartCultureInfo_t2361725737 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
