﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BattleReportResourcesLine
struct BattleReportResourcesLine_t2955920725;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<ResourcesModel>
struct List_1_t2348107090;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportResourcesTableController
struct  BattleReportResourcesTableController_t4005631621  : public MonoBehaviour_t1158329972
{
public:
	// BattleReportResourcesLine BattleReportResourcesTableController::m_cellPrefab
	BattleReportResourcesLine_t2955920725 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportResourcesTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.Generic.List`1<ResourcesModel> BattleReportResourcesTableController::armyResources
	List_1_t2348107090 * ___armyResources_4;
	// System.Int32 BattleReportResourcesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t4005631621, ___m_cellPrefab_2)); }
	inline BattleReportResourcesLine_t2955920725 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportResourcesLine_t2955920725 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportResourcesLine_t2955920725 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t4005631621, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_armyResources_4() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t4005631621, ___armyResources_4)); }
	inline List_1_t2348107090 * get_armyResources_4() const { return ___armyResources_4; }
	inline List_1_t2348107090 ** get_address_of_armyResources_4() { return &___armyResources_4; }
	inline void set_armyResources_4(List_1_t2348107090 * value)
	{
		___armyResources_4 = value;
		Il2CppCodeGenWriteBarrier(&___armyResources_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t4005631621, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
