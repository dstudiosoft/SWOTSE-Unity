﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5
struct U3CResetCoroutineU3Ec__Iterator5_t2926400508;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::.ctor()
extern "C"  void U3CResetCoroutineU3Ec__Iterator5__ctor_m1077380471 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CResetCoroutineU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3302291361 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CResetCoroutineU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2712343993 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::MoveNext()
extern "C"  bool U3CResetCoroutineU3Ec__Iterator5_MoveNext_m589328821 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::Dispose()
extern "C"  void U3CResetCoroutineU3Ec__Iterator5_Dispose_m3910511464 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator5::Reset()
extern "C"  void U3CResetCoroutineU3Ec__Iterator5_Reset_m3226374250 (U3CResetCoroutineU3Ec__Iterator5_t2926400508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
