﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.TransformGesture
struct TransformGesture_t780972309;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_TransformGes462923329.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_ProjectionPar2712959773.h"

// System.Void TouchScript.Gestures.TransformGesture::.ctor()
extern "C"  void TransformGesture__ctor_m2133823226 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.TransformGesture/ProjectionType TouchScript.Gestures.TransformGesture::get_Projection()
extern "C"  int32_t TransformGesture_get_Projection_m1174406928 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::set_Projection(TouchScript.Gestures.TransformGesture/ProjectionType)
extern "C"  void TransformGesture_set_Projection_m4157936393 (TransformGesture_t780972309 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::get_ProjectionPlaneNormal()
extern "C"  Vector3_t2243707580  TransformGesture_get_ProjectionPlaneNormal_m1387588621 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::set_ProjectionPlaneNormal(UnityEngine.Vector3)
extern "C"  void TransformGesture_set_ProjectionPlaneNormal_m3875833844 (TransformGesture_t780972309 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane TouchScript.Gestures.TransformGesture::get_TransformPlane()
extern "C"  Plane_t3727654732  TransformGesture_get_TransformPlane_m4082771791 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::get_LocalDeltaPosition()
extern "C"  Vector3_t2243707580  TransformGesture_get_LocalDeltaPosition_m3103566809 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::get_RotationAxis()
extern "C"  Vector3_t2243707580  TransformGesture_get_RotationAxis_m1884773032 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::ApplyTransform(UnityEngine.Transform)
extern "C"  void TransformGesture_ApplyTransform_m972184173 (TransformGesture_t780972309 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::Awake()
extern "C"  void TransformGesture_Awake_m3398162099 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::OnEnable()
extern "C"  void TransformGesture_OnEnable_m734681822 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TransformGesture_touchesBegan_m562262910 (TransformGesture_t780972309 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::projectScaledRotated(UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  TransformGesture_projectScaledRotated_m3166887127 (TransformGesture_t780972309 * __this, Vector2_t2243707579  ___point0, float ___dR1, float ___dS2, ProjectionParams_t2712959773 * ___projectionParams3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.TransformGesture::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float TransformGesture_doRotation_m2865982466 (TransformGesture_t780972309 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.TransformGesture::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float TransformGesture_doScaling_m1166321873 (TransformGesture_t780972309 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  TransformGesture_doOnePointTranslation_m507459883 (TransformGesture_t780972309 * __this, Vector2_t2243707579  ___oldScreenPos0, Vector2_t2243707579  ___newScreenPos1, ProjectionParams_t2712959773 * ___projectionParams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  TransformGesture_doTwoPointTranslation_m3325351999 (TransformGesture_t780972309 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, float ___dR4, float ___dS5, ProjectionParams_t2712959773 * ___projectionParams6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::updateProjectionPlane()
extern "C"  void TransformGesture_updateProjectionPlane_m2233396956 (TransformGesture_t780972309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformStarted_m3074974047 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformStarted_m1714936466 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_add_Transformed_m4135338823 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_remove_Transformed_m3729789470 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformCompleted_m1435303435 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.TransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void TransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformCompleted_m50194326 (TransformGesture_t780972309 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
