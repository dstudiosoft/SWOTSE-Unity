﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketBuyTableController
struct MarketBuyTableController_t1709776860;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void MarketBuyTableController::.ctor()
extern "C"  void MarketBuyTableController__ctor_m345595019 (MarketBuyTableController_t1709776860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketBuyTableController::Start()
extern "C"  void MarketBuyTableController_Start_m1792213319 (MarketBuyTableController_t1709776860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MarketBuyTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t MarketBuyTableController_GetNumberOfRowsForTableView_m1197506847 (MarketBuyTableController_t1709776860 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MarketBuyTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float MarketBuyTableController_GetHeightForRowInTableView_m3765088751 (MarketBuyTableController_t1709776860 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell MarketBuyTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * MarketBuyTableController_GetCellForRowInTableView_m1967857658 (MarketBuyTableController_t1709776860 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketBuyTableController::ReloadTable(System.Collections.ArrayList)
extern "C"  void MarketBuyTableController_ReloadTable_m763374387 (MarketBuyTableController_t1709776860 * __this, ArrayList_t4252133567 * ___managerLots0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
