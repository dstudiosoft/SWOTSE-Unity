﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3773839005.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2309940817_gshared (Enumerator_t3773839005 * __this, Dictionary_2_t1084335567 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2309940817(__this, ___host0, method) ((  void (*) (Enumerator_t3773839005 *, Dictionary_2_t1084335567 *, const MethodInfo*))Enumerator__ctor_m2309940817_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m962189726_gshared (Enumerator_t3773839005 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m962189726(__this, method) ((  Il2CppObject * (*) (Enumerator_t3773839005 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m962189726_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1176142168_gshared (Enumerator_t3773839005 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1176142168(__this, method) ((  void (*) (Enumerator_t3773839005 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1176142168_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m1508664553_gshared (Enumerator_t3773839005 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1508664553(__this, method) ((  void (*) (Enumerator_t3773839005 *, const MethodInfo*))Enumerator_Dispose_m1508664553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m517799476_gshared (Enumerator_t3773839005 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m517799476(__this, method) ((  bool (*) (Enumerator_t3773839005 *, const MethodInfo*))Enumerator_MoveNext_m517799476_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Single>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2371065556_gshared (Enumerator_t3773839005 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2371065556(__this, method) ((  int32_t (*) (Enumerator_t3773839005 *, const MethodInfo*))Enumerator_get_Current_m2371065556_gshared)(__this, method)
