﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.InstallRootCerts
struct InstallRootCerts_t2159695568;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t2766455145;
// System.String
struct String_t;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Unity.InstallRootCerts::.ctor()
extern "C"  void InstallRootCerts__ctor_m42736131 (InstallRootCerts_t2159695568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Unity.InstallRootCerts Firebase.Unity.InstallRootCerts::get_Instance()
extern "C"  InstallRootCerts_t2159695568 * InstallRootCerts_get_Instance_m3793291190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.InstallRootCerts::CFRelease(System.IntPtr)
extern "C"  void InstallRootCerts_CFRelease_m2611521985 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Unity.InstallRootCerts::IsCertBugPresent()
extern "C"  bool InstallRootCerts_IsCertBugPresent_m3556708564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Byte[]> Firebase.Unity.InstallRootCerts::DecodeBase64Blobs(System.String,System.String,System.String)
extern "C"  List_1_t2766455145 * InstallRootCerts_DecodeBase64Blobs_m1109105576 (Il2CppObject * __this /* static, unused */, String_t* ___base64BlobList0, String_t* ___startLine1, String_t* ___endLine2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeCertificateCollectionFromString(System.String)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCertificateCollectionFromString_m841159762 (Il2CppObject * __this /* static, unused */, String_t* ___certString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeDefaultCollection()
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeDefaultCollection_m2968556453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeCollection(Firebase.FirebaseApp)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCollection_m2160110785 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.InstallRootCerts::InstallDefaultCRLs(System.String,System.String)
extern "C"  void InstallRootCerts_InstallDefaultCRLs_m773073831 (Il2CppObject * __this /* static, unused */, String_t* ___resource_name0, String_t* ___directory1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.InstallRootCerts::HackRefreshMonoRootStore()
extern "C"  void InstallRootCerts_HackRefreshMonoRootStore_m3546260873 (InstallRootCerts_t2159695568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::Install(Firebase.FirebaseApp)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_Install_m3522672454 (InstallRootCerts_t2159695568 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.InstallRootCerts::.cctor()
extern "C"  void InstallRootCerts__cctor_m3017225184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
