﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.SimpleTableViewController
struct SimpleTableViewController_t3384536075;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void Tacticsoft.Examples.SimpleTableViewController::.ctor()
extern "C"  void SimpleTableViewController__ctor_m955586811 (SimpleTableViewController_t3384536075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.SimpleTableViewController::Start()
extern "C"  void SimpleTableViewController_Start_m1677975327 (SimpleTableViewController_t3384536075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.SimpleTableViewController::SendBeer()
extern "C"  void SimpleTableViewController_SendBeer_m1911717343 (SimpleTableViewController_t3384536075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.Examples.SimpleTableViewController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t SimpleTableViewController_GetNumberOfRowsForTableView_m3663128879 (SimpleTableViewController_t3384536075 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.Examples.SimpleTableViewController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float SimpleTableViewController_GetHeightForRowInTableView_m3275175751 (SimpleTableViewController_t3384536075 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.Examples.SimpleTableViewController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * SimpleTableViewController_GetCellForRowInTableView_m3967792182 (SimpleTableViewController_t3384536075 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.SimpleTableViewController::TableViewCellVisibilityChanged(System.Int32,System.Boolean)
extern "C"  void SimpleTableViewController_TableViewCellVisibilityChanged_m299450064 (SimpleTableViewController_t3384536075 * __this, int32_t ___row0, bool ___isVisible1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
