﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.GatewayIPAddressInformation
struct GatewayIPAddressInformation_t153474369;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.GatewayIPAddressInformation::.ctor()
extern "C"  void GatewayIPAddressInformation__ctor_m1234926259 (GatewayIPAddressInformation_t153474369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
