﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.VerificationException
struct VerificationException_t2859188124;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void System.Security.VerificationException::.ctor()
extern "C"  void VerificationException__ctor_m615055673 (VerificationException_t2859188124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.VerificationException::.ctor(System.String)
extern "C"  void VerificationException__ctor_m2008339695 (VerificationException_t2859188124 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.VerificationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void VerificationException__ctor_m1305123898 (VerificationException_t2859188124 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.VerificationException::.ctor(System.String,System.Exception)
extern "C"  void VerificationException__ctor_m699651879 (VerificationException_t2859188124 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
