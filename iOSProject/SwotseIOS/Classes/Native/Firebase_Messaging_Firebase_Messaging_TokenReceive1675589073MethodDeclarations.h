﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.TokenReceivedEventArgs
struct TokenReceivedEventArgs_t1675589073;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Messaging.TokenReceivedEventArgs::.ctor(System.String)
extern "C"  void TokenReceivedEventArgs__ctor_m4293293464 (TokenReceivedEventArgs_t1675589073 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.TokenReceivedEventArgs::get_Token()
extern "C"  String_t* TokenReceivedEventArgs_get_Token_m109762161 (TokenReceivedEventArgs_t1675589073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.TokenReceivedEventArgs::set_Token(System.String)
extern "C"  void TokenReceivedEventArgs_set_Token_m478444946 (TokenReceivedEventArgs_t1675589073 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
