﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPv4InterfaceProperties
struct Win32IPv4InterfaceProperties_t164294404;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_INFO
struct Win32_IP_ADAPTER_INFO_t2310876292;
// System.Net.NetworkInformation.Win32_IP_PER_ADAPTER_INFO
struct Win32_IP_PER_ADAPTER_INFO_t1280460216;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP2310876292.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"
#include "System_System_Net_NetworkInformation_Win32_IP_PER_1280460216.h"

// System.Void System.Net.NetworkInformation.Win32IPv4InterfaceProperties::.ctor(System.Net.NetworkInformation.Win32_IP_ADAPTER_INFO,System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPv4InterfaceProperties__ctor_m3410468810 (Win32IPv4InterfaceProperties_t164294404 * __this, Win32_IP_ADAPTER_INFO_t2310876292 * ___ainfo0, Win32_MIB_IFROW_t4215928996  ___mib1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPv4InterfaceProperties::GetPerAdapterInfo(System.Int32,System.Net.NetworkInformation.Win32_IP_PER_ADAPTER_INFO,System.Int32&)
extern "C"  int32_t Win32IPv4InterfaceProperties_GetPerAdapterInfo_m333658868 (Il2CppObject * __this /* static, unused */, int32_t ___IfIndex0, Win32_IP_PER_ADAPTER_INFO_t1280460216 * ___pPerAdapterInfo1, int32_t* ___pOutBufLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_Index()
extern "C"  int32_t Win32IPv4InterfaceProperties_get_Index_m1169040955 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_IsAutomaticPrivateAddressingActive()
extern "C"  bool Win32IPv4InterfaceProperties_get_IsAutomaticPrivateAddressingActive_m3446929695 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_IsAutomaticPrivateAddressingEnabled()
extern "C"  bool Win32IPv4InterfaceProperties_get_IsAutomaticPrivateAddressingEnabled_m3495769092 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_IsDhcpEnabled()
extern "C"  bool Win32IPv4InterfaceProperties_get_IsDhcpEnabled_m4044672155 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_IsForwardingEnabled()
extern "C"  bool Win32IPv4InterfaceProperties_get_IsForwardingEnabled_m576339303 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_Mtu()
extern "C"  int32_t Win32IPv4InterfaceProperties_get_Mtu_m255311569 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPv4InterfaceProperties::get_UsesWins()
extern "C"  bool Win32IPv4InterfaceProperties_get_UsesWins_m3663565512 (Win32IPv4InterfaceProperties_t164294404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
