﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// AllianceManager
struct AllianceManager_t344187419;
// BattleManager
struct BattleManager_t4022130644;
// FinancialContentChanger
struct FinancialContentChanger_t4019109810;
// UnitsModel
struct UnitsModel_t3847956398;
// ResourcesModel
struct ResourcesModel_t2533508513;
// MessageContentManager
struct MessageContentManager_t985817454;
// MarketManager
struct MarketManager_t880414981;
// BattleReportContentManager
struct BattleReportContentManager_t922857224;
// ResidenceProductionContentManager
struct ResidenceProductionContentManager_t3650353576;
// System.Collections.Generic.Dictionary`2<System.Int64,ArmyModel>
struct Dictionary_2_t3131312889;
// SimpleEvent
struct SimpleEvent_t129249603;
// AllianceModel
struct AllianceModel_t2995969982;
// System.Collections.Generic.Dictionary`2<System.Int64,UserModel>
struct Dictionary_2_t2416571389;
// System.Collections.Generic.Dictionary`2<System.Int64,AllianceModel>
struct Dictionary_2_t4058609766;
// BarbarianRecruitManager
struct BarbarianRecruitManager_t3050290676;
// BuildingsManager
struct BuildingsManager_t3721263023;
// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel>
struct Dictionary_2_t3346051284;
// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel>
struct Dictionary_2_t3480614145;
// DispatchManager
struct DispatchManager_t670234083;
// LoginManager
struct LoginManager_t1249555276;
// CityChangerContentManager
struct CityChangerContentManager_t1075607021;
// CityDeedContentManager
struct CityDeedContentManager_t1851112673;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UserModel
struct UserModel_t1353931605;
// BuildingModel
struct BuildingModel_t2283411500;
// WorldFieldModel
struct WorldFieldModel_t2417974361;
// BuildingConstantModel
struct BuildingConstantModel_t2655945000;
// ConfirmationEvent
struct ConfirmationEvent_t890979749;
// GoogleMobileAds.Api.BannerView
struct BannerView_t2480907735;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t207380487;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_t2288675867;
// UnitConstantModel
struct UnitConstantModel_t3582189408;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// BattleReportModel
struct BattleReportModel_t4250794538;
// BattleReportUnitTableController
struct BattleReportUnitTableController_t1840302685;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t913674750;
// ConstructionPitManager
struct ConstructionPitManager_t848510622;
// System.Collections.Generic.List`1<ArmyGeneralModel>
struct List_1_t2209824963;
// ArmyGeneralModel
struct ArmyGeneralModel_t737750221;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// FinanceTableController
struct FinanceTableController_t3435833683;
// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel>
struct Dictionary_2_t3367445707;
// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2609779151;
// ArmyModel
struct ArmyModel_t2068673105;
// BattleReportResourcesTableController
struct BattleReportResourcesTableController_t1929107072;
// BattleReportItemsTableController
struct BattleReportItemsTableController_t14710115;
// IReloadable
struct IReloadable_t494058401;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t3521823603;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t447389798;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// LanguageChangerContentManager
struct LanguageChangerContentManager_t2232597662;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// TextLocalizer
struct TextLocalizer_t4021138871;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t1752731834;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// MarketBuyTableController
struct MarketBuyTableController_t892139868;
// MarketSellTableController
struct MarketSellTableController_t878431470;
// UserMessageReportModel
struct UserMessageReportModel_t4065405063;
// MailInboxTableController
struct MailInboxTableController_t2667561573;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCREATENEWALLLIANCEU3EC__ITERATORF_T126037287_H
#define U3CCREATENEWALLLIANCEU3EC__ITERATORF_T126037287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<CreateNewAllliance>c__IteratorF
struct  U3CCreateNewAlllianceU3Ec__IteratorF_t126037287  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<CreateNewAllliance>c__IteratorF::<newForm>__0
	WWWForm_t4064702195 * ___U3CnewFormU3E__0_0;
	// System.String AllianceManager/<CreateNewAllliance>c__IteratorF::name
	String_t* ___name_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<CreateNewAllliance>c__IteratorF::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<CreateNewAllliance>c__IteratorF::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<CreateNewAllliance>c__IteratorF::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<CreateNewAllliance>c__IteratorF::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<CreateNewAllliance>c__IteratorF::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CnewFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U3CnewFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CnewFormU3E__0_0() const { return ___U3CnewFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CnewFormU3E__0_0() { return &___U3CnewFormU3E__0_0; }
	inline void set_U3CnewFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CnewFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCreateNewAlllianceU3Ec__IteratorF_t126037287, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATENEWALLLIANCEU3EC__ITERATORF_T126037287_H
#ifndef U3CUPDATEGUIDELINEU3EC__ITERATOR6_T1110526823_H
#define U3CUPDATEGUIDELINEU3EC__ITERATOR6_T1110526823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<UpdateGuideline>c__Iterator6
struct  U3CUpdateGuidelineU3Ec__Iterator6_t1110526823  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<UpdateGuideline>c__Iterator6::<requestForm>__0
	WWWForm_t4064702195 * ___U3CrequestFormU3E__0_0;
	// System.String AllianceManager/<UpdateGuideline>c__Iterator6::guideline
	String_t* ___guideline_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<UpdateGuideline>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<UpdateGuideline>c__Iterator6::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<UpdateGuideline>c__Iterator6::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<UpdateGuideline>c__Iterator6::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<UpdateGuideline>c__Iterator6::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_guideline_1() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___guideline_1)); }
	inline String_t* get_guideline_1() const { return ___guideline_1; }
	inline String_t** get_address_of_guideline_1() { return &___guideline_1; }
	inline void set_guideline_1(String_t* value)
	{
		___guideline_1 = value;
		Il2CppCodeGenWriteBarrier((&___guideline_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUpdateGuidelineU3Ec__Iterator6_t1110526823, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEGUIDELINEU3EC__ITERATOR6_T1110526823_H
#ifndef U3CREMOVEENEMYU3EC__ITERATORE_T2306474738_H
#define U3CREMOVEENEMYU3EC__ITERATORE_T2306474738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<RemoveEnemy>c__IteratorE
struct  U3CRemoveEnemyU3Ec__IteratorE_t2306474738  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<RemoveEnemy>c__IteratorE::<removeForm>__0
	WWWForm_t4064702195 * ___U3CremoveFormU3E__0_0;
	// System.Int64 AllianceManager/<RemoveEnemy>c__IteratorE::allianceId
	int64_t ___allianceId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<RemoveEnemy>c__IteratorE::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<RemoveEnemy>c__IteratorE::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<RemoveEnemy>c__IteratorE::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<RemoveEnemy>c__IteratorE::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<RemoveEnemy>c__IteratorE::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CremoveFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U3CremoveFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CremoveFormU3E__0_0() const { return ___U3CremoveFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CremoveFormU3E__0_0() { return &___U3CremoveFormU3E__0_0; }
	inline void set_U3CremoveFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CremoveFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CremoveFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_allianceId_1() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___allianceId_1)); }
	inline int64_t get_allianceId_1() const { return ___allianceId_1; }
	inline int64_t* get_address_of_allianceId_1() { return &___allianceId_1; }
	inline void set_allianceId_1(int64_t value)
	{
		___allianceId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRemoveEnemyU3Ec__IteratorE_t2306474738, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEENEMYU3EC__ITERATORE_T2306474738_H
#ifndef U3CCANCELATTACKU3EC__ITERATOR3_T2200631826_H
#define U3CCANCELATTACKU3EC__ITERATOR3_T2200631826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager/<CancelAttack>c__Iterator3
struct  U3CCancelAttackU3Ec__Iterator3_t2200631826  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BattleManager/<CancelAttack>c__Iterator3::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.Int64 BattleManager/<CancelAttack>c__Iterator3::armyId
	int64_t ___armyId_1;
	// UnityEngine.Networking.UnityWebRequest BattleManager/<CancelAttack>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BattleManager BattleManager/<CancelAttack>c__Iterator3::$this
	BattleManager_t4022130644 * ___U24this_3;
	// System.Object BattleManager/<CancelAttack>c__Iterator3::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BattleManager/<CancelAttack>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 BattleManager/<CancelAttack>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_armyId_1() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___armyId_1)); }
	inline int64_t get_armyId_1() const { return ___armyId_1; }
	inline int64_t* get_address_of_armyId_1() { return &___armyId_1; }
	inline void set_armyId_1(int64_t value)
	{
		___armyId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U24this_3)); }
	inline BattleManager_t4022130644 * get_U24this_3() const { return ___U24this_3; }
	inline BattleManager_t4022130644 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BattleManager_t4022130644 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelAttackU3Ec__Iterator3_t2200631826, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELATTACKU3EC__ITERATOR3_T2200631826_H
#ifndef U3CADDENEMYU3EC__ITERATORD_T3744444522_H
#define U3CADDENEMYU3EC__ITERATORD_T3744444522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<AddEnemy>c__IteratorD
struct  U3CAddEnemyU3Ec__IteratorD_t3744444522  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<AddEnemy>c__IteratorD::<addForm>__0
	WWWForm_t4064702195 * ___U3CaddFormU3E__0_0;
	// System.Int64 AllianceManager/<AddEnemy>c__IteratorD::allianceId
	int64_t ___allianceId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<AddEnemy>c__IteratorD::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<AddEnemy>c__IteratorD::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<AddEnemy>c__IteratorD::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<AddEnemy>c__IteratorD::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<AddEnemy>c__IteratorD::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CaddFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U3CaddFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CaddFormU3E__0_0() const { return ___U3CaddFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CaddFormU3E__0_0() { return &___U3CaddFormU3E__0_0; }
	inline void set_U3CaddFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CaddFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_allianceId_1() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___allianceId_1)); }
	inline int64_t get_allianceId_1() const { return ___allianceId_1; }
	inline int64_t* get_address_of_allianceId_1() { return &___allianceId_1; }
	inline void set_allianceId_1(int64_t value)
	{
		___allianceId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAddEnemyU3Ec__IteratorD_t3744444522, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDENEMYU3EC__ITERATORD_T3744444522_H
#ifndef U3CGETCITYARMIESU3EC__ITERATOR0_T256880612_H
#define U3CGETCITYARMIESU3EC__ITERATOR0_T256880612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager/<GetCityArmies>c__Iterator0
struct  U3CGetCityArmiesU3Ec__Iterator0_t256880612  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BattleManager/<GetCityArmies>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BattleManager BattleManager/<GetCityArmies>c__Iterator0::$this
	BattleManager_t4022130644 * ___U24this_1;
	// System.Object BattleManager/<GetCityArmies>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BattleManager/<GetCityArmies>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BattleManager/<GetCityArmies>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCityArmiesU3Ec__Iterator0_t256880612, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetCityArmiesU3Ec__Iterator0_t256880612, ___U24this_1)); }
	inline BattleManager_t4022130644 * get_U24this_1() const { return ___U24this_1; }
	inline BattleManager_t4022130644 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BattleManager_t4022130644 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetCityArmiesU3Ec__Iterator0_t256880612, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetCityArmiesU3Ec__Iterator0_t256880612, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetCityArmiesU3Ec__Iterator0_t256880612, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCITYARMIESU3EC__ITERATOR0_T256880612_H
#ifndef U3CGETFINANCEOFFERSU3EC__ITERATOR1_T3666753709_H
#define U3CGETFINANCEOFFERSU3EC__ITERATOR1_T3666753709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger/<GetFinanceOffers>c__Iterator1
struct  U3CGetFinanceOffersU3Ec__Iterator1_t3666753709  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest FinancialContentChanger/<GetFinanceOffers>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// FinancialContentChanger FinancialContentChanger/<GetFinanceOffers>c__Iterator1::$this
	FinancialContentChanger_t4019109810 * ___U24this_1;
	// System.Object FinancialContentChanger/<GetFinanceOffers>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean FinancialContentChanger/<GetFinanceOffers>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 FinancialContentChanger/<GetFinanceOffers>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetFinanceOffersU3Ec__Iterator1_t3666753709, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetFinanceOffersU3Ec__Iterator1_t3666753709, ___U24this_1)); }
	inline FinancialContentChanger_t4019109810 * get_U24this_1() const { return ___U24this_1; }
	inline FinancialContentChanger_t4019109810 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FinancialContentChanger_t4019109810 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetFinanceOffersU3Ec__Iterator1_t3666753709, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetFinanceOffersU3Ec__Iterator1_t3666753709, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetFinanceOffersU3Ec__Iterator1_t3666753709, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFINANCEOFFERSU3EC__ITERATOR1_T3666753709_H
#ifndef U3CGETFINANCEREPORTSU3EC__ITERATOR0_T4010566809_H
#define U3CGETFINANCEREPORTSU3EC__ITERATOR0_T4010566809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger/<GetFinanceReports>c__Iterator0
struct  U3CGetFinanceReportsU3Ec__Iterator0_t4010566809  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest FinancialContentChanger/<GetFinanceReports>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// FinancialContentChanger FinancialContentChanger/<GetFinanceReports>c__Iterator0::$this
	FinancialContentChanger_t4019109810 * ___U24this_1;
	// System.Object FinancialContentChanger/<GetFinanceReports>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean FinancialContentChanger/<GetFinanceReports>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 FinancialContentChanger/<GetFinanceReports>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__Iterator0_t4010566809, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__Iterator0_t4010566809, ___U24this_1)); }
	inline FinancialContentChanger_t4019109810 * get_U24this_1() const { return ___U24this_1; }
	inline FinancialContentChanger_t4019109810 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FinancialContentChanger_t4019109810 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__Iterator0_t4010566809, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__Iterator0_t4010566809, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__Iterator0_t4010566809, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFINANCEREPORTSU3EC__ITERATOR0_T4010566809_H
#ifndef U3CCANCELOFFERREQUESTU3EC__ITERATOR2_T3904290043_H
#define U3CCANCELOFFERREQUESTU3EC__ITERATOR2_T3904290043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger/<CancelOfferRequest>c__Iterator2
struct  U3CCancelOfferRequestU3Ec__Iterator2_t3904290043  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm FinancialContentChanger/<CancelOfferRequest>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String FinancialContentChanger/<CancelOfferRequest>c__Iterator2::id
	String_t* ___id_1;
	// UnityEngine.Networking.UnityWebRequest FinancialContentChanger/<CancelOfferRequest>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// FinancialContentChanger FinancialContentChanger/<CancelOfferRequest>c__Iterator2::$this
	FinancialContentChanger_t4019109810 * ___U24this_3;
	// System.Object FinancialContentChanger/<CancelOfferRequest>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean FinancialContentChanger/<CancelOfferRequest>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 FinancialContentChanger/<CancelOfferRequest>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U24this_3)); }
	inline FinancialContentChanger_t4019109810 * get_U24this_3() const { return ___U24this_3; }
	inline FinancialContentChanger_t4019109810 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(FinancialContentChanger_t4019109810 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelOfferRequestU3Ec__Iterator2_t3904290043, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELOFFERREQUESTU3EC__ITERATOR2_T3904290043_H
#ifndef U3CMARCHU3EC__ITERATOR1_T976351973_H
#define U3CMARCHU3EC__ITERATOR1_T976351973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager/<March>c__Iterator1
struct  U3CMarchU3Ec__Iterator1_t976351973  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BattleManager/<March>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String BattleManager/<March>c__Iterator1::attcakType
	String_t* ___attcakType_1;
	// UnitsModel BattleManager/<March>c__Iterator1::army
	UnitsModel_t3847956398 * ___army_2;
	// ResourcesModel BattleManager/<March>c__Iterator1::resources
	ResourcesModel_t2533508513 * ___resources_3;
	// System.Int64 BattleManager/<March>c__Iterator1::destX
	int64_t ___destX_4;
	// System.Int64 BattleManager/<March>c__Iterator1::destY
	int64_t ___destY_5;
	// System.Int64 BattleManager/<March>c__Iterator1::knightId
	int64_t ___knightId_6;
	// UnityEngine.Networking.UnityWebRequest BattleManager/<March>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_7;
	// BattleManager BattleManager/<March>c__Iterator1::$this
	BattleManager_t4022130644 * ___U24this_8;
	// System.Object BattleManager/<March>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean BattleManager/<March>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 BattleManager/<March>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_attcakType_1() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___attcakType_1)); }
	inline String_t* get_attcakType_1() const { return ___attcakType_1; }
	inline String_t** get_address_of_attcakType_1() { return &___attcakType_1; }
	inline void set_attcakType_1(String_t* value)
	{
		___attcakType_1 = value;
		Il2CppCodeGenWriteBarrier((&___attcakType_1), value);
	}

	inline static int32_t get_offset_of_army_2() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___army_2)); }
	inline UnitsModel_t3847956398 * get_army_2() const { return ___army_2; }
	inline UnitsModel_t3847956398 ** get_address_of_army_2() { return &___army_2; }
	inline void set_army_2(UnitsModel_t3847956398 * value)
	{
		___army_2 = value;
		Il2CppCodeGenWriteBarrier((&___army_2), value);
	}

	inline static int32_t get_offset_of_resources_3() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___resources_3)); }
	inline ResourcesModel_t2533508513 * get_resources_3() const { return ___resources_3; }
	inline ResourcesModel_t2533508513 ** get_address_of_resources_3() { return &___resources_3; }
	inline void set_resources_3(ResourcesModel_t2533508513 * value)
	{
		___resources_3 = value;
		Il2CppCodeGenWriteBarrier((&___resources_3), value);
	}

	inline static int32_t get_offset_of_destX_4() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___destX_4)); }
	inline int64_t get_destX_4() const { return ___destX_4; }
	inline int64_t* get_address_of_destX_4() { return &___destX_4; }
	inline void set_destX_4(int64_t value)
	{
		___destX_4 = value;
	}

	inline static int32_t get_offset_of_destY_5() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___destY_5)); }
	inline int64_t get_destY_5() const { return ___destY_5; }
	inline int64_t* get_address_of_destY_5() { return &___destY_5; }
	inline void set_destY_5(int64_t value)
	{
		___destY_5 = value;
	}

	inline static int32_t get_offset_of_knightId_6() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___knightId_6)); }
	inline int64_t get_knightId_6() const { return ___knightId_6; }
	inline int64_t* get_address_of_knightId_6() { return &___knightId_6; }
	inline void set_knightId_6(int64_t value)
	{
		___knightId_6 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_7() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U3CrequestU3E__0_7)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_7() const { return ___U3CrequestU3E__0_7; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_7() { return &___U3CrequestU3E__0_7; }
	inline void set_U3CrequestU3E__0_7(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U24this_8)); }
	inline BattleManager_t4022130644 * get_U24this_8() const { return ___U24this_8; }
	inline BattleManager_t4022130644 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(BattleManager_t4022130644 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CMarchU3Ec__Iterator1_t976351973, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARCHU3EC__ITERATOR1_T976351973_H
#ifndef U3CRECRUITBARBARIANSU3EC__ITERATOR2_T783165763_H
#define U3CRECRUITBARBARIANSU3EC__ITERATOR2_T783165763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager/<RecruitBarbarians>c__Iterator2
struct  U3CRecruitBarbariansU3Ec__Iterator2_t783165763  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BattleManager/<RecruitBarbarians>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.Int64 BattleManager/<RecruitBarbarians>c__Iterator2::barbariansId
	int64_t ___barbariansId_1;
	// UnitsModel BattleManager/<RecruitBarbarians>c__Iterator2::army
	UnitsModel_t3847956398 * ___army_2;
	// System.Int64 BattleManager/<RecruitBarbarians>c__Iterator2::destX
	int64_t ___destX_3;
	// System.Int64 BattleManager/<RecruitBarbarians>c__Iterator2::destY
	int64_t ___destY_4;
	// UnityEngine.Networking.UnityWebRequest BattleManager/<RecruitBarbarians>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// BattleManager BattleManager/<RecruitBarbarians>c__Iterator2::$this
	BattleManager_t4022130644 * ___U24this_6;
	// System.Object BattleManager/<RecruitBarbarians>c__Iterator2::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean BattleManager/<RecruitBarbarians>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 BattleManager/<RecruitBarbarians>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_barbariansId_1() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___barbariansId_1)); }
	inline int64_t get_barbariansId_1() const { return ___barbariansId_1; }
	inline int64_t* get_address_of_barbariansId_1() { return &___barbariansId_1; }
	inline void set_barbariansId_1(int64_t value)
	{
		___barbariansId_1 = value;
	}

	inline static int32_t get_offset_of_army_2() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___army_2)); }
	inline UnitsModel_t3847956398 * get_army_2() const { return ___army_2; }
	inline UnitsModel_t3847956398 ** get_address_of_army_2() { return &___army_2; }
	inline void set_army_2(UnitsModel_t3847956398 * value)
	{
		___army_2 = value;
		Il2CppCodeGenWriteBarrier((&___army_2), value);
	}

	inline static int32_t get_offset_of_destX_3() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___destX_3)); }
	inline int64_t get_destX_3() const { return ___destX_3; }
	inline int64_t* get_address_of_destX_3() { return &___destX_3; }
	inline void set_destX_3(int64_t value)
	{
		___destX_3 = value;
	}

	inline static int32_t get_offset_of_destY_4() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___destY_4)); }
	inline int64_t get_destY_4() const { return ___destY_4; }
	inline int64_t* get_address_of_destY_4() { return &___destY_4; }
	inline void set_destY_4(int64_t value)
	{
		___destY_4 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U24this_6)); }
	inline BattleManager_t4022130644 * get_U24this_6() const { return ___U24this_6; }
	inline BattleManager_t4022130644 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(BattleManager_t4022130644 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CRecruitBarbariansU3Ec__Iterator2_t783165763, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECRUITBARBARIANSU3EC__ITERATOR2_T783165763_H
#ifndef U3CSETMEMBERRANKU3EC__ITERATOR9_T700886960_H
#define U3CSETMEMBERRANKU3EC__ITERATOR9_T700886960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<SetMemberRank>c__Iterator9
struct  U3CSetMemberRankU3Ec__Iterator9_t700886960  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<SetMemberRank>c__Iterator9::<rankForm>__0
	WWWForm_t4064702195 * ___U3CrankFormU3E__0_0;
	// System.Int64 AllianceManager/<SetMemberRank>c__Iterator9::userId
	int64_t ___userId_1;
	// System.String AllianceManager/<SetMemberRank>c__Iterator9::rank
	String_t* ___rank_2;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<SetMemberRank>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// AllianceManager AllianceManager/<SetMemberRank>c__Iterator9::$this
	AllianceManager_t344187419 * ___U24this_4;
	// System.Object AllianceManager/<SetMemberRank>c__Iterator9::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean AllianceManager/<SetMemberRank>c__Iterator9::$disposing
	bool ___U24disposing_6;
	// System.Int32 AllianceManager/<SetMemberRank>c__Iterator9::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CrankFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U3CrankFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrankFormU3E__0_0() const { return ___U3CrankFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrankFormU3E__0_0() { return &___U3CrankFormU3E__0_0; }
	inline void set_U3CrankFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrankFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrankFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___userId_1)); }
	inline int64_t get_userId_1() const { return ___userId_1; }
	inline int64_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int64_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier((&___rank_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U24this_4)); }
	inline AllianceManager_t344187419 * get_U24this_4() const { return ___U24this_4; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AllianceManager_t344187419 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSetMemberRankU3Ec__Iterator9_t700886960, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETMEMBERRANKU3EC__ITERATOR9_T700886960_H
#ifndef U3CRESIGNRANKU3EC__ITERATORA_T783501582_H
#define U3CRESIGNRANKU3EC__ITERATORA_T783501582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<ResignRank>c__IteratorA
struct  U3CResignRankU3Ec__IteratorA_t783501582  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<ResignRank>c__IteratorA::<rankForm>__0
	WWWForm_t4064702195 * ___U3CrankFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<ResignRank>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// System.Object AllianceManager/<ResignRank>c__IteratorA::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AllianceManager/<ResignRank>c__IteratorA::$disposing
	bool ___U24disposing_3;
	// System.Int32 AllianceManager/<ResignRank>c__IteratorA::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrankFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CResignRankU3Ec__IteratorA_t783501582, ___U3CrankFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrankFormU3E__0_0() const { return ___U3CrankFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrankFormU3E__0_0() { return &___U3CrankFormU3E__0_0; }
	inline void set_U3CrankFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrankFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrankFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CResignRankU3Ec__IteratorA_t783501582, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CResignRankU3Ec__IteratorA_t783501582, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CResignRankU3Ec__IteratorA_t783501582, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CResignRankU3Ec__IteratorA_t783501582, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESIGNRANKU3EC__ITERATORA_T783501582_H
#ifndef U3CADDALLYU3EC__ITERATORB_T1622364050_H
#define U3CADDALLYU3EC__ITERATORB_T1622364050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<AddAlly>c__IteratorB
struct  U3CAddAllyU3Ec__IteratorB_t1622364050  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<AddAlly>c__IteratorB::<addForm>__0
	WWWForm_t4064702195 * ___U3CaddFormU3E__0_0;
	// System.Int64 AllianceManager/<AddAlly>c__IteratorB::allianceId
	int64_t ___allianceId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<AddAlly>c__IteratorB::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<AddAlly>c__IteratorB::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<AddAlly>c__IteratorB::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<AddAlly>c__IteratorB::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<AddAlly>c__IteratorB::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CaddFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U3CaddFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CaddFormU3E__0_0() const { return ___U3CaddFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CaddFormU3E__0_0() { return &___U3CaddFormU3E__0_0; }
	inline void set_U3CaddFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CaddFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_allianceId_1() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___allianceId_1)); }
	inline int64_t get_allianceId_1() const { return ___allianceId_1; }
	inline int64_t* get_address_of_allianceId_1() { return &___allianceId_1; }
	inline void set_allianceId_1(int64_t value)
	{
		___allianceId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAddAllyU3Ec__IteratorB_t1622364050, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDALLYU3EC__ITERATORB_T1622364050_H
#ifndef U3CACCEPTINVITEU3EC__ITERATOR7_T1011583579_H
#define U3CACCEPTINVITEU3EC__ITERATOR7_T1011583579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<AcceptInvite>c__Iterator7
struct  U3CAcceptInviteU3Ec__Iterator7_t1011583579  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<AcceptInvite>c__Iterator7::<requestForm>__0
	WWWForm_t4064702195 * ___U3CrequestFormU3E__0_0;
	// System.Int64 AllianceManager/<AcceptInvite>c__Iterator7::inviteId
	int64_t ___inviteId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<AcceptInvite>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<AcceptInvite>c__Iterator7::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<AcceptInvite>c__Iterator7::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<AcceptInvite>c__Iterator7::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<AcceptInvite>c__Iterator7::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_inviteId_1() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___inviteId_1)); }
	inline int64_t get_inviteId_1() const { return ___inviteId_1; }
	inline int64_t* get_address_of_inviteId_1() { return &___inviteId_1; }
	inline void set_inviteId_1(int64_t value)
	{
		___inviteId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAcceptInviteU3Ec__Iterator7_t1011583579, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACCEPTINVITEU3EC__ITERATOR7_T1011583579_H
#ifndef U3CCANCELINVITEU3EC__ITERATOR8_T279811585_H
#define U3CCANCELINVITEU3EC__ITERATOR8_T279811585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<CancelInvite>c__Iterator8
struct  U3CCancelInviteU3Ec__Iterator8_t279811585  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<CancelInvite>c__Iterator8::<requestForm>__0
	WWWForm_t4064702195 * ___U3CrequestFormU3E__0_0;
	// System.Int64 AllianceManager/<CancelInvite>c__Iterator8::inviteId
	int64_t ___inviteId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<CancelInvite>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<CancelInvite>c__Iterator8::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<CancelInvite>c__Iterator8::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<CancelInvite>c__Iterator8::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<CancelInvite>c__Iterator8::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_inviteId_1() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___inviteId_1)); }
	inline int64_t get_inviteId_1() const { return ___inviteId_1; }
	inline int64_t* get_address_of_inviteId_1() { return &___inviteId_1; }
	inline void set_inviteId_1(int64_t value)
	{
		___inviteId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelInviteU3Ec__Iterator8_t279811585, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELINVITEU3EC__ITERATOR8_T279811585_H
#ifndef U3CGETMESSSAGEDETAILSU3EC__ITERATOR0_T1899079435_H
#define U3CGETMESSSAGEDETAILSU3EC__ITERATOR0_T1899079435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageContentManager/<GetMesssageDetails>c__Iterator0
struct  U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest MessageContentManager/<GetMesssageDetails>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// MessageContentManager MessageContentManager/<GetMesssageDetails>c__Iterator0::$this
	MessageContentManager_t985817454 * ___U24this_1;
	// System.Object MessageContentManager/<GetMesssageDetails>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MessageContentManager/<GetMesssageDetails>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MessageContentManager/<GetMesssageDetails>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435, ___U24this_1)); }
	inline MessageContentManager_t985817454 * get_U24this_1() const { return ___U24this_1; }
	inline MessageContentManager_t985817454 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MessageContentManager_t985817454 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETMESSSAGEDETAILSU3EC__ITERATOR0_T1899079435_H
#ifndef U3CGETLOTSTOSELLU3EC__ITERATOR1_T1027051147_H
#define U3CGETLOTSTOSELLU3EC__ITERATOR1_T1027051147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<GetLotsToSell>c__Iterator1
struct  U3CGetLotsToSellU3Ec__Iterator1_t1027051147  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest MarketManager/<GetLotsToSell>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// MarketManager MarketManager/<GetLotsToSell>c__Iterator1::$this
	MarketManager_t880414981 * ___U24this_1;
	// System.Object MarketManager/<GetLotsToSell>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MarketManager/<GetLotsToSell>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 MarketManager/<GetLotsToSell>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLotsToSellU3Ec__Iterator1_t1027051147, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetLotsToSellU3Ec__Iterator1_t1027051147, ___U24this_1)); }
	inline MarketManager_t880414981 * get_U24this_1() const { return ___U24this_1; }
	inline MarketManager_t880414981 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MarketManager_t880414981 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetLotsToSellU3Ec__Iterator1_t1027051147, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetLotsToSellU3Ec__Iterator1_t1027051147, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetLotsToSellU3Ec__Iterator1_t1027051147, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLOTSTOSELLU3EC__ITERATOR1_T1027051147_H
#ifndef U3CGETLOTSTOBUYU3EC__ITERATOR0_T2177983711_H
#define U3CGETLOTSTOBUYU3EC__ITERATOR0_T2177983711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<GetLotsToBuy>c__Iterator0
struct  U3CGetLotsToBuyU3Ec__Iterator0_t2177983711  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest MarketManager/<GetLotsToBuy>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// MarketManager MarketManager/<GetLotsToBuy>c__Iterator0::$this
	MarketManager_t880414981 * ___U24this_1;
	// System.Object MarketManager/<GetLotsToBuy>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MarketManager/<GetLotsToBuy>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MarketManager/<GetLotsToBuy>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__Iterator0_t2177983711, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__Iterator0_t2177983711, ___U24this_1)); }
	inline MarketManager_t880414981 * get_U24this_1() const { return ___U24this_1; }
	inline MarketManager_t880414981 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MarketManager_t880414981 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__Iterator0_t2177983711, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__Iterator0_t2177983711, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetLotsToBuyU3Ec__Iterator0_t2177983711, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLOTSTOBUYU3EC__ITERATOR0_T2177983711_H
#ifndef U3CREMOVEALLYU3EC__ITERATORC_T4044411523_H
#define U3CREMOVEALLYU3EC__ITERATORC_T4044411523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<RemoveAlly>c__IteratorC
struct  U3CRemoveAllyU3Ec__IteratorC_t4044411523  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<RemoveAlly>c__IteratorC::<removeForm>__0
	WWWForm_t4064702195 * ___U3CremoveFormU3E__0_0;
	// System.Int64 AllianceManager/<RemoveAlly>c__IteratorC::allianceId
	int64_t ___allianceId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<RemoveAlly>c__IteratorC::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<RemoveAlly>c__IteratorC::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<RemoveAlly>c__IteratorC::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<RemoveAlly>c__IteratorC::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<RemoveAlly>c__IteratorC::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CremoveFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U3CremoveFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CremoveFormU3E__0_0() const { return ___U3CremoveFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CremoveFormU3E__0_0() { return &___U3CremoveFormU3E__0_0; }
	inline void set_U3CremoveFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CremoveFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CremoveFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_allianceId_1() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___allianceId_1)); }
	inline int64_t get_allianceId_1() const { return ___allianceId_1; }
	inline int64_t* get_address_of_allianceId_1() { return &___allianceId_1; }
	inline void set_allianceId_1(int64_t value)
	{
		___allianceId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRemoveAllyU3Ec__IteratorC_t4044411523, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEALLYU3EC__ITERATORC_T4044411523_H
#ifndef U3CBUYLOTSU3EC__ITERATOR4_T1124615586_H
#define U3CBUYLOTSU3EC__ITERATOR4_T1124615586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<BuyLots>c__Iterator4
struct  U3CBuyLotsU3Ec__Iterator4_t1124615586  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MarketManager/<BuyLots>c__Iterator4::<buyForm>__0
	WWWForm_t4064702195 * ___U3CbuyFormU3E__0_0;
	// System.String MarketManager/<BuyLots>c__Iterator4::lotsDict
	String_t* ___lotsDict_1;
	// UnityEngine.Networking.UnityWebRequest MarketManager/<BuyLots>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// MarketManager MarketManager/<BuyLots>c__Iterator4::$this
	MarketManager_t880414981 * ___U24this_3;
	// System.Object MarketManager/<BuyLots>c__Iterator4::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MarketManager/<BuyLots>c__Iterator4::$disposing
	bool ___U24disposing_5;
	// System.Int32 MarketManager/<BuyLots>c__Iterator4::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CbuyFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U3CbuyFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CbuyFormU3E__0_0() const { return ___U3CbuyFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CbuyFormU3E__0_0() { return &___U3CbuyFormU3E__0_0; }
	inline void set_U3CbuyFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CbuyFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuyFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_lotsDict_1() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___lotsDict_1)); }
	inline String_t* get_lotsDict_1() const { return ___lotsDict_1; }
	inline String_t** get_address_of_lotsDict_1() { return &___lotsDict_1; }
	inline void set_lotsDict_1(String_t* value)
	{
		___lotsDict_1 = value;
		Il2CppCodeGenWriteBarrier((&___lotsDict_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U24this_3)); }
	inline MarketManager_t880414981 * get_U24this_3() const { return ___U24this_3; }
	inline MarketManager_t880414981 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MarketManager_t880414981 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator4_t1124615586, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUYLOTSU3EC__ITERATOR4_T1124615586_H
#ifndef U3CREMOVELOTSU3EC__ITERATOR3_T2683563112_H
#define U3CREMOVELOTSU3EC__ITERATOR3_T2683563112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<RemoveLots>c__Iterator3
struct  U3CRemoveLotsU3Ec__Iterator3_t2683563112  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MarketManager/<RemoveLots>c__Iterator3::<removeForm>__0
	WWWForm_t4064702195 * ___U3CremoveFormU3E__0_0;
	// System.Int64 MarketManager/<RemoveLots>c__Iterator3::lotId
	int64_t ___lotId_1;
	// UnityEngine.Networking.UnityWebRequest MarketManager/<RemoveLots>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// MarketManager MarketManager/<RemoveLots>c__Iterator3::$this
	MarketManager_t880414981 * ___U24this_3;
	// System.Object MarketManager/<RemoveLots>c__Iterator3::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MarketManager/<RemoveLots>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 MarketManager/<RemoveLots>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CremoveFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U3CremoveFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CremoveFormU3E__0_0() const { return ___U3CremoveFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CremoveFormU3E__0_0() { return &___U3CremoveFormU3E__0_0; }
	inline void set_U3CremoveFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CremoveFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CremoveFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_lotId_1() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___lotId_1)); }
	inline int64_t get_lotId_1() const { return ___lotId_1; }
	inline int64_t* get_address_of_lotId_1() { return &___lotId_1; }
	inline void set_lotId_1(int64_t value)
	{
		___lotId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U24this_3)); }
	inline MarketManager_t880414981 * get_U24this_3() const { return ___U24this_3; }
	inline MarketManager_t880414981 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MarketManager_t880414981 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRemoveLotsU3Ec__Iterator3_t2683563112, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVELOTSU3EC__ITERATOR3_T2683563112_H
#ifndef U3CADDLOTU3EC__ITERATOR2_T3555095447_H
#define U3CADDLOTU3EC__ITERATOR2_T3555095447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<AddLot>c__Iterator2
struct  U3CAddLotU3Ec__Iterator2_t3555095447  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MarketManager/<AddLot>c__Iterator2::<addForm>__0
	WWWForm_t4064702195 * ___U3CaddFormU3E__0_0;
	// System.Int64 MarketManager/<AddLot>c__Iterator2::resourceCount
	int64_t ___resourceCount_1;
	// System.Int64 MarketManager/<AddLot>c__Iterator2::unitPrice
	int64_t ___unitPrice_2;
	// UnityEngine.Networking.UnityWebRequest MarketManager/<AddLot>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// MarketManager MarketManager/<AddLot>c__Iterator2::$this
	MarketManager_t880414981 * ___U24this_4;
	// System.Object MarketManager/<AddLot>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MarketManager/<AddLot>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 MarketManager/<AddLot>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CaddFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U3CaddFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CaddFormU3E__0_0() const { return ___U3CaddFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CaddFormU3E__0_0() { return &___U3CaddFormU3E__0_0; }
	inline void set_U3CaddFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CaddFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_resourceCount_1() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___resourceCount_1)); }
	inline int64_t get_resourceCount_1() const { return ___resourceCount_1; }
	inline int64_t* get_address_of_resourceCount_1() { return &___resourceCount_1; }
	inline void set_resourceCount_1(int64_t value)
	{
		___resourceCount_1 = value;
	}

	inline static int32_t get_offset_of_unitPrice_2() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___unitPrice_2)); }
	inline int64_t get_unitPrice_2() const { return ___unitPrice_2; }
	inline int64_t* get_address_of_unitPrice_2() { return &___unitPrice_2; }
	inline void set_unitPrice_2(int64_t value)
	{
		___unitPrice_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U24this_4)); }
	inline MarketManager_t880414981 * get_U24this_4() const { return ___U24this_4; }
	inline MarketManager_t880414981 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MarketManager_t880414981 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAddLotU3Ec__Iterator2_t3555095447, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDLOTU3EC__ITERATOR2_T3555095447_H
#ifndef U3CGETBATTLEREPORTU3EC__ITERATOR0_T2580360730_H
#define U3CGETBATTLEREPORTU3EC__ITERATOR0_T2580360730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportContentManager/<GetBattleReport>c__Iterator0
struct  U3CGetBattleReportU3Ec__Iterator0_t2580360730  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BattleReportContentManager/<GetBattleReport>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BattleReportContentManager BattleReportContentManager/<GetBattleReport>c__Iterator0::$this
	BattleReportContentManager_t922857224 * ___U24this_1;
	// System.Object BattleReportContentManager/<GetBattleReport>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BattleReportContentManager/<GetBattleReport>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BattleReportContentManager/<GetBattleReport>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator0_t2580360730, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator0_t2580360730, ___U24this_1)); }
	inline BattleReportContentManager_t922857224 * get_U24this_1() const { return ___U24this_1; }
	inline BattleReportContentManager_t922857224 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BattleReportContentManager_t922857224 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator0_t2580360730, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator0_t2580360730, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator0_t2580360730, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBATTLEREPORTU3EC__ITERATOR0_T2580360730_H
#ifndef U3CGETCITYPRODUCTIONU3EC__ITERATOR0_T3498229313_H
#define U3CGETCITYPRODUCTIONU3EC__ITERATOR0_T3498229313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceProductionContentManager/<GetCityProduction>c__Iterator0
struct  U3CGetCityProductionU3Ec__Iterator0_t3498229313  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ResidenceProductionContentManager/<GetCityProduction>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ResidenceProductionContentManager ResidenceProductionContentManager/<GetCityProduction>c__Iterator0::$this
	ResidenceProductionContentManager_t3650353576 * ___U24this_1;
	// System.Object ResidenceProductionContentManager/<GetCityProduction>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ResidenceProductionContentManager/<GetCityProduction>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator0_t3498229313, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator0_t3498229313, ___U24this_1)); }
	inline ResidenceProductionContentManager_t3650353576 * get_U24this_1() const { return ___U24this_1; }
	inline ResidenceProductionContentManager_t3650353576 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ResidenceProductionContentManager_t3650353576 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator0_t3498229313, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator0_t3498229313, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator0_t3498229313, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCITYPRODUCTIONU3EC__ITERATOR0_T3498229313_H
#ifndef U3CINVITEUSERU3EC__ITERATOR2_T2202159703_H
#define U3CINVITEUSERU3EC__ITERATOR2_T2202159703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<InviteUser>c__Iterator2
struct  U3CInviteUserU3Ec__Iterator2_t2202159703  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<InviteUser>c__Iterator2::<inviteForm>__0
	WWWForm_t4064702195 * ___U3CinviteFormU3E__0_0;
	// System.Int64 AllianceManager/<InviteUser>c__Iterator2::userId
	int64_t ___userId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<InviteUser>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<InviteUser>c__Iterator2::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<InviteUser>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<InviteUser>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<InviteUser>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CinviteFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U3CinviteFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CinviteFormU3E__0_0() const { return ___U3CinviteFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CinviteFormU3E__0_0() { return &___U3CinviteFormU3E__0_0; }
	inline void set_U3CinviteFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CinviteFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinviteFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___userId_1)); }
	inline int64_t get_userId_1() const { return ___userId_1; }
	inline int64_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int64_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CInviteUserU3Ec__Iterator2_t2202159703, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVITEUSERU3EC__ITERATOR2_T2202159703_H
#ifndef U3CREQUESTJOINU3EC__ITERATOR3_T3847028616_H
#define U3CREQUESTJOINU3EC__ITERATOR3_T3847028616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<RequestJoin>c__Iterator3
struct  U3CRequestJoinU3Ec__Iterator3_t3847028616  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<RequestJoin>c__Iterator3::<requestForm>__0
	WWWForm_t4064702195 * ___U3CrequestFormU3E__0_0;
	// System.Int64 AllianceManager/<RequestJoin>c__Iterator3::allianceId
	int64_t ___allianceId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<RequestJoin>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<RequestJoin>c__Iterator3::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<RequestJoin>c__Iterator3::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<RequestJoin>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<RequestJoin>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CrequestFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U3CrequestFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrequestFormU3E__0_0() const { return ___U3CrequestFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrequestFormU3E__0_0() { return &___U3CrequestFormU3E__0_0; }
	inline void set_U3CrequestFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrequestFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_allianceId_1() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___allianceId_1)); }
	inline int64_t get_allianceId_1() const { return ___allianceId_1; }
	inline int64_t* get_address_of_allianceId_1() { return &___allianceId_1; }
	inline void set_allianceId_1(int64_t value)
	{
		___allianceId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRequestJoinU3Ec__Iterator3_t3847028616, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTJOINU3EC__ITERATOR3_T3847028616_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CEXCLUDEUSERU3EC__ITERATOR1_T1861355320_H
#define U3CEXCLUDEUSERU3EC__ITERATOR1_T1861355320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<ExcludeUser>c__Iterator1
struct  U3CExcludeUserU3Ec__Iterator1_t1861355320  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<ExcludeUser>c__Iterator1::<excludeForm>__0
	WWWForm_t4064702195 * ___U3CexcludeFormU3E__0_0;
	// System.Int64 AllianceManager/<ExcludeUser>c__Iterator1::userId
	int64_t ___userId_1;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<ExcludeUser>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// AllianceManager AllianceManager/<ExcludeUser>c__Iterator1::$this
	AllianceManager_t344187419 * ___U24this_3;
	// System.Object AllianceManager/<ExcludeUser>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AllianceManager/<ExcludeUser>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 AllianceManager/<ExcludeUser>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CexcludeFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U3CexcludeFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CexcludeFormU3E__0_0() const { return ___U3CexcludeFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CexcludeFormU3E__0_0() { return &___U3CexcludeFormU3E__0_0; }
	inline void set_U3CexcludeFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CexcludeFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexcludeFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___userId_1)); }
	inline int64_t get_userId_1() const { return ___userId_1; }
	inline int64_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int64_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U24this_3)); }
	inline AllianceManager_t344187419 * get_U24this_3() const { return ___U24this_3; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AllianceManager_t344187419 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CExcludeUserU3Ec__Iterator1_t1861355320, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXCLUDEUSERU3EC__ITERATOR1_T1861355320_H
#ifndef U3CADDRESOURCEBONUSU3EC__ITERATOR0_T3918272656_H
#define U3CADDRESOURCEBONUSU3EC__ITERATOR0_T3918272656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADManager/<AddResourceBonus>c__Iterator0
struct  U3CAddResourceBonusU3Ec__Iterator0_t3918272656  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ADManager/<AddResourceBonus>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String ADManager/<AddResourceBonus>c__Iterator0::resource
	String_t* ___resource_1;
	// UnityEngine.Networking.UnityWebRequest ADManager/<AddResourceBonus>c__Iterator0::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_2;
	// System.Object ADManager/<AddResourceBonus>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ADManager/<AddResourceBonus>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ADManager/<AddResourceBonus>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_resource_1() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___resource_1)); }
	inline String_t* get_resource_1() const { return ___resource_1; }
	inline String_t** get_address_of_resource_1() { return &___resource_1; }
	inline void set_resource_1(String_t* value)
	{
		___resource_1 = value;
		Il2CppCodeGenWriteBarrier((&___resource_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___U3CwwwU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CAddResourceBonusU3Ec__Iterator0_t3918272656, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDRESOURCEBONUSU3EC__ITERATOR0_T3918272656_H
#ifndef BATTLEMANAGER_T4022130644_H
#define BATTLEMANAGER_T4022130644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager
struct  BattleManager_t4022130644  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,ArmyModel> BattleManager::cityArmies
	Dictionary_2_t3131312889 * ___cityArmies_0;
	// SimpleEvent BattleManager::onGetCityArmies
	SimpleEvent_t129249603 * ___onGetCityArmies_1;
	// SimpleEvent BattleManager::onMarchLaunched
	SimpleEvent_t129249603 * ___onMarchLaunched_2;
	// SimpleEvent BattleManager::onMarchCanceled
	SimpleEvent_t129249603 * ___onMarchCanceled_3;
	// SimpleEvent BattleManager::onArmyReturnInitiated
	SimpleEvent_t129249603 * ___onArmyReturnInitiated_4;

public:
	inline static int32_t get_offset_of_cityArmies_0() { return static_cast<int32_t>(offsetof(BattleManager_t4022130644, ___cityArmies_0)); }
	inline Dictionary_2_t3131312889 * get_cityArmies_0() const { return ___cityArmies_0; }
	inline Dictionary_2_t3131312889 ** get_address_of_cityArmies_0() { return &___cityArmies_0; }
	inline void set_cityArmies_0(Dictionary_2_t3131312889 * value)
	{
		___cityArmies_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityArmies_0), value);
	}

	inline static int32_t get_offset_of_onGetCityArmies_1() { return static_cast<int32_t>(offsetof(BattleManager_t4022130644, ___onGetCityArmies_1)); }
	inline SimpleEvent_t129249603 * get_onGetCityArmies_1() const { return ___onGetCityArmies_1; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetCityArmies_1() { return &___onGetCityArmies_1; }
	inline void set_onGetCityArmies_1(SimpleEvent_t129249603 * value)
	{
		___onGetCityArmies_1 = value;
		Il2CppCodeGenWriteBarrier((&___onGetCityArmies_1), value);
	}

	inline static int32_t get_offset_of_onMarchLaunched_2() { return static_cast<int32_t>(offsetof(BattleManager_t4022130644, ___onMarchLaunched_2)); }
	inline SimpleEvent_t129249603 * get_onMarchLaunched_2() const { return ___onMarchLaunched_2; }
	inline SimpleEvent_t129249603 ** get_address_of_onMarchLaunched_2() { return &___onMarchLaunched_2; }
	inline void set_onMarchLaunched_2(SimpleEvent_t129249603 * value)
	{
		___onMarchLaunched_2 = value;
		Il2CppCodeGenWriteBarrier((&___onMarchLaunched_2), value);
	}

	inline static int32_t get_offset_of_onMarchCanceled_3() { return static_cast<int32_t>(offsetof(BattleManager_t4022130644, ___onMarchCanceled_3)); }
	inline SimpleEvent_t129249603 * get_onMarchCanceled_3() const { return ___onMarchCanceled_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onMarchCanceled_3() { return &___onMarchCanceled_3; }
	inline void set_onMarchCanceled_3(SimpleEvent_t129249603 * value)
	{
		___onMarchCanceled_3 = value;
		Il2CppCodeGenWriteBarrier((&___onMarchCanceled_3), value);
	}

	inline static int32_t get_offset_of_onArmyReturnInitiated_4() { return static_cast<int32_t>(offsetof(BattleManager_t4022130644, ___onArmyReturnInitiated_4)); }
	inline SimpleEvent_t129249603 * get_onArmyReturnInitiated_4() const { return ___onArmyReturnInitiated_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onArmyReturnInitiated_4() { return &___onArmyReturnInitiated_4; }
	inline void set_onArmyReturnInitiated_4(SimpleEvent_t129249603 * value)
	{
		___onArmyReturnInitiated_4 = value;
		Il2CppCodeGenWriteBarrier((&___onArmyReturnInitiated_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEMANAGER_T4022130644_H
#ifndef ALLIANCEMANAGER_T344187419_H
#define ALLIANCEMANAGER_T344187419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager
struct  AllianceManager_t344187419  : public RuntimeObject
{
public:
	// AllianceModel AllianceManager::userAlliance
	AllianceModel_t2995969982 * ___userAlliance_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,UserModel> AllianceManager::allianceMembers
	Dictionary_2_t2416571389 * ___allianceMembers_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,AllianceModel> AllianceManager::allAlliances
	Dictionary_2_t4058609766 * ___allAlliances_2;
	// SimpleEvent AllianceManager::onGetAllianceMembers
	SimpleEvent_t129249603 * ___onGetAllianceMembers_3;
	// SimpleEvent AllianceManager::onGetAlliances
	SimpleEvent_t129249603 * ___onGetAlliances_4;
	// SimpleEvent AllianceManager::onExcludeUser
	SimpleEvent_t129249603 * ___onExcludeUser_5;
	// SimpleEvent AllianceManager::onInviteUser
	SimpleEvent_t129249603 * ___onInviteUser_6;
	// SimpleEvent AllianceManager::onRequestJoin
	SimpleEvent_t129249603 * ___onRequestJoin_7;
	// SimpleEvent AllianceManager::onQuitAlliance
	SimpleEvent_t129249603 * ___onQuitAlliance_8;
	// SimpleEvent AllianceManager::onUpdateGuideline
	SimpleEvent_t129249603 * ___onUpdateGuideline_9;
	// SimpleEvent AllianceManager::onAcceptInvite
	SimpleEvent_t129249603 * ___onAcceptInvite_10;
	// SimpleEvent AllianceManager::onCancelInvite
	SimpleEvent_t129249603 * ___onCancelInvite_11;
	// SimpleEvent AllianceManager::onAddAlly
	SimpleEvent_t129249603 * ___onAddAlly_12;
	// SimpleEvent AllianceManager::onAddEnemy
	SimpleEvent_t129249603 * ___onAddEnemy_13;
	// SimpleEvent AllianceManager::onRemoveAlly
	SimpleEvent_t129249603 * ___onRemoveAlly_14;
	// SimpleEvent AllianceManager::onRemoveEnemy
	SimpleEvent_t129249603 * ___onRemoveEnemy_15;
	// SimpleEvent AllianceManager::onCreateNewAlliance
	SimpleEvent_t129249603 * ___onCreateNewAlliance_16;
	// System.String AllianceManager::dateFormat
	String_t* ___dateFormat_17;

public:
	inline static int32_t get_offset_of_userAlliance_0() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___userAlliance_0)); }
	inline AllianceModel_t2995969982 * get_userAlliance_0() const { return ___userAlliance_0; }
	inline AllianceModel_t2995969982 ** get_address_of_userAlliance_0() { return &___userAlliance_0; }
	inline void set_userAlliance_0(AllianceModel_t2995969982 * value)
	{
		___userAlliance_0 = value;
		Il2CppCodeGenWriteBarrier((&___userAlliance_0), value);
	}

	inline static int32_t get_offset_of_allianceMembers_1() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___allianceMembers_1)); }
	inline Dictionary_2_t2416571389 * get_allianceMembers_1() const { return ___allianceMembers_1; }
	inline Dictionary_2_t2416571389 ** get_address_of_allianceMembers_1() { return &___allianceMembers_1; }
	inline void set_allianceMembers_1(Dictionary_2_t2416571389 * value)
	{
		___allianceMembers_1 = value;
		Il2CppCodeGenWriteBarrier((&___allianceMembers_1), value);
	}

	inline static int32_t get_offset_of_allAlliances_2() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___allAlliances_2)); }
	inline Dictionary_2_t4058609766 * get_allAlliances_2() const { return ___allAlliances_2; }
	inline Dictionary_2_t4058609766 ** get_address_of_allAlliances_2() { return &___allAlliances_2; }
	inline void set_allAlliances_2(Dictionary_2_t4058609766 * value)
	{
		___allAlliances_2 = value;
		Il2CppCodeGenWriteBarrier((&___allAlliances_2), value);
	}

	inline static int32_t get_offset_of_onGetAllianceMembers_3() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onGetAllianceMembers_3)); }
	inline SimpleEvent_t129249603 * get_onGetAllianceMembers_3() const { return ___onGetAllianceMembers_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAllianceMembers_3() { return &___onGetAllianceMembers_3; }
	inline void set_onGetAllianceMembers_3(SimpleEvent_t129249603 * value)
	{
		___onGetAllianceMembers_3 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAllianceMembers_3), value);
	}

	inline static int32_t get_offset_of_onGetAlliances_4() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onGetAlliances_4)); }
	inline SimpleEvent_t129249603 * get_onGetAlliances_4() const { return ___onGetAlliances_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAlliances_4() { return &___onGetAlliances_4; }
	inline void set_onGetAlliances_4(SimpleEvent_t129249603 * value)
	{
		___onGetAlliances_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAlliances_4), value);
	}

	inline static int32_t get_offset_of_onExcludeUser_5() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onExcludeUser_5)); }
	inline SimpleEvent_t129249603 * get_onExcludeUser_5() const { return ___onExcludeUser_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onExcludeUser_5() { return &___onExcludeUser_5; }
	inline void set_onExcludeUser_5(SimpleEvent_t129249603 * value)
	{
		___onExcludeUser_5 = value;
		Il2CppCodeGenWriteBarrier((&___onExcludeUser_5), value);
	}

	inline static int32_t get_offset_of_onInviteUser_6() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onInviteUser_6)); }
	inline SimpleEvent_t129249603 * get_onInviteUser_6() const { return ___onInviteUser_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onInviteUser_6() { return &___onInviteUser_6; }
	inline void set_onInviteUser_6(SimpleEvent_t129249603 * value)
	{
		___onInviteUser_6 = value;
		Il2CppCodeGenWriteBarrier((&___onInviteUser_6), value);
	}

	inline static int32_t get_offset_of_onRequestJoin_7() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onRequestJoin_7)); }
	inline SimpleEvent_t129249603 * get_onRequestJoin_7() const { return ___onRequestJoin_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onRequestJoin_7() { return &___onRequestJoin_7; }
	inline void set_onRequestJoin_7(SimpleEvent_t129249603 * value)
	{
		___onRequestJoin_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRequestJoin_7), value);
	}

	inline static int32_t get_offset_of_onQuitAlliance_8() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onQuitAlliance_8)); }
	inline SimpleEvent_t129249603 * get_onQuitAlliance_8() const { return ___onQuitAlliance_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onQuitAlliance_8() { return &___onQuitAlliance_8; }
	inline void set_onQuitAlliance_8(SimpleEvent_t129249603 * value)
	{
		___onQuitAlliance_8 = value;
		Il2CppCodeGenWriteBarrier((&___onQuitAlliance_8), value);
	}

	inline static int32_t get_offset_of_onUpdateGuideline_9() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onUpdateGuideline_9)); }
	inline SimpleEvent_t129249603 * get_onUpdateGuideline_9() const { return ___onUpdateGuideline_9; }
	inline SimpleEvent_t129249603 ** get_address_of_onUpdateGuideline_9() { return &___onUpdateGuideline_9; }
	inline void set_onUpdateGuideline_9(SimpleEvent_t129249603 * value)
	{
		___onUpdateGuideline_9 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateGuideline_9), value);
	}

	inline static int32_t get_offset_of_onAcceptInvite_10() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onAcceptInvite_10)); }
	inline SimpleEvent_t129249603 * get_onAcceptInvite_10() const { return ___onAcceptInvite_10; }
	inline SimpleEvent_t129249603 ** get_address_of_onAcceptInvite_10() { return &___onAcceptInvite_10; }
	inline void set_onAcceptInvite_10(SimpleEvent_t129249603 * value)
	{
		___onAcceptInvite_10 = value;
		Il2CppCodeGenWriteBarrier((&___onAcceptInvite_10), value);
	}

	inline static int32_t get_offset_of_onCancelInvite_11() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onCancelInvite_11)); }
	inline SimpleEvent_t129249603 * get_onCancelInvite_11() const { return ___onCancelInvite_11; }
	inline SimpleEvent_t129249603 ** get_address_of_onCancelInvite_11() { return &___onCancelInvite_11; }
	inline void set_onCancelInvite_11(SimpleEvent_t129249603 * value)
	{
		___onCancelInvite_11 = value;
		Il2CppCodeGenWriteBarrier((&___onCancelInvite_11), value);
	}

	inline static int32_t get_offset_of_onAddAlly_12() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onAddAlly_12)); }
	inline SimpleEvent_t129249603 * get_onAddAlly_12() const { return ___onAddAlly_12; }
	inline SimpleEvent_t129249603 ** get_address_of_onAddAlly_12() { return &___onAddAlly_12; }
	inline void set_onAddAlly_12(SimpleEvent_t129249603 * value)
	{
		___onAddAlly_12 = value;
		Il2CppCodeGenWriteBarrier((&___onAddAlly_12), value);
	}

	inline static int32_t get_offset_of_onAddEnemy_13() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onAddEnemy_13)); }
	inline SimpleEvent_t129249603 * get_onAddEnemy_13() const { return ___onAddEnemy_13; }
	inline SimpleEvent_t129249603 ** get_address_of_onAddEnemy_13() { return &___onAddEnemy_13; }
	inline void set_onAddEnemy_13(SimpleEvent_t129249603 * value)
	{
		___onAddEnemy_13 = value;
		Il2CppCodeGenWriteBarrier((&___onAddEnemy_13), value);
	}

	inline static int32_t get_offset_of_onRemoveAlly_14() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onRemoveAlly_14)); }
	inline SimpleEvent_t129249603 * get_onRemoveAlly_14() const { return ___onRemoveAlly_14; }
	inline SimpleEvent_t129249603 ** get_address_of_onRemoveAlly_14() { return &___onRemoveAlly_14; }
	inline void set_onRemoveAlly_14(SimpleEvent_t129249603 * value)
	{
		___onRemoveAlly_14 = value;
		Il2CppCodeGenWriteBarrier((&___onRemoveAlly_14), value);
	}

	inline static int32_t get_offset_of_onRemoveEnemy_15() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onRemoveEnemy_15)); }
	inline SimpleEvent_t129249603 * get_onRemoveEnemy_15() const { return ___onRemoveEnemy_15; }
	inline SimpleEvent_t129249603 ** get_address_of_onRemoveEnemy_15() { return &___onRemoveEnemy_15; }
	inline void set_onRemoveEnemy_15(SimpleEvent_t129249603 * value)
	{
		___onRemoveEnemy_15 = value;
		Il2CppCodeGenWriteBarrier((&___onRemoveEnemy_15), value);
	}

	inline static int32_t get_offset_of_onCreateNewAlliance_16() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___onCreateNewAlliance_16)); }
	inline SimpleEvent_t129249603 * get_onCreateNewAlliance_16() const { return ___onCreateNewAlliance_16; }
	inline SimpleEvent_t129249603 ** get_address_of_onCreateNewAlliance_16() { return &___onCreateNewAlliance_16; }
	inline void set_onCreateNewAlliance_16(SimpleEvent_t129249603 * value)
	{
		___onCreateNewAlliance_16 = value;
		Il2CppCodeGenWriteBarrier((&___onCreateNewAlliance_16), value);
	}

	inline static int32_t get_offset_of_dateFormat_17() { return static_cast<int32_t>(offsetof(AllianceManager_t344187419, ___dateFormat_17)); }
	inline String_t* get_dateFormat_17() const { return ___dateFormat_17; }
	inline String_t** get_address_of_dateFormat_17() { return &___dateFormat_17; }
	inline void set_dateFormat_17(String_t* value)
	{
		___dateFormat_17 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEMANAGER_T344187419_H
#ifndef U3CGETBARBARIANSARMYU3EC__ITERATOR0_T2434597637_H
#define U3CGETBARBARIANSARMYU3EC__ITERATOR0_T2434597637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0
struct  U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BarbarianRecruitManager BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0::$this
	BarbarianRecruitManager_t3050290676 * ___U24this_1;
	// System.Object BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637, ___U24this_1)); }
	inline BarbarianRecruitManager_t3050290676 * get_U24this_1() const { return ___U24this_1; }
	inline BarbarianRecruitManager_t3050290676 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BarbarianRecruitManager_t3050290676 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBARBARIANSARMYU3EC__ITERATOR0_T2434597637_H
#ifndef U3CALLIANCEMEMEBERSU3EC__ITERATOR0_T3649014814_H
#define U3CALLIANCEMEMEBERSU3EC__ITERATOR0_T3649014814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<AllianceMemebers>c__Iterator0
struct  U3CAllianceMemebersU3Ec__Iterator0_t3649014814  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<AllianceMemebers>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// AllianceManager AllianceManager/<AllianceMemebers>c__Iterator0::$this
	AllianceManager_t344187419 * ___U24this_1;
	// System.Object AllianceManager/<AllianceMemebers>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AllianceManager/<AllianceMemebers>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 AllianceManager/<AllianceMemebers>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAllianceMemebersU3Ec__Iterator0_t3649014814, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAllianceMemebersU3Ec__Iterator0_t3649014814, ___U24this_1)); }
	inline AllianceManager_t344187419 * get_U24this_1() const { return ___U24this_1; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AllianceManager_t344187419 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAllianceMemebersU3Ec__Iterator0_t3649014814, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAllianceMemebersU3Ec__Iterator0_t3649014814, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAllianceMemebersU3Ec__Iterator0_t3649014814, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CALLIANCEMEMEBERSU3EC__ITERATOR0_T3649014814_H
#ifndef U3CALLALLIANCESU3EC__ITERATOR4_T366036290_H
#define U3CALLALLIANCESU3EC__ITERATOR4_T366036290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<AllAlliances>c__Iterator4
struct  U3CAllAlliancesU3Ec__Iterator4_t366036290  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<AllAlliances>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// AllianceManager AllianceManager/<AllAlliances>c__Iterator4::$this
	AllianceManager_t344187419 * ___U24this_1;
	// System.Object AllianceManager/<AllAlliances>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AllianceManager/<AllAlliances>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 AllianceManager/<AllAlliances>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAllAlliancesU3Ec__Iterator4_t366036290, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAllAlliancesU3Ec__Iterator4_t366036290, ___U24this_1)); }
	inline AllianceManager_t344187419 * get_U24this_1() const { return ___U24this_1; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AllianceManager_t344187419 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAllAlliancesU3Ec__Iterator4_t366036290, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAllAlliancesU3Ec__Iterator4_t366036290, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAllAlliancesU3Ec__Iterator4_t366036290, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CALLALLIANCESU3EC__ITERATOR4_T366036290_H
#ifndef U3CGETINITIALCITYBUILDINGSU3EC__ITERATOR0_T1552903605_H
#define U3CGETINITIALCITYBUILDINGSU3EC__ITERATOR0_T1552903605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<GetInitialCityBuildings>c__Iterator0
struct  U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<GetInitialCityBuildings>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BuildingsManager BuildingsManager/<GetInitialCityBuildings>c__Iterator0::$this
	BuildingsManager_t3721263023 * ___U24this_1;
	// System.Object BuildingsManager/<GetInitialCityBuildings>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BuildingsManager/<GetInitialCityBuildings>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BuildingsManager/<GetInitialCityBuildings>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605, ___U24this_1)); }
	inline BuildingsManager_t3721263023 * get_U24this_1() const { return ___U24this_1; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BuildingsManager_t3721263023 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITYBUILDINGSU3EC__ITERATOR0_T1552903605_H
#ifndef U3CUPDATEBUILDINGSANDFIELDSSTATEU3EC__ITERATOR1_T1930216895_H
#define U3CUPDATEBUILDINGSANDFIELDSSTATEU3EC__ITERATOR1_T1930216895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1
struct  U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BuildingsManager BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1::$this
	BuildingsManager_t3721263023 * ___U24this_1;
	// System.Object BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 BuildingsManager/<UpdateBuildingsAndFieldsState>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895, ___U24this_1)); }
	inline BuildingsManager_t3721263023 * get_U24this_1() const { return ___U24this_1; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BuildingsManager_t3721263023 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUILDINGSANDFIELDSSTATEU3EC__ITERATOR1_T1930216895_H
#ifndef BUILDINGSMANAGER_T3721263023_H
#define BUILDINGSMANAGER_T3721263023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager
struct  BuildingsManager_t3721263023  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityBuildings
	Dictionary_2_t3346051284 * ___cityBuildings_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityFields
	Dictionary_2_t3346051284 * ___cityFields_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel> BuildingsManager::cityValleys
	Dictionary_2_t3480614145 * ___cityValleys_2;
	// SimpleEvent BuildingsManager::onBuildingUpdate
	SimpleEvent_t129249603 * ___onBuildingUpdate_3;
	// SimpleEvent BuildingsManager::onGotValleys
	SimpleEvent_t129249603 * ___onGotValleys_4;
	// SimpleEvent BuildingsManager::onBuildingCancelled
	SimpleEvent_t129249603 * ___onBuildingCancelled_5;

public:
	inline static int32_t get_offset_of_cityBuildings_0() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityBuildings_0)); }
	inline Dictionary_2_t3346051284 * get_cityBuildings_0() const { return ___cityBuildings_0; }
	inline Dictionary_2_t3346051284 ** get_address_of_cityBuildings_0() { return &___cityBuildings_0; }
	inline void set_cityBuildings_0(Dictionary_2_t3346051284 * value)
	{
		___cityBuildings_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityBuildings_0), value);
	}

	inline static int32_t get_offset_of_cityFields_1() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityFields_1)); }
	inline Dictionary_2_t3346051284 * get_cityFields_1() const { return ___cityFields_1; }
	inline Dictionary_2_t3346051284 ** get_address_of_cityFields_1() { return &___cityFields_1; }
	inline void set_cityFields_1(Dictionary_2_t3346051284 * value)
	{
		___cityFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___cityFields_1), value);
	}

	inline static int32_t get_offset_of_cityValleys_2() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityValleys_2)); }
	inline Dictionary_2_t3480614145 * get_cityValleys_2() const { return ___cityValleys_2; }
	inline Dictionary_2_t3480614145 ** get_address_of_cityValleys_2() { return &___cityValleys_2; }
	inline void set_cityValleys_2(Dictionary_2_t3480614145 * value)
	{
		___cityValleys_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityValleys_2), value);
	}

	inline static int32_t get_offset_of_onBuildingUpdate_3() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onBuildingUpdate_3)); }
	inline SimpleEvent_t129249603 * get_onBuildingUpdate_3() const { return ___onBuildingUpdate_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onBuildingUpdate_3() { return &___onBuildingUpdate_3; }
	inline void set_onBuildingUpdate_3(SimpleEvent_t129249603 * value)
	{
		___onBuildingUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBuildingUpdate_3), value);
	}

	inline static int32_t get_offset_of_onGotValleys_4() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onGotValleys_4)); }
	inline SimpleEvent_t129249603 * get_onGotValleys_4() const { return ___onGotValleys_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGotValleys_4() { return &___onGotValleys_4; }
	inline void set_onGotValleys_4(SimpleEvent_t129249603 * value)
	{
		___onGotValleys_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGotValleys_4), value);
	}

	inline static int32_t get_offset_of_onBuildingCancelled_5() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onBuildingCancelled_5)); }
	inline SimpleEvent_t129249603 * get_onBuildingCancelled_5() const { return ___onBuildingCancelled_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onBuildingCancelled_5() { return &___onBuildingCancelled_5; }
	inline void set_onBuildingCancelled_5(SimpleEvent_t129249603 * value)
	{
		___onBuildingCancelled_5 = value;
		Il2CppCodeGenWriteBarrier((&___onBuildingCancelled_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGSMANAGER_T3721263023_H
#ifndef U3CGETATTACKPARAMSU3EC__ITERATOR0_T1449406293_H
#define U3CGETATTACKPARAMSU3EC__ITERATOR0_T1449406293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DispatchManager/<GetAttackParams>c__Iterator0
struct  U3CGetAttackParamsU3Ec__Iterator0_t1449406293  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm DispatchManager/<GetAttackParams>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String DispatchManager/<GetAttackParams>c__Iterator0::attcakType
	String_t* ___attcakType_1;
	// UnitsModel DispatchManager/<GetAttackParams>c__Iterator0::army
	UnitsModel_t3847956398 * ___army_2;
	// ResourcesModel DispatchManager/<GetAttackParams>c__Iterator0::resources
	ResourcesModel_t2533508513 * ___resources_3;
	// System.Int64 DispatchManager/<GetAttackParams>c__Iterator0::destX
	int64_t ___destX_4;
	// System.Int64 DispatchManager/<GetAttackParams>c__Iterator0::destY
	int64_t ___destY_5;
	// System.Int64 DispatchManager/<GetAttackParams>c__Iterator0::knightId
	int64_t ___knightId_6;
	// UnityEngine.Networking.UnityWebRequest DispatchManager/<GetAttackParams>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_7;
	// DispatchManager DispatchManager/<GetAttackParams>c__Iterator0::$this
	DispatchManager_t670234083 * ___U24this_8;
	// System.Object DispatchManager/<GetAttackParams>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean DispatchManager/<GetAttackParams>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 DispatchManager/<GetAttackParams>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_attcakType_1() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___attcakType_1)); }
	inline String_t* get_attcakType_1() const { return ___attcakType_1; }
	inline String_t** get_address_of_attcakType_1() { return &___attcakType_1; }
	inline void set_attcakType_1(String_t* value)
	{
		___attcakType_1 = value;
		Il2CppCodeGenWriteBarrier((&___attcakType_1), value);
	}

	inline static int32_t get_offset_of_army_2() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___army_2)); }
	inline UnitsModel_t3847956398 * get_army_2() const { return ___army_2; }
	inline UnitsModel_t3847956398 ** get_address_of_army_2() { return &___army_2; }
	inline void set_army_2(UnitsModel_t3847956398 * value)
	{
		___army_2 = value;
		Il2CppCodeGenWriteBarrier((&___army_2), value);
	}

	inline static int32_t get_offset_of_resources_3() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___resources_3)); }
	inline ResourcesModel_t2533508513 * get_resources_3() const { return ___resources_3; }
	inline ResourcesModel_t2533508513 ** get_address_of_resources_3() { return &___resources_3; }
	inline void set_resources_3(ResourcesModel_t2533508513 * value)
	{
		___resources_3 = value;
		Il2CppCodeGenWriteBarrier((&___resources_3), value);
	}

	inline static int32_t get_offset_of_destX_4() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___destX_4)); }
	inline int64_t get_destX_4() const { return ___destX_4; }
	inline int64_t* get_address_of_destX_4() { return &___destX_4; }
	inline void set_destX_4(int64_t value)
	{
		___destX_4 = value;
	}

	inline static int32_t get_offset_of_destY_5() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___destY_5)); }
	inline int64_t get_destY_5() const { return ___destY_5; }
	inline int64_t* get_address_of_destY_5() { return &___destY_5; }
	inline void set_destY_5(int64_t value)
	{
		___destY_5 = value;
	}

	inline static int32_t get_offset_of_knightId_6() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___knightId_6)); }
	inline int64_t get_knightId_6() const { return ___knightId_6; }
	inline int64_t* get_address_of_knightId_6() { return &___knightId_6; }
	inline void set_knightId_6(int64_t value)
	{
		___knightId_6 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_7() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U3CrequestU3E__0_7)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_7() const { return ___U3CrequestU3E__0_7; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_7() { return &___U3CrequestU3E__0_7; }
	inline void set_U3CrequestU3E__0_7(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U24this_8)); }
	inline DispatchManager_t670234083 * get_U24this_8() const { return ___U24this_8; }
	inline DispatchManager_t670234083 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(DispatchManager_t670234083 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__Iterator0_t1449406293, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETATTACKPARAMSU3EC__ITERATOR0_T1449406293_H
#ifndef U3CRETURNARMYU3EC__ITERATOR4_T1160843784_H
#define U3CRETURNARMYU3EC__ITERATOR4_T1160843784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager/<ReturnArmy>c__Iterator4
struct  U3CReturnArmyU3Ec__Iterator4_t1160843784  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BattleManager/<ReturnArmy>c__Iterator4::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.Int64 BattleManager/<ReturnArmy>c__Iterator4::armyId
	int64_t ___armyId_1;
	// UnityEngine.Networking.UnityWebRequest BattleManager/<ReturnArmy>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BattleManager BattleManager/<ReturnArmy>c__Iterator4::$this
	BattleManager_t4022130644 * ___U24this_3;
	// System.Object BattleManager/<ReturnArmy>c__Iterator4::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BattleManager/<ReturnArmy>c__Iterator4::$disposing
	bool ___U24disposing_5;
	// System.Int32 BattleManager/<ReturnArmy>c__Iterator4::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_armyId_1() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___armyId_1)); }
	inline int64_t get_armyId_1() const { return ___armyId_1; }
	inline int64_t* get_address_of_armyId_1() { return &___armyId_1; }
	inline void set_armyId_1(int64_t value)
	{
		___armyId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U24this_3)); }
	inline BattleManager_t4022130644 * get_U24this_3() const { return ___U24this_3; }
	inline BattleManager_t4022130644 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BattleManager_t4022130644 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CReturnArmyU3Ec__Iterator4_t1160843784, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRETURNARMYU3EC__ITERATOR4_T1160843784_H
#ifndef U3CGETINITIALCITYFIELDSU3EC__ITERATOR2_T3018373358_H
#define U3CGETINITIALCITYFIELDSU3EC__ITERATOR2_T3018373358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<GetInitialCityFields>c__Iterator2
struct  U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<GetInitialCityFields>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// BuildingsManager BuildingsManager/<GetInitialCityFields>c__Iterator2::$this
	BuildingsManager_t3721263023 * ___U24this_1;
	// System.Object BuildingsManager/<GetInitialCityFields>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BuildingsManager/<GetInitialCityFields>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 BuildingsManager/<GetInitialCityFields>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358, ___U24this_1)); }
	inline BuildingsManager_t3721263023 * get_U24this_1() const { return ___U24this_1; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BuildingsManager_t3721263023 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITYFIELDSU3EC__ITERATOR2_T3018373358_H
#ifndef U3CQUITALLIANCEU3EC__ITERATOR5_T878846607_H
#define U3CQUITALLIANCEU3EC__ITERATOR5_T878846607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager/<QuitAlliance>c__Iterator5
struct  U3CQuitAllianceU3Ec__Iterator5_t878846607  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AllianceManager/<QuitAlliance>c__Iterator5::<quitForm>__0
	WWWForm_t4064702195 * ___U3CquitFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest AllianceManager/<QuitAlliance>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// AllianceManager AllianceManager/<QuitAlliance>c__Iterator5::$this
	AllianceManager_t344187419 * ___U24this_2;
	// System.Object AllianceManager/<QuitAlliance>c__Iterator5::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean AllianceManager/<QuitAlliance>c__Iterator5::$disposing
	bool ___U24disposing_4;
	// System.Int32 AllianceManager/<QuitAlliance>c__Iterator5::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CquitFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U3CquitFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CquitFormU3E__0_0() const { return ___U3CquitFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CquitFormU3E__0_0() { return &___U3CquitFormU3E__0_0; }
	inline void set_U3CquitFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CquitFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquitFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U24this_2)); }
	inline AllianceManager_t344187419 * get_U24this_2() const { return ___U24this_2; }
	inline AllianceManager_t344187419 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AllianceManager_t344187419 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CQuitAllianceU3Ec__Iterator5_t878846607, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CQUITALLIANCEU3EC__ITERATOR5_T878846607_H
#ifndef U3CCHANGECITYU3EC__ITERATOR0_T82477809_H
#define U3CCHANGECITYU3EC__ITERATOR0_T82477809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityChangerContentManager/<ChangeCity>c__Iterator0
struct  U3CChangeCityU3Ec__Iterator0_t82477809  : public RuntimeObject
{
public:
	// LoginManager CityChangerContentManager/<ChangeCity>c__Iterator0::<loginManager>__0
	LoginManager_t1249555276 * ___U3CloginManagerU3E__0_0;
	// System.Int64 CityChangerContentManager/<ChangeCity>c__Iterator0::cityId
	int64_t ___cityId_1;
	// CityChangerContentManager CityChangerContentManager/<ChangeCity>c__Iterator0::$this
	CityChangerContentManager_t1075607021 * ___U24this_2;
	// System.Object CityChangerContentManager/<ChangeCity>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CityChangerContentManager/<ChangeCity>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 CityChangerContentManager/<ChangeCity>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CloginManagerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___U3CloginManagerU3E__0_0)); }
	inline LoginManager_t1249555276 * get_U3CloginManagerU3E__0_0() const { return ___U3CloginManagerU3E__0_0; }
	inline LoginManager_t1249555276 ** get_address_of_U3CloginManagerU3E__0_0() { return &___U3CloginManagerU3E__0_0; }
	inline void set_U3CloginManagerU3E__0_0(LoginManager_t1249555276 * value)
	{
		___U3CloginManagerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloginManagerU3E__0_0), value);
	}

	inline static int32_t get_offset_of_cityId_1() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___cityId_1)); }
	inline int64_t get_cityId_1() const { return ___cityId_1; }
	inline int64_t* get_address_of_cityId_1() { return &___cityId_1; }
	inline void set_cityId_1(int64_t value)
	{
		___cityId_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___U24this_2)); }
	inline CityChangerContentManager_t1075607021 * get_U24this_2() const { return ___U24this_2; }
	inline CityChangerContentManager_t1075607021 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CityChangerContentManager_t1075607021 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CChangeCityU3Ec__Iterator0_t82477809, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGECITYU3EC__ITERATOR0_T82477809_H
#ifndef U3CGETEXOTICINFOU3EC__ITERATOR0_T1672915424_H
#define U3CGETEXOTICINFOU3EC__ITERATOR0_T1672915424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityDeedContentManager/<GetExoticInfo>c__Iterator0
struct  U3CGetExoticInfoU3Ec__Iterator0_t1672915424  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest CityDeedContentManager/<GetExoticInfo>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// CityDeedContentManager CityDeedContentManager/<GetExoticInfo>c__Iterator0::$this
	CityDeedContentManager_t1851112673 * ___U24this_1;
	// System.Object CityDeedContentManager/<GetExoticInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CityDeedContentManager/<GetExoticInfo>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CityDeedContentManager/<GetExoticInfo>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetExoticInfoU3Ec__Iterator0_t1672915424, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetExoticInfoU3Ec__Iterator0_t1672915424, ___U24this_1)); }
	inline CityDeedContentManager_t1851112673 * get_U24this_1() const { return ___U24this_1; }
	inline CityDeedContentManager_t1851112673 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CityDeedContentManager_t1851112673 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetExoticInfoU3Ec__Iterator0_t1672915424, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetExoticInfoU3Ec__Iterator0_t1672915424, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetExoticInfoU3Ec__Iterator0_t1672915424, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETEXOTICINFOU3EC__ITERATOR0_T1672915424_H
#ifndef U3CGETINITIALCITYVALLEYSU3EC__ITERATOR3_T2909717123_H
#define U3CGETINITIALCITYVALLEYSU3EC__ITERATOR3_T2909717123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<GetInitialCityValleys>c__Iterator3
struct  U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<GetInitialCityValleys>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean BuildingsManager/<GetInitialCityValleys>c__Iterator3::flag
	bool ___flag_1;
	// BuildingsManager BuildingsManager/<GetInitialCityValleys>c__Iterator3::$this
	BuildingsManager_t3721263023 * ___U24this_2;
	// System.Object BuildingsManager/<GetInitialCityValleys>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BuildingsManager/<GetInitialCityValleys>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 BuildingsManager/<GetInitialCityValleys>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_flag_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___flag_1)); }
	inline bool get_flag_1() const { return ___flag_1; }
	inline bool* get_address_of_flag_1() { return &___flag_1; }
	inline void set_flag_1(bool value)
	{
		___flag_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___U24this_2)); }
	inline BuildingsManager_t3721263023 * get_U24this_2() const { return ___U24this_2; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BuildingsManager_t3721263023 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITYVALLEYSU3EC__ITERATOR3_T2909717123_H
#ifndef U3CUPDATEBUILDINGU3EC__ITERATOR4_T568524150_H
#define U3CUPDATEBUILDINGU3EC__ITERATOR4_T568524150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<UpdateBuilding>c__Iterator4
struct  U3CUpdateBuildingU3Ec__Iterator4_t568524150  : public RuntimeObject
{
public:
	// System.String BuildingsManager/<UpdateBuilding>c__Iterator4::<locationAdress>__0
	String_t* ___U3ClocationAdressU3E__0_0;
	// System.String BuildingsManager/<UpdateBuilding>c__Iterator4::location
	String_t* ___location_1;
	// UnityEngine.WWWForm BuildingsManager/<UpdateBuilding>c__Iterator4::<buildForm>__0
	WWWForm_t4064702195 * ___U3CbuildFormU3E__0_2;
	// System.Int64 BuildingsManager/<UpdateBuilding>c__Iterator4::city
	int64_t ___city_3;
	// System.Int64 BuildingsManager/<UpdateBuilding>c__Iterator4::pit_id
	int64_t ___pit_id_4;
	// System.String BuildingsManager/<UpdateBuilding>c__Iterator4::command
	String_t* ___command_5;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<UpdateBuilding>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_6;
	// BuildingsManager BuildingsManager/<UpdateBuilding>c__Iterator4::$this
	BuildingsManager_t3721263023 * ___U24this_7;
	// System.Object BuildingsManager/<UpdateBuilding>c__Iterator4::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean BuildingsManager/<UpdateBuilding>c__Iterator4::$disposing
	bool ___U24disposing_9;
	// System.Int32 BuildingsManager/<UpdateBuilding>c__Iterator4::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3ClocationAdressU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U3ClocationAdressU3E__0_0)); }
	inline String_t* get_U3ClocationAdressU3E__0_0() const { return ___U3ClocationAdressU3E__0_0; }
	inline String_t** get_address_of_U3ClocationAdressU3E__0_0() { return &___U3ClocationAdressU3E__0_0; }
	inline void set_U3ClocationAdressU3E__0_0(String_t* value)
	{
		___U3ClocationAdressU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocationAdressU3E__0_0), value);
	}

	inline static int32_t get_offset_of_location_1() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___location_1)); }
	inline String_t* get_location_1() const { return ___location_1; }
	inline String_t** get_address_of_location_1() { return &___location_1; }
	inline void set_location_1(String_t* value)
	{
		___location_1 = value;
		Il2CppCodeGenWriteBarrier((&___location_1), value);
	}

	inline static int32_t get_offset_of_U3CbuildFormU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U3CbuildFormU3E__0_2)); }
	inline WWWForm_t4064702195 * get_U3CbuildFormU3E__0_2() const { return ___U3CbuildFormU3E__0_2; }
	inline WWWForm_t4064702195 ** get_address_of_U3CbuildFormU3E__0_2() { return &___U3CbuildFormU3E__0_2; }
	inline void set_U3CbuildFormU3E__0_2(WWWForm_t4064702195 * value)
	{
		___U3CbuildFormU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuildFormU3E__0_2), value);
	}

	inline static int32_t get_offset_of_city_3() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___city_3)); }
	inline int64_t get_city_3() const { return ___city_3; }
	inline int64_t* get_address_of_city_3() { return &___city_3; }
	inline void set_city_3(int64_t value)
	{
		___city_3 = value;
	}

	inline static int32_t get_offset_of_pit_id_4() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___pit_id_4)); }
	inline int64_t get_pit_id_4() const { return ___pit_id_4; }
	inline int64_t* get_address_of_pit_id_4() { return &___pit_id_4; }
	inline void set_pit_id_4(int64_t value)
	{
		___pit_id_4 = value;
	}

	inline static int32_t get_offset_of_command_5() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___command_5)); }
	inline String_t* get_command_5() const { return ___command_5; }
	inline String_t** get_address_of_command_5() { return &___command_5; }
	inline void set_command_5(String_t* value)
	{
		___command_5 = value;
		Il2CppCodeGenWriteBarrier((&___command_5), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_6() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U3CrequestU3E__0_6)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_6() const { return ___U3CrequestU3E__0_6; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_6() { return &___U3CrequestU3E__0_6; }
	inline void set_U3CrequestU3E__0_6(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U24this_7)); }
	inline BuildingsManager_t3721263023 * get_U24this_7() const { return ___U24this_7; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(BuildingsManager_t3721263023 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CUpdateBuildingU3Ec__Iterator4_t568524150, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUILDINGU3EC__ITERATOR4_T568524150_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef USERINFOCONTENTMANAGER_T1915571260_H
#define USERINFOCONTENTMANAGER_T1915571260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfoContentManager
struct  UserInfoContentManager_t1915571260  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UserInfoContentManager::currentUserPanel
	GameObject_t1113636619 * ___currentUserPanel_2;
	// UnityEngine.GameObject UserInfoContentManager::otherUserPanel
	GameObject_t1113636619 * ___otherUserPanel_3;
	// UnityEngine.GameObject UserInfoContentManager::otherPlayerActions
	GameObject_t1113636619 * ___otherPlayerActions_4;
	// UnityEngine.GameObject UserInfoContentManager::allianceMemberActions
	GameObject_t1113636619 * ___allianceMemberActions_5;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserNick
	InputField_t3762917431 * ___currentUserNick_6;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserEmail
	InputField_t3762917431 * ___currentUserEmail_7;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserMobile
	InputField_t3762917431 * ___currentUserMobile_8;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserFlag
	InputField_t3762917431 * ___currentUserFlag_9;
	// UnityEngine.UI.Image UserInfoContentManager::currentUserImage
	Image_t2670269651 * ___currentUserImage_10;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserNick
	Text_t1901882714 * ___otherUserNick_11;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserExperience
	Text_t1901882714 * ___otherUserExperience_12;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserRank
	Text_t1901882714 * ___otherUserRank_13;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCities
	Text_t1901882714 * ___otherUserCities_14;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCapital
	Text_t1901882714 * ___otherUserCapital_15;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCapitalCoords
	Text_t1901882714 * ___otherUserCapitalCoords_16;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserLastOnline
	Text_t1901882714 * ___otherUserLastOnline_17;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserFlag
	Text_t1901882714 * ___otherUserFlag_18;
	// UnityEngine.UI.Image UserInfoContentManager::otherUserImage
	Image_t2670269651 * ___otherUserImage_19;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserPosition
	Text_t1901882714 * ___otherUserPosition_20;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserAlliance
	Text_t1901882714 * ___otherUserAlliance_21;
	// UnityEngine.UI.Dropdown UserInfoContentManager::allianceMemberPosition
	Dropdown_t2274391225 * ___allianceMemberPosition_22;
	// UnityEngine.GameObject UserInfoContentManager::changeBtn
	GameObject_t1113636619 * ___changeBtn_23;
	// UnityEngine.GameObject UserInfoContentManager::excludeBtn
	GameObject_t1113636619 * ___excludeBtn_24;
	// UserModel UserInfoContentManager::user
	UserModel_t1353931605 * ___user_25;
	// System.Int64 UserInfoContentManager::iconIndex
	int64_t ___iconIndex_26;
	// System.String[] UserInfoContentManager::ranks
	StringU5BU5D_t1281789340* ___ranks_27;

public:
	inline static int32_t get_offset_of_currentUserPanel_2() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserPanel_2)); }
	inline GameObject_t1113636619 * get_currentUserPanel_2() const { return ___currentUserPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_currentUserPanel_2() { return &___currentUserPanel_2; }
	inline void set_currentUserPanel_2(GameObject_t1113636619 * value)
	{
		___currentUserPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserPanel_2), value);
	}

	inline static int32_t get_offset_of_otherUserPanel_3() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserPanel_3)); }
	inline GameObject_t1113636619 * get_otherUserPanel_3() const { return ___otherUserPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_otherUserPanel_3() { return &___otherUserPanel_3; }
	inline void set_otherUserPanel_3(GameObject_t1113636619 * value)
	{
		___otherUserPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserPanel_3), value);
	}

	inline static int32_t get_offset_of_otherPlayerActions_4() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherPlayerActions_4)); }
	inline GameObject_t1113636619 * get_otherPlayerActions_4() const { return ___otherPlayerActions_4; }
	inline GameObject_t1113636619 ** get_address_of_otherPlayerActions_4() { return &___otherPlayerActions_4; }
	inline void set_otherPlayerActions_4(GameObject_t1113636619 * value)
	{
		___otherPlayerActions_4 = value;
		Il2CppCodeGenWriteBarrier((&___otherPlayerActions_4), value);
	}

	inline static int32_t get_offset_of_allianceMemberActions_5() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___allianceMemberActions_5)); }
	inline GameObject_t1113636619 * get_allianceMemberActions_5() const { return ___allianceMemberActions_5; }
	inline GameObject_t1113636619 ** get_address_of_allianceMemberActions_5() { return &___allianceMemberActions_5; }
	inline void set_allianceMemberActions_5(GameObject_t1113636619 * value)
	{
		___allianceMemberActions_5 = value;
		Il2CppCodeGenWriteBarrier((&___allianceMemberActions_5), value);
	}

	inline static int32_t get_offset_of_currentUserNick_6() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserNick_6)); }
	inline InputField_t3762917431 * get_currentUserNick_6() const { return ___currentUserNick_6; }
	inline InputField_t3762917431 ** get_address_of_currentUserNick_6() { return &___currentUserNick_6; }
	inline void set_currentUserNick_6(InputField_t3762917431 * value)
	{
		___currentUserNick_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserNick_6), value);
	}

	inline static int32_t get_offset_of_currentUserEmail_7() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserEmail_7)); }
	inline InputField_t3762917431 * get_currentUserEmail_7() const { return ___currentUserEmail_7; }
	inline InputField_t3762917431 ** get_address_of_currentUserEmail_7() { return &___currentUserEmail_7; }
	inline void set_currentUserEmail_7(InputField_t3762917431 * value)
	{
		___currentUserEmail_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserEmail_7), value);
	}

	inline static int32_t get_offset_of_currentUserMobile_8() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserMobile_8)); }
	inline InputField_t3762917431 * get_currentUserMobile_8() const { return ___currentUserMobile_8; }
	inline InputField_t3762917431 ** get_address_of_currentUserMobile_8() { return &___currentUserMobile_8; }
	inline void set_currentUserMobile_8(InputField_t3762917431 * value)
	{
		___currentUserMobile_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserMobile_8), value);
	}

	inline static int32_t get_offset_of_currentUserFlag_9() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserFlag_9)); }
	inline InputField_t3762917431 * get_currentUserFlag_9() const { return ___currentUserFlag_9; }
	inline InputField_t3762917431 ** get_address_of_currentUserFlag_9() { return &___currentUserFlag_9; }
	inline void set_currentUserFlag_9(InputField_t3762917431 * value)
	{
		___currentUserFlag_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserFlag_9), value);
	}

	inline static int32_t get_offset_of_currentUserImage_10() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___currentUserImage_10)); }
	inline Image_t2670269651 * get_currentUserImage_10() const { return ___currentUserImage_10; }
	inline Image_t2670269651 ** get_address_of_currentUserImage_10() { return &___currentUserImage_10; }
	inline void set_currentUserImage_10(Image_t2670269651 * value)
	{
		___currentUserImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserImage_10), value);
	}

	inline static int32_t get_offset_of_otherUserNick_11() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserNick_11)); }
	inline Text_t1901882714 * get_otherUserNick_11() const { return ___otherUserNick_11; }
	inline Text_t1901882714 ** get_address_of_otherUserNick_11() { return &___otherUserNick_11; }
	inline void set_otherUserNick_11(Text_t1901882714 * value)
	{
		___otherUserNick_11 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserNick_11), value);
	}

	inline static int32_t get_offset_of_otherUserExperience_12() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserExperience_12)); }
	inline Text_t1901882714 * get_otherUserExperience_12() const { return ___otherUserExperience_12; }
	inline Text_t1901882714 ** get_address_of_otherUserExperience_12() { return &___otherUserExperience_12; }
	inline void set_otherUserExperience_12(Text_t1901882714 * value)
	{
		___otherUserExperience_12 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserExperience_12), value);
	}

	inline static int32_t get_offset_of_otherUserRank_13() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserRank_13)); }
	inline Text_t1901882714 * get_otherUserRank_13() const { return ___otherUserRank_13; }
	inline Text_t1901882714 ** get_address_of_otherUserRank_13() { return &___otherUserRank_13; }
	inline void set_otherUserRank_13(Text_t1901882714 * value)
	{
		___otherUserRank_13 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserRank_13), value);
	}

	inline static int32_t get_offset_of_otherUserCities_14() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserCities_14)); }
	inline Text_t1901882714 * get_otherUserCities_14() const { return ___otherUserCities_14; }
	inline Text_t1901882714 ** get_address_of_otherUserCities_14() { return &___otherUserCities_14; }
	inline void set_otherUserCities_14(Text_t1901882714 * value)
	{
		___otherUserCities_14 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserCities_14), value);
	}

	inline static int32_t get_offset_of_otherUserCapital_15() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserCapital_15)); }
	inline Text_t1901882714 * get_otherUserCapital_15() const { return ___otherUserCapital_15; }
	inline Text_t1901882714 ** get_address_of_otherUserCapital_15() { return &___otherUserCapital_15; }
	inline void set_otherUserCapital_15(Text_t1901882714 * value)
	{
		___otherUserCapital_15 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserCapital_15), value);
	}

	inline static int32_t get_offset_of_otherUserCapitalCoords_16() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserCapitalCoords_16)); }
	inline Text_t1901882714 * get_otherUserCapitalCoords_16() const { return ___otherUserCapitalCoords_16; }
	inline Text_t1901882714 ** get_address_of_otherUserCapitalCoords_16() { return &___otherUserCapitalCoords_16; }
	inline void set_otherUserCapitalCoords_16(Text_t1901882714 * value)
	{
		___otherUserCapitalCoords_16 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserCapitalCoords_16), value);
	}

	inline static int32_t get_offset_of_otherUserLastOnline_17() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserLastOnline_17)); }
	inline Text_t1901882714 * get_otherUserLastOnline_17() const { return ___otherUserLastOnline_17; }
	inline Text_t1901882714 ** get_address_of_otherUserLastOnline_17() { return &___otherUserLastOnline_17; }
	inline void set_otherUserLastOnline_17(Text_t1901882714 * value)
	{
		___otherUserLastOnline_17 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserLastOnline_17), value);
	}

	inline static int32_t get_offset_of_otherUserFlag_18() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserFlag_18)); }
	inline Text_t1901882714 * get_otherUserFlag_18() const { return ___otherUserFlag_18; }
	inline Text_t1901882714 ** get_address_of_otherUserFlag_18() { return &___otherUserFlag_18; }
	inline void set_otherUserFlag_18(Text_t1901882714 * value)
	{
		___otherUserFlag_18 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserFlag_18), value);
	}

	inline static int32_t get_offset_of_otherUserImage_19() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserImage_19)); }
	inline Image_t2670269651 * get_otherUserImage_19() const { return ___otherUserImage_19; }
	inline Image_t2670269651 ** get_address_of_otherUserImage_19() { return &___otherUserImage_19; }
	inline void set_otherUserImage_19(Image_t2670269651 * value)
	{
		___otherUserImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserImage_19), value);
	}

	inline static int32_t get_offset_of_otherUserPosition_20() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserPosition_20)); }
	inline Text_t1901882714 * get_otherUserPosition_20() const { return ___otherUserPosition_20; }
	inline Text_t1901882714 ** get_address_of_otherUserPosition_20() { return &___otherUserPosition_20; }
	inline void set_otherUserPosition_20(Text_t1901882714 * value)
	{
		___otherUserPosition_20 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserPosition_20), value);
	}

	inline static int32_t get_offset_of_otherUserAlliance_21() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___otherUserAlliance_21)); }
	inline Text_t1901882714 * get_otherUserAlliance_21() const { return ___otherUserAlliance_21; }
	inline Text_t1901882714 ** get_address_of_otherUserAlliance_21() { return &___otherUserAlliance_21; }
	inline void set_otherUserAlliance_21(Text_t1901882714 * value)
	{
		___otherUserAlliance_21 = value;
		Il2CppCodeGenWriteBarrier((&___otherUserAlliance_21), value);
	}

	inline static int32_t get_offset_of_allianceMemberPosition_22() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___allianceMemberPosition_22)); }
	inline Dropdown_t2274391225 * get_allianceMemberPosition_22() const { return ___allianceMemberPosition_22; }
	inline Dropdown_t2274391225 ** get_address_of_allianceMemberPosition_22() { return &___allianceMemberPosition_22; }
	inline void set_allianceMemberPosition_22(Dropdown_t2274391225 * value)
	{
		___allianceMemberPosition_22 = value;
		Il2CppCodeGenWriteBarrier((&___allianceMemberPosition_22), value);
	}

	inline static int32_t get_offset_of_changeBtn_23() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___changeBtn_23)); }
	inline GameObject_t1113636619 * get_changeBtn_23() const { return ___changeBtn_23; }
	inline GameObject_t1113636619 ** get_address_of_changeBtn_23() { return &___changeBtn_23; }
	inline void set_changeBtn_23(GameObject_t1113636619 * value)
	{
		___changeBtn_23 = value;
		Il2CppCodeGenWriteBarrier((&___changeBtn_23), value);
	}

	inline static int32_t get_offset_of_excludeBtn_24() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___excludeBtn_24)); }
	inline GameObject_t1113636619 * get_excludeBtn_24() const { return ___excludeBtn_24; }
	inline GameObject_t1113636619 ** get_address_of_excludeBtn_24() { return &___excludeBtn_24; }
	inline void set_excludeBtn_24(GameObject_t1113636619 * value)
	{
		___excludeBtn_24 = value;
		Il2CppCodeGenWriteBarrier((&___excludeBtn_24), value);
	}

	inline static int32_t get_offset_of_user_25() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___user_25)); }
	inline UserModel_t1353931605 * get_user_25() const { return ___user_25; }
	inline UserModel_t1353931605 ** get_address_of_user_25() { return &___user_25; }
	inline void set_user_25(UserModel_t1353931605 * value)
	{
		___user_25 = value;
		Il2CppCodeGenWriteBarrier((&___user_25), value);
	}

	inline static int32_t get_offset_of_iconIndex_26() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___iconIndex_26)); }
	inline int64_t get_iconIndex_26() const { return ___iconIndex_26; }
	inline int64_t* get_address_of_iconIndex_26() { return &___iconIndex_26; }
	inline void set_iconIndex_26(int64_t value)
	{
		___iconIndex_26 = value;
	}

	inline static int32_t get_offset_of_ranks_27() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1915571260, ___ranks_27)); }
	inline StringU5BU5D_t1281789340* get_ranks_27() const { return ___ranks_27; }
	inline StringU5BU5D_t1281789340** get_address_of_ranks_27() { return &___ranks_27; }
	inline void set_ranks_27(StringU5BU5D_t1281789340* value)
	{
		___ranks_27 = value;
		Il2CppCodeGenWriteBarrier((&___ranks_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINFOCONTENTMANAGER_T1915571260_H
#ifndef UPDATEBUILDINGCONTENTMANAGER_T2330211018_H
#define UPDATEBUILDINGCONTENTMANAGER_T2330211018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateBuildingContentManager
struct  UpdateBuildingContentManager_t2330211018  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UpdateBuildingContentManager::upgrade
	bool ___upgrade_2;
	// System.String UpdateBuildingContentManager::type
	String_t* ___type_3;
	// System.Int64 UpdateBuildingContentManager::pitId
	int64_t ___pitId_4;
	// UnityEngine.UI.Text UpdateBuildingContentManager::contentLabel
	Text_t1901882714 * ___contentLabel_5;
	// UnityEngine.UI.Text UpdateBuildingContentManager::description
	Text_t1901882714 * ___description_6;
	// UnityEngine.GameObject UpdateBuildingContentManager::resources
	GameObject_t1113636619 * ___resources_7;
	// UnityEngine.GameObject UpdateBuildingContentManager::downgradeDescription
	GameObject_t1113636619 * ___downgradeDescription_8;
	// UnityEngine.UI.Text UpdateBuildingContentManager::foodNeeded
	Text_t1901882714 * ___foodNeeded_9;
	// UnityEngine.UI.Text UpdateBuildingContentManager::woodNeeded
	Text_t1901882714 * ___woodNeeded_10;
	// UnityEngine.UI.Text UpdateBuildingContentManager::stoneNeeded
	Text_t1901882714 * ___stoneNeeded_11;
	// UnityEngine.UI.Text UpdateBuildingContentManager::ironNeeded
	Text_t1901882714 * ___ironNeeded_12;
	// UnityEngine.UI.Text UpdateBuildingContentManager::copperNeeded
	Text_t1901882714 * ___copperNeeded_13;
	// UnityEngine.UI.Text UpdateBuildingContentManager::silverNeeded
	Text_t1901882714 * ___silverNeeded_14;
	// UnityEngine.UI.Text UpdateBuildingContentManager::goldNeeded
	Text_t1901882714 * ___goldNeeded_15;
	// UnityEngine.UI.Text UpdateBuildingContentManager::foodPresent
	Text_t1901882714 * ___foodPresent_16;
	// UnityEngine.UI.Text UpdateBuildingContentManager::woodPresent
	Text_t1901882714 * ___woodPresent_17;
	// UnityEngine.UI.Text UpdateBuildingContentManager::stonePresent
	Text_t1901882714 * ___stonePresent_18;
	// UnityEngine.UI.Text UpdateBuildingContentManager::ironPresent
	Text_t1901882714 * ___ironPresent_19;
	// UnityEngine.UI.Text UpdateBuildingContentManager::copperPresent
	Text_t1901882714 * ___copperPresent_20;
	// UnityEngine.UI.Text UpdateBuildingContentManager::silverPresent
	Text_t1901882714 * ___silverPresent_21;
	// UnityEngine.UI.Text UpdateBuildingContentManager::goldPresent
	Text_t1901882714 * ___goldPresent_22;
	// BuildingModel UpdateBuildingContentManager::building
	BuildingModel_t2283411500 * ___building_23;
	// WorldFieldModel UpdateBuildingContentManager::valley
	WorldFieldModel_t2417974361 * ___valley_24;
	// BuildingConstantModel UpdateBuildingContentManager::constant
	BuildingConstantModel_t2655945000 * ___constant_25;
	// ResourcesModel UpdateBuildingContentManager::cityResources
	ResourcesModel_t2533508513 * ___cityResources_26;

public:
	inline static int32_t get_offset_of_upgrade_2() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___upgrade_2)); }
	inline bool get_upgrade_2() const { return ___upgrade_2; }
	inline bool* get_address_of_upgrade_2() { return &___upgrade_2; }
	inline void set_upgrade_2(bool value)
	{
		___upgrade_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___type_3)); }
	inline String_t* get_type_3() const { return ___type_3; }
	inline String_t** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(String_t* value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_pitId_4() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___pitId_4)); }
	inline int64_t get_pitId_4() const { return ___pitId_4; }
	inline int64_t* get_address_of_pitId_4() { return &___pitId_4; }
	inline void set_pitId_4(int64_t value)
	{
		___pitId_4 = value;
	}

	inline static int32_t get_offset_of_contentLabel_5() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___contentLabel_5)); }
	inline Text_t1901882714 * get_contentLabel_5() const { return ___contentLabel_5; }
	inline Text_t1901882714 ** get_address_of_contentLabel_5() { return &___contentLabel_5; }
	inline void set_contentLabel_5(Text_t1901882714 * value)
	{
		___contentLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___contentLabel_5), value);
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___description_6)); }
	inline Text_t1901882714 * get_description_6() const { return ___description_6; }
	inline Text_t1901882714 ** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(Text_t1901882714 * value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier((&___description_6), value);
	}

	inline static int32_t get_offset_of_resources_7() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___resources_7)); }
	inline GameObject_t1113636619 * get_resources_7() const { return ___resources_7; }
	inline GameObject_t1113636619 ** get_address_of_resources_7() { return &___resources_7; }
	inline void set_resources_7(GameObject_t1113636619 * value)
	{
		___resources_7 = value;
		Il2CppCodeGenWriteBarrier((&___resources_7), value);
	}

	inline static int32_t get_offset_of_downgradeDescription_8() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___downgradeDescription_8)); }
	inline GameObject_t1113636619 * get_downgradeDescription_8() const { return ___downgradeDescription_8; }
	inline GameObject_t1113636619 ** get_address_of_downgradeDescription_8() { return &___downgradeDescription_8; }
	inline void set_downgradeDescription_8(GameObject_t1113636619 * value)
	{
		___downgradeDescription_8 = value;
		Il2CppCodeGenWriteBarrier((&___downgradeDescription_8), value);
	}

	inline static int32_t get_offset_of_foodNeeded_9() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___foodNeeded_9)); }
	inline Text_t1901882714 * get_foodNeeded_9() const { return ___foodNeeded_9; }
	inline Text_t1901882714 ** get_address_of_foodNeeded_9() { return &___foodNeeded_9; }
	inline void set_foodNeeded_9(Text_t1901882714 * value)
	{
		___foodNeeded_9 = value;
		Il2CppCodeGenWriteBarrier((&___foodNeeded_9), value);
	}

	inline static int32_t get_offset_of_woodNeeded_10() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___woodNeeded_10)); }
	inline Text_t1901882714 * get_woodNeeded_10() const { return ___woodNeeded_10; }
	inline Text_t1901882714 ** get_address_of_woodNeeded_10() { return &___woodNeeded_10; }
	inline void set_woodNeeded_10(Text_t1901882714 * value)
	{
		___woodNeeded_10 = value;
		Il2CppCodeGenWriteBarrier((&___woodNeeded_10), value);
	}

	inline static int32_t get_offset_of_stoneNeeded_11() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___stoneNeeded_11)); }
	inline Text_t1901882714 * get_stoneNeeded_11() const { return ___stoneNeeded_11; }
	inline Text_t1901882714 ** get_address_of_stoneNeeded_11() { return &___stoneNeeded_11; }
	inline void set_stoneNeeded_11(Text_t1901882714 * value)
	{
		___stoneNeeded_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoneNeeded_11), value);
	}

	inline static int32_t get_offset_of_ironNeeded_12() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___ironNeeded_12)); }
	inline Text_t1901882714 * get_ironNeeded_12() const { return ___ironNeeded_12; }
	inline Text_t1901882714 ** get_address_of_ironNeeded_12() { return &___ironNeeded_12; }
	inline void set_ironNeeded_12(Text_t1901882714 * value)
	{
		___ironNeeded_12 = value;
		Il2CppCodeGenWriteBarrier((&___ironNeeded_12), value);
	}

	inline static int32_t get_offset_of_copperNeeded_13() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___copperNeeded_13)); }
	inline Text_t1901882714 * get_copperNeeded_13() const { return ___copperNeeded_13; }
	inline Text_t1901882714 ** get_address_of_copperNeeded_13() { return &___copperNeeded_13; }
	inline void set_copperNeeded_13(Text_t1901882714 * value)
	{
		___copperNeeded_13 = value;
		Il2CppCodeGenWriteBarrier((&___copperNeeded_13), value);
	}

	inline static int32_t get_offset_of_silverNeeded_14() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___silverNeeded_14)); }
	inline Text_t1901882714 * get_silverNeeded_14() const { return ___silverNeeded_14; }
	inline Text_t1901882714 ** get_address_of_silverNeeded_14() { return &___silverNeeded_14; }
	inline void set_silverNeeded_14(Text_t1901882714 * value)
	{
		___silverNeeded_14 = value;
		Il2CppCodeGenWriteBarrier((&___silverNeeded_14), value);
	}

	inline static int32_t get_offset_of_goldNeeded_15() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___goldNeeded_15)); }
	inline Text_t1901882714 * get_goldNeeded_15() const { return ___goldNeeded_15; }
	inline Text_t1901882714 ** get_address_of_goldNeeded_15() { return &___goldNeeded_15; }
	inline void set_goldNeeded_15(Text_t1901882714 * value)
	{
		___goldNeeded_15 = value;
		Il2CppCodeGenWriteBarrier((&___goldNeeded_15), value);
	}

	inline static int32_t get_offset_of_foodPresent_16() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___foodPresent_16)); }
	inline Text_t1901882714 * get_foodPresent_16() const { return ___foodPresent_16; }
	inline Text_t1901882714 ** get_address_of_foodPresent_16() { return &___foodPresent_16; }
	inline void set_foodPresent_16(Text_t1901882714 * value)
	{
		___foodPresent_16 = value;
		Il2CppCodeGenWriteBarrier((&___foodPresent_16), value);
	}

	inline static int32_t get_offset_of_woodPresent_17() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___woodPresent_17)); }
	inline Text_t1901882714 * get_woodPresent_17() const { return ___woodPresent_17; }
	inline Text_t1901882714 ** get_address_of_woodPresent_17() { return &___woodPresent_17; }
	inline void set_woodPresent_17(Text_t1901882714 * value)
	{
		___woodPresent_17 = value;
		Il2CppCodeGenWriteBarrier((&___woodPresent_17), value);
	}

	inline static int32_t get_offset_of_stonePresent_18() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___stonePresent_18)); }
	inline Text_t1901882714 * get_stonePresent_18() const { return ___stonePresent_18; }
	inline Text_t1901882714 ** get_address_of_stonePresent_18() { return &___stonePresent_18; }
	inline void set_stonePresent_18(Text_t1901882714 * value)
	{
		___stonePresent_18 = value;
		Il2CppCodeGenWriteBarrier((&___stonePresent_18), value);
	}

	inline static int32_t get_offset_of_ironPresent_19() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___ironPresent_19)); }
	inline Text_t1901882714 * get_ironPresent_19() const { return ___ironPresent_19; }
	inline Text_t1901882714 ** get_address_of_ironPresent_19() { return &___ironPresent_19; }
	inline void set_ironPresent_19(Text_t1901882714 * value)
	{
		___ironPresent_19 = value;
		Il2CppCodeGenWriteBarrier((&___ironPresent_19), value);
	}

	inline static int32_t get_offset_of_copperPresent_20() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___copperPresent_20)); }
	inline Text_t1901882714 * get_copperPresent_20() const { return ___copperPresent_20; }
	inline Text_t1901882714 ** get_address_of_copperPresent_20() { return &___copperPresent_20; }
	inline void set_copperPresent_20(Text_t1901882714 * value)
	{
		___copperPresent_20 = value;
		Il2CppCodeGenWriteBarrier((&___copperPresent_20), value);
	}

	inline static int32_t get_offset_of_silverPresent_21() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___silverPresent_21)); }
	inline Text_t1901882714 * get_silverPresent_21() const { return ___silverPresent_21; }
	inline Text_t1901882714 ** get_address_of_silverPresent_21() { return &___silverPresent_21; }
	inline void set_silverPresent_21(Text_t1901882714 * value)
	{
		___silverPresent_21 = value;
		Il2CppCodeGenWriteBarrier((&___silverPresent_21), value);
	}

	inline static int32_t get_offset_of_goldPresent_22() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___goldPresent_22)); }
	inline Text_t1901882714 * get_goldPresent_22() const { return ___goldPresent_22; }
	inline Text_t1901882714 ** get_address_of_goldPresent_22() { return &___goldPresent_22; }
	inline void set_goldPresent_22(Text_t1901882714 * value)
	{
		___goldPresent_22 = value;
		Il2CppCodeGenWriteBarrier((&___goldPresent_22), value);
	}

	inline static int32_t get_offset_of_building_23() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___building_23)); }
	inline BuildingModel_t2283411500 * get_building_23() const { return ___building_23; }
	inline BuildingModel_t2283411500 ** get_address_of_building_23() { return &___building_23; }
	inline void set_building_23(BuildingModel_t2283411500 * value)
	{
		___building_23 = value;
		Il2CppCodeGenWriteBarrier((&___building_23), value);
	}

	inline static int32_t get_offset_of_valley_24() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___valley_24)); }
	inline WorldFieldModel_t2417974361 * get_valley_24() const { return ___valley_24; }
	inline WorldFieldModel_t2417974361 ** get_address_of_valley_24() { return &___valley_24; }
	inline void set_valley_24(WorldFieldModel_t2417974361 * value)
	{
		___valley_24 = value;
		Il2CppCodeGenWriteBarrier((&___valley_24), value);
	}

	inline static int32_t get_offset_of_constant_25() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___constant_25)); }
	inline BuildingConstantModel_t2655945000 * get_constant_25() const { return ___constant_25; }
	inline BuildingConstantModel_t2655945000 ** get_address_of_constant_25() { return &___constant_25; }
	inline void set_constant_25(BuildingConstantModel_t2655945000 * value)
	{
		___constant_25 = value;
		Il2CppCodeGenWriteBarrier((&___constant_25), value);
	}

	inline static int32_t get_offset_of_cityResources_26() { return static_cast<int32_t>(offsetof(UpdateBuildingContentManager_t2330211018, ___cityResources_26)); }
	inline ResourcesModel_t2533508513 * get_cityResources_26() const { return ___cityResources_26; }
	inline ResourcesModel_t2533508513 ** get_address_of_cityResources_26() { return &___cityResources_26; }
	inline void set_cityResources_26(ResourcesModel_t2533508513 * value)
	{
		___cityResources_26 = value;
		Il2CppCodeGenWriteBarrier((&___cityResources_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBUILDINGCONTENTMANAGER_T2330211018_H
#ifndef UNITSTABLEMANAGER_T4191917185_H
#define UNITSTABLEMANAGER_T4191917185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsTableManager
struct  UnitsTableManager_t4191917185  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text UnitsTableManager::cityWorker
	Text_t1901882714 * ___cityWorker_2;
	// UnityEngine.UI.Text UnitsTableManager::citySpy
	Text_t1901882714 * ___citySpy_3;
	// UnityEngine.UI.Text UnitsTableManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_4;
	// UnityEngine.UI.Text UnitsTableManager::citySpearman
	Text_t1901882714 * ___citySpearman_5;
	// UnityEngine.UI.Text UnitsTableManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_6;
	// UnityEngine.UI.Text UnitsTableManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_7;
	// UnityEngine.UI.Text UnitsTableManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_8;
	// UnityEngine.UI.Text UnitsTableManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_9;
	// UnityEngine.UI.Text UnitsTableManager::cityArcher
	Text_t1901882714 * ___cityArcher_10;
	// UnityEngine.UI.Text UnitsTableManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_11;
	// UnityEngine.UI.Text UnitsTableManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_12;
	// UnityEngine.UI.Text UnitsTableManager::cityWagon
	Text_t1901882714 * ___cityWagon_13;
	// UnityEngine.UI.Text UnitsTableManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_14;
	// UnityEngine.UI.Text UnitsTableManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_15;
	// UnityEngine.UI.Text UnitsTableManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_16;
	// UnityEngine.UI.Text UnitsTableManager::cityBallista
	Text_t1901882714 * ___cityBallista_17;
	// UnityEngine.UI.Text UnitsTableManager::cityBeaconTower
	Text_t1901882714 * ___cityBeaconTower_18;
	// UnityEngine.UI.Text UnitsTableManager::cityArcherTower
	Text_t1901882714 * ___cityArcherTower_19;
	// UnityEngine.UI.Text UnitsTableManager::cityHotOilHole
	Text_t1901882714 * ___cityHotOilHole_20;
	// UnityEngine.UI.Text UnitsTableManager::cityTrebuchetTower
	Text_t1901882714 * ___cityTrebuchetTower_21;
	// UnityEngine.UI.Text UnitsTableManager::cityBallistaTower
	Text_t1901882714 * ___cityBallistaTower_22;

public:
	inline static int32_t get_offset_of_cityWorker_2() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityWorker_2)); }
	inline Text_t1901882714 * get_cityWorker_2() const { return ___cityWorker_2; }
	inline Text_t1901882714 ** get_address_of_cityWorker_2() { return &___cityWorker_2; }
	inline void set_cityWorker_2(Text_t1901882714 * value)
	{
		___cityWorker_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_2), value);
	}

	inline static int32_t get_offset_of_citySpy_3() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___citySpy_3)); }
	inline Text_t1901882714 * get_citySpy_3() const { return ___citySpy_3; }
	inline Text_t1901882714 ** get_address_of_citySpy_3() { return &___citySpy_3; }
	inline void set_citySpy_3(Text_t1901882714 * value)
	{
		___citySpy_3 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_3), value);
	}

	inline static int32_t get_offset_of_citySwordsman_4() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___citySwordsman_4)); }
	inline Text_t1901882714 * get_citySwordsman_4() const { return ___citySwordsman_4; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_4() { return &___citySwordsman_4; }
	inline void set_citySwordsman_4(Text_t1901882714 * value)
	{
		___citySwordsman_4 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_4), value);
	}

	inline static int32_t get_offset_of_citySpearman_5() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___citySpearman_5)); }
	inline Text_t1901882714 * get_citySpearman_5() const { return ___citySpearman_5; }
	inline Text_t1901882714 ** get_address_of_citySpearman_5() { return &___citySpearman_5; }
	inline void set_citySpearman_5(Text_t1901882714 * value)
	{
		___citySpearman_5 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_5), value);
	}

	inline static int32_t get_offset_of_cityPikeman_6() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityPikeman_6)); }
	inline Text_t1901882714 * get_cityPikeman_6() const { return ___cityPikeman_6; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_6() { return &___cityPikeman_6; }
	inline void set_cityPikeman_6(Text_t1901882714 * value)
	{
		___cityPikeman_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_6), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_7() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityScoutRider_7)); }
	inline Text_t1901882714 * get_cityScoutRider_7() const { return ___cityScoutRider_7; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_7() { return &___cityScoutRider_7; }
	inline void set_cityScoutRider_7(Text_t1901882714 * value)
	{
		___cityScoutRider_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_7), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_8() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityLightCavalry_8)); }
	inline Text_t1901882714 * get_cityLightCavalry_8() const { return ___cityLightCavalry_8; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_8() { return &___cityLightCavalry_8; }
	inline void set_cityLightCavalry_8(Text_t1901882714 * value)
	{
		___cityLightCavalry_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_8), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_9() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityHeavyCavalry_9)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_9() const { return ___cityHeavyCavalry_9; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_9() { return &___cityHeavyCavalry_9; }
	inline void set_cityHeavyCavalry_9(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_9), value);
	}

	inline static int32_t get_offset_of_cityArcher_10() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityArcher_10)); }
	inline Text_t1901882714 * get_cityArcher_10() const { return ___cityArcher_10; }
	inline Text_t1901882714 ** get_address_of_cityArcher_10() { return &___cityArcher_10; }
	inline void set_cityArcher_10(Text_t1901882714 * value)
	{
		___cityArcher_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_10), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_11() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityArcherRider_11)); }
	inline Text_t1901882714 * get_cityArcherRider_11() const { return ___cityArcherRider_11; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_11() { return &___cityArcherRider_11; }
	inline void set_cityArcherRider_11(Text_t1901882714 * value)
	{
		___cityArcherRider_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_11), value);
	}

	inline static int32_t get_offset_of_cityHollyman_12() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityHollyman_12)); }
	inline Text_t1901882714 * get_cityHollyman_12() const { return ___cityHollyman_12; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_12() { return &___cityHollyman_12; }
	inline void set_cityHollyman_12(Text_t1901882714 * value)
	{
		___cityHollyman_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_12), value);
	}

	inline static int32_t get_offset_of_cityWagon_13() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityWagon_13)); }
	inline Text_t1901882714 * get_cityWagon_13() const { return ___cityWagon_13; }
	inline Text_t1901882714 ** get_address_of_cityWagon_13() { return &___cityWagon_13; }
	inline void set_cityWagon_13(Text_t1901882714 * value)
	{
		___cityWagon_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_13), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_14() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityTrebuchet_14)); }
	inline Text_t1901882714 * get_cityTrebuchet_14() const { return ___cityTrebuchet_14; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_14() { return &___cityTrebuchet_14; }
	inline void set_cityTrebuchet_14(Text_t1901882714 * value)
	{
		___cityTrebuchet_14 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_14), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_15() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___citySiegeTower_15)); }
	inline Text_t1901882714 * get_citySiegeTower_15() const { return ___citySiegeTower_15; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_15() { return &___citySiegeTower_15; }
	inline void set_citySiegeTower_15(Text_t1901882714 * value)
	{
		___citySiegeTower_15 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_15), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_16() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityBatteringRam_16)); }
	inline Text_t1901882714 * get_cityBatteringRam_16() const { return ___cityBatteringRam_16; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_16() { return &___cityBatteringRam_16; }
	inline void set_cityBatteringRam_16(Text_t1901882714 * value)
	{
		___cityBatteringRam_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_16), value);
	}

	inline static int32_t get_offset_of_cityBallista_17() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityBallista_17)); }
	inline Text_t1901882714 * get_cityBallista_17() const { return ___cityBallista_17; }
	inline Text_t1901882714 ** get_address_of_cityBallista_17() { return &___cityBallista_17; }
	inline void set_cityBallista_17(Text_t1901882714 * value)
	{
		___cityBallista_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_17), value);
	}

	inline static int32_t get_offset_of_cityBeaconTower_18() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityBeaconTower_18)); }
	inline Text_t1901882714 * get_cityBeaconTower_18() const { return ___cityBeaconTower_18; }
	inline Text_t1901882714 ** get_address_of_cityBeaconTower_18() { return &___cityBeaconTower_18; }
	inline void set_cityBeaconTower_18(Text_t1901882714 * value)
	{
		___cityBeaconTower_18 = value;
		Il2CppCodeGenWriteBarrier((&___cityBeaconTower_18), value);
	}

	inline static int32_t get_offset_of_cityArcherTower_19() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityArcherTower_19)); }
	inline Text_t1901882714 * get_cityArcherTower_19() const { return ___cityArcherTower_19; }
	inline Text_t1901882714 ** get_address_of_cityArcherTower_19() { return &___cityArcherTower_19; }
	inline void set_cityArcherTower_19(Text_t1901882714 * value)
	{
		___cityArcherTower_19 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherTower_19), value);
	}

	inline static int32_t get_offset_of_cityHotOilHole_20() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityHotOilHole_20)); }
	inline Text_t1901882714 * get_cityHotOilHole_20() const { return ___cityHotOilHole_20; }
	inline Text_t1901882714 ** get_address_of_cityHotOilHole_20() { return &___cityHotOilHole_20; }
	inline void set_cityHotOilHole_20(Text_t1901882714 * value)
	{
		___cityHotOilHole_20 = value;
		Il2CppCodeGenWriteBarrier((&___cityHotOilHole_20), value);
	}

	inline static int32_t get_offset_of_cityTrebuchetTower_21() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityTrebuchetTower_21)); }
	inline Text_t1901882714 * get_cityTrebuchetTower_21() const { return ___cityTrebuchetTower_21; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchetTower_21() { return &___cityTrebuchetTower_21; }
	inline void set_cityTrebuchetTower_21(Text_t1901882714 * value)
	{
		___cityTrebuchetTower_21 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchetTower_21), value);
	}

	inline static int32_t get_offset_of_cityBallistaTower_22() { return static_cast<int32_t>(offsetof(UnitsTableManager_t4191917185, ___cityBallistaTower_22)); }
	inline Text_t1901882714 * get_cityBallistaTower_22() const { return ___cityBallistaTower_22; }
	inline Text_t1901882714 ** get_address_of_cityBallistaTower_22() { return &___cityBallistaTower_22; }
	inline void set_cityBallistaTower_22(Text_t1901882714 * value)
	{
		___cityBallistaTower_22 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallistaTower_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSTABLEMANAGER_T4191917185_H
#ifndef DONTDESTROYME_T4280075198_H
#define DONTDESTROYME_T4280075198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyMe
struct  DontDestroyMe_t4280075198  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYME_T4280075198_H
#ifndef ALLIANCEINFORMATIONCONTENTMANAGER_T1344194199_H
#define ALLIANCEINFORMATIONCONTENTMANAGER_T1344194199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInformationContentManager
struct  AllianceInformationContentManager_t1344194199  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceName
	Text_t1901882714 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceFounder
	Text_t1901882714 * ___allianceFounder_3;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceLeader
	Text_t1901882714 * ___allianceLeader_4;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceRank
	Text_t1901882714 * ___allianceRank_5;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceExperiance
	Text_t1901882714 * ___allianceExperiance_6;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceMembersCount
	Text_t1901882714 * ___allianceMembersCount_7;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceGuideline
	Text_t1901882714 * ___allianceGuideline_8;
	// ConfirmationEvent AllianceInformationContentManager::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_9;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceName_2)); }
	inline Text_t1901882714 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t1901882714 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t1901882714 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_2), value);
	}

	inline static int32_t get_offset_of_allianceFounder_3() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceFounder_3)); }
	inline Text_t1901882714 * get_allianceFounder_3() const { return ___allianceFounder_3; }
	inline Text_t1901882714 ** get_address_of_allianceFounder_3() { return &___allianceFounder_3; }
	inline void set_allianceFounder_3(Text_t1901882714 * value)
	{
		___allianceFounder_3 = value;
		Il2CppCodeGenWriteBarrier((&___allianceFounder_3), value);
	}

	inline static int32_t get_offset_of_allianceLeader_4() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceLeader_4)); }
	inline Text_t1901882714 * get_allianceLeader_4() const { return ___allianceLeader_4; }
	inline Text_t1901882714 ** get_address_of_allianceLeader_4() { return &___allianceLeader_4; }
	inline void set_allianceLeader_4(Text_t1901882714 * value)
	{
		___allianceLeader_4 = value;
		Il2CppCodeGenWriteBarrier((&___allianceLeader_4), value);
	}

	inline static int32_t get_offset_of_allianceRank_5() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceRank_5)); }
	inline Text_t1901882714 * get_allianceRank_5() const { return ___allianceRank_5; }
	inline Text_t1901882714 ** get_address_of_allianceRank_5() { return &___allianceRank_5; }
	inline void set_allianceRank_5(Text_t1901882714 * value)
	{
		___allianceRank_5 = value;
		Il2CppCodeGenWriteBarrier((&___allianceRank_5), value);
	}

	inline static int32_t get_offset_of_allianceExperiance_6() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceExperiance_6)); }
	inline Text_t1901882714 * get_allianceExperiance_6() const { return ___allianceExperiance_6; }
	inline Text_t1901882714 ** get_address_of_allianceExperiance_6() { return &___allianceExperiance_6; }
	inline void set_allianceExperiance_6(Text_t1901882714 * value)
	{
		___allianceExperiance_6 = value;
		Il2CppCodeGenWriteBarrier((&___allianceExperiance_6), value);
	}

	inline static int32_t get_offset_of_allianceMembersCount_7() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceMembersCount_7)); }
	inline Text_t1901882714 * get_allianceMembersCount_7() const { return ___allianceMembersCount_7; }
	inline Text_t1901882714 ** get_address_of_allianceMembersCount_7() { return &___allianceMembersCount_7; }
	inline void set_allianceMembersCount_7(Text_t1901882714 * value)
	{
		___allianceMembersCount_7 = value;
		Il2CppCodeGenWriteBarrier((&___allianceMembersCount_7), value);
	}

	inline static int32_t get_offset_of_allianceGuideline_8() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___allianceGuideline_8)); }
	inline Text_t1901882714 * get_allianceGuideline_8() const { return ___allianceGuideline_8; }
	inline Text_t1901882714 ** get_address_of_allianceGuideline_8() { return &___allianceGuideline_8; }
	inline void set_allianceGuideline_8(Text_t1901882714 * value)
	{
		___allianceGuideline_8 = value;
		Il2CppCodeGenWriteBarrier((&___allianceGuideline_8), value);
	}

	inline static int32_t get_offset_of_confirmed_9() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t1344194199, ___confirmed_9)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_9() const { return ___confirmed_9; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_9() { return &___confirmed_9; }
	inline void set_confirmed_9(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_9 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEINFORMATIONCONTENTMANAGER_T1344194199_H
#ifndef ADMANAGER_T261308543_H
#define ADMANAGER_T261308543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADManager
struct  ADManager_t261308543  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ADManager::UI
	GameObject_t1113636619 * ___UI_4;
	// GoogleMobileAds.Api.BannerView ADManager::bannerView
	BannerView_t2480907735 * ___bannerView_5;
	// GoogleMobileAds.Api.InterstitialAd ADManager::interstitial
	InterstitialAd_t207380487 * ___interstitial_6;
	// GoogleMobileAds.Api.RewardBasedVideoAd ADManager::rewardBasedVideo
	RewardBasedVideoAd_t2288675867 * ___rewardBasedVideo_7;
	// System.DateTime ADManager::nextBonusWoodDate
	DateTime_t3738529785  ___nextBonusWoodDate_8;
	// System.DateTime ADManager::nextBonusFoodDate
	DateTime_t3738529785  ___nextBonusFoodDate_9;
	// System.DateTime ADManager::nextBonusStoneDate
	DateTime_t3738529785  ___nextBonusStoneDate_10;
	// System.DateTime ADManager::nextBonusIronDate
	DateTime_t3738529785  ___nextBonusIronDate_11;
	// System.DateTime ADManager::nextBonusGoldDate
	DateTime_t3738529785  ___nextBonusGoldDate_12;
	// System.DateTime ADManager::nextBonusSilverDate
	DateTime_t3738529785  ___nextBonusSilverDate_13;
	// System.DateTime ADManager::nextBonusCopperDate
	DateTime_t3738529785  ___nextBonusCopperDate_14;
	// System.String ADManager::resource
	String_t* ___resource_15;

public:
	inline static int32_t get_offset_of_UI_4() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___UI_4)); }
	inline GameObject_t1113636619 * get_UI_4() const { return ___UI_4; }
	inline GameObject_t1113636619 ** get_address_of_UI_4() { return &___UI_4; }
	inline void set_UI_4(GameObject_t1113636619 * value)
	{
		___UI_4 = value;
		Il2CppCodeGenWriteBarrier((&___UI_4), value);
	}

	inline static int32_t get_offset_of_bannerView_5() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___bannerView_5)); }
	inline BannerView_t2480907735 * get_bannerView_5() const { return ___bannerView_5; }
	inline BannerView_t2480907735 ** get_address_of_bannerView_5() { return &___bannerView_5; }
	inline void set_bannerView_5(BannerView_t2480907735 * value)
	{
		___bannerView_5 = value;
		Il2CppCodeGenWriteBarrier((&___bannerView_5), value);
	}

	inline static int32_t get_offset_of_interstitial_6() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___interstitial_6)); }
	inline InterstitialAd_t207380487 * get_interstitial_6() const { return ___interstitial_6; }
	inline InterstitialAd_t207380487 ** get_address_of_interstitial_6() { return &___interstitial_6; }
	inline void set_interstitial_6(InterstitialAd_t207380487 * value)
	{
		___interstitial_6 = value;
		Il2CppCodeGenWriteBarrier((&___interstitial_6), value);
	}

	inline static int32_t get_offset_of_rewardBasedVideo_7() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___rewardBasedVideo_7)); }
	inline RewardBasedVideoAd_t2288675867 * get_rewardBasedVideo_7() const { return ___rewardBasedVideo_7; }
	inline RewardBasedVideoAd_t2288675867 ** get_address_of_rewardBasedVideo_7() { return &___rewardBasedVideo_7; }
	inline void set_rewardBasedVideo_7(RewardBasedVideoAd_t2288675867 * value)
	{
		___rewardBasedVideo_7 = value;
		Il2CppCodeGenWriteBarrier((&___rewardBasedVideo_7), value);
	}

	inline static int32_t get_offset_of_nextBonusWoodDate_8() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusWoodDate_8)); }
	inline DateTime_t3738529785  get_nextBonusWoodDate_8() const { return ___nextBonusWoodDate_8; }
	inline DateTime_t3738529785 * get_address_of_nextBonusWoodDate_8() { return &___nextBonusWoodDate_8; }
	inline void set_nextBonusWoodDate_8(DateTime_t3738529785  value)
	{
		___nextBonusWoodDate_8 = value;
	}

	inline static int32_t get_offset_of_nextBonusFoodDate_9() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusFoodDate_9)); }
	inline DateTime_t3738529785  get_nextBonusFoodDate_9() const { return ___nextBonusFoodDate_9; }
	inline DateTime_t3738529785 * get_address_of_nextBonusFoodDate_9() { return &___nextBonusFoodDate_9; }
	inline void set_nextBonusFoodDate_9(DateTime_t3738529785  value)
	{
		___nextBonusFoodDate_9 = value;
	}

	inline static int32_t get_offset_of_nextBonusStoneDate_10() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusStoneDate_10)); }
	inline DateTime_t3738529785  get_nextBonusStoneDate_10() const { return ___nextBonusStoneDate_10; }
	inline DateTime_t3738529785 * get_address_of_nextBonusStoneDate_10() { return &___nextBonusStoneDate_10; }
	inline void set_nextBonusStoneDate_10(DateTime_t3738529785  value)
	{
		___nextBonusStoneDate_10 = value;
	}

	inline static int32_t get_offset_of_nextBonusIronDate_11() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusIronDate_11)); }
	inline DateTime_t3738529785  get_nextBonusIronDate_11() const { return ___nextBonusIronDate_11; }
	inline DateTime_t3738529785 * get_address_of_nextBonusIronDate_11() { return &___nextBonusIronDate_11; }
	inline void set_nextBonusIronDate_11(DateTime_t3738529785  value)
	{
		___nextBonusIronDate_11 = value;
	}

	inline static int32_t get_offset_of_nextBonusGoldDate_12() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusGoldDate_12)); }
	inline DateTime_t3738529785  get_nextBonusGoldDate_12() const { return ___nextBonusGoldDate_12; }
	inline DateTime_t3738529785 * get_address_of_nextBonusGoldDate_12() { return &___nextBonusGoldDate_12; }
	inline void set_nextBonusGoldDate_12(DateTime_t3738529785  value)
	{
		___nextBonusGoldDate_12 = value;
	}

	inline static int32_t get_offset_of_nextBonusSilverDate_13() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusSilverDate_13)); }
	inline DateTime_t3738529785  get_nextBonusSilverDate_13() const { return ___nextBonusSilverDate_13; }
	inline DateTime_t3738529785 * get_address_of_nextBonusSilverDate_13() { return &___nextBonusSilverDate_13; }
	inline void set_nextBonusSilverDate_13(DateTime_t3738529785  value)
	{
		___nextBonusSilverDate_13 = value;
	}

	inline static int32_t get_offset_of_nextBonusCopperDate_14() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___nextBonusCopperDate_14)); }
	inline DateTime_t3738529785  get_nextBonusCopperDate_14() const { return ___nextBonusCopperDate_14; }
	inline DateTime_t3738529785 * get_address_of_nextBonusCopperDate_14() { return &___nextBonusCopperDate_14; }
	inline void set_nextBonusCopperDate_14(DateTime_t3738529785  value)
	{
		___nextBonusCopperDate_14 = value;
	}

	inline static int32_t get_offset_of_resource_15() { return static_cast<int32_t>(offsetof(ADManager_t261308543, ___resource_15)); }
	inline String_t* get_resource_15() const { return ___resource_15; }
	inline String_t** get_address_of_resource_15() { return &___resource_15; }
	inline void set_resource_15(String_t* value)
	{
		___resource_15 = value;
		Il2CppCodeGenWriteBarrier((&___resource_15), value);
	}
};

struct ADManager_t261308543_StaticFields
{
public:
	// System.String ADManager::adBannerUnitId
	String_t* ___adBannerUnitId_2;
	// System.String ADManager::adInterstitialUnitId
	String_t* ___adInterstitialUnitId_3;

public:
	inline static int32_t get_offset_of_adBannerUnitId_2() { return static_cast<int32_t>(offsetof(ADManager_t261308543_StaticFields, ___adBannerUnitId_2)); }
	inline String_t* get_adBannerUnitId_2() const { return ___adBannerUnitId_2; }
	inline String_t** get_address_of_adBannerUnitId_2() { return &___adBannerUnitId_2; }
	inline void set_adBannerUnitId_2(String_t* value)
	{
		___adBannerUnitId_2 = value;
		Il2CppCodeGenWriteBarrier((&___adBannerUnitId_2), value);
	}

	inline static int32_t get_offset_of_adInterstitialUnitId_3() { return static_cast<int32_t>(offsetof(ADManager_t261308543_StaticFields, ___adInterstitialUnitId_3)); }
	inline String_t* get_adInterstitialUnitId_3() const { return ___adInterstitialUnitId_3; }
	inline String_t** get_address_of_adInterstitialUnitId_3() { return &___adInterstitialUnitId_3; }
	inline void set_adInterstitialUnitId_3(String_t* value)
	{
		___adInterstitialUnitId_3 = value;
		Il2CppCodeGenWriteBarrier((&___adInterstitialUnitId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMANAGER_T261308543_H
#ifndef FROMFIELDSTOCASTLE_T767099202_H
#define FROMFIELDSTOCASTLE_T767099202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FromFieldsToCastle
struct  FromFieldsToCastle_t767099202  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMFIELDSTOCASTLE_T767099202_H
#ifndef UNITDETAILSMANAGER_T3163445022_H
#define UNITDETAILSMANAGER_T3163445022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitDetailsManager
struct  UnitDetailsManager_t3163445022  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text UnitDetailsManager::unitCountLabel
	Text_t1901882714 * ___unitCountLabel_2;
	// UnityEngine.UI.Text UnitDetailsManager::unitDescriptionLabel
	Text_t1901882714 * ___unitDescriptionLabel_3;
	// UnityEngine.UI.Image UnitDetailsManager::unitIcon
	Image_t2670269651 * ___unitIcon_4;
	// UnityEngine.UI.Text UnitDetailsManager::speed
	Text_t1901882714 * ___speed_5;
	// UnityEngine.UI.Text UnitDetailsManager::defence
	Text_t1901882714 * ___defence_6;
	// UnityEngine.UI.Text UnitDetailsManager::food
	Text_t1901882714 * ___food_7;
	// UnityEngine.UI.Text UnitDetailsManager::range
	Text_t1901882714 * ___range_8;
	// UnityEngine.UI.Text UnitDetailsManager::life
	Text_t1901882714 * ___life_9;
	// UnityEngine.UI.Text UnitDetailsManager::population
	Text_t1901882714 * ___population_10;
	// UnityEngine.UI.Text UnitDetailsManager::attack
	Text_t1901882714 * ___attack_11;
	// UnityEngine.UI.Text UnitDetailsManager::capacity
	Text_t1901882714 * ___capacity_12;
	// UnityEngine.UI.Text UnitDetailsManager::time
	Text_t1901882714 * ___time_13;
	// UnityEngine.UI.Text UnitDetailsManager::foodNeeded
	Text_t1901882714 * ___foodNeeded_14;
	// UnityEngine.UI.Text UnitDetailsManager::woodNeeded
	Text_t1901882714 * ___woodNeeded_15;
	// UnityEngine.UI.Text UnitDetailsManager::stoneNeeded
	Text_t1901882714 * ___stoneNeeded_16;
	// UnityEngine.UI.Text UnitDetailsManager::ironNeeded
	Text_t1901882714 * ___ironNeeded_17;
	// UnityEngine.UI.Text UnitDetailsManager::copperNeeded
	Text_t1901882714 * ___copperNeeded_18;
	// UnityEngine.UI.Text UnitDetailsManager::silverNeeded
	Text_t1901882714 * ___silverNeeded_19;
	// UnityEngine.UI.Text UnitDetailsManager::goldNeeded
	Text_t1901882714 * ___goldNeeded_20;
	// UnityEngine.UI.Text UnitDetailsManager::foodPresent
	Text_t1901882714 * ___foodPresent_21;
	// UnityEngine.UI.Text UnitDetailsManager::woodPresent
	Text_t1901882714 * ___woodPresent_22;
	// UnityEngine.UI.Text UnitDetailsManager::stonePresent
	Text_t1901882714 * ___stonePresent_23;
	// UnityEngine.UI.Text UnitDetailsManager::ironPresent
	Text_t1901882714 * ___ironPresent_24;
	// UnityEngine.UI.Text UnitDetailsManager::copperPresent
	Text_t1901882714 * ___copperPresent_25;
	// UnityEngine.UI.Text UnitDetailsManager::silverPresent
	Text_t1901882714 * ___silverPresent_26;
	// UnityEngine.UI.Text UnitDetailsManager::goldPresent
	Text_t1901882714 * ___goldPresent_27;
	// UnityEngine.UI.InputField UnitDetailsManager::unitsToRecruit
	InputField_t3762917431 * ___unitsToRecruit_28;
	// System.String UnitDetailsManager::unitName
	String_t* ___unitName_29;
	// UnitConstantModel UnitDetailsManager::unit
	UnitConstantModel_t3582189408 * ___unit_30;
	// ResourcesModel UnitDetailsManager::resources
	ResourcesModel_t2533508513 * ___resources_31;

public:
	inline static int32_t get_offset_of_unitCountLabel_2() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unitCountLabel_2)); }
	inline Text_t1901882714 * get_unitCountLabel_2() const { return ___unitCountLabel_2; }
	inline Text_t1901882714 ** get_address_of_unitCountLabel_2() { return &___unitCountLabel_2; }
	inline void set_unitCountLabel_2(Text_t1901882714 * value)
	{
		___unitCountLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___unitCountLabel_2), value);
	}

	inline static int32_t get_offset_of_unitDescriptionLabel_3() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unitDescriptionLabel_3)); }
	inline Text_t1901882714 * get_unitDescriptionLabel_3() const { return ___unitDescriptionLabel_3; }
	inline Text_t1901882714 ** get_address_of_unitDescriptionLabel_3() { return &___unitDescriptionLabel_3; }
	inline void set_unitDescriptionLabel_3(Text_t1901882714 * value)
	{
		___unitDescriptionLabel_3 = value;
		Il2CppCodeGenWriteBarrier((&___unitDescriptionLabel_3), value);
	}

	inline static int32_t get_offset_of_unitIcon_4() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unitIcon_4)); }
	inline Image_t2670269651 * get_unitIcon_4() const { return ___unitIcon_4; }
	inline Image_t2670269651 ** get_address_of_unitIcon_4() { return &___unitIcon_4; }
	inline void set_unitIcon_4(Image_t2670269651 * value)
	{
		___unitIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitIcon_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___speed_5)); }
	inline Text_t1901882714 * get_speed_5() const { return ___speed_5; }
	inline Text_t1901882714 ** get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(Text_t1901882714 * value)
	{
		___speed_5 = value;
		Il2CppCodeGenWriteBarrier((&___speed_5), value);
	}

	inline static int32_t get_offset_of_defence_6() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___defence_6)); }
	inline Text_t1901882714 * get_defence_6() const { return ___defence_6; }
	inline Text_t1901882714 ** get_address_of_defence_6() { return &___defence_6; }
	inline void set_defence_6(Text_t1901882714 * value)
	{
		___defence_6 = value;
		Il2CppCodeGenWriteBarrier((&___defence_6), value);
	}

	inline static int32_t get_offset_of_food_7() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___food_7)); }
	inline Text_t1901882714 * get_food_7() const { return ___food_7; }
	inline Text_t1901882714 ** get_address_of_food_7() { return &___food_7; }
	inline void set_food_7(Text_t1901882714 * value)
	{
		___food_7 = value;
		Il2CppCodeGenWriteBarrier((&___food_7), value);
	}

	inline static int32_t get_offset_of_range_8() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___range_8)); }
	inline Text_t1901882714 * get_range_8() const { return ___range_8; }
	inline Text_t1901882714 ** get_address_of_range_8() { return &___range_8; }
	inline void set_range_8(Text_t1901882714 * value)
	{
		___range_8 = value;
		Il2CppCodeGenWriteBarrier((&___range_8), value);
	}

	inline static int32_t get_offset_of_life_9() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___life_9)); }
	inline Text_t1901882714 * get_life_9() const { return ___life_9; }
	inline Text_t1901882714 ** get_address_of_life_9() { return &___life_9; }
	inline void set_life_9(Text_t1901882714 * value)
	{
		___life_9 = value;
		Il2CppCodeGenWriteBarrier((&___life_9), value);
	}

	inline static int32_t get_offset_of_population_10() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___population_10)); }
	inline Text_t1901882714 * get_population_10() const { return ___population_10; }
	inline Text_t1901882714 ** get_address_of_population_10() { return &___population_10; }
	inline void set_population_10(Text_t1901882714 * value)
	{
		___population_10 = value;
		Il2CppCodeGenWriteBarrier((&___population_10), value);
	}

	inline static int32_t get_offset_of_attack_11() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___attack_11)); }
	inline Text_t1901882714 * get_attack_11() const { return ___attack_11; }
	inline Text_t1901882714 ** get_address_of_attack_11() { return &___attack_11; }
	inline void set_attack_11(Text_t1901882714 * value)
	{
		___attack_11 = value;
		Il2CppCodeGenWriteBarrier((&___attack_11), value);
	}

	inline static int32_t get_offset_of_capacity_12() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___capacity_12)); }
	inline Text_t1901882714 * get_capacity_12() const { return ___capacity_12; }
	inline Text_t1901882714 ** get_address_of_capacity_12() { return &___capacity_12; }
	inline void set_capacity_12(Text_t1901882714 * value)
	{
		___capacity_12 = value;
		Il2CppCodeGenWriteBarrier((&___capacity_12), value);
	}

	inline static int32_t get_offset_of_time_13() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___time_13)); }
	inline Text_t1901882714 * get_time_13() const { return ___time_13; }
	inline Text_t1901882714 ** get_address_of_time_13() { return &___time_13; }
	inline void set_time_13(Text_t1901882714 * value)
	{
		___time_13 = value;
		Il2CppCodeGenWriteBarrier((&___time_13), value);
	}

	inline static int32_t get_offset_of_foodNeeded_14() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___foodNeeded_14)); }
	inline Text_t1901882714 * get_foodNeeded_14() const { return ___foodNeeded_14; }
	inline Text_t1901882714 ** get_address_of_foodNeeded_14() { return &___foodNeeded_14; }
	inline void set_foodNeeded_14(Text_t1901882714 * value)
	{
		___foodNeeded_14 = value;
		Il2CppCodeGenWriteBarrier((&___foodNeeded_14), value);
	}

	inline static int32_t get_offset_of_woodNeeded_15() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___woodNeeded_15)); }
	inline Text_t1901882714 * get_woodNeeded_15() const { return ___woodNeeded_15; }
	inline Text_t1901882714 ** get_address_of_woodNeeded_15() { return &___woodNeeded_15; }
	inline void set_woodNeeded_15(Text_t1901882714 * value)
	{
		___woodNeeded_15 = value;
		Il2CppCodeGenWriteBarrier((&___woodNeeded_15), value);
	}

	inline static int32_t get_offset_of_stoneNeeded_16() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___stoneNeeded_16)); }
	inline Text_t1901882714 * get_stoneNeeded_16() const { return ___stoneNeeded_16; }
	inline Text_t1901882714 ** get_address_of_stoneNeeded_16() { return &___stoneNeeded_16; }
	inline void set_stoneNeeded_16(Text_t1901882714 * value)
	{
		___stoneNeeded_16 = value;
		Il2CppCodeGenWriteBarrier((&___stoneNeeded_16), value);
	}

	inline static int32_t get_offset_of_ironNeeded_17() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___ironNeeded_17)); }
	inline Text_t1901882714 * get_ironNeeded_17() const { return ___ironNeeded_17; }
	inline Text_t1901882714 ** get_address_of_ironNeeded_17() { return &___ironNeeded_17; }
	inline void set_ironNeeded_17(Text_t1901882714 * value)
	{
		___ironNeeded_17 = value;
		Il2CppCodeGenWriteBarrier((&___ironNeeded_17), value);
	}

	inline static int32_t get_offset_of_copperNeeded_18() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___copperNeeded_18)); }
	inline Text_t1901882714 * get_copperNeeded_18() const { return ___copperNeeded_18; }
	inline Text_t1901882714 ** get_address_of_copperNeeded_18() { return &___copperNeeded_18; }
	inline void set_copperNeeded_18(Text_t1901882714 * value)
	{
		___copperNeeded_18 = value;
		Il2CppCodeGenWriteBarrier((&___copperNeeded_18), value);
	}

	inline static int32_t get_offset_of_silverNeeded_19() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___silverNeeded_19)); }
	inline Text_t1901882714 * get_silverNeeded_19() const { return ___silverNeeded_19; }
	inline Text_t1901882714 ** get_address_of_silverNeeded_19() { return &___silverNeeded_19; }
	inline void set_silverNeeded_19(Text_t1901882714 * value)
	{
		___silverNeeded_19 = value;
		Il2CppCodeGenWriteBarrier((&___silverNeeded_19), value);
	}

	inline static int32_t get_offset_of_goldNeeded_20() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___goldNeeded_20)); }
	inline Text_t1901882714 * get_goldNeeded_20() const { return ___goldNeeded_20; }
	inline Text_t1901882714 ** get_address_of_goldNeeded_20() { return &___goldNeeded_20; }
	inline void set_goldNeeded_20(Text_t1901882714 * value)
	{
		___goldNeeded_20 = value;
		Il2CppCodeGenWriteBarrier((&___goldNeeded_20), value);
	}

	inline static int32_t get_offset_of_foodPresent_21() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___foodPresent_21)); }
	inline Text_t1901882714 * get_foodPresent_21() const { return ___foodPresent_21; }
	inline Text_t1901882714 ** get_address_of_foodPresent_21() { return &___foodPresent_21; }
	inline void set_foodPresent_21(Text_t1901882714 * value)
	{
		___foodPresent_21 = value;
		Il2CppCodeGenWriteBarrier((&___foodPresent_21), value);
	}

	inline static int32_t get_offset_of_woodPresent_22() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___woodPresent_22)); }
	inline Text_t1901882714 * get_woodPresent_22() const { return ___woodPresent_22; }
	inline Text_t1901882714 ** get_address_of_woodPresent_22() { return &___woodPresent_22; }
	inline void set_woodPresent_22(Text_t1901882714 * value)
	{
		___woodPresent_22 = value;
		Il2CppCodeGenWriteBarrier((&___woodPresent_22), value);
	}

	inline static int32_t get_offset_of_stonePresent_23() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___stonePresent_23)); }
	inline Text_t1901882714 * get_stonePresent_23() const { return ___stonePresent_23; }
	inline Text_t1901882714 ** get_address_of_stonePresent_23() { return &___stonePresent_23; }
	inline void set_stonePresent_23(Text_t1901882714 * value)
	{
		___stonePresent_23 = value;
		Il2CppCodeGenWriteBarrier((&___stonePresent_23), value);
	}

	inline static int32_t get_offset_of_ironPresent_24() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___ironPresent_24)); }
	inline Text_t1901882714 * get_ironPresent_24() const { return ___ironPresent_24; }
	inline Text_t1901882714 ** get_address_of_ironPresent_24() { return &___ironPresent_24; }
	inline void set_ironPresent_24(Text_t1901882714 * value)
	{
		___ironPresent_24 = value;
		Il2CppCodeGenWriteBarrier((&___ironPresent_24), value);
	}

	inline static int32_t get_offset_of_copperPresent_25() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___copperPresent_25)); }
	inline Text_t1901882714 * get_copperPresent_25() const { return ___copperPresent_25; }
	inline Text_t1901882714 ** get_address_of_copperPresent_25() { return &___copperPresent_25; }
	inline void set_copperPresent_25(Text_t1901882714 * value)
	{
		___copperPresent_25 = value;
		Il2CppCodeGenWriteBarrier((&___copperPresent_25), value);
	}

	inline static int32_t get_offset_of_silverPresent_26() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___silverPresent_26)); }
	inline Text_t1901882714 * get_silverPresent_26() const { return ___silverPresent_26; }
	inline Text_t1901882714 ** get_address_of_silverPresent_26() { return &___silverPresent_26; }
	inline void set_silverPresent_26(Text_t1901882714 * value)
	{
		___silverPresent_26 = value;
		Il2CppCodeGenWriteBarrier((&___silverPresent_26), value);
	}

	inline static int32_t get_offset_of_goldPresent_27() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___goldPresent_27)); }
	inline Text_t1901882714 * get_goldPresent_27() const { return ___goldPresent_27; }
	inline Text_t1901882714 ** get_address_of_goldPresent_27() { return &___goldPresent_27; }
	inline void set_goldPresent_27(Text_t1901882714 * value)
	{
		___goldPresent_27 = value;
		Il2CppCodeGenWriteBarrier((&___goldPresent_27), value);
	}

	inline static int32_t get_offset_of_unitsToRecruit_28() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unitsToRecruit_28)); }
	inline InputField_t3762917431 * get_unitsToRecruit_28() const { return ___unitsToRecruit_28; }
	inline InputField_t3762917431 ** get_address_of_unitsToRecruit_28() { return &___unitsToRecruit_28; }
	inline void set_unitsToRecruit_28(InputField_t3762917431 * value)
	{
		___unitsToRecruit_28 = value;
		Il2CppCodeGenWriteBarrier((&___unitsToRecruit_28), value);
	}

	inline static int32_t get_offset_of_unitName_29() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unitName_29)); }
	inline String_t* get_unitName_29() const { return ___unitName_29; }
	inline String_t** get_address_of_unitName_29() { return &___unitName_29; }
	inline void set_unitName_29(String_t* value)
	{
		___unitName_29 = value;
		Il2CppCodeGenWriteBarrier((&___unitName_29), value);
	}

	inline static int32_t get_offset_of_unit_30() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___unit_30)); }
	inline UnitConstantModel_t3582189408 * get_unit_30() const { return ___unit_30; }
	inline UnitConstantModel_t3582189408 ** get_address_of_unit_30() { return &___unit_30; }
	inline void set_unit_30(UnitConstantModel_t3582189408 * value)
	{
		___unit_30 = value;
		Il2CppCodeGenWriteBarrier((&___unit_30), value);
	}

	inline static int32_t get_offset_of_resources_31() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022, ___resources_31)); }
	inline ResourcesModel_t2533508513 * get_resources_31() const { return ___resources_31; }
	inline ResourcesModel_t2533508513 ** get_address_of_resources_31() { return &___resources_31; }
	inline void set_resources_31(ResourcesModel_t2533508513 * value)
	{
		___resources_31 = value;
		Il2CppCodeGenWriteBarrier((&___resources_31), value);
	}
};

struct UnitDetailsManager_t3163445022_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnitDetailsManager::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_32() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t3163445022_StaticFields, ___U3CU3Ef__switchU24map7_32)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_32() const { return ___U3CU3Ef__switchU24map7_32; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_32() { return &___U3CU3Ef__switchU24map7_32; }
	inline void set_U3CU3Ef__switchU24map7_32(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITDETAILSMANAGER_T3163445022_H
#ifndef DIPLOMACYCONTENTMANAGER_T1332674241_H
#define DIPLOMACYCONTENTMANAGER_T1332674241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DiplomacyContentManager
struct  DiplomacyContentManager_t1332674241  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle DiplomacyContentManager::allowTroopsTGL
	Toggle_t2735377061 * ___allowTroopsTGL_2;

public:
	inline static int32_t get_offset_of_allowTroopsTGL_2() { return static_cast<int32_t>(offsetof(DiplomacyContentManager_t1332674241, ___allowTroopsTGL_2)); }
	inline Toggle_t2735377061 * get_allowTroopsTGL_2() const { return ___allowTroopsTGL_2; }
	inline Toggle_t2735377061 ** get_address_of_allowTroopsTGL_2() { return &___allowTroopsTGL_2; }
	inline void set_allowTroopsTGL_2(Toggle_t2735377061 * value)
	{
		___allowTroopsTGL_2 = value;
		Il2CppCodeGenWriteBarrier((&___allowTroopsTGL_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIPLOMACYCONTENTMANAGER_T1332674241_H
#ifndef DISMISSCONTENTMANAGER_T3416937869_H
#define DISMISSCONTENTMANAGER_T3416937869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DismissContentManager
struct  DismissContentManager_t3416937869  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DismissContentManager::cityWorker
	Text_t1901882714 * ___cityWorker_2;
	// UnityEngine.UI.Text DismissContentManager::citySpy
	Text_t1901882714 * ___citySpy_3;
	// UnityEngine.UI.Text DismissContentManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_4;
	// UnityEngine.UI.Text DismissContentManager::citySpearman
	Text_t1901882714 * ___citySpearman_5;
	// UnityEngine.UI.Text DismissContentManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_6;
	// UnityEngine.UI.Text DismissContentManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_7;
	// UnityEngine.UI.Text DismissContentManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_8;
	// UnityEngine.UI.Text DismissContentManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_9;
	// UnityEngine.UI.Text DismissContentManager::cityArcher
	Text_t1901882714 * ___cityArcher_10;
	// UnityEngine.UI.Text DismissContentManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_11;
	// UnityEngine.UI.Text DismissContentManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_12;
	// UnityEngine.UI.Text DismissContentManager::cityWagon
	Text_t1901882714 * ___cityWagon_13;
	// UnityEngine.UI.Text DismissContentManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_14;
	// UnityEngine.UI.Text DismissContentManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_15;
	// UnityEngine.UI.Text DismissContentManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_16;
	// UnityEngine.UI.Text DismissContentManager::cityBallista
	Text_t1901882714 * ___cityBallista_17;
	// UnityEngine.UI.InputField DismissContentManager::dismissWorker
	InputField_t3762917431 * ___dismissWorker_18;
	// UnityEngine.UI.InputField DismissContentManager::dismissSpy
	InputField_t3762917431 * ___dismissSpy_19;
	// UnityEngine.UI.InputField DismissContentManager::dismissSwordsman
	InputField_t3762917431 * ___dismissSwordsman_20;
	// UnityEngine.UI.InputField DismissContentManager::dismissSpearman
	InputField_t3762917431 * ___dismissSpearman_21;
	// UnityEngine.UI.InputField DismissContentManager::dismissPikeman
	InputField_t3762917431 * ___dismissPikeman_22;
	// UnityEngine.UI.InputField DismissContentManager::dismissScoutRider
	InputField_t3762917431 * ___dismissScoutRider_23;
	// UnityEngine.UI.InputField DismissContentManager::dismissLightCavalry
	InputField_t3762917431 * ___dismissLightCavalry_24;
	// UnityEngine.UI.InputField DismissContentManager::dismissHeavyCavalry
	InputField_t3762917431 * ___dismissHeavyCavalry_25;
	// UnityEngine.UI.InputField DismissContentManager::dismissArcher
	InputField_t3762917431 * ___dismissArcher_26;
	// UnityEngine.UI.InputField DismissContentManager::dismissArcherRider
	InputField_t3762917431 * ___dismissArcherRider_27;
	// UnityEngine.UI.InputField DismissContentManager::dismissHollyman
	InputField_t3762917431 * ___dismissHollyman_28;
	// UnityEngine.UI.InputField DismissContentManager::dismissWagon
	InputField_t3762917431 * ___dismissWagon_29;
	// UnityEngine.UI.InputField DismissContentManager::dismissTrebuchet
	InputField_t3762917431 * ___dismissTrebuchet_30;
	// UnityEngine.UI.InputField DismissContentManager::dismissSiegeTower
	InputField_t3762917431 * ___dismissSiegeTower_31;
	// UnityEngine.UI.InputField DismissContentManager::dismissBatteringRam
	InputField_t3762917431 * ___dismissBatteringRam_32;
	// UnityEngine.UI.InputField DismissContentManager::dismissBallista
	InputField_t3762917431 * ___dismissBallista_33;
	// UnitsModel DismissContentManager::units
	UnitsModel_t3847956398 * ___units_34;

public:
	inline static int32_t get_offset_of_cityWorker_2() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityWorker_2)); }
	inline Text_t1901882714 * get_cityWorker_2() const { return ___cityWorker_2; }
	inline Text_t1901882714 ** get_address_of_cityWorker_2() { return &___cityWorker_2; }
	inline void set_cityWorker_2(Text_t1901882714 * value)
	{
		___cityWorker_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_2), value);
	}

	inline static int32_t get_offset_of_citySpy_3() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___citySpy_3)); }
	inline Text_t1901882714 * get_citySpy_3() const { return ___citySpy_3; }
	inline Text_t1901882714 ** get_address_of_citySpy_3() { return &___citySpy_3; }
	inline void set_citySpy_3(Text_t1901882714 * value)
	{
		___citySpy_3 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_3), value);
	}

	inline static int32_t get_offset_of_citySwordsman_4() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___citySwordsman_4)); }
	inline Text_t1901882714 * get_citySwordsman_4() const { return ___citySwordsman_4; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_4() { return &___citySwordsman_4; }
	inline void set_citySwordsman_4(Text_t1901882714 * value)
	{
		___citySwordsman_4 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_4), value);
	}

	inline static int32_t get_offset_of_citySpearman_5() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___citySpearman_5)); }
	inline Text_t1901882714 * get_citySpearman_5() const { return ___citySpearman_5; }
	inline Text_t1901882714 ** get_address_of_citySpearman_5() { return &___citySpearman_5; }
	inline void set_citySpearman_5(Text_t1901882714 * value)
	{
		___citySpearman_5 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_5), value);
	}

	inline static int32_t get_offset_of_cityPikeman_6() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityPikeman_6)); }
	inline Text_t1901882714 * get_cityPikeman_6() const { return ___cityPikeman_6; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_6() { return &___cityPikeman_6; }
	inline void set_cityPikeman_6(Text_t1901882714 * value)
	{
		___cityPikeman_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_6), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_7() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityScoutRider_7)); }
	inline Text_t1901882714 * get_cityScoutRider_7() const { return ___cityScoutRider_7; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_7() { return &___cityScoutRider_7; }
	inline void set_cityScoutRider_7(Text_t1901882714 * value)
	{
		___cityScoutRider_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_7), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_8() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityLightCavalry_8)); }
	inline Text_t1901882714 * get_cityLightCavalry_8() const { return ___cityLightCavalry_8; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_8() { return &___cityLightCavalry_8; }
	inline void set_cityLightCavalry_8(Text_t1901882714 * value)
	{
		___cityLightCavalry_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_8), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_9() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityHeavyCavalry_9)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_9() const { return ___cityHeavyCavalry_9; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_9() { return &___cityHeavyCavalry_9; }
	inline void set_cityHeavyCavalry_9(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_9), value);
	}

	inline static int32_t get_offset_of_cityArcher_10() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityArcher_10)); }
	inline Text_t1901882714 * get_cityArcher_10() const { return ___cityArcher_10; }
	inline Text_t1901882714 ** get_address_of_cityArcher_10() { return &___cityArcher_10; }
	inline void set_cityArcher_10(Text_t1901882714 * value)
	{
		___cityArcher_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_10), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_11() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityArcherRider_11)); }
	inline Text_t1901882714 * get_cityArcherRider_11() const { return ___cityArcherRider_11; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_11() { return &___cityArcherRider_11; }
	inline void set_cityArcherRider_11(Text_t1901882714 * value)
	{
		___cityArcherRider_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_11), value);
	}

	inline static int32_t get_offset_of_cityHollyman_12() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityHollyman_12)); }
	inline Text_t1901882714 * get_cityHollyman_12() const { return ___cityHollyman_12; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_12() { return &___cityHollyman_12; }
	inline void set_cityHollyman_12(Text_t1901882714 * value)
	{
		___cityHollyman_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_12), value);
	}

	inline static int32_t get_offset_of_cityWagon_13() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityWagon_13)); }
	inline Text_t1901882714 * get_cityWagon_13() const { return ___cityWagon_13; }
	inline Text_t1901882714 ** get_address_of_cityWagon_13() { return &___cityWagon_13; }
	inline void set_cityWagon_13(Text_t1901882714 * value)
	{
		___cityWagon_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_13), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_14() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityTrebuchet_14)); }
	inline Text_t1901882714 * get_cityTrebuchet_14() const { return ___cityTrebuchet_14; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_14() { return &___cityTrebuchet_14; }
	inline void set_cityTrebuchet_14(Text_t1901882714 * value)
	{
		___cityTrebuchet_14 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_14), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_15() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___citySiegeTower_15)); }
	inline Text_t1901882714 * get_citySiegeTower_15() const { return ___citySiegeTower_15; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_15() { return &___citySiegeTower_15; }
	inline void set_citySiegeTower_15(Text_t1901882714 * value)
	{
		___citySiegeTower_15 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_15), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_16() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityBatteringRam_16)); }
	inline Text_t1901882714 * get_cityBatteringRam_16() const { return ___cityBatteringRam_16; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_16() { return &___cityBatteringRam_16; }
	inline void set_cityBatteringRam_16(Text_t1901882714 * value)
	{
		___cityBatteringRam_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_16), value);
	}

	inline static int32_t get_offset_of_cityBallista_17() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___cityBallista_17)); }
	inline Text_t1901882714 * get_cityBallista_17() const { return ___cityBallista_17; }
	inline Text_t1901882714 ** get_address_of_cityBallista_17() { return &___cityBallista_17; }
	inline void set_cityBallista_17(Text_t1901882714 * value)
	{
		___cityBallista_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_17), value);
	}

	inline static int32_t get_offset_of_dismissWorker_18() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissWorker_18)); }
	inline InputField_t3762917431 * get_dismissWorker_18() const { return ___dismissWorker_18; }
	inline InputField_t3762917431 ** get_address_of_dismissWorker_18() { return &___dismissWorker_18; }
	inline void set_dismissWorker_18(InputField_t3762917431 * value)
	{
		___dismissWorker_18 = value;
		Il2CppCodeGenWriteBarrier((&___dismissWorker_18), value);
	}

	inline static int32_t get_offset_of_dismissSpy_19() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissSpy_19)); }
	inline InputField_t3762917431 * get_dismissSpy_19() const { return ___dismissSpy_19; }
	inline InputField_t3762917431 ** get_address_of_dismissSpy_19() { return &___dismissSpy_19; }
	inline void set_dismissSpy_19(InputField_t3762917431 * value)
	{
		___dismissSpy_19 = value;
		Il2CppCodeGenWriteBarrier((&___dismissSpy_19), value);
	}

	inline static int32_t get_offset_of_dismissSwordsman_20() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissSwordsman_20)); }
	inline InputField_t3762917431 * get_dismissSwordsman_20() const { return ___dismissSwordsman_20; }
	inline InputField_t3762917431 ** get_address_of_dismissSwordsman_20() { return &___dismissSwordsman_20; }
	inline void set_dismissSwordsman_20(InputField_t3762917431 * value)
	{
		___dismissSwordsman_20 = value;
		Il2CppCodeGenWriteBarrier((&___dismissSwordsman_20), value);
	}

	inline static int32_t get_offset_of_dismissSpearman_21() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissSpearman_21)); }
	inline InputField_t3762917431 * get_dismissSpearman_21() const { return ___dismissSpearman_21; }
	inline InputField_t3762917431 ** get_address_of_dismissSpearman_21() { return &___dismissSpearman_21; }
	inline void set_dismissSpearman_21(InputField_t3762917431 * value)
	{
		___dismissSpearman_21 = value;
		Il2CppCodeGenWriteBarrier((&___dismissSpearman_21), value);
	}

	inline static int32_t get_offset_of_dismissPikeman_22() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissPikeman_22)); }
	inline InputField_t3762917431 * get_dismissPikeman_22() const { return ___dismissPikeman_22; }
	inline InputField_t3762917431 ** get_address_of_dismissPikeman_22() { return &___dismissPikeman_22; }
	inline void set_dismissPikeman_22(InputField_t3762917431 * value)
	{
		___dismissPikeman_22 = value;
		Il2CppCodeGenWriteBarrier((&___dismissPikeman_22), value);
	}

	inline static int32_t get_offset_of_dismissScoutRider_23() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissScoutRider_23)); }
	inline InputField_t3762917431 * get_dismissScoutRider_23() const { return ___dismissScoutRider_23; }
	inline InputField_t3762917431 ** get_address_of_dismissScoutRider_23() { return &___dismissScoutRider_23; }
	inline void set_dismissScoutRider_23(InputField_t3762917431 * value)
	{
		___dismissScoutRider_23 = value;
		Il2CppCodeGenWriteBarrier((&___dismissScoutRider_23), value);
	}

	inline static int32_t get_offset_of_dismissLightCavalry_24() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissLightCavalry_24)); }
	inline InputField_t3762917431 * get_dismissLightCavalry_24() const { return ___dismissLightCavalry_24; }
	inline InputField_t3762917431 ** get_address_of_dismissLightCavalry_24() { return &___dismissLightCavalry_24; }
	inline void set_dismissLightCavalry_24(InputField_t3762917431 * value)
	{
		___dismissLightCavalry_24 = value;
		Il2CppCodeGenWriteBarrier((&___dismissLightCavalry_24), value);
	}

	inline static int32_t get_offset_of_dismissHeavyCavalry_25() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissHeavyCavalry_25)); }
	inline InputField_t3762917431 * get_dismissHeavyCavalry_25() const { return ___dismissHeavyCavalry_25; }
	inline InputField_t3762917431 ** get_address_of_dismissHeavyCavalry_25() { return &___dismissHeavyCavalry_25; }
	inline void set_dismissHeavyCavalry_25(InputField_t3762917431 * value)
	{
		___dismissHeavyCavalry_25 = value;
		Il2CppCodeGenWriteBarrier((&___dismissHeavyCavalry_25), value);
	}

	inline static int32_t get_offset_of_dismissArcher_26() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissArcher_26)); }
	inline InputField_t3762917431 * get_dismissArcher_26() const { return ___dismissArcher_26; }
	inline InputField_t3762917431 ** get_address_of_dismissArcher_26() { return &___dismissArcher_26; }
	inline void set_dismissArcher_26(InputField_t3762917431 * value)
	{
		___dismissArcher_26 = value;
		Il2CppCodeGenWriteBarrier((&___dismissArcher_26), value);
	}

	inline static int32_t get_offset_of_dismissArcherRider_27() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissArcherRider_27)); }
	inline InputField_t3762917431 * get_dismissArcherRider_27() const { return ___dismissArcherRider_27; }
	inline InputField_t3762917431 ** get_address_of_dismissArcherRider_27() { return &___dismissArcherRider_27; }
	inline void set_dismissArcherRider_27(InputField_t3762917431 * value)
	{
		___dismissArcherRider_27 = value;
		Il2CppCodeGenWriteBarrier((&___dismissArcherRider_27), value);
	}

	inline static int32_t get_offset_of_dismissHollyman_28() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissHollyman_28)); }
	inline InputField_t3762917431 * get_dismissHollyman_28() const { return ___dismissHollyman_28; }
	inline InputField_t3762917431 ** get_address_of_dismissHollyman_28() { return &___dismissHollyman_28; }
	inline void set_dismissHollyman_28(InputField_t3762917431 * value)
	{
		___dismissHollyman_28 = value;
		Il2CppCodeGenWriteBarrier((&___dismissHollyman_28), value);
	}

	inline static int32_t get_offset_of_dismissWagon_29() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissWagon_29)); }
	inline InputField_t3762917431 * get_dismissWagon_29() const { return ___dismissWagon_29; }
	inline InputField_t3762917431 ** get_address_of_dismissWagon_29() { return &___dismissWagon_29; }
	inline void set_dismissWagon_29(InputField_t3762917431 * value)
	{
		___dismissWagon_29 = value;
		Il2CppCodeGenWriteBarrier((&___dismissWagon_29), value);
	}

	inline static int32_t get_offset_of_dismissTrebuchet_30() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissTrebuchet_30)); }
	inline InputField_t3762917431 * get_dismissTrebuchet_30() const { return ___dismissTrebuchet_30; }
	inline InputField_t3762917431 ** get_address_of_dismissTrebuchet_30() { return &___dismissTrebuchet_30; }
	inline void set_dismissTrebuchet_30(InputField_t3762917431 * value)
	{
		___dismissTrebuchet_30 = value;
		Il2CppCodeGenWriteBarrier((&___dismissTrebuchet_30), value);
	}

	inline static int32_t get_offset_of_dismissSiegeTower_31() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissSiegeTower_31)); }
	inline InputField_t3762917431 * get_dismissSiegeTower_31() const { return ___dismissSiegeTower_31; }
	inline InputField_t3762917431 ** get_address_of_dismissSiegeTower_31() { return &___dismissSiegeTower_31; }
	inline void set_dismissSiegeTower_31(InputField_t3762917431 * value)
	{
		___dismissSiegeTower_31 = value;
		Il2CppCodeGenWriteBarrier((&___dismissSiegeTower_31), value);
	}

	inline static int32_t get_offset_of_dismissBatteringRam_32() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissBatteringRam_32)); }
	inline InputField_t3762917431 * get_dismissBatteringRam_32() const { return ___dismissBatteringRam_32; }
	inline InputField_t3762917431 ** get_address_of_dismissBatteringRam_32() { return &___dismissBatteringRam_32; }
	inline void set_dismissBatteringRam_32(InputField_t3762917431 * value)
	{
		___dismissBatteringRam_32 = value;
		Il2CppCodeGenWriteBarrier((&___dismissBatteringRam_32), value);
	}

	inline static int32_t get_offset_of_dismissBallista_33() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___dismissBallista_33)); }
	inline InputField_t3762917431 * get_dismissBallista_33() const { return ___dismissBallista_33; }
	inline InputField_t3762917431 ** get_address_of_dismissBallista_33() { return &___dismissBallista_33; }
	inline void set_dismissBallista_33(InputField_t3762917431 * value)
	{
		___dismissBallista_33 = value;
		Il2CppCodeGenWriteBarrier((&___dismissBallista_33), value);
	}

	inline static int32_t get_offset_of_units_34() { return static_cast<int32_t>(offsetof(DismissContentManager_t3416937869, ___units_34)); }
	inline UnitsModel_t3847956398 * get_units_34() const { return ___units_34; }
	inline UnitsModel_t3847956398 ** get_address_of_units_34() { return &___units_34; }
	inline void set_units_34(UnitsModel_t3847956398 * value)
	{
		___units_34 = value;
		Il2CppCodeGenWriteBarrier((&___units_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISMISSCONTENTMANAGER_T3416937869_H
#ifndef CONVERTREPORTMANAGER_T1530780108_H
#define CONVERTREPORTMANAGER_T1530780108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConvertReportManager
struct  ConvertReportManager_t1530780108  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ConvertReportManager::attackerName
	Text_t1901882714 * ___attackerName_2;
	// UnityEngine.UI.Image ConvertReportManager::attackerImage
	Image_t2670269651 * ___attackerImage_3;
	// UnityEngine.UI.Text ConvertReportManager::attackerRank
	Text_t1901882714 * ___attackerRank_4;
	// UnityEngine.UI.Text ConvertReportManager::attackerAllianceName
	Text_t1901882714 * ___attackerAllianceName_5;
	// UnityEngine.UI.Text ConvertReportManager::attackerAlliancePosition
	Text_t1901882714 * ___attackerAlliancePosition_6;
	// UnityEngine.UI.Text ConvertReportManager::attackerCityName
	Text_t1901882714 * ___attackerCityName_7;
	// UnityEngine.UI.Image ConvertReportManager::attackerCityImage
	Image_t2670269651 * ___attackerCityImage_8;
	// UnityEngine.UI.Text ConvertReportManager::attackerExperiance
	Text_t1901882714 * ___attackerExperiance_9;
	// UnityEngine.UI.Text ConvertReportManager::attackerCityRegion
	Text_t1901882714 * ___attackerCityRegion_10;
	// UnityEngine.UI.Text ConvertReportManager::defenderName
	Text_t1901882714 * ___defenderName_11;
	// UnityEngine.UI.Image ConvertReportManager::defenderImage
	Image_t2670269651 * ___defenderImage_12;
	// UnityEngine.UI.Text ConvertReportManager::defenderRank
	Text_t1901882714 * ___defenderRank_13;
	// UnityEngine.UI.Text ConvertReportManager::defenderAllianceName
	Text_t1901882714 * ___defenderAllianceName_14;
	// UnityEngine.UI.Text ConvertReportManager::defenderAlliancePosition
	Text_t1901882714 * ___defenderAlliancePosition_15;
	// UnityEngine.UI.Text ConvertReportManager::defenderCityName
	Text_t1901882714 * ___defenderCityName_16;
	// UnityEngine.UI.Image ConvertReportManager::defenderCityImage
	Image_t2670269651 * ___defenderCityImage_17;
	// UnityEngine.UI.Text ConvertReportManager::defenderExperiance
	Text_t1901882714 * ___defenderExperiance_18;
	// UnityEngine.UI.Text ConvertReportManager::defenderCityRegion
	Text_t1901882714 * ___defenderCityRegion_19;
	// UnityEngine.UI.Text ConvertReportManager::defenderCityHappiness
	Text_t1901882714 * ___defenderCityHappiness_20;
	// UnityEngine.UI.Text ConvertReportManager::defenderCityCourage
	Text_t1901882714 * ___defenderCityCourage_21;
	// UnityEngine.UI.Text ConvertReportManager::attackerResult
	Text_t1901882714 * ___attackerResult_22;
	// UnityEngine.UI.Text ConvertReportManager::defenderResult
	Text_t1901882714 * ___defenderResult_23;
	// BattleReportModel ConvertReportManager::report
	BattleReportModel_t4250794538 * ___report_24;
	// BattleReportUnitTableController ConvertReportManager::attackerUnitsController
	BattleReportUnitTableController_t1840302685 * ___attackerUnitsController_25;
	// BattleReportUnitTableController ConvertReportManager::defenderUnitsController
	BattleReportUnitTableController_t1840302685 * ___defenderUnitsController_26;
	// System.Collections.Generic.List`1<System.String> ConvertReportManager::attackerUnitNames
	List_1_t3319525431 * ___attackerUnitNames_27;
	// System.Collections.Generic.List`1<System.Int64> ConvertReportManager::attackerBeforeCount
	List_1_t913674750 * ___attackerBeforeCount_28;
	// System.Collections.Generic.List`1<System.Int64> ConvertReportManager::attackerAfterCount
	List_1_t913674750 * ___attackerAfterCount_29;
	// System.Collections.Generic.List`1<System.String> ConvertReportManager::defenderUnitNames
	List_1_t3319525431 * ___defenderUnitNames_30;
	// System.Collections.Generic.List`1<System.Int64> ConvertReportManager::defenderBeforeCount
	List_1_t913674750 * ___defenderBeforeCount_31;
	// System.Collections.Generic.List`1<System.Int64> ConvertReportManager::defenderAfterCount
	List_1_t913674750 * ___defenderAfterCount_32;

public:
	inline static int32_t get_offset_of_attackerName_2() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerName_2)); }
	inline Text_t1901882714 * get_attackerName_2() const { return ___attackerName_2; }
	inline Text_t1901882714 ** get_address_of_attackerName_2() { return &___attackerName_2; }
	inline void set_attackerName_2(Text_t1901882714 * value)
	{
		___attackerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___attackerName_2), value);
	}

	inline static int32_t get_offset_of_attackerImage_3() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerImage_3)); }
	inline Image_t2670269651 * get_attackerImage_3() const { return ___attackerImage_3; }
	inline Image_t2670269651 ** get_address_of_attackerImage_3() { return &___attackerImage_3; }
	inline void set_attackerImage_3(Image_t2670269651 * value)
	{
		___attackerImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___attackerImage_3), value);
	}

	inline static int32_t get_offset_of_attackerRank_4() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerRank_4)); }
	inline Text_t1901882714 * get_attackerRank_4() const { return ___attackerRank_4; }
	inline Text_t1901882714 ** get_address_of_attackerRank_4() { return &___attackerRank_4; }
	inline void set_attackerRank_4(Text_t1901882714 * value)
	{
		___attackerRank_4 = value;
		Il2CppCodeGenWriteBarrier((&___attackerRank_4), value);
	}

	inline static int32_t get_offset_of_attackerAllianceName_5() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerAllianceName_5)); }
	inline Text_t1901882714 * get_attackerAllianceName_5() const { return ___attackerAllianceName_5; }
	inline Text_t1901882714 ** get_address_of_attackerAllianceName_5() { return &___attackerAllianceName_5; }
	inline void set_attackerAllianceName_5(Text_t1901882714 * value)
	{
		___attackerAllianceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAllianceName_5), value);
	}

	inline static int32_t get_offset_of_attackerAlliancePosition_6() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerAlliancePosition_6)); }
	inline Text_t1901882714 * get_attackerAlliancePosition_6() const { return ___attackerAlliancePosition_6; }
	inline Text_t1901882714 ** get_address_of_attackerAlliancePosition_6() { return &___attackerAlliancePosition_6; }
	inline void set_attackerAlliancePosition_6(Text_t1901882714 * value)
	{
		___attackerAlliancePosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAlliancePosition_6), value);
	}

	inline static int32_t get_offset_of_attackerCityName_7() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerCityName_7)); }
	inline Text_t1901882714 * get_attackerCityName_7() const { return ___attackerCityName_7; }
	inline Text_t1901882714 ** get_address_of_attackerCityName_7() { return &___attackerCityName_7; }
	inline void set_attackerCityName_7(Text_t1901882714 * value)
	{
		___attackerCityName_7 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityName_7), value);
	}

	inline static int32_t get_offset_of_attackerCityImage_8() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerCityImage_8)); }
	inline Image_t2670269651 * get_attackerCityImage_8() const { return ___attackerCityImage_8; }
	inline Image_t2670269651 ** get_address_of_attackerCityImage_8() { return &___attackerCityImage_8; }
	inline void set_attackerCityImage_8(Image_t2670269651 * value)
	{
		___attackerCityImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityImage_8), value);
	}

	inline static int32_t get_offset_of_attackerExperiance_9() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerExperiance_9)); }
	inline Text_t1901882714 * get_attackerExperiance_9() const { return ___attackerExperiance_9; }
	inline Text_t1901882714 ** get_address_of_attackerExperiance_9() { return &___attackerExperiance_9; }
	inline void set_attackerExperiance_9(Text_t1901882714 * value)
	{
		___attackerExperiance_9 = value;
		Il2CppCodeGenWriteBarrier((&___attackerExperiance_9), value);
	}

	inline static int32_t get_offset_of_attackerCityRegion_10() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerCityRegion_10)); }
	inline Text_t1901882714 * get_attackerCityRegion_10() const { return ___attackerCityRegion_10; }
	inline Text_t1901882714 ** get_address_of_attackerCityRegion_10() { return &___attackerCityRegion_10; }
	inline void set_attackerCityRegion_10(Text_t1901882714 * value)
	{
		___attackerCityRegion_10 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityRegion_10), value);
	}

	inline static int32_t get_offset_of_defenderName_11() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderName_11)); }
	inline Text_t1901882714 * get_defenderName_11() const { return ___defenderName_11; }
	inline Text_t1901882714 ** get_address_of_defenderName_11() { return &___defenderName_11; }
	inline void set_defenderName_11(Text_t1901882714 * value)
	{
		___defenderName_11 = value;
		Il2CppCodeGenWriteBarrier((&___defenderName_11), value);
	}

	inline static int32_t get_offset_of_defenderImage_12() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderImage_12)); }
	inline Image_t2670269651 * get_defenderImage_12() const { return ___defenderImage_12; }
	inline Image_t2670269651 ** get_address_of_defenderImage_12() { return &___defenderImage_12; }
	inline void set_defenderImage_12(Image_t2670269651 * value)
	{
		___defenderImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___defenderImage_12), value);
	}

	inline static int32_t get_offset_of_defenderRank_13() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderRank_13)); }
	inline Text_t1901882714 * get_defenderRank_13() const { return ___defenderRank_13; }
	inline Text_t1901882714 ** get_address_of_defenderRank_13() { return &___defenderRank_13; }
	inline void set_defenderRank_13(Text_t1901882714 * value)
	{
		___defenderRank_13 = value;
		Il2CppCodeGenWriteBarrier((&___defenderRank_13), value);
	}

	inline static int32_t get_offset_of_defenderAllianceName_14() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderAllianceName_14)); }
	inline Text_t1901882714 * get_defenderAllianceName_14() const { return ___defenderAllianceName_14; }
	inline Text_t1901882714 ** get_address_of_defenderAllianceName_14() { return &___defenderAllianceName_14; }
	inline void set_defenderAllianceName_14(Text_t1901882714 * value)
	{
		___defenderAllianceName_14 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAllianceName_14), value);
	}

	inline static int32_t get_offset_of_defenderAlliancePosition_15() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderAlliancePosition_15)); }
	inline Text_t1901882714 * get_defenderAlliancePosition_15() const { return ___defenderAlliancePosition_15; }
	inline Text_t1901882714 ** get_address_of_defenderAlliancePosition_15() { return &___defenderAlliancePosition_15; }
	inline void set_defenderAlliancePosition_15(Text_t1901882714 * value)
	{
		___defenderAlliancePosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAlliancePosition_15), value);
	}

	inline static int32_t get_offset_of_defenderCityName_16() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderCityName_16)); }
	inline Text_t1901882714 * get_defenderCityName_16() const { return ___defenderCityName_16; }
	inline Text_t1901882714 ** get_address_of_defenderCityName_16() { return &___defenderCityName_16; }
	inline void set_defenderCityName_16(Text_t1901882714 * value)
	{
		___defenderCityName_16 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityName_16), value);
	}

	inline static int32_t get_offset_of_defenderCityImage_17() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderCityImage_17)); }
	inline Image_t2670269651 * get_defenderCityImage_17() const { return ___defenderCityImage_17; }
	inline Image_t2670269651 ** get_address_of_defenderCityImage_17() { return &___defenderCityImage_17; }
	inline void set_defenderCityImage_17(Image_t2670269651 * value)
	{
		___defenderCityImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityImage_17), value);
	}

	inline static int32_t get_offset_of_defenderExperiance_18() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderExperiance_18)); }
	inline Text_t1901882714 * get_defenderExperiance_18() const { return ___defenderExperiance_18; }
	inline Text_t1901882714 ** get_address_of_defenderExperiance_18() { return &___defenderExperiance_18; }
	inline void set_defenderExperiance_18(Text_t1901882714 * value)
	{
		___defenderExperiance_18 = value;
		Il2CppCodeGenWriteBarrier((&___defenderExperiance_18), value);
	}

	inline static int32_t get_offset_of_defenderCityRegion_19() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderCityRegion_19)); }
	inline Text_t1901882714 * get_defenderCityRegion_19() const { return ___defenderCityRegion_19; }
	inline Text_t1901882714 ** get_address_of_defenderCityRegion_19() { return &___defenderCityRegion_19; }
	inline void set_defenderCityRegion_19(Text_t1901882714 * value)
	{
		___defenderCityRegion_19 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityRegion_19), value);
	}

	inline static int32_t get_offset_of_defenderCityHappiness_20() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderCityHappiness_20)); }
	inline Text_t1901882714 * get_defenderCityHappiness_20() const { return ___defenderCityHappiness_20; }
	inline Text_t1901882714 ** get_address_of_defenderCityHappiness_20() { return &___defenderCityHappiness_20; }
	inline void set_defenderCityHappiness_20(Text_t1901882714 * value)
	{
		___defenderCityHappiness_20 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityHappiness_20), value);
	}

	inline static int32_t get_offset_of_defenderCityCourage_21() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderCityCourage_21)); }
	inline Text_t1901882714 * get_defenderCityCourage_21() const { return ___defenderCityCourage_21; }
	inline Text_t1901882714 ** get_address_of_defenderCityCourage_21() { return &___defenderCityCourage_21; }
	inline void set_defenderCityCourage_21(Text_t1901882714 * value)
	{
		___defenderCityCourage_21 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityCourage_21), value);
	}

	inline static int32_t get_offset_of_attackerResult_22() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerResult_22)); }
	inline Text_t1901882714 * get_attackerResult_22() const { return ___attackerResult_22; }
	inline Text_t1901882714 ** get_address_of_attackerResult_22() { return &___attackerResult_22; }
	inline void set_attackerResult_22(Text_t1901882714 * value)
	{
		___attackerResult_22 = value;
		Il2CppCodeGenWriteBarrier((&___attackerResult_22), value);
	}

	inline static int32_t get_offset_of_defenderResult_23() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderResult_23)); }
	inline Text_t1901882714 * get_defenderResult_23() const { return ___defenderResult_23; }
	inline Text_t1901882714 ** get_address_of_defenderResult_23() { return &___defenderResult_23; }
	inline void set_defenderResult_23(Text_t1901882714 * value)
	{
		___defenderResult_23 = value;
		Il2CppCodeGenWriteBarrier((&___defenderResult_23), value);
	}

	inline static int32_t get_offset_of_report_24() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___report_24)); }
	inline BattleReportModel_t4250794538 * get_report_24() const { return ___report_24; }
	inline BattleReportModel_t4250794538 ** get_address_of_report_24() { return &___report_24; }
	inline void set_report_24(BattleReportModel_t4250794538 * value)
	{
		___report_24 = value;
		Il2CppCodeGenWriteBarrier((&___report_24), value);
	}

	inline static int32_t get_offset_of_attackerUnitsController_25() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerUnitsController_25)); }
	inline BattleReportUnitTableController_t1840302685 * get_attackerUnitsController_25() const { return ___attackerUnitsController_25; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_attackerUnitsController_25() { return &___attackerUnitsController_25; }
	inline void set_attackerUnitsController_25(BattleReportUnitTableController_t1840302685 * value)
	{
		___attackerUnitsController_25 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitsController_25), value);
	}

	inline static int32_t get_offset_of_defenderUnitsController_26() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderUnitsController_26)); }
	inline BattleReportUnitTableController_t1840302685 * get_defenderUnitsController_26() const { return ___defenderUnitsController_26; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_defenderUnitsController_26() { return &___defenderUnitsController_26; }
	inline void set_defenderUnitsController_26(BattleReportUnitTableController_t1840302685 * value)
	{
		___defenderUnitsController_26 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitsController_26), value);
	}

	inline static int32_t get_offset_of_attackerUnitNames_27() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerUnitNames_27)); }
	inline List_1_t3319525431 * get_attackerUnitNames_27() const { return ___attackerUnitNames_27; }
	inline List_1_t3319525431 ** get_address_of_attackerUnitNames_27() { return &___attackerUnitNames_27; }
	inline void set_attackerUnitNames_27(List_1_t3319525431 * value)
	{
		___attackerUnitNames_27 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitNames_27), value);
	}

	inline static int32_t get_offset_of_attackerBeforeCount_28() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerBeforeCount_28)); }
	inline List_1_t913674750 * get_attackerBeforeCount_28() const { return ___attackerBeforeCount_28; }
	inline List_1_t913674750 ** get_address_of_attackerBeforeCount_28() { return &___attackerBeforeCount_28; }
	inline void set_attackerBeforeCount_28(List_1_t913674750 * value)
	{
		___attackerBeforeCount_28 = value;
		Il2CppCodeGenWriteBarrier((&___attackerBeforeCount_28), value);
	}

	inline static int32_t get_offset_of_attackerAfterCount_29() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___attackerAfterCount_29)); }
	inline List_1_t913674750 * get_attackerAfterCount_29() const { return ___attackerAfterCount_29; }
	inline List_1_t913674750 ** get_address_of_attackerAfterCount_29() { return &___attackerAfterCount_29; }
	inline void set_attackerAfterCount_29(List_1_t913674750 * value)
	{
		___attackerAfterCount_29 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAfterCount_29), value);
	}

	inline static int32_t get_offset_of_defenderUnitNames_30() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderUnitNames_30)); }
	inline List_1_t3319525431 * get_defenderUnitNames_30() const { return ___defenderUnitNames_30; }
	inline List_1_t3319525431 ** get_address_of_defenderUnitNames_30() { return &___defenderUnitNames_30; }
	inline void set_defenderUnitNames_30(List_1_t3319525431 * value)
	{
		___defenderUnitNames_30 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitNames_30), value);
	}

	inline static int32_t get_offset_of_defenderBeforeCount_31() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderBeforeCount_31)); }
	inline List_1_t913674750 * get_defenderBeforeCount_31() const { return ___defenderBeforeCount_31; }
	inline List_1_t913674750 ** get_address_of_defenderBeforeCount_31() { return &___defenderBeforeCount_31; }
	inline void set_defenderBeforeCount_31(List_1_t913674750 * value)
	{
		___defenderBeforeCount_31 = value;
		Il2CppCodeGenWriteBarrier((&___defenderBeforeCount_31), value);
	}

	inline static int32_t get_offset_of_defenderAfterCount_32() { return static_cast<int32_t>(offsetof(ConvertReportManager_t1530780108, ___defenderAfterCount_32)); }
	inline List_1_t913674750 * get_defenderAfterCount_32() const { return ___defenderAfterCount_32; }
	inline List_1_t913674750 ** get_address_of_defenderAfterCount_32() { return &___defenderAfterCount_32; }
	inline void set_defenderAfterCount_32(List_1_t913674750 * value)
	{
		___defenderAfterCount_32 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAfterCount_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTREPORTMANAGER_T1530780108_H
#ifndef CONFIRMATIONCONTENTMANAGER_T3012879264_H
#define CONFIRMATIONCONTENTMANAGER_T3012879264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfirmationContentManager
struct  ConfirmationContentManager_t3012879264  : public MonoBehaviour_t3962482529
{
public:
	// ConfirmationEvent ConfirmationContentManager::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_2;
	// UnityEngine.UI.Text ConfirmationContentManager::confirmationMessage
	Text_t1901882714 * ___confirmationMessage_3;

public:
	inline static int32_t get_offset_of_confirmed_2() { return static_cast<int32_t>(offsetof(ConfirmationContentManager_t3012879264, ___confirmed_2)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_2() const { return ___confirmed_2; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_2() { return &___confirmed_2; }
	inline void set_confirmed_2(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_2 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_2), value);
	}

	inline static int32_t get_offset_of_confirmationMessage_3() { return static_cast<int32_t>(offsetof(ConfirmationContentManager_t3012879264, ___confirmationMessage_3)); }
	inline Text_t1901882714 * get_confirmationMessage_3() const { return ___confirmationMessage_3; }
	inline Text_t1901882714 ** get_address_of_confirmationMessage_3() { return &___confirmationMessage_3; }
	inline void set_confirmationMessage_3(Text_t1901882714 * value)
	{
		___confirmationMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___confirmationMessage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIRMATIONCONTENTMANAGER_T3012879264_H
#ifndef CONSTRUCTIONDETAILSMANAGER_T275842940_H
#define CONSTRUCTIONDETAILSMANAGER_T275842940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstructionDetailsManager
struct  ConstructionDetailsManager_t275842940  : public MonoBehaviour_t3962482529
{
public:
	// ConstructionPitManager ConstructionDetailsManager::pitManager
	ConstructionPitManager_t848510622 * ___pitManager_2;
	// UnityEngine.GameObject ConstructionDetailsManager::cityPanel
	GameObject_t1113636619 * ___cityPanel_3;
	// UnityEngine.GameObject ConstructionDetailsManager::fieldsPanel
	GameObject_t1113636619 * ___fieldsPanel_4;
	// UnityEngine.GameObject ConstructionDetailsManager::feastingHall
	GameObject_t1113636619 * ___feastingHall_5;
	// UnityEngine.GameObject ConstructionDetailsManager::market
	GameObject_t1113636619 * ___market_6;
	// UnityEngine.GameObject ConstructionDetailsManager::guestHouse
	GameObject_t1113636619 * ___guestHouse_7;
	// UnityEngine.GameObject ConstructionDetailsManager::storage
	GameObject_t1113636619 * ___storage_8;
	// UnityEngine.GameObject ConstructionDetailsManager::furnace
	GameObject_t1113636619 * ___furnace_9;
	// UnityEngine.GameObject ConstructionDetailsManager::blackSmith
	GameObject_t1113636619 * ___blackSmith_10;
	// UnityEngine.GameObject ConstructionDetailsManager::hospital
	GameObject_t1113636619 * ___hospital_11;
	// UnityEngine.GameObject ConstructionDetailsManager::godHouse
	GameObject_t1113636619 * ___godHouse_12;
	// UnityEngine.GameObject ConstructionDetailsManager::commandCenter
	GameObject_t1113636619 * ___commandCenter_13;
	// UnityEngine.GameObject ConstructionDetailsManager::diplomacy
	GameObject_t1113636619 * ___diplomacy_14;
	// UnityEngine.GameObject ConstructionDetailsManager::trainingGround
	GameObject_t1113636619 * ___trainingGround_15;
	// UnityEngine.GameObject ConstructionDetailsManager::sportArena
	GameObject_t1113636619 * ___sportArena_16;
	// UnityEngine.GameObject ConstructionDetailsManager::univercity
	GameObject_t1113636619 * ___univercity_17;
	// UnityEngine.GameObject ConstructionDetailsManager::militarySchool
	GameObject_t1113636619 * ___militarySchool_18;
	// UnityEngine.GameObject ConstructionDetailsManager::archery
	GameObject_t1113636619 * ___archery_19;
	// UnityEngine.GameObject ConstructionDetailsManager::stable
	GameObject_t1113636619 * ___stable_20;
	// UnityEngine.GameObject ConstructionDetailsManager::engineering
	GameObject_t1113636619 * ___engineering_21;

public:
	inline static int32_t get_offset_of_pitManager_2() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___pitManager_2)); }
	inline ConstructionPitManager_t848510622 * get_pitManager_2() const { return ___pitManager_2; }
	inline ConstructionPitManager_t848510622 ** get_address_of_pitManager_2() { return &___pitManager_2; }
	inline void set_pitManager_2(ConstructionPitManager_t848510622 * value)
	{
		___pitManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___pitManager_2), value);
	}

	inline static int32_t get_offset_of_cityPanel_3() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___cityPanel_3)); }
	inline GameObject_t1113636619 * get_cityPanel_3() const { return ___cityPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_cityPanel_3() { return &___cityPanel_3; }
	inline void set_cityPanel_3(GameObject_t1113636619 * value)
	{
		___cityPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityPanel_3), value);
	}

	inline static int32_t get_offset_of_fieldsPanel_4() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___fieldsPanel_4)); }
	inline GameObject_t1113636619 * get_fieldsPanel_4() const { return ___fieldsPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_fieldsPanel_4() { return &___fieldsPanel_4; }
	inline void set_fieldsPanel_4(GameObject_t1113636619 * value)
	{
		___fieldsPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___fieldsPanel_4), value);
	}

	inline static int32_t get_offset_of_feastingHall_5() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___feastingHall_5)); }
	inline GameObject_t1113636619 * get_feastingHall_5() const { return ___feastingHall_5; }
	inline GameObject_t1113636619 ** get_address_of_feastingHall_5() { return &___feastingHall_5; }
	inline void set_feastingHall_5(GameObject_t1113636619 * value)
	{
		___feastingHall_5 = value;
		Il2CppCodeGenWriteBarrier((&___feastingHall_5), value);
	}

	inline static int32_t get_offset_of_market_6() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___market_6)); }
	inline GameObject_t1113636619 * get_market_6() const { return ___market_6; }
	inline GameObject_t1113636619 ** get_address_of_market_6() { return &___market_6; }
	inline void set_market_6(GameObject_t1113636619 * value)
	{
		___market_6 = value;
		Il2CppCodeGenWriteBarrier((&___market_6), value);
	}

	inline static int32_t get_offset_of_guestHouse_7() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___guestHouse_7)); }
	inline GameObject_t1113636619 * get_guestHouse_7() const { return ___guestHouse_7; }
	inline GameObject_t1113636619 ** get_address_of_guestHouse_7() { return &___guestHouse_7; }
	inline void set_guestHouse_7(GameObject_t1113636619 * value)
	{
		___guestHouse_7 = value;
		Il2CppCodeGenWriteBarrier((&___guestHouse_7), value);
	}

	inline static int32_t get_offset_of_storage_8() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___storage_8)); }
	inline GameObject_t1113636619 * get_storage_8() const { return ___storage_8; }
	inline GameObject_t1113636619 ** get_address_of_storage_8() { return &___storage_8; }
	inline void set_storage_8(GameObject_t1113636619 * value)
	{
		___storage_8 = value;
		Il2CppCodeGenWriteBarrier((&___storage_8), value);
	}

	inline static int32_t get_offset_of_furnace_9() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___furnace_9)); }
	inline GameObject_t1113636619 * get_furnace_9() const { return ___furnace_9; }
	inline GameObject_t1113636619 ** get_address_of_furnace_9() { return &___furnace_9; }
	inline void set_furnace_9(GameObject_t1113636619 * value)
	{
		___furnace_9 = value;
		Il2CppCodeGenWriteBarrier((&___furnace_9), value);
	}

	inline static int32_t get_offset_of_blackSmith_10() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___blackSmith_10)); }
	inline GameObject_t1113636619 * get_blackSmith_10() const { return ___blackSmith_10; }
	inline GameObject_t1113636619 ** get_address_of_blackSmith_10() { return &___blackSmith_10; }
	inline void set_blackSmith_10(GameObject_t1113636619 * value)
	{
		___blackSmith_10 = value;
		Il2CppCodeGenWriteBarrier((&___blackSmith_10), value);
	}

	inline static int32_t get_offset_of_hospital_11() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___hospital_11)); }
	inline GameObject_t1113636619 * get_hospital_11() const { return ___hospital_11; }
	inline GameObject_t1113636619 ** get_address_of_hospital_11() { return &___hospital_11; }
	inline void set_hospital_11(GameObject_t1113636619 * value)
	{
		___hospital_11 = value;
		Il2CppCodeGenWriteBarrier((&___hospital_11), value);
	}

	inline static int32_t get_offset_of_godHouse_12() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___godHouse_12)); }
	inline GameObject_t1113636619 * get_godHouse_12() const { return ___godHouse_12; }
	inline GameObject_t1113636619 ** get_address_of_godHouse_12() { return &___godHouse_12; }
	inline void set_godHouse_12(GameObject_t1113636619 * value)
	{
		___godHouse_12 = value;
		Il2CppCodeGenWriteBarrier((&___godHouse_12), value);
	}

	inline static int32_t get_offset_of_commandCenter_13() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___commandCenter_13)); }
	inline GameObject_t1113636619 * get_commandCenter_13() const { return ___commandCenter_13; }
	inline GameObject_t1113636619 ** get_address_of_commandCenter_13() { return &___commandCenter_13; }
	inline void set_commandCenter_13(GameObject_t1113636619 * value)
	{
		___commandCenter_13 = value;
		Il2CppCodeGenWriteBarrier((&___commandCenter_13), value);
	}

	inline static int32_t get_offset_of_diplomacy_14() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___diplomacy_14)); }
	inline GameObject_t1113636619 * get_diplomacy_14() const { return ___diplomacy_14; }
	inline GameObject_t1113636619 ** get_address_of_diplomacy_14() { return &___diplomacy_14; }
	inline void set_diplomacy_14(GameObject_t1113636619 * value)
	{
		___diplomacy_14 = value;
		Il2CppCodeGenWriteBarrier((&___diplomacy_14), value);
	}

	inline static int32_t get_offset_of_trainingGround_15() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___trainingGround_15)); }
	inline GameObject_t1113636619 * get_trainingGround_15() const { return ___trainingGround_15; }
	inline GameObject_t1113636619 ** get_address_of_trainingGround_15() { return &___trainingGround_15; }
	inline void set_trainingGround_15(GameObject_t1113636619 * value)
	{
		___trainingGround_15 = value;
		Il2CppCodeGenWriteBarrier((&___trainingGround_15), value);
	}

	inline static int32_t get_offset_of_sportArena_16() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___sportArena_16)); }
	inline GameObject_t1113636619 * get_sportArena_16() const { return ___sportArena_16; }
	inline GameObject_t1113636619 ** get_address_of_sportArena_16() { return &___sportArena_16; }
	inline void set_sportArena_16(GameObject_t1113636619 * value)
	{
		___sportArena_16 = value;
		Il2CppCodeGenWriteBarrier((&___sportArena_16), value);
	}

	inline static int32_t get_offset_of_univercity_17() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___univercity_17)); }
	inline GameObject_t1113636619 * get_univercity_17() const { return ___univercity_17; }
	inline GameObject_t1113636619 ** get_address_of_univercity_17() { return &___univercity_17; }
	inline void set_univercity_17(GameObject_t1113636619 * value)
	{
		___univercity_17 = value;
		Il2CppCodeGenWriteBarrier((&___univercity_17), value);
	}

	inline static int32_t get_offset_of_militarySchool_18() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___militarySchool_18)); }
	inline GameObject_t1113636619 * get_militarySchool_18() const { return ___militarySchool_18; }
	inline GameObject_t1113636619 ** get_address_of_militarySchool_18() { return &___militarySchool_18; }
	inline void set_militarySchool_18(GameObject_t1113636619 * value)
	{
		___militarySchool_18 = value;
		Il2CppCodeGenWriteBarrier((&___militarySchool_18), value);
	}

	inline static int32_t get_offset_of_archery_19() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___archery_19)); }
	inline GameObject_t1113636619 * get_archery_19() const { return ___archery_19; }
	inline GameObject_t1113636619 ** get_address_of_archery_19() { return &___archery_19; }
	inline void set_archery_19(GameObject_t1113636619 * value)
	{
		___archery_19 = value;
		Il2CppCodeGenWriteBarrier((&___archery_19), value);
	}

	inline static int32_t get_offset_of_stable_20() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___stable_20)); }
	inline GameObject_t1113636619 * get_stable_20() const { return ___stable_20; }
	inline GameObject_t1113636619 ** get_address_of_stable_20() { return &___stable_20; }
	inline void set_stable_20(GameObject_t1113636619 * value)
	{
		___stable_20 = value;
		Il2CppCodeGenWriteBarrier((&___stable_20), value);
	}

	inline static int32_t get_offset_of_engineering_21() { return static_cast<int32_t>(offsetof(ConstructionDetailsManager_t275842940, ___engineering_21)); }
	inline GameObject_t1113636619 * get_engineering_21() const { return ___engineering_21; }
	inline GameObject_t1113636619 ** get_address_of_engineering_21() { return &___engineering_21; }
	inline void set_engineering_21(GameObject_t1113636619 * value)
	{
		___engineering_21 = value;
		Il2CppCodeGenWriteBarrier((&___engineering_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTIONDETAILSMANAGER_T275842940_H
#ifndef DISPATCHMANAGER_T670234083_H
#define DISPATCHMANAGER_T670234083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DispatchManager
struct  DispatchManager_t670234083  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DispatchManager::Page1
	GameObject_t1113636619 * ___Page1_2;
	// UnityEngine.GameObject DispatchManager::Page2
	GameObject_t1113636619 * ___Page2_3;
	// UnityEngine.UI.Text DispatchManager::cityFood
	Text_t1901882714 * ___cityFood_4;
	// UnityEngine.UI.Text DispatchManager::cityWood
	Text_t1901882714 * ___cityWood_5;
	// UnityEngine.UI.Text DispatchManager::cityIron
	Text_t1901882714 * ___cityIron_6;
	// UnityEngine.UI.Text DispatchManager::cityStone
	Text_t1901882714 * ___cityStone_7;
	// UnityEngine.UI.Text DispatchManager::cityCopper
	Text_t1901882714 * ___cityCopper_8;
	// UnityEngine.UI.Text DispatchManager::citySilver
	Text_t1901882714 * ___citySilver_9;
	// UnityEngine.UI.Text DispatchManager::cityGold
	Text_t1901882714 * ___cityGold_10;
	// UnityEngine.UI.Text DispatchManager::cityWorker
	Text_t1901882714 * ___cityWorker_11;
	// UnityEngine.UI.Text DispatchManager::citySpy
	Text_t1901882714 * ___citySpy_12;
	// UnityEngine.UI.Text DispatchManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_13;
	// UnityEngine.UI.Text DispatchManager::citySpearman
	Text_t1901882714 * ___citySpearman_14;
	// UnityEngine.UI.Text DispatchManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_15;
	// UnityEngine.UI.Text DispatchManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_16;
	// UnityEngine.UI.Text DispatchManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_17;
	// UnityEngine.UI.Text DispatchManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_18;
	// UnityEngine.UI.Text DispatchManager::cityArcher
	Text_t1901882714 * ___cityArcher_19;
	// UnityEngine.UI.Text DispatchManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_20;
	// UnityEngine.UI.Text DispatchManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_21;
	// UnityEngine.UI.Text DispatchManager::cityWagon
	Text_t1901882714 * ___cityWagon_22;
	// UnityEngine.UI.Text DispatchManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_23;
	// UnityEngine.UI.Text DispatchManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_24;
	// UnityEngine.UI.Text DispatchManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_25;
	// UnityEngine.UI.Text DispatchManager::cityBallista
	Text_t1901882714 * ___cityBallista_26;
	// UnityEngine.UI.Text DispatchManager::cityWorkerName
	Text_t1901882714 * ___cityWorkerName_27;
	// UnityEngine.UI.Text DispatchManager::citySpyName
	Text_t1901882714 * ___citySpyName_28;
	// UnityEngine.UI.Text DispatchManager::citySwordsmanName
	Text_t1901882714 * ___citySwordsmanName_29;
	// UnityEngine.UI.Text DispatchManager::citySpearmanName
	Text_t1901882714 * ___citySpearmanName_30;
	// UnityEngine.UI.Text DispatchManager::cityPikemanName
	Text_t1901882714 * ___cityPikemanName_31;
	// UnityEngine.UI.Text DispatchManager::cityScoutRiderName
	Text_t1901882714 * ___cityScoutRiderName_32;
	// UnityEngine.UI.Text DispatchManager::cityLightCavalryName
	Text_t1901882714 * ___cityLightCavalryName_33;
	// UnityEngine.UI.Text DispatchManager::cityHeavyCavalryName
	Text_t1901882714 * ___cityHeavyCavalryName_34;
	// UnityEngine.UI.Text DispatchManager::cityArcherName
	Text_t1901882714 * ___cityArcherName_35;
	// UnityEngine.UI.Text DispatchManager::cityArcherRiderName
	Text_t1901882714 * ___cityArcherRiderName_36;
	// UnityEngine.UI.Text DispatchManager::cityHollymanName
	Text_t1901882714 * ___cityHollymanName_37;
	// UnityEngine.UI.Text DispatchManager::cityWagonName
	Text_t1901882714 * ___cityWagonName_38;
	// UnityEngine.UI.Text DispatchManager::cityTrebuchetName
	Text_t1901882714 * ___cityTrebuchetName_39;
	// UnityEngine.UI.Text DispatchManager::citySiegeTowerName
	Text_t1901882714 * ___citySiegeTowerName_40;
	// UnityEngine.UI.Text DispatchManager::cityBatteringRamName
	Text_t1901882714 * ___cityBatteringRamName_41;
	// UnityEngine.UI.Text DispatchManager::cityBallistaName
	Text_t1901882714 * ___cityBallistaName_42;
	// UnityEngine.UI.InputField DispatchManager::dispatchFood
	InputField_t3762917431 * ___dispatchFood_43;
	// UnityEngine.UI.InputField DispatchManager::dispatchWood
	InputField_t3762917431 * ___dispatchWood_44;
	// UnityEngine.UI.InputField DispatchManager::dispatchIron
	InputField_t3762917431 * ___dispatchIron_45;
	// UnityEngine.UI.InputField DispatchManager::dispatchStone
	InputField_t3762917431 * ___dispatchStone_46;
	// UnityEngine.UI.InputField DispatchManager::dispatchCopper
	InputField_t3762917431 * ___dispatchCopper_47;
	// UnityEngine.UI.InputField DispatchManager::dispatchSilver
	InputField_t3762917431 * ___dispatchSilver_48;
	// UnityEngine.UI.InputField DispatchManager::dispatchGold
	InputField_t3762917431 * ___dispatchGold_49;
	// UnityEngine.UI.InputField DispatchManager::dispatchWorker
	InputField_t3762917431 * ___dispatchWorker_50;
	// UnityEngine.UI.InputField DispatchManager::dispatchSpy
	InputField_t3762917431 * ___dispatchSpy_51;
	// UnityEngine.UI.InputField DispatchManager::dispatchSwordsman
	InputField_t3762917431 * ___dispatchSwordsman_52;
	// UnityEngine.UI.InputField DispatchManager::dispatchSpearman
	InputField_t3762917431 * ___dispatchSpearman_53;
	// UnityEngine.UI.InputField DispatchManager::dispatchPikeman
	InputField_t3762917431 * ___dispatchPikeman_54;
	// UnityEngine.UI.InputField DispatchManager::dispatchScoutRider
	InputField_t3762917431 * ___dispatchScoutRider_55;
	// UnityEngine.UI.InputField DispatchManager::dispatchLightCavalry
	InputField_t3762917431 * ___dispatchLightCavalry_56;
	// UnityEngine.UI.InputField DispatchManager::dispatchHeavyCavalry
	InputField_t3762917431 * ___dispatchHeavyCavalry_57;
	// UnityEngine.UI.InputField DispatchManager::dispatchArcher
	InputField_t3762917431 * ___dispatchArcher_58;
	// UnityEngine.UI.InputField DispatchManager::dispatchArcherRider
	InputField_t3762917431 * ___dispatchArcherRider_59;
	// UnityEngine.UI.InputField DispatchManager::dispatchHollyman
	InputField_t3762917431 * ___dispatchHollyman_60;
	// UnityEngine.UI.InputField DispatchManager::dispatchWagon
	InputField_t3762917431 * ___dispatchWagon_61;
	// UnityEngine.UI.InputField DispatchManager::dispatchTrebuchet
	InputField_t3762917431 * ___dispatchTrebuchet_62;
	// UnityEngine.UI.InputField DispatchManager::dispatchSiegeTower
	InputField_t3762917431 * ___dispatchSiegeTower_63;
	// UnityEngine.UI.InputField DispatchManager::dispatchBatteringRam
	InputField_t3762917431 * ___dispatchBatteringRam_64;
	// UnityEngine.UI.InputField DispatchManager::dispatchBallista
	InputField_t3762917431 * ___dispatchBallista_65;
	// UnityEngine.UI.Dropdown DispatchManager::attackType
	Dropdown_t2274391225 * ___attackType_66;
	// UnityEngine.UI.Dropdown DispatchManager::knights
	Dropdown_t2274391225 * ___knights_67;
	// UnityEngine.UI.InputField DispatchManager::dispatchHour
	InputField_t3762917431 * ___dispatchHour_68;
	// UnityEngine.UI.InputField DispatchManager::dispatchMinute
	InputField_t3762917431 * ___dispatchMinute_69;
	// UnityEngine.UI.Text DispatchManager::xCoord
	Text_t1901882714 * ___xCoord_70;
	// UnityEngine.UI.Text DispatchManager::yCoord
	Text_t1901882714 * ___yCoord_71;
	// UnityEngine.UI.InputField DispatchManager::xInput
	InputField_t3762917431 * ___xInput_72;
	// UnityEngine.UI.InputField DispatchManager::yInput
	InputField_t3762917431 * ___yInput_73;
	// UnityEngine.UI.InputField DispatchManager::foodPrice
	InputField_t3762917431 * ___foodPrice_74;
	// UnityEngine.UI.InputField DispatchManager::loadVacancy
	InputField_t3762917431 * ___loadVacancy_75;
	// UnityEngine.UI.InputField DispatchManager::tripTime
	InputField_t3762917431 * ___tripTime_76;
	// System.Boolean DispatchManager::fromMap
	bool ___fromMap_77;
	// WorldFieldModel DispatchManager::targetField
	WorldFieldModel_t2417974361 * ___targetField_78;
	// System.Collections.Generic.List`1<ArmyGeneralModel> DispatchManager::generals
	List_1_t2209824963 * ___generals_79;
	// UnitsModel DispatchManager::army
	UnitsModel_t3847956398 * ___army_80;
	// ResourcesModel DispatchManager::resources
	ResourcesModel_t2533508513 * ___resources_81;
	// System.Boolean DispatchManager::calculated
	bool ___calculated_82;
	// SimpleEvent DispatchManager::onGotParams
	SimpleEvent_t129249603 * ___onGotParams_83;
	// System.Collections.Generic.List`1<System.String> DispatchManager::attackTypes
	List_1_t3319525431 * ___attackTypes_84;

public:
	inline static int32_t get_offset_of_Page1_2() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___Page1_2)); }
	inline GameObject_t1113636619 * get_Page1_2() const { return ___Page1_2; }
	inline GameObject_t1113636619 ** get_address_of_Page1_2() { return &___Page1_2; }
	inline void set_Page1_2(GameObject_t1113636619 * value)
	{
		___Page1_2 = value;
		Il2CppCodeGenWriteBarrier((&___Page1_2), value);
	}

	inline static int32_t get_offset_of_Page2_3() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___Page2_3)); }
	inline GameObject_t1113636619 * get_Page2_3() const { return ___Page2_3; }
	inline GameObject_t1113636619 ** get_address_of_Page2_3() { return &___Page2_3; }
	inline void set_Page2_3(GameObject_t1113636619 * value)
	{
		___Page2_3 = value;
		Il2CppCodeGenWriteBarrier((&___Page2_3), value);
	}

	inline static int32_t get_offset_of_cityFood_4() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityFood_4)); }
	inline Text_t1901882714 * get_cityFood_4() const { return ___cityFood_4; }
	inline Text_t1901882714 ** get_address_of_cityFood_4() { return &___cityFood_4; }
	inline void set_cityFood_4(Text_t1901882714 * value)
	{
		___cityFood_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityFood_4), value);
	}

	inline static int32_t get_offset_of_cityWood_5() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityWood_5)); }
	inline Text_t1901882714 * get_cityWood_5() const { return ___cityWood_5; }
	inline Text_t1901882714 ** get_address_of_cityWood_5() { return &___cityWood_5; }
	inline void set_cityWood_5(Text_t1901882714 * value)
	{
		___cityWood_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityWood_5), value);
	}

	inline static int32_t get_offset_of_cityIron_6() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityIron_6)); }
	inline Text_t1901882714 * get_cityIron_6() const { return ___cityIron_6; }
	inline Text_t1901882714 ** get_address_of_cityIron_6() { return &___cityIron_6; }
	inline void set_cityIron_6(Text_t1901882714 * value)
	{
		___cityIron_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityIron_6), value);
	}

	inline static int32_t get_offset_of_cityStone_7() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityStone_7)); }
	inline Text_t1901882714 * get_cityStone_7() const { return ___cityStone_7; }
	inline Text_t1901882714 ** get_address_of_cityStone_7() { return &___cityStone_7; }
	inline void set_cityStone_7(Text_t1901882714 * value)
	{
		___cityStone_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityStone_7), value);
	}

	inline static int32_t get_offset_of_cityCopper_8() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityCopper_8)); }
	inline Text_t1901882714 * get_cityCopper_8() const { return ___cityCopper_8; }
	inline Text_t1901882714 ** get_address_of_cityCopper_8() { return &___cityCopper_8; }
	inline void set_cityCopper_8(Text_t1901882714 * value)
	{
		___cityCopper_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityCopper_8), value);
	}

	inline static int32_t get_offset_of_citySilver_9() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySilver_9)); }
	inline Text_t1901882714 * get_citySilver_9() const { return ___citySilver_9; }
	inline Text_t1901882714 ** get_address_of_citySilver_9() { return &___citySilver_9; }
	inline void set_citySilver_9(Text_t1901882714 * value)
	{
		___citySilver_9 = value;
		Il2CppCodeGenWriteBarrier((&___citySilver_9), value);
	}

	inline static int32_t get_offset_of_cityGold_10() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityGold_10)); }
	inline Text_t1901882714 * get_cityGold_10() const { return ___cityGold_10; }
	inline Text_t1901882714 ** get_address_of_cityGold_10() { return &___cityGold_10; }
	inline void set_cityGold_10(Text_t1901882714 * value)
	{
		___cityGold_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityGold_10), value);
	}

	inline static int32_t get_offset_of_cityWorker_11() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityWorker_11)); }
	inline Text_t1901882714 * get_cityWorker_11() const { return ___cityWorker_11; }
	inline Text_t1901882714 ** get_address_of_cityWorker_11() { return &___cityWorker_11; }
	inline void set_cityWorker_11(Text_t1901882714 * value)
	{
		___cityWorker_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_11), value);
	}

	inline static int32_t get_offset_of_citySpy_12() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySpy_12)); }
	inline Text_t1901882714 * get_citySpy_12() const { return ___citySpy_12; }
	inline Text_t1901882714 ** get_address_of_citySpy_12() { return &___citySpy_12; }
	inline void set_citySpy_12(Text_t1901882714 * value)
	{
		___citySpy_12 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_12), value);
	}

	inline static int32_t get_offset_of_citySwordsman_13() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySwordsman_13)); }
	inline Text_t1901882714 * get_citySwordsman_13() const { return ___citySwordsman_13; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_13() { return &___citySwordsman_13; }
	inline void set_citySwordsman_13(Text_t1901882714 * value)
	{
		___citySwordsman_13 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_13), value);
	}

	inline static int32_t get_offset_of_citySpearman_14() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySpearman_14)); }
	inline Text_t1901882714 * get_citySpearman_14() const { return ___citySpearman_14; }
	inline Text_t1901882714 ** get_address_of_citySpearman_14() { return &___citySpearman_14; }
	inline void set_citySpearman_14(Text_t1901882714 * value)
	{
		___citySpearman_14 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_14), value);
	}

	inline static int32_t get_offset_of_cityPikeman_15() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityPikeman_15)); }
	inline Text_t1901882714 * get_cityPikeman_15() const { return ___cityPikeman_15; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_15() { return &___cityPikeman_15; }
	inline void set_cityPikeman_15(Text_t1901882714 * value)
	{
		___cityPikeman_15 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_15), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_16() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityScoutRider_16)); }
	inline Text_t1901882714 * get_cityScoutRider_16() const { return ___cityScoutRider_16; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_16() { return &___cityScoutRider_16; }
	inline void set_cityScoutRider_16(Text_t1901882714 * value)
	{
		___cityScoutRider_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_16), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_17() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityLightCavalry_17)); }
	inline Text_t1901882714 * get_cityLightCavalry_17() const { return ___cityLightCavalry_17; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_17() { return &___cityLightCavalry_17; }
	inline void set_cityLightCavalry_17(Text_t1901882714 * value)
	{
		___cityLightCavalry_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_17), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_18() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityHeavyCavalry_18)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_18() const { return ___cityHeavyCavalry_18; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_18() { return &___cityHeavyCavalry_18; }
	inline void set_cityHeavyCavalry_18(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_18 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_18), value);
	}

	inline static int32_t get_offset_of_cityArcher_19() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityArcher_19)); }
	inline Text_t1901882714 * get_cityArcher_19() const { return ___cityArcher_19; }
	inline Text_t1901882714 ** get_address_of_cityArcher_19() { return &___cityArcher_19; }
	inline void set_cityArcher_19(Text_t1901882714 * value)
	{
		___cityArcher_19 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_19), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_20() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityArcherRider_20)); }
	inline Text_t1901882714 * get_cityArcherRider_20() const { return ___cityArcherRider_20; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_20() { return &___cityArcherRider_20; }
	inline void set_cityArcherRider_20(Text_t1901882714 * value)
	{
		___cityArcherRider_20 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_20), value);
	}

	inline static int32_t get_offset_of_cityHollyman_21() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityHollyman_21)); }
	inline Text_t1901882714 * get_cityHollyman_21() const { return ___cityHollyman_21; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_21() { return &___cityHollyman_21; }
	inline void set_cityHollyman_21(Text_t1901882714 * value)
	{
		___cityHollyman_21 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_21), value);
	}

	inline static int32_t get_offset_of_cityWagon_22() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityWagon_22)); }
	inline Text_t1901882714 * get_cityWagon_22() const { return ___cityWagon_22; }
	inline Text_t1901882714 ** get_address_of_cityWagon_22() { return &___cityWagon_22; }
	inline void set_cityWagon_22(Text_t1901882714 * value)
	{
		___cityWagon_22 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_22), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_23() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityTrebuchet_23)); }
	inline Text_t1901882714 * get_cityTrebuchet_23() const { return ___cityTrebuchet_23; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_23() { return &___cityTrebuchet_23; }
	inline void set_cityTrebuchet_23(Text_t1901882714 * value)
	{
		___cityTrebuchet_23 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_23), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_24() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySiegeTower_24)); }
	inline Text_t1901882714 * get_citySiegeTower_24() const { return ___citySiegeTower_24; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_24() { return &___citySiegeTower_24; }
	inline void set_citySiegeTower_24(Text_t1901882714 * value)
	{
		___citySiegeTower_24 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_24), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_25() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityBatteringRam_25)); }
	inline Text_t1901882714 * get_cityBatteringRam_25() const { return ___cityBatteringRam_25; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_25() { return &___cityBatteringRam_25; }
	inline void set_cityBatteringRam_25(Text_t1901882714 * value)
	{
		___cityBatteringRam_25 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_25), value);
	}

	inline static int32_t get_offset_of_cityBallista_26() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityBallista_26)); }
	inline Text_t1901882714 * get_cityBallista_26() const { return ___cityBallista_26; }
	inline Text_t1901882714 ** get_address_of_cityBallista_26() { return &___cityBallista_26; }
	inline void set_cityBallista_26(Text_t1901882714 * value)
	{
		___cityBallista_26 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_26), value);
	}

	inline static int32_t get_offset_of_cityWorkerName_27() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityWorkerName_27)); }
	inline Text_t1901882714 * get_cityWorkerName_27() const { return ___cityWorkerName_27; }
	inline Text_t1901882714 ** get_address_of_cityWorkerName_27() { return &___cityWorkerName_27; }
	inline void set_cityWorkerName_27(Text_t1901882714 * value)
	{
		___cityWorkerName_27 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorkerName_27), value);
	}

	inline static int32_t get_offset_of_citySpyName_28() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySpyName_28)); }
	inline Text_t1901882714 * get_citySpyName_28() const { return ___citySpyName_28; }
	inline Text_t1901882714 ** get_address_of_citySpyName_28() { return &___citySpyName_28; }
	inline void set_citySpyName_28(Text_t1901882714 * value)
	{
		___citySpyName_28 = value;
		Il2CppCodeGenWriteBarrier((&___citySpyName_28), value);
	}

	inline static int32_t get_offset_of_citySwordsmanName_29() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySwordsmanName_29)); }
	inline Text_t1901882714 * get_citySwordsmanName_29() const { return ___citySwordsmanName_29; }
	inline Text_t1901882714 ** get_address_of_citySwordsmanName_29() { return &___citySwordsmanName_29; }
	inline void set_citySwordsmanName_29(Text_t1901882714 * value)
	{
		___citySwordsmanName_29 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsmanName_29), value);
	}

	inline static int32_t get_offset_of_citySpearmanName_30() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySpearmanName_30)); }
	inline Text_t1901882714 * get_citySpearmanName_30() const { return ___citySpearmanName_30; }
	inline Text_t1901882714 ** get_address_of_citySpearmanName_30() { return &___citySpearmanName_30; }
	inline void set_citySpearmanName_30(Text_t1901882714 * value)
	{
		___citySpearmanName_30 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearmanName_30), value);
	}

	inline static int32_t get_offset_of_cityPikemanName_31() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityPikemanName_31)); }
	inline Text_t1901882714 * get_cityPikemanName_31() const { return ___cityPikemanName_31; }
	inline Text_t1901882714 ** get_address_of_cityPikemanName_31() { return &___cityPikemanName_31; }
	inline void set_cityPikemanName_31(Text_t1901882714 * value)
	{
		___cityPikemanName_31 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikemanName_31), value);
	}

	inline static int32_t get_offset_of_cityScoutRiderName_32() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityScoutRiderName_32)); }
	inline Text_t1901882714 * get_cityScoutRiderName_32() const { return ___cityScoutRiderName_32; }
	inline Text_t1901882714 ** get_address_of_cityScoutRiderName_32() { return &___cityScoutRiderName_32; }
	inline void set_cityScoutRiderName_32(Text_t1901882714 * value)
	{
		___cityScoutRiderName_32 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRiderName_32), value);
	}

	inline static int32_t get_offset_of_cityLightCavalryName_33() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityLightCavalryName_33)); }
	inline Text_t1901882714 * get_cityLightCavalryName_33() const { return ___cityLightCavalryName_33; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalryName_33() { return &___cityLightCavalryName_33; }
	inline void set_cityLightCavalryName_33(Text_t1901882714 * value)
	{
		___cityLightCavalryName_33 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalryName_33), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalryName_34() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityHeavyCavalryName_34)); }
	inline Text_t1901882714 * get_cityHeavyCavalryName_34() const { return ___cityHeavyCavalryName_34; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalryName_34() { return &___cityHeavyCavalryName_34; }
	inline void set_cityHeavyCavalryName_34(Text_t1901882714 * value)
	{
		___cityHeavyCavalryName_34 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalryName_34), value);
	}

	inline static int32_t get_offset_of_cityArcherName_35() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityArcherName_35)); }
	inline Text_t1901882714 * get_cityArcherName_35() const { return ___cityArcherName_35; }
	inline Text_t1901882714 ** get_address_of_cityArcherName_35() { return &___cityArcherName_35; }
	inline void set_cityArcherName_35(Text_t1901882714 * value)
	{
		___cityArcherName_35 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherName_35), value);
	}

	inline static int32_t get_offset_of_cityArcherRiderName_36() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityArcherRiderName_36)); }
	inline Text_t1901882714 * get_cityArcherRiderName_36() const { return ___cityArcherRiderName_36; }
	inline Text_t1901882714 ** get_address_of_cityArcherRiderName_36() { return &___cityArcherRiderName_36; }
	inline void set_cityArcherRiderName_36(Text_t1901882714 * value)
	{
		___cityArcherRiderName_36 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRiderName_36), value);
	}

	inline static int32_t get_offset_of_cityHollymanName_37() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityHollymanName_37)); }
	inline Text_t1901882714 * get_cityHollymanName_37() const { return ___cityHollymanName_37; }
	inline Text_t1901882714 ** get_address_of_cityHollymanName_37() { return &___cityHollymanName_37; }
	inline void set_cityHollymanName_37(Text_t1901882714 * value)
	{
		___cityHollymanName_37 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollymanName_37), value);
	}

	inline static int32_t get_offset_of_cityWagonName_38() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityWagonName_38)); }
	inline Text_t1901882714 * get_cityWagonName_38() const { return ___cityWagonName_38; }
	inline Text_t1901882714 ** get_address_of_cityWagonName_38() { return &___cityWagonName_38; }
	inline void set_cityWagonName_38(Text_t1901882714 * value)
	{
		___cityWagonName_38 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagonName_38), value);
	}

	inline static int32_t get_offset_of_cityTrebuchetName_39() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityTrebuchetName_39)); }
	inline Text_t1901882714 * get_cityTrebuchetName_39() const { return ___cityTrebuchetName_39; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchetName_39() { return &___cityTrebuchetName_39; }
	inline void set_cityTrebuchetName_39(Text_t1901882714 * value)
	{
		___cityTrebuchetName_39 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchetName_39), value);
	}

	inline static int32_t get_offset_of_citySiegeTowerName_40() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___citySiegeTowerName_40)); }
	inline Text_t1901882714 * get_citySiegeTowerName_40() const { return ___citySiegeTowerName_40; }
	inline Text_t1901882714 ** get_address_of_citySiegeTowerName_40() { return &___citySiegeTowerName_40; }
	inline void set_citySiegeTowerName_40(Text_t1901882714 * value)
	{
		___citySiegeTowerName_40 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTowerName_40), value);
	}

	inline static int32_t get_offset_of_cityBatteringRamName_41() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityBatteringRamName_41)); }
	inline Text_t1901882714 * get_cityBatteringRamName_41() const { return ___cityBatteringRamName_41; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRamName_41() { return &___cityBatteringRamName_41; }
	inline void set_cityBatteringRamName_41(Text_t1901882714 * value)
	{
		___cityBatteringRamName_41 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRamName_41), value);
	}

	inline static int32_t get_offset_of_cityBallistaName_42() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___cityBallistaName_42)); }
	inline Text_t1901882714 * get_cityBallistaName_42() const { return ___cityBallistaName_42; }
	inline Text_t1901882714 ** get_address_of_cityBallistaName_42() { return &___cityBallistaName_42; }
	inline void set_cityBallistaName_42(Text_t1901882714 * value)
	{
		___cityBallistaName_42 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallistaName_42), value);
	}

	inline static int32_t get_offset_of_dispatchFood_43() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchFood_43)); }
	inline InputField_t3762917431 * get_dispatchFood_43() const { return ___dispatchFood_43; }
	inline InputField_t3762917431 ** get_address_of_dispatchFood_43() { return &___dispatchFood_43; }
	inline void set_dispatchFood_43(InputField_t3762917431 * value)
	{
		___dispatchFood_43 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchFood_43), value);
	}

	inline static int32_t get_offset_of_dispatchWood_44() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchWood_44)); }
	inline InputField_t3762917431 * get_dispatchWood_44() const { return ___dispatchWood_44; }
	inline InputField_t3762917431 ** get_address_of_dispatchWood_44() { return &___dispatchWood_44; }
	inline void set_dispatchWood_44(InputField_t3762917431 * value)
	{
		___dispatchWood_44 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchWood_44), value);
	}

	inline static int32_t get_offset_of_dispatchIron_45() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchIron_45)); }
	inline InputField_t3762917431 * get_dispatchIron_45() const { return ___dispatchIron_45; }
	inline InputField_t3762917431 ** get_address_of_dispatchIron_45() { return &___dispatchIron_45; }
	inline void set_dispatchIron_45(InputField_t3762917431 * value)
	{
		___dispatchIron_45 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchIron_45), value);
	}

	inline static int32_t get_offset_of_dispatchStone_46() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchStone_46)); }
	inline InputField_t3762917431 * get_dispatchStone_46() const { return ___dispatchStone_46; }
	inline InputField_t3762917431 ** get_address_of_dispatchStone_46() { return &___dispatchStone_46; }
	inline void set_dispatchStone_46(InputField_t3762917431 * value)
	{
		___dispatchStone_46 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchStone_46), value);
	}

	inline static int32_t get_offset_of_dispatchCopper_47() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchCopper_47)); }
	inline InputField_t3762917431 * get_dispatchCopper_47() const { return ___dispatchCopper_47; }
	inline InputField_t3762917431 ** get_address_of_dispatchCopper_47() { return &___dispatchCopper_47; }
	inline void set_dispatchCopper_47(InputField_t3762917431 * value)
	{
		___dispatchCopper_47 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchCopper_47), value);
	}

	inline static int32_t get_offset_of_dispatchSilver_48() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchSilver_48)); }
	inline InputField_t3762917431 * get_dispatchSilver_48() const { return ___dispatchSilver_48; }
	inline InputField_t3762917431 ** get_address_of_dispatchSilver_48() { return &___dispatchSilver_48; }
	inline void set_dispatchSilver_48(InputField_t3762917431 * value)
	{
		___dispatchSilver_48 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSilver_48), value);
	}

	inline static int32_t get_offset_of_dispatchGold_49() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchGold_49)); }
	inline InputField_t3762917431 * get_dispatchGold_49() const { return ___dispatchGold_49; }
	inline InputField_t3762917431 ** get_address_of_dispatchGold_49() { return &___dispatchGold_49; }
	inline void set_dispatchGold_49(InputField_t3762917431 * value)
	{
		___dispatchGold_49 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchGold_49), value);
	}

	inline static int32_t get_offset_of_dispatchWorker_50() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchWorker_50)); }
	inline InputField_t3762917431 * get_dispatchWorker_50() const { return ___dispatchWorker_50; }
	inline InputField_t3762917431 ** get_address_of_dispatchWorker_50() { return &___dispatchWorker_50; }
	inline void set_dispatchWorker_50(InputField_t3762917431 * value)
	{
		___dispatchWorker_50 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchWorker_50), value);
	}

	inline static int32_t get_offset_of_dispatchSpy_51() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchSpy_51)); }
	inline InputField_t3762917431 * get_dispatchSpy_51() const { return ___dispatchSpy_51; }
	inline InputField_t3762917431 ** get_address_of_dispatchSpy_51() { return &___dispatchSpy_51; }
	inline void set_dispatchSpy_51(InputField_t3762917431 * value)
	{
		___dispatchSpy_51 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSpy_51), value);
	}

	inline static int32_t get_offset_of_dispatchSwordsman_52() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchSwordsman_52)); }
	inline InputField_t3762917431 * get_dispatchSwordsman_52() const { return ___dispatchSwordsman_52; }
	inline InputField_t3762917431 ** get_address_of_dispatchSwordsman_52() { return &___dispatchSwordsman_52; }
	inline void set_dispatchSwordsman_52(InputField_t3762917431 * value)
	{
		___dispatchSwordsman_52 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSwordsman_52), value);
	}

	inline static int32_t get_offset_of_dispatchSpearman_53() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchSpearman_53)); }
	inline InputField_t3762917431 * get_dispatchSpearman_53() const { return ___dispatchSpearman_53; }
	inline InputField_t3762917431 ** get_address_of_dispatchSpearman_53() { return &___dispatchSpearman_53; }
	inline void set_dispatchSpearman_53(InputField_t3762917431 * value)
	{
		___dispatchSpearman_53 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSpearman_53), value);
	}

	inline static int32_t get_offset_of_dispatchPikeman_54() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchPikeman_54)); }
	inline InputField_t3762917431 * get_dispatchPikeman_54() const { return ___dispatchPikeman_54; }
	inline InputField_t3762917431 ** get_address_of_dispatchPikeman_54() { return &___dispatchPikeman_54; }
	inline void set_dispatchPikeman_54(InputField_t3762917431 * value)
	{
		___dispatchPikeman_54 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchPikeman_54), value);
	}

	inline static int32_t get_offset_of_dispatchScoutRider_55() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchScoutRider_55)); }
	inline InputField_t3762917431 * get_dispatchScoutRider_55() const { return ___dispatchScoutRider_55; }
	inline InputField_t3762917431 ** get_address_of_dispatchScoutRider_55() { return &___dispatchScoutRider_55; }
	inline void set_dispatchScoutRider_55(InputField_t3762917431 * value)
	{
		___dispatchScoutRider_55 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchScoutRider_55), value);
	}

	inline static int32_t get_offset_of_dispatchLightCavalry_56() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchLightCavalry_56)); }
	inline InputField_t3762917431 * get_dispatchLightCavalry_56() const { return ___dispatchLightCavalry_56; }
	inline InputField_t3762917431 ** get_address_of_dispatchLightCavalry_56() { return &___dispatchLightCavalry_56; }
	inline void set_dispatchLightCavalry_56(InputField_t3762917431 * value)
	{
		___dispatchLightCavalry_56 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchLightCavalry_56), value);
	}

	inline static int32_t get_offset_of_dispatchHeavyCavalry_57() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchHeavyCavalry_57)); }
	inline InputField_t3762917431 * get_dispatchHeavyCavalry_57() const { return ___dispatchHeavyCavalry_57; }
	inline InputField_t3762917431 ** get_address_of_dispatchHeavyCavalry_57() { return &___dispatchHeavyCavalry_57; }
	inline void set_dispatchHeavyCavalry_57(InputField_t3762917431 * value)
	{
		___dispatchHeavyCavalry_57 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchHeavyCavalry_57), value);
	}

	inline static int32_t get_offset_of_dispatchArcher_58() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchArcher_58)); }
	inline InputField_t3762917431 * get_dispatchArcher_58() const { return ___dispatchArcher_58; }
	inline InputField_t3762917431 ** get_address_of_dispatchArcher_58() { return &___dispatchArcher_58; }
	inline void set_dispatchArcher_58(InputField_t3762917431 * value)
	{
		___dispatchArcher_58 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchArcher_58), value);
	}

	inline static int32_t get_offset_of_dispatchArcherRider_59() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchArcherRider_59)); }
	inline InputField_t3762917431 * get_dispatchArcherRider_59() const { return ___dispatchArcherRider_59; }
	inline InputField_t3762917431 ** get_address_of_dispatchArcherRider_59() { return &___dispatchArcherRider_59; }
	inline void set_dispatchArcherRider_59(InputField_t3762917431 * value)
	{
		___dispatchArcherRider_59 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchArcherRider_59), value);
	}

	inline static int32_t get_offset_of_dispatchHollyman_60() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchHollyman_60)); }
	inline InputField_t3762917431 * get_dispatchHollyman_60() const { return ___dispatchHollyman_60; }
	inline InputField_t3762917431 ** get_address_of_dispatchHollyman_60() { return &___dispatchHollyman_60; }
	inline void set_dispatchHollyman_60(InputField_t3762917431 * value)
	{
		___dispatchHollyman_60 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchHollyman_60), value);
	}

	inline static int32_t get_offset_of_dispatchWagon_61() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchWagon_61)); }
	inline InputField_t3762917431 * get_dispatchWagon_61() const { return ___dispatchWagon_61; }
	inline InputField_t3762917431 ** get_address_of_dispatchWagon_61() { return &___dispatchWagon_61; }
	inline void set_dispatchWagon_61(InputField_t3762917431 * value)
	{
		___dispatchWagon_61 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchWagon_61), value);
	}

	inline static int32_t get_offset_of_dispatchTrebuchet_62() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchTrebuchet_62)); }
	inline InputField_t3762917431 * get_dispatchTrebuchet_62() const { return ___dispatchTrebuchet_62; }
	inline InputField_t3762917431 ** get_address_of_dispatchTrebuchet_62() { return &___dispatchTrebuchet_62; }
	inline void set_dispatchTrebuchet_62(InputField_t3762917431 * value)
	{
		___dispatchTrebuchet_62 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchTrebuchet_62), value);
	}

	inline static int32_t get_offset_of_dispatchSiegeTower_63() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchSiegeTower_63)); }
	inline InputField_t3762917431 * get_dispatchSiegeTower_63() const { return ___dispatchSiegeTower_63; }
	inline InputField_t3762917431 ** get_address_of_dispatchSiegeTower_63() { return &___dispatchSiegeTower_63; }
	inline void set_dispatchSiegeTower_63(InputField_t3762917431 * value)
	{
		___dispatchSiegeTower_63 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSiegeTower_63), value);
	}

	inline static int32_t get_offset_of_dispatchBatteringRam_64() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchBatteringRam_64)); }
	inline InputField_t3762917431 * get_dispatchBatteringRam_64() const { return ___dispatchBatteringRam_64; }
	inline InputField_t3762917431 ** get_address_of_dispatchBatteringRam_64() { return &___dispatchBatteringRam_64; }
	inline void set_dispatchBatteringRam_64(InputField_t3762917431 * value)
	{
		___dispatchBatteringRam_64 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchBatteringRam_64), value);
	}

	inline static int32_t get_offset_of_dispatchBallista_65() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchBallista_65)); }
	inline InputField_t3762917431 * get_dispatchBallista_65() const { return ___dispatchBallista_65; }
	inline InputField_t3762917431 ** get_address_of_dispatchBallista_65() { return &___dispatchBallista_65; }
	inline void set_dispatchBallista_65(InputField_t3762917431 * value)
	{
		___dispatchBallista_65 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchBallista_65), value);
	}

	inline static int32_t get_offset_of_attackType_66() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___attackType_66)); }
	inline Dropdown_t2274391225 * get_attackType_66() const { return ___attackType_66; }
	inline Dropdown_t2274391225 ** get_address_of_attackType_66() { return &___attackType_66; }
	inline void set_attackType_66(Dropdown_t2274391225 * value)
	{
		___attackType_66 = value;
		Il2CppCodeGenWriteBarrier((&___attackType_66), value);
	}

	inline static int32_t get_offset_of_knights_67() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___knights_67)); }
	inline Dropdown_t2274391225 * get_knights_67() const { return ___knights_67; }
	inline Dropdown_t2274391225 ** get_address_of_knights_67() { return &___knights_67; }
	inline void set_knights_67(Dropdown_t2274391225 * value)
	{
		___knights_67 = value;
		Il2CppCodeGenWriteBarrier((&___knights_67), value);
	}

	inline static int32_t get_offset_of_dispatchHour_68() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchHour_68)); }
	inline InputField_t3762917431 * get_dispatchHour_68() const { return ___dispatchHour_68; }
	inline InputField_t3762917431 ** get_address_of_dispatchHour_68() { return &___dispatchHour_68; }
	inline void set_dispatchHour_68(InputField_t3762917431 * value)
	{
		___dispatchHour_68 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchHour_68), value);
	}

	inline static int32_t get_offset_of_dispatchMinute_69() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___dispatchMinute_69)); }
	inline InputField_t3762917431 * get_dispatchMinute_69() const { return ___dispatchMinute_69; }
	inline InputField_t3762917431 ** get_address_of_dispatchMinute_69() { return &___dispatchMinute_69; }
	inline void set_dispatchMinute_69(InputField_t3762917431 * value)
	{
		___dispatchMinute_69 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchMinute_69), value);
	}

	inline static int32_t get_offset_of_xCoord_70() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___xCoord_70)); }
	inline Text_t1901882714 * get_xCoord_70() const { return ___xCoord_70; }
	inline Text_t1901882714 ** get_address_of_xCoord_70() { return &___xCoord_70; }
	inline void set_xCoord_70(Text_t1901882714 * value)
	{
		___xCoord_70 = value;
		Il2CppCodeGenWriteBarrier((&___xCoord_70), value);
	}

	inline static int32_t get_offset_of_yCoord_71() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___yCoord_71)); }
	inline Text_t1901882714 * get_yCoord_71() const { return ___yCoord_71; }
	inline Text_t1901882714 ** get_address_of_yCoord_71() { return &___yCoord_71; }
	inline void set_yCoord_71(Text_t1901882714 * value)
	{
		___yCoord_71 = value;
		Il2CppCodeGenWriteBarrier((&___yCoord_71), value);
	}

	inline static int32_t get_offset_of_xInput_72() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___xInput_72)); }
	inline InputField_t3762917431 * get_xInput_72() const { return ___xInput_72; }
	inline InputField_t3762917431 ** get_address_of_xInput_72() { return &___xInput_72; }
	inline void set_xInput_72(InputField_t3762917431 * value)
	{
		___xInput_72 = value;
		Il2CppCodeGenWriteBarrier((&___xInput_72), value);
	}

	inline static int32_t get_offset_of_yInput_73() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___yInput_73)); }
	inline InputField_t3762917431 * get_yInput_73() const { return ___yInput_73; }
	inline InputField_t3762917431 ** get_address_of_yInput_73() { return &___yInput_73; }
	inline void set_yInput_73(InputField_t3762917431 * value)
	{
		___yInput_73 = value;
		Il2CppCodeGenWriteBarrier((&___yInput_73), value);
	}

	inline static int32_t get_offset_of_foodPrice_74() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___foodPrice_74)); }
	inline InputField_t3762917431 * get_foodPrice_74() const { return ___foodPrice_74; }
	inline InputField_t3762917431 ** get_address_of_foodPrice_74() { return &___foodPrice_74; }
	inline void set_foodPrice_74(InputField_t3762917431 * value)
	{
		___foodPrice_74 = value;
		Il2CppCodeGenWriteBarrier((&___foodPrice_74), value);
	}

	inline static int32_t get_offset_of_loadVacancy_75() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___loadVacancy_75)); }
	inline InputField_t3762917431 * get_loadVacancy_75() const { return ___loadVacancy_75; }
	inline InputField_t3762917431 ** get_address_of_loadVacancy_75() { return &___loadVacancy_75; }
	inline void set_loadVacancy_75(InputField_t3762917431 * value)
	{
		___loadVacancy_75 = value;
		Il2CppCodeGenWriteBarrier((&___loadVacancy_75), value);
	}

	inline static int32_t get_offset_of_tripTime_76() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___tripTime_76)); }
	inline InputField_t3762917431 * get_tripTime_76() const { return ___tripTime_76; }
	inline InputField_t3762917431 ** get_address_of_tripTime_76() { return &___tripTime_76; }
	inline void set_tripTime_76(InputField_t3762917431 * value)
	{
		___tripTime_76 = value;
		Il2CppCodeGenWriteBarrier((&___tripTime_76), value);
	}

	inline static int32_t get_offset_of_fromMap_77() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___fromMap_77)); }
	inline bool get_fromMap_77() const { return ___fromMap_77; }
	inline bool* get_address_of_fromMap_77() { return &___fromMap_77; }
	inline void set_fromMap_77(bool value)
	{
		___fromMap_77 = value;
	}

	inline static int32_t get_offset_of_targetField_78() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___targetField_78)); }
	inline WorldFieldModel_t2417974361 * get_targetField_78() const { return ___targetField_78; }
	inline WorldFieldModel_t2417974361 ** get_address_of_targetField_78() { return &___targetField_78; }
	inline void set_targetField_78(WorldFieldModel_t2417974361 * value)
	{
		___targetField_78 = value;
		Il2CppCodeGenWriteBarrier((&___targetField_78), value);
	}

	inline static int32_t get_offset_of_generals_79() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___generals_79)); }
	inline List_1_t2209824963 * get_generals_79() const { return ___generals_79; }
	inline List_1_t2209824963 ** get_address_of_generals_79() { return &___generals_79; }
	inline void set_generals_79(List_1_t2209824963 * value)
	{
		___generals_79 = value;
		Il2CppCodeGenWriteBarrier((&___generals_79), value);
	}

	inline static int32_t get_offset_of_army_80() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___army_80)); }
	inline UnitsModel_t3847956398 * get_army_80() const { return ___army_80; }
	inline UnitsModel_t3847956398 ** get_address_of_army_80() { return &___army_80; }
	inline void set_army_80(UnitsModel_t3847956398 * value)
	{
		___army_80 = value;
		Il2CppCodeGenWriteBarrier((&___army_80), value);
	}

	inline static int32_t get_offset_of_resources_81() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___resources_81)); }
	inline ResourcesModel_t2533508513 * get_resources_81() const { return ___resources_81; }
	inline ResourcesModel_t2533508513 ** get_address_of_resources_81() { return &___resources_81; }
	inline void set_resources_81(ResourcesModel_t2533508513 * value)
	{
		___resources_81 = value;
		Il2CppCodeGenWriteBarrier((&___resources_81), value);
	}

	inline static int32_t get_offset_of_calculated_82() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___calculated_82)); }
	inline bool get_calculated_82() const { return ___calculated_82; }
	inline bool* get_address_of_calculated_82() { return &___calculated_82; }
	inline void set_calculated_82(bool value)
	{
		___calculated_82 = value;
	}

	inline static int32_t get_offset_of_onGotParams_83() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___onGotParams_83)); }
	inline SimpleEvent_t129249603 * get_onGotParams_83() const { return ___onGotParams_83; }
	inline SimpleEvent_t129249603 ** get_address_of_onGotParams_83() { return &___onGotParams_83; }
	inline void set_onGotParams_83(SimpleEvent_t129249603 * value)
	{
		___onGotParams_83 = value;
		Il2CppCodeGenWriteBarrier((&___onGotParams_83), value);
	}

	inline static int32_t get_offset_of_attackTypes_84() { return static_cast<int32_t>(offsetof(DispatchManager_t670234083, ___attackTypes_84)); }
	inline List_1_t3319525431 * get_attackTypes_84() const { return ___attackTypes_84; }
	inline List_1_t3319525431 ** get_address_of_attackTypes_84() { return &___attackTypes_84; }
	inline void set_attackTypes_84(List_1_t3319525431 * value)
	{
		___attackTypes_84 = value;
		Il2CppCodeGenWriteBarrier((&___attackTypes_84), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHMANAGER_T670234083_H
#ifndef KNIGHTINFOCONTENTMANAGER_T331339578_H
#define KNIGHTINFOCONTENTMANAGER_T331339578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnightInfoContentManager
struct  KnightInfoContentManager_t331339578  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text KnightInfoContentManager::knightName
	Text_t1901882714 * ___knightName_2;
	// UnityEngine.UI.Text KnightInfoContentManager::knightPoint
	Text_t1901882714 * ___knightPoint_3;
	// UnityEngine.UI.Text KnightInfoContentManager::knightMarches
	Text_t1901882714 * ___knightMarches_4;
	// UnityEngine.UI.Text KnightInfoContentManager::knightPolitics
	Text_t1901882714 * ___knightPolitics_5;
	// UnityEngine.UI.Text KnightInfoContentManager::knightSpeed
	Text_t1901882714 * ___knightSpeed_6;
	// UnityEngine.UI.Text KnightInfoContentManager::knightLevel
	Text_t1901882714 * ___knightLevel_7;
	// UnityEngine.UI.Text KnightInfoContentManager::knightLoyalty
	Text_t1901882714 * ___knightLoyalty_8;
	// UnityEngine.UI.Text KnightInfoContentManager::knightSalary
	Text_t1901882714 * ___knightSalary_9;
	// ArmyGeneralModel KnightInfoContentManager::general
	ArmyGeneralModel_t737750221 * ___general_10;
	// System.Int64 KnightInfoContentManager::increaseMarches
	int64_t ___increaseMarches_11;
	// System.Int64 KnightInfoContentManager::increasePolitics
	int64_t ___increasePolitics_12;
	// System.Int64 KnightInfoContentManager::increaseSpeed
	int64_t ___increaseSpeed_13;
	// System.Int64 KnightInfoContentManager::increaseSalary
	int64_t ___increaseSalary_14;
	// System.Int64 KnightInfoContentManager::increaseLoyalty
	int64_t ___increaseLoyalty_15;
	// System.Int64 KnightInfoContentManager::increaseLevel
	int64_t ___increaseLevel_16;
	// System.Int64 KnightInfoContentManager::decreaseExperience
	int64_t ___decreaseExperience_17;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightName_2)); }
	inline Text_t1901882714 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t1901882714 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t1901882714 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier((&___knightName_2), value);
	}

	inline static int32_t get_offset_of_knightPoint_3() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightPoint_3)); }
	inline Text_t1901882714 * get_knightPoint_3() const { return ___knightPoint_3; }
	inline Text_t1901882714 ** get_address_of_knightPoint_3() { return &___knightPoint_3; }
	inline void set_knightPoint_3(Text_t1901882714 * value)
	{
		___knightPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___knightPoint_3), value);
	}

	inline static int32_t get_offset_of_knightMarches_4() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightMarches_4)); }
	inline Text_t1901882714 * get_knightMarches_4() const { return ___knightMarches_4; }
	inline Text_t1901882714 ** get_address_of_knightMarches_4() { return &___knightMarches_4; }
	inline void set_knightMarches_4(Text_t1901882714 * value)
	{
		___knightMarches_4 = value;
		Il2CppCodeGenWriteBarrier((&___knightMarches_4), value);
	}

	inline static int32_t get_offset_of_knightPolitics_5() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightPolitics_5)); }
	inline Text_t1901882714 * get_knightPolitics_5() const { return ___knightPolitics_5; }
	inline Text_t1901882714 ** get_address_of_knightPolitics_5() { return &___knightPolitics_5; }
	inline void set_knightPolitics_5(Text_t1901882714 * value)
	{
		___knightPolitics_5 = value;
		Il2CppCodeGenWriteBarrier((&___knightPolitics_5), value);
	}

	inline static int32_t get_offset_of_knightSpeed_6() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightSpeed_6)); }
	inline Text_t1901882714 * get_knightSpeed_6() const { return ___knightSpeed_6; }
	inline Text_t1901882714 ** get_address_of_knightSpeed_6() { return &___knightSpeed_6; }
	inline void set_knightSpeed_6(Text_t1901882714 * value)
	{
		___knightSpeed_6 = value;
		Il2CppCodeGenWriteBarrier((&___knightSpeed_6), value);
	}

	inline static int32_t get_offset_of_knightLevel_7() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightLevel_7)); }
	inline Text_t1901882714 * get_knightLevel_7() const { return ___knightLevel_7; }
	inline Text_t1901882714 ** get_address_of_knightLevel_7() { return &___knightLevel_7; }
	inline void set_knightLevel_7(Text_t1901882714 * value)
	{
		___knightLevel_7 = value;
		Il2CppCodeGenWriteBarrier((&___knightLevel_7), value);
	}

	inline static int32_t get_offset_of_knightLoyalty_8() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightLoyalty_8)); }
	inline Text_t1901882714 * get_knightLoyalty_8() const { return ___knightLoyalty_8; }
	inline Text_t1901882714 ** get_address_of_knightLoyalty_8() { return &___knightLoyalty_8; }
	inline void set_knightLoyalty_8(Text_t1901882714 * value)
	{
		___knightLoyalty_8 = value;
		Il2CppCodeGenWriteBarrier((&___knightLoyalty_8), value);
	}

	inline static int32_t get_offset_of_knightSalary_9() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___knightSalary_9)); }
	inline Text_t1901882714 * get_knightSalary_9() const { return ___knightSalary_9; }
	inline Text_t1901882714 ** get_address_of_knightSalary_9() { return &___knightSalary_9; }
	inline void set_knightSalary_9(Text_t1901882714 * value)
	{
		___knightSalary_9 = value;
		Il2CppCodeGenWriteBarrier((&___knightSalary_9), value);
	}

	inline static int32_t get_offset_of_general_10() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___general_10)); }
	inline ArmyGeneralModel_t737750221 * get_general_10() const { return ___general_10; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_general_10() { return &___general_10; }
	inline void set_general_10(ArmyGeneralModel_t737750221 * value)
	{
		___general_10 = value;
		Il2CppCodeGenWriteBarrier((&___general_10), value);
	}

	inline static int32_t get_offset_of_increaseMarches_11() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increaseMarches_11)); }
	inline int64_t get_increaseMarches_11() const { return ___increaseMarches_11; }
	inline int64_t* get_address_of_increaseMarches_11() { return &___increaseMarches_11; }
	inline void set_increaseMarches_11(int64_t value)
	{
		___increaseMarches_11 = value;
	}

	inline static int32_t get_offset_of_increasePolitics_12() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increasePolitics_12)); }
	inline int64_t get_increasePolitics_12() const { return ___increasePolitics_12; }
	inline int64_t* get_address_of_increasePolitics_12() { return &___increasePolitics_12; }
	inline void set_increasePolitics_12(int64_t value)
	{
		___increasePolitics_12 = value;
	}

	inline static int32_t get_offset_of_increaseSpeed_13() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increaseSpeed_13)); }
	inline int64_t get_increaseSpeed_13() const { return ___increaseSpeed_13; }
	inline int64_t* get_address_of_increaseSpeed_13() { return &___increaseSpeed_13; }
	inline void set_increaseSpeed_13(int64_t value)
	{
		___increaseSpeed_13 = value;
	}

	inline static int32_t get_offset_of_increaseSalary_14() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increaseSalary_14)); }
	inline int64_t get_increaseSalary_14() const { return ___increaseSalary_14; }
	inline int64_t* get_address_of_increaseSalary_14() { return &___increaseSalary_14; }
	inline void set_increaseSalary_14(int64_t value)
	{
		___increaseSalary_14 = value;
	}

	inline static int32_t get_offset_of_increaseLoyalty_15() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increaseLoyalty_15)); }
	inline int64_t get_increaseLoyalty_15() const { return ___increaseLoyalty_15; }
	inline int64_t* get_address_of_increaseLoyalty_15() { return &___increaseLoyalty_15; }
	inline void set_increaseLoyalty_15(int64_t value)
	{
		___increaseLoyalty_15 = value;
	}

	inline static int32_t get_offset_of_increaseLevel_16() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___increaseLevel_16)); }
	inline int64_t get_increaseLevel_16() const { return ___increaseLevel_16; }
	inline int64_t* get_address_of_increaseLevel_16() { return &___increaseLevel_16; }
	inline void set_increaseLevel_16(int64_t value)
	{
		___increaseLevel_16 = value;
	}

	inline static int32_t get_offset_of_decreaseExperience_17() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t331339578, ___decreaseExperience_17)); }
	inline int64_t get_decreaseExperience_17() const { return ___decreaseExperience_17; }
	inline int64_t* get_address_of_decreaseExperience_17() { return &___decreaseExperience_17; }
	inline void set_decreaseExperience_17(int64_t value)
	{
		___decreaseExperience_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNIGHTINFOCONTENTMANAGER_T331339578_H
#ifndef LANGUAGECHANGERCONTENTMANAGER_T2232597662_H
#define LANGUAGECHANGERCONTENTMANAGER_T2232597662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageChangerContentManager
struct  LanguageChangerContentManager_t2232597662  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown LanguageChangerContentManager::language
	Dropdown_t2274391225 * ___language_2;
	// System.Int32 LanguageChangerContentManager::previousValue
	int32_t ___previousValue_3;

public:
	inline static int32_t get_offset_of_language_2() { return static_cast<int32_t>(offsetof(LanguageChangerContentManager_t2232597662, ___language_2)); }
	inline Dropdown_t2274391225 * get_language_2() const { return ___language_2; }
	inline Dropdown_t2274391225 ** get_address_of_language_2() { return &___language_2; }
	inline void set_language_2(Dropdown_t2274391225 * value)
	{
		___language_2 = value;
		Il2CppCodeGenWriteBarrier((&___language_2), value);
	}

	inline static int32_t get_offset_of_previousValue_3() { return static_cast<int32_t>(offsetof(LanguageChangerContentManager_t2232597662, ___previousValue_3)); }
	inline int32_t get_previousValue_3() const { return ___previousValue_3; }
	inline int32_t* get_address_of_previousValue_3() { return &___previousValue_3; }
	inline void set_previousValue_3(int32_t value)
	{
		___previousValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGECHANGERCONTENTMANAGER_T2232597662_H
#ifndef HELPCONTENTCHANGER_T1793113890_H
#define HELPCONTENTCHANGER_T1793113890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpContentChanger
struct  HelpContentChanger_t1793113890  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HelpContentChanger::homeContent
	GameObject_t1113636619 * ___homeContent_2;
	// UnityEngine.GameObject HelpContentChanger::unitsContent
	GameObject_t1113636619 * ___unitsContent_3;
	// UnityEngine.GameObject HelpContentChanger::hierarchyContent
	GameObject_t1113636619 * ___hierarchyContent_4;
	// UnityEngine.GameObject HelpContentChanger::promotionContent
	GameObject_t1113636619 * ___promotionContent_5;
	// UnityEngine.GameObject HelpContentChanger::engSteps
	GameObject_t1113636619 * ___engSteps_6;
	// UnityEngine.GameObject HelpContentChanger::arSteps
	GameObject_t1113636619 * ___arSteps_7;
	// UnityEngine.UI.Image HelpContentChanger::homeButtonImage
	Image_t2670269651 * ___homeButtonImage_8;
	// UnityEngine.UI.Image HelpContentChanger::unitsButtonImage
	Image_t2670269651 * ___unitsButtonImage_9;
	// UnityEngine.UI.Image HelpContentChanger::hierarchyButtonImage
	Image_t2670269651 * ___hierarchyButtonImage_10;
	// UnityEngine.UI.Image HelpContentChanger::promotionButtonImage
	Image_t2670269651 * ___promotionButtonImage_11;
	// UnityEngine.Sprite HelpContentChanger::tabSelected
	Sprite_t280657092 * ___tabSelected_12;
	// UnityEngine.Sprite HelpContentChanger::tabUnselected
	Sprite_t280657092 * ___tabUnselected_13;

public:
	inline static int32_t get_offset_of_homeContent_2() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___homeContent_2)); }
	inline GameObject_t1113636619 * get_homeContent_2() const { return ___homeContent_2; }
	inline GameObject_t1113636619 ** get_address_of_homeContent_2() { return &___homeContent_2; }
	inline void set_homeContent_2(GameObject_t1113636619 * value)
	{
		___homeContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___homeContent_2), value);
	}

	inline static int32_t get_offset_of_unitsContent_3() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___unitsContent_3)); }
	inline GameObject_t1113636619 * get_unitsContent_3() const { return ___unitsContent_3; }
	inline GameObject_t1113636619 ** get_address_of_unitsContent_3() { return &___unitsContent_3; }
	inline void set_unitsContent_3(GameObject_t1113636619 * value)
	{
		___unitsContent_3 = value;
		Il2CppCodeGenWriteBarrier((&___unitsContent_3), value);
	}

	inline static int32_t get_offset_of_hierarchyContent_4() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___hierarchyContent_4)); }
	inline GameObject_t1113636619 * get_hierarchyContent_4() const { return ___hierarchyContent_4; }
	inline GameObject_t1113636619 ** get_address_of_hierarchyContent_4() { return &___hierarchyContent_4; }
	inline void set_hierarchyContent_4(GameObject_t1113636619 * value)
	{
		___hierarchyContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___hierarchyContent_4), value);
	}

	inline static int32_t get_offset_of_promotionContent_5() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___promotionContent_5)); }
	inline GameObject_t1113636619 * get_promotionContent_5() const { return ___promotionContent_5; }
	inline GameObject_t1113636619 ** get_address_of_promotionContent_5() { return &___promotionContent_5; }
	inline void set_promotionContent_5(GameObject_t1113636619 * value)
	{
		___promotionContent_5 = value;
		Il2CppCodeGenWriteBarrier((&___promotionContent_5), value);
	}

	inline static int32_t get_offset_of_engSteps_6() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___engSteps_6)); }
	inline GameObject_t1113636619 * get_engSteps_6() const { return ___engSteps_6; }
	inline GameObject_t1113636619 ** get_address_of_engSteps_6() { return &___engSteps_6; }
	inline void set_engSteps_6(GameObject_t1113636619 * value)
	{
		___engSteps_6 = value;
		Il2CppCodeGenWriteBarrier((&___engSteps_6), value);
	}

	inline static int32_t get_offset_of_arSteps_7() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___arSteps_7)); }
	inline GameObject_t1113636619 * get_arSteps_7() const { return ___arSteps_7; }
	inline GameObject_t1113636619 ** get_address_of_arSteps_7() { return &___arSteps_7; }
	inline void set_arSteps_7(GameObject_t1113636619 * value)
	{
		___arSteps_7 = value;
		Il2CppCodeGenWriteBarrier((&___arSteps_7), value);
	}

	inline static int32_t get_offset_of_homeButtonImage_8() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___homeButtonImage_8)); }
	inline Image_t2670269651 * get_homeButtonImage_8() const { return ___homeButtonImage_8; }
	inline Image_t2670269651 ** get_address_of_homeButtonImage_8() { return &___homeButtonImage_8; }
	inline void set_homeButtonImage_8(Image_t2670269651 * value)
	{
		___homeButtonImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___homeButtonImage_8), value);
	}

	inline static int32_t get_offset_of_unitsButtonImage_9() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___unitsButtonImage_9)); }
	inline Image_t2670269651 * get_unitsButtonImage_9() const { return ___unitsButtonImage_9; }
	inline Image_t2670269651 ** get_address_of_unitsButtonImage_9() { return &___unitsButtonImage_9; }
	inline void set_unitsButtonImage_9(Image_t2670269651 * value)
	{
		___unitsButtonImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___unitsButtonImage_9), value);
	}

	inline static int32_t get_offset_of_hierarchyButtonImage_10() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___hierarchyButtonImage_10)); }
	inline Image_t2670269651 * get_hierarchyButtonImage_10() const { return ___hierarchyButtonImage_10; }
	inline Image_t2670269651 ** get_address_of_hierarchyButtonImage_10() { return &___hierarchyButtonImage_10; }
	inline void set_hierarchyButtonImage_10(Image_t2670269651 * value)
	{
		___hierarchyButtonImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___hierarchyButtonImage_10), value);
	}

	inline static int32_t get_offset_of_promotionButtonImage_11() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___promotionButtonImage_11)); }
	inline Image_t2670269651 * get_promotionButtonImage_11() const { return ___promotionButtonImage_11; }
	inline Image_t2670269651 ** get_address_of_promotionButtonImage_11() { return &___promotionButtonImage_11; }
	inline void set_promotionButtonImage_11(Image_t2670269651 * value)
	{
		___promotionButtonImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___promotionButtonImage_11), value);
	}

	inline static int32_t get_offset_of_tabSelected_12() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___tabSelected_12)); }
	inline Sprite_t280657092 * get_tabSelected_12() const { return ___tabSelected_12; }
	inline Sprite_t280657092 ** get_address_of_tabSelected_12() { return &___tabSelected_12; }
	inline void set_tabSelected_12(Sprite_t280657092 * value)
	{
		___tabSelected_12 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelected_12), value);
	}

	inline static int32_t get_offset_of_tabUnselected_13() { return static_cast<int32_t>(offsetof(HelpContentChanger_t1793113890, ___tabUnselected_13)); }
	inline Sprite_t280657092 * get_tabUnselected_13() const { return ___tabUnselected_13; }
	inline Sprite_t280657092 ** get_address_of_tabUnselected_13() { return &___tabUnselected_13; }
	inline void set_tabUnselected_13(Sprite_t280657092 * value)
	{
		___tabUnselected_13 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselected_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPCONTENTCHANGER_T1793113890_H
#ifndef FINANCIALCONTENTCHANGER_T4019109810_H
#define FINANCIALCONTENTCHANGER_T4019109810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger
struct  FinancialContentChanger_t4019109810  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text FinancialContentChanger::contentLabel
	Text_t1901882714 * ___contentLabel_2;
	// UnityEngine.UI.Text FinancialContentChanger::shillingsCount
	Text_t1901882714 * ___shillingsCount_3;
	// UnityEngine.UI.Text FinancialContentChanger::totalPurchases
	Text_t1901882714 * ___totalPurchases_4;
	// UnityEngine.UI.Text FinancialContentChanger::totalLabel
	Text_t1901882714 * ___totalLabel_5;
	// UnityEngine.UI.Text FinancialContentChanger::tableColumn
	Text_t1901882714 * ___tableColumn_6;
	// UnityEngine.Sprite FinancialContentChanger::tabSelectedSprite
	Sprite_t280657092 * ___tabSelectedSprite_7;
	// UnityEngine.Sprite FinancialContentChanger::tabUnselectedSprite
	Sprite_t280657092 * ___tabUnselectedSprite_8;
	// UnityEngine.UI.Image[] FinancialContentChanger::tabs
	ImageU5BU5D_t2439009922* ___tabs_9;
	// System.Collections.ArrayList FinancialContentChanger::boughtItems
	ArrayList_t2718874744 * ___boughtItems_10;
	// System.Collections.ArrayList FinancialContentChanger::soldItems
	ArrayList_t2718874744 * ___soldItems_11;
	// System.Collections.ArrayList FinancialContentChanger::offeredItems
	ArrayList_t2718874744 * ___offeredItems_12;
	// SimpleEvent FinancialContentChanger::gotOffers
	SimpleEvent_t129249603 * ___gotOffers_13;
	// SimpleEvent FinancialContentChanger::offerCanceled
	SimpleEvent_t129249603 * ___offerCanceled_14;
	// FinanceTableController FinancialContentChanger::tableController
	FinanceTableController_t3435833683 * ___tableController_15;
	// System.String FinancialContentChanger::content
	String_t* ___content_16;

public:
	inline static int32_t get_offset_of_contentLabel_2() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___contentLabel_2)); }
	inline Text_t1901882714 * get_contentLabel_2() const { return ___contentLabel_2; }
	inline Text_t1901882714 ** get_address_of_contentLabel_2() { return &___contentLabel_2; }
	inline void set_contentLabel_2(Text_t1901882714 * value)
	{
		___contentLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentLabel_2), value);
	}

	inline static int32_t get_offset_of_shillingsCount_3() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___shillingsCount_3)); }
	inline Text_t1901882714 * get_shillingsCount_3() const { return ___shillingsCount_3; }
	inline Text_t1901882714 ** get_address_of_shillingsCount_3() { return &___shillingsCount_3; }
	inline void set_shillingsCount_3(Text_t1901882714 * value)
	{
		___shillingsCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___shillingsCount_3), value);
	}

	inline static int32_t get_offset_of_totalPurchases_4() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___totalPurchases_4)); }
	inline Text_t1901882714 * get_totalPurchases_4() const { return ___totalPurchases_4; }
	inline Text_t1901882714 ** get_address_of_totalPurchases_4() { return &___totalPurchases_4; }
	inline void set_totalPurchases_4(Text_t1901882714 * value)
	{
		___totalPurchases_4 = value;
		Il2CppCodeGenWriteBarrier((&___totalPurchases_4), value);
	}

	inline static int32_t get_offset_of_totalLabel_5() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___totalLabel_5)); }
	inline Text_t1901882714 * get_totalLabel_5() const { return ___totalLabel_5; }
	inline Text_t1901882714 ** get_address_of_totalLabel_5() { return &___totalLabel_5; }
	inline void set_totalLabel_5(Text_t1901882714 * value)
	{
		___totalLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___totalLabel_5), value);
	}

	inline static int32_t get_offset_of_tableColumn_6() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___tableColumn_6)); }
	inline Text_t1901882714 * get_tableColumn_6() const { return ___tableColumn_6; }
	inline Text_t1901882714 ** get_address_of_tableColumn_6() { return &___tableColumn_6; }
	inline void set_tableColumn_6(Text_t1901882714 * value)
	{
		___tableColumn_6 = value;
		Il2CppCodeGenWriteBarrier((&___tableColumn_6), value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_7() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___tabSelectedSprite_7)); }
	inline Sprite_t280657092 * get_tabSelectedSprite_7() const { return ___tabSelectedSprite_7; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedSprite_7() { return &___tabSelectedSprite_7; }
	inline void set_tabSelectedSprite_7(Sprite_t280657092 * value)
	{
		___tabSelectedSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedSprite_7), value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_8() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___tabUnselectedSprite_8)); }
	inline Sprite_t280657092 * get_tabUnselectedSprite_8() const { return ___tabUnselectedSprite_8; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedSprite_8() { return &___tabUnselectedSprite_8; }
	inline void set_tabUnselectedSprite_8(Sprite_t280657092 * value)
	{
		___tabUnselectedSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedSprite_8), value);
	}

	inline static int32_t get_offset_of_tabs_9() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___tabs_9)); }
	inline ImageU5BU5D_t2439009922* get_tabs_9() const { return ___tabs_9; }
	inline ImageU5BU5D_t2439009922** get_address_of_tabs_9() { return &___tabs_9; }
	inline void set_tabs_9(ImageU5BU5D_t2439009922* value)
	{
		___tabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_9), value);
	}

	inline static int32_t get_offset_of_boughtItems_10() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___boughtItems_10)); }
	inline ArrayList_t2718874744 * get_boughtItems_10() const { return ___boughtItems_10; }
	inline ArrayList_t2718874744 ** get_address_of_boughtItems_10() { return &___boughtItems_10; }
	inline void set_boughtItems_10(ArrayList_t2718874744 * value)
	{
		___boughtItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___boughtItems_10), value);
	}

	inline static int32_t get_offset_of_soldItems_11() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___soldItems_11)); }
	inline ArrayList_t2718874744 * get_soldItems_11() const { return ___soldItems_11; }
	inline ArrayList_t2718874744 ** get_address_of_soldItems_11() { return &___soldItems_11; }
	inline void set_soldItems_11(ArrayList_t2718874744 * value)
	{
		___soldItems_11 = value;
		Il2CppCodeGenWriteBarrier((&___soldItems_11), value);
	}

	inline static int32_t get_offset_of_offeredItems_12() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___offeredItems_12)); }
	inline ArrayList_t2718874744 * get_offeredItems_12() const { return ___offeredItems_12; }
	inline ArrayList_t2718874744 ** get_address_of_offeredItems_12() { return &___offeredItems_12; }
	inline void set_offeredItems_12(ArrayList_t2718874744 * value)
	{
		___offeredItems_12 = value;
		Il2CppCodeGenWriteBarrier((&___offeredItems_12), value);
	}

	inline static int32_t get_offset_of_gotOffers_13() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___gotOffers_13)); }
	inline SimpleEvent_t129249603 * get_gotOffers_13() const { return ___gotOffers_13; }
	inline SimpleEvent_t129249603 ** get_address_of_gotOffers_13() { return &___gotOffers_13; }
	inline void set_gotOffers_13(SimpleEvent_t129249603 * value)
	{
		___gotOffers_13 = value;
		Il2CppCodeGenWriteBarrier((&___gotOffers_13), value);
	}

	inline static int32_t get_offset_of_offerCanceled_14() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___offerCanceled_14)); }
	inline SimpleEvent_t129249603 * get_offerCanceled_14() const { return ___offerCanceled_14; }
	inline SimpleEvent_t129249603 ** get_address_of_offerCanceled_14() { return &___offerCanceled_14; }
	inline void set_offerCanceled_14(SimpleEvent_t129249603 * value)
	{
		___offerCanceled_14 = value;
		Il2CppCodeGenWriteBarrier((&___offerCanceled_14), value);
	}

	inline static int32_t get_offset_of_tableController_15() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___tableController_15)); }
	inline FinanceTableController_t3435833683 * get_tableController_15() const { return ___tableController_15; }
	inline FinanceTableController_t3435833683 ** get_address_of_tableController_15() { return &___tableController_15; }
	inline void set_tableController_15(FinanceTableController_t3435833683 * value)
	{
		___tableController_15 = value;
		Il2CppCodeGenWriteBarrier((&___tableController_15), value);
	}

	inline static int32_t get_offset_of_content_16() { return static_cast<int32_t>(offsetof(FinancialContentChanger_t4019109810, ___content_16)); }
	inline String_t* get_content_16() const { return ___content_16; }
	inline String_t** get_address_of_content_16() { return &___content_16; }
	inline void set_content_16(String_t* value)
	{
		___content_16 = value;
		Il2CppCodeGenWriteBarrier((&___content_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINANCIALCONTENTCHANGER_T4019109810_H
#ifndef GENERICBUILDINGINFOCONTENTMANAGER_T1427092153_H
#define GENERICBUILDINGINFOCONTENTMANAGER_T1427092153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericBuildingInfoContentManager
struct  GenericBuildingInfoContentManager_t1427092153  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingName
	Text_t1901882714 * ___buildingName_2;
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingLevel
	Text_t1901882714 * ___buildingLevel_3;
	// UnityEngine.UI.Text GenericBuildingInfoContentManager::buildingDescription
	Text_t1901882714 * ___buildingDescription_4;
	// UnityEngine.GameObject GenericBuildingInfoContentManager::demolishBtn
	GameObject_t1113636619 * ___demolishBtn_5;
	// ConfirmationEvent GenericBuildingInfoContentManager::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_8;

public:
	inline static int32_t get_offset_of_buildingName_2() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153, ___buildingName_2)); }
	inline Text_t1901882714 * get_buildingName_2() const { return ___buildingName_2; }
	inline Text_t1901882714 ** get_address_of_buildingName_2() { return &___buildingName_2; }
	inline void set_buildingName_2(Text_t1901882714 * value)
	{
		___buildingName_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildingName_2), value);
	}

	inline static int32_t get_offset_of_buildingLevel_3() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153, ___buildingLevel_3)); }
	inline Text_t1901882714 * get_buildingLevel_3() const { return ___buildingLevel_3; }
	inline Text_t1901882714 ** get_address_of_buildingLevel_3() { return &___buildingLevel_3; }
	inline void set_buildingLevel_3(Text_t1901882714 * value)
	{
		___buildingLevel_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingLevel_3), value);
	}

	inline static int32_t get_offset_of_buildingDescription_4() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153, ___buildingDescription_4)); }
	inline Text_t1901882714 * get_buildingDescription_4() const { return ___buildingDescription_4; }
	inline Text_t1901882714 ** get_address_of_buildingDescription_4() { return &___buildingDescription_4; }
	inline void set_buildingDescription_4(Text_t1901882714 * value)
	{
		___buildingDescription_4 = value;
		Il2CppCodeGenWriteBarrier((&___buildingDescription_4), value);
	}

	inline static int32_t get_offset_of_demolishBtn_5() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153, ___demolishBtn_5)); }
	inline GameObject_t1113636619 * get_demolishBtn_5() const { return ___demolishBtn_5; }
	inline GameObject_t1113636619 ** get_address_of_demolishBtn_5() { return &___demolishBtn_5; }
	inline void set_demolishBtn_5(GameObject_t1113636619 * value)
	{
		___demolishBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___demolishBtn_5), value);
	}

	inline static int32_t get_offset_of_confirmed_8() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153, ___confirmed_8)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_8() const { return ___confirmed_8; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_8() { return &___confirmed_8; }
	inline void set_confirmed_8(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_8 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_8), value);
	}
};

struct GenericBuildingInfoContentManager_t1427092153_StaticFields
{
public:
	// System.String GenericBuildingInfoContentManager::type
	String_t* ___type_6;
	// System.Int64 GenericBuildingInfoContentManager::pitId
	int64_t ___pitId_7;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153_StaticFields, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}

	inline static int32_t get_offset_of_pitId_7() { return static_cast<int32_t>(offsetof(GenericBuildingInfoContentManager_t1427092153_StaticFields, ___pitId_7)); }
	inline int64_t get_pitId_7() const { return ___pitId_7; }
	inline int64_t* get_address_of_pitId_7() { return &___pitId_7; }
	inline void set_pitId_7(int64_t value)
	{
		___pitId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICBUILDINGINFOCONTENTMANAGER_T1427092153_H
#ifndef COMMANDCENTERMANAGER_T2415216723_H
#define COMMANDCENTERMANAGER_T2415216723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterManager
struct  CommandCenterManager_t2415216723  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CommandCenterManager::cityWorker
	Text_t1901882714 * ___cityWorker_2;
	// UnityEngine.UI.Text CommandCenterManager::citySpy
	Text_t1901882714 * ___citySpy_3;
	// UnityEngine.UI.Text CommandCenterManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_4;
	// UnityEngine.UI.Text CommandCenterManager::citySpearman
	Text_t1901882714 * ___citySpearman_5;
	// UnityEngine.UI.Text CommandCenterManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_6;
	// UnityEngine.UI.Text CommandCenterManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_7;
	// UnityEngine.UI.Text CommandCenterManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_8;
	// UnityEngine.UI.Text CommandCenterManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_9;
	// UnityEngine.UI.Text CommandCenterManager::cityArcher
	Text_t1901882714 * ___cityArcher_10;
	// UnityEngine.UI.Text CommandCenterManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_11;
	// UnityEngine.UI.Text CommandCenterManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_12;
	// UnityEngine.UI.Text CommandCenterManager::cityWagon
	Text_t1901882714 * ___cityWagon_13;
	// UnityEngine.UI.Text CommandCenterManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_14;
	// UnityEngine.UI.Text CommandCenterManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_15;
	// UnityEngine.UI.Text CommandCenterManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_16;
	// UnityEngine.UI.Text CommandCenterManager::cityBallista
	Text_t1901882714 * ___cityBallista_17;

public:
	inline static int32_t get_offset_of_cityWorker_2() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityWorker_2)); }
	inline Text_t1901882714 * get_cityWorker_2() const { return ___cityWorker_2; }
	inline Text_t1901882714 ** get_address_of_cityWorker_2() { return &___cityWorker_2; }
	inline void set_cityWorker_2(Text_t1901882714 * value)
	{
		___cityWorker_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_2), value);
	}

	inline static int32_t get_offset_of_citySpy_3() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___citySpy_3)); }
	inline Text_t1901882714 * get_citySpy_3() const { return ___citySpy_3; }
	inline Text_t1901882714 ** get_address_of_citySpy_3() { return &___citySpy_3; }
	inline void set_citySpy_3(Text_t1901882714 * value)
	{
		___citySpy_3 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_3), value);
	}

	inline static int32_t get_offset_of_citySwordsman_4() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___citySwordsman_4)); }
	inline Text_t1901882714 * get_citySwordsman_4() const { return ___citySwordsman_4; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_4() { return &___citySwordsman_4; }
	inline void set_citySwordsman_4(Text_t1901882714 * value)
	{
		___citySwordsman_4 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_4), value);
	}

	inline static int32_t get_offset_of_citySpearman_5() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___citySpearman_5)); }
	inline Text_t1901882714 * get_citySpearman_5() const { return ___citySpearman_5; }
	inline Text_t1901882714 ** get_address_of_citySpearman_5() { return &___citySpearman_5; }
	inline void set_citySpearman_5(Text_t1901882714 * value)
	{
		___citySpearman_5 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_5), value);
	}

	inline static int32_t get_offset_of_cityPikeman_6() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityPikeman_6)); }
	inline Text_t1901882714 * get_cityPikeman_6() const { return ___cityPikeman_6; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_6() { return &___cityPikeman_6; }
	inline void set_cityPikeman_6(Text_t1901882714 * value)
	{
		___cityPikeman_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_6), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_7() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityScoutRider_7)); }
	inline Text_t1901882714 * get_cityScoutRider_7() const { return ___cityScoutRider_7; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_7() { return &___cityScoutRider_7; }
	inline void set_cityScoutRider_7(Text_t1901882714 * value)
	{
		___cityScoutRider_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_7), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_8() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityLightCavalry_8)); }
	inline Text_t1901882714 * get_cityLightCavalry_8() const { return ___cityLightCavalry_8; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_8() { return &___cityLightCavalry_8; }
	inline void set_cityLightCavalry_8(Text_t1901882714 * value)
	{
		___cityLightCavalry_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_8), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_9() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityHeavyCavalry_9)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_9() const { return ___cityHeavyCavalry_9; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_9() { return &___cityHeavyCavalry_9; }
	inline void set_cityHeavyCavalry_9(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_9), value);
	}

	inline static int32_t get_offset_of_cityArcher_10() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityArcher_10)); }
	inline Text_t1901882714 * get_cityArcher_10() const { return ___cityArcher_10; }
	inline Text_t1901882714 ** get_address_of_cityArcher_10() { return &___cityArcher_10; }
	inline void set_cityArcher_10(Text_t1901882714 * value)
	{
		___cityArcher_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_10), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_11() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityArcherRider_11)); }
	inline Text_t1901882714 * get_cityArcherRider_11() const { return ___cityArcherRider_11; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_11() { return &___cityArcherRider_11; }
	inline void set_cityArcherRider_11(Text_t1901882714 * value)
	{
		___cityArcherRider_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_11), value);
	}

	inline static int32_t get_offset_of_cityHollyman_12() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityHollyman_12)); }
	inline Text_t1901882714 * get_cityHollyman_12() const { return ___cityHollyman_12; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_12() { return &___cityHollyman_12; }
	inline void set_cityHollyman_12(Text_t1901882714 * value)
	{
		___cityHollyman_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_12), value);
	}

	inline static int32_t get_offset_of_cityWagon_13() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityWagon_13)); }
	inline Text_t1901882714 * get_cityWagon_13() const { return ___cityWagon_13; }
	inline Text_t1901882714 ** get_address_of_cityWagon_13() { return &___cityWagon_13; }
	inline void set_cityWagon_13(Text_t1901882714 * value)
	{
		___cityWagon_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_13), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_14() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityTrebuchet_14)); }
	inline Text_t1901882714 * get_cityTrebuchet_14() const { return ___cityTrebuchet_14; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_14() { return &___cityTrebuchet_14; }
	inline void set_cityTrebuchet_14(Text_t1901882714 * value)
	{
		___cityTrebuchet_14 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_14), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_15() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___citySiegeTower_15)); }
	inline Text_t1901882714 * get_citySiegeTower_15() const { return ___citySiegeTower_15; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_15() { return &___citySiegeTower_15; }
	inline void set_citySiegeTower_15(Text_t1901882714 * value)
	{
		___citySiegeTower_15 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_15), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_16() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityBatteringRam_16)); }
	inline Text_t1901882714 * get_cityBatteringRam_16() const { return ___cityBatteringRam_16; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_16() { return &___cityBatteringRam_16; }
	inline void set_cityBatteringRam_16(Text_t1901882714 * value)
	{
		___cityBatteringRam_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_16), value);
	}

	inline static int32_t get_offset_of_cityBallista_17() { return static_cast<int32_t>(offsetof(CommandCenterManager_t2415216723, ___cityBallista_17)); }
	inline Text_t1901882714 * get_cityBallista_17() const { return ___cityBallista_17; }
	inline Text_t1901882714 ** get_address_of_cityBallista_17() { return &___cityBallista_17; }
	inline void set_cityBallista_17(Text_t1901882714 * value)
	{
		___cityBallista_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDCENTERMANAGER_T2415216723_H
#ifndef BATTLELOGCONTENTMANAGER_T1438568355_H
#define BATTLELOGCONTENTMANAGER_T1438568355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleLogContentManager
struct  BattleLogContentManager_t1438568355  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text BattleLogContentManager::content
	Text_t1901882714 * ___content_2;
	// System.String BattleLogContentManager::log
	String_t* ___log_3;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(BattleLogContentManager_t1438568355, ___content_2)); }
	inline Text_t1901882714 * get_content_2() const { return ___content_2; }
	inline Text_t1901882714 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Text_t1901882714 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}

	inline static int32_t get_offset_of_log_3() { return static_cast<int32_t>(offsetof(BattleLogContentManager_t1438568355, ___log_3)); }
	inline String_t* get_log_3() const { return ___log_3; }
	inline String_t** get_address_of_log_3() { return &___log_3; }
	inline void set_log_3(String_t* value)
	{
		___log_3 = value;
		Il2CppCodeGenWriteBarrier((&___log_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLELOGCONTENTMANAGER_T1438568355_H
#ifndef BATTLEREPORTCONTENTMANAGER_T922857224_H
#define BATTLEREPORTCONTENTMANAGER_T922857224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportContentManager
struct  BattleReportContentManager_t922857224  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BattleReportContentManager::attackColonizeDestroySection
	GameObject_t1113636619 * ___attackColonizeDestroySection_2;
	// UnityEngine.GameObject BattleReportContentManager::convertSection
	GameObject_t1113636619 * ___convertSection_3;
	// UnityEngine.GameObject BattleReportContentManager::scoutSection
	GameObject_t1113636619 * ___scoutSection_4;
	// UnityEngine.GameObject BattleReportContentManager::reinforceTransportSection
	GameObject_t1113636619 * ___reinforceTransportSection_5;
	// System.Int64 BattleReportContentManager::reportId
	int64_t ___reportId_6;
	// BattleReportModel BattleReportContentManager::report
	BattleReportModel_t4250794538 * ___report_7;
	// SimpleEvent BattleReportContentManager::onGetBattleReport
	SimpleEvent_t129249603 * ___onGetBattleReport_8;
	// System.String BattleReportContentManager::dateFormat
	String_t* ___dateFormat_9;

public:
	inline static int32_t get_offset_of_attackColonizeDestroySection_2() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___attackColonizeDestroySection_2)); }
	inline GameObject_t1113636619 * get_attackColonizeDestroySection_2() const { return ___attackColonizeDestroySection_2; }
	inline GameObject_t1113636619 ** get_address_of_attackColonizeDestroySection_2() { return &___attackColonizeDestroySection_2; }
	inline void set_attackColonizeDestroySection_2(GameObject_t1113636619 * value)
	{
		___attackColonizeDestroySection_2 = value;
		Il2CppCodeGenWriteBarrier((&___attackColonizeDestroySection_2), value);
	}

	inline static int32_t get_offset_of_convertSection_3() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___convertSection_3)); }
	inline GameObject_t1113636619 * get_convertSection_3() const { return ___convertSection_3; }
	inline GameObject_t1113636619 ** get_address_of_convertSection_3() { return &___convertSection_3; }
	inline void set_convertSection_3(GameObject_t1113636619 * value)
	{
		___convertSection_3 = value;
		Il2CppCodeGenWriteBarrier((&___convertSection_3), value);
	}

	inline static int32_t get_offset_of_scoutSection_4() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___scoutSection_4)); }
	inline GameObject_t1113636619 * get_scoutSection_4() const { return ___scoutSection_4; }
	inline GameObject_t1113636619 ** get_address_of_scoutSection_4() { return &___scoutSection_4; }
	inline void set_scoutSection_4(GameObject_t1113636619 * value)
	{
		___scoutSection_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoutSection_4), value);
	}

	inline static int32_t get_offset_of_reinforceTransportSection_5() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___reinforceTransportSection_5)); }
	inline GameObject_t1113636619 * get_reinforceTransportSection_5() const { return ___reinforceTransportSection_5; }
	inline GameObject_t1113636619 ** get_address_of_reinforceTransportSection_5() { return &___reinforceTransportSection_5; }
	inline void set_reinforceTransportSection_5(GameObject_t1113636619 * value)
	{
		___reinforceTransportSection_5 = value;
		Il2CppCodeGenWriteBarrier((&___reinforceTransportSection_5), value);
	}

	inline static int32_t get_offset_of_reportId_6() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___reportId_6)); }
	inline int64_t get_reportId_6() const { return ___reportId_6; }
	inline int64_t* get_address_of_reportId_6() { return &___reportId_6; }
	inline void set_reportId_6(int64_t value)
	{
		___reportId_6 = value;
	}

	inline static int32_t get_offset_of_report_7() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___report_7)); }
	inline BattleReportModel_t4250794538 * get_report_7() const { return ___report_7; }
	inline BattleReportModel_t4250794538 ** get_address_of_report_7() { return &___report_7; }
	inline void set_report_7(BattleReportModel_t4250794538 * value)
	{
		___report_7 = value;
		Il2CppCodeGenWriteBarrier((&___report_7), value);
	}

	inline static int32_t get_offset_of_onGetBattleReport_8() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___onGetBattleReport_8)); }
	inline SimpleEvent_t129249603 * get_onGetBattleReport_8() const { return ___onGetBattleReport_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetBattleReport_8() { return &___onGetBattleReport_8; }
	inline void set_onGetBattleReport_8(SimpleEvent_t129249603 * value)
	{
		___onGetBattleReport_8 = value;
		Il2CppCodeGenWriteBarrier((&___onGetBattleReport_8), value);
	}

	inline static int32_t get_offset_of_dateFormat_9() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224, ___dateFormat_9)); }
	inline String_t* get_dateFormat_9() const { return ___dateFormat_9; }
	inline String_t** get_address_of_dateFormat_9() { return &___dateFormat_9; }
	inline void set_dateFormat_9(String_t* value)
	{
		___dateFormat_9 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_9), value);
	}
};

struct BattleReportContentManager_t922857224_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BattleReportContentManager::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_10() { return static_cast<int32_t>(offsetof(BattleReportContentManager_t922857224_StaticFields, ___U3CU3Ef__switchU24map1_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_10() const { return ___U3CU3Ef__switchU24map1_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_10() { return &___U3CU3Ef__switchU24map1_10; }
	inline void set_U3CU3Ef__switchU24map1_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTCONTENTMANAGER_T922857224_H
#ifndef BARBARIANRECRUITMANAGER_T3050290676_H
#define BARBARIANRECRUITMANAGER_T3050290676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarbarianRecruitManager
struct  BarbarianRecruitManager_t3050290676  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text BarbarianRecruitManager::citySilver
	Text_t1901882714 * ___citySilver_2;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityGold
	Text_t1901882714 * ___cityGold_3;
	// UnityEngine.UI.Text BarbarianRecruitManager::priceSilver
	Text_t1901882714 * ___priceSilver_4;
	// UnityEngine.UI.Text BarbarianRecruitManager::priceGold
	Text_t1901882714 * ___priceGold_5;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityWorker
	Text_t1901882714 * ___cityWorker_6;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySpy
	Text_t1901882714 * ___citySpy_7;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_8;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySpearman
	Text_t1901882714 * ___citySpearman_9;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_10;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_11;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_12;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_13;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityArcher
	Text_t1901882714 * ___cityArcher_14;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_15;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_16;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityWagon
	Text_t1901882714 * ___cityWagon_17;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_18;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_19;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_20;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityBallista
	Text_t1901882714 * ___cityBallista_21;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchWorker
	InputField_t3762917431 * ___dispatchWorker_22;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSpy
	InputField_t3762917431 * ___dispatchSpy_23;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSwordsman
	InputField_t3762917431 * ___dispatchSwordsman_24;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSpearman
	InputField_t3762917431 * ___dispatchSpearman_25;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchPikeman
	InputField_t3762917431 * ___dispatchPikeman_26;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchScoutRider
	InputField_t3762917431 * ___dispatchScoutRider_27;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchLightCavalry
	InputField_t3762917431 * ___dispatchLightCavalry_28;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchHeavyCavalry
	InputField_t3762917431 * ___dispatchHeavyCavalry_29;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchArcher
	InputField_t3762917431 * ___dispatchArcher_30;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchArcherRider
	InputField_t3762917431 * ___dispatchArcherRider_31;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchHollyman
	InputField_t3762917431 * ___dispatchHollyman_32;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchWagon
	InputField_t3762917431 * ___dispatchWagon_33;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchTrebuchet
	InputField_t3762917431 * ___dispatchTrebuchet_34;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSiegeTower
	InputField_t3762917431 * ___dispatchSiegeTower_35;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchBatteringRam
	InputField_t3762917431 * ___dispatchBatteringRam_36;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchBallista
	InputField_t3762917431 * ___dispatchBallista_37;
	// UnityEngine.UI.InputField BarbarianRecruitManager::xInput
	InputField_t3762917431 * ___xInput_38;
	// UnityEngine.UI.InputField BarbarianRecruitManager::yInput
	InputField_t3762917431 * ___yInput_39;
	// WorldFieldModel BarbarianRecruitManager::targetField
	WorldFieldModel_t2417974361 * ___targetField_40;
	// SimpleEvent BarbarianRecruitManager::onGetBarbariansArmy
	SimpleEvent_t129249603 * ___onGetBarbariansArmy_41;
	// UnitsModel BarbarianRecruitManager::presentArmy
	UnitsModel_t3847956398 * ___presentArmy_42;
	// UnitsModel BarbarianRecruitManager::army
	UnitsModel_t3847956398 * ___army_43;
	// ResourcesModel BarbarianRecruitManager::cityResources
	ResourcesModel_t2533508513 * ___cityResources_44;
	// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel> BarbarianRecruitManager::unitConstants
	Dictionary_2_t3367445707 * ___unitConstants_45;

public:
	inline static int32_t get_offset_of_citySilver_2() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___citySilver_2)); }
	inline Text_t1901882714 * get_citySilver_2() const { return ___citySilver_2; }
	inline Text_t1901882714 ** get_address_of_citySilver_2() { return &___citySilver_2; }
	inline void set_citySilver_2(Text_t1901882714 * value)
	{
		___citySilver_2 = value;
		Il2CppCodeGenWriteBarrier((&___citySilver_2), value);
	}

	inline static int32_t get_offset_of_cityGold_3() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityGold_3)); }
	inline Text_t1901882714 * get_cityGold_3() const { return ___cityGold_3; }
	inline Text_t1901882714 ** get_address_of_cityGold_3() { return &___cityGold_3; }
	inline void set_cityGold_3(Text_t1901882714 * value)
	{
		___cityGold_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityGold_3), value);
	}

	inline static int32_t get_offset_of_priceSilver_4() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___priceSilver_4)); }
	inline Text_t1901882714 * get_priceSilver_4() const { return ___priceSilver_4; }
	inline Text_t1901882714 ** get_address_of_priceSilver_4() { return &___priceSilver_4; }
	inline void set_priceSilver_4(Text_t1901882714 * value)
	{
		___priceSilver_4 = value;
		Il2CppCodeGenWriteBarrier((&___priceSilver_4), value);
	}

	inline static int32_t get_offset_of_priceGold_5() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___priceGold_5)); }
	inline Text_t1901882714 * get_priceGold_5() const { return ___priceGold_5; }
	inline Text_t1901882714 ** get_address_of_priceGold_5() { return &___priceGold_5; }
	inline void set_priceGold_5(Text_t1901882714 * value)
	{
		___priceGold_5 = value;
		Il2CppCodeGenWriteBarrier((&___priceGold_5), value);
	}

	inline static int32_t get_offset_of_cityWorker_6() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityWorker_6)); }
	inline Text_t1901882714 * get_cityWorker_6() const { return ___cityWorker_6; }
	inline Text_t1901882714 ** get_address_of_cityWorker_6() { return &___cityWorker_6; }
	inline void set_cityWorker_6(Text_t1901882714 * value)
	{
		___cityWorker_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_6), value);
	}

	inline static int32_t get_offset_of_citySpy_7() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___citySpy_7)); }
	inline Text_t1901882714 * get_citySpy_7() const { return ___citySpy_7; }
	inline Text_t1901882714 ** get_address_of_citySpy_7() { return &___citySpy_7; }
	inline void set_citySpy_7(Text_t1901882714 * value)
	{
		___citySpy_7 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_7), value);
	}

	inline static int32_t get_offset_of_citySwordsman_8() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___citySwordsman_8)); }
	inline Text_t1901882714 * get_citySwordsman_8() const { return ___citySwordsman_8; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_8() { return &___citySwordsman_8; }
	inline void set_citySwordsman_8(Text_t1901882714 * value)
	{
		___citySwordsman_8 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_8), value);
	}

	inline static int32_t get_offset_of_citySpearman_9() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___citySpearman_9)); }
	inline Text_t1901882714 * get_citySpearman_9() const { return ___citySpearman_9; }
	inline Text_t1901882714 ** get_address_of_citySpearman_9() { return &___citySpearman_9; }
	inline void set_citySpearman_9(Text_t1901882714 * value)
	{
		___citySpearman_9 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_9), value);
	}

	inline static int32_t get_offset_of_cityPikeman_10() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityPikeman_10)); }
	inline Text_t1901882714 * get_cityPikeman_10() const { return ___cityPikeman_10; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_10() { return &___cityPikeman_10; }
	inline void set_cityPikeman_10(Text_t1901882714 * value)
	{
		___cityPikeman_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_10), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_11() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityScoutRider_11)); }
	inline Text_t1901882714 * get_cityScoutRider_11() const { return ___cityScoutRider_11; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_11() { return &___cityScoutRider_11; }
	inline void set_cityScoutRider_11(Text_t1901882714 * value)
	{
		___cityScoutRider_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_11), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_12() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityLightCavalry_12)); }
	inline Text_t1901882714 * get_cityLightCavalry_12() const { return ___cityLightCavalry_12; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_12() { return &___cityLightCavalry_12; }
	inline void set_cityLightCavalry_12(Text_t1901882714 * value)
	{
		___cityLightCavalry_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_12), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_13() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityHeavyCavalry_13)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_13() const { return ___cityHeavyCavalry_13; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_13() { return &___cityHeavyCavalry_13; }
	inline void set_cityHeavyCavalry_13(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_13), value);
	}

	inline static int32_t get_offset_of_cityArcher_14() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityArcher_14)); }
	inline Text_t1901882714 * get_cityArcher_14() const { return ___cityArcher_14; }
	inline Text_t1901882714 ** get_address_of_cityArcher_14() { return &___cityArcher_14; }
	inline void set_cityArcher_14(Text_t1901882714 * value)
	{
		___cityArcher_14 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_14), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_15() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityArcherRider_15)); }
	inline Text_t1901882714 * get_cityArcherRider_15() const { return ___cityArcherRider_15; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_15() { return &___cityArcherRider_15; }
	inline void set_cityArcherRider_15(Text_t1901882714 * value)
	{
		___cityArcherRider_15 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_15), value);
	}

	inline static int32_t get_offset_of_cityHollyman_16() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityHollyman_16)); }
	inline Text_t1901882714 * get_cityHollyman_16() const { return ___cityHollyman_16; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_16() { return &___cityHollyman_16; }
	inline void set_cityHollyman_16(Text_t1901882714 * value)
	{
		___cityHollyman_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_16), value);
	}

	inline static int32_t get_offset_of_cityWagon_17() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityWagon_17)); }
	inline Text_t1901882714 * get_cityWagon_17() const { return ___cityWagon_17; }
	inline Text_t1901882714 ** get_address_of_cityWagon_17() { return &___cityWagon_17; }
	inline void set_cityWagon_17(Text_t1901882714 * value)
	{
		___cityWagon_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_17), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_18() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityTrebuchet_18)); }
	inline Text_t1901882714 * get_cityTrebuchet_18() const { return ___cityTrebuchet_18; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_18() { return &___cityTrebuchet_18; }
	inline void set_cityTrebuchet_18(Text_t1901882714 * value)
	{
		___cityTrebuchet_18 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_18), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_19() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___citySiegeTower_19)); }
	inline Text_t1901882714 * get_citySiegeTower_19() const { return ___citySiegeTower_19; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_19() { return &___citySiegeTower_19; }
	inline void set_citySiegeTower_19(Text_t1901882714 * value)
	{
		___citySiegeTower_19 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_19), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_20() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityBatteringRam_20)); }
	inline Text_t1901882714 * get_cityBatteringRam_20() const { return ___cityBatteringRam_20; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_20() { return &___cityBatteringRam_20; }
	inline void set_cityBatteringRam_20(Text_t1901882714 * value)
	{
		___cityBatteringRam_20 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_20), value);
	}

	inline static int32_t get_offset_of_cityBallista_21() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityBallista_21)); }
	inline Text_t1901882714 * get_cityBallista_21() const { return ___cityBallista_21; }
	inline Text_t1901882714 ** get_address_of_cityBallista_21() { return &___cityBallista_21; }
	inline void set_cityBallista_21(Text_t1901882714 * value)
	{
		___cityBallista_21 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_21), value);
	}

	inline static int32_t get_offset_of_dispatchWorker_22() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchWorker_22)); }
	inline InputField_t3762917431 * get_dispatchWorker_22() const { return ___dispatchWorker_22; }
	inline InputField_t3762917431 ** get_address_of_dispatchWorker_22() { return &___dispatchWorker_22; }
	inline void set_dispatchWorker_22(InputField_t3762917431 * value)
	{
		___dispatchWorker_22 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchWorker_22), value);
	}

	inline static int32_t get_offset_of_dispatchSpy_23() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchSpy_23)); }
	inline InputField_t3762917431 * get_dispatchSpy_23() const { return ___dispatchSpy_23; }
	inline InputField_t3762917431 ** get_address_of_dispatchSpy_23() { return &___dispatchSpy_23; }
	inline void set_dispatchSpy_23(InputField_t3762917431 * value)
	{
		___dispatchSpy_23 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSpy_23), value);
	}

	inline static int32_t get_offset_of_dispatchSwordsman_24() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchSwordsman_24)); }
	inline InputField_t3762917431 * get_dispatchSwordsman_24() const { return ___dispatchSwordsman_24; }
	inline InputField_t3762917431 ** get_address_of_dispatchSwordsman_24() { return &___dispatchSwordsman_24; }
	inline void set_dispatchSwordsman_24(InputField_t3762917431 * value)
	{
		___dispatchSwordsman_24 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSwordsman_24), value);
	}

	inline static int32_t get_offset_of_dispatchSpearman_25() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchSpearman_25)); }
	inline InputField_t3762917431 * get_dispatchSpearman_25() const { return ___dispatchSpearman_25; }
	inline InputField_t3762917431 ** get_address_of_dispatchSpearman_25() { return &___dispatchSpearman_25; }
	inline void set_dispatchSpearman_25(InputField_t3762917431 * value)
	{
		___dispatchSpearman_25 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSpearman_25), value);
	}

	inline static int32_t get_offset_of_dispatchPikeman_26() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchPikeman_26)); }
	inline InputField_t3762917431 * get_dispatchPikeman_26() const { return ___dispatchPikeman_26; }
	inline InputField_t3762917431 ** get_address_of_dispatchPikeman_26() { return &___dispatchPikeman_26; }
	inline void set_dispatchPikeman_26(InputField_t3762917431 * value)
	{
		___dispatchPikeman_26 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchPikeman_26), value);
	}

	inline static int32_t get_offset_of_dispatchScoutRider_27() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchScoutRider_27)); }
	inline InputField_t3762917431 * get_dispatchScoutRider_27() const { return ___dispatchScoutRider_27; }
	inline InputField_t3762917431 ** get_address_of_dispatchScoutRider_27() { return &___dispatchScoutRider_27; }
	inline void set_dispatchScoutRider_27(InputField_t3762917431 * value)
	{
		___dispatchScoutRider_27 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchScoutRider_27), value);
	}

	inline static int32_t get_offset_of_dispatchLightCavalry_28() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchLightCavalry_28)); }
	inline InputField_t3762917431 * get_dispatchLightCavalry_28() const { return ___dispatchLightCavalry_28; }
	inline InputField_t3762917431 ** get_address_of_dispatchLightCavalry_28() { return &___dispatchLightCavalry_28; }
	inline void set_dispatchLightCavalry_28(InputField_t3762917431 * value)
	{
		___dispatchLightCavalry_28 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchLightCavalry_28), value);
	}

	inline static int32_t get_offset_of_dispatchHeavyCavalry_29() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchHeavyCavalry_29)); }
	inline InputField_t3762917431 * get_dispatchHeavyCavalry_29() const { return ___dispatchHeavyCavalry_29; }
	inline InputField_t3762917431 ** get_address_of_dispatchHeavyCavalry_29() { return &___dispatchHeavyCavalry_29; }
	inline void set_dispatchHeavyCavalry_29(InputField_t3762917431 * value)
	{
		___dispatchHeavyCavalry_29 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchHeavyCavalry_29), value);
	}

	inline static int32_t get_offset_of_dispatchArcher_30() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchArcher_30)); }
	inline InputField_t3762917431 * get_dispatchArcher_30() const { return ___dispatchArcher_30; }
	inline InputField_t3762917431 ** get_address_of_dispatchArcher_30() { return &___dispatchArcher_30; }
	inline void set_dispatchArcher_30(InputField_t3762917431 * value)
	{
		___dispatchArcher_30 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchArcher_30), value);
	}

	inline static int32_t get_offset_of_dispatchArcherRider_31() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchArcherRider_31)); }
	inline InputField_t3762917431 * get_dispatchArcherRider_31() const { return ___dispatchArcherRider_31; }
	inline InputField_t3762917431 ** get_address_of_dispatchArcherRider_31() { return &___dispatchArcherRider_31; }
	inline void set_dispatchArcherRider_31(InputField_t3762917431 * value)
	{
		___dispatchArcherRider_31 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchArcherRider_31), value);
	}

	inline static int32_t get_offset_of_dispatchHollyman_32() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchHollyman_32)); }
	inline InputField_t3762917431 * get_dispatchHollyman_32() const { return ___dispatchHollyman_32; }
	inline InputField_t3762917431 ** get_address_of_dispatchHollyman_32() { return &___dispatchHollyman_32; }
	inline void set_dispatchHollyman_32(InputField_t3762917431 * value)
	{
		___dispatchHollyman_32 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchHollyman_32), value);
	}

	inline static int32_t get_offset_of_dispatchWagon_33() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchWagon_33)); }
	inline InputField_t3762917431 * get_dispatchWagon_33() const { return ___dispatchWagon_33; }
	inline InputField_t3762917431 ** get_address_of_dispatchWagon_33() { return &___dispatchWagon_33; }
	inline void set_dispatchWagon_33(InputField_t3762917431 * value)
	{
		___dispatchWagon_33 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchWagon_33), value);
	}

	inline static int32_t get_offset_of_dispatchTrebuchet_34() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchTrebuchet_34)); }
	inline InputField_t3762917431 * get_dispatchTrebuchet_34() const { return ___dispatchTrebuchet_34; }
	inline InputField_t3762917431 ** get_address_of_dispatchTrebuchet_34() { return &___dispatchTrebuchet_34; }
	inline void set_dispatchTrebuchet_34(InputField_t3762917431 * value)
	{
		___dispatchTrebuchet_34 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchTrebuchet_34), value);
	}

	inline static int32_t get_offset_of_dispatchSiegeTower_35() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchSiegeTower_35)); }
	inline InputField_t3762917431 * get_dispatchSiegeTower_35() const { return ___dispatchSiegeTower_35; }
	inline InputField_t3762917431 ** get_address_of_dispatchSiegeTower_35() { return &___dispatchSiegeTower_35; }
	inline void set_dispatchSiegeTower_35(InputField_t3762917431 * value)
	{
		___dispatchSiegeTower_35 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchSiegeTower_35), value);
	}

	inline static int32_t get_offset_of_dispatchBatteringRam_36() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchBatteringRam_36)); }
	inline InputField_t3762917431 * get_dispatchBatteringRam_36() const { return ___dispatchBatteringRam_36; }
	inline InputField_t3762917431 ** get_address_of_dispatchBatteringRam_36() { return &___dispatchBatteringRam_36; }
	inline void set_dispatchBatteringRam_36(InputField_t3762917431 * value)
	{
		___dispatchBatteringRam_36 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchBatteringRam_36), value);
	}

	inline static int32_t get_offset_of_dispatchBallista_37() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___dispatchBallista_37)); }
	inline InputField_t3762917431 * get_dispatchBallista_37() const { return ___dispatchBallista_37; }
	inline InputField_t3762917431 ** get_address_of_dispatchBallista_37() { return &___dispatchBallista_37; }
	inline void set_dispatchBallista_37(InputField_t3762917431 * value)
	{
		___dispatchBallista_37 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchBallista_37), value);
	}

	inline static int32_t get_offset_of_xInput_38() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___xInput_38)); }
	inline InputField_t3762917431 * get_xInput_38() const { return ___xInput_38; }
	inline InputField_t3762917431 ** get_address_of_xInput_38() { return &___xInput_38; }
	inline void set_xInput_38(InputField_t3762917431 * value)
	{
		___xInput_38 = value;
		Il2CppCodeGenWriteBarrier((&___xInput_38), value);
	}

	inline static int32_t get_offset_of_yInput_39() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___yInput_39)); }
	inline InputField_t3762917431 * get_yInput_39() const { return ___yInput_39; }
	inline InputField_t3762917431 ** get_address_of_yInput_39() { return &___yInput_39; }
	inline void set_yInput_39(InputField_t3762917431 * value)
	{
		___yInput_39 = value;
		Il2CppCodeGenWriteBarrier((&___yInput_39), value);
	}

	inline static int32_t get_offset_of_targetField_40() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___targetField_40)); }
	inline WorldFieldModel_t2417974361 * get_targetField_40() const { return ___targetField_40; }
	inline WorldFieldModel_t2417974361 ** get_address_of_targetField_40() { return &___targetField_40; }
	inline void set_targetField_40(WorldFieldModel_t2417974361 * value)
	{
		___targetField_40 = value;
		Il2CppCodeGenWriteBarrier((&___targetField_40), value);
	}

	inline static int32_t get_offset_of_onGetBarbariansArmy_41() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___onGetBarbariansArmy_41)); }
	inline SimpleEvent_t129249603 * get_onGetBarbariansArmy_41() const { return ___onGetBarbariansArmy_41; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetBarbariansArmy_41() { return &___onGetBarbariansArmy_41; }
	inline void set_onGetBarbariansArmy_41(SimpleEvent_t129249603 * value)
	{
		___onGetBarbariansArmy_41 = value;
		Il2CppCodeGenWriteBarrier((&___onGetBarbariansArmy_41), value);
	}

	inline static int32_t get_offset_of_presentArmy_42() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___presentArmy_42)); }
	inline UnitsModel_t3847956398 * get_presentArmy_42() const { return ___presentArmy_42; }
	inline UnitsModel_t3847956398 ** get_address_of_presentArmy_42() { return &___presentArmy_42; }
	inline void set_presentArmy_42(UnitsModel_t3847956398 * value)
	{
		___presentArmy_42 = value;
		Il2CppCodeGenWriteBarrier((&___presentArmy_42), value);
	}

	inline static int32_t get_offset_of_army_43() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___army_43)); }
	inline UnitsModel_t3847956398 * get_army_43() const { return ___army_43; }
	inline UnitsModel_t3847956398 ** get_address_of_army_43() { return &___army_43; }
	inline void set_army_43(UnitsModel_t3847956398 * value)
	{
		___army_43 = value;
		Il2CppCodeGenWriteBarrier((&___army_43), value);
	}

	inline static int32_t get_offset_of_cityResources_44() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___cityResources_44)); }
	inline ResourcesModel_t2533508513 * get_cityResources_44() const { return ___cityResources_44; }
	inline ResourcesModel_t2533508513 ** get_address_of_cityResources_44() { return &___cityResources_44; }
	inline void set_cityResources_44(ResourcesModel_t2533508513 * value)
	{
		___cityResources_44 = value;
		Il2CppCodeGenWriteBarrier((&___cityResources_44), value);
	}

	inline static int32_t get_offset_of_unitConstants_45() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t3050290676, ___unitConstants_45)); }
	inline Dictionary_2_t3367445707 * get_unitConstants_45() const { return ___unitConstants_45; }
	inline Dictionary_2_t3367445707 ** get_address_of_unitConstants_45() { return &___unitConstants_45; }
	inline void set_unitConstants_45(Dictionary_2_t3367445707 * value)
	{
		___unitConstants_45 = value;
		Il2CppCodeGenWriteBarrier((&___unitConstants_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARBARIANRECRUITMANAGER_T3050290676_H
#ifndef ARMYDETAILSCONTENTMANAGER_T3479482664_H
#define ARMYDETAILSCONTENTMANAGER_T3479482664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyDetailsContentManager
struct  ArmyDetailsContentManager_t3479482664  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ArmyDetailsContentManager::coords
	Text_t1901882714 * ___coords_2;
	// UnityEngine.UI.Text ArmyDetailsContentManager::cityName
	Text_t1901882714 * ___cityName_3;
	// UnityEngine.UI.Text ArmyDetailsContentManager::targetPlayer
	Text_t1901882714 * ___targetPlayer_4;
	// System.Int64 ArmyDetailsContentManager::armyId
	int64_t ___armyId_5;
	// CommandCenterArmiesTableController ArmyDetailsContentManager::owner
	CommandCenterArmiesTableController_t2609779151 * ___owner_6;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyWorker
	Text_t1901882714 * ___armyWorker_7;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySpy
	Text_t1901882714 * ___armySpy_8;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySwordsman
	Text_t1901882714 * ___armySwordsman_9;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySpearman
	Text_t1901882714 * ___armySpearman_10;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyPikeman
	Text_t1901882714 * ___armyPikeman_11;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyScoutRider
	Text_t1901882714 * ___armyScoutRider_12;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyLightCavalry
	Text_t1901882714 * ___armyLightCavalry_13;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyHeavyCavalry
	Text_t1901882714 * ___armyHeavyCavalry_14;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyArcher
	Text_t1901882714 * ___armyArcher_15;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyArcherRider
	Text_t1901882714 * ___armyArcherRider_16;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyHollyman
	Text_t1901882714 * ___armyHollyman_17;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyWagon
	Text_t1901882714 * ___armyWagon_18;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyTrebuchet
	Text_t1901882714 * ___armyTrebuchet_19;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySiegeTower
	Text_t1901882714 * ___armySiegeTower_20;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyBatteringRam
	Text_t1901882714 * ___armyBatteringRam_21;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyBallista
	Text_t1901882714 * ___armyBallista_22;
	// ArmyModel ArmyDetailsContentManager::army
	ArmyModel_t2068673105 * ___army_23;
	// SimpleEvent ArmyDetailsContentManager::gotReturnArmy
	SimpleEvent_t129249603 * ___gotReturnArmy_24;

public:
	inline static int32_t get_offset_of_coords_2() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___coords_2)); }
	inline Text_t1901882714 * get_coords_2() const { return ___coords_2; }
	inline Text_t1901882714 ** get_address_of_coords_2() { return &___coords_2; }
	inline void set_coords_2(Text_t1901882714 * value)
	{
		___coords_2 = value;
		Il2CppCodeGenWriteBarrier((&___coords_2), value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___cityName_3)); }
	inline Text_t1901882714 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t1901882714 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t1901882714 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_3), value);
	}

	inline static int32_t get_offset_of_targetPlayer_4() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___targetPlayer_4)); }
	inline Text_t1901882714 * get_targetPlayer_4() const { return ___targetPlayer_4; }
	inline Text_t1901882714 ** get_address_of_targetPlayer_4() { return &___targetPlayer_4; }
	inline void set_targetPlayer_4(Text_t1901882714 * value)
	{
		___targetPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetPlayer_4), value);
	}

	inline static int32_t get_offset_of_armyId_5() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyId_5)); }
	inline int64_t get_armyId_5() const { return ___armyId_5; }
	inline int64_t* get_address_of_armyId_5() { return &___armyId_5; }
	inline void set_armyId_5(int64_t value)
	{
		___armyId_5 = value;
	}

	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___owner_6)); }
	inline CommandCenterArmiesTableController_t2609779151 * get_owner_6() const { return ___owner_6; }
	inline CommandCenterArmiesTableController_t2609779151 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(CommandCenterArmiesTableController_t2609779151 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier((&___owner_6), value);
	}

	inline static int32_t get_offset_of_armyWorker_7() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyWorker_7)); }
	inline Text_t1901882714 * get_armyWorker_7() const { return ___armyWorker_7; }
	inline Text_t1901882714 ** get_address_of_armyWorker_7() { return &___armyWorker_7; }
	inline void set_armyWorker_7(Text_t1901882714 * value)
	{
		___armyWorker_7 = value;
		Il2CppCodeGenWriteBarrier((&___armyWorker_7), value);
	}

	inline static int32_t get_offset_of_armySpy_8() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armySpy_8)); }
	inline Text_t1901882714 * get_armySpy_8() const { return ___armySpy_8; }
	inline Text_t1901882714 ** get_address_of_armySpy_8() { return &___armySpy_8; }
	inline void set_armySpy_8(Text_t1901882714 * value)
	{
		___armySpy_8 = value;
		Il2CppCodeGenWriteBarrier((&___armySpy_8), value);
	}

	inline static int32_t get_offset_of_armySwordsman_9() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armySwordsman_9)); }
	inline Text_t1901882714 * get_armySwordsman_9() const { return ___armySwordsman_9; }
	inline Text_t1901882714 ** get_address_of_armySwordsman_9() { return &___armySwordsman_9; }
	inline void set_armySwordsman_9(Text_t1901882714 * value)
	{
		___armySwordsman_9 = value;
		Il2CppCodeGenWriteBarrier((&___armySwordsman_9), value);
	}

	inline static int32_t get_offset_of_armySpearman_10() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armySpearman_10)); }
	inline Text_t1901882714 * get_armySpearman_10() const { return ___armySpearman_10; }
	inline Text_t1901882714 ** get_address_of_armySpearman_10() { return &___armySpearman_10; }
	inline void set_armySpearman_10(Text_t1901882714 * value)
	{
		___armySpearman_10 = value;
		Il2CppCodeGenWriteBarrier((&___armySpearman_10), value);
	}

	inline static int32_t get_offset_of_armyPikeman_11() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyPikeman_11)); }
	inline Text_t1901882714 * get_armyPikeman_11() const { return ___armyPikeman_11; }
	inline Text_t1901882714 ** get_address_of_armyPikeman_11() { return &___armyPikeman_11; }
	inline void set_armyPikeman_11(Text_t1901882714 * value)
	{
		___armyPikeman_11 = value;
		Il2CppCodeGenWriteBarrier((&___armyPikeman_11), value);
	}

	inline static int32_t get_offset_of_armyScoutRider_12() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyScoutRider_12)); }
	inline Text_t1901882714 * get_armyScoutRider_12() const { return ___armyScoutRider_12; }
	inline Text_t1901882714 ** get_address_of_armyScoutRider_12() { return &___armyScoutRider_12; }
	inline void set_armyScoutRider_12(Text_t1901882714 * value)
	{
		___armyScoutRider_12 = value;
		Il2CppCodeGenWriteBarrier((&___armyScoutRider_12), value);
	}

	inline static int32_t get_offset_of_armyLightCavalry_13() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyLightCavalry_13)); }
	inline Text_t1901882714 * get_armyLightCavalry_13() const { return ___armyLightCavalry_13; }
	inline Text_t1901882714 ** get_address_of_armyLightCavalry_13() { return &___armyLightCavalry_13; }
	inline void set_armyLightCavalry_13(Text_t1901882714 * value)
	{
		___armyLightCavalry_13 = value;
		Il2CppCodeGenWriteBarrier((&___armyLightCavalry_13), value);
	}

	inline static int32_t get_offset_of_armyHeavyCavalry_14() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyHeavyCavalry_14)); }
	inline Text_t1901882714 * get_armyHeavyCavalry_14() const { return ___armyHeavyCavalry_14; }
	inline Text_t1901882714 ** get_address_of_armyHeavyCavalry_14() { return &___armyHeavyCavalry_14; }
	inline void set_armyHeavyCavalry_14(Text_t1901882714 * value)
	{
		___armyHeavyCavalry_14 = value;
		Il2CppCodeGenWriteBarrier((&___armyHeavyCavalry_14), value);
	}

	inline static int32_t get_offset_of_armyArcher_15() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyArcher_15)); }
	inline Text_t1901882714 * get_armyArcher_15() const { return ___armyArcher_15; }
	inline Text_t1901882714 ** get_address_of_armyArcher_15() { return &___armyArcher_15; }
	inline void set_armyArcher_15(Text_t1901882714 * value)
	{
		___armyArcher_15 = value;
		Il2CppCodeGenWriteBarrier((&___armyArcher_15), value);
	}

	inline static int32_t get_offset_of_armyArcherRider_16() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyArcherRider_16)); }
	inline Text_t1901882714 * get_armyArcherRider_16() const { return ___armyArcherRider_16; }
	inline Text_t1901882714 ** get_address_of_armyArcherRider_16() { return &___armyArcherRider_16; }
	inline void set_armyArcherRider_16(Text_t1901882714 * value)
	{
		___armyArcherRider_16 = value;
		Il2CppCodeGenWriteBarrier((&___armyArcherRider_16), value);
	}

	inline static int32_t get_offset_of_armyHollyman_17() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyHollyman_17)); }
	inline Text_t1901882714 * get_armyHollyman_17() const { return ___armyHollyman_17; }
	inline Text_t1901882714 ** get_address_of_armyHollyman_17() { return &___armyHollyman_17; }
	inline void set_armyHollyman_17(Text_t1901882714 * value)
	{
		___armyHollyman_17 = value;
		Il2CppCodeGenWriteBarrier((&___armyHollyman_17), value);
	}

	inline static int32_t get_offset_of_armyWagon_18() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyWagon_18)); }
	inline Text_t1901882714 * get_armyWagon_18() const { return ___armyWagon_18; }
	inline Text_t1901882714 ** get_address_of_armyWagon_18() { return &___armyWagon_18; }
	inline void set_armyWagon_18(Text_t1901882714 * value)
	{
		___armyWagon_18 = value;
		Il2CppCodeGenWriteBarrier((&___armyWagon_18), value);
	}

	inline static int32_t get_offset_of_armyTrebuchet_19() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyTrebuchet_19)); }
	inline Text_t1901882714 * get_armyTrebuchet_19() const { return ___armyTrebuchet_19; }
	inline Text_t1901882714 ** get_address_of_armyTrebuchet_19() { return &___armyTrebuchet_19; }
	inline void set_armyTrebuchet_19(Text_t1901882714 * value)
	{
		___armyTrebuchet_19 = value;
		Il2CppCodeGenWriteBarrier((&___armyTrebuchet_19), value);
	}

	inline static int32_t get_offset_of_armySiegeTower_20() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armySiegeTower_20)); }
	inline Text_t1901882714 * get_armySiegeTower_20() const { return ___armySiegeTower_20; }
	inline Text_t1901882714 ** get_address_of_armySiegeTower_20() { return &___armySiegeTower_20; }
	inline void set_armySiegeTower_20(Text_t1901882714 * value)
	{
		___armySiegeTower_20 = value;
		Il2CppCodeGenWriteBarrier((&___armySiegeTower_20), value);
	}

	inline static int32_t get_offset_of_armyBatteringRam_21() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyBatteringRam_21)); }
	inline Text_t1901882714 * get_armyBatteringRam_21() const { return ___armyBatteringRam_21; }
	inline Text_t1901882714 ** get_address_of_armyBatteringRam_21() { return &___armyBatteringRam_21; }
	inline void set_armyBatteringRam_21(Text_t1901882714 * value)
	{
		___armyBatteringRam_21 = value;
		Il2CppCodeGenWriteBarrier((&___armyBatteringRam_21), value);
	}

	inline static int32_t get_offset_of_armyBallista_22() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___armyBallista_22)); }
	inline Text_t1901882714 * get_armyBallista_22() const { return ___armyBallista_22; }
	inline Text_t1901882714 ** get_address_of_armyBallista_22() { return &___armyBallista_22; }
	inline void set_armyBallista_22(Text_t1901882714 * value)
	{
		___armyBallista_22 = value;
		Il2CppCodeGenWriteBarrier((&___armyBallista_22), value);
	}

	inline static int32_t get_offset_of_army_23() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___army_23)); }
	inline ArmyModel_t2068673105 * get_army_23() const { return ___army_23; }
	inline ArmyModel_t2068673105 ** get_address_of_army_23() { return &___army_23; }
	inline void set_army_23(ArmyModel_t2068673105 * value)
	{
		___army_23 = value;
		Il2CppCodeGenWriteBarrier((&___army_23), value);
	}

	inline static int32_t get_offset_of_gotReturnArmy_24() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t3479482664, ___gotReturnArmy_24)); }
	inline SimpleEvent_t129249603 * get_gotReturnArmy_24() const { return ___gotReturnArmy_24; }
	inline SimpleEvent_t129249603 ** get_address_of_gotReturnArmy_24() { return &___gotReturnArmy_24; }
	inline void set_gotReturnArmy_24(SimpleEvent_t129249603 * value)
	{
		___gotReturnArmy_24 = value;
		Il2CppCodeGenWriteBarrier((&___gotReturnArmy_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMYDETAILSCONTENTMANAGER_T3479482664_H
#ifndef ATTACKCOLONIZEDESTROYREPORTMANAGER_T1990770801_H
#define ATTACKCOLONIZEDESTROYREPORTMANAGER_T1990770801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackColonizeDestroyReportManager
struct  AttackColonizeDestroyReportManager_t1990770801  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerName
	Text_t1901882714 * ___attackerName_2;
	// UnityEngine.UI.Image AttackColonizeDestroyReportManager::attackerImage
	Image_t2670269651 * ___attackerImage_3;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerRank
	Text_t1901882714 * ___attackerRank_4;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerAllianceName
	Text_t1901882714 * ___attackerAllianceName_5;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerAlliancePosition
	Text_t1901882714 * ___attackerAlliancePosition_6;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerCityName
	Text_t1901882714 * ___attackerCityName_7;
	// UnityEngine.UI.Image AttackColonizeDestroyReportManager::attackerCityImage
	Image_t2670269651 * ___attackerCityImage_8;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerExperiance
	Text_t1901882714 * ___attackerExperiance_9;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerCityRegion
	Text_t1901882714 * ___attackerCityRegion_10;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderName
	Text_t1901882714 * ___defenderName_11;
	// UnityEngine.UI.Image AttackColonizeDestroyReportManager::defenderImage
	Image_t2670269651 * ___defenderImage_12;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderRank
	Text_t1901882714 * ___defenderRank_13;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderAllianceName
	Text_t1901882714 * ___defenderAllianceName_14;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderAlliancePosition
	Text_t1901882714 * ___defenderAlliancePosition_15;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderCityName
	Text_t1901882714 * ___defenderCityName_16;
	// UnityEngine.UI.Image AttackColonizeDestroyReportManager::defenderCityImage
	Image_t2670269651 * ___defenderCityImage_17;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderExperiance
	Text_t1901882714 * ___defenderExperiance_18;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderCityRegion
	Text_t1901882714 * ___defenderCityRegion_19;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderCityHappiness
	Text_t1901882714 * ___defenderCityHappiness_20;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderCityCourage
	Text_t1901882714 * ___defenderCityCourage_21;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::attackerResult
	Text_t1901882714 * ___attackerResult_22;
	// UnityEngine.UI.Text AttackColonizeDestroyReportManager::defenderResult
	Text_t1901882714 * ___defenderResult_23;
	// BattleReportModel AttackColonizeDestroyReportManager::report
	BattleReportModel_t4250794538 * ___report_24;
	// BattleReportUnitTableController AttackColonizeDestroyReportManager::attackerUnitsController
	BattleReportUnitTableController_t1840302685 * ___attackerUnitsController_25;
	// BattleReportUnitTableController AttackColonizeDestroyReportManager::defenderUnitsController
	BattleReportUnitTableController_t1840302685 * ___defenderUnitsController_26;
	// BattleReportResourcesTableController AttackColonizeDestroyReportManager::attackerResourcesController
	BattleReportResourcesTableController_t1929107072 * ___attackerResourcesController_27;
	// BattleReportResourcesTableController AttackColonizeDestroyReportManager::defenderResourcesController
	BattleReportResourcesTableController_t1929107072 * ___defenderResourcesController_28;
	// BattleReportItemsTableController AttackColonizeDestroyReportManager::attackerItemsController
	BattleReportItemsTableController_t14710115 * ___attackerItemsController_29;
	// System.Collections.Generic.List`1<System.String> AttackColonizeDestroyReportManager::attackerUnitNames
	List_1_t3319525431 * ___attackerUnitNames_30;
	// System.Collections.Generic.List`1<System.Int64> AttackColonizeDestroyReportManager::attackerBeforeCount
	List_1_t913674750 * ___attackerBeforeCount_31;
	// System.Collections.Generic.List`1<System.Int64> AttackColonizeDestroyReportManager::attackerAfterCount
	List_1_t913674750 * ___attackerAfterCount_32;
	// System.Collections.Generic.List`1<System.String> AttackColonizeDestroyReportManager::defenderUnitNames
	List_1_t3319525431 * ___defenderUnitNames_33;
	// System.Collections.Generic.List`1<System.Int64> AttackColonizeDestroyReportManager::defenderBeforeCount
	List_1_t913674750 * ___defenderBeforeCount_34;
	// System.Collections.Generic.List`1<System.Int64> AttackColonizeDestroyReportManager::defenderAfterCount
	List_1_t913674750 * ___defenderAfterCount_35;

public:
	inline static int32_t get_offset_of_attackerName_2() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerName_2)); }
	inline Text_t1901882714 * get_attackerName_2() const { return ___attackerName_2; }
	inline Text_t1901882714 ** get_address_of_attackerName_2() { return &___attackerName_2; }
	inline void set_attackerName_2(Text_t1901882714 * value)
	{
		___attackerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___attackerName_2), value);
	}

	inline static int32_t get_offset_of_attackerImage_3() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerImage_3)); }
	inline Image_t2670269651 * get_attackerImage_3() const { return ___attackerImage_3; }
	inline Image_t2670269651 ** get_address_of_attackerImage_3() { return &___attackerImage_3; }
	inline void set_attackerImage_3(Image_t2670269651 * value)
	{
		___attackerImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___attackerImage_3), value);
	}

	inline static int32_t get_offset_of_attackerRank_4() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerRank_4)); }
	inline Text_t1901882714 * get_attackerRank_4() const { return ___attackerRank_4; }
	inline Text_t1901882714 ** get_address_of_attackerRank_4() { return &___attackerRank_4; }
	inline void set_attackerRank_4(Text_t1901882714 * value)
	{
		___attackerRank_4 = value;
		Il2CppCodeGenWriteBarrier((&___attackerRank_4), value);
	}

	inline static int32_t get_offset_of_attackerAllianceName_5() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerAllianceName_5)); }
	inline Text_t1901882714 * get_attackerAllianceName_5() const { return ___attackerAllianceName_5; }
	inline Text_t1901882714 ** get_address_of_attackerAllianceName_5() { return &___attackerAllianceName_5; }
	inline void set_attackerAllianceName_5(Text_t1901882714 * value)
	{
		___attackerAllianceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAllianceName_5), value);
	}

	inline static int32_t get_offset_of_attackerAlliancePosition_6() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerAlliancePosition_6)); }
	inline Text_t1901882714 * get_attackerAlliancePosition_6() const { return ___attackerAlliancePosition_6; }
	inline Text_t1901882714 ** get_address_of_attackerAlliancePosition_6() { return &___attackerAlliancePosition_6; }
	inline void set_attackerAlliancePosition_6(Text_t1901882714 * value)
	{
		___attackerAlliancePosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAlliancePosition_6), value);
	}

	inline static int32_t get_offset_of_attackerCityName_7() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerCityName_7)); }
	inline Text_t1901882714 * get_attackerCityName_7() const { return ___attackerCityName_7; }
	inline Text_t1901882714 ** get_address_of_attackerCityName_7() { return &___attackerCityName_7; }
	inline void set_attackerCityName_7(Text_t1901882714 * value)
	{
		___attackerCityName_7 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityName_7), value);
	}

	inline static int32_t get_offset_of_attackerCityImage_8() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerCityImage_8)); }
	inline Image_t2670269651 * get_attackerCityImage_8() const { return ___attackerCityImage_8; }
	inline Image_t2670269651 ** get_address_of_attackerCityImage_8() { return &___attackerCityImage_8; }
	inline void set_attackerCityImage_8(Image_t2670269651 * value)
	{
		___attackerCityImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityImage_8), value);
	}

	inline static int32_t get_offset_of_attackerExperiance_9() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerExperiance_9)); }
	inline Text_t1901882714 * get_attackerExperiance_9() const { return ___attackerExperiance_9; }
	inline Text_t1901882714 ** get_address_of_attackerExperiance_9() { return &___attackerExperiance_9; }
	inline void set_attackerExperiance_9(Text_t1901882714 * value)
	{
		___attackerExperiance_9 = value;
		Il2CppCodeGenWriteBarrier((&___attackerExperiance_9), value);
	}

	inline static int32_t get_offset_of_attackerCityRegion_10() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerCityRegion_10)); }
	inline Text_t1901882714 * get_attackerCityRegion_10() const { return ___attackerCityRegion_10; }
	inline Text_t1901882714 ** get_address_of_attackerCityRegion_10() { return &___attackerCityRegion_10; }
	inline void set_attackerCityRegion_10(Text_t1901882714 * value)
	{
		___attackerCityRegion_10 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityRegion_10), value);
	}

	inline static int32_t get_offset_of_defenderName_11() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderName_11)); }
	inline Text_t1901882714 * get_defenderName_11() const { return ___defenderName_11; }
	inline Text_t1901882714 ** get_address_of_defenderName_11() { return &___defenderName_11; }
	inline void set_defenderName_11(Text_t1901882714 * value)
	{
		___defenderName_11 = value;
		Il2CppCodeGenWriteBarrier((&___defenderName_11), value);
	}

	inline static int32_t get_offset_of_defenderImage_12() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderImage_12)); }
	inline Image_t2670269651 * get_defenderImage_12() const { return ___defenderImage_12; }
	inline Image_t2670269651 ** get_address_of_defenderImage_12() { return &___defenderImage_12; }
	inline void set_defenderImage_12(Image_t2670269651 * value)
	{
		___defenderImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___defenderImage_12), value);
	}

	inline static int32_t get_offset_of_defenderRank_13() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderRank_13)); }
	inline Text_t1901882714 * get_defenderRank_13() const { return ___defenderRank_13; }
	inline Text_t1901882714 ** get_address_of_defenderRank_13() { return &___defenderRank_13; }
	inline void set_defenderRank_13(Text_t1901882714 * value)
	{
		___defenderRank_13 = value;
		Il2CppCodeGenWriteBarrier((&___defenderRank_13), value);
	}

	inline static int32_t get_offset_of_defenderAllianceName_14() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderAllianceName_14)); }
	inline Text_t1901882714 * get_defenderAllianceName_14() const { return ___defenderAllianceName_14; }
	inline Text_t1901882714 ** get_address_of_defenderAllianceName_14() { return &___defenderAllianceName_14; }
	inline void set_defenderAllianceName_14(Text_t1901882714 * value)
	{
		___defenderAllianceName_14 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAllianceName_14), value);
	}

	inline static int32_t get_offset_of_defenderAlliancePosition_15() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderAlliancePosition_15)); }
	inline Text_t1901882714 * get_defenderAlliancePosition_15() const { return ___defenderAlliancePosition_15; }
	inline Text_t1901882714 ** get_address_of_defenderAlliancePosition_15() { return &___defenderAlliancePosition_15; }
	inline void set_defenderAlliancePosition_15(Text_t1901882714 * value)
	{
		___defenderAlliancePosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAlliancePosition_15), value);
	}

	inline static int32_t get_offset_of_defenderCityName_16() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderCityName_16)); }
	inline Text_t1901882714 * get_defenderCityName_16() const { return ___defenderCityName_16; }
	inline Text_t1901882714 ** get_address_of_defenderCityName_16() { return &___defenderCityName_16; }
	inline void set_defenderCityName_16(Text_t1901882714 * value)
	{
		___defenderCityName_16 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityName_16), value);
	}

	inline static int32_t get_offset_of_defenderCityImage_17() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderCityImage_17)); }
	inline Image_t2670269651 * get_defenderCityImage_17() const { return ___defenderCityImage_17; }
	inline Image_t2670269651 ** get_address_of_defenderCityImage_17() { return &___defenderCityImage_17; }
	inline void set_defenderCityImage_17(Image_t2670269651 * value)
	{
		___defenderCityImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityImage_17), value);
	}

	inline static int32_t get_offset_of_defenderExperiance_18() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderExperiance_18)); }
	inline Text_t1901882714 * get_defenderExperiance_18() const { return ___defenderExperiance_18; }
	inline Text_t1901882714 ** get_address_of_defenderExperiance_18() { return &___defenderExperiance_18; }
	inline void set_defenderExperiance_18(Text_t1901882714 * value)
	{
		___defenderExperiance_18 = value;
		Il2CppCodeGenWriteBarrier((&___defenderExperiance_18), value);
	}

	inline static int32_t get_offset_of_defenderCityRegion_19() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderCityRegion_19)); }
	inline Text_t1901882714 * get_defenderCityRegion_19() const { return ___defenderCityRegion_19; }
	inline Text_t1901882714 ** get_address_of_defenderCityRegion_19() { return &___defenderCityRegion_19; }
	inline void set_defenderCityRegion_19(Text_t1901882714 * value)
	{
		___defenderCityRegion_19 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityRegion_19), value);
	}

	inline static int32_t get_offset_of_defenderCityHappiness_20() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderCityHappiness_20)); }
	inline Text_t1901882714 * get_defenderCityHappiness_20() const { return ___defenderCityHappiness_20; }
	inline Text_t1901882714 ** get_address_of_defenderCityHappiness_20() { return &___defenderCityHappiness_20; }
	inline void set_defenderCityHappiness_20(Text_t1901882714 * value)
	{
		___defenderCityHappiness_20 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityHappiness_20), value);
	}

	inline static int32_t get_offset_of_defenderCityCourage_21() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderCityCourage_21)); }
	inline Text_t1901882714 * get_defenderCityCourage_21() const { return ___defenderCityCourage_21; }
	inline Text_t1901882714 ** get_address_of_defenderCityCourage_21() { return &___defenderCityCourage_21; }
	inline void set_defenderCityCourage_21(Text_t1901882714 * value)
	{
		___defenderCityCourage_21 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityCourage_21), value);
	}

	inline static int32_t get_offset_of_attackerResult_22() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerResult_22)); }
	inline Text_t1901882714 * get_attackerResult_22() const { return ___attackerResult_22; }
	inline Text_t1901882714 ** get_address_of_attackerResult_22() { return &___attackerResult_22; }
	inline void set_attackerResult_22(Text_t1901882714 * value)
	{
		___attackerResult_22 = value;
		Il2CppCodeGenWriteBarrier((&___attackerResult_22), value);
	}

	inline static int32_t get_offset_of_defenderResult_23() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderResult_23)); }
	inline Text_t1901882714 * get_defenderResult_23() const { return ___defenderResult_23; }
	inline Text_t1901882714 ** get_address_of_defenderResult_23() { return &___defenderResult_23; }
	inline void set_defenderResult_23(Text_t1901882714 * value)
	{
		___defenderResult_23 = value;
		Il2CppCodeGenWriteBarrier((&___defenderResult_23), value);
	}

	inline static int32_t get_offset_of_report_24() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___report_24)); }
	inline BattleReportModel_t4250794538 * get_report_24() const { return ___report_24; }
	inline BattleReportModel_t4250794538 ** get_address_of_report_24() { return &___report_24; }
	inline void set_report_24(BattleReportModel_t4250794538 * value)
	{
		___report_24 = value;
		Il2CppCodeGenWriteBarrier((&___report_24), value);
	}

	inline static int32_t get_offset_of_attackerUnitsController_25() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerUnitsController_25)); }
	inline BattleReportUnitTableController_t1840302685 * get_attackerUnitsController_25() const { return ___attackerUnitsController_25; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_attackerUnitsController_25() { return &___attackerUnitsController_25; }
	inline void set_attackerUnitsController_25(BattleReportUnitTableController_t1840302685 * value)
	{
		___attackerUnitsController_25 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitsController_25), value);
	}

	inline static int32_t get_offset_of_defenderUnitsController_26() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderUnitsController_26)); }
	inline BattleReportUnitTableController_t1840302685 * get_defenderUnitsController_26() const { return ___defenderUnitsController_26; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_defenderUnitsController_26() { return &___defenderUnitsController_26; }
	inline void set_defenderUnitsController_26(BattleReportUnitTableController_t1840302685 * value)
	{
		___defenderUnitsController_26 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitsController_26), value);
	}

	inline static int32_t get_offset_of_attackerResourcesController_27() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerResourcesController_27)); }
	inline BattleReportResourcesTableController_t1929107072 * get_attackerResourcesController_27() const { return ___attackerResourcesController_27; }
	inline BattleReportResourcesTableController_t1929107072 ** get_address_of_attackerResourcesController_27() { return &___attackerResourcesController_27; }
	inline void set_attackerResourcesController_27(BattleReportResourcesTableController_t1929107072 * value)
	{
		___attackerResourcesController_27 = value;
		Il2CppCodeGenWriteBarrier((&___attackerResourcesController_27), value);
	}

	inline static int32_t get_offset_of_defenderResourcesController_28() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderResourcesController_28)); }
	inline BattleReportResourcesTableController_t1929107072 * get_defenderResourcesController_28() const { return ___defenderResourcesController_28; }
	inline BattleReportResourcesTableController_t1929107072 ** get_address_of_defenderResourcesController_28() { return &___defenderResourcesController_28; }
	inline void set_defenderResourcesController_28(BattleReportResourcesTableController_t1929107072 * value)
	{
		___defenderResourcesController_28 = value;
		Il2CppCodeGenWriteBarrier((&___defenderResourcesController_28), value);
	}

	inline static int32_t get_offset_of_attackerItemsController_29() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerItemsController_29)); }
	inline BattleReportItemsTableController_t14710115 * get_attackerItemsController_29() const { return ___attackerItemsController_29; }
	inline BattleReportItemsTableController_t14710115 ** get_address_of_attackerItemsController_29() { return &___attackerItemsController_29; }
	inline void set_attackerItemsController_29(BattleReportItemsTableController_t14710115 * value)
	{
		___attackerItemsController_29 = value;
		Il2CppCodeGenWriteBarrier((&___attackerItemsController_29), value);
	}

	inline static int32_t get_offset_of_attackerUnitNames_30() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerUnitNames_30)); }
	inline List_1_t3319525431 * get_attackerUnitNames_30() const { return ___attackerUnitNames_30; }
	inline List_1_t3319525431 ** get_address_of_attackerUnitNames_30() { return &___attackerUnitNames_30; }
	inline void set_attackerUnitNames_30(List_1_t3319525431 * value)
	{
		___attackerUnitNames_30 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitNames_30), value);
	}

	inline static int32_t get_offset_of_attackerBeforeCount_31() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerBeforeCount_31)); }
	inline List_1_t913674750 * get_attackerBeforeCount_31() const { return ___attackerBeforeCount_31; }
	inline List_1_t913674750 ** get_address_of_attackerBeforeCount_31() { return &___attackerBeforeCount_31; }
	inline void set_attackerBeforeCount_31(List_1_t913674750 * value)
	{
		___attackerBeforeCount_31 = value;
		Il2CppCodeGenWriteBarrier((&___attackerBeforeCount_31), value);
	}

	inline static int32_t get_offset_of_attackerAfterCount_32() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___attackerAfterCount_32)); }
	inline List_1_t913674750 * get_attackerAfterCount_32() const { return ___attackerAfterCount_32; }
	inline List_1_t913674750 ** get_address_of_attackerAfterCount_32() { return &___attackerAfterCount_32; }
	inline void set_attackerAfterCount_32(List_1_t913674750 * value)
	{
		___attackerAfterCount_32 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAfterCount_32), value);
	}

	inline static int32_t get_offset_of_defenderUnitNames_33() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderUnitNames_33)); }
	inline List_1_t3319525431 * get_defenderUnitNames_33() const { return ___defenderUnitNames_33; }
	inline List_1_t3319525431 ** get_address_of_defenderUnitNames_33() { return &___defenderUnitNames_33; }
	inline void set_defenderUnitNames_33(List_1_t3319525431 * value)
	{
		___defenderUnitNames_33 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitNames_33), value);
	}

	inline static int32_t get_offset_of_defenderBeforeCount_34() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderBeforeCount_34)); }
	inline List_1_t913674750 * get_defenderBeforeCount_34() const { return ___defenderBeforeCount_34; }
	inline List_1_t913674750 ** get_address_of_defenderBeforeCount_34() { return &___defenderBeforeCount_34; }
	inline void set_defenderBeforeCount_34(List_1_t913674750 * value)
	{
		___defenderBeforeCount_34 = value;
		Il2CppCodeGenWriteBarrier((&___defenderBeforeCount_34), value);
	}

	inline static int32_t get_offset_of_defenderAfterCount_35() { return static_cast<int32_t>(offsetof(AttackColonizeDestroyReportManager_t1990770801, ___defenderAfterCount_35)); }
	inline List_1_t913674750 * get_defenderAfterCount_35() const { return ___defenderAfterCount_35; }
	inline List_1_t913674750 ** get_address_of_defenderAfterCount_35() { return &___defenderAfterCount_35; }
	inline void set_defenderAfterCount_35(List_1_t913674750 * value)
	{
		___defenderAfterCount_35 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAfterCount_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKCOLONIZEDESTROYREPORTMANAGER_T1990770801_H
#ifndef BUILDINGINFORMATIONMANAGER_T3344348785_H
#define BUILDINGINFORMATIONMANAGER_T3344348785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingInformationManager
struct  BuildingInformationManager_t3344348785  : public MonoBehaviour_t3962482529
{
public:
	// ConstructionPitManager BuildingInformationManager::pitManager
	ConstructionPitManager_t848510622 * ___pitManager_2;
	// UnityEngine.GameObject BuildingInformationManager::trainingPanel
	GameObject_t1113636619 * ___trainingPanel_3;
	// UnityEngine.GameObject BuildingInformationManager::marketPanel
	GameObject_t1113636619 * ___marketPanel_4;
	// UnityEngine.GameObject BuildingInformationManager::residencePanel
	GameObject_t1113636619 * ___residencePanel_5;
	// UnityEngine.GameObject BuildingInformationManager::guestHousePanel
	GameObject_t1113636619 * ___guestHousePanel_6;
	// UnityEngine.GameObject BuildingInformationManager::feastingHallPanel
	GameObject_t1113636619 * ___feastingHallPanel_7;
	// UnityEngine.GameObject BuildingInformationManager::diplomacyPanel
	GameObject_t1113636619 * ___diplomacyPanel_8;
	// UnityEngine.GameObject BuildingInformationManager::commandCenterPanel
	GameObject_t1113636619 * ___commandCenterPanel_9;
	// UnityEngine.GameObject BuildingInformationManager::archery
	GameObject_t1113636619 * ___archery_10;
	// UnityEngine.GameObject BuildingInformationManager::godhouse
	GameObject_t1113636619 * ___godhouse_11;
	// UnityEngine.GameObject BuildingInformationManager::stable
	GameObject_t1113636619 * ___stable_12;
	// UnityEngine.GameObject BuildingInformationManager::academy
	GameObject_t1113636619 * ___academy_13;
	// UnityEngine.GameObject BuildingInformationManager::engineering
	GameObject_t1113636619 * ___engineering_14;
	// UnityEngine.UI.Text BuildingInformationManager::buildingName
	Text_t1901882714 * ___buildingName_15;
	// UnityEngine.UI.Text BuildingInformationManager::buildingDesctiption
	Text_t1901882714 * ___buildingDesctiption_16;
	// UnityEngine.UI.Text BuildingInformationManager::buildingLevel
	Text_t1901882714 * ___buildingLevel_17;
	// UnityEngine.UI.Image BuildingInformationManager::buildingImage
	Image_t2670269651 * ___buildingImage_18;
	// UnityEngine.UI.Dropdown BuildingInformationManager::taxDropdown
	Dropdown_t2274391225 * ___taxDropdown_19;
	// System.Int64 BuildingInformationManager::taxIndex
	int64_t ___taxIndex_20;
	// System.String BuildingInformationManager::type
	String_t* ___type_21;
	// System.Int64 BuildingInformationManager::world_id
	int64_t ___world_id_22;

public:
	inline static int32_t get_offset_of_pitManager_2() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___pitManager_2)); }
	inline ConstructionPitManager_t848510622 * get_pitManager_2() const { return ___pitManager_2; }
	inline ConstructionPitManager_t848510622 ** get_address_of_pitManager_2() { return &___pitManager_2; }
	inline void set_pitManager_2(ConstructionPitManager_t848510622 * value)
	{
		___pitManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___pitManager_2), value);
	}

	inline static int32_t get_offset_of_trainingPanel_3() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___trainingPanel_3)); }
	inline GameObject_t1113636619 * get_trainingPanel_3() const { return ___trainingPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_trainingPanel_3() { return &___trainingPanel_3; }
	inline void set_trainingPanel_3(GameObject_t1113636619 * value)
	{
		___trainingPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___trainingPanel_3), value);
	}

	inline static int32_t get_offset_of_marketPanel_4() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___marketPanel_4)); }
	inline GameObject_t1113636619 * get_marketPanel_4() const { return ___marketPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_marketPanel_4() { return &___marketPanel_4; }
	inline void set_marketPanel_4(GameObject_t1113636619 * value)
	{
		___marketPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___marketPanel_4), value);
	}

	inline static int32_t get_offset_of_residencePanel_5() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___residencePanel_5)); }
	inline GameObject_t1113636619 * get_residencePanel_5() const { return ___residencePanel_5; }
	inline GameObject_t1113636619 ** get_address_of_residencePanel_5() { return &___residencePanel_5; }
	inline void set_residencePanel_5(GameObject_t1113636619 * value)
	{
		___residencePanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___residencePanel_5), value);
	}

	inline static int32_t get_offset_of_guestHousePanel_6() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___guestHousePanel_6)); }
	inline GameObject_t1113636619 * get_guestHousePanel_6() const { return ___guestHousePanel_6; }
	inline GameObject_t1113636619 ** get_address_of_guestHousePanel_6() { return &___guestHousePanel_6; }
	inline void set_guestHousePanel_6(GameObject_t1113636619 * value)
	{
		___guestHousePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___guestHousePanel_6), value);
	}

	inline static int32_t get_offset_of_feastingHallPanel_7() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___feastingHallPanel_7)); }
	inline GameObject_t1113636619 * get_feastingHallPanel_7() const { return ___feastingHallPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_feastingHallPanel_7() { return &___feastingHallPanel_7; }
	inline void set_feastingHallPanel_7(GameObject_t1113636619 * value)
	{
		___feastingHallPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___feastingHallPanel_7), value);
	}

	inline static int32_t get_offset_of_diplomacyPanel_8() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___diplomacyPanel_8)); }
	inline GameObject_t1113636619 * get_diplomacyPanel_8() const { return ___diplomacyPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_diplomacyPanel_8() { return &___diplomacyPanel_8; }
	inline void set_diplomacyPanel_8(GameObject_t1113636619 * value)
	{
		___diplomacyPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___diplomacyPanel_8), value);
	}

	inline static int32_t get_offset_of_commandCenterPanel_9() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___commandCenterPanel_9)); }
	inline GameObject_t1113636619 * get_commandCenterPanel_9() const { return ___commandCenterPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_commandCenterPanel_9() { return &___commandCenterPanel_9; }
	inline void set_commandCenterPanel_9(GameObject_t1113636619 * value)
	{
		___commandCenterPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___commandCenterPanel_9), value);
	}

	inline static int32_t get_offset_of_archery_10() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___archery_10)); }
	inline GameObject_t1113636619 * get_archery_10() const { return ___archery_10; }
	inline GameObject_t1113636619 ** get_address_of_archery_10() { return &___archery_10; }
	inline void set_archery_10(GameObject_t1113636619 * value)
	{
		___archery_10 = value;
		Il2CppCodeGenWriteBarrier((&___archery_10), value);
	}

	inline static int32_t get_offset_of_godhouse_11() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___godhouse_11)); }
	inline GameObject_t1113636619 * get_godhouse_11() const { return ___godhouse_11; }
	inline GameObject_t1113636619 ** get_address_of_godhouse_11() { return &___godhouse_11; }
	inline void set_godhouse_11(GameObject_t1113636619 * value)
	{
		___godhouse_11 = value;
		Il2CppCodeGenWriteBarrier((&___godhouse_11), value);
	}

	inline static int32_t get_offset_of_stable_12() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___stable_12)); }
	inline GameObject_t1113636619 * get_stable_12() const { return ___stable_12; }
	inline GameObject_t1113636619 ** get_address_of_stable_12() { return &___stable_12; }
	inline void set_stable_12(GameObject_t1113636619 * value)
	{
		___stable_12 = value;
		Il2CppCodeGenWriteBarrier((&___stable_12), value);
	}

	inline static int32_t get_offset_of_academy_13() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___academy_13)); }
	inline GameObject_t1113636619 * get_academy_13() const { return ___academy_13; }
	inline GameObject_t1113636619 ** get_address_of_academy_13() { return &___academy_13; }
	inline void set_academy_13(GameObject_t1113636619 * value)
	{
		___academy_13 = value;
		Il2CppCodeGenWriteBarrier((&___academy_13), value);
	}

	inline static int32_t get_offset_of_engineering_14() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___engineering_14)); }
	inline GameObject_t1113636619 * get_engineering_14() const { return ___engineering_14; }
	inline GameObject_t1113636619 ** get_address_of_engineering_14() { return &___engineering_14; }
	inline void set_engineering_14(GameObject_t1113636619 * value)
	{
		___engineering_14 = value;
		Il2CppCodeGenWriteBarrier((&___engineering_14), value);
	}

	inline static int32_t get_offset_of_buildingName_15() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___buildingName_15)); }
	inline Text_t1901882714 * get_buildingName_15() const { return ___buildingName_15; }
	inline Text_t1901882714 ** get_address_of_buildingName_15() { return &___buildingName_15; }
	inline void set_buildingName_15(Text_t1901882714 * value)
	{
		___buildingName_15 = value;
		Il2CppCodeGenWriteBarrier((&___buildingName_15), value);
	}

	inline static int32_t get_offset_of_buildingDesctiption_16() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___buildingDesctiption_16)); }
	inline Text_t1901882714 * get_buildingDesctiption_16() const { return ___buildingDesctiption_16; }
	inline Text_t1901882714 ** get_address_of_buildingDesctiption_16() { return &___buildingDesctiption_16; }
	inline void set_buildingDesctiption_16(Text_t1901882714 * value)
	{
		___buildingDesctiption_16 = value;
		Il2CppCodeGenWriteBarrier((&___buildingDesctiption_16), value);
	}

	inline static int32_t get_offset_of_buildingLevel_17() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___buildingLevel_17)); }
	inline Text_t1901882714 * get_buildingLevel_17() const { return ___buildingLevel_17; }
	inline Text_t1901882714 ** get_address_of_buildingLevel_17() { return &___buildingLevel_17; }
	inline void set_buildingLevel_17(Text_t1901882714 * value)
	{
		___buildingLevel_17 = value;
		Il2CppCodeGenWriteBarrier((&___buildingLevel_17), value);
	}

	inline static int32_t get_offset_of_buildingImage_18() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___buildingImage_18)); }
	inline Image_t2670269651 * get_buildingImage_18() const { return ___buildingImage_18; }
	inline Image_t2670269651 ** get_address_of_buildingImage_18() { return &___buildingImage_18; }
	inline void set_buildingImage_18(Image_t2670269651 * value)
	{
		___buildingImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___buildingImage_18), value);
	}

	inline static int32_t get_offset_of_taxDropdown_19() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___taxDropdown_19)); }
	inline Dropdown_t2274391225 * get_taxDropdown_19() const { return ___taxDropdown_19; }
	inline Dropdown_t2274391225 ** get_address_of_taxDropdown_19() { return &___taxDropdown_19; }
	inline void set_taxDropdown_19(Dropdown_t2274391225 * value)
	{
		___taxDropdown_19 = value;
		Il2CppCodeGenWriteBarrier((&___taxDropdown_19), value);
	}

	inline static int32_t get_offset_of_taxIndex_20() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___taxIndex_20)); }
	inline int64_t get_taxIndex_20() const { return ___taxIndex_20; }
	inline int64_t* get_address_of_taxIndex_20() { return &___taxIndex_20; }
	inline void set_taxIndex_20(int64_t value)
	{
		___taxIndex_20 = value;
	}

	inline static int32_t get_offset_of_type_21() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___type_21)); }
	inline String_t* get_type_21() const { return ___type_21; }
	inline String_t** get_address_of_type_21() { return &___type_21; }
	inline void set_type_21(String_t* value)
	{
		___type_21 = value;
		Il2CppCodeGenWriteBarrier((&___type_21), value);
	}

	inline static int32_t get_offset_of_world_id_22() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785, ___world_id_22)); }
	inline int64_t get_world_id_22() const { return ___world_id_22; }
	inline int64_t* get_address_of_world_id_22() { return &___world_id_22; }
	inline void set_world_id_22(int64_t value)
	{
		___world_id_22 = value;
	}
};

struct BuildingInformationManager_t3344348785_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BuildingInformationManager::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_23() { return static_cast<int32_t>(offsetof(BuildingInformationManager_t3344348785_StaticFields, ___U3CU3Ef__switchU24map2_23)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_23() const { return ___U3CU3Ef__switchU24map2_23; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_23() { return &___U3CU3Ef__switchU24map2_23; }
	inline void set_U3CU3Ef__switchU24map2_23(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGINFORMATIONMANAGER_T3344348785_H
#ifndef CITYCHANGERCONTENTMANAGER_T1075607021_H
#define CITYCHANGERCONTENTMANAGER_T1075607021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityChangerContentManager
struct  CityChangerContentManager_t1075607021  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CityChangerContentManager::cityItemPrefab
	GameObject_t1113636619 * ___cityItemPrefab_2;

public:
	inline static int32_t get_offset_of_cityItemPrefab_2() { return static_cast<int32_t>(offsetof(CityChangerContentManager_t1075607021, ___cityItemPrefab_2)); }
	inline GameObject_t1113636619 * get_cityItemPrefab_2() const { return ___cityItemPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_cityItemPrefab_2() { return &___cityItemPrefab_2; }
	inline void set_cityItemPrefab_2(GameObject_t1113636619 * value)
	{
		___cityItemPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityItemPrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYCHANGERCONTENTMANAGER_T1075607021_H
#ifndef CITYDEEDCONTENTMANAGER_T1851112673_H
#define CITYDEEDCONTENTMANAGER_T1851112673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityDeedContentManager
struct  CityDeedContentManager_t1851112673  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CityDeedContentManager::totalBuildings
	Text_t1901882714 * ___totalBuildings_2;
	// UnityEngine.UI.Text CityDeedContentManager::buildingsHighestLevel
	Text_t1901882714 * ___buildingsHighestLevel_3;
	// UnityEngine.UI.Text CityDeedContentManager::totalFields
	Text_t1901882714 * ___totalFields_4;
	// UnityEngine.UI.Text CityDeedContentManager::fieldsHighestLevel
	Text_t1901882714 * ___fieldsHighestLevel_5;
	// UnityEngine.UI.Text CityDeedContentManager::totalUnits
	Text_t1901882714 * ___totalUnits_6;
	// UnityEngine.UI.Dropdown CityDeedContentManager::citiesDropdown
	Dropdown_t2274391225 * ___citiesDropdown_7;
	// UnityEngine.UI.Text CityDeedContentManager::cityName
	Text_t1901882714 * ___cityName_8;
	// UnityEngine.UI.Text CityDeedContentManager::cityCoords
	Text_t1901882714 * ___cityCoords_9;
	// UnityEngine.UI.InputField CityDeedContentManager::cityPrice
	InputField_t3762917431 * ___cityPrice_10;
	// UnityEngine.UI.InputField CityDeedContentManager::buyerName
	InputField_t3762917431 * ___buyerName_11;
	// UnityEngine.UI.InputField CityDeedContentManager::buyerEmail
	InputField_t3762917431 * ___buyerEmail_12;
	// UnityEngine.UI.Image CityDeedContentManager::cityIcon
	Image_t2670269651 * ___cityIcon_13;
	// IReloadable CityDeedContentManager::owner
	RuntimeObject* ___owner_14;
	// System.String CityDeedContentManager::item_id
	String_t* ___item_id_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> CityDeedContentManager::nameToId
	Dictionary_2_t3521823603 * ___nameToId_16;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> CityDeedContentManager::citiesList
	List_1_t447389798 * ___citiesList_17;

public:
	inline static int32_t get_offset_of_totalBuildings_2() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___totalBuildings_2)); }
	inline Text_t1901882714 * get_totalBuildings_2() const { return ___totalBuildings_2; }
	inline Text_t1901882714 ** get_address_of_totalBuildings_2() { return &___totalBuildings_2; }
	inline void set_totalBuildings_2(Text_t1901882714 * value)
	{
		___totalBuildings_2 = value;
		Il2CppCodeGenWriteBarrier((&___totalBuildings_2), value);
	}

	inline static int32_t get_offset_of_buildingsHighestLevel_3() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___buildingsHighestLevel_3)); }
	inline Text_t1901882714 * get_buildingsHighestLevel_3() const { return ___buildingsHighestLevel_3; }
	inline Text_t1901882714 ** get_address_of_buildingsHighestLevel_3() { return &___buildingsHighestLevel_3; }
	inline void set_buildingsHighestLevel_3(Text_t1901882714 * value)
	{
		___buildingsHighestLevel_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsHighestLevel_3), value);
	}

	inline static int32_t get_offset_of_totalFields_4() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___totalFields_4)); }
	inline Text_t1901882714 * get_totalFields_4() const { return ___totalFields_4; }
	inline Text_t1901882714 ** get_address_of_totalFields_4() { return &___totalFields_4; }
	inline void set_totalFields_4(Text_t1901882714 * value)
	{
		___totalFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___totalFields_4), value);
	}

	inline static int32_t get_offset_of_fieldsHighestLevel_5() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___fieldsHighestLevel_5)); }
	inline Text_t1901882714 * get_fieldsHighestLevel_5() const { return ___fieldsHighestLevel_5; }
	inline Text_t1901882714 ** get_address_of_fieldsHighestLevel_5() { return &___fieldsHighestLevel_5; }
	inline void set_fieldsHighestLevel_5(Text_t1901882714 * value)
	{
		___fieldsHighestLevel_5 = value;
		Il2CppCodeGenWriteBarrier((&___fieldsHighestLevel_5), value);
	}

	inline static int32_t get_offset_of_totalUnits_6() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___totalUnits_6)); }
	inline Text_t1901882714 * get_totalUnits_6() const { return ___totalUnits_6; }
	inline Text_t1901882714 ** get_address_of_totalUnits_6() { return &___totalUnits_6; }
	inline void set_totalUnits_6(Text_t1901882714 * value)
	{
		___totalUnits_6 = value;
		Il2CppCodeGenWriteBarrier((&___totalUnits_6), value);
	}

	inline static int32_t get_offset_of_citiesDropdown_7() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___citiesDropdown_7)); }
	inline Dropdown_t2274391225 * get_citiesDropdown_7() const { return ___citiesDropdown_7; }
	inline Dropdown_t2274391225 ** get_address_of_citiesDropdown_7() { return &___citiesDropdown_7; }
	inline void set_citiesDropdown_7(Dropdown_t2274391225 * value)
	{
		___citiesDropdown_7 = value;
		Il2CppCodeGenWriteBarrier((&___citiesDropdown_7), value);
	}

	inline static int32_t get_offset_of_cityName_8() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___cityName_8)); }
	inline Text_t1901882714 * get_cityName_8() const { return ___cityName_8; }
	inline Text_t1901882714 ** get_address_of_cityName_8() { return &___cityName_8; }
	inline void set_cityName_8(Text_t1901882714 * value)
	{
		___cityName_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_8), value);
	}

	inline static int32_t get_offset_of_cityCoords_9() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___cityCoords_9)); }
	inline Text_t1901882714 * get_cityCoords_9() const { return ___cityCoords_9; }
	inline Text_t1901882714 ** get_address_of_cityCoords_9() { return &___cityCoords_9; }
	inline void set_cityCoords_9(Text_t1901882714 * value)
	{
		___cityCoords_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityCoords_9), value);
	}

	inline static int32_t get_offset_of_cityPrice_10() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___cityPrice_10)); }
	inline InputField_t3762917431 * get_cityPrice_10() const { return ___cityPrice_10; }
	inline InputField_t3762917431 ** get_address_of_cityPrice_10() { return &___cityPrice_10; }
	inline void set_cityPrice_10(InputField_t3762917431 * value)
	{
		___cityPrice_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityPrice_10), value);
	}

	inline static int32_t get_offset_of_buyerName_11() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___buyerName_11)); }
	inline InputField_t3762917431 * get_buyerName_11() const { return ___buyerName_11; }
	inline InputField_t3762917431 ** get_address_of_buyerName_11() { return &___buyerName_11; }
	inline void set_buyerName_11(InputField_t3762917431 * value)
	{
		___buyerName_11 = value;
		Il2CppCodeGenWriteBarrier((&___buyerName_11), value);
	}

	inline static int32_t get_offset_of_buyerEmail_12() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___buyerEmail_12)); }
	inline InputField_t3762917431 * get_buyerEmail_12() const { return ___buyerEmail_12; }
	inline InputField_t3762917431 ** get_address_of_buyerEmail_12() { return &___buyerEmail_12; }
	inline void set_buyerEmail_12(InputField_t3762917431 * value)
	{
		___buyerEmail_12 = value;
		Il2CppCodeGenWriteBarrier((&___buyerEmail_12), value);
	}

	inline static int32_t get_offset_of_cityIcon_13() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___cityIcon_13)); }
	inline Image_t2670269651 * get_cityIcon_13() const { return ___cityIcon_13; }
	inline Image_t2670269651 ** get_address_of_cityIcon_13() { return &___cityIcon_13; }
	inline void set_cityIcon_13(Image_t2670269651 * value)
	{
		___cityIcon_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityIcon_13), value);
	}

	inline static int32_t get_offset_of_owner_14() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___owner_14)); }
	inline RuntimeObject* get_owner_14() const { return ___owner_14; }
	inline RuntimeObject** get_address_of_owner_14() { return &___owner_14; }
	inline void set_owner_14(RuntimeObject* value)
	{
		___owner_14 = value;
		Il2CppCodeGenWriteBarrier((&___owner_14), value);
	}

	inline static int32_t get_offset_of_item_id_15() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___item_id_15)); }
	inline String_t* get_item_id_15() const { return ___item_id_15; }
	inline String_t** get_address_of_item_id_15() { return &___item_id_15; }
	inline void set_item_id_15(String_t* value)
	{
		___item_id_15 = value;
		Il2CppCodeGenWriteBarrier((&___item_id_15), value);
	}

	inline static int32_t get_offset_of_nameToId_16() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___nameToId_16)); }
	inline Dictionary_2_t3521823603 * get_nameToId_16() const { return ___nameToId_16; }
	inline Dictionary_2_t3521823603 ** get_address_of_nameToId_16() { return &___nameToId_16; }
	inline void set_nameToId_16(Dictionary_2_t3521823603 * value)
	{
		___nameToId_16 = value;
		Il2CppCodeGenWriteBarrier((&___nameToId_16), value);
	}

	inline static int32_t get_offset_of_citiesList_17() { return static_cast<int32_t>(offsetof(CityDeedContentManager_t1851112673, ___citiesList_17)); }
	inline List_1_t447389798 * get_citiesList_17() const { return ___citiesList_17; }
	inline List_1_t447389798 ** get_address_of_citiesList_17() { return &___citiesList_17; }
	inline void set_citiesList_17(List_1_t447389798 * value)
	{
		___citiesList_17 = value;
		Il2CppCodeGenWriteBarrier((&___citiesList_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYDEEDCONTENTMANAGER_T1851112673_H
#ifndef CHANGEPASSWORDCONTENTMANAGER_T2087197744_H
#define CHANGEPASSWORDCONTENTMANAGER_T2087197744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangePasswordContentManager
struct  ChangePasswordContentManager_t2087197744  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField ChangePasswordContentManager::oldPassword
	InputField_t3762917431 * ___oldPassword_2;
	// UnityEngine.UI.InputField ChangePasswordContentManager::newPassword
	InputField_t3762917431 * ___newPassword_3;
	// UnityEngine.UI.InputField ChangePasswordContentManager::confirmPassword
	InputField_t3762917431 * ___confirmPassword_4;

public:
	inline static int32_t get_offset_of_oldPassword_2() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t2087197744, ___oldPassword_2)); }
	inline InputField_t3762917431 * get_oldPassword_2() const { return ___oldPassword_2; }
	inline InputField_t3762917431 ** get_address_of_oldPassword_2() { return &___oldPassword_2; }
	inline void set_oldPassword_2(InputField_t3762917431 * value)
	{
		___oldPassword_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldPassword_2), value);
	}

	inline static int32_t get_offset_of_newPassword_3() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t2087197744, ___newPassword_3)); }
	inline InputField_t3762917431 * get_newPassword_3() const { return ___newPassword_3; }
	inline InputField_t3762917431 ** get_address_of_newPassword_3() { return &___newPassword_3; }
	inline void set_newPassword_3(InputField_t3762917431 * value)
	{
		___newPassword_3 = value;
		Il2CppCodeGenWriteBarrier((&___newPassword_3), value);
	}

	inline static int32_t get_offset_of_confirmPassword_4() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t2087197744, ___confirmPassword_4)); }
	inline InputField_t3762917431 * get_confirmPassword_4() const { return ___confirmPassword_4; }
	inline InputField_t3762917431 ** get_address_of_confirmPassword_4() { return &___confirmPassword_4; }
	inline void set_confirmPassword_4(InputField_t3762917431 * value)
	{
		___confirmPassword_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmPassword_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEPASSWORDCONTENTMANAGER_T2087197744_H
#ifndef BUILDNEWCITYCONTENTMANAGER_T101571291_H
#define BUILDNEWCITYCONTENTMANAGER_T101571291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildNewCityContentManager
struct  BuildNewCityContentManager_t101571291  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField BuildNewCityContentManager::cityName
	InputField_t3762917431 * ___cityName_2;
	// System.Int64 BuildNewCityContentManager::posX
	int64_t ___posX_3;
	// System.Int64 BuildNewCityContentManager::posY
	int64_t ___posY_4;
	// UnityEngine.UI.Text BuildNewCityContentManager::foodPresent
	Text_t1901882714 * ___foodPresent_5;
	// UnityEngine.UI.Text BuildNewCityContentManager::woodPresent
	Text_t1901882714 * ___woodPresent_6;
	// UnityEngine.UI.Text BuildNewCityContentManager::stonePresent
	Text_t1901882714 * ___stonePresent_7;
	// UnityEngine.UI.Text BuildNewCityContentManager::ironPresent
	Text_t1901882714 * ___ironPresent_8;
	// UnityEngine.UI.Text BuildNewCityContentManager::copperPresent
	Text_t1901882714 * ___copperPresent_9;
	// UnityEngine.UI.Text BuildNewCityContentManager::silverPresent
	Text_t1901882714 * ___silverPresent_10;
	// UnityEngine.UI.Text BuildNewCityContentManager::goldPresent
	Text_t1901882714 * ___goldPresent_11;
	// ResourcesModel BuildNewCityContentManager::cityResources
	ResourcesModel_t2533508513 * ___cityResources_12;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___cityName_2)); }
	inline InputField_t3762917431 * get_cityName_2() const { return ___cityName_2; }
	inline InputField_t3762917431 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(InputField_t3762917431 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_2), value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___posX_3)); }
	inline int64_t get_posX_3() const { return ___posX_3; }
	inline int64_t* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(int64_t value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___posY_4)); }
	inline int64_t get_posY_4() const { return ___posY_4; }
	inline int64_t* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(int64_t value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_foodPresent_5() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___foodPresent_5)); }
	inline Text_t1901882714 * get_foodPresent_5() const { return ___foodPresent_5; }
	inline Text_t1901882714 ** get_address_of_foodPresent_5() { return &___foodPresent_5; }
	inline void set_foodPresent_5(Text_t1901882714 * value)
	{
		___foodPresent_5 = value;
		Il2CppCodeGenWriteBarrier((&___foodPresent_5), value);
	}

	inline static int32_t get_offset_of_woodPresent_6() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___woodPresent_6)); }
	inline Text_t1901882714 * get_woodPresent_6() const { return ___woodPresent_6; }
	inline Text_t1901882714 ** get_address_of_woodPresent_6() { return &___woodPresent_6; }
	inline void set_woodPresent_6(Text_t1901882714 * value)
	{
		___woodPresent_6 = value;
		Il2CppCodeGenWriteBarrier((&___woodPresent_6), value);
	}

	inline static int32_t get_offset_of_stonePresent_7() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___stonePresent_7)); }
	inline Text_t1901882714 * get_stonePresent_7() const { return ___stonePresent_7; }
	inline Text_t1901882714 ** get_address_of_stonePresent_7() { return &___stonePresent_7; }
	inline void set_stonePresent_7(Text_t1901882714 * value)
	{
		___stonePresent_7 = value;
		Il2CppCodeGenWriteBarrier((&___stonePresent_7), value);
	}

	inline static int32_t get_offset_of_ironPresent_8() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___ironPresent_8)); }
	inline Text_t1901882714 * get_ironPresent_8() const { return ___ironPresent_8; }
	inline Text_t1901882714 ** get_address_of_ironPresent_8() { return &___ironPresent_8; }
	inline void set_ironPresent_8(Text_t1901882714 * value)
	{
		___ironPresent_8 = value;
		Il2CppCodeGenWriteBarrier((&___ironPresent_8), value);
	}

	inline static int32_t get_offset_of_copperPresent_9() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___copperPresent_9)); }
	inline Text_t1901882714 * get_copperPresent_9() const { return ___copperPresent_9; }
	inline Text_t1901882714 ** get_address_of_copperPresent_9() { return &___copperPresent_9; }
	inline void set_copperPresent_9(Text_t1901882714 * value)
	{
		___copperPresent_9 = value;
		Il2CppCodeGenWriteBarrier((&___copperPresent_9), value);
	}

	inline static int32_t get_offset_of_silverPresent_10() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___silverPresent_10)); }
	inline Text_t1901882714 * get_silverPresent_10() const { return ___silverPresent_10; }
	inline Text_t1901882714 ** get_address_of_silverPresent_10() { return &___silverPresent_10; }
	inline void set_silverPresent_10(Text_t1901882714 * value)
	{
		___silverPresent_10 = value;
		Il2CppCodeGenWriteBarrier((&___silverPresent_10), value);
	}

	inline static int32_t get_offset_of_goldPresent_11() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___goldPresent_11)); }
	inline Text_t1901882714 * get_goldPresent_11() const { return ___goldPresent_11; }
	inline Text_t1901882714 ** get_address_of_goldPresent_11() { return &___goldPresent_11; }
	inline void set_goldPresent_11(Text_t1901882714 * value)
	{
		___goldPresent_11 = value;
		Il2CppCodeGenWriteBarrier((&___goldPresent_11), value);
	}

	inline static int32_t get_offset_of_cityResources_12() { return static_cast<int32_t>(offsetof(BuildNewCityContentManager_t101571291, ___cityResources_12)); }
	inline ResourcesModel_t2533508513 * get_cityResources_12() const { return ___cityResources_12; }
	inline ResourcesModel_t2533508513 ** get_address_of_cityResources_12() { return &___cityResources_12; }
	inline void set_cityResources_12(ResourcesModel_t2533508513 * value)
	{
		___cityResources_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityResources_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDNEWCITYCONTENTMANAGER_T101571291_H
#ifndef BUYITEMCONTENTMANAGER_T1100417986_H
#define BUYITEMCONTENTMANAGER_T1100417986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyItemContentManager
struct  BuyItemContentManager_t1100417986  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text BuyItemContentManager::itemName
	Text_t1901882714 * ___itemName_2;
	// UnityEngine.UI.Image BuyItemContentManager::itemImage
	Image_t2670269651 * ___itemImage_3;
	// UnityEngine.UI.Text BuyItemContentManager::itemsOwned
	Text_t1901882714 * ___itemsOwned_4;
	// UnityEngine.UI.Text BuyItemContentManager::unitPrice
	Text_t1901882714 * ___unitPrice_5;
	// UnityEngine.UI.InputField BuyItemContentManager::quantityInput
	InputField_t3762917431 * ___quantityInput_6;
	// UnityEngine.UI.Text BuyItemContentManager::totalShillings
	Text_t1901882714 * ___totalShillings_7;
	// System.String BuyItemContentManager::itemId
	String_t* ___itemId_8;
	// System.Int64 BuyItemContentManager::unitCost
	int64_t ___unitCost_9;
	// System.Int64 BuyItemContentManager::itemcount
	int64_t ___itemcount_10;
	// System.Int64 BuyItemContentManager::toBuyCount
	int64_t ___toBuyCount_11;
	// IReloadable BuyItemContentManager::owner
	RuntimeObject* ___owner_12;
	// System.Boolean BuyItemContentManager::buy
	bool ___buy_13;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___itemName_2)); }
	inline Text_t1901882714 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t1901882714 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t1901882714 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemName_2), value);
	}

	inline static int32_t get_offset_of_itemImage_3() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___itemImage_3)); }
	inline Image_t2670269651 * get_itemImage_3() const { return ___itemImage_3; }
	inline Image_t2670269651 ** get_address_of_itemImage_3() { return &___itemImage_3; }
	inline void set_itemImage_3(Image_t2670269651 * value)
	{
		___itemImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage_3), value);
	}

	inline static int32_t get_offset_of_itemsOwned_4() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___itemsOwned_4)); }
	inline Text_t1901882714 * get_itemsOwned_4() const { return ___itemsOwned_4; }
	inline Text_t1901882714 ** get_address_of_itemsOwned_4() { return &___itemsOwned_4; }
	inline void set_itemsOwned_4(Text_t1901882714 * value)
	{
		___itemsOwned_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemsOwned_4), value);
	}

	inline static int32_t get_offset_of_unitPrice_5() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___unitPrice_5)); }
	inline Text_t1901882714 * get_unitPrice_5() const { return ___unitPrice_5; }
	inline Text_t1901882714 ** get_address_of_unitPrice_5() { return &___unitPrice_5; }
	inline void set_unitPrice_5(Text_t1901882714 * value)
	{
		___unitPrice_5 = value;
		Il2CppCodeGenWriteBarrier((&___unitPrice_5), value);
	}

	inline static int32_t get_offset_of_quantityInput_6() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___quantityInput_6)); }
	inline InputField_t3762917431 * get_quantityInput_6() const { return ___quantityInput_6; }
	inline InputField_t3762917431 ** get_address_of_quantityInput_6() { return &___quantityInput_6; }
	inline void set_quantityInput_6(InputField_t3762917431 * value)
	{
		___quantityInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___quantityInput_6), value);
	}

	inline static int32_t get_offset_of_totalShillings_7() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___totalShillings_7)); }
	inline Text_t1901882714 * get_totalShillings_7() const { return ___totalShillings_7; }
	inline Text_t1901882714 ** get_address_of_totalShillings_7() { return &___totalShillings_7; }
	inline void set_totalShillings_7(Text_t1901882714 * value)
	{
		___totalShillings_7 = value;
		Il2CppCodeGenWriteBarrier((&___totalShillings_7), value);
	}

	inline static int32_t get_offset_of_itemId_8() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___itemId_8)); }
	inline String_t* get_itemId_8() const { return ___itemId_8; }
	inline String_t** get_address_of_itemId_8() { return &___itemId_8; }
	inline void set_itemId_8(String_t* value)
	{
		___itemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_8), value);
	}

	inline static int32_t get_offset_of_unitCost_9() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___unitCost_9)); }
	inline int64_t get_unitCost_9() const { return ___unitCost_9; }
	inline int64_t* get_address_of_unitCost_9() { return &___unitCost_9; }
	inline void set_unitCost_9(int64_t value)
	{
		___unitCost_9 = value;
	}

	inline static int32_t get_offset_of_itemcount_10() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___itemcount_10)); }
	inline int64_t get_itemcount_10() const { return ___itemcount_10; }
	inline int64_t* get_address_of_itemcount_10() { return &___itemcount_10; }
	inline void set_itemcount_10(int64_t value)
	{
		___itemcount_10 = value;
	}

	inline static int32_t get_offset_of_toBuyCount_11() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___toBuyCount_11)); }
	inline int64_t get_toBuyCount_11() const { return ___toBuyCount_11; }
	inline int64_t* get_address_of_toBuyCount_11() { return &___toBuyCount_11; }
	inline void set_toBuyCount_11(int64_t value)
	{
		___toBuyCount_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___owner_12)); }
	inline RuntimeObject* get_owner_12() const { return ___owner_12; }
	inline RuntimeObject** get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(RuntimeObject* value)
	{
		___owner_12 = value;
		Il2CppCodeGenWriteBarrier((&___owner_12), value);
	}

	inline static int32_t get_offset_of_buy_13() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t1100417986, ___buy_13)); }
	inline bool get_buy_13() const { return ___buy_13; }
	inline bool* get_address_of_buy_13() { return &___buy_13; }
	inline void set_buy_13(bool value)
	{
		___buy_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYITEMCONTENTMANAGER_T1100417986_H
#ifndef LOGINPROGRESSBARCONTENTMANAGER_T4082662398_H
#define LOGINPROGRESSBARCONTENTMANAGER_T4082662398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginProgressbarContentManager
struct  LoginProgressbarContentManager_t4082662398  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform LoginProgressbarContentManager::progressTransform
	RectTransform_t3704657025 * ___progressTransform_2;
	// LoginManager LoginProgressbarContentManager::loginManager
	LoginManager_t1249555276 * ___loginManager_3;
	// System.Int64 LoginProgressbarContentManager::loadCount
	int64_t ___loadCount_4;
	// System.Single LoginProgressbarContentManager::step
	float ___step_5;

public:
	inline static int32_t get_offset_of_progressTransform_2() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t4082662398, ___progressTransform_2)); }
	inline RectTransform_t3704657025 * get_progressTransform_2() const { return ___progressTransform_2; }
	inline RectTransform_t3704657025 ** get_address_of_progressTransform_2() { return &___progressTransform_2; }
	inline void set_progressTransform_2(RectTransform_t3704657025 * value)
	{
		___progressTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___progressTransform_2), value);
	}

	inline static int32_t get_offset_of_loginManager_3() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t4082662398, ___loginManager_3)); }
	inline LoginManager_t1249555276 * get_loginManager_3() const { return ___loginManager_3; }
	inline LoginManager_t1249555276 ** get_address_of_loginManager_3() { return &___loginManager_3; }
	inline void set_loginManager_3(LoginManager_t1249555276 * value)
	{
		___loginManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___loginManager_3), value);
	}

	inline static int32_t get_offset_of_loadCount_4() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t4082662398, ___loadCount_4)); }
	inline int64_t get_loadCount_4() const { return ___loadCount_4; }
	inline int64_t* get_address_of_loadCount_4() { return &___loadCount_4; }
	inline void set_loadCount_4(int64_t value)
	{
		___loadCount_4 = value;
	}

	inline static int32_t get_offset_of_step_5() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t4082662398, ___step_5)); }
	inline float get_step_5() const { return ___step_5; }
	inline float* get_address_of_step_5() { return &___step_5; }
	inline void set_step_5(float value)
	{
		___step_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINPROGRESSBARCONTENTMANAGER_T4082662398_H
#ifndef SELLITEMCONTENTMANAGER_T2167318587_H
#define SELLITEMCONTENTMANAGER_T2167318587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SellItemContentManager
struct  SellItemContentManager_t2167318587  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text SellItemContentManager::itemName
	Text_t1901882714 * ___itemName_2;
	// UnityEngine.UI.Text SellItemContentManager::itemsOwned
	Text_t1901882714 * ___itemsOwned_3;
	// UnityEngine.UI.Image SellItemContentManager::itemImage
	Image_t2670269651 * ___itemImage_4;
	// UnityEngine.UI.Dropdown SellItemContentManager::discount
	Dropdown_t2274391225 * ___discount_5;
	// UnityEngine.UI.InputField SellItemContentManager::quantity
	InputField_t3762917431 * ___quantity_6;
	// IReloadable SellItemContentManager::owner
	RuntimeObject* ___owner_7;
	// System.Int64 SellItemContentManager::itemcount
	int64_t ___itemcount_8;
	// System.String SellItemContentManager::itemId
	String_t* ___itemId_9;
	// System.Int64 SellItemContentManager::toSellCount
	int64_t ___toSellCount_10;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___itemName_2)); }
	inline Text_t1901882714 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t1901882714 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t1901882714 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemName_2), value);
	}

	inline static int32_t get_offset_of_itemsOwned_3() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___itemsOwned_3)); }
	inline Text_t1901882714 * get_itemsOwned_3() const { return ___itemsOwned_3; }
	inline Text_t1901882714 ** get_address_of_itemsOwned_3() { return &___itemsOwned_3; }
	inline void set_itemsOwned_3(Text_t1901882714 * value)
	{
		___itemsOwned_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemsOwned_3), value);
	}

	inline static int32_t get_offset_of_itemImage_4() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___itemImage_4)); }
	inline Image_t2670269651 * get_itemImage_4() const { return ___itemImage_4; }
	inline Image_t2670269651 ** get_address_of_itemImage_4() { return &___itemImage_4; }
	inline void set_itemImage_4(Image_t2670269651 * value)
	{
		___itemImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage_4), value);
	}

	inline static int32_t get_offset_of_discount_5() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___discount_5)); }
	inline Dropdown_t2274391225 * get_discount_5() const { return ___discount_5; }
	inline Dropdown_t2274391225 ** get_address_of_discount_5() { return &___discount_5; }
	inline void set_discount_5(Dropdown_t2274391225 * value)
	{
		___discount_5 = value;
		Il2CppCodeGenWriteBarrier((&___discount_5), value);
	}

	inline static int32_t get_offset_of_quantity_6() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___quantity_6)); }
	inline InputField_t3762917431 * get_quantity_6() const { return ___quantity_6; }
	inline InputField_t3762917431 ** get_address_of_quantity_6() { return &___quantity_6; }
	inline void set_quantity_6(InputField_t3762917431 * value)
	{
		___quantity_6 = value;
		Il2CppCodeGenWriteBarrier((&___quantity_6), value);
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___owner_7)); }
	inline RuntimeObject* get_owner_7() const { return ___owner_7; }
	inline RuntimeObject** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(RuntimeObject* value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier((&___owner_7), value);
	}

	inline static int32_t get_offset_of_itemcount_8() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___itemcount_8)); }
	inline int64_t get_itemcount_8() const { return ___itemcount_8; }
	inline int64_t* get_address_of_itemcount_8() { return &___itemcount_8; }
	inline void set_itemcount_8(int64_t value)
	{
		___itemcount_8 = value;
	}

	inline static int32_t get_offset_of_itemId_9() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___itemId_9)); }
	inline String_t* get_itemId_9() const { return ___itemId_9; }
	inline String_t** get_address_of_itemId_9() { return &___itemId_9; }
	inline void set_itemId_9(String_t* value)
	{
		___itemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_9), value);
	}

	inline static int32_t get_offset_of_toSellCount_10() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2167318587, ___toSellCount_10)); }
	inline int64_t get_toSellCount_10() const { return ___toSellCount_10; }
	inline int64_t* get_address_of_toSellCount_10() { return &___toSellCount_10; }
	inline void set_toSellCount_10(int64_t value)
	{
		___toSellCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELLITEMCONTENTMANAGER_T2167318587_H
#ifndef SETTINGSCONTENTMANAGER_T1587921867_H
#define SETTINGSCONTENTMANAGER_T1587921867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsContentManager
struct  SettingsContentManager_t1587921867  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField SettingsContentManager::currentUserNick
	InputField_t3762917431 * ___currentUserNick_2;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserEmail
	InputField_t3762917431 * ___currentUserEmail_3;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserMobile
	InputField_t3762917431 * ___currentUserMobile_4;
	// UnityEngine.UI.InputField SettingsContentManager::currentUserFlag
	InputField_t3762917431 * ___currentUserFlag_5;
	// UnityEngine.UI.Image SettingsContentManager::currentUserImage
	Image_t2670269651 * ___currentUserImage_6;
	// LanguageChangerContentManager SettingsContentManager::languageChanger
	LanguageChangerContentManager_t2232597662 * ___languageChanger_7;
	// ConfirmationEvent SettingsContentManager::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_8;
	// UserModel SettingsContentManager::user
	UserModel_t1353931605 * ___user_9;

public:
	inline static int32_t get_offset_of_currentUserNick_2() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___currentUserNick_2)); }
	inline InputField_t3762917431 * get_currentUserNick_2() const { return ___currentUserNick_2; }
	inline InputField_t3762917431 ** get_address_of_currentUserNick_2() { return &___currentUserNick_2; }
	inline void set_currentUserNick_2(InputField_t3762917431 * value)
	{
		___currentUserNick_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserNick_2), value);
	}

	inline static int32_t get_offset_of_currentUserEmail_3() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___currentUserEmail_3)); }
	inline InputField_t3762917431 * get_currentUserEmail_3() const { return ___currentUserEmail_3; }
	inline InputField_t3762917431 ** get_address_of_currentUserEmail_3() { return &___currentUserEmail_3; }
	inline void set_currentUserEmail_3(InputField_t3762917431 * value)
	{
		___currentUserEmail_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserEmail_3), value);
	}

	inline static int32_t get_offset_of_currentUserMobile_4() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___currentUserMobile_4)); }
	inline InputField_t3762917431 * get_currentUserMobile_4() const { return ___currentUserMobile_4; }
	inline InputField_t3762917431 ** get_address_of_currentUserMobile_4() { return &___currentUserMobile_4; }
	inline void set_currentUserMobile_4(InputField_t3762917431 * value)
	{
		___currentUserMobile_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserMobile_4), value);
	}

	inline static int32_t get_offset_of_currentUserFlag_5() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___currentUserFlag_5)); }
	inline InputField_t3762917431 * get_currentUserFlag_5() const { return ___currentUserFlag_5; }
	inline InputField_t3762917431 ** get_address_of_currentUserFlag_5() { return &___currentUserFlag_5; }
	inline void set_currentUserFlag_5(InputField_t3762917431 * value)
	{
		___currentUserFlag_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserFlag_5), value);
	}

	inline static int32_t get_offset_of_currentUserImage_6() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___currentUserImage_6)); }
	inline Image_t2670269651 * get_currentUserImage_6() const { return ___currentUserImage_6; }
	inline Image_t2670269651 ** get_address_of_currentUserImage_6() { return &___currentUserImage_6; }
	inline void set_currentUserImage_6(Image_t2670269651 * value)
	{
		___currentUserImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentUserImage_6), value);
	}

	inline static int32_t get_offset_of_languageChanger_7() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___languageChanger_7)); }
	inline LanguageChangerContentManager_t2232597662 * get_languageChanger_7() const { return ___languageChanger_7; }
	inline LanguageChangerContentManager_t2232597662 ** get_address_of_languageChanger_7() { return &___languageChanger_7; }
	inline void set_languageChanger_7(LanguageChangerContentManager_t2232597662 * value)
	{
		___languageChanger_7 = value;
		Il2CppCodeGenWriteBarrier((&___languageChanger_7), value);
	}

	inline static int32_t get_offset_of_confirmed_8() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___confirmed_8)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_8() const { return ___confirmed_8; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_8() { return &___confirmed_8; }
	inline void set_confirmed_8(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_8 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_8), value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(SettingsContentManager_t1587921867, ___user_9)); }
	inline UserModel_t1353931605 * get_user_9() const { return ___user_9; }
	inline UserModel_t1353931605 ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(UserModel_t1353931605 * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier((&___user_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSCONTENTMANAGER_T1587921867_H
#ifndef SCOUTREPORTMANAGER_T3306641401_H
#define SCOUTREPORTMANAGER_T3306641401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutReportManager
struct  ScoutReportManager_t3306641401  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ScoutReportManager::attackerName
	Text_t1901882714 * ___attackerName_2;
	// UnityEngine.UI.Image ScoutReportManager::attackerImage
	Image_t2670269651 * ___attackerImage_3;
	// UnityEngine.UI.Text ScoutReportManager::attackerRank
	Text_t1901882714 * ___attackerRank_4;
	// UnityEngine.UI.Text ScoutReportManager::attackerAllianceName
	Text_t1901882714 * ___attackerAllianceName_5;
	// UnityEngine.UI.Text ScoutReportManager::attackerAlliancePosition
	Text_t1901882714 * ___attackerAlliancePosition_6;
	// UnityEngine.UI.Text ScoutReportManager::attackerCityName
	Text_t1901882714 * ___attackerCityName_7;
	// UnityEngine.UI.Image ScoutReportManager::attackerCityImage
	Image_t2670269651 * ___attackerCityImage_8;
	// UnityEngine.UI.Text ScoutReportManager::attackerExperiance
	Text_t1901882714 * ___attackerExperiance_9;
	// UnityEngine.UI.Text ScoutReportManager::attackerCityRegion
	Text_t1901882714 * ___attackerCityRegion_10;
	// UnityEngine.UI.Text ScoutReportManager::defenderName
	Text_t1901882714 * ___defenderName_11;
	// UnityEngine.UI.Image ScoutReportManager::defenderImage
	Image_t2670269651 * ___defenderImage_12;
	// UnityEngine.UI.Text ScoutReportManager::defenderRank
	Text_t1901882714 * ___defenderRank_13;
	// UnityEngine.UI.Text ScoutReportManager::defenderAllianceName
	Text_t1901882714 * ___defenderAllianceName_14;
	// UnityEngine.UI.Text ScoutReportManager::defenderAlliancePosition
	Text_t1901882714 * ___defenderAlliancePosition_15;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityName
	Text_t1901882714 * ___defenderCityName_16;
	// UnityEngine.UI.Image ScoutReportManager::defenderCityImage
	Image_t2670269651 * ___defenderCityImage_17;
	// UnityEngine.UI.Text ScoutReportManager::defenderExperiance
	Text_t1901882714 * ___defenderExperiance_18;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityRegion
	Text_t1901882714 * ___defenderCityRegion_19;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityHappiness
	Text_t1901882714 * ___defenderCityHappiness_20;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityCourage
	Text_t1901882714 * ___defenderCityCourage_21;
	// UnityEngine.UI.Text ScoutReportManager::attackerResult
	Text_t1901882714 * ___attackerResult_22;
	// UnityEngine.UI.Text ScoutReportManager::defenderResult
	Text_t1901882714 * ___defenderResult_23;
	// BattleReportModel ScoutReportManager::report
	BattleReportModel_t4250794538 * ___report_24;
	// BattleReportUnitTableController ScoutReportManager::attackerUnitsController
	BattleReportUnitTableController_t1840302685 * ___attackerUnitsController_25;
	// BattleReportUnitTableController ScoutReportManager::defenderUnitsController
	BattleReportUnitTableController_t1840302685 * ___defenderUnitsController_26;
	// BattleReportResourcesTableController ScoutReportManager::defenderResourcesController
	BattleReportResourcesTableController_t1929107072 * ___defenderResourcesController_27;
	// BattleReportItemsTableController ScoutReportManager::defendersItemsController
	BattleReportItemsTableController_t14710115 * ___defendersItemsController_28;
	// System.Collections.Generic.List`1<System.String> ScoutReportManager::attackerUnitNames
	List_1_t3319525431 * ___attackerUnitNames_29;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::attackerBeforeCount
	List_1_t913674750 * ___attackerBeforeCount_30;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::attackerAfterCount
	List_1_t913674750 * ___attackerAfterCount_31;
	// System.Collections.Generic.List`1<System.String> ScoutReportManager::defenderUnitNames
	List_1_t3319525431 * ___defenderUnitNames_32;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::defenderBeforeCount
	List_1_t913674750 * ___defenderBeforeCount_33;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::defenderAfterCount
	List_1_t913674750 * ___defenderAfterCount_34;

public:
	inline static int32_t get_offset_of_attackerName_2() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerName_2)); }
	inline Text_t1901882714 * get_attackerName_2() const { return ___attackerName_2; }
	inline Text_t1901882714 ** get_address_of_attackerName_2() { return &___attackerName_2; }
	inline void set_attackerName_2(Text_t1901882714 * value)
	{
		___attackerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___attackerName_2), value);
	}

	inline static int32_t get_offset_of_attackerImage_3() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerImage_3)); }
	inline Image_t2670269651 * get_attackerImage_3() const { return ___attackerImage_3; }
	inline Image_t2670269651 ** get_address_of_attackerImage_3() { return &___attackerImage_3; }
	inline void set_attackerImage_3(Image_t2670269651 * value)
	{
		___attackerImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___attackerImage_3), value);
	}

	inline static int32_t get_offset_of_attackerRank_4() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerRank_4)); }
	inline Text_t1901882714 * get_attackerRank_4() const { return ___attackerRank_4; }
	inline Text_t1901882714 ** get_address_of_attackerRank_4() { return &___attackerRank_4; }
	inline void set_attackerRank_4(Text_t1901882714 * value)
	{
		___attackerRank_4 = value;
		Il2CppCodeGenWriteBarrier((&___attackerRank_4), value);
	}

	inline static int32_t get_offset_of_attackerAllianceName_5() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerAllianceName_5)); }
	inline Text_t1901882714 * get_attackerAllianceName_5() const { return ___attackerAllianceName_5; }
	inline Text_t1901882714 ** get_address_of_attackerAllianceName_5() { return &___attackerAllianceName_5; }
	inline void set_attackerAllianceName_5(Text_t1901882714 * value)
	{
		___attackerAllianceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAllianceName_5), value);
	}

	inline static int32_t get_offset_of_attackerAlliancePosition_6() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerAlliancePosition_6)); }
	inline Text_t1901882714 * get_attackerAlliancePosition_6() const { return ___attackerAlliancePosition_6; }
	inline Text_t1901882714 ** get_address_of_attackerAlliancePosition_6() { return &___attackerAlliancePosition_6; }
	inline void set_attackerAlliancePosition_6(Text_t1901882714 * value)
	{
		___attackerAlliancePosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAlliancePosition_6), value);
	}

	inline static int32_t get_offset_of_attackerCityName_7() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerCityName_7)); }
	inline Text_t1901882714 * get_attackerCityName_7() const { return ___attackerCityName_7; }
	inline Text_t1901882714 ** get_address_of_attackerCityName_7() { return &___attackerCityName_7; }
	inline void set_attackerCityName_7(Text_t1901882714 * value)
	{
		___attackerCityName_7 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityName_7), value);
	}

	inline static int32_t get_offset_of_attackerCityImage_8() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerCityImage_8)); }
	inline Image_t2670269651 * get_attackerCityImage_8() const { return ___attackerCityImage_8; }
	inline Image_t2670269651 ** get_address_of_attackerCityImage_8() { return &___attackerCityImage_8; }
	inline void set_attackerCityImage_8(Image_t2670269651 * value)
	{
		___attackerCityImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityImage_8), value);
	}

	inline static int32_t get_offset_of_attackerExperiance_9() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerExperiance_9)); }
	inline Text_t1901882714 * get_attackerExperiance_9() const { return ___attackerExperiance_9; }
	inline Text_t1901882714 ** get_address_of_attackerExperiance_9() { return &___attackerExperiance_9; }
	inline void set_attackerExperiance_9(Text_t1901882714 * value)
	{
		___attackerExperiance_9 = value;
		Il2CppCodeGenWriteBarrier((&___attackerExperiance_9), value);
	}

	inline static int32_t get_offset_of_attackerCityRegion_10() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerCityRegion_10)); }
	inline Text_t1901882714 * get_attackerCityRegion_10() const { return ___attackerCityRegion_10; }
	inline Text_t1901882714 ** get_address_of_attackerCityRegion_10() { return &___attackerCityRegion_10; }
	inline void set_attackerCityRegion_10(Text_t1901882714 * value)
	{
		___attackerCityRegion_10 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityRegion_10), value);
	}

	inline static int32_t get_offset_of_defenderName_11() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderName_11)); }
	inline Text_t1901882714 * get_defenderName_11() const { return ___defenderName_11; }
	inline Text_t1901882714 ** get_address_of_defenderName_11() { return &___defenderName_11; }
	inline void set_defenderName_11(Text_t1901882714 * value)
	{
		___defenderName_11 = value;
		Il2CppCodeGenWriteBarrier((&___defenderName_11), value);
	}

	inline static int32_t get_offset_of_defenderImage_12() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderImage_12)); }
	inline Image_t2670269651 * get_defenderImage_12() const { return ___defenderImage_12; }
	inline Image_t2670269651 ** get_address_of_defenderImage_12() { return &___defenderImage_12; }
	inline void set_defenderImage_12(Image_t2670269651 * value)
	{
		___defenderImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___defenderImage_12), value);
	}

	inline static int32_t get_offset_of_defenderRank_13() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderRank_13)); }
	inline Text_t1901882714 * get_defenderRank_13() const { return ___defenderRank_13; }
	inline Text_t1901882714 ** get_address_of_defenderRank_13() { return &___defenderRank_13; }
	inline void set_defenderRank_13(Text_t1901882714 * value)
	{
		___defenderRank_13 = value;
		Il2CppCodeGenWriteBarrier((&___defenderRank_13), value);
	}

	inline static int32_t get_offset_of_defenderAllianceName_14() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderAllianceName_14)); }
	inline Text_t1901882714 * get_defenderAllianceName_14() const { return ___defenderAllianceName_14; }
	inline Text_t1901882714 ** get_address_of_defenderAllianceName_14() { return &___defenderAllianceName_14; }
	inline void set_defenderAllianceName_14(Text_t1901882714 * value)
	{
		___defenderAllianceName_14 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAllianceName_14), value);
	}

	inline static int32_t get_offset_of_defenderAlliancePosition_15() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderAlliancePosition_15)); }
	inline Text_t1901882714 * get_defenderAlliancePosition_15() const { return ___defenderAlliancePosition_15; }
	inline Text_t1901882714 ** get_address_of_defenderAlliancePosition_15() { return &___defenderAlliancePosition_15; }
	inline void set_defenderAlliancePosition_15(Text_t1901882714 * value)
	{
		___defenderAlliancePosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAlliancePosition_15), value);
	}

	inline static int32_t get_offset_of_defenderCityName_16() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderCityName_16)); }
	inline Text_t1901882714 * get_defenderCityName_16() const { return ___defenderCityName_16; }
	inline Text_t1901882714 ** get_address_of_defenderCityName_16() { return &___defenderCityName_16; }
	inline void set_defenderCityName_16(Text_t1901882714 * value)
	{
		___defenderCityName_16 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityName_16), value);
	}

	inline static int32_t get_offset_of_defenderCityImage_17() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderCityImage_17)); }
	inline Image_t2670269651 * get_defenderCityImage_17() const { return ___defenderCityImage_17; }
	inline Image_t2670269651 ** get_address_of_defenderCityImage_17() { return &___defenderCityImage_17; }
	inline void set_defenderCityImage_17(Image_t2670269651 * value)
	{
		___defenderCityImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityImage_17), value);
	}

	inline static int32_t get_offset_of_defenderExperiance_18() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderExperiance_18)); }
	inline Text_t1901882714 * get_defenderExperiance_18() const { return ___defenderExperiance_18; }
	inline Text_t1901882714 ** get_address_of_defenderExperiance_18() { return &___defenderExperiance_18; }
	inline void set_defenderExperiance_18(Text_t1901882714 * value)
	{
		___defenderExperiance_18 = value;
		Il2CppCodeGenWriteBarrier((&___defenderExperiance_18), value);
	}

	inline static int32_t get_offset_of_defenderCityRegion_19() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderCityRegion_19)); }
	inline Text_t1901882714 * get_defenderCityRegion_19() const { return ___defenderCityRegion_19; }
	inline Text_t1901882714 ** get_address_of_defenderCityRegion_19() { return &___defenderCityRegion_19; }
	inline void set_defenderCityRegion_19(Text_t1901882714 * value)
	{
		___defenderCityRegion_19 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityRegion_19), value);
	}

	inline static int32_t get_offset_of_defenderCityHappiness_20() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderCityHappiness_20)); }
	inline Text_t1901882714 * get_defenderCityHappiness_20() const { return ___defenderCityHappiness_20; }
	inline Text_t1901882714 ** get_address_of_defenderCityHappiness_20() { return &___defenderCityHappiness_20; }
	inline void set_defenderCityHappiness_20(Text_t1901882714 * value)
	{
		___defenderCityHappiness_20 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityHappiness_20), value);
	}

	inline static int32_t get_offset_of_defenderCityCourage_21() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderCityCourage_21)); }
	inline Text_t1901882714 * get_defenderCityCourage_21() const { return ___defenderCityCourage_21; }
	inline Text_t1901882714 ** get_address_of_defenderCityCourage_21() { return &___defenderCityCourage_21; }
	inline void set_defenderCityCourage_21(Text_t1901882714 * value)
	{
		___defenderCityCourage_21 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityCourage_21), value);
	}

	inline static int32_t get_offset_of_attackerResult_22() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerResult_22)); }
	inline Text_t1901882714 * get_attackerResult_22() const { return ___attackerResult_22; }
	inline Text_t1901882714 ** get_address_of_attackerResult_22() { return &___attackerResult_22; }
	inline void set_attackerResult_22(Text_t1901882714 * value)
	{
		___attackerResult_22 = value;
		Il2CppCodeGenWriteBarrier((&___attackerResult_22), value);
	}

	inline static int32_t get_offset_of_defenderResult_23() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderResult_23)); }
	inline Text_t1901882714 * get_defenderResult_23() const { return ___defenderResult_23; }
	inline Text_t1901882714 ** get_address_of_defenderResult_23() { return &___defenderResult_23; }
	inline void set_defenderResult_23(Text_t1901882714 * value)
	{
		___defenderResult_23 = value;
		Il2CppCodeGenWriteBarrier((&___defenderResult_23), value);
	}

	inline static int32_t get_offset_of_report_24() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___report_24)); }
	inline BattleReportModel_t4250794538 * get_report_24() const { return ___report_24; }
	inline BattleReportModel_t4250794538 ** get_address_of_report_24() { return &___report_24; }
	inline void set_report_24(BattleReportModel_t4250794538 * value)
	{
		___report_24 = value;
		Il2CppCodeGenWriteBarrier((&___report_24), value);
	}

	inline static int32_t get_offset_of_attackerUnitsController_25() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerUnitsController_25)); }
	inline BattleReportUnitTableController_t1840302685 * get_attackerUnitsController_25() const { return ___attackerUnitsController_25; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_attackerUnitsController_25() { return &___attackerUnitsController_25; }
	inline void set_attackerUnitsController_25(BattleReportUnitTableController_t1840302685 * value)
	{
		___attackerUnitsController_25 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitsController_25), value);
	}

	inline static int32_t get_offset_of_defenderUnitsController_26() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderUnitsController_26)); }
	inline BattleReportUnitTableController_t1840302685 * get_defenderUnitsController_26() const { return ___defenderUnitsController_26; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_defenderUnitsController_26() { return &___defenderUnitsController_26; }
	inline void set_defenderUnitsController_26(BattleReportUnitTableController_t1840302685 * value)
	{
		___defenderUnitsController_26 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitsController_26), value);
	}

	inline static int32_t get_offset_of_defenderResourcesController_27() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderResourcesController_27)); }
	inline BattleReportResourcesTableController_t1929107072 * get_defenderResourcesController_27() const { return ___defenderResourcesController_27; }
	inline BattleReportResourcesTableController_t1929107072 ** get_address_of_defenderResourcesController_27() { return &___defenderResourcesController_27; }
	inline void set_defenderResourcesController_27(BattleReportResourcesTableController_t1929107072 * value)
	{
		___defenderResourcesController_27 = value;
		Il2CppCodeGenWriteBarrier((&___defenderResourcesController_27), value);
	}

	inline static int32_t get_offset_of_defendersItemsController_28() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defendersItemsController_28)); }
	inline BattleReportItemsTableController_t14710115 * get_defendersItemsController_28() const { return ___defendersItemsController_28; }
	inline BattleReportItemsTableController_t14710115 ** get_address_of_defendersItemsController_28() { return &___defendersItemsController_28; }
	inline void set_defendersItemsController_28(BattleReportItemsTableController_t14710115 * value)
	{
		___defendersItemsController_28 = value;
		Il2CppCodeGenWriteBarrier((&___defendersItemsController_28), value);
	}

	inline static int32_t get_offset_of_attackerUnitNames_29() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerUnitNames_29)); }
	inline List_1_t3319525431 * get_attackerUnitNames_29() const { return ___attackerUnitNames_29; }
	inline List_1_t3319525431 ** get_address_of_attackerUnitNames_29() { return &___attackerUnitNames_29; }
	inline void set_attackerUnitNames_29(List_1_t3319525431 * value)
	{
		___attackerUnitNames_29 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitNames_29), value);
	}

	inline static int32_t get_offset_of_attackerBeforeCount_30() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerBeforeCount_30)); }
	inline List_1_t913674750 * get_attackerBeforeCount_30() const { return ___attackerBeforeCount_30; }
	inline List_1_t913674750 ** get_address_of_attackerBeforeCount_30() { return &___attackerBeforeCount_30; }
	inline void set_attackerBeforeCount_30(List_1_t913674750 * value)
	{
		___attackerBeforeCount_30 = value;
		Il2CppCodeGenWriteBarrier((&___attackerBeforeCount_30), value);
	}

	inline static int32_t get_offset_of_attackerAfterCount_31() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___attackerAfterCount_31)); }
	inline List_1_t913674750 * get_attackerAfterCount_31() const { return ___attackerAfterCount_31; }
	inline List_1_t913674750 ** get_address_of_attackerAfterCount_31() { return &___attackerAfterCount_31; }
	inline void set_attackerAfterCount_31(List_1_t913674750 * value)
	{
		___attackerAfterCount_31 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAfterCount_31), value);
	}

	inline static int32_t get_offset_of_defenderUnitNames_32() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderUnitNames_32)); }
	inline List_1_t3319525431 * get_defenderUnitNames_32() const { return ___defenderUnitNames_32; }
	inline List_1_t3319525431 ** get_address_of_defenderUnitNames_32() { return &___defenderUnitNames_32; }
	inline void set_defenderUnitNames_32(List_1_t3319525431 * value)
	{
		___defenderUnitNames_32 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitNames_32), value);
	}

	inline static int32_t get_offset_of_defenderBeforeCount_33() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderBeforeCount_33)); }
	inline List_1_t913674750 * get_defenderBeforeCount_33() const { return ___defenderBeforeCount_33; }
	inline List_1_t913674750 ** get_address_of_defenderBeforeCount_33() { return &___defenderBeforeCount_33; }
	inline void set_defenderBeforeCount_33(List_1_t913674750 * value)
	{
		___defenderBeforeCount_33 = value;
		Il2CppCodeGenWriteBarrier((&___defenderBeforeCount_33), value);
	}

	inline static int32_t get_offset_of_defenderAfterCount_34() { return static_cast<int32_t>(offsetof(ScoutReportManager_t3306641401, ___defenderAfterCount_34)); }
	inline List_1_t913674750 * get_defenderAfterCount_34() const { return ___defenderAfterCount_34; }
	inline List_1_t913674750 ** get_address_of_defenderAfterCount_34() { return &___defenderAfterCount_34; }
	inline void set_defenderAfterCount_34(List_1_t913674750 * value)
	{
		___defenderAfterCount_34 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAfterCount_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOUTREPORTMANAGER_T3306641401_H
#ifndef RESIDENCEPRODUCTIONCONTENTMANAGER_T3650353576_H
#define RESIDENCEPRODUCTIONCONTENTMANAGER_T3650353576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceProductionContentManager
struct  ResidenceProductionContentManager_t3650353576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::laborForces
	TextU5BU5D_t422084607* ___laborForces_2;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::basicProduction
	TextU5BU5D_t422084607* ___basicProduction_3;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::coloniesTributes
	TextU5BU5D_t422084607* ___coloniesTributes_4;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::valleysProduction
	TextU5BU5D_t422084607* ___valleysProduction_5;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::mayorPlus
	TextU5BU5D_t422084607* ___mayorPlus_6;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::tributesPaid
	TextU5BU5D_t422084607* ___tributesPaid_7;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::troopUpkeep
	TextU5BU5D_t422084607* ___troopUpkeep_8;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::netProduction
	TextU5BU5D_t422084607* ___netProduction_9;

public:
	inline static int32_t get_offset_of_laborForces_2() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___laborForces_2)); }
	inline TextU5BU5D_t422084607* get_laborForces_2() const { return ___laborForces_2; }
	inline TextU5BU5D_t422084607** get_address_of_laborForces_2() { return &___laborForces_2; }
	inline void set_laborForces_2(TextU5BU5D_t422084607* value)
	{
		___laborForces_2 = value;
		Il2CppCodeGenWriteBarrier((&___laborForces_2), value);
	}

	inline static int32_t get_offset_of_basicProduction_3() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___basicProduction_3)); }
	inline TextU5BU5D_t422084607* get_basicProduction_3() const { return ___basicProduction_3; }
	inline TextU5BU5D_t422084607** get_address_of_basicProduction_3() { return &___basicProduction_3; }
	inline void set_basicProduction_3(TextU5BU5D_t422084607* value)
	{
		___basicProduction_3 = value;
		Il2CppCodeGenWriteBarrier((&___basicProduction_3), value);
	}

	inline static int32_t get_offset_of_coloniesTributes_4() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___coloniesTributes_4)); }
	inline TextU5BU5D_t422084607* get_coloniesTributes_4() const { return ___coloniesTributes_4; }
	inline TextU5BU5D_t422084607** get_address_of_coloniesTributes_4() { return &___coloniesTributes_4; }
	inline void set_coloniesTributes_4(TextU5BU5D_t422084607* value)
	{
		___coloniesTributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___coloniesTributes_4), value);
	}

	inline static int32_t get_offset_of_valleysProduction_5() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___valleysProduction_5)); }
	inline TextU5BU5D_t422084607* get_valleysProduction_5() const { return ___valleysProduction_5; }
	inline TextU5BU5D_t422084607** get_address_of_valleysProduction_5() { return &___valleysProduction_5; }
	inline void set_valleysProduction_5(TextU5BU5D_t422084607* value)
	{
		___valleysProduction_5 = value;
		Il2CppCodeGenWriteBarrier((&___valleysProduction_5), value);
	}

	inline static int32_t get_offset_of_mayorPlus_6() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___mayorPlus_6)); }
	inline TextU5BU5D_t422084607* get_mayorPlus_6() const { return ___mayorPlus_6; }
	inline TextU5BU5D_t422084607** get_address_of_mayorPlus_6() { return &___mayorPlus_6; }
	inline void set_mayorPlus_6(TextU5BU5D_t422084607* value)
	{
		___mayorPlus_6 = value;
		Il2CppCodeGenWriteBarrier((&___mayorPlus_6), value);
	}

	inline static int32_t get_offset_of_tributesPaid_7() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___tributesPaid_7)); }
	inline TextU5BU5D_t422084607* get_tributesPaid_7() const { return ___tributesPaid_7; }
	inline TextU5BU5D_t422084607** get_address_of_tributesPaid_7() { return &___tributesPaid_7; }
	inline void set_tributesPaid_7(TextU5BU5D_t422084607* value)
	{
		___tributesPaid_7 = value;
		Il2CppCodeGenWriteBarrier((&___tributesPaid_7), value);
	}

	inline static int32_t get_offset_of_troopUpkeep_8() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___troopUpkeep_8)); }
	inline TextU5BU5D_t422084607* get_troopUpkeep_8() const { return ___troopUpkeep_8; }
	inline TextU5BU5D_t422084607** get_address_of_troopUpkeep_8() { return &___troopUpkeep_8; }
	inline void set_troopUpkeep_8(TextU5BU5D_t422084607* value)
	{
		___troopUpkeep_8 = value;
		Il2CppCodeGenWriteBarrier((&___troopUpkeep_8), value);
	}

	inline static int32_t get_offset_of_netProduction_9() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t3650353576, ___netProduction_9)); }
	inline TextU5BU5D_t422084607* get_netProduction_9() const { return ___netProduction_9; }
	inline TextU5BU5D_t422084607** get_address_of_netProduction_9() { return &___netProduction_9; }
	inline void set_netProduction_9(TextU5BU5D_t422084607* value)
	{
		___netProduction_9 = value;
		Il2CppCodeGenWriteBarrier((&___netProduction_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCEPRODUCTIONCONTENTMANAGER_T3650353576_H
#ifndef RESOURCESMANAGER_T1031245130_H
#define RESOURCESMANAGER_T1031245130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesManager
struct  ResourcesManager_t1031245130  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ResourcesManager::cityFood
	Text_t1901882714 * ___cityFood_2;
	// UnityEngine.UI.Text ResourcesManager::cityWood
	Text_t1901882714 * ___cityWood_3;
	// UnityEngine.UI.Text ResourcesManager::cityIron
	Text_t1901882714 * ___cityIron_4;
	// UnityEngine.UI.Text ResourcesManager::cityStone
	Text_t1901882714 * ___cityStone_5;
	// UnityEngine.UI.Text ResourcesManager::cityCopper
	Text_t1901882714 * ___cityCopper_6;
	// UnityEngine.UI.Text ResourcesManager::citySilver
	Text_t1901882714 * ___citySilver_7;
	// UnityEngine.UI.Text ResourcesManager::cityGold
	Text_t1901882714 * ___cityGold_8;

public:
	inline static int32_t get_offset_of_cityFood_2() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityFood_2)); }
	inline Text_t1901882714 * get_cityFood_2() const { return ___cityFood_2; }
	inline Text_t1901882714 ** get_address_of_cityFood_2() { return &___cityFood_2; }
	inline void set_cityFood_2(Text_t1901882714 * value)
	{
		___cityFood_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityFood_2), value);
	}

	inline static int32_t get_offset_of_cityWood_3() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityWood_3)); }
	inline Text_t1901882714 * get_cityWood_3() const { return ___cityWood_3; }
	inline Text_t1901882714 ** get_address_of_cityWood_3() { return &___cityWood_3; }
	inline void set_cityWood_3(Text_t1901882714 * value)
	{
		___cityWood_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityWood_3), value);
	}

	inline static int32_t get_offset_of_cityIron_4() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityIron_4)); }
	inline Text_t1901882714 * get_cityIron_4() const { return ___cityIron_4; }
	inline Text_t1901882714 ** get_address_of_cityIron_4() { return &___cityIron_4; }
	inline void set_cityIron_4(Text_t1901882714 * value)
	{
		___cityIron_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityIron_4), value);
	}

	inline static int32_t get_offset_of_cityStone_5() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityStone_5)); }
	inline Text_t1901882714 * get_cityStone_5() const { return ___cityStone_5; }
	inline Text_t1901882714 ** get_address_of_cityStone_5() { return &___cityStone_5; }
	inline void set_cityStone_5(Text_t1901882714 * value)
	{
		___cityStone_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityStone_5), value);
	}

	inline static int32_t get_offset_of_cityCopper_6() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityCopper_6)); }
	inline Text_t1901882714 * get_cityCopper_6() const { return ___cityCopper_6; }
	inline Text_t1901882714 ** get_address_of_cityCopper_6() { return &___cityCopper_6; }
	inline void set_cityCopper_6(Text_t1901882714 * value)
	{
		___cityCopper_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityCopper_6), value);
	}

	inline static int32_t get_offset_of_citySilver_7() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___citySilver_7)); }
	inline Text_t1901882714 * get_citySilver_7() const { return ___citySilver_7; }
	inline Text_t1901882714 ** get_address_of_citySilver_7() { return &___citySilver_7; }
	inline void set_citySilver_7(Text_t1901882714 * value)
	{
		___citySilver_7 = value;
		Il2CppCodeGenWriteBarrier((&___citySilver_7), value);
	}

	inline static int32_t get_offset_of_cityGold_8() { return static_cast<int32_t>(offsetof(ResourcesManager_t1031245130, ___cityGold_8)); }
	inline Text_t1901882714 * get_cityGold_8() const { return ___cityGold_8; }
	inline Text_t1901882714 ** get_address_of_cityGold_8() { return &___cityGold_8; }
	inline void set_cityGold_8(Text_t1901882714 * value)
	{
		___cityGold_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityGold_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCESMANAGER_T1031245130_H
#ifndef SHELLCONTENTMANAGER_T476539759_H
#define SHELLCONTENTMANAGER_T476539759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShellContentManager
struct  ShellContentManager_t476539759  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image ShellContentManager::icon
	Image_t2670269651 * ___icon_2;
	// TextLocalizer ShellContentManager::windowName
	TextLocalizer_t4021138871 * ___windowName_3;
	// UnityEngine.GameObject ShellContentManager::content
	GameObject_t1113636619 * ___content_4;
	// UnityEngine.Sprite ShellContentManager::tabSelectedTop
	Sprite_t280657092 * ___tabSelectedTop_5;
	// UnityEngine.Sprite ShellContentManager::tabSelectedBottom
	Sprite_t280657092 * ___tabSelectedBottom_6;
	// UnityEngine.Sprite ShellContentManager::tabUnselectedTop
	Sprite_t280657092 * ___tabUnselectedTop_7;
	// UnityEngine.Sprite ShellContentManager::tabUnselectedBottom
	Sprite_t280657092 * ___tabUnselectedBottom_8;
	// UnityEngine.GameObject ShellContentManager::moreButton
	GameObject_t1113636619 * ___moreButton_9;
	// UnityEngine.GameObject ShellContentManager::moreLeftArrow
	GameObject_t1113636619 * ___moreLeftArrow_10;
	// UnityEngine.GameObject ShellContentManager::moreRightArrow
	GameObject_t1113636619 * ___moreRightArrow_11;
	// UnityEngine.GameObject[] ShellContentManager::tabs
	GameObjectU5BU5D_t3328599146* ___tabs_12;
	// UnityEngine.GameObject[] ShellContentManager::contents
	GameObjectU5BU5D_t3328599146* ___contents_13;
	// System.Int32 ShellContentManager::selectedTabIndex
	int32_t ___selectedTabIndex_14;
	// System.Collections.Generic.List`1<System.String> ShellContentManager::keys
	List_1_t3319525431 * ___keys_15;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> ShellContentManager::icons
	List_1_t1752731834 * ___icons_16;
	// System.String[] ShellContentManager::tabsNames
	StringU5BU5D_t1281789340* ___tabsNames_17;
	// System.String[] ShellContentManager::actionTabsNames
	StringU5BU5D_t1281789340* ___actionTabsNames_18;
	// System.String[] ShellContentManager::shopTabsNames
	StringU5BU5D_t1281789340* ___shopTabsNames_19;
	// System.String[] ShellContentManager::treasureTabsNames
	StringU5BU5D_t1281789340* ___treasureTabsNames_20;
	// System.String[] ShellContentManager::allianceTabsNames
	StringU5BU5D_t1281789340* ___allianceTabsNames_21;
	// System.String[] ShellContentManager::mailTabsNames
	StringU5BU5D_t1281789340* ___mailTabsNames_22;
	// System.String[] ShellContentManager::chatTabsNames
	StringU5BU5D_t1281789340* ___chatTabsNames_23;
	// System.String[] ShellContentManager::genericBuildingTabsNames
	StringU5BU5D_t1281789340* ___genericBuildingTabsNames_24;
	// System.String[] ShellContentManager::commandCenterTabsNames
	StringU5BU5D_t1281789340* ___commandCenterTabsNames_25;
	// System.String[] ShellContentManager::archeryTabsNames
	StringU5BU5D_t1281789340* ___archeryTabsNames_26;
	// System.String[] ShellContentManager::enginneringTabsNames
	StringU5BU5D_t1281789340* ___enginneringTabsNames_27;
	// System.String[] ShellContentManager::godHouseTabsNames
	StringU5BU5D_t1281789340* ___godHouseTabsNames_28;
	// System.String[] ShellContentManager::militarySchoolTabsNames
	StringU5BU5D_t1281789340* ___militarySchoolTabsNames_29;
	// System.String[] ShellContentManager::stableTabsNames
	StringU5BU5D_t1281789340* ___stableTabsNames_30;
	// System.String[] ShellContentManager::dispatchTabsNames
	StringU5BU5D_t1281789340* ___dispatchTabsNames_31;
	// System.String[] ShellContentManager::residenceTabsNames
	StringU5BU5D_t1281789340* ___residenceTabsNames_32;
	// System.String[] ShellContentManager::guestHouseTabsNames
	StringU5BU5D_t1281789340* ___guestHouseTabsNames_33;
	// System.String[] ShellContentManager::feastingHallTabsNames
	StringU5BU5D_t1281789340* ___feastingHallTabsNames_34;
	// System.String[] ShellContentManager::marketTabsNames
	StringU5BU5D_t1281789340* ___marketTabsNames_35;
	// System.String[] ShellContentManager::settingsTabsNames
	StringU5BU5D_t1281789340* ___settingsTabsNames_36;
	// UnityEngine.GameObject[] ShellContentManager::actionContents
	GameObjectU5BU5D_t3328599146* ___actionContents_37;
	// UnityEngine.GameObject[] ShellContentManager::shopContents
	GameObjectU5BU5D_t3328599146* ___shopContents_38;
	// UnityEngine.GameObject[] ShellContentManager::treasureContents
	GameObjectU5BU5D_t3328599146* ___treasureContents_39;
	// UnityEngine.GameObject[] ShellContentManager::allianceContents
	GameObjectU5BU5D_t3328599146* ___allianceContents_40;
	// UnityEngine.GameObject[] ShellContentManager::mailContents
	GameObjectU5BU5D_t3328599146* ___mailContents_41;
	// UnityEngine.GameObject[] ShellContentManager::chatContents
	GameObjectU5BU5D_t3328599146* ___chatContents_42;
	// UnityEngine.GameObject[] ShellContentManager::genericBuildingsContents
	GameObjectU5BU5D_t3328599146* ___genericBuildingsContents_43;
	// UnityEngine.GameObject[] ShellContentManager::commandCenterContents
	GameObjectU5BU5D_t3328599146* ___commandCenterContents_44;
	// UnityEngine.GameObject[] ShellContentManager::archeryContents
	GameObjectU5BU5D_t3328599146* ___archeryContents_45;
	// UnityEngine.GameObject[] ShellContentManager::enginneringContents
	GameObjectU5BU5D_t3328599146* ___enginneringContents_46;
	// UnityEngine.GameObject[] ShellContentManager::godHouseContents
	GameObjectU5BU5D_t3328599146* ___godHouseContents_47;
	// UnityEngine.GameObject[] ShellContentManager::militarySchoolContents
	GameObjectU5BU5D_t3328599146* ___militarySchoolContents_48;
	// UnityEngine.GameObject[] ShellContentManager::stableContents
	GameObjectU5BU5D_t3328599146* ___stableContents_49;
	// UnityEngine.GameObject[] ShellContentManager::dispatchContents
	GameObjectU5BU5D_t3328599146* ___dispatchContents_50;
	// UnityEngine.GameObject[] ShellContentManager::residenceContents
	GameObjectU5BU5D_t3328599146* ___residenceContents_51;
	// UnityEngine.GameObject[] ShellContentManager::guestHouseContents
	GameObjectU5BU5D_t3328599146* ___guestHouseContents_52;
	// UnityEngine.GameObject[] ShellContentManager::feastingHallContents
	GameObjectU5BU5D_t3328599146* ___feastingHallContents_53;
	// UnityEngine.GameObject[] ShellContentManager::marketContents
	GameObjectU5BU5D_t3328599146* ___marketContents_54;
	// UnityEngine.GameObject[] ShellContentManager::settingsContents
	GameObjectU5BU5D_t3328599146* ___settingsContents_55;
	// UnityEngine.MonoBehaviour ShellContentManager::contentChanger
	MonoBehaviour_t3962482529 * ___contentChanger_56;
	// System.String ShellContentManager::type
	String_t* ___type_57;
	// System.Int64 ShellContentManager::pitId
	int64_t ___pitId_58;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___icon_2)); }
	inline Image_t2670269651 * get_icon_2() const { return ___icon_2; }
	inline Image_t2670269651 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(Image_t2670269651 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((&___icon_2), value);
	}

	inline static int32_t get_offset_of_windowName_3() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___windowName_3)); }
	inline TextLocalizer_t4021138871 * get_windowName_3() const { return ___windowName_3; }
	inline TextLocalizer_t4021138871 ** get_address_of_windowName_3() { return &___windowName_3; }
	inline void set_windowName_3(TextLocalizer_t4021138871 * value)
	{
		___windowName_3 = value;
		Il2CppCodeGenWriteBarrier((&___windowName_3), value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___content_4)); }
	inline GameObject_t1113636619 * get_content_4() const { return ___content_4; }
	inline GameObject_t1113636619 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(GameObject_t1113636619 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_4), value);
	}

	inline static int32_t get_offset_of_tabSelectedTop_5() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabSelectedTop_5)); }
	inline Sprite_t280657092 * get_tabSelectedTop_5() const { return ___tabSelectedTop_5; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedTop_5() { return &___tabSelectedTop_5; }
	inline void set_tabSelectedTop_5(Sprite_t280657092 * value)
	{
		___tabSelectedTop_5 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedTop_5), value);
	}

	inline static int32_t get_offset_of_tabSelectedBottom_6() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabSelectedBottom_6)); }
	inline Sprite_t280657092 * get_tabSelectedBottom_6() const { return ___tabSelectedBottom_6; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedBottom_6() { return &___tabSelectedBottom_6; }
	inline void set_tabSelectedBottom_6(Sprite_t280657092 * value)
	{
		___tabSelectedBottom_6 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedBottom_6), value);
	}

	inline static int32_t get_offset_of_tabUnselectedTop_7() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabUnselectedTop_7)); }
	inline Sprite_t280657092 * get_tabUnselectedTop_7() const { return ___tabUnselectedTop_7; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedTop_7() { return &___tabUnselectedTop_7; }
	inline void set_tabUnselectedTop_7(Sprite_t280657092 * value)
	{
		___tabUnselectedTop_7 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedTop_7), value);
	}

	inline static int32_t get_offset_of_tabUnselectedBottom_8() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabUnselectedBottom_8)); }
	inline Sprite_t280657092 * get_tabUnselectedBottom_8() const { return ___tabUnselectedBottom_8; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedBottom_8() { return &___tabUnselectedBottom_8; }
	inline void set_tabUnselectedBottom_8(Sprite_t280657092 * value)
	{
		___tabUnselectedBottom_8 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedBottom_8), value);
	}

	inline static int32_t get_offset_of_moreButton_9() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___moreButton_9)); }
	inline GameObject_t1113636619 * get_moreButton_9() const { return ___moreButton_9; }
	inline GameObject_t1113636619 ** get_address_of_moreButton_9() { return &___moreButton_9; }
	inline void set_moreButton_9(GameObject_t1113636619 * value)
	{
		___moreButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___moreButton_9), value);
	}

	inline static int32_t get_offset_of_moreLeftArrow_10() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___moreLeftArrow_10)); }
	inline GameObject_t1113636619 * get_moreLeftArrow_10() const { return ___moreLeftArrow_10; }
	inline GameObject_t1113636619 ** get_address_of_moreLeftArrow_10() { return &___moreLeftArrow_10; }
	inline void set_moreLeftArrow_10(GameObject_t1113636619 * value)
	{
		___moreLeftArrow_10 = value;
		Il2CppCodeGenWriteBarrier((&___moreLeftArrow_10), value);
	}

	inline static int32_t get_offset_of_moreRightArrow_11() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___moreRightArrow_11)); }
	inline GameObject_t1113636619 * get_moreRightArrow_11() const { return ___moreRightArrow_11; }
	inline GameObject_t1113636619 ** get_address_of_moreRightArrow_11() { return &___moreRightArrow_11; }
	inline void set_moreRightArrow_11(GameObject_t1113636619 * value)
	{
		___moreRightArrow_11 = value;
		Il2CppCodeGenWriteBarrier((&___moreRightArrow_11), value);
	}

	inline static int32_t get_offset_of_tabs_12() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabs_12)); }
	inline GameObjectU5BU5D_t3328599146* get_tabs_12() const { return ___tabs_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_tabs_12() { return &___tabs_12; }
	inline void set_tabs_12(GameObjectU5BU5D_t3328599146* value)
	{
		___tabs_12 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_12), value);
	}

	inline static int32_t get_offset_of_contents_13() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___contents_13)); }
	inline GameObjectU5BU5D_t3328599146* get_contents_13() const { return ___contents_13; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_contents_13() { return &___contents_13; }
	inline void set_contents_13(GameObjectU5BU5D_t3328599146* value)
	{
		___contents_13 = value;
		Il2CppCodeGenWriteBarrier((&___contents_13), value);
	}

	inline static int32_t get_offset_of_selectedTabIndex_14() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___selectedTabIndex_14)); }
	inline int32_t get_selectedTabIndex_14() const { return ___selectedTabIndex_14; }
	inline int32_t* get_address_of_selectedTabIndex_14() { return &___selectedTabIndex_14; }
	inline void set_selectedTabIndex_14(int32_t value)
	{
		___selectedTabIndex_14 = value;
	}

	inline static int32_t get_offset_of_keys_15() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___keys_15)); }
	inline List_1_t3319525431 * get_keys_15() const { return ___keys_15; }
	inline List_1_t3319525431 ** get_address_of_keys_15() { return &___keys_15; }
	inline void set_keys_15(List_1_t3319525431 * value)
	{
		___keys_15 = value;
		Il2CppCodeGenWriteBarrier((&___keys_15), value);
	}

	inline static int32_t get_offset_of_icons_16() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___icons_16)); }
	inline List_1_t1752731834 * get_icons_16() const { return ___icons_16; }
	inline List_1_t1752731834 ** get_address_of_icons_16() { return &___icons_16; }
	inline void set_icons_16(List_1_t1752731834 * value)
	{
		___icons_16 = value;
		Il2CppCodeGenWriteBarrier((&___icons_16), value);
	}

	inline static int32_t get_offset_of_tabsNames_17() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___tabsNames_17)); }
	inline StringU5BU5D_t1281789340* get_tabsNames_17() const { return ___tabsNames_17; }
	inline StringU5BU5D_t1281789340** get_address_of_tabsNames_17() { return &___tabsNames_17; }
	inline void set_tabsNames_17(StringU5BU5D_t1281789340* value)
	{
		___tabsNames_17 = value;
		Il2CppCodeGenWriteBarrier((&___tabsNames_17), value);
	}

	inline static int32_t get_offset_of_actionTabsNames_18() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___actionTabsNames_18)); }
	inline StringU5BU5D_t1281789340* get_actionTabsNames_18() const { return ___actionTabsNames_18; }
	inline StringU5BU5D_t1281789340** get_address_of_actionTabsNames_18() { return &___actionTabsNames_18; }
	inline void set_actionTabsNames_18(StringU5BU5D_t1281789340* value)
	{
		___actionTabsNames_18 = value;
		Il2CppCodeGenWriteBarrier((&___actionTabsNames_18), value);
	}

	inline static int32_t get_offset_of_shopTabsNames_19() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___shopTabsNames_19)); }
	inline StringU5BU5D_t1281789340* get_shopTabsNames_19() const { return ___shopTabsNames_19; }
	inline StringU5BU5D_t1281789340** get_address_of_shopTabsNames_19() { return &___shopTabsNames_19; }
	inline void set_shopTabsNames_19(StringU5BU5D_t1281789340* value)
	{
		___shopTabsNames_19 = value;
		Il2CppCodeGenWriteBarrier((&___shopTabsNames_19), value);
	}

	inline static int32_t get_offset_of_treasureTabsNames_20() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___treasureTabsNames_20)); }
	inline StringU5BU5D_t1281789340* get_treasureTabsNames_20() const { return ___treasureTabsNames_20; }
	inline StringU5BU5D_t1281789340** get_address_of_treasureTabsNames_20() { return &___treasureTabsNames_20; }
	inline void set_treasureTabsNames_20(StringU5BU5D_t1281789340* value)
	{
		___treasureTabsNames_20 = value;
		Il2CppCodeGenWriteBarrier((&___treasureTabsNames_20), value);
	}

	inline static int32_t get_offset_of_allianceTabsNames_21() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___allianceTabsNames_21)); }
	inline StringU5BU5D_t1281789340* get_allianceTabsNames_21() const { return ___allianceTabsNames_21; }
	inline StringU5BU5D_t1281789340** get_address_of_allianceTabsNames_21() { return &___allianceTabsNames_21; }
	inline void set_allianceTabsNames_21(StringU5BU5D_t1281789340* value)
	{
		___allianceTabsNames_21 = value;
		Il2CppCodeGenWriteBarrier((&___allianceTabsNames_21), value);
	}

	inline static int32_t get_offset_of_mailTabsNames_22() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___mailTabsNames_22)); }
	inline StringU5BU5D_t1281789340* get_mailTabsNames_22() const { return ___mailTabsNames_22; }
	inline StringU5BU5D_t1281789340** get_address_of_mailTabsNames_22() { return &___mailTabsNames_22; }
	inline void set_mailTabsNames_22(StringU5BU5D_t1281789340* value)
	{
		___mailTabsNames_22 = value;
		Il2CppCodeGenWriteBarrier((&___mailTabsNames_22), value);
	}

	inline static int32_t get_offset_of_chatTabsNames_23() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___chatTabsNames_23)); }
	inline StringU5BU5D_t1281789340* get_chatTabsNames_23() const { return ___chatTabsNames_23; }
	inline StringU5BU5D_t1281789340** get_address_of_chatTabsNames_23() { return &___chatTabsNames_23; }
	inline void set_chatTabsNames_23(StringU5BU5D_t1281789340* value)
	{
		___chatTabsNames_23 = value;
		Il2CppCodeGenWriteBarrier((&___chatTabsNames_23), value);
	}

	inline static int32_t get_offset_of_genericBuildingTabsNames_24() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___genericBuildingTabsNames_24)); }
	inline StringU5BU5D_t1281789340* get_genericBuildingTabsNames_24() const { return ___genericBuildingTabsNames_24; }
	inline StringU5BU5D_t1281789340** get_address_of_genericBuildingTabsNames_24() { return &___genericBuildingTabsNames_24; }
	inline void set_genericBuildingTabsNames_24(StringU5BU5D_t1281789340* value)
	{
		___genericBuildingTabsNames_24 = value;
		Il2CppCodeGenWriteBarrier((&___genericBuildingTabsNames_24), value);
	}

	inline static int32_t get_offset_of_commandCenterTabsNames_25() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___commandCenterTabsNames_25)); }
	inline StringU5BU5D_t1281789340* get_commandCenterTabsNames_25() const { return ___commandCenterTabsNames_25; }
	inline StringU5BU5D_t1281789340** get_address_of_commandCenterTabsNames_25() { return &___commandCenterTabsNames_25; }
	inline void set_commandCenterTabsNames_25(StringU5BU5D_t1281789340* value)
	{
		___commandCenterTabsNames_25 = value;
		Il2CppCodeGenWriteBarrier((&___commandCenterTabsNames_25), value);
	}

	inline static int32_t get_offset_of_archeryTabsNames_26() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___archeryTabsNames_26)); }
	inline StringU5BU5D_t1281789340* get_archeryTabsNames_26() const { return ___archeryTabsNames_26; }
	inline StringU5BU5D_t1281789340** get_address_of_archeryTabsNames_26() { return &___archeryTabsNames_26; }
	inline void set_archeryTabsNames_26(StringU5BU5D_t1281789340* value)
	{
		___archeryTabsNames_26 = value;
		Il2CppCodeGenWriteBarrier((&___archeryTabsNames_26), value);
	}

	inline static int32_t get_offset_of_enginneringTabsNames_27() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___enginneringTabsNames_27)); }
	inline StringU5BU5D_t1281789340* get_enginneringTabsNames_27() const { return ___enginneringTabsNames_27; }
	inline StringU5BU5D_t1281789340** get_address_of_enginneringTabsNames_27() { return &___enginneringTabsNames_27; }
	inline void set_enginneringTabsNames_27(StringU5BU5D_t1281789340* value)
	{
		___enginneringTabsNames_27 = value;
		Il2CppCodeGenWriteBarrier((&___enginneringTabsNames_27), value);
	}

	inline static int32_t get_offset_of_godHouseTabsNames_28() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___godHouseTabsNames_28)); }
	inline StringU5BU5D_t1281789340* get_godHouseTabsNames_28() const { return ___godHouseTabsNames_28; }
	inline StringU5BU5D_t1281789340** get_address_of_godHouseTabsNames_28() { return &___godHouseTabsNames_28; }
	inline void set_godHouseTabsNames_28(StringU5BU5D_t1281789340* value)
	{
		___godHouseTabsNames_28 = value;
		Il2CppCodeGenWriteBarrier((&___godHouseTabsNames_28), value);
	}

	inline static int32_t get_offset_of_militarySchoolTabsNames_29() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___militarySchoolTabsNames_29)); }
	inline StringU5BU5D_t1281789340* get_militarySchoolTabsNames_29() const { return ___militarySchoolTabsNames_29; }
	inline StringU5BU5D_t1281789340** get_address_of_militarySchoolTabsNames_29() { return &___militarySchoolTabsNames_29; }
	inline void set_militarySchoolTabsNames_29(StringU5BU5D_t1281789340* value)
	{
		___militarySchoolTabsNames_29 = value;
		Il2CppCodeGenWriteBarrier((&___militarySchoolTabsNames_29), value);
	}

	inline static int32_t get_offset_of_stableTabsNames_30() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___stableTabsNames_30)); }
	inline StringU5BU5D_t1281789340* get_stableTabsNames_30() const { return ___stableTabsNames_30; }
	inline StringU5BU5D_t1281789340** get_address_of_stableTabsNames_30() { return &___stableTabsNames_30; }
	inline void set_stableTabsNames_30(StringU5BU5D_t1281789340* value)
	{
		___stableTabsNames_30 = value;
		Il2CppCodeGenWriteBarrier((&___stableTabsNames_30), value);
	}

	inline static int32_t get_offset_of_dispatchTabsNames_31() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___dispatchTabsNames_31)); }
	inline StringU5BU5D_t1281789340* get_dispatchTabsNames_31() const { return ___dispatchTabsNames_31; }
	inline StringU5BU5D_t1281789340** get_address_of_dispatchTabsNames_31() { return &___dispatchTabsNames_31; }
	inline void set_dispatchTabsNames_31(StringU5BU5D_t1281789340* value)
	{
		___dispatchTabsNames_31 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchTabsNames_31), value);
	}

	inline static int32_t get_offset_of_residenceTabsNames_32() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___residenceTabsNames_32)); }
	inline StringU5BU5D_t1281789340* get_residenceTabsNames_32() const { return ___residenceTabsNames_32; }
	inline StringU5BU5D_t1281789340** get_address_of_residenceTabsNames_32() { return &___residenceTabsNames_32; }
	inline void set_residenceTabsNames_32(StringU5BU5D_t1281789340* value)
	{
		___residenceTabsNames_32 = value;
		Il2CppCodeGenWriteBarrier((&___residenceTabsNames_32), value);
	}

	inline static int32_t get_offset_of_guestHouseTabsNames_33() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___guestHouseTabsNames_33)); }
	inline StringU5BU5D_t1281789340* get_guestHouseTabsNames_33() const { return ___guestHouseTabsNames_33; }
	inline StringU5BU5D_t1281789340** get_address_of_guestHouseTabsNames_33() { return &___guestHouseTabsNames_33; }
	inline void set_guestHouseTabsNames_33(StringU5BU5D_t1281789340* value)
	{
		___guestHouseTabsNames_33 = value;
		Il2CppCodeGenWriteBarrier((&___guestHouseTabsNames_33), value);
	}

	inline static int32_t get_offset_of_feastingHallTabsNames_34() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___feastingHallTabsNames_34)); }
	inline StringU5BU5D_t1281789340* get_feastingHallTabsNames_34() const { return ___feastingHallTabsNames_34; }
	inline StringU5BU5D_t1281789340** get_address_of_feastingHallTabsNames_34() { return &___feastingHallTabsNames_34; }
	inline void set_feastingHallTabsNames_34(StringU5BU5D_t1281789340* value)
	{
		___feastingHallTabsNames_34 = value;
		Il2CppCodeGenWriteBarrier((&___feastingHallTabsNames_34), value);
	}

	inline static int32_t get_offset_of_marketTabsNames_35() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___marketTabsNames_35)); }
	inline StringU5BU5D_t1281789340* get_marketTabsNames_35() const { return ___marketTabsNames_35; }
	inline StringU5BU5D_t1281789340** get_address_of_marketTabsNames_35() { return &___marketTabsNames_35; }
	inline void set_marketTabsNames_35(StringU5BU5D_t1281789340* value)
	{
		___marketTabsNames_35 = value;
		Il2CppCodeGenWriteBarrier((&___marketTabsNames_35), value);
	}

	inline static int32_t get_offset_of_settingsTabsNames_36() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___settingsTabsNames_36)); }
	inline StringU5BU5D_t1281789340* get_settingsTabsNames_36() const { return ___settingsTabsNames_36; }
	inline StringU5BU5D_t1281789340** get_address_of_settingsTabsNames_36() { return &___settingsTabsNames_36; }
	inline void set_settingsTabsNames_36(StringU5BU5D_t1281789340* value)
	{
		___settingsTabsNames_36 = value;
		Il2CppCodeGenWriteBarrier((&___settingsTabsNames_36), value);
	}

	inline static int32_t get_offset_of_actionContents_37() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___actionContents_37)); }
	inline GameObjectU5BU5D_t3328599146* get_actionContents_37() const { return ___actionContents_37; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_actionContents_37() { return &___actionContents_37; }
	inline void set_actionContents_37(GameObjectU5BU5D_t3328599146* value)
	{
		___actionContents_37 = value;
		Il2CppCodeGenWriteBarrier((&___actionContents_37), value);
	}

	inline static int32_t get_offset_of_shopContents_38() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___shopContents_38)); }
	inline GameObjectU5BU5D_t3328599146* get_shopContents_38() const { return ___shopContents_38; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_shopContents_38() { return &___shopContents_38; }
	inline void set_shopContents_38(GameObjectU5BU5D_t3328599146* value)
	{
		___shopContents_38 = value;
		Il2CppCodeGenWriteBarrier((&___shopContents_38), value);
	}

	inline static int32_t get_offset_of_treasureContents_39() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___treasureContents_39)); }
	inline GameObjectU5BU5D_t3328599146* get_treasureContents_39() const { return ___treasureContents_39; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_treasureContents_39() { return &___treasureContents_39; }
	inline void set_treasureContents_39(GameObjectU5BU5D_t3328599146* value)
	{
		___treasureContents_39 = value;
		Il2CppCodeGenWriteBarrier((&___treasureContents_39), value);
	}

	inline static int32_t get_offset_of_allianceContents_40() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___allianceContents_40)); }
	inline GameObjectU5BU5D_t3328599146* get_allianceContents_40() const { return ___allianceContents_40; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_allianceContents_40() { return &___allianceContents_40; }
	inline void set_allianceContents_40(GameObjectU5BU5D_t3328599146* value)
	{
		___allianceContents_40 = value;
		Il2CppCodeGenWriteBarrier((&___allianceContents_40), value);
	}

	inline static int32_t get_offset_of_mailContents_41() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___mailContents_41)); }
	inline GameObjectU5BU5D_t3328599146* get_mailContents_41() const { return ___mailContents_41; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_mailContents_41() { return &___mailContents_41; }
	inline void set_mailContents_41(GameObjectU5BU5D_t3328599146* value)
	{
		___mailContents_41 = value;
		Il2CppCodeGenWriteBarrier((&___mailContents_41), value);
	}

	inline static int32_t get_offset_of_chatContents_42() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___chatContents_42)); }
	inline GameObjectU5BU5D_t3328599146* get_chatContents_42() const { return ___chatContents_42; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_chatContents_42() { return &___chatContents_42; }
	inline void set_chatContents_42(GameObjectU5BU5D_t3328599146* value)
	{
		___chatContents_42 = value;
		Il2CppCodeGenWriteBarrier((&___chatContents_42), value);
	}

	inline static int32_t get_offset_of_genericBuildingsContents_43() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___genericBuildingsContents_43)); }
	inline GameObjectU5BU5D_t3328599146* get_genericBuildingsContents_43() const { return ___genericBuildingsContents_43; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_genericBuildingsContents_43() { return &___genericBuildingsContents_43; }
	inline void set_genericBuildingsContents_43(GameObjectU5BU5D_t3328599146* value)
	{
		___genericBuildingsContents_43 = value;
		Il2CppCodeGenWriteBarrier((&___genericBuildingsContents_43), value);
	}

	inline static int32_t get_offset_of_commandCenterContents_44() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___commandCenterContents_44)); }
	inline GameObjectU5BU5D_t3328599146* get_commandCenterContents_44() const { return ___commandCenterContents_44; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_commandCenterContents_44() { return &___commandCenterContents_44; }
	inline void set_commandCenterContents_44(GameObjectU5BU5D_t3328599146* value)
	{
		___commandCenterContents_44 = value;
		Il2CppCodeGenWriteBarrier((&___commandCenterContents_44), value);
	}

	inline static int32_t get_offset_of_archeryContents_45() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___archeryContents_45)); }
	inline GameObjectU5BU5D_t3328599146* get_archeryContents_45() const { return ___archeryContents_45; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_archeryContents_45() { return &___archeryContents_45; }
	inline void set_archeryContents_45(GameObjectU5BU5D_t3328599146* value)
	{
		___archeryContents_45 = value;
		Il2CppCodeGenWriteBarrier((&___archeryContents_45), value);
	}

	inline static int32_t get_offset_of_enginneringContents_46() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___enginneringContents_46)); }
	inline GameObjectU5BU5D_t3328599146* get_enginneringContents_46() const { return ___enginneringContents_46; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_enginneringContents_46() { return &___enginneringContents_46; }
	inline void set_enginneringContents_46(GameObjectU5BU5D_t3328599146* value)
	{
		___enginneringContents_46 = value;
		Il2CppCodeGenWriteBarrier((&___enginneringContents_46), value);
	}

	inline static int32_t get_offset_of_godHouseContents_47() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___godHouseContents_47)); }
	inline GameObjectU5BU5D_t3328599146* get_godHouseContents_47() const { return ___godHouseContents_47; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_godHouseContents_47() { return &___godHouseContents_47; }
	inline void set_godHouseContents_47(GameObjectU5BU5D_t3328599146* value)
	{
		___godHouseContents_47 = value;
		Il2CppCodeGenWriteBarrier((&___godHouseContents_47), value);
	}

	inline static int32_t get_offset_of_militarySchoolContents_48() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___militarySchoolContents_48)); }
	inline GameObjectU5BU5D_t3328599146* get_militarySchoolContents_48() const { return ___militarySchoolContents_48; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_militarySchoolContents_48() { return &___militarySchoolContents_48; }
	inline void set_militarySchoolContents_48(GameObjectU5BU5D_t3328599146* value)
	{
		___militarySchoolContents_48 = value;
		Il2CppCodeGenWriteBarrier((&___militarySchoolContents_48), value);
	}

	inline static int32_t get_offset_of_stableContents_49() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___stableContents_49)); }
	inline GameObjectU5BU5D_t3328599146* get_stableContents_49() const { return ___stableContents_49; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_stableContents_49() { return &___stableContents_49; }
	inline void set_stableContents_49(GameObjectU5BU5D_t3328599146* value)
	{
		___stableContents_49 = value;
		Il2CppCodeGenWriteBarrier((&___stableContents_49), value);
	}

	inline static int32_t get_offset_of_dispatchContents_50() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___dispatchContents_50)); }
	inline GameObjectU5BU5D_t3328599146* get_dispatchContents_50() const { return ___dispatchContents_50; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_dispatchContents_50() { return &___dispatchContents_50; }
	inline void set_dispatchContents_50(GameObjectU5BU5D_t3328599146* value)
	{
		___dispatchContents_50 = value;
		Il2CppCodeGenWriteBarrier((&___dispatchContents_50), value);
	}

	inline static int32_t get_offset_of_residenceContents_51() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___residenceContents_51)); }
	inline GameObjectU5BU5D_t3328599146* get_residenceContents_51() const { return ___residenceContents_51; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_residenceContents_51() { return &___residenceContents_51; }
	inline void set_residenceContents_51(GameObjectU5BU5D_t3328599146* value)
	{
		___residenceContents_51 = value;
		Il2CppCodeGenWriteBarrier((&___residenceContents_51), value);
	}

	inline static int32_t get_offset_of_guestHouseContents_52() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___guestHouseContents_52)); }
	inline GameObjectU5BU5D_t3328599146* get_guestHouseContents_52() const { return ___guestHouseContents_52; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_guestHouseContents_52() { return &___guestHouseContents_52; }
	inline void set_guestHouseContents_52(GameObjectU5BU5D_t3328599146* value)
	{
		___guestHouseContents_52 = value;
		Il2CppCodeGenWriteBarrier((&___guestHouseContents_52), value);
	}

	inline static int32_t get_offset_of_feastingHallContents_53() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___feastingHallContents_53)); }
	inline GameObjectU5BU5D_t3328599146* get_feastingHallContents_53() const { return ___feastingHallContents_53; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_feastingHallContents_53() { return &___feastingHallContents_53; }
	inline void set_feastingHallContents_53(GameObjectU5BU5D_t3328599146* value)
	{
		___feastingHallContents_53 = value;
		Il2CppCodeGenWriteBarrier((&___feastingHallContents_53), value);
	}

	inline static int32_t get_offset_of_marketContents_54() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___marketContents_54)); }
	inline GameObjectU5BU5D_t3328599146* get_marketContents_54() const { return ___marketContents_54; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_marketContents_54() { return &___marketContents_54; }
	inline void set_marketContents_54(GameObjectU5BU5D_t3328599146* value)
	{
		___marketContents_54 = value;
		Il2CppCodeGenWriteBarrier((&___marketContents_54), value);
	}

	inline static int32_t get_offset_of_settingsContents_55() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___settingsContents_55)); }
	inline GameObjectU5BU5D_t3328599146* get_settingsContents_55() const { return ___settingsContents_55; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_settingsContents_55() { return &___settingsContents_55; }
	inline void set_settingsContents_55(GameObjectU5BU5D_t3328599146* value)
	{
		___settingsContents_55 = value;
		Il2CppCodeGenWriteBarrier((&___settingsContents_55), value);
	}

	inline static int32_t get_offset_of_contentChanger_56() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___contentChanger_56)); }
	inline MonoBehaviour_t3962482529 * get_contentChanger_56() const { return ___contentChanger_56; }
	inline MonoBehaviour_t3962482529 ** get_address_of_contentChanger_56() { return &___contentChanger_56; }
	inline void set_contentChanger_56(MonoBehaviour_t3962482529 * value)
	{
		___contentChanger_56 = value;
		Il2CppCodeGenWriteBarrier((&___contentChanger_56), value);
	}

	inline static int32_t get_offset_of_type_57() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___type_57)); }
	inline String_t* get_type_57() const { return ___type_57; }
	inline String_t** get_address_of_type_57() { return &___type_57; }
	inline void set_type_57(String_t* value)
	{
		___type_57 = value;
		Il2CppCodeGenWriteBarrier((&___type_57), value);
	}

	inline static int32_t get_offset_of_pitId_58() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759, ___pitId_58)); }
	inline int64_t get_pitId_58() const { return ___pitId_58; }
	inline int64_t* get_address_of_pitId_58() { return &___pitId_58; }
	inline void set_pitId_58(int64_t value)
	{
		___pitId_58 = value;
	}
};

struct ShellContentManager_t476539759_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShellContentManager::<>f__switch$map3
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3_59;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShellContentManager::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_60;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_59() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759_StaticFields, ___U3CU3Ef__switchU24map3_59)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3_59() const { return ___U3CU3Ef__switchU24map3_59; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3_59() { return &___U3CU3Ef__switchU24map3_59; }
	inline void set_U3CU3Ef__switchU24map3_59(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3_59 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_59), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_60() { return static_cast<int32_t>(offsetof(ShellContentManager_t476539759_StaticFields, ___U3CU3Ef__switchU24map4_60)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_60() const { return ___U3CU3Ef__switchU24map4_60; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_60() { return &___U3CU3Ef__switchU24map4_60; }
	inline void set_U3CU3Ef__switchU24map4_60(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_60 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_60), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHELLCONTENTMANAGER_T476539759_H
#ifndef TRIBUTESCONTENTMANAGER_T2446486829_H
#define TRIBUTESCONTENTMANAGER_T2446486829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TributesContentManager
struct  TributesContentManager_t2446486829  : public MonoBehaviour_t3962482529
{
public:
	// ResourcesModel TributesContentManager::tributes
	ResourcesModel_t2533508513 * ___tributes_2;
	// UnityEngine.UI.Text TributesContentManager::tributesFood
	Text_t1901882714 * ___tributesFood_3;
	// UnityEngine.UI.Text TributesContentManager::tributesWood
	Text_t1901882714 * ___tributesWood_4;
	// UnityEngine.UI.Text TributesContentManager::tributesIron
	Text_t1901882714 * ___tributesIron_5;
	// UnityEngine.UI.Text TributesContentManager::tributesStone
	Text_t1901882714 * ___tributesStone_6;
	// UnityEngine.UI.Text TributesContentManager::tributesCopper
	Text_t1901882714 * ___tributesCopper_7;
	// UnityEngine.UI.Text TributesContentManager::tributesSilver
	Text_t1901882714 * ___tributesSilver_8;
	// UnityEngine.UI.Text TributesContentManager::tributesGold
	Text_t1901882714 * ___tributesGold_9;

public:
	inline static int32_t get_offset_of_tributes_2() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributes_2)); }
	inline ResourcesModel_t2533508513 * get_tributes_2() const { return ___tributes_2; }
	inline ResourcesModel_t2533508513 ** get_address_of_tributes_2() { return &___tributes_2; }
	inline void set_tributes_2(ResourcesModel_t2533508513 * value)
	{
		___tributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___tributes_2), value);
	}

	inline static int32_t get_offset_of_tributesFood_3() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesFood_3)); }
	inline Text_t1901882714 * get_tributesFood_3() const { return ___tributesFood_3; }
	inline Text_t1901882714 ** get_address_of_tributesFood_3() { return &___tributesFood_3; }
	inline void set_tributesFood_3(Text_t1901882714 * value)
	{
		___tributesFood_3 = value;
		Il2CppCodeGenWriteBarrier((&___tributesFood_3), value);
	}

	inline static int32_t get_offset_of_tributesWood_4() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesWood_4)); }
	inline Text_t1901882714 * get_tributesWood_4() const { return ___tributesWood_4; }
	inline Text_t1901882714 ** get_address_of_tributesWood_4() { return &___tributesWood_4; }
	inline void set_tributesWood_4(Text_t1901882714 * value)
	{
		___tributesWood_4 = value;
		Il2CppCodeGenWriteBarrier((&___tributesWood_4), value);
	}

	inline static int32_t get_offset_of_tributesIron_5() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesIron_5)); }
	inline Text_t1901882714 * get_tributesIron_5() const { return ___tributesIron_5; }
	inline Text_t1901882714 ** get_address_of_tributesIron_5() { return &___tributesIron_5; }
	inline void set_tributesIron_5(Text_t1901882714 * value)
	{
		___tributesIron_5 = value;
		Il2CppCodeGenWriteBarrier((&___tributesIron_5), value);
	}

	inline static int32_t get_offset_of_tributesStone_6() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesStone_6)); }
	inline Text_t1901882714 * get_tributesStone_6() const { return ___tributesStone_6; }
	inline Text_t1901882714 ** get_address_of_tributesStone_6() { return &___tributesStone_6; }
	inline void set_tributesStone_6(Text_t1901882714 * value)
	{
		___tributesStone_6 = value;
		Il2CppCodeGenWriteBarrier((&___tributesStone_6), value);
	}

	inline static int32_t get_offset_of_tributesCopper_7() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesCopper_7)); }
	inline Text_t1901882714 * get_tributesCopper_7() const { return ___tributesCopper_7; }
	inline Text_t1901882714 ** get_address_of_tributesCopper_7() { return &___tributesCopper_7; }
	inline void set_tributesCopper_7(Text_t1901882714 * value)
	{
		___tributesCopper_7 = value;
		Il2CppCodeGenWriteBarrier((&___tributesCopper_7), value);
	}

	inline static int32_t get_offset_of_tributesSilver_8() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesSilver_8)); }
	inline Text_t1901882714 * get_tributesSilver_8() const { return ___tributesSilver_8; }
	inline Text_t1901882714 ** get_address_of_tributesSilver_8() { return &___tributesSilver_8; }
	inline void set_tributesSilver_8(Text_t1901882714 * value)
	{
		___tributesSilver_8 = value;
		Il2CppCodeGenWriteBarrier((&___tributesSilver_8), value);
	}

	inline static int32_t get_offset_of_tributesGold_9() { return static_cast<int32_t>(offsetof(TributesContentManager_t2446486829, ___tributesGold_9)); }
	inline Text_t1901882714 * get_tributesGold_9() const { return ___tributesGold_9; }
	inline Text_t1901882714 ** get_address_of_tributesGold_9() { return &___tributesGold_9; }
	inline void set_tributesGold_9(Text_t1901882714 * value)
	{
		___tributesGold_9 = value;
		Il2CppCodeGenWriteBarrier((&___tributesGold_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIBUTESCONTENTMANAGER_T2446486829_H
#ifndef TUTORIALCONTENTMANAGER_T1277416346_H
#define TUTORIALCONTENTMANAGER_T1277416346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialContentManager
struct  TutorialContentManager_t1277416346  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text TutorialContentManager::pageTitle
	Text_t1901882714 * ___pageTitle_2;
	// UnityEngine.UI.Text TutorialContentManager::pageText
	Text_t1901882714 * ___pageText_3;
	// UnityEngine.UI.Image TutorialContentManager::pageImage
	Image_t2670269651 * ___pageImage_4;
	// System.Int32 TutorialContentManager::currrentPage
	int32_t ___currrentPage_5;

public:
	inline static int32_t get_offset_of_pageTitle_2() { return static_cast<int32_t>(offsetof(TutorialContentManager_t1277416346, ___pageTitle_2)); }
	inline Text_t1901882714 * get_pageTitle_2() const { return ___pageTitle_2; }
	inline Text_t1901882714 ** get_address_of_pageTitle_2() { return &___pageTitle_2; }
	inline void set_pageTitle_2(Text_t1901882714 * value)
	{
		___pageTitle_2 = value;
		Il2CppCodeGenWriteBarrier((&___pageTitle_2), value);
	}

	inline static int32_t get_offset_of_pageText_3() { return static_cast<int32_t>(offsetof(TutorialContentManager_t1277416346, ___pageText_3)); }
	inline Text_t1901882714 * get_pageText_3() const { return ___pageText_3; }
	inline Text_t1901882714 ** get_address_of_pageText_3() { return &___pageText_3; }
	inline void set_pageText_3(Text_t1901882714 * value)
	{
		___pageText_3 = value;
		Il2CppCodeGenWriteBarrier((&___pageText_3), value);
	}

	inline static int32_t get_offset_of_pageImage_4() { return static_cast<int32_t>(offsetof(TutorialContentManager_t1277416346, ___pageImage_4)); }
	inline Image_t2670269651 * get_pageImage_4() const { return ___pageImage_4; }
	inline Image_t2670269651 ** get_address_of_pageImage_4() { return &___pageImage_4; }
	inline void set_pageImage_4(Image_t2670269651 * value)
	{
		___pageImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___pageImage_4), value);
	}

	inline static int32_t get_offset_of_currrentPage_5() { return static_cast<int32_t>(offsetof(TutorialContentManager_t1277416346, ___currrentPage_5)); }
	inline int32_t get_currrentPage_5() const { return ___currrentPage_5; }
	inline int32_t* get_address_of_currrentPage_5() { return &___currrentPage_5; }
	inline void set_currrentPage_5(int32_t value)
	{
		___currrentPage_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCONTENTMANAGER_T1277416346_H
#ifndef TRAININGICONMANAGER_T1144058280_H
#define TRAININGICONMANAGER_T1144058280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingIconManager
struct  TrainingIconManager_t1144058280  : public MonoBehaviour_t3962482529
{
public:
	// System.String TrainingIconManager::internalUnitName
	String_t* ___internalUnitName_2;
	// UnityEngine.UI.Text TrainingIconManager::foodPrice
	Text_t1901882714 * ___foodPrice_3;
	// UnityEngine.UI.Text TrainingIconManager::woodPrice
	Text_t1901882714 * ___woodPrice_4;
	// UnityEngine.UI.Text TrainingIconManager::stonePrice
	Text_t1901882714 * ___stonePrice_5;
	// UnityEngine.UI.Text TrainingIconManager::ironPrice
	Text_t1901882714 * ___ironPrice_6;
	// UnityEngine.UI.Text TrainingIconManager::copperPrice
	Text_t1901882714 * ___copperPrice_7;
	// UnityEngine.UI.Text TrainingIconManager::silverPrice
	Text_t1901882714 * ___silverPrice_8;
	// UnityEngine.UI.Text TrainingIconManager::goldPrice
	Text_t1901882714 * ___goldPrice_9;
	// UnityEngine.UI.Text TrainingIconManager::trainingTime
	Text_t1901882714 * ___trainingTime_10;
	// UnityEngine.UI.Text TrainingIconManager::unitCount
	Text_t1901882714 * ___unitCount_11;
	// UnityEngine.UI.Text TrainingIconManager::unitName
	Text_t1901882714 * ___unitName_12;

public:
	inline static int32_t get_offset_of_internalUnitName_2() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___internalUnitName_2)); }
	inline String_t* get_internalUnitName_2() const { return ___internalUnitName_2; }
	inline String_t** get_address_of_internalUnitName_2() { return &___internalUnitName_2; }
	inline void set_internalUnitName_2(String_t* value)
	{
		___internalUnitName_2 = value;
		Il2CppCodeGenWriteBarrier((&___internalUnitName_2), value);
	}

	inline static int32_t get_offset_of_foodPrice_3() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___foodPrice_3)); }
	inline Text_t1901882714 * get_foodPrice_3() const { return ___foodPrice_3; }
	inline Text_t1901882714 ** get_address_of_foodPrice_3() { return &___foodPrice_3; }
	inline void set_foodPrice_3(Text_t1901882714 * value)
	{
		___foodPrice_3 = value;
		Il2CppCodeGenWriteBarrier((&___foodPrice_3), value);
	}

	inline static int32_t get_offset_of_woodPrice_4() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___woodPrice_4)); }
	inline Text_t1901882714 * get_woodPrice_4() const { return ___woodPrice_4; }
	inline Text_t1901882714 ** get_address_of_woodPrice_4() { return &___woodPrice_4; }
	inline void set_woodPrice_4(Text_t1901882714 * value)
	{
		___woodPrice_4 = value;
		Il2CppCodeGenWriteBarrier((&___woodPrice_4), value);
	}

	inline static int32_t get_offset_of_stonePrice_5() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___stonePrice_5)); }
	inline Text_t1901882714 * get_stonePrice_5() const { return ___stonePrice_5; }
	inline Text_t1901882714 ** get_address_of_stonePrice_5() { return &___stonePrice_5; }
	inline void set_stonePrice_5(Text_t1901882714 * value)
	{
		___stonePrice_5 = value;
		Il2CppCodeGenWriteBarrier((&___stonePrice_5), value);
	}

	inline static int32_t get_offset_of_ironPrice_6() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___ironPrice_6)); }
	inline Text_t1901882714 * get_ironPrice_6() const { return ___ironPrice_6; }
	inline Text_t1901882714 ** get_address_of_ironPrice_6() { return &___ironPrice_6; }
	inline void set_ironPrice_6(Text_t1901882714 * value)
	{
		___ironPrice_6 = value;
		Il2CppCodeGenWriteBarrier((&___ironPrice_6), value);
	}

	inline static int32_t get_offset_of_copperPrice_7() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___copperPrice_7)); }
	inline Text_t1901882714 * get_copperPrice_7() const { return ___copperPrice_7; }
	inline Text_t1901882714 ** get_address_of_copperPrice_7() { return &___copperPrice_7; }
	inline void set_copperPrice_7(Text_t1901882714 * value)
	{
		___copperPrice_7 = value;
		Il2CppCodeGenWriteBarrier((&___copperPrice_7), value);
	}

	inline static int32_t get_offset_of_silverPrice_8() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___silverPrice_8)); }
	inline Text_t1901882714 * get_silverPrice_8() const { return ___silverPrice_8; }
	inline Text_t1901882714 ** get_address_of_silverPrice_8() { return &___silverPrice_8; }
	inline void set_silverPrice_8(Text_t1901882714 * value)
	{
		___silverPrice_8 = value;
		Il2CppCodeGenWriteBarrier((&___silverPrice_8), value);
	}

	inline static int32_t get_offset_of_goldPrice_9() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___goldPrice_9)); }
	inline Text_t1901882714 * get_goldPrice_9() const { return ___goldPrice_9; }
	inline Text_t1901882714 ** get_address_of_goldPrice_9() { return &___goldPrice_9; }
	inline void set_goldPrice_9(Text_t1901882714 * value)
	{
		___goldPrice_9 = value;
		Il2CppCodeGenWriteBarrier((&___goldPrice_9), value);
	}

	inline static int32_t get_offset_of_trainingTime_10() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___trainingTime_10)); }
	inline Text_t1901882714 * get_trainingTime_10() const { return ___trainingTime_10; }
	inline Text_t1901882714 ** get_address_of_trainingTime_10() { return &___trainingTime_10; }
	inline void set_trainingTime_10(Text_t1901882714 * value)
	{
		___trainingTime_10 = value;
		Il2CppCodeGenWriteBarrier((&___trainingTime_10), value);
	}

	inline static int32_t get_offset_of_unitCount_11() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___unitCount_11)); }
	inline Text_t1901882714 * get_unitCount_11() const { return ___unitCount_11; }
	inline Text_t1901882714 ** get_address_of_unitCount_11() { return &___unitCount_11; }
	inline void set_unitCount_11(Text_t1901882714 * value)
	{
		___unitCount_11 = value;
		Il2CppCodeGenWriteBarrier((&___unitCount_11), value);
	}

	inline static int32_t get_offset_of_unitName_12() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280, ___unitName_12)); }
	inline Text_t1901882714 * get_unitName_12() const { return ___unitName_12; }
	inline Text_t1901882714 ** get_address_of_unitName_12() { return &___unitName_12; }
	inline void set_unitName_12(Text_t1901882714 * value)
	{
		___unitName_12 = value;
		Il2CppCodeGenWriteBarrier((&___unitName_12), value);
	}
};

struct TrainingIconManager_t1144058280_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> TrainingIconManager::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> TrainingIconManager::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_13() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280_StaticFields, ___U3CU3Ef__switchU24map5_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_13() const { return ___U3CU3Ef__switchU24map5_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_13() { return &___U3CU3Ef__switchU24map5_13; }
	inline void set_U3CU3Ef__switchU24map5_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_14() { return static_cast<int32_t>(offsetof(TrainingIconManager_t1144058280_StaticFields, ___U3CU3Ef__switchU24map6_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_14() const { return ___U3CU3Ef__switchU24map6_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_14() { return &___U3CU3Ef__switchU24map6_14; }
	inline void set_U3CU3Ef__switchU24map6_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAININGICONMANAGER_T1144058280_H
#ifndef SHIELDTIMECONTENTMANAGER_T1076880368_H
#define SHIELDTIMECONTENTMANAGER_T1076880368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldTimeContentManager
struct  ShieldTimeContentManager_t1076880368  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ShieldTimeContentManager::hour
	bool ___hour_2;
	// IReloadable ShieldTimeContentManager::owner
	RuntimeObject* ___owner_3;
	// System.String ShieldTimeContentManager::itemId
	String_t* ___itemId_4;
	// UnityEngine.UI.Text ShieldTimeContentManager::timeLabel
	Text_t1901882714 * ___timeLabel_5;
	// UnityEngine.UI.Dropdown ShieldTimeContentManager::timeValue
	Dropdown_t2274391225 * ___timeValue_6;

public:
	inline static int32_t get_offset_of_hour_2() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368, ___hour_2)); }
	inline bool get_hour_2() const { return ___hour_2; }
	inline bool* get_address_of_hour_2() { return &___hour_2; }
	inline void set_hour_2(bool value)
	{
		___hour_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368, ___owner_3)); }
	inline RuntimeObject* get_owner_3() const { return ___owner_3; }
	inline RuntimeObject** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(RuntimeObject* value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier((&___owner_3), value);
	}

	inline static int32_t get_offset_of_itemId_4() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368, ___itemId_4)); }
	inline String_t* get_itemId_4() const { return ___itemId_4; }
	inline String_t** get_address_of_itemId_4() { return &___itemId_4; }
	inline void set_itemId_4(String_t* value)
	{
		___itemId_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_4), value);
	}

	inline static int32_t get_offset_of_timeLabel_5() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368, ___timeLabel_5)); }
	inline Text_t1901882714 * get_timeLabel_5() const { return ___timeLabel_5; }
	inline Text_t1901882714 ** get_address_of_timeLabel_5() { return &___timeLabel_5; }
	inline void set_timeLabel_5(Text_t1901882714 * value)
	{
		___timeLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___timeLabel_5), value);
	}

	inline static int32_t get_offset_of_timeValue_6() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368, ___timeValue_6)); }
	inline Dropdown_t2274391225 * get_timeValue_6() const { return ___timeValue_6; }
	inline Dropdown_t2274391225 ** get_address_of_timeValue_6() { return &___timeValue_6; }
	inline void set_timeValue_6(Dropdown_t2274391225 * value)
	{
		___timeValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeValue_6), value);
	}
};

struct ShieldTimeContentManager_t1076880368_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> ShieldTimeContentManager::days
	List_1_t3319525431 * ___days_7;

public:
	inline static int32_t get_offset_of_days_7() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t1076880368_StaticFields, ___days_7)); }
	inline List_1_t3319525431 * get_days_7() const { return ___days_7; }
	inline List_1_t3319525431 ** get_address_of_days_7() { return &___days_7; }
	inline void set_days_7(List_1_t3319525431 * value)
	{
		___days_7 = value;
		Il2CppCodeGenWriteBarrier((&___days_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELDTIMECONTENTMANAGER_T1076880368_H
#ifndef STATISTICSMANAGER_T1700503451_H
#define STATISTICSMANAGER_T1700503451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatisticsManager
struct  StatisticsManager_t1700503451  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject StatisticsManager::resourcesPanel
	GameObject_t1113636619 * ___resourcesPanel_2;
	// UnityEngine.GameObject StatisticsManager::knightsPanel
	GameObject_t1113636619 * ___knightsPanel_3;
	// UnityEngine.GameObject StatisticsManager::unitsPanel
	GameObject_t1113636619 * ___unitsPanel_4;
	// UnityEngine.UI.Text StatisticsManager::cityFood
	Text_t1901882714 * ___cityFood_5;
	// UnityEngine.UI.Text StatisticsManager::cityWood
	Text_t1901882714 * ___cityWood_6;
	// UnityEngine.UI.Text StatisticsManager::cityIron
	Text_t1901882714 * ___cityIron_7;
	// UnityEngine.UI.Text StatisticsManager::cityStone
	Text_t1901882714 * ___cityStone_8;
	// UnityEngine.UI.Text StatisticsManager::cityCopper
	Text_t1901882714 * ___cityCopper_9;
	// UnityEngine.UI.Text StatisticsManager::citySilver
	Text_t1901882714 * ___citySilver_10;
	// UnityEngine.UI.Text StatisticsManager::cityGold
	Text_t1901882714 * ___cityGold_11;
	// UnityEngine.UI.Text StatisticsManager::cityWorker
	Text_t1901882714 * ___cityWorker_12;
	// UnityEngine.UI.Text StatisticsManager::citySpy
	Text_t1901882714 * ___citySpy_13;
	// UnityEngine.UI.Text StatisticsManager::citySwordsman
	Text_t1901882714 * ___citySwordsman_14;
	// UnityEngine.UI.Text StatisticsManager::citySpearman
	Text_t1901882714 * ___citySpearman_15;
	// UnityEngine.UI.Text StatisticsManager::cityPikeman
	Text_t1901882714 * ___cityPikeman_16;
	// UnityEngine.UI.Text StatisticsManager::cityScoutRider
	Text_t1901882714 * ___cityScoutRider_17;
	// UnityEngine.UI.Text StatisticsManager::cityLightCavalry
	Text_t1901882714 * ___cityLightCavalry_18;
	// UnityEngine.UI.Text StatisticsManager::cityHeavyCavalry
	Text_t1901882714 * ___cityHeavyCavalry_19;
	// UnityEngine.UI.Text StatisticsManager::cityArcher
	Text_t1901882714 * ___cityArcher_20;
	// UnityEngine.UI.Text StatisticsManager::cityArcherRider
	Text_t1901882714 * ___cityArcherRider_21;
	// UnityEngine.UI.Text StatisticsManager::cityHollyman
	Text_t1901882714 * ___cityHollyman_22;
	// UnityEngine.UI.Text StatisticsManager::cityWagon
	Text_t1901882714 * ___cityWagon_23;
	// UnityEngine.UI.Text StatisticsManager::cityTrebuchet
	Text_t1901882714 * ___cityTrebuchet_24;
	// UnityEngine.UI.Text StatisticsManager::citySiegeTower
	Text_t1901882714 * ___citySiegeTower_25;
	// UnityEngine.UI.Text StatisticsManager::cityBatteringRam
	Text_t1901882714 * ___cityBatteringRam_26;
	// UnityEngine.UI.Text StatisticsManager::cityBallista
	Text_t1901882714 * ___cityBallista_27;
	// UnityEngine.UI.Text StatisticsManager::cityBeaconTower
	Text_t1901882714 * ___cityBeaconTower_28;
	// UnityEngine.UI.Text StatisticsManager::cityArcherTower
	Text_t1901882714 * ___cityArcherTower_29;
	// UnityEngine.UI.Text StatisticsManager::cityHotOilHole
	Text_t1901882714 * ___cityHotOilHole_30;
	// UnityEngine.UI.Text StatisticsManager::cityTrebuchetTower
	Text_t1901882714 * ___cityTrebuchetTower_31;
	// UnityEngine.UI.Text StatisticsManager::cityBallistaTower
	Text_t1901882714 * ___cityBallistaTower_32;

public:
	inline static int32_t get_offset_of_resourcesPanel_2() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___resourcesPanel_2)); }
	inline GameObject_t1113636619 * get_resourcesPanel_2() const { return ___resourcesPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_resourcesPanel_2() { return &___resourcesPanel_2; }
	inline void set_resourcesPanel_2(GameObject_t1113636619 * value)
	{
		___resourcesPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___resourcesPanel_2), value);
	}

	inline static int32_t get_offset_of_knightsPanel_3() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___knightsPanel_3)); }
	inline GameObject_t1113636619 * get_knightsPanel_3() const { return ___knightsPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_knightsPanel_3() { return &___knightsPanel_3; }
	inline void set_knightsPanel_3(GameObject_t1113636619 * value)
	{
		___knightsPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___knightsPanel_3), value);
	}

	inline static int32_t get_offset_of_unitsPanel_4() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___unitsPanel_4)); }
	inline GameObject_t1113636619 * get_unitsPanel_4() const { return ___unitsPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_unitsPanel_4() { return &___unitsPanel_4; }
	inline void set_unitsPanel_4(GameObject_t1113636619 * value)
	{
		___unitsPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitsPanel_4), value);
	}

	inline static int32_t get_offset_of_cityFood_5() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityFood_5)); }
	inline Text_t1901882714 * get_cityFood_5() const { return ___cityFood_5; }
	inline Text_t1901882714 ** get_address_of_cityFood_5() { return &___cityFood_5; }
	inline void set_cityFood_5(Text_t1901882714 * value)
	{
		___cityFood_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityFood_5), value);
	}

	inline static int32_t get_offset_of_cityWood_6() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityWood_6)); }
	inline Text_t1901882714 * get_cityWood_6() const { return ___cityWood_6; }
	inline Text_t1901882714 ** get_address_of_cityWood_6() { return &___cityWood_6; }
	inline void set_cityWood_6(Text_t1901882714 * value)
	{
		___cityWood_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityWood_6), value);
	}

	inline static int32_t get_offset_of_cityIron_7() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityIron_7)); }
	inline Text_t1901882714 * get_cityIron_7() const { return ___cityIron_7; }
	inline Text_t1901882714 ** get_address_of_cityIron_7() { return &___cityIron_7; }
	inline void set_cityIron_7(Text_t1901882714 * value)
	{
		___cityIron_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityIron_7), value);
	}

	inline static int32_t get_offset_of_cityStone_8() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityStone_8)); }
	inline Text_t1901882714 * get_cityStone_8() const { return ___cityStone_8; }
	inline Text_t1901882714 ** get_address_of_cityStone_8() { return &___cityStone_8; }
	inline void set_cityStone_8(Text_t1901882714 * value)
	{
		___cityStone_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityStone_8), value);
	}

	inline static int32_t get_offset_of_cityCopper_9() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityCopper_9)); }
	inline Text_t1901882714 * get_cityCopper_9() const { return ___cityCopper_9; }
	inline Text_t1901882714 ** get_address_of_cityCopper_9() { return &___cityCopper_9; }
	inline void set_cityCopper_9(Text_t1901882714 * value)
	{
		___cityCopper_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityCopper_9), value);
	}

	inline static int32_t get_offset_of_citySilver_10() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___citySilver_10)); }
	inline Text_t1901882714 * get_citySilver_10() const { return ___citySilver_10; }
	inline Text_t1901882714 ** get_address_of_citySilver_10() { return &___citySilver_10; }
	inline void set_citySilver_10(Text_t1901882714 * value)
	{
		___citySilver_10 = value;
		Il2CppCodeGenWriteBarrier((&___citySilver_10), value);
	}

	inline static int32_t get_offset_of_cityGold_11() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityGold_11)); }
	inline Text_t1901882714 * get_cityGold_11() const { return ___cityGold_11; }
	inline Text_t1901882714 ** get_address_of_cityGold_11() { return &___cityGold_11; }
	inline void set_cityGold_11(Text_t1901882714 * value)
	{
		___cityGold_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityGold_11), value);
	}

	inline static int32_t get_offset_of_cityWorker_12() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityWorker_12)); }
	inline Text_t1901882714 * get_cityWorker_12() const { return ___cityWorker_12; }
	inline Text_t1901882714 ** get_address_of_cityWorker_12() { return &___cityWorker_12; }
	inline void set_cityWorker_12(Text_t1901882714 * value)
	{
		___cityWorker_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityWorker_12), value);
	}

	inline static int32_t get_offset_of_citySpy_13() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___citySpy_13)); }
	inline Text_t1901882714 * get_citySpy_13() const { return ___citySpy_13; }
	inline Text_t1901882714 ** get_address_of_citySpy_13() { return &___citySpy_13; }
	inline void set_citySpy_13(Text_t1901882714 * value)
	{
		___citySpy_13 = value;
		Il2CppCodeGenWriteBarrier((&___citySpy_13), value);
	}

	inline static int32_t get_offset_of_citySwordsman_14() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___citySwordsman_14)); }
	inline Text_t1901882714 * get_citySwordsman_14() const { return ___citySwordsman_14; }
	inline Text_t1901882714 ** get_address_of_citySwordsman_14() { return &___citySwordsman_14; }
	inline void set_citySwordsman_14(Text_t1901882714 * value)
	{
		___citySwordsman_14 = value;
		Il2CppCodeGenWriteBarrier((&___citySwordsman_14), value);
	}

	inline static int32_t get_offset_of_citySpearman_15() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___citySpearman_15)); }
	inline Text_t1901882714 * get_citySpearman_15() const { return ___citySpearman_15; }
	inline Text_t1901882714 ** get_address_of_citySpearman_15() { return &___citySpearman_15; }
	inline void set_citySpearman_15(Text_t1901882714 * value)
	{
		___citySpearman_15 = value;
		Il2CppCodeGenWriteBarrier((&___citySpearman_15), value);
	}

	inline static int32_t get_offset_of_cityPikeman_16() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityPikeman_16)); }
	inline Text_t1901882714 * get_cityPikeman_16() const { return ___cityPikeman_16; }
	inline Text_t1901882714 ** get_address_of_cityPikeman_16() { return &___cityPikeman_16; }
	inline void set_cityPikeman_16(Text_t1901882714 * value)
	{
		___cityPikeman_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityPikeman_16), value);
	}

	inline static int32_t get_offset_of_cityScoutRider_17() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityScoutRider_17)); }
	inline Text_t1901882714 * get_cityScoutRider_17() const { return ___cityScoutRider_17; }
	inline Text_t1901882714 ** get_address_of_cityScoutRider_17() { return &___cityScoutRider_17; }
	inline void set_cityScoutRider_17(Text_t1901882714 * value)
	{
		___cityScoutRider_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityScoutRider_17), value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_18() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityLightCavalry_18)); }
	inline Text_t1901882714 * get_cityLightCavalry_18() const { return ___cityLightCavalry_18; }
	inline Text_t1901882714 ** get_address_of_cityLightCavalry_18() { return &___cityLightCavalry_18; }
	inline void set_cityLightCavalry_18(Text_t1901882714 * value)
	{
		___cityLightCavalry_18 = value;
		Il2CppCodeGenWriteBarrier((&___cityLightCavalry_18), value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_19() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityHeavyCavalry_19)); }
	inline Text_t1901882714 * get_cityHeavyCavalry_19() const { return ___cityHeavyCavalry_19; }
	inline Text_t1901882714 ** get_address_of_cityHeavyCavalry_19() { return &___cityHeavyCavalry_19; }
	inline void set_cityHeavyCavalry_19(Text_t1901882714 * value)
	{
		___cityHeavyCavalry_19 = value;
		Il2CppCodeGenWriteBarrier((&___cityHeavyCavalry_19), value);
	}

	inline static int32_t get_offset_of_cityArcher_20() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityArcher_20)); }
	inline Text_t1901882714 * get_cityArcher_20() const { return ___cityArcher_20; }
	inline Text_t1901882714 ** get_address_of_cityArcher_20() { return &___cityArcher_20; }
	inline void set_cityArcher_20(Text_t1901882714 * value)
	{
		___cityArcher_20 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcher_20), value);
	}

	inline static int32_t get_offset_of_cityArcherRider_21() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityArcherRider_21)); }
	inline Text_t1901882714 * get_cityArcherRider_21() const { return ___cityArcherRider_21; }
	inline Text_t1901882714 ** get_address_of_cityArcherRider_21() { return &___cityArcherRider_21; }
	inline void set_cityArcherRider_21(Text_t1901882714 * value)
	{
		___cityArcherRider_21 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherRider_21), value);
	}

	inline static int32_t get_offset_of_cityHollyman_22() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityHollyman_22)); }
	inline Text_t1901882714 * get_cityHollyman_22() const { return ___cityHollyman_22; }
	inline Text_t1901882714 ** get_address_of_cityHollyman_22() { return &___cityHollyman_22; }
	inline void set_cityHollyman_22(Text_t1901882714 * value)
	{
		___cityHollyman_22 = value;
		Il2CppCodeGenWriteBarrier((&___cityHollyman_22), value);
	}

	inline static int32_t get_offset_of_cityWagon_23() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityWagon_23)); }
	inline Text_t1901882714 * get_cityWagon_23() const { return ___cityWagon_23; }
	inline Text_t1901882714 ** get_address_of_cityWagon_23() { return &___cityWagon_23; }
	inline void set_cityWagon_23(Text_t1901882714 * value)
	{
		___cityWagon_23 = value;
		Il2CppCodeGenWriteBarrier((&___cityWagon_23), value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_24() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityTrebuchet_24)); }
	inline Text_t1901882714 * get_cityTrebuchet_24() const { return ___cityTrebuchet_24; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchet_24() { return &___cityTrebuchet_24; }
	inline void set_cityTrebuchet_24(Text_t1901882714 * value)
	{
		___cityTrebuchet_24 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchet_24), value);
	}

	inline static int32_t get_offset_of_citySiegeTower_25() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___citySiegeTower_25)); }
	inline Text_t1901882714 * get_citySiegeTower_25() const { return ___citySiegeTower_25; }
	inline Text_t1901882714 ** get_address_of_citySiegeTower_25() { return &___citySiegeTower_25; }
	inline void set_citySiegeTower_25(Text_t1901882714 * value)
	{
		___citySiegeTower_25 = value;
		Il2CppCodeGenWriteBarrier((&___citySiegeTower_25), value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_26() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityBatteringRam_26)); }
	inline Text_t1901882714 * get_cityBatteringRam_26() const { return ___cityBatteringRam_26; }
	inline Text_t1901882714 ** get_address_of_cityBatteringRam_26() { return &___cityBatteringRam_26; }
	inline void set_cityBatteringRam_26(Text_t1901882714 * value)
	{
		___cityBatteringRam_26 = value;
		Il2CppCodeGenWriteBarrier((&___cityBatteringRam_26), value);
	}

	inline static int32_t get_offset_of_cityBallista_27() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityBallista_27)); }
	inline Text_t1901882714 * get_cityBallista_27() const { return ___cityBallista_27; }
	inline Text_t1901882714 ** get_address_of_cityBallista_27() { return &___cityBallista_27; }
	inline void set_cityBallista_27(Text_t1901882714 * value)
	{
		___cityBallista_27 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallista_27), value);
	}

	inline static int32_t get_offset_of_cityBeaconTower_28() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityBeaconTower_28)); }
	inline Text_t1901882714 * get_cityBeaconTower_28() const { return ___cityBeaconTower_28; }
	inline Text_t1901882714 ** get_address_of_cityBeaconTower_28() { return &___cityBeaconTower_28; }
	inline void set_cityBeaconTower_28(Text_t1901882714 * value)
	{
		___cityBeaconTower_28 = value;
		Il2CppCodeGenWriteBarrier((&___cityBeaconTower_28), value);
	}

	inline static int32_t get_offset_of_cityArcherTower_29() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityArcherTower_29)); }
	inline Text_t1901882714 * get_cityArcherTower_29() const { return ___cityArcherTower_29; }
	inline Text_t1901882714 ** get_address_of_cityArcherTower_29() { return &___cityArcherTower_29; }
	inline void set_cityArcherTower_29(Text_t1901882714 * value)
	{
		___cityArcherTower_29 = value;
		Il2CppCodeGenWriteBarrier((&___cityArcherTower_29), value);
	}

	inline static int32_t get_offset_of_cityHotOilHole_30() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityHotOilHole_30)); }
	inline Text_t1901882714 * get_cityHotOilHole_30() const { return ___cityHotOilHole_30; }
	inline Text_t1901882714 ** get_address_of_cityHotOilHole_30() { return &___cityHotOilHole_30; }
	inline void set_cityHotOilHole_30(Text_t1901882714 * value)
	{
		___cityHotOilHole_30 = value;
		Il2CppCodeGenWriteBarrier((&___cityHotOilHole_30), value);
	}

	inline static int32_t get_offset_of_cityTrebuchetTower_31() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityTrebuchetTower_31)); }
	inline Text_t1901882714 * get_cityTrebuchetTower_31() const { return ___cityTrebuchetTower_31; }
	inline Text_t1901882714 ** get_address_of_cityTrebuchetTower_31() { return &___cityTrebuchetTower_31; }
	inline void set_cityTrebuchetTower_31(Text_t1901882714 * value)
	{
		___cityTrebuchetTower_31 = value;
		Il2CppCodeGenWriteBarrier((&___cityTrebuchetTower_31), value);
	}

	inline static int32_t get_offset_of_cityBallistaTower_32() { return static_cast<int32_t>(offsetof(StatisticsManager_t1700503451, ___cityBallistaTower_32)); }
	inline Text_t1901882714 * get_cityBallistaTower_32() const { return ___cityBallistaTower_32; }
	inline Text_t1901882714 ** get_address_of_cityBallistaTower_32() { return &___cityBallistaTower_32; }
	inline void set_cityBallistaTower_32(Text_t1901882714 * value)
	{
		___cityBallistaTower_32 = value;
		Il2CppCodeGenWriteBarrier((&___cityBallistaTower_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATISTICSMANAGER_T1700503451_H
#ifndef RESIDENCECONTENTCHANGER_T4286680255_H
#define RESIDENCECONTENTCHANGER_T4286680255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceContentChanger
struct  ResidenceContentChanger_t4286680255  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ResidenceContentChanger::contentLabel
	Text_t1901882714 * ___contentLabel_2;
	// UnityEngine.UI.Text ResidenceContentChanger::cityName
	Text_t1901882714 * ___cityName_3;
	// UnityEngine.UI.InputField ResidenceContentChanger::cityNameInput
	InputField_t3762917431 * ___cityNameInput_4;
	// UnityEngine.UI.Image ResidenceContentChanger::cityIcon
	Image_t2670269651 * ___cityIcon_5;
	// UnityEngine.GameObject ResidenceContentChanger::buildingsContent
	GameObject_t1113636619 * ___buildingsContent_6;
	// UnityEngine.GameObject ResidenceContentChanger::productionContent
	GameObject_t1113636619 * ___productionContent_7;
	// UnityEngine.GameObject ResidenceContentChanger::citiesContent
	GameObject_t1113636619 * ___citiesContent_8;
	// UnityEngine.GameObject ResidenceContentChanger::valleysContent
	GameObject_t1113636619 * ___valleysContent_9;
	// UnityEngine.GameObject ResidenceContentChanger::coloniesContent
	GameObject_t1113636619 * ___coloniesContent_10;
	// System.Int64 ResidenceContentChanger::iconIndex
	int64_t ___iconIndex_11;

public:
	inline static int32_t get_offset_of_contentLabel_2() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___contentLabel_2)); }
	inline Text_t1901882714 * get_contentLabel_2() const { return ___contentLabel_2; }
	inline Text_t1901882714 ** get_address_of_contentLabel_2() { return &___contentLabel_2; }
	inline void set_contentLabel_2(Text_t1901882714 * value)
	{
		___contentLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentLabel_2), value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___cityName_3)); }
	inline Text_t1901882714 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t1901882714 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t1901882714 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_3), value);
	}

	inline static int32_t get_offset_of_cityNameInput_4() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___cityNameInput_4)); }
	inline InputField_t3762917431 * get_cityNameInput_4() const { return ___cityNameInput_4; }
	inline InputField_t3762917431 ** get_address_of_cityNameInput_4() { return &___cityNameInput_4; }
	inline void set_cityNameInput_4(InputField_t3762917431 * value)
	{
		___cityNameInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityNameInput_4), value);
	}

	inline static int32_t get_offset_of_cityIcon_5() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___cityIcon_5)); }
	inline Image_t2670269651 * get_cityIcon_5() const { return ___cityIcon_5; }
	inline Image_t2670269651 ** get_address_of_cityIcon_5() { return &___cityIcon_5; }
	inline void set_cityIcon_5(Image_t2670269651 * value)
	{
		___cityIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityIcon_5), value);
	}

	inline static int32_t get_offset_of_buildingsContent_6() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___buildingsContent_6)); }
	inline GameObject_t1113636619 * get_buildingsContent_6() const { return ___buildingsContent_6; }
	inline GameObject_t1113636619 ** get_address_of_buildingsContent_6() { return &___buildingsContent_6; }
	inline void set_buildingsContent_6(GameObject_t1113636619 * value)
	{
		___buildingsContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsContent_6), value);
	}

	inline static int32_t get_offset_of_productionContent_7() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___productionContent_7)); }
	inline GameObject_t1113636619 * get_productionContent_7() const { return ___productionContent_7; }
	inline GameObject_t1113636619 ** get_address_of_productionContent_7() { return &___productionContent_7; }
	inline void set_productionContent_7(GameObject_t1113636619 * value)
	{
		___productionContent_7 = value;
		Il2CppCodeGenWriteBarrier((&___productionContent_7), value);
	}

	inline static int32_t get_offset_of_citiesContent_8() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___citiesContent_8)); }
	inline GameObject_t1113636619 * get_citiesContent_8() const { return ___citiesContent_8; }
	inline GameObject_t1113636619 ** get_address_of_citiesContent_8() { return &___citiesContent_8; }
	inline void set_citiesContent_8(GameObject_t1113636619 * value)
	{
		___citiesContent_8 = value;
		Il2CppCodeGenWriteBarrier((&___citiesContent_8), value);
	}

	inline static int32_t get_offset_of_valleysContent_9() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___valleysContent_9)); }
	inline GameObject_t1113636619 * get_valleysContent_9() const { return ___valleysContent_9; }
	inline GameObject_t1113636619 ** get_address_of_valleysContent_9() { return &___valleysContent_9; }
	inline void set_valleysContent_9(GameObject_t1113636619 * value)
	{
		___valleysContent_9 = value;
		Il2CppCodeGenWriteBarrier((&___valleysContent_9), value);
	}

	inline static int32_t get_offset_of_coloniesContent_10() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___coloniesContent_10)); }
	inline GameObject_t1113636619 * get_coloniesContent_10() const { return ___coloniesContent_10; }
	inline GameObject_t1113636619 ** get_address_of_coloniesContent_10() { return &___coloniesContent_10; }
	inline void set_coloniesContent_10(GameObject_t1113636619 * value)
	{
		___coloniesContent_10 = value;
		Il2CppCodeGenWriteBarrier((&___coloniesContent_10), value);
	}

	inline static int32_t get_offset_of_iconIndex_11() { return static_cast<int32_t>(offsetof(ResidenceContentChanger_t4286680255, ___iconIndex_11)); }
	inline int64_t get_iconIndex_11() const { return ___iconIndex_11; }
	inline int64_t* get_address_of_iconIndex_11() { return &___iconIndex_11; }
	inline void set_iconIndex_11(int64_t value)
	{
		___iconIndex_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCECONTENTCHANGER_T4286680255_H
#ifndef MAYORRESIDENCEOVERVIEWCONTENTMANAGER_T259933799_H
#define MAYORRESIDENCEOVERVIEWCONTENTMANAGER_T259933799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MayorResidenceOverviewContentManager
struct  MayorResidenceOverviewContentManager_t259933799  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityName
	Text_t1901882714 * ___cityName_2;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityRegion
	Text_t1901882714 * ___cityRegion_3;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityCarrage
	Text_t1901882714 * ___cityCarrage_4;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityHappiness
	Text_t1901882714 * ___cityHappiness_5;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityPopulation
	Text_t1901882714 * ___cityPopulation_6;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityIdlePopulation
	Text_t1901882714 * ___cityIdlePopulation_7;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityTax
	Text_t1901882714 * ___cityTax_8;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::playerWins
	Text_t1901882714 * ___playerWins_9;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::barbarianWins
	Text_t1901882714 * ___barbarianWins_10;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::playerLoses
	Text_t1901882714 * ___playerLoses_11;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::barbarianLoses
	Text_t1901882714 * ___barbarianLoses_12;
	// UnityEngine.UI.Dropdown MayorResidenceOverviewContentManager::taxDropdown
	Dropdown_t2274391225 * ___taxDropdown_13;
	// System.Int64 MayorResidenceOverviewContentManager::taxIndex
	int64_t ___taxIndex_14;
	// UnityEngine.UI.InputField MayorResidenceOverviewContentManager::cityNameInput
	InputField_t3762917431 * ___cityNameInput_15;
	// UnityEngine.UI.Image MayorResidenceOverviewContentManager::cityIcon
	Image_t2670269651 * ___cityIcon_16;
	// System.Int64 MayorResidenceOverviewContentManager::iconIndex
	int64_t ___iconIndex_17;
	// System.String MayorResidenceOverviewContentManager::userImageName
	String_t* ___userImageName_18;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityName_2)); }
	inline Text_t1901882714 * get_cityName_2() const { return ___cityName_2; }
	inline Text_t1901882714 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(Text_t1901882714 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_2), value);
	}

	inline static int32_t get_offset_of_cityRegion_3() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityRegion_3)); }
	inline Text_t1901882714 * get_cityRegion_3() const { return ___cityRegion_3; }
	inline Text_t1901882714 ** get_address_of_cityRegion_3() { return &___cityRegion_3; }
	inline void set_cityRegion_3(Text_t1901882714 * value)
	{
		___cityRegion_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityRegion_3), value);
	}

	inline static int32_t get_offset_of_cityCarrage_4() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityCarrage_4)); }
	inline Text_t1901882714 * get_cityCarrage_4() const { return ___cityCarrage_4; }
	inline Text_t1901882714 ** get_address_of_cityCarrage_4() { return &___cityCarrage_4; }
	inline void set_cityCarrage_4(Text_t1901882714 * value)
	{
		___cityCarrage_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityCarrage_4), value);
	}

	inline static int32_t get_offset_of_cityHappiness_5() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityHappiness_5)); }
	inline Text_t1901882714 * get_cityHappiness_5() const { return ___cityHappiness_5; }
	inline Text_t1901882714 ** get_address_of_cityHappiness_5() { return &___cityHappiness_5; }
	inline void set_cityHappiness_5(Text_t1901882714 * value)
	{
		___cityHappiness_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityHappiness_5), value);
	}

	inline static int32_t get_offset_of_cityPopulation_6() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityPopulation_6)); }
	inline Text_t1901882714 * get_cityPopulation_6() const { return ___cityPopulation_6; }
	inline Text_t1901882714 ** get_address_of_cityPopulation_6() { return &___cityPopulation_6; }
	inline void set_cityPopulation_6(Text_t1901882714 * value)
	{
		___cityPopulation_6 = value;
		Il2CppCodeGenWriteBarrier((&___cityPopulation_6), value);
	}

	inline static int32_t get_offset_of_cityIdlePopulation_7() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityIdlePopulation_7)); }
	inline Text_t1901882714 * get_cityIdlePopulation_7() const { return ___cityIdlePopulation_7; }
	inline Text_t1901882714 ** get_address_of_cityIdlePopulation_7() { return &___cityIdlePopulation_7; }
	inline void set_cityIdlePopulation_7(Text_t1901882714 * value)
	{
		___cityIdlePopulation_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityIdlePopulation_7), value);
	}

	inline static int32_t get_offset_of_cityTax_8() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityTax_8)); }
	inline Text_t1901882714 * get_cityTax_8() const { return ___cityTax_8; }
	inline Text_t1901882714 ** get_address_of_cityTax_8() { return &___cityTax_8; }
	inline void set_cityTax_8(Text_t1901882714 * value)
	{
		___cityTax_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityTax_8), value);
	}

	inline static int32_t get_offset_of_playerWins_9() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___playerWins_9)); }
	inline Text_t1901882714 * get_playerWins_9() const { return ___playerWins_9; }
	inline Text_t1901882714 ** get_address_of_playerWins_9() { return &___playerWins_9; }
	inline void set_playerWins_9(Text_t1901882714 * value)
	{
		___playerWins_9 = value;
		Il2CppCodeGenWriteBarrier((&___playerWins_9), value);
	}

	inline static int32_t get_offset_of_barbarianWins_10() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___barbarianWins_10)); }
	inline Text_t1901882714 * get_barbarianWins_10() const { return ___barbarianWins_10; }
	inline Text_t1901882714 ** get_address_of_barbarianWins_10() { return &___barbarianWins_10; }
	inline void set_barbarianWins_10(Text_t1901882714 * value)
	{
		___barbarianWins_10 = value;
		Il2CppCodeGenWriteBarrier((&___barbarianWins_10), value);
	}

	inline static int32_t get_offset_of_playerLoses_11() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___playerLoses_11)); }
	inline Text_t1901882714 * get_playerLoses_11() const { return ___playerLoses_11; }
	inline Text_t1901882714 ** get_address_of_playerLoses_11() { return &___playerLoses_11; }
	inline void set_playerLoses_11(Text_t1901882714 * value)
	{
		___playerLoses_11 = value;
		Il2CppCodeGenWriteBarrier((&___playerLoses_11), value);
	}

	inline static int32_t get_offset_of_barbarianLoses_12() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___barbarianLoses_12)); }
	inline Text_t1901882714 * get_barbarianLoses_12() const { return ___barbarianLoses_12; }
	inline Text_t1901882714 ** get_address_of_barbarianLoses_12() { return &___barbarianLoses_12; }
	inline void set_barbarianLoses_12(Text_t1901882714 * value)
	{
		___barbarianLoses_12 = value;
		Il2CppCodeGenWriteBarrier((&___barbarianLoses_12), value);
	}

	inline static int32_t get_offset_of_taxDropdown_13() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___taxDropdown_13)); }
	inline Dropdown_t2274391225 * get_taxDropdown_13() const { return ___taxDropdown_13; }
	inline Dropdown_t2274391225 ** get_address_of_taxDropdown_13() { return &___taxDropdown_13; }
	inline void set_taxDropdown_13(Dropdown_t2274391225 * value)
	{
		___taxDropdown_13 = value;
		Il2CppCodeGenWriteBarrier((&___taxDropdown_13), value);
	}

	inline static int32_t get_offset_of_taxIndex_14() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___taxIndex_14)); }
	inline int64_t get_taxIndex_14() const { return ___taxIndex_14; }
	inline int64_t* get_address_of_taxIndex_14() { return &___taxIndex_14; }
	inline void set_taxIndex_14(int64_t value)
	{
		___taxIndex_14 = value;
	}

	inline static int32_t get_offset_of_cityNameInput_15() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityNameInput_15)); }
	inline InputField_t3762917431 * get_cityNameInput_15() const { return ___cityNameInput_15; }
	inline InputField_t3762917431 ** get_address_of_cityNameInput_15() { return &___cityNameInput_15; }
	inline void set_cityNameInput_15(InputField_t3762917431 * value)
	{
		___cityNameInput_15 = value;
		Il2CppCodeGenWriteBarrier((&___cityNameInput_15), value);
	}

	inline static int32_t get_offset_of_cityIcon_16() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___cityIcon_16)); }
	inline Image_t2670269651 * get_cityIcon_16() const { return ___cityIcon_16; }
	inline Image_t2670269651 ** get_address_of_cityIcon_16() { return &___cityIcon_16; }
	inline void set_cityIcon_16(Image_t2670269651 * value)
	{
		___cityIcon_16 = value;
		Il2CppCodeGenWriteBarrier((&___cityIcon_16), value);
	}

	inline static int32_t get_offset_of_iconIndex_17() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___iconIndex_17)); }
	inline int64_t get_iconIndex_17() const { return ___iconIndex_17; }
	inline int64_t* get_address_of_iconIndex_17() { return &___iconIndex_17; }
	inline void set_iconIndex_17(int64_t value)
	{
		___iconIndex_17 = value;
	}

	inline static int32_t get_offset_of_userImageName_18() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t259933799, ___userImageName_18)); }
	inline String_t* get_userImageName_18() const { return ___userImageName_18; }
	inline String_t** get_address_of_userImageName_18() { return &___userImageName_18; }
	inline void set_userImageName_18(String_t* value)
	{
		___userImageName_18 = value;
		Il2CppCodeGenWriteBarrier((&___userImageName_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAYORRESIDENCEOVERVIEWCONTENTMANAGER_T259933799_H
#ifndef MESSAGEALLIANCEMANAGER_T646576267_H
#define MESSAGEALLIANCEMANAGER_T646576267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageAllianceManager
struct  MessageAllianceManager_t646576267  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField MessageAllianceManager::subject
	InputField_t3762917431 * ___subject_2;
	// UnityEngine.UI.InputField MessageAllianceManager::message
	InputField_t3762917431 * ___message_3;
	// System.Int64 MessageAllianceManager::userId
	int64_t ___userId_4;

public:
	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(MessageAllianceManager_t646576267, ___subject_2)); }
	inline InputField_t3762917431 * get_subject_2() const { return ___subject_2; }
	inline InputField_t3762917431 ** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(InputField_t3762917431 * value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(MessageAllianceManager_t646576267, ___message_3)); }
	inline InputField_t3762917431 * get_message_3() const { return ___message_3; }
	inline InputField_t3762917431 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(InputField_t3762917431 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_userId_4() { return static_cast<int32_t>(offsetof(MessageAllianceManager_t646576267, ___userId_4)); }
	inline int64_t get_userId_4() const { return ___userId_4; }
	inline int64_t* get_address_of_userId_4() { return &___userId_4; }
	inline void set_userId_4(int64_t value)
	{
		___userId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEALLIANCEMANAGER_T646576267_H
#ifndef MARKETMANAGER_T880414981_H
#define MARKETMANAGER_T880414981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager
struct  MarketManager_t880414981  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MarketManager::goldValue
	Text_t1901882714 * ___goldValue_2;
	// UnityEngine.UI.Text MarketManager::silverValue
	Text_t1901882714 * ___silverValue_3;
	// UnityEngine.UI.Text MarketManager::resourceLabel
	Text_t1901882714 * ___resourceLabel_4;
	// UnityEngine.UI.InputField MarketManager::buyCount
	InputField_t3762917431 * ___buyCount_5;
	// UnityEngine.UI.InputField MarketManager::sellCount
	InputField_t3762917431 * ___sellCount_6;
	// UnityEngine.UI.InputField MarketManager::sellUnitPrice
	InputField_t3762917431 * ___sellUnitPrice_7;
	// UnityEngine.UI.Text MarketManager::buyUnitPrice
	Text_t1901882714 * ___buyUnitPrice_8;
	// UnityEngine.UI.Text MarketManager::buyComission
	Text_t1901882714 * ___buyComission_9;
	// UnityEngine.UI.Text MarketManager::buyTotal
	Text_t1901882714 * ___buyTotal_10;
	// UnityEngine.UI.Text MarketManager::sellComission
	Text_t1901882714 * ___sellComission_11;
	// UnityEngine.UI.Text MarketManager::sellTotal
	Text_t1901882714 * ___sellTotal_12;
	// MarketBuyTableController MarketManager::buyTable
	MarketBuyTableController_t892139868 * ___buyTable_13;
	// MarketSellTableController MarketManager::sellTable
	MarketSellTableController_t878431470 * ___sellTable_14;
	// SimpleEvent MarketManager::onBuyLotsUpdated
	SimpleEvent_t129249603 * ___onBuyLotsUpdated_15;
	// SimpleEvent MarketManager::onSellLotsUpdated
	SimpleEvent_t129249603 * ___onSellLotsUpdated_16;
	// SimpleEvent MarketManager::onResourceBought
	SimpleEvent_t129249603 * ___onResourceBought_17;
	// SimpleEvent MarketManager::onResourceSold
	SimpleEvent_t129249603 * ___onResourceSold_18;
	// SimpleEvent MarketManager::onLotRetracted
	SimpleEvent_t129249603 * ___onLotRetracted_19;
	// System.Collections.ArrayList MarketManager::lotsToBuy
	ArrayList_t2718874744 * ___lotsToBuy_20;
	// System.Collections.ArrayList MarketManager::lotsToSell
	ArrayList_t2718874744 * ___lotsToSell_21;
	// System.String MarketManager::currentResource
	String_t* ___currentResource_22;

public:
	inline static int32_t get_offset_of_goldValue_2() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___goldValue_2)); }
	inline Text_t1901882714 * get_goldValue_2() const { return ___goldValue_2; }
	inline Text_t1901882714 ** get_address_of_goldValue_2() { return &___goldValue_2; }
	inline void set_goldValue_2(Text_t1901882714 * value)
	{
		___goldValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___goldValue_2), value);
	}

	inline static int32_t get_offset_of_silverValue_3() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___silverValue_3)); }
	inline Text_t1901882714 * get_silverValue_3() const { return ___silverValue_3; }
	inline Text_t1901882714 ** get_address_of_silverValue_3() { return &___silverValue_3; }
	inline void set_silverValue_3(Text_t1901882714 * value)
	{
		___silverValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___silverValue_3), value);
	}

	inline static int32_t get_offset_of_resourceLabel_4() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___resourceLabel_4)); }
	inline Text_t1901882714 * get_resourceLabel_4() const { return ___resourceLabel_4; }
	inline Text_t1901882714 ** get_address_of_resourceLabel_4() { return &___resourceLabel_4; }
	inline void set_resourceLabel_4(Text_t1901882714 * value)
	{
		___resourceLabel_4 = value;
		Il2CppCodeGenWriteBarrier((&___resourceLabel_4), value);
	}

	inline static int32_t get_offset_of_buyCount_5() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___buyCount_5)); }
	inline InputField_t3762917431 * get_buyCount_5() const { return ___buyCount_5; }
	inline InputField_t3762917431 ** get_address_of_buyCount_5() { return &___buyCount_5; }
	inline void set_buyCount_5(InputField_t3762917431 * value)
	{
		___buyCount_5 = value;
		Il2CppCodeGenWriteBarrier((&___buyCount_5), value);
	}

	inline static int32_t get_offset_of_sellCount_6() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___sellCount_6)); }
	inline InputField_t3762917431 * get_sellCount_6() const { return ___sellCount_6; }
	inline InputField_t3762917431 ** get_address_of_sellCount_6() { return &___sellCount_6; }
	inline void set_sellCount_6(InputField_t3762917431 * value)
	{
		___sellCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___sellCount_6), value);
	}

	inline static int32_t get_offset_of_sellUnitPrice_7() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___sellUnitPrice_7)); }
	inline InputField_t3762917431 * get_sellUnitPrice_7() const { return ___sellUnitPrice_7; }
	inline InputField_t3762917431 ** get_address_of_sellUnitPrice_7() { return &___sellUnitPrice_7; }
	inline void set_sellUnitPrice_7(InputField_t3762917431 * value)
	{
		___sellUnitPrice_7 = value;
		Il2CppCodeGenWriteBarrier((&___sellUnitPrice_7), value);
	}

	inline static int32_t get_offset_of_buyUnitPrice_8() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___buyUnitPrice_8)); }
	inline Text_t1901882714 * get_buyUnitPrice_8() const { return ___buyUnitPrice_8; }
	inline Text_t1901882714 ** get_address_of_buyUnitPrice_8() { return &___buyUnitPrice_8; }
	inline void set_buyUnitPrice_8(Text_t1901882714 * value)
	{
		___buyUnitPrice_8 = value;
		Il2CppCodeGenWriteBarrier((&___buyUnitPrice_8), value);
	}

	inline static int32_t get_offset_of_buyComission_9() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___buyComission_9)); }
	inline Text_t1901882714 * get_buyComission_9() const { return ___buyComission_9; }
	inline Text_t1901882714 ** get_address_of_buyComission_9() { return &___buyComission_9; }
	inline void set_buyComission_9(Text_t1901882714 * value)
	{
		___buyComission_9 = value;
		Il2CppCodeGenWriteBarrier((&___buyComission_9), value);
	}

	inline static int32_t get_offset_of_buyTotal_10() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___buyTotal_10)); }
	inline Text_t1901882714 * get_buyTotal_10() const { return ___buyTotal_10; }
	inline Text_t1901882714 ** get_address_of_buyTotal_10() { return &___buyTotal_10; }
	inline void set_buyTotal_10(Text_t1901882714 * value)
	{
		___buyTotal_10 = value;
		Il2CppCodeGenWriteBarrier((&___buyTotal_10), value);
	}

	inline static int32_t get_offset_of_sellComission_11() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___sellComission_11)); }
	inline Text_t1901882714 * get_sellComission_11() const { return ___sellComission_11; }
	inline Text_t1901882714 ** get_address_of_sellComission_11() { return &___sellComission_11; }
	inline void set_sellComission_11(Text_t1901882714 * value)
	{
		___sellComission_11 = value;
		Il2CppCodeGenWriteBarrier((&___sellComission_11), value);
	}

	inline static int32_t get_offset_of_sellTotal_12() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___sellTotal_12)); }
	inline Text_t1901882714 * get_sellTotal_12() const { return ___sellTotal_12; }
	inline Text_t1901882714 ** get_address_of_sellTotal_12() { return &___sellTotal_12; }
	inline void set_sellTotal_12(Text_t1901882714 * value)
	{
		___sellTotal_12 = value;
		Il2CppCodeGenWriteBarrier((&___sellTotal_12), value);
	}

	inline static int32_t get_offset_of_buyTable_13() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___buyTable_13)); }
	inline MarketBuyTableController_t892139868 * get_buyTable_13() const { return ___buyTable_13; }
	inline MarketBuyTableController_t892139868 ** get_address_of_buyTable_13() { return &___buyTable_13; }
	inline void set_buyTable_13(MarketBuyTableController_t892139868 * value)
	{
		___buyTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___buyTable_13), value);
	}

	inline static int32_t get_offset_of_sellTable_14() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___sellTable_14)); }
	inline MarketSellTableController_t878431470 * get_sellTable_14() const { return ___sellTable_14; }
	inline MarketSellTableController_t878431470 ** get_address_of_sellTable_14() { return &___sellTable_14; }
	inline void set_sellTable_14(MarketSellTableController_t878431470 * value)
	{
		___sellTable_14 = value;
		Il2CppCodeGenWriteBarrier((&___sellTable_14), value);
	}

	inline static int32_t get_offset_of_onBuyLotsUpdated_15() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___onBuyLotsUpdated_15)); }
	inline SimpleEvent_t129249603 * get_onBuyLotsUpdated_15() const { return ___onBuyLotsUpdated_15; }
	inline SimpleEvent_t129249603 ** get_address_of_onBuyLotsUpdated_15() { return &___onBuyLotsUpdated_15; }
	inline void set_onBuyLotsUpdated_15(SimpleEvent_t129249603 * value)
	{
		___onBuyLotsUpdated_15 = value;
		Il2CppCodeGenWriteBarrier((&___onBuyLotsUpdated_15), value);
	}

	inline static int32_t get_offset_of_onSellLotsUpdated_16() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___onSellLotsUpdated_16)); }
	inline SimpleEvent_t129249603 * get_onSellLotsUpdated_16() const { return ___onSellLotsUpdated_16; }
	inline SimpleEvent_t129249603 ** get_address_of_onSellLotsUpdated_16() { return &___onSellLotsUpdated_16; }
	inline void set_onSellLotsUpdated_16(SimpleEvent_t129249603 * value)
	{
		___onSellLotsUpdated_16 = value;
		Il2CppCodeGenWriteBarrier((&___onSellLotsUpdated_16), value);
	}

	inline static int32_t get_offset_of_onResourceBought_17() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___onResourceBought_17)); }
	inline SimpleEvent_t129249603 * get_onResourceBought_17() const { return ___onResourceBought_17; }
	inline SimpleEvent_t129249603 ** get_address_of_onResourceBought_17() { return &___onResourceBought_17; }
	inline void set_onResourceBought_17(SimpleEvent_t129249603 * value)
	{
		___onResourceBought_17 = value;
		Il2CppCodeGenWriteBarrier((&___onResourceBought_17), value);
	}

	inline static int32_t get_offset_of_onResourceSold_18() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___onResourceSold_18)); }
	inline SimpleEvent_t129249603 * get_onResourceSold_18() const { return ___onResourceSold_18; }
	inline SimpleEvent_t129249603 ** get_address_of_onResourceSold_18() { return &___onResourceSold_18; }
	inline void set_onResourceSold_18(SimpleEvent_t129249603 * value)
	{
		___onResourceSold_18 = value;
		Il2CppCodeGenWriteBarrier((&___onResourceSold_18), value);
	}

	inline static int32_t get_offset_of_onLotRetracted_19() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___onLotRetracted_19)); }
	inline SimpleEvent_t129249603 * get_onLotRetracted_19() const { return ___onLotRetracted_19; }
	inline SimpleEvent_t129249603 ** get_address_of_onLotRetracted_19() { return &___onLotRetracted_19; }
	inline void set_onLotRetracted_19(SimpleEvent_t129249603 * value)
	{
		___onLotRetracted_19 = value;
		Il2CppCodeGenWriteBarrier((&___onLotRetracted_19), value);
	}

	inline static int32_t get_offset_of_lotsToBuy_20() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___lotsToBuy_20)); }
	inline ArrayList_t2718874744 * get_lotsToBuy_20() const { return ___lotsToBuy_20; }
	inline ArrayList_t2718874744 ** get_address_of_lotsToBuy_20() { return &___lotsToBuy_20; }
	inline void set_lotsToBuy_20(ArrayList_t2718874744 * value)
	{
		___lotsToBuy_20 = value;
		Il2CppCodeGenWriteBarrier((&___lotsToBuy_20), value);
	}

	inline static int32_t get_offset_of_lotsToSell_21() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___lotsToSell_21)); }
	inline ArrayList_t2718874744 * get_lotsToSell_21() const { return ___lotsToSell_21; }
	inline ArrayList_t2718874744 ** get_address_of_lotsToSell_21() { return &___lotsToSell_21; }
	inline void set_lotsToSell_21(ArrayList_t2718874744 * value)
	{
		___lotsToSell_21 = value;
		Il2CppCodeGenWriteBarrier((&___lotsToSell_21), value);
	}

	inline static int32_t get_offset_of_currentResource_22() { return static_cast<int32_t>(offsetof(MarketManager_t880414981, ___currentResource_22)); }
	inline String_t* get_currentResource_22() const { return ___currentResource_22; }
	inline String_t** get_address_of_currentResource_22() { return &___currentResource_22; }
	inline void set_currentResource_22(String_t* value)
	{
		___currentResource_22 = value;
		Il2CppCodeGenWriteBarrier((&___currentResource_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETMANAGER_T880414981_H
#ifndef LOGINREGISTRATIONCONTENTMANAGER_T2754536803_H
#define LOGINREGISTRATIONCONTENTMANAGER_T2754536803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginRegistrationContentManager
struct  LoginRegistrationContentManager_t2754536803  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LoginRegistrationContentManager::loginContent
	GameObject_t1113636619 * ___loginContent_2;
	// UnityEngine.GameObject LoginRegistrationContentManager::registrationContent
	GameObject_t1113636619 * ___registrationContent_3;
	// UnityEngine.GameObject LoginRegistrationContentManager::forgotPasswordContent
	GameObject_t1113636619 * ___forgotPasswordContent_4;
	// UnityEngine.GameObject LoginRegistrationContentManager::resetPasswordContent
	GameObject_t1113636619 * ___resetPasswordContent_5;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::loginUsername
	InputField_t3762917431 * ___loginUsername_6;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::loginPassword
	InputField_t3762917431 * ___loginPassword_7;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerUsername
	InputField_t3762917431 * ___registerUsername_8;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerPassword
	InputField_t3762917431 * ___registerPassword_9;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerConfirmPassword
	InputField_t3762917431 * ___registerConfirmPassword_10;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerEmail
	InputField_t3762917431 * ___registerEmail_11;
	// UnityEngine.UI.Dropdown LoginRegistrationContentManager::registerEmperors
	Dropdown_t2274391225 * ___registerEmperors_12;
	// UnityEngine.UI.Dropdown LoginRegistrationContentManager::registerLanguage
	Dropdown_t2274391225 * ___registerLanguage_13;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::forgotPasswordEmail
	InputField_t3762917431 * ___forgotPasswordEmail_14;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::resetSecretKey
	InputField_t3762917431 * ___resetSecretKey_15;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::resetNewPassword
	InputField_t3762917431 * ___resetNewPassword_16;
	// UnityEngine.GameObject LoginRegistrationContentManager::directLoginButton
	GameObject_t1113636619 * ___directLoginButton_17;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> LoginRegistrationContentManager::emperorsList
	List_1_t447389798 * ___emperorsList_18;

public:
	inline static int32_t get_offset_of_loginContent_2() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___loginContent_2)); }
	inline GameObject_t1113636619 * get_loginContent_2() const { return ___loginContent_2; }
	inline GameObject_t1113636619 ** get_address_of_loginContent_2() { return &___loginContent_2; }
	inline void set_loginContent_2(GameObject_t1113636619 * value)
	{
		___loginContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginContent_2), value);
	}

	inline static int32_t get_offset_of_registrationContent_3() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registrationContent_3)); }
	inline GameObject_t1113636619 * get_registrationContent_3() const { return ___registrationContent_3; }
	inline GameObject_t1113636619 ** get_address_of_registrationContent_3() { return &___registrationContent_3; }
	inline void set_registrationContent_3(GameObject_t1113636619 * value)
	{
		___registrationContent_3 = value;
		Il2CppCodeGenWriteBarrier((&___registrationContent_3), value);
	}

	inline static int32_t get_offset_of_forgotPasswordContent_4() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___forgotPasswordContent_4)); }
	inline GameObject_t1113636619 * get_forgotPasswordContent_4() const { return ___forgotPasswordContent_4; }
	inline GameObject_t1113636619 ** get_address_of_forgotPasswordContent_4() { return &___forgotPasswordContent_4; }
	inline void set_forgotPasswordContent_4(GameObject_t1113636619 * value)
	{
		___forgotPasswordContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___forgotPasswordContent_4), value);
	}

	inline static int32_t get_offset_of_resetPasswordContent_5() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___resetPasswordContent_5)); }
	inline GameObject_t1113636619 * get_resetPasswordContent_5() const { return ___resetPasswordContent_5; }
	inline GameObject_t1113636619 ** get_address_of_resetPasswordContent_5() { return &___resetPasswordContent_5; }
	inline void set_resetPasswordContent_5(GameObject_t1113636619 * value)
	{
		___resetPasswordContent_5 = value;
		Il2CppCodeGenWriteBarrier((&___resetPasswordContent_5), value);
	}

	inline static int32_t get_offset_of_loginUsername_6() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___loginUsername_6)); }
	inline InputField_t3762917431 * get_loginUsername_6() const { return ___loginUsername_6; }
	inline InputField_t3762917431 ** get_address_of_loginUsername_6() { return &___loginUsername_6; }
	inline void set_loginUsername_6(InputField_t3762917431 * value)
	{
		___loginUsername_6 = value;
		Il2CppCodeGenWriteBarrier((&___loginUsername_6), value);
	}

	inline static int32_t get_offset_of_loginPassword_7() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___loginPassword_7)); }
	inline InputField_t3762917431 * get_loginPassword_7() const { return ___loginPassword_7; }
	inline InputField_t3762917431 ** get_address_of_loginPassword_7() { return &___loginPassword_7; }
	inline void set_loginPassword_7(InputField_t3762917431 * value)
	{
		___loginPassword_7 = value;
		Il2CppCodeGenWriteBarrier((&___loginPassword_7), value);
	}

	inline static int32_t get_offset_of_registerUsername_8() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerUsername_8)); }
	inline InputField_t3762917431 * get_registerUsername_8() const { return ___registerUsername_8; }
	inline InputField_t3762917431 ** get_address_of_registerUsername_8() { return &___registerUsername_8; }
	inline void set_registerUsername_8(InputField_t3762917431 * value)
	{
		___registerUsername_8 = value;
		Il2CppCodeGenWriteBarrier((&___registerUsername_8), value);
	}

	inline static int32_t get_offset_of_registerPassword_9() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerPassword_9)); }
	inline InputField_t3762917431 * get_registerPassword_9() const { return ___registerPassword_9; }
	inline InputField_t3762917431 ** get_address_of_registerPassword_9() { return &___registerPassword_9; }
	inline void set_registerPassword_9(InputField_t3762917431 * value)
	{
		___registerPassword_9 = value;
		Il2CppCodeGenWriteBarrier((&___registerPassword_9), value);
	}

	inline static int32_t get_offset_of_registerConfirmPassword_10() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerConfirmPassword_10)); }
	inline InputField_t3762917431 * get_registerConfirmPassword_10() const { return ___registerConfirmPassword_10; }
	inline InputField_t3762917431 ** get_address_of_registerConfirmPassword_10() { return &___registerConfirmPassword_10; }
	inline void set_registerConfirmPassword_10(InputField_t3762917431 * value)
	{
		___registerConfirmPassword_10 = value;
		Il2CppCodeGenWriteBarrier((&___registerConfirmPassword_10), value);
	}

	inline static int32_t get_offset_of_registerEmail_11() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerEmail_11)); }
	inline InputField_t3762917431 * get_registerEmail_11() const { return ___registerEmail_11; }
	inline InputField_t3762917431 ** get_address_of_registerEmail_11() { return &___registerEmail_11; }
	inline void set_registerEmail_11(InputField_t3762917431 * value)
	{
		___registerEmail_11 = value;
		Il2CppCodeGenWriteBarrier((&___registerEmail_11), value);
	}

	inline static int32_t get_offset_of_registerEmperors_12() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerEmperors_12)); }
	inline Dropdown_t2274391225 * get_registerEmperors_12() const { return ___registerEmperors_12; }
	inline Dropdown_t2274391225 ** get_address_of_registerEmperors_12() { return &___registerEmperors_12; }
	inline void set_registerEmperors_12(Dropdown_t2274391225 * value)
	{
		___registerEmperors_12 = value;
		Il2CppCodeGenWriteBarrier((&___registerEmperors_12), value);
	}

	inline static int32_t get_offset_of_registerLanguage_13() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___registerLanguage_13)); }
	inline Dropdown_t2274391225 * get_registerLanguage_13() const { return ___registerLanguage_13; }
	inline Dropdown_t2274391225 ** get_address_of_registerLanguage_13() { return &___registerLanguage_13; }
	inline void set_registerLanguage_13(Dropdown_t2274391225 * value)
	{
		___registerLanguage_13 = value;
		Il2CppCodeGenWriteBarrier((&___registerLanguage_13), value);
	}

	inline static int32_t get_offset_of_forgotPasswordEmail_14() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___forgotPasswordEmail_14)); }
	inline InputField_t3762917431 * get_forgotPasswordEmail_14() const { return ___forgotPasswordEmail_14; }
	inline InputField_t3762917431 ** get_address_of_forgotPasswordEmail_14() { return &___forgotPasswordEmail_14; }
	inline void set_forgotPasswordEmail_14(InputField_t3762917431 * value)
	{
		___forgotPasswordEmail_14 = value;
		Il2CppCodeGenWriteBarrier((&___forgotPasswordEmail_14), value);
	}

	inline static int32_t get_offset_of_resetSecretKey_15() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___resetSecretKey_15)); }
	inline InputField_t3762917431 * get_resetSecretKey_15() const { return ___resetSecretKey_15; }
	inline InputField_t3762917431 ** get_address_of_resetSecretKey_15() { return &___resetSecretKey_15; }
	inline void set_resetSecretKey_15(InputField_t3762917431 * value)
	{
		___resetSecretKey_15 = value;
		Il2CppCodeGenWriteBarrier((&___resetSecretKey_15), value);
	}

	inline static int32_t get_offset_of_resetNewPassword_16() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___resetNewPassword_16)); }
	inline InputField_t3762917431 * get_resetNewPassword_16() const { return ___resetNewPassword_16; }
	inline InputField_t3762917431 ** get_address_of_resetNewPassword_16() { return &___resetNewPassword_16; }
	inline void set_resetNewPassword_16(InputField_t3762917431 * value)
	{
		___resetNewPassword_16 = value;
		Il2CppCodeGenWriteBarrier((&___resetNewPassword_16), value);
	}

	inline static int32_t get_offset_of_directLoginButton_17() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___directLoginButton_17)); }
	inline GameObject_t1113636619 * get_directLoginButton_17() const { return ___directLoginButton_17; }
	inline GameObject_t1113636619 ** get_address_of_directLoginButton_17() { return &___directLoginButton_17; }
	inline void set_directLoginButton_17(GameObject_t1113636619 * value)
	{
		___directLoginButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___directLoginButton_17), value);
	}

	inline static int32_t get_offset_of_emperorsList_18() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t2754536803, ___emperorsList_18)); }
	inline List_1_t447389798 * get_emperorsList_18() const { return ___emperorsList_18; }
	inline List_1_t447389798 ** get_address_of_emperorsList_18() { return &___emperorsList_18; }
	inline void set_emperorsList_18(List_1_t447389798 * value)
	{
		___emperorsList_18 = value;
		Il2CppCodeGenWriteBarrier((&___emperorsList_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINREGISTRATIONCONTENTMANAGER_T2754536803_H
#ifndef MAILCONTENTCHANGE_T3691412170_H
#define MAILCONTENTCHANGE_T3691412170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailContentChange
struct  MailContentChange_t3691412170  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MailContentChange::inboxPanel
	GameObject_t1113636619 * ___inboxPanel_2;
	// UnityEngine.GameObject MailContentChange::reportsPanel
	GameObject_t1113636619 * ___reportsPanel_3;
	// UnityEngine.GameObject MailContentChange::systemPanel
	GameObject_t1113636619 * ___systemPanel_4;
	// UnityEngine.GameObject MailContentChange::invitePanel
	GameObject_t1113636619 * ___invitePanel_5;
	// UnityEngine.UI.Text MailContentChange::panelName
	Text_t1901882714 * ___panelName_6;
	// UnityEngine.UI.Image[] MailContentChange::tabs
	ImageU5BU5D_t2439009922* ___tabs_7;
	// UnityEngine.Sprite MailContentChange::tabSelectedSprite
	Sprite_t280657092 * ___tabSelectedSprite_8;
	// UnityEngine.Sprite MailContentChange::tabUnselectedSprite
	Sprite_t280657092 * ___tabUnselectedSprite_9;

public:
	inline static int32_t get_offset_of_inboxPanel_2() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___inboxPanel_2)); }
	inline GameObject_t1113636619 * get_inboxPanel_2() const { return ___inboxPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_inboxPanel_2() { return &___inboxPanel_2; }
	inline void set_inboxPanel_2(GameObject_t1113636619 * value)
	{
		___inboxPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___inboxPanel_2), value);
	}

	inline static int32_t get_offset_of_reportsPanel_3() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___reportsPanel_3)); }
	inline GameObject_t1113636619 * get_reportsPanel_3() const { return ___reportsPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_reportsPanel_3() { return &___reportsPanel_3; }
	inline void set_reportsPanel_3(GameObject_t1113636619 * value)
	{
		___reportsPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___reportsPanel_3), value);
	}

	inline static int32_t get_offset_of_systemPanel_4() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___systemPanel_4)); }
	inline GameObject_t1113636619 * get_systemPanel_4() const { return ___systemPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_systemPanel_4() { return &___systemPanel_4; }
	inline void set_systemPanel_4(GameObject_t1113636619 * value)
	{
		___systemPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___systemPanel_4), value);
	}

	inline static int32_t get_offset_of_invitePanel_5() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___invitePanel_5)); }
	inline GameObject_t1113636619 * get_invitePanel_5() const { return ___invitePanel_5; }
	inline GameObject_t1113636619 ** get_address_of_invitePanel_5() { return &___invitePanel_5; }
	inline void set_invitePanel_5(GameObject_t1113636619 * value)
	{
		___invitePanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___invitePanel_5), value);
	}

	inline static int32_t get_offset_of_panelName_6() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___panelName_6)); }
	inline Text_t1901882714 * get_panelName_6() const { return ___panelName_6; }
	inline Text_t1901882714 ** get_address_of_panelName_6() { return &___panelName_6; }
	inline void set_panelName_6(Text_t1901882714 * value)
	{
		___panelName_6 = value;
		Il2CppCodeGenWriteBarrier((&___panelName_6), value);
	}

	inline static int32_t get_offset_of_tabs_7() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___tabs_7)); }
	inline ImageU5BU5D_t2439009922* get_tabs_7() const { return ___tabs_7; }
	inline ImageU5BU5D_t2439009922** get_address_of_tabs_7() { return &___tabs_7; }
	inline void set_tabs_7(ImageU5BU5D_t2439009922* value)
	{
		___tabs_7 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_7), value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_8() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___tabSelectedSprite_8)); }
	inline Sprite_t280657092 * get_tabSelectedSprite_8() const { return ___tabSelectedSprite_8; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedSprite_8() { return &___tabSelectedSprite_8; }
	inline void set_tabSelectedSprite_8(Sprite_t280657092 * value)
	{
		___tabSelectedSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedSprite_8), value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_9() { return static_cast<int32_t>(offsetof(MailContentChange_t3691412170, ___tabUnselectedSprite_9)); }
	inline Sprite_t280657092 * get_tabUnselectedSprite_9() const { return ___tabUnselectedSprite_9; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedSprite_9() { return &___tabUnselectedSprite_9; }
	inline void set_tabUnselectedSprite_9(Sprite_t280657092 * value)
	{
		___tabUnselectedSprite_9 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedSprite_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILCONTENTCHANGE_T3691412170_H
#ifndef MESSAGECONTENTMANAGER_T985817454_H
#define MESSAGECONTENTMANAGER_T985817454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageContentManager
struct  MessageContentManager_t985817454  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MessageContentManager::readContent
	GameObject_t1113636619 * ___readContent_2;
	// UnityEngine.GameObject MessageContentManager::replyContent
	GameObject_t1113636619 * ___replyContent_3;
	// UnityEngine.UI.Text MessageContentManager::username
	Text_t1901882714 * ___username_4;
	// UnityEngine.UI.Image MessageContentManager::userIcon
	Image_t2670269651 * ___userIcon_5;
	// UnityEngine.UI.Text MessageContentManager::userRank
	Text_t1901882714 * ___userRank_6;
	// UnityEngine.UI.Text MessageContentManager::userAlliance
	Text_t1901882714 * ___userAlliance_7;
	// UnityEngine.UI.Text MessageContentManager::userPosition
	Text_t1901882714 * ___userPosition_8;
	// UnityEngine.UI.Text MessageContentManager::userExperiance
	Text_t1901882714 * ___userExperiance_9;
	// UnityEngine.UI.Text MessageContentManager::messageSubject
	Text_t1901882714 * ___messageSubject_10;
	// UnityEngine.UI.Text MessageContentManager::messageText
	Text_t1901882714 * ___messageText_11;
	// UnityEngine.UI.Text MessageContentManager::messageDate
	Text_t1901882714 * ___messageDate_12;
	// UnityEngine.UI.Text MessageContentManager::initialSubject
	Text_t1901882714 * ___initialSubject_13;
	// UnityEngine.UI.InputField MessageContentManager::initialMessage
	InputField_t3762917431 * ___initialMessage_14;
	// UnityEngine.UI.InputField MessageContentManager::replySubject
	InputField_t3762917431 * ___replySubject_15;
	// UnityEngine.UI.InputField MessageContentManager::replyMessage
	InputField_t3762917431 * ___replyMessage_16;
	// System.Int64 MessageContentManager::messageId
	int64_t ___messageId_17;
	// System.Boolean MessageContentManager::reply
	bool ___reply_18;
	// UserMessageReportModel MessageContentManager::message
	UserMessageReportModel_t4065405063 * ___message_19;
	// MailInboxTableController MessageContentManager::ownerTable
	MailInboxTableController_t2667561573 * ___ownerTable_20;
	// System.String MessageContentManager::dateFormat
	String_t* ___dateFormat_21;
	// SimpleEvent MessageContentManager::gotMessageDetails
	SimpleEvent_t129249603 * ___gotMessageDetails_22;

public:
	inline static int32_t get_offset_of_readContent_2() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___readContent_2)); }
	inline GameObject_t1113636619 * get_readContent_2() const { return ___readContent_2; }
	inline GameObject_t1113636619 ** get_address_of_readContent_2() { return &___readContent_2; }
	inline void set_readContent_2(GameObject_t1113636619 * value)
	{
		___readContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___readContent_2), value);
	}

	inline static int32_t get_offset_of_replyContent_3() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___replyContent_3)); }
	inline GameObject_t1113636619 * get_replyContent_3() const { return ___replyContent_3; }
	inline GameObject_t1113636619 ** get_address_of_replyContent_3() { return &___replyContent_3; }
	inline void set_replyContent_3(GameObject_t1113636619 * value)
	{
		___replyContent_3 = value;
		Il2CppCodeGenWriteBarrier((&___replyContent_3), value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___username_4)); }
	inline Text_t1901882714 * get_username_4() const { return ___username_4; }
	inline Text_t1901882714 ** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(Text_t1901882714 * value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier((&___username_4), value);
	}

	inline static int32_t get_offset_of_userIcon_5() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___userIcon_5)); }
	inline Image_t2670269651 * get_userIcon_5() const { return ___userIcon_5; }
	inline Image_t2670269651 ** get_address_of_userIcon_5() { return &___userIcon_5; }
	inline void set_userIcon_5(Image_t2670269651 * value)
	{
		___userIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___userIcon_5), value);
	}

	inline static int32_t get_offset_of_userRank_6() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___userRank_6)); }
	inline Text_t1901882714 * get_userRank_6() const { return ___userRank_6; }
	inline Text_t1901882714 ** get_address_of_userRank_6() { return &___userRank_6; }
	inline void set_userRank_6(Text_t1901882714 * value)
	{
		___userRank_6 = value;
		Il2CppCodeGenWriteBarrier((&___userRank_6), value);
	}

	inline static int32_t get_offset_of_userAlliance_7() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___userAlliance_7)); }
	inline Text_t1901882714 * get_userAlliance_7() const { return ___userAlliance_7; }
	inline Text_t1901882714 ** get_address_of_userAlliance_7() { return &___userAlliance_7; }
	inline void set_userAlliance_7(Text_t1901882714 * value)
	{
		___userAlliance_7 = value;
		Il2CppCodeGenWriteBarrier((&___userAlliance_7), value);
	}

	inline static int32_t get_offset_of_userPosition_8() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___userPosition_8)); }
	inline Text_t1901882714 * get_userPosition_8() const { return ___userPosition_8; }
	inline Text_t1901882714 ** get_address_of_userPosition_8() { return &___userPosition_8; }
	inline void set_userPosition_8(Text_t1901882714 * value)
	{
		___userPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___userPosition_8), value);
	}

	inline static int32_t get_offset_of_userExperiance_9() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___userExperiance_9)); }
	inline Text_t1901882714 * get_userExperiance_9() const { return ___userExperiance_9; }
	inline Text_t1901882714 ** get_address_of_userExperiance_9() { return &___userExperiance_9; }
	inline void set_userExperiance_9(Text_t1901882714 * value)
	{
		___userExperiance_9 = value;
		Il2CppCodeGenWriteBarrier((&___userExperiance_9), value);
	}

	inline static int32_t get_offset_of_messageSubject_10() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___messageSubject_10)); }
	inline Text_t1901882714 * get_messageSubject_10() const { return ___messageSubject_10; }
	inline Text_t1901882714 ** get_address_of_messageSubject_10() { return &___messageSubject_10; }
	inline void set_messageSubject_10(Text_t1901882714 * value)
	{
		___messageSubject_10 = value;
		Il2CppCodeGenWriteBarrier((&___messageSubject_10), value);
	}

	inline static int32_t get_offset_of_messageText_11() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___messageText_11)); }
	inline Text_t1901882714 * get_messageText_11() const { return ___messageText_11; }
	inline Text_t1901882714 ** get_address_of_messageText_11() { return &___messageText_11; }
	inline void set_messageText_11(Text_t1901882714 * value)
	{
		___messageText_11 = value;
		Il2CppCodeGenWriteBarrier((&___messageText_11), value);
	}

	inline static int32_t get_offset_of_messageDate_12() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___messageDate_12)); }
	inline Text_t1901882714 * get_messageDate_12() const { return ___messageDate_12; }
	inline Text_t1901882714 ** get_address_of_messageDate_12() { return &___messageDate_12; }
	inline void set_messageDate_12(Text_t1901882714 * value)
	{
		___messageDate_12 = value;
		Il2CppCodeGenWriteBarrier((&___messageDate_12), value);
	}

	inline static int32_t get_offset_of_initialSubject_13() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___initialSubject_13)); }
	inline Text_t1901882714 * get_initialSubject_13() const { return ___initialSubject_13; }
	inline Text_t1901882714 ** get_address_of_initialSubject_13() { return &___initialSubject_13; }
	inline void set_initialSubject_13(Text_t1901882714 * value)
	{
		___initialSubject_13 = value;
		Il2CppCodeGenWriteBarrier((&___initialSubject_13), value);
	}

	inline static int32_t get_offset_of_initialMessage_14() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___initialMessage_14)); }
	inline InputField_t3762917431 * get_initialMessage_14() const { return ___initialMessage_14; }
	inline InputField_t3762917431 ** get_address_of_initialMessage_14() { return &___initialMessage_14; }
	inline void set_initialMessage_14(InputField_t3762917431 * value)
	{
		___initialMessage_14 = value;
		Il2CppCodeGenWriteBarrier((&___initialMessage_14), value);
	}

	inline static int32_t get_offset_of_replySubject_15() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___replySubject_15)); }
	inline InputField_t3762917431 * get_replySubject_15() const { return ___replySubject_15; }
	inline InputField_t3762917431 ** get_address_of_replySubject_15() { return &___replySubject_15; }
	inline void set_replySubject_15(InputField_t3762917431 * value)
	{
		___replySubject_15 = value;
		Il2CppCodeGenWriteBarrier((&___replySubject_15), value);
	}

	inline static int32_t get_offset_of_replyMessage_16() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___replyMessage_16)); }
	inline InputField_t3762917431 * get_replyMessage_16() const { return ___replyMessage_16; }
	inline InputField_t3762917431 ** get_address_of_replyMessage_16() { return &___replyMessage_16; }
	inline void set_replyMessage_16(InputField_t3762917431 * value)
	{
		___replyMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___replyMessage_16), value);
	}

	inline static int32_t get_offset_of_messageId_17() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___messageId_17)); }
	inline int64_t get_messageId_17() const { return ___messageId_17; }
	inline int64_t* get_address_of_messageId_17() { return &___messageId_17; }
	inline void set_messageId_17(int64_t value)
	{
		___messageId_17 = value;
	}

	inline static int32_t get_offset_of_reply_18() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___reply_18)); }
	inline bool get_reply_18() const { return ___reply_18; }
	inline bool* get_address_of_reply_18() { return &___reply_18; }
	inline void set_reply_18(bool value)
	{
		___reply_18 = value;
	}

	inline static int32_t get_offset_of_message_19() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___message_19)); }
	inline UserMessageReportModel_t4065405063 * get_message_19() const { return ___message_19; }
	inline UserMessageReportModel_t4065405063 ** get_address_of_message_19() { return &___message_19; }
	inline void set_message_19(UserMessageReportModel_t4065405063 * value)
	{
		___message_19 = value;
		Il2CppCodeGenWriteBarrier((&___message_19), value);
	}

	inline static int32_t get_offset_of_ownerTable_20() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___ownerTable_20)); }
	inline MailInboxTableController_t2667561573 * get_ownerTable_20() const { return ___ownerTable_20; }
	inline MailInboxTableController_t2667561573 ** get_address_of_ownerTable_20() { return &___ownerTable_20; }
	inline void set_ownerTable_20(MailInboxTableController_t2667561573 * value)
	{
		___ownerTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___ownerTable_20), value);
	}

	inline static int32_t get_offset_of_dateFormat_21() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___dateFormat_21)); }
	inline String_t* get_dateFormat_21() const { return ___dateFormat_21; }
	inline String_t** get_address_of_dateFormat_21() { return &___dateFormat_21; }
	inline void set_dateFormat_21(String_t* value)
	{
		___dateFormat_21 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_21), value);
	}

	inline static int32_t get_offset_of_gotMessageDetails_22() { return static_cast<int32_t>(offsetof(MessageContentManager_t985817454, ___gotMessageDetails_22)); }
	inline SimpleEvent_t129249603 * get_gotMessageDetails_22() const { return ___gotMessageDetails_22; }
	inline SimpleEvent_t129249603 ** get_address_of_gotMessageDetails_22() { return &___gotMessageDetails_22; }
	inline void set_gotMessageDetails_22(SimpleEvent_t129249603 * value)
	{
		___gotMessageDetails_22 = value;
		Il2CppCodeGenWriteBarrier((&___gotMessageDetails_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGECONTENTMANAGER_T985817454_H
#ifndef OVERVIEWMANAGER_T3338185744_H
#define OVERVIEWMANAGER_T3338185744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverviewManager
struct  OverviewManager_t3338185744  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject OverviewManager::lordPanel
	GameObject_t1113636619 * ___lordPanel_2;
	// UnityEngine.GameObject OverviewManager::overviewPanel
	GameObject_t1113636619 * ___overviewPanel_3;
	// UnityEngine.UI.Image OverviewManager::playerImage
	Image_t2670269651 * ___playerImage_4;
	// UnityEngine.UI.Text OverviewManager::playerName
	Text_t1901882714 * ___playerName_5;
	// UnityEngine.UI.Text OverviewManager::playerRank
	Text_t1901882714 * ___playerRank_6;
	// UnityEngine.UI.Text OverviewManager::playerAlliance
	Text_t1901882714 * ___playerAlliance_7;
	// UnityEngine.UI.Text OverviewManager::playerExperience
	Text_t1901882714 * ___playerExperience_8;
	// UnityEngine.UI.Text OverviewManager::cityName
	Text_t1901882714 * ___cityName_9;
	// UnityEngine.UI.Text OverviewManager::cityRegion
	Text_t1901882714 * ___cityRegion_10;
	// UnityEngine.UI.Text OverviewManager::cityCarrage
	Text_t1901882714 * ___cityCarrage_11;
	// UnityEngine.UI.Text OverviewManager::cityHappiness
	Text_t1901882714 * ___cityHappiness_12;
	// UnityEngine.UI.Text OverviewManager::cityPopulation
	Text_t1901882714 * ___cityPopulation_13;
	// UnityEngine.UI.Text OverviewManager::cityIdlePopulation
	Text_t1901882714 * ___cityIdlePopulation_14;
	// UnityEngine.UI.Text OverviewManager::cityTax
	Text_t1901882714 * ___cityTax_15;
	// System.String OverviewManager::userImageName
	String_t* ___userImageName_16;

public:
	inline static int32_t get_offset_of_lordPanel_2() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___lordPanel_2)); }
	inline GameObject_t1113636619 * get_lordPanel_2() const { return ___lordPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_lordPanel_2() { return &___lordPanel_2; }
	inline void set_lordPanel_2(GameObject_t1113636619 * value)
	{
		___lordPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___lordPanel_2), value);
	}

	inline static int32_t get_offset_of_overviewPanel_3() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___overviewPanel_3)); }
	inline GameObject_t1113636619 * get_overviewPanel_3() const { return ___overviewPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_overviewPanel_3() { return &___overviewPanel_3; }
	inline void set_overviewPanel_3(GameObject_t1113636619 * value)
	{
		___overviewPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___overviewPanel_3), value);
	}

	inline static int32_t get_offset_of_playerImage_4() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___playerImage_4)); }
	inline Image_t2670269651 * get_playerImage_4() const { return ___playerImage_4; }
	inline Image_t2670269651 ** get_address_of_playerImage_4() { return &___playerImage_4; }
	inline void set_playerImage_4(Image_t2670269651 * value)
	{
		___playerImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerImage_4), value);
	}

	inline static int32_t get_offset_of_playerName_5() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___playerName_5)); }
	inline Text_t1901882714 * get_playerName_5() const { return ___playerName_5; }
	inline Text_t1901882714 ** get_address_of_playerName_5() { return &___playerName_5; }
	inline void set_playerName_5(Text_t1901882714 * value)
	{
		___playerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_5), value);
	}

	inline static int32_t get_offset_of_playerRank_6() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___playerRank_6)); }
	inline Text_t1901882714 * get_playerRank_6() const { return ___playerRank_6; }
	inline Text_t1901882714 ** get_address_of_playerRank_6() { return &___playerRank_6; }
	inline void set_playerRank_6(Text_t1901882714 * value)
	{
		___playerRank_6 = value;
		Il2CppCodeGenWriteBarrier((&___playerRank_6), value);
	}

	inline static int32_t get_offset_of_playerAlliance_7() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___playerAlliance_7)); }
	inline Text_t1901882714 * get_playerAlliance_7() const { return ___playerAlliance_7; }
	inline Text_t1901882714 ** get_address_of_playerAlliance_7() { return &___playerAlliance_7; }
	inline void set_playerAlliance_7(Text_t1901882714 * value)
	{
		___playerAlliance_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerAlliance_7), value);
	}

	inline static int32_t get_offset_of_playerExperience_8() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___playerExperience_8)); }
	inline Text_t1901882714 * get_playerExperience_8() const { return ___playerExperience_8; }
	inline Text_t1901882714 ** get_address_of_playerExperience_8() { return &___playerExperience_8; }
	inline void set_playerExperience_8(Text_t1901882714 * value)
	{
		___playerExperience_8 = value;
		Il2CppCodeGenWriteBarrier((&___playerExperience_8), value);
	}

	inline static int32_t get_offset_of_cityName_9() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityName_9)); }
	inline Text_t1901882714 * get_cityName_9() const { return ___cityName_9; }
	inline Text_t1901882714 ** get_address_of_cityName_9() { return &___cityName_9; }
	inline void set_cityName_9(Text_t1901882714 * value)
	{
		___cityName_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_9), value);
	}

	inline static int32_t get_offset_of_cityRegion_10() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityRegion_10)); }
	inline Text_t1901882714 * get_cityRegion_10() const { return ___cityRegion_10; }
	inline Text_t1901882714 ** get_address_of_cityRegion_10() { return &___cityRegion_10; }
	inline void set_cityRegion_10(Text_t1901882714 * value)
	{
		___cityRegion_10 = value;
		Il2CppCodeGenWriteBarrier((&___cityRegion_10), value);
	}

	inline static int32_t get_offset_of_cityCarrage_11() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityCarrage_11)); }
	inline Text_t1901882714 * get_cityCarrage_11() const { return ___cityCarrage_11; }
	inline Text_t1901882714 ** get_address_of_cityCarrage_11() { return &___cityCarrage_11; }
	inline void set_cityCarrage_11(Text_t1901882714 * value)
	{
		___cityCarrage_11 = value;
		Il2CppCodeGenWriteBarrier((&___cityCarrage_11), value);
	}

	inline static int32_t get_offset_of_cityHappiness_12() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityHappiness_12)); }
	inline Text_t1901882714 * get_cityHappiness_12() const { return ___cityHappiness_12; }
	inline Text_t1901882714 ** get_address_of_cityHappiness_12() { return &___cityHappiness_12; }
	inline void set_cityHappiness_12(Text_t1901882714 * value)
	{
		___cityHappiness_12 = value;
		Il2CppCodeGenWriteBarrier((&___cityHappiness_12), value);
	}

	inline static int32_t get_offset_of_cityPopulation_13() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityPopulation_13)); }
	inline Text_t1901882714 * get_cityPopulation_13() const { return ___cityPopulation_13; }
	inline Text_t1901882714 ** get_address_of_cityPopulation_13() { return &___cityPopulation_13; }
	inline void set_cityPopulation_13(Text_t1901882714 * value)
	{
		___cityPopulation_13 = value;
		Il2CppCodeGenWriteBarrier((&___cityPopulation_13), value);
	}

	inline static int32_t get_offset_of_cityIdlePopulation_14() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityIdlePopulation_14)); }
	inline Text_t1901882714 * get_cityIdlePopulation_14() const { return ___cityIdlePopulation_14; }
	inline Text_t1901882714 ** get_address_of_cityIdlePopulation_14() { return &___cityIdlePopulation_14; }
	inline void set_cityIdlePopulation_14(Text_t1901882714 * value)
	{
		___cityIdlePopulation_14 = value;
		Il2CppCodeGenWriteBarrier((&___cityIdlePopulation_14), value);
	}

	inline static int32_t get_offset_of_cityTax_15() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___cityTax_15)); }
	inline Text_t1901882714 * get_cityTax_15() const { return ___cityTax_15; }
	inline Text_t1901882714 ** get_address_of_cityTax_15() { return &___cityTax_15; }
	inline void set_cityTax_15(Text_t1901882714 * value)
	{
		___cityTax_15 = value;
		Il2CppCodeGenWriteBarrier((&___cityTax_15), value);
	}

	inline static int32_t get_offset_of_userImageName_16() { return static_cast<int32_t>(offsetof(OverviewManager_t3338185744, ___userImageName_16)); }
	inline String_t* get_userImageName_16() const { return ___userImageName_16; }
	inline String_t** get_address_of_userImageName_16() { return &___userImageName_16; }
	inline void set_userImageName_16(String_t* value)
	{
		___userImageName_16 = value;
		Il2CppCodeGenWriteBarrier((&___userImageName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERVIEWMANAGER_T3338185744_H
#ifndef REINFORCETRANSPORTREPORTMANAGER_T2386156816_H
#define REINFORCETRANSPORTREPORTMANAGER_T2386156816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReinforceTransportReportManager
struct  ReinforceTransportReportManager_t2386156816  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerName
	Text_t1901882714 * ___attackerName_2;
	// UnityEngine.UI.Image ReinforceTransportReportManager::attackerImage
	Image_t2670269651 * ___attackerImage_3;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerRank
	Text_t1901882714 * ___attackerRank_4;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerAllianceName
	Text_t1901882714 * ___attackerAllianceName_5;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerAlliancePosition
	Text_t1901882714 * ___attackerAlliancePosition_6;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerCityName
	Text_t1901882714 * ___attackerCityName_7;
	// UnityEngine.UI.Image ReinforceTransportReportManager::attackerCityImage
	Image_t2670269651 * ___attackerCityImage_8;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerExperiance
	Text_t1901882714 * ___attackerExperiance_9;
	// UnityEngine.UI.Text ReinforceTransportReportManager::attackerCityRegion
	Text_t1901882714 * ___attackerCityRegion_10;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderName
	Text_t1901882714 * ___defenderName_11;
	// UnityEngine.UI.Image ReinforceTransportReportManager::defenderImage
	Image_t2670269651 * ___defenderImage_12;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderRank
	Text_t1901882714 * ___defenderRank_13;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderAllianceName
	Text_t1901882714 * ___defenderAllianceName_14;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderAlliancePosition
	Text_t1901882714 * ___defenderAlliancePosition_15;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderCityName
	Text_t1901882714 * ___defenderCityName_16;
	// UnityEngine.UI.Image ReinforceTransportReportManager::defenderCityImage
	Image_t2670269651 * ___defenderCityImage_17;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderExperiance
	Text_t1901882714 * ___defenderExperiance_18;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderCityRegion
	Text_t1901882714 * ___defenderCityRegion_19;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderCityHappiness
	Text_t1901882714 * ___defenderCityHappiness_20;
	// UnityEngine.UI.Text ReinforceTransportReportManager::defenderCityCourage
	Text_t1901882714 * ___defenderCityCourage_21;
	// BattleReportModel ReinforceTransportReportManager::report
	BattleReportModel_t4250794538 * ___report_22;
	// BattleReportUnitTableController ReinforceTransportReportManager::attackerUnitsController
	BattleReportUnitTableController_t1840302685 * ___attackerUnitsController_23;
	// BattleReportResourcesTableController ReinforceTransportReportManager::attackerResourcesController
	BattleReportResourcesTableController_t1929107072 * ___attackerResourcesController_24;
	// System.Collections.Generic.List`1<System.String> ReinforceTransportReportManager::attackerUnitNames
	List_1_t3319525431 * ___attackerUnitNames_25;
	// System.Collections.Generic.List`1<System.Int64> ReinforceTransportReportManager::attackerBeforeCount
	List_1_t913674750 * ___attackerBeforeCount_26;
	// System.Collections.Generic.List`1<System.Int64> ReinforceTransportReportManager::attackerAfterCount
	List_1_t913674750 * ___attackerAfterCount_27;
	// System.Collections.Generic.List`1<System.String> ReinforceTransportReportManager::defenderUnitNames
	List_1_t3319525431 * ___defenderUnitNames_28;
	// System.Collections.Generic.List`1<System.Int64> ReinforceTransportReportManager::defenderBeforeCount
	List_1_t913674750 * ___defenderBeforeCount_29;
	// System.Collections.Generic.List`1<System.Int64> ReinforceTransportReportManager::defenderAfterCount
	List_1_t913674750 * ___defenderAfterCount_30;

public:
	inline static int32_t get_offset_of_attackerName_2() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerName_2)); }
	inline Text_t1901882714 * get_attackerName_2() const { return ___attackerName_2; }
	inline Text_t1901882714 ** get_address_of_attackerName_2() { return &___attackerName_2; }
	inline void set_attackerName_2(Text_t1901882714 * value)
	{
		___attackerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___attackerName_2), value);
	}

	inline static int32_t get_offset_of_attackerImage_3() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerImage_3)); }
	inline Image_t2670269651 * get_attackerImage_3() const { return ___attackerImage_3; }
	inline Image_t2670269651 ** get_address_of_attackerImage_3() { return &___attackerImage_3; }
	inline void set_attackerImage_3(Image_t2670269651 * value)
	{
		___attackerImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___attackerImage_3), value);
	}

	inline static int32_t get_offset_of_attackerRank_4() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerRank_4)); }
	inline Text_t1901882714 * get_attackerRank_4() const { return ___attackerRank_4; }
	inline Text_t1901882714 ** get_address_of_attackerRank_4() { return &___attackerRank_4; }
	inline void set_attackerRank_4(Text_t1901882714 * value)
	{
		___attackerRank_4 = value;
		Il2CppCodeGenWriteBarrier((&___attackerRank_4), value);
	}

	inline static int32_t get_offset_of_attackerAllianceName_5() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerAllianceName_5)); }
	inline Text_t1901882714 * get_attackerAllianceName_5() const { return ___attackerAllianceName_5; }
	inline Text_t1901882714 ** get_address_of_attackerAllianceName_5() { return &___attackerAllianceName_5; }
	inline void set_attackerAllianceName_5(Text_t1901882714 * value)
	{
		___attackerAllianceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAllianceName_5), value);
	}

	inline static int32_t get_offset_of_attackerAlliancePosition_6() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerAlliancePosition_6)); }
	inline Text_t1901882714 * get_attackerAlliancePosition_6() const { return ___attackerAlliancePosition_6; }
	inline Text_t1901882714 ** get_address_of_attackerAlliancePosition_6() { return &___attackerAlliancePosition_6; }
	inline void set_attackerAlliancePosition_6(Text_t1901882714 * value)
	{
		___attackerAlliancePosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAlliancePosition_6), value);
	}

	inline static int32_t get_offset_of_attackerCityName_7() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerCityName_7)); }
	inline Text_t1901882714 * get_attackerCityName_7() const { return ___attackerCityName_7; }
	inline Text_t1901882714 ** get_address_of_attackerCityName_7() { return &___attackerCityName_7; }
	inline void set_attackerCityName_7(Text_t1901882714 * value)
	{
		___attackerCityName_7 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityName_7), value);
	}

	inline static int32_t get_offset_of_attackerCityImage_8() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerCityImage_8)); }
	inline Image_t2670269651 * get_attackerCityImage_8() const { return ___attackerCityImage_8; }
	inline Image_t2670269651 ** get_address_of_attackerCityImage_8() { return &___attackerCityImage_8; }
	inline void set_attackerCityImage_8(Image_t2670269651 * value)
	{
		___attackerCityImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityImage_8), value);
	}

	inline static int32_t get_offset_of_attackerExperiance_9() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerExperiance_9)); }
	inline Text_t1901882714 * get_attackerExperiance_9() const { return ___attackerExperiance_9; }
	inline Text_t1901882714 ** get_address_of_attackerExperiance_9() { return &___attackerExperiance_9; }
	inline void set_attackerExperiance_9(Text_t1901882714 * value)
	{
		___attackerExperiance_9 = value;
		Il2CppCodeGenWriteBarrier((&___attackerExperiance_9), value);
	}

	inline static int32_t get_offset_of_attackerCityRegion_10() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerCityRegion_10)); }
	inline Text_t1901882714 * get_attackerCityRegion_10() const { return ___attackerCityRegion_10; }
	inline Text_t1901882714 ** get_address_of_attackerCityRegion_10() { return &___attackerCityRegion_10; }
	inline void set_attackerCityRegion_10(Text_t1901882714 * value)
	{
		___attackerCityRegion_10 = value;
		Il2CppCodeGenWriteBarrier((&___attackerCityRegion_10), value);
	}

	inline static int32_t get_offset_of_defenderName_11() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderName_11)); }
	inline Text_t1901882714 * get_defenderName_11() const { return ___defenderName_11; }
	inline Text_t1901882714 ** get_address_of_defenderName_11() { return &___defenderName_11; }
	inline void set_defenderName_11(Text_t1901882714 * value)
	{
		___defenderName_11 = value;
		Il2CppCodeGenWriteBarrier((&___defenderName_11), value);
	}

	inline static int32_t get_offset_of_defenderImage_12() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderImage_12)); }
	inline Image_t2670269651 * get_defenderImage_12() const { return ___defenderImage_12; }
	inline Image_t2670269651 ** get_address_of_defenderImage_12() { return &___defenderImage_12; }
	inline void set_defenderImage_12(Image_t2670269651 * value)
	{
		___defenderImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___defenderImage_12), value);
	}

	inline static int32_t get_offset_of_defenderRank_13() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderRank_13)); }
	inline Text_t1901882714 * get_defenderRank_13() const { return ___defenderRank_13; }
	inline Text_t1901882714 ** get_address_of_defenderRank_13() { return &___defenderRank_13; }
	inline void set_defenderRank_13(Text_t1901882714 * value)
	{
		___defenderRank_13 = value;
		Il2CppCodeGenWriteBarrier((&___defenderRank_13), value);
	}

	inline static int32_t get_offset_of_defenderAllianceName_14() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderAllianceName_14)); }
	inline Text_t1901882714 * get_defenderAllianceName_14() const { return ___defenderAllianceName_14; }
	inline Text_t1901882714 ** get_address_of_defenderAllianceName_14() { return &___defenderAllianceName_14; }
	inline void set_defenderAllianceName_14(Text_t1901882714 * value)
	{
		___defenderAllianceName_14 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAllianceName_14), value);
	}

	inline static int32_t get_offset_of_defenderAlliancePosition_15() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderAlliancePosition_15)); }
	inline Text_t1901882714 * get_defenderAlliancePosition_15() const { return ___defenderAlliancePosition_15; }
	inline Text_t1901882714 ** get_address_of_defenderAlliancePosition_15() { return &___defenderAlliancePosition_15; }
	inline void set_defenderAlliancePosition_15(Text_t1901882714 * value)
	{
		___defenderAlliancePosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAlliancePosition_15), value);
	}

	inline static int32_t get_offset_of_defenderCityName_16() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderCityName_16)); }
	inline Text_t1901882714 * get_defenderCityName_16() const { return ___defenderCityName_16; }
	inline Text_t1901882714 ** get_address_of_defenderCityName_16() { return &___defenderCityName_16; }
	inline void set_defenderCityName_16(Text_t1901882714 * value)
	{
		___defenderCityName_16 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityName_16), value);
	}

	inline static int32_t get_offset_of_defenderCityImage_17() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderCityImage_17)); }
	inline Image_t2670269651 * get_defenderCityImage_17() const { return ___defenderCityImage_17; }
	inline Image_t2670269651 ** get_address_of_defenderCityImage_17() { return &___defenderCityImage_17; }
	inline void set_defenderCityImage_17(Image_t2670269651 * value)
	{
		___defenderCityImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityImage_17), value);
	}

	inline static int32_t get_offset_of_defenderExperiance_18() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderExperiance_18)); }
	inline Text_t1901882714 * get_defenderExperiance_18() const { return ___defenderExperiance_18; }
	inline Text_t1901882714 ** get_address_of_defenderExperiance_18() { return &___defenderExperiance_18; }
	inline void set_defenderExperiance_18(Text_t1901882714 * value)
	{
		___defenderExperiance_18 = value;
		Il2CppCodeGenWriteBarrier((&___defenderExperiance_18), value);
	}

	inline static int32_t get_offset_of_defenderCityRegion_19() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderCityRegion_19)); }
	inline Text_t1901882714 * get_defenderCityRegion_19() const { return ___defenderCityRegion_19; }
	inline Text_t1901882714 ** get_address_of_defenderCityRegion_19() { return &___defenderCityRegion_19; }
	inline void set_defenderCityRegion_19(Text_t1901882714 * value)
	{
		___defenderCityRegion_19 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityRegion_19), value);
	}

	inline static int32_t get_offset_of_defenderCityHappiness_20() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderCityHappiness_20)); }
	inline Text_t1901882714 * get_defenderCityHappiness_20() const { return ___defenderCityHappiness_20; }
	inline Text_t1901882714 ** get_address_of_defenderCityHappiness_20() { return &___defenderCityHappiness_20; }
	inline void set_defenderCityHappiness_20(Text_t1901882714 * value)
	{
		___defenderCityHappiness_20 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityHappiness_20), value);
	}

	inline static int32_t get_offset_of_defenderCityCourage_21() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderCityCourage_21)); }
	inline Text_t1901882714 * get_defenderCityCourage_21() const { return ___defenderCityCourage_21; }
	inline Text_t1901882714 ** get_address_of_defenderCityCourage_21() { return &___defenderCityCourage_21; }
	inline void set_defenderCityCourage_21(Text_t1901882714 * value)
	{
		___defenderCityCourage_21 = value;
		Il2CppCodeGenWriteBarrier((&___defenderCityCourage_21), value);
	}

	inline static int32_t get_offset_of_report_22() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___report_22)); }
	inline BattleReportModel_t4250794538 * get_report_22() const { return ___report_22; }
	inline BattleReportModel_t4250794538 ** get_address_of_report_22() { return &___report_22; }
	inline void set_report_22(BattleReportModel_t4250794538 * value)
	{
		___report_22 = value;
		Il2CppCodeGenWriteBarrier((&___report_22), value);
	}

	inline static int32_t get_offset_of_attackerUnitsController_23() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerUnitsController_23)); }
	inline BattleReportUnitTableController_t1840302685 * get_attackerUnitsController_23() const { return ___attackerUnitsController_23; }
	inline BattleReportUnitTableController_t1840302685 ** get_address_of_attackerUnitsController_23() { return &___attackerUnitsController_23; }
	inline void set_attackerUnitsController_23(BattleReportUnitTableController_t1840302685 * value)
	{
		___attackerUnitsController_23 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitsController_23), value);
	}

	inline static int32_t get_offset_of_attackerResourcesController_24() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerResourcesController_24)); }
	inline BattleReportResourcesTableController_t1929107072 * get_attackerResourcesController_24() const { return ___attackerResourcesController_24; }
	inline BattleReportResourcesTableController_t1929107072 ** get_address_of_attackerResourcesController_24() { return &___attackerResourcesController_24; }
	inline void set_attackerResourcesController_24(BattleReportResourcesTableController_t1929107072 * value)
	{
		___attackerResourcesController_24 = value;
		Il2CppCodeGenWriteBarrier((&___attackerResourcesController_24), value);
	}

	inline static int32_t get_offset_of_attackerUnitNames_25() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerUnitNames_25)); }
	inline List_1_t3319525431 * get_attackerUnitNames_25() const { return ___attackerUnitNames_25; }
	inline List_1_t3319525431 ** get_address_of_attackerUnitNames_25() { return &___attackerUnitNames_25; }
	inline void set_attackerUnitNames_25(List_1_t3319525431 * value)
	{
		___attackerUnitNames_25 = value;
		Il2CppCodeGenWriteBarrier((&___attackerUnitNames_25), value);
	}

	inline static int32_t get_offset_of_attackerBeforeCount_26() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerBeforeCount_26)); }
	inline List_1_t913674750 * get_attackerBeforeCount_26() const { return ___attackerBeforeCount_26; }
	inline List_1_t913674750 ** get_address_of_attackerBeforeCount_26() { return &___attackerBeforeCount_26; }
	inline void set_attackerBeforeCount_26(List_1_t913674750 * value)
	{
		___attackerBeforeCount_26 = value;
		Il2CppCodeGenWriteBarrier((&___attackerBeforeCount_26), value);
	}

	inline static int32_t get_offset_of_attackerAfterCount_27() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___attackerAfterCount_27)); }
	inline List_1_t913674750 * get_attackerAfterCount_27() const { return ___attackerAfterCount_27; }
	inline List_1_t913674750 ** get_address_of_attackerAfterCount_27() { return &___attackerAfterCount_27; }
	inline void set_attackerAfterCount_27(List_1_t913674750 * value)
	{
		___attackerAfterCount_27 = value;
		Il2CppCodeGenWriteBarrier((&___attackerAfterCount_27), value);
	}

	inline static int32_t get_offset_of_defenderUnitNames_28() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderUnitNames_28)); }
	inline List_1_t3319525431 * get_defenderUnitNames_28() const { return ___defenderUnitNames_28; }
	inline List_1_t3319525431 ** get_address_of_defenderUnitNames_28() { return &___defenderUnitNames_28; }
	inline void set_defenderUnitNames_28(List_1_t3319525431 * value)
	{
		___defenderUnitNames_28 = value;
		Il2CppCodeGenWriteBarrier((&___defenderUnitNames_28), value);
	}

	inline static int32_t get_offset_of_defenderBeforeCount_29() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderBeforeCount_29)); }
	inline List_1_t913674750 * get_defenderBeforeCount_29() const { return ___defenderBeforeCount_29; }
	inline List_1_t913674750 ** get_address_of_defenderBeforeCount_29() { return &___defenderBeforeCount_29; }
	inline void set_defenderBeforeCount_29(List_1_t913674750 * value)
	{
		___defenderBeforeCount_29 = value;
		Il2CppCodeGenWriteBarrier((&___defenderBeforeCount_29), value);
	}

	inline static int32_t get_offset_of_defenderAfterCount_30() { return static_cast<int32_t>(offsetof(ReinforceTransportReportManager_t2386156816, ___defenderAfterCount_30)); }
	inline List_1_t913674750 * get_defenderAfterCount_30() const { return ___defenderAfterCount_30; }
	inline List_1_t913674750 ** get_address_of_defenderAfterCount_30() { return &___defenderAfterCount_30; }
	inline void set_defenderAfterCount_30(List_1_t913674750 * value)
	{
		___defenderAfterCount_30 = value;
		Il2CppCodeGenWriteBarrier((&___defenderAfterCount_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REINFORCETRANSPORTREPORTMANAGER_T2386156816_H
#ifndef NOTIFICATIONWINDOWMANAGER_T3101019839_H
#define NOTIFICATIONWINDOWMANAGER_T3101019839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationWindowManager
struct  NotificationWindowManager_t3101019839  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text NotificationWindowManager::notificationText
	Text_t1901882714 * ___notificationText_2;

public:
	inline static int32_t get_offset_of_notificationText_2() { return static_cast<int32_t>(offsetof(NotificationWindowManager_t3101019839, ___notificationText_2)); }
	inline Text_t1901882714 * get_notificationText_2() const { return ___notificationText_2; }
	inline Text_t1901882714 ** get_address_of_notificationText_2() { return &___notificationText_2; }
	inline void set_notificationText_2(Text_t1901882714 * value)
	{
		___notificationText_2 = value;
		Il2CppCodeGenWriteBarrier((&___notificationText_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONWINDOWMANAGER_T3101019839_H
#ifndef NEWALLIANCECONTENTMANAGER_T2496725915_H
#define NEWALLIANCECONTENTMANAGER_T2496725915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewAllianceContentManager
struct  NewAllianceContentManager_t2496725915  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField NewAllianceContentManager::allianceName
	InputField_t3762917431 * ___allianceName_2;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(NewAllianceContentManager_t2496725915, ___allianceName_2)); }
	inline InputField_t3762917431 * get_allianceName_2() const { return ___allianceName_2; }
	inline InputField_t3762917431 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(InputField_t3762917431 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWALLIANCECONTENTMANAGER_T2496725915_H
#ifndef NEWMAILCONTENTMANAGER_T1552843396_H
#define NEWMAILCONTENTMANAGER_T1552843396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailContentManager
struct  NewMailContentManager_t1552843396  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField NewMailContentManager::subject
	InputField_t3762917431 * ___subject_2;
	// UnityEngine.UI.InputField NewMailContentManager::message
	InputField_t3762917431 * ___message_3;

public:
	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(NewMailContentManager_t1552843396, ___subject_2)); }
	inline InputField_t3762917431 * get_subject_2() const { return ___subject_2; }
	inline InputField_t3762917431 ** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(InputField_t3762917431 * value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(NewMailContentManager_t1552843396, ___message_3)); }
	inline InputField_t3762917431 * get_message_3() const { return ___message_3; }
	inline InputField_t3762917431 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(InputField_t3762917431 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWMAILCONTENTMANAGER_T1552843396_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (AllianceInformationContentManager_t1344194199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[8] = 
{
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceName_2(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceFounder_3(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceLeader_4(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceRank_5(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceExperiance_6(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceMembersCount_7(),
	AllianceInformationContentManager_t1344194199::get_offset_of_allianceGuideline_8(),
	AllianceInformationContentManager_t1344194199::get_offset_of_confirmed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (ArmyDetailsContentManager_t3479482664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[23] = 
{
	ArmyDetailsContentManager_t3479482664::get_offset_of_coords_2(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_cityName_3(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_targetPlayer_4(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyId_5(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_owner_6(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyWorker_7(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armySpy_8(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armySwordsman_9(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armySpearman_10(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyPikeman_11(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyScoutRider_12(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyLightCavalry_13(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyHeavyCavalry_14(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyArcher_15(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyArcherRider_16(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyHollyman_17(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyWagon_18(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyTrebuchet_19(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armySiegeTower_20(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyBatteringRam_21(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_armyBallista_22(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_army_23(),
	ArmyDetailsContentManager_t3479482664::get_offset_of_gotReturnArmy_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (AttackColonizeDestroyReportManager_t1990770801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[34] = 
{
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerName_2(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerImage_3(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerRank_4(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerAllianceName_5(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerAlliancePosition_6(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerCityName_7(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerCityImage_8(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerExperiance_9(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerCityRegion_10(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderName_11(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderImage_12(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderRank_13(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderAllianceName_14(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderAlliancePosition_15(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderCityName_16(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderCityImage_17(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderExperiance_18(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderCityRegion_19(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderCityHappiness_20(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderCityCourage_21(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerResult_22(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderResult_23(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_report_24(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerUnitsController_25(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderUnitsController_26(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerResourcesController_27(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderResourcesController_28(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerItemsController_29(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerUnitNames_30(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerBeforeCount_31(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_attackerAfterCount_32(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderUnitNames_33(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderBeforeCount_34(),
	AttackColonizeDestroyReportManager_t1990770801::get_offset_of_defenderAfterCount_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (BarbarianRecruitManager_t3050290676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[44] = 
{
	BarbarianRecruitManager_t3050290676::get_offset_of_citySilver_2(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityGold_3(),
	BarbarianRecruitManager_t3050290676::get_offset_of_priceSilver_4(),
	BarbarianRecruitManager_t3050290676::get_offset_of_priceGold_5(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityWorker_6(),
	BarbarianRecruitManager_t3050290676::get_offset_of_citySpy_7(),
	BarbarianRecruitManager_t3050290676::get_offset_of_citySwordsman_8(),
	BarbarianRecruitManager_t3050290676::get_offset_of_citySpearman_9(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityPikeman_10(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityScoutRider_11(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityLightCavalry_12(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityHeavyCavalry_13(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityArcher_14(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityArcherRider_15(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityHollyman_16(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityWagon_17(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityTrebuchet_18(),
	BarbarianRecruitManager_t3050290676::get_offset_of_citySiegeTower_19(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityBatteringRam_20(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityBallista_21(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchWorker_22(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchSpy_23(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchSwordsman_24(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchSpearman_25(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchPikeman_26(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchScoutRider_27(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchLightCavalry_28(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchHeavyCavalry_29(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchArcher_30(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchArcherRider_31(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchHollyman_32(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchWagon_33(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchTrebuchet_34(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchSiegeTower_35(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchBatteringRam_36(),
	BarbarianRecruitManager_t3050290676::get_offset_of_dispatchBallista_37(),
	BarbarianRecruitManager_t3050290676::get_offset_of_xInput_38(),
	BarbarianRecruitManager_t3050290676::get_offset_of_yInput_39(),
	BarbarianRecruitManager_t3050290676::get_offset_of_targetField_40(),
	BarbarianRecruitManager_t3050290676::get_offset_of_onGetBarbariansArmy_41(),
	BarbarianRecruitManager_t3050290676::get_offset_of_presentArmy_42(),
	BarbarianRecruitManager_t3050290676::get_offset_of_army_43(),
	BarbarianRecruitManager_t3050290676::get_offset_of_cityResources_44(),
	BarbarianRecruitManager_t3050290676::get_offset_of_unitConstants_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[5] = 
{
	U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637::get_offset_of_U24this_1(),
	U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637::get_offset_of_U24current_2(),
	U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637::get_offset_of_U24disposing_3(),
	U3CGetBarbariansArmyU3Ec__Iterator0_t2434597637::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (BattleLogContentManager_t1438568355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[2] = 
{
	BattleLogContentManager_t1438568355::get_offset_of_content_2(),
	BattleLogContentManager_t1438568355::get_offset_of_log_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (BattleReportContentManager_t922857224), -1, sizeof(BattleReportContentManager_t922857224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3306[9] = 
{
	BattleReportContentManager_t922857224::get_offset_of_attackColonizeDestroySection_2(),
	BattleReportContentManager_t922857224::get_offset_of_convertSection_3(),
	BattleReportContentManager_t922857224::get_offset_of_scoutSection_4(),
	BattleReportContentManager_t922857224::get_offset_of_reinforceTransportSection_5(),
	BattleReportContentManager_t922857224::get_offset_of_reportId_6(),
	BattleReportContentManager_t922857224::get_offset_of_report_7(),
	BattleReportContentManager_t922857224::get_offset_of_onGetBattleReport_8(),
	BattleReportContentManager_t922857224::get_offset_of_dateFormat_9(),
	BattleReportContentManager_t922857224_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (U3CGetBattleReportU3Ec__Iterator0_t2580360730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[5] = 
{
	U3CGetBattleReportU3Ec__Iterator0_t2580360730::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetBattleReportU3Ec__Iterator0_t2580360730::get_offset_of_U24this_1(),
	U3CGetBattleReportU3Ec__Iterator0_t2580360730::get_offset_of_U24current_2(),
	U3CGetBattleReportU3Ec__Iterator0_t2580360730::get_offset_of_U24disposing_3(),
	U3CGetBattleReportU3Ec__Iterator0_t2580360730::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (BuildingInformationManager_t3344348785), -1, sizeof(BuildingInformationManager_t3344348785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3308[22] = 
{
	BuildingInformationManager_t3344348785::get_offset_of_pitManager_2(),
	BuildingInformationManager_t3344348785::get_offset_of_trainingPanel_3(),
	BuildingInformationManager_t3344348785::get_offset_of_marketPanel_4(),
	BuildingInformationManager_t3344348785::get_offset_of_residencePanel_5(),
	BuildingInformationManager_t3344348785::get_offset_of_guestHousePanel_6(),
	BuildingInformationManager_t3344348785::get_offset_of_feastingHallPanel_7(),
	BuildingInformationManager_t3344348785::get_offset_of_diplomacyPanel_8(),
	BuildingInformationManager_t3344348785::get_offset_of_commandCenterPanel_9(),
	BuildingInformationManager_t3344348785::get_offset_of_archery_10(),
	BuildingInformationManager_t3344348785::get_offset_of_godhouse_11(),
	BuildingInformationManager_t3344348785::get_offset_of_stable_12(),
	BuildingInformationManager_t3344348785::get_offset_of_academy_13(),
	BuildingInformationManager_t3344348785::get_offset_of_engineering_14(),
	BuildingInformationManager_t3344348785::get_offset_of_buildingName_15(),
	BuildingInformationManager_t3344348785::get_offset_of_buildingDesctiption_16(),
	BuildingInformationManager_t3344348785::get_offset_of_buildingLevel_17(),
	BuildingInformationManager_t3344348785::get_offset_of_buildingImage_18(),
	BuildingInformationManager_t3344348785::get_offset_of_taxDropdown_19(),
	BuildingInformationManager_t3344348785::get_offset_of_taxIndex_20(),
	BuildingInformationManager_t3344348785::get_offset_of_type_21(),
	BuildingInformationManager_t3344348785::get_offset_of_world_id_22(),
	BuildingInformationManager_t3344348785_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (BuildNewCityContentManager_t101571291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[11] = 
{
	BuildNewCityContentManager_t101571291::get_offset_of_cityName_2(),
	BuildNewCityContentManager_t101571291::get_offset_of_posX_3(),
	BuildNewCityContentManager_t101571291::get_offset_of_posY_4(),
	BuildNewCityContentManager_t101571291::get_offset_of_foodPresent_5(),
	BuildNewCityContentManager_t101571291::get_offset_of_woodPresent_6(),
	BuildNewCityContentManager_t101571291::get_offset_of_stonePresent_7(),
	BuildNewCityContentManager_t101571291::get_offset_of_ironPresent_8(),
	BuildNewCityContentManager_t101571291::get_offset_of_copperPresent_9(),
	BuildNewCityContentManager_t101571291::get_offset_of_silverPresent_10(),
	BuildNewCityContentManager_t101571291::get_offset_of_goldPresent_11(),
	BuildNewCityContentManager_t101571291::get_offset_of_cityResources_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (BuyItemContentManager_t1100417986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[12] = 
{
	BuyItemContentManager_t1100417986::get_offset_of_itemName_2(),
	BuyItemContentManager_t1100417986::get_offset_of_itemImage_3(),
	BuyItemContentManager_t1100417986::get_offset_of_itemsOwned_4(),
	BuyItemContentManager_t1100417986::get_offset_of_unitPrice_5(),
	BuyItemContentManager_t1100417986::get_offset_of_quantityInput_6(),
	BuyItemContentManager_t1100417986::get_offset_of_totalShillings_7(),
	BuyItemContentManager_t1100417986::get_offset_of_itemId_8(),
	BuyItemContentManager_t1100417986::get_offset_of_unitCost_9(),
	BuyItemContentManager_t1100417986::get_offset_of_itemcount_10(),
	BuyItemContentManager_t1100417986::get_offset_of_toBuyCount_11(),
	BuyItemContentManager_t1100417986::get_offset_of_owner_12(),
	BuyItemContentManager_t1100417986::get_offset_of_buy_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (ChangePasswordContentManager_t2087197744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[3] = 
{
	ChangePasswordContentManager_t2087197744::get_offset_of_oldPassword_2(),
	ChangePasswordContentManager_t2087197744::get_offset_of_newPassword_3(),
	ChangePasswordContentManager_t2087197744::get_offset_of_confirmPassword_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (CityChangerContentManager_t1075607021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[1] = 
{
	CityChangerContentManager_t1075607021::get_offset_of_cityItemPrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (U3CChangeCityU3Ec__Iterator0_t82477809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[6] = 
{
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_U3CloginManagerU3E__0_0(),
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_cityId_1(),
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_U24this_2(),
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_U24current_3(),
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_U24disposing_4(),
	U3CChangeCityU3Ec__Iterator0_t82477809::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (CityDeedContentManager_t1851112673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[16] = 
{
	CityDeedContentManager_t1851112673::get_offset_of_totalBuildings_2(),
	CityDeedContentManager_t1851112673::get_offset_of_buildingsHighestLevel_3(),
	CityDeedContentManager_t1851112673::get_offset_of_totalFields_4(),
	CityDeedContentManager_t1851112673::get_offset_of_fieldsHighestLevel_5(),
	CityDeedContentManager_t1851112673::get_offset_of_totalUnits_6(),
	CityDeedContentManager_t1851112673::get_offset_of_citiesDropdown_7(),
	CityDeedContentManager_t1851112673::get_offset_of_cityName_8(),
	CityDeedContentManager_t1851112673::get_offset_of_cityCoords_9(),
	CityDeedContentManager_t1851112673::get_offset_of_cityPrice_10(),
	CityDeedContentManager_t1851112673::get_offset_of_buyerName_11(),
	CityDeedContentManager_t1851112673::get_offset_of_buyerEmail_12(),
	CityDeedContentManager_t1851112673::get_offset_of_cityIcon_13(),
	CityDeedContentManager_t1851112673::get_offset_of_owner_14(),
	CityDeedContentManager_t1851112673::get_offset_of_item_id_15(),
	CityDeedContentManager_t1851112673::get_offset_of_nameToId_16(),
	CityDeedContentManager_t1851112673::get_offset_of_citiesList_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (U3CGetExoticInfoU3Ec__Iterator0_t1672915424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[5] = 
{
	U3CGetExoticInfoU3Ec__Iterator0_t1672915424::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetExoticInfoU3Ec__Iterator0_t1672915424::get_offset_of_U24this_1(),
	U3CGetExoticInfoU3Ec__Iterator0_t1672915424::get_offset_of_U24current_2(),
	U3CGetExoticInfoU3Ec__Iterator0_t1672915424::get_offset_of_U24disposing_3(),
	U3CGetExoticInfoU3Ec__Iterator0_t1672915424::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (CommandCenterManager_t2415216723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[16] = 
{
	CommandCenterManager_t2415216723::get_offset_of_cityWorker_2(),
	CommandCenterManager_t2415216723::get_offset_of_citySpy_3(),
	CommandCenterManager_t2415216723::get_offset_of_citySwordsman_4(),
	CommandCenterManager_t2415216723::get_offset_of_citySpearman_5(),
	CommandCenterManager_t2415216723::get_offset_of_cityPikeman_6(),
	CommandCenterManager_t2415216723::get_offset_of_cityScoutRider_7(),
	CommandCenterManager_t2415216723::get_offset_of_cityLightCavalry_8(),
	CommandCenterManager_t2415216723::get_offset_of_cityHeavyCavalry_9(),
	CommandCenterManager_t2415216723::get_offset_of_cityArcher_10(),
	CommandCenterManager_t2415216723::get_offset_of_cityArcherRider_11(),
	CommandCenterManager_t2415216723::get_offset_of_cityHollyman_12(),
	CommandCenterManager_t2415216723::get_offset_of_cityWagon_13(),
	CommandCenterManager_t2415216723::get_offset_of_cityTrebuchet_14(),
	CommandCenterManager_t2415216723::get_offset_of_citySiegeTower_15(),
	CommandCenterManager_t2415216723::get_offset_of_cityBatteringRam_16(),
	CommandCenterManager_t2415216723::get_offset_of_cityBallista_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (ConfirmationContentManager_t3012879264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[2] = 
{
	ConfirmationContentManager_t3012879264::get_offset_of_confirmed_2(),
	ConfirmationContentManager_t3012879264::get_offset_of_confirmationMessage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (ConstructionDetailsManager_t275842940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[20] = 
{
	ConstructionDetailsManager_t275842940::get_offset_of_pitManager_2(),
	ConstructionDetailsManager_t275842940::get_offset_of_cityPanel_3(),
	ConstructionDetailsManager_t275842940::get_offset_of_fieldsPanel_4(),
	ConstructionDetailsManager_t275842940::get_offset_of_feastingHall_5(),
	ConstructionDetailsManager_t275842940::get_offset_of_market_6(),
	ConstructionDetailsManager_t275842940::get_offset_of_guestHouse_7(),
	ConstructionDetailsManager_t275842940::get_offset_of_storage_8(),
	ConstructionDetailsManager_t275842940::get_offset_of_furnace_9(),
	ConstructionDetailsManager_t275842940::get_offset_of_blackSmith_10(),
	ConstructionDetailsManager_t275842940::get_offset_of_hospital_11(),
	ConstructionDetailsManager_t275842940::get_offset_of_godHouse_12(),
	ConstructionDetailsManager_t275842940::get_offset_of_commandCenter_13(),
	ConstructionDetailsManager_t275842940::get_offset_of_diplomacy_14(),
	ConstructionDetailsManager_t275842940::get_offset_of_trainingGround_15(),
	ConstructionDetailsManager_t275842940::get_offset_of_sportArena_16(),
	ConstructionDetailsManager_t275842940::get_offset_of_univercity_17(),
	ConstructionDetailsManager_t275842940::get_offset_of_militarySchool_18(),
	ConstructionDetailsManager_t275842940::get_offset_of_archery_19(),
	ConstructionDetailsManager_t275842940::get_offset_of_stable_20(),
	ConstructionDetailsManager_t275842940::get_offset_of_engineering_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (ConvertReportManager_t1530780108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[31] = 
{
	ConvertReportManager_t1530780108::get_offset_of_attackerName_2(),
	ConvertReportManager_t1530780108::get_offset_of_attackerImage_3(),
	ConvertReportManager_t1530780108::get_offset_of_attackerRank_4(),
	ConvertReportManager_t1530780108::get_offset_of_attackerAllianceName_5(),
	ConvertReportManager_t1530780108::get_offset_of_attackerAlliancePosition_6(),
	ConvertReportManager_t1530780108::get_offset_of_attackerCityName_7(),
	ConvertReportManager_t1530780108::get_offset_of_attackerCityImage_8(),
	ConvertReportManager_t1530780108::get_offset_of_attackerExperiance_9(),
	ConvertReportManager_t1530780108::get_offset_of_attackerCityRegion_10(),
	ConvertReportManager_t1530780108::get_offset_of_defenderName_11(),
	ConvertReportManager_t1530780108::get_offset_of_defenderImage_12(),
	ConvertReportManager_t1530780108::get_offset_of_defenderRank_13(),
	ConvertReportManager_t1530780108::get_offset_of_defenderAllianceName_14(),
	ConvertReportManager_t1530780108::get_offset_of_defenderAlliancePosition_15(),
	ConvertReportManager_t1530780108::get_offset_of_defenderCityName_16(),
	ConvertReportManager_t1530780108::get_offset_of_defenderCityImage_17(),
	ConvertReportManager_t1530780108::get_offset_of_defenderExperiance_18(),
	ConvertReportManager_t1530780108::get_offset_of_defenderCityRegion_19(),
	ConvertReportManager_t1530780108::get_offset_of_defenderCityHappiness_20(),
	ConvertReportManager_t1530780108::get_offset_of_defenderCityCourage_21(),
	ConvertReportManager_t1530780108::get_offset_of_attackerResult_22(),
	ConvertReportManager_t1530780108::get_offset_of_defenderResult_23(),
	ConvertReportManager_t1530780108::get_offset_of_report_24(),
	ConvertReportManager_t1530780108::get_offset_of_attackerUnitsController_25(),
	ConvertReportManager_t1530780108::get_offset_of_defenderUnitsController_26(),
	ConvertReportManager_t1530780108::get_offset_of_attackerUnitNames_27(),
	ConvertReportManager_t1530780108::get_offset_of_attackerBeforeCount_28(),
	ConvertReportManager_t1530780108::get_offset_of_attackerAfterCount_29(),
	ConvertReportManager_t1530780108::get_offset_of_defenderUnitNames_30(),
	ConvertReportManager_t1530780108::get_offset_of_defenderBeforeCount_31(),
	ConvertReportManager_t1530780108::get_offset_of_defenderAfterCount_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (DiplomacyContentManager_t1332674241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[1] = 
{
	DiplomacyContentManager_t1332674241::get_offset_of_allowTroopsTGL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (DismissContentManager_t3416937869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[33] = 
{
	DismissContentManager_t3416937869::get_offset_of_cityWorker_2(),
	DismissContentManager_t3416937869::get_offset_of_citySpy_3(),
	DismissContentManager_t3416937869::get_offset_of_citySwordsman_4(),
	DismissContentManager_t3416937869::get_offset_of_citySpearman_5(),
	DismissContentManager_t3416937869::get_offset_of_cityPikeman_6(),
	DismissContentManager_t3416937869::get_offset_of_cityScoutRider_7(),
	DismissContentManager_t3416937869::get_offset_of_cityLightCavalry_8(),
	DismissContentManager_t3416937869::get_offset_of_cityHeavyCavalry_9(),
	DismissContentManager_t3416937869::get_offset_of_cityArcher_10(),
	DismissContentManager_t3416937869::get_offset_of_cityArcherRider_11(),
	DismissContentManager_t3416937869::get_offset_of_cityHollyman_12(),
	DismissContentManager_t3416937869::get_offset_of_cityWagon_13(),
	DismissContentManager_t3416937869::get_offset_of_cityTrebuchet_14(),
	DismissContentManager_t3416937869::get_offset_of_citySiegeTower_15(),
	DismissContentManager_t3416937869::get_offset_of_cityBatteringRam_16(),
	DismissContentManager_t3416937869::get_offset_of_cityBallista_17(),
	DismissContentManager_t3416937869::get_offset_of_dismissWorker_18(),
	DismissContentManager_t3416937869::get_offset_of_dismissSpy_19(),
	DismissContentManager_t3416937869::get_offset_of_dismissSwordsman_20(),
	DismissContentManager_t3416937869::get_offset_of_dismissSpearman_21(),
	DismissContentManager_t3416937869::get_offset_of_dismissPikeman_22(),
	DismissContentManager_t3416937869::get_offset_of_dismissScoutRider_23(),
	DismissContentManager_t3416937869::get_offset_of_dismissLightCavalry_24(),
	DismissContentManager_t3416937869::get_offset_of_dismissHeavyCavalry_25(),
	DismissContentManager_t3416937869::get_offset_of_dismissArcher_26(),
	DismissContentManager_t3416937869::get_offset_of_dismissArcherRider_27(),
	DismissContentManager_t3416937869::get_offset_of_dismissHollyman_28(),
	DismissContentManager_t3416937869::get_offset_of_dismissWagon_29(),
	DismissContentManager_t3416937869::get_offset_of_dismissTrebuchet_30(),
	DismissContentManager_t3416937869::get_offset_of_dismissSiegeTower_31(),
	DismissContentManager_t3416937869::get_offset_of_dismissBatteringRam_32(),
	DismissContentManager_t3416937869::get_offset_of_dismissBallista_33(),
	DismissContentManager_t3416937869::get_offset_of_units_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (DispatchManager_t670234083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3322[83] = 
{
	DispatchManager_t670234083::get_offset_of_Page1_2(),
	DispatchManager_t670234083::get_offset_of_Page2_3(),
	DispatchManager_t670234083::get_offset_of_cityFood_4(),
	DispatchManager_t670234083::get_offset_of_cityWood_5(),
	DispatchManager_t670234083::get_offset_of_cityIron_6(),
	DispatchManager_t670234083::get_offset_of_cityStone_7(),
	DispatchManager_t670234083::get_offset_of_cityCopper_8(),
	DispatchManager_t670234083::get_offset_of_citySilver_9(),
	DispatchManager_t670234083::get_offset_of_cityGold_10(),
	DispatchManager_t670234083::get_offset_of_cityWorker_11(),
	DispatchManager_t670234083::get_offset_of_citySpy_12(),
	DispatchManager_t670234083::get_offset_of_citySwordsman_13(),
	DispatchManager_t670234083::get_offset_of_citySpearman_14(),
	DispatchManager_t670234083::get_offset_of_cityPikeman_15(),
	DispatchManager_t670234083::get_offset_of_cityScoutRider_16(),
	DispatchManager_t670234083::get_offset_of_cityLightCavalry_17(),
	DispatchManager_t670234083::get_offset_of_cityHeavyCavalry_18(),
	DispatchManager_t670234083::get_offset_of_cityArcher_19(),
	DispatchManager_t670234083::get_offset_of_cityArcherRider_20(),
	DispatchManager_t670234083::get_offset_of_cityHollyman_21(),
	DispatchManager_t670234083::get_offset_of_cityWagon_22(),
	DispatchManager_t670234083::get_offset_of_cityTrebuchet_23(),
	DispatchManager_t670234083::get_offset_of_citySiegeTower_24(),
	DispatchManager_t670234083::get_offset_of_cityBatteringRam_25(),
	DispatchManager_t670234083::get_offset_of_cityBallista_26(),
	DispatchManager_t670234083::get_offset_of_cityWorkerName_27(),
	DispatchManager_t670234083::get_offset_of_citySpyName_28(),
	DispatchManager_t670234083::get_offset_of_citySwordsmanName_29(),
	DispatchManager_t670234083::get_offset_of_citySpearmanName_30(),
	DispatchManager_t670234083::get_offset_of_cityPikemanName_31(),
	DispatchManager_t670234083::get_offset_of_cityScoutRiderName_32(),
	DispatchManager_t670234083::get_offset_of_cityLightCavalryName_33(),
	DispatchManager_t670234083::get_offset_of_cityHeavyCavalryName_34(),
	DispatchManager_t670234083::get_offset_of_cityArcherName_35(),
	DispatchManager_t670234083::get_offset_of_cityArcherRiderName_36(),
	DispatchManager_t670234083::get_offset_of_cityHollymanName_37(),
	DispatchManager_t670234083::get_offset_of_cityWagonName_38(),
	DispatchManager_t670234083::get_offset_of_cityTrebuchetName_39(),
	DispatchManager_t670234083::get_offset_of_citySiegeTowerName_40(),
	DispatchManager_t670234083::get_offset_of_cityBatteringRamName_41(),
	DispatchManager_t670234083::get_offset_of_cityBallistaName_42(),
	DispatchManager_t670234083::get_offset_of_dispatchFood_43(),
	DispatchManager_t670234083::get_offset_of_dispatchWood_44(),
	DispatchManager_t670234083::get_offset_of_dispatchIron_45(),
	DispatchManager_t670234083::get_offset_of_dispatchStone_46(),
	DispatchManager_t670234083::get_offset_of_dispatchCopper_47(),
	DispatchManager_t670234083::get_offset_of_dispatchSilver_48(),
	DispatchManager_t670234083::get_offset_of_dispatchGold_49(),
	DispatchManager_t670234083::get_offset_of_dispatchWorker_50(),
	DispatchManager_t670234083::get_offset_of_dispatchSpy_51(),
	DispatchManager_t670234083::get_offset_of_dispatchSwordsman_52(),
	DispatchManager_t670234083::get_offset_of_dispatchSpearman_53(),
	DispatchManager_t670234083::get_offset_of_dispatchPikeman_54(),
	DispatchManager_t670234083::get_offset_of_dispatchScoutRider_55(),
	DispatchManager_t670234083::get_offset_of_dispatchLightCavalry_56(),
	DispatchManager_t670234083::get_offset_of_dispatchHeavyCavalry_57(),
	DispatchManager_t670234083::get_offset_of_dispatchArcher_58(),
	DispatchManager_t670234083::get_offset_of_dispatchArcherRider_59(),
	DispatchManager_t670234083::get_offset_of_dispatchHollyman_60(),
	DispatchManager_t670234083::get_offset_of_dispatchWagon_61(),
	DispatchManager_t670234083::get_offset_of_dispatchTrebuchet_62(),
	DispatchManager_t670234083::get_offset_of_dispatchSiegeTower_63(),
	DispatchManager_t670234083::get_offset_of_dispatchBatteringRam_64(),
	DispatchManager_t670234083::get_offset_of_dispatchBallista_65(),
	DispatchManager_t670234083::get_offset_of_attackType_66(),
	DispatchManager_t670234083::get_offset_of_knights_67(),
	DispatchManager_t670234083::get_offset_of_dispatchHour_68(),
	DispatchManager_t670234083::get_offset_of_dispatchMinute_69(),
	DispatchManager_t670234083::get_offset_of_xCoord_70(),
	DispatchManager_t670234083::get_offset_of_yCoord_71(),
	DispatchManager_t670234083::get_offset_of_xInput_72(),
	DispatchManager_t670234083::get_offset_of_yInput_73(),
	DispatchManager_t670234083::get_offset_of_foodPrice_74(),
	DispatchManager_t670234083::get_offset_of_loadVacancy_75(),
	DispatchManager_t670234083::get_offset_of_tripTime_76(),
	DispatchManager_t670234083::get_offset_of_fromMap_77(),
	DispatchManager_t670234083::get_offset_of_targetField_78(),
	DispatchManager_t670234083::get_offset_of_generals_79(),
	DispatchManager_t670234083::get_offset_of_army_80(),
	DispatchManager_t670234083::get_offset_of_resources_81(),
	DispatchManager_t670234083::get_offset_of_calculated_82(),
	DispatchManager_t670234083::get_offset_of_onGotParams_83(),
	DispatchManager_t670234083::get_offset_of_attackTypes_84(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (U3CGetAttackParamsU3Ec__Iterator0_t1449406293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[12] = 
{
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U3CformU3E__0_0(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_attcakType_1(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_army_2(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_resources_3(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_destX_4(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_destY_5(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_knightId_6(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U3CrequestU3E__0_7(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U24this_8(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U24current_9(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U24disposing_10(),
	U3CGetAttackParamsU3Ec__Iterator0_t1449406293::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (FinancialContentChanger_t4019109810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[15] = 
{
	FinancialContentChanger_t4019109810::get_offset_of_contentLabel_2(),
	FinancialContentChanger_t4019109810::get_offset_of_shillingsCount_3(),
	FinancialContentChanger_t4019109810::get_offset_of_totalPurchases_4(),
	FinancialContentChanger_t4019109810::get_offset_of_totalLabel_5(),
	FinancialContentChanger_t4019109810::get_offset_of_tableColumn_6(),
	FinancialContentChanger_t4019109810::get_offset_of_tabSelectedSprite_7(),
	FinancialContentChanger_t4019109810::get_offset_of_tabUnselectedSprite_8(),
	FinancialContentChanger_t4019109810::get_offset_of_tabs_9(),
	FinancialContentChanger_t4019109810::get_offset_of_boughtItems_10(),
	FinancialContentChanger_t4019109810::get_offset_of_soldItems_11(),
	FinancialContentChanger_t4019109810::get_offset_of_offeredItems_12(),
	FinancialContentChanger_t4019109810::get_offset_of_gotOffers_13(),
	FinancialContentChanger_t4019109810::get_offset_of_offerCanceled_14(),
	FinancialContentChanger_t4019109810::get_offset_of_tableController_15(),
	FinancialContentChanger_t4019109810::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (U3CGetFinanceReportsU3Ec__Iterator0_t4010566809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[5] = 
{
	U3CGetFinanceReportsU3Ec__Iterator0_t4010566809::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetFinanceReportsU3Ec__Iterator0_t4010566809::get_offset_of_U24this_1(),
	U3CGetFinanceReportsU3Ec__Iterator0_t4010566809::get_offset_of_U24current_2(),
	U3CGetFinanceReportsU3Ec__Iterator0_t4010566809::get_offset_of_U24disposing_3(),
	U3CGetFinanceReportsU3Ec__Iterator0_t4010566809::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (U3CGetFinanceOffersU3Ec__Iterator1_t3666753709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[5] = 
{
	U3CGetFinanceOffersU3Ec__Iterator1_t3666753709::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetFinanceOffersU3Ec__Iterator1_t3666753709::get_offset_of_U24this_1(),
	U3CGetFinanceOffersU3Ec__Iterator1_t3666753709::get_offset_of_U24current_2(),
	U3CGetFinanceOffersU3Ec__Iterator1_t3666753709::get_offset_of_U24disposing_3(),
	U3CGetFinanceOffersU3Ec__Iterator1_t3666753709::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (U3CCancelOfferRequestU3Ec__Iterator2_t3904290043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[7] = 
{
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U3CformU3E__0_0(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_id_1(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U24this_3(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U24current_4(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U24disposing_5(),
	U3CCancelOfferRequestU3Ec__Iterator2_t3904290043::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (GenericBuildingInfoContentManager_t1427092153), -1, sizeof(GenericBuildingInfoContentManager_t1427092153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3328[7] = 
{
	GenericBuildingInfoContentManager_t1427092153::get_offset_of_buildingName_2(),
	GenericBuildingInfoContentManager_t1427092153::get_offset_of_buildingLevel_3(),
	GenericBuildingInfoContentManager_t1427092153::get_offset_of_buildingDescription_4(),
	GenericBuildingInfoContentManager_t1427092153::get_offset_of_demolishBtn_5(),
	GenericBuildingInfoContentManager_t1427092153_StaticFields::get_offset_of_type_6(),
	GenericBuildingInfoContentManager_t1427092153_StaticFields::get_offset_of_pitId_7(),
	GenericBuildingInfoContentManager_t1427092153::get_offset_of_confirmed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (HelpContentChanger_t1793113890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[12] = 
{
	HelpContentChanger_t1793113890::get_offset_of_homeContent_2(),
	HelpContentChanger_t1793113890::get_offset_of_unitsContent_3(),
	HelpContentChanger_t1793113890::get_offset_of_hierarchyContent_4(),
	HelpContentChanger_t1793113890::get_offset_of_promotionContent_5(),
	HelpContentChanger_t1793113890::get_offset_of_engSteps_6(),
	HelpContentChanger_t1793113890::get_offset_of_arSteps_7(),
	HelpContentChanger_t1793113890::get_offset_of_homeButtonImage_8(),
	HelpContentChanger_t1793113890::get_offset_of_unitsButtonImage_9(),
	HelpContentChanger_t1793113890::get_offset_of_hierarchyButtonImage_10(),
	HelpContentChanger_t1793113890::get_offset_of_promotionButtonImage_11(),
	HelpContentChanger_t1793113890::get_offset_of_tabSelected_12(),
	HelpContentChanger_t1793113890::get_offset_of_tabUnselected_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (KnightInfoContentManager_t331339578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[16] = 
{
	KnightInfoContentManager_t331339578::get_offset_of_knightName_2(),
	KnightInfoContentManager_t331339578::get_offset_of_knightPoint_3(),
	KnightInfoContentManager_t331339578::get_offset_of_knightMarches_4(),
	KnightInfoContentManager_t331339578::get_offset_of_knightPolitics_5(),
	KnightInfoContentManager_t331339578::get_offset_of_knightSpeed_6(),
	KnightInfoContentManager_t331339578::get_offset_of_knightLevel_7(),
	KnightInfoContentManager_t331339578::get_offset_of_knightLoyalty_8(),
	KnightInfoContentManager_t331339578::get_offset_of_knightSalary_9(),
	KnightInfoContentManager_t331339578::get_offset_of_general_10(),
	KnightInfoContentManager_t331339578::get_offset_of_increaseMarches_11(),
	KnightInfoContentManager_t331339578::get_offset_of_increasePolitics_12(),
	KnightInfoContentManager_t331339578::get_offset_of_increaseSpeed_13(),
	KnightInfoContentManager_t331339578::get_offset_of_increaseSalary_14(),
	KnightInfoContentManager_t331339578::get_offset_of_increaseLoyalty_15(),
	KnightInfoContentManager_t331339578::get_offset_of_increaseLevel_16(),
	KnightInfoContentManager_t331339578::get_offset_of_decreaseExperience_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (LanguageChangerContentManager_t2232597662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[2] = 
{
	LanguageChangerContentManager_t2232597662::get_offset_of_language_2(),
	LanguageChangerContentManager_t2232597662::get_offset_of_previousValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (LoginProgressbarContentManager_t4082662398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[4] = 
{
	LoginProgressbarContentManager_t4082662398::get_offset_of_progressTransform_2(),
	LoginProgressbarContentManager_t4082662398::get_offset_of_loginManager_3(),
	LoginProgressbarContentManager_t4082662398::get_offset_of_loadCount_4(),
	LoginProgressbarContentManager_t4082662398::get_offset_of_step_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (LoginRegistrationContentManager_t2754536803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[17] = 
{
	LoginRegistrationContentManager_t2754536803::get_offset_of_loginContent_2(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registrationContent_3(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_forgotPasswordContent_4(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_resetPasswordContent_5(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_loginUsername_6(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_loginPassword_7(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerUsername_8(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerPassword_9(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerConfirmPassword_10(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerEmail_11(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerEmperors_12(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_registerLanguage_13(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_forgotPasswordEmail_14(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_resetSecretKey_15(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_resetNewPassword_16(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_directLoginButton_17(),
	LoginRegistrationContentManager_t2754536803::get_offset_of_emperorsList_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (MailContentChange_t3691412170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[8] = 
{
	MailContentChange_t3691412170::get_offset_of_inboxPanel_2(),
	MailContentChange_t3691412170::get_offset_of_reportsPanel_3(),
	MailContentChange_t3691412170::get_offset_of_systemPanel_4(),
	MailContentChange_t3691412170::get_offset_of_invitePanel_5(),
	MailContentChange_t3691412170::get_offset_of_panelName_6(),
	MailContentChange_t3691412170::get_offset_of_tabs_7(),
	MailContentChange_t3691412170::get_offset_of_tabSelectedSprite_8(),
	MailContentChange_t3691412170::get_offset_of_tabUnselectedSprite_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (MarketManager_t880414981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[21] = 
{
	MarketManager_t880414981::get_offset_of_goldValue_2(),
	MarketManager_t880414981::get_offset_of_silverValue_3(),
	MarketManager_t880414981::get_offset_of_resourceLabel_4(),
	MarketManager_t880414981::get_offset_of_buyCount_5(),
	MarketManager_t880414981::get_offset_of_sellCount_6(),
	MarketManager_t880414981::get_offset_of_sellUnitPrice_7(),
	MarketManager_t880414981::get_offset_of_buyUnitPrice_8(),
	MarketManager_t880414981::get_offset_of_buyComission_9(),
	MarketManager_t880414981::get_offset_of_buyTotal_10(),
	MarketManager_t880414981::get_offset_of_sellComission_11(),
	MarketManager_t880414981::get_offset_of_sellTotal_12(),
	MarketManager_t880414981::get_offset_of_buyTable_13(),
	MarketManager_t880414981::get_offset_of_sellTable_14(),
	MarketManager_t880414981::get_offset_of_onBuyLotsUpdated_15(),
	MarketManager_t880414981::get_offset_of_onSellLotsUpdated_16(),
	MarketManager_t880414981::get_offset_of_onResourceBought_17(),
	MarketManager_t880414981::get_offset_of_onResourceSold_18(),
	MarketManager_t880414981::get_offset_of_onLotRetracted_19(),
	MarketManager_t880414981::get_offset_of_lotsToBuy_20(),
	MarketManager_t880414981::get_offset_of_lotsToSell_21(),
	MarketManager_t880414981::get_offset_of_currentResource_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (U3CGetLotsToBuyU3Ec__Iterator0_t2177983711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[5] = 
{
	U3CGetLotsToBuyU3Ec__Iterator0_t2177983711::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetLotsToBuyU3Ec__Iterator0_t2177983711::get_offset_of_U24this_1(),
	U3CGetLotsToBuyU3Ec__Iterator0_t2177983711::get_offset_of_U24current_2(),
	U3CGetLotsToBuyU3Ec__Iterator0_t2177983711::get_offset_of_U24disposing_3(),
	U3CGetLotsToBuyU3Ec__Iterator0_t2177983711::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (U3CGetLotsToSellU3Ec__Iterator1_t1027051147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[5] = 
{
	U3CGetLotsToSellU3Ec__Iterator1_t1027051147::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetLotsToSellU3Ec__Iterator1_t1027051147::get_offset_of_U24this_1(),
	U3CGetLotsToSellU3Ec__Iterator1_t1027051147::get_offset_of_U24current_2(),
	U3CGetLotsToSellU3Ec__Iterator1_t1027051147::get_offset_of_U24disposing_3(),
	U3CGetLotsToSellU3Ec__Iterator1_t1027051147::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (U3CAddLotU3Ec__Iterator2_t3555095447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[8] = 
{
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U3CaddFormU3E__0_0(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_resourceCount_1(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_unitPrice_2(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U3CrequestU3E__0_3(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U24this_4(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U24current_5(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U24disposing_6(),
	U3CAddLotU3Ec__Iterator2_t3555095447::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (U3CRemoveLotsU3Ec__Iterator3_t2683563112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[7] = 
{
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U3CremoveFormU3E__0_0(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_lotId_1(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U3CrequestU3E__0_2(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U24this_3(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U24current_4(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U24disposing_5(),
	U3CRemoveLotsU3Ec__Iterator3_t2683563112::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (U3CBuyLotsU3Ec__Iterator4_t1124615586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3340[7] = 
{
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U3CbuyFormU3E__0_0(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_lotsDict_1(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U3CrequestU3E__0_2(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U24this_3(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U24current_4(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U24disposing_5(),
	U3CBuyLotsU3Ec__Iterator4_t1124615586::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (MayorResidenceOverviewContentManager_t259933799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[17] = 
{
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityName_2(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityRegion_3(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityCarrage_4(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityHappiness_5(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityPopulation_6(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityIdlePopulation_7(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityTax_8(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_playerWins_9(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_barbarianWins_10(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_playerLoses_11(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_barbarianLoses_12(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_taxDropdown_13(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_taxIndex_14(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityNameInput_15(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_cityIcon_16(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_iconIndex_17(),
	MayorResidenceOverviewContentManager_t259933799::get_offset_of_userImageName_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (MessageAllianceManager_t646576267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[3] = 
{
	MessageAllianceManager_t646576267::get_offset_of_subject_2(),
	MessageAllianceManager_t646576267::get_offset_of_message_3(),
	MessageAllianceManager_t646576267::get_offset_of_userId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (MessageContentManager_t985817454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[21] = 
{
	MessageContentManager_t985817454::get_offset_of_readContent_2(),
	MessageContentManager_t985817454::get_offset_of_replyContent_3(),
	MessageContentManager_t985817454::get_offset_of_username_4(),
	MessageContentManager_t985817454::get_offset_of_userIcon_5(),
	MessageContentManager_t985817454::get_offset_of_userRank_6(),
	MessageContentManager_t985817454::get_offset_of_userAlliance_7(),
	MessageContentManager_t985817454::get_offset_of_userPosition_8(),
	MessageContentManager_t985817454::get_offset_of_userExperiance_9(),
	MessageContentManager_t985817454::get_offset_of_messageSubject_10(),
	MessageContentManager_t985817454::get_offset_of_messageText_11(),
	MessageContentManager_t985817454::get_offset_of_messageDate_12(),
	MessageContentManager_t985817454::get_offset_of_initialSubject_13(),
	MessageContentManager_t985817454::get_offset_of_initialMessage_14(),
	MessageContentManager_t985817454::get_offset_of_replySubject_15(),
	MessageContentManager_t985817454::get_offset_of_replyMessage_16(),
	MessageContentManager_t985817454::get_offset_of_messageId_17(),
	MessageContentManager_t985817454::get_offset_of_reply_18(),
	MessageContentManager_t985817454::get_offset_of_message_19(),
	MessageContentManager_t985817454::get_offset_of_ownerTable_20(),
	MessageContentManager_t985817454::get_offset_of_dateFormat_21(),
	MessageContentManager_t985817454::get_offset_of_gotMessageDetails_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[5] = 
{
	U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435::get_offset_of_U24this_1(),
	U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435::get_offset_of_U24current_2(),
	U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435::get_offset_of_U24disposing_3(),
	U3CGetMesssageDetailsU3Ec__Iterator0_t1899079435::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (NewAllianceContentManager_t2496725915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[1] = 
{
	NewAllianceContentManager_t2496725915::get_offset_of_allianceName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (NewMailContentManager_t1552843396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[2] = 
{
	NewMailContentManager_t1552843396::get_offset_of_subject_2(),
	NewMailContentManager_t1552843396::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (NotificationWindowManager_t3101019839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[1] = 
{
	NotificationWindowManager_t3101019839::get_offset_of_notificationText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (OverviewManager_t3338185744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[15] = 
{
	OverviewManager_t3338185744::get_offset_of_lordPanel_2(),
	OverviewManager_t3338185744::get_offset_of_overviewPanel_3(),
	OverviewManager_t3338185744::get_offset_of_playerImage_4(),
	OverviewManager_t3338185744::get_offset_of_playerName_5(),
	OverviewManager_t3338185744::get_offset_of_playerRank_6(),
	OverviewManager_t3338185744::get_offset_of_playerAlliance_7(),
	OverviewManager_t3338185744::get_offset_of_playerExperience_8(),
	OverviewManager_t3338185744::get_offset_of_cityName_9(),
	OverviewManager_t3338185744::get_offset_of_cityRegion_10(),
	OverviewManager_t3338185744::get_offset_of_cityCarrage_11(),
	OverviewManager_t3338185744::get_offset_of_cityHappiness_12(),
	OverviewManager_t3338185744::get_offset_of_cityPopulation_13(),
	OverviewManager_t3338185744::get_offset_of_cityIdlePopulation_14(),
	OverviewManager_t3338185744::get_offset_of_cityTax_15(),
	OverviewManager_t3338185744::get_offset_of_userImageName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (ReinforceTransportReportManager_t2386156816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[29] = 
{
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerName_2(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerImage_3(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerRank_4(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerAllianceName_5(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerAlliancePosition_6(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerCityName_7(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerCityImage_8(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerExperiance_9(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerCityRegion_10(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderName_11(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderImage_12(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderRank_13(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderAllianceName_14(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderAlliancePosition_15(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderCityName_16(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderCityImage_17(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderExperiance_18(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderCityRegion_19(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderCityHappiness_20(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderCityCourage_21(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_report_22(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerUnitsController_23(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerResourcesController_24(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerUnitNames_25(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerBeforeCount_26(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_attackerAfterCount_27(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderUnitNames_28(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderBeforeCount_29(),
	ReinforceTransportReportManager_t2386156816::get_offset_of_defenderAfterCount_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (ResidenceContentChanger_t4286680255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[10] = 
{
	ResidenceContentChanger_t4286680255::get_offset_of_contentLabel_2(),
	ResidenceContentChanger_t4286680255::get_offset_of_cityName_3(),
	ResidenceContentChanger_t4286680255::get_offset_of_cityNameInput_4(),
	ResidenceContentChanger_t4286680255::get_offset_of_cityIcon_5(),
	ResidenceContentChanger_t4286680255::get_offset_of_buildingsContent_6(),
	ResidenceContentChanger_t4286680255::get_offset_of_productionContent_7(),
	ResidenceContentChanger_t4286680255::get_offset_of_citiesContent_8(),
	ResidenceContentChanger_t4286680255::get_offset_of_valleysContent_9(),
	ResidenceContentChanger_t4286680255::get_offset_of_coloniesContent_10(),
	ResidenceContentChanger_t4286680255::get_offset_of_iconIndex_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (ResidenceProductionContentManager_t3650353576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[8] = 
{
	ResidenceProductionContentManager_t3650353576::get_offset_of_laborForces_2(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_basicProduction_3(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_coloniesTributes_4(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_valleysProduction_5(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_mayorPlus_6(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_tributesPaid_7(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_troopUpkeep_8(),
	ResidenceProductionContentManager_t3650353576::get_offset_of_netProduction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (U3CGetCityProductionU3Ec__Iterator0_t3498229313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[5] = 
{
	U3CGetCityProductionU3Ec__Iterator0_t3498229313::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetCityProductionU3Ec__Iterator0_t3498229313::get_offset_of_U24this_1(),
	U3CGetCityProductionU3Ec__Iterator0_t3498229313::get_offset_of_U24current_2(),
	U3CGetCityProductionU3Ec__Iterator0_t3498229313::get_offset_of_U24disposing_3(),
	U3CGetCityProductionU3Ec__Iterator0_t3498229313::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (ResourcesManager_t1031245130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[7] = 
{
	ResourcesManager_t1031245130::get_offset_of_cityFood_2(),
	ResourcesManager_t1031245130::get_offset_of_cityWood_3(),
	ResourcesManager_t1031245130::get_offset_of_cityIron_4(),
	ResourcesManager_t1031245130::get_offset_of_cityStone_5(),
	ResourcesManager_t1031245130::get_offset_of_cityCopper_6(),
	ResourcesManager_t1031245130::get_offset_of_citySilver_7(),
	ResourcesManager_t1031245130::get_offset_of_cityGold_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (ScoutReportManager_t3306641401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[33] = 
{
	ScoutReportManager_t3306641401::get_offset_of_attackerName_2(),
	ScoutReportManager_t3306641401::get_offset_of_attackerImage_3(),
	ScoutReportManager_t3306641401::get_offset_of_attackerRank_4(),
	ScoutReportManager_t3306641401::get_offset_of_attackerAllianceName_5(),
	ScoutReportManager_t3306641401::get_offset_of_attackerAlliancePosition_6(),
	ScoutReportManager_t3306641401::get_offset_of_attackerCityName_7(),
	ScoutReportManager_t3306641401::get_offset_of_attackerCityImage_8(),
	ScoutReportManager_t3306641401::get_offset_of_attackerExperiance_9(),
	ScoutReportManager_t3306641401::get_offset_of_attackerCityRegion_10(),
	ScoutReportManager_t3306641401::get_offset_of_defenderName_11(),
	ScoutReportManager_t3306641401::get_offset_of_defenderImage_12(),
	ScoutReportManager_t3306641401::get_offset_of_defenderRank_13(),
	ScoutReportManager_t3306641401::get_offset_of_defenderAllianceName_14(),
	ScoutReportManager_t3306641401::get_offset_of_defenderAlliancePosition_15(),
	ScoutReportManager_t3306641401::get_offset_of_defenderCityName_16(),
	ScoutReportManager_t3306641401::get_offset_of_defenderCityImage_17(),
	ScoutReportManager_t3306641401::get_offset_of_defenderExperiance_18(),
	ScoutReportManager_t3306641401::get_offset_of_defenderCityRegion_19(),
	ScoutReportManager_t3306641401::get_offset_of_defenderCityHappiness_20(),
	ScoutReportManager_t3306641401::get_offset_of_defenderCityCourage_21(),
	ScoutReportManager_t3306641401::get_offset_of_attackerResult_22(),
	ScoutReportManager_t3306641401::get_offset_of_defenderResult_23(),
	ScoutReportManager_t3306641401::get_offset_of_report_24(),
	ScoutReportManager_t3306641401::get_offset_of_attackerUnitsController_25(),
	ScoutReportManager_t3306641401::get_offset_of_defenderUnitsController_26(),
	ScoutReportManager_t3306641401::get_offset_of_defenderResourcesController_27(),
	ScoutReportManager_t3306641401::get_offset_of_defendersItemsController_28(),
	ScoutReportManager_t3306641401::get_offset_of_attackerUnitNames_29(),
	ScoutReportManager_t3306641401::get_offset_of_attackerBeforeCount_30(),
	ScoutReportManager_t3306641401::get_offset_of_attackerAfterCount_31(),
	ScoutReportManager_t3306641401::get_offset_of_defenderUnitNames_32(),
	ScoutReportManager_t3306641401::get_offset_of_defenderBeforeCount_33(),
	ScoutReportManager_t3306641401::get_offset_of_defenderAfterCount_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (SellItemContentManager_t2167318587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[9] = 
{
	SellItemContentManager_t2167318587::get_offset_of_itemName_2(),
	SellItemContentManager_t2167318587::get_offset_of_itemsOwned_3(),
	SellItemContentManager_t2167318587::get_offset_of_itemImage_4(),
	SellItemContentManager_t2167318587::get_offset_of_discount_5(),
	SellItemContentManager_t2167318587::get_offset_of_quantity_6(),
	SellItemContentManager_t2167318587::get_offset_of_owner_7(),
	SellItemContentManager_t2167318587::get_offset_of_itemcount_8(),
	SellItemContentManager_t2167318587::get_offset_of_itemId_9(),
	SellItemContentManager_t2167318587::get_offset_of_toSellCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (SettingsContentManager_t1587921867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[8] = 
{
	SettingsContentManager_t1587921867::get_offset_of_currentUserNick_2(),
	SettingsContentManager_t1587921867::get_offset_of_currentUserEmail_3(),
	SettingsContentManager_t1587921867::get_offset_of_currentUserMobile_4(),
	SettingsContentManager_t1587921867::get_offset_of_currentUserFlag_5(),
	SettingsContentManager_t1587921867::get_offset_of_currentUserImage_6(),
	SettingsContentManager_t1587921867::get_offset_of_languageChanger_7(),
	SettingsContentManager_t1587921867::get_offset_of_confirmed_8(),
	SettingsContentManager_t1587921867::get_offset_of_user_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (ShellContentManager_t476539759), -1, sizeof(ShellContentManager_t476539759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3357[59] = 
{
	ShellContentManager_t476539759::get_offset_of_icon_2(),
	ShellContentManager_t476539759::get_offset_of_windowName_3(),
	ShellContentManager_t476539759::get_offset_of_content_4(),
	ShellContentManager_t476539759::get_offset_of_tabSelectedTop_5(),
	ShellContentManager_t476539759::get_offset_of_tabSelectedBottom_6(),
	ShellContentManager_t476539759::get_offset_of_tabUnselectedTop_7(),
	ShellContentManager_t476539759::get_offset_of_tabUnselectedBottom_8(),
	ShellContentManager_t476539759::get_offset_of_moreButton_9(),
	ShellContentManager_t476539759::get_offset_of_moreLeftArrow_10(),
	ShellContentManager_t476539759::get_offset_of_moreRightArrow_11(),
	ShellContentManager_t476539759::get_offset_of_tabs_12(),
	ShellContentManager_t476539759::get_offset_of_contents_13(),
	ShellContentManager_t476539759::get_offset_of_selectedTabIndex_14(),
	ShellContentManager_t476539759::get_offset_of_keys_15(),
	ShellContentManager_t476539759::get_offset_of_icons_16(),
	ShellContentManager_t476539759::get_offset_of_tabsNames_17(),
	ShellContentManager_t476539759::get_offset_of_actionTabsNames_18(),
	ShellContentManager_t476539759::get_offset_of_shopTabsNames_19(),
	ShellContentManager_t476539759::get_offset_of_treasureTabsNames_20(),
	ShellContentManager_t476539759::get_offset_of_allianceTabsNames_21(),
	ShellContentManager_t476539759::get_offset_of_mailTabsNames_22(),
	ShellContentManager_t476539759::get_offset_of_chatTabsNames_23(),
	ShellContentManager_t476539759::get_offset_of_genericBuildingTabsNames_24(),
	ShellContentManager_t476539759::get_offset_of_commandCenterTabsNames_25(),
	ShellContentManager_t476539759::get_offset_of_archeryTabsNames_26(),
	ShellContentManager_t476539759::get_offset_of_enginneringTabsNames_27(),
	ShellContentManager_t476539759::get_offset_of_godHouseTabsNames_28(),
	ShellContentManager_t476539759::get_offset_of_militarySchoolTabsNames_29(),
	ShellContentManager_t476539759::get_offset_of_stableTabsNames_30(),
	ShellContentManager_t476539759::get_offset_of_dispatchTabsNames_31(),
	ShellContentManager_t476539759::get_offset_of_residenceTabsNames_32(),
	ShellContentManager_t476539759::get_offset_of_guestHouseTabsNames_33(),
	ShellContentManager_t476539759::get_offset_of_feastingHallTabsNames_34(),
	ShellContentManager_t476539759::get_offset_of_marketTabsNames_35(),
	ShellContentManager_t476539759::get_offset_of_settingsTabsNames_36(),
	ShellContentManager_t476539759::get_offset_of_actionContents_37(),
	ShellContentManager_t476539759::get_offset_of_shopContents_38(),
	ShellContentManager_t476539759::get_offset_of_treasureContents_39(),
	ShellContentManager_t476539759::get_offset_of_allianceContents_40(),
	ShellContentManager_t476539759::get_offset_of_mailContents_41(),
	ShellContentManager_t476539759::get_offset_of_chatContents_42(),
	ShellContentManager_t476539759::get_offset_of_genericBuildingsContents_43(),
	ShellContentManager_t476539759::get_offset_of_commandCenterContents_44(),
	ShellContentManager_t476539759::get_offset_of_archeryContents_45(),
	ShellContentManager_t476539759::get_offset_of_enginneringContents_46(),
	ShellContentManager_t476539759::get_offset_of_godHouseContents_47(),
	ShellContentManager_t476539759::get_offset_of_militarySchoolContents_48(),
	ShellContentManager_t476539759::get_offset_of_stableContents_49(),
	ShellContentManager_t476539759::get_offset_of_dispatchContents_50(),
	ShellContentManager_t476539759::get_offset_of_residenceContents_51(),
	ShellContentManager_t476539759::get_offset_of_guestHouseContents_52(),
	ShellContentManager_t476539759::get_offset_of_feastingHallContents_53(),
	ShellContentManager_t476539759::get_offset_of_marketContents_54(),
	ShellContentManager_t476539759::get_offset_of_settingsContents_55(),
	ShellContentManager_t476539759::get_offset_of_contentChanger_56(),
	ShellContentManager_t476539759::get_offset_of_type_57(),
	ShellContentManager_t476539759::get_offset_of_pitId_58(),
	ShellContentManager_t476539759_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_59(),
	ShellContentManager_t476539759_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (ShieldTimeContentManager_t1076880368), -1, sizeof(ShieldTimeContentManager_t1076880368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3358[6] = 
{
	ShieldTimeContentManager_t1076880368::get_offset_of_hour_2(),
	ShieldTimeContentManager_t1076880368::get_offset_of_owner_3(),
	ShieldTimeContentManager_t1076880368::get_offset_of_itemId_4(),
	ShieldTimeContentManager_t1076880368::get_offset_of_timeLabel_5(),
	ShieldTimeContentManager_t1076880368::get_offset_of_timeValue_6(),
	ShieldTimeContentManager_t1076880368_StaticFields::get_offset_of_days_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (StatisticsManager_t1700503451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[31] = 
{
	StatisticsManager_t1700503451::get_offset_of_resourcesPanel_2(),
	StatisticsManager_t1700503451::get_offset_of_knightsPanel_3(),
	StatisticsManager_t1700503451::get_offset_of_unitsPanel_4(),
	StatisticsManager_t1700503451::get_offset_of_cityFood_5(),
	StatisticsManager_t1700503451::get_offset_of_cityWood_6(),
	StatisticsManager_t1700503451::get_offset_of_cityIron_7(),
	StatisticsManager_t1700503451::get_offset_of_cityStone_8(),
	StatisticsManager_t1700503451::get_offset_of_cityCopper_9(),
	StatisticsManager_t1700503451::get_offset_of_citySilver_10(),
	StatisticsManager_t1700503451::get_offset_of_cityGold_11(),
	StatisticsManager_t1700503451::get_offset_of_cityWorker_12(),
	StatisticsManager_t1700503451::get_offset_of_citySpy_13(),
	StatisticsManager_t1700503451::get_offset_of_citySwordsman_14(),
	StatisticsManager_t1700503451::get_offset_of_citySpearman_15(),
	StatisticsManager_t1700503451::get_offset_of_cityPikeman_16(),
	StatisticsManager_t1700503451::get_offset_of_cityScoutRider_17(),
	StatisticsManager_t1700503451::get_offset_of_cityLightCavalry_18(),
	StatisticsManager_t1700503451::get_offset_of_cityHeavyCavalry_19(),
	StatisticsManager_t1700503451::get_offset_of_cityArcher_20(),
	StatisticsManager_t1700503451::get_offset_of_cityArcherRider_21(),
	StatisticsManager_t1700503451::get_offset_of_cityHollyman_22(),
	StatisticsManager_t1700503451::get_offset_of_cityWagon_23(),
	StatisticsManager_t1700503451::get_offset_of_cityTrebuchet_24(),
	StatisticsManager_t1700503451::get_offset_of_citySiegeTower_25(),
	StatisticsManager_t1700503451::get_offset_of_cityBatteringRam_26(),
	StatisticsManager_t1700503451::get_offset_of_cityBallista_27(),
	StatisticsManager_t1700503451::get_offset_of_cityBeaconTower_28(),
	StatisticsManager_t1700503451::get_offset_of_cityArcherTower_29(),
	StatisticsManager_t1700503451::get_offset_of_cityHotOilHole_30(),
	StatisticsManager_t1700503451::get_offset_of_cityTrebuchetTower_31(),
	StatisticsManager_t1700503451::get_offset_of_cityBallistaTower_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (TrainingIconManager_t1144058280), -1, sizeof(TrainingIconManager_t1144058280_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3360[13] = 
{
	TrainingIconManager_t1144058280::get_offset_of_internalUnitName_2(),
	TrainingIconManager_t1144058280::get_offset_of_foodPrice_3(),
	TrainingIconManager_t1144058280::get_offset_of_woodPrice_4(),
	TrainingIconManager_t1144058280::get_offset_of_stonePrice_5(),
	TrainingIconManager_t1144058280::get_offset_of_ironPrice_6(),
	TrainingIconManager_t1144058280::get_offset_of_copperPrice_7(),
	TrainingIconManager_t1144058280::get_offset_of_silverPrice_8(),
	TrainingIconManager_t1144058280::get_offset_of_goldPrice_9(),
	TrainingIconManager_t1144058280::get_offset_of_trainingTime_10(),
	TrainingIconManager_t1144058280::get_offset_of_unitCount_11(),
	TrainingIconManager_t1144058280::get_offset_of_unitName_12(),
	TrainingIconManager_t1144058280_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_13(),
	TrainingIconManager_t1144058280_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (TributesContentManager_t2446486829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[8] = 
{
	TributesContentManager_t2446486829::get_offset_of_tributes_2(),
	TributesContentManager_t2446486829::get_offset_of_tributesFood_3(),
	TributesContentManager_t2446486829::get_offset_of_tributesWood_4(),
	TributesContentManager_t2446486829::get_offset_of_tributesIron_5(),
	TributesContentManager_t2446486829::get_offset_of_tributesStone_6(),
	TributesContentManager_t2446486829::get_offset_of_tributesCopper_7(),
	TributesContentManager_t2446486829::get_offset_of_tributesSilver_8(),
	TributesContentManager_t2446486829::get_offset_of_tributesGold_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (TutorialContentManager_t1277416346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[4] = 
{
	TutorialContentManager_t1277416346::get_offset_of_pageTitle_2(),
	TutorialContentManager_t1277416346::get_offset_of_pageText_3(),
	TutorialContentManager_t1277416346::get_offset_of_pageImage_4(),
	TutorialContentManager_t1277416346::get_offset_of_currrentPage_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (UnitDetailsManager_t3163445022), -1, sizeof(UnitDetailsManager_t3163445022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3363[31] = 
{
	UnitDetailsManager_t3163445022::get_offset_of_unitCountLabel_2(),
	UnitDetailsManager_t3163445022::get_offset_of_unitDescriptionLabel_3(),
	UnitDetailsManager_t3163445022::get_offset_of_unitIcon_4(),
	UnitDetailsManager_t3163445022::get_offset_of_speed_5(),
	UnitDetailsManager_t3163445022::get_offset_of_defence_6(),
	UnitDetailsManager_t3163445022::get_offset_of_food_7(),
	UnitDetailsManager_t3163445022::get_offset_of_range_8(),
	UnitDetailsManager_t3163445022::get_offset_of_life_9(),
	UnitDetailsManager_t3163445022::get_offset_of_population_10(),
	UnitDetailsManager_t3163445022::get_offset_of_attack_11(),
	UnitDetailsManager_t3163445022::get_offset_of_capacity_12(),
	UnitDetailsManager_t3163445022::get_offset_of_time_13(),
	UnitDetailsManager_t3163445022::get_offset_of_foodNeeded_14(),
	UnitDetailsManager_t3163445022::get_offset_of_woodNeeded_15(),
	UnitDetailsManager_t3163445022::get_offset_of_stoneNeeded_16(),
	UnitDetailsManager_t3163445022::get_offset_of_ironNeeded_17(),
	UnitDetailsManager_t3163445022::get_offset_of_copperNeeded_18(),
	UnitDetailsManager_t3163445022::get_offset_of_silverNeeded_19(),
	UnitDetailsManager_t3163445022::get_offset_of_goldNeeded_20(),
	UnitDetailsManager_t3163445022::get_offset_of_foodPresent_21(),
	UnitDetailsManager_t3163445022::get_offset_of_woodPresent_22(),
	UnitDetailsManager_t3163445022::get_offset_of_stonePresent_23(),
	UnitDetailsManager_t3163445022::get_offset_of_ironPresent_24(),
	UnitDetailsManager_t3163445022::get_offset_of_copperPresent_25(),
	UnitDetailsManager_t3163445022::get_offset_of_silverPresent_26(),
	UnitDetailsManager_t3163445022::get_offset_of_goldPresent_27(),
	UnitDetailsManager_t3163445022::get_offset_of_unitsToRecruit_28(),
	UnitDetailsManager_t3163445022::get_offset_of_unitName_29(),
	UnitDetailsManager_t3163445022::get_offset_of_unit_30(),
	UnitDetailsManager_t3163445022::get_offset_of_resources_31(),
	UnitDetailsManager_t3163445022_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (UnitsTableManager_t4191917185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[21] = 
{
	UnitsTableManager_t4191917185::get_offset_of_cityWorker_2(),
	UnitsTableManager_t4191917185::get_offset_of_citySpy_3(),
	UnitsTableManager_t4191917185::get_offset_of_citySwordsman_4(),
	UnitsTableManager_t4191917185::get_offset_of_citySpearman_5(),
	UnitsTableManager_t4191917185::get_offset_of_cityPikeman_6(),
	UnitsTableManager_t4191917185::get_offset_of_cityScoutRider_7(),
	UnitsTableManager_t4191917185::get_offset_of_cityLightCavalry_8(),
	UnitsTableManager_t4191917185::get_offset_of_cityHeavyCavalry_9(),
	UnitsTableManager_t4191917185::get_offset_of_cityArcher_10(),
	UnitsTableManager_t4191917185::get_offset_of_cityArcherRider_11(),
	UnitsTableManager_t4191917185::get_offset_of_cityHollyman_12(),
	UnitsTableManager_t4191917185::get_offset_of_cityWagon_13(),
	UnitsTableManager_t4191917185::get_offset_of_cityTrebuchet_14(),
	UnitsTableManager_t4191917185::get_offset_of_citySiegeTower_15(),
	UnitsTableManager_t4191917185::get_offset_of_cityBatteringRam_16(),
	UnitsTableManager_t4191917185::get_offset_of_cityBallista_17(),
	UnitsTableManager_t4191917185::get_offset_of_cityBeaconTower_18(),
	UnitsTableManager_t4191917185::get_offset_of_cityArcherTower_19(),
	UnitsTableManager_t4191917185::get_offset_of_cityHotOilHole_20(),
	UnitsTableManager_t4191917185::get_offset_of_cityTrebuchetTower_21(),
	UnitsTableManager_t4191917185::get_offset_of_cityBallistaTower_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (UpdateBuildingContentManager_t2330211018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[25] = 
{
	UpdateBuildingContentManager_t2330211018::get_offset_of_upgrade_2(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_type_3(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_pitId_4(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_contentLabel_5(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_description_6(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_resources_7(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_downgradeDescription_8(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_foodNeeded_9(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_woodNeeded_10(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_stoneNeeded_11(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_ironNeeded_12(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_copperNeeded_13(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_silverNeeded_14(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_goldNeeded_15(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_foodPresent_16(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_woodPresent_17(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_stonePresent_18(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_ironPresent_19(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_copperPresent_20(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_silverPresent_21(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_goldPresent_22(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_building_23(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_valley_24(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_constant_25(),
	UpdateBuildingContentManager_t2330211018::get_offset_of_cityResources_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (UserInfoContentManager_t1915571260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[26] = 
{
	UserInfoContentManager_t1915571260::get_offset_of_currentUserPanel_2(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserPanel_3(),
	UserInfoContentManager_t1915571260::get_offset_of_otherPlayerActions_4(),
	UserInfoContentManager_t1915571260::get_offset_of_allianceMemberActions_5(),
	UserInfoContentManager_t1915571260::get_offset_of_currentUserNick_6(),
	UserInfoContentManager_t1915571260::get_offset_of_currentUserEmail_7(),
	UserInfoContentManager_t1915571260::get_offset_of_currentUserMobile_8(),
	UserInfoContentManager_t1915571260::get_offset_of_currentUserFlag_9(),
	UserInfoContentManager_t1915571260::get_offset_of_currentUserImage_10(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserNick_11(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserExperience_12(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserRank_13(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserCities_14(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserCapital_15(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserCapitalCoords_16(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserLastOnline_17(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserFlag_18(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserImage_19(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserPosition_20(),
	UserInfoContentManager_t1915571260::get_offset_of_otherUserAlliance_21(),
	UserInfoContentManager_t1915571260::get_offset_of_allianceMemberPosition_22(),
	UserInfoContentManager_t1915571260::get_offset_of_changeBtn_23(),
	UserInfoContentManager_t1915571260::get_offset_of_excludeBtn_24(),
	UserInfoContentManager_t1915571260::get_offset_of_user_25(),
	UserInfoContentManager_t1915571260::get_offset_of_iconIndex_26(),
	UserInfoContentManager_t1915571260::get_offset_of_ranks_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (DontDestroyMe_t4280075198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (FromFieldsToCastle_t767099202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (ADManager_t261308543), -1, sizeof(ADManager_t261308543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3369[14] = 
{
	ADManager_t261308543_StaticFields::get_offset_of_adBannerUnitId_2(),
	ADManager_t261308543_StaticFields::get_offset_of_adInterstitialUnitId_3(),
	ADManager_t261308543::get_offset_of_UI_4(),
	ADManager_t261308543::get_offset_of_bannerView_5(),
	ADManager_t261308543::get_offset_of_interstitial_6(),
	ADManager_t261308543::get_offset_of_rewardBasedVideo_7(),
	ADManager_t261308543::get_offset_of_nextBonusWoodDate_8(),
	ADManager_t261308543::get_offset_of_nextBonusFoodDate_9(),
	ADManager_t261308543::get_offset_of_nextBonusStoneDate_10(),
	ADManager_t261308543::get_offset_of_nextBonusIronDate_11(),
	ADManager_t261308543::get_offset_of_nextBonusGoldDate_12(),
	ADManager_t261308543::get_offset_of_nextBonusSilverDate_13(),
	ADManager_t261308543::get_offset_of_nextBonusCopperDate_14(),
	ADManager_t261308543::get_offset_of_resource_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (U3CAddResourceBonusU3Ec__Iterator0_t3918272656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[6] = 
{
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_U3CformU3E__0_0(),
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_resource_1(),
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_U3CwwwU3E__0_2(),
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_U24current_3(),
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_U24disposing_4(),
	U3CAddResourceBonusU3Ec__Iterator0_t3918272656::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (AllianceManager_t344187419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[18] = 
{
	AllianceManager_t344187419::get_offset_of_userAlliance_0(),
	AllianceManager_t344187419::get_offset_of_allianceMembers_1(),
	AllianceManager_t344187419::get_offset_of_allAlliances_2(),
	AllianceManager_t344187419::get_offset_of_onGetAllianceMembers_3(),
	AllianceManager_t344187419::get_offset_of_onGetAlliances_4(),
	AllianceManager_t344187419::get_offset_of_onExcludeUser_5(),
	AllianceManager_t344187419::get_offset_of_onInviteUser_6(),
	AllianceManager_t344187419::get_offset_of_onRequestJoin_7(),
	AllianceManager_t344187419::get_offset_of_onQuitAlliance_8(),
	AllianceManager_t344187419::get_offset_of_onUpdateGuideline_9(),
	AllianceManager_t344187419::get_offset_of_onAcceptInvite_10(),
	AllianceManager_t344187419::get_offset_of_onCancelInvite_11(),
	AllianceManager_t344187419::get_offset_of_onAddAlly_12(),
	AllianceManager_t344187419::get_offset_of_onAddEnemy_13(),
	AllianceManager_t344187419::get_offset_of_onRemoveAlly_14(),
	AllianceManager_t344187419::get_offset_of_onRemoveEnemy_15(),
	AllianceManager_t344187419::get_offset_of_onCreateNewAlliance_16(),
	AllianceManager_t344187419::get_offset_of_dateFormat_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (U3CAllianceMemebersU3Ec__Iterator0_t3649014814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[5] = 
{
	U3CAllianceMemebersU3Ec__Iterator0_t3649014814::get_offset_of_U3CrequestU3E__0_0(),
	U3CAllianceMemebersU3Ec__Iterator0_t3649014814::get_offset_of_U24this_1(),
	U3CAllianceMemebersU3Ec__Iterator0_t3649014814::get_offset_of_U24current_2(),
	U3CAllianceMemebersU3Ec__Iterator0_t3649014814::get_offset_of_U24disposing_3(),
	U3CAllianceMemebersU3Ec__Iterator0_t3649014814::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (U3CExcludeUserU3Ec__Iterator1_t1861355320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[7] = 
{
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U3CexcludeFormU3E__0_0(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_userId_1(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U3CrequestU3E__0_2(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U24this_3(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U24current_4(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U24disposing_5(),
	U3CExcludeUserU3Ec__Iterator1_t1861355320::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (U3CInviteUserU3Ec__Iterator2_t2202159703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[7] = 
{
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U3CinviteFormU3E__0_0(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_userId_1(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U3CrequestU3E__0_2(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U24this_3(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U24current_4(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U24disposing_5(),
	U3CInviteUserU3Ec__Iterator2_t2202159703::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (U3CRequestJoinU3Ec__Iterator3_t3847028616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[7] = 
{
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U3CrequestFormU3E__0_0(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_allianceId_1(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U3CrequestU3E__0_2(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U24this_3(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U24current_4(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U24disposing_5(),
	U3CRequestJoinU3Ec__Iterator3_t3847028616::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (U3CAllAlliancesU3Ec__Iterator4_t366036290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[5] = 
{
	U3CAllAlliancesU3Ec__Iterator4_t366036290::get_offset_of_U3CrequestU3E__0_0(),
	U3CAllAlliancesU3Ec__Iterator4_t366036290::get_offset_of_U24this_1(),
	U3CAllAlliancesU3Ec__Iterator4_t366036290::get_offset_of_U24current_2(),
	U3CAllAlliancesU3Ec__Iterator4_t366036290::get_offset_of_U24disposing_3(),
	U3CAllAlliancesU3Ec__Iterator4_t366036290::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (U3CQuitAllianceU3Ec__Iterator5_t878846607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[6] = 
{
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U3CquitFormU3E__0_0(),
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U3CrequestU3E__0_1(),
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U24this_2(),
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U24current_3(),
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U24disposing_4(),
	U3CQuitAllianceU3Ec__Iterator5_t878846607::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (U3CUpdateGuidelineU3Ec__Iterator6_t1110526823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[7] = 
{
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U3CrequestFormU3E__0_0(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_guideline_1(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U3CrequestU3E__0_2(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U24this_3(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U24current_4(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U24disposing_5(),
	U3CUpdateGuidelineU3Ec__Iterator6_t1110526823::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (U3CAcceptInviteU3Ec__Iterator7_t1011583579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[7] = 
{
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U3CrequestFormU3E__0_0(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_inviteId_1(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U3CrequestU3E__0_2(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U24this_3(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U24current_4(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U24disposing_5(),
	U3CAcceptInviteU3Ec__Iterator7_t1011583579::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (U3CCancelInviteU3Ec__Iterator8_t279811585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3380[7] = 
{
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U3CrequestFormU3E__0_0(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_inviteId_1(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U24this_3(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U24current_4(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U24disposing_5(),
	U3CCancelInviteU3Ec__Iterator8_t279811585::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (U3CSetMemberRankU3Ec__Iterator9_t700886960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[8] = 
{
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U3CrankFormU3E__0_0(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_userId_1(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_rank_2(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U3CrequestU3E__0_3(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U24this_4(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U24current_5(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U24disposing_6(),
	U3CSetMemberRankU3Ec__Iterator9_t700886960::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (U3CResignRankU3Ec__IteratorA_t783501582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[5] = 
{
	U3CResignRankU3Ec__IteratorA_t783501582::get_offset_of_U3CrankFormU3E__0_0(),
	U3CResignRankU3Ec__IteratorA_t783501582::get_offset_of_U3CrequestU3E__0_1(),
	U3CResignRankU3Ec__IteratorA_t783501582::get_offset_of_U24current_2(),
	U3CResignRankU3Ec__IteratorA_t783501582::get_offset_of_U24disposing_3(),
	U3CResignRankU3Ec__IteratorA_t783501582::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (U3CAddAllyU3Ec__IteratorB_t1622364050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[7] = 
{
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U3CaddFormU3E__0_0(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_allianceId_1(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U3CrequestU3E__0_2(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U24this_3(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U24current_4(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U24disposing_5(),
	U3CAddAllyU3Ec__IteratorB_t1622364050::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (U3CRemoveAllyU3Ec__IteratorC_t4044411523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3384[7] = 
{
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U3CremoveFormU3E__0_0(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_allianceId_1(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U3CrequestU3E__0_2(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U24this_3(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U24current_4(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U24disposing_5(),
	U3CRemoveAllyU3Ec__IteratorC_t4044411523::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (U3CAddEnemyU3Ec__IteratorD_t3744444522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[7] = 
{
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U3CaddFormU3E__0_0(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_allianceId_1(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U3CrequestU3E__0_2(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U24this_3(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U24current_4(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U24disposing_5(),
	U3CAddEnemyU3Ec__IteratorD_t3744444522::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (U3CRemoveEnemyU3Ec__IteratorE_t2306474738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3386[7] = 
{
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U3CremoveFormU3E__0_0(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_allianceId_1(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U3CrequestU3E__0_2(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U24this_3(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U24current_4(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U24disposing_5(),
	U3CRemoveEnemyU3Ec__IteratorE_t2306474738::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (U3CCreateNewAlllianceU3Ec__IteratorF_t126037287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[7] = 
{
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U3CnewFormU3E__0_0(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_name_1(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U3CrequestU3E__0_2(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U24this_3(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U24current_4(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U24disposing_5(),
	U3CCreateNewAlllianceU3Ec__IteratorF_t126037287::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (BattleManager_t4022130644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[5] = 
{
	BattleManager_t4022130644::get_offset_of_cityArmies_0(),
	BattleManager_t4022130644::get_offset_of_onGetCityArmies_1(),
	BattleManager_t4022130644::get_offset_of_onMarchLaunched_2(),
	BattleManager_t4022130644::get_offset_of_onMarchCanceled_3(),
	BattleManager_t4022130644::get_offset_of_onArmyReturnInitiated_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (U3CGetCityArmiesU3Ec__Iterator0_t256880612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[5] = 
{
	U3CGetCityArmiesU3Ec__Iterator0_t256880612::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetCityArmiesU3Ec__Iterator0_t256880612::get_offset_of_U24this_1(),
	U3CGetCityArmiesU3Ec__Iterator0_t256880612::get_offset_of_U24current_2(),
	U3CGetCityArmiesU3Ec__Iterator0_t256880612::get_offset_of_U24disposing_3(),
	U3CGetCityArmiesU3Ec__Iterator0_t256880612::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (U3CMarchU3Ec__Iterator1_t976351973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[12] = 
{
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U3CformU3E__0_0(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_attcakType_1(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_army_2(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_resources_3(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_destX_4(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_destY_5(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_knightId_6(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U3CrequestU3E__0_7(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U24this_8(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U24current_9(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U24disposing_10(),
	U3CMarchU3Ec__Iterator1_t976351973::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (U3CRecruitBarbariansU3Ec__Iterator2_t783165763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[10] = 
{
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U3CformU3E__0_0(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_barbariansId_1(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_army_2(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_destX_3(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_destY_4(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U3CrequestU3E__0_5(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U24this_6(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U24current_7(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U24disposing_8(),
	U3CRecruitBarbariansU3Ec__Iterator2_t783165763::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (U3CCancelAttackU3Ec__Iterator3_t2200631826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[7] = 
{
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U3CformU3E__0_0(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_armyId_1(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U24this_3(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U24current_4(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U24disposing_5(),
	U3CCancelAttackU3Ec__Iterator3_t2200631826::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (U3CReturnArmyU3Ec__Iterator4_t1160843784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[7] = 
{
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U3CformU3E__0_0(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_armyId_1(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U3CrequestU3E__0_2(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U24this_3(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U24current_4(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U24disposing_5(),
	U3CReturnArmyU3Ec__Iterator4_t1160843784::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (BuildingsManager_t3721263023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[6] = 
{
	BuildingsManager_t3721263023::get_offset_of_cityBuildings_0(),
	BuildingsManager_t3721263023::get_offset_of_cityFields_1(),
	BuildingsManager_t3721263023::get_offset_of_cityValleys_2(),
	BuildingsManager_t3721263023::get_offset_of_onBuildingUpdate_3(),
	BuildingsManager_t3721263023::get_offset_of_onGotValleys_4(),
	BuildingsManager_t3721263023::get_offset_of_onBuildingCancelled_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[5] = 
{
	U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605::get_offset_of_U24this_1(),
	U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605::get_offset_of_U24current_2(),
	U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605::get_offset_of_U24disposing_3(),
	U3CGetInitialCityBuildingsU3Ec__Iterator0_t1552903605::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[5] = 
{
	U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895::get_offset_of_U24this_1(),
	U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895::get_offset_of_U24current_2(),
	U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895::get_offset_of_U24disposing_3(),
	U3CUpdateBuildingsAndFieldsStateU3Ec__Iterator1_t1930216895::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[5] = 
{
	U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358::get_offset_of_U24this_1(),
	U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358::get_offset_of_U24current_2(),
	U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358::get_offset_of_U24disposing_3(),
	U3CGetInitialCityFieldsU3Ec__Iterator2_t3018373358::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[6] = 
{
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_flag_1(),
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_U24this_2(),
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_U24current_3(),
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_U24disposing_4(),
	U3CGetInitialCityValleysU3Ec__Iterator3_t2909717123::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (U3CUpdateBuildingU3Ec__Iterator4_t568524150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[11] = 
{
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U3ClocationAdressU3E__0_0(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_location_1(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U3CbuildFormU3E__0_2(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_city_3(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_pit_id_4(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_command_5(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U3CrequestU3E__0_6(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U24this_7(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U24current_8(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U24disposing_9(),
	U3CUpdateBuildingU3Ec__Iterator4_t568524150::get_offset_of_U24PC_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
