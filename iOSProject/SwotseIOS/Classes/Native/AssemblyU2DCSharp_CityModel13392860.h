﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityModel
struct  CityModel_t13392860  : public Il2CppObject
{
public:
	// System.Int64 CityModel::id
	int64_t ___id_0;
	// System.Int64 CityModel::owner
	int64_t ___owner_1;
	// System.String CityModel::city_name
	String_t* ___city_name_2;
	// System.String CityModel::image_name
	String_t* ___image_name_3;
	// System.String CityModel::occupantCityOwnerName
	String_t* ___occupantCityOwnerName_4;
	// System.String CityModel::occupantCityName
	String_t* ___occupantCityName_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___owner_1)); }
	inline int64_t get_owner_1() const { return ___owner_1; }
	inline int64_t* get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(int64_t value)
	{
		___owner_1 = value;
	}

	inline static int32_t get_offset_of_city_name_2() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___city_name_2)); }
	inline String_t* get_city_name_2() const { return ___city_name_2; }
	inline String_t** get_address_of_city_name_2() { return &___city_name_2; }
	inline void set_city_name_2(String_t* value)
	{
		___city_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___city_name_2, value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier(&___image_name_3, value);
	}

	inline static int32_t get_offset_of_occupantCityOwnerName_4() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___occupantCityOwnerName_4)); }
	inline String_t* get_occupantCityOwnerName_4() const { return ___occupantCityOwnerName_4; }
	inline String_t** get_address_of_occupantCityOwnerName_4() { return &___occupantCityOwnerName_4; }
	inline void set_occupantCityOwnerName_4(String_t* value)
	{
		___occupantCityOwnerName_4 = value;
		Il2CppCodeGenWriteBarrier(&___occupantCityOwnerName_4, value);
	}

	inline static int32_t get_offset_of_occupantCityName_5() { return static_cast<int32_t>(offsetof(CityModel_t13392860, ___occupantCityName_5)); }
	inline String_t* get_occupantCityName_5() const { return ___occupantCityName_5; }
	inline String_t** get_address_of_occupantCityName_5() { return &___occupantCityName_5; }
	inline void set_occupantCityName_5(String_t* value)
	{
		___occupantCityName_5 = value;
		Il2CppCodeGenWriteBarrier(&___occupantCityName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
