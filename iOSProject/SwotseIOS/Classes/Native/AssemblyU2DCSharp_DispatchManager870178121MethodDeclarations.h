﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DispatchManager
struct DispatchManager_t870178121;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnitsModel
struct UnitsModel_t1926818124;
// ResourcesModel
struct ResourcesModel_t2978985958;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_UnitsModel1926818124.h"
#include "AssemblyU2DCSharp_ResourcesModel2978985958.h"

// System.Void DispatchManager::.ctor()
extern "C"  void DispatchManager__ctor_m204868820 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::add_onGotParams(SimpleEvent)
extern "C"  void DispatchManager_add_onGotParams_m845453841 (DispatchManager_t870178121 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::remove_onGotParams(SimpleEvent)
extern "C"  void DispatchManager_remove_onGotParams_m3396369234 (DispatchManager_t870178121 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::OnEnable()
extern "C"  void DispatchManager_OnEnable_m250678628 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::FixedUpdate()
extern "C"  void DispatchManager_FixedUpdate_m3212541839 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::March()
extern "C"  void DispatchManager_March_m1472628405 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::MarchLaunched(System.Object,System.String)
extern "C"  void DispatchManager_MarchLaunched_m2493649621 (DispatchManager_t870178121 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::RefreshAttackParams()
extern "C"  void DispatchManager_RefreshAttackParams_m3607080897 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DispatchManager::GetAttackParams(UnitsModel,ResourcesModel,System.String,System.Int64,System.Int64,System.Int64)
extern "C"  Il2CppObject * DispatchManager_GetAttackParams_m3543134316 (DispatchManager_t870178121 * __this, UnitsModel_t1926818124 * ___army0, ResourcesModel_t2978985958 * ___resources1, String_t* ___attcakType2, int64_t ___destX3, int64_t ___destY4, int64_t ___knightId5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::GotParams(System.Object,System.String)
extern "C"  void DispatchManager_GotParams_m1963421676 (DispatchManager_t870178121 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager::GoBack()
extern "C"  void DispatchManager_GoBack_m2045782951 (DispatchManager_t870178121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
