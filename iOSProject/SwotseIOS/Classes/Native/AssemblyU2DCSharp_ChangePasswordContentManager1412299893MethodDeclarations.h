﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangePasswordContentManager
struct ChangePasswordContentManager_t1412299893;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChangePasswordContentManager::.ctor()
extern "C"  void ChangePasswordContentManager__ctor_m1011505236 (ChangePasswordContentManager_t1412299893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordContentManager::ChangePassword()
extern "C"  void ChangePasswordContentManager_ChangePassword_m3642139913 (ChangePasswordContentManager_t1412299893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordContentManager::PasswordChanged(System.Object,System.String)
extern "C"  void ChangePasswordContentManager_PasswordChanged_m476874277 (ChangePasswordContentManager_t1412299893 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
