﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;
// UnitConstantModel
struct UnitConstantModel_t1807269633;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitDetailsManager
struct  UnitDetailsManager_t1603586725  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UnitDetailsManager::unitCountLabel
	Text_t356221433 * ___unitCountLabel_2;
	// UnityEngine.UI.Text UnitDetailsManager::unitDescriptionLabel
	Text_t356221433 * ___unitDescriptionLabel_3;
	// UnityEngine.UI.Image UnitDetailsManager::unitIcon
	Image_t2042527209 * ___unitIcon_4;
	// UnityEngine.UI.Text UnitDetailsManager::speed
	Text_t356221433 * ___speed_5;
	// UnityEngine.UI.Text UnitDetailsManager::defence
	Text_t356221433 * ___defence_6;
	// UnityEngine.UI.Text UnitDetailsManager::food
	Text_t356221433 * ___food_7;
	// UnityEngine.UI.Text UnitDetailsManager::range
	Text_t356221433 * ___range_8;
	// UnityEngine.UI.Text UnitDetailsManager::life
	Text_t356221433 * ___life_9;
	// UnityEngine.UI.Text UnitDetailsManager::population
	Text_t356221433 * ___population_10;
	// UnityEngine.UI.Text UnitDetailsManager::attack
	Text_t356221433 * ___attack_11;
	// UnityEngine.UI.Text UnitDetailsManager::capacity
	Text_t356221433 * ___capacity_12;
	// UnityEngine.UI.Text UnitDetailsManager::time
	Text_t356221433 * ___time_13;
	// UnityEngine.UI.Text UnitDetailsManager::foodNeeded
	Text_t356221433 * ___foodNeeded_14;
	// UnityEngine.UI.Text UnitDetailsManager::woodNeeded
	Text_t356221433 * ___woodNeeded_15;
	// UnityEngine.UI.Text UnitDetailsManager::stoneNeeded
	Text_t356221433 * ___stoneNeeded_16;
	// UnityEngine.UI.Text UnitDetailsManager::ironNeeded
	Text_t356221433 * ___ironNeeded_17;
	// UnityEngine.UI.Text UnitDetailsManager::copperNeeded
	Text_t356221433 * ___copperNeeded_18;
	// UnityEngine.UI.Text UnitDetailsManager::silverNeeded
	Text_t356221433 * ___silverNeeded_19;
	// UnityEngine.UI.Text UnitDetailsManager::goldNeeded
	Text_t356221433 * ___goldNeeded_20;
	// UnityEngine.UI.Text UnitDetailsManager::foodPresent
	Text_t356221433 * ___foodPresent_21;
	// UnityEngine.UI.Text UnitDetailsManager::woodPresent
	Text_t356221433 * ___woodPresent_22;
	// UnityEngine.UI.Text UnitDetailsManager::stonePresent
	Text_t356221433 * ___stonePresent_23;
	// UnityEngine.UI.Text UnitDetailsManager::ironPresent
	Text_t356221433 * ___ironPresent_24;
	// UnityEngine.UI.Text UnitDetailsManager::copperPresent
	Text_t356221433 * ___copperPresent_25;
	// UnityEngine.UI.Text UnitDetailsManager::silverPresent
	Text_t356221433 * ___silverPresent_26;
	// UnityEngine.UI.Text UnitDetailsManager::goldPresent
	Text_t356221433 * ___goldPresent_27;
	// UnityEngine.UI.InputField UnitDetailsManager::unitsToRecruit
	InputField_t1631627530 * ___unitsToRecruit_28;
	// System.String UnitDetailsManager::unitName
	String_t* ___unitName_29;
	// UnitConstantModel UnitDetailsManager::unit
	UnitConstantModel_t1807269633 * ___unit_30;
	// ResourcesModel UnitDetailsManager::resources
	ResourcesModel_t2978985958 * ___resources_31;

public:
	inline static int32_t get_offset_of_unitCountLabel_2() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unitCountLabel_2)); }
	inline Text_t356221433 * get_unitCountLabel_2() const { return ___unitCountLabel_2; }
	inline Text_t356221433 ** get_address_of_unitCountLabel_2() { return &___unitCountLabel_2; }
	inline void set_unitCountLabel_2(Text_t356221433 * value)
	{
		___unitCountLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___unitCountLabel_2, value);
	}

	inline static int32_t get_offset_of_unitDescriptionLabel_3() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unitDescriptionLabel_3)); }
	inline Text_t356221433 * get_unitDescriptionLabel_3() const { return ___unitDescriptionLabel_3; }
	inline Text_t356221433 ** get_address_of_unitDescriptionLabel_3() { return &___unitDescriptionLabel_3; }
	inline void set_unitDescriptionLabel_3(Text_t356221433 * value)
	{
		___unitDescriptionLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___unitDescriptionLabel_3, value);
	}

	inline static int32_t get_offset_of_unitIcon_4() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unitIcon_4)); }
	inline Image_t2042527209 * get_unitIcon_4() const { return ___unitIcon_4; }
	inline Image_t2042527209 ** get_address_of_unitIcon_4() { return &___unitIcon_4; }
	inline void set_unitIcon_4(Image_t2042527209 * value)
	{
		___unitIcon_4 = value;
		Il2CppCodeGenWriteBarrier(&___unitIcon_4, value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___speed_5)); }
	inline Text_t356221433 * get_speed_5() const { return ___speed_5; }
	inline Text_t356221433 ** get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(Text_t356221433 * value)
	{
		___speed_5 = value;
		Il2CppCodeGenWriteBarrier(&___speed_5, value);
	}

	inline static int32_t get_offset_of_defence_6() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___defence_6)); }
	inline Text_t356221433 * get_defence_6() const { return ___defence_6; }
	inline Text_t356221433 ** get_address_of_defence_6() { return &___defence_6; }
	inline void set_defence_6(Text_t356221433 * value)
	{
		___defence_6 = value;
		Il2CppCodeGenWriteBarrier(&___defence_6, value);
	}

	inline static int32_t get_offset_of_food_7() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___food_7)); }
	inline Text_t356221433 * get_food_7() const { return ___food_7; }
	inline Text_t356221433 ** get_address_of_food_7() { return &___food_7; }
	inline void set_food_7(Text_t356221433 * value)
	{
		___food_7 = value;
		Il2CppCodeGenWriteBarrier(&___food_7, value);
	}

	inline static int32_t get_offset_of_range_8() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___range_8)); }
	inline Text_t356221433 * get_range_8() const { return ___range_8; }
	inline Text_t356221433 ** get_address_of_range_8() { return &___range_8; }
	inline void set_range_8(Text_t356221433 * value)
	{
		___range_8 = value;
		Il2CppCodeGenWriteBarrier(&___range_8, value);
	}

	inline static int32_t get_offset_of_life_9() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___life_9)); }
	inline Text_t356221433 * get_life_9() const { return ___life_9; }
	inline Text_t356221433 ** get_address_of_life_9() { return &___life_9; }
	inline void set_life_9(Text_t356221433 * value)
	{
		___life_9 = value;
		Il2CppCodeGenWriteBarrier(&___life_9, value);
	}

	inline static int32_t get_offset_of_population_10() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___population_10)); }
	inline Text_t356221433 * get_population_10() const { return ___population_10; }
	inline Text_t356221433 ** get_address_of_population_10() { return &___population_10; }
	inline void set_population_10(Text_t356221433 * value)
	{
		___population_10 = value;
		Il2CppCodeGenWriteBarrier(&___population_10, value);
	}

	inline static int32_t get_offset_of_attack_11() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___attack_11)); }
	inline Text_t356221433 * get_attack_11() const { return ___attack_11; }
	inline Text_t356221433 ** get_address_of_attack_11() { return &___attack_11; }
	inline void set_attack_11(Text_t356221433 * value)
	{
		___attack_11 = value;
		Il2CppCodeGenWriteBarrier(&___attack_11, value);
	}

	inline static int32_t get_offset_of_capacity_12() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___capacity_12)); }
	inline Text_t356221433 * get_capacity_12() const { return ___capacity_12; }
	inline Text_t356221433 ** get_address_of_capacity_12() { return &___capacity_12; }
	inline void set_capacity_12(Text_t356221433 * value)
	{
		___capacity_12 = value;
		Il2CppCodeGenWriteBarrier(&___capacity_12, value);
	}

	inline static int32_t get_offset_of_time_13() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___time_13)); }
	inline Text_t356221433 * get_time_13() const { return ___time_13; }
	inline Text_t356221433 ** get_address_of_time_13() { return &___time_13; }
	inline void set_time_13(Text_t356221433 * value)
	{
		___time_13 = value;
		Il2CppCodeGenWriteBarrier(&___time_13, value);
	}

	inline static int32_t get_offset_of_foodNeeded_14() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___foodNeeded_14)); }
	inline Text_t356221433 * get_foodNeeded_14() const { return ___foodNeeded_14; }
	inline Text_t356221433 ** get_address_of_foodNeeded_14() { return &___foodNeeded_14; }
	inline void set_foodNeeded_14(Text_t356221433 * value)
	{
		___foodNeeded_14 = value;
		Il2CppCodeGenWriteBarrier(&___foodNeeded_14, value);
	}

	inline static int32_t get_offset_of_woodNeeded_15() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___woodNeeded_15)); }
	inline Text_t356221433 * get_woodNeeded_15() const { return ___woodNeeded_15; }
	inline Text_t356221433 ** get_address_of_woodNeeded_15() { return &___woodNeeded_15; }
	inline void set_woodNeeded_15(Text_t356221433 * value)
	{
		___woodNeeded_15 = value;
		Il2CppCodeGenWriteBarrier(&___woodNeeded_15, value);
	}

	inline static int32_t get_offset_of_stoneNeeded_16() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___stoneNeeded_16)); }
	inline Text_t356221433 * get_stoneNeeded_16() const { return ___stoneNeeded_16; }
	inline Text_t356221433 ** get_address_of_stoneNeeded_16() { return &___stoneNeeded_16; }
	inline void set_stoneNeeded_16(Text_t356221433 * value)
	{
		___stoneNeeded_16 = value;
		Il2CppCodeGenWriteBarrier(&___stoneNeeded_16, value);
	}

	inline static int32_t get_offset_of_ironNeeded_17() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___ironNeeded_17)); }
	inline Text_t356221433 * get_ironNeeded_17() const { return ___ironNeeded_17; }
	inline Text_t356221433 ** get_address_of_ironNeeded_17() { return &___ironNeeded_17; }
	inline void set_ironNeeded_17(Text_t356221433 * value)
	{
		___ironNeeded_17 = value;
		Il2CppCodeGenWriteBarrier(&___ironNeeded_17, value);
	}

	inline static int32_t get_offset_of_copperNeeded_18() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___copperNeeded_18)); }
	inline Text_t356221433 * get_copperNeeded_18() const { return ___copperNeeded_18; }
	inline Text_t356221433 ** get_address_of_copperNeeded_18() { return &___copperNeeded_18; }
	inline void set_copperNeeded_18(Text_t356221433 * value)
	{
		___copperNeeded_18 = value;
		Il2CppCodeGenWriteBarrier(&___copperNeeded_18, value);
	}

	inline static int32_t get_offset_of_silverNeeded_19() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___silverNeeded_19)); }
	inline Text_t356221433 * get_silverNeeded_19() const { return ___silverNeeded_19; }
	inline Text_t356221433 ** get_address_of_silverNeeded_19() { return &___silverNeeded_19; }
	inline void set_silverNeeded_19(Text_t356221433 * value)
	{
		___silverNeeded_19 = value;
		Il2CppCodeGenWriteBarrier(&___silverNeeded_19, value);
	}

	inline static int32_t get_offset_of_goldNeeded_20() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___goldNeeded_20)); }
	inline Text_t356221433 * get_goldNeeded_20() const { return ___goldNeeded_20; }
	inline Text_t356221433 ** get_address_of_goldNeeded_20() { return &___goldNeeded_20; }
	inline void set_goldNeeded_20(Text_t356221433 * value)
	{
		___goldNeeded_20 = value;
		Il2CppCodeGenWriteBarrier(&___goldNeeded_20, value);
	}

	inline static int32_t get_offset_of_foodPresent_21() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___foodPresent_21)); }
	inline Text_t356221433 * get_foodPresent_21() const { return ___foodPresent_21; }
	inline Text_t356221433 ** get_address_of_foodPresent_21() { return &___foodPresent_21; }
	inline void set_foodPresent_21(Text_t356221433 * value)
	{
		___foodPresent_21 = value;
		Il2CppCodeGenWriteBarrier(&___foodPresent_21, value);
	}

	inline static int32_t get_offset_of_woodPresent_22() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___woodPresent_22)); }
	inline Text_t356221433 * get_woodPresent_22() const { return ___woodPresent_22; }
	inline Text_t356221433 ** get_address_of_woodPresent_22() { return &___woodPresent_22; }
	inline void set_woodPresent_22(Text_t356221433 * value)
	{
		___woodPresent_22 = value;
		Il2CppCodeGenWriteBarrier(&___woodPresent_22, value);
	}

	inline static int32_t get_offset_of_stonePresent_23() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___stonePresent_23)); }
	inline Text_t356221433 * get_stonePresent_23() const { return ___stonePresent_23; }
	inline Text_t356221433 ** get_address_of_stonePresent_23() { return &___stonePresent_23; }
	inline void set_stonePresent_23(Text_t356221433 * value)
	{
		___stonePresent_23 = value;
		Il2CppCodeGenWriteBarrier(&___stonePresent_23, value);
	}

	inline static int32_t get_offset_of_ironPresent_24() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___ironPresent_24)); }
	inline Text_t356221433 * get_ironPresent_24() const { return ___ironPresent_24; }
	inline Text_t356221433 ** get_address_of_ironPresent_24() { return &___ironPresent_24; }
	inline void set_ironPresent_24(Text_t356221433 * value)
	{
		___ironPresent_24 = value;
		Il2CppCodeGenWriteBarrier(&___ironPresent_24, value);
	}

	inline static int32_t get_offset_of_copperPresent_25() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___copperPresent_25)); }
	inline Text_t356221433 * get_copperPresent_25() const { return ___copperPresent_25; }
	inline Text_t356221433 ** get_address_of_copperPresent_25() { return &___copperPresent_25; }
	inline void set_copperPresent_25(Text_t356221433 * value)
	{
		___copperPresent_25 = value;
		Il2CppCodeGenWriteBarrier(&___copperPresent_25, value);
	}

	inline static int32_t get_offset_of_silverPresent_26() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___silverPresent_26)); }
	inline Text_t356221433 * get_silverPresent_26() const { return ___silverPresent_26; }
	inline Text_t356221433 ** get_address_of_silverPresent_26() { return &___silverPresent_26; }
	inline void set_silverPresent_26(Text_t356221433 * value)
	{
		___silverPresent_26 = value;
		Il2CppCodeGenWriteBarrier(&___silverPresent_26, value);
	}

	inline static int32_t get_offset_of_goldPresent_27() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___goldPresent_27)); }
	inline Text_t356221433 * get_goldPresent_27() const { return ___goldPresent_27; }
	inline Text_t356221433 ** get_address_of_goldPresent_27() { return &___goldPresent_27; }
	inline void set_goldPresent_27(Text_t356221433 * value)
	{
		___goldPresent_27 = value;
		Il2CppCodeGenWriteBarrier(&___goldPresent_27, value);
	}

	inline static int32_t get_offset_of_unitsToRecruit_28() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unitsToRecruit_28)); }
	inline InputField_t1631627530 * get_unitsToRecruit_28() const { return ___unitsToRecruit_28; }
	inline InputField_t1631627530 ** get_address_of_unitsToRecruit_28() { return &___unitsToRecruit_28; }
	inline void set_unitsToRecruit_28(InputField_t1631627530 * value)
	{
		___unitsToRecruit_28 = value;
		Il2CppCodeGenWriteBarrier(&___unitsToRecruit_28, value);
	}

	inline static int32_t get_offset_of_unitName_29() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unitName_29)); }
	inline String_t* get_unitName_29() const { return ___unitName_29; }
	inline String_t** get_address_of_unitName_29() { return &___unitName_29; }
	inline void set_unitName_29(String_t* value)
	{
		___unitName_29 = value;
		Il2CppCodeGenWriteBarrier(&___unitName_29, value);
	}

	inline static int32_t get_offset_of_unit_30() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___unit_30)); }
	inline UnitConstantModel_t1807269633 * get_unit_30() const { return ___unit_30; }
	inline UnitConstantModel_t1807269633 ** get_address_of_unit_30() { return &___unit_30; }
	inline void set_unit_30(UnitConstantModel_t1807269633 * value)
	{
		___unit_30 = value;
		Il2CppCodeGenWriteBarrier(&___unit_30, value);
	}

	inline static int32_t get_offset_of_resources_31() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725, ___resources_31)); }
	inline ResourcesModel_t2978985958 * get_resources_31() const { return ___resources_31; }
	inline ResourcesModel_t2978985958 ** get_address_of_resources_31() { return &___resources_31; }
	inline void set_resources_31(ResourcesModel_t2978985958 * value)
	{
		___resources_31 = value;
		Il2CppCodeGenWriteBarrier(&___resources_31, value);
	}
};

struct UnitDetailsManager_t1603586725_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnitDetailsManager::<>f__switch$map17
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map17_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map17_32() { return static_cast<int32_t>(offsetof(UnitDetailsManager_t1603586725_StaticFields, ___U3CU3Ef__switchU24map17_32)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map17_32() const { return ___U3CU3Ef__switchU24map17_32; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map17_32() { return &___U3CU3Ef__switchU24map17_32; }
	inline void set_U3CU3Ef__switchU24map17_32(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map17_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map17_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
