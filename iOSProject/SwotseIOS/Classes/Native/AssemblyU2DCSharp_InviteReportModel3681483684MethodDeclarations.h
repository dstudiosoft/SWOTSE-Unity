﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InviteReportModel
struct InviteReportModel_t3681483684;

#include "codegen/il2cpp-codegen.h"

// System.Void InviteReportModel::.ctor()
extern "C"  void InviteReportModel__ctor_m1289014279 (InviteReportModel_t3681483684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
