﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_4_gen3997243488MethodDeclarations.h"

// System.Void System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m2070457699(__this, ___object0, ___method1, method) ((  void (*) (Func_4_t1475708928 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m3058358903_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>::Invoke(T1,T2,T3)
#define Func_4_Invoke_m2941143576(__this, ___arg10, ___arg21, ___arg32, method) ((  TouchPoint_t959629083 * (*) (Func_4_t1475708928 *, Vector2_t2243707579 , Tags_t1265380163 *, bool, const MethodInfo*))Func_4_Invoke_m1918486657_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m3356702168(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Func_4_t1475708928 *, Vector2_t2243707579 , Tags_t1265380163 *, bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m620803524_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TResult System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m2944141551(__this, ___result0, method) ((  TouchPoint_t959629083 * (*) (Func_4_t1475708928 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m3087466591_gshared)(__this, ___result0, method)
