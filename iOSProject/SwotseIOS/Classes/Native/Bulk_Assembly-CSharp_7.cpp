﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// WorldChat
struct WorldChat_t753147188;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// WorldChat/<BeginChat>c__Iterator0
struct U3CBeginChatU3Ec__Iterator0_t121862130;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// System.String
struct String_t;
// System.Uri
struct Uri_t100236324;
// WebSocket
struct WebSocket_t1645401340;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// JSONObject
struct JSONObject_t1339445639;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// WorldChunkManager
struct WorldChunkManager_t1513314516;
// SimpleEvent
struct SimpleEvent_t129249603;
// System.Delegate
struct Delegate_t1188392813;
// WorldChunkManager/<GetChunkInfo>c__Iterator0
struct U3CGetChunkInfoU3Ec__Iterator0_t549570326;
// UnityEngine.Transform
struct Transform_t3600365921;
// WorldFieldManager
struct WorldFieldManager_t2312070818;
// WorldFieldModel
struct WorldFieldModel_t2417974361;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557;
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t2811520381;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// SimpleUserModel
struct SimpleUserModel_t455560495;
// CityModel
struct CityModel_t1286289939;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t913674750;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// WindowInstanceManager
struct WindowInstanceManager_t3234774687;
// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel>
struct Dictionary_2_t3480614145;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t4142745948;
// WorldMapCoordsChanger
struct WorldMapCoordsChanger_t692571169;
// UnityEngine.Object
struct Object_t631007953;
// TestWorldChunksManager
struct TestWorldChunksManager_t1200850834;
// WorldMapTimer
struct WorldMapTimer_t60483741;
// ZoomManager
struct ZoomManager_t89017121;
// UnityEngine.Camera
struct Camera_t4157153871;
// JSONObject[]
struct JSONObjectU5BU5D_t1304767614;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// WebSocketSharp.WebSocket
struct WebSocket_t62038747;
// System.Collections.Generic.Queue`1<System.Byte[]>
struct Queue_1_t3962907151;
// CityManager
struct CityManager_t2587329200;
// BattleManager
struct BattleManager_t4022130644;
// LevelLoadManager
struct LevelLoadManager_t362334468;
// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel>
struct Dictionary_2_t2441201299;
// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel>
struct Dictionary_2_t3367445707;
// System.Collections.Generic.List`1<TutorialPageModel>
struct List_1_t1672770702;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// ResourceManager
struct ResourceManager_t484397614;
// UnitsManager
struct UnitsManager_t4062574081;
// BuildingsManager
struct BuildingsManager_t3721263023;
// TreasureManager
struct TreasureManager_t110095690;
// MyCityModel
struct MyCityModel_t3961736920;
// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel>
struct Dictionary_2_t729409408;
// ReportManager
struct ReportManager_t1122460921;
// AllianceManager
struct AllianceManager_t344187419;
// LoginManager
struct LoginManager_t1249555276;
// UserModel
struct UserModel_t1353931605;
// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel>
struct Dictionary_2_t3346051284;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// WorldFieldModel[]
struct WorldFieldModelU5BU5D_t237916708;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t1548932026;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,WorldFieldModel,System.Collections.DictionaryEntry>
struct Transform_1_t1008717335;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t2082808316;
// System.UriParser
struct UriParser_t3890150400;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1617499438;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// AllianceModel
struct AllianceModel_t2995969982;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// ActionManager
struct ActionManager_t2430268190;
// AllianceContentChanger
struct AllianceContentChanger_t2140085977;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// WorldFieldModel[0...,0...]
struct WorldFieldModelU5BU2CU5D_t237916709;
// UnityEngine.GameObject[0...,0...]
struct GameObjectU5B0___U2C0___U5D_t3328599147;
// PlayerManager
struct PlayerManager_t1349889689;
// GameManager
struct GameManager_t1536523654;
// GCMManager
struct GCMManager_t2058212271;
// CityChangerContentManager
struct CityChangerContentManager_t1075607021;
// PanelKnightsTableController
struct PanelKnightsTableController_t3023536378;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t149898510;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t648412432;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t467195904;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t2355412304;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.UI.FontData
struct FontData_t746620069;

extern RuntimeClass* U3CBeginChatU3Ec__Iterator0_t121862130_il2cpp_TypeInfo_var;
extern const uint32_t WorldChat_BeginChat_m246759783_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WorldChat_SendMessage_m305074220_MetadataUsageId;
extern RuntimeClass* GlobalGOScript_t724808191_il2cpp_TypeInfo_var;
extern RuntimeClass* Uri_t100236324_il2cpp_TypeInfo_var;
extern RuntimeClass* WebSocket_t1645401340_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* JSONObject_t1339445639_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3236910554;
extern String_t* _stringLiteral44433460;
extern String_t* _stringLiteral3451959283;
extern String_t* _stringLiteral2994480591;
extern String_t* _stringLiteral2637164798;
extern String_t* _stringLiteral3787497789;
extern String_t* _stringLiteral1653617040;
extern String_t* _stringLiteral3452614566;
extern String_t* _stringLiteral3434186627;
extern String_t* _stringLiteral3253941996;
extern String_t* _stringLiteral1503557889;
extern const uint32_t U3CBeginChatU3Ec__Iterator0_MoveNext_m1159218783_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CBeginChatU3Ec__Iterator0_Reset_m3958700887_MetadataUsageId;
extern RuntimeClass* SimpleEvent_t129249603_il2cpp_TypeInfo_var;
extern const uint32_t WorldChunkManager_add_onGetChunkInfo_m274685594_MetadataUsageId;
extern const uint32_t WorldChunkManager_remove_onGetChunkInfo_m3933643421_MetadataUsageId;
extern RuntimeClass* U3CGetChunkInfoU3Ec__Iterator0_t549570326_il2cpp_TypeInfo_var;
extern const uint32_t WorldChunkManager_GetChunkInfo_m887525653_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028_RuntimeMethod_var;
extern const RuntimeMethod* WorldChunkManager_InitChunkFields_m2411822551_RuntimeMethod_var;
extern const uint32_t WorldChunkManager_InitChunkFields_m2411822551_MetadataUsageId;
extern const uint32_t WorldChunkManager_ResetFieldsState_m1490371685_MetadataUsageId;
extern RuntimeClass* WWWForm_t4064702195_il2cpp_TypeInfo_var;
extern RuntimeClass* WorldFieldModelU5BU2CU5D_t237916709_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m2755735191_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisWorldFieldModel_t2417974361_m1764925164_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisSimpleUserModel_t455560495_m1828887640_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisCityModel_t1286289939_m1365451430_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3161834082_RuntimeMethod_var;
extern String_t* _stringLiteral2703599964;
extern String_t* _stringLiteral2703665500;
extern String_t* _stringLiteral2909097137;
extern String_t* _stringLiteral570444977;
extern String_t* _stringLiteral3099255805;
extern String_t* _stringLiteral4135483156;
extern String_t* _stringLiteral1573338805;
extern String_t* _stringLiteral2007429539;
extern String_t* _stringLiteral1619347993;
extern String_t* _stringLiteral2330854147;
extern const uint32_t U3CGetChunkInfoU3Ec__Iterator0_MoveNext_m2859353126_MetadataUsageId;
extern const uint32_t U3CGetChunkInfoU3Ec__Iterator0_Reset_m4067757616_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var;
extern const uint32_t WorldFieldManager_DisableField_m3970299500_MetadataUsageId;
extern const RuntimeMethod* Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m679968318_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisTextMesh_t1536577757_m1070281259_RuntimeMethod_var;
extern String_t* _stringLiteral3236852389;
extern String_t* _stringLiteral546498118;
extern String_t* _stringLiteral3451500417;
extern String_t* _stringLiteral2956737341;
extern String_t* _stringLiteral4069353026;
extern String_t* _stringLiteral4223840193;
extern String_t* _stringLiteral1496271294;
extern String_t* _stringLiteral2154736121;
extern String_t* _stringLiteral4107074515;
extern String_t* _stringLiteral11425075;
extern const uint32_t WorldFieldManager_InitFieldState_m4079297551_MetadataUsageId;
extern RuntimeClass* EventSystem_t1003666588_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m3662773728_RuntimeMethod_var;
extern String_t* _stringLiteral2330854179;
extern String_t* _stringLiteral2277606826;
extern String_t* _stringLiteral2788218763;
extern const uint32_t WorldFieldManager_OnMouseUpAsButton_m3354412269_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisTestWorldChunksManager_t1200850834_m598940813_RuntimeMethod_var;
extern String_t* _stringLiteral1581809604;
extern const uint32_t WorldMapCoordsChanger_OnEnable_m4271087035_MetadataUsageId;
extern const uint32_t WorldMapCoordsChanger_GoToCoords_m4009681775_MetadataUsageId;
extern const uint32_t WorldMapCoordsChanger_GoHome_m3771174939_MetadataUsageId;
struct DownloadHandler_t2937767557_marshaled_com;
struct UploadHandler_t2993558019_marshaled_com;

struct StringU5BU5D_t1281789340;
struct WorldFieldModelU5BU2CU5D_t237916709;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef LIST_1_T2811520381_H
#define LIST_1_T2811520381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<JSONObject>
struct  List_1_t2811520381  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JSONObjectU5BU5D_t1304767614* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2811520381, ____items_1)); }
	inline JSONObjectU5BU5D_t1304767614* get__items_1() const { return ____items_1; }
	inline JSONObjectU5BU5D_t1304767614** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JSONObjectU5BU5D_t1304767614* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2811520381, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2811520381, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2811520381_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	JSONObjectU5BU5D_t1304767614* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2811520381_StaticFields, ___EmptyArray_4)); }
	inline JSONObjectU5BU5D_t1304767614* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline JSONObjectU5BU5D_t1304767614** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(JSONObjectU5BU5D_t1304767614* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2811520381_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef WWWFORM_T4064702195_H
#define WWWFORM_T4064702195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t4064702195  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_t1293755103 * ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_t3319525431 * ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_t3319525431 * ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_t3319525431 * ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_t4116647657* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;

public:
	inline static int32_t get_offset_of_formData_0() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___formData_0)); }
	inline List_1_t1293755103 * get_formData_0() const { return ___formData_0; }
	inline List_1_t1293755103 ** get_address_of_formData_0() { return &___formData_0; }
	inline void set_formData_0(List_1_t1293755103 * value)
	{
		___formData_0 = value;
		Il2CppCodeGenWriteBarrier((&___formData_0), value);
	}

	inline static int32_t get_offset_of_fieldNames_1() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fieldNames_1)); }
	inline List_1_t3319525431 * get_fieldNames_1() const { return ___fieldNames_1; }
	inline List_1_t3319525431 ** get_address_of_fieldNames_1() { return &___fieldNames_1; }
	inline void set_fieldNames_1(List_1_t3319525431 * value)
	{
		___fieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldNames_1), value);
	}

	inline static int32_t get_offset_of_fileNames_2() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fileNames_2)); }
	inline List_1_t3319525431 * get_fileNames_2() const { return ___fileNames_2; }
	inline List_1_t3319525431 ** get_address_of_fileNames_2() { return &___fileNames_2; }
	inline void set_fileNames_2(List_1_t3319525431 * value)
	{
		___fileNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNames_2), value);
	}

	inline static int32_t get_offset_of_types_3() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___types_3)); }
	inline List_1_t3319525431 * get_types_3() const { return ___types_3; }
	inline List_1_t3319525431 ** get_address_of_types_3() { return &___types_3; }
	inline void set_types_3(List_1_t3319525431 * value)
	{
		___types_3 = value;
		Il2CppCodeGenWriteBarrier((&___types_3), value);
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___boundary_4)); }
	inline ByteU5BU5D_t4116647657* get_boundary_4() const { return ___boundary_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(ByteU5BU5D_t4116647657* value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_containsFiles_5() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___containsFiles_5)); }
	inline bool get_containsFiles_5() const { return ___containsFiles_5; }
	inline bool* get_address_of_containsFiles_5() { return &___containsFiles_5; }
	inline void set_containsFiles_5(bool value)
	{
		___containsFiles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T4064702195_H
#ifndef ALLIANCEMODEL_T2995969982_H
#define ALLIANCEMODEL_T2995969982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceModel
struct  AllianceModel_t2995969982  : public RuntimeObject
{
public:
	// System.Int64 AllianceModel::id
	int64_t ___id_0;
	// System.String AllianceModel::alliance_name
	String_t* ___alliance_name_1;
	// System.Int64 AllianceModel::founderId
	int64_t ___founderId_2;
	// System.String AllianceModel::founderName
	String_t* ___founderName_3;
	// System.Int64 AllianceModel::leaderId
	int64_t ___leaderId_4;
	// System.String AllianceModel::leaderName
	String_t* ___leaderName_5;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::allies
	List_1_t913674750 * ___allies_6;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::enemies
	List_1_t913674750 * ___enemies_7;
	// System.Int64 AllianceModel::rank
	int64_t ___rank_8;
	// System.Int64 AllianceModel::members_count
	int64_t ___members_count_9;
	// System.Int64 AllianceModel::alliance_experience
	int64_t ___alliance_experience_10;
	// System.String AllianceModel::guideline
	String_t* ___guideline_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_alliance_name_1() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___alliance_name_1)); }
	inline String_t* get_alliance_name_1() const { return ___alliance_name_1; }
	inline String_t** get_address_of_alliance_name_1() { return &___alliance_name_1; }
	inline void set_alliance_name_1(String_t* value)
	{
		___alliance_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_name_1), value);
	}

	inline static int32_t get_offset_of_founderId_2() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___founderId_2)); }
	inline int64_t get_founderId_2() const { return ___founderId_2; }
	inline int64_t* get_address_of_founderId_2() { return &___founderId_2; }
	inline void set_founderId_2(int64_t value)
	{
		___founderId_2 = value;
	}

	inline static int32_t get_offset_of_founderName_3() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___founderName_3)); }
	inline String_t* get_founderName_3() const { return ___founderName_3; }
	inline String_t** get_address_of_founderName_3() { return &___founderName_3; }
	inline void set_founderName_3(String_t* value)
	{
		___founderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___founderName_3), value);
	}

	inline static int32_t get_offset_of_leaderId_4() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___leaderId_4)); }
	inline int64_t get_leaderId_4() const { return ___leaderId_4; }
	inline int64_t* get_address_of_leaderId_4() { return &___leaderId_4; }
	inline void set_leaderId_4(int64_t value)
	{
		___leaderId_4 = value;
	}

	inline static int32_t get_offset_of_leaderName_5() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___leaderName_5)); }
	inline String_t* get_leaderName_5() const { return ___leaderName_5; }
	inline String_t** get_address_of_leaderName_5() { return &___leaderName_5; }
	inline void set_leaderName_5(String_t* value)
	{
		___leaderName_5 = value;
		Il2CppCodeGenWriteBarrier((&___leaderName_5), value);
	}

	inline static int32_t get_offset_of_allies_6() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___allies_6)); }
	inline List_1_t913674750 * get_allies_6() const { return ___allies_6; }
	inline List_1_t913674750 ** get_address_of_allies_6() { return &___allies_6; }
	inline void set_allies_6(List_1_t913674750 * value)
	{
		___allies_6 = value;
		Il2CppCodeGenWriteBarrier((&___allies_6), value);
	}

	inline static int32_t get_offset_of_enemies_7() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___enemies_7)); }
	inline List_1_t913674750 * get_enemies_7() const { return ___enemies_7; }
	inline List_1_t913674750 ** get_address_of_enemies_7() { return &___enemies_7; }
	inline void set_enemies_7(List_1_t913674750 * value)
	{
		___enemies_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemies_7), value);
	}

	inline static int32_t get_offset_of_rank_8() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___rank_8)); }
	inline int64_t get_rank_8() const { return ___rank_8; }
	inline int64_t* get_address_of_rank_8() { return &___rank_8; }
	inline void set_rank_8(int64_t value)
	{
		___rank_8 = value;
	}

	inline static int32_t get_offset_of_members_count_9() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___members_count_9)); }
	inline int64_t get_members_count_9() const { return ___members_count_9; }
	inline int64_t* get_address_of_members_count_9() { return &___members_count_9; }
	inline void set_members_count_9(int64_t value)
	{
		___members_count_9 = value;
	}

	inline static int32_t get_offset_of_alliance_experience_10() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___alliance_experience_10)); }
	inline int64_t get_alliance_experience_10() const { return ___alliance_experience_10; }
	inline int64_t* get_address_of_alliance_experience_10() { return &___alliance_experience_10; }
	inline void set_alliance_experience_10(int64_t value)
	{
		___alliance_experience_10 = value;
	}

	inline static int32_t get_offset_of_guideline_11() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___guideline_11)); }
	inline String_t* get_guideline_11() const { return ___guideline_11; }
	inline String_t** get_address_of_guideline_11() { return &___guideline_11; }
	inline void set_guideline_11(String_t* value)
	{
		___guideline_11 = value;
		Il2CppCodeGenWriteBarrier((&___guideline_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEMODEL_T2995969982_H
#ifndef WEBSOCKET_T1645401340_H
#define WEBSOCKET_T1645401340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket
struct  WebSocket_t1645401340  : public RuntimeObject
{
public:
	// System.Uri WebSocket::mUrl
	Uri_t100236324 * ___mUrl_0;
	// WebSocketSharp.WebSocket WebSocket::m_Socket
	WebSocket_t62038747 * ___m_Socket_1;
	// System.Collections.Generic.Queue`1<System.Byte[]> WebSocket::m_Messages
	Queue_1_t3962907151 * ___m_Messages_2;
	// System.Boolean WebSocket::m_IsConnected
	bool ___m_IsConnected_3;
	// System.String WebSocket::m_Error
	String_t* ___m_Error_4;

public:
	inline static int32_t get_offset_of_mUrl_0() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___mUrl_0)); }
	inline Uri_t100236324 * get_mUrl_0() const { return ___mUrl_0; }
	inline Uri_t100236324 ** get_address_of_mUrl_0() { return &___mUrl_0; }
	inline void set_mUrl_0(Uri_t100236324 * value)
	{
		___mUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUrl_0), value);
	}

	inline static int32_t get_offset_of_m_Socket_1() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Socket_1)); }
	inline WebSocket_t62038747 * get_m_Socket_1() const { return ___m_Socket_1; }
	inline WebSocket_t62038747 ** get_address_of_m_Socket_1() { return &___m_Socket_1; }
	inline void set_m_Socket_1(WebSocket_t62038747 * value)
	{
		___m_Socket_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Socket_1), value);
	}

	inline static int32_t get_offset_of_m_Messages_2() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Messages_2)); }
	inline Queue_1_t3962907151 * get_m_Messages_2() const { return ___m_Messages_2; }
	inline Queue_1_t3962907151 ** get_address_of_m_Messages_2() { return &___m_Messages_2; }
	inline void set_m_Messages_2(Queue_1_t3962907151 * value)
	{
		___m_Messages_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Messages_2), value);
	}

	inline static int32_t get_offset_of_m_IsConnected_3() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_IsConnected_3)); }
	inline bool get_m_IsConnected_3() const { return ___m_IsConnected_3; }
	inline bool* get_address_of_m_IsConnected_3() { return &___m_IsConnected_3; }
	inline void set_m_IsConnected_3(bool value)
	{
		___m_IsConnected_3 = value;
	}

	inline static int32_t get_offset_of_m_Error_4() { return static_cast<int32_t>(offsetof(WebSocket_t1645401340, ___m_Error_4)); }
	inline String_t* get_m_Error_4() const { return ___m_Error_4; }
	inline String_t** get_address_of_m_Error_4() { return &___m_Error_4; }
	inline void set_m_Error_4(String_t* value)
	{
		___m_Error_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Error_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T1645401340_H
#ifndef WORLDFIELDMODEL_T2417974361_H
#define WORLDFIELDMODEL_T2417974361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldModel
struct  WorldFieldModel_t2417974361  : public RuntimeObject
{
public:
	// System.Int64 WorldFieldModel::id
	int64_t ___id_0;
	// System.Int64 WorldFieldModel::posX
	int64_t ___posX_1;
	// System.Int64 WorldFieldModel::posY
	int64_t ___posY_2;
	// System.String WorldFieldModel::cityType
	String_t* ___cityType_3;
	// CityModel WorldFieldModel::city
	CityModel_t1286289939 * ___city_4;
	// SimpleUserModel WorldFieldModel::owner
	SimpleUserModel_t455560495 * ___owner_5;
	// System.Int64 WorldFieldModel::level
	int64_t ___level_6;
	// System.Int64 WorldFieldModel::production
	int64_t ___production_7;
	// System.String WorldFieldModel::region
	String_t* ___region_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_posX_1() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___posX_1)); }
	inline int64_t get_posX_1() const { return ___posX_1; }
	inline int64_t* get_address_of_posX_1() { return &___posX_1; }
	inline void set_posX_1(int64_t value)
	{
		___posX_1 = value;
	}

	inline static int32_t get_offset_of_posY_2() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___posY_2)); }
	inline int64_t get_posY_2() const { return ___posY_2; }
	inline int64_t* get_address_of_posY_2() { return &___posY_2; }
	inline void set_posY_2(int64_t value)
	{
		___posY_2 = value;
	}

	inline static int32_t get_offset_of_cityType_3() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___cityType_3)); }
	inline String_t* get_cityType_3() const { return ___cityType_3; }
	inline String_t** get_address_of_cityType_3() { return &___cityType_3; }
	inline void set_cityType_3(String_t* value)
	{
		___cityType_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityType_3), value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___city_4)); }
	inline CityModel_t1286289939 * get_city_4() const { return ___city_4; }
	inline CityModel_t1286289939 ** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(CityModel_t1286289939 * value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier((&___city_4), value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___owner_5)); }
	inline SimpleUserModel_t455560495 * get_owner_5() const { return ___owner_5; }
	inline SimpleUserModel_t455560495 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(SimpleUserModel_t455560495 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}

	inline static int32_t get_offset_of_level_6() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___level_6)); }
	inline int64_t get_level_6() const { return ___level_6; }
	inline int64_t* get_address_of_level_6() { return &___level_6; }
	inline void set_level_6(int64_t value)
	{
		___level_6 = value;
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___production_7)); }
	inline int64_t get_production_7() const { return ___production_7; }
	inline int64_t* get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(int64_t value)
	{
		___production_7 = value;
	}

	inline static int32_t get_offset_of_region_8() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___region_8)); }
	inline String_t* get_region_8() const { return ___region_8; }
	inline String_t** get_address_of_region_8() { return &___region_8; }
	inline void set_region_8(String_t* value)
	{
		___region_8 = value;
		Il2CppCodeGenWriteBarrier((&___region_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDFIELDMODEL_T2417974361_H
#ifndef U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#define U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChunkManager/<GetChunkInfo>c__Iterator0
struct  U3CGetChunkInfoU3Ec__Iterator0_t549570326  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WorldChunkManager/<GetChunkInfo>c__Iterator0::<unitForm>__0
	WWWForm_t4064702195 * ___U3CunitFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest WorldChunkManager/<GetChunkInfo>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// WorldChunkManager WorldChunkManager/<GetChunkInfo>c__Iterator0::$this
	WorldChunkManager_t1513314516 * ___U24this_2;
	// System.Object WorldChunkManager/<GetChunkInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WorldChunkManager/<GetChunkInfo>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WorldChunkManager/<GetChunkInfo>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CunitFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U3CunitFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CunitFormU3E__0_0() const { return ___U3CunitFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CunitFormU3E__0_0() { return &___U3CunitFormU3E__0_0; }
	inline void set_U3CunitFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CunitFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunitFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24this_2)); }
	inline WorldChunkManager_t1513314516 * get_U24this_2() const { return ___U24this_2; }
	inline WorldChunkManager_t1513314516 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WorldChunkManager_t1513314516 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetChunkInfoU3Ec__Iterator0_t549570326, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCHUNKINFOU3EC__ITERATOR0_T549570326_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public RuntimeObject
{
public:
	// CityManager GameManager::cityManager
	CityManager_t2587329200 * ___cityManager_0;
	// BattleManager GameManager::battleManager
	BattleManager_t4022130644 * ___battleManager_1;
	// LevelLoadManager GameManager::levelLoadManager
	LevelLoadManager_t362334468 * ___levelLoadManager_2;
	// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel> GameManager::buildingConstants
	Dictionary_2_t2441201299 * ___buildingConstants_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel> GameManager::unitConstants
	Dictionary_2_t3367445707 * ___unitConstants_4;
	// System.Collections.Generic.List`1<TutorialPageModel> GameManager::tutorials
	List_1_t1672770702 * ___tutorials_5;
	// System.Int64[] GameManager::residenceFields
	Int64U5BU5D_t2559172825* ___residenceFields_6;
	// SimpleEvent GameManager::onGetBuildingNames
	SimpleEvent_t129249603 * ___onGetBuildingNames_7;
	// System.Collections.Generic.List`1<System.String> GameManager::buildingIds
	List_1_t3319525431 * ___buildingIds_8;

public:
	inline static int32_t get_offset_of_cityManager_0() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___cityManager_0)); }
	inline CityManager_t2587329200 * get_cityManager_0() const { return ___cityManager_0; }
	inline CityManager_t2587329200 ** get_address_of_cityManager_0() { return &___cityManager_0; }
	inline void set_cityManager_0(CityManager_t2587329200 * value)
	{
		___cityManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityManager_0), value);
	}

	inline static int32_t get_offset_of_battleManager_1() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___battleManager_1)); }
	inline BattleManager_t4022130644 * get_battleManager_1() const { return ___battleManager_1; }
	inline BattleManager_t4022130644 ** get_address_of_battleManager_1() { return &___battleManager_1; }
	inline void set_battleManager_1(BattleManager_t4022130644 * value)
	{
		___battleManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___battleManager_1), value);
	}

	inline static int32_t get_offset_of_levelLoadManager_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___levelLoadManager_2)); }
	inline LevelLoadManager_t362334468 * get_levelLoadManager_2() const { return ___levelLoadManager_2; }
	inline LevelLoadManager_t362334468 ** get_address_of_levelLoadManager_2() { return &___levelLoadManager_2; }
	inline void set_levelLoadManager_2(LevelLoadManager_t362334468 * value)
	{
		___levelLoadManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelLoadManager_2), value);
	}

	inline static int32_t get_offset_of_buildingConstants_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___buildingConstants_3)); }
	inline Dictionary_2_t2441201299 * get_buildingConstants_3() const { return ___buildingConstants_3; }
	inline Dictionary_2_t2441201299 ** get_address_of_buildingConstants_3() { return &___buildingConstants_3; }
	inline void set_buildingConstants_3(Dictionary_2_t2441201299 * value)
	{
		___buildingConstants_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingConstants_3), value);
	}

	inline static int32_t get_offset_of_unitConstants_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___unitConstants_4)); }
	inline Dictionary_2_t3367445707 * get_unitConstants_4() const { return ___unitConstants_4; }
	inline Dictionary_2_t3367445707 ** get_address_of_unitConstants_4() { return &___unitConstants_4; }
	inline void set_unitConstants_4(Dictionary_2_t3367445707 * value)
	{
		___unitConstants_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitConstants_4), value);
	}

	inline static int32_t get_offset_of_tutorials_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___tutorials_5)); }
	inline List_1_t1672770702 * get_tutorials_5() const { return ___tutorials_5; }
	inline List_1_t1672770702 ** get_address_of_tutorials_5() { return &___tutorials_5; }
	inline void set_tutorials_5(List_1_t1672770702 * value)
	{
		___tutorials_5 = value;
		Il2CppCodeGenWriteBarrier((&___tutorials_5), value);
	}

	inline static int32_t get_offset_of_residenceFields_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___residenceFields_6)); }
	inline Int64U5BU5D_t2559172825* get_residenceFields_6() const { return ___residenceFields_6; }
	inline Int64U5BU5D_t2559172825** get_address_of_residenceFields_6() { return &___residenceFields_6; }
	inline void set_residenceFields_6(Int64U5BU5D_t2559172825* value)
	{
		___residenceFields_6 = value;
		Il2CppCodeGenWriteBarrier((&___residenceFields_6), value);
	}

	inline static int32_t get_offset_of_onGetBuildingNames_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___onGetBuildingNames_7)); }
	inline SimpleEvent_t129249603 * get_onGetBuildingNames_7() const { return ___onGetBuildingNames_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetBuildingNames_7() { return &___onGetBuildingNames_7; }
	inline void set_onGetBuildingNames_7(SimpleEvent_t129249603 * value)
	{
		___onGetBuildingNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___onGetBuildingNames_7), value);
	}

	inline static int32_t get_offset_of_buildingIds_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___buildingIds_8)); }
	inline List_1_t3319525431 * get_buildingIds_8() const { return ___buildingIds_8; }
	inline List_1_t3319525431 ** get_address_of_buildingIds_8() { return &___buildingIds_8; }
	inline void set_buildingIds_8(List_1_t3319525431 * value)
	{
		___buildingIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___buildingIds_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef CITYMANAGER_T2587329200_H
#define CITYMANAGER_T2587329200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager
struct  CityManager_t2587329200  : public RuntimeObject
{
public:
	// ResourceManager CityManager::resourceManager
	ResourceManager_t484397614 * ___resourceManager_0;
	// UnitsManager CityManager::unitsManager
	UnitsManager_t4062574081 * ___unitsManager_1;
	// BuildingsManager CityManager::buildingsManager
	BuildingsManager_t3721263023 * ___buildingsManager_2;
	// TreasureManager CityManager::treasureManager
	TreasureManager_t110095690 * ___treasureManager_3;
	// SimpleEvent CityManager::onCityBuilt
	SimpleEvent_t129249603 * ___onCityBuilt_4;
	// SimpleEvent CityManager::onGotNewCity
	SimpleEvent_t129249603 * ___onGotNewCity_5;
	// SimpleEvent CityManager::onCityNameAndIconChanged
	SimpleEvent_t129249603 * ___onCityNameAndIconChanged_6;
	// SimpleEvent CityManager::cityChangerUpdateImage
	SimpleEvent_t129249603 * ___cityChangerUpdateImage_7;
	// SimpleEvent CityManager::gotAllowTroopsState
	SimpleEvent_t129249603 * ___gotAllowTroopsState_8;
	// SimpleEvent CityManager::onTaxUpdated
	SimpleEvent_t129249603 * ___onTaxUpdated_9;
	// MyCityModel CityManager::currentCity
	MyCityModel_t3961736920 * ___currentCity_10;
	// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel> CityManager::myCities
	Dictionary_2_t729409408 * ___myCities_11;

public:
	inline static int32_t get_offset_of_resourceManager_0() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___resourceManager_0)); }
	inline ResourceManager_t484397614 * get_resourceManager_0() const { return ___resourceManager_0; }
	inline ResourceManager_t484397614 ** get_address_of_resourceManager_0() { return &___resourceManager_0; }
	inline void set_resourceManager_0(ResourceManager_t484397614 * value)
	{
		___resourceManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourceManager_0), value);
	}

	inline static int32_t get_offset_of_unitsManager_1() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___unitsManager_1)); }
	inline UnitsManager_t4062574081 * get_unitsManager_1() const { return ___unitsManager_1; }
	inline UnitsManager_t4062574081 ** get_address_of_unitsManager_1() { return &___unitsManager_1; }
	inline void set_unitsManager_1(UnitsManager_t4062574081 * value)
	{
		___unitsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___unitsManager_1), value);
	}

	inline static int32_t get_offset_of_buildingsManager_2() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___buildingsManager_2)); }
	inline BuildingsManager_t3721263023 * get_buildingsManager_2() const { return ___buildingsManager_2; }
	inline BuildingsManager_t3721263023 ** get_address_of_buildingsManager_2() { return &___buildingsManager_2; }
	inline void set_buildingsManager_2(BuildingsManager_t3721263023 * value)
	{
		___buildingsManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsManager_2), value);
	}

	inline static int32_t get_offset_of_treasureManager_3() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___treasureManager_3)); }
	inline TreasureManager_t110095690 * get_treasureManager_3() const { return ___treasureManager_3; }
	inline TreasureManager_t110095690 ** get_address_of_treasureManager_3() { return &___treasureManager_3; }
	inline void set_treasureManager_3(TreasureManager_t110095690 * value)
	{
		___treasureManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___treasureManager_3), value);
	}

	inline static int32_t get_offset_of_onCityBuilt_4() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onCityBuilt_4)); }
	inline SimpleEvent_t129249603 * get_onCityBuilt_4() const { return ___onCityBuilt_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onCityBuilt_4() { return &___onCityBuilt_4; }
	inline void set_onCityBuilt_4(SimpleEvent_t129249603 * value)
	{
		___onCityBuilt_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCityBuilt_4), value);
	}

	inline static int32_t get_offset_of_onGotNewCity_5() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onGotNewCity_5)); }
	inline SimpleEvent_t129249603 * get_onGotNewCity_5() const { return ___onGotNewCity_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onGotNewCity_5() { return &___onGotNewCity_5; }
	inline void set_onGotNewCity_5(SimpleEvent_t129249603 * value)
	{
		___onGotNewCity_5 = value;
		Il2CppCodeGenWriteBarrier((&___onGotNewCity_5), value);
	}

	inline static int32_t get_offset_of_onCityNameAndIconChanged_6() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onCityNameAndIconChanged_6)); }
	inline SimpleEvent_t129249603 * get_onCityNameAndIconChanged_6() const { return ___onCityNameAndIconChanged_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onCityNameAndIconChanged_6() { return &___onCityNameAndIconChanged_6; }
	inline void set_onCityNameAndIconChanged_6(SimpleEvent_t129249603 * value)
	{
		___onCityNameAndIconChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___onCityNameAndIconChanged_6), value);
	}

	inline static int32_t get_offset_of_cityChangerUpdateImage_7() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___cityChangerUpdateImage_7)); }
	inline SimpleEvent_t129249603 * get_cityChangerUpdateImage_7() const { return ___cityChangerUpdateImage_7; }
	inline SimpleEvent_t129249603 ** get_address_of_cityChangerUpdateImage_7() { return &___cityChangerUpdateImage_7; }
	inline void set_cityChangerUpdateImage_7(SimpleEvent_t129249603 * value)
	{
		___cityChangerUpdateImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityChangerUpdateImage_7), value);
	}

	inline static int32_t get_offset_of_gotAllowTroopsState_8() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___gotAllowTroopsState_8)); }
	inline SimpleEvent_t129249603 * get_gotAllowTroopsState_8() const { return ___gotAllowTroopsState_8; }
	inline SimpleEvent_t129249603 ** get_address_of_gotAllowTroopsState_8() { return &___gotAllowTroopsState_8; }
	inline void set_gotAllowTroopsState_8(SimpleEvent_t129249603 * value)
	{
		___gotAllowTroopsState_8 = value;
		Il2CppCodeGenWriteBarrier((&___gotAllowTroopsState_8), value);
	}

	inline static int32_t get_offset_of_onTaxUpdated_9() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onTaxUpdated_9)); }
	inline SimpleEvent_t129249603 * get_onTaxUpdated_9() const { return ___onTaxUpdated_9; }
	inline SimpleEvent_t129249603 ** get_address_of_onTaxUpdated_9() { return &___onTaxUpdated_9; }
	inline void set_onTaxUpdated_9(SimpleEvent_t129249603 * value)
	{
		___onTaxUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTaxUpdated_9), value);
	}

	inline static int32_t get_offset_of_currentCity_10() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___currentCity_10)); }
	inline MyCityModel_t3961736920 * get_currentCity_10() const { return ___currentCity_10; }
	inline MyCityModel_t3961736920 ** get_address_of_currentCity_10() { return &___currentCity_10; }
	inline void set_currentCity_10(MyCityModel_t3961736920 * value)
	{
		___currentCity_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentCity_10), value);
	}

	inline static int32_t get_offset_of_myCities_11() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___myCities_11)); }
	inline Dictionary_2_t729409408 * get_myCities_11() const { return ___myCities_11; }
	inline Dictionary_2_t729409408 ** get_address_of_myCities_11() { return &___myCities_11; }
	inline void set_myCities_11(Dictionary_2_t729409408 * value)
	{
		___myCities_11 = value;
		Il2CppCodeGenWriteBarrier((&___myCities_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYMANAGER_T2587329200_H
#ifndef PLAYERMANAGER_T1349889689_H
#define PLAYERMANAGER_T1349889689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1349889689  : public RuntimeObject
{
public:
	// ReportManager PlayerManager::reportManager
	ReportManager_t1122460921 * ___reportManager_0;
	// AllianceManager PlayerManager::allianceManager
	AllianceManager_t344187419 * ___allianceManager_1;
	// LoginManager PlayerManager::loginManager
	LoginManager_t1249555276 * ___loginManager_2;
	// UserModel PlayerManager::user
	UserModel_t1353931605 * ___user_3;
	// SimpleEvent PlayerManager::onGetMyUserInfo
	SimpleEvent_t129249603 * ___onGetMyUserInfo_4;
	// SimpleEvent PlayerManager::onLanguageChanged
	SimpleEvent_t129249603 * ___onLanguageChanged_5;
	// SimpleEvent PlayerManager::onPasswordChanged
	SimpleEvent_t129249603 * ___onPasswordChanged_6;
	// SimpleEvent PlayerManager::onResetPasswordRequested
	SimpleEvent_t129249603 * ___onResetPasswordRequested_7;
	// SimpleEvent PlayerManager::onPasswordReset
	SimpleEvent_t129249603 * ___onPasswordReset_8;

public:
	inline static int32_t get_offset_of_reportManager_0() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___reportManager_0)); }
	inline ReportManager_t1122460921 * get_reportManager_0() const { return ___reportManager_0; }
	inline ReportManager_t1122460921 ** get_address_of_reportManager_0() { return &___reportManager_0; }
	inline void set_reportManager_0(ReportManager_t1122460921 * value)
	{
		___reportManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___reportManager_0), value);
	}

	inline static int32_t get_offset_of_allianceManager_1() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___allianceManager_1)); }
	inline AllianceManager_t344187419 * get_allianceManager_1() const { return ___allianceManager_1; }
	inline AllianceManager_t344187419 ** get_address_of_allianceManager_1() { return &___allianceManager_1; }
	inline void set_allianceManager_1(AllianceManager_t344187419 * value)
	{
		___allianceManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___allianceManager_1), value);
	}

	inline static int32_t get_offset_of_loginManager_2() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___loginManager_2)); }
	inline LoginManager_t1249555276 * get_loginManager_2() const { return ___loginManager_2; }
	inline LoginManager_t1249555276 ** get_address_of_loginManager_2() { return &___loginManager_2; }
	inline void set_loginManager_2(LoginManager_t1249555276 * value)
	{
		___loginManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginManager_2), value);
	}

	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___user_3)); }
	inline UserModel_t1353931605 * get_user_3() const { return ___user_3; }
	inline UserModel_t1353931605 ** get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(UserModel_t1353931605 * value)
	{
		___user_3 = value;
		Il2CppCodeGenWriteBarrier((&___user_3), value);
	}

	inline static int32_t get_offset_of_onGetMyUserInfo_4() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onGetMyUserInfo_4)); }
	inline SimpleEvent_t129249603 * get_onGetMyUserInfo_4() const { return ___onGetMyUserInfo_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetMyUserInfo_4() { return &___onGetMyUserInfo_4; }
	inline void set_onGetMyUserInfo_4(SimpleEvent_t129249603 * value)
	{
		___onGetMyUserInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGetMyUserInfo_4), value);
	}

	inline static int32_t get_offset_of_onLanguageChanged_5() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onLanguageChanged_5)); }
	inline SimpleEvent_t129249603 * get_onLanguageChanged_5() const { return ___onLanguageChanged_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onLanguageChanged_5() { return &___onLanguageChanged_5; }
	inline void set_onLanguageChanged_5(SimpleEvent_t129249603 * value)
	{
		___onLanguageChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___onLanguageChanged_5), value);
	}

	inline static int32_t get_offset_of_onPasswordChanged_6() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onPasswordChanged_6)); }
	inline SimpleEvent_t129249603 * get_onPasswordChanged_6() const { return ___onPasswordChanged_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onPasswordChanged_6() { return &___onPasswordChanged_6; }
	inline void set_onPasswordChanged_6(SimpleEvent_t129249603 * value)
	{
		___onPasswordChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPasswordChanged_6), value);
	}

	inline static int32_t get_offset_of_onResetPasswordRequested_7() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onResetPasswordRequested_7)); }
	inline SimpleEvent_t129249603 * get_onResetPasswordRequested_7() const { return ___onResetPasswordRequested_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onResetPasswordRequested_7() { return &___onResetPasswordRequested_7; }
	inline void set_onResetPasswordRequested_7(SimpleEvent_t129249603 * value)
	{
		___onResetPasswordRequested_7 = value;
		Il2CppCodeGenWriteBarrier((&___onResetPasswordRequested_7), value);
	}

	inline static int32_t get_offset_of_onPasswordReset_8() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onPasswordReset_8)); }
	inline SimpleEvent_t129249603 * get_onPasswordReset_8() const { return ___onPasswordReset_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onPasswordReset_8() { return &___onPasswordReset_8; }
	inline void set_onPasswordReset_8(SimpleEvent_t129249603 * value)
	{
		___onPasswordReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPasswordReset_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T1349889689_H
#ifndef LIST_1_T913674750_H
#define LIST_1_T913674750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int64>
struct  List_1_t913674750  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int64U5BU5D_t2559172825* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t913674750, ____items_1)); }
	inline Int64U5BU5D_t2559172825* get__items_1() const { return ____items_1; }
	inline Int64U5BU5D_t2559172825** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int64U5BU5D_t2559172825* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t913674750, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t913674750, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t913674750_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int64U5BU5D_t2559172825* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t913674750_StaticFields, ___EmptyArray_4)); }
	inline Int64U5BU5D_t2559172825* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int64U5BU5D_t2559172825** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int64U5BU5D_t2559172825* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T913674750_H
#ifndef BUILDINGSMANAGER_T3721263023_H
#define BUILDINGSMANAGER_T3721263023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager
struct  BuildingsManager_t3721263023  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityBuildings
	Dictionary_2_t3346051284 * ___cityBuildings_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityFields
	Dictionary_2_t3346051284 * ___cityFields_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel> BuildingsManager::cityValleys
	Dictionary_2_t3480614145 * ___cityValleys_2;
	// SimpleEvent BuildingsManager::onBuildingUpdate
	SimpleEvent_t129249603 * ___onBuildingUpdate_3;
	// SimpleEvent BuildingsManager::onGotValleys
	SimpleEvent_t129249603 * ___onGotValleys_4;
	// SimpleEvent BuildingsManager::onBuildingCancelled
	SimpleEvent_t129249603 * ___onBuildingCancelled_5;

public:
	inline static int32_t get_offset_of_cityBuildings_0() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityBuildings_0)); }
	inline Dictionary_2_t3346051284 * get_cityBuildings_0() const { return ___cityBuildings_0; }
	inline Dictionary_2_t3346051284 ** get_address_of_cityBuildings_0() { return &___cityBuildings_0; }
	inline void set_cityBuildings_0(Dictionary_2_t3346051284 * value)
	{
		___cityBuildings_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityBuildings_0), value);
	}

	inline static int32_t get_offset_of_cityFields_1() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityFields_1)); }
	inline Dictionary_2_t3346051284 * get_cityFields_1() const { return ___cityFields_1; }
	inline Dictionary_2_t3346051284 ** get_address_of_cityFields_1() { return &___cityFields_1; }
	inline void set_cityFields_1(Dictionary_2_t3346051284 * value)
	{
		___cityFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___cityFields_1), value);
	}

	inline static int32_t get_offset_of_cityValleys_2() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___cityValleys_2)); }
	inline Dictionary_2_t3480614145 * get_cityValleys_2() const { return ___cityValleys_2; }
	inline Dictionary_2_t3480614145 ** get_address_of_cityValleys_2() { return &___cityValleys_2; }
	inline void set_cityValleys_2(Dictionary_2_t3480614145 * value)
	{
		___cityValleys_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityValleys_2), value);
	}

	inline static int32_t get_offset_of_onBuildingUpdate_3() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onBuildingUpdate_3)); }
	inline SimpleEvent_t129249603 * get_onBuildingUpdate_3() const { return ___onBuildingUpdate_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onBuildingUpdate_3() { return &___onBuildingUpdate_3; }
	inline void set_onBuildingUpdate_3(SimpleEvent_t129249603 * value)
	{
		___onBuildingUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBuildingUpdate_3), value);
	}

	inline static int32_t get_offset_of_onGotValleys_4() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onGotValleys_4)); }
	inline SimpleEvent_t129249603 * get_onGotValleys_4() const { return ___onGotValleys_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGotValleys_4() { return &___onGotValleys_4; }
	inline void set_onGotValleys_4(SimpleEvent_t129249603 * value)
	{
		___onGotValleys_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGotValleys_4), value);
	}

	inline static int32_t get_offset_of_onBuildingCancelled_5() { return static_cast<int32_t>(offsetof(BuildingsManager_t3721263023, ___onBuildingCancelled_5)); }
	inline SimpleEvent_t129249603 * get_onBuildingCancelled_5() const { return ___onBuildingCancelled_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onBuildingCancelled_5() { return &___onBuildingCancelled_5; }
	inline void set_onBuildingCancelled_5(SimpleEvent_t129249603 * value)
	{
		___onBuildingCancelled_5 = value;
		Il2CppCodeGenWriteBarrier((&___onBuildingCancelled_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGSMANAGER_T3721263023_H
#ifndef CITYMODEL_T1286289939_H
#define CITYMODEL_T1286289939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityModel
struct  CityModel_t1286289939  : public RuntimeObject
{
public:
	// System.Int64 CityModel::id
	int64_t ___id_0;
	// System.Int64 CityModel::owner
	int64_t ___owner_1;
	// System.String CityModel::city_name
	String_t* ___city_name_2;
	// System.String CityModel::image_name
	String_t* ___image_name_3;
	// System.String CityModel::occupantCityOwnerName
	String_t* ___occupantCityOwnerName_4;
	// System.String CityModel::occupantCityName
	String_t* ___occupantCityName_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___owner_1)); }
	inline int64_t get_owner_1() const { return ___owner_1; }
	inline int64_t* get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(int64_t value)
	{
		___owner_1 = value;
	}

	inline static int32_t get_offset_of_city_name_2() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___city_name_2)); }
	inline String_t* get_city_name_2() const { return ___city_name_2; }
	inline String_t** get_address_of_city_name_2() { return &___city_name_2; }
	inline void set_city_name_2(String_t* value)
	{
		___city_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___city_name_2), value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_3), value);
	}

	inline static int32_t get_offset_of_occupantCityOwnerName_4() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___occupantCityOwnerName_4)); }
	inline String_t* get_occupantCityOwnerName_4() const { return ___occupantCityOwnerName_4; }
	inline String_t** get_address_of_occupantCityOwnerName_4() { return &___occupantCityOwnerName_4; }
	inline void set_occupantCityOwnerName_4(String_t* value)
	{
		___occupantCityOwnerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___occupantCityOwnerName_4), value);
	}

	inline static int32_t get_offset_of_occupantCityName_5() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___occupantCityName_5)); }
	inline String_t* get_occupantCityName_5() const { return ___occupantCityName_5; }
	inline String_t** get_address_of_occupantCityName_5() { return &___occupantCityName_5; }
	inline void set_occupantCityName_5(String_t* value)
	{
		___occupantCityName_5 = value;
		Il2CppCodeGenWriteBarrier((&___occupantCityName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYMODEL_T1286289939_H
#ifndef SIMPLEUSERMODEL_T455560495_H
#define SIMPLEUSERMODEL_T455560495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleUserModel
struct  SimpleUserModel_t455560495  : public RuntimeObject
{
public:
	// System.Int64 SimpleUserModel::id
	int64_t ___id_0;
	// System.String SimpleUserModel::username
	String_t* ___username_1;
	// System.String SimpleUserModel::rank
	String_t* ___rank_2;
	// System.Int64 SimpleUserModel::emperor_id
	int64_t ___emperor_id_3;
	// System.String SimpleUserModel::status
	String_t* ___status_4;
	// System.Int64 SimpleUserModel::experience
	int64_t ___experience_5;
	// System.String SimpleUserModel::alliance_name
	String_t* ___alliance_name_6;
	// System.Int64 SimpleUserModel::allianceID
	int64_t ___allianceID_7;
	// System.Int64 SimpleUserModel::capitalID
	int64_t ___capitalID_8;
	// System.String SimpleUserModel::image_name
	String_t* ___image_name_9;
	// System.String SimpleUserModel::flag
	String_t* ___flag_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier((&___rank_2), value);
	}

	inline static int32_t get_offset_of_emperor_id_3() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___emperor_id_3)); }
	inline int64_t get_emperor_id_3() const { return ___emperor_id_3; }
	inline int64_t* get_address_of_emperor_id_3() { return &___emperor_id_3; }
	inline void set_emperor_id_3(int64_t value)
	{
		___emperor_id_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___status_4)); }
	inline String_t* get_status_4() const { return ___status_4; }
	inline String_t** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(String_t* value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier((&___status_4), value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___experience_5)); }
	inline int64_t get_experience_5() const { return ___experience_5; }
	inline int64_t* get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(int64_t value)
	{
		___experience_5 = value;
	}

	inline static int32_t get_offset_of_alliance_name_6() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___alliance_name_6)); }
	inline String_t* get_alliance_name_6() const { return ___alliance_name_6; }
	inline String_t** get_address_of_alliance_name_6() { return &___alliance_name_6; }
	inline void set_alliance_name_6(String_t* value)
	{
		___alliance_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_name_6), value);
	}

	inline static int32_t get_offset_of_allianceID_7() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___allianceID_7)); }
	inline int64_t get_allianceID_7() const { return ___allianceID_7; }
	inline int64_t* get_address_of_allianceID_7() { return &___allianceID_7; }
	inline void set_allianceID_7(int64_t value)
	{
		___allianceID_7 = value;
	}

	inline static int32_t get_offset_of_capitalID_8() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___capitalID_8)); }
	inline int64_t get_capitalID_8() const { return ___capitalID_8; }
	inline int64_t* get_address_of_capitalID_8() { return &___capitalID_8; }
	inline void set_capitalID_8(int64_t value)
	{
		___capitalID_8 = value;
	}

	inline static int32_t get_offset_of_image_name_9() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___image_name_9)); }
	inline String_t* get_image_name_9() const { return ___image_name_9; }
	inline String_t** get_address_of_image_name_9() { return &___image_name_9; }
	inline void set_image_name_9(String_t* value)
	{
		___image_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___flag_10)); }
	inline String_t* get_flag_10() const { return ___flag_10; }
	inline String_t** get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(String_t* value)
	{
		___flag_10 = value;
		Il2CppCodeGenWriteBarrier((&___flag_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEUSERMODEL_T455560495_H
#ifndef DICTIONARY_2_T3480614145_H
#define DICTIONARY_2_T3480614145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel>
struct  Dictionary_2_t3480614145  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int64U5BU5D_t2559172825* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	WorldFieldModelU5BU5D_t237916708* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___keySlots_6)); }
	inline Int64U5BU5D_t2559172825* get_keySlots_6() const { return ___keySlots_6; }
	inline Int64U5BU5D_t2559172825** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int64U5BU5D_t2559172825* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___valueSlots_7)); }
	inline WorldFieldModelU5BU5D_t237916708* get_valueSlots_7() const { return ___valueSlots_7; }
	inline WorldFieldModelU5BU5D_t237916708** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(WorldFieldModelU5BU5D_t237916708* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3480614145_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1008717335 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3480614145_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1008717335 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1008717335 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1008717335 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3480614145_H
#ifndef MYCITYMODEL_T3961736920_H
#define MYCITYMODEL_T3961736920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyCityModel
struct  MyCityModel_t3961736920  : public RuntimeObject
{
public:
	// System.Int64 MyCityModel::id
	int64_t ___id_0;
	// System.String MyCityModel::city_name
	String_t* ___city_name_1;
	// System.Int64 MyCityModel::posX
	int64_t ___posX_2;
	// System.Int64 MyCityModel::posY
	int64_t ___posY_3;
	// System.String MyCityModel::region
	String_t* ___region_4;
	// System.String MyCityModel::image_name
	String_t* ___image_name_5;
	// System.Int64 MyCityModel::max_population
	int64_t ___max_population_6;
	// System.Int64 MyCityModel::current_population
	int64_t ___current_population_7;
	// System.Int64 MyCityModel::happiness
	int64_t ___happiness_8;
	// System.Int64 MyCityModel::courage
	int64_t ___courage_9;
	// System.Boolean MyCityModel::isExpantion
	bool ___isExpantion_10;
	// System.Boolean MyCityModel::reinforce_flag
	bool ___reinforce_flag_11;
	// System.Int64 MyCityModel::gold_tax_rate
	int64_t ___gold_tax_rate_12;
	// System.Int64 MyCityModel::fields_count
	int64_t ___fields_count_13;
	// System.Int64 MyCityModel::valleys_count
	int64_t ___valleys_count_14;
	// System.Int64 MyCityModel::mayer_residence_level
	int64_t ___mayer_residence_level_15;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_city_name_1() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___city_name_1)); }
	inline String_t* get_city_name_1() const { return ___city_name_1; }
	inline String_t** get_address_of_city_name_1() { return &___city_name_1; }
	inline void set_city_name_1(String_t* value)
	{
		___city_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___city_name_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___posX_2)); }
	inline int64_t get_posX_2() const { return ___posX_2; }
	inline int64_t* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(int64_t value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___posY_3)); }
	inline int64_t get_posY_3() const { return ___posY_3; }
	inline int64_t* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(int64_t value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___region_4)); }
	inline String_t* get_region_4() const { return ___region_4; }
	inline String_t** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(String_t* value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_image_name_5() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___image_name_5)); }
	inline String_t* get_image_name_5() const { return ___image_name_5; }
	inline String_t** get_address_of_image_name_5() { return &___image_name_5; }
	inline void set_image_name_5(String_t* value)
	{
		___image_name_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_5), value);
	}

	inline static int32_t get_offset_of_max_population_6() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___max_population_6)); }
	inline int64_t get_max_population_6() const { return ___max_population_6; }
	inline int64_t* get_address_of_max_population_6() { return &___max_population_6; }
	inline void set_max_population_6(int64_t value)
	{
		___max_population_6 = value;
	}

	inline static int32_t get_offset_of_current_population_7() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___current_population_7)); }
	inline int64_t get_current_population_7() const { return ___current_population_7; }
	inline int64_t* get_address_of_current_population_7() { return &___current_population_7; }
	inline void set_current_population_7(int64_t value)
	{
		___current_population_7 = value;
	}

	inline static int32_t get_offset_of_happiness_8() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___happiness_8)); }
	inline int64_t get_happiness_8() const { return ___happiness_8; }
	inline int64_t* get_address_of_happiness_8() { return &___happiness_8; }
	inline void set_happiness_8(int64_t value)
	{
		___happiness_8 = value;
	}

	inline static int32_t get_offset_of_courage_9() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___courage_9)); }
	inline int64_t get_courage_9() const { return ___courage_9; }
	inline int64_t* get_address_of_courage_9() { return &___courage_9; }
	inline void set_courage_9(int64_t value)
	{
		___courage_9 = value;
	}

	inline static int32_t get_offset_of_isExpantion_10() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___isExpantion_10)); }
	inline bool get_isExpantion_10() const { return ___isExpantion_10; }
	inline bool* get_address_of_isExpantion_10() { return &___isExpantion_10; }
	inline void set_isExpantion_10(bool value)
	{
		___isExpantion_10 = value;
	}

	inline static int32_t get_offset_of_reinforce_flag_11() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___reinforce_flag_11)); }
	inline bool get_reinforce_flag_11() const { return ___reinforce_flag_11; }
	inline bool* get_address_of_reinforce_flag_11() { return &___reinforce_flag_11; }
	inline void set_reinforce_flag_11(bool value)
	{
		___reinforce_flag_11 = value;
	}

	inline static int32_t get_offset_of_gold_tax_rate_12() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___gold_tax_rate_12)); }
	inline int64_t get_gold_tax_rate_12() const { return ___gold_tax_rate_12; }
	inline int64_t* get_address_of_gold_tax_rate_12() { return &___gold_tax_rate_12; }
	inline void set_gold_tax_rate_12(int64_t value)
	{
		___gold_tax_rate_12 = value;
	}

	inline static int32_t get_offset_of_fields_count_13() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___fields_count_13)); }
	inline int64_t get_fields_count_13() const { return ___fields_count_13; }
	inline int64_t* get_address_of_fields_count_13() { return &___fields_count_13; }
	inline void set_fields_count_13(int64_t value)
	{
		___fields_count_13 = value;
	}

	inline static int32_t get_offset_of_valleys_count_14() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___valleys_count_14)); }
	inline int64_t get_valleys_count_14() const { return ___valleys_count_14; }
	inline int64_t* get_address_of_valleys_count_14() { return &___valleys_count_14; }
	inline void set_valleys_count_14(int64_t value)
	{
		___valleys_count_14 = value;
	}

	inline static int32_t get_offset_of_mayer_residence_level_15() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___mayer_residence_level_15)); }
	inline int64_t get_mayer_residence_level_15() const { return ___mayer_residence_level_15; }
	inline int64_t* get_address_of_mayer_residence_level_15() { return &___mayer_residence_level_15; }
	inline void set_mayer_residence_level_15(int64_t value)
	{
		___mayer_residence_level_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYCITYMODEL_T3961736920_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_1;
	// System.String System.Uri::source
	String_t* ___source_2;
	// System.String System.Uri::scheme
	String_t* ___scheme_3;
	// System.String System.Uri::host
	String_t* ___host_4;
	// System.Int32 System.Uri::port
	int32_t ___port_5;
	// System.String System.Uri::path
	String_t* ___path_6;
	// System.String System.Uri::query
	String_t* ___query_7;
	// System.String System.Uri::fragment
	String_t* ___fragment_8;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_9;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_10;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_11;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_12;
	// System.String[] System.Uri::segments
	StringU5BU5D_t1281789340* ___segments_13;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_14;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_15;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_16;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_17;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_18;
	// System.UriParser System.Uri::parser
	UriParser_t3890150400 * ___parser_32;

public:
	inline static int32_t get_offset_of_isUnixFilePath_1() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnixFilePath_1)); }
	inline bool get_isUnixFilePath_1() const { return ___isUnixFilePath_1; }
	inline bool* get_address_of_isUnixFilePath_1() { return &___isUnixFilePath_1; }
	inline void set_isUnixFilePath_1(bool value)
	{
		___isUnixFilePath_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___source_2)); }
	inline String_t* get_source_2() const { return ___source_2; }
	inline String_t** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(String_t* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_scheme_3() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___scheme_3)); }
	inline String_t* get_scheme_3() const { return ___scheme_3; }
	inline String_t** get_address_of_scheme_3() { return &___scheme_3; }
	inline void set_scheme_3(String_t* value)
	{
		___scheme_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_3), value);
	}

	inline static int32_t get_offset_of_host_4() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___host_4)); }
	inline String_t* get_host_4() const { return ___host_4; }
	inline String_t** get_address_of_host_4() { return &___host_4; }
	inline void set_host_4(String_t* value)
	{
		___host_4 = value;
		Il2CppCodeGenWriteBarrier((&___host_4), value);
	}

	inline static int32_t get_offset_of_port_5() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___port_5)); }
	inline int32_t get_port_5() const { return ___port_5; }
	inline int32_t* get_address_of_port_5() { return &___port_5; }
	inline void set_port_5(int32_t value)
	{
		___port_5 = value;
	}

	inline static int32_t get_offset_of_path_6() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___path_6)); }
	inline String_t* get_path_6() const { return ___path_6; }
	inline String_t** get_address_of_path_6() { return &___path_6; }
	inline void set_path_6(String_t* value)
	{
		___path_6 = value;
		Il2CppCodeGenWriteBarrier((&___path_6), value);
	}

	inline static int32_t get_offset_of_query_7() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___query_7)); }
	inline String_t* get_query_7() const { return ___query_7; }
	inline String_t** get_address_of_query_7() { return &___query_7; }
	inline void set_query_7(String_t* value)
	{
		___query_7 = value;
		Il2CppCodeGenWriteBarrier((&___query_7), value);
	}

	inline static int32_t get_offset_of_fragment_8() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___fragment_8)); }
	inline String_t* get_fragment_8() const { return ___fragment_8; }
	inline String_t** get_address_of_fragment_8() { return &___fragment_8; }
	inline void set_fragment_8(String_t* value)
	{
		___fragment_8 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_8), value);
	}

	inline static int32_t get_offset_of_userinfo_9() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userinfo_9)); }
	inline String_t* get_userinfo_9() const { return ___userinfo_9; }
	inline String_t** get_address_of_userinfo_9() { return &___userinfo_9; }
	inline void set_userinfo_9(String_t* value)
	{
		___userinfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_9), value);
	}

	inline static int32_t get_offset_of_isUnc_10() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isUnc_10)); }
	inline bool get_isUnc_10() const { return ___isUnc_10; }
	inline bool* get_address_of_isUnc_10() { return &___isUnc_10; }
	inline void set_isUnc_10(bool value)
	{
		___isUnc_10 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_11() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isOpaquePart_11)); }
	inline bool get_isOpaquePart_11() const { return ___isOpaquePart_11; }
	inline bool* get_address_of_isOpaquePart_11() { return &___isOpaquePart_11; }
	inline void set_isOpaquePart_11(bool value)
	{
		___isOpaquePart_11 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_12() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___isAbsoluteUri_12)); }
	inline bool get_isAbsoluteUri_12() const { return ___isAbsoluteUri_12; }
	inline bool* get_address_of_isAbsoluteUri_12() { return &___isAbsoluteUri_12; }
	inline void set_isAbsoluteUri_12(bool value)
	{
		___isAbsoluteUri_12 = value;
	}

	inline static int32_t get_offset_of_segments_13() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___segments_13)); }
	inline StringU5BU5D_t1281789340* get_segments_13() const { return ___segments_13; }
	inline StringU5BU5D_t1281789340** get_address_of_segments_13() { return &___segments_13; }
	inline void set_segments_13(StringU5BU5D_t1281789340* value)
	{
		___segments_13 = value;
		Il2CppCodeGenWriteBarrier((&___segments_13), value);
	}

	inline static int32_t get_offset_of_userEscaped_14() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___userEscaped_14)); }
	inline bool get_userEscaped_14() const { return ___userEscaped_14; }
	inline bool* get_address_of_userEscaped_14() { return &___userEscaped_14; }
	inline void set_userEscaped_14(bool value)
	{
		___userEscaped_14 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_15() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedAbsoluteUri_15)); }
	inline String_t* get_cachedAbsoluteUri_15() const { return ___cachedAbsoluteUri_15; }
	inline String_t** get_address_of_cachedAbsoluteUri_15() { return &___cachedAbsoluteUri_15; }
	inline void set_cachedAbsoluteUri_15(String_t* value)
	{
		___cachedAbsoluteUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_15), value);
	}

	inline static int32_t get_offset_of_cachedToString_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedToString_16)); }
	inline String_t* get_cachedToString_16() const { return ___cachedToString_16; }
	inline String_t** get_address_of_cachedToString_16() { return &___cachedToString_16; }
	inline void set_cachedToString_16(String_t* value)
	{
		___cachedToString_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_16), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedLocalPath_17)); }
	inline String_t* get_cachedLocalPath_17() const { return ___cachedLocalPath_17; }
	inline String_t** get_address_of_cachedLocalPath_17() { return &___cachedLocalPath_17; }
	inline void set_cachedLocalPath_17(String_t* value)
	{
		___cachedLocalPath_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_17), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___cachedHashCode_18)); }
	inline int32_t get_cachedHashCode_18() const { return ___cachedHashCode_18; }
	inline int32_t* get_address_of_cachedHashCode_18() { return &___cachedHashCode_18; }
	inline void set_cachedHashCode_18(int32_t value)
	{
		___cachedHashCode_18 = value;
	}

	inline static int32_t get_offset_of_parser_32() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___parser_32)); }
	inline UriParser_t3890150400 * get_parser_32() const { return ___parser_32; }
	inline UriParser_t3890150400 ** get_address_of_parser_32() { return &___parser_32; }
	inline void set_parser_32(UriParser_t3890150400 * value)
	{
		___parser_32 = value;
		Il2CppCodeGenWriteBarrier((&___parser_32), value);
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_19;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_20;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_21;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_22;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_23;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_24;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_25;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_26;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_27;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_28;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_29;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_30;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t2082808316* ___schemes_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map12_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map13_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map14_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map16_37;

public:
	inline static int32_t get_offset_of_hexUpperChars_19() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___hexUpperChars_19)); }
	inline String_t* get_hexUpperChars_19() const { return ___hexUpperChars_19; }
	inline String_t** get_address_of_hexUpperChars_19() { return &___hexUpperChars_19; }
	inline void set_hexUpperChars_19(String_t* value)
	{
		___hexUpperChars_19 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_19), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_20() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_20)); }
	inline String_t* get_SchemeDelimiter_20() const { return ___SchemeDelimiter_20; }
	inline String_t** get_address_of_SchemeDelimiter_20() { return &___SchemeDelimiter_20; }
	inline void set_SchemeDelimiter_20(String_t* value)
	{
		___SchemeDelimiter_20 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_21() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_21)); }
	inline String_t* get_UriSchemeFile_21() const { return ___UriSchemeFile_21; }
	inline String_t** get_address_of_UriSchemeFile_21() { return &___UriSchemeFile_21; }
	inline void set_UriSchemeFile_21(String_t* value)
	{
		___UriSchemeFile_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_22() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_22)); }
	inline String_t* get_UriSchemeFtp_22() const { return ___UriSchemeFtp_22; }
	inline String_t** get_address_of_UriSchemeFtp_22() { return &___UriSchemeFtp_22; }
	inline void set_UriSchemeFtp_22(String_t* value)
	{
		___UriSchemeFtp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_23)); }
	inline String_t* get_UriSchemeGopher_23() const { return ___UriSchemeGopher_23; }
	inline String_t** get_address_of_UriSchemeGopher_23() { return &___UriSchemeGopher_23; }
	inline void set_UriSchemeGopher_23(String_t* value)
	{
		___UriSchemeGopher_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_24)); }
	inline String_t* get_UriSchemeHttp_24() const { return ___UriSchemeHttp_24; }
	inline String_t** get_address_of_UriSchemeHttp_24() { return &___UriSchemeHttp_24; }
	inline void set_UriSchemeHttp_24(String_t* value)
	{
		___UriSchemeHttp_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_25)); }
	inline String_t* get_UriSchemeHttps_25() const { return ___UriSchemeHttps_25; }
	inline String_t** get_address_of_UriSchemeHttps_25() { return &___UriSchemeHttps_25; }
	inline void set_UriSchemeHttps_25(String_t* value)
	{
		___UriSchemeHttps_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_26)); }
	inline String_t* get_UriSchemeMailto_26() const { return ___UriSchemeMailto_26; }
	inline String_t** get_address_of_UriSchemeMailto_26() { return &___UriSchemeMailto_26; }
	inline void set_UriSchemeMailto_26(String_t* value)
	{
		___UriSchemeMailto_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_27)); }
	inline String_t* get_UriSchemeNews_27() const { return ___UriSchemeNews_27; }
	inline String_t** get_address_of_UriSchemeNews_27() { return &___UriSchemeNews_27; }
	inline void set_UriSchemeNews_27(String_t* value)
	{
		___UriSchemeNews_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_28() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_28)); }
	inline String_t* get_UriSchemeNntp_28() const { return ___UriSchemeNntp_28; }
	inline String_t** get_address_of_UriSchemeNntp_28() { return &___UriSchemeNntp_28; }
	inline void set_UriSchemeNntp_28(String_t* value)
	{
		___UriSchemeNntp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_28), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_29)); }
	inline String_t* get_UriSchemeNetPipe_29() const { return ___UriSchemeNetPipe_29; }
	inline String_t** get_address_of_UriSchemeNetPipe_29() { return &___UriSchemeNetPipe_29; }
	inline void set_UriSchemeNetPipe_29(String_t* value)
	{
		___UriSchemeNetPipe_29 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_29), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_30)); }
	inline String_t* get_UriSchemeNetTcp_30() const { return ___UriSchemeNetTcp_30; }
	inline String_t** get_address_of_UriSchemeNetTcp_30() { return &___UriSchemeNetTcp_30; }
	inline void set_UriSchemeNetTcp_30(String_t* value)
	{
		___UriSchemeNetTcp_30 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_30), value);
	}

	inline static int32_t get_offset_of_schemes_31() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___schemes_31)); }
	inline UriSchemeU5BU5D_t2082808316* get_schemes_31() const { return ___schemes_31; }
	inline UriSchemeU5BU5D_t2082808316** get_address_of_schemes_31() { return &___schemes_31; }
	inline void set_schemes_31(UriSchemeU5BU5D_t2082808316* value)
	{
		___schemes_31 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_33() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map12_33)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map12_33() const { return ___U3CU3Ef__switchU24map12_33; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map12_33() { return &___U3CU3Ef__switchU24map12_33; }
	inline void set_U3CU3Ef__switchU24map12_33(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map12_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map13_34)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map13_34() const { return ___U3CU3Ef__switchU24map13_34; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map13_34() { return &___U3CU3Ef__switchU24map13_34; }
	inline void set_U3CU3Ef__switchU24map13_34(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map13_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map14_35)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map14_35() const { return ___U3CU3Ef__switchU24map14_35; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map14_35() { return &___U3CU3Ef__switchU24map14_35; }
	inline void set_U3CU3Ef__switchU24map14_35(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map14_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_36() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map15_36)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_36() const { return ___U3CU3Ef__switchU24map15_36; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_36() { return &___U3CU3Ef__switchU24map15_36; }
	inline void set_U3CU3Ef__switchU24map15_36(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_37() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___U3CU3Ef__switchU24map16_37)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map16_37() const { return ___U3CU3Ef__switchU24map16_37; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map16_37() { return &___U3CU3Ef__switchU24map16_37; }
	inline void set_U3CU3Ef__switchU24map16_37(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map16_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
#define U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChat/<BeginChat>c__Iterator0
struct  U3CBeginChatU3Ec__Iterator0_t121862130  : public RuntimeObject
{
public:
	// WebSocket WorldChat/<BeginChat>c__Iterator0::<w>__0
	WebSocket_t1645401340 * ___U3CwU3E__0_0;
	// System.String WorldChat/<BeginChat>c__Iterator0::<reply>__1
	String_t* ___U3CreplyU3E__1_1;
	// WorldChat WorldChat/<BeginChat>c__Iterator0::$this
	WorldChat_t753147188 * ___U24this_2;
	// System.Object WorldChat/<BeginChat>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WorldChat/<BeginChat>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WorldChat/<BeginChat>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U3CwU3E__0_0)); }
	inline WebSocket_t1645401340 * get_U3CwU3E__0_0() const { return ___U3CwU3E__0_0; }
	inline WebSocket_t1645401340 ** get_address_of_U3CwU3E__0_0() { return &___U3CwU3E__0_0; }
	inline void set_U3CwU3E__0_0(WebSocket_t1645401340 * value)
	{
		___U3CwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CreplyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U3CreplyU3E__1_1)); }
	inline String_t* get_U3CreplyU3E__1_1() const { return ___U3CreplyU3E__1_1; }
	inline String_t** get_address_of_U3CreplyU3E__1_1() { return &___U3CreplyU3E__1_1; }
	inline void set_U3CreplyU3E__1_1(String_t* value)
	{
		___U3CreplyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreplyU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24this_2)); }
	inline WorldChat_t753147188 * get_U24this_2() const { return ___U24this_2; }
	inline WorldChat_t753147188 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WorldChat_t753147188 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator0_t121862130, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINCHATU3EC__ITERATOR0_T121862130_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DIRECTION_T3470714353_H
#define DIRECTION_T3470714353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t3470714353 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3470714353, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3470714353_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef UPLOADHANDLER_T2993558019_H
#define UPLOADHANDLER_T2993558019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t2993558019  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t2993558019, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T2993558019_H
#ifndef CONTENTTYPE_T1787303396_H
#define CONTENTTYPE_T1787303396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1787303396 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t1787303396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1787303396_H
#ifndef INPUTTYPE_T1770400679_H
#define INPUTTYPE_T1770400679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t1770400679 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t1770400679, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1770400679_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef CHARACTERVALIDATION_T4051914437_H
#define CHARACTERVALIDATION_T4051914437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t4051914437 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t4051914437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T4051914437_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef LINETYPE_T4214648469_H
#define LINETYPE_T4214648469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4214648469 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t4214648469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4214648469_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef TYPE_T1992804461_H
#define TYPE_T1992804461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject/Type
struct  Type_t1992804461 
{
public:
	// System.Int32 JSONObject/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1992804461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1992804461_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ASYNCOPERATION_T1445031843_H
#define ASYNCOPERATION_T1445031843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1445031843  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1617499438 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_completeCallback_1)); }
	inline Action_1_t1617499438 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1617499438 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1617499438 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1445031843_H
#ifndef DOWNLOADHANDLER_T2937767557_H
#define DOWNLOADHANDLER_T2937767557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t2937767557  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t2937767557, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T2937767557_H
#ifndef STRINGSPLITOPTIONS_T641086070_H
#define STRINGSPLITOPTIONS_T641086070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringSplitOptions
struct  StringSplitOptions_t641086070 
{
public:
	// System.Int32 System.StringSplitOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringSplitOptions_t641086070, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSPLITOPTIONS_T641086070_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef UNITYWEBREQUEST_T463507806_H
#define UNITYWEBREQUEST_T463507806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t463507806  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t2937767557 * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t2993558019 * ___m_UploadHandler_2;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_DownloadHandler_1)); }
	inline DownloadHandler_t2937767557 * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_t2937767557 ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_t2937767557 * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadHandler_1), value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_UploadHandler_2)); }
	inline UploadHandler_t2993558019 * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t2993558019 ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t2993558019 * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_UploadHandler_2), value);
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t2937767557_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t2993558019_marshaled_pinvoke ___m_UploadHandler_2;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t2937767557_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t2993558019_marshaled_com* ___m_UploadHandler_2;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;
};
#endif // UNITYWEBREQUEST_T463507806_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef JSONOBJECT_T1339445639_H
#define JSONOBJECT_T1339445639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject
struct  JSONObject_t1339445639  : public RuntimeObject
{
public:
	// JSONObject/Type JSONObject::type
	int32_t ___type_6;
	// System.Collections.Generic.List`1<JSONObject> JSONObject::list
	List_1_t2811520381 * ___list_7;
	// System.Collections.Generic.List`1<System.String> JSONObject::keys
	List_1_t3319525431 * ___keys_8;
	// System.String JSONObject::str
	String_t* ___str_9;
	// System.Single JSONObject::n
	float ___n_10;
	// System.Boolean JSONObject::useInt
	bool ___useInt_11;
	// System.Int64 JSONObject::i
	int64_t ___i_12;
	// System.Boolean JSONObject::b
	bool ___b_13;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_list_7() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___list_7)); }
	inline List_1_t2811520381 * get_list_7() const { return ___list_7; }
	inline List_1_t2811520381 ** get_address_of_list_7() { return &___list_7; }
	inline void set_list_7(List_1_t2811520381 * value)
	{
		___list_7 = value;
		Il2CppCodeGenWriteBarrier((&___list_7), value);
	}

	inline static int32_t get_offset_of_keys_8() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___keys_8)); }
	inline List_1_t3319525431 * get_keys_8() const { return ___keys_8; }
	inline List_1_t3319525431 ** get_address_of_keys_8() { return &___keys_8; }
	inline void set_keys_8(List_1_t3319525431 * value)
	{
		___keys_8 = value;
		Il2CppCodeGenWriteBarrier((&___keys_8), value);
	}

	inline static int32_t get_offset_of_str_9() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___str_9)); }
	inline String_t* get_str_9() const { return ___str_9; }
	inline String_t** get_address_of_str_9() { return &___str_9; }
	inline void set_str_9(String_t* value)
	{
		___str_9 = value;
		Il2CppCodeGenWriteBarrier((&___str_9), value);
	}

	inline static int32_t get_offset_of_n_10() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___n_10)); }
	inline float get_n_10() const { return ___n_10; }
	inline float* get_address_of_n_10() { return &___n_10; }
	inline void set_n_10(float value)
	{
		___n_10 = value;
	}

	inline static int32_t get_offset_of_useInt_11() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___useInt_11)); }
	inline bool get_useInt_11() const { return ___useInt_11; }
	inline bool* get_address_of_useInt_11() { return &___useInt_11; }
	inline void set_useInt_11(bool value)
	{
		___useInt_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___i_12)); }
	inline int64_t get_i_12() const { return ___i_12; }
	inline int64_t* get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(int64_t value)
	{
		___i_12 = value;
	}

	inline static int32_t get_offset_of_b_13() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639, ___b_13)); }
	inline bool get_b_13() const { return ___b_13; }
	inline bool* get_address_of_b_13() { return &___b_13; }
	inline void set_b_13(bool value)
	{
		___b_13 = value;
	}
};

struct JSONObject_t1339445639_StaticFields
{
public:
	// System.Char[] JSONObject::WHITESPACE
	CharU5BU5D_t3528271667* ___WHITESPACE_5;
	// System.Diagnostics.Stopwatch JSONObject::printWatch
	Stopwatch_t305734070 * ___printWatch_15;

public:
	inline static int32_t get_offset_of_WHITESPACE_5() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639_StaticFields, ___WHITESPACE_5)); }
	inline CharU5BU5D_t3528271667* get_WHITESPACE_5() const { return ___WHITESPACE_5; }
	inline CharU5BU5D_t3528271667** get_address_of_WHITESPACE_5() { return &___WHITESPACE_5; }
	inline void set_WHITESPACE_5(CharU5BU5D_t3528271667* value)
	{
		___WHITESPACE_5 = value;
		Il2CppCodeGenWriteBarrier((&___WHITESPACE_5), value);
	}

	inline static int32_t get_offset_of_printWatch_15() { return static_cast<int32_t>(offsetof(JSONObject_t1339445639_StaticFields, ___printWatch_15)); }
	inline Stopwatch_t305734070 * get_printWatch_15() const { return ___printWatch_15; }
	inline Stopwatch_t305734070 ** get_address_of_printWatch_15() { return &___printWatch_15; }
	inline void set_printWatch_15(Stopwatch_t305734070 * value)
	{
		___printWatch_15 = value;
		Il2CppCodeGenWriteBarrier((&___printWatch_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T1339445639_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef SIMPLEEVENT_T129249603_H
#define SIMPLEEVENT_T129249603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleEvent
struct  SimpleEvent_t129249603  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEEVENT_T129249603_H
#ifndef TEXTMESH_T1536577757_H
#define TEXTMESH_T1536577757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextMesh
struct  TextMesh_t1536577757  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESH_T1536577757_H
#ifndef WORLDMAPTIMER_T60483741_H
#define WORLDMAPTIMER_T60483741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapTimer
struct  WorldMapTimer_t60483741  : public RuntimeObject
{
public:
	// System.Int64 WorldMapTimer::id
	int64_t ___id_0;
	// System.Int64 WorldMapTimer::worldMap
	int64_t ___worldMap_1;
	// System.String WorldMapTimer::status
	String_t* ___status_2;
	// System.Int64 WorldMapTimer::event_id
	int64_t ___event_id_3;
	// System.DateTime WorldMapTimer::start_time
	DateTime_t3738529785  ___start_time_4;
	// System.DateTime WorldMapTimer::finish_time
	DateTime_t3738529785  ___finish_time_5;
	// System.String WorldMapTimer::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_worldMap_1() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___worldMap_1)); }
	inline int64_t get_worldMap_1() const { return ___worldMap_1; }
	inline int64_t* get_address_of_worldMap_1() { return &___worldMap_1; }
	inline void set_worldMap_1(int64_t value)
	{
		___worldMap_1 = value;
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_event_id_3() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___event_id_3)); }
	inline int64_t get_event_id_3() const { return ___event_id_3; }
	inline int64_t* get_address_of_event_id_3() { return &___event_id_3; }
	inline void set_event_id_3(int64_t value)
	{
		___event_id_3 = value;
	}

	inline static int32_t get_offset_of_start_time_4() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___start_time_4)); }
	inline DateTime_t3738529785  get_start_time_4() const { return ___start_time_4; }
	inline DateTime_t3738529785 * get_address_of_start_time_4() { return &___start_time_4; }
	inline void set_start_time_4(DateTime_t3738529785  value)
	{
		___start_time_4 = value;
	}

	inline static int32_t get_offset_of_finish_time_5() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___finish_time_5)); }
	inline DateTime_t3738529785  get_finish_time_5() const { return ___finish_time_5; }
	inline DateTime_t3738529785 * get_address_of_finish_time_5() { return &___finish_time_5; }
	inline void set_finish_time_5(DateTime_t3738529785  value)
	{
		___finish_time_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDMAPTIMER_T60483741_H
#ifndef USERMODEL_T1353931605_H
#define USERMODEL_T1353931605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserModel
struct  UserModel_t1353931605  : public RuntimeObject
{
public:
	// System.Int64 UserModel::id
	int64_t ___id_0;
	// System.String UserModel::username
	String_t* ___username_1;
	// System.String UserModel::status
	String_t* ___status_2;
	// System.String UserModel::image_name
	String_t* ___image_name_3;
	// System.String UserModel::rank
	String_t* ___rank_4;
	// System.Int64 UserModel::emperor_id
	int64_t ___emperor_id_5;
	// System.String UserModel::email
	String_t* ___email_6;
	// System.Int64 UserModel::experience
	int64_t ___experience_7;
	// System.String UserModel::alliance_rank
	String_t* ___alliance_rank_8;
	// System.DateTime UserModel::last_login
	DateTime_t3738529785  ___last_login_9;
	// System.Int64 UserModel::city_count
	int64_t ___city_count_10;
	// AllianceModel UserModel::alliance
	AllianceModel_t2995969982 * ___alliance_11;
	// MyCityModel UserModel::capital
	MyCityModel_t3961736920 * ___capital_12;
	// System.Int64 UserModel::shillings
	int64_t ___shillings_13;
	// System.String UserModel::language
	String_t* ___language_14;
	// System.String UserModel::flag
	String_t* ___flag_15;
	// System.Int64 UserModel::players_wins
	int64_t ___players_wins_16;
	// System.Int64 UserModel::players_losses
	int64_t ___players_losses_17;
	// System.Int64 UserModel::barbarians_wins
	int64_t ___barbarians_wins_18;
	// System.Int64 UserModel::barbarians_losses
	int64_t ___barbarians_losses_19;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_3), value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___rank_4)); }
	inline String_t* get_rank_4() const { return ___rank_4; }
	inline String_t** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(String_t* value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier((&___rank_4), value);
	}

	inline static int32_t get_offset_of_emperor_id_5() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___emperor_id_5)); }
	inline int64_t get_emperor_id_5() const { return ___emperor_id_5; }
	inline int64_t* get_address_of_emperor_id_5() { return &___emperor_id_5; }
	inline void set_emperor_id_5(int64_t value)
	{
		___emperor_id_5 = value;
	}

	inline static int32_t get_offset_of_email_6() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___email_6)); }
	inline String_t* get_email_6() const { return ___email_6; }
	inline String_t** get_address_of_email_6() { return &___email_6; }
	inline void set_email_6(String_t* value)
	{
		___email_6 = value;
		Il2CppCodeGenWriteBarrier((&___email_6), value);
	}

	inline static int32_t get_offset_of_experience_7() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___experience_7)); }
	inline int64_t get_experience_7() const { return ___experience_7; }
	inline int64_t* get_address_of_experience_7() { return &___experience_7; }
	inline void set_experience_7(int64_t value)
	{
		___experience_7 = value;
	}

	inline static int32_t get_offset_of_alliance_rank_8() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___alliance_rank_8)); }
	inline String_t* get_alliance_rank_8() const { return ___alliance_rank_8; }
	inline String_t** get_address_of_alliance_rank_8() { return &___alliance_rank_8; }
	inline void set_alliance_rank_8(String_t* value)
	{
		___alliance_rank_8 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_rank_8), value);
	}

	inline static int32_t get_offset_of_last_login_9() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___last_login_9)); }
	inline DateTime_t3738529785  get_last_login_9() const { return ___last_login_9; }
	inline DateTime_t3738529785 * get_address_of_last_login_9() { return &___last_login_9; }
	inline void set_last_login_9(DateTime_t3738529785  value)
	{
		___last_login_9 = value;
	}

	inline static int32_t get_offset_of_city_count_10() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___city_count_10)); }
	inline int64_t get_city_count_10() const { return ___city_count_10; }
	inline int64_t* get_address_of_city_count_10() { return &___city_count_10; }
	inline void set_city_count_10(int64_t value)
	{
		___city_count_10 = value;
	}

	inline static int32_t get_offset_of_alliance_11() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___alliance_11)); }
	inline AllianceModel_t2995969982 * get_alliance_11() const { return ___alliance_11; }
	inline AllianceModel_t2995969982 ** get_address_of_alliance_11() { return &___alliance_11; }
	inline void set_alliance_11(AllianceModel_t2995969982 * value)
	{
		___alliance_11 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_11), value);
	}

	inline static int32_t get_offset_of_capital_12() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___capital_12)); }
	inline MyCityModel_t3961736920 * get_capital_12() const { return ___capital_12; }
	inline MyCityModel_t3961736920 ** get_address_of_capital_12() { return &___capital_12; }
	inline void set_capital_12(MyCityModel_t3961736920 * value)
	{
		___capital_12 = value;
		Il2CppCodeGenWriteBarrier((&___capital_12), value);
	}

	inline static int32_t get_offset_of_shillings_13() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___shillings_13)); }
	inline int64_t get_shillings_13() const { return ___shillings_13; }
	inline int64_t* get_address_of_shillings_13() { return &___shillings_13; }
	inline void set_shillings_13(int64_t value)
	{
		___shillings_13 = value;
	}

	inline static int32_t get_offset_of_language_14() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___language_14)); }
	inline String_t* get_language_14() const { return ___language_14; }
	inline String_t** get_address_of_language_14() { return &___language_14; }
	inline void set_language_14(String_t* value)
	{
		___language_14 = value;
		Il2CppCodeGenWriteBarrier((&___language_14), value);
	}

	inline static int32_t get_offset_of_flag_15() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___flag_15)); }
	inline String_t* get_flag_15() const { return ___flag_15; }
	inline String_t** get_address_of_flag_15() { return &___flag_15; }
	inline void set_flag_15(String_t* value)
	{
		___flag_15 = value;
		Il2CppCodeGenWriteBarrier((&___flag_15), value);
	}

	inline static int32_t get_offset_of_players_wins_16() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___players_wins_16)); }
	inline int64_t get_players_wins_16() const { return ___players_wins_16; }
	inline int64_t* get_address_of_players_wins_16() { return &___players_wins_16; }
	inline void set_players_wins_16(int64_t value)
	{
		___players_wins_16 = value;
	}

	inline static int32_t get_offset_of_players_losses_17() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___players_losses_17)); }
	inline int64_t get_players_losses_17() const { return ___players_losses_17; }
	inline int64_t* get_address_of_players_losses_17() { return &___players_losses_17; }
	inline void set_players_losses_17(int64_t value)
	{
		___players_losses_17 = value;
	}

	inline static int32_t get_offset_of_barbarians_wins_18() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___barbarians_wins_18)); }
	inline int64_t get_barbarians_wins_18() const { return ___barbarians_wins_18; }
	inline int64_t* get_address_of_barbarians_wins_18() { return &___barbarians_wins_18; }
	inline void set_barbarians_wins_18(int64_t value)
	{
		___barbarians_wins_18 = value;
	}

	inline static int32_t get_offset_of_barbarians_losses_19() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___barbarians_losses_19)); }
	inline int64_t get_barbarians_losses_19() const { return ___barbarians_losses_19; }
	inline int64_t* get_address_of_barbarians_losses_19() { return &___barbarians_losses_19; }
	inline void set_barbarians_losses_19(int64_t value)
	{
		___barbarians_losses_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERMODEL_T1353931605_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef MESHRENDERER_T587009260_H
#define MESHRENDERER_T587009260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t587009260  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T587009260_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef WINDOWINSTANCEMANAGER_T3234774687_H
#define WINDOWINSTANCEMANAGER_T3234774687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowInstanceManager
struct  WindowInstanceManager_t3234774687  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean WindowInstanceManager::windowOpened
	bool ___windowOpened_3;
	// System.String WindowInstanceManager::shillingsHtml
	String_t* ___shillingsHtml_4;
	// UnityEngine.GameObject WindowInstanceManager::BuildingConstructionWindow
	GameObject_t1113636619 * ___BuildingConstructionWindow_5;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindow
	GameObject_t1113636619 * ___BuildingInformationWindow_6;
	// UnityEngine.GameObject WindowInstanceManager::ReportContainerWindow
	GameObject_t1113636619 * ___ReportContainerWindow_7;
	// UnityEngine.GameObject WindowInstanceManager::ShopWindow
	GameObject_t1113636619 * ___ShopWindow_8;
	// UnityEngine.GameObject WindowInstanceManager::MailWindow
	GameObject_t1113636619 * ___MailWindow_9;
	// UnityEngine.GameObject WindowInstanceManager::AlliancesWindow
	GameObject_t1113636619 * ___AlliancesWindow_10;
	// UnityEngine.GameObject WindowInstanceManager::UserInfoWindow
	GameObject_t1113636619 * ___UserInfoWindow_11;
	// UnityEngine.GameObject WindowInstanceManager::FinanceWindow
	GameObject_t1113636619 * ___FinanceWindow_12;
	// UnityEngine.GameObject WindowInstanceManager::HelpWindow
	GameObject_t1113636619 * ___HelpWindow_13;
	// UnityEngine.GameObject WindowInstanceManager::NewMailWindow
	GameObject_t1113636619 * ___NewMailWindow_14;
	// UnityEngine.GameObject WindowInstanceManager::MessageWindow
	GameObject_t1113636619 * ___MessageWindow_15;
	// UnityEngine.GameObject WindowInstanceManager::MessageAllianceMemberWindow
	GameObject_t1113636619 * ___MessageAllianceMemberWindow_16;
	// UnityEngine.GameObject WindowInstanceManager::EventsWindow
	GameObject_t1113636619 * ___EventsWindow_17;
	// UnityEngine.GameObject WindowInstanceManager::AllianceInfoWindow
	GameObject_t1113636619 * ___AllianceInfoWindow_18;
	// UnityEngine.GameObject WindowInstanceManager::UnitDetailsWindow
	GameObject_t1113636619 * ___UnitDetailsWindow_19;
	// UnityEngine.GameObject WindowInstanceManager::CityInfoWindow
	GameObject_t1113636619 * ___CityInfoWindow_20;
	// UnityEngine.GameObject WindowInstanceManager::ValleyInfoWindow
	GameObject_t1113636619 * ___ValleyInfoWindow_21;
	// UnityEngine.GameObject WindowInstanceManager::DispatchWindow
	GameObject_t1113636619 * ___DispatchWindow_22;
	// UnityEngine.GameObject WindowInstanceManager::RecruitBarbariansWindow
	GameObject_t1113636619 * ___RecruitBarbariansWindow_23;
	// UnityEngine.GameObject WindowInstanceManager::NotificationWindow
	GameObject_t1113636619 * ___NotificationWindow_24;
	// UnityEngine.GameObject WindowInstanceManager::LoginProgressBar
	GameObject_t1113636619 * ___LoginProgressBar_25;
	// UnityEngine.GameObject WindowInstanceManager::KnightInfoWindow
	GameObject_t1113636619 * ___KnightInfoWindow_26;
	// UnityEngine.GameObject WindowInstanceManager::BuyItemWindow
	GameObject_t1113636619 * ___BuyItemWindow_27;
	// UnityEngine.GameObject WindowInstanceManager::SellItemWindow
	GameObject_t1113636619 * ___SellItemWindow_28;
	// UnityEngine.GameObject WindowInstanceManager::SpeedUpWindow
	GameObject_t1113636619 * ___SpeedUpWindow_29;
	// UnityEngine.GameObject WindowInstanceManager::UpgradeBuildingWindow
	GameObject_t1113636619 * ___UpgradeBuildingWindow_30;
	// UnityEngine.GameObject WindowInstanceManager::AdvencedTeleportWindow
	GameObject_t1113636619 * ___AdvencedTeleportWindow_31;
	// UnityEngine.GameObject WindowInstanceManager::CityDeedWindow
	GameObject_t1113636619 * ___CityDeedWindow_32;
	// UnityEngine.GameObject WindowInstanceManager::BuyShillingsWindow
	GameObject_t1113636619 * ___BuyShillingsWindow_33;
	// UnityEngine.GameObject WindowInstanceManager::ChangeLanguageWindow
	GameObject_t1113636619 * ___ChangeLanguageWindow_34;
	// UnityEngine.GameObject WindowInstanceManager::DismissWindow
	GameObject_t1113636619 * ___DismissWindow_35;
	// UnityEngine.GameObject WindowInstanceManager::ChangePasswordWindow
	GameObject_t1113636619 * ___ChangePasswordWindow_36;
	// UnityEngine.GameObject WindowInstanceManager::TributesWindow
	GameObject_t1113636619 * ___TributesWindow_37;
	// UnityEngine.GameObject WindowInstanceManager::UpdateBuildingWindow
	GameObject_t1113636619 * ___UpdateBuildingWindow_38;
	// UnityEngine.GameObject WindowInstanceManager::BattleLogWindow
	GameObject_t1113636619 * ___BattleLogWindow_39;
	// UnityEngine.GameObject WindowInstanceManager::BuildNewCityWindow
	GameObject_t1113636619 * ___BuildNewCityWindow_40;
	// UnityEngine.GameObject WindowInstanceManager::ArmyDetailsWIndow
	GameObject_t1113636619 * ___ArmyDetailsWIndow_41;
	// UnityEngine.GameObject WindowInstanceManager::NewAllianceNameWindow
	GameObject_t1113636619 * ___NewAllianceNameWindow_42;
	// UnityEngine.GameObject WindowInstanceManager::ConfirmationWindow
	GameObject_t1113636619 * ___ConfirmationWindow_43;
	// UnityEngine.GameObject WindowInstanceManager::ShieldTimeWindow
	GameObject_t1113636619 * ___ShieldTimeWindow_44;
	// UnityEngine.GameObject WindowInstanceManager::TutorialWindow
	GameObject_t1113636619 * ___TutorialWindow_45;
	// UnityEngine.GameObject WindowInstanceManager::VideoAdsWindow
	GameObject_t1113636619 * ___VideoAdsWindow_46;
	// UnityEngine.GameObject WindowInstanceManager::BuildingConstructionWindowInstance
	GameObject_t1113636619 * ___BuildingConstructionWindowInstance_47;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindowInstance
	GameObject_t1113636619 * ___BuildingInformationWindowInstance_48;
	// UnityEngine.GameObject WindowInstanceManager::BuildingInformationWindowInstance2
	GameObject_t1113636619 * ___BuildingInformationWindowInstance2_49;
	// UnityEngine.GameObject WindowInstanceManager::ReportContainerWindowInstance
	GameObject_t1113636619 * ___ReportContainerWindowInstance_50;
	// UnityEngine.GameObject WindowInstanceManager::ShopWindowInstance
	GameObject_t1113636619 * ___ShopWindowInstance_51;
	// UnityEngine.GameObject WindowInstanceManager::MailWindowInstance
	GameObject_t1113636619 * ___MailWindowInstance_52;
	// UnityEngine.GameObject WindowInstanceManager::AlliancesWindowInstance
	GameObject_t1113636619 * ___AlliancesWindowInstance_53;
	// UnityEngine.GameObject WindowInstanceManager::UserInfoWindowInstance
	GameObject_t1113636619 * ___UserInfoWindowInstance_54;
	// UnityEngine.GameObject WindowInstanceManager::FinanceWindowInstance
	GameObject_t1113636619 * ___FinanceWindowInstance_55;
	// UnityEngine.GameObject WindowInstanceManager::HelpWindowInstance
	GameObject_t1113636619 * ___HelpWindowInstance_56;
	// UnityEngine.GameObject WindowInstanceManager::NewMailWindowInstance
	GameObject_t1113636619 * ___NewMailWindowInstance_57;
	// UnityEngine.GameObject WindowInstanceManager::MessageWindowInstance
	GameObject_t1113636619 * ___MessageWindowInstance_58;
	// UnityEngine.GameObject WindowInstanceManager::MessageAllianceMemberWindowInstance
	GameObject_t1113636619 * ___MessageAllianceMemberWindowInstance_59;
	// UnityEngine.GameObject WindowInstanceManager::EventsWindowInstance
	GameObject_t1113636619 * ___EventsWindowInstance_60;
	// UnityEngine.GameObject WindowInstanceManager::AllianceInfoWindowInstance
	GameObject_t1113636619 * ___AllianceInfoWindowInstance_61;
	// UnityEngine.GameObject WindowInstanceManager::UnitDetailsWindowInstance
	GameObject_t1113636619 * ___UnitDetailsWindowInstance_62;
	// UnityEngine.GameObject WindowInstanceManager::CityInfoWindowInstance
	GameObject_t1113636619 * ___CityInfoWindowInstance_63;
	// UnityEngine.GameObject WindowInstanceManager::DispatchWindowInstance
	GameObject_t1113636619 * ___DispatchWindowInstance_64;
	// UnityEngine.GameObject WindowInstanceManager::RecruitBarbariansWindowInstance
	GameObject_t1113636619 * ___RecruitBarbariansWindowInstance_65;
	// UnityEngine.GameObject WindowInstanceManager::ValleyInfoWindowInstance
	GameObject_t1113636619 * ___ValleyInfoWindowInstance_66;
	// UnityEngine.GameObject WindowInstanceManager::NotificationWindowInstance
	GameObject_t1113636619 * ___NotificationWindowInstance_67;
	// UnityEngine.GameObject WindowInstanceManager::LoginProgressBarInstance
	GameObject_t1113636619 * ___LoginProgressBarInstance_68;
	// UnityEngine.GameObject WindowInstanceManager::KnightInfoWindowInstance
	GameObject_t1113636619 * ___KnightInfoWindowInstance_69;
	// UnityEngine.GameObject WindowInstanceManager::BuyItemWindowInstance
	GameObject_t1113636619 * ___BuyItemWindowInstance_70;
	// UnityEngine.GameObject WindowInstanceManager::SellItemWindowInstance
	GameObject_t1113636619 * ___SellItemWindowInstance_71;
	// UnityEngine.GameObject WindowInstanceManager::SpeedUpWindowInstance
	GameObject_t1113636619 * ___SpeedUpWindowInstance_72;
	// UnityEngine.GameObject WindowInstanceManager::UpgradeBuildingWindowInstance
	GameObject_t1113636619 * ___UpgradeBuildingWindowInstance_73;
	// UnityEngine.GameObject WindowInstanceManager::AdvencedTeleportWindowInstance
	GameObject_t1113636619 * ___AdvencedTeleportWindowInstance_74;
	// UnityEngine.GameObject WindowInstanceManager::CityDeedWindowInstance
	GameObject_t1113636619 * ___CityDeedWindowInstance_75;
	// UnityEngine.GameObject WindowInstanceManager::BuyShillingsWindowInstance
	GameObject_t1113636619 * ___BuyShillingsWindowInstance_76;
	// UnityEngine.GameObject WindowInstanceManager::ChangeLanguageWindowInstance
	GameObject_t1113636619 * ___ChangeLanguageWindowInstance_77;
	// UnityEngine.GameObject WindowInstanceManager::DismissWindowInstance
	GameObject_t1113636619 * ___DismissWindowInstance_78;
	// UnityEngine.GameObject WindowInstanceManager::ChangePasswordWindowInstance
	GameObject_t1113636619 * ___ChangePasswordWindowInstance_79;
	// UnityEngine.GameObject WindowInstanceManager::TributesWindowInstance
	GameObject_t1113636619 * ___TributesWindowInstance_80;
	// UnityEngine.GameObject WindowInstanceManager::UpdateBuildingWindowInstance
	GameObject_t1113636619 * ___UpdateBuildingWindowInstance_81;
	// UnityEngine.GameObject WindowInstanceManager::BattleLogWindowInstance
	GameObject_t1113636619 * ___BattleLogWindowInstance_82;
	// UnityEngine.GameObject WindowInstanceManager::BuildNewCityWindowInstance
	GameObject_t1113636619 * ___BuildNewCityWindowInstance_83;
	// UnityEngine.GameObject WindowInstanceManager::ArmyDetailsWindowInstance
	GameObject_t1113636619 * ___ArmyDetailsWindowInstance_84;
	// UnityEngine.GameObject WindowInstanceManager::ShillingsWebViewWindowInstance
	GameObject_t1113636619 * ___ShillingsWebViewWindowInstance_85;
	// UnityEngine.GameObject WindowInstanceManager::NewAllianceNameWindowInstance
	GameObject_t1113636619 * ___NewAllianceNameWindowInstance_86;
	// UnityEngine.GameObject WindowInstanceManager::ConfirmationWindowInstance
	GameObject_t1113636619 * ___ConfirmationWindowInstance_87;
	// UnityEngine.GameObject WindowInstanceManager::ShieldTimeWindowInstance
	GameObject_t1113636619 * ___ShieldTimeWindowInstance_88;
	// UnityEngine.GameObject WindowInstanceManager::TutorialWindowInstance
	GameObject_t1113636619 * ___TutorialWindowInstance_89;
	// UnityEngine.GameObject WindowInstanceManager::VideoAdsWindowInstance
	GameObject_t1113636619 * ___VideoAdsWindowInstance_90;
	// UnityEngine.GameObject WindowInstanceManager::overviewPanel
	GameObject_t1113636619 * ___overviewPanel_91;
	// UnityEngine.GameObject WindowInstanceManager::statisticsPanel
	GameObject_t1113636619 * ___statisticsPanel_92;
	// UnityEngine.GameObject WindowInstanceManager::actionPanel
	GameObject_t1113636619 * ___actionPanel_93;
	// ActionManager WindowInstanceManager::actionManager
	ActionManager_t2430268190 * ___actionManager_94;
	// AllianceContentChanger WindowInstanceManager::allianceManager
	AllianceContentChanger_t2140085977 * ___allianceManager_95;
	// TreasureManager WindowInstanceManager::treasureManager
	TreasureManager_t110095690 * ___treasureManager_96;
	// UnityEngine.GameObject WindowInstanceManager::oldDesignPanel
	GameObject_t1113636619 * ___oldDesignPanel_97;
	// UnityEngine.GameObject WindowInstanceManager::newDesignPanel
	GameObject_t1113636619 * ___newDesignPanel_98;
	// UnityEngine.GameObject WindowInstanceManager::viewPanel
	GameObject_t1113636619 * ___viewPanel_99;
	// UnityEngine.GameObject WindowInstanceManager::itemPanel
	GameObject_t1113636619 * ___itemPanel_100;
	// UnityEngine.GameObject WindowInstanceManager::citiesPanel
	GameObject_t1113636619 * ___citiesPanel_101;
	// UnityEngine.UI.Image WindowInstanceManager::viewButton
	Image_t2670269651 * ___viewButton_102;
	// UnityEngine.UI.Image WindowInstanceManager::itemButton
	Image_t2670269651 * ___itemButton_103;
	// UnityEngine.UI.Image WindowInstanceManager::voiceButton
	Image_t2670269651 * ___voiceButton_104;
	// UnityEngine.Sprite WindowInstanceManager::viewClosedSprite
	Sprite_t280657092 * ___viewClosedSprite_105;
	// UnityEngine.Sprite WindowInstanceManager::viewOpenedSprite
	Sprite_t280657092 * ___viewOpenedSprite_106;
	// UnityEngine.Sprite WindowInstanceManager::itemClosedSprite
	Sprite_t280657092 * ___itemClosedSprite_107;
	// UnityEngine.Sprite WindowInstanceManager::itemOpenedSprite
	Sprite_t280657092 * ___itemOpenedSprite_108;
	// UnityEngine.Sprite WindowInstanceManager::voiceOnSprite
	Sprite_t280657092 * ___voiceOnSprite_109;
	// UnityEngine.Sprite WindowInstanceManager::voiceOffSprite
	Sprite_t280657092 * ___voiceOffSprite_110;
	// UnityEngine.GameObject WindowInstanceManager::UI
	GameObject_t1113636619 * ___UI_111;
	// UnityEngine.GameObject WindowInstanceManager::Shell
	GameObject_t1113636619 * ___Shell_112;

public:
	inline static int32_t get_offset_of_windowOpened_3() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___windowOpened_3)); }
	inline bool get_windowOpened_3() const { return ___windowOpened_3; }
	inline bool* get_address_of_windowOpened_3() { return &___windowOpened_3; }
	inline void set_windowOpened_3(bool value)
	{
		___windowOpened_3 = value;
	}

	inline static int32_t get_offset_of_shillingsHtml_4() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___shillingsHtml_4)); }
	inline String_t* get_shillingsHtml_4() const { return ___shillingsHtml_4; }
	inline String_t** get_address_of_shillingsHtml_4() { return &___shillingsHtml_4; }
	inline void set_shillingsHtml_4(String_t* value)
	{
		___shillingsHtml_4 = value;
		Il2CppCodeGenWriteBarrier((&___shillingsHtml_4), value);
	}

	inline static int32_t get_offset_of_BuildingConstructionWindow_5() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingConstructionWindow_5)); }
	inline GameObject_t1113636619 * get_BuildingConstructionWindow_5() const { return ___BuildingConstructionWindow_5; }
	inline GameObject_t1113636619 ** get_address_of_BuildingConstructionWindow_5() { return &___BuildingConstructionWindow_5; }
	inline void set_BuildingConstructionWindow_5(GameObject_t1113636619 * value)
	{
		___BuildingConstructionWindow_5 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingConstructionWindow_5), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindow_6() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindow_6)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindow_6() const { return ___BuildingInformationWindow_6; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindow_6() { return &___BuildingInformationWindow_6; }
	inline void set_BuildingInformationWindow_6(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindow_6 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindow_6), value);
	}

	inline static int32_t get_offset_of_ReportContainerWindow_7() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ReportContainerWindow_7)); }
	inline GameObject_t1113636619 * get_ReportContainerWindow_7() const { return ___ReportContainerWindow_7; }
	inline GameObject_t1113636619 ** get_address_of_ReportContainerWindow_7() { return &___ReportContainerWindow_7; }
	inline void set_ReportContainerWindow_7(GameObject_t1113636619 * value)
	{
		___ReportContainerWindow_7 = value;
		Il2CppCodeGenWriteBarrier((&___ReportContainerWindow_7), value);
	}

	inline static int32_t get_offset_of_ShopWindow_8() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShopWindow_8)); }
	inline GameObject_t1113636619 * get_ShopWindow_8() const { return ___ShopWindow_8; }
	inline GameObject_t1113636619 ** get_address_of_ShopWindow_8() { return &___ShopWindow_8; }
	inline void set_ShopWindow_8(GameObject_t1113636619 * value)
	{
		___ShopWindow_8 = value;
		Il2CppCodeGenWriteBarrier((&___ShopWindow_8), value);
	}

	inline static int32_t get_offset_of_MailWindow_9() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MailWindow_9)); }
	inline GameObject_t1113636619 * get_MailWindow_9() const { return ___MailWindow_9; }
	inline GameObject_t1113636619 ** get_address_of_MailWindow_9() { return &___MailWindow_9; }
	inline void set_MailWindow_9(GameObject_t1113636619 * value)
	{
		___MailWindow_9 = value;
		Il2CppCodeGenWriteBarrier((&___MailWindow_9), value);
	}

	inline static int32_t get_offset_of_AlliancesWindow_10() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AlliancesWindow_10)); }
	inline GameObject_t1113636619 * get_AlliancesWindow_10() const { return ___AlliancesWindow_10; }
	inline GameObject_t1113636619 ** get_address_of_AlliancesWindow_10() { return &___AlliancesWindow_10; }
	inline void set_AlliancesWindow_10(GameObject_t1113636619 * value)
	{
		___AlliancesWindow_10 = value;
		Il2CppCodeGenWriteBarrier((&___AlliancesWindow_10), value);
	}

	inline static int32_t get_offset_of_UserInfoWindow_11() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UserInfoWindow_11)); }
	inline GameObject_t1113636619 * get_UserInfoWindow_11() const { return ___UserInfoWindow_11; }
	inline GameObject_t1113636619 ** get_address_of_UserInfoWindow_11() { return &___UserInfoWindow_11; }
	inline void set_UserInfoWindow_11(GameObject_t1113636619 * value)
	{
		___UserInfoWindow_11 = value;
		Il2CppCodeGenWriteBarrier((&___UserInfoWindow_11), value);
	}

	inline static int32_t get_offset_of_FinanceWindow_12() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___FinanceWindow_12)); }
	inline GameObject_t1113636619 * get_FinanceWindow_12() const { return ___FinanceWindow_12; }
	inline GameObject_t1113636619 ** get_address_of_FinanceWindow_12() { return &___FinanceWindow_12; }
	inline void set_FinanceWindow_12(GameObject_t1113636619 * value)
	{
		___FinanceWindow_12 = value;
		Il2CppCodeGenWriteBarrier((&___FinanceWindow_12), value);
	}

	inline static int32_t get_offset_of_HelpWindow_13() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___HelpWindow_13)); }
	inline GameObject_t1113636619 * get_HelpWindow_13() const { return ___HelpWindow_13; }
	inline GameObject_t1113636619 ** get_address_of_HelpWindow_13() { return &___HelpWindow_13; }
	inline void set_HelpWindow_13(GameObject_t1113636619 * value)
	{
		___HelpWindow_13 = value;
		Il2CppCodeGenWriteBarrier((&___HelpWindow_13), value);
	}

	inline static int32_t get_offset_of_NewMailWindow_14() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewMailWindow_14)); }
	inline GameObject_t1113636619 * get_NewMailWindow_14() const { return ___NewMailWindow_14; }
	inline GameObject_t1113636619 ** get_address_of_NewMailWindow_14() { return &___NewMailWindow_14; }
	inline void set_NewMailWindow_14(GameObject_t1113636619 * value)
	{
		___NewMailWindow_14 = value;
		Il2CppCodeGenWriteBarrier((&___NewMailWindow_14), value);
	}

	inline static int32_t get_offset_of_MessageWindow_15() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageWindow_15)); }
	inline GameObject_t1113636619 * get_MessageWindow_15() const { return ___MessageWindow_15; }
	inline GameObject_t1113636619 ** get_address_of_MessageWindow_15() { return &___MessageWindow_15; }
	inline void set_MessageWindow_15(GameObject_t1113636619 * value)
	{
		___MessageWindow_15 = value;
		Il2CppCodeGenWriteBarrier((&___MessageWindow_15), value);
	}

	inline static int32_t get_offset_of_MessageAllianceMemberWindow_16() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageAllianceMemberWindow_16)); }
	inline GameObject_t1113636619 * get_MessageAllianceMemberWindow_16() const { return ___MessageAllianceMemberWindow_16; }
	inline GameObject_t1113636619 ** get_address_of_MessageAllianceMemberWindow_16() { return &___MessageAllianceMemberWindow_16; }
	inline void set_MessageAllianceMemberWindow_16(GameObject_t1113636619 * value)
	{
		___MessageAllianceMemberWindow_16 = value;
		Il2CppCodeGenWriteBarrier((&___MessageAllianceMemberWindow_16), value);
	}

	inline static int32_t get_offset_of_EventsWindow_17() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___EventsWindow_17)); }
	inline GameObject_t1113636619 * get_EventsWindow_17() const { return ___EventsWindow_17; }
	inline GameObject_t1113636619 ** get_address_of_EventsWindow_17() { return &___EventsWindow_17; }
	inline void set_EventsWindow_17(GameObject_t1113636619 * value)
	{
		___EventsWindow_17 = value;
		Il2CppCodeGenWriteBarrier((&___EventsWindow_17), value);
	}

	inline static int32_t get_offset_of_AllianceInfoWindow_18() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AllianceInfoWindow_18)); }
	inline GameObject_t1113636619 * get_AllianceInfoWindow_18() const { return ___AllianceInfoWindow_18; }
	inline GameObject_t1113636619 ** get_address_of_AllianceInfoWindow_18() { return &___AllianceInfoWindow_18; }
	inline void set_AllianceInfoWindow_18(GameObject_t1113636619 * value)
	{
		___AllianceInfoWindow_18 = value;
		Il2CppCodeGenWriteBarrier((&___AllianceInfoWindow_18), value);
	}

	inline static int32_t get_offset_of_UnitDetailsWindow_19() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UnitDetailsWindow_19)); }
	inline GameObject_t1113636619 * get_UnitDetailsWindow_19() const { return ___UnitDetailsWindow_19; }
	inline GameObject_t1113636619 ** get_address_of_UnitDetailsWindow_19() { return &___UnitDetailsWindow_19; }
	inline void set_UnitDetailsWindow_19(GameObject_t1113636619 * value)
	{
		___UnitDetailsWindow_19 = value;
		Il2CppCodeGenWriteBarrier((&___UnitDetailsWindow_19), value);
	}

	inline static int32_t get_offset_of_CityInfoWindow_20() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityInfoWindow_20)); }
	inline GameObject_t1113636619 * get_CityInfoWindow_20() const { return ___CityInfoWindow_20; }
	inline GameObject_t1113636619 ** get_address_of_CityInfoWindow_20() { return &___CityInfoWindow_20; }
	inline void set_CityInfoWindow_20(GameObject_t1113636619 * value)
	{
		___CityInfoWindow_20 = value;
		Il2CppCodeGenWriteBarrier((&___CityInfoWindow_20), value);
	}

	inline static int32_t get_offset_of_ValleyInfoWindow_21() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ValleyInfoWindow_21)); }
	inline GameObject_t1113636619 * get_ValleyInfoWindow_21() const { return ___ValleyInfoWindow_21; }
	inline GameObject_t1113636619 ** get_address_of_ValleyInfoWindow_21() { return &___ValleyInfoWindow_21; }
	inline void set_ValleyInfoWindow_21(GameObject_t1113636619 * value)
	{
		___ValleyInfoWindow_21 = value;
		Il2CppCodeGenWriteBarrier((&___ValleyInfoWindow_21), value);
	}

	inline static int32_t get_offset_of_DispatchWindow_22() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DispatchWindow_22)); }
	inline GameObject_t1113636619 * get_DispatchWindow_22() const { return ___DispatchWindow_22; }
	inline GameObject_t1113636619 ** get_address_of_DispatchWindow_22() { return &___DispatchWindow_22; }
	inline void set_DispatchWindow_22(GameObject_t1113636619 * value)
	{
		___DispatchWindow_22 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchWindow_22), value);
	}

	inline static int32_t get_offset_of_RecruitBarbariansWindow_23() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___RecruitBarbariansWindow_23)); }
	inline GameObject_t1113636619 * get_RecruitBarbariansWindow_23() const { return ___RecruitBarbariansWindow_23; }
	inline GameObject_t1113636619 ** get_address_of_RecruitBarbariansWindow_23() { return &___RecruitBarbariansWindow_23; }
	inline void set_RecruitBarbariansWindow_23(GameObject_t1113636619 * value)
	{
		___RecruitBarbariansWindow_23 = value;
		Il2CppCodeGenWriteBarrier((&___RecruitBarbariansWindow_23), value);
	}

	inline static int32_t get_offset_of_NotificationWindow_24() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NotificationWindow_24)); }
	inline GameObject_t1113636619 * get_NotificationWindow_24() const { return ___NotificationWindow_24; }
	inline GameObject_t1113636619 ** get_address_of_NotificationWindow_24() { return &___NotificationWindow_24; }
	inline void set_NotificationWindow_24(GameObject_t1113636619 * value)
	{
		___NotificationWindow_24 = value;
		Il2CppCodeGenWriteBarrier((&___NotificationWindow_24), value);
	}

	inline static int32_t get_offset_of_LoginProgressBar_25() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___LoginProgressBar_25)); }
	inline GameObject_t1113636619 * get_LoginProgressBar_25() const { return ___LoginProgressBar_25; }
	inline GameObject_t1113636619 ** get_address_of_LoginProgressBar_25() { return &___LoginProgressBar_25; }
	inline void set_LoginProgressBar_25(GameObject_t1113636619 * value)
	{
		___LoginProgressBar_25 = value;
		Il2CppCodeGenWriteBarrier((&___LoginProgressBar_25), value);
	}

	inline static int32_t get_offset_of_KnightInfoWindow_26() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___KnightInfoWindow_26)); }
	inline GameObject_t1113636619 * get_KnightInfoWindow_26() const { return ___KnightInfoWindow_26; }
	inline GameObject_t1113636619 ** get_address_of_KnightInfoWindow_26() { return &___KnightInfoWindow_26; }
	inline void set_KnightInfoWindow_26(GameObject_t1113636619 * value)
	{
		___KnightInfoWindow_26 = value;
		Il2CppCodeGenWriteBarrier((&___KnightInfoWindow_26), value);
	}

	inline static int32_t get_offset_of_BuyItemWindow_27() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyItemWindow_27)); }
	inline GameObject_t1113636619 * get_BuyItemWindow_27() const { return ___BuyItemWindow_27; }
	inline GameObject_t1113636619 ** get_address_of_BuyItemWindow_27() { return &___BuyItemWindow_27; }
	inline void set_BuyItemWindow_27(GameObject_t1113636619 * value)
	{
		___BuyItemWindow_27 = value;
		Il2CppCodeGenWriteBarrier((&___BuyItemWindow_27), value);
	}

	inline static int32_t get_offset_of_SellItemWindow_28() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SellItemWindow_28)); }
	inline GameObject_t1113636619 * get_SellItemWindow_28() const { return ___SellItemWindow_28; }
	inline GameObject_t1113636619 ** get_address_of_SellItemWindow_28() { return &___SellItemWindow_28; }
	inline void set_SellItemWindow_28(GameObject_t1113636619 * value)
	{
		___SellItemWindow_28 = value;
		Il2CppCodeGenWriteBarrier((&___SellItemWindow_28), value);
	}

	inline static int32_t get_offset_of_SpeedUpWindow_29() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SpeedUpWindow_29)); }
	inline GameObject_t1113636619 * get_SpeedUpWindow_29() const { return ___SpeedUpWindow_29; }
	inline GameObject_t1113636619 ** get_address_of_SpeedUpWindow_29() { return &___SpeedUpWindow_29; }
	inline void set_SpeedUpWindow_29(GameObject_t1113636619 * value)
	{
		___SpeedUpWindow_29 = value;
		Il2CppCodeGenWriteBarrier((&___SpeedUpWindow_29), value);
	}

	inline static int32_t get_offset_of_UpgradeBuildingWindow_30() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpgradeBuildingWindow_30)); }
	inline GameObject_t1113636619 * get_UpgradeBuildingWindow_30() const { return ___UpgradeBuildingWindow_30; }
	inline GameObject_t1113636619 ** get_address_of_UpgradeBuildingWindow_30() { return &___UpgradeBuildingWindow_30; }
	inline void set_UpgradeBuildingWindow_30(GameObject_t1113636619 * value)
	{
		___UpgradeBuildingWindow_30 = value;
		Il2CppCodeGenWriteBarrier((&___UpgradeBuildingWindow_30), value);
	}

	inline static int32_t get_offset_of_AdvencedTeleportWindow_31() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AdvencedTeleportWindow_31)); }
	inline GameObject_t1113636619 * get_AdvencedTeleportWindow_31() const { return ___AdvencedTeleportWindow_31; }
	inline GameObject_t1113636619 ** get_address_of_AdvencedTeleportWindow_31() { return &___AdvencedTeleportWindow_31; }
	inline void set_AdvencedTeleportWindow_31(GameObject_t1113636619 * value)
	{
		___AdvencedTeleportWindow_31 = value;
		Il2CppCodeGenWriteBarrier((&___AdvencedTeleportWindow_31), value);
	}

	inline static int32_t get_offset_of_CityDeedWindow_32() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityDeedWindow_32)); }
	inline GameObject_t1113636619 * get_CityDeedWindow_32() const { return ___CityDeedWindow_32; }
	inline GameObject_t1113636619 ** get_address_of_CityDeedWindow_32() { return &___CityDeedWindow_32; }
	inline void set_CityDeedWindow_32(GameObject_t1113636619 * value)
	{
		___CityDeedWindow_32 = value;
		Il2CppCodeGenWriteBarrier((&___CityDeedWindow_32), value);
	}

	inline static int32_t get_offset_of_BuyShillingsWindow_33() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyShillingsWindow_33)); }
	inline GameObject_t1113636619 * get_BuyShillingsWindow_33() const { return ___BuyShillingsWindow_33; }
	inline GameObject_t1113636619 ** get_address_of_BuyShillingsWindow_33() { return &___BuyShillingsWindow_33; }
	inline void set_BuyShillingsWindow_33(GameObject_t1113636619 * value)
	{
		___BuyShillingsWindow_33 = value;
		Il2CppCodeGenWriteBarrier((&___BuyShillingsWindow_33), value);
	}

	inline static int32_t get_offset_of_ChangeLanguageWindow_34() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangeLanguageWindow_34)); }
	inline GameObject_t1113636619 * get_ChangeLanguageWindow_34() const { return ___ChangeLanguageWindow_34; }
	inline GameObject_t1113636619 ** get_address_of_ChangeLanguageWindow_34() { return &___ChangeLanguageWindow_34; }
	inline void set_ChangeLanguageWindow_34(GameObject_t1113636619 * value)
	{
		___ChangeLanguageWindow_34 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeLanguageWindow_34), value);
	}

	inline static int32_t get_offset_of_DismissWindow_35() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DismissWindow_35)); }
	inline GameObject_t1113636619 * get_DismissWindow_35() const { return ___DismissWindow_35; }
	inline GameObject_t1113636619 ** get_address_of_DismissWindow_35() { return &___DismissWindow_35; }
	inline void set_DismissWindow_35(GameObject_t1113636619 * value)
	{
		___DismissWindow_35 = value;
		Il2CppCodeGenWriteBarrier((&___DismissWindow_35), value);
	}

	inline static int32_t get_offset_of_ChangePasswordWindow_36() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangePasswordWindow_36)); }
	inline GameObject_t1113636619 * get_ChangePasswordWindow_36() const { return ___ChangePasswordWindow_36; }
	inline GameObject_t1113636619 ** get_address_of_ChangePasswordWindow_36() { return &___ChangePasswordWindow_36; }
	inline void set_ChangePasswordWindow_36(GameObject_t1113636619 * value)
	{
		___ChangePasswordWindow_36 = value;
		Il2CppCodeGenWriteBarrier((&___ChangePasswordWindow_36), value);
	}

	inline static int32_t get_offset_of_TributesWindow_37() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TributesWindow_37)); }
	inline GameObject_t1113636619 * get_TributesWindow_37() const { return ___TributesWindow_37; }
	inline GameObject_t1113636619 ** get_address_of_TributesWindow_37() { return &___TributesWindow_37; }
	inline void set_TributesWindow_37(GameObject_t1113636619 * value)
	{
		___TributesWindow_37 = value;
		Il2CppCodeGenWriteBarrier((&___TributesWindow_37), value);
	}

	inline static int32_t get_offset_of_UpdateBuildingWindow_38() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpdateBuildingWindow_38)); }
	inline GameObject_t1113636619 * get_UpdateBuildingWindow_38() const { return ___UpdateBuildingWindow_38; }
	inline GameObject_t1113636619 ** get_address_of_UpdateBuildingWindow_38() { return &___UpdateBuildingWindow_38; }
	inline void set_UpdateBuildingWindow_38(GameObject_t1113636619 * value)
	{
		___UpdateBuildingWindow_38 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateBuildingWindow_38), value);
	}

	inline static int32_t get_offset_of_BattleLogWindow_39() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BattleLogWindow_39)); }
	inline GameObject_t1113636619 * get_BattleLogWindow_39() const { return ___BattleLogWindow_39; }
	inline GameObject_t1113636619 ** get_address_of_BattleLogWindow_39() { return &___BattleLogWindow_39; }
	inline void set_BattleLogWindow_39(GameObject_t1113636619 * value)
	{
		___BattleLogWindow_39 = value;
		Il2CppCodeGenWriteBarrier((&___BattleLogWindow_39), value);
	}

	inline static int32_t get_offset_of_BuildNewCityWindow_40() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildNewCityWindow_40)); }
	inline GameObject_t1113636619 * get_BuildNewCityWindow_40() const { return ___BuildNewCityWindow_40; }
	inline GameObject_t1113636619 ** get_address_of_BuildNewCityWindow_40() { return &___BuildNewCityWindow_40; }
	inline void set_BuildNewCityWindow_40(GameObject_t1113636619 * value)
	{
		___BuildNewCityWindow_40 = value;
		Il2CppCodeGenWriteBarrier((&___BuildNewCityWindow_40), value);
	}

	inline static int32_t get_offset_of_ArmyDetailsWIndow_41() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ArmyDetailsWIndow_41)); }
	inline GameObject_t1113636619 * get_ArmyDetailsWIndow_41() const { return ___ArmyDetailsWIndow_41; }
	inline GameObject_t1113636619 ** get_address_of_ArmyDetailsWIndow_41() { return &___ArmyDetailsWIndow_41; }
	inline void set_ArmyDetailsWIndow_41(GameObject_t1113636619 * value)
	{
		___ArmyDetailsWIndow_41 = value;
		Il2CppCodeGenWriteBarrier((&___ArmyDetailsWIndow_41), value);
	}

	inline static int32_t get_offset_of_NewAllianceNameWindow_42() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewAllianceNameWindow_42)); }
	inline GameObject_t1113636619 * get_NewAllianceNameWindow_42() const { return ___NewAllianceNameWindow_42; }
	inline GameObject_t1113636619 ** get_address_of_NewAllianceNameWindow_42() { return &___NewAllianceNameWindow_42; }
	inline void set_NewAllianceNameWindow_42(GameObject_t1113636619 * value)
	{
		___NewAllianceNameWindow_42 = value;
		Il2CppCodeGenWriteBarrier((&___NewAllianceNameWindow_42), value);
	}

	inline static int32_t get_offset_of_ConfirmationWindow_43() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ConfirmationWindow_43)); }
	inline GameObject_t1113636619 * get_ConfirmationWindow_43() const { return ___ConfirmationWindow_43; }
	inline GameObject_t1113636619 ** get_address_of_ConfirmationWindow_43() { return &___ConfirmationWindow_43; }
	inline void set_ConfirmationWindow_43(GameObject_t1113636619 * value)
	{
		___ConfirmationWindow_43 = value;
		Il2CppCodeGenWriteBarrier((&___ConfirmationWindow_43), value);
	}

	inline static int32_t get_offset_of_ShieldTimeWindow_44() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShieldTimeWindow_44)); }
	inline GameObject_t1113636619 * get_ShieldTimeWindow_44() const { return ___ShieldTimeWindow_44; }
	inline GameObject_t1113636619 ** get_address_of_ShieldTimeWindow_44() { return &___ShieldTimeWindow_44; }
	inline void set_ShieldTimeWindow_44(GameObject_t1113636619 * value)
	{
		___ShieldTimeWindow_44 = value;
		Il2CppCodeGenWriteBarrier((&___ShieldTimeWindow_44), value);
	}

	inline static int32_t get_offset_of_TutorialWindow_45() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TutorialWindow_45)); }
	inline GameObject_t1113636619 * get_TutorialWindow_45() const { return ___TutorialWindow_45; }
	inline GameObject_t1113636619 ** get_address_of_TutorialWindow_45() { return &___TutorialWindow_45; }
	inline void set_TutorialWindow_45(GameObject_t1113636619 * value)
	{
		___TutorialWindow_45 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialWindow_45), value);
	}

	inline static int32_t get_offset_of_VideoAdsWindow_46() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___VideoAdsWindow_46)); }
	inline GameObject_t1113636619 * get_VideoAdsWindow_46() const { return ___VideoAdsWindow_46; }
	inline GameObject_t1113636619 ** get_address_of_VideoAdsWindow_46() { return &___VideoAdsWindow_46; }
	inline void set_VideoAdsWindow_46(GameObject_t1113636619 * value)
	{
		___VideoAdsWindow_46 = value;
		Il2CppCodeGenWriteBarrier((&___VideoAdsWindow_46), value);
	}

	inline static int32_t get_offset_of_BuildingConstructionWindowInstance_47() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingConstructionWindowInstance_47)); }
	inline GameObject_t1113636619 * get_BuildingConstructionWindowInstance_47() const { return ___BuildingConstructionWindowInstance_47; }
	inline GameObject_t1113636619 ** get_address_of_BuildingConstructionWindowInstance_47() { return &___BuildingConstructionWindowInstance_47; }
	inline void set_BuildingConstructionWindowInstance_47(GameObject_t1113636619 * value)
	{
		___BuildingConstructionWindowInstance_47 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingConstructionWindowInstance_47), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindowInstance_48() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindowInstance_48)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindowInstance_48() const { return ___BuildingInformationWindowInstance_48; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindowInstance_48() { return &___BuildingInformationWindowInstance_48; }
	inline void set_BuildingInformationWindowInstance_48(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindowInstance_48 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindowInstance_48), value);
	}

	inline static int32_t get_offset_of_BuildingInformationWindowInstance2_49() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildingInformationWindowInstance2_49)); }
	inline GameObject_t1113636619 * get_BuildingInformationWindowInstance2_49() const { return ___BuildingInformationWindowInstance2_49; }
	inline GameObject_t1113636619 ** get_address_of_BuildingInformationWindowInstance2_49() { return &___BuildingInformationWindowInstance2_49; }
	inline void set_BuildingInformationWindowInstance2_49(GameObject_t1113636619 * value)
	{
		___BuildingInformationWindowInstance2_49 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingInformationWindowInstance2_49), value);
	}

	inline static int32_t get_offset_of_ReportContainerWindowInstance_50() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ReportContainerWindowInstance_50)); }
	inline GameObject_t1113636619 * get_ReportContainerWindowInstance_50() const { return ___ReportContainerWindowInstance_50; }
	inline GameObject_t1113636619 ** get_address_of_ReportContainerWindowInstance_50() { return &___ReportContainerWindowInstance_50; }
	inline void set_ReportContainerWindowInstance_50(GameObject_t1113636619 * value)
	{
		___ReportContainerWindowInstance_50 = value;
		Il2CppCodeGenWriteBarrier((&___ReportContainerWindowInstance_50), value);
	}

	inline static int32_t get_offset_of_ShopWindowInstance_51() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShopWindowInstance_51)); }
	inline GameObject_t1113636619 * get_ShopWindowInstance_51() const { return ___ShopWindowInstance_51; }
	inline GameObject_t1113636619 ** get_address_of_ShopWindowInstance_51() { return &___ShopWindowInstance_51; }
	inline void set_ShopWindowInstance_51(GameObject_t1113636619 * value)
	{
		___ShopWindowInstance_51 = value;
		Il2CppCodeGenWriteBarrier((&___ShopWindowInstance_51), value);
	}

	inline static int32_t get_offset_of_MailWindowInstance_52() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MailWindowInstance_52)); }
	inline GameObject_t1113636619 * get_MailWindowInstance_52() const { return ___MailWindowInstance_52; }
	inline GameObject_t1113636619 ** get_address_of_MailWindowInstance_52() { return &___MailWindowInstance_52; }
	inline void set_MailWindowInstance_52(GameObject_t1113636619 * value)
	{
		___MailWindowInstance_52 = value;
		Il2CppCodeGenWriteBarrier((&___MailWindowInstance_52), value);
	}

	inline static int32_t get_offset_of_AlliancesWindowInstance_53() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AlliancesWindowInstance_53)); }
	inline GameObject_t1113636619 * get_AlliancesWindowInstance_53() const { return ___AlliancesWindowInstance_53; }
	inline GameObject_t1113636619 ** get_address_of_AlliancesWindowInstance_53() { return &___AlliancesWindowInstance_53; }
	inline void set_AlliancesWindowInstance_53(GameObject_t1113636619 * value)
	{
		___AlliancesWindowInstance_53 = value;
		Il2CppCodeGenWriteBarrier((&___AlliancesWindowInstance_53), value);
	}

	inline static int32_t get_offset_of_UserInfoWindowInstance_54() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UserInfoWindowInstance_54)); }
	inline GameObject_t1113636619 * get_UserInfoWindowInstance_54() const { return ___UserInfoWindowInstance_54; }
	inline GameObject_t1113636619 ** get_address_of_UserInfoWindowInstance_54() { return &___UserInfoWindowInstance_54; }
	inline void set_UserInfoWindowInstance_54(GameObject_t1113636619 * value)
	{
		___UserInfoWindowInstance_54 = value;
		Il2CppCodeGenWriteBarrier((&___UserInfoWindowInstance_54), value);
	}

	inline static int32_t get_offset_of_FinanceWindowInstance_55() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___FinanceWindowInstance_55)); }
	inline GameObject_t1113636619 * get_FinanceWindowInstance_55() const { return ___FinanceWindowInstance_55; }
	inline GameObject_t1113636619 ** get_address_of_FinanceWindowInstance_55() { return &___FinanceWindowInstance_55; }
	inline void set_FinanceWindowInstance_55(GameObject_t1113636619 * value)
	{
		___FinanceWindowInstance_55 = value;
		Il2CppCodeGenWriteBarrier((&___FinanceWindowInstance_55), value);
	}

	inline static int32_t get_offset_of_HelpWindowInstance_56() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___HelpWindowInstance_56)); }
	inline GameObject_t1113636619 * get_HelpWindowInstance_56() const { return ___HelpWindowInstance_56; }
	inline GameObject_t1113636619 ** get_address_of_HelpWindowInstance_56() { return &___HelpWindowInstance_56; }
	inline void set_HelpWindowInstance_56(GameObject_t1113636619 * value)
	{
		___HelpWindowInstance_56 = value;
		Il2CppCodeGenWriteBarrier((&___HelpWindowInstance_56), value);
	}

	inline static int32_t get_offset_of_NewMailWindowInstance_57() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewMailWindowInstance_57)); }
	inline GameObject_t1113636619 * get_NewMailWindowInstance_57() const { return ___NewMailWindowInstance_57; }
	inline GameObject_t1113636619 ** get_address_of_NewMailWindowInstance_57() { return &___NewMailWindowInstance_57; }
	inline void set_NewMailWindowInstance_57(GameObject_t1113636619 * value)
	{
		___NewMailWindowInstance_57 = value;
		Il2CppCodeGenWriteBarrier((&___NewMailWindowInstance_57), value);
	}

	inline static int32_t get_offset_of_MessageWindowInstance_58() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageWindowInstance_58)); }
	inline GameObject_t1113636619 * get_MessageWindowInstance_58() const { return ___MessageWindowInstance_58; }
	inline GameObject_t1113636619 ** get_address_of_MessageWindowInstance_58() { return &___MessageWindowInstance_58; }
	inline void set_MessageWindowInstance_58(GameObject_t1113636619 * value)
	{
		___MessageWindowInstance_58 = value;
		Il2CppCodeGenWriteBarrier((&___MessageWindowInstance_58), value);
	}

	inline static int32_t get_offset_of_MessageAllianceMemberWindowInstance_59() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___MessageAllianceMemberWindowInstance_59)); }
	inline GameObject_t1113636619 * get_MessageAllianceMemberWindowInstance_59() const { return ___MessageAllianceMemberWindowInstance_59; }
	inline GameObject_t1113636619 ** get_address_of_MessageAllianceMemberWindowInstance_59() { return &___MessageAllianceMemberWindowInstance_59; }
	inline void set_MessageAllianceMemberWindowInstance_59(GameObject_t1113636619 * value)
	{
		___MessageAllianceMemberWindowInstance_59 = value;
		Il2CppCodeGenWriteBarrier((&___MessageAllianceMemberWindowInstance_59), value);
	}

	inline static int32_t get_offset_of_EventsWindowInstance_60() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___EventsWindowInstance_60)); }
	inline GameObject_t1113636619 * get_EventsWindowInstance_60() const { return ___EventsWindowInstance_60; }
	inline GameObject_t1113636619 ** get_address_of_EventsWindowInstance_60() { return &___EventsWindowInstance_60; }
	inline void set_EventsWindowInstance_60(GameObject_t1113636619 * value)
	{
		___EventsWindowInstance_60 = value;
		Il2CppCodeGenWriteBarrier((&___EventsWindowInstance_60), value);
	}

	inline static int32_t get_offset_of_AllianceInfoWindowInstance_61() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AllianceInfoWindowInstance_61)); }
	inline GameObject_t1113636619 * get_AllianceInfoWindowInstance_61() const { return ___AllianceInfoWindowInstance_61; }
	inline GameObject_t1113636619 ** get_address_of_AllianceInfoWindowInstance_61() { return &___AllianceInfoWindowInstance_61; }
	inline void set_AllianceInfoWindowInstance_61(GameObject_t1113636619 * value)
	{
		___AllianceInfoWindowInstance_61 = value;
		Il2CppCodeGenWriteBarrier((&___AllianceInfoWindowInstance_61), value);
	}

	inline static int32_t get_offset_of_UnitDetailsWindowInstance_62() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UnitDetailsWindowInstance_62)); }
	inline GameObject_t1113636619 * get_UnitDetailsWindowInstance_62() const { return ___UnitDetailsWindowInstance_62; }
	inline GameObject_t1113636619 ** get_address_of_UnitDetailsWindowInstance_62() { return &___UnitDetailsWindowInstance_62; }
	inline void set_UnitDetailsWindowInstance_62(GameObject_t1113636619 * value)
	{
		___UnitDetailsWindowInstance_62 = value;
		Il2CppCodeGenWriteBarrier((&___UnitDetailsWindowInstance_62), value);
	}

	inline static int32_t get_offset_of_CityInfoWindowInstance_63() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityInfoWindowInstance_63)); }
	inline GameObject_t1113636619 * get_CityInfoWindowInstance_63() const { return ___CityInfoWindowInstance_63; }
	inline GameObject_t1113636619 ** get_address_of_CityInfoWindowInstance_63() { return &___CityInfoWindowInstance_63; }
	inline void set_CityInfoWindowInstance_63(GameObject_t1113636619 * value)
	{
		___CityInfoWindowInstance_63 = value;
		Il2CppCodeGenWriteBarrier((&___CityInfoWindowInstance_63), value);
	}

	inline static int32_t get_offset_of_DispatchWindowInstance_64() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DispatchWindowInstance_64)); }
	inline GameObject_t1113636619 * get_DispatchWindowInstance_64() const { return ___DispatchWindowInstance_64; }
	inline GameObject_t1113636619 ** get_address_of_DispatchWindowInstance_64() { return &___DispatchWindowInstance_64; }
	inline void set_DispatchWindowInstance_64(GameObject_t1113636619 * value)
	{
		___DispatchWindowInstance_64 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchWindowInstance_64), value);
	}

	inline static int32_t get_offset_of_RecruitBarbariansWindowInstance_65() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___RecruitBarbariansWindowInstance_65)); }
	inline GameObject_t1113636619 * get_RecruitBarbariansWindowInstance_65() const { return ___RecruitBarbariansWindowInstance_65; }
	inline GameObject_t1113636619 ** get_address_of_RecruitBarbariansWindowInstance_65() { return &___RecruitBarbariansWindowInstance_65; }
	inline void set_RecruitBarbariansWindowInstance_65(GameObject_t1113636619 * value)
	{
		___RecruitBarbariansWindowInstance_65 = value;
		Il2CppCodeGenWriteBarrier((&___RecruitBarbariansWindowInstance_65), value);
	}

	inline static int32_t get_offset_of_ValleyInfoWindowInstance_66() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ValleyInfoWindowInstance_66)); }
	inline GameObject_t1113636619 * get_ValleyInfoWindowInstance_66() const { return ___ValleyInfoWindowInstance_66; }
	inline GameObject_t1113636619 ** get_address_of_ValleyInfoWindowInstance_66() { return &___ValleyInfoWindowInstance_66; }
	inline void set_ValleyInfoWindowInstance_66(GameObject_t1113636619 * value)
	{
		___ValleyInfoWindowInstance_66 = value;
		Il2CppCodeGenWriteBarrier((&___ValleyInfoWindowInstance_66), value);
	}

	inline static int32_t get_offset_of_NotificationWindowInstance_67() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NotificationWindowInstance_67)); }
	inline GameObject_t1113636619 * get_NotificationWindowInstance_67() const { return ___NotificationWindowInstance_67; }
	inline GameObject_t1113636619 ** get_address_of_NotificationWindowInstance_67() { return &___NotificationWindowInstance_67; }
	inline void set_NotificationWindowInstance_67(GameObject_t1113636619 * value)
	{
		___NotificationWindowInstance_67 = value;
		Il2CppCodeGenWriteBarrier((&___NotificationWindowInstance_67), value);
	}

	inline static int32_t get_offset_of_LoginProgressBarInstance_68() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___LoginProgressBarInstance_68)); }
	inline GameObject_t1113636619 * get_LoginProgressBarInstance_68() const { return ___LoginProgressBarInstance_68; }
	inline GameObject_t1113636619 ** get_address_of_LoginProgressBarInstance_68() { return &___LoginProgressBarInstance_68; }
	inline void set_LoginProgressBarInstance_68(GameObject_t1113636619 * value)
	{
		___LoginProgressBarInstance_68 = value;
		Il2CppCodeGenWriteBarrier((&___LoginProgressBarInstance_68), value);
	}

	inline static int32_t get_offset_of_KnightInfoWindowInstance_69() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___KnightInfoWindowInstance_69)); }
	inline GameObject_t1113636619 * get_KnightInfoWindowInstance_69() const { return ___KnightInfoWindowInstance_69; }
	inline GameObject_t1113636619 ** get_address_of_KnightInfoWindowInstance_69() { return &___KnightInfoWindowInstance_69; }
	inline void set_KnightInfoWindowInstance_69(GameObject_t1113636619 * value)
	{
		___KnightInfoWindowInstance_69 = value;
		Il2CppCodeGenWriteBarrier((&___KnightInfoWindowInstance_69), value);
	}

	inline static int32_t get_offset_of_BuyItemWindowInstance_70() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyItemWindowInstance_70)); }
	inline GameObject_t1113636619 * get_BuyItemWindowInstance_70() const { return ___BuyItemWindowInstance_70; }
	inline GameObject_t1113636619 ** get_address_of_BuyItemWindowInstance_70() { return &___BuyItemWindowInstance_70; }
	inline void set_BuyItemWindowInstance_70(GameObject_t1113636619 * value)
	{
		___BuyItemWindowInstance_70 = value;
		Il2CppCodeGenWriteBarrier((&___BuyItemWindowInstance_70), value);
	}

	inline static int32_t get_offset_of_SellItemWindowInstance_71() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SellItemWindowInstance_71)); }
	inline GameObject_t1113636619 * get_SellItemWindowInstance_71() const { return ___SellItemWindowInstance_71; }
	inline GameObject_t1113636619 ** get_address_of_SellItemWindowInstance_71() { return &___SellItemWindowInstance_71; }
	inline void set_SellItemWindowInstance_71(GameObject_t1113636619 * value)
	{
		___SellItemWindowInstance_71 = value;
		Il2CppCodeGenWriteBarrier((&___SellItemWindowInstance_71), value);
	}

	inline static int32_t get_offset_of_SpeedUpWindowInstance_72() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___SpeedUpWindowInstance_72)); }
	inline GameObject_t1113636619 * get_SpeedUpWindowInstance_72() const { return ___SpeedUpWindowInstance_72; }
	inline GameObject_t1113636619 ** get_address_of_SpeedUpWindowInstance_72() { return &___SpeedUpWindowInstance_72; }
	inline void set_SpeedUpWindowInstance_72(GameObject_t1113636619 * value)
	{
		___SpeedUpWindowInstance_72 = value;
		Il2CppCodeGenWriteBarrier((&___SpeedUpWindowInstance_72), value);
	}

	inline static int32_t get_offset_of_UpgradeBuildingWindowInstance_73() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpgradeBuildingWindowInstance_73)); }
	inline GameObject_t1113636619 * get_UpgradeBuildingWindowInstance_73() const { return ___UpgradeBuildingWindowInstance_73; }
	inline GameObject_t1113636619 ** get_address_of_UpgradeBuildingWindowInstance_73() { return &___UpgradeBuildingWindowInstance_73; }
	inline void set_UpgradeBuildingWindowInstance_73(GameObject_t1113636619 * value)
	{
		___UpgradeBuildingWindowInstance_73 = value;
		Il2CppCodeGenWriteBarrier((&___UpgradeBuildingWindowInstance_73), value);
	}

	inline static int32_t get_offset_of_AdvencedTeleportWindowInstance_74() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___AdvencedTeleportWindowInstance_74)); }
	inline GameObject_t1113636619 * get_AdvencedTeleportWindowInstance_74() const { return ___AdvencedTeleportWindowInstance_74; }
	inline GameObject_t1113636619 ** get_address_of_AdvencedTeleportWindowInstance_74() { return &___AdvencedTeleportWindowInstance_74; }
	inline void set_AdvencedTeleportWindowInstance_74(GameObject_t1113636619 * value)
	{
		___AdvencedTeleportWindowInstance_74 = value;
		Il2CppCodeGenWriteBarrier((&___AdvencedTeleportWindowInstance_74), value);
	}

	inline static int32_t get_offset_of_CityDeedWindowInstance_75() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___CityDeedWindowInstance_75)); }
	inline GameObject_t1113636619 * get_CityDeedWindowInstance_75() const { return ___CityDeedWindowInstance_75; }
	inline GameObject_t1113636619 ** get_address_of_CityDeedWindowInstance_75() { return &___CityDeedWindowInstance_75; }
	inline void set_CityDeedWindowInstance_75(GameObject_t1113636619 * value)
	{
		___CityDeedWindowInstance_75 = value;
		Il2CppCodeGenWriteBarrier((&___CityDeedWindowInstance_75), value);
	}

	inline static int32_t get_offset_of_BuyShillingsWindowInstance_76() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuyShillingsWindowInstance_76)); }
	inline GameObject_t1113636619 * get_BuyShillingsWindowInstance_76() const { return ___BuyShillingsWindowInstance_76; }
	inline GameObject_t1113636619 ** get_address_of_BuyShillingsWindowInstance_76() { return &___BuyShillingsWindowInstance_76; }
	inline void set_BuyShillingsWindowInstance_76(GameObject_t1113636619 * value)
	{
		___BuyShillingsWindowInstance_76 = value;
		Il2CppCodeGenWriteBarrier((&___BuyShillingsWindowInstance_76), value);
	}

	inline static int32_t get_offset_of_ChangeLanguageWindowInstance_77() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangeLanguageWindowInstance_77)); }
	inline GameObject_t1113636619 * get_ChangeLanguageWindowInstance_77() const { return ___ChangeLanguageWindowInstance_77; }
	inline GameObject_t1113636619 ** get_address_of_ChangeLanguageWindowInstance_77() { return &___ChangeLanguageWindowInstance_77; }
	inline void set_ChangeLanguageWindowInstance_77(GameObject_t1113636619 * value)
	{
		___ChangeLanguageWindowInstance_77 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeLanguageWindowInstance_77), value);
	}

	inline static int32_t get_offset_of_DismissWindowInstance_78() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___DismissWindowInstance_78)); }
	inline GameObject_t1113636619 * get_DismissWindowInstance_78() const { return ___DismissWindowInstance_78; }
	inline GameObject_t1113636619 ** get_address_of_DismissWindowInstance_78() { return &___DismissWindowInstance_78; }
	inline void set_DismissWindowInstance_78(GameObject_t1113636619 * value)
	{
		___DismissWindowInstance_78 = value;
		Il2CppCodeGenWriteBarrier((&___DismissWindowInstance_78), value);
	}

	inline static int32_t get_offset_of_ChangePasswordWindowInstance_79() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ChangePasswordWindowInstance_79)); }
	inline GameObject_t1113636619 * get_ChangePasswordWindowInstance_79() const { return ___ChangePasswordWindowInstance_79; }
	inline GameObject_t1113636619 ** get_address_of_ChangePasswordWindowInstance_79() { return &___ChangePasswordWindowInstance_79; }
	inline void set_ChangePasswordWindowInstance_79(GameObject_t1113636619 * value)
	{
		___ChangePasswordWindowInstance_79 = value;
		Il2CppCodeGenWriteBarrier((&___ChangePasswordWindowInstance_79), value);
	}

	inline static int32_t get_offset_of_TributesWindowInstance_80() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TributesWindowInstance_80)); }
	inline GameObject_t1113636619 * get_TributesWindowInstance_80() const { return ___TributesWindowInstance_80; }
	inline GameObject_t1113636619 ** get_address_of_TributesWindowInstance_80() { return &___TributesWindowInstance_80; }
	inline void set_TributesWindowInstance_80(GameObject_t1113636619 * value)
	{
		___TributesWindowInstance_80 = value;
		Il2CppCodeGenWriteBarrier((&___TributesWindowInstance_80), value);
	}

	inline static int32_t get_offset_of_UpdateBuildingWindowInstance_81() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UpdateBuildingWindowInstance_81)); }
	inline GameObject_t1113636619 * get_UpdateBuildingWindowInstance_81() const { return ___UpdateBuildingWindowInstance_81; }
	inline GameObject_t1113636619 ** get_address_of_UpdateBuildingWindowInstance_81() { return &___UpdateBuildingWindowInstance_81; }
	inline void set_UpdateBuildingWindowInstance_81(GameObject_t1113636619 * value)
	{
		___UpdateBuildingWindowInstance_81 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateBuildingWindowInstance_81), value);
	}

	inline static int32_t get_offset_of_BattleLogWindowInstance_82() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BattleLogWindowInstance_82)); }
	inline GameObject_t1113636619 * get_BattleLogWindowInstance_82() const { return ___BattleLogWindowInstance_82; }
	inline GameObject_t1113636619 ** get_address_of_BattleLogWindowInstance_82() { return &___BattleLogWindowInstance_82; }
	inline void set_BattleLogWindowInstance_82(GameObject_t1113636619 * value)
	{
		___BattleLogWindowInstance_82 = value;
		Il2CppCodeGenWriteBarrier((&___BattleLogWindowInstance_82), value);
	}

	inline static int32_t get_offset_of_BuildNewCityWindowInstance_83() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___BuildNewCityWindowInstance_83)); }
	inline GameObject_t1113636619 * get_BuildNewCityWindowInstance_83() const { return ___BuildNewCityWindowInstance_83; }
	inline GameObject_t1113636619 ** get_address_of_BuildNewCityWindowInstance_83() { return &___BuildNewCityWindowInstance_83; }
	inline void set_BuildNewCityWindowInstance_83(GameObject_t1113636619 * value)
	{
		___BuildNewCityWindowInstance_83 = value;
		Il2CppCodeGenWriteBarrier((&___BuildNewCityWindowInstance_83), value);
	}

	inline static int32_t get_offset_of_ArmyDetailsWindowInstance_84() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ArmyDetailsWindowInstance_84)); }
	inline GameObject_t1113636619 * get_ArmyDetailsWindowInstance_84() const { return ___ArmyDetailsWindowInstance_84; }
	inline GameObject_t1113636619 ** get_address_of_ArmyDetailsWindowInstance_84() { return &___ArmyDetailsWindowInstance_84; }
	inline void set_ArmyDetailsWindowInstance_84(GameObject_t1113636619 * value)
	{
		___ArmyDetailsWindowInstance_84 = value;
		Il2CppCodeGenWriteBarrier((&___ArmyDetailsWindowInstance_84), value);
	}

	inline static int32_t get_offset_of_ShillingsWebViewWindowInstance_85() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShillingsWebViewWindowInstance_85)); }
	inline GameObject_t1113636619 * get_ShillingsWebViewWindowInstance_85() const { return ___ShillingsWebViewWindowInstance_85; }
	inline GameObject_t1113636619 ** get_address_of_ShillingsWebViewWindowInstance_85() { return &___ShillingsWebViewWindowInstance_85; }
	inline void set_ShillingsWebViewWindowInstance_85(GameObject_t1113636619 * value)
	{
		___ShillingsWebViewWindowInstance_85 = value;
		Il2CppCodeGenWriteBarrier((&___ShillingsWebViewWindowInstance_85), value);
	}

	inline static int32_t get_offset_of_NewAllianceNameWindowInstance_86() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___NewAllianceNameWindowInstance_86)); }
	inline GameObject_t1113636619 * get_NewAllianceNameWindowInstance_86() const { return ___NewAllianceNameWindowInstance_86; }
	inline GameObject_t1113636619 ** get_address_of_NewAllianceNameWindowInstance_86() { return &___NewAllianceNameWindowInstance_86; }
	inline void set_NewAllianceNameWindowInstance_86(GameObject_t1113636619 * value)
	{
		___NewAllianceNameWindowInstance_86 = value;
		Il2CppCodeGenWriteBarrier((&___NewAllianceNameWindowInstance_86), value);
	}

	inline static int32_t get_offset_of_ConfirmationWindowInstance_87() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ConfirmationWindowInstance_87)); }
	inline GameObject_t1113636619 * get_ConfirmationWindowInstance_87() const { return ___ConfirmationWindowInstance_87; }
	inline GameObject_t1113636619 ** get_address_of_ConfirmationWindowInstance_87() { return &___ConfirmationWindowInstance_87; }
	inline void set_ConfirmationWindowInstance_87(GameObject_t1113636619 * value)
	{
		___ConfirmationWindowInstance_87 = value;
		Il2CppCodeGenWriteBarrier((&___ConfirmationWindowInstance_87), value);
	}

	inline static int32_t get_offset_of_ShieldTimeWindowInstance_88() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___ShieldTimeWindowInstance_88)); }
	inline GameObject_t1113636619 * get_ShieldTimeWindowInstance_88() const { return ___ShieldTimeWindowInstance_88; }
	inline GameObject_t1113636619 ** get_address_of_ShieldTimeWindowInstance_88() { return &___ShieldTimeWindowInstance_88; }
	inline void set_ShieldTimeWindowInstance_88(GameObject_t1113636619 * value)
	{
		___ShieldTimeWindowInstance_88 = value;
		Il2CppCodeGenWriteBarrier((&___ShieldTimeWindowInstance_88), value);
	}

	inline static int32_t get_offset_of_TutorialWindowInstance_89() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___TutorialWindowInstance_89)); }
	inline GameObject_t1113636619 * get_TutorialWindowInstance_89() const { return ___TutorialWindowInstance_89; }
	inline GameObject_t1113636619 ** get_address_of_TutorialWindowInstance_89() { return &___TutorialWindowInstance_89; }
	inline void set_TutorialWindowInstance_89(GameObject_t1113636619 * value)
	{
		___TutorialWindowInstance_89 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialWindowInstance_89), value);
	}

	inline static int32_t get_offset_of_VideoAdsWindowInstance_90() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___VideoAdsWindowInstance_90)); }
	inline GameObject_t1113636619 * get_VideoAdsWindowInstance_90() const { return ___VideoAdsWindowInstance_90; }
	inline GameObject_t1113636619 ** get_address_of_VideoAdsWindowInstance_90() { return &___VideoAdsWindowInstance_90; }
	inline void set_VideoAdsWindowInstance_90(GameObject_t1113636619 * value)
	{
		___VideoAdsWindowInstance_90 = value;
		Il2CppCodeGenWriteBarrier((&___VideoAdsWindowInstance_90), value);
	}

	inline static int32_t get_offset_of_overviewPanel_91() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___overviewPanel_91)); }
	inline GameObject_t1113636619 * get_overviewPanel_91() const { return ___overviewPanel_91; }
	inline GameObject_t1113636619 ** get_address_of_overviewPanel_91() { return &___overviewPanel_91; }
	inline void set_overviewPanel_91(GameObject_t1113636619 * value)
	{
		___overviewPanel_91 = value;
		Il2CppCodeGenWriteBarrier((&___overviewPanel_91), value);
	}

	inline static int32_t get_offset_of_statisticsPanel_92() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___statisticsPanel_92)); }
	inline GameObject_t1113636619 * get_statisticsPanel_92() const { return ___statisticsPanel_92; }
	inline GameObject_t1113636619 ** get_address_of_statisticsPanel_92() { return &___statisticsPanel_92; }
	inline void set_statisticsPanel_92(GameObject_t1113636619 * value)
	{
		___statisticsPanel_92 = value;
		Il2CppCodeGenWriteBarrier((&___statisticsPanel_92), value);
	}

	inline static int32_t get_offset_of_actionPanel_93() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___actionPanel_93)); }
	inline GameObject_t1113636619 * get_actionPanel_93() const { return ___actionPanel_93; }
	inline GameObject_t1113636619 ** get_address_of_actionPanel_93() { return &___actionPanel_93; }
	inline void set_actionPanel_93(GameObject_t1113636619 * value)
	{
		___actionPanel_93 = value;
		Il2CppCodeGenWriteBarrier((&___actionPanel_93), value);
	}

	inline static int32_t get_offset_of_actionManager_94() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___actionManager_94)); }
	inline ActionManager_t2430268190 * get_actionManager_94() const { return ___actionManager_94; }
	inline ActionManager_t2430268190 ** get_address_of_actionManager_94() { return &___actionManager_94; }
	inline void set_actionManager_94(ActionManager_t2430268190 * value)
	{
		___actionManager_94 = value;
		Il2CppCodeGenWriteBarrier((&___actionManager_94), value);
	}

	inline static int32_t get_offset_of_allianceManager_95() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___allianceManager_95)); }
	inline AllianceContentChanger_t2140085977 * get_allianceManager_95() const { return ___allianceManager_95; }
	inline AllianceContentChanger_t2140085977 ** get_address_of_allianceManager_95() { return &___allianceManager_95; }
	inline void set_allianceManager_95(AllianceContentChanger_t2140085977 * value)
	{
		___allianceManager_95 = value;
		Il2CppCodeGenWriteBarrier((&___allianceManager_95), value);
	}

	inline static int32_t get_offset_of_treasureManager_96() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___treasureManager_96)); }
	inline TreasureManager_t110095690 * get_treasureManager_96() const { return ___treasureManager_96; }
	inline TreasureManager_t110095690 ** get_address_of_treasureManager_96() { return &___treasureManager_96; }
	inline void set_treasureManager_96(TreasureManager_t110095690 * value)
	{
		___treasureManager_96 = value;
		Il2CppCodeGenWriteBarrier((&___treasureManager_96), value);
	}

	inline static int32_t get_offset_of_oldDesignPanel_97() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___oldDesignPanel_97)); }
	inline GameObject_t1113636619 * get_oldDesignPanel_97() const { return ___oldDesignPanel_97; }
	inline GameObject_t1113636619 ** get_address_of_oldDesignPanel_97() { return &___oldDesignPanel_97; }
	inline void set_oldDesignPanel_97(GameObject_t1113636619 * value)
	{
		___oldDesignPanel_97 = value;
		Il2CppCodeGenWriteBarrier((&___oldDesignPanel_97), value);
	}

	inline static int32_t get_offset_of_newDesignPanel_98() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___newDesignPanel_98)); }
	inline GameObject_t1113636619 * get_newDesignPanel_98() const { return ___newDesignPanel_98; }
	inline GameObject_t1113636619 ** get_address_of_newDesignPanel_98() { return &___newDesignPanel_98; }
	inline void set_newDesignPanel_98(GameObject_t1113636619 * value)
	{
		___newDesignPanel_98 = value;
		Il2CppCodeGenWriteBarrier((&___newDesignPanel_98), value);
	}

	inline static int32_t get_offset_of_viewPanel_99() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewPanel_99)); }
	inline GameObject_t1113636619 * get_viewPanel_99() const { return ___viewPanel_99; }
	inline GameObject_t1113636619 ** get_address_of_viewPanel_99() { return &___viewPanel_99; }
	inline void set_viewPanel_99(GameObject_t1113636619 * value)
	{
		___viewPanel_99 = value;
		Il2CppCodeGenWriteBarrier((&___viewPanel_99), value);
	}

	inline static int32_t get_offset_of_itemPanel_100() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemPanel_100)); }
	inline GameObject_t1113636619 * get_itemPanel_100() const { return ___itemPanel_100; }
	inline GameObject_t1113636619 ** get_address_of_itemPanel_100() { return &___itemPanel_100; }
	inline void set_itemPanel_100(GameObject_t1113636619 * value)
	{
		___itemPanel_100 = value;
		Il2CppCodeGenWriteBarrier((&___itemPanel_100), value);
	}

	inline static int32_t get_offset_of_citiesPanel_101() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___citiesPanel_101)); }
	inline GameObject_t1113636619 * get_citiesPanel_101() const { return ___citiesPanel_101; }
	inline GameObject_t1113636619 ** get_address_of_citiesPanel_101() { return &___citiesPanel_101; }
	inline void set_citiesPanel_101(GameObject_t1113636619 * value)
	{
		___citiesPanel_101 = value;
		Il2CppCodeGenWriteBarrier((&___citiesPanel_101), value);
	}

	inline static int32_t get_offset_of_viewButton_102() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewButton_102)); }
	inline Image_t2670269651 * get_viewButton_102() const { return ___viewButton_102; }
	inline Image_t2670269651 ** get_address_of_viewButton_102() { return &___viewButton_102; }
	inline void set_viewButton_102(Image_t2670269651 * value)
	{
		___viewButton_102 = value;
		Il2CppCodeGenWriteBarrier((&___viewButton_102), value);
	}

	inline static int32_t get_offset_of_itemButton_103() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemButton_103)); }
	inline Image_t2670269651 * get_itemButton_103() const { return ___itemButton_103; }
	inline Image_t2670269651 ** get_address_of_itemButton_103() { return &___itemButton_103; }
	inline void set_itemButton_103(Image_t2670269651 * value)
	{
		___itemButton_103 = value;
		Il2CppCodeGenWriteBarrier((&___itemButton_103), value);
	}

	inline static int32_t get_offset_of_voiceButton_104() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceButton_104)); }
	inline Image_t2670269651 * get_voiceButton_104() const { return ___voiceButton_104; }
	inline Image_t2670269651 ** get_address_of_voiceButton_104() { return &___voiceButton_104; }
	inline void set_voiceButton_104(Image_t2670269651 * value)
	{
		___voiceButton_104 = value;
		Il2CppCodeGenWriteBarrier((&___voiceButton_104), value);
	}

	inline static int32_t get_offset_of_viewClosedSprite_105() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewClosedSprite_105)); }
	inline Sprite_t280657092 * get_viewClosedSprite_105() const { return ___viewClosedSprite_105; }
	inline Sprite_t280657092 ** get_address_of_viewClosedSprite_105() { return &___viewClosedSprite_105; }
	inline void set_viewClosedSprite_105(Sprite_t280657092 * value)
	{
		___viewClosedSprite_105 = value;
		Il2CppCodeGenWriteBarrier((&___viewClosedSprite_105), value);
	}

	inline static int32_t get_offset_of_viewOpenedSprite_106() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___viewOpenedSprite_106)); }
	inline Sprite_t280657092 * get_viewOpenedSprite_106() const { return ___viewOpenedSprite_106; }
	inline Sprite_t280657092 ** get_address_of_viewOpenedSprite_106() { return &___viewOpenedSprite_106; }
	inline void set_viewOpenedSprite_106(Sprite_t280657092 * value)
	{
		___viewOpenedSprite_106 = value;
		Il2CppCodeGenWriteBarrier((&___viewOpenedSprite_106), value);
	}

	inline static int32_t get_offset_of_itemClosedSprite_107() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemClosedSprite_107)); }
	inline Sprite_t280657092 * get_itemClosedSprite_107() const { return ___itemClosedSprite_107; }
	inline Sprite_t280657092 ** get_address_of_itemClosedSprite_107() { return &___itemClosedSprite_107; }
	inline void set_itemClosedSprite_107(Sprite_t280657092 * value)
	{
		___itemClosedSprite_107 = value;
		Il2CppCodeGenWriteBarrier((&___itemClosedSprite_107), value);
	}

	inline static int32_t get_offset_of_itemOpenedSprite_108() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___itemOpenedSprite_108)); }
	inline Sprite_t280657092 * get_itemOpenedSprite_108() const { return ___itemOpenedSprite_108; }
	inline Sprite_t280657092 ** get_address_of_itemOpenedSprite_108() { return &___itemOpenedSprite_108; }
	inline void set_itemOpenedSprite_108(Sprite_t280657092 * value)
	{
		___itemOpenedSprite_108 = value;
		Il2CppCodeGenWriteBarrier((&___itemOpenedSprite_108), value);
	}

	inline static int32_t get_offset_of_voiceOnSprite_109() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceOnSprite_109)); }
	inline Sprite_t280657092 * get_voiceOnSprite_109() const { return ___voiceOnSprite_109; }
	inline Sprite_t280657092 ** get_address_of_voiceOnSprite_109() { return &___voiceOnSprite_109; }
	inline void set_voiceOnSprite_109(Sprite_t280657092 * value)
	{
		___voiceOnSprite_109 = value;
		Il2CppCodeGenWriteBarrier((&___voiceOnSprite_109), value);
	}

	inline static int32_t get_offset_of_voiceOffSprite_110() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___voiceOffSprite_110)); }
	inline Sprite_t280657092 * get_voiceOffSprite_110() const { return ___voiceOffSprite_110; }
	inline Sprite_t280657092 ** get_address_of_voiceOffSprite_110() { return &___voiceOffSprite_110; }
	inline void set_voiceOffSprite_110(Sprite_t280657092 * value)
	{
		___voiceOffSprite_110 = value;
		Il2CppCodeGenWriteBarrier((&___voiceOffSprite_110), value);
	}

	inline static int32_t get_offset_of_UI_111() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___UI_111)); }
	inline GameObject_t1113636619 * get_UI_111() const { return ___UI_111; }
	inline GameObject_t1113636619 ** get_address_of_UI_111() { return &___UI_111; }
	inline void set_UI_111(GameObject_t1113636619 * value)
	{
		___UI_111 = value;
		Il2CppCodeGenWriteBarrier((&___UI_111), value);
	}

	inline static int32_t get_offset_of_Shell_112() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687, ___Shell_112)); }
	inline GameObject_t1113636619 * get_Shell_112() const { return ___Shell_112; }
	inline GameObject_t1113636619 ** get_address_of_Shell_112() { return &___Shell_112; }
	inline void set_Shell_112(GameObject_t1113636619 * value)
	{
		___Shell_112 = value;
		Il2CppCodeGenWriteBarrier((&___Shell_112), value);
	}
};

struct WindowInstanceManager_t3234774687_StaticFields
{
public:
	// WindowInstanceManager WindowInstanceManager::instance
	WindowInstanceManager_t3234774687 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(WindowInstanceManager_t3234774687_StaticFields, ___instance_2)); }
	inline WindowInstanceManager_t3234774687 * get_instance_2() const { return ___instance_2; }
	inline WindowInstanceManager_t3234774687 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(WindowInstanceManager_t3234774687 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWINSTANCEMANAGER_T3234774687_H
#ifndef WORLDCHAT_T753147188_H
#define WORLDCHAT_T753147188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChat
struct  WorldChat_t753147188  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text WorldChat::chatWindow
	Text_t1901882714 * ___chatWindow_2;
	// UnityEngine.UI.InputField WorldChat::input
	InputField_t3762917431 * ___input_3;
	// UnityEngine.UI.Scrollbar WorldChat::Scroll
	Scrollbar_t1494447233 * ___Scroll_4;
	// System.String WorldChat::MessageBuffer
	String_t* ___MessageBuffer_5;
	// System.Boolean WorldChat::chatWorking
	bool ___chatWorking_6;
	// System.Boolean WorldChat::readyToChat
	bool ___readyToChat_7;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___chatWindow_2)); }
	inline Text_t1901882714 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t1901882714 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t1901882714 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier((&___chatWindow_2), value);
	}

	inline static int32_t get_offset_of_input_3() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___input_3)); }
	inline InputField_t3762917431 * get_input_3() const { return ___input_3; }
	inline InputField_t3762917431 ** get_address_of_input_3() { return &___input_3; }
	inline void set_input_3(InputField_t3762917431 * value)
	{
		___input_3 = value;
		Il2CppCodeGenWriteBarrier((&___input_3), value);
	}

	inline static int32_t get_offset_of_Scroll_4() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___Scroll_4)); }
	inline Scrollbar_t1494447233 * get_Scroll_4() const { return ___Scroll_4; }
	inline Scrollbar_t1494447233 ** get_address_of_Scroll_4() { return &___Scroll_4; }
	inline void set_Scroll_4(Scrollbar_t1494447233 * value)
	{
		___Scroll_4 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll_4), value);
	}

	inline static int32_t get_offset_of_MessageBuffer_5() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___MessageBuffer_5)); }
	inline String_t* get_MessageBuffer_5() const { return ___MessageBuffer_5; }
	inline String_t** get_address_of_MessageBuffer_5() { return &___MessageBuffer_5; }
	inline void set_MessageBuffer_5(String_t* value)
	{
		___MessageBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___MessageBuffer_5), value);
	}

	inline static int32_t get_offset_of_chatWorking_6() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___chatWorking_6)); }
	inline bool get_chatWorking_6() const { return ___chatWorking_6; }
	inline bool* get_address_of_chatWorking_6() { return &___chatWorking_6; }
	inline void set_chatWorking_6(bool value)
	{
		___chatWorking_6 = value;
	}

	inline static int32_t get_offset_of_readyToChat_7() { return static_cast<int32_t>(offsetof(WorldChat_t753147188, ___readyToChat_7)); }
	inline bool get_readyToChat_7() const { return ___readyToChat_7; }
	inline bool* get_address_of_readyToChat_7() { return &___readyToChat_7; }
	inline void set_readyToChat_7(bool value)
	{
		___readyToChat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCHAT_T753147188_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef WORLDMAPCOORDSCHANGER_T692571169_H
#define WORLDMAPCOORDSCHANGER_T692571169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapCoordsChanger
struct  WorldMapCoordsChanger_t692571169  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField WorldMapCoordsChanger::xInput
	InputField_t3762917431 * ___xInput_2;
	// UnityEngine.UI.InputField WorldMapCoordsChanger::yInput
	InputField_t3762917431 * ___yInput_3;
	// TestWorldChunksManager WorldMapCoordsChanger::chunksManager
	TestWorldChunksManager_t1200850834 * ___chunksManager_4;

public:
	inline static int32_t get_offset_of_xInput_2() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___xInput_2)); }
	inline InputField_t3762917431 * get_xInput_2() const { return ___xInput_2; }
	inline InputField_t3762917431 ** get_address_of_xInput_2() { return &___xInput_2; }
	inline void set_xInput_2(InputField_t3762917431 * value)
	{
		___xInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___xInput_2), value);
	}

	inline static int32_t get_offset_of_yInput_3() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___yInput_3)); }
	inline InputField_t3762917431 * get_yInput_3() const { return ___yInput_3; }
	inline InputField_t3762917431 ** get_address_of_yInput_3() { return &___yInput_3; }
	inline void set_yInput_3(InputField_t3762917431 * value)
	{
		___yInput_3 = value;
		Il2CppCodeGenWriteBarrier((&___yInput_3), value);
	}

	inline static int32_t get_offset_of_chunksManager_4() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t692571169, ___chunksManager_4)); }
	inline TestWorldChunksManager_t1200850834 * get_chunksManager_4() const { return ___chunksManager_4; }
	inline TestWorldChunksManager_t1200850834 ** get_address_of_chunksManager_4() { return &___chunksManager_4; }
	inline void set_chunksManager_4(TestWorldChunksManager_t1200850834 * value)
	{
		___chunksManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___chunksManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDMAPCOORDSCHANGER_T692571169_H
#ifndef WORLDFIELDMANAGER_T2312070818_H
#define WORLDFIELDMANAGER_T2312070818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldManager
struct  WorldFieldManager_t2312070818  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 WorldFieldManager::internalPitId
	int32_t ___internalPitId_2;
	// UnityEngine.GameObject WorldFieldManager::fieldObject
	GameObject_t1113636619 * ___fieldObject_3;
	// UnityEngine.GameObject WorldFieldManager::flag
	GameObject_t1113636619 * ___flag_4;
	// UnityEngine.GameObject WorldFieldManager::occupationIcon
	GameObject_t1113636619 * ___occupationIcon_5;
	// WorldFieldModel WorldFieldManager::fieldInfo
	WorldFieldModel_t2417974361 * ___fieldInfo_6;

public:
	inline static int32_t get_offset_of_internalPitId_2() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___internalPitId_2)); }
	inline int32_t get_internalPitId_2() const { return ___internalPitId_2; }
	inline int32_t* get_address_of_internalPitId_2() { return &___internalPitId_2; }
	inline void set_internalPitId_2(int32_t value)
	{
		___internalPitId_2 = value;
	}

	inline static int32_t get_offset_of_fieldObject_3() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___fieldObject_3)); }
	inline GameObject_t1113636619 * get_fieldObject_3() const { return ___fieldObject_3; }
	inline GameObject_t1113636619 ** get_address_of_fieldObject_3() { return &___fieldObject_3; }
	inline void set_fieldObject_3(GameObject_t1113636619 * value)
	{
		___fieldObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___fieldObject_3), value);
	}

	inline static int32_t get_offset_of_flag_4() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___flag_4)); }
	inline GameObject_t1113636619 * get_flag_4() const { return ___flag_4; }
	inline GameObject_t1113636619 ** get_address_of_flag_4() { return &___flag_4; }
	inline void set_flag_4(GameObject_t1113636619 * value)
	{
		___flag_4 = value;
		Il2CppCodeGenWriteBarrier((&___flag_4), value);
	}

	inline static int32_t get_offset_of_occupationIcon_5() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___occupationIcon_5)); }
	inline GameObject_t1113636619 * get_occupationIcon_5() const { return ___occupationIcon_5; }
	inline GameObject_t1113636619 ** get_address_of_occupationIcon_5() { return &___occupationIcon_5; }
	inline void set_occupationIcon_5(GameObject_t1113636619 * value)
	{
		___occupationIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___occupationIcon_5), value);
	}

	inline static int32_t get_offset_of_fieldInfo_6() { return static_cast<int32_t>(offsetof(WorldFieldManager_t2312070818, ___fieldInfo_6)); }
	inline WorldFieldModel_t2417974361 * get_fieldInfo_6() const { return ___fieldInfo_6; }
	inline WorldFieldModel_t2417974361 ** get_address_of_fieldInfo_6() { return &___fieldInfo_6; }
	inline void set_fieldInfo_6(WorldFieldModel_t2417974361 * value)
	{
		___fieldInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDFIELDMANAGER_T2312070818_H
#ifndef ZOOMMANAGER_T89017121_H
#define ZOOMMANAGER_T89017121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomManager
struct  ZoomManager_t89017121  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ZoomManager::mainCamera
	Camera_t4157153871 * ___mainCamera_2;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(ZoomManager_t89017121, ___mainCamera_2)); }
	inline Camera_t4157153871 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Camera_t4157153871 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMMANAGER_T89017121_H
#ifndef WORLDCHUNKMANAGER_T1513314516_H
#define WORLDCHUNKMANAGER_T1513314516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChunkManager
struct  WorldChunkManager_t1513314516  : public MonoBehaviour_t3962482529
{
public:
	// System.Int64 WorldChunkManager::startX
	int64_t ___startX_2;
	// System.Int64 WorldChunkManager::startY
	int64_t ___startY_3;
	// WorldFieldModel[0...,0...] WorldChunkManager::chunkFields
	WorldFieldModelU5BU2CU5D_t237916709* ___chunkFields_4;
	// SimpleEvent WorldChunkManager::onGetChunkInfo
	SimpleEvent_t129249603 * ___onGetChunkInfo_5;

public:
	inline static int32_t get_offset_of_startX_2() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___startX_2)); }
	inline int64_t get_startX_2() const { return ___startX_2; }
	inline int64_t* get_address_of_startX_2() { return &___startX_2; }
	inline void set_startX_2(int64_t value)
	{
		___startX_2 = value;
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___startY_3)); }
	inline int64_t get_startY_3() const { return ___startY_3; }
	inline int64_t* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(int64_t value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_chunkFields_4() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___chunkFields_4)); }
	inline WorldFieldModelU5BU2CU5D_t237916709* get_chunkFields_4() const { return ___chunkFields_4; }
	inline WorldFieldModelU5BU2CU5D_t237916709** get_address_of_chunkFields_4() { return &___chunkFields_4; }
	inline void set_chunkFields_4(WorldFieldModelU5BU2CU5D_t237916709* value)
	{
		___chunkFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___chunkFields_4), value);
	}

	inline static int32_t get_offset_of_onGetChunkInfo_5() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1513314516, ___onGetChunkInfo_5)); }
	inline SimpleEvent_t129249603 * get_onGetChunkInfo_5() const { return ___onGetChunkInfo_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetChunkInfo_5() { return &___onGetChunkInfo_5; }
	inline void set_onGetChunkInfo_5(SimpleEvent_t129249603 * value)
	{
		___onGetChunkInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___onGetChunkInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCHUNKMANAGER_T1513314516_H
#ifndef BOXCOLLIDER2D_T3581341831_H
#define BOXCOLLIDER2D_T3581341831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t3581341831  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_T3581341831_H
#ifndef TESTWORLDCHUNKSMANAGER_T1200850834_H
#define TESTWORLDCHUNKSMANAGER_T1200850834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestWorldChunksManager
struct  TestWorldChunksManager_t1200850834  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[0...,0...] TestWorldChunksManager::worldChunks
	GameObjectU5B0___U2C0___U5D_t3328599147* ___worldChunks_2;
	// System.Int64 TestWorldChunksManager::targetX
	int64_t ___targetX_3;
	// System.Int64 TestWorldChunksManager::targetY
	int64_t ___targetY_4;

public:
	inline static int32_t get_offset_of_worldChunks_2() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___worldChunks_2)); }
	inline GameObjectU5B0___U2C0___U5D_t3328599147* get_worldChunks_2() const { return ___worldChunks_2; }
	inline GameObjectU5B0___U2C0___U5D_t3328599147** get_address_of_worldChunks_2() { return &___worldChunks_2; }
	inline void set_worldChunks_2(GameObjectU5B0___U2C0___U5D_t3328599147* value)
	{
		___worldChunks_2 = value;
		Il2CppCodeGenWriteBarrier((&___worldChunks_2), value);
	}

	inline static int32_t get_offset_of_targetX_3() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___targetX_3)); }
	inline int64_t get_targetX_3() const { return ___targetX_3; }
	inline int64_t* get_address_of_targetX_3() { return &___targetX_3; }
	inline void set_targetX_3(int64_t value)
	{
		___targetX_3 = value;
	}

	inline static int32_t get_offset_of_targetY_4() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___targetY_4)); }
	inline int64_t get_targetY_4() const { return ___targetY_4; }
	inline int64_t* get_address_of_targetY_4() { return &___targetY_4; }
	inline void set_targetY_4(int64_t value)
	{
		___targetY_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTWORLDCHUNKSMANAGER_T1200850834_H
#ifndef GLOBALGOSCRIPT_T724808191_H
#define GLOBALGOSCRIPT_T724808191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalGOScript
struct  GlobalGOScript_t724808191  : public MonoBehaviour_t3962482529
{
public:
	// PlayerManager GlobalGOScript::playerManager
	PlayerManager_t1349889689 * ___playerManager_3;
	// GameManager GlobalGOScript::gameManager
	GameManager_t1536523654 * ___gameManager_4;
	// GCMManager GlobalGOScript::messagesManager
	GCMManager_t2058212271 * ___messagesManager_5;
	// System.String GlobalGOScript::host
	String_t* ___host_6;
	// System.String GlobalGOScript::token
	String_t* ___token_7;
	// WindowInstanceManager GlobalGOScript::windowInstanceManager
	WindowInstanceManager_t3234774687 * ___windowInstanceManager_8;
	// CityChangerContentManager GlobalGOScript::cityChanger
	CityChangerContentManager_t1075607021 * ___cityChanger_9;
	// PanelKnightsTableController GlobalGOScript::knightsTable
	PanelKnightsTableController_t3023536378 * ___knightsTable_10;
	// UnityEngine.GameObject GlobalGOScript::newMailNotification
	GameObject_t1113636619 * ___newMailNotification_11;
	// UnityEngine.GameObject GlobalGOScript::underAttackNotification
	GameObject_t1113636619 * ___underAttackNotification_12;
	// UnityEngine.GameObject GlobalGOScript::adBonusNotification
	GameObject_t1113636619 * ___adBonusNotification_13;

public:
	inline static int32_t get_offset_of_playerManager_3() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___playerManager_3)); }
	inline PlayerManager_t1349889689 * get_playerManager_3() const { return ___playerManager_3; }
	inline PlayerManager_t1349889689 ** get_address_of_playerManager_3() { return &___playerManager_3; }
	inline void set_playerManager_3(PlayerManager_t1349889689 * value)
	{
		___playerManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerManager_3), value);
	}

	inline static int32_t get_offset_of_gameManager_4() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___gameManager_4)); }
	inline GameManager_t1536523654 * get_gameManager_4() const { return ___gameManager_4; }
	inline GameManager_t1536523654 ** get_address_of_gameManager_4() { return &___gameManager_4; }
	inline void set_gameManager_4(GameManager_t1536523654 * value)
	{
		___gameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_4), value);
	}

	inline static int32_t get_offset_of_messagesManager_5() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___messagesManager_5)); }
	inline GCMManager_t2058212271 * get_messagesManager_5() const { return ___messagesManager_5; }
	inline GCMManager_t2058212271 ** get_address_of_messagesManager_5() { return &___messagesManager_5; }
	inline void set_messagesManager_5(GCMManager_t2058212271 * value)
	{
		___messagesManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___messagesManager_5), value);
	}

	inline static int32_t get_offset_of_host_6() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___host_6)); }
	inline String_t* get_host_6() const { return ___host_6; }
	inline String_t** get_address_of_host_6() { return &___host_6; }
	inline void set_host_6(String_t* value)
	{
		___host_6 = value;
		Il2CppCodeGenWriteBarrier((&___host_6), value);
	}

	inline static int32_t get_offset_of_token_7() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___token_7)); }
	inline String_t* get_token_7() const { return ___token_7; }
	inline String_t** get_address_of_token_7() { return &___token_7; }
	inline void set_token_7(String_t* value)
	{
		___token_7 = value;
		Il2CppCodeGenWriteBarrier((&___token_7), value);
	}

	inline static int32_t get_offset_of_windowInstanceManager_8() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___windowInstanceManager_8)); }
	inline WindowInstanceManager_t3234774687 * get_windowInstanceManager_8() const { return ___windowInstanceManager_8; }
	inline WindowInstanceManager_t3234774687 ** get_address_of_windowInstanceManager_8() { return &___windowInstanceManager_8; }
	inline void set_windowInstanceManager_8(WindowInstanceManager_t3234774687 * value)
	{
		___windowInstanceManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___windowInstanceManager_8), value);
	}

	inline static int32_t get_offset_of_cityChanger_9() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___cityChanger_9)); }
	inline CityChangerContentManager_t1075607021 * get_cityChanger_9() const { return ___cityChanger_9; }
	inline CityChangerContentManager_t1075607021 ** get_address_of_cityChanger_9() { return &___cityChanger_9; }
	inline void set_cityChanger_9(CityChangerContentManager_t1075607021 * value)
	{
		___cityChanger_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityChanger_9), value);
	}

	inline static int32_t get_offset_of_knightsTable_10() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___knightsTable_10)); }
	inline PanelKnightsTableController_t3023536378 * get_knightsTable_10() const { return ___knightsTable_10; }
	inline PanelKnightsTableController_t3023536378 ** get_address_of_knightsTable_10() { return &___knightsTable_10; }
	inline void set_knightsTable_10(PanelKnightsTableController_t3023536378 * value)
	{
		___knightsTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___knightsTable_10), value);
	}

	inline static int32_t get_offset_of_newMailNotification_11() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___newMailNotification_11)); }
	inline GameObject_t1113636619 * get_newMailNotification_11() const { return ___newMailNotification_11; }
	inline GameObject_t1113636619 ** get_address_of_newMailNotification_11() { return &___newMailNotification_11; }
	inline void set_newMailNotification_11(GameObject_t1113636619 * value)
	{
		___newMailNotification_11 = value;
		Il2CppCodeGenWriteBarrier((&___newMailNotification_11), value);
	}

	inline static int32_t get_offset_of_underAttackNotification_12() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___underAttackNotification_12)); }
	inline GameObject_t1113636619 * get_underAttackNotification_12() const { return ___underAttackNotification_12; }
	inline GameObject_t1113636619 ** get_address_of_underAttackNotification_12() { return &___underAttackNotification_12; }
	inline void set_underAttackNotification_12(GameObject_t1113636619 * value)
	{
		___underAttackNotification_12 = value;
		Il2CppCodeGenWriteBarrier((&___underAttackNotification_12), value);
	}

	inline static int32_t get_offset_of_adBonusNotification_13() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___adBonusNotification_13)); }
	inline GameObject_t1113636619 * get_adBonusNotification_13() const { return ___adBonusNotification_13; }
	inline GameObject_t1113636619 ** get_address_of_adBonusNotification_13() { return &___adBonusNotification_13; }
	inline void set_adBonusNotification_13(GameObject_t1113636619 * value)
	{
		___adBonusNotification_13 = value;
		Il2CppCodeGenWriteBarrier((&___adBonusNotification_13), value);
	}
};

struct GlobalGOScript_t724808191_StaticFields
{
public:
	// GlobalGOScript GlobalGOScript::instance
	GlobalGOScript_t724808191 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191_StaticFields, ___instance_2)); }
	inline GlobalGOScript_t724808191 * get_instance_2() const { return ___instance_2; }
	inline GlobalGOScript_t724808191 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GlobalGOScript_t724808191 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALGOSCRIPT_T724808191_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_2)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_5)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_8)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_11)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2475741330 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2475741330 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SCROLLBAR_T1494447233_H
#define SCROLLBAR_T1494447233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar
struct  Scrollbar_t1494447233  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_16;
	// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_17;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_18;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_19;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_20;
	// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_t149898510 * ___m_OnValueChanged_21;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t3704657025 * ___m_ContainerRect_22;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_t2156229523  ___m_Offset_23;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_24;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_t3829159415 * ___m_PointerDownRepeat_25;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_HandleRect_16)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_Direction_17() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Direction_17)); }
	inline int32_t get_m_Direction_17() const { return ___m_Direction_17; }
	inline int32_t* get_address_of_m_Direction_17() { return &___m_Direction_17; }
	inline void set_m_Direction_17(int32_t value)
	{
		___m_Direction_17 = value;
	}

	inline static int32_t get_offset_of_m_Value_18() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Value_18)); }
	inline float get_m_Value_18() const { return ___m_Value_18; }
	inline float* get_address_of_m_Value_18() { return &___m_Value_18; }
	inline void set_m_Value_18(float value)
	{
		___m_Value_18 = value;
	}

	inline static int32_t get_offset_of_m_Size_19() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Size_19)); }
	inline float get_m_Size_19() const { return ___m_Size_19; }
	inline float* get_address_of_m_Size_19() { return &___m_Size_19; }
	inline void set_m_Size_19(float value)
	{
		___m_Size_19 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_20() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_NumberOfSteps_20)); }
	inline int32_t get_m_NumberOfSteps_20() const { return ___m_NumberOfSteps_20; }
	inline int32_t* get_address_of_m_NumberOfSteps_20() { return &___m_NumberOfSteps_20; }
	inline void set_m_NumberOfSteps_20(int32_t value)
	{
		___m_NumberOfSteps_20 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_21() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_OnValueChanged_21)); }
	inline ScrollEvent_t149898510 * get_m_OnValueChanged_21() const { return ___m_OnValueChanged_21; }
	inline ScrollEvent_t149898510 ** get_address_of_m_OnValueChanged_21() { return &___m_OnValueChanged_21; }
	inline void set_m_OnValueChanged_21(ScrollEvent_t149898510 * value)
	{
		___m_OnValueChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_21), value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_22() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_ContainerRect_22)); }
	inline RectTransform_t3704657025 * get_m_ContainerRect_22() const { return ___m_ContainerRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_m_ContainerRect_22() { return &___m_ContainerRect_22; }
	inline void set_m_ContainerRect_22(RectTransform_t3704657025 * value)
	{
		___m_ContainerRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ContainerRect_22), value);
	}

	inline static int32_t get_offset_of_m_Offset_23() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Offset_23)); }
	inline Vector2_t2156229523  get_m_Offset_23() const { return ___m_Offset_23; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_23() { return &___m_Offset_23; }
	inline void set_m_Offset_23(Vector2_t2156229523  value)
	{
		___m_Offset_23 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_24() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_Tracker_24)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_24() const { return ___m_Tracker_24; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_24() { return &___m_Tracker_24; }
	inline void set_m_Tracker_24(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_24 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_25() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___m_PointerDownRepeat_25)); }
	inline Coroutine_t3829159415 * get_m_PointerDownRepeat_25() const { return ___m_PointerDownRepeat_25; }
	inline Coroutine_t3829159415 ** get_address_of_m_PointerDownRepeat_25() { return &___m_PointerDownRepeat_25; }
	inline void set_m_PointerDownRepeat_25(Coroutine_t3829159415 * value)
	{
		___m_PointerDownRepeat_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerDownRepeat_25), value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_26() { return static_cast<int32_t>(offsetof(Scrollbar_t1494447233, ___isPointerDownAndNotDragging_26)); }
	inline bool get_isPointerDownAndNotDragging_26() const { return ___isPointerDownAndNotDragging_26; }
	inline bool* get_address_of_isPointerDownAndNotDragging_26() { return &___isPointerDownAndNotDragging_26; }
	inline void set_isPointerDownAndNotDragging_26(bool value)
	{
		___isPointerDownAndNotDragging_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBAR_T1494447233_H
#ifndef INPUTFIELD_T3762917431_H
#define INPUTFIELD_T3762917431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3762917431  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_16;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t1901882714 * ___m_TextComponent_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_19;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_20;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_21;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_22;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_23;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_24;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_25;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_26;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_27;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t648412432 * ___m_OnEndEdit_28;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t467195904 * ___m_OnValueChanged_29;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t2355412304 * ___m_OnValidateInput_30;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_31;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_33;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_34;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_35;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_36;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_38;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_39;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_41;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3211863866 * ___m_InputTextCache_42;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_43;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_44;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_45;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_46;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_47;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_48;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_49;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_52;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_53;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_54;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_55;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_56;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_57;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_58;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_59;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_60;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_62;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_18() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_TextComponent_18)); }
	inline Text_t1901882714 * get_m_TextComponent_18() const { return ___m_TextComponent_18; }
	inline Text_t1901882714 ** get_address_of_m_TextComponent_18() { return &___m_TextComponent_18; }
	inline void set_m_TextComponent_18(Text_t1901882714 * value)
	{
		___m_TextComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_18), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_19() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Placeholder_19)); }
	inline Graphic_t1660335611 * get_m_Placeholder_19() const { return ___m_Placeholder_19; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_19() { return &___m_Placeholder_19; }
	inline void set_m_Placeholder_19(Graphic_t1660335611 * value)
	{
		___m_Placeholder_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_19), value);
	}

	inline static int32_t get_offset_of_m_ContentType_20() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ContentType_20)); }
	inline int32_t get_m_ContentType_20() const { return ___m_ContentType_20; }
	inline int32_t* get_address_of_m_ContentType_20() { return &___m_ContentType_20; }
	inline void set_m_ContentType_20(int32_t value)
	{
		___m_ContentType_20 = value;
	}

	inline static int32_t get_offset_of_m_InputType_21() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputType_21)); }
	inline int32_t get_m_InputType_21() const { return ___m_InputType_21; }
	inline int32_t* get_address_of_m_InputType_21() { return &___m_InputType_21; }
	inline void set_m_InputType_21(int32_t value)
	{
		___m_InputType_21 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_22() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AsteriskChar_22)); }
	inline Il2CppChar get_m_AsteriskChar_22() const { return ___m_AsteriskChar_22; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_22() { return &___m_AsteriskChar_22; }
	inline void set_m_AsteriskChar_22(Il2CppChar value)
	{
		___m_AsteriskChar_22 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_23() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_KeyboardType_23)); }
	inline int32_t get_m_KeyboardType_23() const { return ___m_KeyboardType_23; }
	inline int32_t* get_address_of_m_KeyboardType_23() { return &___m_KeyboardType_23; }
	inline void set_m_KeyboardType_23(int32_t value)
	{
		___m_KeyboardType_23 = value;
	}

	inline static int32_t get_offset_of_m_LineType_24() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_LineType_24)); }
	inline int32_t get_m_LineType_24() const { return ___m_LineType_24; }
	inline int32_t* get_address_of_m_LineType_24() { return &___m_LineType_24; }
	inline void set_m_LineType_24(int32_t value)
	{
		___m_LineType_24 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_25() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HideMobileInput_25)); }
	inline bool get_m_HideMobileInput_25() const { return ___m_HideMobileInput_25; }
	inline bool* get_address_of_m_HideMobileInput_25() { return &___m_HideMobileInput_25; }
	inline void set_m_HideMobileInput_25(bool value)
	{
		___m_HideMobileInput_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_26() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterValidation_26)); }
	inline int32_t get_m_CharacterValidation_26() const { return ___m_CharacterValidation_26; }
	inline int32_t* get_address_of_m_CharacterValidation_26() { return &___m_CharacterValidation_26; }
	inline void set_m_CharacterValidation_26(int32_t value)
	{
		___m_CharacterValidation_26 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_27() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterLimit_27)); }
	inline int32_t get_m_CharacterLimit_27() const { return ___m_CharacterLimit_27; }
	inline int32_t* get_address_of_m_CharacterLimit_27() { return &___m_CharacterLimit_27; }
	inline void set_m_CharacterLimit_27(int32_t value)
	{
		___m_CharacterLimit_27 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_28() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnEndEdit_28)); }
	inline SubmitEvent_t648412432 * get_m_OnEndEdit_28() const { return ___m_OnEndEdit_28; }
	inline SubmitEvent_t648412432 ** get_address_of_m_OnEndEdit_28() { return &___m_OnEndEdit_28; }
	inline void set_m_OnEndEdit_28(SubmitEvent_t648412432 * value)
	{
		___m_OnEndEdit_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_28), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_29() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValueChanged_29)); }
	inline OnChangeEvent_t467195904 * get_m_OnValueChanged_29() const { return ___m_OnValueChanged_29; }
	inline OnChangeEvent_t467195904 ** get_address_of_m_OnValueChanged_29() { return &___m_OnValueChanged_29; }
	inline void set_m_OnValueChanged_29(OnChangeEvent_t467195904 * value)
	{
		___m_OnValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_29), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_30() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValidateInput_30)); }
	inline OnValidateInput_t2355412304 * get_m_OnValidateInput_30() const { return ___m_OnValidateInput_30; }
	inline OnValidateInput_t2355412304 ** get_address_of_m_OnValidateInput_30() { return &___m_OnValidateInput_30; }
	inline void set_m_OnValidateInput_30(OnValidateInput_t2355412304 * value)
	{
		___m_OnValidateInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_30), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_31() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretColor_31)); }
	inline Color_t2555686324  get_m_CaretColor_31() const { return ___m_CaretColor_31; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_31() { return &___m_CaretColor_31; }
	inline void set_m_CaretColor_31(Color_t2555686324  value)
	{
		___m_CaretColor_31 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_32() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CustomCaretColor_32)); }
	inline bool get_m_CustomCaretColor_32() const { return ___m_CustomCaretColor_32; }
	inline bool* get_address_of_m_CustomCaretColor_32() { return &___m_CustomCaretColor_32; }
	inline void set_m_CustomCaretColor_32(bool value)
	{
		___m_CustomCaretColor_32 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_33() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_SelectionColor_33)); }
	inline Color_t2555686324  get_m_SelectionColor_33() const { return ___m_SelectionColor_33; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_33() { return &___m_SelectionColor_33; }
	inline void set_m_SelectionColor_33(Color_t2555686324  value)
	{
		___m_SelectionColor_33 = value;
	}

	inline static int32_t get_offset_of_m_Text_34() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Text_34)); }
	inline String_t* get_m_Text_34() const { return ___m_Text_34; }
	inline String_t** get_address_of_m_Text_34() { return &___m_Text_34; }
	inline void set_m_Text_34(String_t* value)
	{
		___m_Text_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_34), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_35() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretBlinkRate_35)); }
	inline float get_m_CaretBlinkRate_35() const { return ___m_CaretBlinkRate_35; }
	inline float* get_address_of_m_CaretBlinkRate_35() { return &___m_CaretBlinkRate_35; }
	inline void set_m_CaretBlinkRate_35(float value)
	{
		___m_CaretBlinkRate_35 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_36() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretWidth_36)); }
	inline int32_t get_m_CaretWidth_36() const { return ___m_CaretWidth_36; }
	inline int32_t* get_address_of_m_CaretWidth_36() { return &___m_CaretWidth_36; }
	inline void set_m_CaretWidth_36(int32_t value)
	{
		___m_CaretWidth_36 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_37() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ReadOnly_37)); }
	inline bool get_m_ReadOnly_37() const { return ___m_ReadOnly_37; }
	inline bool* get_address_of_m_ReadOnly_37() { return &___m_ReadOnly_37; }
	inline void set_m_ReadOnly_37(bool value)
	{
		___m_ReadOnly_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_38() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretPosition_38)); }
	inline int32_t get_m_CaretPosition_38() const { return ___m_CaretPosition_38; }
	inline int32_t* get_address_of_m_CaretPosition_38() { return &___m_CaretPosition_38; }
	inline void set_m_CaretPosition_38(int32_t value)
	{
		___m_CaretPosition_38 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_39() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretSelectPosition_39)); }
	inline int32_t get_m_CaretSelectPosition_39() const { return ___m_CaretSelectPosition_39; }
	inline int32_t* get_address_of_m_CaretSelectPosition_39() { return &___m_CaretSelectPosition_39; }
	inline void set_m_CaretSelectPosition_39(int32_t value)
	{
		___m_CaretSelectPosition_39 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_40() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___caretRectTrans_40)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_40() const { return ___caretRectTrans_40; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_40() { return &___caretRectTrans_40; }
	inline void set_caretRectTrans_40(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_40 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_40), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_41() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CursorVerts_41)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_41() const { return ___m_CursorVerts_41; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_41() { return &___m_CursorVerts_41; }
	inline void set_m_CursorVerts_41(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_41), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_42() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputTextCache_42)); }
	inline TextGenerator_t3211863866 * get_m_InputTextCache_42() const { return ___m_InputTextCache_42; }
	inline TextGenerator_t3211863866 ** get_address_of_m_InputTextCache_42() { return &___m_InputTextCache_42; }
	inline void set_m_InputTextCache_42(TextGenerator_t3211863866 * value)
	{
		___m_InputTextCache_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_42), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_43() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CachedInputRenderer_43)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_43() const { return ___m_CachedInputRenderer_43; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_43() { return &___m_CachedInputRenderer_43; }
	inline void set_m_CachedInputRenderer_43(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_43), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_44() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_PreventFontCallback_44)); }
	inline bool get_m_PreventFontCallback_44() const { return ___m_PreventFontCallback_44; }
	inline bool* get_address_of_m_PreventFontCallback_44() { return &___m_PreventFontCallback_44; }
	inline void set_m_PreventFontCallback_44(bool value)
	{
		___m_PreventFontCallback_44 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_45() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Mesh_45)); }
	inline Mesh_t3648964284 * get_m_Mesh_45() const { return ___m_Mesh_45; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_45() { return &___m_Mesh_45; }
	inline void set_m_Mesh_45(Mesh_t3648964284 * value)
	{
		___m_Mesh_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_45), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_46() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AllowInput_46)); }
	inline bool get_m_AllowInput_46() const { return ___m_AllowInput_46; }
	inline bool* get_address_of_m_AllowInput_46() { return &___m_AllowInput_46; }
	inline void set_m_AllowInput_46(bool value)
	{
		___m_AllowInput_46 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_47() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ShouldActivateNextUpdate_47)); }
	inline bool get_m_ShouldActivateNextUpdate_47() const { return ___m_ShouldActivateNextUpdate_47; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_47() { return &___m_ShouldActivateNextUpdate_47; }
	inline void set_m_ShouldActivateNextUpdate_47(bool value)
	{
		___m_ShouldActivateNextUpdate_47 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_48() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_UpdateDrag_48)); }
	inline bool get_m_UpdateDrag_48() const { return ___m_UpdateDrag_48; }
	inline bool* get_address_of_m_UpdateDrag_48() { return &___m_UpdateDrag_48; }
	inline void set_m_UpdateDrag_48(bool value)
	{
		___m_UpdateDrag_48 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_49() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragPositionOutOfBounds_49)); }
	inline bool get_m_DragPositionOutOfBounds_49() const { return ___m_DragPositionOutOfBounds_49; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_49() { return &___m_DragPositionOutOfBounds_49; }
	inline void set_m_DragPositionOutOfBounds_49(bool value)
	{
		___m_DragPositionOutOfBounds_49 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_52() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretVisible_52)); }
	inline bool get_m_CaretVisible_52() const { return ___m_CaretVisible_52; }
	inline bool* get_address_of_m_CaretVisible_52() { return &___m_CaretVisible_52; }
	inline void set_m_CaretVisible_52(bool value)
	{
		___m_CaretVisible_52 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_53() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkCoroutine_53)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_53() const { return ___m_BlinkCoroutine_53; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_53() { return &___m_BlinkCoroutine_53; }
	inline void set_m_BlinkCoroutine_53(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_53), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_54() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkStartTime_54)); }
	inline float get_m_BlinkStartTime_54() const { return ___m_BlinkStartTime_54; }
	inline float* get_address_of_m_BlinkStartTime_54() { return &___m_BlinkStartTime_54; }
	inline void set_m_BlinkStartTime_54(float value)
	{
		___m_BlinkStartTime_54 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_55() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawStart_55)); }
	inline int32_t get_m_DrawStart_55() const { return ___m_DrawStart_55; }
	inline int32_t* get_address_of_m_DrawStart_55() { return &___m_DrawStart_55; }
	inline void set_m_DrawStart_55(int32_t value)
	{
		___m_DrawStart_55 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_56() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawEnd_56)); }
	inline int32_t get_m_DrawEnd_56() const { return ___m_DrawEnd_56; }
	inline int32_t* get_address_of_m_DrawEnd_56() { return &___m_DrawEnd_56; }
	inline void set_m_DrawEnd_56(int32_t value)
	{
		___m_DrawEnd_56 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_57() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragCoroutine_57)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_57() const { return ___m_DragCoroutine_57; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_57() { return &___m_DragCoroutine_57; }
	inline void set_m_DragCoroutine_57(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_57), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_58() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OriginalText_58)); }
	inline String_t* get_m_OriginalText_58() const { return ___m_OriginalText_58; }
	inline String_t** get_address_of_m_OriginalText_58() { return &___m_OriginalText_58; }
	inline void set_m_OriginalText_58(String_t* value)
	{
		___m_OriginalText_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_58), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_59() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_WasCanceled_59)); }
	inline bool get_m_WasCanceled_59() const { return ___m_WasCanceled_59; }
	inline bool* get_address_of_m_WasCanceled_59() { return &___m_WasCanceled_59; }
	inline void set_m_WasCanceled_59(bool value)
	{
		___m_WasCanceled_59 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_60() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HasDoneFocusTransition_60)); }
	inline bool get_m_HasDoneFocusTransition_60() const { return ___m_HasDoneFocusTransition_60; }
	inline bool* get_address_of_m_HasDoneFocusTransition_60() { return &___m_HasDoneFocusTransition_60; }
	inline void set_m_HasDoneFocusTransition_60(bool value)
	{
		___m_HasDoneFocusTransition_60 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_62() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ProcessingEvent_62)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_62() const { return ___m_ProcessingEvent_62; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_62() { return &___m_ProcessingEvent_62; }
	inline void set_m_ProcessingEvent_62(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_62), value);
	}
};

struct InputField_t3762917431_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(InputField_t3762917431_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3762917431_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WorldFieldModel[,]
struct WorldFieldModelU5BU2CU5D_t237916709  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WorldFieldModel_t2417974361 * m_Items[1];

public:
	inline WorldFieldModel_t2417974361 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WorldFieldModel_t2417974361 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WorldFieldModel_t2417974361 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WorldFieldModel_t2417974361 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WorldFieldModel_t2417974361 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WorldFieldModel_t2417974361 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WorldFieldModel_t2417974361 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline WorldFieldModel_t2417974361 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, WorldFieldModel_t2417974361 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WorldFieldModel_t2417974361 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline WorldFieldModel_t2417974361 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, WorldFieldModel_t2417974361 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  RuntimeObject * JsonUtility_FromJson_TisRuntimeObject_m1405553448_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Contains(!0)
extern "C"  bool List_1_Contains_m679968318_gshared (List_1_t913674750 * __this, int64_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m1218653627_gshared (Dictionary_2_t4142745948 * __this, int64_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WorldChat::BeginChat()
extern "C"  RuntimeObject* WorldChat_BeginChat_m246759783 (WorldChat_t753147188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChat/<BeginChat>c__Iterator0::.ctor()
extern "C"  void U3CBeginChatU3Ec__Iterator0__ctor_m717570039 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m2006396688 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m615723318 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.InputField::get_text()
extern "C"  String_t* InputField_get_text_m3534748202 (InputField_t3762917431 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField::set_text(System.String)
extern "C"  void InputField_set_text_m1877260015 (InputField_t3762917431 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Uri::.ctor(System.String)
extern "C"  void Uri__ctor_m800430703 (Uri_t100236324 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket::.ctor(System.Uri)
extern "C"  void WebSocket__ctor_m2562950751 (WebSocket_t1645401340 * __this, Uri_t100236324 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocket::Connect()
extern "C"  RuntimeObject* WebSocket_Connect_m2154044507 (WebSocket_t1645401340 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket::RecvString()
extern "C"  String_t* WebSocket_RecvString_m2062547647 (WebSocket_t1645401340 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.String[],System.StringSplitOptions)
extern "C"  StringU5BU5D_t1281789340* String_Split_m4013853433 (String_t* __this, StringU5BU5D_t1281789340* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean TextLocalizer::HasArabicGlyphs(System.String)
extern "C"  bool TextLocalizer_HasArabicGlyphs_m3287876718 (RuntimeObject * __this /* static, unused */, String_t* ___text0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ArabicSupport.ArabicFixer::Fix(System.String)
extern "C"  String_t* ArabicFixer_Fix_m3682192890 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_value(System.Single)
extern "C"  void Scrollbar_set_value_m2277767288 (Scrollbar_t1494447233 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(JSONObject/Type)
extern "C"  JSONObject_t1339445639 * JSONObject_Create_m2948879229 (RuntimeObject * __this /* static, unused */, int32_t ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,System.String)
extern "C"  void JSONObject_AddField_m3499203595 (JSONObject_t1339445639 * __this, String_t* ___name0, String_t* ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::Print(System.Boolean)
extern "C"  String_t* JSONObject_Print_m4156363199 (JSONObject_t1339445639 * __this, bool ___pretty0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket::SendString(System.String)
extern "C"  void WebSocket_SendString_m2538589536 (WebSocket_t1645401340 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket::get_error()
extern "C"  String_t* WebSocket_get_error_m1149229605 (WebSocket_t1645401340 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket::Close()
extern "C"  void WebSocket_Close_m2991462339 (WebSocket_t1645401340 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager/<GetChunkInfo>c__Iterator0::.ctor()
extern "C"  void U3CGetChunkInfoU3Ec__Iterator0__ctor_m1396705849 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<WorldFieldManager>()
#define GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028(__this, method) ((  WorldFieldManager_t2312070818 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void WorldFieldManager::InitFieldState(WorldFieldModel)
extern "C"  void WorldFieldManager_InitFieldState_m4079297551 (WorldFieldManager_t2312070818 * __this, WorldFieldModel_t2417974361 * ___field0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void SimpleEvent__ctor_m1046870233 (SimpleEvent_t129249603 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::remove_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_remove_onGetChunkInfo_m3933643421 (WorldChunkManager_t1513314516 * __this, SimpleEvent_t129249603 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldFieldManager::DisableField()
extern "C"  void WorldFieldManager_DisableField_m3970299500 (WorldFieldManager_t2312070818 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChunkManager::add_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_add_onGetChunkInfo_m274685594 (WorldChunkManager_t1513314516 * __this, SimpleEvent_t129249603 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WorldChunkManager::GetChunkInfo()
extern "C"  RuntimeObject* WorldChunkManager_GetChunkInfo_m887525653 (WorldChunkManager_t1513314516 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::.ctor()
extern "C"  void WWWForm__ctor_m2465700452 (WWWForm_t4064702195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m2986581816 (int64_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern "C"  void WWWForm_AddField_m2357902982 (WWWForm_t4064702195 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern "C"  UnityWebRequest_t463507806 * UnityWebRequest_Post_m4193475377 (RuntimeObject * __this /* static, unused */, String_t* p0, WWWForm_t4064702195 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_SetRequestHeader_m2927335855 (UnityWebRequest_t463507806 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Networking.UnityWebRequest::Send()
extern "C"  AsyncOperation_t1445031843 * UnityWebRequest_Send_m637654012 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
extern "C"  bool UnityWebRequest_get_isNetworkError_m1231611882 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C"  String_t* UnityWebRequest_get_error_m1613086199 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t2937767557 * UnityWebRequest_get_downloadHandler_m534911913 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.DownloadHandler::get_text()
extern "C"  String_t* DownloadHandler_get_text_m2427232382 (DownloadHandler_t2937767557 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  JSONObject_t1339445639 * JSONObject_Create_m2744213201 (RuntimeObject * __this /* static, unused */, String_t* ___val0, int32_t ___maxDepth1, bool ___storeExcessLevels2, bool ___strict3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsArray()
extern "C"  bool JSONObject_get_IsArray_m3277574321 (JSONObject_t1339445639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32)
#define List_1_get_Item_m2755735191(__this, p0, method) ((  JSONObject_t1339445639 * (*) (List_1_t2811520381 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<WorldFieldModel>(System.String)
#define JsonUtility_FromJson_TisWorldFieldModel_t2417974361_m1764925164(__this /* static, unused */, p0, method) ((  WorldFieldModel_t2417974361 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m1405553448_gshared)(__this /* static, unused */, p0, method)
// JSONObject JSONObject::GetField(System.String)
extern "C"  JSONObject_t1339445639 * JSONObject_GetField_m2034261646 (JSONObject_t1339445639 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsNull()
extern "C"  bool JSONObject_get_IsNull_m3287769370 (JSONObject_t1339445639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.JsonUtility::FromJson<SimpleUserModel>(System.String)
#define JsonUtility_FromJson_TisSimpleUserModel_t455560495_m1828887640(__this /* static, unused */, p0, method) ((  SimpleUserModel_t455560495 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m1405553448_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<CityModel>(System.String)
#define JsonUtility_FromJson_TisCityModel_t1286289939_m1365451430(__this /* static, unused */, p0, method) ((  CityModel_t1286289939 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m1405553448_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count()
#define List_1_get_Count_m3161834082(__this, method) ((  int32_t (*) (List_1_t2811520381 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void SimpleEvent::Invoke(System.Object,System.String)
extern "C"  void SimpleEvent_Invoke_m17968349 (SimpleEvent_t129249603 * __this, RuntimeObject * ___sender0, String_t* ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(__this, method) ((  BoxCollider2D_t3581341831 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(__this, method) ((  MeshRenderer_t587009260 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m1727253150 (Renderer_t2627027031 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String,System.String)
extern "C"  bool String_Equals_m1744302937 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
#define Resources_Load_TisSprite_t280657092_m4144667290(__this /* static, unused */, p0, method) ((  Sprite_t280657092 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(__this, method) ((  SpriteRenderer_t3235626157 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m1286893786 (SpriteRenderer_t3235626157 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Contains(!0)
#define List_1_Contains_m679968318(__this, p0, method) ((  bool (*) (List_1_t913674750 *, int64_t, const RuntimeMethod*))List_1_Contains_m679968318_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t1536577757_m1070281259(__this, method) ((  TextMesh_t1536577757 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C"  void TextMesh_set_text_m446189179 (TextMesh_t1536577757 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
extern "C"  void Renderer_set_sortingLayerName_m3885968216 (Renderer_t2627027031 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C"  void Renderer_set_sortingOrder_m549573253 (Renderer_t2627027031 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
extern "C"  EventSystem_t1003666588 * EventSystem_get_current_m1416377559 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject(System.Int32)
extern "C"  bool EventSystem_IsPointerOverGameObject_m301566329 (EventSystem_t1003666588 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WindowInstanceManager::openCityInfo(WorldFieldModel)
extern "C"  void WindowInstanceManager_openCityInfo_m1936888155 (WindowInstanceManager_t3234774687 * __this, WorldFieldModel_t2417974361 * ___field0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m3662773728(__this, p0, method) ((  bool (*) (Dictionary_2_t3480614145 *, int64_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m1218653627_gshared)(__this, p0, method)
// System.Void WindowInstanceManager::openBuildingWindwow(System.Int64,System.String)
extern "C"  void WindowInstanceManager_openBuildingWindwow_m2020514623 (WindowInstanceManager_t3234774687 * __this, int64_t ___pitId0, String_t* ___type1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WindowInstanceManager::openValleyInfo(WorldFieldModel)
extern "C"  void WindowInstanceManager_openValleyInfo_m363073673 (WindowInstanceManager_t3234774687 * __this, WorldFieldModel_t2417974361 * ___field0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<TestWorldChunksManager>()
#define GameObject_GetComponent_TisTestWorldChunksManager_t1200850834_m598940813(__this, method) ((  TestWorldChunksManager_t1200850834 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Int64 System.Int64::Parse(System.String)
extern "C"  int64_t Int64_Parse_m662659148 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::GoToCoords(System.Int64,System.Int64)
extern "C"  void TestWorldChunksManager_GoToCoords_m3263514046 (TestWorldChunksManager_t1200850834 * __this, int64_t ___x0, int64_t ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldChat::.ctor()
extern "C"  void WorldChat__ctor_m1490938105 (WorldChat_t753147188 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldChat::Start()
extern "C"  void WorldChat_Start_m744815401 (WorldChat_t753147188 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = WorldChat_BeginChat_m246759783(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator WorldChat::BeginChat()
extern "C"  RuntimeObject* WorldChat_BeginChat_m246759783 (WorldChat_t753147188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChat_BeginChat_m246759783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CBeginChatU3Ec__Iterator0_t121862130 * V_0 = NULL;
	{
		U3CBeginChatU3Ec__Iterator0_t121862130 * L_0 = (U3CBeginChatU3Ec__Iterator0_t121862130 *)il2cpp_codegen_object_new(U3CBeginChatU3Ec__Iterator0_t121862130_il2cpp_TypeInfo_var);
		U3CBeginChatU3Ec__Iterator0__ctor_m717570039(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBeginChatU3Ec__Iterator0_t121862130 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CBeginChatU3Ec__Iterator0_t121862130 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WorldChat::OnApplicationPause(System.Boolean)
extern "C"  void WorldChat_OnApplicationPause_m1997041808 (WorldChat_t753147188 * __this, bool ___pauseStatus0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___pauseStatus0;
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		bool L_1 = __this->get_readyToChat_7();
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeInHierarchy_m2006396688(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_4 = WorldChat_BeginChat_m246759783(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_4, /*hidden argument*/NULL);
	}

IL_002e:
	{
		goto IL_0046;
	}

IL_0033:
	{
		__this->set_chatWorking_6((bool)0);
		RuntimeObject* L_5 = WorldChat_BeginChat_m246759783(__this, /*hidden argument*/NULL);
		MonoBehaviour_StopCoroutine_m615723318(__this, L_5, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void WorldChat::SendMessage()
extern "C"  void WorldChat_SendMessage_m305074220 (WorldChat_t753147188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChat_SendMessage_m305074220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_input_3();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3534748202(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0036;
		}
	}
	{
		InputField_t3762917431 * L_3 = __this->get_input_3();
		NullCheck(L_3);
		String_t* L_4 = InputField_get_text_m3534748202(L_3, /*hidden argument*/NULL);
		__this->set_MessageBuffer_5(L_4);
		InputField_t3762917431 * L_5 = __this->get_input_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_5);
		InputField_set_text_m1877260015(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldChat/<BeginChat>c__Iterator0::.ctor()
extern "C"  void U3CBeginChatU3Ec__Iterator0__ctor_m717570039 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WorldChat/<BeginChat>c__Iterator0::MoveNext()
extern "C"  bool U3CBeginChatU3Ec__Iterator0_MoveNext_m1159218783 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBeginChatU3Ec__Iterator0_MoveNext_m1159218783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1281789340* V_1 = NULL;
	String_t* V_2 = NULL;
	JSONObject_t1339445639 * V_3 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_007e;
			}
			case 2:
			{
				goto IL_0268;
			}
		}
	}
	{
		goto IL_028a;
	}

IL_0025:
	{
		GlobalGOScript_t724808191 * L_2 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_host_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3236910554, L_3, _stringLiteral44433460, /*hidden argument*/NULL);
		Uri_t100236324 * L_5 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_5, L_4, /*hidden argument*/NULL);
		WebSocket_t1645401340 * L_6 = (WebSocket_t1645401340 *)il2cpp_codegen_object_new(WebSocket_t1645401340_il2cpp_TypeInfo_var);
		WebSocket__ctor_m2562950751(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CwU3E__0_0(L_6);
		WorldChat_t753147188 * L_7 = __this->get_U24this_2();
		WebSocket_t1645401340 * L_8 = __this->get_U3CwU3E__0_0();
		NullCheck(L_8);
		RuntimeObject* L_9 = WebSocket_Connect_m2154044507(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Coroutine_t3829159415 * L_10 = MonoBehaviour_StartCoroutine_m3411253000(L_7, L_9, /*hidden argument*/NULL);
		__this->set_U24current_3(L_10);
		bool L_11 = __this->get_U24disposing_4();
		if (L_11)
		{
			goto IL_0079;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0079:
	{
		goto IL_028c;
	}

IL_007e:
	{
		WorldChat_t753147188 * L_12 = __this->get_U24this_2();
		NullCheck(L_12);
		Text_t1901882714 * L_13 = L_12->get_chatWindow_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_14);
		WorldChat_t753147188 * L_15 = __this->get_U24this_2();
		NullCheck(L_15);
		L_15->set_chatWorking_6((bool)1);
		goto IL_0268;
	}

IL_00a4:
	{
		WebSocket_t1645401340 * L_16 = __this->get_U3CwU3E__0_0();
		NullCheck(L_16);
		String_t* L_17 = WebSocket_RecvString_m2062547647(L_16, /*hidden argument*/NULL);
		__this->set_U3CreplyU3E__1_1(L_17);
		String_t* L_18 = __this->get_U3CreplyU3E__1_1();
		if (!L_18)
		{
			goto IL_01b0;
		}
	}
	{
		WorldChat_t753147188 * L_19 = __this->get_U24this_2();
		NullCheck(L_19);
		Text_t1901882714 * L_20 = L_19->get_chatWindow_2();
		NullCheck(L_20);
		float L_21 = VirtFuncInvoker0< float >::Invoke(79 /* System.Single UnityEngine.UI.Text::get_preferredHeight() */, L_20);
		if ((!(((float)L_21) > ((float)(1000.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		WorldChat_t753147188 * L_22 = __this->get_U24this_2();
		NullCheck(L_22);
		Text_t1901882714 * L_23 = L_22->get_chatWindow_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_24);
	}

IL_00ef:
	{
		String_t* L_25 = __this->get_U3CreplyU3E__1_1();
		StringU5BU5D_t1281789340* L_26 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral3451959283);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3451959283);
		NullCheck(L_25);
		StringU5BU5D_t1281789340* L_27 = String_Split_m4013853433(L_25, L_26, 0, /*hidden argument*/NULL);
		V_1 = L_27;
		StringU5BU5D_t1281789340* L_28 = V_1;
		NullCheck(L_28);
		int32_t L_29 = 1;
		String_t* L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		bool L_31 = TextLocalizer_HasArabicGlyphs_m3287876718(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0160;
		}
	}
	{
		StringU5BU5D_t1281789340* L_32 = V_1;
		NullCheck(L_32);
		int32_t L_33 = 1;
		String_t* L_34 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_2 = L_35;
		String_t* L_36 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2994480591, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		String_t* L_38 = V_2;
		String_t* L_39 = ArabicFixer_Fix_m3682192890(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		String_t* L_40 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2637164798, L_39, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		String_t* L_41 = V_2;
		String_t* L_42 = ArabicFixer_Fix_m3682192890(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		V_2 = L_42;
		StringU5BU5D_t1281789340* L_43 = V_1;
		NullCheck(L_43);
		int32_t L_44 = 0;
		String_t* L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		String_t* L_46 = V_2;
		String_t* L_47 = String_Concat_m3755062657(NULL /*static, unused*/, L_45, _stringLiteral3787497789, L_46, /*hidden argument*/NULL);
		__this->set_U3CreplyU3E__1_1(L_47);
	}

IL_0160:
	{
		String_t* L_48 = __this->get_U3CreplyU3E__1_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1653617040, L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		WorldChat_t753147188 * L_50 = __this->get_U24this_2();
		NullCheck(L_50);
		Text_t1901882714 * L_51 = L_50->get_chatWindow_2();
		Text_t1901882714 * L_52 = L_51;
		NullCheck(L_52);
		String_t* L_53 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_52);
		String_t* L_54 = __this->get_U3CreplyU3E__1_1();
		String_t* L_55 = String_Concat_m3755062657(NULL /*static, unused*/, L_53, L_54, _stringLiteral3452614566, /*hidden argument*/NULL);
		NullCheck(L_52);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_52, L_55);
		WorldChat_t753147188 * L_56 = __this->get_U24this_2();
		NullCheck(L_56);
		Scrollbar_t1494447233 * L_57 = L_56->get_Scroll_4();
		NullCheck(L_57);
		Scrollbar_set_value_m2277767288(L_57, (0.0f), /*hidden argument*/NULL);
	}

IL_01b0:
	{
		WorldChat_t753147188 * L_58 = __this->get_U24this_2();
		NullCheck(L_58);
		String_t* L_59 = L_58->get_MessageBuffer_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_0219;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1339445639_il2cpp_TypeInfo_var);
		JSONObject_t1339445639 * L_61 = JSONObject_Create_m2948879229(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_3 = L_61;
		JSONObject_t1339445639 * L_62 = V_3;
		GlobalGOScript_t724808191 * L_63 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_63);
		String_t* L_64 = L_63->get_token_7();
		NullCheck(L_62);
		JSONObject_AddField_m3499203595(L_62, _stringLiteral3434186627, L_64, /*hidden argument*/NULL);
		JSONObject_t1339445639 * L_65 = V_3;
		WorldChat_t753147188 * L_66 = __this->get_U24this_2();
		NullCheck(L_66);
		String_t* L_67 = L_66->get_MessageBuffer_5();
		NullCheck(L_65);
		JSONObject_AddField_m3499203595(L_65, _stringLiteral3253941996, L_67, /*hidden argument*/NULL);
		WebSocket_t1645401340 * L_68 = __this->get_U3CwU3E__0_0();
		JSONObject_t1339445639 * L_69 = V_3;
		NullCheck(L_69);
		String_t* L_70 = JSONObject_Print_m4156363199(L_69, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_68);
		WebSocket_SendString_m2538589536(L_68, L_70, /*hidden argument*/NULL);
		WorldChat_t753147188 * L_71 = __this->get_U24this_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_72 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_71);
		L_71->set_MessageBuffer_5(L_72);
	}

IL_0219:
	{
		WebSocket_t1645401340 * L_73 = __this->get_U3CwU3E__0_0();
		NullCheck(L_73);
		String_t* L_74 = WebSocket_get_error_m1149229605(L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0248;
		}
	}
	{
		WebSocket_t1645401340 * L_75 = __this->get_U3CwU3E__0_0();
		NullCheck(L_75);
		String_t* L_76 = WebSocket_get_error_m1149229605(L_75, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1503557889, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		goto IL_0278;
	}

IL_0248:
	{
		int32_t L_78 = 0;
		RuntimeObject * L_79 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_78);
		__this->set_U24current_3(L_79);
		bool L_80 = __this->get_U24disposing_4();
		if (L_80)
		{
			goto IL_0263;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_0263:
	{
		goto IL_028c;
	}

IL_0268:
	{
		WorldChat_t753147188 * L_81 = __this->get_U24this_2();
		NullCheck(L_81);
		bool L_82 = L_81->get_chatWorking_6();
		if (L_82)
		{
			goto IL_00a4;
		}
	}

IL_0278:
	{
		WebSocket_t1645401340 * L_83 = __this->get_U3CwU3E__0_0();
		NullCheck(L_83);
		WebSocket_Close_m2991462339(L_83, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_028a:
	{
		return (bool)0;
	}

IL_028c:
	{
		return (bool)1;
	}
}
// System.Object WorldChat/<BeginChat>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CBeginChatU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m590497720 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object WorldChat/<BeginChat>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CBeginChatU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m524147544 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void WorldChat/<BeginChat>c__Iterator0::Dispose()
extern "C"  void U3CBeginChatU3Ec__Iterator0_Dispose_m976471934 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void WorldChat/<BeginChat>c__Iterator0::Reset()
extern "C"  void U3CBeginChatU3Ec__Iterator0_Reset_m3958700887 (U3CBeginChatU3Ec__Iterator0_t121862130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CBeginChatU3Ec__Iterator0_Reset_m3958700887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldChunkManager::.ctor()
extern "C"  void WorldChunkManager__ctor_m568542477 (WorldChunkManager_t1513314516 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldChunkManager::add_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_add_onGetChunkInfo_m274685594 (WorldChunkManager_t1513314516 * __this, SimpleEvent_t129249603 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChunkManager_add_onGetChunkInfo_m274685594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SimpleEvent_t129249603 * V_0 = NULL;
	SimpleEvent_t129249603 * V_1 = NULL;
	{
		SimpleEvent_t129249603 * L_0 = __this->get_onGetChunkInfo_5();
		V_0 = L_0;
	}

IL_0007:
	{
		SimpleEvent_t129249603 * L_1 = V_0;
		V_1 = L_1;
		SimpleEvent_t129249603 ** L_2 = __this->get_address_of_onGetChunkInfo_5();
		SimpleEvent_t129249603 * L_3 = V_1;
		SimpleEvent_t129249603 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		SimpleEvent_t129249603 * L_6 = V_0;
		SimpleEvent_t129249603 * L_7 = InterlockedCompareExchangeImpl<SimpleEvent_t129249603 *>(L_2, ((SimpleEvent_t129249603 *)CastclassSealed((RuntimeObject*)L_5, SimpleEvent_t129249603_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		SimpleEvent_t129249603 * L_8 = V_0;
		SimpleEvent_t129249603 * L_9 = V_1;
		if ((!(((RuntimeObject*)(SimpleEvent_t129249603 *)L_8) == ((RuntimeObject*)(SimpleEvent_t129249603 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void WorldChunkManager::remove_onGetChunkInfo(SimpleEvent)
extern "C"  void WorldChunkManager_remove_onGetChunkInfo_m3933643421 (WorldChunkManager_t1513314516 * __this, SimpleEvent_t129249603 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChunkManager_remove_onGetChunkInfo_m3933643421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SimpleEvent_t129249603 * V_0 = NULL;
	SimpleEvent_t129249603 * V_1 = NULL;
	{
		SimpleEvent_t129249603 * L_0 = __this->get_onGetChunkInfo_5();
		V_0 = L_0;
	}

IL_0007:
	{
		SimpleEvent_t129249603 * L_1 = V_0;
		V_1 = L_1;
		SimpleEvent_t129249603 ** L_2 = __this->get_address_of_onGetChunkInfo_5();
		SimpleEvent_t129249603 * L_3 = V_1;
		SimpleEvent_t129249603 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		SimpleEvent_t129249603 * L_6 = V_0;
		SimpleEvent_t129249603 * L_7 = InterlockedCompareExchangeImpl<SimpleEvent_t129249603 *>(L_2, ((SimpleEvent_t129249603 *)CastclassSealed((RuntimeObject*)L_5, SimpleEvent_t129249603_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		SimpleEvent_t129249603 * L_8 = V_0;
		SimpleEvent_t129249603 * L_9 = V_1;
		if ((!(((RuntimeObject*)(SimpleEvent_t129249603 *)L_8) == ((RuntimeObject*)(SimpleEvent_t129249603 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Collections.IEnumerator WorldChunkManager::GetChunkInfo()
extern "C"  RuntimeObject* WorldChunkManager_GetChunkInfo_m887525653 (WorldChunkManager_t1513314516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChunkManager_GetChunkInfo_m887525653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetChunkInfoU3Ec__Iterator0_t549570326 * V_0 = NULL;
	{
		U3CGetChunkInfoU3Ec__Iterator0_t549570326 * L_0 = (U3CGetChunkInfoU3Ec__Iterator0_t549570326 *)il2cpp_codegen_object_new(U3CGetChunkInfoU3Ec__Iterator0_t549570326_il2cpp_TypeInfo_var);
		U3CGetChunkInfoU3Ec__Iterator0__ctor_m1396705849(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetChunkInfoU3Ec__Iterator0_t549570326 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CGetChunkInfoU3Ec__Iterator0_t549570326 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WorldChunkManager::InitChunkFields(System.Object,System.String)
extern "C"  void WorldChunkManager_InitChunkFields_m2411822551 (WorldChunkManager_t1513314516 * __this, RuntimeObject * ___sender0, String_t* ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChunkManager_InitChunkFields_m2411822551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1113636619 * V_1 = NULL;
	WorldFieldManager_t2312070818 * V_2 = NULL;
	{
		V_0 = 0;
		goto IL_0069;
	}

IL_0007:
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transform_t3600365921 * L_2 = Transform_GetChild_m1092972975(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		GameObject_t1113636619 * L_4 = V_1;
		NullCheck(L_4);
		WorldFieldManager_t2312070818 * L_5 = GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028(L_4, /*hidden argument*/GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028_RuntimeMethod_var);
		V_2 = L_5;
		WorldFieldModelU5BU2CU5D_t237916709* L_6 = __this->get_chunkFields_4();
		WorldFieldManager_t2312070818 * L_7 = V_2;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_internalPitId_2();
		WorldFieldManager_t2312070818 * L_9 = V_2;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_internalPitId_2();
		NullCheck((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_6);
		WorldFieldModel_t2417974361 * L_11 = ((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_6)->GetAt(((int32_t)((int32_t)L_8/(int32_t)((int32_t)10))), ((int32_t)((int32_t)L_10%(int32_t)((int32_t)10))));
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		WorldFieldManager_t2312070818 * L_12 = V_2;
		WorldFieldModelU5BU2CU5D_t237916709* L_13 = __this->get_chunkFields_4();
		WorldFieldManager_t2312070818 * L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_internalPitId_2();
		WorldFieldManager_t2312070818 * L_16 = V_2;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_internalPitId_2();
		NullCheck((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_13);
		WorldFieldModel_t2417974361 * L_18 = ((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_13)->GetAt(((int32_t)((int32_t)L_15/(int32_t)((int32_t)10))), ((int32_t)((int32_t)L_17%(int32_t)((int32_t)10))));
		NullCheck(L_12);
		WorldFieldManager_InitFieldState_m4079297551(L_12, L_18, /*hidden argument*/NULL);
	}

IL_0065:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0069:
	{
		int32_t L_20 = V_0;
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Transform_get_childCount_m3145433196(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0007;
		}
	}
	{
		intptr_t L_23 = (intptr_t)WorldChunkManager_InitChunkFields_m2411822551_RuntimeMethod_var;
		SimpleEvent_t129249603 * L_24 = (SimpleEvent_t129249603 *)il2cpp_codegen_object_new(SimpleEvent_t129249603_il2cpp_TypeInfo_var);
		SimpleEvent__ctor_m1046870233(L_24, __this, L_23, /*hidden argument*/NULL);
		WorldChunkManager_remove_onGetChunkInfo_m3933643421(__this, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldChunkManager::ResetFieldsState()
extern "C"  void WorldChunkManager_ResetFieldsState_m1490371685 (WorldChunkManager_t1513314516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldChunkManager_ResetFieldsState_m1490371685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0007:
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transform_t3600365921 * L_2 = Transform_GetChild_m1092972975(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		WorldFieldManager_t2312070818 * L_4 = GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028(L_3, /*hidden argument*/GameObject_GetComponent_TisWorldFieldManager_t2312070818_m554472028_RuntimeMethod_var);
		NullCheck(L_4);
		WorldFieldManager_DisableField_m3970299500(L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_6 = V_0;
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Transform_get_childCount_m3145433196(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0007;
		}
	}
	{
		intptr_t L_9 = (intptr_t)WorldChunkManager_InitChunkFields_m2411822551_RuntimeMethod_var;
		SimpleEvent_t129249603 * L_10 = (SimpleEvent_t129249603 *)il2cpp_codegen_object_new(SimpleEvent_t129249603_il2cpp_TypeInfo_var);
		SimpleEvent__ctor_m1046870233(L_10, __this, L_9, /*hidden argument*/NULL);
		WorldChunkManager_add_onGetChunkInfo_m274685594(__this, L_10, /*hidden argument*/NULL);
		RuntimeObject* L_11 = WorldChunkManager_GetChunkInfo_m887525653(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_11, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldChunkManager/<GetChunkInfo>c__Iterator0::.ctor()
extern "C"  void U3CGetChunkInfoU3Ec__Iterator0__ctor_m1396705849 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WorldChunkManager/<GetChunkInfo>c__Iterator0::MoveNext()
extern "C"  bool U3CGetChunkInfoU3Ec__Iterator0_MoveNext_m2859353126 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetChunkInfoU3Ec__Iterator0_MoveNext_m2859353126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	JSONObject_t1339445639 * V_3 = NULL;
	JSONObject_t1339445639 * V_4 = NULL;
	WorldFieldModel_t2417974361 * V_5 = NULL;
	int32_t V_6 = 0;
	JSONObject_t1339445639 * V_7 = NULL;
	JSONObject_t1339445639 * V_8 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0145;
			}
		}
	}
	{
		goto IL_02cf;
	}

IL_0021:
	{
		WWWForm_t4064702195 * L_2 = (WWWForm_t4064702195 *)il2cpp_codegen_object_new(WWWForm_t4064702195_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2465700452(L_2, /*hidden argument*/NULL);
		__this->set_U3CunitFormU3E__0_0(L_2);
		WWWForm_t4064702195 * L_3 = __this->get_U3CunitFormU3E__0_0();
		WorldChunkManager_t1513314516 * L_4 = __this->get_U24this_2();
		NullCheck(L_4);
		int64_t* L_5 = L_4->get_address_of_startX_2();
		String_t* L_6 = Int64_ToString_m2986581816(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m2357902982(L_3, _stringLiteral2703599964, L_6, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_7 = __this->get_U3CunitFormU3E__0_0();
		WorldChunkManager_t1513314516 * L_8 = __this->get_U24this_2();
		NullCheck(L_8);
		int64_t* L_9 = L_8->get_address_of_startY_3();
		String_t* L_10 = Int64_ToString_m2986581816(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		WWWForm_AddField_m2357902982(L_7, _stringLiteral2703665500, L_10, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_11 = __this->get_U3CunitFormU3E__0_0();
		WorldChunkManager_t1513314516 * L_12 = __this->get_U24this_2();
		NullCheck(L_12);
		int64_t L_13 = L_12->get_startX_2();
		V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_13, (int64_t)(((int64_t)((int64_t)((int32_t)9))))));
		String_t* L_14 = Int64_ToString_m2986581816((&V_1), /*hidden argument*/NULL);
		NullCheck(L_11);
		WWWForm_AddField_m2357902982(L_11, _stringLiteral2909097137, L_14, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_15 = __this->get_U3CunitFormU3E__0_0();
		WorldChunkManager_t1513314516 * L_16 = __this->get_U24this_2();
		NullCheck(L_16);
		int64_t L_17 = L_16->get_startY_3();
		V_2 = ((int64_t)il2cpp_codegen_add((int64_t)L_17, (int64_t)(((int64_t)((int64_t)((int32_t)9))))));
		String_t* L_18 = Int64_ToString_m2986581816((&V_2), /*hidden argument*/NULL);
		NullCheck(L_15);
		WWWForm_AddField_m2357902982(L_15, _stringLiteral570444977, L_18, /*hidden argument*/NULL);
		GlobalGOScript_t724808191 * L_19 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_host_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3099255805, L_20, _stringLiteral4135483156, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_22 = __this->get_U3CunitFormU3E__0_0();
		UnityWebRequest_t463507806 * L_23 = UnityWebRequest_Post_m4193475377(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		__this->set_U3CrequestU3E__0_1(L_23);
		UnityWebRequest_t463507806 * L_24 = __this->get_U3CrequestU3E__0_1();
		GlobalGOScript_t724808191 * L_25 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_25);
		String_t* L_26 = L_25->get_token_7();
		String_t* L_27 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2007429539, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		UnityWebRequest_SetRequestHeader_m2927335855(L_24, _stringLiteral1573338805, L_27, /*hidden argument*/NULL);
		UnityWebRequest_t463507806 * L_28 = __this->get_U3CrequestU3E__0_1();
		NullCheck(L_28);
		AsyncOperation_t1445031843 * L_29 = UnityWebRequest_Send_m637654012(L_28, /*hidden argument*/NULL);
		__this->set_U24current_3(L_29);
		bool L_30 = __this->get_U24disposing_4();
		if (L_30)
		{
			goto IL_0140;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0140:
	{
		goto IL_02d1;
	}

IL_0145:
	{
		UnityWebRequest_t463507806 * L_31 = __this->get_U3CrequestU3E__0_1();
		NullCheck(L_31);
		bool L_32 = UnityWebRequest_get_isNetworkError_m1231611882(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_016a;
		}
	}
	{
		UnityWebRequest_t463507806 * L_33 = __this->get_U3CrequestU3E__0_1();
		NullCheck(L_33);
		String_t* L_34 = UnityWebRequest_get_error_m1613086199(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		goto IL_02c8;
	}

IL_016a:
	{
		UnityWebRequest_t463507806 * L_35 = __this->get_U3CrequestU3E__0_1();
		NullCheck(L_35);
		DownloadHandler_t2937767557 * L_36 = UnityWebRequest_get_downloadHandler_m534911913(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		String_t* L_37 = DownloadHandler_get_text_m2427232382(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1339445639_il2cpp_TypeInfo_var);
		JSONObject_t1339445639 * L_38 = JSONObject_Create_m2744213201(NULL /*static, unused*/, L_37, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_3 = L_38;
		JSONObject_t1339445639 * L_39 = V_3;
		NullCheck(L_39);
		bool L_40 = JSONObject_get_IsArray_m3277574321(L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0194;
		}
	}
	{
		goto IL_02c8;
	}

IL_0194:
	{
		UnityWebRequest_t463507806 * L_41 = __this->get_U3CrequestU3E__0_1();
		NullCheck(L_41);
		DownloadHandler_t2937767557 * L_42 = UnityWebRequest_get_downloadHandler_m534911913(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		String_t* L_43 = DownloadHandler_get_text_m2427232382(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1339445639_il2cpp_TypeInfo_var);
		JSONObject_t1339445639 * L_44 = JSONObject_Create_m2744213201(NULL /*static, unused*/, L_43, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		V_4 = L_44;
		WorldChunkManager_t1513314516 * L_45 = __this->get_U24this_2();
		il2cpp_array_size_t L_47[] = { (il2cpp_array_size_t)((int32_t)10), (il2cpp_array_size_t)((int32_t)10) };
		WorldFieldModelU5BU2CU5D_t237916709* L_46 = (WorldFieldModelU5BU2CU5D_t237916709*)GenArrayNew(WorldFieldModelU5BU2CU5D_t237916709_il2cpp_TypeInfo_var, L_47);
		NullCheck(L_45);
		L_45->set_chunkFields_4((WorldFieldModelU5BU2CU5D_t237916709*)L_46);
		V_6 = 0;
		goto IL_028a;
	}

IL_01cb:
	{
		JSONObject_t1339445639 * L_48 = V_4;
		NullCheck(L_48);
		List_1_t2811520381 * L_49 = L_48->get_list_7();
		int32_t L_50 = V_6;
		NullCheck(L_49);
		JSONObject_t1339445639 * L_51 = List_1_get_Item_m2755735191(L_49, L_50, /*hidden argument*/List_1_get_Item_m2755735191_RuntimeMethod_var);
		NullCheck(L_51);
		String_t* L_52 = JSONObject_Print_m4156363199(L_51, (bool)0, /*hidden argument*/NULL);
		WorldFieldModel_t2417974361 * L_53 = JsonUtility_FromJson_TisWorldFieldModel_t2417974361_m1764925164(NULL /*static, unused*/, L_52, /*hidden argument*/JsonUtility_FromJson_TisWorldFieldModel_t2417974361_m1764925164_RuntimeMethod_var);
		V_5 = L_53;
		JSONObject_t1339445639 * L_54 = V_4;
		NullCheck(L_54);
		List_1_t2811520381 * L_55 = L_54->get_list_7();
		int32_t L_56 = V_6;
		NullCheck(L_55);
		JSONObject_t1339445639 * L_57 = List_1_get_Item_m2755735191(L_55, L_56, /*hidden argument*/List_1_get_Item_m2755735191_RuntimeMethod_var);
		NullCheck(L_57);
		JSONObject_t1339445639 * L_58 = JSONObject_GetField_m2034261646(L_57, _stringLiteral1619347993, /*hidden argument*/NULL);
		V_7 = L_58;
		JSONObject_t1339445639 * L_59 = V_7;
		NullCheck(L_59);
		bool L_60 = JSONObject_get_IsNull_m3287769370(L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_0220;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_61 = V_5;
		JSONObject_t1339445639 * L_62 = V_7;
		NullCheck(L_62);
		String_t* L_63 = JSONObject_Print_m4156363199(L_62, (bool)0, /*hidden argument*/NULL);
		SimpleUserModel_t455560495 * L_64 = JsonUtility_FromJson_TisSimpleUserModel_t455560495_m1828887640(NULL /*static, unused*/, L_63, /*hidden argument*/JsonUtility_FromJson_TisSimpleUserModel_t455560495_m1828887640_RuntimeMethod_var);
		NullCheck(L_61);
		L_61->set_owner_5(L_64);
	}

IL_0220:
	{
		JSONObject_t1339445639 * L_65 = V_4;
		NullCheck(L_65);
		List_1_t2811520381 * L_66 = L_65->get_list_7();
		int32_t L_67 = V_6;
		NullCheck(L_66);
		JSONObject_t1339445639 * L_68 = List_1_get_Item_m2755735191(L_66, L_67, /*hidden argument*/List_1_get_Item_m2755735191_RuntimeMethod_var);
		NullCheck(L_68);
		JSONObject_t1339445639 * L_69 = JSONObject_GetField_m2034261646(L_68, _stringLiteral2330854147, /*hidden argument*/NULL);
		V_8 = L_69;
		JSONObject_t1339445639 * L_70 = V_8;
		NullCheck(L_70);
		bool L_71 = JSONObject_get_IsNull_m3287769370(L_70, /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_025a;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_72 = V_5;
		JSONObject_t1339445639 * L_73 = V_8;
		NullCheck(L_73);
		String_t* L_74 = JSONObject_Print_m4156363199(L_73, (bool)0, /*hidden argument*/NULL);
		CityModel_t1286289939 * L_75 = JsonUtility_FromJson_TisCityModel_t1286289939_m1365451430(NULL /*static, unused*/, L_74, /*hidden argument*/JsonUtility_FromJson_TisCityModel_t1286289939_m1365451430_RuntimeMethod_var);
		NullCheck(L_72);
		L_72->set_city_4(L_75);
	}

IL_025a:
	{
		WorldChunkManager_t1513314516 * L_76 = __this->get_U24this_2();
		NullCheck(L_76);
		WorldFieldModelU5BU2CU5D_t237916709* L_77 = L_76->get_chunkFields_4();
		WorldFieldModel_t2417974361 * L_78 = V_5;
		NullCheck(L_78);
		int64_t L_79 = L_78->get_posY_2();
		if ((int64_t)(((int64_t)((int64_t)L_79%(int64_t)(((int64_t)((int64_t)((int32_t)10))))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		WorldFieldModel_t2417974361 * L_80 = V_5;
		NullCheck(L_80);
		int64_t L_81 = L_80->get_posX_1();
		if ((int64_t)(((int64_t)((int64_t)L_81%(int64_t)(((int64_t)((int64_t)((int32_t)10))))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		WorldFieldModel_t2417974361 * L_82 = V_5;
		NullCheck((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_77);
		((WorldFieldModelU5BU2CU5D_t237916709*)(WorldFieldModelU5BU2CU5D_t237916709*)L_77)->SetAt((((intptr_t)((int64_t)((int64_t)L_79%(int64_t)(((int64_t)((int64_t)((int32_t)10)))))))), (((intptr_t)((int64_t)((int64_t)L_81%(int64_t)(((int64_t)((int64_t)((int32_t)10)))))))), L_82);
		int32_t L_83 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_83, (int32_t)1));
	}

IL_028a:
	{
		int32_t L_84 = V_6;
		JSONObject_t1339445639 * L_85 = V_4;
		NullCheck(L_85);
		List_1_t2811520381 * L_86 = L_85->get_list_7();
		NullCheck(L_86);
		int32_t L_87 = List_1_get_Count_m3161834082(L_86, /*hidden argument*/List_1_get_Count_m3161834082_RuntimeMethod_var);
		if ((((int32_t)L_84) < ((int32_t)L_87)))
		{
			goto IL_01cb;
		}
	}
	{
		WorldChunkManager_t1513314516 * L_88 = __this->get_U24this_2();
		NullCheck(L_88);
		SimpleEvent_t129249603 * L_89 = L_88->get_onGetChunkInfo_5();
		if (!L_89)
		{
			goto IL_02c8;
		}
	}
	{
		WorldChunkManager_t1513314516 * L_90 = __this->get_U24this_2();
		NullCheck(L_90);
		SimpleEvent_t129249603 * L_91 = L_90->get_onGetChunkInfo_5();
		WorldChunkManager_t1513314516 * L_92 = __this->get_U24this_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_91);
		SimpleEvent_Invoke_m17968349(L_91, L_92, L_93, /*hidden argument*/NULL);
	}

IL_02c8:
	{
		__this->set_U24PC_5((-1));
	}

IL_02cf:
	{
		return (bool)0;
	}

IL_02d1:
	{
		return (bool)1;
	}
}
// System.Object WorldChunkManager/<GetChunkInfo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CGetChunkInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2943033901 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object WorldChunkManager/<GetChunkInfo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CGetChunkInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2936453499 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void WorldChunkManager/<GetChunkInfo>c__Iterator0::Dispose()
extern "C"  void U3CGetChunkInfoU3Ec__Iterator0_Dispose_m469007820 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void WorldChunkManager/<GetChunkInfo>c__Iterator0::Reset()
extern "C"  void U3CGetChunkInfoU3Ec__Iterator0_Reset_m4067757616 (U3CGetChunkInfoU3Ec__Iterator0_t549570326 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetChunkInfoU3Ec__Iterator0_Reset_m4067757616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldFieldManager::.ctor()
extern "C"  void WorldFieldManager__ctor_m4017602290 (WorldFieldManager_t2312070818 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldFieldManager::DisableField()
extern "C"  void WorldFieldManager_DisableField_m3970299500 (WorldFieldManager_t2312070818 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldFieldManager_DisableField_m3970299500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t587009260 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BoxCollider2D_t3581341831 * L_1 = GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(L_0, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m20417929(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_flag_4();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_occupationIcon_5();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_flag_4();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3600365921 * L_6 = Transform_GetChild_m1092972975(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		MeshRenderer_t587009260 * L_7 = Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(L_6, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var);
		V_0 = L_7;
		MeshRenderer_t587009260 * L_8 = V_0;
		NullCheck(L_8);
		Renderer_set_enabled_m1727253150(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_fieldObject_3();
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldFieldManager::InitFieldState(WorldFieldModel)
extern "C"  void WorldFieldManager_InitFieldState_m4079297551 (WorldFieldManager_t2312070818 * __this, WorldFieldModel_t2417974361 * ___field0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldFieldManager_InitFieldState_m4079297551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Sprite_t280657092 * V_0 = NULL;
	Sprite_t280657092 * V_1 = NULL;
	Sprite_t280657092 * V_2 = NULL;
	Sprite_t280657092 * V_3 = NULL;
	Sprite_t280657092 * V_4 = NULL;
	Sprite_t280657092 * V_5 = NULL;
	Sprite_t280657092 * V_6 = NULL;
	MeshRenderer_t587009260 * V_7 = NULL;
	TextMesh_t1536577757 * V_8 = NULL;
	Sprite_t280657092 * V_9 = NULL;
	{
		WorldFieldModel_t2417974361 * L_0 = ___field0;
		__this->set_fieldInfo_6(L_0);
		WorldFieldModel_t2417974361 * L_1 = ___field0;
		NullCheck(L_1);
		String_t* L_2 = L_1->get_cityType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_Equals_m1744302937(NULL /*static, unused*/, L_2, _stringLiteral3236852389, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_006a;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_4 = ___field0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_cityType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral546498118, L_5, _stringLiteral3451500417, /*hidden argument*/NULL);
		Sprite_t280657092 * L_7 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, L_6, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_0 = L_7;
		GameObject_t1113636619 * L_8 = __this->get_fieldObject_3();
		NullCheck(L_8);
		SpriteRenderer_t3235626157 * L_9 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_8, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_10 = V_0;
		NullCheck(L_9);
		SpriteRenderer_set_sprite_m1286893786(L_9, L_10, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		BoxCollider2D_t3581341831 * L_12 = GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(L_11, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var);
		NullCheck(L_12);
		Behaviour_set_enabled_m20417929(L_12, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = __this->get_fieldObject_3();
		NullCheck(L_13);
		GameObject_SetActive_m796801857(L_13, (bool)1, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_006a:
	{
		GameObject_t1113636619 * L_14 = __this->get_fieldObject_3();
		NullCheck(L_14);
		SpriteRenderer_t3235626157 * L_15 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_14, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		NullCheck(L_15);
		SpriteRenderer_set_sprite_m1286893786(L_15, (Sprite_t280657092 *)NULL, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		BoxCollider2D_t3581341831 * L_17 = GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(L_16, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var);
		NullCheck(L_17);
		Behaviour_set_enabled_m20417929(L_17, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_18 = __this->get_fieldObject_3();
		NullCheck(L_18);
		GameObject_SetActive_m796801857(L_18, (bool)1, /*hidden argument*/NULL);
	}

IL_0098:
	{
		WorldFieldModel_t2417974361 * L_19 = ___field0;
		NullCheck(L_19);
		SimpleUserModel_t455560495 * L_20 = L_19->get_owner_5();
		NullCheck(L_20);
		int64_t L_21 = L_20->get_id_0();
		if ((((int64_t)L_21) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_030e;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_22 = ___field0;
		NullCheck(L_22);
		SimpleUserModel_t455560495 * L_23 = L_22->get_owner_5();
		NullCheck(L_23);
		int64_t L_24 = L_23->get_id_0();
		GlobalGOScript_t724808191 * L_25 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_25);
		PlayerManager_t1349889689 * L_26 = L_25->get_playerManager_3();
		NullCheck(L_26);
		UserModel_t1353931605 * L_27 = L_26->get_user_3();
		NullCheck(L_27);
		int64_t L_28 = L_27->get_id_0();
		if ((!(((uint64_t)L_24) == ((uint64_t)L_28))))
		{
			goto IL_00fb;
		}
	}
	{
		Sprite_t280657092 * L_29 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral2956737341, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_1 = L_29;
		GameObject_t1113636619 * L_30 = __this->get_flag_4();
		NullCheck(L_30);
		SpriteRenderer_t3235626157 * L_31 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_30, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_32 = V_1;
		NullCheck(L_31);
		SpriteRenderer_set_sprite_m1286893786(L_31, L_32, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_33 = __this->get_flag_4();
		NullCheck(L_33);
		GameObject_SetActive_m796801857(L_33, (bool)1, /*hidden argument*/NULL);
		goto IL_0294;
	}

IL_00fb:
	{
		GlobalGOScript_t724808191 * L_34 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_34);
		PlayerManager_t1349889689 * L_35 = L_34->get_playerManager_3();
		NullCheck(L_35);
		UserModel_t1353931605 * L_36 = L_35->get_user_3();
		NullCheck(L_36);
		AllianceModel_t2995969982 * L_37 = L_36->get_alliance_11();
		NullCheck(L_37);
		int64_t L_38 = L_37->get_id_0();
		if ((((int64_t)L_38) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_026a;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_39 = ___field0;
		NullCheck(L_39);
		SimpleUserModel_t455560495 * L_40 = L_39->get_owner_5();
		NullCheck(L_40);
		int64_t L_41 = L_40->get_allianceID_7();
		if ((((int64_t)L_41) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_026a;
		}
	}
	{
		GlobalGOScript_t724808191 * L_42 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_42);
		PlayerManager_t1349889689 * L_43 = L_42->get_playerManager_3();
		NullCheck(L_43);
		UserModel_t1353931605 * L_44 = L_43->get_user_3();
		NullCheck(L_44);
		AllianceModel_t2995969982 * L_45 = L_44->get_alliance_11();
		NullCheck(L_45);
		int64_t L_46 = L_45->get_id_0();
		WorldFieldModel_t2417974361 * L_47 = ___field0;
		NullCheck(L_47);
		SimpleUserModel_t455560495 * L_48 = L_47->get_owner_5();
		NullCheck(L_48);
		int64_t L_49 = L_48->get_allianceID_7();
		if ((!(((uint64_t)L_46) == ((uint64_t)L_49))))
		{
			goto IL_0183;
		}
	}
	{
		Sprite_t280657092 * L_50 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral4069353026, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_2 = L_50;
		GameObject_t1113636619 * L_51 = __this->get_flag_4();
		NullCheck(L_51);
		SpriteRenderer_t3235626157 * L_52 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_51, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_53 = V_2;
		NullCheck(L_52);
		SpriteRenderer_set_sprite_m1286893786(L_52, L_53, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_54 = __this->get_flag_4();
		NullCheck(L_54);
		GameObject_SetActive_m796801857(L_54, (bool)1, /*hidden argument*/NULL);
		goto IL_0265;
	}

IL_0183:
	{
		GlobalGOScript_t724808191 * L_55 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_55);
		PlayerManager_t1349889689 * L_56 = L_55->get_playerManager_3();
		NullCheck(L_56);
		UserModel_t1353931605 * L_57 = L_56->get_user_3();
		NullCheck(L_57);
		AllianceModel_t2995969982 * L_58 = L_57->get_alliance_11();
		NullCheck(L_58);
		List_1_t913674750 * L_59 = L_58->get_allies_6();
		WorldFieldModel_t2417974361 * L_60 = ___field0;
		NullCheck(L_60);
		SimpleUserModel_t455560495 * L_61 = L_60->get_owner_5();
		NullCheck(L_61);
		int64_t L_62 = L_61->get_allianceID_7();
		NullCheck(L_59);
		bool L_63 = List_1_Contains_m679968318(L_59, L_62, /*hidden argument*/List_1_Contains_m679968318_RuntimeMethod_var);
		if (!L_63)
		{
			goto IL_01de;
		}
	}
	{
		Sprite_t280657092 * L_64 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral4069353026, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_3 = L_64;
		GameObject_t1113636619 * L_65 = __this->get_flag_4();
		NullCheck(L_65);
		SpriteRenderer_t3235626157 * L_66 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_65, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_67 = V_3;
		NullCheck(L_66);
		SpriteRenderer_set_sprite_m1286893786(L_66, L_67, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_68 = __this->get_flag_4();
		NullCheck(L_68);
		GameObject_SetActive_m796801857(L_68, (bool)1, /*hidden argument*/NULL);
		goto IL_0265;
	}

IL_01de:
	{
		GlobalGOScript_t724808191 * L_69 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_69);
		PlayerManager_t1349889689 * L_70 = L_69->get_playerManager_3();
		NullCheck(L_70);
		UserModel_t1353931605 * L_71 = L_70->get_user_3();
		NullCheck(L_71);
		AllianceModel_t2995969982 * L_72 = L_71->get_alliance_11();
		NullCheck(L_72);
		List_1_t913674750 * L_73 = L_72->get_enemies_7();
		WorldFieldModel_t2417974361 * L_74 = ___field0;
		NullCheck(L_74);
		SimpleUserModel_t455560495 * L_75 = L_74->get_owner_5();
		NullCheck(L_75);
		int64_t L_76 = L_75->get_allianceID_7();
		NullCheck(L_73);
		bool L_77 = List_1_Contains_m679968318(L_73, L_76, /*hidden argument*/List_1_Contains_m679968318_RuntimeMethod_var);
		if (!L_77)
		{
			goto IL_023b;
		}
	}
	{
		Sprite_t280657092 * L_78 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral4223840193, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_4 = L_78;
		GameObject_t1113636619 * L_79 = __this->get_flag_4();
		NullCheck(L_79);
		SpriteRenderer_t3235626157 * L_80 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_79, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_81 = V_4;
		NullCheck(L_80);
		SpriteRenderer_set_sprite_m1286893786(L_80, L_81, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_82 = __this->get_flag_4();
		NullCheck(L_82);
		GameObject_SetActive_m796801857(L_82, (bool)1, /*hidden argument*/NULL);
		goto IL_0265;
	}

IL_023b:
	{
		Sprite_t280657092 * L_83 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral1496271294, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_5 = L_83;
		GameObject_t1113636619 * L_84 = __this->get_flag_4();
		NullCheck(L_84);
		SpriteRenderer_t3235626157 * L_85 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_84, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_86 = V_5;
		NullCheck(L_85);
		SpriteRenderer_set_sprite_m1286893786(L_85, L_86, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_87 = __this->get_flag_4();
		NullCheck(L_87);
		GameObject_SetActive_m796801857(L_87, (bool)1, /*hidden argument*/NULL);
	}

IL_0265:
	{
		goto IL_0294;
	}

IL_026a:
	{
		Sprite_t280657092 * L_88 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral1496271294, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_6 = L_88;
		GameObject_t1113636619 * L_89 = __this->get_flag_4();
		NullCheck(L_89);
		SpriteRenderer_t3235626157 * L_90 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_89, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_91 = V_6;
		NullCheck(L_90);
		SpriteRenderer_set_sprite_m1286893786(L_90, L_91, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_92 = __this->get_flag_4();
		NullCheck(L_92);
		GameObject_SetActive_m796801857(L_92, (bool)1, /*hidden argument*/NULL);
	}

IL_0294:
	{
		WorldFieldModel_t2417974361 * L_93 = __this->get_fieldInfo_6();
		NullCheck(L_93);
		CityModel_t1286289939 * L_94 = L_93->get_city_4();
		NullCheck(L_94);
		String_t* L_95 = L_94->get_occupantCityOwnerName_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_96 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_02ba;
		}
	}
	{
		GameObject_t1113636619 * L_97 = __this->get_occupationIcon_5();
		NullCheck(L_97);
		GameObject_SetActive_m796801857(L_97, (bool)1, /*hidden argument*/NULL);
	}

IL_02ba:
	{
		GameObject_t1113636619 * L_98 = __this->get_flag_4();
		NullCheck(L_98);
		Transform_t3600365921 * L_99 = GameObject_get_transform_m1369836730(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		Transform_t3600365921 * L_100 = Transform_GetChild_m1092972975(L_99, 0, /*hidden argument*/NULL);
		NullCheck(L_100);
		MeshRenderer_t587009260 * L_101 = Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(L_100, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var);
		V_7 = L_101;
		MeshRenderer_t587009260 * L_102 = V_7;
		NullCheck(L_102);
		TextMesh_t1536577757 * L_103 = Component_GetComponent_TisTextMesh_t1536577757_m1070281259(L_102, /*hidden argument*/Component_GetComponent_TisTextMesh_t1536577757_m1070281259_RuntimeMethod_var);
		V_8 = L_103;
		TextMesh_t1536577757 * L_104 = V_8;
		WorldFieldModel_t2417974361 * L_105 = ___field0;
		NullCheck(L_105);
		SimpleUserModel_t455560495 * L_106 = L_105->get_owner_5();
		NullCheck(L_106);
		String_t* L_107 = L_106->get_flag_10();
		NullCheck(L_104);
		TextMesh_set_text_m446189179(L_104, L_107, /*hidden argument*/NULL);
		MeshRenderer_t587009260 * L_108 = V_7;
		NullCheck(L_108);
		Renderer_set_sortingLayerName_m3885968216(L_108, _stringLiteral2154736121, /*hidden argument*/NULL);
		MeshRenderer_t587009260 * L_109 = V_7;
		NullCheck(L_109);
		Renderer_set_sortingOrder_m549573253(L_109, 1, /*hidden argument*/NULL);
		MeshRenderer_t587009260 * L_110 = V_7;
		NullCheck(L_110);
		Renderer_set_enabled_m1727253150(L_110, (bool)1, /*hidden argument*/NULL);
		goto IL_035e;
	}

IL_030e:
	{
		WorldFieldModel_t2417974361 * L_111 = ___field0;
		NullCheck(L_111);
		String_t* L_112 = L_111->get_cityType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_113 = String_Equals_m1744302937(NULL /*static, unused*/, L_112, _stringLiteral4107074515, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_0352;
		}
	}
	{
		Sprite_t280657092 * L_114 = Resources_Load_TisSprite_t280657092_m4144667290(NULL /*static, unused*/, _stringLiteral11425075, /*hidden argument*/Resources_Load_TisSprite_t280657092_m4144667290_RuntimeMethod_var);
		V_9 = L_114;
		GameObject_t1113636619 * L_115 = __this->get_flag_4();
		NullCheck(L_115);
		SpriteRenderer_t3235626157 * L_116 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_115, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		Sprite_t280657092 * L_117 = V_9;
		NullCheck(L_116);
		SpriteRenderer_set_sprite_m1286893786(L_116, L_117, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_118 = __this->get_flag_4();
		NullCheck(L_118);
		GameObject_SetActive_m796801857(L_118, (bool)1, /*hidden argument*/NULL);
		goto IL_035e;
	}

IL_0352:
	{
		GameObject_t1113636619 * L_119 = __this->get_flag_4();
		NullCheck(L_119);
		GameObject_SetActive_m796801857(L_119, (bool)0, /*hidden argument*/NULL);
	}

IL_035e:
	{
		return;
	}
}
// System.Void WorldFieldManager::OnMouseUpAsButton()
extern "C"  void WorldFieldManager_OnMouseUpAsButton_m3354412269 (WorldFieldManager_t2312070818 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldFieldManager_OnMouseUpAsButton_m3354412269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t1003666588_il2cpp_TypeInfo_var);
		EventSystem_t1003666588 * L_0 = EventSystem_get_current_m1416377559(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = EventSystem_IsPointerOverGameObject_m301566329(L_0, 0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00df;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_fieldObject_3();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeInHierarchy_m2006396688(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00df;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_4 = __this->get_fieldInfo_6();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_cityType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_Equals_m1744302937(NULL /*static, unused*/, L_5, _stringLiteral2330854179, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		GlobalGOScript_t724808191 * L_7 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_7);
		WindowInstanceManager_t3234774687 * L_8 = L_7->get_windowInstanceManager_8();
		WorldFieldModel_t2417974361 * L_9 = __this->get_fieldInfo_6();
		NullCheck(L_8);
		WindowInstanceManager_openCityInfo_m1936888155(L_8, L_9, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_0054:
	{
		GlobalGOScript_t724808191 * L_10 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_10);
		GameManager_t1536523654 * L_11 = L_10->get_gameManager_4();
		NullCheck(L_11);
		CityManager_t2587329200 * L_12 = L_11->get_cityManager_0();
		NullCheck(L_12);
		BuildingsManager_t3721263023 * L_13 = L_12->get_buildingsManager_2();
		NullCheck(L_13);
		Dictionary_2_t3480614145 * L_14 = L_13->get_cityValleys_2();
		WorldFieldModel_t2417974361 * L_15 = __this->get_fieldInfo_6();
		NullCheck(L_15);
		int64_t L_16 = L_15->get_id_0();
		NullCheck(L_14);
		bool L_17 = Dictionary_2_ContainsKey_m3662773728(L_14, L_16, /*hidden argument*/Dictionary_2_ContainsKey_m3662773728_RuntimeMethod_var);
		if (!L_17)
		{
			goto IL_00ca;
		}
	}
	{
		WorldFieldModel_t2417974361 * L_18 = __this->get_fieldInfo_6();
		NullCheck(L_18);
		String_t* L_19 = L_18->get_cityType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_Equals_m1744302937(NULL /*static, unused*/, L_19, _stringLiteral3236852389, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ca;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2277606826, /*hidden argument*/NULL);
		GlobalGOScript_t724808191 * L_21 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_21);
		WindowInstanceManager_t3234774687 * L_22 = L_21->get_windowInstanceManager_8();
		WorldFieldModel_t2417974361 * L_23 = __this->get_fieldInfo_6();
		NullCheck(L_23);
		int64_t L_24 = L_23->get_id_0();
		NullCheck(L_22);
		WindowInstanceManager_openBuildingWindwow_m2020514623(L_22, L_24, _stringLiteral2788218763, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00ca:
	{
		GlobalGOScript_t724808191 * L_25 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_25);
		WindowInstanceManager_t3234774687 * L_26 = L_25->get_windowInstanceManager_8();
		WorldFieldModel_t2417974361 * L_27 = __this->get_fieldInfo_6();
		NullCheck(L_26);
		WindowInstanceManager_openValleyInfo_m363073673(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldFieldModel::.ctor()
extern "C"  void WorldFieldModel__ctor_m3076504495 (WorldFieldModel_t2417974361 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldMapCoordsChanger::.ctor()
extern "C"  void WorldMapCoordsChanger__ctor_m2775315301 (WorldMapCoordsChanger_t692571169 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldMapCoordsChanger::OnEnable()
extern "C"  void WorldMapCoordsChanger_OnEnable_m4271087035 (WorldMapCoordsChanger_t692571169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldMapCoordsChanger_OnEnable_m4271087035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1581809604, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		TestWorldChunksManager_t1200850834 * L_4 = GameObject_GetComponent_TisTestWorldChunksManager_t1200850834_m598940813(L_3, /*hidden argument*/GameObject_GetComponent_TisTestWorldChunksManager_t1200850834_m598940813_RuntimeMethod_var);
		__this->set_chunksManager_4(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Void WorldMapCoordsChanger::GoToCoords()
extern "C"  void WorldMapCoordsChanger_GoToCoords_m4009681775 (WorldMapCoordsChanger_t692571169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldMapCoordsChanger_GoToCoords_m4009681775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_xInput_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3534748202(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0055;
		}
	}
	{
		InputField_t3762917431 * L_3 = __this->get_yInput_3();
		NullCheck(L_3);
		String_t* L_4 = InputField_get_text_m3534748202(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0055;
		}
	}
	{
		TestWorldChunksManager_t1200850834 * L_6 = __this->get_chunksManager_4();
		InputField_t3762917431 * L_7 = __this->get_xInput_2();
		NullCheck(L_7);
		String_t* L_8 = InputField_get_text_m3534748202(L_7, /*hidden argument*/NULL);
		int64_t L_9 = Int64_Parse_m662659148(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		InputField_t3762917431 * L_10 = __this->get_yInput_3();
		NullCheck(L_10);
		String_t* L_11 = InputField_get_text_m3534748202(L_10, /*hidden argument*/NULL);
		int64_t L_12 = Int64_Parse_m662659148(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		TestWorldChunksManager_GoToCoords_m3263514046(L_6, L_9, L_12, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void WorldMapCoordsChanger::GoHome()
extern "C"  void WorldMapCoordsChanger_GoHome_m3771174939 (WorldMapCoordsChanger_t692571169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldMapCoordsChanger_GoHome_m3771174939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	{
		GlobalGOScript_t724808191 * L_0 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = L_0->get_gameManager_4();
		NullCheck(L_1);
		CityManager_t2587329200 * L_2 = L_1->get_cityManager_0();
		NullCheck(L_2);
		MyCityModel_t3961736920 * L_3 = L_2->get_currentCity_10();
		NullCheck(L_3);
		int64_t L_4 = L_3->get_posX_2();
		V_0 = L_4;
		GlobalGOScript_t724808191 * L_5 = ((GlobalGOScript_t724808191_StaticFields*)il2cpp_codegen_static_fields_for(GlobalGOScript_t724808191_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_5);
		GameManager_t1536523654 * L_6 = L_5->get_gameManager_4();
		NullCheck(L_6);
		CityManager_t2587329200 * L_7 = L_6->get_cityManager_0();
		NullCheck(L_7);
		MyCityModel_t3961736920 * L_8 = L_7->get_currentCity_10();
		NullCheck(L_8);
		int64_t L_9 = L_8->get_posY_3();
		V_1 = L_9;
		TestWorldChunksManager_t1200850834 * L_10 = __this->get_chunksManager_4();
		int64_t L_11 = V_0;
		int64_t L_12 = V_1;
		NullCheck(L_10);
		TestWorldChunksManager_GoToCoords_m3263514046(L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldMapTimer::.ctor()
extern "C"  void WorldMapTimer__ctor_m3588005802 (WorldMapTimer_t60483741 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomManager::.ctor()
extern "C"  void ZoomManager__ctor_m878509778 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomManager::Start()
extern "C"  void ZoomManager_Start_m2532716488 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mainCamera_2(L_0);
		return;
	}
}
// System.Void ZoomManager::ZoomIn()
extern "C"  void ZoomManager_ZoomIn_m968364307 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		NullCheck(L_0);
		float L_1 = Camera_get_orthographicSize_m3903216845(L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_mainCamera_2();
		Camera_t4157153871 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = Camera_get_orthographicSize_m3903216845(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_set_orthographicSize_m76971700(L_3, ((float)il2cpp_codegen_subtract((float)L_4, (float)(0.5f))), /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void ZoomManager::ZoomOut()
extern "C"  void ZoomManager_ZoomOut_m2850791881 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		NullCheck(L_0);
		float L_1 = Camera_get_orthographicSize_m3903216845(L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) < ((float)(5.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_mainCamera_2();
		Camera_t4157153871 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = Camera_get_orthographicSize_m3903216845(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_set_orthographicSize_m76971700(L_3, ((float)il2cpp_codegen_add((float)L_4, (float)(0.5f))), /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void ZoomManager::MaxZoom()
extern "C"  void ZoomManager_MaxZoom_m2181835660 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		NullCheck(L_0);
		Camera_set_orthographicSize_m76971700(L_0, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomManager::MinZoom()
extern "C"  void ZoomManager_MinZoom_m4010222004 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		NullCheck(L_0);
		Camera_set_orthographicSize_m76971700(L_0, (5.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomManager::MiddleZoom()
extern "C"  void ZoomManager_MiddleZoom_m4078003941 (ZoomManager_t89017121 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		NullCheck(L_0);
		Camera_set_orthographicSize_m76971700(L_0, (2.5f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
