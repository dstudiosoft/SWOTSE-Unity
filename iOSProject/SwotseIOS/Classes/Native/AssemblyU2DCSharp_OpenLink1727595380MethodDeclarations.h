﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenLink
struct OpenLink_t1727595380;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenLink::.ctor()
extern "C"  void OpenLink__ctor_m1505909591 (OpenLink_t1727595380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenLink::openLink()
extern "C"  void OpenLink_openLink_m3420461347 (OpenLink_t1727595380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
