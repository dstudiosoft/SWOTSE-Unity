﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.GridTableView
struct GridTableView_t4197980433;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.GridTableView::.ctor()
extern "C"  void GridTableView__ctor_m2513895222 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::ReloadData()
extern "C"  void GridTableView_ReloadData_m367128449 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::NotifyCellDimensionsChanged(System.Int32)
extern "C"  void GridTableView_NotifyCellDimensionsChanged_m4248909849 (GridTableView_t4197980433 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::Awake()
extern "C"  void GridTableView_Awake_m3147572841 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::Update()
extern "C"  void GridTableView_Update_m2036952845 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::LateUpdate()
extern "C"  void GridTableView_LateUpdate_m660603369 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::OnEnable()
extern "C"  void GridTableView_OnEnable_m3882037078 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::OnDisable()
extern "C"  void GridTableView_OnDisable_m354450537 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::AddRow(System.Int32,System.Boolean)
extern "C"  void GridTableView_AddRow_m1675259021 (GridTableView_t4197980433 * __this, int32_t ___row0, bool ___atEnd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.GridTableView::UpdatePaddingElements()
extern "C"  void GridTableView_UpdatePaddingElements_m1462317265 (GridTableView_t4197980433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
