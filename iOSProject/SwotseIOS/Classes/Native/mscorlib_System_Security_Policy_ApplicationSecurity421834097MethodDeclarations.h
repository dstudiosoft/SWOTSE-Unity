﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.IApplicationTrustManager
struct IApplicationTrustManager_t2152210378;
// System.ActivationContext
struct ActivationContext_t1572332809;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t3342600394;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ActivationContext1572332809.h"
#include "mscorlib_System_Security_Policy_TrustManagerContex3342600394.h"

// System.Security.Policy.IApplicationTrustManager System.Security.Policy.ApplicationSecurityManager::get_ApplicationTrustManager()
extern "C"  Il2CppObject * ApplicationSecurityManager_get_ApplicationTrustManager_m1481863112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationSecurityManager::DetermineApplicationTrust(System.ActivationContext,System.Security.Policy.TrustManagerContext)
extern "C"  bool ApplicationSecurityManager_DetermineApplicationTrust_m108562836 (Il2CppObject * __this /* static, unused */, ActivationContext_t1572332809 * ___activationContext0, TrustManagerContext_t3342600394 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
