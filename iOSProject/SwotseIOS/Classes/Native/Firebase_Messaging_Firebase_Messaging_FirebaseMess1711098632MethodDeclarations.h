﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1711098632;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t899845187;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMessa899845187.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::.cctor()
extern "C"  void SWIGStringHelper__cctor_m349172075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1822474848 (SWIGStringHelper_t1711098632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_FirebaseMessaging_m1829604336 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t899845187 * ___stringDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m1549363940 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m1549363940(char* ___message0);
