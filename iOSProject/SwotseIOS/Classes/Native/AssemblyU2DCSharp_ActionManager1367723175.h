﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager
struct  ActionManager_t1367723175  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ActionManager::constructionPanel
	GameObject_t1756533147 * ___constructionPanel_2;
	// UnityEngine.GameObject ActionManager::marchesPanel
	GameObject_t1756533147 * ___marchesPanel_3;
	// UnityEngine.GameObject ActionManager::itemsPanel
	GameObject_t1756533147 * ___itemsPanel_4;
	// UnityEngine.GameObject ActionManager::unitsPanel
	GameObject_t1756533147 * ___unitsPanel_5;

public:
	inline static int32_t get_offset_of_constructionPanel_2() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___constructionPanel_2)); }
	inline GameObject_t1756533147 * get_constructionPanel_2() const { return ___constructionPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_constructionPanel_2() { return &___constructionPanel_2; }
	inline void set_constructionPanel_2(GameObject_t1756533147 * value)
	{
		___constructionPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___constructionPanel_2, value);
	}

	inline static int32_t get_offset_of_marchesPanel_3() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___marchesPanel_3)); }
	inline GameObject_t1756533147 * get_marchesPanel_3() const { return ___marchesPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_marchesPanel_3() { return &___marchesPanel_3; }
	inline void set_marchesPanel_3(GameObject_t1756533147 * value)
	{
		___marchesPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___marchesPanel_3, value);
	}

	inline static int32_t get_offset_of_itemsPanel_4() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___itemsPanel_4)); }
	inline GameObject_t1756533147 * get_itemsPanel_4() const { return ___itemsPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_itemsPanel_4() { return &___itemsPanel_4; }
	inline void set_itemsPanel_4(GameObject_t1756533147 * value)
	{
		___itemsPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemsPanel_4, value);
	}

	inline static int32_t get_offset_of_unitsPanel_5() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___unitsPanel_5)); }
	inline GameObject_t1756533147 * get_unitsPanel_5() const { return ___unitsPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_unitsPanel_5() { return &___unitsPanel_5; }
	inline void set_unitsPanel_5(GameObject_t1756533147 * value)
	{
		___unitsPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___unitsPanel_5, value);
	}
};

struct ActionManager_t1367723175_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ActionManager::<>f__switch$map3
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_6() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175_StaticFields, ___U3CU3Ef__switchU24map3_6)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3_6() const { return ___U3CU3Ef__switchU24map3_6; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3_6() { return &___U3CU3Ef__switchU24map3_6; }
	inline void set_U3CU3Ef__switchU24map3_6(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
