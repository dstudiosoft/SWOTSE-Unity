﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2000702335(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1040145024 *, Dictionary_2_t4015087618 *, const MethodInfo*))Enumerator__ctor_m3045959731_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1871966848(__this, method) ((  Il2CppObject * (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m524777314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2699522750(__this, method) ((  void (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3684036852_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m711188941(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2746776866(__this, method) ((  Il2CppObject * (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3840267344(__this, method) ((  Il2CppObject * (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::MoveNext()
#define Enumerator_MoveNext_m671304230(__this, method) ((  bool (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_MoveNext_m3139459564_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::get_Current()
#define Enumerator_get_Current_m3893602582(__this, method) ((  KeyValuePair_2_t1772432840  (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_get_Current_m3988231868_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2759553883(__this, method) ((  int64_t (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_get_CurrentKey_m4167196999_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3891255075(__this, method) ((  UserModel_t3025569216 * (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_get_CurrentValue_m659195263_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::Reset()
#define Enumerator_Reset_m1625208341(__this, method) ((  void (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_Reset_m598725905_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::VerifyState()
#define Enumerator_VerifyState_m117355306(__this, method) ((  void (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_VerifyState_m3803534940_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2337122862(__this, method) ((  void (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_VerifyCurrent_m1011807396_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UserModel>::Dispose()
#define Enumerator_Dispose_m2990956195(__this, method) ((  void (*) (Enumerator_t1040145024 *, const MethodInfo*))Enumerator_Dispose_m3446908287_gshared)(__this, method)
