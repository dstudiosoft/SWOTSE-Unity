﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FinancialContentChanger
struct FinancialContentChanger_t2753898366;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialTableLine
struct  FinancialTableLine_t2609388451  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Image FinancialTableLine::productImage
	Image_t2042527209 * ___productImage_2;
	// UnityEngine.UI.Text FinancialTableLine::productName
	Text_t356221433 * ___productName_3;
	// UnityEngine.UI.Text FinancialTableLine::quantity
	Text_t356221433 * ___quantity_4;
	// UnityEngine.UI.Text FinancialTableLine::price
	Text_t356221433 * ___price_5;
	// UnityEngine.UI.Text FinancialTableLine::date
	Text_t356221433 * ___date_6;
	// UnityEngine.UI.Text FinancialTableLine::fromUser
	Text_t356221433 * ___fromUser_7;
	// UnityEngine.GameObject FinancialTableLine::cancelOfferBtn
	GameObject_t1756533147 * ___cancelOfferBtn_8;
	// FinancialContentChanger FinancialTableLine::owner
	FinancialContentChanger_t2753898366 * ___owner_9;
	// System.String FinancialTableLine::lotId
	String_t* ___lotId_10;

public:
	inline static int32_t get_offset_of_productImage_2() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___productImage_2)); }
	inline Image_t2042527209 * get_productImage_2() const { return ___productImage_2; }
	inline Image_t2042527209 ** get_address_of_productImage_2() { return &___productImage_2; }
	inline void set_productImage_2(Image_t2042527209 * value)
	{
		___productImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___productImage_2, value);
	}

	inline static int32_t get_offset_of_productName_3() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___productName_3)); }
	inline Text_t356221433 * get_productName_3() const { return ___productName_3; }
	inline Text_t356221433 ** get_address_of_productName_3() { return &___productName_3; }
	inline void set_productName_3(Text_t356221433 * value)
	{
		___productName_3 = value;
		Il2CppCodeGenWriteBarrier(&___productName_3, value);
	}

	inline static int32_t get_offset_of_quantity_4() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___quantity_4)); }
	inline Text_t356221433 * get_quantity_4() const { return ___quantity_4; }
	inline Text_t356221433 ** get_address_of_quantity_4() { return &___quantity_4; }
	inline void set_quantity_4(Text_t356221433 * value)
	{
		___quantity_4 = value;
		Il2CppCodeGenWriteBarrier(&___quantity_4, value);
	}

	inline static int32_t get_offset_of_price_5() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___price_5)); }
	inline Text_t356221433 * get_price_5() const { return ___price_5; }
	inline Text_t356221433 ** get_address_of_price_5() { return &___price_5; }
	inline void set_price_5(Text_t356221433 * value)
	{
		___price_5 = value;
		Il2CppCodeGenWriteBarrier(&___price_5, value);
	}

	inline static int32_t get_offset_of_date_6() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___date_6)); }
	inline Text_t356221433 * get_date_6() const { return ___date_6; }
	inline Text_t356221433 ** get_address_of_date_6() { return &___date_6; }
	inline void set_date_6(Text_t356221433 * value)
	{
		___date_6 = value;
		Il2CppCodeGenWriteBarrier(&___date_6, value);
	}

	inline static int32_t get_offset_of_fromUser_7() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___fromUser_7)); }
	inline Text_t356221433 * get_fromUser_7() const { return ___fromUser_7; }
	inline Text_t356221433 ** get_address_of_fromUser_7() { return &___fromUser_7; }
	inline void set_fromUser_7(Text_t356221433 * value)
	{
		___fromUser_7 = value;
		Il2CppCodeGenWriteBarrier(&___fromUser_7, value);
	}

	inline static int32_t get_offset_of_cancelOfferBtn_8() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___cancelOfferBtn_8)); }
	inline GameObject_t1756533147 * get_cancelOfferBtn_8() const { return ___cancelOfferBtn_8; }
	inline GameObject_t1756533147 ** get_address_of_cancelOfferBtn_8() { return &___cancelOfferBtn_8; }
	inline void set_cancelOfferBtn_8(GameObject_t1756533147 * value)
	{
		___cancelOfferBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&___cancelOfferBtn_8, value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___owner_9)); }
	inline FinancialContentChanger_t2753898366 * get_owner_9() const { return ___owner_9; }
	inline FinancialContentChanger_t2753898366 ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(FinancialContentChanger_t2753898366 * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier(&___owner_9, value);
	}

	inline static int32_t get_offset_of_lotId_10() { return static_cast<int32_t>(offsetof(FinancialTableLine_t2609388451, ___lotId_10)); }
	inline String_t* get_lotId_10() const { return ___lotId_10; }
	inline String_t** get_address_of_lotId_10() { return &___lotId_10; }
	inline void set_lotId_10(String_t* value)
	{
		___lotId_10 = value;
		Il2CppCodeGenWriteBarrier(&___lotId_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
