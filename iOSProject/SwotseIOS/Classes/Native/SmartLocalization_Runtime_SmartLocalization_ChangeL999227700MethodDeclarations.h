﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.ChangeLanguageEventHandler
struct ChangeLanguageEventHandler_t999227700;
// System.Object
struct Il2CppObject;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "SmartLocalization_Runtime_SmartLocalization_Languag132697751.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void SmartLocalization.ChangeLanguageEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ChangeLanguageEventHandler__ctor_m1425683676 (ChangeLanguageEventHandler_t999227700 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.ChangeLanguageEventHandler::Invoke(SmartLocalization.LanguageManager)
extern "C"  void ChangeLanguageEventHandler_Invoke_m2009378203 (ChangeLanguageEventHandler_t999227700 * __this, LanguageManager_t132697751 * ___thisLanguageManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SmartLocalization.ChangeLanguageEventHandler::BeginInvoke(SmartLocalization.LanguageManager,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ChangeLanguageEventHandler_BeginInvoke_m930550846 (ChangeLanguageEventHandler_t999227700 * __this, LanguageManager_t132697751 * ___thisLanguageManager0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.ChangeLanguageEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ChangeLanguageEventHandler_EndInvoke_m3638622790 (ChangeLanguageEventHandler_t999227700 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
