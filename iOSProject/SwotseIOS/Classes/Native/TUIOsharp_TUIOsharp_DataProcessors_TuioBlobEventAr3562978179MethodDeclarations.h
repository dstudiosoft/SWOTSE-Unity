﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct TuioBlobEventArgs_t3562978179;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t2046943414;

#include "codegen/il2cpp-codegen.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioBlob2046943414.h"

// System.Void TUIOsharp.DataProcessors.TuioBlobEventArgs::.ctor(TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioBlobEventArgs__ctor_m3423126035 (TuioBlobEventArgs_t3562978179 * __this, TuioBlob_t2046943414 * ___blob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
