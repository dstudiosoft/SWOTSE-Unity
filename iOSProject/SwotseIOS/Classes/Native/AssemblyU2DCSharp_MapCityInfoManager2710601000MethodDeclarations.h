﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapCityInfoManager
struct MapCityInfoManager_t2710601000;

#include "codegen/il2cpp-codegen.h"

// System.Void MapCityInfoManager::.ctor()
extern "C"  void MapCityInfoManager__ctor_m3258372973 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::OnEnable()
extern "C"  void MapCityInfoManager_OnEnable_m1318159765 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::Revolt()
extern "C"  void MapCityInfoManager_Revolt_m3201966871 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::Nominate()
extern "C"  void MapCityInfoManager_Nominate_m387286100 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::March()
extern "C"  void MapCityInfoManager_March_m1253573496 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::OpenPlayerShop()
extern "C"  void MapCityInfoManager_OpenPlayerShop_m329003758 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCityInfoManager::OpenGiftItem()
extern "C"  void MapCityInfoManager_OpenGiftItem_m3384435190 (MapCityInfoManager_t2710601000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
