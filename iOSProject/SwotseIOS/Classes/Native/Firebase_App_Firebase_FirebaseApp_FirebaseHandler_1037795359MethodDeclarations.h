﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs
struct ApplicationFocusChangedEventArgs_t1037795359;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs::.ctor()
extern "C"  void ApplicationFocusChangedEventArgs__ctor_m2814623206 (ApplicationFocusChangedEventArgs_t1037795359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs::get_HasFocus()
extern "C"  bool ApplicationFocusChangedEventArgs_get_HasFocus_m3050623079 (ApplicationFocusChangedEventArgs_t1037795359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs::set_HasFocus(System.Boolean)
extern "C"  void ApplicationFocusChangedEventArgs_set_HasFocus_m345003552 (ApplicationFocusChangedEventArgs_t1037795359 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
