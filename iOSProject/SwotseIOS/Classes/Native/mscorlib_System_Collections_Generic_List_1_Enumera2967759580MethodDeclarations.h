﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>
struct List_1_t3433029906;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2967759580.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m469395752_gshared (Enumerator_t2967759580 * __this, List_1_t3433029906 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m469395752(__this, ___l0, method) ((  void (*) (Enumerator_t2967759580 *, List_1_t3433029906 *, const MethodInfo*))Enumerator__ctor_m469395752_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2731867138_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2731867138(__this, method) ((  void (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2731867138_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m421072796_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m421072796(__this, method) ((  Il2CppObject * (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m421072796_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m2744504081_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2744504081(__this, method) ((  void (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_Dispose_m2744504081_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3421403858_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3421403858(__this, method) ((  void (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_VerifyState_m3421403858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m411101286_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m411101286(__this, method) ((  bool (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_MoveNext_m411101286_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RaycastHit2D>::get_Current()
extern "C"  RaycastHit2D_t4063908774  Enumerator_get_Current_m2244800689_gshared (Enumerator_t2967759580 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2244800689(__this, method) ((  RaycastHit2D_t4063908774  (*) (Enumerator_t2967759580 *, const MethodInfo*))Enumerator_get_Current_m2244800689_gshared)(__this, method)
