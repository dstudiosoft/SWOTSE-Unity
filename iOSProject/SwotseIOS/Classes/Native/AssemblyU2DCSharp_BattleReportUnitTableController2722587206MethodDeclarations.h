﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportUnitTableController
struct BattleReportUnitTableController_t2722587206;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void BattleReportUnitTableController::.ctor()
extern "C"  void BattleReportUnitTableController__ctor_m1174340355 (BattleReportUnitTableController_t2722587206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportUnitTableController::Start()
extern "C"  void BattleReportUnitTableController_Start_m358007927 (BattleReportUnitTableController_t2722587206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BattleReportUnitTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t BattleReportUnitTableController_GetNumberOfRowsForTableView_m1264727327 (BattleReportUnitTableController_t2722587206 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BattleReportUnitTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float BattleReportUnitTableController_GetHeightForRowInTableView_m3096549583 (BattleReportUnitTableController_t2722587206 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell BattleReportUnitTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * BattleReportUnitTableController_GetCellForRowInTableView_m1988265560 (BattleReportUnitTableController_t2722587206 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
