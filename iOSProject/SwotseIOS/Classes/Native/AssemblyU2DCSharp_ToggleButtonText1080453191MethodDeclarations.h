﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToggleButtonText
struct ToggleButtonText_t1080453191;

#include "codegen/il2cpp-codegen.h"

// System.Void ToggleButtonText::.ctor()
extern "C"  void ToggleButtonText__ctor_m385117814 (ToggleButtonText_t1080453191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleButtonText::Start()
extern "C"  void ToggleButtonText_Start_m497298482 (ToggleButtonText_t1080453191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleButtonText::ToggleTextActive(System.Boolean)
extern "C"  void ToggleButtonText_ToggleTextActive_m558513388 (ToggleButtonText_t1080453191 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
