﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Boolean OSCsharp.Data.OscMessage::get_IsBundle()
extern "C"  bool OscMessage_get_IsBundle_m4053277334 (OscMessage_t2764280154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscMessage::.ctor(System.String)
extern "C"  void OscMessage__ctor_m2211520241 (OscMessage_t2764280154 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscMessage OSCsharp.Data.OscMessage::FromByteArray(System.Byte[],System.Int32&)
extern "C"  OscMessage_t2764280154 * OscMessage_FromByteArray_m4248709050 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OSCsharp.Data.OscMessage::Append(System.Object)
extern "C"  int32_t OscMessage_Append_m2811322561 (OscMessage_t2764280154 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
