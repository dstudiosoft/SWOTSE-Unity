﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityDeedContentManager
struct CityDeedContentManager_t3098988935;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void CityDeedContentManager::.ctor()
extern "C"  void CityDeedContentManager__ctor_m1665893984 (CityDeedContentManager_t3098988935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityDeedContentManager::OnEnable()
extern "C"  void CityDeedContentManager_OnEnable_m4229980340 (CityDeedContentManager_t3098988935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityDeedContentManager::GetExoticInfo()
extern "C"  Il2CppObject * CityDeedContentManager_GetExoticInfo_m2877751350 (CityDeedContentManager_t3098988935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityDeedContentManager::DropdownValueChanged()
extern "C"  void CityDeedContentManager_DropdownValueChanged_m689631932 (CityDeedContentManager_t3098988935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityDeedContentManager::Activate()
extern "C"  void CityDeedContentManager_Activate_m580179047 (CityDeedContentManager_t3098988935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
