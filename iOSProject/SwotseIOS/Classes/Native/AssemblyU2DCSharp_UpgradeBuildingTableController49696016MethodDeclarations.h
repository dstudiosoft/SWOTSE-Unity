﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpgradeBuildingTableController
struct UpgradeBuildingTableController_t49696016;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UpgradeBuildingTableController::.ctor()
extern "C"  void UpgradeBuildingTableController__ctor_m1845414255 (UpgradeBuildingTableController_t49696016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableController::OnEnable()
extern "C"  void UpgradeBuildingTableController_OnEnable_m3361619771 (UpgradeBuildingTableController_t49696016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UpgradeBuildingTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t UpgradeBuildingTableController_GetNumberOfRowsForTableView_m737602883 (UpgradeBuildingTableController_t49696016 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UpgradeBuildingTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float UpgradeBuildingTableController_GetHeightForRowInTableView_m3181934355 (UpgradeBuildingTableController_t49696016 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell UpgradeBuildingTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * UpgradeBuildingTableController_GetCellForRowInTableView_m91306406 (UpgradeBuildingTableController_t49696016 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableController::UpgradeBuilding(System.Int64)
extern "C"  void UpgradeBuildingTableController_UpgradeBuilding_m3699797087 (UpgradeBuildingTableController_t49696016 * __this, int64_t ___building_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeBuildingTableController::BuildingUpgraded(System.Object,System.String)
extern "C"  void UpgradeBuildingTableController_BuildingUpgraded_m4141819289 (UpgradeBuildingTableController_t49696016 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
