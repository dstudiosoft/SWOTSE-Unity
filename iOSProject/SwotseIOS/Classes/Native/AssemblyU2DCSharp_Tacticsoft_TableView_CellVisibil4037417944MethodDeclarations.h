﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableView/CellVisibilityChangeEvent
struct CellVisibilityChangeEvent_t4037417944;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.TableView/CellVisibilityChangeEvent::.ctor()
extern "C"  void CellVisibilityChangeEvent__ctor_m3214858737 (CellVisibilityChangeEvent_t4037417944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
