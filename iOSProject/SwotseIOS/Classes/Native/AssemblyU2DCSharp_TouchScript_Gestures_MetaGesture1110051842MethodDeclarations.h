﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.MetaGesture
struct MetaGesture_t1110051842;
// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>
struct EventHandler_1_t3142866083;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.Gestures.MetaGesture::.ctor()
extern "C"  void MetaGesture__ctor_m724088783 (MetaGesture_t1110051842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::add_TouchBegan(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_add_TouchBegan_m1640816346 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::remove_TouchBegan(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_remove_TouchBegan_m2169721139 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::add_TouchMoved(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_add_TouchMoved_m3517694744 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::remove_TouchMoved(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_remove_TouchMoved_m649450145 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::add_TouchEnded(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_add_TouchEnded_m54634373 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::remove_TouchEnded(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_remove_TouchEnded_m3393052422 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::add_TouchCancelled(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_add_TouchCancelled_m2225733172 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::remove_TouchCancelled(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern "C"  void MetaGesture_remove_TouchCancelled_m1684505291 (MetaGesture_t1110051842 * __this, EventHandler_1_t3142866083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void MetaGesture_touchesBegan_m3509774337 (MetaGesture_t1110051842 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void MetaGesture_touchesMoved_m91464003 (MetaGesture_t1110051842 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void MetaGesture_touchesEnded_m2608872628 (MetaGesture_t1110051842 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGesture::touchesCancelled(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void MetaGesture_touchesCancelled_m3098997797 (MetaGesture_t1110051842 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
