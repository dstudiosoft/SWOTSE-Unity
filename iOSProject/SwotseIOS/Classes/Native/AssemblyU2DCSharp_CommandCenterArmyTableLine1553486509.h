﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2450766557;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterArmyTableLine
struct  CommandCenterArmyTableLine_t1553486509  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text CommandCenterArmyTableLine::knightName
	Text_t356221433 * ___knightName_2;
	// UnityEngine.UI.Text CommandCenterArmyTableLine::status
	Text_t356221433 * ___status_3;
	// System.Int64 CommandCenterArmyTableLine::armyId
	int64_t ___armyId_4;
	// CommandCenterArmiesTableController CommandCenterArmyTableLine::owner
	CommandCenterArmiesTableController_t2450766557 * ___owner_5;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1553486509, ___knightName_2)); }
	inline Text_t356221433 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t356221433 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t356221433 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier(&___knightName_2, value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1553486509, ___status_3)); }
	inline Text_t356221433 * get_status_3() const { return ___status_3; }
	inline Text_t356221433 ** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(Text_t356221433 * value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier(&___status_3, value);
	}

	inline static int32_t get_offset_of_armyId_4() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1553486509, ___armyId_4)); }
	inline int64_t get_armyId_4() const { return ___armyId_4; }
	inline int64_t* get_address_of_armyId_4() { return &___armyId_4; }
	inline void set_armyId_4(int64_t value)
	{
		___armyId_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1553486509, ___owner_5)); }
	inline CommandCenterArmiesTableController_t2450766557 * get_owner_5() const { return ___owner_5; }
	inline CommandCenterArmiesTableController_t2450766557 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(CommandCenterArmiesTableController_t2450766557 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
