﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginRegistrationContentManager
struct LoginRegistrationContentManager_t511879792;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoginRegistrationContentManager::.ctor()
extern "C"  void LoginRegistrationContentManager__ctor_m2147362415 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::Start()
extern "C"  void LoginRegistrationContentManager_Start_m4113227083 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::Update()
extern "C"  void LoginRegistrationContentManager_Update_m1886889554 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::TokenValid(System.Object,System.String)
extern "C"  void LoginRegistrationContentManager_TokenValid_m2523098050 (LoginRegistrationContentManager_t511879792 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::LoginContent()
extern "C"  void LoginRegistrationContentManager_LoginContent_m3839929093 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::RegisterContent()
extern "C"  void LoginRegistrationContentManager_RegisterContent_m2222883119 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::ForgotPasswordContent()
extern "C"  void LoginRegistrationContentManager_ForgotPasswordContent_m1322685088 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::GoToReset()
extern "C"  void LoginRegistrationContentManager_GoToReset_m681388361 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::InitializeEmperorsDropdown(System.Object,System.String)
extern "C"  void LoginRegistrationContentManager_InitializeEmperorsDropdown_m2784857989 (LoginRegistrationContentManager_t511879792 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::RegistrationHandler(System.Object,System.String)
extern "C"  void LoginRegistrationContentManager_RegistrationHandler_m3059577868 (LoginRegistrationContentManager_t511879792 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::Register()
extern "C"  void LoginRegistrationContentManager_Register_m1144129706 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::Login()
extern "C"  void LoginRegistrationContentManager_Login_m2081309798 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::SendForgotPassword()
extern "C"  void LoginRegistrationContentManager_SendForgotPassword_m1160826099 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::SendResetassword()
extern "C"  void LoginRegistrationContentManager_SendResetassword_m3665088685 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::ForgotPasswordSent(System.Object,System.String)
extern "C"  void LoginRegistrationContentManager_ForgotPasswordSent_m3482744447 (LoginRegistrationContentManager_t511879792 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::PasswordReset(System.Object,System.String)
extern "C"  void LoginRegistrationContentManager_PasswordReset_m4186784221 (LoginRegistrationContentManager_t511879792 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::EndEdit()
extern "C"  void LoginRegistrationContentManager_EndEdit_m2015523614 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::AddEmperorsOption(System.String)
extern "C"  void LoginRegistrationContentManager_AddEmperorsOption_m876108350 (LoginRegistrationContentManager_t511879792 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationContentManager::DirectLogin()
extern "C"  void LoginRegistrationContentManager_DirectLogin_m2047501125 (LoginRegistrationContentManager_t511879792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
