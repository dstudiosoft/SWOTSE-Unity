﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"

// System.Int32 Tacticsoft.RangeExtensions::Last(UnityEngine.SocialPlatforms.Range)
extern "C"  int32_t RangeExtensions_Last_m2965677155 (Il2CppObject * __this /* static, unused */, Range_t3455291607  ___range0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tacticsoft.RangeExtensions::Contains(UnityEngine.SocialPlatforms.Range,System.Int32)
extern "C"  bool RangeExtensions_Contains_m200564781 (Il2CppObject * __this /* static, unused */, Range_t3455291607  ___range0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
