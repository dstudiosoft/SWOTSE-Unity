﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.Default.BaseAuthService
struct BaseAuthService_t1401274330;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Platform.Default.BaseAuthService::.ctor()
extern "C"  void BaseAuthService__ctor_m3063501696 (BaseAuthService_t1401274330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.Default.BaseAuthService Firebase.Platform.Default.BaseAuthService::get_BaseInstance()
extern "C"  BaseAuthService_t1401274330 * BaseAuthService_get_BaseInstance_m3461016531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Default.BaseAuthService::.cctor()
extern "C"  void BaseAuthService__cctor_m147678205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
