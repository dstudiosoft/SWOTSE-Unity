﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.CameraLayer2D
struct CameraLayer2D_t3366536156;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_HitTest_ObjectHi3057876522.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void TouchScript.Layers.CameraLayer2D::.ctor()
extern "C"  void CameraLayer2D__ctor_m3751314901 (CameraLayer2D_t3366536156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayer2D::OnEnable()
extern "C"  void CameraLayer2D_OnEnable_m2185922677 (CameraLayer2D_t3366536156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.CameraLayer2D::castRay(UnityEngine.Ray,TouchScript.Hit.TouchHit&)
extern "C"  int32_t CameraLayer2D_castRay_m1977041065 (CameraLayer2D_t3366536156 * __this, Ray_t2469606224  ___ray0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.HitTest/ObjectHitResult TouchScript.Layers.CameraLayer2D::doHit(UnityEngine.RaycastHit2D,TouchScript.Hit.TouchHit&)
extern "C"  int32_t CameraLayer2D_doHit_m4004707958 (CameraLayer2D_t3366536156 * __this, RaycastHit2D_t4063908774  ___raycastHit0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayer2D::sortHits(UnityEngine.RaycastHit2D[])
extern "C"  void CameraLayer2D_sortHits_m3739555918 (CameraLayer2D_t3366536156 * __this, RaycastHit2DU5BU5D_t4176517891* ___hits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Layers.CameraLayer2D::<sortHits>m__1F(UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D)
extern "C"  int32_t CameraLayer2D_U3CsortHitsU3Em__1F_m554210979 (CameraLayer2D_t3366536156 * __this, RaycastHit2D_t4063908774  ___a0, RaycastHit2D_t4063908774  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
