﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t1716693387;
// System.Collections.Generic.IDictionary`2<System.IntPtr,System.Object>
struct IDictionary_2_t1130558034;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>[]
struct KeyValuePair_2U5BU5D_t3308171066;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct IEnumerator_1_t2659310958;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>
struct KeyCollection_t1320005088;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Object>
struct ValueCollection_t1834534456;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2049292515_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2049292515(__this, method) ((  void (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2__ctor_m2049292515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3517987906_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3517987906(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3517987906_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m381258573_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m381258573(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m381258573_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1920777434_gshared (Dictionary_2_t3131474613 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1920777434(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3131474613 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1920777434_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2656202810_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2656202810(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2656202810_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2185433197_gshared (Dictionary_2_t3131474613 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2185433197(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3131474613 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2185433197_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1512192236_gshared (Dictionary_2_t3131474613 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1512192236(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3131474613 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1512192236_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1481833205_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1481833205(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1481833205_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m681880277_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m681880277(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m681880277_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3046053902_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3046053902(__this, method) ((  bool (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3046053902_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1428883769_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1428883769(__this, method) ((  bool (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1428883769_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2748534755_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2748534755(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2748534755_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2290664072_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2290664072(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2290664072_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1525303029_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1525303029(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1525303029_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2062256889_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2062256889(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2062256889_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2901901588_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2901901588(__this, ___key0, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2901901588_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1738433079_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1738433079(__this, method) ((  bool (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1738433079_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1558190079_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1558190079(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1558190079_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m891739589_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m891739589(__this, method) ((  bool (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m891739589_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2806487606_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2_t888819835  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2806487606(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3131474613 *, KeyValuePair_2_t888819835 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2806487606_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3854541194_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2_t888819835  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3854541194(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3131474613 *, KeyValuePair_2_t888819835 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3854541194_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3494157322_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2U5BU5D_t3308171066* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3494157322(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3131474613 *, KeyValuePair_2U5BU5D_t3308171066*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3494157322_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3080541539_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2_t888819835  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3080541539(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3131474613 *, KeyValuePair_2_t888819835 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3080541539_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4271699631_gshared (Dictionary_2_t3131474613 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4271699631(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4271699631_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3398999812_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3398999812(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3398999812_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1742521661_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1742521661(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1742521661_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1484720898_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1484720898(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1484720898_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2956627055_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2956627055(__this, method) ((  int32_t (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_get_Count_m2956627055_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3968213314_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3968213314(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, IntPtr_t, const MethodInfo*))Dictionary_2_get_Item_m3968213314_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2612758555_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2612758555(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3131474613 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2612758555_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m248376403_gshared (Dictionary_2_t3131474613 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m248376403(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3131474613 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m248376403_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m852496598_gshared (Dictionary_2_t3131474613 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m852496598(__this, ___size0, method) ((  void (*) (Dictionary_2_t3131474613 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m852496598_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1657324124_gshared (Dictionary_2_t3131474613 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1657324124(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1657324124_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t888819835  Dictionary_2_make_pair_m2801182366_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2801182366(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t888819835  (*) (Il2CppObject * /* static, unused */, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2801182366_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::pick_key(TKey,TValue)
extern "C"  IntPtr_t Dictionary_2_pick_key_m885990472_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m885990472(__this /* static, unused */, ___key0, ___value1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m885990472_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m4103429600_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4103429600(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m4103429600_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3167539575_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2U5BU5D_t3308171066* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3167539575(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3131474613 *, KeyValuePair_2U5BU5D_t3308171066*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3167539575_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m4210316965_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4210316965(__this, method) ((  void (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_Resize_m4210316965_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4115462262_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4115462262(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3131474613 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4115462262_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3142955910_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3142955910(__this, method) ((  void (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_Clear_m3142955910_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3825551682_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3825551682(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3131474613 *, IntPtr_t, const MethodInfo*))Dictionary_2_ContainsKey_m3825551682_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3023067914_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3023067914(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3023067914_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3396287675_gshared (Dictionary_2_t3131474613 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3396287675(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3131474613 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3396287675_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1506989171_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1506989171(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1506989171_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1849931834_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1849931834(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3131474613 *, IntPtr_t, const MethodInfo*))Dictionary_2_Remove_m1849931834_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m148623115_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m148623115(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3131474613 *, IntPtr_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m148623115_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::get_Keys()
extern "C"  KeyCollection_t1320005088 * Dictionary_2_get_Keys_m1515697838_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1515697838(__this, method) ((  KeyCollection_t1320005088 * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_get_Keys_m1515697838_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::get_Values()
extern "C"  ValueCollection_t1834534456 * Dictionary_2_get_Values_m3988685278_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3988685278(__this, method) ((  ValueCollection_t1834534456 * (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_get_Values_m3988685278_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::ToTKey(System.Object)
extern "C"  IntPtr_t Dictionary_2_ToTKey_m2921766269_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2921766269(__this, ___key0, method) ((  IntPtr_t (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2921766269_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1227484397_gshared (Dictionary_2_t3131474613 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1227484397(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3131474613 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1227484397_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1750905675_gshared (Dictionary_2_t3131474613 * __this, KeyValuePair_2_t888819835  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1750905675(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3131474613 *, KeyValuePair_2_t888819835 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1750905675_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::GetEnumerator()
extern "C"  Enumerator_t156532019  Dictionary_2_GetEnumerator_m1420373766_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1420373766(__this, method) ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1420373766_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1430486661_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1430486661(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, IntPtr_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1430486661_gshared)(__this /* static, unused */, ___key0, ___value1, method)
