﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalGOScript
struct GlobalGOScript_t4026089874;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GlobalGOScript::.ctor()
extern "C"  void GlobalGOScript__ctor_m858133547 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::Start()
extern "C"  void GlobalGOScript_Start_m969403679 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlobalGOScript::LoadCoroutine()
extern "C"  Il2CppObject * GlobalGOScript_LoadCoroutine_m1403822229 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::initClassTree()
extern "C"  void GlobalGOScript_initClassTree_m1653451161 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::GetCities(System.Object,System.String)
extern "C"  void GlobalGOScript_GetCities_m2921004202 (GlobalGOScript_t4026089874 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::loadFields()
extern "C"  void GlobalGOScript_loadFields_m4284969884 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::loadCity()
extern "C"  void GlobalGOScript_loadCity_m3855020302 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::loadWorld()
extern "C"  void GlobalGOScript_loadWorld_m545514419 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::LogOut()
extern "C"  void GlobalGOScript_LogOut_m2447904517 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::Relogin()
extern "C"  void GlobalGOScript_Relogin_m82992127 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOScript::openMyUserInfo()
extern "C"  void GlobalGOScript_openMyUserInfo_m1774861928 (GlobalGOScript_t4026089874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
