﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t899845187;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct  SWIGStringHelper_t1711098632  : public Il2CppObject
{
public:

public:
};

struct SWIGStringHelper_t1711098632_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t899845187 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t1711098632_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_t899845187 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_t899845187 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_t899845187 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&___stringDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
