﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceModel
struct AllianceModel_t1834891198;

#include "codegen/il2cpp-codegen.h"

// System.Void AllianceModel::.ctor()
extern "C"  void AllianceModel__ctor_m4080114785 (AllianceModel_t1834891198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
