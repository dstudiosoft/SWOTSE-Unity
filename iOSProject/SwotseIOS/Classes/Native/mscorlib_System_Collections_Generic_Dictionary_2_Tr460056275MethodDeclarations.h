﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,AllianceModel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2935550117(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t460056275 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3373533409_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,AllianceModel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2769357049(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t460056275 *, int64_t, AllianceModel_t1834891198 *, const MethodInfo*))Transform_1_Invoke_m1299483797_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,AllianceModel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2658421374(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t460056275 *, int64_t, AllianceModel_t1834891198 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3448776694_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,AllianceModel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2281228099(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t460056275 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3006940059_gshared)(__this, ___result0, method)
