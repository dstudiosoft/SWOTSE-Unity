﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TUIOsharp.DataProcessors.CursorProcessor
struct CursorProcessor_t1785954004;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct EventHandler_1_t3249264480;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscMessage2764280154.h"

// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorAdded_m3817569644 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorAdded_m11359957 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorUpdated_m1967314603 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorUpdated_m107114368 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorRemoved_m4072586006 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorRemoved_m2056441227 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::set_FrameNumber(System.Int32)
extern "C"  void CursorProcessor_set_FrameNumber_m78169520 (CursorProcessor_t1785954004 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::.ctor()
extern "C"  void CursorProcessor__ctor_m3237265030 (CursorProcessor_t1785954004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.CursorProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void CursorProcessor_ProcessMessage_m823963401 (CursorProcessor_t1785954004 * __this, OscMessage_t2764280154 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
