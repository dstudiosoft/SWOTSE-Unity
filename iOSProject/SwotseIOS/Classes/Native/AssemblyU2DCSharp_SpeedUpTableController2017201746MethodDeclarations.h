﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpeedUpTableController
struct SpeedUpTableController_t2017201746;
// UnityEngine.Sprite
struct Sprite_t309593783;
// PanelTableController
struct PanelTableController_t1117991214;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_PanelTableController1117991214.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SpeedUpTableController::.ctor()
extern "C"  void SpeedUpTableController__ctor_m2339051863 (SpeedUpTableController_t2017201746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::SetContent(UnityEngine.Sprite,System.Int64,System.DateTime,System.DateTime,PanelTableController)
extern "C"  void SpeedUpTableController_SetContent_m2152238596 (SpeedUpTableController_t2017201746 * __this, Sprite_t309593783 * ___image0, int64_t ___id1, DateTime_t693205669  ___start_time2, DateTime_t693205669  ___finish_time3, PanelTableController_t1117991214 * ___controller4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::OnEnable()
extern "C"  void SpeedUpTableController_OnEnable_m1379828563 (SpeedUpTableController_t2017201746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::FixedUpdate()
extern "C"  void SpeedUpTableController_FixedUpdate_m2505224042 (SpeedUpTableController_t2017201746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::Start()
extern "C"  void SpeedUpTableController_Start_m4142205491 (SpeedUpTableController_t2017201746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpeedUpTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t SpeedUpTableController_GetNumberOfRowsForTableView_m1601456667 (SpeedUpTableController_t2017201746 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SpeedUpTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float SpeedUpTableController_GetHeightForRowInTableView_m3931443971 (SpeedUpTableController_t2017201746 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell SpeedUpTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * SpeedUpTableController_GetCellForRowInTableView_m1385843392 (SpeedUpTableController_t2017201746 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::TeleportUsed(System.Object,System.String)
extern "C"  void SpeedUpTableController_TeleportUsed_m1483868485 (SpeedUpTableController_t2017201746 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::ExpandUsed(System.Object,System.String)
extern "C"  void SpeedUpTableController_ExpandUsed_m2167377340 (SpeedUpTableController_t2017201746 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::GotShopItems(System.Object,System.String)
extern "C"  void SpeedUpTableController_GotShopItems_m3818954317 (SpeedUpTableController_t2017201746 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::UpdateTime(System.Object,System.String)
extern "C"  void SpeedUpTableController_UpdateTime_m4090888463 (SpeedUpTableController_t2017201746 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpTableController::ReloadTable(System.Object,System.String)
extern "C"  void SpeedUpTableController_ReloadTable_m1324800554 (SpeedUpTableController_t2017201746 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
