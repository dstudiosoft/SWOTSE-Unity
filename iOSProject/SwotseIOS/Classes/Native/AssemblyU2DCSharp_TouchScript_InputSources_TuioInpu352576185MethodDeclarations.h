﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.TuioInput
struct TuioInput_t352576185;
// System.Collections.Generic.IList`1<TouchScript.InputSources.TuioObjectMapping>
struct IList_1_t326900433;
// TouchScript.Tags
struct Tags_t1265380163;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t2046943414;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t1236821014;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct TuioCursorEventArgs_t362990012;
// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct TuioBlobEventArgs_t3562978179;
// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct TuioObjectEventArgs_t1810743341;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_TuioInpu948942308.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioBlob2046943414.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioObject1236821014.h"
#include "mscorlib_System_Object2689449295.h"
#include "TUIOsharp_TUIOsharp_DataProcessors_TuioCursorEventA362990012.h"
#include "TUIOsharp_TUIOsharp_DataProcessors_TuioBlobEventAr3562978179.h"
#include "TUIOsharp_TUIOsharp_DataProcessors_TuioObjectEvent1810743341.h"

// System.Void TouchScript.InputSources.TuioInput::.ctor()
extern "C"  void TuioInput__ctor_m676978166 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.InputSources.TuioInput::get_TuioPort()
extern "C"  int32_t TuioInput_get_TuioPort_m2067852177 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::set_TuioPort(System.Int32)
extern "C"  void TuioInput_set_TuioPort_m425562120 (TuioInput_t352576185 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.InputSources.TuioInput/InputType TouchScript.InputSources.TuioInput::get_SupportedInputs()
extern "C"  int32_t TuioInput_get_SupportedInputs_m456643271 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::set_SupportedInputs(TouchScript.InputSources.TuioInput/InputType)
extern "C"  void TuioInput_set_SupportedInputs_m962818250 (TuioInput_t352576185 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.InputSources.TuioObjectMapping> TouchScript.InputSources.TuioInput::get_TuioObjectMappings()
extern "C"  Il2CppObject* TuioInput_get_TuioObjectMappings_m2813004674 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Tags TouchScript.InputSources.TuioInput::get_CursorTags()
extern "C"  Tags_t1265380163 * TuioInput_get_CursorTags_m3861091728 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Tags TouchScript.InputSources.TuioInput::get_BlobTags()
extern "C"  Tags_t1265380163 * TuioInput_get_BlobTags_m2477109769 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Tags TouchScript.InputSources.TuioInput::get_ObjectTags()
extern "C"  Tags_t1265380163 * TuioInput_get_ObjectTags_m1774429911 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::UpdateInput()
extern "C"  void TuioInput_UpdateInput_m3576128039 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  void TuioInput_CancelTouch_m1259888973 (TuioInput_t352576185 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnEnable()
extern "C"  void TuioInput_OnEnable_m3232650838 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnDisable()
extern "C"  void TuioInput_OnDisable_m342421033 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::connect()
extern "C"  void TuioInput_connect_m2783455924 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::disconnect()
extern "C"  void TuioInput_disconnect_m2157420738 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::updateInputs()
extern "C"  void TuioInput_updateInputs_m1523969248 (TuioInput_t352576185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::updateBlobProperties(TouchScript.TouchPoint,TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioInput_updateBlobProperties_m3189835072 (TuioInput_t352576185 * __this, TouchPoint_t959629083 * ___touch0, TuioBlob_t2046943414 * ___blob1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::updateObjectProperties(TouchScript.TouchPoint,TUIOsharp.Entities.TuioObject)
extern "C"  void TuioInput_updateObjectProperties_m3027936836 (TuioInput_t352576185 * __this, TouchPoint_t959629083 * ___touch0, TuioObject_t1236821014 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.InputSources.TuioInput::getTagById(System.Int32)
extern "C"  String_t* TuioInput_getTagById_m3882234520 (TuioInput_t352576185 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnCursorAdded(System.Object,TUIOsharp.DataProcessors.TuioCursorEventArgs)
extern "C"  void TuioInput_OnCursorAdded_m1374456933 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioCursorEventArgs_t362990012 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnCursorUpdated(System.Object,TUIOsharp.DataProcessors.TuioCursorEventArgs)
extern "C"  void TuioInput_OnCursorUpdated_m1311251918 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioCursorEventArgs_t362990012 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnCursorRemoved(System.Object,TUIOsharp.DataProcessors.TuioCursorEventArgs)
extern "C"  void TuioInput_OnCursorRemoved_m1362002799 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioCursorEventArgs_t362990012 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnBlobAdded(System.Object,TUIOsharp.DataProcessors.TuioBlobEventArgs)
extern "C"  void TuioInput_OnBlobAdded_m2439423641 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioBlobEventArgs_t3562978179 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnBlobUpdated(System.Object,TUIOsharp.DataProcessors.TuioBlobEventArgs)
extern "C"  void TuioInput_OnBlobUpdated_m3284270840 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioBlobEventArgs_t3562978179 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnBlobRemoved(System.Object,TUIOsharp.DataProcessors.TuioBlobEventArgs)
extern "C"  void TuioInput_OnBlobRemoved_m1081222883 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioBlobEventArgs_t3562978179 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnObjectAdded(System.Object,TUIOsharp.DataProcessors.TuioObjectEventArgs)
extern "C"  void TuioInput_OnObjectAdded_m2806244885 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioObjectEventArgs_t1810743341 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnObjectUpdated(System.Object,TUIOsharp.DataProcessors.TuioObjectEventArgs)
extern "C"  void TuioInput_OnObjectUpdated_m1396578124 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioObjectEventArgs_t1810743341 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.TuioInput::OnObjectRemoved(System.Object,TUIOsharp.DataProcessors.TuioObjectEventArgs)
extern "C"  void TuioInput_OnObjectRemoved_m2437382507 (TuioInput_t352576185 * __this, Il2CppObject * ___sender0, TuioObjectEventArgs_t1810743341 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
