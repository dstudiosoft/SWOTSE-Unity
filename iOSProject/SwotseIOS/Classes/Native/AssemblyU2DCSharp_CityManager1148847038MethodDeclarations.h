﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityManager
struct CityManager_t1148847038;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CityManager::.ctor()
extern "C"  void CityManager__ctor_m2496501791 (CityManager_t1148847038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_onCityBuilt(SimpleEvent)
extern "C"  void CityManager_add_onCityBuilt_m1949420107 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_onCityBuilt(SimpleEvent)
extern "C"  void CityManager_remove_onCityBuilt_m1168573580 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_onGotNewCity(SimpleEvent)
extern "C"  void CityManager_add_onGotNewCity_m3595405977 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_onGotNewCity(SimpleEvent)
extern "C"  void CityManager_remove_onGotNewCity_m4046410594 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_onCityNameAndIconChanged(SimpleEvent)
extern "C"  void CityManager_add_onCityNameAndIconChanged_m3058483232 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_onCityNameAndIconChanged(SimpleEvent)
extern "C"  void CityManager_remove_onCityNameAndIconChanged_m845798411 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_cityChangerUpdateImage(SimpleEvent)
extern "C"  void CityManager_add_cityChangerUpdateImage_m1055672470 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_cityChangerUpdateImage(SimpleEvent)
extern "C"  void CityManager_remove_cityChangerUpdateImage_m501741745 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_gotAllowTroopsState(SimpleEvent)
extern "C"  void CityManager_add_gotAllowTroopsState_m2519512782 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_gotAllowTroopsState(SimpleEvent)
extern "C"  void CityManager_remove_gotAllowTroopsState_m3883588563 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::add_onTaxUpdated(SimpleEvent)
extern "C"  void CityManager_add_onTaxUpdated_m1050004048 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityManager::remove_onTaxUpdated(SimpleEvent)
extern "C"  void CityManager_remove_onTaxUpdated_m4162825249 (CityManager_t1148847038 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::GetInitialCities()
extern "C"  Il2CppObject * CityManager_GetInitialCities_m3863714442 (CityManager_t1148847038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::BuildCity(System.Int64,System.Int64,System.String)
extern "C"  Il2CppObject * CityManager_BuildCity_m2667028784 (CityManager_t1148847038 * __this, int64_t ___x0, int64_t ___y1, String_t* ___cityName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::ChangeNameAndImage(System.String,System.String)
extern "C"  Il2CppObject * CityManager_ChangeNameAndImage_m1213226702 (CityManager_t1148847038 * __this, String_t* ___cityName0, String_t* ___imageName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::UpdateCurrentCityCoords()
extern "C"  Il2CppObject * CityManager_UpdateCurrentCityCoords_m1886164250 (CityManager_t1148847038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::UpdateCityExpantion()
extern "C"  Il2CppObject * CityManager_UpdateCityExpantion_m1364878137 (CityManager_t1148847038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::UpdateCityAllowTroops()
extern "C"  Il2CppObject * CityManager_UpdateCityAllowTroops_m1855801491 (CityManager_t1148847038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::SetAllowTroops(System.Boolean)
extern "C"  Il2CppObject * CityManager_SetAllowTroops_m1458683720 (CityManager_t1148847038 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::UpdateCityTax(System.Int64)
extern "C"  Il2CppObject * CityManager_UpdateCityTax_m2387222414 (CityManager_t1148847038 * __this, int64_t ___taxRate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityManager::StartRevolution(System.Int64)
extern "C"  Il2CppObject * CityManager_StartRevolution_m3919997630 (CityManager_t1148847038 * __this, int64_t ___cityID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
