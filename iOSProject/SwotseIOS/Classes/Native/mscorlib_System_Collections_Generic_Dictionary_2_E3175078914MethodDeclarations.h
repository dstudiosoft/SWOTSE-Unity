﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m545369057(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3175078914 *, Dictionary_2_t1855054212 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2472510838(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1060379446(__this, method) ((  void (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2227453915(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1295574216(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2981850790(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::MoveNext()
#define Enumerator_MoveNext_m2288966834(__this, method) ((  bool (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::get_Current()
#define Enumerator_get_Current_m3603586230(__this, method) ((  KeyValuePair_2_t3907366730  (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2642074277(__this, method) ((  int32_t (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1850944645(__this, method) ((  Action_t2847228577 * (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::Reset()
#define Enumerator_Reset_m1330444447(__this, method) ((  void (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::VerifyState()
#define Enumerator_VerifyState_m1075657502(__this, method) ((  void (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3666985166(__this, method) ((  void (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.FutureVoid/Action>::Dispose()
#define Enumerator_Dispose_m308253833(__this, method) ((  void (*) (Enumerator_t3175078914 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
