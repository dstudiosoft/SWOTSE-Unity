﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.WindowsPrincipal
struct WindowsPrincipal_t1402612279;
// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t373339331;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Principal_WindowsIdentity373339331.h"

// System.Void System.Security.Principal.WindowsPrincipal::.ctor(System.Security.Principal.WindowsIdentity)
extern "C"  void WindowsPrincipal__ctor_m1953065950 (WindowsPrincipal_t1402612279 * __this, WindowsIdentity_t373339331 * ___ntIdentity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
