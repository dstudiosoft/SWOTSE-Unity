﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct EventHandler_1_t1343734987;
// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct EventHandler_1_t266896245;
// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_t1597498947;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging
struct  FirebaseMessaging_t1383481983  : public Il2CppObject
{
public:

public:
};

struct FirebaseMessaging_t1383481983_StaticFields
{
public:
	// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::MessageReceivedInternal
	EventHandler_1_t1343734987 * ___MessageReceivedInternal_0;
	// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::TokenReceivedInternal
	EventHandler_1_t266896245 * ___TokenReceivedInternal_1;
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging::listener
	Listener_t1597498947 * ___listener_2;

public:
	inline static int32_t get_offset_of_MessageReceivedInternal_0() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t1383481983_StaticFields, ___MessageReceivedInternal_0)); }
	inline EventHandler_1_t1343734987 * get_MessageReceivedInternal_0() const { return ___MessageReceivedInternal_0; }
	inline EventHandler_1_t1343734987 ** get_address_of_MessageReceivedInternal_0() { return &___MessageReceivedInternal_0; }
	inline void set_MessageReceivedInternal_0(EventHandler_1_t1343734987 * value)
	{
		___MessageReceivedInternal_0 = value;
		Il2CppCodeGenWriteBarrier(&___MessageReceivedInternal_0, value);
	}

	inline static int32_t get_offset_of_TokenReceivedInternal_1() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t1383481983_StaticFields, ___TokenReceivedInternal_1)); }
	inline EventHandler_1_t266896245 * get_TokenReceivedInternal_1() const { return ___TokenReceivedInternal_1; }
	inline EventHandler_1_t266896245 ** get_address_of_TokenReceivedInternal_1() { return &___TokenReceivedInternal_1; }
	inline void set_TokenReceivedInternal_1(EventHandler_1_t266896245 * value)
	{
		___TokenReceivedInternal_1 = value;
		Il2CppCodeGenWriteBarrier(&___TokenReceivedInternal_1, value);
	}

	inline static int32_t get_offset_of_listener_2() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t1383481983_StaticFields, ___listener_2)); }
	inline Listener_t1597498947 * get_listener_2() const { return ___listener_2; }
	inline Listener_t1597498947 ** get_address_of_listener_2() { return &___listener_2; }
	inline void set_listener_2(Listener_t1597498947 * value)
	{
		___listener_2 = value;
		Il2CppCodeGenWriteBarrier(&___listener_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
