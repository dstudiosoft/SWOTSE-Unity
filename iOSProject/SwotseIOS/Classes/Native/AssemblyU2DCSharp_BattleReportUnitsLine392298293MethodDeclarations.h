﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportUnitsLine
struct BattleReportUnitsLine_t392298293;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BattleReportUnitsLine::.ctor()
extern "C"  void BattleReportUnitsLine__ctor_m2450332188 (BattleReportUnitsLine_t392298293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportUnitsLine::SetUnitName(System.String)
extern "C"  void BattleReportUnitsLine_SetUnitName_m2613175625 (BattleReportUnitsLine_t392298293 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportUnitsLine::SetBeforeCount(System.Int64)
extern "C"  void BattleReportUnitsLine_SetBeforeCount_m746504892 (BattleReportUnitsLine_t392298293 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportUnitsLine::SetLostCount(System.Int64)
extern "C"  void BattleReportUnitsLine_SetLostCount_m1593503023 (BattleReportUnitsLine_t392298293 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportUnitsLine::SetRemainingCount(System.Int64)
extern "C"  void BattleReportUnitsLine_SetRemainingCount_m317107571 (BattleReportUnitsLine_t392298293 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
