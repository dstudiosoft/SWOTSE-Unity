﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m503429277(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t581754822 *, int64_t, AllianceModel_t1834891198 *, const MethodInfo*))KeyValuePair_2__ctor_m1940188057_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::get_Key()
#define KeyValuePair_2_get_Key_m2844943655(__this, method) ((  int64_t (*) (KeyValuePair_2_t581754822 *, const MethodInfo*))KeyValuePair_2_get_Key_m3157806383_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1510866502(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t581754822 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Key_m3454144490_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::get_Value()
#define KeyValuePair_2_get_Value_m2700309463(__this, method) ((  AllianceModel_t1834891198 * (*) (KeyValuePair_2_t581754822 *, const MethodInfo*))KeyValuePair_2_get_Value_m1978486399_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2047983366(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t581754822 *, AllianceModel_t1834891198 *, const MethodInfo*))KeyValuePair_2_set_Value_m332482250_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,AllianceModel>::ToString()
#define KeyValuePair_2_ToString_m1230467344(__this, method) ((  String_t* (*) (KeyValuePair_2_t581754822 *, const MethodInfo*))KeyValuePair_2_ToString_m338067320_gshared)(__this, method)
