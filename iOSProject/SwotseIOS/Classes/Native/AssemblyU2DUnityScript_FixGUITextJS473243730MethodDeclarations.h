﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FixGUITextJS
struct FixGUITextJS_t473243730;

#include "codegen/il2cpp-codegen.h"

// System.Void FixGUITextJS::.ctor()
extern "C"  void FixGUITextJS__ctor_m1594083844 (FixGUITextJS_t473243730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixGUITextJS::Start()
extern "C"  void FixGUITextJS_Start_m1342403896 (FixGUITextJS_t473243730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixGUITextJS::Update()
extern "C"  void FixGUITextJS_Update_m264811743 (FixGUITextJS_t473243730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixGUITextJS::Main()
extern "C"  void FixGUITextJS_Main_m2077158513 (FixGUITextJS_t473243730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
