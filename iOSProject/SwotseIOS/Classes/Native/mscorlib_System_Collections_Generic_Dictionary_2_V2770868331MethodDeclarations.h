﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2770868331.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3195405961_gshared (Enumerator_t2770868331 * __this, Dictionary_2_t1084335567 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3195405961(__this, ___host0, method) ((  void (*) (Enumerator_t2770868331 *, Dictionary_2_t1084335567 *, const MethodInfo*))Enumerator__ctor_m3195405961_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4255222436_gshared (Enumerator_t2770868331 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4255222436(__this, method) ((  Il2CppObject * (*) (Enumerator_t2770868331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4255222436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3170477914_gshared (Enumerator_t2770868331 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3170477914(__this, method) ((  void (*) (Enumerator_t2770868331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3170477914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m2077543441_gshared (Enumerator_t2770868331 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2077543441(__this, method) ((  void (*) (Enumerator_t2770868331 *, const MethodInfo*))Enumerator_Dispose_m2077543441_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4146601070_gshared (Enumerator_t2770868331 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4146601070(__this, method) ((  bool (*) (Enumerator_t2770868331 *, const MethodInfo*))Enumerator_MoveNext_m4146601070_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m3247687540_gshared (Enumerator_t2770868331 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3247687540(__this, method) ((  float (*) (Enumerator_t2770868331 *, const MethodInfo*))Enumerator_get_Current_m3247687540_gshared)(__this, method)
