﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Variant
struct Variant_t4275788079;
// Firebase.VariantList
struct VariantList_t2408673687;
// Firebase.VariantVariantMap
struct VariantVariantMap_t4097173110;
// System.String
struct String_t;
// Firebase.CharVector
struct CharVector_t100493339;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_Variant4275788079.h"
#include "Firebase_App_Firebase_Variant_Type1054148541.h"

// System.Void Firebase.Variant::.ctor(System.IntPtr,System.Boolean)
extern "C"  void Variant__ctor_m2926939316 (Variant_t4275788079 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.Variant::getCPtr(Firebase.Variant)
extern "C"  HandleRef_t2419939847  Variant_getCPtr_m993256526 (Il2CppObject * __this /* static, unused */, Variant_t4275788079 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Variant::Finalize()
extern "C"  void Variant_Finalize_m2723095643 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Variant::Dispose()
extern "C"  void Variant_Dispose_m2059275888 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant/Type Firebase.Variant::type()
extern "C"  int32_t Variant_type_m680069213 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Variant::is_string()
extern "C"  bool Variant_is_string_m178977129 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Variant::is_fundamental_type()
extern "C"  bool Variant_is_fundamental_type_m3372920486 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.Variant::AsString()
extern "C"  Variant_t4275788079 * Variant_AsString_m330643501 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantList Firebase.Variant::vector()
extern "C"  VariantList_t2408673687 * Variant_vector_m1178991757 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantVariantMap Firebase.Variant::map()
extern "C"  VariantVariantMap_t4097173110 * Variant_map_m2369869105 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Firebase.Variant::int64_value()
extern "C"  int64_t Variant_int64_value_m1611442941 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Firebase.Variant::double_value()
extern "C"  double Variant_double_value_m810825177 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Variant::bool_value()
extern "C"  bool Variant_bool_value_m1192102035 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Variant::string_value()
extern "C"  String_t* Variant_string_value_m1582645401 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.CharVector Firebase.Variant::blob_as_vector()
extern "C"  CharVector_t100493339 * Variant_blob_as_vector_m1140487796 (Variant_t4275788079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
