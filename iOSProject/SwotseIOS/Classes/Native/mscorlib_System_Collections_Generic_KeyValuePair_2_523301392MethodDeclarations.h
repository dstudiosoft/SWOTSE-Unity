﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1438588297(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t523301392 *, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Key()
#define KeyValuePair_2_get_Key_m1778912775(__this, method) ((  Canvas_t209405766 * (*) (KeyValuePair_2_t523301392 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3762587826(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t523301392 *, Canvas_t209405766 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Value()
#define KeyValuePair_2_get_Value_m3920605799(__this, method) ((  ProjectionParams_t2712959773 * (*) (KeyValuePair_2_t523301392 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2661141250(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t523301392 *, ProjectionParams_t2712959773 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ToString()
#define KeyValuePair_2_ToString_m896509336(__this, method) ((  String_t* (*) (KeyValuePair_2_t523301392 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
