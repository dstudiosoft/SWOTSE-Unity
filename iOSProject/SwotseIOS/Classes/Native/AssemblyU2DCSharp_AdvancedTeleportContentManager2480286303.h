﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;
// IReloadable
struct IReloadable_t861412162;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdvancedTeleportContentManager
struct  AdvancedTeleportContentManager_t2480286303  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField AdvancedTeleportContentManager::xCoord
	InputField_t1631627530 * ___xCoord_2;
	// UnityEngine.UI.InputField AdvancedTeleportContentManager::yCoord
	InputField_t1631627530 * ___yCoord_3;
	// System.String AdvancedTeleportContentManager::item_id
	String_t* ___item_id_4;
	// IReloadable AdvancedTeleportContentManager::owner
	Il2CppObject * ___owner_5;

public:
	inline static int32_t get_offset_of_xCoord_2() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2480286303, ___xCoord_2)); }
	inline InputField_t1631627530 * get_xCoord_2() const { return ___xCoord_2; }
	inline InputField_t1631627530 ** get_address_of_xCoord_2() { return &___xCoord_2; }
	inline void set_xCoord_2(InputField_t1631627530 * value)
	{
		___xCoord_2 = value;
		Il2CppCodeGenWriteBarrier(&___xCoord_2, value);
	}

	inline static int32_t get_offset_of_yCoord_3() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2480286303, ___yCoord_3)); }
	inline InputField_t1631627530 * get_yCoord_3() const { return ___yCoord_3; }
	inline InputField_t1631627530 ** get_address_of_yCoord_3() { return &___yCoord_3; }
	inline void set_yCoord_3(InputField_t1631627530 * value)
	{
		___yCoord_3 = value;
		Il2CppCodeGenWriteBarrier(&___yCoord_3, value);
	}

	inline static int32_t get_offset_of_item_id_4() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2480286303, ___item_id_4)); }
	inline String_t* get_item_id_4() const { return ___item_id_4; }
	inline String_t** get_address_of_item_id_4() { return &___item_id_4; }
	inline void set_item_id_4(String_t* value)
	{
		___item_id_4 = value;
		Il2CppCodeGenWriteBarrier(&___item_id_4, value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(AdvancedTeleportContentManager_t2480286303, ___owner_5)); }
	inline Il2CppObject * get_owner_5() const { return ___owner_5; }
	inline Il2CppObject ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(Il2CppObject * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
