﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizedBuildingName
struct LocalizedBuildingName_t3447391150;

#include "codegen/il2cpp-codegen.h"

// System.Void LocalizedBuildingName::.ctor()
extern "C"  void LocalizedBuildingName__ctor_m2686816873 (LocalizedBuildingName_t3447391150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizedBuildingName::OnEnable()
extern "C"  void LocalizedBuildingName_OnEnable_m3388854601 (LocalizedBuildingName_t3447391150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
