﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3190641603MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2503180409(__this, ___host0, method) ((  void (*) (Enumerator_t1218393441 *, Dictionary_2_t2823857299 *, const MethodInfo*))Enumerator__ctor_m3766187165_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2724015524(__this, method) ((  Il2CppObject * (*) (Enumerator_t1218393441 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m254358010_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m866669946(__this, method) ((  void (*) (Enumerator_t1218393441 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3092560164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m2572792673(__this, method) ((  void (*) (Enumerator_t1218393441 *, const MethodInfo*))Enumerator_Dispose_m2438322117_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m1789410423(__this, method) ((  bool (*) (Enumerator_t1218393441 *, const MethodInfo*))Enumerator_MoveNext_m3431020440_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m240634275(__this, method) ((  String_t* (*) (Enumerator_t1218393441 *, const MethodInfo*))Enumerator_get_Current_m2489864552_gshared)(__this, method)
