﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldMapCoordsChanger
struct WorldMapCoordsChanger_t1297297106;

#include "codegen/il2cpp-codegen.h"

// System.Void WorldMapCoordsChanger::.ctor()
extern "C"  void WorldMapCoordsChanger__ctor_m2773376439 (WorldMapCoordsChanger_t1297297106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldMapCoordsChanger::OnEnable()
extern "C"  void WorldMapCoordsChanger_OnEnable_m2752070755 (WorldMapCoordsChanger_t1297297106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldMapCoordsChanger::GoToCoords()
extern "C"  void WorldMapCoordsChanger_GoToCoords_m2513434656 (WorldMapCoordsChanger_t1297297106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldMapCoordsChanger::GoHome()
extern "C"  void WorldMapCoordsChanger_GoHome_m80857704 (WorldMapCoordsChanger_t1297297106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
