﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m973578783(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3162513898 *, Dictionary_2_t164486759 *, const MethodInfo*))ValueCollection__ctor_m2077882560_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2343920417(__this, ___item0, method) ((  void (*) (ValueCollection_t3162513898 *, WorldFieldModel_t3469935653 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4134876666(__this, method) ((  void (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m995227355(__this, ___item0, method) ((  bool (*) (ValueCollection_t3162513898 *, WorldFieldModel_t3469935653 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4058548678_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2265385928(__this, ___item0, method) ((  bool (*) (ValueCollection_t3162513898 *, WorldFieldModel_t3469935653 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2561266088(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1223126429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3283001022(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3162513898 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3768245709_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3335664473(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m674376046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m416793628(__this, method) ((  bool (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3628342391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m493153550(__this, method) ((  bool (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4219624793_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3192256808(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1629432690(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3162513898 *, WorldFieldModelU5BU5D_t2403726760*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1607943379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3598422641(__this, method) ((  Enumerator_t1851019523  (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_GetEnumerator_m1386936904_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,WorldFieldModel>::get_Count()
#define ValueCollection_get_Count_m2946106354(__this, method) ((  int32_t (*) (ValueCollection_t3162513898 *, const MethodInfo*))ValueCollection_get_Count_m2322833661_gshared)(__this, method)
