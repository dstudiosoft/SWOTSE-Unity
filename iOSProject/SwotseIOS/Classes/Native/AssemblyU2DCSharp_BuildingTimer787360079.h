﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BuildingModel
struct BuildingModel_t2830632387;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingTimer
struct  BuildingTimer_t787360079  : public Il2CppObject
{
public:
	// System.Int64 BuildingTimer::id
	int64_t ___id_0;
	// BuildingModel BuildingTimer::building
	BuildingModel_t2830632387 * ___building_1;
	// System.String BuildingTimer::status
	String_t* ___status_2;
	// System.Int64 BuildingTimer::event_id
	int64_t ___event_id_3;
	// System.DateTime BuildingTimer::start_time
	DateTime_t693205669  ___start_time_4;
	// System.DateTime BuildingTimer::finish_time
	DateTime_t693205669  ___finish_time_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_building_1() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___building_1)); }
	inline BuildingModel_t2830632387 * get_building_1() const { return ___building_1; }
	inline BuildingModel_t2830632387 ** get_address_of_building_1() { return &___building_1; }
	inline void set_building_1(BuildingModel_t2830632387 * value)
	{
		___building_1 = value;
		Il2CppCodeGenWriteBarrier(&___building_1, value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier(&___status_2, value);
	}

	inline static int32_t get_offset_of_event_id_3() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___event_id_3)); }
	inline int64_t get_event_id_3() const { return ___event_id_3; }
	inline int64_t* get_address_of_event_id_3() { return &___event_id_3; }
	inline void set_event_id_3(int64_t value)
	{
		___event_id_3 = value;
	}

	inline static int32_t get_offset_of_start_time_4() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___start_time_4)); }
	inline DateTime_t693205669  get_start_time_4() const { return ___start_time_4; }
	inline DateTime_t693205669 * get_address_of_start_time_4() { return &___start_time_4; }
	inline void set_start_time_4(DateTime_t693205669  value)
	{
		___start_time_4 = value;
	}

	inline static int32_t get_offset_of_finish_time_5() { return static_cast<int32_t>(offsetof(BuildingTimer_t787360079, ___finish_time_5)); }
	inline DateTime_t693205669  get_finish_time_5() const { return ___finish_time_5; }
	inline DateTime_t693205669 * get_address_of_finish_time_5() { return &___finish_time_5; }
	inline void set_finish_time_5(DateTime_t693205669  value)
	{
		___finish_time_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
