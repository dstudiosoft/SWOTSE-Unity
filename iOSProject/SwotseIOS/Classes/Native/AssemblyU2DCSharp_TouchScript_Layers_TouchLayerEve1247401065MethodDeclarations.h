﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.TouchLayerEventArgs
struct TouchLayerEventArgs_t1247401065;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Layers.TouchLayerEventArgs::.ctor(TouchScript.TouchPoint)
extern "C"  void TouchLayerEventArgs__ctor_m1775873165 (TouchLayerEventArgs_t1247401065 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.Layers.TouchLayerEventArgs::get_Touch()
extern "C"  TouchPoint_t959629083 * TouchLayerEventArgs_get_Touch_m1608093250 (TouchLayerEventArgs_t1247401065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayerEventArgs::set_Touch(TouchScript.TouchPoint)
extern "C"  void TouchLayerEventArgs_set_Touch_m3262620405 (TouchLayerEventArgs_t1247401065 * __this, TouchPoint_t959629083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
