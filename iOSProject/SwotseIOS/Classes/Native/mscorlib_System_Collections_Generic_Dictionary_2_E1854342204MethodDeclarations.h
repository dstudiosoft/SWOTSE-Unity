﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1735299387(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1854342204 *, Dictionary_2_t534317502 *, const MethodInfo*))Enumerator__ctor_m3045959731_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1479585004(__this, method) ((  Il2CppObject * (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m524777314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1003565618(__this, method) ((  void (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3684036852_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4002513125(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2078300018(__this, method) ((  Il2CppObject * (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m181923960(__this, method) ((  Il2CppObject * (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::MoveNext()
#define Enumerator_MoveNext_m3167394562(__this, method) ((  bool (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_MoveNext_m3139459564_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::get_Current()
#define Enumerator_get_Current_m1711686954(__this, method) ((  KeyValuePair_2_t2586630020  (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_get_Current_m3988231868_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1899610599(__this, method) ((  int64_t (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_get_CurrentKey_m4167196999_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2945299719(__this, method) ((  ItemTimer_t3839766396 * (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_get_CurrentValue_m659195263_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::Reset()
#define Enumerator_Reset_m2026923621(__this, method) ((  void (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_Reset_m598725905_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::VerifyState()
#define Enumerator_VerifyState_m2954488530(__this, method) ((  void (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_VerifyState_m3803534940_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m998731546(__this, method) ((  void (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_VerifyCurrent_m1011807396_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,ItemTimer>::Dispose()
#define Enumerator_Dispose_m1545123975(__this, method) ((  void (*) (Enumerator_t1854342204 *, const MethodInfo*))Enumerator_Dispose_m3446908287_gshared)(__this, method)
