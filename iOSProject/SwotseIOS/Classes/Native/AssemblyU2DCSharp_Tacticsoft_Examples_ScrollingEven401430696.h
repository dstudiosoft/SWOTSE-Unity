﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.TableView
struct TableView_t3179510217;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.ScrollingEventsHandler
struct  ScrollingEventsHandler_t401430696  : public MonoBehaviour_t1158329972
{
public:
	// Tacticsoft.TableView Tacticsoft.Examples.ScrollingEventsHandler::m_tableView
	TableView_t3179510217 * ___m_tableView_2;

public:
	inline static int32_t get_offset_of_m_tableView_2() { return static_cast<int32_t>(offsetof(ScrollingEventsHandler_t401430696, ___m_tableView_2)); }
	inline TableView_t3179510217 * get_m_tableView_2() const { return ___m_tableView_2; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_2() { return &___m_tableView_2; }
	inline void set_m_tableView_2(TableView_t3179510217 * value)
	{
		___m_tableView_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
