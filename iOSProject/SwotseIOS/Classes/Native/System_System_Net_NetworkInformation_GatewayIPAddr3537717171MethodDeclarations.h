﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.GatewayIPAddressInformationCollection
struct GatewayIPAddressInformationCollection_t3537717171;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Net.NetworkInformation.GatewayIPAddressInformation
struct GatewayIPAddressInformation_t153474369;
// System.Net.NetworkInformation.GatewayIPAddressInformation[]
struct GatewayIPAddressInformationU5BU5D_t566923420;
// System.Collections.Generic.IEnumerator`1<System.Net.NetworkInformation.GatewayIPAddressInformation>
struct IEnumerator_1_t1923965492;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_GatewayIPAddre153474369.h"

// System.Void System.Net.NetworkInformation.GatewayIPAddressInformationCollection::.ctor()
extern "C"  void GatewayIPAddressInformationCollection__ctor_m3427578889 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.NetworkInformation.GatewayIPAddressInformationCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * GatewayIPAddressInformationCollection_System_Collections_IEnumerable_GetEnumerator_m1026141548 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.GatewayIPAddressInformationCollection::Add(System.Net.NetworkInformation.GatewayIPAddressInformation)
extern "C"  void GatewayIPAddressInformationCollection_Add_m4139981625 (GatewayIPAddressInformationCollection_t3537717171 * __this, GatewayIPAddressInformation_t153474369 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.GatewayIPAddressInformationCollection::Clear()
extern "C"  void GatewayIPAddressInformationCollection_Clear_m3343388818 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.GatewayIPAddressInformationCollection::Contains(System.Net.NetworkInformation.GatewayIPAddressInformation)
extern "C"  bool GatewayIPAddressInformationCollection_Contains_m281701603 (GatewayIPAddressInformationCollection_t3537717171 * __this, GatewayIPAddressInformation_t153474369 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.GatewayIPAddressInformationCollection::CopyTo(System.Net.NetworkInformation.GatewayIPAddressInformation[],System.Int32)
extern "C"  void GatewayIPAddressInformationCollection_CopyTo_m2660026517 (GatewayIPAddressInformationCollection_t3537717171 * __this, GatewayIPAddressInformationU5BU5D_t566923420* ___array0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Net.NetworkInformation.GatewayIPAddressInformation> System.Net.NetworkInformation.GatewayIPAddressInformationCollection::GetEnumerator()
extern "C"  Il2CppObject* GatewayIPAddressInformationCollection_GetEnumerator_m2692370582 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.GatewayIPAddressInformationCollection::Remove(System.Net.NetworkInformation.GatewayIPAddressInformation)
extern "C"  bool GatewayIPAddressInformationCollection_Remove_m1260476652 (GatewayIPAddressInformationCollection_t3537717171 * __this, GatewayIPAddressInformation_t153474369 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.GatewayIPAddressInformationCollection::get_Count()
extern "C"  int32_t GatewayIPAddressInformationCollection_get_Count_m2970754853 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.GatewayIPAddressInformationCollection::get_IsReadOnly()
extern "C"  bool GatewayIPAddressInformationCollection_get_IsReadOnly_m2392920080 (GatewayIPAddressInformationCollection_t3537717171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.GatewayIPAddressInformation System.Net.NetworkInformation.GatewayIPAddressInformationCollection::get_Item(System.Int32)
extern "C"  GatewayIPAddressInformation_t153474369 * GatewayIPAddressInformationCollection_get_Item_m4017663050 (GatewayIPAddressInformationCollection_t3537717171 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
