﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18
struct __StaticArrayInitTypeSizeU3D18_t3819911766;
struct __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_pinvoke;
struct __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D18_t3819911766;
struct __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_pinvoke;

extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D18_t3819911766& unmarshaled, __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_pinvoke& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D18_t3819911766& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D18_t3819911766;
struct __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_com;

extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_com(const __StaticArrayInitTypeSizeU3D18_t3819911766& unmarshaled, __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_com& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_com_back(const __StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D18_t3819911766& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D18_t3819911766_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D18_t3819911766_marshaled_com& marshaled);
