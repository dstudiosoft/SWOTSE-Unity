﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceMembersPermissionsLine
struct AllianceMembersPermissionsLine_t275924858;
// Tacticsoft.TableView
struct TableView_t3179510217;
// UserModel[]
struct UserModelU5BU5D_t2754079553;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceMembersPermisionsTableController
struct  AllianceMembersPermisionsTableController_t2920559745  : public MonoBehaviour_t1158329972
{
public:
	// AllianceMembersPermissionsLine AllianceMembersPermisionsTableController::m_cellPrefab
	AllianceMembersPermissionsLine_t275924858 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceMembersPermisionsTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// UserModel[] AllianceMembersPermisionsTableController::allianceMembers
	UserModelU5BU5D_t2754079553* ___allianceMembers_4;
	// System.Int32 AllianceMembersPermisionsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t2920559745, ___m_cellPrefab_2)); }
	inline AllianceMembersPermissionsLine_t275924858 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceMembersPermissionsLine_t275924858 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceMembersPermissionsLine_t275924858 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t2920559745, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_allianceMembers_4() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t2920559745, ___allianceMembers_4)); }
	inline UserModelU5BU5D_t2754079553* get_allianceMembers_4() const { return ___allianceMembers_4; }
	inline UserModelU5BU5D_t2754079553** get_address_of_allianceMembers_4() { return &___allianceMembers_4; }
	inline void set_allianceMembers_4(UserModelU5BU5D_t2754079553* value)
	{
		___allianceMembers_4 = value;
		Il2CppCodeGenWriteBarrier(&___allianceMembers_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t2920559745, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
