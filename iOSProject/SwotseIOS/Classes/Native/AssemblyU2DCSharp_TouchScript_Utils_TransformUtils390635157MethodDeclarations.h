﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalPosition(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  TransformUtils_GlobalToLocalPosition_m3147492685 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, Vector3_t2243707580  ___global1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalDirection(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  TransformUtils_GlobalToLocalDirection_m1768551245 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, Vector3_t2243707580  ___global1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalVector(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  TransformUtils_GlobalToLocalVector_m2139235201 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, Vector3_t2243707580  ___global1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
