﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.SendPacketsElement
struct SendPacketsElement_t622643137;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.Byte[])
extern "C"  void SendPacketsElement__ctor_m35672532 (SendPacketsElement_t622643137 * __this, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.Byte[],System.Int32,System.Int32)
extern "C"  void SendPacketsElement__ctor_m1868772446 (SendPacketsElement_t622643137 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  void SendPacketsElement__ctor_m1241091353 (SendPacketsElement_t622643137 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, bool ___endOfPacket3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.String)
extern "C"  void SendPacketsElement__ctor_m3587647363 (SendPacketsElement_t622643137 * __this, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.String,System.Int32,System.Int32)
extern "C"  void SendPacketsElement__ctor_m1000733903 (SendPacketsElement_t622643137 * __this, String_t* ___filepath0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::.ctor(System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  void SendPacketsElement__ctor_m4241482142 (SendPacketsElement_t622643137 * __this, String_t* ___filepath0, int32_t ___offset1, int32_t ___count2, bool ___endOfPacket3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.SendPacketsElement::get_Buffer()
extern "C"  ByteU5BU5D_t3397334013* SendPacketsElement_get_Buffer_m2514563894 (SendPacketsElement_t622643137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::set_Buffer(System.Byte[])
extern "C"  void SendPacketsElement_set_Buffer_m1735245095 (SendPacketsElement_t622643137 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.SendPacketsElement::get_Count()
extern "C"  int32_t SendPacketsElement_get_Count_m3136557189 (SendPacketsElement_t622643137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::set_Count(System.Int32)
extern "C"  void SendPacketsElement_set_Count_m1666418832 (SendPacketsElement_t622643137 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.SendPacketsElement::get_EndOfPacket()
extern "C"  bool SendPacketsElement_get_EndOfPacket_m1260838658 (SendPacketsElement_t622643137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::set_EndOfPacket(System.Boolean)
extern "C"  void SendPacketsElement_set_EndOfPacket_m3718304257 (SendPacketsElement_t622643137 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.Sockets.SendPacketsElement::get_FilePath()
extern "C"  String_t* SendPacketsElement_get_FilePath_m2709064502 (SendPacketsElement_t622643137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::set_FilePath(System.String)
extern "C"  void SendPacketsElement_set_FilePath_m3587623553 (SendPacketsElement_t622643137 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.SendPacketsElement::get_Offset()
extern "C"  int32_t SendPacketsElement_get_Offset_m3598589083 (SendPacketsElement_t622643137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SendPacketsElement::set_Offset(System.Int32)
extern "C"  void SendPacketsElement_set_Offset_m2714905358 (SendPacketsElement_t622643137 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
