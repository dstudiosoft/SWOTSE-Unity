﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CityManager
struct CityManager_t1148847038;
// BattleManager
struct BattleManager_t2697593283;
// LevelLoadManager
struct LevelLoadManager_t1376428509;
// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel>
struct Dictionary_2_t166462515;
// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel>
struct Dictionary_2_t3722048895;
// System.Collections.Generic.List`1<TutorialPageModel>
struct List_1_t1877365438;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public Il2CppObject
{
public:
	// CityManager GameManager::cityManager
	CityManager_t1148847038 * ___cityManager_0;
	// BattleManager GameManager::battleManager
	BattleManager_t2697593283 * ___battleManager_1;
	// LevelLoadManager GameManager::levelLoadManager
	LevelLoadManager_t1376428509 * ___levelLoadManager_2;
	// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel> GameManager::buildingConstants
	Dictionary_2_t166462515 * ___buildingConstants_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel> GameManager::unitConstants
	Dictionary_2_t3722048895 * ___unitConstants_4;
	// System.Collections.Generic.List`1<TutorialPageModel> GameManager::tutorials
	List_1_t1877365438 * ___tutorials_5;
	// System.Int64[] GameManager::residenceFields
	Int64U5BU5D_t717125112* ___residenceFields_6;
	// System.Collections.Generic.List`1<System.String> GameManager::buildingIds
	List_1_t1398341365 * ___buildingIds_7;
	// SimpleEvent GameManager::onGetBuildingNames
	SimpleEvent_t1509017202 * ___onGetBuildingNames_8;

public:
	inline static int32_t get_offset_of_cityManager_0() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___cityManager_0)); }
	inline CityManager_t1148847038 * get_cityManager_0() const { return ___cityManager_0; }
	inline CityManager_t1148847038 ** get_address_of_cityManager_0() { return &___cityManager_0; }
	inline void set_cityManager_0(CityManager_t1148847038 * value)
	{
		___cityManager_0 = value;
		Il2CppCodeGenWriteBarrier(&___cityManager_0, value);
	}

	inline static int32_t get_offset_of_battleManager_1() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___battleManager_1)); }
	inline BattleManager_t2697593283 * get_battleManager_1() const { return ___battleManager_1; }
	inline BattleManager_t2697593283 ** get_address_of_battleManager_1() { return &___battleManager_1; }
	inline void set_battleManager_1(BattleManager_t2697593283 * value)
	{
		___battleManager_1 = value;
		Il2CppCodeGenWriteBarrier(&___battleManager_1, value);
	}

	inline static int32_t get_offset_of_levelLoadManager_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___levelLoadManager_2)); }
	inline LevelLoadManager_t1376428509 * get_levelLoadManager_2() const { return ___levelLoadManager_2; }
	inline LevelLoadManager_t1376428509 ** get_address_of_levelLoadManager_2() { return &___levelLoadManager_2; }
	inline void set_levelLoadManager_2(LevelLoadManager_t1376428509 * value)
	{
		___levelLoadManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___levelLoadManager_2, value);
	}

	inline static int32_t get_offset_of_buildingConstants_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___buildingConstants_3)); }
	inline Dictionary_2_t166462515 * get_buildingConstants_3() const { return ___buildingConstants_3; }
	inline Dictionary_2_t166462515 ** get_address_of_buildingConstants_3() { return &___buildingConstants_3; }
	inline void set_buildingConstants_3(Dictionary_2_t166462515 * value)
	{
		___buildingConstants_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildingConstants_3, value);
	}

	inline static int32_t get_offset_of_unitConstants_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___unitConstants_4)); }
	inline Dictionary_2_t3722048895 * get_unitConstants_4() const { return ___unitConstants_4; }
	inline Dictionary_2_t3722048895 ** get_address_of_unitConstants_4() { return &___unitConstants_4; }
	inline void set_unitConstants_4(Dictionary_2_t3722048895 * value)
	{
		___unitConstants_4 = value;
		Il2CppCodeGenWriteBarrier(&___unitConstants_4, value);
	}

	inline static int32_t get_offset_of_tutorials_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___tutorials_5)); }
	inline List_1_t1877365438 * get_tutorials_5() const { return ___tutorials_5; }
	inline List_1_t1877365438 ** get_address_of_tutorials_5() { return &___tutorials_5; }
	inline void set_tutorials_5(List_1_t1877365438 * value)
	{
		___tutorials_5 = value;
		Il2CppCodeGenWriteBarrier(&___tutorials_5, value);
	}

	inline static int32_t get_offset_of_residenceFields_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___residenceFields_6)); }
	inline Int64U5BU5D_t717125112* get_residenceFields_6() const { return ___residenceFields_6; }
	inline Int64U5BU5D_t717125112** get_address_of_residenceFields_6() { return &___residenceFields_6; }
	inline void set_residenceFields_6(Int64U5BU5D_t717125112* value)
	{
		___residenceFields_6 = value;
		Il2CppCodeGenWriteBarrier(&___residenceFields_6, value);
	}

	inline static int32_t get_offset_of_buildingIds_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___buildingIds_7)); }
	inline List_1_t1398341365 * get_buildingIds_7() const { return ___buildingIds_7; }
	inline List_1_t1398341365 ** get_address_of_buildingIds_7() { return &___buildingIds_7; }
	inline void set_buildingIds_7(List_1_t1398341365 * value)
	{
		___buildingIds_7 = value;
		Il2CppCodeGenWriteBarrier(&___buildingIds_7, value);
	}

	inline static int32_t get_offset_of_onGetBuildingNames_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___onGetBuildingNames_8)); }
	inline SimpleEvent_t1509017202 * get_onGetBuildingNames_8() const { return ___onGetBuildingNames_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetBuildingNames_8() { return &___onGetBuildingNames_8; }
	inline void set_onGetBuildingNames_8(SimpleEvent_t1509017202 * value)
	{
		___onGetBuildingNames_8 = value;
		Il2CppCodeGenWriteBarrier(&___onGetBuildingNames_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
