﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageChangerContentManager
struct LanguageChangerContentManager_t3463970044;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LanguageChangerContentManager::.ctor()
extern "C"  void LanguageChangerContentManager__ctor_m884535227 (LanguageChangerContentManager_t3463970044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageChangerContentManager::Start()
extern "C"  void LanguageChangerContentManager_Start_m1676823191 (LanguageChangerContentManager_t3463970044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageChangerContentManager::Close()
extern "C"  void LanguageChangerContentManager_Close_m3669279821 (LanguageChangerContentManager_t3463970044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageChangerContentManager::Save()
extern "C"  void LanguageChangerContentManager_Save_m3465558286 (LanguageChangerContentManager_t3463970044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LanguageChangerContentManager::LanguageChanged(System.Object,System.String)
extern "C"  void LanguageChangerContentManager_LanguageChanged_m1065889847 (LanguageChangerContentManager_t3463970044 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
