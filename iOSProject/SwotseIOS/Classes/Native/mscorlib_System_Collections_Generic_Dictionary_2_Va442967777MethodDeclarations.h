﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct ValueCollection_t442967777;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Collections.Generic.IEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct IEnumerator_1_t207606126;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// TouchScript.InputSources.InputHandlers.TouchHandler/TouchState[]
struct TouchStateU5BU5D_t2121689914;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3426440698.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m30636740_gshared (ValueCollection_t442967777 * __this, Dictionary_2_t1739907934 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m30636740(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t442967777 *, Dictionary_2_t1739907934 *, const MethodInfo*))ValueCollection__ctor_m30636740_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3998681054_gshared (ValueCollection_t442967777 * __this, TouchState_t2732082299  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3998681054(__this, ___item0, method) ((  void (*) (ValueCollection_t442967777 *, TouchState_t2732082299 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3998681054_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m104385363_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m104385363(__this, method) ((  void (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m104385363_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1453991234_gshared (ValueCollection_t442967777 * __this, TouchState_t2732082299  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1453991234(__this, ___item0, method) ((  bool (*) (ValueCollection_t442967777 *, TouchState_t2732082299 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1453991234_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2341600899_gshared (ValueCollection_t442967777 * __this, TouchState_t2732082299  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2341600899(__this, ___item0, method) ((  bool (*) (ValueCollection_t442967777 *, TouchState_t2732082299 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2341600899_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3821674441_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3821674441(__this, method) ((  Il2CppObject* (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3821674441_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3007786405_gshared (ValueCollection_t442967777 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3007786405(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t442967777 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3007786405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2050779590_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2050779590(__this, method) ((  Il2CppObject * (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2050779590_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1500267055_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1500267055(__this, method) ((  bool (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1500267055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3596415145_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3596415145(__this, method) ((  bool (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3596415145_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m858771269_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m858771269(__this, method) ((  Il2CppObject * (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m858771269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2817741923_gshared (ValueCollection_t442967777 * __this, TouchStateU5BU5D_t2121689914* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2817741923(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t442967777 *, TouchStateU5BU5D_t2121689914*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2817741923_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::GetEnumerator()
extern "C"  Enumerator_t3426440698  ValueCollection_GetEnumerator_m2679777982_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2679777982(__this, method) ((  Enumerator_t3426440698  (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_GetEnumerator_m2679777982_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3618960097_gshared (ValueCollection_t442967777 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3618960097(__this, method) ((  int32_t (*) (ValueCollection_t442967777 *, const MethodInfo*))ValueCollection_get_Count_m3618960097_gshared)(__this, method)
