﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppUtilPINVOKE
struct AppUtilPINVOKE_t3515465473;
// System.String
struct String_t;
// Firebase.FutureString/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t1819656562;
// Firebase.FutureVoid/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t2903358321;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t1988210674;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562.h"
#include "Firebase_App_Firebase_FutureVoid_SWIG_CompletionDe2903358321.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"

// System.Void Firebase.AppUtilPINVOKE::.cctor()
extern "C"  void AppUtilPINVOKE__cctor_m1477430606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::.ctor()
extern "C"  void AppUtilPINVOKE__ctor_m3413015119 (AppUtilPINVOKE_t3515465473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_FutureBase__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_FutureBase__SWIG_0_m309569306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureBase_m3328440028 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_FutureBase__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_FutureBase__SWIG_1_m2975860302 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_Release(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_FutureBase_Release_m962074574 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_status_m3496292823 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_error(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_error_m610151147 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_error_message(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureBase_error_message_m3856324900 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_StringStringMap__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_StringStringMap__SWIG_0_m3424339566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_StringStringMap__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_StringStringMap__SWIG_1_m3447066002 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_size(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_StringStringMap_size_m97296679 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_empty(System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_StringStringMap_empty_m3034927656 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_Clear(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringStringMap_Clear_m4139249338 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_getitem(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_StringStringMap_getitem_m754196089 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_setitem(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringStringMap_setitem_m3116937892 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, String_t* ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_ContainsKey(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool AppUtilPINVOKE_Firebase_App_StringStringMap_ContainsKey_m30977959 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_Add(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringStringMap_Add_m1633187708 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, String_t* ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool AppUtilPINVOKE_Firebase_App_StringStringMap_Remove_m3071612029 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_create_iterator_begin(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_StringStringMap_create_iterator_begin_m4015246997 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_get_next_key(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_StringStringMap_get_next_key_m1430457928 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringStringMap_destroy_iterator(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringStringMap_destroy_iterator_m3060002368 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_StringStringMap(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_StringStringMap_m4058594632 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_Clear(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_Clear_m2072611619 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_Add(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_Add_m1715401657 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_StringList_size(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_StringList_size_m1615904234 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_StringList_capacity(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_StringList_capacity_m1834754521 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_reserve(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_reserve_m1913176480 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_StringList__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_StringList__SWIG_0_m755285331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_StringList__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_StringList__SWIG_1_m1783933635 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_StringList__SWIG_2(System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_StringList__SWIG_2_m4002458274 (Il2CppObject * __this /* static, unused */, int32_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_StringList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_StringList_getitemcopy_m890752350 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_StringList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_StringList_getitem_m1858440609 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_setitem_m2373045882 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, String_t* ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_AddRange(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_AddRange_m3599209449 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_StringList_GetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_StringList_GetRange_m759123376 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_Insert_m266782736 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, String_t* ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_InsertRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_InsertRange_m384170618 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_RemoveAt_m3623279812 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_RemoveRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_RemoveRange_m1760671111 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_StringList_Repeat(System.String,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_StringList_Repeat_m1517919358 (Il2CppObject * __this /* static, unused */, String_t* ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_Reverse__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_Reverse__SWIG_0_m1771281639 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_Reverse__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_Reverse__SWIG_1_m1827275900 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_StringList_SetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_StringList_SetRange_m166359459 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_StringList_Contains(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool AppUtilPINVOKE_Firebase_App_StringList_Contains_m2654274855 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_StringList_IndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_StringList_IndexOf_m3478531965 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_StringList_LastIndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_StringList_LastIndexOf_m3941481987 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_StringList_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool AppUtilPINVOKE_Firebase_App_StringList_Remove_m497419678 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_StringList(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_StringList_m3021541427 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Clear(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_Clear_m1588067309 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Add(System.Runtime.InteropServices.HandleRef,System.Byte)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_Add_m1164017812 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint8_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_CharVector_size(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_CharVector_size_m2063551848 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_CharVector_capacity(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_CharVector_capacity_m952151935 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_reserve(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_reserve_m1048236102 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_CharVector__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_CharVector__SWIG_0_m586296405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_CharVector__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_CharVector__SWIG_1_m1851587673 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_CharVector__SWIG_2(System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_CharVector__SWIG_2_m2964448012 (Il2CppObject * __this /* static, unused */, int32_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.AppUtilPINVOKE::Firebase_App_CharVector_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  uint8_t AppUtilPINVOKE_Firebase_App_CharVector_getitemcopy_m510535225 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.AppUtilPINVOKE::Firebase_App_CharVector_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  uint8_t AppUtilPINVOKE_Firebase_App_CharVector_getitem_m2188563116 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Byte)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_setitem_m3483348199 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, uint8_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_AddRange(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_AddRange_m2192698235 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_CharVector_GetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_CharVector_GetRange_m1480024094 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Byte)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_Insert_m619861559 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, uint8_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_InsertRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_InsertRange_m2234579980 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_RemoveAt_m1908593826 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_RemoveRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_RemoveRange_m171678449 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Repeat(System.Byte,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_CharVector_Repeat_m3150662991 (Il2CppObject * __this /* static, unused */, uint8_t ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Reverse__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_Reverse__SWIG_0_m1329194241 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Reverse__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_Reverse__SWIG_1_m1856120894 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_CharVector_SetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_CharVector_SetRange_m1458548341 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Contains(System.Runtime.InteropServices.HandleRef,System.Byte)
extern "C"  bool AppUtilPINVOKE_Firebase_App_CharVector_Contains_m4142224576 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint8_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_CharVector_IndexOf(System.Runtime.InteropServices.HandleRef,System.Byte)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_CharVector_IndexOf_m418091012 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint8_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_CharVector_LastIndexOf(System.Runtime.InteropServices.HandleRef,System.Byte)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_CharVector_LastIndexOf_m1578738992 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint8_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_CharVector_Remove(System.Runtime.InteropServices.HandleRef,System.Byte)
extern "C"  bool AppUtilPINVOKE_Firebase_App_CharVector_Remove_m1127658087 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint8_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_CharVector(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_CharVector_m283292013 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_FutureString()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_FutureString_m2251250765 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m2011264479 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, SWIG_CompletionDelegate_t1819656562 * ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_m628326608 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureString_result(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureString_result_m1869108685 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureString(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureString_m298034936 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_FutureVoid()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_FutureVoid_m3307763054 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureVoid/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_OnCompletion_m3321337283 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, SWIG_CompletionDelegate_t2903358321 * ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_FreeCompletionData_m4373647 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureVoid(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureVoid_m1591702333 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_AppOptionsInternal()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_AppOptionsInternal_m4200831839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_SetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_SetDatabaseUrlInternal_m2014973322 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_GetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_GetDatabaseUrlInternal_m3399650429 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_AppId_set(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_AppId_set_m729446322 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_AppId_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_AppId_get_m3575489523 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ApiKey_set(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ApiKey_set_m3590833687 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ApiKey_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ApiKey_get_m1170227514 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_MessageSenderId_set(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_MessageSenderId_set_m2891551389 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_MessageSenderId_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_MessageSenderId_get_m865783326 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_StorageBucket_set(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_StorageBucket_set_m546806575 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_StorageBucket_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_StorageBucket_get_m1354429902 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ProjectId_set(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ProjectId_set_m2912232676 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ProjectId_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ProjectId_get_m1264574243 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_AppOptionsInternal(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_AppOptionsInternal_m1353127832 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FirebaseApp(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_options(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_options_m3987617044 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Name_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m2787922779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_1_m3030381643 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_2(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_2_m3236381846 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_SetLogLevelInternal(System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_FirebaseApp_SetLogLevelInternal_m3963747966 (Il2CppObject * __this /* static, unused */, int32_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_GetLogLevelInternal()
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FirebaseApp_GetLogLevelInternal_m4103818293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_DefaultName_get()
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m4097276820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern "C"  void AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_AppGetLogLevel()
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_AppGetLogLevel_m1131543554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_CheckAndroidDependencies()
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_CheckAndroidDependencies_m804966217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FixAndroidDependencies()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FixAndroidDependencies_m2871668075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_InitializePlayServicesInternal()
extern "C"  void AppUtilPINVOKE_Firebase_App_InitializePlayServicesInternal_m1012218924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_TerminatePlayServicesInternal()
extern "C"  void AppUtilPINVOKE_Firebase_App_TerminatePlayServicesInternal_m4152149419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_VariantVariantMap__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_VariantVariantMap__SWIG_0_m1326177516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_VariantVariantMap__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_VariantVariantMap__SWIG_1_m3163852440 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_size(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_VariantVariantMap_size_m3517583091 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_empty(System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_VariantVariantMap_empty_m894755762 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_Clear(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantVariantMap_Clear_m1481671024 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_getitem(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantVariantMap_getitem_m2815934614 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_setitem(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantVariantMap_setitem_m1144500844 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_ContainsKey(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_VariantVariantMap_ContainsKey_m3006247188 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantVariantMap_Add_m2857276528 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_Remove(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_VariantVariantMap_Remove_m4125607474 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_create_iterator_begin(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantVariantMap_create_iterator_begin_m2630553637 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_get_next_key(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantVariantMap_get_next_key_m2635662974 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantVariantMap_destroy_iterator(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantVariantMap_destroy_iterator_m2267870602 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_VariantVariantMap(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_VariantVariantMap_m2364494434 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Clear(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_Clear_m1209948525 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_Add_m202533524 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_VariantList_size(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_VariantList_size_m574587348 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.AppUtilPINVOKE::Firebase_App_VariantList_capacity(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t AppUtilPINVOKE_Firebase_App_VariantList_capacity_m2920264447 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_reserve(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_reserve_m526664224 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, uint32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_VariantList__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_VariantList__SWIG_0_m1601467585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_VariantList__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_VariantList__SWIG_1_m2261396037 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_new_VariantList__SWIG_2(System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_new_VariantList__SWIG_2_m3658477298 (Il2CppObject * __this /* static, unused */, int32_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantList_getitemcopy_m44487344 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantList_getitem_m14237883 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_setitem_m4204988007 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_AddRange(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_AddRange_m351822567 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, HandleRef_t2419939847  ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantList_GetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantList_GetRange_m456681510 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_Insert_m2479682031 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_InsertRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_InsertRange_m1880034602 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_RemoveAt_m2006087692 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_RemoveRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_RemoveRange_m1766086801 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Repeat(System.Runtime.InteropServices.HandleRef,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_VariantList_Repeat_m3520447095 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Reverse__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_Reverse__SWIG_0_m1094062545 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_Reverse__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_Reverse__SWIG_1_m4163951328 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_VariantList_SetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_VariantList_SetRange_m3823566717 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t ___jarg21, HandleRef_t2419939847  ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_VariantList(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_VariantList_m4176382369 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_Variant_m3147773815 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_Variant_type(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_Variant_type_m4034010862 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_Variant_is_string(System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_Variant_is_string_m1250530722 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_Variant_is_fundamental_type(System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_Variant_is_fundamental_type_m1368902019 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_Variant_AsString(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_Variant_AsString_m3812961664 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_Variant_vector__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_Variant_vector__SWIG_0_m2773084735 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_Variant_map__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_Variant_map__SWIG_0_m2657725666 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Firebase.AppUtilPINVOKE::Firebase_App_Variant_int64_value(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t AppUtilPINVOKE_Firebase_App_Variant_int64_value_m3357617920 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Firebase.AppUtilPINVOKE::Firebase_App_Variant_double_value(System.Runtime.InteropServices.HandleRef)
extern "C"  double AppUtilPINVOKE_Firebase_App_Variant_double_value_m730586634 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_Variant_bool_value(System.Runtime.InteropServices.HandleRef)
extern "C"  bool AppUtilPINVOKE_Firebase_App_Variant_bool_value_m1574088376 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_Variant_string_value(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_Variant_string_value_m2737172810 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_Variant_blob_as_vector(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_Variant_blob_as_vector_m700289815 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIGUpcast(System.IntPtr)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_m1098833757 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIGUpcast(System.IntPtr)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureVoid_SWIGUpcast_m3457194814 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
