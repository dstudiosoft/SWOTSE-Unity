﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4268020328.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m313732903_gshared (InternalEnumerator_1_t4268020328 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m313732903(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m313732903_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351(__this, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m599783274_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m599783274(__this, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_Dispose_m599783274_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4248732811_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4248732811(__this, method) ((  bool (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4248732811_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::get_Current()
extern "C"  GCHandle_t3409268066  InternalEnumerator_1_get_Current_m1012591846_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1012591846(__this, method) ((  GCHandle_t3409268066  (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1012591846_gshared)(__this, method)
