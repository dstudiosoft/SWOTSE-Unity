﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/GetHostEntryIPCallback
struct GetHostEntryIPCallback_t1650989718;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.Dns/GetHostEntryIPCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHostEntryIPCallback__ctor_m2676395598 (GetHostEntryIPCallback_t1650989718 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryIPCallback::Invoke(System.Net.IPAddress)
extern "C"  IPHostEntry_t994738509 * GetHostEntryIPCallback_Invoke_m745740393 (GetHostEntryIPCallback_t1650989718 * __this, IPAddress_t1399971723 * ___hostAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Dns/GetHostEntryIPCallback::BeginInvoke(System.Net.IPAddress,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHostEntryIPCallback_BeginInvoke_m449528058 (GetHostEntryIPCallback_t1650989718 * __this, IPAddress_t1399971723 * ___hostAddress0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryIPCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t994738509 * GetHostEntryIPCallback_EndInvoke_m720010448 (GetHostEntryIPCallback_t1650989718 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
