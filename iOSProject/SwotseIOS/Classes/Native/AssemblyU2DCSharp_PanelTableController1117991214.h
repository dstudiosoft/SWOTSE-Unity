﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PanelTableLine
struct PanelTableLine_t30214712;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTableController
struct  PanelTableController_t1117991214  : public MonoBehaviour_t1158329972
{
public:
	// PanelTableLine PanelTableController::m_cellPrefab
	PanelTableLine_t30214712 * ___m_cellPrefab_2;
	// Tacticsoft.TableView PanelTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.ArrayList PanelTableController::actionList
	ArrayList_t4252133567 * ___actionList_4;
	// System.String PanelTableController::content
	String_t* ___content_5;
	// System.Int32 PanelTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214, ___m_cellPrefab_2)); }
	inline PanelTableLine_t30214712 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline PanelTableLine_t30214712 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(PanelTableLine_t30214712 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_actionList_4() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214, ___actionList_4)); }
	inline ArrayList_t4252133567 * get_actionList_4() const { return ___actionList_4; }
	inline ArrayList_t4252133567 ** get_address_of_actionList_4() { return &___actionList_4; }
	inline void set_actionList_4(ArrayList_t4252133567 * value)
	{
		___actionList_4 = value;
		Il2CppCodeGenWriteBarrier(&___actionList_4, value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214, ___content_5)); }
	inline String_t* get_content_5() const { return ___content_5; }
	inline String_t** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(String_t* value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier(&___content_5, value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

struct PanelTableController_t1117991214_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableController::<>f__switch$map23
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map23_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableController::<>f__switch$map24
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map24_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableController::<>f__switch$map25
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map25_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableController::<>f__switch$map26
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map26_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map23_7() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214_StaticFields, ___U3CU3Ef__switchU24map23_7)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map23_7() const { return ___U3CU3Ef__switchU24map23_7; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map23_7() { return &___U3CU3Ef__switchU24map23_7; }
	inline void set_U3CU3Ef__switchU24map23_7(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map23_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map23_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map24_8() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214_StaticFields, ___U3CU3Ef__switchU24map24_8)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map24_8() const { return ___U3CU3Ef__switchU24map24_8; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map24_8() { return &___U3CU3Ef__switchU24map24_8; }
	inline void set_U3CU3Ef__switchU24map24_8(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map24_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map24_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map25_9() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214_StaticFields, ___U3CU3Ef__switchU24map25_9)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map25_9() const { return ___U3CU3Ef__switchU24map25_9; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map25_9() { return &___U3CU3Ef__switchU24map25_9; }
	inline void set_U3CU3Ef__switchU24map25_9(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map25_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map25_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map26_10() { return static_cast<int32_t>(offsetof(PanelTableController_t1117991214_StaticFields, ___U3CU3Ef__switchU24map26_10)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map26_10() const { return ___U3CU3Ef__switchU24map26_10; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map26_10() { return &___U3CU3Ef__switchU24map26_10; }
	inline void set_U3CU3Ef__switchU24map26_10(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map26_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map26_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
