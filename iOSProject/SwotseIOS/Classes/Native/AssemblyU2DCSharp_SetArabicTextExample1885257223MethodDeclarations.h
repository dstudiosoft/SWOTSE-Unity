﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SetArabicTextExample
struct SetArabicTextExample_t1885257223;

#include "codegen/il2cpp-codegen.h"

// System.Void SetArabicTextExample::.ctor()
extern "C"  void SetArabicTextExample__ctor_m2154946242 (SetArabicTextExample_t1885257223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetArabicTextExample::Start()
extern "C"  void SetArabicTextExample_Start_m832057414 (SetArabicTextExample_t1885257223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetArabicTextExample::Update()
extern "C"  void SetArabicTextExample_Update_m1980512275 (SetArabicTextExample_t1885257223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
