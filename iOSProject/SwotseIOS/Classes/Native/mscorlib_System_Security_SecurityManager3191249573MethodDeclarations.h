﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Security.PermissionSet
struct PermissionSet_t1941658161;
// System.AppDomain
struct AppDomain_t2719102437;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t43919632;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;
// System.Security.Policy.Evidence[]
struct EvidenceU5BU5D_t56349854;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Reflection.MethodBase
struct MethodBase_t904190842;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "mscorlib_System_Security_PermissionSet1941658161.h"
#include "mscorlib_System_AppDomain2719102437.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Security_PolicyLevelType2082293816.h"
#include "mscorlib_System_Security_Policy_Evidence1407710183.h"
#include "mscorlib_System_Security_Policy_PolicyLevel43919632.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityAction4265870207.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void System.Security.SecurityManager::.cctor()
extern "C"  void SecurityManager__cctor_m3929275401 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::get_CheckExecutionRights()
extern "C"  bool SecurityManager_get_CheckExecutionRights_m3149836832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_CheckExecutionRights(System.Boolean)
extern "C"  void SecurityManager_set_CheckExecutionRights_m3494310891 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::get_SecurityEnabled()
extern "C"  bool SecurityManager_get_SecurityEnabled_m51574294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_SecurityEnabled(System.Boolean)
extern "C"  void SecurityManager_set_SecurityEnabled_m1210652295 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::GetZoneAndOrigin(System.Collections.ArrayList&,System.Collections.ArrayList&)
extern "C"  void SecurityManager_GetZoneAndOrigin_m2454382277 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 ** ___zone0, ArrayList_t4252133567 ** ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::IsGranted(System.Security.IPermission)
extern "C"  bool SecurityManager_IsGranted_m1135518222 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___perm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::IsGranted(System.Reflection.Assembly,System.Security.IPermission)
extern "C"  bool SecurityManager_IsGranted_m2438055908 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, Il2CppObject * ___perm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::CheckPermissionSet(System.Reflection.Assembly,System.Security.PermissionSet,System.Boolean)
extern "C"  Il2CppObject * SecurityManager_CheckPermissionSet_m3892417640 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, PermissionSet_t1941658161 * ___ps1, bool ___noncas2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::CheckPermissionSet(System.AppDomain,System.Security.PermissionSet)
extern "C"  Il2CppObject * SecurityManager_CheckPermissionSet_m1512209463 (Il2CppObject * __this /* static, unused */, AppDomain_t2719102437 * ___ad0, PermissionSet_t1941658161 * ___ps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::LoadPolicyLevelFromFile(System.String,System.Security.PolicyLevelType)
extern "C"  PolicyLevel_t43919632 * SecurityManager_LoadPolicyLevelFromFile_m1801882045 (Il2CppObject * __this /* static, unused */, String_t* ___path0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::LoadPolicyLevelFromString(System.String,System.Security.PolicyLevelType)
extern "C"  PolicyLevel_t43919632 * SecurityManager_LoadPolicyLevelFromString_m459852768 (Il2CppObject * __this /* static, unused */, String_t* ___str0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::PolicyHierarchy()
extern "C"  Il2CppObject * SecurityManager_PolicyHierarchy_m1604115995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence)
extern "C"  PermissionSet_t1941658161 * SecurityManager_ResolvePolicy_m1347232993 (Il2CppObject * __this /* static, unused */, Evidence_t1407710183 * ___evidence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence[])
extern "C"  PermissionSet_t1941658161 * SecurityManager_ResolvePolicy_m3982683591 (Il2CppObject * __this /* static, unused */, EvidenceU5BU5D_t56349854* ___evidences0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolveSystemPolicy(System.Security.Policy.Evidence)
extern "C"  PermissionSet_t1941658161 * SecurityManager_ResolveSystemPolicy_m3871461722 (Il2CppObject * __this /* static, unused */, Evidence_t1407710183 * ___evidence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet&)
extern "C"  PermissionSet_t1941658161 * SecurityManager_ResolvePolicy_m1069076191 (Il2CppObject * __this /* static, unused */, Evidence_t1407710183 * ___evidence0, PermissionSet_t1941658161 * ___reqdPset1, PermissionSet_t1941658161 * ___optPset2, PermissionSet_t1941658161 * ___denyPset3, PermissionSet_t1941658161 ** ___denied4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::ResolvePolicyGroups(System.Security.Policy.Evidence)
extern "C"  Il2CppObject * SecurityManager_ResolvePolicyGroups_m561733148 (Il2CppObject * __this /* static, unused */, Evidence_t1407710183 * ___evidence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::SavePolicy()
extern "C"  void SecurityManager_SavePolicy_m856190995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::SavePolicyLevel(System.Security.Policy.PolicyLevel)
extern "C"  void SecurityManager_SavePolicyLevel_m1937987732 (Il2CppObject * __this /* static, unused */, PolicyLevel_t43919632 * ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::get_Hierarchy()
extern "C"  Il2CppObject * SecurityManager_get_Hierarchy_m2985322036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InitializePolicyHierarchy()
extern "C"  void SecurityManager_InitializePolicyHierarchy_m3054073013 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::ResolvePolicyLevel(System.Security.PermissionSet&,System.Security.Policy.PolicyLevel,System.Security.Policy.Evidence)
extern "C"  bool SecurityManager_ResolvePolicyLevel_m3039881993 (Il2CppObject * __this /* static, unused */, PermissionSet_t1941658161 ** ___ps0, PolicyLevel_t43919632 * ___pl1, Evidence_t1407710183 * ___evidence2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ResolveIdentityPermissions(System.Security.PermissionSet,System.Security.Policy.Evidence)
extern "C"  void SecurityManager_ResolveIdentityPermissions_m1481575524 (Il2CppObject * __this /* static, unused */, PermissionSet_t1941658161 * ___ps0, Evidence_t1407710183 * ___evidence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::get_ResolvingPolicyLevel()
extern "C"  PolicyLevel_t43919632 * SecurityManager_get_ResolvingPolicyLevel_m158118460 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_ResolvingPolicyLevel(System.Security.Policy.PolicyLevel)
extern "C"  void SecurityManager_set_ResolvingPolicyLevel_m18996727 (Il2CppObject * __this /* static, unused */, PolicyLevel_t43919632 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::Decode(System.IntPtr,System.Int32)
extern "C"  PermissionSet_t1941658161 * SecurityManager_Decode_m3784014484 (Il2CppObject * __this /* static, unused */, IntPtr_t ___permissions0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::Decode(System.Byte[])
extern "C"  PermissionSet_t1941658161 * SecurityManager_Decode_m470212436 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___encodedPermissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::get_UnmanagedCode()
extern "C"  Il2CppObject * SecurityManager_get_UnmanagedCode_m2519470694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::GetLinkDemandSecurity(System.Reflection.MethodBase,System.Security.RuntimeDeclSecurityActions*,System.Security.RuntimeDeclSecurityActions*)
extern "C"  bool SecurityManager_GetLinkDemandSecurity_m467965909 (Il2CppObject * __this /* static, unused */, MethodBase_t904190842 * ___method0, RuntimeDeclSecurityActions_t4265870207 * ___cdecl1, RuntimeDeclSecurityActions_t4265870207 * ___mdecl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ReflectedLinkDemandInvoke(System.Reflection.MethodBase)
extern "C"  void SecurityManager_ReflectedLinkDemandInvoke_m2864340559 (Il2CppObject * __this /* static, unused */, MethodBase_t904190842 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::ReflectedLinkDemandQuery(System.Reflection.MethodBase)
extern "C"  bool SecurityManager_ReflectedLinkDemandQuery_m2408661465 (Il2CppObject * __this /* static, unused */, MethodBase_t904190842 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemand(System.Reflection.Assembly,System.Security.RuntimeDeclSecurityActions*,System.Security.RuntimeDeclSecurityActions*)
extern "C"  bool SecurityManager_LinkDemand_m4132789037 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, RuntimeDeclSecurityActions_t4265870207 * ___klass1, RuntimeDeclSecurityActions_t4265870207 * ___method2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemandFullTrust(System.Reflection.Assembly)
extern "C"  bool SecurityManager_LinkDemandFullTrust_m769605652 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemandUnmanaged(System.Reflection.Assembly)
extern "C"  bool SecurityManager_LinkDemandUnmanaged_m1314635587 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::LinkDemandSecurityException(System.Int32,System.IntPtr)
extern "C"  void SecurityManager_LinkDemandSecurityException_m2867045087 (Il2CppObject * __this /* static, unused */, int32_t ___securityViolation0, IntPtr_t ___methodHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InheritanceDemandSecurityException(System.Int32,System.Reflection.Assembly,System.Type,System.Reflection.MethodInfo)
extern "C"  void SecurityManager_InheritanceDemandSecurityException_m3571600337 (Il2CppObject * __this /* static, unused */, int32_t ___securityViolation0, Assembly_t4268412390 * ___a1, Type_t * ___t2, MethodInfo_t * ___method3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ThrowException(System.Exception)
extern "C"  void SecurityManager_ThrowException_m545594407 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::InheritanceDemand(System.AppDomain,System.Reflection.Assembly,System.Security.RuntimeDeclSecurityActions*)
extern "C"  bool SecurityManager_InheritanceDemand_m623868243 (Il2CppObject * __this /* static, unused */, AppDomain_t2719102437 * ___ad0, Assembly_t4268412390 * ___a1, RuntimeDeclSecurityActions_t4265870207 * ___actions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::DemandUnmanaged()
extern "C"  void SecurityManager_DemandUnmanaged_m876761327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InternalDemand(System.IntPtr,System.Int32)
extern "C"  void SecurityManager_InternalDemand_m3969153561 (Il2CppObject * __this /* static, unused */, IntPtr_t ___permissions0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InternalDemandChoice(System.IntPtr,System.Int32)
extern "C"  void SecurityManager_InternalDemandChoice_m618498872 (Il2CppObject * __this /* static, unused */, IntPtr_t ___permissions0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
