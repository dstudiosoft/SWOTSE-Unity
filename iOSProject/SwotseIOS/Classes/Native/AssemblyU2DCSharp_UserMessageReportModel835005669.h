﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserModel
struct UserModel_t3025569216;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserMessageReportModel
struct  UserMessageReportModel_t835005669  : public Il2CppObject
{
public:
	// System.Int64 UserMessageReportModel::id
	int64_t ___id_0;
	// UserModel UserMessageReportModel::fromUser
	UserModel_t3025569216 * ___fromUser_1;
	// System.String UserMessageReportModel::subject
	String_t* ___subject_2;
	// System.String UserMessageReportModel::message
	String_t* ___message_3;
	// System.Boolean UserMessageReportModel::isRead
	bool ___isRead_4;
	// System.DateTime UserMessageReportModel::creation_date
	DateTime_t693205669  ___creation_date_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_fromUser_1() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___fromUser_1)); }
	inline UserModel_t3025569216 * get_fromUser_1() const { return ___fromUser_1; }
	inline UserModel_t3025569216 ** get_address_of_fromUser_1() { return &___fromUser_1; }
	inline void set_fromUser_1(UserModel_t3025569216 * value)
	{
		___fromUser_1 = value;
		Il2CppCodeGenWriteBarrier(&___fromUser_1, value);
	}

	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___subject_2)); }
	inline String_t* get_subject_2() const { return ___subject_2; }
	inline String_t** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(String_t* value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier(&___subject_2, value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier(&___message_3, value);
	}

	inline static int32_t get_offset_of_isRead_4() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___isRead_4)); }
	inline bool get_isRead_4() const { return ___isRead_4; }
	inline bool* get_address_of_isRead_4() { return &___isRead_4; }
	inline void set_isRead_4(bool value)
	{
		___isRead_4 = value;
	}

	inline static int32_t get_offset_of_creation_date_5() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t835005669, ___creation_date_5)); }
	inline DateTime_t693205669  get_creation_date_5() const { return ___creation_date_5; }
	inline DateTime_t693205669 * get_address_of_creation_date_5() { return &___creation_date_5; }
	inline void set_creation_date_5(DateTime_t693205669  value)
	{
		___creation_date_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
