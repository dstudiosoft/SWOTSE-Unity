﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.UILayer
struct UILayer_t314035379;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_HitTest_ObjectHi3057876522.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"

// System.Void TouchScript.Layers.UILayer::.ctor()
extern "C"  void UILayer__ctor_m3092509568 (UILayer_t314035379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.UILayer::Hit(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  int32_t UILayer_Hit_m1398887157 (UILayer_t314035379 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.Layers.UILayer::GetProjectionParams(TouchScript.TouchPoint)
extern "C"  ProjectionParams_t2712959773 * UILayer_GetProjectionParams_m3370283882 (UILayer_t314035379 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.UILayer::Awake()
extern "C"  void UILayer_Awake_m680896203 (UILayer_t314035379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchScript.Layers.UILayer::lateAwake()
extern "C"  Il2CppObject * UILayer_lateAwake_m2228265397 (UILayer_t314035379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.UILayer::setName()
extern "C"  void UILayer_setName_m3040064967 (UILayer_t314035379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.HitTest/ObjectHitResult TouchScript.Layers.UILayer::doHit(UnityEngine.EventSystems.RaycastResult,TouchScript.Hit.TouchHit&)
extern "C"  int32_t UILayer_doHit_m1008945913 (UILayer_t314035379 * __this, RaycastResult_t21186376  ___raycastHit0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
