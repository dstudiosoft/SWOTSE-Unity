﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t2752427815;
// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t1319001572;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMess1319001572.h"

// System.Void Firebase.Messaging.MessageReceivedEventArgs::.ctor(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs__ctor_m2304679791 (MessageReceivedEventArgs_t2752427815 * __this, FirebaseMessage_t1319001572 * ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Messaging.FirebaseMessage Firebase.Messaging.MessageReceivedEventArgs::get_Message()
extern "C"  FirebaseMessage_t1319001572 * MessageReceivedEventArgs_get_Message_m839654266 (MessageReceivedEventArgs_t2752427815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.MessageReceivedEventArgs::set_Message(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs_set_Message_m1227283801 (MessageReceivedEventArgs_t2752427815 * __this, FirebaseMessage_t1319001572 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
