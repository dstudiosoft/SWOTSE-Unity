﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate
struct AsyncHandshakeDelegate_t2884244898;
// System.Object
struct Il2CppObject;
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
struct InternalAsyncResult_t1610391122;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1610391122.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncHandshakeDelegate__ctor_m3889379558 (AsyncHandshakeDelegate_t2884244898 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::Invoke(Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult,System.Boolean)
extern "C"  void AsyncHandshakeDelegate_Invoke_m3108641541 (AsyncHandshakeDelegate_t2884244898 * __this, InternalAsyncResult_t1610391122 * ___asyncResult0, bool ___fromWrite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::BeginInvoke(Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AsyncHandshakeDelegate_BeginInvoke_m1677069994 (AsyncHandshakeDelegate_t2884244898 * __this, InternalAsyncResult_t1610391122 * ___asyncResult0, bool ___fromWrite1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void AsyncHandshakeDelegate_EndInvoke_m355003332 (AsyncHandshakeDelegate_t2884244898 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
