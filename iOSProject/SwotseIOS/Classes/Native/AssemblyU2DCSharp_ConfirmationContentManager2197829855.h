﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfirmationContentManager
struct  ConfirmationContentManager_t2197829855  : public MonoBehaviour_t1158329972
{
public:
	// ConfirmationEvent ConfirmationContentManager::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_2;
	// UnityEngine.UI.Text ConfirmationContentManager::confirmationMessage
	Text_t356221433 * ___confirmationMessage_3;

public:
	inline static int32_t get_offset_of_confirmed_2() { return static_cast<int32_t>(offsetof(ConfirmationContentManager_t2197829855, ___confirmed_2)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_2() const { return ___confirmed_2; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_2() { return &___confirmed_2; }
	inline void set_confirmed_2(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_2 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_2, value);
	}

	inline static int32_t get_offset_of_confirmationMessage_3() { return static_cast<int32_t>(offsetof(ConfirmationContentManager_t2197829855, ___confirmationMessage_3)); }
	inline Text_t356221433 * get_confirmationMessage_3() const { return ___confirmationMessage_3; }
	inline Text_t356221433 ** get_address_of_confirmationMessage_3() { return &___confirmationMessage_3; }
	inline void set_confirmationMessage_3(Text_t356221433 * value)
	{
		___confirmationMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___confirmationMessage_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
