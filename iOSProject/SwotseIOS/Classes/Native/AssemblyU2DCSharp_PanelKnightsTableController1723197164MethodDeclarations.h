﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelKnightsTableController
struct PanelKnightsTableController_t1723197164;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PanelKnightsTableController::.ctor()
extern "C"  void PanelKnightsTableController__ctor_m3135734303 (PanelKnightsTableController_t1723197164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightsTableController::OnEnable()
extern "C"  void PanelKnightsTableController_OnEnable_m1421728691 (PanelKnightsTableController_t1723197164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PanelKnightsTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t PanelKnightsTableController_GetNumberOfRowsForTableView_m2307649211 (PanelKnightsTableController_t1723197164 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PanelKnightsTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float PanelKnightsTableController_GetHeightForRowInTableView_m3073361827 (PanelKnightsTableController_t1723197164 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell PanelKnightsTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * PanelKnightsTableController_GetCellForRowInTableView_m4047525570 (PanelKnightsTableController_t1723197164 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightsTableController::ReloadData()
extern "C"  void PanelKnightsTableController_ReloadData_m139092354 (PanelKnightsTableController_t1723197164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightsTableController::GeneralsUpdated(System.Object,System.String)
extern "C"  void PanelKnightsTableController_GeneralsUpdated_m1061915791 (PanelKnightsTableController_t1723197164 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
