﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct TaskCompletionSource_1_t1571883375;
// System.Func`1<System.Object>
struct Func_1_t348874681;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<System.Object>
struct  U3CStartNewU3Ec__AnonStorey0_1_t2110490679  : public Il2CppObject
{
public:
	// System.Threading.Tasks.TaskCompletionSource`1<T> System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1::tcs
	TaskCompletionSource_1_t1571883375 * ___tcs_0;
	// System.Func`1<T> System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1::func
	Func_1_t348874681 * ___func_1;

public:
	inline static int32_t get_offset_of_tcs_0() { return static_cast<int32_t>(offsetof(U3CStartNewU3Ec__AnonStorey0_1_t2110490679, ___tcs_0)); }
	inline TaskCompletionSource_1_t1571883375 * get_tcs_0() const { return ___tcs_0; }
	inline TaskCompletionSource_1_t1571883375 ** get_address_of_tcs_0() { return &___tcs_0; }
	inline void set_tcs_0(TaskCompletionSource_1_t1571883375 * value)
	{
		___tcs_0 = value;
		Il2CppCodeGenWriteBarrier(&___tcs_0, value);
	}

	inline static int32_t get_offset_of_func_1() { return static_cast<int32_t>(offsetof(U3CStartNewU3Ec__AnonStorey0_1_t2110490679, ___func_1)); }
	inline Func_1_t348874681 * get_func_1() const { return ___func_1; }
	inline Func_1_t348874681 ** get_address_of_func_1() { return &___func_1; }
	inline void set_func_1(Func_1_t348874681 * value)
	{
		___func_1 = value;
		Il2CppCodeGenWriteBarrier(&___func_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
