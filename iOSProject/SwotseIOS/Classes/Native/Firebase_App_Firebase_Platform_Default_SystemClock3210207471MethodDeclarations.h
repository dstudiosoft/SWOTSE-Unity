﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.Default.SystemClock
struct SystemClock_t3210207471;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Platform.Default.SystemClock::.ctor()
extern "C"  void SystemClock__ctor_m1243204495 (SystemClock_t3210207471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Default.SystemClock::.cctor()
extern "C"  void SystemClock__cctor_m2613367008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
