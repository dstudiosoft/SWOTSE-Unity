﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArmyTimer
struct ArmyTimer_t470110518;

#include "codegen/il2cpp-codegen.h"

// System.Void ArmyTimer::.ctor()
extern "C"  void ArmyTimer__ctor_m1885245907 (ArmyTimer_t470110518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
