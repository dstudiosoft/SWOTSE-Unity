﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.GenericPrincipal
struct GenericPrincipal_t3710548099;
// System.Security.Principal.IIdentity
struct IIdentity_t2445095625;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.GenericPrincipal::.ctor(System.Security.Principal.IIdentity,System.String[])
extern "C"  void GenericPrincipal__ctor_m1974487220 (GenericPrincipal_t3710548099 * __this, Il2CppObject * ___identity0, StringU5BU5D_t1642385972* ___roles1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
