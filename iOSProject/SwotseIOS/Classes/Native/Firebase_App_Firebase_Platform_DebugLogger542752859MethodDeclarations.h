﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.DebugLogger
struct DebugLogger_t542752859;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Platform.DebugLogger::.ctor()
extern "C"  void DebugLogger__ctor_m4221268538 (DebugLogger_t542752859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::get_Instance()
extern "C"  DebugLogger_t542752859 * DebugLogger_get_Instance_m2545041366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.DebugLogger::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void DebugLogger_LogMessage_m350263290 (DebugLogger_t542752859 * __this, int32_t ___level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.DebugLogger::.cctor()
extern "C"  void DebugLogger__cctor_m290983641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
