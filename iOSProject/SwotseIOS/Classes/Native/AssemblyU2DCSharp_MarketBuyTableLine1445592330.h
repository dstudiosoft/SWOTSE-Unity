﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketBuyTableLine
struct  MarketBuyTableLine_t1445592330  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text MarketBuyTableLine::quantity
	Text_t356221433 * ___quantity_2;
	// UnityEngine.UI.Text MarketBuyTableLine::unitPrice
	Text_t356221433 * ___unitPrice_3;

public:
	inline static int32_t get_offset_of_quantity_2() { return static_cast<int32_t>(offsetof(MarketBuyTableLine_t1445592330, ___quantity_2)); }
	inline Text_t356221433 * get_quantity_2() const { return ___quantity_2; }
	inline Text_t356221433 ** get_address_of_quantity_2() { return &___quantity_2; }
	inline void set_quantity_2(Text_t356221433 * value)
	{
		___quantity_2 = value;
		Il2CppCodeGenWriteBarrier(&___quantity_2, value);
	}

	inline static int32_t get_offset_of_unitPrice_3() { return static_cast<int32_t>(offsetof(MarketBuyTableLine_t1445592330, ___unitPrice_3)); }
	inline Text_t356221433 * get_unitPrice_3() const { return ___unitPrice_3; }
	inline Text_t356221433 ** get_address_of_unitPrice_3() { return &___unitPrice_3; }
	inline void set_unitPrice_3(Text_t356221433 * value)
	{
		___unitPrice_3 = value;
		Il2CppCodeGenWriteBarrier(&___unitPrice_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
