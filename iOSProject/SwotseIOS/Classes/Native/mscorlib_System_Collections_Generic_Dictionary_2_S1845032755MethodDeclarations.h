﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct ShimEnumerator_t1845032755;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1721721448_gshared (ShimEnumerator_t1845032755 * __this, Dictionary_2_t1739907934 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1721721448(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1845032755 *, Dictionary_2_t1739907934 *, const MethodInfo*))ShimEnumerator__ctor_m1721721448_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m15389973_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15389973(__this, method) ((  bool (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_MoveNext_m15389973_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2548943009_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2548943009(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_get_Entry_m2548943009_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2804577678_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2804577678(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_get_Key_m2804577678_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3200467892_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3200467892(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_get_Value_m3200467892_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2106217360_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2106217360(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_get_Current_m2106217360_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Reset()
extern "C"  void ShimEnumerator_Reset_m2192166918_gshared (ShimEnumerator_t1845032755 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2192166918(__this, method) ((  void (*) (ShimEnumerator_t1845032755 *, const MethodInfo*))ShimEnumerator_Reset_m2192166918_gshared)(__this, method)
