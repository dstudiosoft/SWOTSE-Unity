﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor()
#define List_1__ctor_m3131880517(__this, method) ((  void (*) (List_1_t1219472551 *, const MethodInfo*))List_1__ctor_m3040225017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4071045702(__this, ___collection0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor(System.Int32)
#define List_1__ctor_m2024926184(__this, ___capacity0, method) ((  void (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor(T[],System.Int32)
#define List_1__ctor_m1126276356(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1219472551 *, TuioCursorU5BU5D_t885656954*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.cctor()
#define List_1__cctor_m872386256(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m159147001(__this, method) ((  Il2CppObject* (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3122789349(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1219472551 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2128219654(__this, method) ((  Il2CppObject * (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1405424361(__this, ___item0, method) ((  int32_t (*) (List_1_t1219472551 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1161851381(__this, ___item0, method) ((  bool (*) (List_1_t1219472551 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3724366107(__this, ___item0, method) ((  int32_t (*) (List_1_t1219472551 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1799391536(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1219472551 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m4126625306(__this, ___item0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3744090082(__this, method) ((  bool (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2479371457(__this, method) ((  bool (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3803467717(__this, method) ((  Il2CppObject * (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1451747032(__this, method) ((  bool (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1806922445(__this, method) ((  bool (*) (List_1_t1219472551 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m503220444(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3532048331(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1219472551 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Add(T)
#define List_1_Add_m2438782433(__this, ___item0, method) ((  void (*) (List_1_t1219472551 *, TuioCursor_t1850351419 *, const MethodInfo*))List_1_Add_m1194694229_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m584546925(__this, ___newCount0, method) ((  void (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1990025181(__this, ___collection0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2978999709(__this, ___enumerable0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1582440178(__this, ___collection0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::AsReadOnly()
#define List_1_AsReadOnly_m1294935349(__this, method) ((  ReadOnlyCollection_1_t2036137111 * (*) (List_1_t1219472551 *, const MethodInfo*))List_1_AsReadOnly_m1618299177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Clear()
#define List_1_Clear_m2159038286(__this, method) ((  void (*) (List_1_t1219472551 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Contains(T)
#define List_1_Contains_m1976372074(__this, ___item0, method) ((  bool (*) (List_1_t1219472551 *, TuioCursor_t1850351419 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1267755588(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1219472551 *, TuioCursorU5BU5D_t885656954*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Find(System.Predicate`1<T>)
#define List_1_Find_m3747455844(__this, ___match0, method) ((  TuioCursor_t1850351419 * (*) (List_1_t1219472551 *, Predicate_1_t293321534 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m557986957(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t293321534 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m817238693(__this, ___match0, method) ((  List_1_t1219472551 * (*) (List_1_t1219472551 *, Predicate_1_t293321534 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1973650551(__this, ___match0, method) ((  List_1_t1219472551 * (*) (List_1_t1219472551 *, Predicate_1_t293321534 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3950781975(__this, ___match0, method) ((  List_1_t1219472551 * (*) (List_1_t1219472551 *, Predicate_1_t293321534 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m372373324(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1219472551 *, int32_t, int32_t, Predicate_1_t293321534 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3586167559(__this, ___action0, method) ((  void (*) (List_1_t1219472551 *, Action_1_t1652150801 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::GetEnumerator()
#define List_1_GetEnumerator_m1328495141(__this, method) ((  Enumerator_t754202225  (*) (List_1_t1219472551 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::IndexOf(T)
#define List_1_IndexOf_m2772473294(__this, ___item0, method) ((  int32_t (*) (List_1_t1219472551 *, TuioCursor_t1850351419 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m233283505(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1219472551 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2997925208(__this, ___index0, method) ((  void (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Insert(System.Int32,T)
#define List_1_Insert_m2590509883(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1219472551 *, int32_t, TuioCursor_t1850351419 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m806267042(__this, ___collection0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Remove(T)
#define List_1_Remove_m840144815(__this, ___item0, method) ((  bool (*) (List_1_t1219472551 *, TuioCursor_t1850351419 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2503615433(__this, ___match0, method) ((  int32_t (*) (List_1_t1219472551 *, Predicate_1_t293321534 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m46850919(__this, ___index0, method) ((  void (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Reverse()
#define List_1_Reverse_m3075288113(__this, method) ((  void (*) (List_1_t1219472551 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Sort()
#define List_1_Sort_m177106703(__this, method) ((  void (*) (List_1_t1219472551 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m686284011(__this, ___comparer0, method) ((  void (*) (List_1_t1219472551 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m202856424(__this, ___comparison0, method) ((  void (*) (List_1_t1219472551 *, Comparison_1_t3112090270 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::ToArray()
#define List_1_ToArray_m3763185872(__this, method) ((  TuioCursorU5BU5D_t885656954* (*) (List_1_t1219472551 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::TrimExcess()
#define List_1_TrimExcess_m1649695622(__this, method) ((  void (*) (List_1_t1219472551 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Capacity()
#define List_1_get_Capacity_m1393750036(__this, method) ((  int32_t (*) (List_1_t1219472551 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1728061789(__this, ___value0, method) ((  void (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Count()
#define List_1_get_Count_m302198121(__this, method) ((  int32_t (*) (List_1_t1219472551 *, const MethodInfo*))List_1_get_Count_m1549410684_gshared)(__this, method)
// T System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Item(System.Int32)
#define List_1_get_Item_m2568686926(__this, ___index0, method) ((  TuioCursor_t1850351419 * (*) (List_1_t1219472551 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::set_Item(System.Int32,T)
#define List_1_set_Item_m4264565222(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1219472551 *, int32_t, TuioCursor_t1850351419 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
