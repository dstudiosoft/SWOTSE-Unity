﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.StandardInput
struct StandardInput_t4102879489;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.InputSources.StandardInput::.ctor()
extern "C"  void StandardInput__ctor_m1626980516 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::UpdateInput()
extern "C"  void StandardInput_UpdateInput_m3207246367 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  void StandardInput_CancelTouch_m1329266869 (StandardInput_t4102879489 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::OnEnable()
extern "C"  void StandardInput_OnEnable_m2271669852 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::OnDisable()
extern "C"  void StandardInput_OnDisable_m2628675297 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::enableMouse()
extern "C"  void StandardInput_enableMouse_m840621550 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::disableMouse()
extern "C"  void StandardInput_disableMouse_m3592812397 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::enableTouch()
extern "C"  void StandardInput_enableTouch_m3100008682 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.StandardInput::disableTouch()
extern "C"  void StandardInput_disableTouch_m1567561337 (StandardInput_t4102879489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
