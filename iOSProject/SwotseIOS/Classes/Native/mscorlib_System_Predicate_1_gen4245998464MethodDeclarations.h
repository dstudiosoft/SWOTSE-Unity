﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<CityTreasureModel>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2066020566(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t4245998464 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<CityTreasureModel>::Invoke(T)
#define Predicate_1_Invoke_m3690319050(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4245998464 *, CityTreasureModel_t1508061053 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<CityTreasureModel>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1508114845(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t4245998464 *, CityTreasureModel_t1508061053 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<CityTreasureModel>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m769578736(__this, ___result0, method) ((  bool (*) (Predicate_1_t4245998464 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
