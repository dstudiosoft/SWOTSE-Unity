﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginManager
struct LoginManager_t973619992;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void LoginManager::.ctor()
extern "C"  void LoginManager__ctor_m930990491 (LoginManager_t973619992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::add_onGetEmperors(SimpleEvent)
extern "C"  void LoginManager_add_onGetEmperors_m1350050525 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::remove_onGetEmperors(SimpleEvent)
extern "C"  void LoginManager_remove_onGetEmperors_m1206289630 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::add_onRegister(SimpleEvent)
extern "C"  void LoginManager_add_onRegister_m3796150279 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::remove_onRegister(SimpleEvent)
extern "C"  void LoginManager_remove_onRegister_m318373050 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::add_onLogin(SimpleEvent)
extern "C"  void LoginManager_add_onLogin_m782458093 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::remove_onLogin(SimpleEvent)
extern "C"  void LoginManager_remove_onLogin_m1792419538 (LoginManager_t973619992 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::LogOut(System.Boolean)
extern "C"  void LoginManager_LogOut_m67564188 (LoginManager_t973619992 * __this, bool ___relogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginManager::RegisterRequest(System.String,System.String,System.String,System.String,System.String)
extern "C"  Il2CppObject * LoginManager_RegisterRequest_m1621915735 (LoginManager_t973619992 * __this, String_t* ___username0, String_t* ___password1, String_t* ___email2, String_t* ___emperor_name3, String_t* ___language4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginManager::LoginRequest(System.String,System.String)
extern "C"  Il2CppObject * LoginManager_LoginRequest_m3414623899 (LoginManager_t973619992 * __this, String_t* ___username0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> LoginManager::EmperorNames()
extern "C"  List_1_t1398341365 * LoginManager_EmperorNames_m3079900672 (LoginManager_t973619992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::addEmperor(JSONObject)
extern "C"  void LoginManager_addEmperor_m1865259193 (LoginManager_t973619992 * __this, JSONObject_t1971882247 * ___emperor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginManager::GetEmperors()
extern "C"  Il2CppObject * LoginManager_GetEmperors_m830696014 (LoginManager_t973619992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::GotBuildingNamesHandler(System.Object,System.String)
extern "C"  void LoginManager_GotBuildingNamesHandler_m3599548745 (LoginManager_t973619992 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::LoginHandler(System.Object,System.String)
extern "C"  void LoginManager_LoginHandler_m2645511216 (LoginManager_t973619992 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginManager::GotUserInformationHandler(System.Object,System.String)
extern "C"  void LoginManager_GotUserInformationHandler_m1914249372 (LoginManager_t973619992 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginManager::loadMainScene()
extern "C"  Il2CppObject * LoginManager_loadMainScene_m1252319360 (LoginManager_t973619992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
