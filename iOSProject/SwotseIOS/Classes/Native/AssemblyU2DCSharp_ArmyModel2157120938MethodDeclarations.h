﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArmyModel
struct ArmyModel_t2157120938;

#include "codegen/il2cpp-codegen.h"

// System.Void ArmyModel::.ctor()
extern "C"  void ArmyModel__ctor_m1260825043 (ArmyModel_t2157120938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
