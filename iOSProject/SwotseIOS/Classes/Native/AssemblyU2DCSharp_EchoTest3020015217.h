﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest
struct  EchoTest_t3020015217  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text EchoTest::chatWindow
	Text_t356221433 * ___chatWindow_2;
	// UnityEngine.UI.InputField EchoTest::Input
	InputField_t1631627530 * ___Input_3;
	// System.String EchoTest::MessageBuffer
	String_t* ___MessageBuffer_4;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___chatWindow_2)); }
	inline Text_t356221433 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t356221433 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t356221433 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier(&___chatWindow_2, value);
	}

	inline static int32_t get_offset_of_Input_3() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___Input_3)); }
	inline InputField_t1631627530 * get_Input_3() const { return ___Input_3; }
	inline InputField_t1631627530 ** get_address_of_Input_3() { return &___Input_3; }
	inline void set_Input_3(InputField_t1631627530 * value)
	{
		___Input_3 = value;
		Il2CppCodeGenWriteBarrier(&___Input_3, value);
	}

	inline static int32_t get_offset_of_MessageBuffer_4() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___MessageBuffer_4)); }
	inline String_t* get_MessageBuffer_4() const { return ___MessageBuffer_4; }
	inline String_t** get_address_of_MessageBuffer_4() { return &___MessageBuffer_4; }
	inline void set_MessageBuffer_4(String_t* value)
	{
		___MessageBuffer_4 = value;
		Il2CppCodeGenWriteBarrier(&___MessageBuffer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
