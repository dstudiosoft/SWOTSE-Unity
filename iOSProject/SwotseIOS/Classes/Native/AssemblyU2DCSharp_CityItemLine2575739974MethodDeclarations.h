﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityItemLine
struct CityItemLine_t2575739974;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CityItemLine::.ctor()
extern "C"  void CityItemLine__ctor_m2634687835 (CityItemLine_t2575739974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityItemLine::SetCityId(System.Int64)
extern "C"  void CityItemLine_SetCityId_m3385127369 (CityItemLine_t2575739974 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityItemLine::SetCurrent(System.Boolean)
extern "C"  void CityItemLine_SetCurrent_m515416351 (CityItemLine_t2575739974 * __this, bool ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityItemLine::SetCityImage(System.String)
extern "C"  void CityItemLine_SetCityImage_m1856342987 (CityItemLine_t2575739974 * __this, String_t* ___imageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityItemLine::SetCoords(System.Int64,System.Int64)
extern "C"  void CityItemLine_SetCoords_m4102977711 (CityItemLine_t2575739974 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityItemLine::ChangeCity()
extern "C"  void CityItemLine_ChangeCity_m3629709590 (CityItemLine_t2575739974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
