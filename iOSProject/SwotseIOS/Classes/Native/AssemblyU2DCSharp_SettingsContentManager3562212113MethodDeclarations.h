﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsContentManager
struct SettingsContentManager_t3562212113;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"

// System.Void SettingsContentManager::.ctor()
extern "C"  void SettingsContentManager__ctor_m1845178308 (SettingsContentManager_t3562212113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::add_confirmed(ConfirmationEvent)
extern "C"  void SettingsContentManager_add_confirmed_m1801552408 (SettingsContentManager_t3562212113 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::remove_confirmed(ConfirmationEvent)
extern "C"  void SettingsContentManager_remove_confirmed_m3988928065 (SettingsContentManager_t3562212113 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::OnEnable()
extern "C"  void SettingsContentManager_OnEnable_m2535225724 (SettingsContentManager_t3562212113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::ConfirmSave()
extern "C"  void SettingsContentManager_ConfirmSave_m354748719 (SettingsContentManager_t3562212113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::saveConfirmed(System.Boolean)
extern "C"  void SettingsContentManager_saveConfirmed_m2471536621 (SettingsContentManager_t3562212113 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsContentManager::SaveUserData()
extern "C"  void SettingsContentManager_SaveUserData_m3090099016 (SettingsContentManager_t3562212113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
