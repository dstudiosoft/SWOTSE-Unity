﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_t4213021259;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.UploadProgressChangedEventArgs::.ctor(System.Int64,System.Int64,System.Int64,System.Int64,System.Int32,System.Object)
extern "C"  void UploadProgressChangedEventArgs__ctor_m4023395864 (UploadProgressChangedEventArgs_t4213021259 * __this, int64_t ___bytesReceived0, int64_t ___totalBytesToReceive1, int64_t ___bytesSent2, int64_t ___totalBytesToSend3, int32_t ___progressPercentage4, Il2CppObject * ___userState5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_BytesReceived()
extern "C"  int64_t UploadProgressChangedEventArgs_get_BytesReceived_m2546051237 (UploadProgressChangedEventArgs_t4213021259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_TotalBytesToReceive()
extern "C"  int64_t UploadProgressChangedEventArgs_get_TotalBytesToReceive_m281014228 (UploadProgressChangedEventArgs_t4213021259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_BytesSent()
extern "C"  int64_t UploadProgressChangedEventArgs_get_BytesSent_m1171086112 (UploadProgressChangedEventArgs_t4213021259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_TotalBytesToSend()
extern "C"  int64_t UploadProgressChangedEventArgs_get_TotalBytesToSend_m3402743759 (UploadProgressChangedEventArgs_t4213021259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
