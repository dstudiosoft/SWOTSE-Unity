﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2898143515(__this, ___host0, method) ((  void (*) (Enumerator_t3146161684 *, Dictionary_2_t1459628920 *, const MethodInfo*))Enumerator__ctor_m419014865_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2498495900(__this, method) ((  Il2CppObject * (*) (Enumerator_t3146161684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1972155554(__this, method) ((  void (*) (Enumerator_t3146161684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::Dispose()
#define Enumerator_Dispose_m604685319(__this, method) ((  void (*) (Enumerator_t3146161684 *, const MethodInfo*))Enumerator_Dispose_m942415041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::MoveNext()
#define Enumerator_MoveNext_m2998879337(__this, method) ((  bool (*) (Enumerator_t3146161684 *, const MethodInfo*))Enumerator_MoveNext_m2045321910_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,ArmyTimer>::get_Current()
#define Enumerator_get_Current_m2083153186(__this, method) ((  ArmyTimer_t470110518 * (*) (Enumerator_t3146161684 *, const MethodInfo*))Enumerator_get_Current_m1952080304_gshared)(__this, method)
