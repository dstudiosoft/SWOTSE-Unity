﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Hit.HitTest
struct HitTest_t768639505;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_HitTest_ObjectHi3057876522.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"

// System.Void TouchScript.Hit.HitTest::.ctor()
extern "C"  void HitTest__ctor_m3899236291 (HitTest_t768639505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.HitTest/ObjectHitResult TouchScript.Hit.HitTest::IsHit(TouchScript.Hit.TouchHit)
extern "C"  int32_t HitTest_IsHit_m1081590976 (HitTest_t768639505 * __this, TouchHit_t4186847494  ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Hit.HitTest::OnEnable()
extern "C"  void HitTest_OnEnable_m911160143 (HitTest_t768639505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
