﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Int32,System.Single>
struct InvokableCall_2_t2602522310;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Single>
struct UnityAction_2_t2587731426;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m154006538_gshared (InvokableCall_2_t2602522310 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m154006538(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t2602522310 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m154006538_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Single>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m833504397_gshared (InvokableCall_2_t2602522310 * __this, UnityAction_2_t2587731426 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m833504397(__this, ___action0, method) ((  void (*) (InvokableCall_2_t2602522310 *, UnityAction_2_t2587731426 *, const MethodInfo*))InvokableCall_2__ctor_m833504397_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Single>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m2714629973_gshared (InvokableCall_2_t2602522310 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m2714629973(__this, ___args0, method) ((  void (*) (InvokableCall_2_t2602522310 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_2_Invoke_m2714629973_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Int32,System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2037717773_gshared (InvokableCall_2_t2602522310 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2037717773(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t2602522310 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2037717773_gshared)(__this, ___targetObj0, ___method1, method)
