﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m251762288(__this, ___l0, method) ((  void (*) (Enumerator_t3352292471 *, List_1_t3817562797 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m694403810(__this, method) ((  void (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1307990654(__this, method) ((  Il2CppObject * (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::Dispose()
#define Enumerator_Dispose_m1545060179(__this, method) ((  void (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::VerifyState()
#define Enumerator_VerifyState_m3531971282(__this, method) ((  void (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::MoveNext()
#define Enumerator_MoveNext_m2798585298(__this, method) ((  bool (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.GatewayIPAddressInformation>::get_Current()
#define Enumerator_get_Current_m1669842403(__this, method) ((  GatewayIPAddressInformation_t153474369 * (*) (Enumerator_t3352292471 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
