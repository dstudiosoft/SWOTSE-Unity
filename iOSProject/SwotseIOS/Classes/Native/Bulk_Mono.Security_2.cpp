﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
struct X509CertificateEnumerator_t3515934698;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1542168550;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t489243025;
// Mono.Security.X509.X509Chain
struct X509Chain_t863783601;
// System.String
struct String_t;
// System.Collections.CollectionBase
struct CollectionBase_t2727926298;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t609554709;
// Mono.Security.X509.X509Extension
struct X509Extension_t3173393653;
// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct BasicConstraintsExtension_t2462195279;
// System.Security.Cryptography.RSA
struct RSA_t2385438082;
// Mono.Security.X509.X509Crl
struct X509Crl_t1148767388;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// Mono.Security.ASN1
struct ASN1_t2114160833;
// System.Security.Cryptography.CryptographicException
struct CryptographicException_t248831461;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t645568789;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// Mono.Security.X509.Extensions.KeyUsageExtension
struct KeyUsageExtension_t1795615912;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Security.Cryptography.DSA
struct DSA_t2386879874;
// System.Security.Cryptography.DSASignatureDeformatter
struct DSASignatureDeformatter_t3677955172;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t932037087;
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
struct RSAPKCS1SignatureDeformatter_t3767223008;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.IO.FileStream
struct FileStream_t4292183065;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.Exception
struct Exception_t;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t3108766909;
// Mono.Security.X509.X509Store
struct X509Store_t2777415284;
// Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension
struct SubjectKeyIdentifierExtension_t2404375272;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.String[]
struct StringU5BU5D_t1281789340;
// Mono.Security.X509.X509StoreManager
struct X509StoreManager_t1046782376;
// Mono.Security.X509.X509Stores
struct X509Stores_t1373936238;
// Mono.Security.X509.X509Stores/Names
struct Names_t1325641082;
// Mono.Security.X509.X520
struct X520_t3325039438;
// Mono.Security.X509.X520/AttributeTypeAndValue
struct AttributeTypeAndValue_t3245693428;
// System.FormatException
struct FormatException_t154580423;
// System.Text.Encoding
struct Encoding_t1523322056;
// Mono.Security.X509.X520/CommonName
struct CommonName_t2882805359;
// Mono.Security.X509.X520/CountryName
struct CountryName_t4165042092;
// Mono.Security.X509.X520/DnQualifier
struct DnQualifier_t2746394302;
// Mono.Security.X509.X520/DomainComponent
struct DomainComponent_t1597975113;
// Mono.Security.X509.X520/EmailAddress
struct EmailAddress_t484866411;
// Mono.Security.X509.X520/GivenName
struct GivenName_t3114888956;
// Mono.Security.X509.X520/Initial
struct Initial_t2745963796;
// Mono.Security.X509.X520/LocalityName
struct LocalityName_t1511149830;
// Mono.Security.X509.X520/Name
struct Name_t3626842933;
// Mono.Security.X509.X520/Oid
struct Oid_t2014794921;
// Mono.Security.X509.X520/OrganizationalUnitName
struct OrganizationalUnitName_t1968218587;
// Mono.Security.X509.X520/OrganizationName
struct OrganizationName_t857038441;
// Mono.Security.X509.X520/SerialNumber
struct SerialNumber_t3812427306;
// Mono.Security.X509.X520/StateOrProvinceName
struct StateOrProvinceName_t1916702996;
// Mono.Security.X509.X520/Surname
struct Surname_t4221813936;
// Mono.Security.X509.X520/Title
struct Title_t2026585634;
// Mono.Security.X509.X520/UserId
struct UserId_t70549454;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2405853701;
// System.Globalization.TextInfo
struct TextInfo_t3810425522;
// System.Globalization.CompareInfo
struct CompareInfo_t1092934962;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t3985046076;
// System.Globalization.Calendar
struct Calendar_t1661121569;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Int32
struct Int32_t2950945753;
// System.Void
struct Void_t1185182177;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3530625384;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Reflection.Assembly
struct Assembly_t;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;

extern RuntimeClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern const uint32_t X509CertificateEnumerator__ctor_m85694331_MetadataUsageId;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern const uint32_t X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1846030361_MetadataUsageId;
extern const uint32_t X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2626270621_MetadataUsageId;
extern const uint32_t X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m2039524926_MetadataUsageId;
extern RuntimeClass* X509Certificate_t489243025_il2cpp_TypeInfo_var;
extern const uint32_t X509CertificateEnumerator_get_Current_m1004537031_MetadataUsageId;
extern const uint32_t X509CertificateEnumerator_MoveNext_m3925432749_MetadataUsageId;
extern const uint32_t X509CertificateEnumerator_Reset_m1825523691_MetadataUsageId;
extern RuntimeClass* X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var;
extern const uint32_t X509Chain__ctor_m3563800449_MetadataUsageId;
extern const uint32_t X509Chain__ctor_m1084071882_MetadataUsageId;
extern const uint32_t X509Chain_get_TrustAnchors_m2434696767_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t X509Chain_FindByIssuerName_m2280432457_MetadataUsageId;
extern const uint32_t X509Chain_Build_m2469702749_MetadataUsageId;
extern RuntimeClass* ServicePointManager_t170559685_il2cpp_TypeInfo_var;
extern const uint32_t X509Chain_IsValid_m3670863655_MetadataUsageId;
extern const uint32_t X509Chain_FindCertificateParent_m2809823532_MetadataUsageId;
extern const uint32_t X509Chain_FindCertificateRoot_m1937726457_MetadataUsageId;
extern RuntimeClass* BasicConstraintsExtension_t2462195279_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1004423994;
extern const uint32_t X509Chain_IsParent_m2689546349_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2910693339;
extern const uint32_t X509Crl__ctor_m1817187405_MetadataUsageId;
extern RuntimeClass* ASN1_t2114160833_il2cpp_TypeInfo_var;
extern RuntimeClass* CryptographicException_t248831461_il2cpp_TypeInfo_var;
extern RuntimeClass* X501_t1758824426_il2cpp_TypeInfo_var;
extern RuntimeClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern RuntimeClass* X509CrlEntry_t645568789_il2cpp_TypeInfo_var;
extern RuntimeClass* X509ExtensionCollection_t609554709_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1752854249;
extern String_t* _stringLiteral3761512114;
extern const uint32_t X509Crl_Parse_m3164013387_MetadataUsageId;
extern const uint32_t X509Crl_get_Entries_m4032389711_MetadataUsageId;
extern const uint32_t X509Crl_get_Item_m2105052623_MetadataUsageId;
extern const uint32_t X509Crl_get_Hash_m464217067_MetadataUsageId;
extern const uint32_t X509Crl_get_Signature_m1378549588_MetadataUsageId;
extern const uint32_t X509Crl_get_RawData_m3623997699_MetadataUsageId;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern const uint32_t X509Crl_get_IsCurrent_m298812277_MetadataUsageId;
extern const uint32_t X509Crl_WasCurrent_m662015296_MetadataUsageId;
extern const uint32_t X509Crl_GetBytes_m326324156_MetadataUsageId;
extern String_t* _stringLiteral3501283186;
extern const uint32_t X509Crl_GetCrlEntry_m1550247114_MetadataUsageId;
extern String_t* _stringLiteral731045672;
extern const uint32_t X509Crl_GetCrlEntry_m641501875_MetadataUsageId;
extern RuntimeClass* KeyUsageExtension_t1795615912_il2cpp_TypeInfo_var;
extern RuntimeClass* X509Crl_t1148767388_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2392909825_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m282647386_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var;
extern String_t* _stringLiteral1004423982;
extern String_t* _stringLiteral254300466;
extern const uint32_t X509Crl_VerifySignature_m757547902_MetadataUsageId;
extern String_t* _stringLiteral1384881100;
extern String_t* _stringLiteral1002544076;
extern String_t* _stringLiteral2958859212;
extern String_t* _stringLiteral4242423987;
extern String_t* _stringLiteral3839139460;
extern String_t* _stringLiteral1144609714;
extern String_t* _stringLiteral2511804911;
extern const uint32_t X509Crl_GetHashName_m4214678741_MetadataUsageId;
extern RuntimeClass* DSASignatureDeformatter_t3677955172_il2cpp_TypeInfo_var;
extern const uint32_t X509Crl_VerifySignature_m1902456590_MetadataUsageId;
extern RuntimeClass* RSAPKCS1SignatureDeformatter_t3767223008_il2cpp_TypeInfo_var;
extern const uint32_t X509Crl_VerifySignature_m1808348256_MetadataUsageId;
extern RuntimeClass* RSA_t2385438082_il2cpp_TypeInfo_var;
extern RuntimeClass* DSA_t2386879874_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3454646207;
extern String_t* _stringLiteral1243364763;
extern const uint32_t X509Crl_VerifySignature_m2177510742_MetadataUsageId;
extern const uint32_t X509Crl_CreateFromFile_m1507150054_MetadataUsageId;
extern const uint32_t X509CrlEntry__ctor_m783552701_MetadataUsageId;
extern const uint32_t X509CrlEntry__ctor_m4013514048_MetadataUsageId;
extern const uint32_t X509CrlEntry_get_SerialNumber_m3627212772_MetadataUsageId;
extern const uint32_t X509CrlEntry_GetBytes_m2980586034_MetadataUsageId;
extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1590810976;
extern const uint32_t X509Extension__ctor_m710637961_MetadataUsageId;
extern String_t* _stringLiteral1610623306;
extern const uint32_t X509Extension__ctor_m1474351312_MetadataUsageId;
extern const uint32_t X509Extension_get_ASN1_m1064344075_MetadataUsageId;
extern RuntimeClass* X509Extension_t3173393653_il2cpp_TypeInfo_var;
extern const uint32_t X509Extension_Equals_m1779194186_MetadataUsageId;
extern RuntimeClass* CultureInfo_t4157843068_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3451435000;
extern String_t* _stringLiteral3452614528;
extern String_t* _stringLiteral3786055882;
extern String_t* _stringLiteral3450517376;
extern String_t* _stringLiteral3452614530;
extern const uint32_t X509Extension_WriteLine_m1662885247_MetadataUsageId;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern const uint32_t X509Extension_ToString_m3727002866_MetadataUsageId;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral632220839;
extern const uint32_t X509ExtensionCollection__ctor_m551870633_MetadataUsageId;
extern String_t* _stringLiteral1609817887;
extern const uint32_t X509ExtensionCollection_Add_m2251235768_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_AddRange_m3305410105_MetadataUsageId;
extern String_t* _stringLiteral3723644332;
extern const uint32_t X509ExtensionCollection_AddRange_m1104225129_MetadataUsageId;
extern String_t* _stringLiteral1607280970;
extern const uint32_t X509ExtensionCollection_CopyTo_m3181643769_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_IndexOf_m2606992261_MetadataUsageId;
extern String_t* _stringLiteral3266464951;
extern const uint32_t X509ExtensionCollection_IndexOf_m2996504451_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_Insert_m873879632_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_Remove_m2342091090_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_Remove_m2521723919_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_get_Item_m2771335836_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_get_Item_m4249795832_MetadataUsageId;
extern const uint32_t X509ExtensionCollection_GetBytes_m1622025118_MetadataUsageId;
extern const uint32_t X509Store_get_Crls_m1211262034_MetadataUsageId;
extern RuntimeClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern const uint32_t X509Store_get_Name_m597920689_MetadataUsageId;
extern const uint32_t X509Store_Import_m2542166443_MetadataUsageId;
extern const uint32_t X509Store_Import_m1052667485_MetadataUsageId;
extern const uint32_t X509Store_Remove_m3732268634_MetadataUsageId;
extern const uint32_t X509Store_Remove_m1800501587_MetadataUsageId;
extern String_t* _stringLiteral227815590;
extern String_t* _stringLiteral3313388058;
extern String_t* _stringLiteral1903542547;
extern const uint32_t X509Store_GetUniqueName_m4271638378_MetadataUsageId;
extern String_t* _stringLiteral3977648530;
extern const uint32_t X509Store_GetUniqueName_m3285060726_MetadataUsageId;
extern RuntimeClass* SubjectKeyIdentifierExtension_t2404375272_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1004423983;
extern const uint32_t X509Store_GetUniqueName_m132964055_MetadataUsageId;
extern String_t* _stringLiteral3452614531;
extern const uint32_t X509Store_GetUniqueName_m915074968_MetadataUsageId;
extern const uint32_t X509Store_Load_m2048139132_MetadataUsageId;
extern const uint32_t X509Store_LoadCertificate_m1587806288_MetadataUsageId;
extern const uint32_t X509Store_LoadCrl_m1881903843_MetadataUsageId;
extern const uint32_t X509Store_CheckStore_m2045435685_MetadataUsageId;
extern String_t* _stringLiteral2225310117;
extern const uint32_t X509Store_BuildCertificatesCollection_m3030935583_MetadataUsageId;
extern String_t* _stringLiteral3710028195;
extern const uint32_t X509Store_BuildCrlsCollection_m1991312527_MetadataUsageId;
extern RuntimeClass* X509StoreManager_t1046782376_il2cpp_TypeInfo_var;
extern RuntimeClass* X509Stores_t1373936238_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3525949144;
extern String_t* _stringLiteral9622098;
extern const uint32_t X509StoreManager_get_CurrentUser_m719101392_MetadataUsageId;
extern const uint32_t X509StoreManager_get_LocalMachine_m269504582_MetadataUsageId;
extern const uint32_t X509StoreManager_get_IntermediateCACertificates_m1576610804_MetadataUsageId;
extern const uint32_t X509StoreManager_get_IntermediateCACrls_m3919824182_MetadataUsageId;
extern const uint32_t X509StoreManager_get_TrustedRootCertificates_m2180997293_MetadataUsageId;
extern const uint32_t X509StoreManager_get_TrustedRootCACrls_m2267763919_MetadataUsageId;
extern const uint32_t X509StoreManager_get_UntrustedCertificates_m829127408_MetadataUsageId;
extern RuntimeClass* X509Store_t2777415284_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3456219107;
extern const uint32_t X509Stores_get_Personal_m1313610043_MetadataUsageId;
extern String_t* _stringLiteral4072668552;
extern const uint32_t X509Stores_get_OtherPeople_m2341174106_MetadataUsageId;
extern String_t* _stringLiteral3456743389;
extern const uint32_t X509Stores_get_IntermediateCA_m1350619599_MetadataUsageId;
extern String_t* _stringLiteral1986082327;
extern const uint32_t X509Stores_get_TrustedRoot_m1736182879_MetadataUsageId;
extern String_t* _stringLiteral1940787968;
extern const uint32_t X509Stores_get_Untrusted_m1029989579_MetadataUsageId;
extern String_t* _stringLiteral2153662835;
extern const uint32_t X509Stores_Open_m1037335183_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* FormatException_t154580423_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2649751261;
extern const uint32_t AttributeTypeAndValue_set_Value_m3270529695_MetadataUsageId;
extern RuntimeClass* Encoding_t1523322056_il2cpp_TypeInfo_var;
extern const uint32_t AttributeTypeAndValue_GetASN1_m735511441_MetadataUsageId;
extern String_t* _stringLiteral1452586790;
extern const uint32_t CommonName__ctor_m4066435887_MetadataUsageId;
extern String_t* _stringLiteral1855871317;
extern const uint32_t CountryName__ctor_m1759248525_MetadataUsageId;
extern String_t* _stringLiteral456868491;
extern const uint32_t DnQualifier__ctor_m2304537709_MetadataUsageId;
extern String_t* _stringLiteral1926373733;
extern const uint32_t DomainComponent__ctor_m2984562191_MetadataUsageId;
extern String_t* _stringLiteral3724057548;
extern const uint32_t EmailAddress__ctor_m1682437348_MetadataUsageId;
extern String_t* _stringLiteral3987161739;
extern const uint32_t GivenName__ctor_m2285954234_MetadataUsageId;
extern String_t* _stringLiteral2030846603;
extern const uint32_t Initial__ctor_m1539622106_MetadataUsageId;
extern String_t* _stringLiteral3421955258;
extern const uint32_t LocalityName__ctor_m816181155_MetadataUsageId;
extern String_t* _stringLiteral1648509579;
extern const uint32_t Name__ctor_m2961952406_MetadataUsageId;
extern String_t* _stringLiteral1245225052;
extern const uint32_t OrganizationalUnitName__ctor_m4279817838_MetadataUsageId;
extern String_t* _stringLiteral3201540188;
extern const uint32_t OrganizationName__ctor_m3237996566_MetadataUsageId;
extern String_t* _stringLiteral289787376;
extern const uint32_t SerialNumber__ctor_m2630741392_MetadataUsageId;
extern String_t* _stringLiteral1405532623;
extern const uint32_t StateOrProvinceName__ctor_m3786137020_MetadataUsageId;
extern String_t* _stringLiteral3018670731;
extern const uint32_t Surname__ctor_m4076845856_MetadataUsageId;
extern String_t* _stringLiteral3583877212;
extern const uint32_t Title__ctor_m3468619135_MetadataUsageId;
extern String_t* _stringLiteral1929716066;
extern const uint32_t UserId__ctor_m730120360_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct X509ExtensionU5BU5D_t3108766909;
struct StringU5BU5D_t1281789340;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CULTUREINFO_T4157843068_H
#define CULTUREINFO_T4157843068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4157843068  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t435877138 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3810425522 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1092934962 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t3985046076* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4157843068 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1661121569 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t4116647657* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___numInfo_14)); }
	inline NumberFormatInfo_t435877138 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t435877138 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t2405853701 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t2405853701 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t2405853701 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textInfo_16)); }
	inline TextInfo_t3810425522 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t3810425522 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t3810425522 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___compareInfo_26)); }
	inline CompareInfo_t1092934962 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t1092934962 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t1092934962 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t3985046076* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t3985046076** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t3985046076* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_culture_30)); }
	inline CultureInfo_t4157843068 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t4157843068 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t4157843068 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___calendar_32)); }
	inline Calendar_t1661121569 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t1661121569 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t1661121569 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t4116647657* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t4116647657** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t4116647657* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t4157843068_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4157843068 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t1853889766 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t1853889766 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t4157843068 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t4157843068 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t4157843068 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t1853889766 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t1853889766 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t1853889766 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t1853889766 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t1853889766 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t1853889766 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T4157843068_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef X509STORE_T2777415284_H
#define X509STORE_T2777415284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Store
struct  X509Store_t2777415284  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Store::_storePath
	String_t* ____storePath_0;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::_certificates
	X509CertificateCollection_t1542168550 * ____certificates_1;
	// System.Collections.ArrayList Mono.Security.X509.X509Store::_crls
	ArrayList_t2718874744 * ____crls_2;
	// System.Boolean Mono.Security.X509.X509Store::_crl
	bool ____crl_3;
	// System.String Mono.Security.X509.X509Store::_name
	String_t* ____name_4;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Store_t2777415284, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__certificates_1() { return static_cast<int32_t>(offsetof(X509Store_t2777415284, ____certificates_1)); }
	inline X509CertificateCollection_t1542168550 * get__certificates_1() const { return ____certificates_1; }
	inline X509CertificateCollection_t1542168550 ** get_address_of__certificates_1() { return &____certificates_1; }
	inline void set__certificates_1(X509CertificateCollection_t1542168550 * value)
	{
		____certificates_1 = value;
		Il2CppCodeGenWriteBarrier((&____certificates_1), value);
	}

	inline static int32_t get_offset_of__crls_2() { return static_cast<int32_t>(offsetof(X509Store_t2777415284, ____crls_2)); }
	inline ArrayList_t2718874744 * get__crls_2() const { return ____crls_2; }
	inline ArrayList_t2718874744 ** get_address_of__crls_2() { return &____crls_2; }
	inline void set__crls_2(ArrayList_t2718874744 * value)
	{
		____crls_2 = value;
		Il2CppCodeGenWriteBarrier((&____crls_2), value);
	}

	inline static int32_t get_offset_of__crl_3() { return static_cast<int32_t>(offsetof(X509Store_t2777415284, ____crl_3)); }
	inline bool get__crl_3() const { return ____crl_3; }
	inline bool* get_address_of__crl_3() { return &____crl_3; }
	inline void set__crl_3(bool value)
	{
		____crl_3 = value;
	}

	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(X509Store_t2777415284, ____name_4)); }
	inline String_t* get__name_4() const { return ____name_4; }
	inline String_t** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(String_t* value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T2777415284_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef DICTIONARY_2_T2736202052_H
#define DICTIONARY_2_T2736202052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_t2736202052  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t385246372* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___valueSlots_7)); }
	inline Int32U5BU5D_t385246372* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t385246372** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t385246372* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2736202052_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3530625384 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3530625384 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3530625384 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3530625384 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2736202052_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::disposed
	bool ___disposed_3;

public:
	inline static int32_t get_offset_of_HashValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_0)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_0() const { return ___HashValue_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_0() { return &___HashValue_0; }
	inline void set_HashValue_0(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_0), value);
	}

	inline static int32_t get_offset_of_HashSizeValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_1)); }
	inline int32_t get_HashSizeValue_1() const { return ___HashSizeValue_1; }
	inline int32_t* get_address_of_HashSizeValue_1() { return &___HashSizeValue_1; }
	inline void set_HashSizeValue_1(int32_t value)
	{
		___HashSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef ARRAYLIST_T2718874744_H
#define ARRAYLIST_T2718874744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t2718874744  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t2843939325* ____items_2;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__items_2() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____items_2)); }
	inline ObjectU5BU5D_t2843939325* get__items_2() const { return ____items_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_2() { return &____items_2; }
	inline void set__items_2(ObjectU5BU5D_t2843939325* value)
	{
		____items_2 = value;
		Il2CppCodeGenWriteBarrier((&____items_2), value);
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct ArrayList_t2718874744_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t2718874744_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T2718874744_H
#ifndef ASYMMETRICALGORITHM_T932037087_H
#define ASYMMETRICALGORITHM_T932037087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_t932037087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_1;

public:
	inline static int32_t get_offset_of_KeySizeValue_0() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___KeySizeValue_0)); }
	inline int32_t get_KeySizeValue_0() const { return ___KeySizeValue_0; }
	inline int32_t* get_address_of_KeySizeValue_0() { return &___KeySizeValue_0; }
	inline void set_KeySizeValue_0(int32_t value)
	{
		___KeySizeValue_0 = value;
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_1() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___LegalKeySizesValue_1)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_1() const { return ___LegalKeySizesValue_1; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_1() { return &___LegalKeySizesValue_1; }
	inline void set_LegalKeySizesValue_1(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICALGORITHM_T932037087_H
#ifndef PATH_T1605229823_H
#define PATH_T1605229823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Path
struct  Path_t1605229823  : public RuntimeObject
{
public:

public:
};

struct Path_t1605229823_StaticFields
{
public:
	// System.Char[] System.IO.Path::InvalidPathChars
	CharU5BU5D_t3528271667* ___InvalidPathChars_0;
	// System.Char System.IO.Path::AltDirectorySeparatorChar
	Il2CppChar ___AltDirectorySeparatorChar_1;
	// System.Char System.IO.Path::DirectorySeparatorChar
	Il2CppChar ___DirectorySeparatorChar_2;
	// System.Char System.IO.Path::PathSeparator
	Il2CppChar ___PathSeparator_3;
	// System.String System.IO.Path::DirectorySeparatorStr
	String_t* ___DirectorySeparatorStr_4;
	// System.Char System.IO.Path::VolumeSeparatorChar
	Il2CppChar ___VolumeSeparatorChar_5;
	// System.Char[] System.IO.Path::PathSeparatorChars
	CharU5BU5D_t3528271667* ___PathSeparatorChars_6;
	// System.Boolean System.IO.Path::dirEqualsVolume
	bool ___dirEqualsVolume_7;

public:
	inline static int32_t get_offset_of_InvalidPathChars_0() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___InvalidPathChars_0)); }
	inline CharU5BU5D_t3528271667* get_InvalidPathChars_0() const { return ___InvalidPathChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_InvalidPathChars_0() { return &___InvalidPathChars_0; }
	inline void set_InvalidPathChars_0(CharU5BU5D_t3528271667* value)
	{
		___InvalidPathChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidPathChars_0), value);
	}

	inline static int32_t get_offset_of_AltDirectorySeparatorChar_1() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___AltDirectorySeparatorChar_1)); }
	inline Il2CppChar get_AltDirectorySeparatorChar_1() const { return ___AltDirectorySeparatorChar_1; }
	inline Il2CppChar* get_address_of_AltDirectorySeparatorChar_1() { return &___AltDirectorySeparatorChar_1; }
	inline void set_AltDirectorySeparatorChar_1(Il2CppChar value)
	{
		___AltDirectorySeparatorChar_1 = value;
	}

	inline static int32_t get_offset_of_DirectorySeparatorChar_2() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___DirectorySeparatorChar_2)); }
	inline Il2CppChar get_DirectorySeparatorChar_2() const { return ___DirectorySeparatorChar_2; }
	inline Il2CppChar* get_address_of_DirectorySeparatorChar_2() { return &___DirectorySeparatorChar_2; }
	inline void set_DirectorySeparatorChar_2(Il2CppChar value)
	{
		___DirectorySeparatorChar_2 = value;
	}

	inline static int32_t get_offset_of_PathSeparator_3() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___PathSeparator_3)); }
	inline Il2CppChar get_PathSeparator_3() const { return ___PathSeparator_3; }
	inline Il2CppChar* get_address_of_PathSeparator_3() { return &___PathSeparator_3; }
	inline void set_PathSeparator_3(Il2CppChar value)
	{
		___PathSeparator_3 = value;
	}

	inline static int32_t get_offset_of_DirectorySeparatorStr_4() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___DirectorySeparatorStr_4)); }
	inline String_t* get_DirectorySeparatorStr_4() const { return ___DirectorySeparatorStr_4; }
	inline String_t** get_address_of_DirectorySeparatorStr_4() { return &___DirectorySeparatorStr_4; }
	inline void set_DirectorySeparatorStr_4(String_t* value)
	{
		___DirectorySeparatorStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___DirectorySeparatorStr_4), value);
	}

	inline static int32_t get_offset_of_VolumeSeparatorChar_5() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___VolumeSeparatorChar_5)); }
	inline Il2CppChar get_VolumeSeparatorChar_5() const { return ___VolumeSeparatorChar_5; }
	inline Il2CppChar* get_address_of_VolumeSeparatorChar_5() { return &___VolumeSeparatorChar_5; }
	inline void set_VolumeSeparatorChar_5(Il2CppChar value)
	{
		___VolumeSeparatorChar_5 = value;
	}

	inline static int32_t get_offset_of_PathSeparatorChars_6() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___PathSeparatorChars_6)); }
	inline CharU5BU5D_t3528271667* get_PathSeparatorChars_6() const { return ___PathSeparatorChars_6; }
	inline CharU5BU5D_t3528271667** get_address_of_PathSeparatorChars_6() { return &___PathSeparatorChars_6; }
	inline void set_PathSeparatorChars_6(CharU5BU5D_t3528271667* value)
	{
		___PathSeparatorChars_6 = value;
		Il2CppCodeGenWriteBarrier((&___PathSeparatorChars_6), value);
	}

	inline static int32_t get_offset_of_dirEqualsVolume_7() { return static_cast<int32_t>(offsetof(Path_t1605229823_StaticFields, ___dirEqualsVolume_7)); }
	inline bool get_dirEqualsVolume_7() const { return ___dirEqualsVolume_7; }
	inline bool* get_address_of_dirEqualsVolume_7() { return &___dirEqualsVolume_7; }
	inline void set_dirEqualsVolume_7(bool value)
	{
		___dirEqualsVolume_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T1605229823_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t3123823036 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1188251036 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoder_fallback_3)); }
	inline DecoderFallback_t3123823036 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t3123823036 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoder_fallback_4)); }
	inline EncoderFallback_t1188251036 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1188251036 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t2843939325* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t1523322056 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t1523322056 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t1523322056 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t1523322056 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t1523322056 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t1523322056 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t2843939325* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t2843939325* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t1523322056 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t1523322056 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t1523322056 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t1523322056 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t1523322056 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t1523322056 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t1523322056 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t1523322056 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t1523322056 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t1523322056 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t1523322056 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t1523322056 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t1523322056 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t1523322056 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t1523322056 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t1523322056 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t1523322056 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t1523322056 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t1523322056 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t1523322056 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t1523322056 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#define ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct  AsymmetricSignatureDeformatter_t2681190756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#ifndef ATTRIBUTETYPEANDVALUE_T3245693428_H
#define ATTRIBUTETYPEANDVALUE_T3245693428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/AttributeTypeAndValue
struct  AttributeTypeAndValue_t3245693428  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X520/AttributeTypeAndValue::oid
	String_t* ___oid_0;
	// System.String Mono.Security.X509.X520/AttributeTypeAndValue::attrValue
	String_t* ___attrValue_1;
	// System.Int32 Mono.Security.X509.X520/AttributeTypeAndValue::upperBound
	int32_t ___upperBound_2;
	// System.Byte Mono.Security.X509.X520/AttributeTypeAndValue::encoding
	uint8_t ___encoding_3;

public:
	inline static int32_t get_offset_of_oid_0() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t3245693428, ___oid_0)); }
	inline String_t* get_oid_0() const { return ___oid_0; }
	inline String_t** get_address_of_oid_0() { return &___oid_0; }
	inline void set_oid_0(String_t* value)
	{
		___oid_0 = value;
		Il2CppCodeGenWriteBarrier((&___oid_0), value);
	}

	inline static int32_t get_offset_of_attrValue_1() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t3245693428, ___attrValue_1)); }
	inline String_t* get_attrValue_1() const { return ___attrValue_1; }
	inline String_t** get_address_of_attrValue_1() { return &___attrValue_1; }
	inline void set_attrValue_1(String_t* value)
	{
		___attrValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___attrValue_1), value);
	}

	inline static int32_t get_offset_of_upperBound_2() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t3245693428, ___upperBound_2)); }
	inline int32_t get_upperBound_2() const { return ___upperBound_2; }
	inline int32_t* get_address_of_upperBound_2() { return &___upperBound_2; }
	inline void set_upperBound_2(int32_t value)
	{
		___upperBound_2 = value;
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t3245693428, ___encoding_3)); }
	inline uint8_t get_encoding_3() const { return ___encoding_3; }
	inline uint8_t* get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(uint8_t value)
	{
		___encoding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETYPEANDVALUE_T3245693428_H
#ifndef X509STORES_T1373936238_H
#define X509STORES_T1373936238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Stores
struct  X509Stores_t1373936238  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Stores::_storePath
	String_t* ____storePath_0;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_personal
	X509Store_t2777415284 * ____personal_1;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_other
	X509Store_t2777415284 * ____other_2;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_intermediate
	X509Store_t2777415284 * ____intermediate_3;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_trusted
	X509Store_t2777415284 * ____trusted_4;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_untrusted
	X509Store_t2777415284 * ____untrusted_5;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__personal_1() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____personal_1)); }
	inline X509Store_t2777415284 * get__personal_1() const { return ____personal_1; }
	inline X509Store_t2777415284 ** get_address_of__personal_1() { return &____personal_1; }
	inline void set__personal_1(X509Store_t2777415284 * value)
	{
		____personal_1 = value;
		Il2CppCodeGenWriteBarrier((&____personal_1), value);
	}

	inline static int32_t get_offset_of__other_2() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____other_2)); }
	inline X509Store_t2777415284 * get__other_2() const { return ____other_2; }
	inline X509Store_t2777415284 ** get_address_of__other_2() { return &____other_2; }
	inline void set__other_2(X509Store_t2777415284 * value)
	{
		____other_2 = value;
		Il2CppCodeGenWriteBarrier((&____other_2), value);
	}

	inline static int32_t get_offset_of__intermediate_3() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____intermediate_3)); }
	inline X509Store_t2777415284 * get__intermediate_3() const { return ____intermediate_3; }
	inline X509Store_t2777415284 ** get_address_of__intermediate_3() { return &____intermediate_3; }
	inline void set__intermediate_3(X509Store_t2777415284 * value)
	{
		____intermediate_3 = value;
		Il2CppCodeGenWriteBarrier((&____intermediate_3), value);
	}

	inline static int32_t get_offset_of__trusted_4() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____trusted_4)); }
	inline X509Store_t2777415284 * get__trusted_4() const { return ____trusted_4; }
	inline X509Store_t2777415284 ** get_address_of__trusted_4() { return &____trusted_4; }
	inline void set__trusted_4(X509Store_t2777415284 * value)
	{
		____trusted_4 = value;
		Il2CppCodeGenWriteBarrier((&____trusted_4), value);
	}

	inline static int32_t get_offset_of__untrusted_5() { return static_cast<int32_t>(offsetof(X509Stores_t1373936238, ____untrusted_5)); }
	inline X509Store_t2777415284 * get__untrusted_5() const { return ____untrusted_5; }
	inline X509Store_t2777415284 ** get_address_of__untrusted_5() { return &____untrusted_5; }
	inline void set__untrusted_5(X509Store_t2777415284 * value)
	{
		____untrusted_5 = value;
		Il2CppCodeGenWriteBarrier((&____untrusted_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORES_T1373936238_H
#ifndef X509STOREMANAGER_T1046782376_H
#define X509STOREMANAGER_T1046782376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509StoreManager
struct  X509StoreManager_t1046782376  : public RuntimeObject
{
public:

public:
};

struct X509StoreManager_t1046782376_StaticFields
{
public:
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_userStore
	X509Stores_t1373936238 * ____userStore_0;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_machineStore
	X509Stores_t1373936238 * ____machineStore_1;

public:
	inline static int32_t get_offset_of__userStore_0() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____userStore_0)); }
	inline X509Stores_t1373936238 * get__userStore_0() const { return ____userStore_0; }
	inline X509Stores_t1373936238 ** get_address_of__userStore_0() { return &____userStore_0; }
	inline void set__userStore_0(X509Stores_t1373936238 * value)
	{
		____userStore_0 = value;
		Il2CppCodeGenWriteBarrier((&____userStore_0), value);
	}

	inline static int32_t get_offset_of__machineStore_1() { return static_cast<int32_t>(offsetof(X509StoreManager_t1046782376_StaticFields, ____machineStore_1)); }
	inline X509Stores_t1373936238 * get__machineStore_1() const { return ____machineStore_1; }
	inline X509Stores_t1373936238 ** get_address_of__machineStore_1() { return &____machineStore_1; }
	inline void set__machineStore_1(X509Stores_t1373936238 * value)
	{
		____machineStore_1 = value;
		Il2CppCodeGenWriteBarrier((&____machineStore_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STOREMANAGER_T1046782376_H
#ifndef X520_T3325039438_H
#define X520_T3325039438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520
struct  X520_t3325039438  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X520_T3325039438_H
#ifndef NAMES_T1325641082_H
#define NAMES_T1325641082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Stores/Names
struct  Names_t1325641082  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMES_T1325641082_H
#ifndef COLLECTIONBASE_T2727926298_H
#define COLLECTIONBASE_T2727926298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t2727926298  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t2718874744 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t2727926298, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T2727926298_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef X509EXTENSION_T3173393653_H
#define X509EXTENSION_T3173393653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Extension
struct  X509Extension_t3173393653  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Extension::extnOid
	String_t* ___extnOid_0;
	// System.Boolean Mono.Security.X509.X509Extension::extnCritical
	bool ___extnCritical_1;
	// Mono.Security.ASN1 Mono.Security.X509.X509Extension::extnValue
	ASN1_t2114160833 * ___extnValue_2;

public:
	inline static int32_t get_offset_of_extnOid_0() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnOid_0)); }
	inline String_t* get_extnOid_0() const { return ___extnOid_0; }
	inline String_t** get_address_of_extnOid_0() { return &___extnOid_0; }
	inline void set_extnOid_0(String_t* value)
	{
		___extnOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___extnOid_0), value);
	}

	inline static int32_t get_offset_of_extnCritical_1() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnCritical_1)); }
	inline bool get_extnCritical_1() const { return ___extnCritical_1; }
	inline bool* get_address_of_extnCritical_1() { return &___extnCritical_1; }
	inline void set_extnCritical_1(bool value)
	{
		___extnCritical_1 = value;
	}

	inline static int32_t get_offset_of_extnValue_2() { return static_cast<int32_t>(offsetof(X509Extension_t3173393653, ___extnValue_2)); }
	inline ASN1_t2114160833 * get_extnValue_2() const { return ___extnValue_2; }
	inline ASN1_t2114160833 ** get_address_of_extnValue_2() { return &___extnValue_2; }
	inline void set_extnValue_2(ASN1_t2114160833 * value)
	{
		___extnValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___extnValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T3173393653_H
#ifndef ASN1_T2114160833_H
#define ASN1_T2114160833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1
struct  ASN1_t2114160833  : public RuntimeObject
{
public:
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_t4116647657* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t2718874744 * ___elist_2;

public:
	inline static int32_t get_offset_of_m_nTag_0() { return static_cast<int32_t>(offsetof(ASN1_t2114160833, ___m_nTag_0)); }
	inline uint8_t get_m_nTag_0() const { return ___m_nTag_0; }
	inline uint8_t* get_address_of_m_nTag_0() { return &___m_nTag_0; }
	inline void set_m_nTag_0(uint8_t value)
	{
		___m_nTag_0 = value;
	}

	inline static int32_t get_offset_of_m_aValue_1() { return static_cast<int32_t>(offsetof(ASN1_t2114160833, ___m_aValue_1)); }
	inline ByteU5BU5D_t4116647657* get_m_aValue_1() const { return ___m_aValue_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_aValue_1() { return &___m_aValue_1; }
	inline void set_m_aValue_1(ByteU5BU5D_t4116647657* value)
	{
		___m_aValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_aValue_1), value);
	}

	inline static int32_t get_offset_of_elist_2() { return static_cast<int32_t>(offsetof(ASN1_t2114160833, ___elist_2)); }
	inline ArrayList_t2718874744 * get_elist_2() const { return ___elist_2; }
	inline ArrayList_t2718874744 ** get_address_of_elist_2() { return &___elist_2; }
	inline void set_elist_2(ArrayList_t2718874744 * value)
	{
		___elist_2 = value;
		Il2CppCodeGenWriteBarrier((&___elist_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1_T2114160833_H
#ifndef X509CERTIFICATEENUMERATOR_T3515934698_H
#define X509CERTIFICATEENUMERATOR_T3515934698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_t3515934698  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_t3515934698, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_T3515934698_H
#ifndef DNQUALIFIER_T2746394302_H
#define DNQUALIFIER_T2746394302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/DnQualifier
struct  DnQualifier_t2746394302  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNQUALIFIER_T2746394302_H
#ifndef EMAILADDRESS_T484866411_H
#define EMAILADDRESS_T484866411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/EmailAddress
struct  EmailAddress_t484866411  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAILADDRESS_T484866411_H
#ifndef GIVENNAME_T3114888956_H
#define GIVENNAME_T3114888956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/GivenName
struct  GivenName_t3114888956  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIVENNAME_T3114888956_H
#ifndef DOMAINCOMPONENT_T1597975113_H
#define DOMAINCOMPONENT_T1597975113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/DomainComponent
struct  DomainComponent_t1597975113  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOMAINCOMPONENT_T1597975113_H
#ifndef COUNTRYNAME_T4165042092_H
#define COUNTRYNAME_T4165042092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/CountryName
struct  CountryName_t4165042092  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTRYNAME_T4165042092_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef COMMONNAME_T2882805359_H
#define COMMONNAME_T2882805359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/CommonName
struct  CommonName_t2882805359  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONNAME_T2882805359_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INITIAL_T2745963796_H
#define INITIAL_T2745963796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/Initial
struct  Initial_t2745963796  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIAL_T2745963796_H
#ifndef TITLE_T2026585634_H
#define TITLE_T2026585634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/Title
struct  Title_t2026585634  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLE_T2026585634_H
#ifndef SURNAME_T4221813936_H
#define SURNAME_T4221813936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/Surname
struct  Surname_t4221813936  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURNAME_T4221813936_H
#ifndef STATEORPROVINCENAME_T1916702996_H
#define STATEORPROVINCENAME_T1916702996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/StateOrProvinceName
struct  StateOrProvinceName_t1916702996  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEORPROVINCENAME_T1916702996_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef X509CERTIFICATECOLLECTION_T1542168550_H
#define X509CERTIFICATECOLLECTION_T1542168550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509CertificateCollection
struct  X509CertificateCollection_t1542168550  : public CollectionBase_t2727926298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T1542168550_H
#ifndef USERID_T70549454_H
#define USERID_T70549454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/UserId
struct  UserId_t70549454  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERID_T70549454_H
#ifndef OID_T2014794921_H
#define OID_T2014794921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/Oid
struct  Oid_t2014794921  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_T2014794921_H
#ifndef NAME_T3626842933_H
#define NAME_T3626842933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/Name
struct  Name_t3626842933  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAME_T3626842933_H
#ifndef LOCALITYNAME_T1511149830_H
#define LOCALITYNAME_T1511149830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/LocalityName
struct  LocalityName_t1511149830  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALITYNAME_T1511149830_H
#ifndef SERIALNUMBER_T3812427306_H
#define SERIALNUMBER_T3812427306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/SerialNumber
struct  SerialNumber_t3812427306  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALNUMBER_T3812427306_H
#ifndef ORGANIZATIONNAME_T857038441_H
#define ORGANIZATIONNAME_T857038441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/OrganizationName
struct  OrganizationName_t857038441  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORGANIZATIONNAME_T857038441_H
#ifndef ORGANIZATIONALUNITNAME_T1968218587_H
#define ORGANIZATIONALUNITNAME_T1968218587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/OrganizationalUnitName
struct  OrganizationalUnitName_t1968218587  : public AttributeTypeAndValue_t3245693428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORGANIZATIONALUNITNAME_T1968218587_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef X509EXTENSIONCOLLECTION_T609554709_H
#define X509EXTENSIONCOLLECTION_T609554709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509ExtensionCollection
struct  X509ExtensionCollection_t609554709  : public CollectionBase_t2727926298
{
public:
	// System.Boolean Mono.Security.X509.X509ExtensionCollection::readOnly
	bool ___readOnly_1;

public:
	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t609554709, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T609554709_H
#ifndef KEYUSAGEEXTENSION_T1795615912_H
#define KEYUSAGEEXTENSION_T1795615912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsageExtension
struct  KeyUsageExtension_t1795615912  : public X509Extension_t3173393653
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsageExtension::kubits
	int32_t ___kubits_3;

public:
	inline static int32_t get_offset_of_kubits_3() { return static_cast<int32_t>(offsetof(KeyUsageExtension_t1795615912, ___kubits_3)); }
	inline int32_t get_kubits_3() const { return ___kubits_3; }
	inline int32_t* get_address_of_kubits_3() { return &___kubits_3; }
	inline void set_kubits_3(int32_t value)
	{
		___kubits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGEEXTENSION_T1795615912_H
#ifndef BASICCONSTRAINTSEXTENSION_T2462195279_H
#define BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct  BasicConstraintsExtension_t2462195279  : public X509Extension_t3173393653
{
public:
	// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::cA
	bool ___cA_3;
	// System.Int32 Mono.Security.X509.Extensions.BasicConstraintsExtension::pathLenConstraint
	int32_t ___pathLenConstraint_4;

public:
	inline static int32_t get_offset_of_cA_3() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___cA_3)); }
	inline bool get_cA_3() const { return ___cA_3; }
	inline bool* get_address_of_cA_3() { return &___cA_3; }
	inline void set_cA_3(bool value)
	{
		___cA_3 = value;
	}

	inline static int32_t get_offset_of_pathLenConstraint_4() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t2462195279, ___pathLenConstraint_4)); }
	inline int32_t get_pathLenConstraint_4() const { return ___pathLenConstraint_4; }
	inline int32_t* get_address_of_pathLenConstraint_4() { return &___pathLenConstraint_4; }
	inline void set_pathLenConstraint_4(int32_t value)
	{
		___pathLenConstraint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTSEXTENSION_T2462195279_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef DSASIGNATUREDEFORMATTER_T3677955172_H
#define DSASIGNATUREDEFORMATTER_T3677955172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSASignatureDeformatter
struct  DSASignatureDeformatter_t3677955172  : public AsymmetricSignatureDeformatter_t2681190756
{
public:
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureDeformatter::dsa
	DSA_t2386879874 * ___dsa_0;

public:
	inline static int32_t get_offset_of_dsa_0() { return static_cast<int32_t>(offsetof(DSASignatureDeformatter_t3677955172, ___dsa_0)); }
	inline DSA_t2386879874 * get_dsa_0() const { return ___dsa_0; }
	inline DSA_t2386879874 ** get_address_of_dsa_0() { return &___dsa_0; }
	inline void set_dsa_0(DSA_t2386879874 * value)
	{
		___dsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___dsa_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSASIGNATUREDEFORMATTER_T3677955172_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#define RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
struct  RSAPKCS1SignatureDeformatter_t3767223008  : public AsymmetricSignatureDeformatter_t2681190756
{
public:
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAPKCS1SignatureDeformatter::rsa
	RSA_t2385438082 * ___rsa_0;
	// System.String System.Security.Cryptography.RSAPKCS1SignatureDeformatter::hashName
	String_t* ___hashName_1;

public:
	inline static int32_t get_offset_of_rsa_0() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureDeformatter_t3767223008, ___rsa_0)); }
	inline RSA_t2385438082 * get_rsa_0() const { return ___rsa_0; }
	inline RSA_t2385438082 ** get_address_of_rsa_0() { return &___rsa_0; }
	inline void set_rsa_0(RSA_t2385438082 * value)
	{
		___rsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsa_0), value);
	}

	inline static int32_t get_offset_of_hashName_1() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureDeformatter_t3767223008, ___hashName_1)); }
	inline String_t* get_hashName_1() const { return ___hashName_1; }
	inline String_t** get_address_of_hashName_1() { return &___hashName_1; }
	inline void set_hashName_1(String_t* value)
	{
		___hashName_1 = value;
		Il2CppCodeGenWriteBarrier((&___hashName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef RSA_T2385438082_H
#define RSA_T2385438082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSA
struct  RSA_t2385438082  : public AsymmetricAlgorithm_t932037087
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSA_T2385438082_H
#ifndef SUBJECTKEYIDENTIFIEREXTENSION_T2404375272_H
#define SUBJECTKEYIDENTIFIEREXTENSION_T2404375272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension
struct  SubjectKeyIdentifierExtension_t2404375272  : public X509Extension_t3173393653
{
public:
	// System.Byte[] Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::ski
	ByteU5BU5D_t4116647657* ___ski_3;

public:
	inline static int32_t get_offset_of_ski_3() { return static_cast<int32_t>(offsetof(SubjectKeyIdentifierExtension_t2404375272, ___ski_3)); }
	inline ByteU5BU5D_t4116647657* get_ski_3() const { return ___ski_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_ski_3() { return &___ski_3; }
	inline void set_ski_3(ByteU5BU5D_t4116647657* value)
	{
		___ski_3 = value;
		Il2CppCodeGenWriteBarrier((&___ski_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTKEYIDENTIFIEREXTENSION_T2404375272_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef DSA_T2386879874_H
#define DSA_T2386879874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSA
struct  DSA_t2386879874  : public AsymmetricAlgorithm_t932037087
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSA_T2386879874_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef KEYUSAGES_T820456313_H
#define KEYUSAGES_T820456313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsages
struct  KeyUsages_t820456313 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsages::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyUsages_t820456313, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGES_T820456313_H
#ifndef CRYPTOGRAPHICEXCEPTION_T248831461_H
#define CRYPTOGRAPHICEXCEPTION_T248831461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptographicException
struct  CryptographicException_t248831461  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOGRAPHICEXCEPTION_T248831461_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef X509CHAINSTATUSFLAGS_T1831553603_H
#define X509CHAINSTATUSFLAGS_T1831553603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509ChainStatusFlags
struct  X509ChainStatusFlags_t1831553603 
{
public:
	// System.Int32 Mono.Security.X509.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t1831553603, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T1831553603_H
#ifndef SPECIALFOLDER_T3871784040_H
#define SPECIALFOLDER_T3871784040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Environment/SpecialFolder
struct  SpecialFolder_t3871784040 
{
public:
	// System.Int32 System.Environment/SpecialFolder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialFolder_t3871784040, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALFOLDER_T3871784040_H
#ifndef FILEMODE_T1183438340_H
#define FILEMODE_T1183438340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileMode
struct  FileMode_t1183438340 
{
public:
	// System.Int32 System.IO.FileMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileMode_t1183438340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEMODE_T1183438340_H
#ifndef FILEACCESS_T1659085276_H
#define FILEACCESS_T1659085276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t1659085276 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAccess_t1659085276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T1659085276_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef FILESHARE_T3553318550_H
#define FILESHARE_T3553318550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileShare
struct  FileShare_t3553318550 
{
public:
	// System.Int32 System.IO.FileShare::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileShare_t3553318550, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESHARE_T3553318550_H
#ifndef MONOIOSTAT_T592533987_H
#define MONOIOSTAT_T592533987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t592533987 
{
public:
	// System.String System.IO.MonoIOStat::Name
	String_t* ___Name_0;
	// System.IO.FileAttributes System.IO.MonoIOStat::Attributes
	int32_t ___Attributes_1;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_2;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_3;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_4;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Length_2)); }
	inline int64_t get_Length_2() const { return ___Length_2; }
	inline int64_t* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(int64_t value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___CreationTime_3)); }
	inline int64_t get_CreationTime_3() const { return ___CreationTime_3; }
	inline int64_t* get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(int64_t value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastAccessTime_4)); }
	inline int64_t get_LastAccessTime_4() const { return ___LastAccessTime_4; }
	inline int64_t* get_address_of_LastAccessTime_4() { return &___LastAccessTime_4; }
	inline void set_LastAccessTime_4(int64_t value)
	{
		___LastAccessTime_4 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_5() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastWriteTime_5)); }
	inline int64_t get_LastWriteTime_5() const { return ___LastWriteTime_5; }
	inline int64_t* get_address_of_LastWriteTime_5() { return &___LastWriteTime_5; }
	inline void set_LastWriteTime_5(int64_t value)
	{
		___LastWriteTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
// Native definition for COM marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
#endif // MONOIOSTAT_T592533987_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef X509CHAIN_T863783601_H
#define X509CHAIN_T863783601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Chain
struct  X509Chain_t863783601  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::roots
	X509CertificateCollection_t1542168550 * ___roots_0;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::certs
	X509CertificateCollection_t1542168550 * ___certs_1;
	// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::_root
	X509Certificate_t489243025 * ____root_2;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::_chain
	X509CertificateCollection_t1542168550 * ____chain_3;
	// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::_status
	int32_t ____status_4;

public:
	inline static int32_t get_offset_of_roots_0() { return static_cast<int32_t>(offsetof(X509Chain_t863783601, ___roots_0)); }
	inline X509CertificateCollection_t1542168550 * get_roots_0() const { return ___roots_0; }
	inline X509CertificateCollection_t1542168550 ** get_address_of_roots_0() { return &___roots_0; }
	inline void set_roots_0(X509CertificateCollection_t1542168550 * value)
	{
		___roots_0 = value;
		Il2CppCodeGenWriteBarrier((&___roots_0), value);
	}

	inline static int32_t get_offset_of_certs_1() { return static_cast<int32_t>(offsetof(X509Chain_t863783601, ___certs_1)); }
	inline X509CertificateCollection_t1542168550 * get_certs_1() const { return ___certs_1; }
	inline X509CertificateCollection_t1542168550 ** get_address_of_certs_1() { return &___certs_1; }
	inline void set_certs_1(X509CertificateCollection_t1542168550 * value)
	{
		___certs_1 = value;
		Il2CppCodeGenWriteBarrier((&___certs_1), value);
	}

	inline static int32_t get_offset_of__root_2() { return static_cast<int32_t>(offsetof(X509Chain_t863783601, ____root_2)); }
	inline X509Certificate_t489243025 * get__root_2() const { return ____root_2; }
	inline X509Certificate_t489243025 ** get_address_of__root_2() { return &____root_2; }
	inline void set__root_2(X509Certificate_t489243025 * value)
	{
		____root_2 = value;
		Il2CppCodeGenWriteBarrier((&____root_2), value);
	}

	inline static int32_t get_offset_of__chain_3() { return static_cast<int32_t>(offsetof(X509Chain_t863783601, ____chain_3)); }
	inline X509CertificateCollection_t1542168550 * get__chain_3() const { return ____chain_3; }
	inline X509CertificateCollection_t1542168550 ** get_address_of__chain_3() { return &____chain_3; }
	inline void set__chain_3(X509CertificateCollection_t1542168550 * value)
	{
		____chain_3 = value;
		Il2CppCodeGenWriteBarrier((&____chain_3), value);
	}

	inline static int32_t get_offset_of__status_4() { return static_cast<int32_t>(offsetof(X509Chain_t863783601, ____status_4)); }
	inline int32_t get__status_4() const { return ____status_4; }
	inline int32_t* get_address_of__status_4() { return &____status_4; }
	inline void set__status_4(int32_t value)
	{
		____status_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T863783601_H
#ifndef FILESTREAM_T4292183065_H
#define FILESTREAM_T4292183065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t4292183065  : public Stream_t1273022909
{
public:
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_1;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_2;
	// System.Boolean System.IO.FileStream::async
	bool ___async_3;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_4;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_5;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_6;
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t4116647657* ___buf_7;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_8;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_9;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_10;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_11;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_12;
	// System.String System.IO.FileStream::name
	String_t* ___name_13;
	// System.IntPtr System.IO.FileStream::handle
	intptr_t ___handle_14;

public:
	inline static int32_t get_offset_of_access_1() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___access_1)); }
	inline int32_t get_access_1() const { return ___access_1; }
	inline int32_t* get_address_of_access_1() { return &___access_1; }
	inline void set_access_1(int32_t value)
	{
		___access_1 = value;
	}

	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___owner_2)); }
	inline bool get_owner_2() const { return ___owner_2; }
	inline bool* get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(bool value)
	{
		___owner_2 = value;
	}

	inline static int32_t get_offset_of_async_3() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___async_3)); }
	inline bool get_async_3() const { return ___async_3; }
	inline bool* get_address_of_async_3() { return &___async_3; }
	inline void set_async_3(bool value)
	{
		___async_3 = value;
	}

	inline static int32_t get_offset_of_canseek_4() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___canseek_4)); }
	inline bool get_canseek_4() const { return ___canseek_4; }
	inline bool* get_address_of_canseek_4() { return &___canseek_4; }
	inline void set_canseek_4(bool value)
	{
		___canseek_4 = value;
	}

	inline static int32_t get_offset_of_append_startpos_5() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___append_startpos_5)); }
	inline int64_t get_append_startpos_5() const { return ___append_startpos_5; }
	inline int64_t* get_address_of_append_startpos_5() { return &___append_startpos_5; }
	inline void set_append_startpos_5(int64_t value)
	{
		___append_startpos_5 = value;
	}

	inline static int32_t get_offset_of_anonymous_6() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___anonymous_6)); }
	inline bool get_anonymous_6() const { return ___anonymous_6; }
	inline bool* get_address_of_anonymous_6() { return &___anonymous_6; }
	inline void set_anonymous_6(bool value)
	{
		___anonymous_6 = value;
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_7)); }
	inline ByteU5BU5D_t4116647657* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_t4116647657* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_buf_size_8() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_size_8)); }
	inline int32_t get_buf_size_8() const { return ___buf_size_8; }
	inline int32_t* get_address_of_buf_size_8() { return &___buf_size_8; }
	inline void set_buf_size_8(int32_t value)
	{
		___buf_size_8 = value;
	}

	inline static int32_t get_offset_of_buf_length_9() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_length_9)); }
	inline int32_t get_buf_length_9() const { return ___buf_length_9; }
	inline int32_t* get_address_of_buf_length_9() { return &___buf_length_9; }
	inline void set_buf_length_9(int32_t value)
	{
		___buf_length_9 = value;
	}

	inline static int32_t get_offset_of_buf_offset_10() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_offset_10)); }
	inline int32_t get_buf_offset_10() const { return ___buf_offset_10; }
	inline int32_t* get_address_of_buf_offset_10() { return &___buf_offset_10; }
	inline void set_buf_offset_10(int32_t value)
	{
		___buf_offset_10 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_11() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_dirty_11)); }
	inline bool get_buf_dirty_11() const { return ___buf_dirty_11; }
	inline bool* get_address_of_buf_dirty_11() { return &___buf_dirty_11; }
	inline void set_buf_dirty_11(bool value)
	{
		___buf_dirty_11 = value;
	}

	inline static int32_t get_offset_of_buf_start_12() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_start_12)); }
	inline int64_t get_buf_start_12() const { return ___buf_start_12; }
	inline int64_t* get_address_of_buf_start_12() { return &___buf_start_12; }
	inline void set_buf_start_12(int64_t value)
	{
		___buf_start_12 = value;
	}

	inline static int32_t get_offset_of_name_13() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___name_13)); }
	inline String_t* get_name_13() const { return ___name_13; }
	inline String_t** get_address_of_name_13() { return &___name_13; }
	inline void set_name_13(String_t* value)
	{
		___name_13 = value;
		Il2CppCodeGenWriteBarrier((&___name_13), value);
	}

	inline static int32_t get_offset_of_handle_14() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___handle_14)); }
	inline intptr_t get_handle_14() const { return ___handle_14; }
	inline intptr_t* get_address_of_handle_14() { return &___handle_14; }
	inline void set_handle_14(intptr_t value)
	{
		___handle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_T4292183065_H
#ifndef X509CRL_T1148767388_H
#define X509CRL_T1148767388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Crl
struct  X509Crl_t1148767388  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Crl::issuer
	String_t* ___issuer_0;
	// System.Byte Mono.Security.X509.X509Crl::version
	uint8_t ___version_1;
	// System.DateTime Mono.Security.X509.X509Crl::thisUpdate
	DateTime_t3738529785  ___thisUpdate_2;
	// System.DateTime Mono.Security.X509.X509Crl::nextUpdate
	DateTime_t3738529785  ___nextUpdate_3;
	// System.Collections.ArrayList Mono.Security.X509.X509Crl::entries
	ArrayList_t2718874744 * ___entries_4;
	// System.String Mono.Security.X509.X509Crl::signatureOID
	String_t* ___signatureOID_5;
	// System.Byte[] Mono.Security.X509.X509Crl::signature
	ByteU5BU5D_t4116647657* ___signature_6;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::extensions
	X509ExtensionCollection_t609554709 * ___extensions_7;
	// System.Byte[] Mono.Security.X509.X509Crl::encoded
	ByteU5BU5D_t4116647657* ___encoded_8;
	// System.Byte[] Mono.Security.X509.X509Crl::hash_value
	ByteU5BU5D_t4116647657* ___hash_value_9;

public:
	inline static int32_t get_offset_of_issuer_0() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___issuer_0)); }
	inline String_t* get_issuer_0() const { return ___issuer_0; }
	inline String_t** get_address_of_issuer_0() { return &___issuer_0; }
	inline void set_issuer_0(String_t* value)
	{
		___issuer_0 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___version_1)); }
	inline uint8_t get_version_1() const { return ___version_1; }
	inline uint8_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint8_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_thisUpdate_2() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___thisUpdate_2)); }
	inline DateTime_t3738529785  get_thisUpdate_2() const { return ___thisUpdate_2; }
	inline DateTime_t3738529785 * get_address_of_thisUpdate_2() { return &___thisUpdate_2; }
	inline void set_thisUpdate_2(DateTime_t3738529785  value)
	{
		___thisUpdate_2 = value;
	}

	inline static int32_t get_offset_of_nextUpdate_3() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___nextUpdate_3)); }
	inline DateTime_t3738529785  get_nextUpdate_3() const { return ___nextUpdate_3; }
	inline DateTime_t3738529785 * get_address_of_nextUpdate_3() { return &___nextUpdate_3; }
	inline void set_nextUpdate_3(DateTime_t3738529785  value)
	{
		___nextUpdate_3 = value;
	}

	inline static int32_t get_offset_of_entries_4() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___entries_4)); }
	inline ArrayList_t2718874744 * get_entries_4() const { return ___entries_4; }
	inline ArrayList_t2718874744 ** get_address_of_entries_4() { return &___entries_4; }
	inline void set_entries_4(ArrayList_t2718874744 * value)
	{
		___entries_4 = value;
		Il2CppCodeGenWriteBarrier((&___entries_4), value);
	}

	inline static int32_t get_offset_of_signatureOID_5() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___signatureOID_5)); }
	inline String_t* get_signatureOID_5() const { return ___signatureOID_5; }
	inline String_t** get_address_of_signatureOID_5() { return &___signatureOID_5; }
	inline void set_signatureOID_5(String_t* value)
	{
		___signatureOID_5 = value;
		Il2CppCodeGenWriteBarrier((&___signatureOID_5), value);
	}

	inline static int32_t get_offset_of_signature_6() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___signature_6)); }
	inline ByteU5BU5D_t4116647657* get_signature_6() const { return ___signature_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_signature_6() { return &___signature_6; }
	inline void set_signature_6(ByteU5BU5D_t4116647657* value)
	{
		___signature_6 = value;
		Il2CppCodeGenWriteBarrier((&___signature_6), value);
	}

	inline static int32_t get_offset_of_extensions_7() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___extensions_7)); }
	inline X509ExtensionCollection_t609554709 * get_extensions_7() const { return ___extensions_7; }
	inline X509ExtensionCollection_t609554709 ** get_address_of_extensions_7() { return &___extensions_7; }
	inline void set_extensions_7(X509ExtensionCollection_t609554709 * value)
	{
		___extensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_7), value);
	}

	inline static int32_t get_offset_of_encoded_8() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___encoded_8)); }
	inline ByteU5BU5D_t4116647657* get_encoded_8() const { return ___encoded_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_encoded_8() { return &___encoded_8; }
	inline void set_encoded_8(ByteU5BU5D_t4116647657* value)
	{
		___encoded_8 = value;
		Il2CppCodeGenWriteBarrier((&___encoded_8), value);
	}

	inline static int32_t get_offset_of_hash_value_9() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388, ___hash_value_9)); }
	inline ByteU5BU5D_t4116647657* get_hash_value_9() const { return ___hash_value_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_hash_value_9() { return &___hash_value_9; }
	inline void set_hash_value_9(ByteU5BU5D_t4116647657* value)
	{
		___hash_value_9 = value;
		Il2CppCodeGenWriteBarrier((&___hash_value_9), value);
	}
};

struct X509Crl_t1148767388_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Crl::<>f__switch$map12
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map12_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Crl::<>f__switch$map13
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map13_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_10() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388_StaticFields, ___U3CU3Ef__switchU24map12_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map12_10() const { return ___U3CU3Ef__switchU24map12_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map12_10() { return &___U3CU3Ef__switchU24map12_10; }
	inline void set_U3CU3Ef__switchU24map12_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map12_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_11() { return static_cast<int32_t>(offsetof(X509Crl_t1148767388_StaticFields, ___U3CU3Ef__switchU24map13_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map13_11() const { return ___U3CU3Ef__switchU24map13_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map13_11() { return &___U3CU3Ef__switchU24map13_11; }
	inline void set_U3CU3Ef__switchU24map13_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map13_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRL_T1148767388_H
#ifndef X509CRLENTRY_T645568789_H
#define X509CRLENTRY_T645568789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Crl/X509CrlEntry
struct  X509CrlEntry_t645568789  : public RuntimeObject
{
public:
	// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::sn
	ByteU5BU5D_t4116647657* ___sn_0;
	// System.DateTime Mono.Security.X509.X509Crl/X509CrlEntry::revocationDate
	DateTime_t3738529785  ___revocationDate_1;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl/X509CrlEntry::extensions
	X509ExtensionCollection_t609554709 * ___extensions_2;

public:
	inline static int32_t get_offset_of_sn_0() { return static_cast<int32_t>(offsetof(X509CrlEntry_t645568789, ___sn_0)); }
	inline ByteU5BU5D_t4116647657* get_sn_0() const { return ___sn_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_sn_0() { return &___sn_0; }
	inline void set_sn_0(ByteU5BU5D_t4116647657* value)
	{
		___sn_0 = value;
		Il2CppCodeGenWriteBarrier((&___sn_0), value);
	}

	inline static int32_t get_offset_of_revocationDate_1() { return static_cast<int32_t>(offsetof(X509CrlEntry_t645568789, ___revocationDate_1)); }
	inline DateTime_t3738529785  get_revocationDate_1() const { return ___revocationDate_1; }
	inline DateTime_t3738529785 * get_address_of_revocationDate_1() { return &___revocationDate_1; }
	inline void set_revocationDate_1(DateTime_t3738529785  value)
	{
		___revocationDate_1 = value;
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(X509CrlEntry_t645568789, ___extensions_2)); }
	inline X509ExtensionCollection_t609554709 * get_extensions_2() const { return ___extensions_2; }
	inline X509ExtensionCollection_t609554709 ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(X509ExtensionCollection_t609554709 * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRLENTRY_T645568789_H
#ifndef X509CERTIFICATE_T489243025_H
#define X509CERTIFICATE_T489243025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Certificate
struct  X509Certificate_t489243025  : public RuntimeObject
{
public:
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::decoder
	ASN1_t2114160833 * ___decoder_0;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_encodedcert
	ByteU5BU5D_t4116647657* ___m_encodedcert_1;
	// System.DateTime Mono.Security.X509.X509Certificate::m_from
	DateTime_t3738529785  ___m_from_2;
	// System.DateTime Mono.Security.X509.X509Certificate::m_until
	DateTime_t3738529785  ___m_until_3;
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::issuer
	ASN1_t2114160833 * ___issuer_4;
	// System.String Mono.Security.X509.X509Certificate::m_issuername
	String_t* ___m_issuername_5;
	// System.String Mono.Security.X509.X509Certificate::m_keyalgo
	String_t* ___m_keyalgo_6;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_keyalgoparams
	ByteU5BU5D_t4116647657* ___m_keyalgoparams_7;
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::subject
	ASN1_t2114160833 * ___subject_8;
	// System.String Mono.Security.X509.X509Certificate::m_subject
	String_t* ___m_subject_9;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_publickey
	ByteU5BU5D_t4116647657* ___m_publickey_10;
	// System.Byte[] Mono.Security.X509.X509Certificate::signature
	ByteU5BU5D_t4116647657* ___signature_11;
	// System.String Mono.Security.X509.X509Certificate::m_signaturealgo
	String_t* ___m_signaturealgo_12;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_signaturealgoparams
	ByteU5BU5D_t4116647657* ___m_signaturealgoparams_13;
	// System.Byte[] Mono.Security.X509.X509Certificate::certhash
	ByteU5BU5D_t4116647657* ___certhash_14;
	// System.Security.Cryptography.RSA Mono.Security.X509.X509Certificate::_rsa
	RSA_t2385438082 * ____rsa_15;
	// System.Security.Cryptography.DSA Mono.Security.X509.X509Certificate::_dsa
	DSA_t2386879874 * ____dsa_16;
	// System.Int32 Mono.Security.X509.X509Certificate::version
	int32_t ___version_17;
	// System.Byte[] Mono.Security.X509.X509Certificate::serialnumber
	ByteU5BU5D_t4116647657* ___serialnumber_18;
	// System.Byte[] Mono.Security.X509.X509Certificate::issuerUniqueID
	ByteU5BU5D_t4116647657* ___issuerUniqueID_19;
	// System.Byte[] Mono.Security.X509.X509Certificate::subjectUniqueID
	ByteU5BU5D_t4116647657* ___subjectUniqueID_20;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Certificate::extensions
	X509ExtensionCollection_t609554709 * ___extensions_21;

public:
	inline static int32_t get_offset_of_decoder_0() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___decoder_0)); }
	inline ASN1_t2114160833 * get_decoder_0() const { return ___decoder_0; }
	inline ASN1_t2114160833 ** get_address_of_decoder_0() { return &___decoder_0; }
	inline void set_decoder_0(ASN1_t2114160833 * value)
	{
		___decoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_0), value);
	}

	inline static int32_t get_offset_of_m_encodedcert_1() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_encodedcert_1)); }
	inline ByteU5BU5D_t4116647657* get_m_encodedcert_1() const { return ___m_encodedcert_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_encodedcert_1() { return &___m_encodedcert_1; }
	inline void set_m_encodedcert_1(ByteU5BU5D_t4116647657* value)
	{
		___m_encodedcert_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_encodedcert_1), value);
	}

	inline static int32_t get_offset_of_m_from_2() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_from_2)); }
	inline DateTime_t3738529785  get_m_from_2() const { return ___m_from_2; }
	inline DateTime_t3738529785 * get_address_of_m_from_2() { return &___m_from_2; }
	inline void set_m_from_2(DateTime_t3738529785  value)
	{
		___m_from_2 = value;
	}

	inline static int32_t get_offset_of_m_until_3() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_until_3)); }
	inline DateTime_t3738529785  get_m_until_3() const { return ___m_until_3; }
	inline DateTime_t3738529785 * get_address_of_m_until_3() { return &___m_until_3; }
	inline void set_m_until_3(DateTime_t3738529785  value)
	{
		___m_until_3 = value;
	}

	inline static int32_t get_offset_of_issuer_4() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___issuer_4)); }
	inline ASN1_t2114160833 * get_issuer_4() const { return ___issuer_4; }
	inline ASN1_t2114160833 ** get_address_of_issuer_4() { return &___issuer_4; }
	inline void set_issuer_4(ASN1_t2114160833 * value)
	{
		___issuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_4), value);
	}

	inline static int32_t get_offset_of_m_issuername_5() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_issuername_5)); }
	inline String_t* get_m_issuername_5() const { return ___m_issuername_5; }
	inline String_t** get_address_of_m_issuername_5() { return &___m_issuername_5; }
	inline void set_m_issuername_5(String_t* value)
	{
		___m_issuername_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_issuername_5), value);
	}

	inline static int32_t get_offset_of_m_keyalgo_6() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_keyalgo_6)); }
	inline String_t* get_m_keyalgo_6() const { return ___m_keyalgo_6; }
	inline String_t** get_address_of_m_keyalgo_6() { return &___m_keyalgo_6; }
	inline void set_m_keyalgo_6(String_t* value)
	{
		___m_keyalgo_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyalgo_6), value);
	}

	inline static int32_t get_offset_of_m_keyalgoparams_7() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_keyalgoparams_7)); }
	inline ByteU5BU5D_t4116647657* get_m_keyalgoparams_7() const { return ___m_keyalgoparams_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_keyalgoparams_7() { return &___m_keyalgoparams_7; }
	inline void set_m_keyalgoparams_7(ByteU5BU5D_t4116647657* value)
	{
		___m_keyalgoparams_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyalgoparams_7), value);
	}

	inline static int32_t get_offset_of_subject_8() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___subject_8)); }
	inline ASN1_t2114160833 * get_subject_8() const { return ___subject_8; }
	inline ASN1_t2114160833 ** get_address_of_subject_8() { return &___subject_8; }
	inline void set_subject_8(ASN1_t2114160833 * value)
	{
		___subject_8 = value;
		Il2CppCodeGenWriteBarrier((&___subject_8), value);
	}

	inline static int32_t get_offset_of_m_subject_9() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_subject_9)); }
	inline String_t* get_m_subject_9() const { return ___m_subject_9; }
	inline String_t** get_address_of_m_subject_9() { return &___m_subject_9; }
	inline void set_m_subject_9(String_t* value)
	{
		___m_subject_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_subject_9), value);
	}

	inline static int32_t get_offset_of_m_publickey_10() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_publickey_10)); }
	inline ByteU5BU5D_t4116647657* get_m_publickey_10() const { return ___m_publickey_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_publickey_10() { return &___m_publickey_10; }
	inline void set_m_publickey_10(ByteU5BU5D_t4116647657* value)
	{
		___m_publickey_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_publickey_10), value);
	}

	inline static int32_t get_offset_of_signature_11() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___signature_11)); }
	inline ByteU5BU5D_t4116647657* get_signature_11() const { return ___signature_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_signature_11() { return &___signature_11; }
	inline void set_signature_11(ByteU5BU5D_t4116647657* value)
	{
		___signature_11 = value;
		Il2CppCodeGenWriteBarrier((&___signature_11), value);
	}

	inline static int32_t get_offset_of_m_signaturealgo_12() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_signaturealgo_12)); }
	inline String_t* get_m_signaturealgo_12() const { return ___m_signaturealgo_12; }
	inline String_t** get_address_of_m_signaturealgo_12() { return &___m_signaturealgo_12; }
	inline void set_m_signaturealgo_12(String_t* value)
	{
		___m_signaturealgo_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_signaturealgo_12), value);
	}

	inline static int32_t get_offset_of_m_signaturealgoparams_13() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___m_signaturealgoparams_13)); }
	inline ByteU5BU5D_t4116647657* get_m_signaturealgoparams_13() const { return ___m_signaturealgoparams_13; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_signaturealgoparams_13() { return &___m_signaturealgoparams_13; }
	inline void set_m_signaturealgoparams_13(ByteU5BU5D_t4116647657* value)
	{
		___m_signaturealgoparams_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_signaturealgoparams_13), value);
	}

	inline static int32_t get_offset_of_certhash_14() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___certhash_14)); }
	inline ByteU5BU5D_t4116647657* get_certhash_14() const { return ___certhash_14; }
	inline ByteU5BU5D_t4116647657** get_address_of_certhash_14() { return &___certhash_14; }
	inline void set_certhash_14(ByteU5BU5D_t4116647657* value)
	{
		___certhash_14 = value;
		Il2CppCodeGenWriteBarrier((&___certhash_14), value);
	}

	inline static int32_t get_offset_of__rsa_15() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ____rsa_15)); }
	inline RSA_t2385438082 * get__rsa_15() const { return ____rsa_15; }
	inline RSA_t2385438082 ** get_address_of__rsa_15() { return &____rsa_15; }
	inline void set__rsa_15(RSA_t2385438082 * value)
	{
		____rsa_15 = value;
		Il2CppCodeGenWriteBarrier((&____rsa_15), value);
	}

	inline static int32_t get_offset_of__dsa_16() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ____dsa_16)); }
	inline DSA_t2386879874 * get__dsa_16() const { return ____dsa_16; }
	inline DSA_t2386879874 ** get_address_of__dsa_16() { return &____dsa_16; }
	inline void set__dsa_16(DSA_t2386879874 * value)
	{
		____dsa_16 = value;
		Il2CppCodeGenWriteBarrier((&____dsa_16), value);
	}

	inline static int32_t get_offset_of_version_17() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___version_17)); }
	inline int32_t get_version_17() const { return ___version_17; }
	inline int32_t* get_address_of_version_17() { return &___version_17; }
	inline void set_version_17(int32_t value)
	{
		___version_17 = value;
	}

	inline static int32_t get_offset_of_serialnumber_18() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___serialnumber_18)); }
	inline ByteU5BU5D_t4116647657* get_serialnumber_18() const { return ___serialnumber_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_serialnumber_18() { return &___serialnumber_18; }
	inline void set_serialnumber_18(ByteU5BU5D_t4116647657* value)
	{
		___serialnumber_18 = value;
		Il2CppCodeGenWriteBarrier((&___serialnumber_18), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_19() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___issuerUniqueID_19)); }
	inline ByteU5BU5D_t4116647657* get_issuerUniqueID_19() const { return ___issuerUniqueID_19; }
	inline ByteU5BU5D_t4116647657** get_address_of_issuerUniqueID_19() { return &___issuerUniqueID_19; }
	inline void set_issuerUniqueID_19(ByteU5BU5D_t4116647657* value)
	{
		___issuerUniqueID_19 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_19), value);
	}

	inline static int32_t get_offset_of_subjectUniqueID_20() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___subjectUniqueID_20)); }
	inline ByteU5BU5D_t4116647657* get_subjectUniqueID_20() const { return ___subjectUniqueID_20; }
	inline ByteU5BU5D_t4116647657** get_address_of_subjectUniqueID_20() { return &___subjectUniqueID_20; }
	inline void set_subjectUniqueID_20(ByteU5BU5D_t4116647657* value)
	{
		___subjectUniqueID_20 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUniqueID_20), value);
	}

	inline static int32_t get_offset_of_extensions_21() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025, ___extensions_21)); }
	inline X509ExtensionCollection_t609554709 * get_extensions_21() const { return ___extensions_21; }
	inline X509ExtensionCollection_t609554709 ** get_address_of_extensions_21() { return &___extensions_21; }
	inline void set_extensions_21(X509ExtensionCollection_t609554709 * value)
	{
		___extensions_21 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_21), value);
	}
};

struct X509Certificate_t489243025_StaticFields
{
public:
	// System.String Mono.Security.X509.X509Certificate::encoding_error
	String_t* ___encoding_error_22;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switch$mapF
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapF_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switch$map10
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map10_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switch$map11
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map11_25;

public:
	inline static int32_t get_offset_of_encoding_error_22() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025_StaticFields, ___encoding_error_22)); }
	inline String_t* get_encoding_error_22() const { return ___encoding_error_22; }
	inline String_t** get_address_of_encoding_error_22() { return &___encoding_error_22; }
	inline void set_encoding_error_22(String_t* value)
	{
		___encoding_error_22 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_error_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_23() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025_StaticFields, ___U3CU3Ef__switchU24mapF_23)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapF_23() const { return ___U3CU3Ef__switchU24mapF_23; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapF_23() { return &___U3CU3Ef__switchU24mapF_23; }
	inline void set_U3CU3Ef__switchU24mapF_23(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapF_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_24() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025_StaticFields, ___U3CU3Ef__switchU24map10_24)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map10_24() const { return ___U3CU3Ef__switchU24map10_24; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map10_24() { return &___U3CU3Ef__switchU24map10_24; }
	inline void set_U3CU3Ef__switchU24map10_24(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map10_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map10_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_25() { return static_cast<int32_t>(offsetof(X509Certificate_t489243025_StaticFields, ___U3CU3Ef__switchU24map11_25)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map11_25() const { return ___U3CU3Ef__switchU24map11_25; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map11_25() { return &___U3CU3Ef__switchU24map11_25; }
	inline void set_U3CU3Ef__switchU24map11_25(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map11_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map11_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T489243025_H
#ifndef FILESYSTEMINFO_T3745885336_H
#define FILESYSTEMINFO_T3745885336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t3745885336  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_1;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_2;
	// System.IO.MonoIOStat System.IO.FileSystemInfo::stat
	MonoIOStat_t592533987  ___stat_3;
	// System.Boolean System.IO.FileSystemInfo::valid
	bool ___valid_4;

public:
	inline static int32_t get_offset_of_FullPath_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___FullPath_1)); }
	inline String_t* get_FullPath_1() const { return ___FullPath_1; }
	inline String_t** get_address_of_FullPath_1() { return &___FullPath_1; }
	inline void set_FullPath_1(String_t* value)
	{
		___FullPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_1), value);
	}

	inline static int32_t get_offset_of_OriginalPath_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___OriginalPath_2)); }
	inline String_t* get_OriginalPath_2() const { return ___OriginalPath_2; }
	inline String_t** get_address_of_OriginalPath_2() { return &___OriginalPath_2; }
	inline void set_OriginalPath_2(String_t* value)
	{
		___OriginalPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_2), value);
	}

	inline static int32_t get_offset_of_stat_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___stat_3)); }
	inline MonoIOStat_t592533987  get_stat_3() const { return ___stat_3; }
	inline MonoIOStat_t592533987 * get_address_of_stat_3() { return &___stat_3; }
	inline void set_stat_3(MonoIOStat_t592533987  value)
	{
		___stat_3 = value;
	}

	inline static int32_t get_offset_of_valid_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___valid_4)); }
	inline bool get_valid_4() const { return ___valid_4; }
	inline bool* get_address_of_valid_4() { return &___valid_4; }
	inline void set_valid_4(bool value)
	{
		___valid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T3745885336_H
#ifndef DIRECTORYINFO_T35957480_H
#define DIRECTORYINFO_T35957480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DirectoryInfo
struct  DirectoryInfo_t35957480  : public FileSystemInfo_t3745885336
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_5;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_6;

public:
	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___current_5)); }
	inline String_t* get_current_5() const { return ___current_5; }
	inline String_t** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(String_t* value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___parent_6)); }
	inline String_t* get_parent_6() const { return ___parent_6; }
	inline String_t** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(String_t* value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYINFO_T35957480_H
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t3108766909  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) X509Extension_t3173393653 * m_Items[1];

public:
	inline X509Extension_t3173393653 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline X509Extension_t3173393653 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, X509Extension_t3173393653 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline X509Extension_t3173393653 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline X509Extension_t3173393653 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, X509Extension_t3173393653 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m182537451_gshared (Dictionary_2_t3384741 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1279427033_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3959998165_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t* p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::.ctor()
extern "C"  void X509CertificateCollection__ctor_m2066277891 (X509CertificateCollection_t1542168550 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::.ctor()
extern "C"  void X509Chain__ctor_m3563800449 (X509Chain_t863783601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::AddRange(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509CertificateCollection_AddRange_m2165814476 (X509CertificateCollection_t1542168550 * __this, X509CertificateCollection_t1542168550 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
extern "C"  X509CertificateCollection_t1542168550 * X509StoreManager_get_TrustedRootCertificates_m2180997293 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509CertificateCollection::Add(Mono.Security.X509.X509Certificate)
extern "C"  int32_t X509CertificateCollection_Add_m2277657976 (X509CertificateCollection_t1542168550 * __this, X509Certificate_t489243025 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator Mono.Security.X509.X509CertificateCollection::GetEnumerator()
extern "C"  X509CertificateEnumerator_t3515934698 * X509CertificateCollection_GetEnumerator_m1275665495 (X509CertificateCollection_t1542168550 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::get_Current()
extern "C"  X509Certificate_t489243025 * X509CertificateEnumerator_get_Current_m1004537031 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::MoveNext()
extern "C"  bool X509CertificateEnumerator_MoveNext_m3925432749 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateParent(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t489243025 * X509Chain_FindCertificateParent_m2809823532 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___child0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Certificate::get_IsSelfSigned()
extern "C"  bool X509Certificate_get_IsSelfSigned_m4064195693 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateRoot(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t489243025 * X509Chain_FindCertificateRoot_m1937726457 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___potentialRoot0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.CollectionBase::get_Count()
extern "C"  int32_t CollectionBase_get_Count_m1708965601 (CollectionBase_t2727926298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509CertificateCollection::get_Item(System.Int32)
extern "C"  X509Certificate_t489243025 * X509CertificateCollection_get_Item_m3285563224 (X509CertificateCollection_t1542168550 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsParent(Mono.Security.X509.X509Certificate,Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsParent_m2689546349 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___child0, X509Certificate_t489243025 * ___parent1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsValid(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsValid_m3670863655 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___cert0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CollectionBase::Clear()
extern "C"  void CollectionBase_Clear_m1509125218 (CollectionBase_t2727926298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Certificate::get_IsCurrent()
extern "C"  bool X509Certificate_get_IsCurrent_m469817010 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_CheckCertificateRevocationList()
extern "C"  bool ServicePointManager_get_CheckCertificateRevocationList_m1716454075 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsTrusted(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsTrusted_m1243554942 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___potentialTrusted0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_TrustAnchors()
extern "C"  X509CertificateCollection_t1542168550 * X509Chain_get_TrustAnchors_m2434696767 (X509Chain_t863783601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509CertificateCollection::Contains(Mono.Security.X509.X509Certificate)
extern "C"  bool X509CertificateCollection_Contains_m743657353 (X509CertificateCollection_t1542168550 * __this, X509Certificate_t489243025 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509Certificate::get_Version()
extern "C"  int32_t X509Certificate_get_Version_m3419034307 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Certificate::get_Extensions()
extern "C"  X509ExtensionCollection_t609554709 * X509Certificate_get_Extensions_m2532937142 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.String)
extern "C"  X509Extension_t3173393653 * X509ExtensionCollection_get_Item_m4249795832 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C"  void BasicConstraintsExtension__ctor_m3191645544 (BasicConstraintsExtension_t2462195279 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::get_CertificateAuthority()
extern "C"  bool BasicConstraintsExtension_get_CertificateAuthority_m391198292 (BasicConstraintsExtension_t2462195279 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Certificate::VerifySignature(System.Security.Cryptography.RSA)
extern "C"  bool X509Certificate_VerifySignature_m3538124832 (X509Certificate_t489243025 * __this, RSA_t2385438082 * ___rsa0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array::Clone()
extern "C"  RuntimeObject * Array_Clone_m2672907798 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C"  void X509Crl_Parse_m3164013387 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___crl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.ASN1::.ctor(System.Byte[])
extern "C"  void ASN1__ctor_m1638893325 (ASN1_t2114160833 * __this, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.ASN1::get_Tag()
extern "C"  uint8_t ASN1_get_Tag_m2789147236 (ASN1_t2114160833 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.ASN1::get_Count()
extern "C"  int32_t ASN1_get_Count_m1789520042 (ASN1_t2114160833 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String)
extern "C"  void CryptographicException__ctor_m503735289 (CryptographicException_t248831461 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1::get_Item(System.Int32)
extern "C"  ASN1_t2114160833 * ASN1_get_Item_m2255075813 (ASN1_t2114160833 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.ASN1::get_Value()
extern "C"  ByteU5BU5D_t4116647657* ASN1_get_Value_m63296490 (ASN1_t2114160833 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.ASN1Convert::ToOid(Mono.Security.ASN1)
extern "C"  String_t* ASN1Convert_ToOid_m3847701408 (RuntimeObject * __this /* static, unused */, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X501::ToString(Mono.Security.ASN1)
extern "C"  String_t* X501_ToString_m2260475203 (RuntimeObject * __this /* static, unused */, ASN1_t2114160833 * ___seq0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.ASN1Convert::ToDateTime(Mono.Security.ASN1)
extern "C"  DateTime_t3738529785  ASN1Convert_ToDateTime_m1246060840 (RuntimeObject * __this /* static, unused */, ASN1_t2114160833 * ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(Mono.Security.ASN1)
extern "C"  void X509CrlEntry__ctor_m4013514048 (X509CrlEntry_t645568789 * __this, ASN1_t2114160833 * ___entry0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor(Mono.Security.ASN1)
extern "C"  void X509ExtensionCollection__ctor_m551870633 (X509ExtensionCollection_t609554709 * __this, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Buffer_BlockCopy_m2884209081 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::ReadOnly(System.Collections.ArrayList)
extern "C"  ArrayList_t2718874744 * ArrayList_ReadOnly_m1905796817 (RuntimeObject * __this /* static, unused */, ArrayList_t2718874744 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C"  X509CrlEntry_t645568789 * X509Crl_GetCrlEntry_m641501875 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___serialNumber0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C"  String_t* X509Crl_GetHashName_m4214678741 (X509Crl_t1148767388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HashAlgorithm::Create(System.String)
extern "C"  HashAlgorithm_t1432317219 * HashAlgorithm_Create_m644612360 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[])
extern "C"  ByteU5BU5D_t4116647657* HashAlgorithm_ComputeHash_m2825542963 (HashAlgorithm_t1432317219 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::get_Now()
extern "C"  DateTime_t3738529785  DateTime_get_Now_m1277138875 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::WasCurrent(System.DateTime)
extern "C"  bool X509Crl_WasCurrent_m662015296 (X509Crl_t1148767388 * __this, DateTime_t3738529785  ___instant0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_Equality(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_Equality_m1022058599 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_GreaterThanOrEqual(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_GreaterThanOrEqual_m674703316 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_LessThanOrEqual(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_LessThanOrEqual_m2360948759 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::get_SerialNumber()
extern "C"  ByteU5BU5D_t4116647657* X509CrlEntry_get_SerialNumber_m3627212772 (X509CrlEntry_t645568789 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C"  bool X509Crl_Compare_m3418726913 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___array10, ByteU5BU5D_t4116647657* ___array21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C"  void KeyUsageExtension__ctor_m3414452076 (KeyUsageExtension_t1795615912 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.Extensions.KeyUsageExtension::Support(Mono.Security.X509.Extensions.KeyUsages)
extern "C"  bool KeyUsageExtension_Support_m3508856672 (KeyUsageExtension_t1795615912 * __this, int32_t ___usage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2392909825(__this, p0, method) ((  void (*) (Dictionary_2_t2736202052 *, int32_t, const RuntimeMethod*))Dictionary_2__ctor_m182537451_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m282647386(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m1279427033_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1013208020(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2736202052 *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_m3959998165_gshared)(__this, p0, p1, method)
// System.Security.Cryptography.DSA Mono.Security.X509.X509Certificate::get_DSA()
extern "C"  DSA_t2386879874 * X509Certificate_get_DSA_m2644963799 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C"  bool X509Crl_VerifySignature_m1902456590 (X509Crl_t1148767388 * __this, DSA_t2386879874 * ___dsa0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C"  bool X509Crl_VerifySignature_m1808348256 (X509Crl_t1148767388 * __this, RSA_t2385438082 * ___rsa0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  void DSASignatureDeformatter__ctor_m2889130126 (DSASignatureDeformatter_t3677955172 * __this, AsymmetricAlgorithm_t932037087 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m1873195862 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_get_Hash_m464217067 (X509Crl_t1148767388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  void RSAPKCS1SignatureDeformatter__ctor_m3706544163 (RSAPKCS1SignatureDeformatter_t3767223008 * __this, AsymmetricAlgorithm_t932037087 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C"  FileStream_t4292183065 * File_Open_m3218582222 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C"  void X509Crl__ctor_m1817187405 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___crl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor()
extern "C"  void X509ExtensionCollection__ctor_m2474799343 (X509ExtensionCollection_t609554709 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Reverse(System.Array)
extern "C"  void Array_Reverse_m3714848183 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.ASN1::.ctor(System.Byte)
extern "C"  void ASN1__ctor_m1239252869 (ASN1_t2114160833 * __this, uint8_t ___tag0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.ASN1::.ctor(System.Byte,System.Byte[])
extern "C"  void ASN1__ctor_m682794872 (ASN1_t2114160833 * __this, uint8_t ___tag0, ByteU5BU5D_t4116647657* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1::Add(Mono.Security.ASN1)
extern "C"  ASN1_t2114160833 * ASN1_Add_m2431139999 (ASN1_t2114160833 * __this, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromDateTime(System.DateTime)
extern "C"  ASN1_t2114160833 * ASN1Convert_FromDateTime_m1024852799 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  ___dt0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509ExtensionCollection::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* X509ExtensionCollection_GetBytes_m1622025118 (X509ExtensionCollection_t609554709 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Locale::GetText(System.String)
extern "C"  String_t* Locale_GetText_m3520169047 (RuntimeObject * __this /* static, unused */, String_t* ___msg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.ASN1::get_Length()
extern "C"  int32_t ASN1_get_Length_m3269728307 (ASN1_t2114160833 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.ASN1::set_Value(System.Byte[])
extern "C"  void ASN1_set_Value_m647861841 (ASN1_t2114160833 * __this, ByteU5BU5D_t4116647657* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509Extension::get_Value()
extern "C"  ASN1_t2114160833 * X509Extension_get_Value_m3529546267 (X509Extension_t3173393653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Extension::get_Oid()
extern "C"  String_t* X509Extension_get_Oid_m1003388288 (X509Extension_t3173393653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Extension::get_Critical()
extern "C"  bool X509Extension_get_Critical_m2974578711 (X509Extension_t3173393653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromOid(System.String)
extern "C"  ASN1_t2114160833 * ASN1Convert_FromOid_m1517037532 (RuntimeObject * __this /* static, unused */, String_t* ___oid0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509Extension::get_ASN1()
extern "C"  ASN1_t2114160833 * X509Extension_get_ASN1_m1064344075 (X509Extension_t3173393653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::GetHashCode()
extern "C"  int32_t String_GetHashCode_m1906374149 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t4157843068 * CultureInfo_get_InvariantCulture_m3532445182 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Byte::ToString(System.String,System.IFormatProvider)
extern "C"  String_t* Byte_ToString_m4063101981 (uint8_t* __this, String_t* p0, RuntimeObject* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.Convert::ToChar(System.Byte)
extern "C"  Il2CppChar Convert_ToChar_m2532412511 (RuntimeObject * __this /* static, unused */, uint8_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_NewLine()
extern "C"  String_t* Environment_get_NewLine_m3211016485 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Extension::WriteLine(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C"  void X509Extension_WriteLine_m1662885247 (X509Extension_t3173393653 * __this, StringBuilder_t * ___sb0, int32_t ___n1, int32_t ___pos2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m3317489284 (StringBuilder_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CollectionBase::.ctor()
extern "C"  void CollectionBase__ctor_m3343513710 (CollectionBase_t2727926298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m1152696503 (Exception_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Extension::.ctor(Mono.Security.ASN1)
extern "C"  void X509Extension__ctor_m710637961 (X509Extension_t3173393653 * __this, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.CollectionBase::get_InnerList()
extern "C"  ArrayList_t2718874744 * CollectionBase_get_InnerList_m132195395 (CollectionBase_t2727926298 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.Int32)
extern "C"  X509Extension_t3173393653 * X509ExtensionCollection_get_Item_m2771335836 (X509ExtensionCollection_t609554709 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m2606992261 (X509ExtensionCollection_t609554709 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(System.String)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m2996504451 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::BuildCertificatesCollection(System.String)
extern "C"  X509CertificateCollection_t1542168550 * X509Store_BuildCertificatesCollection_m3030935583 (X509Store_t2777415284 * __this, String_t* ___storeName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::BuildCrlsCollection(System.String)
extern "C"  ArrayList_t2718874744 * X509Store_BuildCrlsCollection_m1991312527 (X509Store_t2777415284 * __this, String_t* ___storeName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.Char)
extern "C"  int32_t String_LastIndexOf_m3451222878 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Store::CheckStore(System.String,System.Boolean)
extern "C"  bool X509Store_CheckStore_m2045435685 (X509Store_t2777415284 * __this, String_t* ___path0, bool ___throwException1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Certificate)
extern "C"  String_t* X509Store_GetUniqueName_m4271638378 (X509Store_t2777415284 * __this, X509Certificate_t489243025 * ___certificate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m3943585060 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Create(System.String)
extern "C"  FileStream_t4292183065 * File_Create_m2207667142 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Crl)
extern "C"  String_t* X509Store_GetUniqueName_m3285060726 (X509Store_t2777415284 * __this, X509Crl_t1148767388 * ___crl0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_RawData()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_get_RawData_m3623997699 (X509Crl_t1148767388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Delete(System.String)
extern "C"  void File_Delete_m321251800 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509ExtensionCollection)
extern "C"  ByteU5BU5D_t4116647657* X509Store_GetUniqueName_m132964055 (X509Store_t2777415284 * __this, X509ExtensionCollection_t609554709 * ___extensions0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Certificate::get_Hash()
extern "C"  ByteU5BU5D_t4116647657* X509Certificate_get_Hash_m410033711 (X509Certificate_t489243025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(System.String,System.Byte[],System.String)
extern "C"  String_t* X509Store_GetUniqueName_m915074968 (X509Store_t2777415284 * __this, String_t* ___method0, ByteU5BU5D_t4116647657* ___name1, String_t* ___fileExtension2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C"  X509ExtensionCollection_t609554709 * X509Crl_get_Extensions_m922657393 (X509Crl_t1148767388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C"  void SubjectKeyIdentifierExtension__ctor_m2055470965 (SubjectKeyIdentifierExtension_t2404375272 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::get_Identifier()
extern "C"  ByteU5BU5D_t4116647657* SubjectKeyIdentifierExtension_get_Identifier_m3780825379 (SubjectKeyIdentifierExtension_t2404375272 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String)
extern "C"  void StringBuilder__ctor_m2989139009 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::OpenRead(System.String)
extern "C"  FileStream_t4292183065 * File_OpenRead_m2936789020 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Store::Load(System.String)
extern "C"  ByteU5BU5D_t4116647657* X509Store_Load_m2048139132 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Certificate::.ctor(System.Byte[])
extern "C"  void X509Certificate__ctor_m553243489 (X509Certificate_t489243025 * __this, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::Exists(System.String)
extern "C"  bool Directory_Exists_m1484791558 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
extern "C"  DirectoryInfo_t35957480 * Directory_CreateDirectory_m751642867 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String,System.String)
extern "C"  StringU5BU5D_t1281789340* Directory_GetFiles_m2624572368 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Store::LoadCertificate(System.String)
extern "C"  X509Certificate_t489243025 * X509Store_LoadCertificate_m1587806288 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Store::LoadCrl(System.String)
extern "C"  X509Crl_t1148767388 * X509Store_LoadCrl_m1881903843 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetFolderPath(System.Environment/SpecialFolder)
extern "C"  String_t* Environment_GetFolderPath_m327623990 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Stores::.ctor(System.String)
extern "C"  void X509Stores__ctor_m1786355972 (X509Stores_t1373936238 * __this, String_t* ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
extern "C"  X509Stores_t1373936238 * X509StoreManager_get_CurrentUser_m719101392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_IntermediateCA()
extern "C"  X509Store_t2777415284 * X509Stores_get_IntermediateCA_m1350619599 (X509Stores_t1373936238 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::get_Certificates()
extern "C"  X509CertificateCollection_t1542168550 * X509Store_get_Certificates_m1092347772 (X509Store_t2777415284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
extern "C"  X509Stores_t1373936238 * X509StoreManager_get_LocalMachine_m269504582 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::get_Crls()
extern "C"  ArrayList_t2718874744 * X509Store_get_Crls_m1211262034 (X509Store_t2777415284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_TrustedRoot()
extern "C"  X509Store_t2777415284 * X509Stores_get_TrustedRoot_m1736182879 (X509Stores_t1373936238 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_Untrusted()
extern "C"  X509Store_t2777415284 * X509Stores_get_Untrusted_m1029989579 (X509Stores_t1373936238 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::.ctor(System.String,System.Boolean)
extern "C"  void X509Store__ctor_m2736551756 (X509Store_t2777415284 * __this, String_t* ___path0, bool ___crl1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Clear()
extern "C"  void X509Store_Clear_m2126432876 (X509Store_t2777415284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.FormatException::.ctor(System.String)
extern "C"  void FormatException__ctor_m4049685996 (FormatException_t154580423 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1()
extern "C"  ASN1_t2114160833 * AttributeTypeAndValue_GetASN1_m1976348179 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X520/AttributeTypeAndValue::SelectBestEncoding()
extern "C"  uint8_t AttributeTypeAndValue_SelectBestEncoding_m4133162804 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
extern "C"  Encoding_t1523322056 * Encoding_get_ASCII_m3595602635 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_BigEndianUnicode()
extern "C"  Encoding_t1523322056 * Encoding_get_BigEndianUnicode_m684646764 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1(System.Byte)
extern "C"  ASN1_t2114160833 * AttributeTypeAndValue_GetASN1_m735511441 (AttributeTypeAndValue_t3245693428 * __this, uint8_t ___encoding0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32)
extern "C"  void AttributeTypeAndValue__ctor_m682681796 (AttributeTypeAndValue_t3245693428 * __this, String_t* ___oid0, int32_t ___upperBound1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32,System.Byte)
extern "C"  void AttributeTypeAndValue__ctor_m3579516111 (AttributeTypeAndValue_t3245693428 * __this, String_t* ___oid0, int32_t ___upperBound1, uint8_t ___encoding2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509CertificateEnumerator__ctor_m85694331 (X509CertificateEnumerator_t3515934698 * __this, X509CertificateCollection_t1542168550 * ___mappings0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator__ctor_m85694331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_0 = ___mappings0;
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_0);
		__this->set_enumerator_0(L_1);
		return;
	}
}
// System.Object Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1846030361 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1846030361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.MoveNext()
extern "C"  bool X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2626270621 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2626270621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.Reset()
extern "C"  void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m2039524926 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m2039524926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::get_Current()
extern "C"  X509Certificate_t489243025 * X509CertificateEnumerator_get_Current_m1004537031 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_get_Current_m1004537031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return ((X509Certificate_t489243025 *)CastclassClass((RuntimeObject*)L_1, X509Certificate_t489243025_il2cpp_TypeInfo_var));
	}
}
// System.Boolean Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::MoveNext()
extern "C"  bool X509CertificateEnumerator_MoveNext_m3925432749 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_MoveNext_m3925432749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::Reset()
extern "C"  void X509CertificateEnumerator_Reset_m1825523691 (X509CertificateEnumerator_t3515934698 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CertificateEnumerator_Reset_m1825523691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_enumerator_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Chain::.ctor()
extern "C"  void X509Chain__ctor_m3563800449 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain__ctor_m3563800449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		__this->set_certs_1(L_0);
		return;
	}
}
// System.Void Mono.Security.X509.X509Chain::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain__ctor_m1084071882 (X509Chain_t863783601 * __this, X509CertificateCollection_t1542168550 * ___chain0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain__ctor_m1084071882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Chain__ctor_m3563800449(__this, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		__this->set__chain_3(L_0);
		X509CertificateCollection_t1542168550 * L_1 = __this->get__chain_3();
		X509CertificateCollection_t1542168550 * L_2 = ___chain0;
		NullCheck(L_1);
		X509CertificateCollection_AddRange_m2165814476(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_Chain()
extern "C"  X509CertificateCollection_t1542168550 * X509Chain_get_Chain_m2770029413 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get__chain_3();
		return L_0;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::get_Root()
extern "C"  X509Certificate_t489243025 * X509Chain_get_Root_m549030582 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	{
		X509Certificate_t489243025 * L_0 = __this->get__root_2();
		return L_0;
	}
}
// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::get_Status()
extern "C"  int32_t X509Chain_get_Status_m348797749 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__status_4();
		return L_0;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_TrustAnchors()
extern "C"  X509CertificateCollection_t1542168550 * X509Chain_get_TrustAnchors_m2434696767 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_get_TrustAnchors_m2434696767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get_roots_0();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_1 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_1, /*hidden argument*/NULL);
		__this->set_roots_0(L_1);
		X509CertificateCollection_t1542168550 * L_2 = __this->get_roots_0();
		X509CertificateCollection_t1542168550 * L_3 = X509StoreManager_get_TrustedRootCertificates_m2180997293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509CertificateCollection_AddRange_m2165814476(L_2, L_3, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_4 = __this->get_roots_0();
		return L_4;
	}

IL_002d:
	{
		X509CertificateCollection_t1542168550 * L_5 = __this->get_roots_0();
		return L_5;
	}
}
// System.Void Mono.Security.X509.X509Chain::set_TrustAnchors(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_set_TrustAnchors_m2894876452 (X509Chain_t863783601 * __this, X509CertificateCollection_t1542168550 * ___value0, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = ___value0;
		__this->set_roots_0(L_0);
		return;
	}
}
// System.Void Mono.Security.X509.X509Chain::LoadCertificate(Mono.Security.X509.X509Certificate)
extern "C"  void X509Chain_LoadCertificate_m4250821044 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___x5090, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get_certs_1();
		X509Certificate_t489243025 * L_1 = ___x5090;
		NullCheck(L_0);
		X509CertificateCollection_Add_m2277657976(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.X509.X509Chain::LoadCertificates(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_LoadCertificates_m749246668 (X509Chain_t863783601 * __this, X509CertificateCollection_t1542168550 * ___collection0, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get_certs_1();
		X509CertificateCollection_t1542168550 * L_1 = ___collection0;
		NullCheck(L_0);
		X509CertificateCollection_AddRange_m2165814476(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindByIssuerName(System.String)
extern "C"  X509Certificate_t489243025 * X509Chain_FindByIssuerName_m2280432457 (X509Chain_t863783601 * __this, String_t* ___issuerName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_FindByIssuerName_m2280432457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Certificate_t489243025 * V_0 = NULL;
	X509CertificateEnumerator_t3515934698 * V_1 = NULL;
	X509Certificate_t489243025 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get_certs_1();
		NullCheck(L_0);
		X509CertificateEnumerator_t3515934698 * L_1 = X509CertificateCollection_GetEnumerator_m1275665495(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0011:
		{
			X509CertificateEnumerator_t3515934698 * L_2 = V_1;
			NullCheck(L_2);
			X509Certificate_t489243025 * L_3 = X509CertificateEnumerator_get_Current_m1004537031(L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			X509Certificate_t489243025 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String Mono.Security.X509.X509Certificate::get_IssuerName() */, L_4);
			String_t* L_6 = ___issuerName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0030;
			}
		}

IL_0029:
		{
			X509Certificate_t489243025 * L_8 = V_0;
			V_2 = L_8;
			IL2CPP_LEAVE(0x54, FINALLY_0040);
		}

IL_0030:
		{
			X509CertificateEnumerator_t3515934698 * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = X509CertificateEnumerator_MoveNext_m3925432749(L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_0011;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			X509CertificateEnumerator_t3515934698 * L_11 = V_1;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			RuntimeObject* L_12 = V_3;
			if (L_12)
			{
				goto IL_004b;
			}
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_004b:
		{
			RuntimeObject* L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0052:
	{
		return (X509Certificate_t489243025 *)NULL;
	}

IL_0054:
	{
		X509Certificate_t489243025 * L_14 = V_2;
		return L_14;
	}
}
// System.Boolean Mono.Security.X509.X509Chain::Build(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_Build_m2469702749 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___leaf0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_Build_m2469702749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Certificate_t489243025 * V_0 = NULL;
	X509Certificate_t489243025 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	X509Certificate_t489243025 * V_4 = NULL;
	X509CertificateEnumerator_t3515934698 * V_5 = NULL;
	bool V_6 = false;
	RuntimeObject* V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set__status_4(0);
		X509CertificateCollection_t1542168550 * L_0 = __this->get__chain_3();
		if (L_0)
		{
			goto IL_0060;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_1 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_1, /*hidden argument*/NULL);
		__this->set__chain_3(L_1);
		X509Certificate_t489243025 * L_2 = ___leaf0;
		V_0 = L_2;
		X509Certificate_t489243025 * L_3 = V_0;
		V_1 = L_3;
		goto IL_003d;
	}

IL_0026:
	{
		X509Certificate_t489243025 * L_4 = V_0;
		V_1 = L_4;
		X509CertificateCollection_t1542168550 * L_5 = __this->get__chain_3();
		X509Certificate_t489243025 * L_6 = V_0;
		NullCheck(L_5);
		X509CertificateCollection_Add_m2277657976(L_5, L_6, /*hidden argument*/NULL);
		X509Certificate_t489243025 * L_7 = V_0;
		X509Certificate_t489243025 * L_8 = X509Chain_FindCertificateParent_m2809823532(__this, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003d:
	{
		X509Certificate_t489243025 * L_9 = V_0;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		X509Certificate_t489243025 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = X509Certificate_get_IsSelfSigned_m4064195693(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0026;
		}
	}

IL_004e:
	{
		X509Certificate_t489243025 * L_12 = V_1;
		X509Certificate_t489243025 * L_13 = X509Chain_FindCertificateRoot_m1937726457(__this, L_12, /*hidden argument*/NULL);
		__this->set__root_2(L_13);
		goto IL_00fa;
	}

IL_0060:
	{
		X509CertificateCollection_t1542168550 * L_14 = __this->get__chain_3();
		NullCheck(L_14);
		int32_t L_15 = CollectionBase_get_Count_m1708965601(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = V_2;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		X509Certificate_t489243025 * L_17 = ___leaf0;
		X509CertificateCollection_t1542168550 * L_18 = __this->get__chain_3();
		NullCheck(L_18);
		X509Certificate_t489243025 * L_19 = X509CertificateCollection_get_Item_m3285563224(L_18, 0, /*hidden argument*/NULL);
		bool L_20 = X509Chain_IsParent_m2689546349(__this, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e8;
		}
	}
	{
		V_3 = 1;
		goto IL_00c0;
	}

IL_0092:
	{
		X509CertificateCollection_t1542168550 * L_21 = __this->get__chain_3();
		int32_t L_22 = V_3;
		NullCheck(L_21);
		X509Certificate_t489243025 * L_23 = X509CertificateCollection_get_Item_m3285563224(L_21, ((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1)), /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_24 = __this->get__chain_3();
		int32_t L_25 = V_3;
		NullCheck(L_24);
		X509Certificate_t489243025 * L_26 = X509CertificateCollection_get_Item_m3285563224(L_24, L_25, /*hidden argument*/NULL);
		bool L_27 = X509Chain_IsParent_m2689546349(__this, L_23, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00bc;
		}
	}
	{
		goto IL_00c7;
	}

IL_00bc:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_00c0:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_2;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_0092;
		}
	}

IL_00c7:
	{
		int32_t L_31 = V_3;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_00e8;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_33 = __this->get__chain_3();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		X509Certificate_t489243025 * L_35 = X509CertificateCollection_get_Item_m3285563224(L_33, ((int32_t)il2cpp_codegen_subtract((int32_t)L_34, (int32_t)1)), /*hidden argument*/NULL);
		X509Certificate_t489243025 * L_36 = X509Chain_FindCertificateRoot_m1937726457(__this, L_35, /*hidden argument*/NULL);
		__this->set__root_2(L_36);
	}

IL_00e8:
	{
		goto IL_00fa;
	}

IL_00ed:
	{
		X509Certificate_t489243025 * L_37 = ___leaf0;
		X509Certificate_t489243025 * L_38 = X509Chain_FindCertificateRoot_m1937726457(__this, L_37, /*hidden argument*/NULL);
		__this->set__root_2(L_38);
	}

IL_00fa:
	{
		X509CertificateCollection_t1542168550 * L_39 = __this->get__chain_3();
		if (!L_39)
		{
			goto IL_01a6;
		}
	}
	{
		int32_t L_40 = __this->get__status_4();
		if (L_40)
		{
			goto IL_01a6;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_41 = __this->get__chain_3();
		NullCheck(L_41);
		X509CertificateEnumerator_t3515934698 * L_42 = X509CertificateCollection_GetEnumerator_m1275665495(L_41, /*hidden argument*/NULL);
		V_5 = L_42;
	}

IL_011d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0140;
		}

IL_0122:
		{
			X509CertificateEnumerator_t3515934698 * L_43 = V_5;
			NullCheck(L_43);
			X509Certificate_t489243025 * L_44 = X509CertificateEnumerator_get_Current_m1004537031(L_43, /*hidden argument*/NULL);
			V_4 = L_44;
			X509Certificate_t489243025 * L_45 = V_4;
			bool L_46 = X509Chain_IsValid_m3670863655(__this, L_45, /*hidden argument*/NULL);
			if (L_46)
			{
				goto IL_0140;
			}
		}

IL_0138:
		{
			V_6 = (bool)0;
			IL2CPP_LEAVE(0x1B0, FINALLY_0151);
		}

IL_0140:
		{
			X509CertificateEnumerator_t3515934698 * L_47 = V_5;
			NullCheck(L_47);
			bool L_48 = X509CertificateEnumerator_MoveNext_m3925432749(L_47, /*hidden argument*/NULL);
			if (L_48)
			{
				goto IL_0122;
			}
		}

IL_014c:
		{
			IL2CPP_LEAVE(0x167, FINALLY_0151);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0151;
	}

FINALLY_0151:
	{ // begin finally (depth: 1)
		{
			X509CertificateEnumerator_t3515934698 * L_49 = V_5;
			V_7 = ((RuntimeObject*)IsInst((RuntimeObject*)L_49, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			RuntimeObject* L_50 = V_7;
			if (L_50)
			{
				goto IL_015f;
			}
		}

IL_015e:
		{
			IL2CPP_END_FINALLY(337)
		}

IL_015f:
		{
			RuntimeObject* L_51 = V_7;
			NullCheck(L_51);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_51);
			IL2CPP_END_FINALLY(337)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(337)
	{
		IL2CPP_JUMP_TBL(0x1B0, IL_01b0)
		IL2CPP_JUMP_TBL(0x167, IL_0167)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0167:
	{
		X509Certificate_t489243025 * L_52 = ___leaf0;
		bool L_53 = X509Chain_IsValid_m3670863655(__this, L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_0188;
		}
	}
	{
		int32_t L_54 = __this->get__status_4();
		if ((!(((uint32_t)L_54) == ((uint32_t)2))))
		{
			goto IL_0186;
		}
	}
	{
		__this->set__status_4(1);
	}

IL_0186:
	{
		return (bool)0;
	}

IL_0188:
	{
		X509Certificate_t489243025 * L_55 = __this->get__root_2();
		if (!L_55)
		{
			goto IL_01a6;
		}
	}
	{
		X509Certificate_t489243025 * L_56 = __this->get__root_2();
		bool L_57 = X509Chain_IsValid_m3670863655(__this, L_56, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_01a6;
		}
	}
	{
		return (bool)0;
	}

IL_01a6:
	{
		int32_t L_58 = __this->get__status_4();
		return (bool)((((int32_t)L_58) == ((int32_t)0))? 1 : 0);
	}

IL_01b0:
	{
		bool L_59 = V_6;
		return L_59;
	}
}
// System.Void Mono.Security.X509.X509Chain::Reset()
extern "C"  void X509Chain_Reset_m2222574256 (X509Chain_t863783601 * __this, const RuntimeMethod* method)
{
	{
		__this->set__status_4(0);
		__this->set_roots_0((X509CertificateCollection_t1542168550 *)NULL);
		X509CertificateCollection_t1542168550 * L_0 = __this->get_certs_1();
		NullCheck(L_0);
		CollectionBase_Clear_m1509125218(L_0, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_1 = __this->get__chain_3();
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_2 = __this->get__chain_3();
		NullCheck(L_2);
		CollectionBase_Clear_m1509125218(L_2, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean Mono.Security.X509.X509Chain::IsValid(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsValid_m3670863655 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___cert0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_IsValid_m3670863655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Certificate_t489243025 * L_0 = ___cert0;
		NullCheck(L_0);
		bool L_1 = X509Certificate_get_IsCurrent_m469817010(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		__this->set__status_4(2);
		return (bool)0;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ServicePointManager_t170559685_il2cpp_TypeInfo_var);
		bool L_2 = ServicePointManager_get_CheckCertificateRevocationList_m1716454075(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}

IL_001e:
	{
		return (bool)1;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateParent(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t489243025 * X509Chain_FindCertificateParent_m2809823532 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___child0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_FindCertificateParent_m2809823532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Certificate_t489243025 * V_0 = NULL;
	X509CertificateEnumerator_t3515934698 * V_1 = NULL;
	X509Certificate_t489243025 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get_certs_1();
		NullCheck(L_0);
		X509CertificateEnumerator_t3515934698 * L_1 = X509CertificateCollection_GetEnumerator_m1275665495(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			X509CertificateEnumerator_t3515934698 * L_2 = V_1;
			NullCheck(L_2);
			X509Certificate_t489243025 * L_3 = X509CertificateEnumerator_get_Current_m1004537031(L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			X509Certificate_t489243025 * L_4 = ___child0;
			X509Certificate_t489243025 * L_5 = V_0;
			bool L_6 = X509Chain_IsParent_m2689546349(__this, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			X509Certificate_t489243025 * L_7 = V_0;
			V_2 = L_7;
			IL2CPP_LEAVE(0x50, FINALLY_003c);
		}

IL_002c:
		{
			X509CertificateEnumerator_t3515934698 * L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = X509CertificateEnumerator_MoveNext_m3925432749(L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		{
			X509CertificateEnumerator_t3515934698 * L_10 = V_1;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_10, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			RuntimeObject* L_11 = V_3;
			if (L_11)
			{
				goto IL_0047;
			}
		}

IL_0046:
		{
			IL2CPP_END_FINALLY(60)
		}

IL_0047:
		{
			RuntimeObject* L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(60)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004e:
	{
		return (X509Certificate_t489243025 *)NULL;
	}

IL_0050:
	{
		X509Certificate_t489243025 * L_13 = V_2;
		return L_13;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateRoot(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t489243025 * X509Chain_FindCertificateRoot_m1937726457 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___potentialRoot0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_FindCertificateRoot_m1937726457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Certificate_t489243025 * V_0 = NULL;
	X509CertificateEnumerator_t3515934698 * V_1 = NULL;
	X509Certificate_t489243025 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509Certificate_t489243025 * L_0 = ___potentialRoot0;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		__this->set__status_4(((int32_t)65536));
		return (X509Certificate_t489243025 *)NULL;
	}

IL_0013:
	{
		X509Certificate_t489243025 * L_1 = ___potentialRoot0;
		bool L_2 = X509Chain_IsTrusted_m1243554942(__this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		X509Certificate_t489243025 * L_3 = ___potentialRoot0;
		return L_3;
	}

IL_0021:
	{
		X509CertificateCollection_t1542168550 * L_4 = X509Chain_get_TrustAnchors_m2434696767(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		X509CertificateEnumerator_t3515934698 * L_5 = X509CertificateCollection_GetEnumerator_m1275665495(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			X509CertificateEnumerator_t3515934698 * L_6 = V_1;
			NullCheck(L_6);
			X509Certificate_t489243025 * L_7 = X509CertificateEnumerator_get_Current_m1004537031(L_6, /*hidden argument*/NULL);
			V_0 = L_7;
			X509Certificate_t489243025 * L_8 = ___potentialRoot0;
			X509Certificate_t489243025 * L_9 = V_0;
			bool L_10 = X509Chain_IsParent_m2689546349(__this, L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_004d;
			}
		}

IL_0046:
		{
			X509Certificate_t489243025 * L_11 = V_0;
			V_2 = L_11;
			IL2CPP_LEAVE(0x91, FINALLY_005d);
		}

IL_004d:
		{
			X509CertificateEnumerator_t3515934698 * L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = X509CertificateEnumerator_MoveNext_m3925432749(L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			X509CertificateEnumerator_t3515934698 * L_14 = V_1;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_14, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			RuntimeObject* L_15 = V_3;
			if (L_15)
			{
				goto IL_0068;
			}
		}

IL_0067:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0068:
		{
			RuntimeObject* L_16 = V_3;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x91, IL_0091)
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006f:
	{
		X509Certificate_t489243025 * L_17 = ___potentialRoot0;
		NullCheck(L_17);
		bool L_18 = X509Certificate_get_IsSelfSigned_m4064195693(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0084;
		}
	}
	{
		__this->set__status_4(((int32_t)32));
		X509Certificate_t489243025 * L_19 = ___potentialRoot0;
		return L_19;
	}

IL_0084:
	{
		__this->set__status_4(((int32_t)65536));
		return (X509Certificate_t489243025 *)NULL;
	}

IL_0091:
	{
		X509Certificate_t489243025 * L_20 = V_2;
		return L_20;
	}
}
// System.Boolean Mono.Security.X509.X509Chain::IsTrusted(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsTrusted_m1243554942 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___potentialTrusted0, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = X509Chain_get_TrustAnchors_m2434696767(__this, /*hidden argument*/NULL);
		X509Certificate_t489243025 * L_1 = ___potentialTrusted0;
		NullCheck(L_0);
		bool L_2 = X509CertificateCollection_Contains_m743657353(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Mono.Security.X509.X509Chain::IsParent(Mono.Security.X509.X509Certificate,Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsParent_m2689546349 (X509Chain_t863783601 * __this, X509Certificate_t489243025 * ___child0, X509Certificate_t489243025 * ___parent1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Chain_IsParent_m2689546349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Extension_t3173393653 * V_0 = NULL;
	BasicConstraintsExtension_t2462195279 * V_1 = NULL;
	{
		X509Certificate_t489243025 * L_0 = ___child0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String Mono.Security.X509.X509Certificate::get_IssuerName() */, L_0);
		X509Certificate_t489243025 * L_2 = ___parent1;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String Mono.Security.X509.X509Certificate::get_SubjectName() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		X509Certificate_t489243025 * L_5 = ___parent1;
		NullCheck(L_5);
		int32_t L_6 = X509Certificate_get_Version_m3419034307(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)2)))
		{
			goto IL_0074;
		}
	}
	{
		X509Certificate_t489243025 * L_7 = ___parent1;
		bool L_8 = X509Chain_IsTrusted_m1243554942(__this, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0074;
		}
	}
	{
		X509Certificate_t489243025 * L_9 = ___parent1;
		NullCheck(L_9);
		X509ExtensionCollection_t609554709 * L_10 = X509Certificate_get_Extensions_m2532937142(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		X509Extension_t3173393653 * L_11 = X509ExtensionCollection_get_Item_m4249795832(L_10, _stringLiteral1004423994, /*hidden argument*/NULL);
		V_0 = L_11;
		X509Extension_t3173393653 * L_12 = V_0;
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		X509Extension_t3173393653 * L_13 = V_0;
		BasicConstraintsExtension_t2462195279 * L_14 = (BasicConstraintsExtension_t2462195279 *)il2cpp_codegen_object_new(BasicConstraintsExtension_t2462195279_il2cpp_TypeInfo_var);
		BasicConstraintsExtension__ctor_m3191645544(L_14, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		BasicConstraintsExtension_t2462195279 * L_15 = V_1;
		NullCheck(L_15);
		bool L_16 = BasicConstraintsExtension_get_CertificateAuthority_m391198292(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0064;
		}
	}
	{
		__this->set__status_4(((int32_t)1024));
	}

IL_0064:
	{
		goto IL_0074;
	}

IL_0069:
	{
		__this->set__status_4(((int32_t)1024));
	}

IL_0074:
	{
		X509Certificate_t489243025 * L_17 = ___child0;
		X509Certificate_t489243025 * L_18 = ___parent1;
		NullCheck(L_18);
		RSA_t2385438082 * L_19 = VirtFuncInvoker0< RSA_t2385438082 * >::Invoke(10 /* System.Security.Cryptography.RSA Mono.Security.X509.X509Certificate::get_RSA() */, L_18);
		NullCheck(L_17);
		bool L_20 = X509Certificate_VerifySignature_m3538124832(L_17, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_008e;
		}
	}
	{
		__this->set__status_4(8);
		return (bool)0;
	}

IL_008e:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C"  void X509Crl__ctor_m1817187405 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___crl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl__ctor_m1817187405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_0 = ___crl0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral2910693339, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		ByteU5BU5D_t4116647657* L_2 = ___crl0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_2);
		RuntimeObject * L_3 = Array_Clone_m2672907798((RuntimeArray *)(RuntimeArray *)L_2, /*hidden argument*/NULL);
		__this->set_encoded_8(((ByteU5BU5D_t4116647657*)Castclass((RuntimeObject*)L_3, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var)));
		ByteU5BU5D_t4116647657* L_4 = __this->get_encoded_8();
		X509Crl_Parse_m3164013387(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C"  void X509Crl_Parse_m3164013387 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___crl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_Parse_m3164013387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ASN1_t2114160833 * V_1 = NULL;
	ASN1_t2114160833 * V_2 = NULL;
	int32_t V_3 = 0;
	ASN1_t2114160833 * V_4 = NULL;
	ASN1_t2114160833 * V_5 = NULL;
	int32_t V_6 = 0;
	ASN1_t2114160833 * V_7 = NULL;
	String_t* V_8 = NULL;
	ByteU5BU5D_t4116647657* V_9 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral1752854249;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t4116647657* L_0 = __this->get_encoded_8();
			ASN1_t2114160833 * L_1 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
			ASN1__ctor_m1638893325(L_1, L_0, /*hidden argument*/NULL);
			V_1 = L_1;
			ASN1_t2114160833 * L_2 = V_1;
			NullCheck(L_2);
			uint8_t L_3 = ASN1_get_Tag_m2789147236(L_2, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)48)))))
			{
				goto IL_002b;
			}
		}

IL_001f:
		{
			ASN1_t2114160833 * L_4 = V_1;
			NullCheck(L_4);
			int32_t L_5 = ASN1_get_Count_m1789520042(L_4, /*hidden argument*/NULL);
			if ((((int32_t)L_5) == ((int32_t)3)))
			{
				goto IL_0032;
			}
		}

IL_002b:
		{
			String_t* L_6 = V_0;
			CryptographicException_t248831461 * L_7 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
			CryptographicException__ctor_m503735289(L_7, L_6, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0032:
		{
			ASN1_t2114160833 * L_8 = V_1;
			NullCheck(L_8);
			ASN1_t2114160833 * L_9 = ASN1_get_Item_m2255075813(L_8, 0, /*hidden argument*/NULL);
			V_2 = L_9;
			ASN1_t2114160833 * L_10 = V_2;
			NullCheck(L_10);
			uint8_t L_11 = ASN1_get_Tag_m2789147236(L_10, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)48)))))
			{
				goto IL_0053;
			}
		}

IL_0047:
		{
			ASN1_t2114160833 * L_12 = V_2;
			NullCheck(L_12);
			int32_t L_13 = ASN1_get_Count_m1789520042(L_12, /*hidden argument*/NULL);
			if ((((int32_t)L_13) >= ((int32_t)3)))
			{
				goto IL_005a;
			}
		}

IL_0053:
		{
			String_t* L_14 = V_0;
			CryptographicException_t248831461 * L_15 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
			CryptographicException__ctor_m503735289(L_15, L_14, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
		}

IL_005a:
		{
			V_3 = 0;
			ASN1_t2114160833 * L_16 = V_2;
			int32_t L_17 = V_3;
			NullCheck(L_16);
			ASN1_t2114160833 * L_18 = ASN1_get_Item_m2255075813(L_16, L_17, /*hidden argument*/NULL);
			NullCheck(L_18);
			uint8_t L_19 = ASN1_get_Tag_m2789147236(L_18, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_19) == ((uint32_t)2))))
			{
				goto IL_008e;
			}
		}

IL_006e:
		{
			ASN1_t2114160833 * L_20 = V_2;
			int32_t L_21 = V_3;
			int32_t L_22 = L_21;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
			NullCheck(L_20);
			ASN1_t2114160833 * L_23 = ASN1_get_Item_m2255075813(L_20, L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			ByteU5BU5D_t4116647657* L_24 = ASN1_get_Value_m63296490(L_23, /*hidden argument*/NULL);
			NullCheck(L_24);
			int32_t L_25 = 0;
			uint8_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
			__this->set_version_1((uint8_t)(((int32_t)((uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1))))));
			goto IL_0095;
		}

IL_008e:
		{
			__this->set_version_1((uint8_t)1);
		}

IL_0095:
		{
			ASN1_t2114160833 * L_27 = V_2;
			int32_t L_28 = V_3;
			int32_t L_29 = L_28;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
			NullCheck(L_27);
			ASN1_t2114160833 * L_30 = ASN1_get_Item_m2255075813(L_27, L_29, /*hidden argument*/NULL);
			NullCheck(L_30);
			ASN1_t2114160833 * L_31 = ASN1_get_Item_m2255075813(L_30, 0, /*hidden argument*/NULL);
			String_t* L_32 = ASN1Convert_ToOid_m3847701408(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			__this->set_signatureOID_5(L_32);
			ASN1_t2114160833 * L_33 = V_2;
			int32_t L_34 = V_3;
			int32_t L_35 = L_34;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
			NullCheck(L_33);
			ASN1_t2114160833 * L_36 = ASN1_get_Item_m2255075813(L_33, L_35, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(X501_t1758824426_il2cpp_TypeInfo_var);
			String_t* L_37 = X501_ToString_m2260475203(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
			__this->set_issuer_0(L_37);
			ASN1_t2114160833 * L_38 = V_2;
			int32_t L_39 = V_3;
			int32_t L_40 = L_39;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1));
			NullCheck(L_38);
			ASN1_t2114160833 * L_41 = ASN1_get_Item_m2255075813(L_38, L_40, /*hidden argument*/NULL);
			DateTime_t3738529785  L_42 = ASN1Convert_ToDateTime_m1246060840(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
			__this->set_thisUpdate_2(L_42);
			ASN1_t2114160833 * L_43 = V_2;
			int32_t L_44 = V_3;
			int32_t L_45 = L_44;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
			NullCheck(L_43);
			ASN1_t2114160833 * L_46 = ASN1_get_Item_m2255075813(L_43, L_45, /*hidden argument*/NULL);
			V_4 = L_46;
			ASN1_t2114160833 * L_47 = V_4;
			NullCheck(L_47);
			uint8_t L_48 = ASN1_get_Tag_m2789147236(L_47, /*hidden argument*/NULL);
			if ((((int32_t)L_48) == ((int32_t)((int32_t)23))))
			{
				goto IL_0106;
			}
		}

IL_00f8:
		{
			ASN1_t2114160833 * L_49 = V_4;
			NullCheck(L_49);
			uint8_t L_50 = ASN1_get_Tag_m2789147236(L_49, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)24)))))
			{
				goto IL_0120;
			}
		}

IL_0106:
		{
			ASN1_t2114160833 * L_51 = V_4;
			DateTime_t3738529785  L_52 = ASN1Convert_ToDateTime_m1246060840(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
			__this->set_nextUpdate_3(L_52);
			ASN1_t2114160833 * L_53 = V_2;
			int32_t L_54 = V_3;
			int32_t L_55 = L_54;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)1));
			NullCheck(L_53);
			ASN1_t2114160833 * L_56 = ASN1_get_Item_m2255075813(L_53, L_55, /*hidden argument*/NULL);
			V_4 = L_56;
		}

IL_0120:
		{
			ArrayList_t2718874744 * L_57 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
			ArrayList__ctor_m4254721275(L_57, /*hidden argument*/NULL);
			__this->set_entries_4(L_57);
			ASN1_t2114160833 * L_58 = V_4;
			if (!L_58)
			{
				goto IL_017f;
			}
		}

IL_0132:
		{
			ASN1_t2114160833 * L_59 = V_4;
			NullCheck(L_59);
			uint8_t L_60 = ASN1_get_Tag_m2789147236(L_59, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_60) == ((uint32_t)((int32_t)48)))))
			{
				goto IL_017f;
			}
		}

IL_0140:
		{
			ASN1_t2114160833 * L_61 = V_4;
			V_5 = L_61;
			V_6 = 0;
			goto IL_016c;
		}

IL_014c:
		{
			ArrayList_t2718874744 * L_62 = __this->get_entries_4();
			ASN1_t2114160833 * L_63 = V_5;
			int32_t L_64 = V_6;
			NullCheck(L_63);
			ASN1_t2114160833 * L_65 = ASN1_get_Item_m2255075813(L_63, L_64, /*hidden argument*/NULL);
			X509CrlEntry_t645568789 * L_66 = (X509CrlEntry_t645568789 *)il2cpp_codegen_object_new(X509CrlEntry_t645568789_il2cpp_TypeInfo_var);
			X509CrlEntry__ctor_m4013514048(L_66, L_65, /*hidden argument*/NULL);
			NullCheck(L_62);
			VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_62, L_66);
			int32_t L_67 = V_6;
			V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
		}

IL_016c:
		{
			int32_t L_68 = V_6;
			ASN1_t2114160833 * L_69 = V_5;
			NullCheck(L_69);
			int32_t L_70 = ASN1_get_Count_m1789520042(L_69, /*hidden argument*/NULL);
			if ((((int32_t)L_68) < ((int32_t)L_70)))
			{
				goto IL_014c;
			}
		}

IL_017a:
		{
			goto IL_0183;
		}

IL_017f:
		{
			int32_t L_71 = V_3;
			V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_71, (int32_t)1));
		}

IL_0183:
		{
			ASN1_t2114160833 * L_72 = V_2;
			int32_t L_73 = V_3;
			NullCheck(L_72);
			ASN1_t2114160833 * L_74 = ASN1_get_Item_m2255075813(L_72, L_73, /*hidden argument*/NULL);
			V_7 = L_74;
			ASN1_t2114160833 * L_75 = V_7;
			if (!L_75)
			{
				goto IL_01c9;
			}
		}

IL_0193:
		{
			ASN1_t2114160833 * L_76 = V_7;
			NullCheck(L_76);
			uint8_t L_77 = ASN1_get_Tag_m2789147236(L_76, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_77) == ((uint32_t)((int32_t)160)))))
			{
				goto IL_01c9;
			}
		}

IL_01a4:
		{
			ASN1_t2114160833 * L_78 = V_7;
			NullCheck(L_78);
			int32_t L_79 = ASN1_get_Count_m1789520042(L_78, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_79) == ((uint32_t)1))))
			{
				goto IL_01c9;
			}
		}

IL_01b1:
		{
			ASN1_t2114160833 * L_80 = V_7;
			NullCheck(L_80);
			ASN1_t2114160833 * L_81 = ASN1_get_Item_m2255075813(L_80, 0, /*hidden argument*/NULL);
			X509ExtensionCollection_t609554709 * L_82 = (X509ExtensionCollection_t609554709 *)il2cpp_codegen_object_new(X509ExtensionCollection_t609554709_il2cpp_TypeInfo_var);
			X509ExtensionCollection__ctor_m551870633(L_82, L_81, /*hidden argument*/NULL);
			__this->set_extensions_7(L_82);
			goto IL_01d5;
		}

IL_01c9:
		{
			X509ExtensionCollection_t609554709 * L_83 = (X509ExtensionCollection_t609554709 *)il2cpp_codegen_object_new(X509ExtensionCollection_t609554709_il2cpp_TypeInfo_var);
			X509ExtensionCollection__ctor_m551870633(L_83, (ASN1_t2114160833 *)NULL, /*hidden argument*/NULL);
			__this->set_extensions_7(L_83);
		}

IL_01d5:
		{
			ASN1_t2114160833 * L_84 = V_1;
			NullCheck(L_84);
			ASN1_t2114160833 * L_85 = ASN1_get_Item_m2255075813(L_84, 1, /*hidden argument*/NULL);
			NullCheck(L_85);
			ASN1_t2114160833 * L_86 = ASN1_get_Item_m2255075813(L_85, 0, /*hidden argument*/NULL);
			String_t* L_87 = ASN1Convert_ToOid_m3847701408(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
			V_8 = L_87;
			String_t* L_88 = __this->get_signatureOID_5();
			String_t* L_89 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_90 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			if (!L_90)
			{
				goto IL_020c;
			}
		}

IL_01fb:
		{
			String_t* L_91 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m3937257545(NULL /*static, unused*/, L_91, _stringLiteral3761512114, /*hidden argument*/NULL);
			CryptographicException_t248831461 * L_93 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
			CryptographicException__ctor_m503735289(L_93, L_92, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_93);
		}

IL_020c:
		{
			ASN1_t2114160833 * L_94 = V_1;
			NullCheck(L_94);
			ASN1_t2114160833 * L_95 = ASN1_get_Item_m2255075813(L_94, 2, /*hidden argument*/NULL);
			NullCheck(L_95);
			ByteU5BU5D_t4116647657* L_96 = ASN1_get_Value_m63296490(L_95, /*hidden argument*/NULL);
			V_9 = L_96;
			ByteU5BU5D_t4116647657* L_97 = V_9;
			NullCheck(L_97);
			__this->set_signature_6(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_97)->max_length)))), (int32_t)1)))));
			ByteU5BU5D_t4116647657* L_98 = V_9;
			ByteU5BU5D_t4116647657* L_99 = __this->get_signature_6();
			ByteU5BU5D_t4116647657* L_100 = __this->get_signature_6();
			NullCheck(L_100);
			Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_98, 1, (RuntimeArray *)(RuntimeArray *)L_99, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_100)->max_length)))), /*hidden argument*/NULL);
			goto IL_0254;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0247;
		throw e;
	}

CATCH_0247:
	{ // begin catch(System.Object)
		{
			String_t* L_101 = V_0;
			CryptographicException_t248831461 * L_102 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
			CryptographicException__ctor_m503735289(L_102, L_101, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_102);
		}

IL_024f:
		{
			goto IL_0254;
		}
	} // end catch (depth: 1)

IL_0254:
	{
		return;
	}
}
// System.Collections.ArrayList Mono.Security.X509.X509Crl::get_Entries()
extern "C"  ArrayList_t2718874744 * X509Crl_get_Entries_m4032389711 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_Entries_m4032389711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t2718874744 * L_0 = __this->get_entries_4();
		IL2CPP_RUNTIME_CLASS_INIT(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList_t2718874744 * L_1 = ArrayList_ReadOnly_m1905796817(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::get_Item(System.Int32)
extern "C"  X509CrlEntry_t645568789 * X509Crl_get_Item_m2105052623 (X509Crl_t1148767388 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_Item_m2105052623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t2718874744 * L_0 = __this->get_entries_4();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		RuntimeObject * L_2 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((X509CrlEntry_t645568789 *)CastclassClass((RuntimeObject*)L_2, X509CrlEntry_t645568789_il2cpp_TypeInfo_var));
	}
}
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::get_Item(System.Byte[])
extern "C"  X509CrlEntry_t645568789 * X509Crl_get_Item_m2795983057 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___serialNumber0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___serialNumber0;
		X509CrlEntry_t645568789 * L_1 = X509Crl_GetCrlEntry_m641501875(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C"  X509ExtensionCollection_t609554709 * X509Crl_get_Extensions_m922657393 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		X509ExtensionCollection_t609554709 * L_0 = __this->get_extensions_7();
		return L_0;
	}
}
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_get_Hash_m464217067 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_Hash_m464217067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ASN1_t2114160833 * V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	HashAlgorithm_t1432317219 * V_2 = NULL;
	{
		ByteU5BU5D_t4116647657* L_0 = __this->get_hash_value_9();
		if (L_0)
		{
			goto IL_003d;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_1 = __this->get_encoded_8();
		ASN1_t2114160833 * L_2 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1638893325(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ASN1_t2114160833 * L_3 = V_0;
		NullCheck(L_3);
		ASN1_t2114160833 * L_4 = ASN1_get_Item_m2255075813(L_3, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t4116647657* L_5 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_4);
		V_1 = L_5;
		String_t* L_6 = X509Crl_GetHashName_m4214678741(__this, /*hidden argument*/NULL);
		HashAlgorithm_t1432317219 * L_7 = HashAlgorithm_Create_m644612360(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		HashAlgorithm_t1432317219 * L_8 = V_2;
		ByteU5BU5D_t4116647657* L_9 = V_1;
		NullCheck(L_8);
		ByteU5BU5D_t4116647657* L_10 = HashAlgorithm_ComputeHash_m2825542963(L_8, L_9, /*hidden argument*/NULL);
		__this->set_hash_value_9(L_10);
	}

IL_003d:
	{
		ByteU5BU5D_t4116647657* L_11 = __this->get_hash_value_9();
		return L_11;
	}
}
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
extern "C"  String_t* X509Crl_get_IssuerName_m552696835 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_issuer_0();
		return L_0;
	}
}
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
extern "C"  DateTime_t3738529785  X509Crl_get_NextUpdate_m604794549 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785  L_0 = __this->get_nextUpdate_3();
		return L_0;
	}
}
// System.DateTime Mono.Security.X509.X509Crl::get_ThisUpdate()
extern "C"  DateTime_t3738529785  X509Crl_get_ThisUpdate_m1179888977 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785  L_0 = __this->get_thisUpdate_2();
		return L_0;
	}
}
// System.String Mono.Security.X509.X509Crl::get_SignatureAlgorithm()
extern "C"  String_t* X509Crl_get_SignatureAlgorithm_m3994908724 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_signatureOID_5();
		return L_0;
	}
}
// System.Byte[] Mono.Security.X509.X509Crl::get_Signature()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_get_Signature_m1378549588 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_Signature_m1378549588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = __this->get_signature_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (ByteU5BU5D_t4116647657*)NULL;
	}

IL_000d:
	{
		ByteU5BU5D_t4116647657* L_1 = __this->get_signature_6();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_1);
		RuntimeObject * L_2 = Array_Clone_m2672907798((RuntimeArray *)(RuntimeArray *)L_1, /*hidden argument*/NULL);
		return ((ByteU5BU5D_t4116647657*)Castclass((RuntimeObject*)L_2, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] Mono.Security.X509.X509Crl::get_RawData()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_get_RawData_m3623997699 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_RawData_m3623997699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = __this->get_encoded_8();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1 = Array_Clone_m2672907798((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		return ((ByteU5BU5D_t4116647657*)Castclass((RuntimeObject*)L_1, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var));
	}
}
// System.Byte Mono.Security.X509.X509Crl::get_Version()
extern "C"  uint8_t X509Crl_get_Version_m2322061776 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = __this->get_version_1();
		return L_0;
	}
}
// System.Boolean Mono.Security.X509.X509Crl::get_IsCurrent()
extern "C"  bool X509Crl_get_IsCurrent_m298812277 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_get_IsCurrent_m298812277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_0 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = X509Crl_WasCurrent_m662015296(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Mono.Security.X509.X509Crl::WasCurrent(System.DateTime)
extern "C"  bool X509Crl_WasCurrent_m662015296 (X509Crl_t1148767388 * __this, DateTime_t3738529785  ___instant0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_WasCurrent_m662015296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		DateTime_t3738529785  L_0 = __this->get_nextUpdate_3();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = ((DateTime_t3738529785_StaticFields*)il2cpp_codegen_static_fields_for(DateTime_t3738529785_il2cpp_TypeInfo_var))->get_MinValue_13();
		bool L_2 = DateTime_op_Equality_m1022058599(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		DateTime_t3738529785  L_3 = ___instant0;
		DateTime_t3738529785  L_4 = __this->get_thisUpdate_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		bool L_5 = DateTime_op_GreaterThanOrEqual_m674703316(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0022:
	{
		DateTime_t3738529785  L_6 = ___instant0;
		DateTime_t3738529785  L_7 = __this->get_thisUpdate_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		bool L_8 = DateTime_op_GreaterThanOrEqual_m674703316(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		DateTime_t3738529785  L_9 = ___instant0;
		DateTime_t3738529785  L_10 = __this->get_nextUpdate_3();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		bool L_11 = DateTime_op_LessThanOrEqual_m2360948759(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0042;
	}

IL_0041:
	{
		G_B5_0 = 0;
	}

IL_0042:
	{
		return (bool)G_B5_0;
	}
}
// System.Byte[] Mono.Security.X509.X509Crl::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* X509Crl_GetBytes_m326324156 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_GetBytes_m326324156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = __this->get_encoded_8();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1 = Array_Clone_m2672907798((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		return ((ByteU5BU5D_t4116647657*)Castclass((RuntimeObject*)L_1, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var));
	}
}
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C"  bool X509Crl_Compare_m3418726913 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___array10, ByteU5BU5D_t4116647657* ___array21, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t4116647657* L_0 = ___array10;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_1 = ___array21;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		ByteU5BU5D_t4116647657* L_2 = ___array10;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_3 = ___array21;
		if (L_3)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		ByteU5BU5D_t4116647657* L_4 = ___array10;
		NullCheck(L_4);
		ByteU5BU5D_t4116647657* L_5 = ___array21;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))))))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		V_0 = 0;
		goto IL_0041;
	}

IL_0030:
	{
		ByteU5BU5D_t4116647657* L_6 = ___array10;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		ByteU5BU5D_t4116647657* L_10 = ___array21;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		uint8_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		if ((((int32_t)L_9) == ((int32_t)L_13)))
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0041:
	{
		int32_t L_15 = V_0;
		ByteU5BU5D_t4116647657* L_16 = ___array10;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)1;
	}
}
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
extern "C"  X509CrlEntry_t645568789 * X509Crl_GetCrlEntry_m1550247114 (X509Crl_t1148767388 * __this, X509Certificate_t489243025 * ___x5090, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_GetCrlEntry_m1550247114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Certificate_t489243025 * L_0 = ___x5090;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3501283186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		X509Certificate_t489243025 * L_2 = ___x5090;
		NullCheck(L_2);
		ByteU5BU5D_t4116647657* L_3 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(13 /* System.Byte[] Mono.Security.X509.X509Certificate::get_SerialNumber() */, L_2);
		X509CrlEntry_t645568789 * L_4 = X509Crl_GetCrlEntry_m641501875(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C"  X509CrlEntry_t645568789 * X509Crl_GetCrlEntry_m641501875 (X509Crl_t1148767388 * __this, ByteU5BU5D_t4116647657* ___serialNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_GetCrlEntry_m641501875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	X509CrlEntry_t645568789 * V_1 = NULL;
	{
		ByteU5BU5D_t4116647657* L_0 = ___serialNumber0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral731045672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_0042;
	}

IL_0018:
	{
		ArrayList_t2718874744 * L_2 = __this->get_entries_4();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_1 = ((X509CrlEntry_t645568789 *)CastclassClass((RuntimeObject*)L_4, X509CrlEntry_t645568789_il2cpp_TypeInfo_var));
		ByteU5BU5D_t4116647657* L_5 = ___serialNumber0;
		X509CrlEntry_t645568789 * L_6 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t4116647657* L_7 = X509CrlEntry_get_SerialNumber_m3627212772(L_6, /*hidden argument*/NULL);
		bool L_8 = X509Crl_Compare_m3418726913(__this, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		X509CrlEntry_t645568789 * L_9 = V_1;
		return L_9;
	}

IL_003e:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0042:
	{
		int32_t L_11 = V_0;
		ArrayList_t2718874744 * L_12 = __this->get_entries_4();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0018;
		}
	}
	{
		return (X509CrlEntry_t645568789 *)NULL;
	}
}
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Crl_VerifySignature_m757547902 (X509Crl_t1148767388 * __this, X509Certificate_t489243025 * ___x5090, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_VerifySignature_m757547902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Extension_t3173393653 * V_0 = NULL;
	KeyUsageExtension_t1795615912 * V_1 = NULL;
	BasicConstraintsExtension_t2462195279 * V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t2736202052 * V_4 = NULL;
	int32_t V_5 = 0;
	{
		X509Certificate_t489243025 * L_0 = ___x5090;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3501283186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		X509Certificate_t489243025 * L_2 = ___x5090;
		NullCheck(L_2);
		int32_t L_3 = X509Certificate_get_Version_m3419034307(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)3)))
		{
			goto IL_0074;
		}
	}
	{
		X509Certificate_t489243025 * L_4 = ___x5090;
		NullCheck(L_4);
		X509ExtensionCollection_t609554709 * L_5 = X509Certificate_get_Extensions_m2532937142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		X509Extension_t3173393653 * L_6 = X509ExtensionCollection_get_Item_m4249795832(L_5, _stringLiteral1004423982, /*hidden argument*/NULL);
		V_0 = L_6;
		X509Extension_t3173393653 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		X509Extension_t3173393653 * L_8 = V_0;
		KeyUsageExtension_t1795615912 * L_9 = (KeyUsageExtension_t1795615912 *)il2cpp_codegen_object_new(KeyUsageExtension_t1795615912_il2cpp_TypeInfo_var);
		KeyUsageExtension__ctor_m3414452076(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		KeyUsageExtension_t1795615912 * L_10 = V_1;
		NullCheck(L_10);
		bool L_11 = KeyUsageExtension_Support_m3508856672(L_10, 2, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0049;
		}
	}
	{
		return (bool)0;
	}

IL_0049:
	{
		X509Certificate_t489243025 * L_12 = ___x5090;
		NullCheck(L_12);
		X509ExtensionCollection_t609554709 * L_13 = X509Certificate_get_Extensions_m2532937142(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		X509Extension_t3173393653 * L_14 = X509ExtensionCollection_get_Item_m4249795832(L_13, _stringLiteral1004423994, /*hidden argument*/NULL);
		V_0 = L_14;
		X509Extension_t3173393653 * L_15 = V_0;
		if (!L_15)
		{
			goto IL_0074;
		}
	}
	{
		X509Extension_t3173393653 * L_16 = V_0;
		BasicConstraintsExtension_t2462195279 * L_17 = (BasicConstraintsExtension_t2462195279 *)il2cpp_codegen_object_new(BasicConstraintsExtension_t2462195279_il2cpp_TypeInfo_var);
		BasicConstraintsExtension__ctor_m3191645544(L_17, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		BasicConstraintsExtension_t2462195279 * L_18 = V_2;
		NullCheck(L_18);
		bool L_19 = BasicConstraintsExtension_get_CertificateAuthority_m391198292(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0074;
		}
	}
	{
		return (bool)0;
	}

IL_0074:
	{
		String_t* L_20 = __this->get_issuer_0();
		X509Certificate_t489243025 * L_21 = ___x5090;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String Mono.Security.X509.X509Certificate::get_SubjectName() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_008c;
		}
	}
	{
		return (bool)0;
	}

IL_008c:
	{
		String_t* L_24 = __this->get_signatureOID_5();
		V_3 = L_24;
		String_t* L_25 = V_3;
		if (!L_25)
		{
			goto IL_00ea;
		}
	}
	{
		Dictionary_2_t2736202052 * L_26 = ((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map12_10();
		if (L_26)
		{
			goto IL_00bf;
		}
	}
	{
		Dictionary_2_t2736202052 * L_27 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2392909825(L_27, 1, /*hidden argument*/Dictionary_2__ctor_m2392909825_RuntimeMethod_var);
		V_4 = L_27;
		Dictionary_2_t2736202052 * L_28 = V_4;
		NullCheck(L_28);
		Dictionary_2_Add_m282647386(L_28, _stringLiteral254300466, 0, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_29 = V_4;
		((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->set_U3CU3Ef__switchU24map12_10(L_29);
	}

IL_00bf:
	{
		Dictionary_2_t2736202052 * L_30 = ((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map12_10();
		String_t* L_31 = V_3;
		NullCheck(L_30);
		bool L_32 = Dictionary_2_TryGetValue_m1013208020(L_30, L_31, (&V_5), /*hidden argument*/Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var);
		if (!L_32)
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_33 = V_5;
		if (!L_33)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ea;
	}

IL_00dd:
	{
		X509Certificate_t489243025 * L_34 = ___x5090;
		NullCheck(L_34);
		DSA_t2386879874 * L_35 = X509Certificate_get_DSA_m2644963799(L_34, /*hidden argument*/NULL);
		bool L_36 = X509Crl_VerifySignature_m1902456590(__this, L_35, /*hidden argument*/NULL);
		return L_36;
	}

IL_00ea:
	{
		X509Certificate_t489243025 * L_37 = ___x5090;
		NullCheck(L_37);
		RSA_t2385438082 * L_38 = VirtFuncInvoker0< RSA_t2385438082 * >::Invoke(10 /* System.Security.Cryptography.RSA Mono.Security.X509.X509Certificate::get_RSA() */, L_37);
		bool L_39 = X509Crl_VerifySignature_m1808348256(__this, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C"  String_t* X509Crl_GetHashName_m4214678741 (X509Crl_t1148767388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_GetHashName_m4214678741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t2736202052 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = __this->get_signatureOID_5();
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_008f;
		}
	}
	{
		Dictionary_2_t2736202052 * L_2 = ((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map13_11();
		if (L_2)
		{
			goto IL_0054;
		}
	}
	{
		Dictionary_2_t2736202052 * L_3 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2392909825(L_3, 4, /*hidden argument*/Dictionary_2__ctor_m2392909825_RuntimeMethod_var);
		V_1 = L_3;
		Dictionary_2_t2736202052 * L_4 = V_1;
		NullCheck(L_4);
		Dictionary_2_Add_m282647386(L_4, _stringLiteral1384881100, 0, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_5 = V_1;
		NullCheck(L_5);
		Dictionary_2_Add_m282647386(L_5, _stringLiteral1002544076, 1, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_6 = V_1;
		NullCheck(L_6);
		Dictionary_2_Add_m282647386(L_6, _stringLiteral254300466, 2, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_7 = V_1;
		NullCheck(L_7);
		Dictionary_2_Add_m282647386(L_7, _stringLiteral2958859212, 2, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_8 = V_1;
		((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->set_U3CU3Ef__switchU24map13_11(L_8);
	}

IL_0054:
	{
		Dictionary_2_t2736202052 * L_9 = ((X509Crl_t1148767388_StaticFields*)il2cpp_codegen_static_fields_for(X509Crl_t1148767388_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map13_11();
		String_t* L_10 = V_0;
		NullCheck(L_9);
		bool L_11 = Dictionary_2_TryGetValue_m1013208020(L_9, L_10, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var);
		if (!L_11)
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_12 = V_2;
		switch (L_12)
		{
			case 0:
			{
				goto IL_007d;
			}
			case 1:
			{
				goto IL_0083;
			}
			case 2:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_008f;
	}

IL_007d:
	{
		return _stringLiteral4242423987;
	}

IL_0083:
	{
		return _stringLiteral3839139460;
	}

IL_0089:
	{
		return _stringLiteral1144609714;
	}

IL_008f:
	{
		String_t* L_13 = __this->get_signatureOID_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2511804911, L_13, /*hidden argument*/NULL);
		CryptographicException_t248831461 * L_15 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}
}
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C"  bool X509Crl_VerifySignature_m1902456590 (X509Crl_t1148767388 * __this, DSA_t2386879874 * ___dsa0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_VerifySignature_m1902456590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DSASignatureDeformatter_t3677955172 * V_0 = NULL;
	ASN1_t2114160833 * V_1 = NULL;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	ByteU5BU5D_t4116647657* V_3 = NULL;
	ByteU5BU5D_t4116647657* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = __this->get_signatureOID_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_0, _stringLiteral254300466, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_2 = __this->get_signatureOID_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2511804911, L_2, /*hidden argument*/NULL);
		CryptographicException_t248831461 * L_4 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002b:
	{
		DSA_t2386879874 * L_5 = ___dsa0;
		DSASignatureDeformatter_t3677955172 * L_6 = (DSASignatureDeformatter_t3677955172 *)il2cpp_codegen_object_new(DSASignatureDeformatter_t3677955172_il2cpp_TypeInfo_var);
		DSASignatureDeformatter__ctor_m2889130126(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		DSASignatureDeformatter_t3677955172 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void System.Security.Cryptography.DSASignatureDeformatter::SetHashAlgorithm(System.String) */, L_7, _stringLiteral1144609714);
		ByteU5BU5D_t4116647657* L_8 = __this->get_signature_6();
		ASN1_t2114160833 * L_9 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1638893325(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		ASN1_t2114160833 * L_10 = V_1;
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		ASN1_t2114160833 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = ASN1_get_Count_m1789520042(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)2)))
		{
			goto IL_005d;
		}
	}

IL_005b:
	{
		return (bool)0;
	}

IL_005d:
	{
		ASN1_t2114160833 * L_13 = V_1;
		NullCheck(L_13);
		ASN1_t2114160833 * L_14 = ASN1_get_Item_m2255075813(L_13, 0, /*hidden argument*/NULL);
		NullCheck(L_14);
		ByteU5BU5D_t4116647657* L_15 = ASN1_get_Value_m63296490(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		ASN1_t2114160833 * L_16 = V_1;
		NullCheck(L_16);
		ASN1_t2114160833 * L_17 = ASN1_get_Item_m2255075813(L_16, 1, /*hidden argument*/NULL);
		NullCheck(L_17);
		ByteU5BU5D_t4116647657* L_18 = ASN1_get_Value_m63296490(L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		V_4 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)40)));
		ByteU5BU5D_t4116647657* L_19 = V_2;
		NullCheck(L_19);
		int32_t L_20 = Math_Max_m1873195862(NULL /*static, unused*/, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))), (int32_t)((int32_t)20))), /*hidden argument*/NULL);
		V_5 = L_20;
		ByteU5BU5D_t4116647657* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = Math_Max_m1873195862(NULL /*static, unused*/, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)20), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))), /*hidden argument*/NULL);
		V_6 = L_22;
		ByteU5BU5D_t4116647657* L_23 = V_2;
		int32_t L_24 = V_5;
		ByteU5BU5D_t4116647657* L_25 = V_4;
		int32_t L_26 = V_6;
		ByteU5BU5D_t4116647657* L_27 = V_2;
		NullCheck(L_27);
		int32_t L_28 = V_5;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_23, L_24, (RuntimeArray *)(RuntimeArray *)L_25, L_26, ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_27)->max_length)))), (int32_t)L_28)), /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_29 = V_3;
		NullCheck(L_29);
		int32_t L_30 = Math_Max_m1873195862(NULL /*static, unused*/, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))), (int32_t)((int32_t)20))), /*hidden argument*/NULL);
		V_7 = L_30;
		ByteU5BU5D_t4116647657* L_31 = V_3;
		NullCheck(L_31);
		int32_t L_32 = Math_Max_m1873195862(NULL /*static, unused*/, ((int32_t)20), ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)40), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_31)->max_length)))))), /*hidden argument*/NULL);
		V_8 = L_32;
		ByteU5BU5D_t4116647657* L_33 = V_3;
		int32_t L_34 = V_7;
		ByteU5BU5D_t4116647657* L_35 = V_4;
		int32_t L_36 = V_8;
		ByteU5BU5D_t4116647657* L_37 = V_3;
		NullCheck(L_37);
		int32_t L_38 = V_7;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_33, L_34, (RuntimeArray *)(RuntimeArray *)L_35, L_36, ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_37)->max_length)))), (int32_t)L_38)), /*hidden argument*/NULL);
		DSASignatureDeformatter_t3677955172 * L_39 = V_0;
		ByteU5BU5D_t4116647657* L_40 = X509Crl_get_Hash_m464217067(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_41 = V_4;
		NullCheck(L_39);
		bool L_42 = VirtFuncInvoker2< bool, ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(6 /* System.Boolean System.Security.Cryptography.DSASignatureDeformatter::VerifySignature(System.Byte[],System.Byte[]) */, L_39, L_40, L_41);
		return L_42;
	}
}
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C"  bool X509Crl_VerifySignature_m1808348256 (X509Crl_t1148767388 * __this, RSA_t2385438082 * ___rsa0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_VerifySignature_m1808348256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RSAPKCS1SignatureDeformatter_t3767223008 * V_0 = NULL;
	{
		RSA_t2385438082 * L_0 = ___rsa0;
		RSAPKCS1SignatureDeformatter_t3767223008 * L_1 = (RSAPKCS1SignatureDeformatter_t3767223008 *)il2cpp_codegen_object_new(RSAPKCS1SignatureDeformatter_t3767223008_il2cpp_TypeInfo_var);
		RSAPKCS1SignatureDeformatter__ctor_m3706544163(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RSAPKCS1SignatureDeformatter_t3767223008 * L_2 = V_0;
		String_t* L_3 = X509Crl_GetHashName_m4214678741(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::SetHashAlgorithm(System.String) */, L_2, L_3);
		RSAPKCS1SignatureDeformatter_t3767223008 * L_4 = V_0;
		ByteU5BU5D_t4116647657* L_5 = X509Crl_get_Hash_m464217067(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_6 = __this->get_signature_6();
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker2< bool, ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(6 /* System.Boolean System.Security.Cryptography.RSAPKCS1SignatureDeformatter::VerifySignature(System.Byte[],System.Byte[]) */, L_4, L_5, L_6);
		return L_7;
	}
}
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  bool X509Crl_VerifySignature_m2177510742 (X509Crl_t1148767388 * __this, AsymmetricAlgorithm_t932037087 * ___aa0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_VerifySignature_m2177510742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsymmetricAlgorithm_t932037087 * L_0 = ___aa0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3454646207, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		AsymmetricAlgorithm_t932037087 * L_2 = ___aa0;
		if (!((RSA_t2385438082 *)IsInstClass((RuntimeObject*)L_2, RSA_t2385438082_il2cpp_TypeInfo_var)))
		{
			goto IL_0029;
		}
	}
	{
		AsymmetricAlgorithm_t932037087 * L_3 = ___aa0;
		bool L_4 = X509Crl_VerifySignature_m1808348256(__this, ((RSA_t2385438082 *)IsInstClass((RuntimeObject*)L_3, RSA_t2385438082_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0029:
	{
		AsymmetricAlgorithm_t932037087 * L_5 = ___aa0;
		if (!((DSA_t2386879874 *)IsInstClass((RuntimeObject*)L_5, DSA_t2386879874_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		AsymmetricAlgorithm_t932037087 * L_6 = ___aa0;
		bool L_7 = X509Crl_VerifySignature_m1902456590(__this, ((DSA_t2386879874 *)IsInstClass((RuntimeObject*)L_6, DSA_t2386879874_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_7;
	}

IL_0041:
	{
		AsymmetricAlgorithm_t932037087 * L_8 = ___aa0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1243364763, L_9, /*hidden argument*/NULL);
		NotSupportedException_t1314879016 * L_11 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Crl::CreateFromFile(System.String)
extern "C"  X509Crl_t1148767388 * X509Crl_CreateFromFile_m1507150054 (RuntimeObject * __this /* static, unused */, String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Crl_CreateFromFile_m1507150054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	FileStream_t4292183065 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (ByteU5BU5D_t4116647657*)NULL;
		String_t* L_0 = ___filename0;
		FileStream_t4292183065 * L_1 = File_Open_m3218582222(NULL /*static, unused*/, L_0, 3, 1, 1, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		FileStream_t4292183065 * L_2 = V_1;
		NullCheck(L_2);
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.FileStream::get_Length() */, L_2);
		if ((int64_t)(L_3) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_3))));
		FileStream_t4292183065 * L_4 = V_1;
		ByteU5BU5D_t4116647657* L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = V_0;
		NullCheck(L_6);
		NullCheck(L_4);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(19 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))));
		FileStream_t4292183065 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_7);
		IL2CPP_LEAVE(0x3D, FINALLY_0030);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			FileStream_t4292183065 * L_8 = V_1;
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			FileStream_t4292183065 * L_9 = V_1;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_9);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003d:
	{
		ByteU5BU5D_t4116647657* L_10 = V_0;
		X509Crl_t1148767388 * L_11 = (X509Crl_t1148767388 *)il2cpp_codegen_object_new(X509Crl_t1148767388_il2cpp_TypeInfo_var);
		X509Crl__ctor_m1817187405(L_11, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(System.Byte[],System.DateTime,Mono.Security.X509.X509ExtensionCollection)
extern "C"  void X509CrlEntry__ctor_m783552701 (X509CrlEntry_t645568789 * __this, ByteU5BU5D_t4116647657* ___serialNumber0, DateTime_t3738529785  ___revocationDate1, X509ExtensionCollection_t609554709 * ___extensions2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CrlEntry__ctor_m783552701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_0 = ___serialNumber0;
		__this->set_sn_0(L_0);
		DateTime_t3738529785  L_1 = ___revocationDate1;
		__this->set_revocationDate_1(L_1);
		X509ExtensionCollection_t609554709 * L_2 = ___extensions2;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		X509ExtensionCollection_t609554709 * L_3 = (X509ExtensionCollection_t609554709 *)il2cpp_codegen_object_new(X509ExtensionCollection_t609554709_il2cpp_TypeInfo_var);
		X509ExtensionCollection__ctor_m2474799343(L_3, /*hidden argument*/NULL);
		__this->set_extensions_2(L_3);
		goto IL_0031;
	}

IL_002a:
	{
		X509ExtensionCollection_t609554709 * L_4 = ___extensions2;
		__this->set_extensions_2(L_4);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(Mono.Security.ASN1)
extern "C"  void X509CrlEntry__ctor_m4013514048 (X509CrlEntry_t645568789 * __this, ASN1_t2114160833 * ___entry0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CrlEntry__ctor_m4013514048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_0 = ___entry0;
		NullCheck(L_0);
		ASN1_t2114160833 * L_1 = ASN1_get_Item_m2255075813(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t4116647657* L_2 = ASN1_get_Value_m63296490(L_1, /*hidden argument*/NULL);
		__this->set_sn_0(L_2);
		ByteU5BU5D_t4116647657* L_3 = __this->get_sn_0();
		Array_Reverse_m3714848183(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_3, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_4 = ___entry0;
		NullCheck(L_4);
		ASN1_t2114160833 * L_5 = ASN1_get_Item_m2255075813(L_4, 1, /*hidden argument*/NULL);
		DateTime_t3738529785  L_6 = ASN1Convert_ToDateTime_m1246060840(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_revocationDate_1(L_6);
		ASN1_t2114160833 * L_7 = ___entry0;
		NullCheck(L_7);
		ASN1_t2114160833 * L_8 = ASN1_get_Item_m2255075813(L_7, 2, /*hidden argument*/NULL);
		X509ExtensionCollection_t609554709 * L_9 = (X509ExtensionCollection_t609554709 *)il2cpp_codegen_object_new(X509ExtensionCollection_t609554709_il2cpp_TypeInfo_var);
		X509ExtensionCollection__ctor_m551870633(L_9, L_8, /*hidden argument*/NULL);
		__this->set_extensions_2(L_9);
		return;
	}
}
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::get_SerialNumber()
extern "C"  ByteU5BU5D_t4116647657* X509CrlEntry_get_SerialNumber_m3627212772 (X509CrlEntry_t645568789 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CrlEntry_get_SerialNumber_m3627212772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = __this->get_sn_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1 = Array_Clone_m2672907798((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		return ((ByteU5BU5D_t4116647657*)Castclass((RuntimeObject*)L_1, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var));
	}
}
// System.DateTime Mono.Security.X509.X509Crl/X509CrlEntry::get_RevocationDate()
extern "C"  DateTime_t3738529785  X509CrlEntry_get_RevocationDate_m303599135 (X509CrlEntry_t645568789 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785  L_0 = __this->get_revocationDate_1();
		return L_0;
	}
}
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl/X509CrlEntry::get_Extensions()
extern "C"  X509ExtensionCollection_t609554709 * X509CrlEntry_get_Extensions_m3390427621 (X509CrlEntry_t645568789 * __this, const RuntimeMethod* method)
{
	{
		X509ExtensionCollection_t609554709 * L_0 = __this->get_extensions_2();
		return L_0;
	}
}
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* X509CrlEntry_GetBytes_m2980586034 (X509CrlEntry_t645568789 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509CrlEntry_GetBytes_m2980586034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ASN1_t2114160833 * V_0 = NULL;
	{
		ASN1_t2114160833 * L_0 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1239252869(L_0, (uint8_t)((int32_t)48), /*hidden argument*/NULL);
		V_0 = L_0;
		ASN1_t2114160833 * L_1 = V_0;
		ByteU5BU5D_t4116647657* L_2 = __this->get_sn_0();
		ASN1_t2114160833 * L_3 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m682794872(L_3, (uint8_t)2, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ASN1_Add_m2431139999(L_1, L_3, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_4 = V_0;
		DateTime_t3738529785  L_5 = __this->get_revocationDate_1();
		ASN1_t2114160833 * L_6 = ASN1Convert_FromDateTime_m1024852799(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ASN1_Add_m2431139999(L_4, L_6, /*hidden argument*/NULL);
		X509ExtensionCollection_t609554709 * L_7 = __this->get_extensions_2();
		NullCheck(L_7);
		int32_t L_8 = CollectionBase_get_Count_m1708965601(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		ASN1_t2114160833 * L_9 = V_0;
		X509ExtensionCollection_t609554709 * L_10 = __this->get_extensions_2();
		NullCheck(L_10);
		ByteU5BU5D_t4116647657* L_11 = X509ExtensionCollection_GetBytes_m1622025118(L_10, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_12 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1638893325(L_12, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		ASN1_Add_m2431139999(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0055:
	{
		ASN1_t2114160833 * L_13 = V_0;
		NullCheck(L_13);
		ByteU5BU5D_t4116647657* L_14 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_13);
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Extension::.ctor()
extern "C"  void X509Extension__ctor_m1812210650 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_extnCritical_1((bool)0);
		return;
	}
}
// System.Void Mono.Security.X509.X509Extension::.ctor(Mono.Security.ASN1)
extern "C"  void X509Extension__ctor_m710637961 (X509Extension_t3173393653 * __this, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension__ctor_m710637961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ASN1_t2114160833 * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	X509Extension_t3173393653 * G_B7_0 = NULL;
	X509Extension_t3173393653 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	X509Extension_t3173393653 * G_B8_1 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_0 = ___asn10;
		NullCheck(L_0);
		uint8_t L_1 = ASN1_get_Tag_m2789147236(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)48)))))
		{
			goto IL_001f;
		}
	}
	{
		ASN1_t2114160833 * L_2 = ___asn10;
		NullCheck(L_2);
		int32_t L_3 = ASN1_get_Count_m1789520042(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)2)))
		{
			goto IL_002f;
		}
	}

IL_001f:
	{
		String_t* L_4 = Locale_GetText_m3520169047(NULL /*static, unused*/, _stringLiteral1590810976, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002f:
	{
		ASN1_t2114160833 * L_6 = ___asn10;
		NullCheck(L_6);
		ASN1_t2114160833 * L_7 = ASN1_get_Item_m2255075813(L_6, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		uint8_t L_8 = ASN1_get_Tag_m2789147236(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)6)))
		{
			goto IL_0051;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m3520169047(NULL /*static, unused*/, _stringLiteral1590810976, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0051:
	{
		ASN1_t2114160833 * L_11 = ___asn10;
		NullCheck(L_11);
		ASN1_t2114160833 * L_12 = ASN1_get_Item_m2255075813(L_11, 0, /*hidden argument*/NULL);
		String_t* L_13 = ASN1Convert_ToOid_m3847701408(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_extnOid_0(L_13);
		ASN1_t2114160833 * L_14 = ___asn10;
		NullCheck(L_14);
		ASN1_t2114160833 * L_15 = ASN1_get_Item_m2255075813(L_14, 1, /*hidden argument*/NULL);
		NullCheck(L_15);
		uint8_t L_16 = ASN1_get_Tag_m2789147236(L_15, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			G_B7_0 = __this;
			goto IL_008d;
		}
	}
	{
		ASN1_t2114160833 * L_17 = ___asn10;
		NullCheck(L_17);
		ASN1_t2114160833 * L_18 = ASN1_get_Item_m2255075813(L_17, 1, /*hidden argument*/NULL);
		NullCheck(L_18);
		ByteU5BU5D_t4116647657* L_19 = ASN1_get_Value_m63296490(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = 0;
		uint8_t L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		G_B8_0 = ((((int32_t)L_21) == ((int32_t)((int32_t)255)))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_008e;
	}

IL_008d:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_008e:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_extnCritical_1((bool)G_B8_0);
		ASN1_t2114160833 * L_22 = ___asn10;
		ASN1_t2114160833 * L_23 = ___asn10;
		NullCheck(L_23);
		int32_t L_24 = ASN1_get_Count_m1789520042(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		ASN1_t2114160833 * L_25 = ASN1_get_Item_m2255075813(L_22, ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_extnValue_2(L_25);
		ASN1_t2114160833 * L_26 = __this->get_extnValue_2();
		NullCheck(L_26);
		uint8_t L_27 = ASN1_get_Tag_m2789147236(L_26, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)4))))
		{
			goto IL_010e;
		}
	}
	{
		ASN1_t2114160833 * L_28 = __this->get_extnValue_2();
		NullCheck(L_28);
		int32_t L_29 = ASN1_get_Length_m3269728307(L_28, /*hidden argument*/NULL);
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_010e;
		}
	}
	{
		ASN1_t2114160833 * L_30 = __this->get_extnValue_2();
		NullCheck(L_30);
		int32_t L_31 = ASN1_get_Count_m1789520042(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_010e;
		}
	}

IL_00d9:
	try
	{ // begin try (depth: 1)
		ASN1_t2114160833 * L_32 = __this->get_extnValue_2();
		NullCheck(L_32);
		ByteU5BU5D_t4116647657* L_33 = ASN1_get_Value_m63296490(L_32, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_34 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1638893325(L_34, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
		ASN1_t2114160833 * L_35 = __this->get_extnValue_2();
		NullCheck(L_35);
		ASN1_set_Value_m647861841(L_35, (ByteU5BU5D_t4116647657*)(ByteU5BU5D_t4116647657*)NULL, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_36 = __this->get_extnValue_2();
		ASN1_t2114160833 * L_37 = V_0;
		NullCheck(L_36);
		ASN1_Add_m2431139999(L_36, L_37, /*hidden argument*/NULL);
		goto IL_010e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0108;
		throw e;
	}

CATCH_0108:
	{ // begin catch(System.Object)
		goto IL_010e;
	} // end catch (depth: 1)

IL_010e:
	{
		VirtActionInvoker0::Invoke(4 /* System.Void Mono.Security.X509.X509Extension::Decode() */, __this);
		return;
	}
}
// System.Void Mono.Security.X509.X509Extension::.ctor(Mono.Security.X509.X509Extension)
extern "C"  void X509Extension__ctor_m1474351312 (X509Extension_t3173393653 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension__ctor_m1474351312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_0 = ___extension0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		X509Extension_t3173393653 * L_2 = ___extension0;
		NullCheck(L_2);
		ASN1_t2114160833 * L_3 = X509Extension_get_Value_m3529546267(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		X509Extension_t3173393653 * L_4 = ___extension0;
		NullCheck(L_4);
		ASN1_t2114160833 * L_5 = X509Extension_get_Value_m3529546267(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		uint8_t L_6 = ASN1_get_Tag_m2789147236(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_0044;
		}
	}
	{
		X509Extension_t3173393653 * L_7 = ___extension0;
		NullCheck(L_7);
		ASN1_t2114160833 * L_8 = X509Extension_get_Value_m3529546267(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = ASN1_get_Count_m1789520042(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0054;
		}
	}

IL_0044:
	{
		String_t* L_10 = Locale_GetText_m3520169047(NULL /*static, unused*/, _stringLiteral1590810976, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_11 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0054:
	{
		X509Extension_t3173393653 * L_12 = ___extension0;
		NullCheck(L_12);
		String_t* L_13 = X509Extension_get_Oid_m1003388288(L_12, /*hidden argument*/NULL);
		__this->set_extnOid_0(L_13);
		X509Extension_t3173393653 * L_14 = ___extension0;
		NullCheck(L_14);
		bool L_15 = X509Extension_get_Critical_m2974578711(L_14, /*hidden argument*/NULL);
		__this->set_extnCritical_1(L_15);
		X509Extension_t3173393653 * L_16 = ___extension0;
		NullCheck(L_16);
		ASN1_t2114160833 * L_17 = X509Extension_get_Value_m3529546267(L_16, /*hidden argument*/NULL);
		__this->set_extnValue_2(L_17);
		VirtActionInvoker0::Invoke(4 /* System.Void Mono.Security.X509.X509Extension::Decode() */, __this);
		return;
	}
}
// System.Void Mono.Security.X509.X509Extension::Decode()
extern "C"  void X509Extension_Decode_m3172373814 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509Extension::Encode()
extern "C"  void X509Extension_Encode_m3152909591 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// Mono.Security.ASN1 Mono.Security.X509.X509Extension::get_ASN1()
extern "C"  ASN1_t2114160833 * X509Extension_get_ASN1_m1064344075 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension_get_ASN1_m1064344075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ASN1_t2114160833 * V_0 = NULL;
	{
		ASN1_t2114160833 * L_0 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1239252869(L_0, (uint8_t)((int32_t)48), /*hidden argument*/NULL);
		V_0 = L_0;
		ASN1_t2114160833 * L_1 = V_0;
		String_t* L_2 = __this->get_extnOid_0();
		ASN1_t2114160833 * L_3 = ASN1Convert_FromOid_m1517037532(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ASN1_Add_m2431139999(L_1, L_3, /*hidden argument*/NULL);
		bool L_4 = __this->get_extnCritical_1();
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		ASN1_t2114160833 * L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)((int32_t)255));
		ASN1_t2114160833 * L_7 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m682794872(L_7, (uint8_t)1, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ASN1_Add_m2431139999(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0040:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void Mono.Security.X509.X509Extension::Encode() */, __this);
		ASN1_t2114160833 * L_8 = V_0;
		ASN1_t2114160833 * L_9 = __this->get_extnValue_2();
		NullCheck(L_8);
		ASN1_Add_m2431139999(L_8, L_9, /*hidden argument*/NULL);
		ASN1_t2114160833 * L_10 = V_0;
		return L_10;
	}
}
// System.String Mono.Security.X509.X509Extension::get_Oid()
extern "C"  String_t* X509Extension_get_Oid_m1003388288 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_extnOid_0();
		return L_0;
	}
}
// System.Boolean Mono.Security.X509.X509Extension::get_Critical()
extern "C"  bool X509Extension_get_Critical_m2974578711 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_extnCritical_1();
		return L_0;
	}
}
// System.Void Mono.Security.X509.X509Extension::set_Critical(System.Boolean)
extern "C"  void X509Extension_set_Critical_m1057755976 (X509Extension_t3173393653 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_extnCritical_1(L_0);
		return;
	}
}
// System.String Mono.Security.X509.X509Extension::get_Name()
extern "C"  String_t* X509Extension_get_Name_m4129189097 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_extnOid_0();
		return L_0;
	}
}
// Mono.Security.ASN1 Mono.Security.X509.X509Extension::get_Value()
extern "C"  ASN1_t2114160833 * X509Extension_get_Value_m3529546267 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		ASN1_t2114160833 * L_0 = __this->get_extnValue_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(5 /* System.Void Mono.Security.X509.X509Extension::Encode() */, __this);
	}

IL_0011:
	{
		ASN1_t2114160833 * L_1 = __this->get_extnValue_2();
		return L_1;
	}
}
// System.Boolean Mono.Security.X509.X509Extension::Equals(System.Object)
extern "C"  bool X509Extension_Equals_m1779194186 (X509Extension_t3173393653 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension_Equals_m1779194186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Extension_t3173393653 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		RuntimeObject * L_1 = ___obj0;
		V_0 = ((X509Extension_t3173393653 *)IsInstClass((RuntimeObject*)L_1, X509Extension_t3173393653_il2cpp_TypeInfo_var));
		X509Extension_t3173393653 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		bool L_3 = __this->get_extnCritical_1();
		X509Extension_t3173393653 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = L_4->get_extnCritical_1();
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		String_t* L_6 = __this->get_extnOid_0();
		X509Extension_t3173393653 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_extnOid_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		ASN1_t2114160833 * L_10 = __this->get_extnValue_2();
		NullCheck(L_10);
		int32_t L_11 = ASN1_get_Length_m3269728307(L_10, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_12 = V_0;
		NullCheck(L_12);
		ASN1_t2114160833 * L_13 = L_12->get_extnValue_2();
		NullCheck(L_13);
		int32_t L_14 = ASN1_get_Length_m3269728307(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)L_14)))
		{
			goto IL_005f;
		}
	}
	{
		return (bool)0;
	}

IL_005f:
	{
		V_1 = 0;
		goto IL_0089;
	}

IL_0066:
	{
		ASN1_t2114160833 * L_15 = __this->get_extnValue_2();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		ASN1_t2114160833 * L_17 = ASN1_get_Item_m2255075813(L_15, L_16, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_18 = V_0;
		NullCheck(L_18);
		ASN1_t2114160833 * L_19 = L_18->get_extnValue_2();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		ASN1_t2114160833 * L_21 = ASN1_get_Item_m2255075813(L_19, L_20, /*hidden argument*/NULL);
		if ((((RuntimeObject*)(ASN1_t2114160833 *)L_17) == ((RuntimeObject*)(ASN1_t2114160833 *)L_21)))
		{
			goto IL_0085;
		}
	}
	{
		return (bool)0;
	}

IL_0085:
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0089:
	{
		int32_t L_23 = V_1;
		ASN1_t2114160833 * L_24 = __this->get_extnValue_2();
		NullCheck(L_24);
		int32_t L_25 = ASN1_get_Length_m3269728307(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_0066;
		}
	}
	{
		return (bool)1;
	}
}
// System.Byte[] Mono.Security.X509.X509Extension::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* X509Extension_GetBytes_m3083683821 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		ASN1_t2114160833 * L_0 = X509Extension_get_ASN1_m1064344075(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t4116647657* L_1 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_0);
		return L_1;
	}
}
// System.Int32 Mono.Security.X509.X509Extension::GetHashCode()
extern "C"  int32_t X509Extension_GetHashCode_m1797796679 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_extnOid_0();
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m1906374149(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Mono.Security.X509.X509Extension::WriteLine(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C"  void X509Extension_WriteLine_m1662885247 (X509Extension_t3173393653 * __this, StringBuilder_t * ___sb0, int32_t ___n1, int32_t ___pos2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension_WriteLine_m1662885247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	{
		ASN1_t2114160833 * L_0 = __this->get_extnValue_2();
		NullCheck(L_0);
		ByteU5BU5D_t4116647657* L_1 = ASN1_get_Value_m63296490(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___pos2;
		V_1 = L_2;
		V_2 = 0;
		goto IL_005e;
	}

IL_0015:
	{
		int32_t L_3 = V_2;
		int32_t L_4 = ___n1;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		StringBuilder_t * L_5 = ___sb0;
		ByteU5BU5D_t4116647657* L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		NullCheck(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_9 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = Byte_ToString_m4063101981(((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))), _stringLiteral3451435000, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m1965104174(L_5, L_10, /*hidden argument*/NULL);
		StringBuilder_t * L_11 = ___sb0;
		NullCheck(L_11);
		StringBuilder_Append_m1965104174(L_11, _stringLiteral3452614528, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_004e:
	{
		StringBuilder_t * L_12 = ___sb0;
		NullCheck(L_12);
		StringBuilder_Append_m1965104174(L_12, _stringLiteral3786055882, /*hidden argument*/NULL);
	}

IL_005a:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_005e:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) < ((int32_t)8)))
		{
			goto IL_0015;
		}
	}
	{
		StringBuilder_t * L_15 = ___sb0;
		NullCheck(L_15);
		StringBuilder_Append_m1965104174(L_15, _stringLiteral3450517376, /*hidden argument*/NULL);
		int32_t L_16 = ___pos2;
		V_1 = L_16;
		V_3 = 0;
		goto IL_00af;
	}

IL_007a:
	{
		ByteU5BU5D_t4116647657* L_17 = V_0;
		int32_t L_18 = V_1;
		int32_t L_19 = L_18;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		NullCheck(L_17);
		int32_t L_20 = L_19;
		uint8_t L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_4 = L_21;
		uint8_t L_22 = V_4;
		if ((((int32_t)L_22) >= ((int32_t)((int32_t)32))))
		{
			goto IL_009d;
		}
	}
	{
		StringBuilder_t * L_23 = ___sb0;
		NullCheck(L_23);
		StringBuilder_Append_m1965104174(L_23, _stringLiteral3452614530, /*hidden argument*/NULL);
		goto IL_00ab;
	}

IL_009d:
	{
		StringBuilder_t * L_24 = ___sb0;
		uint8_t L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		Il2CppChar L_26 = Convert_ToChar_m2532412511(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m2383614642(L_24, L_26, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_00af:
	{
		int32_t L_28 = V_3;
		int32_t L_29 = ___n1;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_007a;
		}
	}
	{
		StringBuilder_t * L_30 = ___sb0;
		String_t* L_31 = Environment_get_NewLine_m3211016485(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m1965104174(L_30, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Security.X509.X509Extension::ToString()
extern "C"  String_t* X509Extension_ToString_m3727002866 (X509Extension_t3173393653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Extension_ToString_m3727002866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ASN1_t2114160833 * L_1 = __this->get_extnValue_2();
		NullCheck(L_1);
		int32_t L_2 = ASN1_get_Length_m3269728307(L_1, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_2>>(int32_t)3));
		ASN1_t2114160833 * L_3 = __this->get_extnValue_2();
		NullCheck(L_3);
		int32_t L_4 = ASN1_get_Length_m3269728307(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)((int32_t)((int32_t)L_5<<(int32_t)3))));
		V_3 = 0;
		V_4 = 0;
		goto IL_0041;
	}

IL_002e:
	{
		StringBuilder_t * L_6 = V_0;
		int32_t L_7 = V_3;
		X509Extension_WriteLine_m1662885247(__this, L_6, 8, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)8));
		int32_t L_9 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0041:
	{
		int32_t L_10 = V_4;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_002e;
		}
	}
	{
		StringBuilder_t * L_12 = V_0;
		int32_t L_13 = V_2;
		int32_t L_14 = V_3;
		X509Extension_WriteLine_m1662885247(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		StringBuilder_t * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = StringBuilder_ToString_m3317489284(L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor()
extern "C"  void X509ExtensionCollection__ctor_m2474799343 (X509ExtensionCollection_t609554709 * __this, const RuntimeMethod* method)
{
	{
		CollectionBase__ctor_m3343513710(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor(Mono.Security.ASN1)
extern "C"  void X509ExtensionCollection__ctor_m551870633 (X509ExtensionCollection_t609554709 * __this, ASN1_t2114160833 * ___asn10, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection__ctor_m551870633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	X509Extension_t3173393653 * V_1 = NULL;
	{
		X509ExtensionCollection__ctor_m2474799343(__this, /*hidden argument*/NULL);
		__this->set_readOnly_1((bool)1);
		ASN1_t2114160833 * L_0 = ___asn10;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		ASN1_t2114160833 * L_1 = ___asn10;
		NullCheck(L_1);
		uint8_t L_2 = ASN1_get_Tag_m2789147236(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)48))))
		{
			goto IL_002c;
		}
	}
	{
		Exception_t * L_3 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m1152696503(L_3, _stringLiteral632220839, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002c:
	{
		V_0 = 0;
		goto IL_0051;
	}

IL_0033:
	{
		ASN1_t2114160833 * L_4 = ___asn10;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		ASN1_t2114160833 * L_6 = ASN1_get_Item_m2255075813(L_4, L_5, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_7 = (X509Extension_t3173393653 *)il2cpp_codegen_object_new(X509Extension_t3173393653_il2cpp_TypeInfo_var);
		X509Extension__ctor_m710637961(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		ArrayList_t2718874744 * L_8 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_9 = V_1;
		NullCheck(L_8);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_0;
		ASN1_t2114160833 * L_12 = ___asn10;
		NullCheck(L_12);
		int32_t L_13 = ASN1_get_Count_m1789520042(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0033;
		}
	}
	{
		return;
	}
}
// System.Collections.IEnumerator Mono.Security.X509.X509ExtensionCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1475785462 (X509ExtensionCollection_t609554709 * __this, const RuntimeMethod* method)
{
	{
		ArrayList_t2718874744 * L_0 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1 = VirtFuncInvoker0< RuntimeObject* >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Int32 Mono.Security.X509.X509ExtensionCollection::Add(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_Add_m2251235768 (X509ExtensionCollection_t609554709 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_Add_m2251235768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Extension_t3173393653 * L_0 = ___extension0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		bool L_2 = __this->get_readOnly_1();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		NotSupportedException_t1314879016 * L_3 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_3, _stringLiteral1609817887, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		ArrayList_t2718874744 * L_4 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_5 = ___extension0;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_5);
		return L_6;
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509Extension[])
extern "C"  void X509ExtensionCollection_AddRange_m3305410105 (X509ExtensionCollection_t609554709 * __this, X509ExtensionU5BU5D_t3108766909* ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_AddRange_m3305410105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		X509ExtensionU5BU5D_t3108766909* L_0 = ___extension0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		bool L_2 = __this->get_readOnly_1();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		NotSupportedException_t1314879016 * L_3 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_3, _stringLiteral1609817887, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		V_0 = 0;
		goto IL_0041;
	}

IL_002e:
	{
		ArrayList_t2718874744 * L_4 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509ExtensionU5BU5D_t3108766909* L_5 = ___extension0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		X509Extension_t3173393653 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_4);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_8);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0041:
	{
		int32_t L_10 = V_0;
		X509ExtensionU5BU5D_t3108766909* L_11 = ___extension0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509ExtensionCollection)
extern "C"  void X509ExtensionCollection_AddRange_m1104225129 (X509ExtensionCollection_t609554709 * __this, X509ExtensionCollection_t609554709 * ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_AddRange_m1104225129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		X509ExtensionCollection_t609554709 * L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3723644332, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		bool L_2 = __this->get_readOnly_1();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		NotSupportedException_t1314879016 * L_3 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_3, _stringLiteral1609817887, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		V_0 = 0;
		goto IL_0045;
	}

IL_002e:
	{
		ArrayList_t2718874744 * L_4 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509ExtensionCollection_t609554709 * L_5 = ___collection0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		X509Extension_t3173393653 * L_7 = X509ExtensionCollection_get_Item_m2771335836(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0045:
	{
		int32_t L_9 = V_0;
		X509ExtensionCollection_t609554709 * L_10 = ___collection0;
		NullCheck(L_10);
		ArrayList_t2718874744 * L_11 = CollectionBase_get_InnerList_m132195395(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_9) < ((int32_t)L_12)))
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(Mono.Security.X509.X509Extension)
extern "C"  bool X509ExtensionCollection_Contains_m2820978563 (X509ExtensionCollection_t609554709 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method)
{
	{
		X509Extension_t3173393653 * L_0 = ___extension0;
		int32_t L_1 = X509ExtensionCollection_IndexOf_m2606992261(__this, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(System.String)
extern "C"  bool X509ExtensionCollection_Contains_m922133374 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___oid0;
		int32_t L_1 = X509ExtensionCollection_IndexOf_m2996504451(__this, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::CopyTo(Mono.Security.X509.X509Extension[],System.Int32)
extern "C"  void X509ExtensionCollection_CopyTo_m3181643769 (X509ExtensionCollection_t609554709 * __this, X509ExtensionU5BU5D_t3108766909* ___extensions0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_CopyTo_m3181643769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509ExtensionU5BU5D_t3108766909* L_0 = ___extensions0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1607280970, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t2718874744 * L_2 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509ExtensionU5BU5D_t3108766909* L_3 = ___extensions0;
		int32_t L_4 = ___index1;
		NullCheck(L_2);
		VirtActionInvoker2< RuntimeArray *, int32_t >::Invoke(41 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_2, (RuntimeArray *)(RuntimeArray *)L_3, L_4);
		return;
	}
}
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m2606992261 (X509ExtensionCollection_t609554709 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_IndexOf_m2606992261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	X509Extension_t3173393653 * V_1 = NULL;
	{
		X509Extension_t3173393653 * L_0 = ___extension0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0018:
	{
		ArrayList_t2718874744 * L_2 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_1 = ((X509Extension_t3173393653 *)CastclassClass((RuntimeObject*)L_4, X509Extension_t3173393653_il2cpp_TypeInfo_var));
		X509Extension_t3173393653 * L_5 = V_1;
		X509Extension_t3173393653 * L_6 = ___extension0;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean Mono.Security.X509.X509Extension::Equals(System.Object) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		ArrayList_t2718874744 * L_11 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0018;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(System.String)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m2996504451 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_IndexOf_m2996504451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	X509Extension_t3173393653 * V_1 = NULL;
	{
		String_t* L_0 = ___oid0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3266464951, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_0041;
	}

IL_0018:
	{
		ArrayList_t2718874744 * L_2 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_1 = ((X509Extension_t3173393653 *)CastclassClass((RuntimeObject*)L_4, X509Extension_t3173393653_il2cpp_TypeInfo_var));
		X509Extension_t3173393653 * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = X509Extension_get_Oid_m1003388288(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___oid0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_9 = V_0;
		return L_9;
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0041:
	{
		int32_t L_11 = V_0;
		ArrayList_t2718874744 * L_12 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0018;
		}
	}
	{
		return (-1);
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::Insert(System.Int32,Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Insert_m873879632 (X509ExtensionCollection_t609554709 * __this, int32_t ___index0, X509Extension_t3173393653 * ___extension1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_Insert_m873879632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Extension_t3173393653 * L_0 = ___extension1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t2718874744 * L_2 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___index0;
		X509Extension_t3173393653 * L_4 = ___extension1;
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, RuntimeObject * >::Invoke(36 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_2, L_3, L_4);
		return;
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Remove_m2342091090 (X509ExtensionCollection_t609554709 * __this, X509Extension_t3173393653 * ___extension0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_Remove_m2342091090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		X509Extension_t3173393653 * L_0 = ___extension0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1610623306, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		ArrayList_t2718874744 * L_2 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		X509Extension_t3173393653 * L_3 = ___extension0;
		NullCheck(L_2);
		VirtActionInvoker1< RuntimeObject * >::Invoke(38 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_2, L_3);
		return;
	}
}
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(System.String)
extern "C"  void X509ExtensionCollection_Remove_m2521723919 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_Remove_m2521723919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___oid0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3266464951, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___oid0;
		int32_t L_3 = X509ExtensionCollection_IndexOf_m2996504451(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002c;
		}
	}
	{
		ArrayList_t2718874744 * L_5 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_5, L_6);
	}

IL_002c:
	{
		return;
	}
}
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.Int32)
extern "C"  X509Extension_t3173393653 * X509ExtensionCollection_get_Item_m2771335836 (X509ExtensionCollection_t609554709 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_get_Item_m2771335836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t2718874744 * L_0 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		RuntimeObject * L_2 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((X509Extension_t3173393653 *)CastclassClass((RuntimeObject*)L_2, X509Extension_t3173393653_il2cpp_TypeInfo_var));
	}
}
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.String)
extern "C"  X509Extension_t3173393653 * X509ExtensionCollection_get_Item_m4249795832 (X509ExtensionCollection_t609554709 * __this, String_t* ___oid0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_get_Item_m4249795832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___oid0;
		int32_t L_1 = X509ExtensionCollection_IndexOf_m2996504451(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (X509Extension_t3173393653 *)NULL;
	}

IL_0011:
	{
		ArrayList_t2718874744 * L_3 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_5 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		return ((X509Extension_t3173393653 *)CastclassClass((RuntimeObject*)L_5, X509Extension_t3173393653_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] Mono.Security.X509.X509ExtensionCollection::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* X509ExtensionCollection_GetBytes_m1622025118 (X509ExtensionCollection_t609554709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509ExtensionCollection_GetBytes_m1622025118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ASN1_t2114160833 * V_0 = NULL;
	int32_t V_1 = 0;
	X509Extension_t3173393653 * V_2 = NULL;
	{
		ArrayList_t2718874744 * L_0 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		if ((((int32_t)L_1) >= ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		return (ByteU5BU5D_t4116647657*)NULL;
	}

IL_0013:
	{
		ASN1_t2114160833 * L_2 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1239252869(L_2, (uint8_t)((int32_t)48), /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0045;
	}

IL_0022:
	{
		ArrayList_t2718874744 * L_3 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		RuntimeObject * L_5 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		V_2 = ((X509Extension_t3173393653 *)CastclassClass((RuntimeObject*)L_5, X509Extension_t3173393653_il2cpp_TypeInfo_var));
		ASN1_t2114160833 * L_6 = V_0;
		X509Extension_t3173393653 * L_7 = V_2;
		NullCheck(L_7);
		ASN1_t2114160833 * L_8 = X509Extension_get_ASN1_m1064344075(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ASN1_Add_m2431139999(L_6, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0045:
	{
		int32_t L_10 = V_1;
		ArrayList_t2718874744 * L_11 = CollectionBase_get_InnerList_m132195395(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0022;
		}
	}
	{
		ASN1_t2114160833 * L_13 = V_0;
		NullCheck(L_13);
		ByteU5BU5D_t4116647657* L_14 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_13);
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Store::.ctor(System.String,System.Boolean)
extern "C"  void X509Store__ctor_m2736551756 (X509Store_t2777415284 * __this, String_t* ___path0, bool ___crl1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___path0;
		__this->set__storePath_0(L_0);
		bool L_1 = ___crl1;
		__this->set__crl_3(L_1);
		return;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::get_Certificates()
extern "C"  X509CertificateCollection_t1542168550 * X509Store_get_Certificates_m1092347772 (X509Store_t2777415284 * __this, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get__certificates_1();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		X509CertificateCollection_t1542168550 * L_2 = X509Store_BuildCertificatesCollection_m3030935583(__this, L_1, /*hidden argument*/NULL);
		__this->set__certificates_1(L_2);
	}

IL_001d:
	{
		X509CertificateCollection_t1542168550 * L_3 = __this->get__certificates_1();
		return L_3;
	}
}
// System.Collections.ArrayList Mono.Security.X509.X509Store::get_Crls()
extern "C"  ArrayList_t2718874744 * X509Store_get_Crls_m1211262034 (X509Store_t2777415284 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_get_Crls_m1211262034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__crl_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArrayList_t2718874744 * L_1 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_1, /*hidden argument*/NULL);
		__this->set__crls_2(L_1);
	}

IL_0016:
	{
		ArrayList_t2718874744 * L_2 = __this->get__crls_2();
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_3 = __this->get__storePath_0();
		ArrayList_t2718874744 * L_4 = X509Store_BuildCrlsCollection_m1991312527(__this, L_3, /*hidden argument*/NULL);
		__this->set__crls_2(L_4);
	}

IL_0033:
	{
		ArrayList_t2718874744 * L_5 = __this->get__crls_2();
		return L_5;
	}
}
// System.String Mono.Security.X509.X509Store::get_Name()
extern "C"  String_t* X509Store_get_Name_m597920689 (X509Store_t2777415284 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_get_Name_m597920689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = __this->get__name_4();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = ((Path_t1605229823_StaticFields*)il2cpp_codegen_static_fields_for(Path_t1605229823_il2cpp_TypeInfo_var))->get_DirectorySeparatorChar_2();
		NullCheck(L_1);
		int32_t L_3 = String_LastIndexOf_m3451222878(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = __this->get__storePath_0();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		String_t* L_6 = String_Substring_m2848979100(L_4, ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)), /*hidden argument*/NULL);
		__this->set__name_4(L_6);
	}

IL_0030:
	{
		String_t* L_7 = __this->get__name_4();
		return L_7;
	}
}
// System.Void Mono.Security.X509.X509Store::Clear()
extern "C"  void X509Store_Clear_m2126432876 (X509Store_t2777415284 * __this, const RuntimeMethod* method)
{
	{
		X509CertificateCollection_t1542168550 * L_0 = __this->get__certificates_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_1 = __this->get__certificates_1();
		NullCheck(L_1);
		CollectionBase_Clear_m1509125218(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set__certificates_1((X509CertificateCollection_t1542168550 *)NULL);
		ArrayList_t2718874744 * L_2 = __this->get__crls_2();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		ArrayList_t2718874744 * L_3 = __this->get__crls_2();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_3);
	}

IL_0033:
	{
		__this->set__crls_2((ArrayList_t2718874744 *)NULL);
		return;
	}
}
// System.Void Mono.Security.X509.X509Store::Import(Mono.Security.X509.X509Certificate)
extern "C"  void X509Store_Import_m2542166443 (X509Store_t2777415284 * __this, X509Certificate_t489243025 * ___certificate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_Import_m2542166443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	FileStream_t4292183065 * V_1 = NULL;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = __this->get__storePath_0();
		X509Store_CheckStore_m2045435685(__this, L_0, (bool)1, /*hidden argument*/NULL);
		String_t* L_1 = __this->get__storePath_0();
		X509Certificate_t489243025 * L_2 = ___certificate0;
		String_t* L_3 = X509Store_GetUniqueName_m4271638378(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_4 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		bool L_6 = File_Exists_m3943585060(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_7 = V_0;
		FileStream_t4292183065 * L_8 = File_Create_m2207667142(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		X509Certificate_t489243025 * L_9 = ___certificate0;
		NullCheck(L_9);
		ByteU5BU5D_t4116647657* L_10 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_9);
		V_2 = L_10;
		FileStream_t4292183065 * L_11 = V_1;
		ByteU5BU5D_t4116647657* L_12 = V_2;
		ByteU5BU5D_t4116647657* L_13 = V_2;
		NullCheck(L_13);
		NullCheck(L_11);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.FileStream::Write(System.Byte[],System.Int32,System.Int32) */, L_11, L_12, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))));
		FileStream_t4292183065 * L_14 = V_1;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_14);
		IL2CPP_LEAVE(0x5D, FINALLY_0050);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			FileStream_t4292183065 * L_15 = V_1;
			if (!L_15)
			{
				goto IL_005c;
			}
		}

IL_0056:
		{
			FileStream_t4292183065 * L_16 = V_1;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_16);
		}

IL_005c:
		{
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005d:
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509Store::Import(Mono.Security.X509.X509Crl)
extern "C"  void X509Store_Import_m1052667485 (X509Store_t2777415284 * __this, X509Crl_t1148767388 * ___crl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_Import_m1052667485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	FileStream_t4292183065 * V_1 = NULL;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = __this->get__storePath_0();
		X509Store_CheckStore_m2045435685(__this, L_0, (bool)1, /*hidden argument*/NULL);
		String_t* L_1 = __this->get__storePath_0();
		X509Crl_t1148767388 * L_2 = ___crl0;
		String_t* L_3 = X509Store_GetUniqueName_m3285060726(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_4 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		bool L_6 = File_Exists_m3943585060(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_7 = V_0;
		FileStream_t4292183065 * L_8 = File_Create_m2207667142(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		X509Crl_t1148767388 * L_9 = ___crl0;
		NullCheck(L_9);
		ByteU5BU5D_t4116647657* L_10 = X509Crl_get_RawData_m3623997699(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		FileStream_t4292183065 * L_11 = V_1;
		ByteU5BU5D_t4116647657* L_12 = V_2;
		ByteU5BU5D_t4116647657* L_13 = V_2;
		NullCheck(L_13);
		NullCheck(L_11);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.FileStream::Write(System.Byte[],System.Int32,System.Int32) */, L_11, L_12, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))));
		IL2CPP_LEAVE(0x57, FINALLY_004a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		{
			FileStream_t4292183065 * L_14 = V_1;
			if (!L_14)
			{
				goto IL_0056;
			}
		}

IL_0050:
		{
			FileStream_t4292183065 * L_15 = V_1;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_15);
		}

IL_0056:
		{
			IL2CPP_END_FINALLY(74)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0057:
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509Store::Remove(Mono.Security.X509.X509Certificate)
extern "C"  void X509Store_Remove_m3732268634 (X509Store_t2777415284 * __this, X509Certificate_t489243025 * ___certificate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_Remove_m3732268634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get__storePath_0();
		X509Certificate_t489243025 * L_1 = ___certificate0;
		String_t* L_2 = X509Store_GetUniqueName_m4271638378(__this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_Combine_m3389272516(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		bool L_5 = File_Exists_m3943585060(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_6 = V_0;
		File_Delete_m321251800(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Mono.Security.X509.X509Store::Remove(Mono.Security.X509.X509Crl)
extern "C"  void X509Store_Remove_m1800501587 (X509Store_t2777415284 * __this, X509Crl_t1148767388 * ___crl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_Remove_m1800501587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get__storePath_0();
		X509Crl_t1148767388 * L_1 = ___crl0;
		String_t* L_2 = X509Store_GetUniqueName_m3285060726(__this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_Combine_m3389272516(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		bool L_5 = File_Exists_m3943585060(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_6 = V_0;
		File_Delete_m321251800(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Certificate)
extern "C"  String_t* X509Store_GetUniqueName_m4271638378 (X509Store_t2777415284 * __this, X509Certificate_t489243025 * ___certificate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_GetUniqueName_m4271638378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	{
		X509Certificate_t489243025 * L_0 = ___certificate0;
		NullCheck(L_0);
		X509ExtensionCollection_t609554709 * L_1 = X509Certificate_get_Extensions_m2532937142(L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_2 = X509Store_GetUniqueName_m132964055(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		ByteU5BU5D_t4116647657* L_3 = V_1;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = _stringLiteral227815590;
		X509Certificate_t489243025 * L_4 = ___certificate0;
		NullCheck(L_4);
		ByteU5BU5D_t4116647657* L_5 = X509Certificate_get_Hash_m410033711(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002b;
	}

IL_0025:
	{
		V_0 = _stringLiteral3313388058;
	}

IL_002b:
	{
		String_t* L_6 = V_0;
		ByteU5BU5D_t4116647657* L_7 = V_1;
		String_t* L_8 = X509Store_GetUniqueName_m915074968(__this, L_6, L_7, _stringLiteral1903542547, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Crl)
extern "C"  String_t* X509Store_GetUniqueName_m3285060726 (X509Store_t2777415284 * __this, X509Crl_t1148767388 * ___crl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_GetUniqueName_m3285060726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	{
		X509Crl_t1148767388 * L_0 = ___crl0;
		NullCheck(L_0);
		X509ExtensionCollection_t609554709 * L_1 = X509Crl_get_Extensions_m922657393(L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_2 = X509Store_GetUniqueName_m132964055(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		ByteU5BU5D_t4116647657* L_3 = V_1;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = _stringLiteral227815590;
		X509Crl_t1148767388 * L_4 = ___crl0;
		NullCheck(L_4);
		ByteU5BU5D_t4116647657* L_5 = X509Crl_get_Hash_m464217067(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002b;
	}

IL_0025:
	{
		V_0 = _stringLiteral3313388058;
	}

IL_002b:
	{
		String_t* L_6 = V_0;
		ByteU5BU5D_t4116647657* L_7 = V_1;
		String_t* L_8 = X509Store_GetUniqueName_m915074968(__this, L_6, L_7, _stringLiteral3977648530, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Byte[] Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509ExtensionCollection)
extern "C"  ByteU5BU5D_t4116647657* X509Store_GetUniqueName_m132964055 (X509Store_t2777415284 * __this, X509ExtensionCollection_t609554709 * ___extensions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_GetUniqueName_m132964055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509Extension_t3173393653 * V_0 = NULL;
	SubjectKeyIdentifierExtension_t2404375272 * V_1 = NULL;
	{
		X509ExtensionCollection_t609554709 * L_0 = ___extensions0;
		NullCheck(L_0);
		X509Extension_t3173393653 * L_1 = X509ExtensionCollection_get_Item_m4249795832(L_0, _stringLiteral1004423983, /*hidden argument*/NULL);
		V_0 = L_1;
		X509Extension_t3173393653 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return (ByteU5BU5D_t4116647657*)NULL;
	}

IL_0014:
	{
		X509Extension_t3173393653 * L_3 = V_0;
		SubjectKeyIdentifierExtension_t2404375272 * L_4 = (SubjectKeyIdentifierExtension_t2404375272 *)il2cpp_codegen_object_new(SubjectKeyIdentifierExtension_t2404375272_il2cpp_TypeInfo_var);
		SubjectKeyIdentifierExtension__ctor_m2055470965(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		SubjectKeyIdentifierExtension_t2404375272 * L_5 = V_1;
		NullCheck(L_5);
		ByteU5BU5D_t4116647657* L_6 = SubjectKeyIdentifierExtension_get_Identifier_m3780825379(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String Mono.Security.X509.X509Store::GetUniqueName(System.String,System.Byte[],System.String)
extern "C"  String_t* X509Store_GetUniqueName_m915074968 (X509Store_t2777415284 * __this, String_t* ___method0, ByteU5BU5D_t4116647657* ___name1, String_t* ___fileExtension2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_GetUniqueName_m915074968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	uint8_t V_1 = 0x0;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___method0;
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2989139009(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Append_m1965104174(L_2, _stringLiteral3452614531, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_3 = ___name1;
		V_2 = L_3;
		V_3 = 0;
		goto IL_003c;
	}

IL_001c:
	{
		ByteU5BU5D_t4116647657* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		StringBuilder_t * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_9 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = Byte_ToString_m4063101981((&V_1), _stringLiteral3451435000, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1965104174(L_8, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_12 = V_3;
		ByteU5BU5D_t4116647657* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t * L_14 = V_0;
		String_t* L_15 = ___fileExtension2;
		NullCheck(L_14);
		StringBuilder_Append_m1965104174(L_14, L_15, /*hidden argument*/NULL);
		StringBuilder_t * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m3317489284(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Byte[] Mono.Security.X509.X509Store::Load(System.String)
extern "C"  ByteU5BU5D_t4116647657* X509Store_Load_m2048139132 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_Load_m2048139132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	FileStream_t4292183065 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (ByteU5BU5D_t4116647657*)NULL;
		String_t* L_0 = ___filename0;
		FileStream_t4292183065 * L_1 = File_OpenRead_m2936789020(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		FileStream_t4292183065 * L_2 = V_1;
		NullCheck(L_2);
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.FileStream::get_Length() */, L_2);
		if ((int64_t)(L_3) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_3))));
		FileStream_t4292183065 * L_4 = V_1;
		ByteU5BU5D_t4116647657* L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = V_0;
		NullCheck(L_6);
		NullCheck(L_4);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(19 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))));
		FileStream_t4292183065 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_7);
		IL2CPP_LEAVE(0x3A, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		{
			FileStream_t4292183065 * L_8 = V_1;
			if (!L_8)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			FileStream_t4292183065 * L_9 = V_1;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_9);
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(45)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003a:
	{
		ByteU5BU5D_t4116647657* L_10 = V_0;
		return L_10;
	}
}
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Store::LoadCertificate(System.String)
extern "C"  X509Certificate_t489243025 * X509Store_LoadCertificate_m1587806288 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_LoadCertificate_m1587806288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	X509Certificate_t489243025 * V_1 = NULL;
	{
		String_t* L_0 = ___filename0;
		ByteU5BU5D_t4116647657* L_1 = X509Store_Load_m2048139132(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t4116647657* L_2 = V_0;
		X509Certificate_t489243025 * L_3 = (X509Certificate_t489243025 *)il2cpp_codegen_object_new(X509Certificate_t489243025_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m553243489(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		X509Certificate_t489243025 * L_4 = V_1;
		return L_4;
	}
}
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Store::LoadCrl(System.String)
extern "C"  X509Crl_t1148767388 * X509Store_LoadCrl_m1881903843 (X509Store_t2777415284 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_LoadCrl_m1881903843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	X509Crl_t1148767388 * V_1 = NULL;
	{
		String_t* L_0 = ___filename0;
		ByteU5BU5D_t4116647657* L_1 = X509Store_Load_m2048139132(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t4116647657* L_2 = V_0;
		X509Crl_t1148767388 * L_3 = (X509Crl_t1148767388 *)il2cpp_codegen_object_new(X509Crl_t1148767388_il2cpp_TypeInfo_var);
		X509Crl__ctor_m1817187405(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		X509Crl_t1148767388 * L_4 = V_1;
		return L_4;
	}
}
// System.Boolean Mono.Security.X509.X509Store::CheckStore(System.String,System.Boolean)
extern "C"  bool X509Store_CheckStore_m2045435685 (X509Store_t2777415284 * __this, String_t* ___path0, bool ___throwException1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_CheckStore_m2045435685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___path0;
			bool L_1 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_0012;
			}
		}

IL_000b:
		{
			V_0 = (bool)1;
			goto IL_003f;
		}

IL_0012:
		{
			String_t* L_2 = ___path0;
			Directory_CreateDirectory_m751642867(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			String_t* L_3 = ___path0;
			bool L_4 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			goto IL_003f;
		}

IL_0025:
		{
			; // IL_0025: leave IL_003f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.Object)
		{
			bool L_5 = ___throwException1;
			if (!L_5)
			{
				goto IL_0033;
			}
		}

IL_0031:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0033:
		{
			V_0 = (bool)0;
			goto IL_003f;
		}

IL_003a:
		{
			; // IL_003a: leave IL_003f
		}
	} // end catch (depth: 1)

IL_003f:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::BuildCertificatesCollection(System.String)
extern "C"  X509CertificateCollection_t1542168550 * X509Store_BuildCertificatesCollection_m3030935583 (X509Store_t2777415284 * __this, String_t* ___storeName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_BuildCertificatesCollection_m3030935583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509CertificateCollection_t1542168550 * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1281789340* V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t1281789340* V_4 = NULL;
	int32_t V_5 = 0;
	X509Certificate_t489243025 * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = __this->get__storePath_0();
		String_t* L_2 = ___storeName0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		bool L_5 = X509Store_CheckStore_m2045435685(__this, L_4, (bool)0, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		X509CertificateCollection_t1542168550 * L_6 = V_0;
		return L_6;
	}

IL_0022:
	{
		String_t* L_7 = V_1;
		StringU5BU5D_t1281789340* L_8 = Directory_GetFiles_m2624572368(NULL /*static, unused*/, L_7, _stringLiteral2225310117, /*hidden argument*/NULL);
		V_2 = L_8;
		StringU5BU5D_t1281789340* L_9 = V_2;
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1281789340* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1281789340* L_11 = V_2;
		V_4 = L_11;
		V_5 = 0;
		goto IL_0071;
	}

IL_0048:
	{
		StringU5BU5D_t1281789340* L_12 = V_4;
		int32_t L_13 = V_5;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_3 = L_15;
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		String_t* L_16 = V_3;
		X509Certificate_t489243025 * L_17 = X509Store_LoadCertificate_m1587806288(__this, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		X509CertificateCollection_t1542168550 * L_18 = V_0;
		X509Certificate_t489243025 * L_19 = V_6;
		NullCheck(L_18);
		X509CertificateCollection_Add_m2277657976(L_18, L_19, /*hidden argument*/NULL);
		goto IL_006b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0065;
		throw e;
	}

CATCH_0065:
	{ // begin catch(System.Object)
		goto IL_006b;
	} // end catch (depth: 1)

IL_006b:
	{
		int32_t L_20 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0071:
	{
		int32_t L_21 = V_5;
		StringU5BU5D_t1281789340* L_22 = V_4;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_0048;
		}
	}

IL_007c:
	{
		X509CertificateCollection_t1542168550 * L_23 = V_0;
		return L_23;
	}
}
// System.Collections.ArrayList Mono.Security.X509.X509Store::BuildCrlsCollection(System.String)
extern "C"  ArrayList_t2718874744 * X509Store_BuildCrlsCollection_m1991312527 (X509Store_t2777415284 * __this, String_t* ___storeName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Store_BuildCrlsCollection_m1991312527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t2718874744 * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1281789340* V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t1281789340* V_4 = NULL;
	int32_t V_5 = 0;
	X509Crl_t1148767388 * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = __this->get__storePath_0();
		String_t* L_2 = ___storeName0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		bool L_5 = X509Store_CheckStore_m2045435685(__this, L_4, (bool)0, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		ArrayList_t2718874744 * L_6 = V_0;
		return L_6;
	}

IL_0022:
	{
		String_t* L_7 = V_1;
		StringU5BU5D_t1281789340* L_8 = Directory_GetFiles_m2624572368(NULL /*static, unused*/, L_7, _stringLiteral3710028195, /*hidden argument*/NULL);
		V_2 = L_8;
		StringU5BU5D_t1281789340* L_9 = V_2;
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1281789340* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1281789340* L_11 = V_2;
		V_4 = L_11;
		V_5 = 0;
		goto IL_0071;
	}

IL_0048:
	{
		StringU5BU5D_t1281789340* L_12 = V_4;
		int32_t L_13 = V_5;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_3 = L_15;
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		String_t* L_16 = V_3;
		X509Crl_t1148767388 * L_17 = X509Store_LoadCrl_m1881903843(__this, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		ArrayList_t2718874744 * L_18 = V_0;
		X509Crl_t1148767388 * L_19 = V_6;
		NullCheck(L_18);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_18, L_19);
		goto IL_006b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0065;
		throw e;
	}

CATCH_0065:
	{ // begin catch(System.Object)
		goto IL_006b;
	} // end catch (depth: 1)

IL_006b:
	{
		int32_t L_20 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0071:
	{
		int32_t L_21 = V_5;
		StringU5BU5D_t1281789340* L_22 = V_4;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_0048;
		}
	}

IL_007c:
	{
		ArrayList_t2718874744 * L_23 = V_0;
		return L_23;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509StoreManager::.ctor()
extern "C"  void X509StoreManager__ctor_m58959084 (X509StoreManager_t1046782376 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
extern "C"  X509Stores_t1373936238 * X509StoreManager_get_CurrentUser_m719101392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_CurrentUser_m719101392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Stores_t1373936238 * L_0 = ((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->get__userStore_0();
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_1 = Environment_GetFolderPath_m327623990(NULL /*static, unused*/, ((int32_t)26), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral3525949144, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = Path_Combine_m3389272516(NULL /*static, unused*/, L_3, _stringLiteral9622098, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		X509Stores_t1373936238 * L_6 = (X509Stores_t1373936238 *)il2cpp_codegen_object_new(X509Stores_t1373936238_il2cpp_TypeInfo_var);
		X509Stores__ctor_m1786355972(L_6, L_5, /*hidden argument*/NULL);
		((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->set__userStore_0(L_6);
	}

IL_0033:
	{
		X509Stores_t1373936238 * L_7 = ((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->get__userStore_0();
		return L_7;
	}
}
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
extern "C"  X509Stores_t1373936238 * X509StoreManager_get_LocalMachine_m269504582 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_LocalMachine_m269504582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Stores_t1373936238 * L_0 = ((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->get__machineStore_1();
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_1 = Environment_GetFolderPath_m327623990(NULL /*static, unused*/, ((int32_t)35), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral3525949144, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = Path_Combine_m3389272516(NULL /*static, unused*/, L_3, _stringLiteral9622098, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		X509Stores_t1373936238 * L_6 = (X509Stores_t1373936238 *)il2cpp_codegen_object_new(X509Stores_t1373936238_il2cpp_TypeInfo_var);
		X509Stores__ctor_m1786355972(L_6, L_5, /*hidden argument*/NULL);
		((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->set__machineStore_1(L_6);
	}

IL_0033:
	{
		X509Stores_t1373936238 * L_7 = ((X509StoreManager_t1046782376_StaticFields*)il2cpp_codegen_static_fields_for(X509StoreManager_t1046782376_il2cpp_TypeInfo_var))->get__machineStore_1();
		return L_7;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_IntermediateCACertificates()
extern "C"  X509CertificateCollection_t1542168550 * X509StoreManager_get_IntermediateCACertificates_m1576610804 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_IntermediateCACertificates_m1576610804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509CertificateCollection_t1542168550 * V_0 = NULL;
	{
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		X509CertificateCollection_t1542168550 * L_1 = V_0;
		X509Stores_t1373936238 * L_2 = X509StoreManager_get_CurrentUser_m719101392(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509Store_t2777415284 * L_3 = X509Stores_get_IntermediateCA_m1350619599(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		X509CertificateCollection_t1542168550 * L_4 = X509Store_get_Certificates_m1092347772(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		X509CertificateCollection_AddRange_m2165814476(L_1, L_4, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_5 = V_0;
		X509Stores_t1373936238 * L_6 = X509StoreManager_get_LocalMachine_m269504582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509Store_t2777415284 * L_7 = X509Stores_get_IntermediateCA_m1350619599(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		X509CertificateCollection_t1542168550 * L_8 = X509Store_get_Certificates_m1092347772(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		X509CertificateCollection_AddRange_m2165814476(L_5, L_8, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_IntermediateCACrls()
extern "C"  ArrayList_t2718874744 * X509StoreManager_get_IntermediateCACrls_m3919824182 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_IntermediateCACrls_m3919824182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t2718874744 * V_0 = NULL;
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t2718874744 * L_1 = V_0;
		X509Stores_t1373936238 * L_2 = X509StoreManager_get_CurrentUser_m719101392(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509Store_t2777415284 * L_3 = X509Stores_get_IntermediateCA_m1350619599(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayList_t2718874744 * L_4 = X509Store_get_Crls_m1211262034(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< RuntimeObject* >::Invoke(44 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_1, L_4);
		ArrayList_t2718874744 * L_5 = V_0;
		X509Stores_t1373936238 * L_6 = X509StoreManager_get_LocalMachine_m269504582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509Store_t2777415284 * L_7 = X509Stores_get_IntermediateCA_m1350619599(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayList_t2718874744 * L_8 = X509Store_get_Crls_m1211262034(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< RuntimeObject* >::Invoke(44 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_5, L_8);
		ArrayList_t2718874744 * L_9 = V_0;
		return L_9;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
extern "C"  X509CertificateCollection_t1542168550 * X509StoreManager_get_TrustedRootCertificates_m2180997293 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_TrustedRootCertificates_m2180997293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509CertificateCollection_t1542168550 * V_0 = NULL;
	{
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		X509CertificateCollection_t1542168550 * L_1 = V_0;
		X509Stores_t1373936238 * L_2 = X509StoreManager_get_CurrentUser_m719101392(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509Store_t2777415284 * L_3 = X509Stores_get_TrustedRoot_m1736182879(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		X509CertificateCollection_t1542168550 * L_4 = X509Store_get_Certificates_m1092347772(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		X509CertificateCollection_AddRange_m2165814476(L_1, L_4, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_5 = V_0;
		X509Stores_t1373936238 * L_6 = X509StoreManager_get_LocalMachine_m269504582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509Store_t2777415284 * L_7 = X509Stores_get_TrustedRoot_m1736182879(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		X509CertificateCollection_t1542168550 * L_8 = X509Store_get_Certificates_m1092347772(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		X509CertificateCollection_AddRange_m2165814476(L_5, L_8, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_TrustedRootCACrls()
extern "C"  ArrayList_t2718874744 * X509StoreManager_get_TrustedRootCACrls_m2267763919 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_TrustedRootCACrls_m2267763919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t2718874744 * V_0 = NULL;
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t2718874744 * L_1 = V_0;
		X509Stores_t1373936238 * L_2 = X509StoreManager_get_CurrentUser_m719101392(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509Store_t2777415284 * L_3 = X509Stores_get_TrustedRoot_m1736182879(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayList_t2718874744 * L_4 = X509Store_get_Crls_m1211262034(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< RuntimeObject* >::Invoke(44 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_1, L_4);
		ArrayList_t2718874744 * L_5 = V_0;
		X509Stores_t1373936238 * L_6 = X509StoreManager_get_LocalMachine_m269504582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509Store_t2777415284 * L_7 = X509Stores_get_TrustedRoot_m1736182879(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayList_t2718874744 * L_8 = X509Store_get_Crls_m1211262034(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< RuntimeObject* >::Invoke(44 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_5, L_8);
		ArrayList_t2718874744 * L_9 = V_0;
		return L_9;
	}
}
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_UntrustedCertificates()
extern "C"  X509CertificateCollection_t1542168550 * X509StoreManager_get_UntrustedCertificates_m829127408 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509StoreManager_get_UntrustedCertificates_m829127408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509CertificateCollection_t1542168550 * V_0 = NULL;
	{
		X509CertificateCollection_t1542168550 * L_0 = (X509CertificateCollection_t1542168550 *)il2cpp_codegen_object_new(X509CertificateCollection_t1542168550_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m2066277891(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		X509CertificateCollection_t1542168550 * L_1 = V_0;
		X509Stores_t1373936238 * L_2 = X509StoreManager_get_CurrentUser_m719101392(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		X509Store_t2777415284 * L_3 = X509Stores_get_Untrusted_m1029989579(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		X509CertificateCollection_t1542168550 * L_4 = X509Store_get_Certificates_m1092347772(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		X509CertificateCollection_AddRange_m2165814476(L_1, L_4, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_5 = V_0;
		X509Stores_t1373936238 * L_6 = X509StoreManager_get_LocalMachine_m269504582(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509Store_t2777415284 * L_7 = X509Stores_get_Untrusted_m1029989579(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		X509CertificateCollection_t1542168550 * L_8 = X509Store_get_Certificates_m1092347772(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		X509CertificateCollection_AddRange_m2165814476(L_5, L_8, /*hidden argument*/NULL);
		X509CertificateCollection_t1542168550 * L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Stores::.ctor(System.String)
extern "C"  void X509Stores__ctor_m1786355972 (X509Stores_t1373936238 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___path0;
		__this->set__storePath_0(L_0);
		return;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_Personal()
extern "C"  X509Store_t2777415284 * X509Stores_get_Personal_m1313610043 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_get_Personal_m1313610043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Store_t2777415284 * L_0 = __this->get__personal_1();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral3456219107, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		X509Store_t2777415284 * L_4 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_4, L_3, (bool)0, /*hidden argument*/NULL);
		__this->set__personal_1(L_4);
	}

IL_0029:
	{
		X509Store_t2777415284 * L_5 = __this->get__personal_1();
		return L_5;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_OtherPeople()
extern "C"  X509Store_t2777415284 * X509Stores_get_OtherPeople_m2341174106 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_get_OtherPeople_m2341174106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Store_t2777415284 * L_0 = __this->get__other_2();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral4072668552, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		X509Store_t2777415284 * L_4 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_4, L_3, (bool)0, /*hidden argument*/NULL);
		__this->set__other_2(L_4);
	}

IL_0029:
	{
		X509Store_t2777415284 * L_5 = __this->get__other_2();
		return L_5;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_IntermediateCA()
extern "C"  X509Store_t2777415284 * X509Stores_get_IntermediateCA_m1350619599 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_get_IntermediateCA_m1350619599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Store_t2777415284 * L_0 = __this->get__intermediate_3();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral3456743389, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		X509Store_t2777415284 * L_4 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_4, L_3, (bool)1, /*hidden argument*/NULL);
		__this->set__intermediate_3(L_4);
	}

IL_0029:
	{
		X509Store_t2777415284 * L_5 = __this->get__intermediate_3();
		return L_5;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_TrustedRoot()
extern "C"  X509Store_t2777415284 * X509Stores_get_TrustedRoot_m1736182879 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_get_TrustedRoot_m1736182879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Store_t2777415284 * L_0 = __this->get__trusted_4();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral1986082327, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		X509Store_t2777415284 * L_4 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_4, L_3, (bool)1, /*hidden argument*/NULL);
		__this->set__trusted_4(L_4);
	}

IL_0029:
	{
		X509Store_t2777415284 * L_5 = __this->get__trusted_4();
		return L_5;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_Untrusted()
extern "C"  X509Store_t2777415284 * X509Stores_get_Untrusted_m1029989579 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_get_Untrusted_m1029989579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		X509Store_t2777415284 * L_0 = __this->get__untrusted_5();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_1 = __this->get__storePath_0();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m3389272516(NULL /*static, unused*/, L_1, _stringLiteral1940787968, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		X509Store_t2777415284 * L_4 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_4, L_3, (bool)0, /*hidden argument*/NULL);
		__this->set__untrusted_5(L_4);
	}

IL_0029:
	{
		X509Store_t2777415284 * L_5 = __this->get__untrusted_5();
		return L_5;
	}
}
// System.Void Mono.Security.X509.X509Stores::Clear()
extern "C"  void X509Stores_Clear_m2582079463 (X509Stores_t1373936238 * __this, const RuntimeMethod* method)
{
	{
		X509Store_t2777415284 * L_0 = __this->get__personal_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		X509Store_t2777415284 * L_1 = __this->get__personal_1();
		NullCheck(L_1);
		X509Store_Clear_m2126432876(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set__personal_1((X509Store_t2777415284 *)NULL);
		X509Store_t2777415284 * L_2 = __this->get__other_2();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		X509Store_t2777415284 * L_3 = __this->get__other_2();
		NullCheck(L_3);
		X509Store_Clear_m2126432876(L_3, /*hidden argument*/NULL);
	}

IL_0033:
	{
		__this->set__other_2((X509Store_t2777415284 *)NULL);
		X509Store_t2777415284 * L_4 = __this->get__intermediate_3();
		if (!L_4)
		{
			goto IL_0050;
		}
	}
	{
		X509Store_t2777415284 * L_5 = __this->get__intermediate_3();
		NullCheck(L_5);
		X509Store_Clear_m2126432876(L_5, /*hidden argument*/NULL);
	}

IL_0050:
	{
		__this->set__intermediate_3((X509Store_t2777415284 *)NULL);
		X509Store_t2777415284 * L_6 = __this->get__trusted_4();
		if (!L_6)
		{
			goto IL_006d;
		}
	}
	{
		X509Store_t2777415284 * L_7 = __this->get__trusted_4();
		NullCheck(L_7);
		X509Store_Clear_m2126432876(L_7, /*hidden argument*/NULL);
	}

IL_006d:
	{
		__this->set__trusted_4((X509Store_t2777415284 *)NULL);
		X509Store_t2777415284 * L_8 = __this->get__untrusted_5();
		if (!L_8)
		{
			goto IL_008a;
		}
	}
	{
		X509Store_t2777415284 * L_9 = __this->get__untrusted_5();
		NullCheck(L_9);
		X509Store_Clear_m2126432876(L_9, /*hidden argument*/NULL);
	}

IL_008a:
	{
		__this->set__untrusted_5((X509Store_t2777415284 *)NULL);
		return;
	}
}
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::Open(System.String,System.Boolean)
extern "C"  X509Store_t2777415284 * X509Stores_Open_m1037335183 (X509Stores_t1373936238 * __this, String_t* ___storeName0, bool ___create1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509Stores_Open_m1037335183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___storeName0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral2153662835, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = __this->get__storePath_0();
		String_t* L_3 = ___storeName0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_4 = Path_Combine_m3389272516(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = ___create1;
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_6 = V_0;
		bool L_7 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0031;
		}
	}
	{
		return (X509Store_t2777415284 *)NULL;
	}

IL_0031:
	{
		String_t* L_8 = V_0;
		X509Store_t2777415284 * L_9 = (X509Store_t2777415284 *)il2cpp_codegen_object_new(X509Store_t2777415284_il2cpp_TypeInfo_var);
		X509Store__ctor_m2736551756(L_9, L_8, (bool)1, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X509Stores/Names::.ctor()
extern "C"  void Names__ctor_m3130886097 (Names_t1325641082 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520::.ctor()
extern "C"  void X520__ctor_m1236427049 (X520_t3325039438 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32)
extern "C"  void AttributeTypeAndValue__ctor_m682681796 (AttributeTypeAndValue_t3245693428 * __this, String_t* ___oid0, int32_t ___upperBound1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oid0;
		__this->set_oid_0(L_0);
		int32_t L_1 = ___upperBound1;
		__this->set_upperBound_2(L_1);
		__this->set_encoding_3((uint8_t)((int32_t)255));
		return;
	}
}
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32,System.Byte)
extern "C"  void AttributeTypeAndValue__ctor_m3579516111 (AttributeTypeAndValue_t3245693428 * __this, String_t* ___oid0, int32_t ___upperBound1, uint8_t ___encoding2, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oid0;
		__this->set_oid_0(L_0);
		int32_t L_1 = ___upperBound1;
		__this->set_upperBound_2(L_1);
		uint8_t L_2 = ___encoding2;
		__this->set_encoding_3(L_2);
		return;
	}
}
// System.String Mono.Security.X509.X520/AttributeTypeAndValue::get_Value()
extern "C"  String_t* AttributeTypeAndValue_get_Value_m2499865396 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_attrValue_1();
		return L_0;
	}
}
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::set_Value(System.String)
extern "C"  void AttributeTypeAndValue_set_Value_m3270529695 (AttributeTypeAndValue_t3245693428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeTypeAndValue_set_Value_m3270529695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_attrValue_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		String_t* L_1 = __this->get_attrValue_1();
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_upperBound_2();
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_0043;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m3520169047(NULL /*static, unused*/, _stringLiteral2649751261, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		int32_t L_6 = __this->get_upperBound_2();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2844511972(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		FormatException_t154580423 * L_10 = (FormatException_t154580423 *)il2cpp_codegen_object_new(FormatException_t154580423_il2cpp_TypeInfo_var);
		FormatException__ctor_m4049685996(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0043:
	{
		String_t* L_11 = ___value0;
		__this->set_attrValue_1(L_11);
		return;
	}
}
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::get_ASN1()
extern "C"  ASN1_t2114160833 * AttributeTypeAndValue_get_ASN1_m896112831 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method)
{
	{
		ASN1_t2114160833 * L_0 = AttributeTypeAndValue_GetASN1_m1976348179(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1(System.Byte)
extern "C"  ASN1_t2114160833 * AttributeTypeAndValue_GetASN1_m735511441 (AttributeTypeAndValue_t3245693428 * __this, uint8_t ___encoding0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeTypeAndValue_GetASN1_m735511441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	ASN1_t2114160833 * V_1 = NULL;
	uint8_t V_2 = 0x0;
	{
		uint8_t L_0 = ___encoding0;
		V_0 = L_0;
		uint8_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)255)))))
		{
			goto IL_0014;
		}
	}
	{
		uint8_t L_2 = AttributeTypeAndValue_SelectBestEncoding_m4133162804(__this, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0014:
	{
		ASN1_t2114160833 * L_3 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m1239252869(L_3, (uint8_t)((int32_t)48), /*hidden argument*/NULL);
		V_1 = L_3;
		ASN1_t2114160833 * L_4 = V_1;
		String_t* L_5 = __this->get_oid_0();
		ASN1_t2114160833 * L_6 = ASN1Convert_FromOid_m1517037532(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ASN1_Add_m2431139999(L_4, L_6, /*hidden argument*/NULL);
		uint8_t L_7 = V_0;
		V_2 = L_7;
		uint8_t L_8 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)((int32_t)19))))
		{
			case 0:
			{
				goto IL_0056;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_0049;
			}
			case 3:
			{
				goto IL_0079;
			}
		}
	}

IL_0049:
	{
		uint8_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)30))))
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00bf;
	}

IL_0056:
	{
		ASN1_t2114160833 * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_11 = Encoding_get_ASCII_m3595602635(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_12 = __this->get_attrValue_1();
		NullCheck(L_11);
		ByteU5BU5D_t4116647657* L_13 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, L_12);
		ASN1_t2114160833 * L_14 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m682794872(L_14, (uint8_t)((int32_t)19), L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		ASN1_Add_m2431139999(L_10, L_14, /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_0079:
	{
		ASN1_t2114160833 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_16 = Encoding_get_ASCII_m3595602635(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = __this->get_attrValue_1();
		NullCheck(L_16);
		ByteU5BU5D_t4116647657* L_18 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_16, L_17);
		ASN1_t2114160833 * L_19 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m682794872(L_19, (uint8_t)((int32_t)22), L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		ASN1_Add_m2431139999(L_15, L_19, /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_009c:
	{
		ASN1_t2114160833 * L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_21 = Encoding_get_BigEndianUnicode_m684646764(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_22 = __this->get_attrValue_1();
		NullCheck(L_21);
		ByteU5BU5D_t4116647657* L_23 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_21, L_22);
		ASN1_t2114160833 * L_24 = (ASN1_t2114160833 *)il2cpp_codegen_object_new(ASN1_t2114160833_il2cpp_TypeInfo_var);
		ASN1__ctor_m682794872(L_24, (uint8_t)((int32_t)30), L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		ASN1_Add_m2431139999(L_20, L_24, /*hidden argument*/NULL);
		goto IL_00bf;
	}

IL_00bf:
	{
		ASN1_t2114160833 * L_25 = V_1;
		return L_25;
	}
}
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1()
extern "C"  ASN1_t2114160833 * AttributeTypeAndValue_GetASN1_m1976348179 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = __this->get_encoding_3();
		ASN1_t2114160833 * L_1 = AttributeTypeAndValue_GetASN1_m735511441(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes(System.Byte)
extern "C"  ByteU5BU5D_t4116647657* AttributeTypeAndValue_GetBytes_m3105335722 (AttributeTypeAndValue_t3245693428 * __this, uint8_t ___encoding0, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___encoding0;
		ASN1_t2114160833 * L_1 = AttributeTypeAndValue_GetASN1_m735511441(__this, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t4116647657* L_2 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_1);
		return L_2;
	}
}
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes()
extern "C"  ByteU5BU5D_t4116647657* AttributeTypeAndValue_GetBytes_m2023713905 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method)
{
	{
		ASN1_t2114160833 * L_0 = AttributeTypeAndValue_GetASN1_m1976348179(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t4116647657* L_1 = VirtFuncInvoker0< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Byte[] Mono.Security.ASN1::GetBytes() */, L_0);
		return L_1;
	}
}
// System.Byte Mono.Security.X509.X520/AttributeTypeAndValue::SelectBestEncoding()
extern "C"  uint8_t AttributeTypeAndValue_SelectBestEncoding_m4133162804 (AttributeTypeAndValue_t3245693428 * __this, const RuntimeMethod* method)
{
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	{
		String_t* L_0 = __this->get_attrValue_1();
		V_1 = L_0;
		V_2 = 0;
		goto IL_0044;
	}

IL_000e:
	{
		String_t* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m2986988803(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppChar L_4 = V_0;
		V_3 = L_4;
		Il2CppChar L_5 = V_3;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)64))))
		{
			goto IL_002d;
		}
	}
	{
		Il2CppChar L_6 = V_3;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)95))))
		{
			goto IL_002d;
		}
	}
	{
		goto IL_0030;
	}

IL_002d:
	{
		return (uint8_t)((int32_t)30);
	}

IL_0030:
	{
		Il2CppChar L_7 = V_0;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)127))))
		{
			goto IL_003b;
		}
	}
	{
		return (uint8_t)((int32_t)30);
	}

IL_003b:
	{
		goto IL_0040;
	}

IL_0040:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_9 = V_2;
		String_t* L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m3847582255(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_000e;
		}
	}
	{
		return (uint8_t)((int32_t)19);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/CommonName::.ctor()
extern "C"  void CommonName__ctor_m4066435887 (CommonName_t2882805359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommonName__ctor_m4066435887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral1452586790, ((int32_t)64), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/CountryName::.ctor()
extern "C"  void CountryName__ctor_m1759248525 (CountryName_t4165042092 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CountryName__ctor_m1759248525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m3579516111(__this, _stringLiteral1855871317, 2, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/DnQualifier::.ctor()
extern "C"  void DnQualifier__ctor_m2304537709 (DnQualifier_t2746394302 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DnQualifier__ctor_m2304537709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m3579516111(__this, _stringLiteral456868491, 2, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/DomainComponent::.ctor()
extern "C"  void DomainComponent__ctor_m2984562191 (DomainComponent_t1597975113 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DomainComponent__ctor_m2984562191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m3579516111(__this, _stringLiteral1926373733, ((int32_t)2147483647LL), (uint8_t)((int32_t)22), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/EmailAddress::.ctor()
extern "C"  void EmailAddress__ctor_m1682437348 (EmailAddress_t484866411 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmailAddress__ctor_m1682437348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m3579516111(__this, _stringLiteral3724057548, ((int32_t)128), (uint8_t)((int32_t)22), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/GivenName::.ctor()
extern "C"  void GivenName__ctor_m2285954234 (GivenName_t3114888956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GivenName__ctor_m2285954234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral3987161739, ((int32_t)16), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/Initial::.ctor()
extern "C"  void Initial__ctor_m1539622106 (Initial_t2745963796 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Initial__ctor_m1539622106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral2030846603, 5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/LocalityName::.ctor()
extern "C"  void LocalityName__ctor_m816181155 (LocalityName_t1511149830 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalityName__ctor_m816181155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral3421955258, ((int32_t)128), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/Name::.ctor()
extern "C"  void Name__ctor_m2961952406 (Name_t3626842933 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Name__ctor_m2961952406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral1648509579, ((int32_t)32768), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/Oid::.ctor(System.String)
extern "C"  void Oid__ctor_m1603120831 (Oid_t2014794921 * __this, String_t* ___oid0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___oid0;
		AttributeTypeAndValue__ctor_m682681796(__this, L_0, ((int32_t)2147483647LL), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/OrganizationalUnitName::.ctor()
extern "C"  void OrganizationalUnitName__ctor_m4279817838 (OrganizationalUnitName_t1968218587 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrganizationalUnitName__ctor_m4279817838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral1245225052, ((int32_t)64), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/OrganizationName::.ctor()
extern "C"  void OrganizationName__ctor_m3237996566 (OrganizationName_t857038441 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrganizationName__ctor_m3237996566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral3201540188, ((int32_t)64), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/SerialNumber::.ctor()
extern "C"  void SerialNumber__ctor_m2630741392 (SerialNumber_t3812427306 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerialNumber__ctor_m2630741392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m3579516111(__this, _stringLiteral289787376, ((int32_t)64), (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/StateOrProvinceName::.ctor()
extern "C"  void StateOrProvinceName__ctor_m3786137020 (StateOrProvinceName_t1916702996 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateOrProvinceName__ctor_m3786137020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral1405532623, ((int32_t)128), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/Surname::.ctor()
extern "C"  void Surname__ctor_m4076845856 (Surname_t4221813936 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Surname__ctor_m4076845856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral3018670731, ((int32_t)32768), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/Title::.ctor()
extern "C"  void Title__ctor_m3468619135 (Title_t2026585634 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Title__ctor_m3468619135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral3583877212, ((int32_t)64), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.X509.X520/UserId::.ctor()
extern "C"  void UserId__ctor_m730120360 (UserId_t70549454 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserId__ctor_m730120360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeTypeAndValue__ctor_m682681796(__this, _stringLiteral1929716066, ((int32_t)256), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
