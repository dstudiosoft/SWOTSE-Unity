﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.Socket/SendFileAsyncResult
struct SendFileAsyncResult_t676923523;
// System.Net.Sockets.Socket/SendFileHandler
struct SendFileHandler_t2613075220;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t677569169;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket_SendFileHandler2613075220.h"

// System.Void System.Net.Sockets.Socket/SendFileAsyncResult::.ctor(System.Net.Sockets.Socket/SendFileHandler,System.IAsyncResult)
extern "C"  void SendFileAsyncResult__ctor_m3947553928 (SendFileAsyncResult_t676923523 * __this, SendFileHandler_t2613075220 * ___d0, Il2CppObject * ___ares1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Net.Sockets.Socket/SendFileAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * SendFileAsyncResult_get_AsyncState_m149445674 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle System.Net.Sockets.Socket/SendFileAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t677569169 * SendFileAsyncResult_get_AsyncWaitHandle_m202432710 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.Socket/SendFileAsyncResult::get_CompletedSynchronously()
extern "C"  bool SendFileAsyncResult_get_CompletedSynchronously_m681568595 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.Socket/SendFileAsyncResult::get_IsCompleted()
extern "C"  bool SendFileAsyncResult_get_IsCompleted_m1136320077 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket/SendFileHandler System.Net.Sockets.Socket/SendFileAsyncResult::get_Delegate()
extern "C"  SendFileHandler_t2613075220 * SendFileAsyncResult_get_Delegate_m4131059738 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.Socket/SendFileAsyncResult::get_Original()
extern "C"  Il2CppObject * SendFileAsyncResult_get_Original_m4156460821 (SendFileAsyncResult_t676923523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
