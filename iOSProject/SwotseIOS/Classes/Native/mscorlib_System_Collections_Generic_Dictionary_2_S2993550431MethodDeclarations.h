﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct ShimEnumerator_t2993550431;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3991899068_gshared (ShimEnumerator_t2993550431 * __this, Dictionary_2_t2888425610 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3991899068(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2993550431 *, Dictionary_2_t2888425610 *, const MethodInfo*))ShimEnumerator__ctor_m3991899068_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2182717501_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2182717501(__this, method) ((  bool (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_MoveNext_m2182717501_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2577526889_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2577526889(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_get_Entry_m2577526889_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m928024900_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m928024900(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_get_Key_m928024900_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4198407854_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m4198407854(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_get_Value_m4198407854_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m400044110_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m400044110(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_get_Current_m400044110_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Reset()
extern "C"  void ShimEnumerator_Reset_m2614328594_gshared (ShimEnumerator_t2993550431 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2614328594(__this, method) ((  void (*) (ShimEnumerator_t2993550431 *, const MethodInfo*))ShimEnumerator_Reset_m2614328594_gshared)(__this, method)
