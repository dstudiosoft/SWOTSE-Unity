﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ListenerAsyncResult
struct ListenerAsyncResult_t2594852849;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Net.HttpListenerContext
struct HttpListenerContext_t506453093;
// System.Threading.WaitHandle
struct WaitHandle_t677569169;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_HttpListenerContext506453093.h"

// System.Void System.Net.ListenerAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern "C"  void ListenerAsyncResult__ctor_m236362123 (ListenerAsyncResult_t2594852849 * __this, AsyncCallback_t163412349 * ___cb0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerAsyncResult::Complete(System.String)
extern "C"  void ListenerAsyncResult_Complete_m2269282050 (ListenerAsyncResult_t2594852849 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerAsyncResult::InvokeCallback(System.Object)
extern "C"  void ListenerAsyncResult_InvokeCallback_m2331345216 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerAsyncResult::Complete(System.Net.HttpListenerContext)
extern "C"  void ListenerAsyncResult_Complete_m1558671651 (ListenerAsyncResult_t2594852849 * __this, HttpListenerContext_t506453093 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerAsyncResult::Complete(System.Net.HttpListenerContext,System.Boolean)
extern "C"  void ListenerAsyncResult_Complete_m696761282 (ListenerAsyncResult_t2594852849 * __this, HttpListenerContext_t506453093 * ___context0, bool ___synch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.ListenerAsyncResult::GetContext()
extern "C"  HttpListenerContext_t506453093 * ListenerAsyncResult_GetContext_m2662944882 (ListenerAsyncResult_t2594852849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Net.ListenerAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * ListenerAsyncResult_get_AsyncState_m3693226072 (ListenerAsyncResult_t2594852849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle System.Net.ListenerAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t677569169 * ListenerAsyncResult_get_AsyncWaitHandle_m2800471988 (ListenerAsyncResult_t2594852849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ListenerAsyncResult::get_CompletedSynchronously()
extern "C"  bool ListenerAsyncResult_get_CompletedSynchronously_m2870527545 (ListenerAsyncResult_t2594852849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ListenerAsyncResult::get_IsCompleted()
extern "C"  bool ListenerAsyncResult_get_IsCompleted_m2535749427 (ListenerAsyncResult_t2594852849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
