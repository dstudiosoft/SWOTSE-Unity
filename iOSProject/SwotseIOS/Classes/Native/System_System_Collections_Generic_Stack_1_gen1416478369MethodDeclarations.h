﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::.ctor()
#define Stack_1__ctor_m645212683(__this, method) ((  void (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::.ctor(System.Int32)
#define Stack_1__ctor_m1838140664(__this, ___count0, method) ((  void (*) (Stack_1_t1416478369 *, int32_t, const MethodInfo*))Stack_1__ctor_m4173684663_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m289607383(__this, method) ((  bool (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2076161108_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m4136894371(__this, method) ((  Il2CppObject * (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3151629354_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m2703440767(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t1416478369 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m2104527616_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3734072459(__this, method) ((  Il2CppObject* (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m680979874_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m759197758(__this, method) ((  Il2CppObject * (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3875192475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::Contains(T)
#define Stack_1_Contains_m2339496338(__this, ___t0, method) ((  bool (*) (Stack_1_t1416478369 *, List_1_t328750215 *, const MethodInfo*))Stack_1_Contains_m973625077_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::Peek()
#define Stack_1_Peek_m3853544283(__this, method) ((  List_1_t328750215 * (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_Peek_m1548778538_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::Pop()
#define Stack_1_Pop_m2803231087(__this, method) ((  List_1_t328750215 * (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_Pop_m535185982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::Push(T)
#define Stack_1_Push_m628128505(__this, ___t0, method) ((  void (*) (Stack_1_t1416478369 *, List_1_t328750215 *, const MethodInfo*))Stack_1_Push_m2122392216_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::get_Count()
#define Stack_1_get_Count_m3970463055(__this, method) ((  int32_t (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>::GetEnumerator()
#define Stack_1_GetEnumerator_m1541295063(__this, method) ((  Enumerator_t2066476729  (*) (Stack_1_t1416478369 *, const MethodInfo*))Stack_1_GetEnumerator_m287848754_gshared)(__this, method)
