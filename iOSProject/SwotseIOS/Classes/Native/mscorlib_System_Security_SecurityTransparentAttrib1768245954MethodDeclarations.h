﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityTransparentAttribute
struct SecurityTransparentAttribute_t1768245954;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecurityTransparentAttribute::.ctor()
extern "C"  void SecurityTransparentAttribute__ctor_m3258341955 (SecurityTransparentAttribute_t1768245954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
