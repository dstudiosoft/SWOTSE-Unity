﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m477426041_gshared (KeyValuePair_2_t888819835 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m477426041(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t888819835 *, IntPtr_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m477426041_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  IntPtr_t KeyValuePair_2_get_Key_m1574332879_gshared (KeyValuePair_2_t888819835 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1574332879(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t888819835 *, const MethodInfo*))KeyValuePair_2_get_Key_m1574332879_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4146602710_gshared (KeyValuePair_2_t888819835 * __this, IntPtr_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4146602710(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t888819835 *, IntPtr_t, const MethodInfo*))KeyValuePair_2_set_Key_m4146602710_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m544293807_gshared (KeyValuePair_2_t888819835 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m544293807(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t888819835 *, const MethodInfo*))KeyValuePair_2_get_Value_m544293807_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1938889438_gshared (KeyValuePair_2_t888819835 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1938889438(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t888819835 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1938889438_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2882821022_gshared (KeyValuePair_2_t888819835 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2882821022(__this, method) ((  String_t* (*) (KeyValuePair_2_t888819835 *, const MethodInfo*))KeyValuePair_2_ToString_m2882821022_gshared)(__this, method)
