﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t661276395;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.DownloadStringCompletedEventArgs::.ctor(System.String,System.Exception,System.Boolean,System.Object)
extern "C"  void DownloadStringCompletedEventArgs__ctor_m3564768942 (DownloadStringCompletedEventArgs_t661276395 * __this, String_t* ___result0, Exception_t1927440687 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.DownloadStringCompletedEventArgs::get_Result()
extern "C"  String_t* DownloadStringCompletedEventArgs_get_Result_m825827952 (DownloadStringCompletedEventArgs_t661276395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
