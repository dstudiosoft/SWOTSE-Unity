﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent
struct CellHeightChangedEvent_t3730749755;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent::.ctor()
extern "C"  void CellHeightChangedEvent__ctor_m1611070796 (CellHeightChangedEvent_t3730749755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
