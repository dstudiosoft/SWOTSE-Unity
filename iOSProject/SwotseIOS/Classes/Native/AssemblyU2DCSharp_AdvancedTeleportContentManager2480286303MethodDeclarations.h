﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdvancedTeleportContentManager
struct AdvancedTeleportContentManager_t2480286303;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AdvancedTeleportContentManager::.ctor()
extern "C"  void AdvancedTeleportContentManager__ctor_m1600327912 (AdvancedTeleportContentManager_t2480286303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedTeleportContentManager::Close()
extern "C"  void AdvancedTeleportContentManager_Close_m2697392392 (AdvancedTeleportContentManager_t2480286303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedTeleportContentManager::Teleport()
extern "C"  void AdvancedTeleportContentManager_Teleport_m1422497323 (AdvancedTeleportContentManager_t2480286303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedTeleportContentManager::CityTeleported(System.Object,System.String)
extern "C"  void AdvancedTeleportContentManager_CityTeleported_m1058839969 (AdvancedTeleportContentManager_t2480286303 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
