﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.NetworkCredential
struct  NetworkCredential_t3911206805  : public Il2CppObject
{
public:
	// System.String WebSocketSharp.Net.NetworkCredential::_domain
	String_t* ____domain_0;
	// System.String WebSocketSharp.Net.NetworkCredential::_password
	String_t* ____password_1;
	// System.String WebSocketSharp.Net.NetworkCredential::_username
	String_t* ____username_2;

public:
	inline static int32_t get_offset_of__domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____domain_0)); }
	inline String_t* get__domain_0() const { return ____domain_0; }
	inline String_t** get_address_of__domain_0() { return &____domain_0; }
	inline void set__domain_0(String_t* value)
	{
		____domain_0 = value;
		Il2CppCodeGenWriteBarrier(&____domain_0, value);
	}

	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____password_1)); }
	inline String_t* get__password_1() const { return ____password_1; }
	inline String_t** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(String_t* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier(&____password_1, value);
	}

	inline static int32_t get_offset_of__username_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____username_2)); }
	inline String_t* get__username_2() const { return ____username_2; }
	inline String_t** get_address_of__username_2() { return &____username_2; }
	inline void set__username_2(String_t* value)
	{
		____username_2 = value;
		Il2CppCodeGenWriteBarrier(&____username_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
