﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageHeader
struct MessageHeader_t3271323822;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageHeader::.ctor()
extern "C"  void MessageHeader__ctor_m136326383 (MessageHeader_t3271323822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
