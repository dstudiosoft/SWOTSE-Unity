﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct TouchProxyBase_t4188753234;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::.ctor()
extern "C"  void TouchProxyBase__ctor_m2639749808 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Behaviors.Visualizer.TouchProxyBase::get_Size()
extern "C"  int32_t TouchProxyBase_get_Size_m1618996132 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::set_Size(System.Int32)
extern "C"  void TouchProxyBase_set_Size_m1328713221 (TouchProxyBase_t4188753234 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::get_ShowTouchId()
extern "C"  bool TouchProxyBase_get_ShowTouchId_m3060356356 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::set_ShowTouchId(System.Boolean)
extern "C"  void TouchProxyBase_set_ShowTouchId_m1219542189 (TouchProxyBase_t4188753234 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::get_ShowTags()
extern "C"  bool TouchProxyBase_get_ShowTags_m891208435 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::set_ShowTags(System.Boolean)
extern "C"  void TouchProxyBase_set_ShowTags_m2492273510 (TouchProxyBase_t4188753234 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::Init(UnityEngine.RectTransform,TouchScript.TouchPoint)
extern "C"  void TouchProxyBase_Init_m4172979160 (TouchProxyBase_t4188753234 * __this, RectTransform_t3349966182 * ___parent0, TouchPoint_t959629083 * ___touch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::UpdateTouch(TouchScript.TouchPoint)
extern "C"  void TouchProxyBase_UpdateTouch_m53694013 (TouchProxyBase_t4188753234 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::Hide()
extern "C"  void TouchProxyBase_Hide_m2075336202 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::Awake()
extern "C"  void TouchProxyBase_Awake_m1017723489 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::hide()
extern "C"  void TouchProxyBase_hide_m3597602730 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::show()
extern "C"  void TouchProxyBase_show_m1671740811 (TouchProxyBase_t4188753234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::updateOnce(TouchScript.TouchPoint)
extern "C"  void TouchProxyBase_updateOnce_m2944197719 (TouchProxyBase_t4188753234 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxyBase::update(TouchScript.TouchPoint)
extern "C"  void TouchProxyBase_update_m1234347020 (TouchProxyBase_t4188753234 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
