﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Net.OscMessageReceivedEventArgs
struct OscMessageReceivedEventArgs_t1263041860;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscMessage2764280154.h"

// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::get_Message()
extern "C"  OscMessage_t2764280154 * OscMessageReceivedEventArgs_get_Message_m3135203290 (OscMessageReceivedEventArgs_t1263041860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::set_Message(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs_set_Message_m841798933 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::.ctor(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs__ctor_m359379643 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
