﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor()
#define List_1__ctor_m4170624131(__this, method) ((  void (*) (List_1_t1583176531 *, const MethodInfo*))List_1__ctor_m3040225017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m134550216(__this, ___collection0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor(System.Int32)
#define List_1__ctor_m2709290466(__this, ___capacity0, method) ((  void (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor(T[],System.Int32)
#define List_1__ctor_m3060889078(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1583176531 *, IDataProcessorU5BU5D_t215962270*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.cctor()
#define List_1__cctor_m3212224554(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3043022699(__this, method) ((  Il2CppObject* (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3849315095(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1583176531 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3830186156(__this, method) ((  Il2CppObject * (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m41127463(__this, ___item0, method) ((  int32_t (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2982533151(__this, ___item0, method) ((  bool (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2698939585(__this, ___item0, method) ((  int32_t (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2197399790(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1583176531 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m372586624(__this, ___item0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1174485984(__this, method) ((  bool (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m349167039(__this, method) ((  bool (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1744087911(__this, method) ((  Il2CppObject * (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m526011546(__this, method) ((  bool (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1926973667(__this, method) ((  bool (*) (List_1_t1583176531 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3879782462(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1284716849(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1583176531 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Add(T)
#define List_1_Add_m2594654071(__this, ___item0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_Add_m1194694229_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m203192043(__this, ___newCount0, method) ((  void (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m941127435(__this, ___collection0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1668313339(__this, ___enumerable0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4180736020(__this, ___collection0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::AsReadOnly()
#define List_1_AsReadOnly_m59746491(__this, method) ((  ReadOnlyCollection_1_t2399841091 * (*) (List_1_t1583176531 *, const MethodInfo*))List_1_AsReadOnly_m1618299177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Clear()
#define List_1_Clear_m3203701604(__this, method) ((  void (*) (List_1_t1583176531 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Contains(T)
#define List_1_Contains_m181590265(__this, ___item0, method) ((  bool (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1186742406(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1583176531 *, IDataProcessorU5BU5D_t215962270*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Find(System.Predicate`1<T>)
#define List_1_Find_m806516882(__this, ___match0, method) ((  Il2CppObject * (*) (List_1_t1583176531 *, Predicate_1_t657025514 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m889538543(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t657025514 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m2526590899(__this, ___match0, method) ((  List_1_t1583176531 * (*) (List_1_t1583176531 *, Predicate_1_t657025514 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m939965141(__this, ___match0, method) ((  List_1_t1583176531 * (*) (List_1_t1583176531 *, Predicate_1_t657025514 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3133181253(__this, ___match0, method) ((  List_1_t1583176531 * (*) (List_1_t1583176531 *, Predicate_1_t657025514 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2476139570(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1583176531 *, int32_t, int32_t, Predicate_1_t657025514 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3200514033(__this, ___action0, method) ((  void (*) (List_1_t1583176531 *, Action_1_t2015854781 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::GetEnumerator()
#define List_1_GetEnumerator_m1861045883(__this, method) ((  Enumerator_t1117906205  (*) (List_1_t1583176531 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::IndexOf(T)
#define List_1_IndexOf_m1777080192(__this, ___item0, method) ((  int32_t (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m365202059(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1583176531 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m340985702(__this, ___index0, method) ((  void (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Insert(System.Int32,T)
#define List_1_Insert_m717815893(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1583176531 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m4268358600(__this, ___collection0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Remove(T)
#define List_1_Remove_m3595814454(__this, ___item0, method) ((  bool (*) (List_1_t1583176531 *, Il2CppObject *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1900038087(__this, ___match0, method) ((  int32_t (*) (List_1_t1583176531 *, Predicate_1_t657025514 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3864596313(__this, ___index0, method) ((  void (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Reverse()
#define List_1_Reverse_m3963289155(__this, method) ((  void (*) (List_1_t1583176531 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Sort()
#define List_1_Sort_m2556718797(__this, method) ((  void (*) (List_1_t1583176531 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1064947697(__this, ___comparer0, method) ((  void (*) (List_1_t1583176531 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1325057426(__this, ___comparison0, method) ((  void (*) (List_1_t1583176531 *, Comparison_1_t3475794250 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::ToArray()
#define List_1_ToArray_m3110538714(__this, method) ((  IDataProcessorU5BU5D_t215962270* (*) (List_1_t1583176531 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::TrimExcess()
#define List_1_TrimExcess_m3711121172(__this, method) ((  void (*) (List_1_t1583176531 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Capacity()
#define List_1_get_Capacity_m197593326(__this, method) ((  int32_t (*) (List_1_t1583176531 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3931238347(__this, ___value0, method) ((  void (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Count()
#define List_1_get_Count_m314033343(__this, method) ((  int32_t (*) (List_1_t1583176531 *, const MethodInfo*))List_1_get_Count_m1549410684_gshared)(__this, method)
// T System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Item(System.Int32)
#define List_1_get_Item_m2840009344(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1583176531 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::set_Item(System.Int32,T)
#define List_1_set_Item_m3339849380(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1583176531 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
