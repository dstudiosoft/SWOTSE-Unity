﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingPrice
struct  BuildingPrice_t4120786961  : public Il2CppObject
{
public:
	// System.String BuildingPrice::building
	String_t* ___building_0;
	// System.Int64 BuildingPrice::level
	int64_t ___level_1;
	// System.Int64 BuildingPrice::food_price
	int64_t ___food_price_2;
	// System.Int64 BuildingPrice::wood_price
	int64_t ___wood_price_3;
	// System.Int64 BuildingPrice::stone_price
	int64_t ___stone_price_4;
	// System.Int64 BuildingPrice::iron_price
	int64_t ___iron_price_5;
	// System.Int64 BuildingPrice::cooper_price
	int64_t ___cooper_price_6;
	// System.Int64 BuildingPrice::gold_price
	int64_t ___gold_price_7;
	// System.Int64 BuildingPrice::silver_price
	int64_t ___silver_price_8;
	// System.Int64 BuildingPrice::time_seconds
	int64_t ___time_seconds_9;
	// System.Int64 BuildingPrice::population_price
	int64_t ___population_price_10;

public:
	inline static int32_t get_offset_of_building_0() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___building_0)); }
	inline String_t* get_building_0() const { return ___building_0; }
	inline String_t** get_address_of_building_0() { return &___building_0; }
	inline void set_building_0(String_t* value)
	{
		___building_0 = value;
		Il2CppCodeGenWriteBarrier(&___building_0, value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___level_1)); }
	inline int64_t get_level_1() const { return ___level_1; }
	inline int64_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int64_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_food_price_2() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___food_price_2)); }
	inline int64_t get_food_price_2() const { return ___food_price_2; }
	inline int64_t* get_address_of_food_price_2() { return &___food_price_2; }
	inline void set_food_price_2(int64_t value)
	{
		___food_price_2 = value;
	}

	inline static int32_t get_offset_of_wood_price_3() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___wood_price_3)); }
	inline int64_t get_wood_price_3() const { return ___wood_price_3; }
	inline int64_t* get_address_of_wood_price_3() { return &___wood_price_3; }
	inline void set_wood_price_3(int64_t value)
	{
		___wood_price_3 = value;
	}

	inline static int32_t get_offset_of_stone_price_4() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___stone_price_4)); }
	inline int64_t get_stone_price_4() const { return ___stone_price_4; }
	inline int64_t* get_address_of_stone_price_4() { return &___stone_price_4; }
	inline void set_stone_price_4(int64_t value)
	{
		___stone_price_4 = value;
	}

	inline static int32_t get_offset_of_iron_price_5() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___iron_price_5)); }
	inline int64_t get_iron_price_5() const { return ___iron_price_5; }
	inline int64_t* get_address_of_iron_price_5() { return &___iron_price_5; }
	inline void set_iron_price_5(int64_t value)
	{
		___iron_price_5 = value;
	}

	inline static int32_t get_offset_of_cooper_price_6() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___cooper_price_6)); }
	inline int64_t get_cooper_price_6() const { return ___cooper_price_6; }
	inline int64_t* get_address_of_cooper_price_6() { return &___cooper_price_6; }
	inline void set_cooper_price_6(int64_t value)
	{
		___cooper_price_6 = value;
	}

	inline static int32_t get_offset_of_gold_price_7() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___gold_price_7)); }
	inline int64_t get_gold_price_7() const { return ___gold_price_7; }
	inline int64_t* get_address_of_gold_price_7() { return &___gold_price_7; }
	inline void set_gold_price_7(int64_t value)
	{
		___gold_price_7 = value;
	}

	inline static int32_t get_offset_of_silver_price_8() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___silver_price_8)); }
	inline int64_t get_silver_price_8() const { return ___silver_price_8; }
	inline int64_t* get_address_of_silver_price_8() { return &___silver_price_8; }
	inline void set_silver_price_8(int64_t value)
	{
		___silver_price_8 = value;
	}

	inline static int32_t get_offset_of_time_seconds_9() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___time_seconds_9)); }
	inline int64_t get_time_seconds_9() const { return ___time_seconds_9; }
	inline int64_t* get_address_of_time_seconds_9() { return &___time_seconds_9; }
	inline void set_time_seconds_9(int64_t value)
	{
		___time_seconds_9 = value;
	}

	inline static int32_t get_offset_of_population_price_10() { return static_cast<int32_t>(offsetof(BuildingPrice_t4120786961, ___population_price_10)); }
	inline int64_t get_population_price_10() const { return ___population_price_10; }
	inline int64_t* get_address_of_population_price_10() { return &___population_price_10; }
	inline void set_population_price_10(int64_t value)
	{
		___population_price_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
