﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t1165316627;
// System.String
struct String_t;
// System.Xml.XmlReader
struct XmlReader_t3675626668;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"

// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageParser::LoadLanguage(System.String)
extern "C"  SortedDictionary_2_t1165316627 * LanguageParser_LoadLanguage_m2618941532 (Il2CppObject * __this /* static, unused */, String_t* ___languageDataInResX0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageParser::ReadElements(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadElements_m2188771964 (Il2CppObject * __this /* static, unused */, XmlReader_t3675626668 * ___reader0, SortedDictionary_2_t1165316627 * ___loadedLanguageDictionary1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageParser::ReadData(System.Xml.XmlReader,System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>)
extern "C"  void LanguageParser_ReadData_m4260239193 (Il2CppObject * __this /* static, unused */, XmlReader_t3675626668 * ___reader0, SortedDictionary_2_t1165316627 * ___loadedLanguageDictionary1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageParser::.cctor()
extern "C"  void LanguageParser__cctor_m3716978656 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
