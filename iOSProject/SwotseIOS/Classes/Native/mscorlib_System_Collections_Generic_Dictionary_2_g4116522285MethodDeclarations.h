﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor()
#define Dictionary_2__ctor_m3153700567(__this, method) ((  void (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2__ctor_m124580243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3692134535(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2326253746_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m3351973596(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m211013737_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Int32)
#define Dictionary_2__ctor_m40481033(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4116522285 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m4124809118_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1472157225(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m325487198_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2528867198(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t4116522285 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2244333169_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m2096418871(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4116522285 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m911950084_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2344229188(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1813892273_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m286082690(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m801292737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1324414395(__this, method) ((  bool (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m646110990_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1575167414(__this, method) ((  bool (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2194943565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3574248404(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3718341703_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3946563391(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1687041320_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1061187924(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3508451905_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1625695148(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m220046645_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m774970667(__this, ___key0, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m4271917748_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m673995282(__this, method) ((  bool (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2486839199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3075715164(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4071822443_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1481184180(__this, method) ((  bool (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m888307177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3782410063(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4116522285 *, KeyValuePair_2_t1873867507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2109085310_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3415442293(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4116522285 *, KeyValuePair_2_t1873867507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m702018262_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m772691555(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4116522285 *, KeyValuePair_2U5BU5D_t1533778914*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3986830834_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3382946190(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4116522285 *, KeyValuePair_2_t1873867507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m726496379_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2135367634(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1208203871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2004217757(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1197244232_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3528055934(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1100726089_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m983786707(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3328478662_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::get_Count()
#define Dictionary_2_get_Count_m3913100327(__this, method) ((  int32_t (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_get_Count_m448824823_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::get_Item(TKey)
#define Dictionary_2_get_Item_m1491732843(__this, ___key0, method) ((  WorldMapTimer_t3127003883 * (*) (Dictionary_2_t4116522285 *, int64_t, const MethodInfo*))Dictionary_2_get_Item_m3471279138_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m2879497656(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4116522285 *, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_set_Item_m833853867_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2414840246(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4116522285 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m714553675_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1575472185(__this, ___size0, method) ((  void (*) (Dictionary_2_t4116522285 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2616434050_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m2643120355(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2218457516_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1120480261(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1873867507  (*) (Il2CppObject * /* static, unused */, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_make_pair_m1191177978_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m4261320625(__this /* static, unused */, ___key0, ___value1, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_pick_key_m2837971332_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3880251537(__this /* static, unused */, ___key0, ___value1, method) ((  WorldMapTimer_t3127003883 * (*) (Il2CppObject * /* static, unused */, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_pick_value_m505674372_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m163003060(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4116522285 *, KeyValuePair_2U5BU5D_t1533778914*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m464317087_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::Resize()
#define Dictionary_2_Resize_m3468876452(__this, method) ((  void (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_Resize_m712255353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::Add(TKey,TValue)
#define Dictionary_2_Add_m2016267151(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4116522285 *, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_Add_m4090877298_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::Clear()
#define Dictionary_2_Clear_m3102312073(__this, method) ((  void (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_Clear_m2719677234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m4100806486(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4116522285 *, int64_t, const MethodInfo*))Dictionary_2_ContainsKey_m1696530778_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m3871122755(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4116522285 *, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_ContainsValue_m708762778_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3374361038(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4116522285 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2535477411_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m1824256864(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m580017739_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::Remove(TKey)
#define Dictionary_2_Remove_m4112624034(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4116522285 *, int64_t, const MethodInfo*))Dictionary_2_Remove_m1021937566_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m416687700(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4116522285 *, int64_t, WorldMapTimer_t3127003883 **, const MethodInfo*))Dictionary_2_TryGetValue_m2816326379_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::get_Keys()
#define Dictionary_2_get_Keys_m632232079(__this, method) ((  KeyCollection_t2305052760 * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_get_Keys_m3224914778_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::get_Values()
#define Dictionary_2_get_Values_m4286278833(__this, method) ((  ValueCollection_t2819582128 * (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_get_Values_m1211041850_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m4279464942(__this, ___key0, method) ((  int64_t (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3985428345_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m848821230(__this, ___value0, method) ((  WorldMapTimer_t3127003883 * (*) (Dictionary_2_t4116522285 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2608319129_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1881747012(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4116522285 *, KeyValuePair_2_t1873867507 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2652199299_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2191841429(__this, method) ((  Enumerator_t1141579691  (*) (Dictionary_2_t4116522285 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3878797192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m2448833828(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int64_t, WorldMapTimer_t3127003883 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1176185521_gshared)(__this /* static, unused */, ___key0, ___value1, method)
