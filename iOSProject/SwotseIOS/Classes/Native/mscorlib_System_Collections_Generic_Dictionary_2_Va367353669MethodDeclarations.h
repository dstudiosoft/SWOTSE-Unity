﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2968042853(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t367353669 *, Dictionary_2_t1664293826 *, const MethodInfo*))ValueCollection__ctor_m1801851342_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3843014211(__this, ___item0, method) ((  void (*) (ValueCollection_t367353669 *, X509CertificateCollection_t1197680765 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1477647540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m858816708(__this, method) ((  void (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m573646175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1213112645(__this, ___item0, method) ((  bool (*) (ValueCollection_t367353669 *, X509CertificateCollection_t1197680765 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1598273024_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1182295862(__this, ___item0, method) ((  bool (*) (ValueCollection_t367353669 *, X509CertificateCollection_t1197680765 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3764375695_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4173578650(__this, method) ((  Il2CppObject* (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3036711881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1703892552(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t367353669 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3792551117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3353334987(__this, method) ((  Il2CppObject * (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1773104428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3121271682(__this, method) ((  bool (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1530798787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2663383796(__this, method) ((  bool (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3044620153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4137831874(__this, method) ((  Il2CppObject * (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m919209341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4200336504(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t367353669 *, X509CertificateCollectionU5BU5D_t126139632*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m927881183_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::GetEnumerator()
#define ValueCollection_GetEnumerator_m299646173(__this, method) ((  Enumerator_t3350826590  (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_GetEnumerator_m401908452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_Count()
#define ValueCollection_get_Count_m4214779740(__this, method) ((  int32_t (*) (ValueCollection_t367353669 *, const MethodInfo*))ValueCollection_get_Count_m3718352161_gshared)(__this, method)
