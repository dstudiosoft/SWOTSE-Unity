﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary2513450244MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>::.ctor(TKey)
#define Node__ctor_m3173469844(__this, ___key0, method) ((  void (*) (Node_t4042612855 *, String_t*, const MethodInfo*))Node__ctor_m4091545924_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>::.ctor(TKey,TValue)
#define Node__ctor_m3946349873(__this, ___key0, ___value1, method) ((  void (*) (Node_t4042612855 *, String_t*, LocalizedObject_t1895892772 *, const MethodInfo*))Node__ctor_m2270571809_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>::SwapValue(System.Collections.Generic.RBTree/Node)
#define Node_SwapValue_m1277356573(__this, ___other0, method) ((  void (*) (Node_t4042612855 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m4043495997_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>::AsKV()
#define Node_AsKV_m2998512568(__this, method) ((  KeyValuePair_2_t1568017256  (*) (Node_t4042612855 *, const MethodInfo*))Node_AsKV_m1661049880_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>::AsDE()
#define Node_AsDE_m1469464185(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t4042612855 *, const MethodInfo*))Node_AsDE_m1705531881_gshared)(__this, method)
