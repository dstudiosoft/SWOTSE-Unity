﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.SRDescriptionAttribute
struct SRDescriptionAttribute_t431985547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.SRDescriptionAttribute::.ctor(System.String)
extern "C"  void SRDescriptionAttribute__ctor_m1538913204 (SRDescriptionAttribute_t431985547 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.SRDescriptionAttribute::get_Description()
extern "C"  String_t* SRDescriptionAttribute_get_Description_m477967286 (SRDescriptionAttribute_t431985547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
