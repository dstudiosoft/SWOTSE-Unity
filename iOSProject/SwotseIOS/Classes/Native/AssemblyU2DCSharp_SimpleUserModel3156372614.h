﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleUserModel
struct  SimpleUserModel_t3156372614  : public Il2CppObject
{
public:
	// System.Int64 SimpleUserModel::id
	int64_t ___id_0;
	// System.String SimpleUserModel::username
	String_t* ___username_1;
	// System.String SimpleUserModel::rank
	String_t* ___rank_2;
	// System.Int64 SimpleUserModel::emperor_id
	int64_t ___emperor_id_3;
	// System.String SimpleUserModel::status
	String_t* ___status_4;
	// System.Int64 SimpleUserModel::experience
	int64_t ___experience_5;
	// System.String SimpleUserModel::alliance_name
	String_t* ___alliance_name_6;
	// System.Int64 SimpleUserModel::allianceID
	int64_t ___allianceID_7;
	// System.Int64 SimpleUserModel::capitalID
	int64_t ___capitalID_8;
	// System.String SimpleUserModel::image_name
	String_t* ___image_name_9;
	// System.String SimpleUserModel::flag
	String_t* ___flag_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier(&___username_1, value);
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier(&___rank_2, value);
	}

	inline static int32_t get_offset_of_emperor_id_3() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___emperor_id_3)); }
	inline int64_t get_emperor_id_3() const { return ___emperor_id_3; }
	inline int64_t* get_address_of_emperor_id_3() { return &___emperor_id_3; }
	inline void set_emperor_id_3(int64_t value)
	{
		___emperor_id_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___status_4)); }
	inline String_t* get_status_4() const { return ___status_4; }
	inline String_t** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(String_t* value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier(&___status_4, value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___experience_5)); }
	inline int64_t get_experience_5() const { return ___experience_5; }
	inline int64_t* get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(int64_t value)
	{
		___experience_5 = value;
	}

	inline static int32_t get_offset_of_alliance_name_6() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___alliance_name_6)); }
	inline String_t* get_alliance_name_6() const { return ___alliance_name_6; }
	inline String_t** get_address_of_alliance_name_6() { return &___alliance_name_6; }
	inline void set_alliance_name_6(String_t* value)
	{
		___alliance_name_6 = value;
		Il2CppCodeGenWriteBarrier(&___alliance_name_6, value);
	}

	inline static int32_t get_offset_of_allianceID_7() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___allianceID_7)); }
	inline int64_t get_allianceID_7() const { return ___allianceID_7; }
	inline int64_t* get_address_of_allianceID_7() { return &___allianceID_7; }
	inline void set_allianceID_7(int64_t value)
	{
		___allianceID_7 = value;
	}

	inline static int32_t get_offset_of_capitalID_8() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___capitalID_8)); }
	inline int64_t get_capitalID_8() const { return ___capitalID_8; }
	inline int64_t* get_address_of_capitalID_8() { return &___capitalID_8; }
	inline void set_capitalID_8(int64_t value)
	{
		___capitalID_8 = value;
	}

	inline static int32_t get_offset_of_image_name_9() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___image_name_9)); }
	inline String_t* get_image_name_9() const { return ___image_name_9; }
	inline String_t** get_address_of_image_name_9() { return &___image_name_9; }
	inline void set_image_name_9(String_t* value)
	{
		___image_name_9 = value;
		Il2CppCodeGenWriteBarrier(&___image_name_9, value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(SimpleUserModel_t3156372614, ___flag_10)); }
	inline String_t* get_flag_10() const { return ___flag_10; }
	inline String_t** get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(String_t* value)
	{
		___flag_10 = value;
		Il2CppCodeGenWriteBarrier(&___flag_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
