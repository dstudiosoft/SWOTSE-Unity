﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceBuildingsTableController
struct ResidenceBuildingsTableController_t3217399565;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void ResidenceBuildingsTableController::.ctor()
extern "C"  void ResidenceBuildingsTableController__ctor_m3893678534 (ResidenceBuildingsTableController_t3217399565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableController::OnEnable()
extern "C"  void ResidenceBuildingsTableController_OnEnable_m692884446 (ResidenceBuildingsTableController_t3217399565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ResidenceBuildingsTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t ResidenceBuildingsTableController_GetNumberOfRowsForTableView_m2812739900 (ResidenceBuildingsTableController_t3217399565 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ResidenceBuildingsTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float ResidenceBuildingsTableController_GetHeightForRowInTableView_m401151292 (ResidenceBuildingsTableController_t3217399565 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell ResidenceBuildingsTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * ResidenceBuildingsTableController_GetCellForRowInTableView_m3271011877 (ResidenceBuildingsTableController_t3217399565 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceBuildingsTableController::OpenBuildingWindow(System.Int64)
extern "C"  void ResidenceBuildingsTableController_OpenBuildingWindow_m1575374736 (ResidenceBuildingsTableController_t3217399565 * __this, int64_t ___pitId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
