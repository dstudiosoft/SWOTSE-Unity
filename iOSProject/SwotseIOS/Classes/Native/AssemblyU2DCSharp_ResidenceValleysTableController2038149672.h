﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResidenceValleysTableLine
struct ResidenceValleysTableLine_t3572173254;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceValleysTableController
struct  ResidenceValleysTableController_t2038149672  : public MonoBehaviour_t1158329972
{
public:
	// ResidenceValleysTableLine ResidenceValleysTableController::m_cellPrefab
	ResidenceValleysTableLine_t3572173254 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceValleysTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.ArrayList ResidenceValleysTableController::buildingsList
	ArrayList_t4252133567 * ___buildingsList_4;
	// System.Int32 ResidenceValleysTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t2038149672, ___m_cellPrefab_2)); }
	inline ResidenceValleysTableLine_t3572173254 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceValleysTableLine_t3572173254 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceValleysTableLine_t3572173254 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t2038149672, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_buildingsList_4() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t2038149672, ___buildingsList_4)); }
	inline ArrayList_t4252133567 * get_buildingsList_4() const { return ___buildingsList_4; }
	inline ArrayList_t4252133567 ** get_address_of_buildingsList_4() { return &___buildingsList_4; }
	inline void set_buildingsList_4(ArrayList_t4252133567 * value)
	{
		___buildingsList_4 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsList_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t2038149672, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

struct ResidenceValleysTableController_t2038149672_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ResidenceValleysTableController::<>f__switch$map27
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map27_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map27_6() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t2038149672_StaticFields, ___U3CU3Ef__switchU24map27_6)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map27_6() const { return ___U3CU3Ef__switchU24map27_6; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map27_6() { return &___U3CU3Ef__switchU24map27_6; }
	inline void set_U3CU3Ef__switchU24map27_6(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map27_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map27_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
