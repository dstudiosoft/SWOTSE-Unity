﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`1<Firebase.DependencyStatus>
struct Func_1_t411844801;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_DependencyStatus2752419415.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Func`1<Firebase.DependencyStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m3309214503_gshared (Func_1_t411844801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_1__ctor_m3309214503(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t411844801 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m3309214503_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<Firebase.DependencyStatus>::Invoke()
extern "C"  int32_t Func_1_Invoke_m2839074895_gshared (Func_1_t411844801 * __this, const MethodInfo* method);
#define Func_1_Invoke_m2839074895(__this, method) ((  int32_t (*) (Func_1_t411844801 *, const MethodInfo*))Func_1_Invoke_m2839074895_gshared)(__this, method)
// System.IAsyncResult System.Func`1<Firebase.DependencyStatus>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m1145451444_gshared (Func_1_t411844801 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define Func_1_BeginInvoke_m1145451444(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t411844801 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m1145451444_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<Firebase.DependencyStatus>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_1_EndInvoke_m257073129_gshared (Func_1_t411844801 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_1_EndInvoke_m257073129(__this, ___result0, method) ((  int32_t (*) (Func_1_t411844801 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m257073129_gshared)(__this, ___result0, method)
