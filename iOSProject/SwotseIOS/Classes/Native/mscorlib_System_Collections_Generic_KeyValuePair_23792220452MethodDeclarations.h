﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23792220452.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2246304529_gshared (KeyValuePair_2_t3792220452 * __this, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2246304529(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3792220452 *, int32_t, TouchState_t2732082299 , const MethodInfo*))KeyValuePair_2__ctor_m2246304529_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2234032700_gshared (KeyValuePair_2_t3792220452 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2234032700(__this, method) ((  int32_t (*) (KeyValuePair_2_t3792220452 *, const MethodInfo*))KeyValuePair_2_get_Key_m2234032700_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m20824718_gshared (KeyValuePair_2_t3792220452 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m20824718(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3792220452 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m20824718_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Value()
extern "C"  TouchState_t2732082299  KeyValuePair_2_get_Value_m2639929887_gshared (KeyValuePair_2_t3792220452 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2639929887(__this, method) ((  TouchState_t2732082299  (*) (KeyValuePair_2_t3792220452 *, const MethodInfo*))KeyValuePair_2_get_Value_m2639929887_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2374468134_gshared (KeyValuePair_2_t3792220452 * __this, TouchState_t2732082299  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2374468134(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3792220452 *, TouchState_t2732082299 , const MethodInfo*))KeyValuePair_2_set_Value_m2374468134_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4250223782_gshared (KeyValuePair_2_t3792220452 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4250223782(__this, method) ((  String_t* (*) (KeyValuePair_2_t3792220452 *, const MethodInfo*))KeyValuePair_2_ToString_m4250223782_gshared)(__this, method)
