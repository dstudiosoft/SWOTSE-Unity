﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageParser
struct  LanguageParser_t628765495  : public Il2CppObject
{
public:

public:
};

struct LanguageParser_t628765495_StaticFields
{
public:
	// System.String SmartLocalization.LanguageParser::xmlHeader
	String_t* ___xmlHeader_0;

public:
	inline static int32_t get_offset_of_xmlHeader_0() { return static_cast<int32_t>(offsetof(LanguageParser_t628765495_StaticFields, ___xmlHeader_0)); }
	inline String_t* get_xmlHeader_0() const { return ___xmlHeader_0; }
	inline String_t** get_address_of_xmlHeader_0() { return &___xmlHeader_0; }
	inline void set_xmlHeader_0(String_t* value)
	{
		___xmlHeader_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlHeader_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
