﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// TextLocalizer
struct TextLocalizer_t2861965306;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShellContentManager
struct  ShellContentManager_t407410174  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image ShellContentManager::icon
	Image_t2042527209 * ___icon_2;
	// TextLocalizer ShellContentManager::windowName
	TextLocalizer_t2861965306 * ___windowName_3;
	// UnityEngine.GameObject ShellContentManager::content
	GameObject_t1756533147 * ___content_4;
	// UnityEngine.Sprite ShellContentManager::tabSelectedTop
	Sprite_t309593783 * ___tabSelectedTop_5;
	// UnityEngine.Sprite ShellContentManager::tabSelectedBottom
	Sprite_t309593783 * ___tabSelectedBottom_6;
	// UnityEngine.Sprite ShellContentManager::tabUnselectedTop
	Sprite_t309593783 * ___tabUnselectedTop_7;
	// UnityEngine.Sprite ShellContentManager::tabUnselectedBottom
	Sprite_t309593783 * ___tabUnselectedBottom_8;
	// UnityEngine.GameObject ShellContentManager::moreButton
	GameObject_t1756533147 * ___moreButton_9;
	// UnityEngine.GameObject ShellContentManager::moreLeftArrow
	GameObject_t1756533147 * ___moreLeftArrow_10;
	// UnityEngine.GameObject ShellContentManager::moreRightArrow
	GameObject_t1756533147 * ___moreRightArrow_11;
	// UnityEngine.GameObject[] ShellContentManager::tabs
	GameObjectU5BU5D_t3057952154* ___tabs_12;
	// UnityEngine.GameObject[] ShellContentManager::contents
	GameObjectU5BU5D_t3057952154* ___contents_13;
	// System.Int32 ShellContentManager::selectedTabIndex
	int32_t ___selectedTabIndex_14;
	// System.Collections.Generic.List`1<System.String> ShellContentManager::keys
	List_1_t1398341365 * ___keys_15;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> ShellContentManager::icons
	List_1_t3973682211 * ___icons_16;
	// System.String[] ShellContentManager::tabsNames
	StringU5BU5D_t1642385972* ___tabsNames_17;
	// System.String[] ShellContentManager::actionTabsNames
	StringU5BU5D_t1642385972* ___actionTabsNames_18;
	// System.String[] ShellContentManager::shopTabsNames
	StringU5BU5D_t1642385972* ___shopTabsNames_19;
	// System.String[] ShellContentManager::treasureTabsNames
	StringU5BU5D_t1642385972* ___treasureTabsNames_20;
	// System.String[] ShellContentManager::allianceTabsNames
	StringU5BU5D_t1642385972* ___allianceTabsNames_21;
	// System.String[] ShellContentManager::mailTabsNames
	StringU5BU5D_t1642385972* ___mailTabsNames_22;
	// System.String[] ShellContentManager::chatTabsNames
	StringU5BU5D_t1642385972* ___chatTabsNames_23;
	// System.String[] ShellContentManager::genericBuildingTabsNames
	StringU5BU5D_t1642385972* ___genericBuildingTabsNames_24;
	// System.String[] ShellContentManager::commandCenterTabsNames
	StringU5BU5D_t1642385972* ___commandCenterTabsNames_25;
	// System.String[] ShellContentManager::archeryTabsNames
	StringU5BU5D_t1642385972* ___archeryTabsNames_26;
	// System.String[] ShellContentManager::enginneringTabsNames
	StringU5BU5D_t1642385972* ___enginneringTabsNames_27;
	// System.String[] ShellContentManager::godHouseTabsNames
	StringU5BU5D_t1642385972* ___godHouseTabsNames_28;
	// System.String[] ShellContentManager::militarySchoolTabsNames
	StringU5BU5D_t1642385972* ___militarySchoolTabsNames_29;
	// System.String[] ShellContentManager::stableTabsNames
	StringU5BU5D_t1642385972* ___stableTabsNames_30;
	// System.String[] ShellContentManager::dispatchTabsNames
	StringU5BU5D_t1642385972* ___dispatchTabsNames_31;
	// System.String[] ShellContentManager::residenceTabsNames
	StringU5BU5D_t1642385972* ___residenceTabsNames_32;
	// System.String[] ShellContentManager::guestHouseTabsNames
	StringU5BU5D_t1642385972* ___guestHouseTabsNames_33;
	// System.String[] ShellContentManager::feastingHallTabsNames
	StringU5BU5D_t1642385972* ___feastingHallTabsNames_34;
	// System.String[] ShellContentManager::marketTabsNames
	StringU5BU5D_t1642385972* ___marketTabsNames_35;
	// System.String[] ShellContentManager::settingsTabsNames
	StringU5BU5D_t1642385972* ___settingsTabsNames_36;
	// UnityEngine.GameObject[] ShellContentManager::actionContents
	GameObjectU5BU5D_t3057952154* ___actionContents_37;
	// UnityEngine.GameObject[] ShellContentManager::shopContents
	GameObjectU5BU5D_t3057952154* ___shopContents_38;
	// UnityEngine.GameObject[] ShellContentManager::treasureContents
	GameObjectU5BU5D_t3057952154* ___treasureContents_39;
	// UnityEngine.GameObject[] ShellContentManager::allianceContents
	GameObjectU5BU5D_t3057952154* ___allianceContents_40;
	// UnityEngine.GameObject[] ShellContentManager::mailContents
	GameObjectU5BU5D_t3057952154* ___mailContents_41;
	// UnityEngine.GameObject[] ShellContentManager::chatContents
	GameObjectU5BU5D_t3057952154* ___chatContents_42;
	// UnityEngine.GameObject[] ShellContentManager::genericBuildingsContents
	GameObjectU5BU5D_t3057952154* ___genericBuildingsContents_43;
	// UnityEngine.GameObject[] ShellContentManager::commandCenterContents
	GameObjectU5BU5D_t3057952154* ___commandCenterContents_44;
	// UnityEngine.GameObject[] ShellContentManager::archeryContents
	GameObjectU5BU5D_t3057952154* ___archeryContents_45;
	// UnityEngine.GameObject[] ShellContentManager::enginneringContents
	GameObjectU5BU5D_t3057952154* ___enginneringContents_46;
	// UnityEngine.GameObject[] ShellContentManager::godHouseContents
	GameObjectU5BU5D_t3057952154* ___godHouseContents_47;
	// UnityEngine.GameObject[] ShellContentManager::militarySchoolContents
	GameObjectU5BU5D_t3057952154* ___militarySchoolContents_48;
	// UnityEngine.GameObject[] ShellContentManager::stableContents
	GameObjectU5BU5D_t3057952154* ___stableContents_49;
	// UnityEngine.GameObject[] ShellContentManager::dispatchContents
	GameObjectU5BU5D_t3057952154* ___dispatchContents_50;
	// UnityEngine.GameObject[] ShellContentManager::residenceContents
	GameObjectU5BU5D_t3057952154* ___residenceContents_51;
	// UnityEngine.GameObject[] ShellContentManager::guestHouseContents
	GameObjectU5BU5D_t3057952154* ___guestHouseContents_52;
	// UnityEngine.GameObject[] ShellContentManager::feastingHallContents
	GameObjectU5BU5D_t3057952154* ___feastingHallContents_53;
	// UnityEngine.GameObject[] ShellContentManager::marketContents
	GameObjectU5BU5D_t3057952154* ___marketContents_54;
	// UnityEngine.GameObject[] ShellContentManager::settingsContents
	GameObjectU5BU5D_t3057952154* ___settingsContents_55;
	// UnityEngine.MonoBehaviour ShellContentManager::contentChanger
	MonoBehaviour_t1158329972 * ___contentChanger_56;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___icon_2)); }
	inline Image_t2042527209 * get_icon_2() const { return ___icon_2; }
	inline Image_t2042527209 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(Image_t2042527209 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_windowName_3() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___windowName_3)); }
	inline TextLocalizer_t2861965306 * get_windowName_3() const { return ___windowName_3; }
	inline TextLocalizer_t2861965306 ** get_address_of_windowName_3() { return &___windowName_3; }
	inline void set_windowName_3(TextLocalizer_t2861965306 * value)
	{
		___windowName_3 = value;
		Il2CppCodeGenWriteBarrier(&___windowName_3, value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___content_4)); }
	inline GameObject_t1756533147 * get_content_4() const { return ___content_4; }
	inline GameObject_t1756533147 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(GameObject_t1756533147 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}

	inline static int32_t get_offset_of_tabSelectedTop_5() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabSelectedTop_5)); }
	inline Sprite_t309593783 * get_tabSelectedTop_5() const { return ___tabSelectedTop_5; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedTop_5() { return &___tabSelectedTop_5; }
	inline void set_tabSelectedTop_5(Sprite_t309593783 * value)
	{
		___tabSelectedTop_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedTop_5, value);
	}

	inline static int32_t get_offset_of_tabSelectedBottom_6() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabSelectedBottom_6)); }
	inline Sprite_t309593783 * get_tabSelectedBottom_6() const { return ___tabSelectedBottom_6; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedBottom_6() { return &___tabSelectedBottom_6; }
	inline void set_tabSelectedBottom_6(Sprite_t309593783 * value)
	{
		___tabSelectedBottom_6 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedBottom_6, value);
	}

	inline static int32_t get_offset_of_tabUnselectedTop_7() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabUnselectedTop_7)); }
	inline Sprite_t309593783 * get_tabUnselectedTop_7() const { return ___tabUnselectedTop_7; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedTop_7() { return &___tabUnselectedTop_7; }
	inline void set_tabUnselectedTop_7(Sprite_t309593783 * value)
	{
		___tabUnselectedTop_7 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedTop_7, value);
	}

	inline static int32_t get_offset_of_tabUnselectedBottom_8() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabUnselectedBottom_8)); }
	inline Sprite_t309593783 * get_tabUnselectedBottom_8() const { return ___tabUnselectedBottom_8; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedBottom_8() { return &___tabUnselectedBottom_8; }
	inline void set_tabUnselectedBottom_8(Sprite_t309593783 * value)
	{
		___tabUnselectedBottom_8 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedBottom_8, value);
	}

	inline static int32_t get_offset_of_moreButton_9() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___moreButton_9)); }
	inline GameObject_t1756533147 * get_moreButton_9() const { return ___moreButton_9; }
	inline GameObject_t1756533147 ** get_address_of_moreButton_9() { return &___moreButton_9; }
	inline void set_moreButton_9(GameObject_t1756533147 * value)
	{
		___moreButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___moreButton_9, value);
	}

	inline static int32_t get_offset_of_moreLeftArrow_10() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___moreLeftArrow_10)); }
	inline GameObject_t1756533147 * get_moreLeftArrow_10() const { return ___moreLeftArrow_10; }
	inline GameObject_t1756533147 ** get_address_of_moreLeftArrow_10() { return &___moreLeftArrow_10; }
	inline void set_moreLeftArrow_10(GameObject_t1756533147 * value)
	{
		___moreLeftArrow_10 = value;
		Il2CppCodeGenWriteBarrier(&___moreLeftArrow_10, value);
	}

	inline static int32_t get_offset_of_moreRightArrow_11() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___moreRightArrow_11)); }
	inline GameObject_t1756533147 * get_moreRightArrow_11() const { return ___moreRightArrow_11; }
	inline GameObject_t1756533147 ** get_address_of_moreRightArrow_11() { return &___moreRightArrow_11; }
	inline void set_moreRightArrow_11(GameObject_t1756533147 * value)
	{
		___moreRightArrow_11 = value;
		Il2CppCodeGenWriteBarrier(&___moreRightArrow_11, value);
	}

	inline static int32_t get_offset_of_tabs_12() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabs_12)); }
	inline GameObjectU5BU5D_t3057952154* get_tabs_12() const { return ___tabs_12; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_tabs_12() { return &___tabs_12; }
	inline void set_tabs_12(GameObjectU5BU5D_t3057952154* value)
	{
		___tabs_12 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_12, value);
	}

	inline static int32_t get_offset_of_contents_13() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___contents_13)); }
	inline GameObjectU5BU5D_t3057952154* get_contents_13() const { return ___contents_13; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_contents_13() { return &___contents_13; }
	inline void set_contents_13(GameObjectU5BU5D_t3057952154* value)
	{
		___contents_13 = value;
		Il2CppCodeGenWriteBarrier(&___contents_13, value);
	}

	inline static int32_t get_offset_of_selectedTabIndex_14() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___selectedTabIndex_14)); }
	inline int32_t get_selectedTabIndex_14() const { return ___selectedTabIndex_14; }
	inline int32_t* get_address_of_selectedTabIndex_14() { return &___selectedTabIndex_14; }
	inline void set_selectedTabIndex_14(int32_t value)
	{
		___selectedTabIndex_14 = value;
	}

	inline static int32_t get_offset_of_keys_15() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___keys_15)); }
	inline List_1_t1398341365 * get_keys_15() const { return ___keys_15; }
	inline List_1_t1398341365 ** get_address_of_keys_15() { return &___keys_15; }
	inline void set_keys_15(List_1_t1398341365 * value)
	{
		___keys_15 = value;
		Il2CppCodeGenWriteBarrier(&___keys_15, value);
	}

	inline static int32_t get_offset_of_icons_16() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___icons_16)); }
	inline List_1_t3973682211 * get_icons_16() const { return ___icons_16; }
	inline List_1_t3973682211 ** get_address_of_icons_16() { return &___icons_16; }
	inline void set_icons_16(List_1_t3973682211 * value)
	{
		___icons_16 = value;
		Il2CppCodeGenWriteBarrier(&___icons_16, value);
	}

	inline static int32_t get_offset_of_tabsNames_17() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___tabsNames_17)); }
	inline StringU5BU5D_t1642385972* get_tabsNames_17() const { return ___tabsNames_17; }
	inline StringU5BU5D_t1642385972** get_address_of_tabsNames_17() { return &___tabsNames_17; }
	inline void set_tabsNames_17(StringU5BU5D_t1642385972* value)
	{
		___tabsNames_17 = value;
		Il2CppCodeGenWriteBarrier(&___tabsNames_17, value);
	}

	inline static int32_t get_offset_of_actionTabsNames_18() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___actionTabsNames_18)); }
	inline StringU5BU5D_t1642385972* get_actionTabsNames_18() const { return ___actionTabsNames_18; }
	inline StringU5BU5D_t1642385972** get_address_of_actionTabsNames_18() { return &___actionTabsNames_18; }
	inline void set_actionTabsNames_18(StringU5BU5D_t1642385972* value)
	{
		___actionTabsNames_18 = value;
		Il2CppCodeGenWriteBarrier(&___actionTabsNames_18, value);
	}

	inline static int32_t get_offset_of_shopTabsNames_19() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___shopTabsNames_19)); }
	inline StringU5BU5D_t1642385972* get_shopTabsNames_19() const { return ___shopTabsNames_19; }
	inline StringU5BU5D_t1642385972** get_address_of_shopTabsNames_19() { return &___shopTabsNames_19; }
	inline void set_shopTabsNames_19(StringU5BU5D_t1642385972* value)
	{
		___shopTabsNames_19 = value;
		Il2CppCodeGenWriteBarrier(&___shopTabsNames_19, value);
	}

	inline static int32_t get_offset_of_treasureTabsNames_20() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___treasureTabsNames_20)); }
	inline StringU5BU5D_t1642385972* get_treasureTabsNames_20() const { return ___treasureTabsNames_20; }
	inline StringU5BU5D_t1642385972** get_address_of_treasureTabsNames_20() { return &___treasureTabsNames_20; }
	inline void set_treasureTabsNames_20(StringU5BU5D_t1642385972* value)
	{
		___treasureTabsNames_20 = value;
		Il2CppCodeGenWriteBarrier(&___treasureTabsNames_20, value);
	}

	inline static int32_t get_offset_of_allianceTabsNames_21() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___allianceTabsNames_21)); }
	inline StringU5BU5D_t1642385972* get_allianceTabsNames_21() const { return ___allianceTabsNames_21; }
	inline StringU5BU5D_t1642385972** get_address_of_allianceTabsNames_21() { return &___allianceTabsNames_21; }
	inline void set_allianceTabsNames_21(StringU5BU5D_t1642385972* value)
	{
		___allianceTabsNames_21 = value;
		Il2CppCodeGenWriteBarrier(&___allianceTabsNames_21, value);
	}

	inline static int32_t get_offset_of_mailTabsNames_22() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___mailTabsNames_22)); }
	inline StringU5BU5D_t1642385972* get_mailTabsNames_22() const { return ___mailTabsNames_22; }
	inline StringU5BU5D_t1642385972** get_address_of_mailTabsNames_22() { return &___mailTabsNames_22; }
	inline void set_mailTabsNames_22(StringU5BU5D_t1642385972* value)
	{
		___mailTabsNames_22 = value;
		Il2CppCodeGenWriteBarrier(&___mailTabsNames_22, value);
	}

	inline static int32_t get_offset_of_chatTabsNames_23() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___chatTabsNames_23)); }
	inline StringU5BU5D_t1642385972* get_chatTabsNames_23() const { return ___chatTabsNames_23; }
	inline StringU5BU5D_t1642385972** get_address_of_chatTabsNames_23() { return &___chatTabsNames_23; }
	inline void set_chatTabsNames_23(StringU5BU5D_t1642385972* value)
	{
		___chatTabsNames_23 = value;
		Il2CppCodeGenWriteBarrier(&___chatTabsNames_23, value);
	}

	inline static int32_t get_offset_of_genericBuildingTabsNames_24() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___genericBuildingTabsNames_24)); }
	inline StringU5BU5D_t1642385972* get_genericBuildingTabsNames_24() const { return ___genericBuildingTabsNames_24; }
	inline StringU5BU5D_t1642385972** get_address_of_genericBuildingTabsNames_24() { return &___genericBuildingTabsNames_24; }
	inline void set_genericBuildingTabsNames_24(StringU5BU5D_t1642385972* value)
	{
		___genericBuildingTabsNames_24 = value;
		Il2CppCodeGenWriteBarrier(&___genericBuildingTabsNames_24, value);
	}

	inline static int32_t get_offset_of_commandCenterTabsNames_25() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___commandCenterTabsNames_25)); }
	inline StringU5BU5D_t1642385972* get_commandCenterTabsNames_25() const { return ___commandCenterTabsNames_25; }
	inline StringU5BU5D_t1642385972** get_address_of_commandCenterTabsNames_25() { return &___commandCenterTabsNames_25; }
	inline void set_commandCenterTabsNames_25(StringU5BU5D_t1642385972* value)
	{
		___commandCenterTabsNames_25 = value;
		Il2CppCodeGenWriteBarrier(&___commandCenterTabsNames_25, value);
	}

	inline static int32_t get_offset_of_archeryTabsNames_26() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___archeryTabsNames_26)); }
	inline StringU5BU5D_t1642385972* get_archeryTabsNames_26() const { return ___archeryTabsNames_26; }
	inline StringU5BU5D_t1642385972** get_address_of_archeryTabsNames_26() { return &___archeryTabsNames_26; }
	inline void set_archeryTabsNames_26(StringU5BU5D_t1642385972* value)
	{
		___archeryTabsNames_26 = value;
		Il2CppCodeGenWriteBarrier(&___archeryTabsNames_26, value);
	}

	inline static int32_t get_offset_of_enginneringTabsNames_27() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___enginneringTabsNames_27)); }
	inline StringU5BU5D_t1642385972* get_enginneringTabsNames_27() const { return ___enginneringTabsNames_27; }
	inline StringU5BU5D_t1642385972** get_address_of_enginneringTabsNames_27() { return &___enginneringTabsNames_27; }
	inline void set_enginneringTabsNames_27(StringU5BU5D_t1642385972* value)
	{
		___enginneringTabsNames_27 = value;
		Il2CppCodeGenWriteBarrier(&___enginneringTabsNames_27, value);
	}

	inline static int32_t get_offset_of_godHouseTabsNames_28() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___godHouseTabsNames_28)); }
	inline StringU5BU5D_t1642385972* get_godHouseTabsNames_28() const { return ___godHouseTabsNames_28; }
	inline StringU5BU5D_t1642385972** get_address_of_godHouseTabsNames_28() { return &___godHouseTabsNames_28; }
	inline void set_godHouseTabsNames_28(StringU5BU5D_t1642385972* value)
	{
		___godHouseTabsNames_28 = value;
		Il2CppCodeGenWriteBarrier(&___godHouseTabsNames_28, value);
	}

	inline static int32_t get_offset_of_militarySchoolTabsNames_29() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___militarySchoolTabsNames_29)); }
	inline StringU5BU5D_t1642385972* get_militarySchoolTabsNames_29() const { return ___militarySchoolTabsNames_29; }
	inline StringU5BU5D_t1642385972** get_address_of_militarySchoolTabsNames_29() { return &___militarySchoolTabsNames_29; }
	inline void set_militarySchoolTabsNames_29(StringU5BU5D_t1642385972* value)
	{
		___militarySchoolTabsNames_29 = value;
		Il2CppCodeGenWriteBarrier(&___militarySchoolTabsNames_29, value);
	}

	inline static int32_t get_offset_of_stableTabsNames_30() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___stableTabsNames_30)); }
	inline StringU5BU5D_t1642385972* get_stableTabsNames_30() const { return ___stableTabsNames_30; }
	inline StringU5BU5D_t1642385972** get_address_of_stableTabsNames_30() { return &___stableTabsNames_30; }
	inline void set_stableTabsNames_30(StringU5BU5D_t1642385972* value)
	{
		___stableTabsNames_30 = value;
		Il2CppCodeGenWriteBarrier(&___stableTabsNames_30, value);
	}

	inline static int32_t get_offset_of_dispatchTabsNames_31() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___dispatchTabsNames_31)); }
	inline StringU5BU5D_t1642385972* get_dispatchTabsNames_31() const { return ___dispatchTabsNames_31; }
	inline StringU5BU5D_t1642385972** get_address_of_dispatchTabsNames_31() { return &___dispatchTabsNames_31; }
	inline void set_dispatchTabsNames_31(StringU5BU5D_t1642385972* value)
	{
		___dispatchTabsNames_31 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchTabsNames_31, value);
	}

	inline static int32_t get_offset_of_residenceTabsNames_32() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___residenceTabsNames_32)); }
	inline StringU5BU5D_t1642385972* get_residenceTabsNames_32() const { return ___residenceTabsNames_32; }
	inline StringU5BU5D_t1642385972** get_address_of_residenceTabsNames_32() { return &___residenceTabsNames_32; }
	inline void set_residenceTabsNames_32(StringU5BU5D_t1642385972* value)
	{
		___residenceTabsNames_32 = value;
		Il2CppCodeGenWriteBarrier(&___residenceTabsNames_32, value);
	}

	inline static int32_t get_offset_of_guestHouseTabsNames_33() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___guestHouseTabsNames_33)); }
	inline StringU5BU5D_t1642385972* get_guestHouseTabsNames_33() const { return ___guestHouseTabsNames_33; }
	inline StringU5BU5D_t1642385972** get_address_of_guestHouseTabsNames_33() { return &___guestHouseTabsNames_33; }
	inline void set_guestHouseTabsNames_33(StringU5BU5D_t1642385972* value)
	{
		___guestHouseTabsNames_33 = value;
		Il2CppCodeGenWriteBarrier(&___guestHouseTabsNames_33, value);
	}

	inline static int32_t get_offset_of_feastingHallTabsNames_34() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___feastingHallTabsNames_34)); }
	inline StringU5BU5D_t1642385972* get_feastingHallTabsNames_34() const { return ___feastingHallTabsNames_34; }
	inline StringU5BU5D_t1642385972** get_address_of_feastingHallTabsNames_34() { return &___feastingHallTabsNames_34; }
	inline void set_feastingHallTabsNames_34(StringU5BU5D_t1642385972* value)
	{
		___feastingHallTabsNames_34 = value;
		Il2CppCodeGenWriteBarrier(&___feastingHallTabsNames_34, value);
	}

	inline static int32_t get_offset_of_marketTabsNames_35() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___marketTabsNames_35)); }
	inline StringU5BU5D_t1642385972* get_marketTabsNames_35() const { return ___marketTabsNames_35; }
	inline StringU5BU5D_t1642385972** get_address_of_marketTabsNames_35() { return &___marketTabsNames_35; }
	inline void set_marketTabsNames_35(StringU5BU5D_t1642385972* value)
	{
		___marketTabsNames_35 = value;
		Il2CppCodeGenWriteBarrier(&___marketTabsNames_35, value);
	}

	inline static int32_t get_offset_of_settingsTabsNames_36() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___settingsTabsNames_36)); }
	inline StringU5BU5D_t1642385972* get_settingsTabsNames_36() const { return ___settingsTabsNames_36; }
	inline StringU5BU5D_t1642385972** get_address_of_settingsTabsNames_36() { return &___settingsTabsNames_36; }
	inline void set_settingsTabsNames_36(StringU5BU5D_t1642385972* value)
	{
		___settingsTabsNames_36 = value;
		Il2CppCodeGenWriteBarrier(&___settingsTabsNames_36, value);
	}

	inline static int32_t get_offset_of_actionContents_37() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___actionContents_37)); }
	inline GameObjectU5BU5D_t3057952154* get_actionContents_37() const { return ___actionContents_37; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_actionContents_37() { return &___actionContents_37; }
	inline void set_actionContents_37(GameObjectU5BU5D_t3057952154* value)
	{
		___actionContents_37 = value;
		Il2CppCodeGenWriteBarrier(&___actionContents_37, value);
	}

	inline static int32_t get_offset_of_shopContents_38() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___shopContents_38)); }
	inline GameObjectU5BU5D_t3057952154* get_shopContents_38() const { return ___shopContents_38; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shopContents_38() { return &___shopContents_38; }
	inline void set_shopContents_38(GameObjectU5BU5D_t3057952154* value)
	{
		___shopContents_38 = value;
		Il2CppCodeGenWriteBarrier(&___shopContents_38, value);
	}

	inline static int32_t get_offset_of_treasureContents_39() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___treasureContents_39)); }
	inline GameObjectU5BU5D_t3057952154* get_treasureContents_39() const { return ___treasureContents_39; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_treasureContents_39() { return &___treasureContents_39; }
	inline void set_treasureContents_39(GameObjectU5BU5D_t3057952154* value)
	{
		___treasureContents_39 = value;
		Il2CppCodeGenWriteBarrier(&___treasureContents_39, value);
	}

	inline static int32_t get_offset_of_allianceContents_40() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___allianceContents_40)); }
	inline GameObjectU5BU5D_t3057952154* get_allianceContents_40() const { return ___allianceContents_40; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_allianceContents_40() { return &___allianceContents_40; }
	inline void set_allianceContents_40(GameObjectU5BU5D_t3057952154* value)
	{
		___allianceContents_40 = value;
		Il2CppCodeGenWriteBarrier(&___allianceContents_40, value);
	}

	inline static int32_t get_offset_of_mailContents_41() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___mailContents_41)); }
	inline GameObjectU5BU5D_t3057952154* get_mailContents_41() const { return ___mailContents_41; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_mailContents_41() { return &___mailContents_41; }
	inline void set_mailContents_41(GameObjectU5BU5D_t3057952154* value)
	{
		___mailContents_41 = value;
		Il2CppCodeGenWriteBarrier(&___mailContents_41, value);
	}

	inline static int32_t get_offset_of_chatContents_42() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___chatContents_42)); }
	inline GameObjectU5BU5D_t3057952154* get_chatContents_42() const { return ___chatContents_42; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_chatContents_42() { return &___chatContents_42; }
	inline void set_chatContents_42(GameObjectU5BU5D_t3057952154* value)
	{
		___chatContents_42 = value;
		Il2CppCodeGenWriteBarrier(&___chatContents_42, value);
	}

	inline static int32_t get_offset_of_genericBuildingsContents_43() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___genericBuildingsContents_43)); }
	inline GameObjectU5BU5D_t3057952154* get_genericBuildingsContents_43() const { return ___genericBuildingsContents_43; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_genericBuildingsContents_43() { return &___genericBuildingsContents_43; }
	inline void set_genericBuildingsContents_43(GameObjectU5BU5D_t3057952154* value)
	{
		___genericBuildingsContents_43 = value;
		Il2CppCodeGenWriteBarrier(&___genericBuildingsContents_43, value);
	}

	inline static int32_t get_offset_of_commandCenterContents_44() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___commandCenterContents_44)); }
	inline GameObjectU5BU5D_t3057952154* get_commandCenterContents_44() const { return ___commandCenterContents_44; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_commandCenterContents_44() { return &___commandCenterContents_44; }
	inline void set_commandCenterContents_44(GameObjectU5BU5D_t3057952154* value)
	{
		___commandCenterContents_44 = value;
		Il2CppCodeGenWriteBarrier(&___commandCenterContents_44, value);
	}

	inline static int32_t get_offset_of_archeryContents_45() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___archeryContents_45)); }
	inline GameObjectU5BU5D_t3057952154* get_archeryContents_45() const { return ___archeryContents_45; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_archeryContents_45() { return &___archeryContents_45; }
	inline void set_archeryContents_45(GameObjectU5BU5D_t3057952154* value)
	{
		___archeryContents_45 = value;
		Il2CppCodeGenWriteBarrier(&___archeryContents_45, value);
	}

	inline static int32_t get_offset_of_enginneringContents_46() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___enginneringContents_46)); }
	inline GameObjectU5BU5D_t3057952154* get_enginneringContents_46() const { return ___enginneringContents_46; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_enginneringContents_46() { return &___enginneringContents_46; }
	inline void set_enginneringContents_46(GameObjectU5BU5D_t3057952154* value)
	{
		___enginneringContents_46 = value;
		Il2CppCodeGenWriteBarrier(&___enginneringContents_46, value);
	}

	inline static int32_t get_offset_of_godHouseContents_47() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___godHouseContents_47)); }
	inline GameObjectU5BU5D_t3057952154* get_godHouseContents_47() const { return ___godHouseContents_47; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_godHouseContents_47() { return &___godHouseContents_47; }
	inline void set_godHouseContents_47(GameObjectU5BU5D_t3057952154* value)
	{
		___godHouseContents_47 = value;
		Il2CppCodeGenWriteBarrier(&___godHouseContents_47, value);
	}

	inline static int32_t get_offset_of_militarySchoolContents_48() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___militarySchoolContents_48)); }
	inline GameObjectU5BU5D_t3057952154* get_militarySchoolContents_48() const { return ___militarySchoolContents_48; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_militarySchoolContents_48() { return &___militarySchoolContents_48; }
	inline void set_militarySchoolContents_48(GameObjectU5BU5D_t3057952154* value)
	{
		___militarySchoolContents_48 = value;
		Il2CppCodeGenWriteBarrier(&___militarySchoolContents_48, value);
	}

	inline static int32_t get_offset_of_stableContents_49() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___stableContents_49)); }
	inline GameObjectU5BU5D_t3057952154* get_stableContents_49() const { return ___stableContents_49; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_stableContents_49() { return &___stableContents_49; }
	inline void set_stableContents_49(GameObjectU5BU5D_t3057952154* value)
	{
		___stableContents_49 = value;
		Il2CppCodeGenWriteBarrier(&___stableContents_49, value);
	}

	inline static int32_t get_offset_of_dispatchContents_50() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___dispatchContents_50)); }
	inline GameObjectU5BU5D_t3057952154* get_dispatchContents_50() const { return ___dispatchContents_50; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_dispatchContents_50() { return &___dispatchContents_50; }
	inline void set_dispatchContents_50(GameObjectU5BU5D_t3057952154* value)
	{
		___dispatchContents_50 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchContents_50, value);
	}

	inline static int32_t get_offset_of_residenceContents_51() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___residenceContents_51)); }
	inline GameObjectU5BU5D_t3057952154* get_residenceContents_51() const { return ___residenceContents_51; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_residenceContents_51() { return &___residenceContents_51; }
	inline void set_residenceContents_51(GameObjectU5BU5D_t3057952154* value)
	{
		___residenceContents_51 = value;
		Il2CppCodeGenWriteBarrier(&___residenceContents_51, value);
	}

	inline static int32_t get_offset_of_guestHouseContents_52() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___guestHouseContents_52)); }
	inline GameObjectU5BU5D_t3057952154* get_guestHouseContents_52() const { return ___guestHouseContents_52; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_guestHouseContents_52() { return &___guestHouseContents_52; }
	inline void set_guestHouseContents_52(GameObjectU5BU5D_t3057952154* value)
	{
		___guestHouseContents_52 = value;
		Il2CppCodeGenWriteBarrier(&___guestHouseContents_52, value);
	}

	inline static int32_t get_offset_of_feastingHallContents_53() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___feastingHallContents_53)); }
	inline GameObjectU5BU5D_t3057952154* get_feastingHallContents_53() const { return ___feastingHallContents_53; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_feastingHallContents_53() { return &___feastingHallContents_53; }
	inline void set_feastingHallContents_53(GameObjectU5BU5D_t3057952154* value)
	{
		___feastingHallContents_53 = value;
		Il2CppCodeGenWriteBarrier(&___feastingHallContents_53, value);
	}

	inline static int32_t get_offset_of_marketContents_54() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___marketContents_54)); }
	inline GameObjectU5BU5D_t3057952154* get_marketContents_54() const { return ___marketContents_54; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_marketContents_54() { return &___marketContents_54; }
	inline void set_marketContents_54(GameObjectU5BU5D_t3057952154* value)
	{
		___marketContents_54 = value;
		Il2CppCodeGenWriteBarrier(&___marketContents_54, value);
	}

	inline static int32_t get_offset_of_settingsContents_55() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___settingsContents_55)); }
	inline GameObjectU5BU5D_t3057952154* get_settingsContents_55() const { return ___settingsContents_55; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_settingsContents_55() { return &___settingsContents_55; }
	inline void set_settingsContents_55(GameObjectU5BU5D_t3057952154* value)
	{
		___settingsContents_55 = value;
		Il2CppCodeGenWriteBarrier(&___settingsContents_55, value);
	}

	inline static int32_t get_offset_of_contentChanger_56() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174, ___contentChanger_56)); }
	inline MonoBehaviour_t1158329972 * get_contentChanger_56() const { return ___contentChanger_56; }
	inline MonoBehaviour_t1158329972 ** get_address_of_contentChanger_56() { return &___contentChanger_56; }
	inline void set_contentChanger_56(MonoBehaviour_t1158329972 * value)
	{
		___contentChanger_56 = value;
		Il2CppCodeGenWriteBarrier(&___contentChanger_56, value);
	}
};

struct ShellContentManager_t407410174_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShellContentManager::<>f__switch$map11
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map11_57;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShellContentManager::<>f__switch$map12
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map12_58;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShellContentManager::<>f__switch$map13
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map13_59;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_57() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174_StaticFields, ___U3CU3Ef__switchU24map11_57)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map11_57() const { return ___U3CU3Ef__switchU24map11_57; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map11_57() { return &___U3CU3Ef__switchU24map11_57; }
	inline void set_U3CU3Ef__switchU24map11_57(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map11_57 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map11_57, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_58() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174_StaticFields, ___U3CU3Ef__switchU24map12_58)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map12_58() const { return ___U3CU3Ef__switchU24map12_58; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map12_58() { return &___U3CU3Ef__switchU24map12_58; }
	inline void set_U3CU3Ef__switchU24map12_58(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map12_58 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map12_58, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_59() { return static_cast<int32_t>(offsetof(ShellContentManager_t407410174_StaticFields, ___U3CU3Ef__switchU24map13_59)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map13_59() const { return ___U3CU3Ef__switchU24map13_59; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map13_59() { return &___U3CU3Ef__switchU24map13_59; }
	inline void set_U3CU3Ef__switchU24map13_59(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map13_59 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map13_59, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
