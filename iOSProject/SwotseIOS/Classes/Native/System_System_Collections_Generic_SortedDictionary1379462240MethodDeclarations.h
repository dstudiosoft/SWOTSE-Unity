﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary4145266925MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m4250090302(__this, ___dic0, method) ((  void (*) (KeyCollection_t1379462240 *, SortedDictionary_2_t1165316627 *, const MethodInfo*))KeyCollection__ctor_m1640811614_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m157541369(__this, ___item0, method) ((  void (*) (KeyCollection_t1379462240 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m125209017_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4223388716(__this, method) ((  void (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4124574780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3293790895(__this, ___item0, method) ((  bool (*) (KeyCollection_t1379462240 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3936183375_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1166148766(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1016326398_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2959502154(__this, method) ((  bool (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m421859978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2572803872(__this, ___item0, method) ((  bool (*) (KeyCollection_t1379462240 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3986283392_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2235977330(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1379462240 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2542066674_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1577974558(__this, method) ((  bool (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m694373310_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1017097730(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2770359586_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1907155953(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1072197233_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2692091468(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t1379462240 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2465470732_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::get_Count()
#define KeyCollection_get_Count_m327180030(__this, method) ((  int32_t (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_get_Count_m1152988478_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.String,SmartLocalization.LocalizedObject>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1093965706(__this, method) ((  Enumerator_t1030438893  (*) (KeyCollection_t1379462240 *, const MethodInfo*))KeyCollection_GetEnumerator_m1078324954_gshared)(__this, method)
