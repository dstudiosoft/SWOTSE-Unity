﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Boolean TouchScript.Utils.TouchUtils::IsTouchOnTarget(TouchScript.TouchPoint,UnityEngine.Transform)
extern "C"  bool TouchUtils_IsTouchOnTarget_m2614418393 (Il2CppObject * __this /* static, unused */, TouchPoint_t959629083 * ___touch0, Transform_t3275118058 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Utils.TouchUtils::IsTouchOnTarget(TouchScript.TouchPoint)
extern "C"  bool TouchUtils_IsTouchOnTarget_m4234527436 (Il2CppObject * __this /* static, unused */, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
