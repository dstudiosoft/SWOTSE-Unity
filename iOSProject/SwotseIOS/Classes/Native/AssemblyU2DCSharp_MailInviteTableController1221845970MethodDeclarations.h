﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailInviteTableController
struct MailInviteTableController_t1221845970;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailInviteTableController::.ctor()
extern "C"  void MailInviteTableController__ctor_m4051458437 (MailInviteTableController_t1221845970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::add_confirmed(ConfirmationEvent)
extern "C"  void MailInviteTableController_add_confirmed_m4060223739 (MailInviteTableController_t1221845970 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::remove_confirmed(ConfirmationEvent)
extern "C"  void MailInviteTableController_remove_confirmed_m3898242094 (MailInviteTableController_t1221845970 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::Start()
extern "C"  void MailInviteTableController_Start_m600230509 (MailInviteTableController_t1221845970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MailInviteTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t MailInviteTableController_GetNumberOfRowsForTableView_m927429097 (MailInviteTableController_t1221845970 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MailInviteTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float MailInviteTableController_GetHeightForRowInTableView_m3605448989 (MailInviteTableController_t1221845970 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell MailInviteTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * MailInviteTableController_GetCellForRowInTableView_m3638988720 (MailInviteTableController_t1221845970 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::AcceptInvite(System.Int64)
extern "C"  void MailInviteTableController_AcceptInvite_m4221122544 (MailInviteTableController_t1221845970 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::AcceptConfirmed(System.Boolean)
extern "C"  void MailInviteTableController_AcceptConfirmed_m801360777 (MailInviteTableController_t1221845970 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::RejectInvite(System.Int64)
extern "C"  void MailInviteTableController_RejectInvite_m1151562189 (MailInviteTableController_t1221845970 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::InviteResponded(System.Object,System.String)
extern "C"  void MailInviteTableController_InviteResponded_m981888730 (MailInviteTableController_t1221845970 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::GotAllReports(System.Object,System.String)
extern "C"  void MailInviteTableController_GotAllReports_m1596364461 (MailInviteTableController_t1221845970 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteTableController::ReloadTable()
extern "C"  void MailInviteTableController_ReloadTable_m2546501006 (MailInviteTableController_t1221845970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
