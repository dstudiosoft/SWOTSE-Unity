﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1924026834.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView/CellVisibilityChangeEvent
struct  CellVisibilityChangeEvent_t4037417944  : public UnityEvent_2_t1924026834
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
