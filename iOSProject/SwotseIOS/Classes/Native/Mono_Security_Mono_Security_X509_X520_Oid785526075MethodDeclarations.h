﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Oid
struct Oid_t785526075;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.X509.X520/Oid::.ctor(System.String)
extern "C"  void Oid__ctor_m467139567 (Oid_t785526075 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
