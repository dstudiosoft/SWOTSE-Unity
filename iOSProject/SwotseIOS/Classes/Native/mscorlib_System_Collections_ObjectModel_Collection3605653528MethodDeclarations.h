﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>
struct Collection_1_t3605653528;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit2D>
struct IEnumerator_1_t1539432601;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit2D>
struct IList_1_t309882079;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void Collection_1__ctor_m390506634_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1__ctor_m390506634(__this, method) ((  void (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1__ctor_m390506634_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2592714319_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2592714319(__this, method) ((  bool (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2592714319_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1928910886_gshared (Collection_1_t3605653528 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1928910886(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3605653528 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1928910886_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1659084883_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1659084883(__this, method) ((  Il2CppObject * (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1659084883_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m280083906_gshared (Collection_1_t3605653528 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m280083906(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3605653528 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m280083906_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m750833604_gshared (Collection_1_t3605653528 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m750833604(__this, ___value0, method) ((  bool (*) (Collection_1_t3605653528 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m750833604_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2643267280_gshared (Collection_1_t3605653528 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2643267280(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3605653528 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2643267280_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1335229875_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1335229875(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1335229875_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3194533563_gshared (Collection_1_t3605653528 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3194533563(__this, ___value0, method) ((  void (*) (Collection_1_t3605653528 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3194533563_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3624938634_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3624938634(__this, method) ((  bool (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3624938634_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m158942284_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m158942284(__this, method) ((  Il2CppObject * (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m158942284_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1883078203_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1883078203(__this, method) ((  bool (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1883078203_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1891110238_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1891110238(__this, method) ((  bool (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1891110238_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3182398911_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3182398911(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3605653528 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3182398911_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m4031524744_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m4031524744(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m4031524744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::Add(T)
extern "C"  void Collection_1_Add_m6685631_gshared (Collection_1_t3605653528 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define Collection_1_Add_m6685631(__this, ___item0, method) ((  void (*) (Collection_1_t3605653528 *, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_Add_m6685631_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::Clear()
extern "C"  void Collection_1_Clear_m3716451811_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3716451811(__this, method) ((  void (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_Clear_m3716451811_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3137909269_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3137909269(__this, method) ((  void (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_ClearItems_m3137909269_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::Contains(T)
extern "C"  bool Collection_1_Contains_m3311681013_gshared (Collection_1_t3605653528 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3311681013(__this, ___item0, method) ((  bool (*) (Collection_1_t3605653528 *, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_Contains_m3311681013_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4117938155_gshared (Collection_1_t3605653528 * __this, RaycastHit2DU5BU5D_t4176517891* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4117938155(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3605653528 *, RaycastHit2DU5BU5D_t4176517891*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4117938155_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2016057462_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2016057462(__this, method) ((  Il2CppObject* (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_GetEnumerator_m2016057462_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m910589295_gshared (Collection_1_t3605653528 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m910589295(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3605653528 *, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_IndexOf_m910589295_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4174518684_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4174518684(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_Insert_m4174518684_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1913566413_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1913566413(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_InsertItem_m1913566413_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::Remove(T)
extern "C"  bool Collection_1_Remove_m1474181138_gshared (Collection_1_t3605653528 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1474181138(__this, ___item0, method) ((  bool (*) (Collection_1_t3605653528 *, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_Remove_m1474181138_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m274043088_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m274043088(__this, ___index0, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m274043088_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2644246006_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2644246006(__this, ___index0, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2644246006_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3870744802_gshared (Collection_1_t3605653528 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3870744802(__this, method) ((  int32_t (*) (Collection_1_t3605653528 *, const MethodInfo*))Collection_1_get_Count_m3870744802_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::get_Item(System.Int32)
extern "C"  RaycastHit2D_t4063908774  Collection_1_get_Item_m1915917292_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1915917292(__this, ___index0, method) ((  RaycastHit2D_t4063908774  (*) (Collection_1_t3605653528 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1915917292_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2112793599_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2112793599(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_set_Item_m2112793599_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2320803352_gshared (Collection_1_t3605653528 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2320803352(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3605653528 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))Collection_1_SetItem_m2320803352_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m245722497_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m245722497(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m245722497_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::ConvertItem(System.Object)
extern "C"  RaycastHit2D_t4063908774  Collection_1_ConvertItem_m1771683453_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1771683453(__this /* static, unused */, ___item0, method) ((  RaycastHit2D_t4063908774  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1771683453_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m619698441_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m619698441(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m619698441_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m4226119057_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m4226119057(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m4226119057_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit2D>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3181382272_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3181382272(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3181382272_gshared)(__this /* static, unused */, ___list0, method)
