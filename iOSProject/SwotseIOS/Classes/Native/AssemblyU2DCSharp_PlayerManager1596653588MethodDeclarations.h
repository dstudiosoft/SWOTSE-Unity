﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerManager
struct PlayerManager_t1596653588;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerManager::.ctor()
extern "C"  void PlayerManager__ctor_m2438461151 (PlayerManager_t1596653588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::add_onGetMyUserInfo(SimpleEvent)
extern "C"  void PlayerManager_add_onGetMyUserInfo_m1296356271 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::remove_onGetMyUserInfo(SimpleEvent)
extern "C"  void PlayerManager_remove_onGetMyUserInfo_m1085890858 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::add_onLanguageChanged(SimpleEvent)
extern "C"  void PlayerManager_add_onLanguageChanged_m4119474822 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::remove_onLanguageChanged(SimpleEvent)
extern "C"  void PlayerManager_remove_onLanguageChanged_m411778897 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::add_onPasswordChanged(SimpleEvent)
extern "C"  void PlayerManager_add_onPasswordChanged_m2193964611 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::remove_onPasswordChanged(SimpleEvent)
extern "C"  void PlayerManager_remove_onPasswordChanged_m779248484 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::add_onResetPasswordRequested(SimpleEvent)
extern "C"  void PlayerManager_add_onResetPasswordRequested_m602395600 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::remove_onResetPasswordRequested(SimpleEvent)
extern "C"  void PlayerManager_remove_onResetPasswordRequested_m3971202623 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::add_onPasswordReset(SimpleEvent)
extern "C"  void PlayerManager_add_onPasswordReset_m3072609412 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::remove_onPasswordReset(SimpleEvent)
extern "C"  void PlayerManager_remove_onPasswordReset_m3809759655 (PlayerManager_t1596653588 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::getMyUserInfo(System.Boolean)
extern "C"  Il2CppObject * PlayerManager_getMyUserInfo_m1708760175 (PlayerManager_t1596653588 * __this, bool ___initial0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::ChangeUserInfo(System.String,System.String,System.String,System.String)
extern "C"  Il2CppObject * PlayerManager_ChangeUserInfo_m1444416926 (PlayerManager_t1596653588 * __this, String_t* ___username0, String_t* ___email1, String_t* ___image2, String_t* ___flag3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::NominateUser(System.Int64)
extern "C"  Il2CppObject * PlayerManager_NominateUser_m712080813 (PlayerManager_t1596653588 * __this, int64_t ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::ChangeLanguage(System.String)
extern "C"  Il2CppObject * PlayerManager_ChangeLanguage_m944066787 (PlayerManager_t1596653588 * __this, String_t* ___lang0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::ChangeUserPassword(System.String,System.String)
extern "C"  Il2CppObject * PlayerManager_ChangeUserPassword_m956047533 (PlayerManager_t1596653588 * __this, String_t* ___oldPass0, String_t* ___newPass1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::ForgotPassword(System.String)
extern "C"  Il2CppObject * PlayerManager_ForgotPassword_m1101230447 (PlayerManager_t1596653588 * __this, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerManager::ResetPassword(System.String,System.String,System.String)
extern "C"  Il2CppObject * PlayerManager_ResetPassword_m754512717 (PlayerManager_t1596653588 * __this, String_t* ___email0, String_t* ___secretKey1, String_t* ___newPassword2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
