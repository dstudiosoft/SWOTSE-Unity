﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemTimer
struct ItemTimer_t3839766396;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemTimer::.ctor()
extern "C"  void ItemTimer__ctor_m585884249 (ItemTimer_t3839766396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
