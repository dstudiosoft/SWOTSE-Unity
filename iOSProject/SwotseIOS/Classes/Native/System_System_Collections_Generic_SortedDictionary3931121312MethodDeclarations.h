﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t4145266925;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary1004715516.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m2646846914_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m2646846914(__this, method) ((  void (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2__ctor_m2646846914_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m2297982677_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m2297982677(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m2297982677_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1627652201_gshared (SortedDictionary_2_t3931121312 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1627652201(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t3931121312 *, KeyValuePair_2_t38854645 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1627652201_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1375072327_gshared (SortedDictionary_2_t3931121312 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1375072327(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, KeyValuePair_2_t38854645 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1375072327_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1154929448_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1154929448(__this, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1154929448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1895620934_gshared (SortedDictionary_2_t3931121312 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1895620934(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, KeyValuePair_2_t38854645 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1895620934_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m1435283656_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1435283656(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1435283656_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_Contains_m2099850172_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m2099850172(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m2099850172_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1167000725_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1167000725(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1167000725_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m463095569_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m463095569(__this, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m463095569_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3722589254_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3722589254(__this, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3722589254_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2424577816_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2424577816(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m2424577816_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m3973233665_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m3973233665(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m3973233665_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Values_m4006702514_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m4006702514(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m4006702514_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m749784322_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m749784322(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m749784322_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m2214150629_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m2214150629(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2214150629_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m2402063550_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m2402063550(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m2402063550_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1383457962_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1383457962(__this, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m1383457962_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2986761918_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2986761918(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2986761918_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m50684391_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m50684391(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m50684391_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1660205152_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1660205152(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1660205152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m738745858_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m738745858(__this, method) ((  int32_t (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_get_Count_m738745858_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedDictionary_2_get_Item_m2496989701_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m2496989701(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_get_Item_m2496989701_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m2198693116_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m2198693116(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_set_Item_m2198693116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  KeyCollection_t4145266925 * SortedDictionary_2_get_Keys_m1481998532_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Keys_m1481998532(__this, method) ((  KeyCollection_t4145266925 * (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_get_Keys_m1481998532_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m3261466727_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m3261466727(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Add_m3261466727_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void SortedDictionary_2_Clear_m2350355463_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m2350355463(__this, method) ((  void (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_Clear_m2350355463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool SortedDictionary_2_ContainsKey_m2379881333_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ContainsKey_m2379881333(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ContainsKey_m2379881333_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C"  bool SortedDictionary_2_ContainsValue_m3842886989_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ContainsValue_m3842886989(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ContainsValue_m3842886989_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m2987881320_gshared (SortedDictionary_2_t3931121312 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m2987881320(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t3931121312 *, KeyValuePair_2U5BU5D_t2854920344*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m2987881320_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1004715516  SortedDictionary_2_GetEnumerator_m3792996474_gshared (SortedDictionary_2_t3931121312 * __this, const MethodInfo* method);
#define SortedDictionary_2_GetEnumerator_m3792996474(__this, method) ((  Enumerator_t1004715516  (*) (SortedDictionary_2_t3931121312 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m3792996474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m2920055847_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m2920055847(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Remove_m2920055847_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m3440732424_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m3440732424(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SortedDictionary_2_TryGetValue_m3440732424_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ToKey(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToKey_m874477334_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m874477334(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m874477334_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToValue_m1308966358_gshared (SortedDictionary_2_t3931121312 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m1308966358(__this, ___value0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t3931121312 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m1308966358_gshared)(__this, ___value0, method)
