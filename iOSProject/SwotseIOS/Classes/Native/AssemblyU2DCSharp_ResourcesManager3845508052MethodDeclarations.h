﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResourcesManager
struct ResourcesManager_t3845508052;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ResourcesManager::.ctor()
extern "C"  void ResourcesManager__ctor_m329448297 (ResourcesManager_t3845508052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourcesManager::OnEnable()
extern "C"  void ResourcesManager_OnEnable_m280175489 (ResourcesManager_t3845508052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourcesManager::FixedUpdate()
extern "C"  void ResourcesManager_FixedUpdate_m1512896956 (ResourcesManager_t3845508052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResourcesManager::Format(System.Int64)
extern "C"  String_t* ResourcesManager_Format_m958641925 (ResourcesManager_t3845508052 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
