﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t1520641178;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
struct DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke;
struct DownloadHandlerAudioClip_t1520641178_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"

// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::.ctor(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip__ctor_m1779320660 (DownloadHandlerAudioClip_t1520641178 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandlerAudioClip::GetData()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandlerAudioClip_GetData_m2445379515 (DownloadHandlerAudioClip_t1520641178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.DownloadHandlerAudioClip::GetText()
extern "C"  String_t* DownloadHandlerAudioClip_GetText_m788833651 (DownloadHandlerAudioClip_t1520641178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandlerAudioClip::InternalGetData()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandlerAudioClip_InternalGetData_m2607427810 (DownloadHandlerAudioClip_t1520641178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct DownloadHandlerAudioClip_t1520641178;
struct DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke;

extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_back(const DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DownloadHandlerAudioClip_t1520641178;
struct DownloadHandlerAudioClip_t1520641178_marshaled_com;

extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_back(const DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled);
