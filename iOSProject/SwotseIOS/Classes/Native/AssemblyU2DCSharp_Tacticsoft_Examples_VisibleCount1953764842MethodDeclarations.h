﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.VisibleCounterCell
struct VisibleCounterCell_t1953764842;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.Examples.VisibleCounterCell::.ctor()
extern "C"  void VisibleCounterCell__ctor_m120978942 (VisibleCounterCell_t1953764842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.VisibleCounterCell::SetRowNumber(System.Int32)
extern "C"  void VisibleCounterCell_SetRowNumber_m2565305320 (VisibleCounterCell_t1953764842 * __this, int32_t ___rowNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.VisibleCounterCell::NotifyBecameVisible()
extern "C"  void VisibleCounterCell_NotifyBecameVisible_m2520836794 (VisibleCounterCell_t1953764842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
