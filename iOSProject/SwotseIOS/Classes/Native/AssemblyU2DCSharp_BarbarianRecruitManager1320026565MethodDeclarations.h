﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarbarianRecruitManager
struct BarbarianRecruitManager_t1320026565;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BarbarianRecruitManager::.ctor()
extern "C"  void BarbarianRecruitManager__ctor_m861491794 (BarbarianRecruitManager_t1320026565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager::OnEnable()
extern "C"  void BarbarianRecruitManager_OnEnable_m4186744938 (BarbarianRecruitManager_t1320026565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager::FixedUpdate()
extern "C"  void BarbarianRecruitManager_FixedUpdate_m1735305643 (BarbarianRecruitManager_t1320026565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BarbarianRecruitManager::GetBarbariansArmy()
extern "C"  Il2CppObject * BarbarianRecruitManager_GetBarbariansArmy_m4286074228 (BarbarianRecruitManager_t1320026565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager::GotBarbariansArmy(System.Object,System.String)
extern "C"  void BarbarianRecruitManager_GotBarbariansArmy_m3062456730 (BarbarianRecruitManager_t1320026565 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager::March()
extern "C"  void BarbarianRecruitManager_March_m1959306329 (BarbarianRecruitManager_t1320026565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager::MarchLaunched(System.Object,System.String)
extern "C"  void BarbarianRecruitManager_MarchLaunched_m2783691265 (BarbarianRecruitManager_t1320026565 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
