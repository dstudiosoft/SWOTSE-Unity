﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1298916669(__this, ___dictionary0, method) ((  void (*) (Enumerator_t221636275 *, Dictionary_2_t3196578869 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3483933618(__this, method) ((  Il2CppObject * (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1393367940(__this, method) ((  void (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m432187547(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1694767980(__this, method) ((  Il2CppObject * (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1827347490(__this, method) ((  Il2CppObject * (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::MoveNext()
#define Enumerator_MoveNext_m2172356264(__this, method) ((  bool (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_Current()
#define Enumerator_get_Current_m4261317416(__this, method) ((  KeyValuePair_2_t953924091  (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2687346609(__this, method) ((  int32_t (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1236052601(__this, method) ((  TouchProxyBase_t4188753234 * (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::Reset()
#define Enumerator_Reset_m1117612827(__this, method) ((  void (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::VerifyState()
#define Enumerator_VerifyState_m2116374056(__this, method) ((  void (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m836070036(__this, method) ((  void (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::Dispose()
#define Enumerator_Dispose_m1834040253(__this, method) ((  void (*) (Enumerator_t221636275 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
