﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityChangerContentManager/<ChangeCity>c__Iterator8
struct U3CChangeCityU3Ec__Iterator8_t2773508460;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CityChangerContentManager/<ChangeCity>c__Iterator8::.ctor()
extern "C"  void U3CChangeCityU3Ec__Iterator8__ctor_m1921647007 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CityChangerContentManager/<ChangeCity>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangeCityU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2613922857 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CityChangerContentManager/<ChangeCity>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangeCityU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1789700337 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CityChangerContentManager/<ChangeCity>c__Iterator8::MoveNext()
extern "C"  bool U3CChangeCityU3Ec__Iterator8_MoveNext_m1030803125 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager/<ChangeCity>c__Iterator8::Dispose()
extern "C"  void U3CChangeCityU3Ec__Iterator8_Dispose_m199356402 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager/<ChangeCity>c__Iterator8::Reset()
extern "C"  void U3CChangeCityU3Ec__Iterator8_Reset_m200759080 (U3CChangeCityU3Ec__Iterator8_t2773508460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
