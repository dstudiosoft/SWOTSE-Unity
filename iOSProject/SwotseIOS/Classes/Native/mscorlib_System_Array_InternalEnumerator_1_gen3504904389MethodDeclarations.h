﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3504904389.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADDR2646152127.h"

// System.Void System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3459855412_gshared (InternalEnumerator_1_t3504904389 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3459855412(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3504904389 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3459855412_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2988361192_gshared (InternalEnumerator_1_t3504904389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2988361192(__this, method) ((  void (*) (InternalEnumerator_1_t3504904389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2988361192_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3539585728_gshared (InternalEnumerator_1_t3504904389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3539585728(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3504904389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3539585728_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3012299853_gshared (InternalEnumerator_1_t3504904389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3012299853(__this, method) ((  void (*) (InternalEnumerator_1_t3504904389 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3012299853_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2121845700_gshared (InternalEnumerator_1_t3504904389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2121845700(__this, method) ((  bool (*) (InternalEnumerator_1_t3504904389 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2121845700_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.NetworkInformation.Win32_IP_ADDR_STRING>::get_Current()
extern "C"  Win32_IP_ADDR_STRING_t2646152127  InternalEnumerator_1_get_Current_m1266653029_gshared (InternalEnumerator_1_t3504904389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1266653029(__this, method) ((  Win32_IP_ADDR_STRING_t2646152127  (*) (InternalEnumerator_1_t3504904389 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1266653029_gshared)(__this, method)
