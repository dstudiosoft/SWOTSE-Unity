﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LevelLoadManager
struct LevelLoadManager_t1376428509;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LevelLoadManager::.ctor()
extern "C"  void LevelLoadManager__ctor_m4145143616 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::Start()
extern "C"  void LevelLoadManager_Start_m575122696 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::LevelLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void LevelLoadManager_LevelLoaded_m2089755094 (LevelLoadManager_t1376428509 * __this, Scene_t1684909666  ___level0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::UpdateCityBuildings(System.Object,System.String)
extern "C"  void LevelLoadManager_UpdateCityBuildings_m2836126643 (LevelLoadManager_t1376428509 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::LoadLogin()
extern "C"  void LevelLoadManager_LoadLogin_m101724901 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::LoadCity()
extern "C"  void LevelLoadManager_LoadCity_m3038044705 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::LoadFields()
extern "C"  void LevelLoadManager_LoadFields_m1769914127 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoadManager::LoadWorld()
extern "C"  void LevelLoadManager_LoadWorld_m840801520 (LevelLoadManager_t1376428509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
