﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1756902760(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1899638712 *, Dictionary_2_t3196578869 *, const MethodInfo*))ValueCollection__ctor_m882866357_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2767506142(__this, ___item0, method) ((  void (*) (ValueCollection_t1899638712 *, TouchProxyBase_t4188753234 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2444808545(__this, method) ((  void (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m531783846(__this, ___item0, method) ((  bool (*) (ValueCollection_t1899638712 *, TouchProxyBase_t4188753234 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1169773309(__this, ___item0, method) ((  bool (*) (ValueCollection_t1899638712 *, TouchProxyBase_t4188753234 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3798456179(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3502281979(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1899638712 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1626083490(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1188634513(__this, method) ((  bool (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m323887539(__this, method) ((  bool (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m898247391(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2406898325(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1899638712 *, TouchProxyBaseU5BU5D_t1988954407*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1460341186_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::GetEnumerator()
#define ValueCollection_GetEnumerator_m789662480(__this, method) ((  Enumerator_t588144337  (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_GetEnumerator_m941805197_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_Count()
#define ValueCollection_get_Count_m234342171(__this, method) ((  int32_t (*) (ValueCollection_t1899638712 *, const MethodInfo*))ValueCollection_get_Count_m90930038_gshared)(__this, method)
