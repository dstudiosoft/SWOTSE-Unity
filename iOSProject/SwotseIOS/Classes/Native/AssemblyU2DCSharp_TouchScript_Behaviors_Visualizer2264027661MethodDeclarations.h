﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Behaviors.Visualizer.TouchVisualizer
struct TouchVisualizer_t2264027661;
// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct TouchProxyBase_t4188753234;
// System.Object
struct Il2CppObject;
// TouchScript.TouchEventArgs
struct TouchEventArgs_t1917927166;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Behaviors_Visualizer4188753234.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_TouchScript_TouchEventArgs1917927166.h"

// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::.ctor()
extern "C"  void TouchVisualizer__ctor_m2342014749 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Behaviors.Visualizer.TouchProxyBase TouchScript.Behaviors.Visualizer.TouchVisualizer::get_TouchProxy()
extern "C"  TouchProxyBase_t4188753234 * TouchVisualizer_get_TouchProxy_m3977603029 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::set_TouchProxy(TouchScript.Behaviors.Visualizer.TouchProxyBase)
extern "C"  void TouchVisualizer_set_TouchProxy_m904954068 (TouchVisualizer_t2264027661 * __this, TouchProxyBase_t4188753234 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::get_ShowTouchId()
extern "C"  bool TouchVisualizer_get_ShowTouchId_m3493738887 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::set_ShowTouchId(System.Boolean)
extern "C"  void TouchVisualizer_set_ShowTouchId_m3975426564 (TouchVisualizer_t2264027661 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::get_ShowTags()
extern "C"  bool TouchVisualizer_get_ShowTags_m1644674668 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::set_ShowTags(System.Boolean)
extern "C"  void TouchVisualizer_set_ShowTags_m598450369 (TouchVisualizer_t2264027661 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::get_UseDPI()
extern "C"  bool TouchVisualizer_get_UseDPI_m3580429498 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::set_UseDPI(System.Boolean)
extern "C"  void TouchVisualizer_set_UseDPI_m2106195669 (TouchVisualizer_t2264027661 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Behaviors.Visualizer.TouchVisualizer::get_TouchSize()
extern "C"  float TouchVisualizer_get_TouchSize_m3663557704 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::set_TouchSize(System.Single)
extern "C"  void TouchVisualizer_set_TouchSize_m2420724285 (TouchVisualizer_t2264027661 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::Awake()
extern "C"  void TouchVisualizer_Awake_m452431104 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::OnEnable()
extern "C"  void TouchVisualizer_OnEnable_m3684614125 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::OnDisable()
extern "C"  void TouchVisualizer_OnDisable_m567284232 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Behaviors.Visualizer.TouchProxyBase TouchScript.Behaviors.Visualizer.TouchVisualizer::instantiateProxy()
extern "C"  TouchProxyBase_t4188753234 * TouchVisualizer_instantiateProxy_m1566690759 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::clearProxy(TouchScript.Behaviors.Visualizer.TouchProxyBase)
extern "C"  void TouchVisualizer_clearProxy_m87539447 (TouchVisualizer_t2264027661 * __this, TouchProxyBase_t4188753234 * ___proxy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Behaviors.Visualizer.TouchVisualizer::getTouchSize()
extern "C"  int32_t TouchVisualizer_getTouchSize_m1392127961 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::updateDefaultSize()
extern "C"  void TouchVisualizer_updateDefaultSize_m712623256 (TouchVisualizer_t2264027661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::touchesBeganHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchVisualizer_touchesBeganHandler_m1088375537 (TouchVisualizer_t2264027661 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::touchesMovedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchVisualizer_touchesMovedHandler_m3263425139 (TouchVisualizer_t2264027661 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::touchesEndedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchVisualizer_touchesEndedHandler_m888041284 (TouchVisualizer_t2264027661 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchVisualizer::touchesCancelledHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void TouchVisualizer_touchesCancelledHandler_m435767349 (TouchVisualizer_t2264027661 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
