﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>
struct ReadOnlyCollection_1_t272966012;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit>
struct IList_1_t628120921;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1857671443;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3722871593_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3722871593(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3722871593_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2241196623_gshared (ReadOnlyCollection_1_t272966012 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2241196623(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2241196623_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2772319027_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2772319027(__this, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2772319027_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m469572512_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, RaycastHit_t87180320  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m469572512(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m469572512_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2331937446_gshared (ReadOnlyCollection_1_t272966012 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2331937446(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2331937446_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3826837852_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3826837852(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3826837852_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  RaycastHit_t87180320  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2697237672_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2697237672(__this, ___index0, method) ((  RaycastHit_t87180320  (*) (ReadOnlyCollection_1_t272966012 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2697237672_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2778518819_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, RaycastHit_t87180320  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2778518819(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2778518819_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3852095879_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3852095879(__this, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3852095879_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3355622950_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3355622950(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3355622950_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1962564019_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1962564019(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1962564019_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m192909722_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m192909722(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t272966012 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m192909722_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m531052960_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m531052960(__this, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m531052960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m403763588_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m403763588(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m403763588_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4202341488_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4202341488(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t272966012 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4202341488_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m874952915_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m874952915(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m874952915_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3505350851_gshared (ReadOnlyCollection_1_t272966012 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3505350851(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3505350851_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m304046265_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m304046265(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m304046265_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2487447202_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2487447202(__this, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2487447202_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1549747372_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1549747372(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1549747372_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4061947891_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4061947891(__this, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4061947891_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2758046846_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2758046846(__this, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2758046846_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m115711519_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m115711519(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t272966012 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m115711519_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m4115511440_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4115511440(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4115511440_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2394715837_gshared (ReadOnlyCollection_1_t272966012 * __this, RaycastHit_t87180320  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2394715837(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t272966012 *, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2394715837_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1655798507_gshared (ReadOnlyCollection_1_t272966012 * __this, RaycastHitU5BU5D_t1214023521* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1655798507(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t272966012 *, RaycastHitU5BU5D_t1214023521*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1655798507_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2770700566_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2770700566(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2770700566_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2114589399_gshared (ReadOnlyCollection_1_t272966012 * __this, RaycastHit_t87180320  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2114589399(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t272966012 *, RaycastHit_t87180320 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2114589399_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m189913978_gshared (ReadOnlyCollection_1_t272966012 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m189913978(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t272966012 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m189913978_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t87180320  ReadOnlyCollection_1_get_Item_m808353620_gshared (ReadOnlyCollection_1_t272966012 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m808353620(__this, ___index0, method) ((  RaycastHit_t87180320  (*) (ReadOnlyCollection_1_t272966012 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m808353620_gshared)(__this, ___index0, method)
