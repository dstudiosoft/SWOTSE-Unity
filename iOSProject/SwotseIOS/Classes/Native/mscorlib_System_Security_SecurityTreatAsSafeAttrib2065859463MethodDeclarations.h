﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityTreatAsSafeAttribute
struct SecurityTreatAsSafeAttribute_t2065859463;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecurityTreatAsSafeAttribute::.ctor()
extern "C"  void SecurityTreatAsSafeAttribute__ctor_m477438662 (SecurityTreatAsSafeAttribute_t2065859463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
