﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingIconManager
struct  TrainingIconManager_t2314554274  : public MonoBehaviour_t1158329972
{
public:
	// System.String TrainingIconManager::internalUnitName
	String_t* ___internalUnitName_2;
	// UnityEngine.UI.Text TrainingIconManager::foodPrice
	Text_t356221433 * ___foodPrice_3;
	// UnityEngine.UI.Text TrainingIconManager::woodPrice
	Text_t356221433 * ___woodPrice_4;
	// UnityEngine.UI.Text TrainingIconManager::stonePrice
	Text_t356221433 * ___stonePrice_5;
	// UnityEngine.UI.Text TrainingIconManager::ironPrice
	Text_t356221433 * ___ironPrice_6;
	// UnityEngine.UI.Text TrainingIconManager::copperPrice
	Text_t356221433 * ___copperPrice_7;
	// UnityEngine.UI.Text TrainingIconManager::silverPrice
	Text_t356221433 * ___silverPrice_8;
	// UnityEngine.UI.Text TrainingIconManager::goldPrice
	Text_t356221433 * ___goldPrice_9;
	// UnityEngine.UI.Text TrainingIconManager::trainingTime
	Text_t356221433 * ___trainingTime_10;
	// UnityEngine.UI.Text TrainingIconManager::unitCount
	Text_t356221433 * ___unitCount_11;
	// UnityEngine.UI.Text TrainingIconManager::unitName
	Text_t356221433 * ___unitName_12;

public:
	inline static int32_t get_offset_of_internalUnitName_2() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___internalUnitName_2)); }
	inline String_t* get_internalUnitName_2() const { return ___internalUnitName_2; }
	inline String_t** get_address_of_internalUnitName_2() { return &___internalUnitName_2; }
	inline void set_internalUnitName_2(String_t* value)
	{
		___internalUnitName_2 = value;
		Il2CppCodeGenWriteBarrier(&___internalUnitName_2, value);
	}

	inline static int32_t get_offset_of_foodPrice_3() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___foodPrice_3)); }
	inline Text_t356221433 * get_foodPrice_3() const { return ___foodPrice_3; }
	inline Text_t356221433 ** get_address_of_foodPrice_3() { return &___foodPrice_3; }
	inline void set_foodPrice_3(Text_t356221433 * value)
	{
		___foodPrice_3 = value;
		Il2CppCodeGenWriteBarrier(&___foodPrice_3, value);
	}

	inline static int32_t get_offset_of_woodPrice_4() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___woodPrice_4)); }
	inline Text_t356221433 * get_woodPrice_4() const { return ___woodPrice_4; }
	inline Text_t356221433 ** get_address_of_woodPrice_4() { return &___woodPrice_4; }
	inline void set_woodPrice_4(Text_t356221433 * value)
	{
		___woodPrice_4 = value;
		Il2CppCodeGenWriteBarrier(&___woodPrice_4, value);
	}

	inline static int32_t get_offset_of_stonePrice_5() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___stonePrice_5)); }
	inline Text_t356221433 * get_stonePrice_5() const { return ___stonePrice_5; }
	inline Text_t356221433 ** get_address_of_stonePrice_5() { return &___stonePrice_5; }
	inline void set_stonePrice_5(Text_t356221433 * value)
	{
		___stonePrice_5 = value;
		Il2CppCodeGenWriteBarrier(&___stonePrice_5, value);
	}

	inline static int32_t get_offset_of_ironPrice_6() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___ironPrice_6)); }
	inline Text_t356221433 * get_ironPrice_6() const { return ___ironPrice_6; }
	inline Text_t356221433 ** get_address_of_ironPrice_6() { return &___ironPrice_6; }
	inline void set_ironPrice_6(Text_t356221433 * value)
	{
		___ironPrice_6 = value;
		Il2CppCodeGenWriteBarrier(&___ironPrice_6, value);
	}

	inline static int32_t get_offset_of_copperPrice_7() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___copperPrice_7)); }
	inline Text_t356221433 * get_copperPrice_7() const { return ___copperPrice_7; }
	inline Text_t356221433 ** get_address_of_copperPrice_7() { return &___copperPrice_7; }
	inline void set_copperPrice_7(Text_t356221433 * value)
	{
		___copperPrice_7 = value;
		Il2CppCodeGenWriteBarrier(&___copperPrice_7, value);
	}

	inline static int32_t get_offset_of_silverPrice_8() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___silverPrice_8)); }
	inline Text_t356221433 * get_silverPrice_8() const { return ___silverPrice_8; }
	inline Text_t356221433 ** get_address_of_silverPrice_8() { return &___silverPrice_8; }
	inline void set_silverPrice_8(Text_t356221433 * value)
	{
		___silverPrice_8 = value;
		Il2CppCodeGenWriteBarrier(&___silverPrice_8, value);
	}

	inline static int32_t get_offset_of_goldPrice_9() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___goldPrice_9)); }
	inline Text_t356221433 * get_goldPrice_9() const { return ___goldPrice_9; }
	inline Text_t356221433 ** get_address_of_goldPrice_9() { return &___goldPrice_9; }
	inline void set_goldPrice_9(Text_t356221433 * value)
	{
		___goldPrice_9 = value;
		Il2CppCodeGenWriteBarrier(&___goldPrice_9, value);
	}

	inline static int32_t get_offset_of_trainingTime_10() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___trainingTime_10)); }
	inline Text_t356221433 * get_trainingTime_10() const { return ___trainingTime_10; }
	inline Text_t356221433 ** get_address_of_trainingTime_10() { return &___trainingTime_10; }
	inline void set_trainingTime_10(Text_t356221433 * value)
	{
		___trainingTime_10 = value;
		Il2CppCodeGenWriteBarrier(&___trainingTime_10, value);
	}

	inline static int32_t get_offset_of_unitCount_11() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___unitCount_11)); }
	inline Text_t356221433 * get_unitCount_11() const { return ___unitCount_11; }
	inline Text_t356221433 ** get_address_of_unitCount_11() { return &___unitCount_11; }
	inline void set_unitCount_11(Text_t356221433 * value)
	{
		___unitCount_11 = value;
		Il2CppCodeGenWriteBarrier(&___unitCount_11, value);
	}

	inline static int32_t get_offset_of_unitName_12() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274, ___unitName_12)); }
	inline Text_t356221433 * get_unitName_12() const { return ___unitName_12; }
	inline Text_t356221433 ** get_address_of_unitName_12() { return &___unitName_12; }
	inline void set_unitName_12(Text_t356221433 * value)
	{
		___unitName_12 = value;
		Il2CppCodeGenWriteBarrier(&___unitName_12, value);
	}
};

struct TrainingIconManager_t2314554274_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> TrainingIconManager::<>f__switch$map15
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map15_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> TrainingIconManager::<>f__switch$map16
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map16_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_13() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274_StaticFields, ___U3CU3Ef__switchU24map15_13)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map15_13() const { return ___U3CU3Ef__switchU24map15_13; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map15_13() { return &___U3CU3Ef__switchU24map15_13; }
	inline void set_U3CU3Ef__switchU24map15_13(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map15_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map15_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_14() { return static_cast<int32_t>(offsetof(TrainingIconManager_t2314554274_StaticFields, ___U3CU3Ef__switchU24map16_14)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map16_14() const { return ___U3CU3Ef__switchU24map16_14; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map16_14() { return &___U3CU3Ef__switchU24map16_14; }
	inline void set_U3CU3Ef__switchU24map16_14(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map16_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map16_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
