﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// TestWorldChunksManager
struct TestWorldChunksManager_t3581849513;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapCoordsChanger
struct  WorldMapCoordsChanger_t1297297106  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField WorldMapCoordsChanger::xInput
	InputField_t1631627530 * ___xInput_2;
	// UnityEngine.UI.InputField WorldMapCoordsChanger::yInput
	InputField_t1631627530 * ___yInput_3;
	// TestWorldChunksManager WorldMapCoordsChanger::chunksManager
	TestWorldChunksManager_t3581849513 * ___chunksManager_4;

public:
	inline static int32_t get_offset_of_xInput_2() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t1297297106, ___xInput_2)); }
	inline InputField_t1631627530 * get_xInput_2() const { return ___xInput_2; }
	inline InputField_t1631627530 ** get_address_of_xInput_2() { return &___xInput_2; }
	inline void set_xInput_2(InputField_t1631627530 * value)
	{
		___xInput_2 = value;
		Il2CppCodeGenWriteBarrier(&___xInput_2, value);
	}

	inline static int32_t get_offset_of_yInput_3() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t1297297106, ___yInput_3)); }
	inline InputField_t1631627530 * get_yInput_3() const { return ___yInput_3; }
	inline InputField_t1631627530 ** get_address_of_yInput_3() { return &___yInput_3; }
	inline void set_yInput_3(InputField_t1631627530 * value)
	{
		___yInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___yInput_3, value);
	}

	inline static int32_t get_offset_of_chunksManager_4() { return static_cast<int32_t>(offsetof(WorldMapCoordsChanger_t1297297106, ___chunksManager_4)); }
	inline TestWorldChunksManager_t3581849513 * get_chunksManager_4() const { return ___chunksManager_4; }
	inline TestWorldChunksManager_t3581849513 ** get_address_of_chunksManager_4() { return &___chunksManager_4; }
	inline void set_chunksManager_4(TestWorldChunksManager_t3581849513 * value)
	{
		___chunksManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___chunksManager_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
