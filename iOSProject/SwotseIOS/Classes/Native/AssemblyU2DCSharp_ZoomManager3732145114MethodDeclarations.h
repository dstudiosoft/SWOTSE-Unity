﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZoomManager
struct ZoomManager_t3732145114;

#include "codegen/il2cpp-codegen.h"

// System.Void ZoomManager::.ctor()
extern "C"  void ZoomManager__ctor_m1553110647 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::Start()
extern "C"  void ZoomManager_Start_m760856971 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::ZoomIn()
extern "C"  void ZoomManager_ZoomIn_m1736233231 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::ZoomOut()
extern "C"  void ZoomManager_ZoomOut_m3191381924 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::MaxZoom()
extern "C"  void ZoomManager_MaxZoom_m2293936484 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::MinZoom()
extern "C"  void ZoomManager_MinZoom_m854460090 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomManager::MiddleZoom()
extern "C"  void ZoomManager_MiddleZoom_m2013475097 (ZoomManager_t3732145114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
