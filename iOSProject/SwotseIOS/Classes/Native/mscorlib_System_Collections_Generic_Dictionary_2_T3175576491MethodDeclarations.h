﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>
struct Transform_1_t3175576491;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_645770832.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1965911748_gshared (Transform_1_t3175576491 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1965911748(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3175576491 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1965911748_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t645770832  Transform_1_Invoke_m299726596_gshared (Transform_1_t3175576491 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m299726596(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t645770832  (*) (Transform_1_t3175576491 *, int32_t, TouchData_t3880599975 , const MethodInfo*))Transform_1_Invoke_m299726596_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2404709939_gshared (Transform_1_t3175576491 * __this, int32_t ___key0, TouchData_t3880599975  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2404709939(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3175576491 *, int32_t, TouchData_t3880599975 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2404709939_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData,System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t645770832  Transform_1_EndInvoke_m2490762286_gshared (Transform_1_t3175576491 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2490762286(__this, ___result0, method) ((  KeyValuePair_2_t645770832  (*) (Transform_1_t3175576491 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2490762286_gshared)(__this, ___result0, method)
