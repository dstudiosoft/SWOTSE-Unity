﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3
struct U3CGetCultureInfoU3Ec__AnonStorey3_t2238938542;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"

// System.Void SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::.ctor()
extern "C"  void U3CGetCultureInfoU3Ec__AnonStorey3__ctor_m3459584642 (U3CGetCultureInfoU3Ec__AnonStorey3_t2238938542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager/<GetCultureInfo>c__AnonStorey3::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CGetCultureInfoU3Ec__AnonStorey3_U3CU3Em__0_m4248449380 (U3CGetCultureInfoU3Ec__AnonStorey3_t2238938542 * __this, SmartCultureInfo_t2361725737 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
