﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Devices.Display.DisplayDevice
struct DisplayDevice_t1478475236;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TouchScript.Devices.Display.DisplayDevice::.ctor()
extern "C"  void DisplayDevice__ctor_m3443422018 (DisplayDevice_t1478475236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Devices.Display.DisplayDevice::get_Name()
extern "C"  String_t* DisplayDevice_get_Name_m1101307779 (DisplayDevice_t1478475236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Devices.Display.DisplayDevice::set_Name(System.String)
extern "C"  void DisplayDevice_set_Name_m3102589408 (DisplayDevice_t1478475236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Devices.Display.DisplayDevice::get_DPI()
extern "C"  float DisplayDevice_get_DPI_m634964604 (DisplayDevice_t1478475236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Devices.Display.DisplayDevice::set_DPI(System.Single)
extern "C"  void DisplayDevice_set_DPI_m1363497173 (DisplayDevice_t1478475236 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Devices.Display.DisplayDevice::OnEnable()
extern "C"  void DisplayDevice_OnEnable_m1624500346 (DisplayDevice_t1478475236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
