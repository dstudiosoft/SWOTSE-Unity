﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MailInviteLine
struct MailInviteLine_t2355843888;
// Tacticsoft.TableView
struct TableView_t3179510217;
// InviteReportModel[]
struct InviteReportModelU5BU5D_t2706836813;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInviteTableController
struct  MailInviteTableController_t1221845970  : public MonoBehaviour_t1158329972
{
public:
	// MailInviteLine MailInviteTableController::m_cellPrefab
	MailInviteLine_t2355843888 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailInviteTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// InviteReportModel[] MailInviteTableController::allInvites
	InviteReportModelU5BU5D_t2706836813* ___allInvites_4;
	// System.Boolean MailInviteTableController::mail
	bool ___mail_5;
	// System.Int64 MailInviteTableController::inviteId
	int64_t ___inviteId_6;
	// System.Int32 MailInviteTableController::m_numRows
	int32_t ___m_numRows_7;
	// ConfirmationEvent MailInviteTableController::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_8;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___m_cellPrefab_2)); }
	inline MailInviteLine_t2355843888 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailInviteLine_t2355843888 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailInviteLine_t2355843888 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_allInvites_4() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___allInvites_4)); }
	inline InviteReportModelU5BU5D_t2706836813* get_allInvites_4() const { return ___allInvites_4; }
	inline InviteReportModelU5BU5D_t2706836813** get_address_of_allInvites_4() { return &___allInvites_4; }
	inline void set_allInvites_4(InviteReportModelU5BU5D_t2706836813* value)
	{
		___allInvites_4 = value;
		Il2CppCodeGenWriteBarrier(&___allInvites_4, value);
	}

	inline static int32_t get_offset_of_mail_5() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___mail_5)); }
	inline bool get_mail_5() const { return ___mail_5; }
	inline bool* get_address_of_mail_5() { return &___mail_5; }
	inline void set_mail_5(bool value)
	{
		___mail_5 = value;
	}

	inline static int32_t get_offset_of_inviteId_6() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___inviteId_6)); }
	inline int64_t get_inviteId_6() const { return ___inviteId_6; }
	inline int64_t* get_address_of_inviteId_6() { return &___inviteId_6; }
	inline void set_inviteId_6(int64_t value)
	{
		___inviteId_6 = value;
	}

	inline static int32_t get_offset_of_m_numRows_7() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___m_numRows_7)); }
	inline int32_t get_m_numRows_7() const { return ___m_numRows_7; }
	inline int32_t* get_address_of_m_numRows_7() { return &___m_numRows_7; }
	inline void set_m_numRows_7(int32_t value)
	{
		___m_numRows_7 = value;
	}

	inline static int32_t get_offset_of_confirmed_8() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1221845970, ___confirmed_8)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_8() const { return ___confirmed_8; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_8() { return &___confirmed_8; }
	inline void set_confirmed_8(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_8 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
