﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Hosting.ActivationArguments
struct ActivationArguments_t640021366;
// System.ActivationContext
struct ActivationContext_t1572332809;
// System.ApplicationIdentity
struct ApplicationIdentity_t3292367950;

#include "codegen/il2cpp-codegen.h"

// System.ActivationContext System.Runtime.Hosting.ActivationArguments::get_ActivationContext()
extern "C"  ActivationContext_t1572332809 * ActivationArguments_get_ActivationContext_m1831446840 (ActivationArguments_t640021366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.Runtime.Hosting.ActivationArguments::get_ApplicationIdentity()
extern "C"  ApplicationIdentity_t3292367950 * ActivationArguments_get_ApplicationIdentity_m2831192792 (ActivationArguments_t640021366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
