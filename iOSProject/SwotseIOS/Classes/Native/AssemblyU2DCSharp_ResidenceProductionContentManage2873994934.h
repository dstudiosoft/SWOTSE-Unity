﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// ResidenceProductionContentManager
struct ResidenceProductionContentManager_t1872985159;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceProductionContentManager/<GetCityProduction>c__Iterator14
struct  U3CGetCityProductionU3Ec__Iterator14_t2873994934  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// JSONObject ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<obj>__1
	JSONObject_t1971882247 * ___U3CobjU3E__1_1;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__2
	int32_t ___U3CiU3E__2_2;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__3
	int32_t ___U3CiU3E__3_3;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__4
	int32_t ___U3CiU3E__4_4;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__5
	int32_t ___U3CiU3E__5_5;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__6
	int32_t ___U3CiU3E__6_6;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__7
	int32_t ___U3CiU3E__7_7;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<i>__8
	int32_t ___U3CiU3E__8_8;
	// System.Int32 ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::$PC
	int32_t ___U24PC_9;
	// System.Object ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::$current
	Il2CppObject * ___U24current_10;
	// ResidenceProductionContentManager ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::<>f__this
	ResidenceProductionContentManager_t1872985159 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CobjU3E__1_1)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__1_1() const { return ___U3CobjU3E__1_1; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__1_1() { return &___U3CobjU3E__1_1; }
	inline void set_U3CobjU3E__1_1(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__3_3)); }
	inline int32_t get_U3CiU3E__3_3() const { return ___U3CiU3E__3_3; }
	inline int32_t* get_address_of_U3CiU3E__3_3() { return &___U3CiU3E__3_3; }
	inline void set_U3CiU3E__3_3(int32_t value)
	{
		___U3CiU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__4_4() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__4_4)); }
	inline int32_t get_U3CiU3E__4_4() const { return ___U3CiU3E__4_4; }
	inline int32_t* get_address_of_U3CiU3E__4_4() { return &___U3CiU3E__4_4; }
	inline void set_U3CiU3E__4_4(int32_t value)
	{
		___U3CiU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__5_5() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__5_5)); }
	inline int32_t get_U3CiU3E__5_5() const { return ___U3CiU3E__5_5; }
	inline int32_t* get_address_of_U3CiU3E__5_5() { return &___U3CiU3E__5_5; }
	inline void set_U3CiU3E__5_5(int32_t value)
	{
		___U3CiU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__6_6() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__6_6)); }
	inline int32_t get_U3CiU3E__6_6() const { return ___U3CiU3E__6_6; }
	inline int32_t* get_address_of_U3CiU3E__6_6() { return &___U3CiU3E__6_6; }
	inline void set_U3CiU3E__6_6(int32_t value)
	{
		___U3CiU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__7_7() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__7_7)); }
	inline int32_t get_U3CiU3E__7_7() const { return ___U3CiU3E__7_7; }
	inline int32_t* get_address_of_U3CiU3E__7_7() { return &___U3CiU3E__7_7; }
	inline void set_U3CiU3E__7_7(int32_t value)
	{
		___U3CiU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__8_8() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CiU3E__8_8)); }
	inline int32_t get_U3CiU3E__8_8() const { return ___U3CiU3E__8_8; }
	inline int32_t* get_address_of_U3CiU3E__8_8() { return &___U3CiU3E__8_8; }
	inline void set_U3CiU3E__8_8(int32_t value)
	{
		___U3CiU3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CGetCityProductionU3Ec__Iterator14_t2873994934, ___U3CU3Ef__this_11)); }
	inline ResidenceProductionContentManager_t1872985159 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline ResidenceProductionContentManager_t1872985159 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(ResidenceProductionContentManager_t1872985159 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
