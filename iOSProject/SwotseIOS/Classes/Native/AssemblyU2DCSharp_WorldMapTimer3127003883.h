﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapTimer
struct  WorldMapTimer_t3127003883  : public Il2CppObject
{
public:
	// System.Int64 WorldMapTimer::id
	int64_t ___id_0;
	// System.Int64 WorldMapTimer::worldMap
	int64_t ___worldMap_1;
	// System.String WorldMapTimer::status
	String_t* ___status_2;
	// System.Int64 WorldMapTimer::event_id
	int64_t ___event_id_3;
	// System.DateTime WorldMapTimer::start_time
	DateTime_t693205669  ___start_time_4;
	// System.DateTime WorldMapTimer::finish_time
	DateTime_t693205669  ___finish_time_5;
	// System.String WorldMapTimer::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_worldMap_1() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___worldMap_1)); }
	inline int64_t get_worldMap_1() const { return ___worldMap_1; }
	inline int64_t* get_address_of_worldMap_1() { return &___worldMap_1; }
	inline void set_worldMap_1(int64_t value)
	{
		___worldMap_1 = value;
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier(&___status_2, value);
	}

	inline static int32_t get_offset_of_event_id_3() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___event_id_3)); }
	inline int64_t get_event_id_3() const { return ___event_id_3; }
	inline int64_t* get_address_of_event_id_3() { return &___event_id_3; }
	inline void set_event_id_3(int64_t value)
	{
		___event_id_3 = value;
	}

	inline static int32_t get_offset_of_start_time_4() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___start_time_4)); }
	inline DateTime_t693205669  get_start_time_4() const { return ___start_time_4; }
	inline DateTime_t693205669 * get_address_of_start_time_4() { return &___start_time_4; }
	inline void set_start_time_4(DateTime_t693205669  value)
	{
		___start_time_4 = value;
	}

	inline static int32_t get_offset_of_finish_time_5() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___finish_time_5)); }
	inline DateTime_t693205669  get_finish_time_5() const { return ___finish_time_5; }
	inline DateTime_t693205669 * get_address_of_finish_time_5() { return &___finish_time_5; }
	inline void set_finish_time_5(DateTime_t693205669  value)
	{
		___finish_time_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WorldMapTimer_t3127003883, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier(&___type_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
