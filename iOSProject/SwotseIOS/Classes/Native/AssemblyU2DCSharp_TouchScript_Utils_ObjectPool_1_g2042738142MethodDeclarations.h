﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.ObjectPool`1<System.Object>
struct ObjectPool_1_t2042738142;
// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>
struct UnityFunc_1_t1709969761;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void TouchScript.Utils.ObjectPool`1<System.Object>::.ctor(System.Int32,TouchScript.Utils.ObjectPool`1/UnityFunc`1<T,T>,UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m3649530527_gshared (ObjectPool_1_t2042738142 * __this, int32_t ___capacity0, UnityFunc_1_t1709969761 * ___actionNew1, UnityAction_1_t4056035046 * ___actionOnGet2, UnityAction_1_t4056035046 * ___actionOnRelease3, const MethodInfo* method);
#define ObjectPool_1__ctor_m3649530527(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method) ((  void (*) (ObjectPool_1_t2042738142 *, int32_t, UnityFunc_1_t1709969761 *, UnityAction_1_t4056035046 *, UnityAction_1_t4056035046 *, const MethodInfo*))ObjectPool_1__ctor_m3649530527_gshared)(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Object>::get_CountAll()
extern "C"  int32_t ObjectPool_1_get_CountAll_m1681014435_gshared (ObjectPool_1_t2042738142 * __this, const MethodInfo* method);
#define ObjectPool_1_get_CountAll_m1681014435(__this, method) ((  int32_t (*) (ObjectPool_1_t2042738142 *, const MethodInfo*))ObjectPool_1_get_CountAll_m1681014435_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Object>::set_CountAll(System.Int32)
extern "C"  void ObjectPool_1_set_CountAll_m122827726_gshared (ObjectPool_1_t2042738142 * __this, int32_t ___value0, const MethodInfo* method);
#define ObjectPool_1_set_CountAll_m122827726(__this, ___value0, method) ((  void (*) (ObjectPool_1_t2042738142 *, int32_t, const MethodInfo*))ObjectPool_1_set_CountAll_m122827726_gshared)(__this, ___value0, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Object>::get_CountActive()
extern "C"  int32_t ObjectPool_1_get_CountActive_m3063653714_gshared (ObjectPool_1_t2042738142 * __this, const MethodInfo* method);
#define ObjectPool_1_get_CountActive_m3063653714(__this, method) ((  int32_t (*) (ObjectPool_1_t2042738142 *, const MethodInfo*))ObjectPool_1_get_CountActive_m3063653714_gshared)(__this, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Object>::get_CountInactive()
extern "C"  int32_t ObjectPool_1_get_CountInactive_m1369084401_gshared (ObjectPool_1_t2042738142 * __this, const MethodInfo* method);
#define ObjectPool_1_get_CountInactive_m1369084401(__this, method) ((  int32_t (*) (ObjectPool_1_t2042738142 *, const MethodInfo*))ObjectPool_1_get_CountInactive_m1369084401_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Object>::WarmUp(System.Int32)
extern "C"  void ObjectPool_1_WarmUp_m1349419031_gshared (ObjectPool_1_t2042738142 * __this, int32_t ___count0, const MethodInfo* method);
#define ObjectPool_1_WarmUp_m1349419031(__this, ___count0, method) ((  void (*) (ObjectPool_1_t2042738142 *, int32_t, const MethodInfo*))ObjectPool_1_WarmUp_m1349419031_gshared)(__this, ___count0, method)
// T TouchScript.Utils.ObjectPool`1<System.Object>::Get()
extern "C"  Il2CppObject * ObjectPool_1_Get_m2843288903_gshared (ObjectPool_1_t2042738142 * __this, const MethodInfo* method);
#define ObjectPool_1_Get_m2843288903(__this, method) ((  Il2CppObject * (*) (ObjectPool_1_t2042738142 *, const MethodInfo*))ObjectPool_1_Get_m2843288903_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m1491729219_gshared (ObjectPool_1_t2042738142 * __this, Il2CppObject * ___element0, const MethodInfo* method);
#define ObjectPool_1_Release_m1491729219(__this, ___element0, method) ((  void (*) (ObjectPool_1_t2042738142 *, Il2CppObject *, const MethodInfo*))ObjectPool_1_Release_m1491729219_gshared)(__this, ___element0, method)
