﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.MobileInput
struct MobileInput_t1619040116;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.InputSources.MobileInput::.ctor()
extern "C"  void MobileInput__ctor_m3298913967 (MobileInput_t1619040116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MobileInput::UpdateInput()
extern "C"  void MobileInput_UpdateInput_m509885908 (MobileInput_t1619040116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MobileInput::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  void MobileInput_CancelTouch_m3361033136 (MobileInput_t1619040116 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MobileInput::OnEnable()
extern "C"  void MobileInput_OnEnable_m1516889059 (MobileInput_t1619040116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.MobileInput::OnDisable()
extern "C"  void MobileInput_OnDisable_m2697022512 (MobileInput_t1619040116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
