﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1134929557(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2573636725 *, Variant_t4275788079 *, Variant_t4275788079 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::get_Key()
#define KeyValuePair_2_get_Key_m456719710(__this, method) ((  Variant_t4275788079 * (*) (KeyValuePair_2_t2573636725 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3663844740(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2573636725 *, Variant_t4275788079 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::get_Value()
#define KeyValuePair_2_get_Value_m1589990381(__this, method) ((  Variant_t4275788079 * (*) (KeyValuePair_2_t2573636725 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2161385660(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2573636725 *, Variant_t4275788079 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>::ToString()
#define KeyValuePair_2_ToString_m464150236(__this, method) ((  String_t* (*) (KeyValuePair_2_t2573636725 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
