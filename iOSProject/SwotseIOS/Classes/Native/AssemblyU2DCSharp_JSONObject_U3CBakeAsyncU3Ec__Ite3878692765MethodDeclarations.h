﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<BakeAsync>c__Iterator1
struct U3CBakeAsyncU3Ec__Iterator1_t3878692765;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<BakeAsync>c__Iterator1::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1__ctor_m1054665036 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m330485632 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m916384712 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m2550202317 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2004861779 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<BakeAsync>c__Iterator1::MoveNext()
extern "C"  bool U3CBakeAsyncU3Ec__Iterator1_MoveNext_m414423228 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator1::Dispose()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Dispose_m528125495 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator1::Reset()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Reset_m2495922637 (U3CBakeAsyncU3Ec__Iterator1_t3878692765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
