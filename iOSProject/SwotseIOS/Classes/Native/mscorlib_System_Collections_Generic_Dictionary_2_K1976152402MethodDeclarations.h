﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1814912095(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1976152402 *, Dictionary_2_t3787621927 *, const MethodInfo*))KeyCollection__ctor_m3885543862_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m247582313(__this, ___item0, method) ((  void (*) (KeyCollection_t1976152402 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2907264390(__this, method) ((  void (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2175240043(__this, ___item0, method) ((  bool (*) (KeyCollection_t1976152402 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3597397082(__this, ___item0, method) ((  bool (*) (KeyCollection_t1976152402 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2825179240(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2995581776(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1976152402 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3197608605(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3607497872(__this, method) ((  bool (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2430603232(__this, method) ((  bool (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3397614064(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2846165398(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1976152402 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1238003947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1787985851(__this, method) ((  Enumerator_t2182158069  (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_GetEnumerator_m3687526536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,BattleReportHeader>::get_Count()
#define KeyCollection_get_Count_m2999816064(__this, method) ((  int32_t (*) (KeyCollection_t1976152402 *, const MethodInfo*))KeyCollection_get_Count_m972382317_gshared)(__this, method)
