﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>
struct KeyCollection_t1249926325;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t2679569160;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t717125112;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1455931992.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2090490505_gshared (KeyCollection_t1249926325 * __this, Dictionary_2_t3061395850 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2090490505(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1249926325 *, Dictionary_2_t3061395850 *, const MethodInfo*))KeyCollection__ctor_m2090490505_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139(__this, ___item0, method) ((  void (*) (KeyCollection_t1249926325 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820(__this, method) ((  void (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1682535721_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1682535721(__this, ___item0, method) ((  bool (*) (KeyCollection_t1249926325 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1682535721_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020(__this, ___item0, method) ((  bool (*) (KeyCollection_t1249926325 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2903263754_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2903263754(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2903263754_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3315901882_gshared (KeyCollection_t1249926325 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3315901882(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1249926325 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3315901882_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m112939771_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m112939771(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m112939771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m343543538_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m343543538(__this, method) ((  bool (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m343543538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2813120990_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2813120990(__this, method) ((  bool (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2813120990_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2833774912_gshared (KeyCollection_t1249926325 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2833774912(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1249926325 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2833774912_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1455931992  KeyCollection_GetEnumerator_m547080581_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m547080581(__this, method) ((  Enumerator_t1455931992  (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_GetEnumerator_m547080581_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1938989726_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1938989726(__this, method) ((  int32_t (*) (KeyCollection_t1249926325 *, const MethodInfo*))KeyCollection_get_Count_m1938989726_gshared)(__this, method)
