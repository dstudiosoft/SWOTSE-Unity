﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldChat
struct WorldChat_t4001456630;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void WorldChat::.ctor()
extern "C"  void WorldChat__ctor_m2706489887 (WorldChat_t4001456630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChat::Start()
extern "C"  void WorldChat_Start_m1922804667 (WorldChat_t4001456630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WorldChat::BeginChat()
extern "C"  Il2CppObject * WorldChat_BeginChat_m3570624130 (WorldChat_t4001456630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChat::OnApplicationPause(System.Boolean)
extern "C"  void WorldChat_OnApplicationPause_m3100921205 (WorldChat_t4001456630 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldChat::SendMessage()
extern "C"  void WorldChat_SendMessage_m247498540 (WorldChat_t4001456630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
