﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>
struct EventHandler_1_t4133675533;
// TouchScript.Layers.ILayerDelegate
struct ILayerDelegate_t701621129;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"

// System.Void TouchScript.Layers.TouchLayer::.ctor()
extern "C"  void TouchLayer__ctor_m3939649279 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::add_TouchBegan(System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>)
extern "C"  void TouchLayer_add_TouchBegan_m2741356030 (TouchLayer_t2635439978 * __this, EventHandler_1_t4133675533 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::remove_TouchBegan(System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>)
extern "C"  void TouchLayer_remove_TouchBegan_m3063049471 (TouchLayer_t2635439978 * __this, EventHandler_1_t4133675533 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.TouchLayer::get_WorldProjectionNormal()
extern "C"  Vector3_t2243707580  TouchLayer_get_WorldProjectionNormal_m1646139358 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ILayerDelegate TouchScript.Layers.TouchLayer::get_Delegate()
extern "C"  Il2CppObject * TouchLayer_get_Delegate_m2490005589 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::set_Delegate(TouchScript.Layers.ILayerDelegate)
extern "C"  void TouchLayer_set_Delegate_m3549913160 (TouchLayer_t2635439978 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::GetProjectionParams(TouchScript.TouchPoint)
extern "C"  ProjectionParams_t2712959773 * TouchLayer_GetProjectionParams_m2663552313 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.TouchLayer::Hit(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  int32_t TouchLayer_Hit_m2945102484 (TouchLayer_t2635439978 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::Awake()
extern "C"  void TouchLayer_Awake_m3939500198 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchScript.Layers.TouchLayer::lateAwake()
extern "C"  Il2CppObject * TouchLayer_lateAwake_m3321676826 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::Start()
extern "C"  void TouchLayer_Start_m3660526619 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::OnDestroy()
extern "C"  void TouchLayer_OnDestroy_m1262777864 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Layers.TouchLayer::INTERNAL_BeginTouch(TouchScript.TouchPoint)
extern "C"  bool TouchLayer_INTERNAL_BeginTouch_m603570398 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::INTERNAL_UpdateTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_INTERNAL_UpdateTouch_m506133740 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::INTERNAL_EndTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_INTERNAL_EndTouch_m1644208128 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::INTERNAL_CancelTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_INTERNAL_CancelTouch_m3006763085 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::setName()
extern "C"  void TouchLayer_setName_m1393877026 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.TouchLayer::beginTouch(TouchScript.TouchPoint,TouchScript.Hit.TouchHit&)
extern "C"  int32_t TouchLayer_beginTouch_m4131373998 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::updateTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_updateTouch_m2403503134 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::endTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_endTouch_m3191065634 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.TouchLayer::cancelTouch(TouchScript.TouchPoint)
extern "C"  void TouchLayer_cancelTouch_m3970823417 (TouchLayer_t2635439978 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::createProjectionParams()
extern "C"  ProjectionParams_t2712959773 * TouchLayer_createProjectionParams_m358774340 (TouchLayer_t2635439978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
