﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.LinuxIPInterfaceProperties
struct LinuxIPInterfaceProperties_t3318447793;
// System.Net.NetworkInformation.LinuxNetworkInterface
struct LinuxNetworkInterface_t3864470295;
// System.Collections.Generic.List`1<System.Net.IPAddress>
struct List_1_t769092855;
// System.Net.NetworkInformation.IPv4InterfaceProperties
struct IPv4InterfaceProperties_t1411071681;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"

// System.Void System.Net.NetworkInformation.LinuxIPInterfaceProperties::.ctor(System.Net.NetworkInformation.LinuxNetworkInterface,System.Collections.Generic.List`1<System.Net.IPAddress>)
extern "C"  void LinuxIPInterfaceProperties__ctor_m280597575 (LinuxIPInterfaceProperties_t3318447793 * __this, LinuxNetworkInterface_t3864470295 * ___iface0, List_1_t769092855 * ___addresses1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPv4InterfaceProperties System.Net.NetworkInformation.LinuxIPInterfaceProperties::GetIPv4Properties()
extern "C"  IPv4InterfaceProperties_t1411071681 * LinuxIPInterfaceProperties_GetIPv4Properties_m703170405 (LinuxIPInterfaceProperties_t3318447793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
