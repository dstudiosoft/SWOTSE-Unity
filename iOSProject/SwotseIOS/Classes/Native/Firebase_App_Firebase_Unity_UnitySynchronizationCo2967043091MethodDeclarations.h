﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3
struct U3CSendU3Ec__AnonStorey3_t2967043091;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey3__ctor_m921131280 (U3CSendU3Ec__AnonStorey3_t2967043091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
