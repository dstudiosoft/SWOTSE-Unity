﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ValleyInfoManager
struct ValleyInfoManager_t1355091478;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ValleyInfoManager::.ctor()
extern "C"  void ValleyInfoManager__ctor_m518475233 (ValleyInfoManager_t1355091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValleyInfoManager::OnEnable()
extern "C"  void ValleyInfoManager_OnEnable_m915015857 (ValleyInfoManager_t1355091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValleyInfoManager::March()
extern "C"  void ValleyInfoManager_March_m414486378 (ValleyInfoManager_t1355091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValleyInfoManager::BuildCity()
extern "C"  void ValleyInfoManager_BuildCity_m2032562518 (ValleyInfoManager_t1355091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValleyInfoManager::Recruit()
extern "C"  void ValleyInfoManager_Recruit_m1586357787 (ValleyInfoManager_t1355091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValleyInfoManager::CityBuiltHandler(System.Object,System.String)
extern "C"  void ValleyInfoManager_CityBuiltHandler_m1667310590 (ValleyInfoManager_t1355091478 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
