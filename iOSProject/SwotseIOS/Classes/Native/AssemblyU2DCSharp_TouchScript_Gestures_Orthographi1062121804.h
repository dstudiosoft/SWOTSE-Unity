﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TestWorldChunksManager
struct TestWorldChunksManager_t3581849513;

#include "AssemblyU2DCSharp_TouchScript_Gestures_Base_Transfor91965108.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.OrthographicCameraTransformGesture
struct  OrthographicCameraTransformGesture_t1062121804  : public TransformGestureBase_t91965108
{
public:
	// System.Boolean TouchScript.Gestures.OrthographicCameraTransformGesture::worldMap
	bool ___worldMap_54;
	// UnityEngine.Material TouchScript.Gestures.OrthographicCameraTransformGesture::mapBackground
	Material_t193706927 * ___mapBackground_55;
	// UnityEngine.GameObject TouchScript.Gestures.OrthographicCameraTransformGesture::mapGameWorld
	GameObject_t1756533147 * ___mapGameWorld_56;
	// TestWorldChunksManager TouchScript.Gestures.OrthographicCameraTransformGesture::chuncksManager
	TestWorldChunksManager_t3581849513 * ___chuncksManager_57;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::leftBottom
	Vector2_t2243707579  ___leftBottom_58;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::rightTop
	Vector2_t2243707579  ___rightTop_59;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::actualLeftBottom
	Vector2_t2243707579  ___actualLeftBottom_60;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::actualRightTop
	Vector2_t2243707579  ___actualRightTop_61;

public:
	inline static int32_t get_offset_of_worldMap_54() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___worldMap_54)); }
	inline bool get_worldMap_54() const { return ___worldMap_54; }
	inline bool* get_address_of_worldMap_54() { return &___worldMap_54; }
	inline void set_worldMap_54(bool value)
	{
		___worldMap_54 = value;
	}

	inline static int32_t get_offset_of_mapBackground_55() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___mapBackground_55)); }
	inline Material_t193706927 * get_mapBackground_55() const { return ___mapBackground_55; }
	inline Material_t193706927 ** get_address_of_mapBackground_55() { return &___mapBackground_55; }
	inline void set_mapBackground_55(Material_t193706927 * value)
	{
		___mapBackground_55 = value;
		Il2CppCodeGenWriteBarrier(&___mapBackground_55, value);
	}

	inline static int32_t get_offset_of_mapGameWorld_56() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___mapGameWorld_56)); }
	inline GameObject_t1756533147 * get_mapGameWorld_56() const { return ___mapGameWorld_56; }
	inline GameObject_t1756533147 ** get_address_of_mapGameWorld_56() { return &___mapGameWorld_56; }
	inline void set_mapGameWorld_56(GameObject_t1756533147 * value)
	{
		___mapGameWorld_56 = value;
		Il2CppCodeGenWriteBarrier(&___mapGameWorld_56, value);
	}

	inline static int32_t get_offset_of_chuncksManager_57() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___chuncksManager_57)); }
	inline TestWorldChunksManager_t3581849513 * get_chuncksManager_57() const { return ___chuncksManager_57; }
	inline TestWorldChunksManager_t3581849513 ** get_address_of_chuncksManager_57() { return &___chuncksManager_57; }
	inline void set_chuncksManager_57(TestWorldChunksManager_t3581849513 * value)
	{
		___chuncksManager_57 = value;
		Il2CppCodeGenWriteBarrier(&___chuncksManager_57, value);
	}

	inline static int32_t get_offset_of_leftBottom_58() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___leftBottom_58)); }
	inline Vector2_t2243707579  get_leftBottom_58() const { return ___leftBottom_58; }
	inline Vector2_t2243707579 * get_address_of_leftBottom_58() { return &___leftBottom_58; }
	inline void set_leftBottom_58(Vector2_t2243707579  value)
	{
		___leftBottom_58 = value;
	}

	inline static int32_t get_offset_of_rightTop_59() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___rightTop_59)); }
	inline Vector2_t2243707579  get_rightTop_59() const { return ___rightTop_59; }
	inline Vector2_t2243707579 * get_address_of_rightTop_59() { return &___rightTop_59; }
	inline void set_rightTop_59(Vector2_t2243707579  value)
	{
		___rightTop_59 = value;
	}

	inline static int32_t get_offset_of_actualLeftBottom_60() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___actualLeftBottom_60)); }
	inline Vector2_t2243707579  get_actualLeftBottom_60() const { return ___actualLeftBottom_60; }
	inline Vector2_t2243707579 * get_address_of_actualLeftBottom_60() { return &___actualLeftBottom_60; }
	inline void set_actualLeftBottom_60(Vector2_t2243707579  value)
	{
		___actualLeftBottom_60 = value;
	}

	inline static int32_t get_offset_of_actualRightTop_61() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1062121804, ___actualRightTop_61)); }
	inline Vector2_t2243707579  get_actualRightTop_61() const { return ___actualRightTop_61; }
	inline Vector2_t2243707579 * get_address_of_actualRightTop_61() { return &___actualRightTop_61; }
	inline void set_actualRightTop_61(Vector2_t2243707579  value)
	{
		___actualRightTop_61 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
