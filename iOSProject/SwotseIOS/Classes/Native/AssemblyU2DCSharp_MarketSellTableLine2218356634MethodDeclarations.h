﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketSellTableLine
struct MarketSellTableLine_t2218356634;
// MarketManager
struct MarketManager_t2047363881;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MarketManager2047363881.h"

// System.Void MarketSellTableLine::.ctor()
extern "C"  void MarketSellTableLine__ctor_m1894606917 (MarketSellTableLine_t2218356634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableLine::SetId(System.Int64)
extern "C"  void MarketSellTableLine_SetId_m1667257572 (MarketSellTableLine_t2218356634 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableLine::SetOwner(MarketManager)
extern "C"  void MarketSellTableLine_SetOwner_m2017640865 (MarketSellTableLine_t2218356634 * __this, MarketManager_t2047363881 * ___manager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableLine::SetQuantity(System.Int64)
extern "C"  void MarketSellTableLine_SetQuantity_m3209310472 (MarketSellTableLine_t2218356634 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableLine::SetUnitPrice(System.Int64)
extern "C"  void MarketSellTableLine_SetUnitPrice_m2075865018 (MarketSellTableLine_t2218356634 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableLine::RetractLot()
extern "C"  void MarketSellTableLine_RetractLot_m2803385613 (MarketSellTableLine_t2218356634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
