﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m2914856732(__this, ___object0, ___method1, method) ((  void (*) (UnityAction_1_t1260371689 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2836997866_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Events.UnityAction`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::Invoke(T0)
#define UnityAction_1_Invoke_m1675585957(__this, ___arg00, method) ((  void (*) (UnityAction_1_t1260371689 *, TouchProxyBase_t4188753234 *, const MethodInfo*))UnityAction_1_Invoke_m1279804060_gshared)(__this, ___arg00, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m2029265042(__this, ___arg00, ___callback1, ___object2, method) ((  Il2CppObject * (*) (UnityAction_1_t1260371689 *, TouchProxyBase_t4188753234 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityAction_1_BeginInvoke_m3462722079_gshared)(__this, ___arg00, ___callback1, ___object2, method)
// System.Void UnityEngine.Events.UnityAction`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m3598986755(__this, ___result0, method) ((  void (*) (UnityAction_1_t1260371689 *, Il2CppObject *, const MethodInfo*))UnityAction_1_EndInvoke_m2822290096_gshared)(__this, ___result0, method)
