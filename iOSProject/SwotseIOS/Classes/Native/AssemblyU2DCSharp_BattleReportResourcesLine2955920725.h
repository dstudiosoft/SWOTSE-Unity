﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportResourcesLine
struct  BattleReportResourcesLine_t2955920725  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text BattleReportResourcesLine::gold
	Text_t356221433 * ___gold_2;
	// UnityEngine.UI.Text BattleReportResourcesLine::silver
	Text_t356221433 * ___silver_3;
	// UnityEngine.UI.Text BattleReportResourcesLine::food
	Text_t356221433 * ___food_4;
	// UnityEngine.UI.Text BattleReportResourcesLine::wood
	Text_t356221433 * ___wood_5;
	// UnityEngine.UI.Text BattleReportResourcesLine::iron
	Text_t356221433 * ___iron_6;
	// UnityEngine.UI.Text BattleReportResourcesLine::copper
	Text_t356221433 * ___copper_7;
	// UnityEngine.UI.Text BattleReportResourcesLine::stone
	Text_t356221433 * ___stone_8;

public:
	inline static int32_t get_offset_of_gold_2() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___gold_2)); }
	inline Text_t356221433 * get_gold_2() const { return ___gold_2; }
	inline Text_t356221433 ** get_address_of_gold_2() { return &___gold_2; }
	inline void set_gold_2(Text_t356221433 * value)
	{
		___gold_2 = value;
		Il2CppCodeGenWriteBarrier(&___gold_2, value);
	}

	inline static int32_t get_offset_of_silver_3() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___silver_3)); }
	inline Text_t356221433 * get_silver_3() const { return ___silver_3; }
	inline Text_t356221433 ** get_address_of_silver_3() { return &___silver_3; }
	inline void set_silver_3(Text_t356221433 * value)
	{
		___silver_3 = value;
		Il2CppCodeGenWriteBarrier(&___silver_3, value);
	}

	inline static int32_t get_offset_of_food_4() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___food_4)); }
	inline Text_t356221433 * get_food_4() const { return ___food_4; }
	inline Text_t356221433 ** get_address_of_food_4() { return &___food_4; }
	inline void set_food_4(Text_t356221433 * value)
	{
		___food_4 = value;
		Il2CppCodeGenWriteBarrier(&___food_4, value);
	}

	inline static int32_t get_offset_of_wood_5() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___wood_5)); }
	inline Text_t356221433 * get_wood_5() const { return ___wood_5; }
	inline Text_t356221433 ** get_address_of_wood_5() { return &___wood_5; }
	inline void set_wood_5(Text_t356221433 * value)
	{
		___wood_5 = value;
		Il2CppCodeGenWriteBarrier(&___wood_5, value);
	}

	inline static int32_t get_offset_of_iron_6() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___iron_6)); }
	inline Text_t356221433 * get_iron_6() const { return ___iron_6; }
	inline Text_t356221433 ** get_address_of_iron_6() { return &___iron_6; }
	inline void set_iron_6(Text_t356221433 * value)
	{
		___iron_6 = value;
		Il2CppCodeGenWriteBarrier(&___iron_6, value);
	}

	inline static int32_t get_offset_of_copper_7() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___copper_7)); }
	inline Text_t356221433 * get_copper_7() const { return ___copper_7; }
	inline Text_t356221433 ** get_address_of_copper_7() { return &___copper_7; }
	inline void set_copper_7(Text_t356221433 * value)
	{
		___copper_7 = value;
		Il2CppCodeGenWriteBarrier(&___copper_7, value);
	}

	inline static int32_t get_offset_of_stone_8() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t2955920725, ___stone_8)); }
	inline Text_t356221433 * get_stone_8() const { return ___stone_8; }
	inline Text_t356221433 ** get_address_of_stone_8() { return &___stone_8; }
	inline void set_stone_8(Text_t356221433 * value)
	{
		___stone_8 = value;
		Il2CppCodeGenWriteBarrier(&___stone_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
