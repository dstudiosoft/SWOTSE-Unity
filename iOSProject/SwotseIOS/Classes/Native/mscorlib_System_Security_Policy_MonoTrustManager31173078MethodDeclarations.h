﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.MonoTrustManager
struct MonoTrustManager_t31173078;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t3968282840;
// System.ActivationContext
struct ActivationContext_t1572332809;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t3342600394;
// System.Security.SecurityElement
struct SecurityElement_t2325568386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ActivationContext1572332809.h"
#include "mscorlib_System_Security_Policy_TrustManagerContex3342600394.h"
#include "mscorlib_System_Security_SecurityElement2325568386.h"

// System.Void System.Security.Policy.MonoTrustManager::.ctor()
extern "C"  void MonoTrustManager__ctor_m3307791225 (MonoTrustManager_t31173078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.Policy.MonoTrustManager::DetermineApplicationTrust(System.ActivationContext,System.Security.Policy.TrustManagerContext)
extern "C"  ApplicationTrust_t3968282840 * MonoTrustManager_DetermineApplicationTrust_m3756125803 (MonoTrustManager_t31173078 * __this, ActivationContext_t1572332809 * ___activationContext0, TrustManagerContext_t3342600394 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.MonoTrustManager::FromXml(System.Security.SecurityElement)
extern "C"  void MonoTrustManager_FromXml_m1216478089 (MonoTrustManager_t31173078 * __this, SecurityElement_t2325568386 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.MonoTrustManager::ToXml()
extern "C"  SecurityElement_t2325568386 * MonoTrustManager_ToXml_m2256271619 (MonoTrustManager_t31173078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
