﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportHeader
struct  BattleReportHeader_t2798103525  : public Il2CppObject
{
public:
	// System.Int64 BattleReportHeader::id
	int64_t ___id_0;
	// System.String BattleReportHeader::attackUserName
	String_t* ___attackUserName_1;
	// System.String BattleReportHeader::defenceUserName
	String_t* ___defenceUserName_2;
	// System.Boolean BattleReportHeader::isSuccessAttack
	bool ___isSuccessAttack_3;
	// System.DateTime BattleReportHeader::creation_date
	DateTime_t693205669  ___creation_date_4;
	// System.Boolean BattleReportHeader::isReadByAttacker
	bool ___isReadByAttacker_5;
	// System.Boolean BattleReportHeader::isReadByDefender
	bool ___isReadByDefender_6;
	// System.String BattleReportHeader::attack_type
	String_t* ___attack_type_7;
	// System.String BattleReportHeader::defenceCityName
	String_t* ___defenceCityName_8;
	// System.String BattleReportHeader::attackCityName
	String_t* ___attackCityName_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_attackUserName_1() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___attackUserName_1)); }
	inline String_t* get_attackUserName_1() const { return ___attackUserName_1; }
	inline String_t** get_address_of_attackUserName_1() { return &___attackUserName_1; }
	inline void set_attackUserName_1(String_t* value)
	{
		___attackUserName_1 = value;
		Il2CppCodeGenWriteBarrier(&___attackUserName_1, value);
	}

	inline static int32_t get_offset_of_defenceUserName_2() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___defenceUserName_2)); }
	inline String_t* get_defenceUserName_2() const { return ___defenceUserName_2; }
	inline String_t** get_address_of_defenceUserName_2() { return &___defenceUserName_2; }
	inline void set_defenceUserName_2(String_t* value)
	{
		___defenceUserName_2 = value;
		Il2CppCodeGenWriteBarrier(&___defenceUserName_2, value);
	}

	inline static int32_t get_offset_of_isSuccessAttack_3() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___isSuccessAttack_3)); }
	inline bool get_isSuccessAttack_3() const { return ___isSuccessAttack_3; }
	inline bool* get_address_of_isSuccessAttack_3() { return &___isSuccessAttack_3; }
	inline void set_isSuccessAttack_3(bool value)
	{
		___isSuccessAttack_3 = value;
	}

	inline static int32_t get_offset_of_creation_date_4() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___creation_date_4)); }
	inline DateTime_t693205669  get_creation_date_4() const { return ___creation_date_4; }
	inline DateTime_t693205669 * get_address_of_creation_date_4() { return &___creation_date_4; }
	inline void set_creation_date_4(DateTime_t693205669  value)
	{
		___creation_date_4 = value;
	}

	inline static int32_t get_offset_of_isReadByAttacker_5() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___isReadByAttacker_5)); }
	inline bool get_isReadByAttacker_5() const { return ___isReadByAttacker_5; }
	inline bool* get_address_of_isReadByAttacker_5() { return &___isReadByAttacker_5; }
	inline void set_isReadByAttacker_5(bool value)
	{
		___isReadByAttacker_5 = value;
	}

	inline static int32_t get_offset_of_isReadByDefender_6() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___isReadByDefender_6)); }
	inline bool get_isReadByDefender_6() const { return ___isReadByDefender_6; }
	inline bool* get_address_of_isReadByDefender_6() { return &___isReadByDefender_6; }
	inline void set_isReadByDefender_6(bool value)
	{
		___isReadByDefender_6 = value;
	}

	inline static int32_t get_offset_of_attack_type_7() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___attack_type_7)); }
	inline String_t* get_attack_type_7() const { return ___attack_type_7; }
	inline String_t** get_address_of_attack_type_7() { return &___attack_type_7; }
	inline void set_attack_type_7(String_t* value)
	{
		___attack_type_7 = value;
		Il2CppCodeGenWriteBarrier(&___attack_type_7, value);
	}

	inline static int32_t get_offset_of_defenceCityName_8() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___defenceCityName_8)); }
	inline String_t* get_defenceCityName_8() const { return ___defenceCityName_8; }
	inline String_t** get_address_of_defenceCityName_8() { return &___defenceCityName_8; }
	inline void set_defenceCityName_8(String_t* value)
	{
		___defenceCityName_8 = value;
		Il2CppCodeGenWriteBarrier(&___defenceCityName_8, value);
	}

	inline static int32_t get_offset_of_attackCityName_9() { return static_cast<int32_t>(offsetof(BattleReportHeader_t2798103525, ___attackCityName_9)); }
	inline String_t* get_attackCityName_9() const { return ___attackCityName_9; }
	inline String_t** get_address_of_attackCityName_9() { return &___attackCityName_9; }
	inline void set_attackCityName_9(String_t* value)
	{
		___attackCityName_9 = value;
		Il2CppCodeGenWriteBarrier(&___attackCityName_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
