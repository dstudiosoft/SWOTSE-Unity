﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Thread
struct Thread_t241561612;
// System.Object
struct Il2CppObject;
// System.Threading.AsyncFlowControl
struct AsyncFlowControl_t3236298431;
struct AsyncFlowControl_t3236298431_marshaled_pinvoke;
struct AsyncFlowControl_t3236298431_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Threading_AsyncFlowControl3236298431.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_Threading_AsyncFlowControlType792122991.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Threading.AsyncFlowControl::.ctor(System.Threading.Thread,System.Threading.AsyncFlowControlType)
extern "C"  void AsyncFlowControl__ctor_m2672180221 (AsyncFlowControl_t3236298431 * __this, Thread_t241561612 * ___t0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AsyncFlowControl::System.IDisposable.Dispose()
extern "C"  void AsyncFlowControl_System_IDisposable_Dispose_m3452278871 (AsyncFlowControl_t3236298431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AsyncFlowControl::Undo()
extern "C"  void AsyncFlowControl_Undo_m3654518612 (AsyncFlowControl_t3236298431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.AsyncFlowControl::GetHashCode()
extern "C"  int32_t AsyncFlowControl_GetHashCode_m1642366879 (AsyncFlowControl_t3236298431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.AsyncFlowControl::Equals(System.Object)
extern "C"  bool AsyncFlowControl_Equals_m226957217 (AsyncFlowControl_t3236298431 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct AsyncFlowControl_t3236298431;
struct AsyncFlowControl_t3236298431_marshaled_pinvoke;

extern "C" void AsyncFlowControl_t3236298431_marshal_pinvoke(const AsyncFlowControl_t3236298431& unmarshaled, AsyncFlowControl_t3236298431_marshaled_pinvoke& marshaled);
extern "C" void AsyncFlowControl_t3236298431_marshal_pinvoke_back(const AsyncFlowControl_t3236298431_marshaled_pinvoke& marshaled, AsyncFlowControl_t3236298431& unmarshaled);
extern "C" void AsyncFlowControl_t3236298431_marshal_pinvoke_cleanup(AsyncFlowControl_t3236298431_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AsyncFlowControl_t3236298431;
struct AsyncFlowControl_t3236298431_marshaled_com;

extern "C" void AsyncFlowControl_t3236298431_marshal_com(const AsyncFlowControl_t3236298431& unmarshaled, AsyncFlowControl_t3236298431_marshaled_com& marshaled);
extern "C" void AsyncFlowControl_t3236298431_marshal_com_back(const AsyncFlowControl_t3236298431_marshaled_com& marshaled, AsyncFlowControl_t3236298431& unmarshaled);
extern "C" void AsyncFlowControl_t3236298431_marshal_com_cleanup(AsyncFlowControl_t3236298431_marshaled_com& marshaled);
