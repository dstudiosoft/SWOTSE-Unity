﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ResidenceValleysTableController
struct ResidenceValleysTableController_t2038149672;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceValleysTableLine
struct  ResidenceValleysTableLine_t3572173254  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyName
	Text_t356221433 * ___valleyName_2;
	// UnityEngine.UI.Text ResidenceValleysTableLine::resource
	Text_t356221433 * ___resource_3;
	// UnityEngine.UI.Text ResidenceValleysTableLine::region
	Text_t356221433 * ___region_4;
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyCoords
	Text_t356221433 * ___valleyCoords_5;
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyLevel
	Text_t356221433 * ___valleyLevel_6;
	// UnityEngine.UI.Text ResidenceValleysTableLine::production
	Text_t356221433 * ___production_7;
	// ResidenceValleysTableController ResidenceValleysTableLine::owner
	ResidenceValleysTableController_t2038149672 * ___owner_8;

public:
	inline static int32_t get_offset_of_valleyName_2() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___valleyName_2)); }
	inline Text_t356221433 * get_valleyName_2() const { return ___valleyName_2; }
	inline Text_t356221433 ** get_address_of_valleyName_2() { return &___valleyName_2; }
	inline void set_valleyName_2(Text_t356221433 * value)
	{
		___valleyName_2 = value;
		Il2CppCodeGenWriteBarrier(&___valleyName_2, value);
	}

	inline static int32_t get_offset_of_resource_3() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___resource_3)); }
	inline Text_t356221433 * get_resource_3() const { return ___resource_3; }
	inline Text_t356221433 ** get_address_of_resource_3() { return &___resource_3; }
	inline void set_resource_3(Text_t356221433 * value)
	{
		___resource_3 = value;
		Il2CppCodeGenWriteBarrier(&___resource_3, value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___region_4)); }
	inline Text_t356221433 * get_region_4() const { return ___region_4; }
	inline Text_t356221433 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t356221433 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier(&___region_4, value);
	}

	inline static int32_t get_offset_of_valleyCoords_5() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___valleyCoords_5)); }
	inline Text_t356221433 * get_valleyCoords_5() const { return ___valleyCoords_5; }
	inline Text_t356221433 ** get_address_of_valleyCoords_5() { return &___valleyCoords_5; }
	inline void set_valleyCoords_5(Text_t356221433 * value)
	{
		___valleyCoords_5 = value;
		Il2CppCodeGenWriteBarrier(&___valleyCoords_5, value);
	}

	inline static int32_t get_offset_of_valleyLevel_6() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___valleyLevel_6)); }
	inline Text_t356221433 * get_valleyLevel_6() const { return ___valleyLevel_6; }
	inline Text_t356221433 ** get_address_of_valleyLevel_6() { return &___valleyLevel_6; }
	inline void set_valleyLevel_6(Text_t356221433 * value)
	{
		___valleyLevel_6 = value;
		Il2CppCodeGenWriteBarrier(&___valleyLevel_6, value);
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___production_7)); }
	inline Text_t356221433 * get_production_7() const { return ___production_7; }
	inline Text_t356221433 ** get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(Text_t356221433 * value)
	{
		___production_7 = value;
		Il2CppCodeGenWriteBarrier(&___production_7, value);
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t3572173254, ___owner_8)); }
	inline ResidenceValleysTableController_t2038149672 * get_owner_8() const { return ___owner_8; }
	inline ResidenceValleysTableController_t2038149672 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(ResidenceValleysTableController_t2038149672 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier(&___owner_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
