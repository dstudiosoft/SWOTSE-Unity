﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedGUITexture
struct  LocalizedGUITexture_t2839782577  : public MonoBehaviour_t1158329972
{
public:
	// System.String SmartLocalization.LocalizedGUITexture::localizedKey
	String_t* ___localizedKey_2;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedGUITexture_t2839782577, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___localizedKey_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
