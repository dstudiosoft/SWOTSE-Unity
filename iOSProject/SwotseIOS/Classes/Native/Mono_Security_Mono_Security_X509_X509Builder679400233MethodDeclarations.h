﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Builder
struct X509Builder_t679400233;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// Mono.Security.ASN1
struct ASN1_t924533536;
// System.Security.Cryptography.RSA
struct RSA_t3719518354;
// System.Security.Cryptography.DSA
struct DSA_t903174880;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "Mono_Security_Mono_Security_ASN1924533535.h"
#include "mscorlib_System_Security_Cryptography_RSA3719518354.h"
#include "mscorlib_System_Security_Cryptography_DSA903174880.h"

// System.Void Mono.Security.X509.X509Builder::.ctor()
extern "C"  void X509Builder__ctor_m3720494854 (X509Builder_t679400233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Builder::GetOid(System.String)
extern "C"  String_t* X509Builder_GetOid_m3141282173 (X509Builder_t679400233 * __this, String_t* ___hashName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Builder::get_Hash()
extern "C"  String_t* X509Builder_get_Hash_m3767497072 (X509Builder_t679400233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Builder::set_Hash(System.String)
extern "C"  void X509Builder_set_Hash_m3492420027 (X509Builder_t679400233 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* X509Builder_Sign_m1254677827 (X509Builder_t679400233 * __this, AsymmetricAlgorithm_t784058677 * ___aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Build(Mono.Security.ASN1,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* X509Builder_Build_m2742557141 (X509Builder_t679400233 * __this, ASN1_t924533536 * ___tbs0, String_t* ___hashoid1, ByteU5BU5D_t3397334013* ___signature2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.RSA)
extern "C"  ByteU5BU5D_t3397334013* X509Builder_Sign_m1047345738 (X509Builder_t679400233 * __this, RSA_t3719518354 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.DSA)
extern "C"  ByteU5BU5D_t3397334013* X509Builder_Sign_m1047346204 (X509Builder_t679400233 * __this, DSA_t903174880 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
