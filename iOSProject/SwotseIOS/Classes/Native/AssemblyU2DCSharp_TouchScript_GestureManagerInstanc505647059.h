﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t505647059;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1721427117;
// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>>
struct Action_2_t2136039064;
// System.Action`1<UnityEngine.Transform>
struct Action_1_t3076917440;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t70405120;
// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t673677165;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>
struct ObjectPool_1_t1074715964;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct ObjectPool_1_t3977006358;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>>
struct ObjectPool_1_t1997528037;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t4252506175;
// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>
struct UnityFunc_1_t1559192105;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>
struct UnityAction_1_t3088012868;
// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct UnityFunc_1_t2053600641;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct UnityAction_1_t1695335966;
// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<UnityEngine.Transform>,System.Collections.Generic.List`1<UnityEngine.Transform>>
struct UnityFunc_1_t1878732133;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Transform>>
struct UnityAction_1_t4010824941;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.GestureManagerInstance
struct  GestureManagerInstance_t505647059  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::gesturesToReset
	List_1_t1721427117 * ___gesturesToReset_4;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateBegan
	Action_2_t2136039064 * ____updateBegan_5;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateMoved
	Action_2_t2136039064 * ____updateMoved_6;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateEnded
	Action_2_t2136039064 * ____updateEnded_7;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateCancelled
	Action_2_t2136039064 * ____updateCancelled_8;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTarget
	Action_1_t3076917440 * ____processTarget_9;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTargetBegan
	Action_1_t3076917440 * ____processTargetBegan_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::targetTouches
	Dictionary_2_t70405120 * ___targetTouches_11;
	// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::gestureTouches
	Dictionary_2_t673677165 * ___gestureTouches_12;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::activeGestures
	List_1_t1721427117 * ___activeGestures_13;
	// TouchScript.IGestureDelegate TouchScript.GestureManagerInstance::<GlobalGestureDelegate>k__BackingField
	Il2CppObject * ___U3CGlobalGestureDelegateU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_gesturesToReset_4() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___gesturesToReset_4)); }
	inline List_1_t1721427117 * get_gesturesToReset_4() const { return ___gesturesToReset_4; }
	inline List_1_t1721427117 ** get_address_of_gesturesToReset_4() { return &___gesturesToReset_4; }
	inline void set_gesturesToReset_4(List_1_t1721427117 * value)
	{
		___gesturesToReset_4 = value;
		Il2CppCodeGenWriteBarrier(&___gesturesToReset_4, value);
	}

	inline static int32_t get_offset_of__updateBegan_5() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateBegan_5)); }
	inline Action_2_t2136039064 * get__updateBegan_5() const { return ____updateBegan_5; }
	inline Action_2_t2136039064 ** get_address_of__updateBegan_5() { return &____updateBegan_5; }
	inline void set__updateBegan_5(Action_2_t2136039064 * value)
	{
		____updateBegan_5 = value;
		Il2CppCodeGenWriteBarrier(&____updateBegan_5, value);
	}

	inline static int32_t get_offset_of__updateMoved_6() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateMoved_6)); }
	inline Action_2_t2136039064 * get__updateMoved_6() const { return ____updateMoved_6; }
	inline Action_2_t2136039064 ** get_address_of__updateMoved_6() { return &____updateMoved_6; }
	inline void set__updateMoved_6(Action_2_t2136039064 * value)
	{
		____updateMoved_6 = value;
		Il2CppCodeGenWriteBarrier(&____updateMoved_6, value);
	}

	inline static int32_t get_offset_of__updateEnded_7() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateEnded_7)); }
	inline Action_2_t2136039064 * get__updateEnded_7() const { return ____updateEnded_7; }
	inline Action_2_t2136039064 ** get_address_of__updateEnded_7() { return &____updateEnded_7; }
	inline void set__updateEnded_7(Action_2_t2136039064 * value)
	{
		____updateEnded_7 = value;
		Il2CppCodeGenWriteBarrier(&____updateEnded_7, value);
	}

	inline static int32_t get_offset_of__updateCancelled_8() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateCancelled_8)); }
	inline Action_2_t2136039064 * get__updateCancelled_8() const { return ____updateCancelled_8; }
	inline Action_2_t2136039064 ** get_address_of__updateCancelled_8() { return &____updateCancelled_8; }
	inline void set__updateCancelled_8(Action_2_t2136039064 * value)
	{
		____updateCancelled_8 = value;
		Il2CppCodeGenWriteBarrier(&____updateCancelled_8, value);
	}

	inline static int32_t get_offset_of__processTarget_9() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____processTarget_9)); }
	inline Action_1_t3076917440 * get__processTarget_9() const { return ____processTarget_9; }
	inline Action_1_t3076917440 ** get_address_of__processTarget_9() { return &____processTarget_9; }
	inline void set__processTarget_9(Action_1_t3076917440 * value)
	{
		____processTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&____processTarget_9, value);
	}

	inline static int32_t get_offset_of__processTargetBegan_10() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____processTargetBegan_10)); }
	inline Action_1_t3076917440 * get__processTargetBegan_10() const { return ____processTargetBegan_10; }
	inline Action_1_t3076917440 ** get_address_of__processTargetBegan_10() { return &____processTargetBegan_10; }
	inline void set__processTargetBegan_10(Action_1_t3076917440 * value)
	{
		____processTargetBegan_10 = value;
		Il2CppCodeGenWriteBarrier(&____processTargetBegan_10, value);
	}

	inline static int32_t get_offset_of_targetTouches_11() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___targetTouches_11)); }
	inline Dictionary_2_t70405120 * get_targetTouches_11() const { return ___targetTouches_11; }
	inline Dictionary_2_t70405120 ** get_address_of_targetTouches_11() { return &___targetTouches_11; }
	inline void set_targetTouches_11(Dictionary_2_t70405120 * value)
	{
		___targetTouches_11 = value;
		Il2CppCodeGenWriteBarrier(&___targetTouches_11, value);
	}

	inline static int32_t get_offset_of_gestureTouches_12() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___gestureTouches_12)); }
	inline Dictionary_2_t673677165 * get_gestureTouches_12() const { return ___gestureTouches_12; }
	inline Dictionary_2_t673677165 ** get_address_of_gestureTouches_12() { return &___gestureTouches_12; }
	inline void set_gestureTouches_12(Dictionary_2_t673677165 * value)
	{
		___gestureTouches_12 = value;
		Il2CppCodeGenWriteBarrier(&___gestureTouches_12, value);
	}

	inline static int32_t get_offset_of_activeGestures_13() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___activeGestures_13)); }
	inline List_1_t1721427117 * get_activeGestures_13() const { return ___activeGestures_13; }
	inline List_1_t1721427117 ** get_address_of_activeGestures_13() { return &___activeGestures_13; }
	inline void set_activeGestures_13(List_1_t1721427117 * value)
	{
		___activeGestures_13 = value;
		Il2CppCodeGenWriteBarrier(&___activeGestures_13, value);
	}

	inline static int32_t get_offset_of_U3CGlobalGestureDelegateU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___U3CGlobalGestureDelegateU3Ek__BackingField_17)); }
	inline Il2CppObject * get_U3CGlobalGestureDelegateU3Ek__BackingField_17() const { return ___U3CGlobalGestureDelegateU3Ek__BackingField_17; }
	inline Il2CppObject ** get_address_of_U3CGlobalGestureDelegateU3Ek__BackingField_17() { return &___U3CGlobalGestureDelegateU3Ek__BackingField_17; }
	inline void set_U3CGlobalGestureDelegateU3Ek__BackingField_17(Il2CppObject * value)
	{
		___U3CGlobalGestureDelegateU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGlobalGestureDelegateU3Ek__BackingField_17, value);
	}
};

struct GestureManagerInstance_t505647059_StaticFields
{
public:
	// TouchScript.GestureManagerInstance TouchScript.GestureManagerInstance::instance
	GestureManagerInstance_t505647059 * ___instance_2;
	// System.Boolean TouchScript.GestureManagerInstance::shuttingDown
	bool ___shuttingDown_3;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>> TouchScript.GestureManagerInstance::gestureListPool
	ObjectPool_1_t1074715964 * ___gestureListPool_14;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::touchListPool
	ObjectPool_1_t3977006358 * ___touchListPool_15;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>> TouchScript.GestureManagerInstance::transformListPool
	ObjectPool_1_t1997528037 * ___transformListPool_16;
	// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>> TouchScript.GestureManagerInstance::<>f__am$cache10
	UnityFunc_1_t1559192105 * ___U3CU3Ef__amU24cache10_18;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>> TouchScript.GestureManagerInstance::<>f__am$cache11
	UnityAction_1_t3088012868 * ___U3CU3Ef__amU24cache11_19;
	// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::<>f__am$cache12
	UnityFunc_1_t2053600641 * ___U3CU3Ef__amU24cache12_20;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::<>f__am$cache13
	UnityAction_1_t1695335966 * ___U3CU3Ef__amU24cache13_21;
	// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<UnityEngine.Transform>,System.Collections.Generic.List`1<UnityEngine.Transform>> TouchScript.GestureManagerInstance::<>f__am$cache14
	UnityFunc_1_t1878732133 * ___U3CU3Ef__amU24cache14_22;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Transform>> TouchScript.GestureManagerInstance::<>f__am$cache15
	UnityAction_1_t4010824941 * ___U3CU3Ef__amU24cache15_23;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___instance_2)); }
	inline GestureManagerInstance_t505647059 * get_instance_2() const { return ___instance_2; }
	inline GestureManagerInstance_t505647059 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GestureManagerInstance_t505647059 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_shuttingDown_3() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___shuttingDown_3)); }
	inline bool get_shuttingDown_3() const { return ___shuttingDown_3; }
	inline bool* get_address_of_shuttingDown_3() { return &___shuttingDown_3; }
	inline void set_shuttingDown_3(bool value)
	{
		___shuttingDown_3 = value;
	}

	inline static int32_t get_offset_of_gestureListPool_14() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___gestureListPool_14)); }
	inline ObjectPool_1_t1074715964 * get_gestureListPool_14() const { return ___gestureListPool_14; }
	inline ObjectPool_1_t1074715964 ** get_address_of_gestureListPool_14() { return &___gestureListPool_14; }
	inline void set_gestureListPool_14(ObjectPool_1_t1074715964 * value)
	{
		___gestureListPool_14 = value;
		Il2CppCodeGenWriteBarrier(&___gestureListPool_14, value);
	}

	inline static int32_t get_offset_of_touchListPool_15() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___touchListPool_15)); }
	inline ObjectPool_1_t3977006358 * get_touchListPool_15() const { return ___touchListPool_15; }
	inline ObjectPool_1_t3977006358 ** get_address_of_touchListPool_15() { return &___touchListPool_15; }
	inline void set_touchListPool_15(ObjectPool_1_t3977006358 * value)
	{
		___touchListPool_15 = value;
		Il2CppCodeGenWriteBarrier(&___touchListPool_15, value);
	}

	inline static int32_t get_offset_of_transformListPool_16() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___transformListPool_16)); }
	inline ObjectPool_1_t1997528037 * get_transformListPool_16() const { return ___transformListPool_16; }
	inline ObjectPool_1_t1997528037 ** get_address_of_transformListPool_16() { return &___transformListPool_16; }
	inline void set_transformListPool_16(ObjectPool_1_t1997528037 * value)
	{
		___transformListPool_16 = value;
		Il2CppCodeGenWriteBarrier(&___transformListPool_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_18() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache10_18)); }
	inline UnityFunc_1_t1559192105 * get_U3CU3Ef__amU24cache10_18() const { return ___U3CU3Ef__amU24cache10_18; }
	inline UnityFunc_1_t1559192105 ** get_address_of_U3CU3Ef__amU24cache10_18() { return &___U3CU3Ef__amU24cache10_18; }
	inline void set_U3CU3Ef__amU24cache10_18(UnityFunc_1_t1559192105 * value)
	{
		___U3CU3Ef__amU24cache10_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_19() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache11_19)); }
	inline UnityAction_1_t3088012868 * get_U3CU3Ef__amU24cache11_19() const { return ___U3CU3Ef__amU24cache11_19; }
	inline UnityAction_1_t3088012868 ** get_address_of_U3CU3Ef__amU24cache11_19() { return &___U3CU3Ef__amU24cache11_19; }
	inline void set_U3CU3Ef__amU24cache11_19(UnityAction_1_t3088012868 * value)
	{
		___U3CU3Ef__amU24cache11_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_20() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache12_20)); }
	inline UnityFunc_1_t2053600641 * get_U3CU3Ef__amU24cache12_20() const { return ___U3CU3Ef__amU24cache12_20; }
	inline UnityFunc_1_t2053600641 ** get_address_of_U3CU3Ef__amU24cache12_20() { return &___U3CU3Ef__amU24cache12_20; }
	inline void set_U3CU3Ef__amU24cache12_20(UnityFunc_1_t2053600641 * value)
	{
		___U3CU3Ef__amU24cache12_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_21() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache13_21)); }
	inline UnityAction_1_t1695335966 * get_U3CU3Ef__amU24cache13_21() const { return ___U3CU3Ef__amU24cache13_21; }
	inline UnityAction_1_t1695335966 ** get_address_of_U3CU3Ef__amU24cache13_21() { return &___U3CU3Ef__amU24cache13_21; }
	inline void set_U3CU3Ef__amU24cache13_21(UnityAction_1_t1695335966 * value)
	{
		___U3CU3Ef__amU24cache13_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_22() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache14_22)); }
	inline UnityFunc_1_t1878732133 * get_U3CU3Ef__amU24cache14_22() const { return ___U3CU3Ef__amU24cache14_22; }
	inline UnityFunc_1_t1878732133 ** get_address_of_U3CU3Ef__amU24cache14_22() { return &___U3CU3Ef__amU24cache14_22; }
	inline void set_U3CU3Ef__amU24cache14_22(UnityFunc_1_t1878732133 * value)
	{
		___U3CU3Ef__amU24cache14_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_23() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___U3CU3Ef__amU24cache15_23)); }
	inline UnityAction_1_t4010824941 * get_U3CU3Ef__amU24cache15_23() const { return ___U3CU3Ef__amU24cache15_23; }
	inline UnityAction_1_t4010824941 ** get_address_of_U3CU3Ef__amU24cache15_23() { return &___U3CU3Ef__amU24cache15_23; }
	inline void set_U3CU3Ef__amU24cache15_23(UnityAction_1_t4010824941 * value)
	{
		___U3CU3Ef__amU24cache15_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
