﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageAllianceManager
struct MessageAllianceManager_t4132282143;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MessageAllianceManager::.ctor()
extern "C"  void MessageAllianceManager__ctor_m3794350772 (MessageAllianceManager_t4132282143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageAllianceManager::SetUserId(System.Int64)
extern "C"  void MessageAllianceManager_SetUserId_m520919630 (MessageAllianceManager_t4132282143 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageAllianceManager::SendUserMessage()
extern "C"  void MessageAllianceManager_SendUserMessage_m2464419420 (MessageAllianceManager_t4132282143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageAllianceManager::UserMessageSent(System.Object,System.String)
extern "C"  void MessageAllianceManager_UserMessageSent_m3613401360 (MessageAllianceManager_t4132282143 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
