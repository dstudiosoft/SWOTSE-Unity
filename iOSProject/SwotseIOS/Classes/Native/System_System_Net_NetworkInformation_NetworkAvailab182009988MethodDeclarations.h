﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.NetworkAvailabilityEventArgs
struct NetworkAvailabilityEventArgs_t182009988;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.NetworkAvailabilityEventArgs::.ctor(System.Boolean)
extern "C"  void NetworkAvailabilityEventArgs__ctor_m4183954163 (NetworkAvailabilityEventArgs_t182009988 * __this, bool ___available0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.NetworkAvailabilityEventArgs::get_IsAvailable()
extern "C"  bool NetworkAvailabilityEventArgs_get_IsAvailable_m1579046116 (NetworkAvailabilityEventArgs_t182009988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
