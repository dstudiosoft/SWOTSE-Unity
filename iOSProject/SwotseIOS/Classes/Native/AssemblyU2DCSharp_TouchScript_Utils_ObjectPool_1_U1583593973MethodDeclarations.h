﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_U1709969761MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.Behaviors.Visualizer.TouchProxyBase,TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Object,System.IntPtr)
#define UnityFunc_1__ctor_m2754943823(__this, ___object0, ___method1, method) ((  void (*) (UnityFunc_1_t1583593973 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityFunc_1__ctor_m3483258383_gshared)(__this, ___object0, ___method1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.Behaviors.Visualizer.TouchProxyBase,TouchScript.Behaviors.Visualizer.TouchProxyBase>::Invoke()
#define UnityFunc_1_Invoke_m1453268034(__this, method) ((  TouchProxyBase_t4188753234 * (*) (UnityFunc_1_t1583593973 *, const MethodInfo*))UnityFunc_1_Invoke_m4133775042_gshared)(__this, method)
// System.IAsyncResult TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.Behaviors.Visualizer.TouchProxyBase,TouchScript.Behaviors.Visualizer.TouchProxyBase>::BeginInvoke(System.AsyncCallback,System.Object)
#define UnityFunc_1_BeginInvoke_m1001839498(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (UnityFunc_1_t1583593973 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_BeginInvoke_m3032171082_gshared)(__this, ___callback0, ___object1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<TouchScript.Behaviors.Visualizer.TouchProxyBase,TouchScript.Behaviors.Visualizer.TouchProxyBase>::EndInvoke(System.IAsyncResult)
#define UnityFunc_1_EndInvoke_m2701715158(__this, ___result0, method) ((  TouchProxyBase_t4188753234 * (*) (UnityFunc_1_t1583593973 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_EndInvoke_m455851222_gshared)(__this, ___result0, method)
