﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialContentManager
struct TutorialContentManager_t1372992118;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialContentManager::.ctor()
extern "C"  void TutorialContentManager__ctor_m58430869 (TutorialContentManager_t1372992118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialContentManager::Start()
extern "C"  void TutorialContentManager_Start_m3937291189 (TutorialContentManager_t1372992118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialContentManager::UpdateContent()
extern "C"  void TutorialContentManager_UpdateContent_m1236219837 (TutorialContentManager_t1372992118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialContentManager::NextPage()
extern "C"  void TutorialContentManager_NextPage_m1070034901 (TutorialContentManager_t1372992118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialContentManager::PreviousPage()
extern "C"  void TutorialContentManager_PreviousPage_m192441847 (TutorialContentManager_t1372992118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
