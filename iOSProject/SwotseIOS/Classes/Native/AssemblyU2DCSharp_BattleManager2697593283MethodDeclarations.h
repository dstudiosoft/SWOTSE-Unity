﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleManager
struct BattleManager_t2697593283;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnitsModel
struct UnitsModel_t1926818124;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_UnitsModel1926818124.h"
#include "AssemblyU2DCSharp_ResourcesModel2978985958.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BattleManager::.ctor()
extern "C"  void BattleManager__ctor_m1613296980 (BattleManager_t2697593283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::add_onGetCityArmies(SimpleEvent)
extern "C"  void BattleManager_add_onGetCityArmies_m4100885139 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::remove_onGetCityArmies(SimpleEvent)
extern "C"  void BattleManager_remove_onGetCityArmies_m3996924012 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::add_onMarchLaunched(SimpleEvent)
extern "C"  void BattleManager_add_onMarchLaunched_m876621502 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::remove_onMarchLaunched(SimpleEvent)
extern "C"  void BattleManager_remove_onMarchLaunched_m783349569 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::add_onMarchCanceled(SimpleEvent)
extern "C"  void BattleManager_add_onMarchCanceled_m1598384249 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::remove_onMarchCanceled(SimpleEvent)
extern "C"  void BattleManager_remove_onMarchCanceled_m3075124834 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::add_onArmyReturnInitiated(SimpleEvent)
extern "C"  void BattleManager_add_onArmyReturnInitiated_m2065036369 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleManager::remove_onArmyReturnInitiated(SimpleEvent)
extern "C"  void BattleManager_remove_onArmyReturnInitiated_m3878282514 (BattleManager_t2697593283 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleManager::GetCityArmies()
extern "C"  Il2CppObject * BattleManager_GetCityArmies_m1345670862 (BattleManager_t2697593283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleManager::March(UnitsModel,ResourcesModel,System.String,System.Int64,System.Int64,System.Int64)
extern "C"  Il2CppObject * BattleManager_March_m1845211069 (BattleManager_t2697593283 * __this, UnitsModel_t1926818124 * ___army0, ResourcesModel_t2978985958 * ___resources1, String_t* ___attcakType2, int64_t ___destX3, int64_t ___destY4, int64_t ___knightId5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleManager::RecruitBarbarians(UnitsModel,System.Int64,System.Int64,System.Int64)
extern "C"  Il2CppObject * BattleManager_RecruitBarbarians_m3736967755 (BattleManager_t2697593283 * __this, UnitsModel_t1926818124 * ___army0, int64_t ___destX1, int64_t ___destY2, int64_t ___barbariansId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleManager::CancelAttack(System.Int64)
extern "C"  Il2CppObject * BattleManager_CancelAttack_m2874521620 (BattleManager_t2697593283 * __this, int64_t ___armyId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleManager::ReturnArmy(System.Int64)
extern "C"  Il2CppObject * BattleManager_ReturnArmy_m3379052843 (BattleManager_t2697593283 * __this, int64_t ___armyId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
