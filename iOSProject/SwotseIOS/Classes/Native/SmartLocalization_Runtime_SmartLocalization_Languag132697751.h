﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject>
struct List_1_t1265013904;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;
// SmartLocalization.ChangeLanguageEventHandler
struct ChangeLanguageEventHandler_t999227700;
// System.String
struct String_t;
// SmartLocalization.SmartCultureInfoCollection
struct SmartCultureInfoCollection_t1434882423;
// SmartLocalization.LanguageDataHandler
struct LanguageDataHandler_t3279109820;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager
struct  LanguageManager_t132697751  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::serializedKeys
	List_1_t1398341365 * ___serializedKeys_6;
	// System.Collections.Generic.List`1<SmartLocalization.LocalizedObject> SmartLocalization.LanguageManager::serializedValues
	List_1_t1265013904 * ___serializedValues_7;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::serializedCulture
	SmartCultureInfo_t2361725737 * ___serializedCulture_8;
	// SmartLocalization.ChangeLanguageEventHandler SmartLocalization.LanguageManager::OnChangeLanguage
	ChangeLanguageEventHandler_t999227700 * ___OnChangeLanguage_9;
	// System.String SmartLocalization.LanguageManager::defaultLanguage
	String_t* ___defaultLanguage_10;
	// SmartLocalization.SmartCultureInfoCollection SmartLocalization.LanguageManager::availableLanguages
	SmartCultureInfoCollection_t1434882423 * ___availableLanguages_11;
	// SmartLocalization.LanguageDataHandler SmartLocalization.LanguageManager::languageDataHandler
	LanguageDataHandler_t3279109820 * ___languageDataHandler_12;

public:
	inline static int32_t get_offset_of_serializedKeys_6() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___serializedKeys_6)); }
	inline List_1_t1398341365 * get_serializedKeys_6() const { return ___serializedKeys_6; }
	inline List_1_t1398341365 ** get_address_of_serializedKeys_6() { return &___serializedKeys_6; }
	inline void set_serializedKeys_6(List_1_t1398341365 * value)
	{
		___serializedKeys_6 = value;
		Il2CppCodeGenWriteBarrier(&___serializedKeys_6, value);
	}

	inline static int32_t get_offset_of_serializedValues_7() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___serializedValues_7)); }
	inline List_1_t1265013904 * get_serializedValues_7() const { return ___serializedValues_7; }
	inline List_1_t1265013904 ** get_address_of_serializedValues_7() { return &___serializedValues_7; }
	inline void set_serializedValues_7(List_1_t1265013904 * value)
	{
		___serializedValues_7 = value;
		Il2CppCodeGenWriteBarrier(&___serializedValues_7, value);
	}

	inline static int32_t get_offset_of_serializedCulture_8() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___serializedCulture_8)); }
	inline SmartCultureInfo_t2361725737 * get_serializedCulture_8() const { return ___serializedCulture_8; }
	inline SmartCultureInfo_t2361725737 ** get_address_of_serializedCulture_8() { return &___serializedCulture_8; }
	inline void set_serializedCulture_8(SmartCultureInfo_t2361725737 * value)
	{
		___serializedCulture_8 = value;
		Il2CppCodeGenWriteBarrier(&___serializedCulture_8, value);
	}

	inline static int32_t get_offset_of_OnChangeLanguage_9() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___OnChangeLanguage_9)); }
	inline ChangeLanguageEventHandler_t999227700 * get_OnChangeLanguage_9() const { return ___OnChangeLanguage_9; }
	inline ChangeLanguageEventHandler_t999227700 ** get_address_of_OnChangeLanguage_9() { return &___OnChangeLanguage_9; }
	inline void set_OnChangeLanguage_9(ChangeLanguageEventHandler_t999227700 * value)
	{
		___OnChangeLanguage_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnChangeLanguage_9, value);
	}

	inline static int32_t get_offset_of_defaultLanguage_10() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___defaultLanguage_10)); }
	inline String_t* get_defaultLanguage_10() const { return ___defaultLanguage_10; }
	inline String_t** get_address_of_defaultLanguage_10() { return &___defaultLanguage_10; }
	inline void set_defaultLanguage_10(String_t* value)
	{
		___defaultLanguage_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultLanguage_10, value);
	}

	inline static int32_t get_offset_of_availableLanguages_11() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___availableLanguages_11)); }
	inline SmartCultureInfoCollection_t1434882423 * get_availableLanguages_11() const { return ___availableLanguages_11; }
	inline SmartCultureInfoCollection_t1434882423 ** get_address_of_availableLanguages_11() { return &___availableLanguages_11; }
	inline void set_availableLanguages_11(SmartCultureInfoCollection_t1434882423 * value)
	{
		___availableLanguages_11 = value;
		Il2CppCodeGenWriteBarrier(&___availableLanguages_11, value);
	}

	inline static int32_t get_offset_of_languageDataHandler_12() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751, ___languageDataHandler_12)); }
	inline LanguageDataHandler_t3279109820 * get_languageDataHandler_12() const { return ___languageDataHandler_12; }
	inline LanguageDataHandler_t3279109820 ** get_address_of_languageDataHandler_12() { return &___languageDataHandler_12; }
	inline void set_languageDataHandler_12(LanguageDataHandler_t3279109820 * value)
	{
		___languageDataHandler_12 = value;
		Il2CppCodeGenWriteBarrier(&___languageDataHandler_12, value);
	}
};

struct LanguageManager_t132697751_StaticFields
{
public:
	// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::instance
	LanguageManager_t132697751 * ___instance_2;
	// System.Boolean SmartLocalization.LanguageManager::IsQuitting
	bool ___IsQuitting_3;
	// System.Boolean SmartLocalization.LanguageManager::DontDestroyOnLoadToggle
	bool ___DontDestroyOnLoadToggle_4;
	// System.Boolean SmartLocalization.LanguageManager::DidSetDontDestroyOnLoad
	bool ___DidSetDontDestroyOnLoad_5;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751_StaticFields, ___instance_2)); }
	inline LanguageManager_t132697751 * get_instance_2() const { return ___instance_2; }
	inline LanguageManager_t132697751 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LanguageManager_t132697751 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_IsQuitting_3() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751_StaticFields, ___IsQuitting_3)); }
	inline bool get_IsQuitting_3() const { return ___IsQuitting_3; }
	inline bool* get_address_of_IsQuitting_3() { return &___IsQuitting_3; }
	inline void set_IsQuitting_3(bool value)
	{
		___IsQuitting_3 = value;
	}

	inline static int32_t get_offset_of_DontDestroyOnLoadToggle_4() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751_StaticFields, ___DontDestroyOnLoadToggle_4)); }
	inline bool get_DontDestroyOnLoadToggle_4() const { return ___DontDestroyOnLoadToggle_4; }
	inline bool* get_address_of_DontDestroyOnLoadToggle_4() { return &___DontDestroyOnLoadToggle_4; }
	inline void set_DontDestroyOnLoadToggle_4(bool value)
	{
		___DontDestroyOnLoadToggle_4 = value;
	}

	inline static int32_t get_offset_of_DidSetDontDestroyOnLoad_5() { return static_cast<int32_t>(offsetof(LanguageManager_t132697751_StaticFields, ___DidSetDontDestroyOnLoad_5)); }
	inline bool get_DidSetDontDestroyOnLoad_5() const { return ___DidSetDontDestroyOnLoad_5; }
	inline bool* get_address_of_DidSetDontDestroyOnLoad_5() { return &___DidSetDontDestroyOnLoad_5; }
	inline void set_DidSetDontDestroyOnLoad_5(bool value)
	{
		___DidSetDontDestroyOnLoad_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
