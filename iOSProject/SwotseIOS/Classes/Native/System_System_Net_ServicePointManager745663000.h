﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t290043810;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t1916536542;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_t745663000  : public Il2CppObject
{
public:

public:
};

struct ServicePointManager_t745663000_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t290043810 * ___servicePoints_2;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	Il2CppObject * ___policy_3;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_4;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_5;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_6;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_7;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_8;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_9;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_10;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::server_cert_cb
	RemoteCertificateValidationCallback_t2756269959 * ___server_cert_cb_11;

public:
	inline static int32_t get_offset_of_servicePoints_2() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___servicePoints_2)); }
	inline HybridDictionary_t290043810 * get_servicePoints_2() const { return ___servicePoints_2; }
	inline HybridDictionary_t290043810 ** get_address_of_servicePoints_2() { return &___servicePoints_2; }
	inline void set_servicePoints_2(HybridDictionary_t290043810 * value)
	{
		___servicePoints_2 = value;
		Il2CppCodeGenWriteBarrier(&___servicePoints_2, value);
	}

	inline static int32_t get_offset_of_policy_3() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___policy_3)); }
	inline Il2CppObject * get_policy_3() const { return ___policy_3; }
	inline Il2CppObject ** get_address_of_policy_3() { return &___policy_3; }
	inline void set_policy_3(Il2CppObject * value)
	{
		___policy_3 = value;
		Il2CppCodeGenWriteBarrier(&___policy_3, value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_4() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___defaultConnectionLimit_4)); }
	inline int32_t get_defaultConnectionLimit_4() const { return ___defaultConnectionLimit_4; }
	inline int32_t* get_address_of_defaultConnectionLimit_4() { return &___defaultConnectionLimit_4; }
	inline void set_defaultConnectionLimit_4(int32_t value)
	{
		___defaultConnectionLimit_4 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_5() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___maxServicePointIdleTime_5)); }
	inline int32_t get_maxServicePointIdleTime_5() const { return ___maxServicePointIdleTime_5; }
	inline int32_t* get_address_of_maxServicePointIdleTime_5() { return &___maxServicePointIdleTime_5; }
	inline void set_maxServicePointIdleTime_5(int32_t value)
	{
		___maxServicePointIdleTime_5 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_6() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___maxServicePoints_6)); }
	inline int32_t get_maxServicePoints_6() const { return ___maxServicePoints_6; }
	inline int32_t* get_address_of_maxServicePoints_6() { return &___maxServicePoints_6; }
	inline void set_maxServicePoints_6(int32_t value)
	{
		___maxServicePoints_6 = value;
	}

	inline static int32_t get_offset_of__checkCRL_7() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ____checkCRL_7)); }
	inline bool get__checkCRL_7() const { return ____checkCRL_7; }
	inline bool* get_address_of__checkCRL_7() { return &____checkCRL_7; }
	inline void set__checkCRL_7(bool value)
	{
		____checkCRL_7 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_8() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ____securityProtocol_8)); }
	inline int32_t get__securityProtocol_8() const { return ____securityProtocol_8; }
	inline int32_t* get_address_of__securityProtocol_8() { return &____securityProtocol_8; }
	inline void set__securityProtocol_8(int32_t value)
	{
		____securityProtocol_8 = value;
	}

	inline static int32_t get_offset_of_expectContinue_9() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___expectContinue_9)); }
	inline bool get_expectContinue_9() const { return ___expectContinue_9; }
	inline bool* get_address_of_expectContinue_9() { return &___expectContinue_9; }
	inline void set_expectContinue_9(bool value)
	{
		___expectContinue_9 = value;
	}

	inline static int32_t get_offset_of_useNagle_10() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___useNagle_10)); }
	inline bool get_useNagle_10() const { return ___useNagle_10; }
	inline bool* get_address_of_useNagle_10() { return &___useNagle_10; }
	inline void set_useNagle_10(bool value)
	{
		___useNagle_10 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_11() { return static_cast<int32_t>(offsetof(ServicePointManager_t745663000_StaticFields, ___server_cert_cb_11)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_server_cert_cb_11() const { return ___server_cert_cb_11; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_server_cert_cb_11() { return &___server_cert_cb_11; }
	inline void set_server_cert_cb_11(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___server_cert_cb_11 = value;
		Il2CppCodeGenWriteBarrier(&___server_cert_cb_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
