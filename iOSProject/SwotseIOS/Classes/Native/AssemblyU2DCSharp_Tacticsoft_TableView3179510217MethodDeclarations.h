﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t386580345;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.String
struct String_t;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

// System.Void Tacticsoft.TableView::.ctor()
extern "C"  void TableView__ctor_m4180731122 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.ITableViewDataSource Tacticsoft.TableView::get_dataSource()
extern "C"  Il2CppObject * TableView_get_dataSource_m3016730248 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_dataSource(Tacticsoft.ITableViewDataSource)
extern "C"  void TableView_set_dataSource_m3123682131 (TableView_t3179510217 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.TableView::GetReusableCell(System.String)
extern "C"  TableViewCell_t1276614623 * TableView_GetReusableCell_m1608429575 (TableView_t3179510217 * __this, String_t* ___reuseIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tacticsoft.TableView::get_isEmpty()
extern "C"  bool TableView_get_isEmpty_m3790869662 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_isEmpty(System.Boolean)
extern "C"  void TableView_set_isEmpty_m3594347429 (TableView_t3179510217 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ReloadData()
extern "C"  void TableView_ReloadData_m2870978589 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.TableView::GetCellAtRow(System.Int32)
extern "C"  TableViewCell_t1276614623 * TableView_GetCellAtRow_m4266448312 (TableView_t3179510217 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::get_visibleRowRange()
extern "C"  Range_t3455291607  TableView_get_visibleRowRange_m4047434366 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::NotifyCellDimensionsChanged(System.Int32)
extern "C"  void TableView_NotifyCellDimensionsChanged_m1450046645 (TableView_t3179510217 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::get_scrollableHeight()
extern "C"  float TableView_get_scrollableHeight_m4151292283 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::get_scrollY()
extern "C"  float TableView_get_scrollY_m807162759 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_scrollY(System.Single)
extern "C"  void TableView_set_scrollY_m1373612018 (TableView_t3179510217 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::GetScrollYForRow(System.Int32,System.Boolean)
extern "C"  float TableView_GetScrollYForRow_m1895424735 (TableView_t3179510217 * __this, int32_t ___row0, bool ___above1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ScrollViewValueChanged(UnityEngine.Vector2)
extern "C"  void TableView_ScrollViewValueChanged_m2771049193 (TableView_t3179510217 * __this, Vector2_t2243707579  ___newScrollValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::RecalculateVisibleRowsFromScratch()
extern "C"  void TableView_RecalculateVisibleRowsFromScratch_m3582705566 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ClearAllRows()
extern "C"  void TableView_ClearAllRows_m2249116465 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::Awake()
extern "C"  void TableView_Awake_m2292315053 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::Update()
extern "C"  void TableView_Update_m759545665 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::LateUpdate()
extern "C"  void TableView_LateUpdate_m684234965 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::OnEnable()
extern "C"  void TableView_OnEnable_m4228725282 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::OnDisable()
extern "C"  void TableView_OnDisable_m2050475901 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::CalculateCurrentVisibleRowRange()
extern "C"  Range_t3455291607  TableView_CalculateCurrentVisibleRowRange_m3675299386 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::SetInitialVisibleRows()
extern "C"  void TableView_SetInitialVisibleRows_m1451299395 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::AddRow(System.Int32,System.Boolean)
extern "C"  void TableView_AddRow_m3283573761 (TableView_t3179510217 * __this, int32_t ___row0, bool ___atEnd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::RefreshVisibleRows()
extern "C"  void TableView_RefreshVisibleRows_m1136036298 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::UpdatePaddingElements()
extern "C"  void TableView_UpdatePaddingElements_m3342414501 (TableView_t3179510217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::HideRow(System.Boolean)
extern "C"  void TableView_HideRow_m3056973709 (TableView_t3179510217 * __this, bool ___last0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.LayoutElement Tacticsoft.TableView::CreateEmptyPaddingElement(System.String)
extern "C"  LayoutElement_t2808691390 * TableView_CreateEmptyPaddingElement_m2086087246 (TableView_t3179510217 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.TableView::FindIndexOfRowAtY(System.Single)
extern "C"  int32_t TableView_FindIndexOfRowAtY_m3120364699 (TableView_t3179510217 * __this, float ___y0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.TableView::FindIndexOfRowAtY(System.Single,System.Int32,System.Int32)
extern "C"  int32_t TableView_FindIndexOfRowAtY_m1448021895 (TableView_t3179510217 * __this, float ___y0, int32_t ___startIndex1, int32_t ___endIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::GetCumulativeRowHeight(System.Int32)
extern "C"  float TableView_GetCumulativeRowHeight_m315162965 (TableView_t3179510217 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::StoreCellForReuse(Tacticsoft.TableViewCell)
extern "C"  void TableView_StoreCellForReuse_m1154975167 (TableView_t3179510217 * __this, TableViewCell_t1276614623 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
