﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.InputHandlers.MouseHandler
struct MouseHandler_t3116661769;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>
struct Func_4_t1475708928;
// System.Action`2<System.Int32,UnityEngine.Vector2>
struct Action_2_t1542075644;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.InputSources.InputHandlers.MouseHandler::.ctor(TouchScript.Tags,System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>,System.Action`2<System.Int32,UnityEngine.Vector2>,System.Action`1<System.Int32>,System.Action`1<System.Int32>)
extern "C"  void MouseHandler__ctor_m1311820746 (MouseHandler_t3116661769 * __this, Tags_t1265380163 * ___tags0, Func_4_t1475708928 * ___beginTouch1, Action_2_t1542075644 * ___moveTouch2, Action_1_t1873676830 * ___endTouch3, Action_1_t1873676830 * ___cancelTouch4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.MouseHandler::EndTouches()
extern "C"  void MouseHandler_EndTouches_m4231251927 (MouseHandler_t3116661769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.MouseHandler::Update()
extern "C"  void MouseHandler_Update_m2528182160 (MouseHandler_t3116661769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.InputSources.InputHandlers.MouseHandler::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  bool MouseHandler_CancelTouch_m1629623884 (MouseHandler_t3116661769 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.MouseHandler::Dispose()
extern "C"  void MouseHandler_Dispose_m926172236 (MouseHandler_t3116661769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
