﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Behaviors.Transformer
struct Transformer_t946377179;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"

// System.Void TouchScript.Behaviors.Transformer::.ctor()
extern "C"  void Transformer__ctor_m4070059633 (Transformer_t946377179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Transformer::Awake()
extern "C"  void Transformer_Awake_m4001051856 (Transformer_t946377179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Transformer::OnEnable()
extern "C"  void Transformer_OnEnable_m1783439537 (Transformer_t946377179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Transformer::OnDisable()
extern "C"  void Transformer_OnDisable_m2025171928 (Transformer_t946377179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Transformer::transformHandler(System.Object,System.EventArgs)
extern "C"  void Transformer_transformHandler_m174827559 (Transformer_t946377179 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
