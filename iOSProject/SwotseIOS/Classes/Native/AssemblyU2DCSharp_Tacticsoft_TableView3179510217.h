﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.TableView/CellVisibilityChangeEvent
struct CellVisibilityChangeEvent_t4037417944;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t386580345;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t2468316403;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>
struct Dictionary_2_t284440258;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>
struct Dictionary_2_t3496102114;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView
struct  TableView_t3179510217  : public MonoBehaviour_t1158329972
{
public:
	// Tacticsoft.TableView/CellVisibilityChangeEvent Tacticsoft.TableView::onCellVisibilityChanged
	CellVisibilityChangeEvent_t4037417944 * ___onCellVisibilityChanged_2;
	// Tacticsoft.ITableViewDataSource Tacticsoft.TableView::m_dataSource
	Il2CppObject * ___m_dataSource_3;
	// System.Boolean Tacticsoft.TableView::m_requiresReload
	bool ___m_requiresReload_4;
	// UnityEngine.UI.VerticalLayoutGroup Tacticsoft.TableView::m_verticalLayoutGroup
	VerticalLayoutGroup_t2468316403 * ___m_verticalLayoutGroup_5;
	// UnityEngine.UI.ScrollRect Tacticsoft.TableView::m_scrollRect
	ScrollRect_t1199013257 * ___m_scrollRect_6;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_topPadding
	LayoutElement_t2808691390 * ___m_topPadding_7;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_bottomPadding
	LayoutElement_t2808691390 * ___m_bottomPadding_8;
	// System.Single[] Tacticsoft.TableView::m_rowHeights
	SingleU5BU5D_t577127397* ___m_rowHeights_9;
	// System.Single[] Tacticsoft.TableView::m_cumulativeRowHeights
	SingleU5BU5D_t577127397* ___m_cumulativeRowHeights_10;
	// System.Int32 Tacticsoft.TableView::m_cleanCumulativeIndex
	int32_t ___m_cleanCumulativeIndex_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell> Tacticsoft.TableView::m_visibleCells
	Dictionary_2_t284440258 * ___m_visibleCells_12;
	// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::m_visibleRowRange
	Range_t3455291607  ___m_visibleRowRange_13;
	// UnityEngine.RectTransform Tacticsoft.TableView::m_reusableCellContainer
	RectTransform_t3349966182 * ___m_reusableCellContainer_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>> Tacticsoft.TableView::m_reusableCells
	Dictionary_2_t3496102114 * ___m_reusableCells_15;
	// System.Single Tacticsoft.TableView::m_scrollY
	float ___m_scrollY_16;
	// System.Boolean Tacticsoft.TableView::m_requiresRefresh
	bool ___m_requiresRefresh_17;
	// System.Boolean Tacticsoft.TableView::<isEmpty>k__BackingField
	bool ___U3CisEmptyU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_onCellVisibilityChanged_2() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___onCellVisibilityChanged_2)); }
	inline CellVisibilityChangeEvent_t4037417944 * get_onCellVisibilityChanged_2() const { return ___onCellVisibilityChanged_2; }
	inline CellVisibilityChangeEvent_t4037417944 ** get_address_of_onCellVisibilityChanged_2() { return &___onCellVisibilityChanged_2; }
	inline void set_onCellVisibilityChanged_2(CellVisibilityChangeEvent_t4037417944 * value)
	{
		___onCellVisibilityChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCellVisibilityChanged_2, value);
	}

	inline static int32_t get_offset_of_m_dataSource_3() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_dataSource_3)); }
	inline Il2CppObject * get_m_dataSource_3() const { return ___m_dataSource_3; }
	inline Il2CppObject ** get_address_of_m_dataSource_3() { return &___m_dataSource_3; }
	inline void set_m_dataSource_3(Il2CppObject * value)
	{
		___m_dataSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_dataSource_3, value);
	}

	inline static int32_t get_offset_of_m_requiresReload_4() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_requiresReload_4)); }
	inline bool get_m_requiresReload_4() const { return ___m_requiresReload_4; }
	inline bool* get_address_of_m_requiresReload_4() { return &___m_requiresReload_4; }
	inline void set_m_requiresReload_4(bool value)
	{
		___m_requiresReload_4 = value;
	}

	inline static int32_t get_offset_of_m_verticalLayoutGroup_5() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_verticalLayoutGroup_5)); }
	inline VerticalLayoutGroup_t2468316403 * get_m_verticalLayoutGroup_5() const { return ___m_verticalLayoutGroup_5; }
	inline VerticalLayoutGroup_t2468316403 ** get_address_of_m_verticalLayoutGroup_5() { return &___m_verticalLayoutGroup_5; }
	inline void set_m_verticalLayoutGroup_5(VerticalLayoutGroup_t2468316403 * value)
	{
		___m_verticalLayoutGroup_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_verticalLayoutGroup_5, value);
	}

	inline static int32_t get_offset_of_m_scrollRect_6() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_scrollRect_6)); }
	inline ScrollRect_t1199013257 * get_m_scrollRect_6() const { return ___m_scrollRect_6; }
	inline ScrollRect_t1199013257 ** get_address_of_m_scrollRect_6() { return &___m_scrollRect_6; }
	inline void set_m_scrollRect_6(ScrollRect_t1199013257 * value)
	{
		___m_scrollRect_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_scrollRect_6, value);
	}

	inline static int32_t get_offset_of_m_topPadding_7() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_topPadding_7)); }
	inline LayoutElement_t2808691390 * get_m_topPadding_7() const { return ___m_topPadding_7; }
	inline LayoutElement_t2808691390 ** get_address_of_m_topPadding_7() { return &___m_topPadding_7; }
	inline void set_m_topPadding_7(LayoutElement_t2808691390 * value)
	{
		___m_topPadding_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_topPadding_7, value);
	}

	inline static int32_t get_offset_of_m_bottomPadding_8() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_bottomPadding_8)); }
	inline LayoutElement_t2808691390 * get_m_bottomPadding_8() const { return ___m_bottomPadding_8; }
	inline LayoutElement_t2808691390 ** get_address_of_m_bottomPadding_8() { return &___m_bottomPadding_8; }
	inline void set_m_bottomPadding_8(LayoutElement_t2808691390 * value)
	{
		___m_bottomPadding_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_bottomPadding_8, value);
	}

	inline static int32_t get_offset_of_m_rowHeights_9() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_rowHeights_9)); }
	inline SingleU5BU5D_t577127397* get_m_rowHeights_9() const { return ___m_rowHeights_9; }
	inline SingleU5BU5D_t577127397** get_address_of_m_rowHeights_9() { return &___m_rowHeights_9; }
	inline void set_m_rowHeights_9(SingleU5BU5D_t577127397* value)
	{
		___m_rowHeights_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_rowHeights_9, value);
	}

	inline static int32_t get_offset_of_m_cumulativeRowHeights_10() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_cumulativeRowHeights_10)); }
	inline SingleU5BU5D_t577127397* get_m_cumulativeRowHeights_10() const { return ___m_cumulativeRowHeights_10; }
	inline SingleU5BU5D_t577127397** get_address_of_m_cumulativeRowHeights_10() { return &___m_cumulativeRowHeights_10; }
	inline void set_m_cumulativeRowHeights_10(SingleU5BU5D_t577127397* value)
	{
		___m_cumulativeRowHeights_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_cumulativeRowHeights_10, value);
	}

	inline static int32_t get_offset_of_m_cleanCumulativeIndex_11() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_cleanCumulativeIndex_11)); }
	inline int32_t get_m_cleanCumulativeIndex_11() const { return ___m_cleanCumulativeIndex_11; }
	inline int32_t* get_address_of_m_cleanCumulativeIndex_11() { return &___m_cleanCumulativeIndex_11; }
	inline void set_m_cleanCumulativeIndex_11(int32_t value)
	{
		___m_cleanCumulativeIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_visibleCells_12() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_visibleCells_12)); }
	inline Dictionary_2_t284440258 * get_m_visibleCells_12() const { return ___m_visibleCells_12; }
	inline Dictionary_2_t284440258 ** get_address_of_m_visibleCells_12() { return &___m_visibleCells_12; }
	inline void set_m_visibleCells_12(Dictionary_2_t284440258 * value)
	{
		___m_visibleCells_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_visibleCells_12, value);
	}

	inline static int32_t get_offset_of_m_visibleRowRange_13() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_visibleRowRange_13)); }
	inline Range_t3455291607  get_m_visibleRowRange_13() const { return ___m_visibleRowRange_13; }
	inline Range_t3455291607 * get_address_of_m_visibleRowRange_13() { return &___m_visibleRowRange_13; }
	inline void set_m_visibleRowRange_13(Range_t3455291607  value)
	{
		___m_visibleRowRange_13 = value;
	}

	inline static int32_t get_offset_of_m_reusableCellContainer_14() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_reusableCellContainer_14)); }
	inline RectTransform_t3349966182 * get_m_reusableCellContainer_14() const { return ___m_reusableCellContainer_14; }
	inline RectTransform_t3349966182 ** get_address_of_m_reusableCellContainer_14() { return &___m_reusableCellContainer_14; }
	inline void set_m_reusableCellContainer_14(RectTransform_t3349966182 * value)
	{
		___m_reusableCellContainer_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_reusableCellContainer_14, value);
	}

	inline static int32_t get_offset_of_m_reusableCells_15() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_reusableCells_15)); }
	inline Dictionary_2_t3496102114 * get_m_reusableCells_15() const { return ___m_reusableCells_15; }
	inline Dictionary_2_t3496102114 ** get_address_of_m_reusableCells_15() { return &___m_reusableCells_15; }
	inline void set_m_reusableCells_15(Dictionary_2_t3496102114 * value)
	{
		___m_reusableCells_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_reusableCells_15, value);
	}

	inline static int32_t get_offset_of_m_scrollY_16() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_scrollY_16)); }
	inline float get_m_scrollY_16() const { return ___m_scrollY_16; }
	inline float* get_address_of_m_scrollY_16() { return &___m_scrollY_16; }
	inline void set_m_scrollY_16(float value)
	{
		___m_scrollY_16 = value;
	}

	inline static int32_t get_offset_of_m_requiresRefresh_17() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___m_requiresRefresh_17)); }
	inline bool get_m_requiresRefresh_17() const { return ___m_requiresRefresh_17; }
	inline bool* get_address_of_m_requiresRefresh_17() { return &___m_requiresRefresh_17; }
	inline void set_m_requiresRefresh_17(bool value)
	{
		___m_requiresRefresh_17 = value;
	}

	inline static int32_t get_offset_of_U3CisEmptyU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(TableView_t3179510217, ___U3CisEmptyU3Ek__BackingField_18)); }
	inline bool get_U3CisEmptyU3Ek__BackingField_18() const { return ___U3CisEmptyU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CisEmptyU3Ek__BackingField_18() { return &___U3CisEmptyU3Ek__BackingField_18; }
	inline void set_U3CisEmptyU3Ek__BackingField_18(bool value)
	{
		___U3CisEmptyU3Ek__BackingField_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
