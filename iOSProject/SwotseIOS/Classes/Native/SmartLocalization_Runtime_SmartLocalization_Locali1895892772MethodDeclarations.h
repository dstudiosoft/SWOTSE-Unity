﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LocalizedObject
struct LocalizedObject_t1895892772;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_Locali3069815932.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void SmartLocalization.LocalizedObject::.ctor()
extern "C"  void LocalizedObject__ctor_m1033965030 (LocalizedObject_t1895892772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::get_ObjectType()
extern "C"  int32_t LocalizedObject_get_ObjectType_m1751508373 (LocalizedObject_t1895892772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ObjectType(SmartLocalization.LocalizedObjectType)
extern "C"  void LocalizedObject_set_ObjectType_m718675688 (LocalizedObject_t1895892772 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::get_TextValue()
extern "C"  String_t* LocalizedObject_get_TextValue_m4064263776 (LocalizedObject_t1895892772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_TextValue(System.String)
extern "C"  void LocalizedObject_set_TextValue_m852198621 (LocalizedObject_t1895892772 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisGameObject(UnityEngine.GameObject)
extern "C"  void LocalizedObject_set_ThisGameObject_m4213181670 (LocalizedObject_t1895892772 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisAudioClip(UnityEngine.AudioClip)
extern "C"  void LocalizedObject_set_ThisAudioClip_m50969550 (LocalizedObject_t1895892772 * __this, AudioClip_t1932558630 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_ThisTexture(UnityEngine.Texture)
extern "C"  void LocalizedObject_set_ThisTexture_m4259235630 (LocalizedObject_t1895892772 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LocalizedObject::get_OverrideLocalizedObject()
extern "C"  bool LocalizedObject_get_OverrideLocalizedObject_m3845687463 (LocalizedObject_t1895892772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_OverrideLocalizedObject(System.Boolean)
extern "C"  void LocalizedObject_set_OverrideLocalizedObject_m1069825542 (LocalizedObject_t1895892772 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::get_OverrideObjectLanguageCode()
extern "C"  String_t* LocalizedObject_get_OverrideObjectLanguageCode_m2324148578 (LocalizedObject_t1895892772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::set_OverrideObjectLanguageCode(System.String)
extern "C"  void LocalizedObject_set_OverrideObjectLanguageCode_m1681975025 (LocalizedObject_t1895892772 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::GetLocalizedObjectType(System.String)
extern "C"  int32_t LocalizedObject_GetLocalizedObjectType_m1358359011 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::GetCleanKey(System.String,SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetCleanKey_m1457188717 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LocalizedObject::GetLocalizedObjectTypeStringValue(SmartLocalization.LocalizedObjectType)
extern "C"  String_t* LocalizedObject_GetLocalizedObjectTypeStringValue_m1570887421 (Il2CppObject * __this /* static, unused */, int32_t ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedObject::.cctor()
extern "C"  void LocalizedObject__cctor_m1014498443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
