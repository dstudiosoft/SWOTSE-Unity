﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// IReloadable
struct IReloadable_t861412162;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableCell
struct  ShopTableCell_t4262618216  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text ShopTableCell::itemLabel1
	Text_t356221433 * ___itemLabel1_2;
	// UnityEngine.UI.Text ShopTableCell::itemDescription1
	Text_t356221433 * ___itemDescription1_3;
	// UnityEngine.UI.Text ShopTableCell::itemCount1
	Text_t356221433 * ___itemCount1_4;
	// UnityEngine.UI.Image ShopTableCell::itemImage1
	Image_t2042527209 * ___itemImage1_5;
	// UnityEngine.UI.Text ShopTableCell::itemCost1
	Text_t356221433 * ___itemCost1_6;
	// UnityEngine.UI.Text ShopTableCell::itemAction1
	Text_t356221433 * ___itemAction1_7;
	// UnityEngine.GameObject ShopTableCell::UseBtn1
	GameObject_t1756533147 * ___UseBtn1_8;
	// System.String ShopTableCell::itemId1
	String_t* ___itemId1_9;
	// System.String ShopTableCell::constantId1
	String_t* ___constantId1_10;
	// UnityEngine.UI.Text ShopTableCell::itemLabel2
	Text_t356221433 * ___itemLabel2_11;
	// UnityEngine.UI.Text ShopTableCell::itemDescription2
	Text_t356221433 * ___itemDescription2_12;
	// UnityEngine.UI.Text ShopTableCell::itemCount2
	Text_t356221433 * ___itemCount2_13;
	// UnityEngine.UI.Image ShopTableCell::itemImage2
	Image_t2042527209 * ___itemImage2_14;
	// UnityEngine.UI.Text ShopTableCell::itemCost2
	Text_t356221433 * ___itemCost2_15;
	// UnityEngine.UI.Text ShopTableCell::itemAction2
	Text_t356221433 * ___itemAction2_16;
	// UnityEngine.GameObject ShopTableCell::UseBtn2
	GameObject_t1756533147 * ___UseBtn2_17;
	// System.String ShopTableCell::itemId2
	String_t* ___itemId2_18;
	// System.String ShopTableCell::constantId2
	String_t* ___constantId2_19;
	// IReloadable ShopTableCell::owner
	Il2CppObject * ___owner_20;
	// System.String ShopTableCell::type
	String_t* ___type_21;
	// System.Int64 ShopTableCell::evendId
	int64_t ___evendId_22;

public:
	inline static int32_t get_offset_of_itemLabel1_2() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemLabel1_2)); }
	inline Text_t356221433 * get_itemLabel1_2() const { return ___itemLabel1_2; }
	inline Text_t356221433 ** get_address_of_itemLabel1_2() { return &___itemLabel1_2; }
	inline void set_itemLabel1_2(Text_t356221433 * value)
	{
		___itemLabel1_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemLabel1_2, value);
	}

	inline static int32_t get_offset_of_itemDescription1_3() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemDescription1_3)); }
	inline Text_t356221433 * get_itemDescription1_3() const { return ___itemDescription1_3; }
	inline Text_t356221433 ** get_address_of_itemDescription1_3() { return &___itemDescription1_3; }
	inline void set_itemDescription1_3(Text_t356221433 * value)
	{
		___itemDescription1_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemDescription1_3, value);
	}

	inline static int32_t get_offset_of_itemCount1_4() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemCount1_4)); }
	inline Text_t356221433 * get_itemCount1_4() const { return ___itemCount1_4; }
	inline Text_t356221433 ** get_address_of_itemCount1_4() { return &___itemCount1_4; }
	inline void set_itemCount1_4(Text_t356221433 * value)
	{
		___itemCount1_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemCount1_4, value);
	}

	inline static int32_t get_offset_of_itemImage1_5() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemImage1_5)); }
	inline Image_t2042527209 * get_itemImage1_5() const { return ___itemImage1_5; }
	inline Image_t2042527209 ** get_address_of_itemImage1_5() { return &___itemImage1_5; }
	inline void set_itemImage1_5(Image_t2042527209 * value)
	{
		___itemImage1_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage1_5, value);
	}

	inline static int32_t get_offset_of_itemCost1_6() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemCost1_6)); }
	inline Text_t356221433 * get_itemCost1_6() const { return ___itemCost1_6; }
	inline Text_t356221433 ** get_address_of_itemCost1_6() { return &___itemCost1_6; }
	inline void set_itemCost1_6(Text_t356221433 * value)
	{
		___itemCost1_6 = value;
		Il2CppCodeGenWriteBarrier(&___itemCost1_6, value);
	}

	inline static int32_t get_offset_of_itemAction1_7() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemAction1_7)); }
	inline Text_t356221433 * get_itemAction1_7() const { return ___itemAction1_7; }
	inline Text_t356221433 ** get_address_of_itemAction1_7() { return &___itemAction1_7; }
	inline void set_itemAction1_7(Text_t356221433 * value)
	{
		___itemAction1_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemAction1_7, value);
	}

	inline static int32_t get_offset_of_UseBtn1_8() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___UseBtn1_8)); }
	inline GameObject_t1756533147 * get_UseBtn1_8() const { return ___UseBtn1_8; }
	inline GameObject_t1756533147 ** get_address_of_UseBtn1_8() { return &___UseBtn1_8; }
	inline void set_UseBtn1_8(GameObject_t1756533147 * value)
	{
		___UseBtn1_8 = value;
		Il2CppCodeGenWriteBarrier(&___UseBtn1_8, value);
	}

	inline static int32_t get_offset_of_itemId1_9() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemId1_9)); }
	inline String_t* get_itemId1_9() const { return ___itemId1_9; }
	inline String_t** get_address_of_itemId1_9() { return &___itemId1_9; }
	inline void set_itemId1_9(String_t* value)
	{
		___itemId1_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemId1_9, value);
	}

	inline static int32_t get_offset_of_constantId1_10() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___constantId1_10)); }
	inline String_t* get_constantId1_10() const { return ___constantId1_10; }
	inline String_t** get_address_of_constantId1_10() { return &___constantId1_10; }
	inline void set_constantId1_10(String_t* value)
	{
		___constantId1_10 = value;
		Il2CppCodeGenWriteBarrier(&___constantId1_10, value);
	}

	inline static int32_t get_offset_of_itemLabel2_11() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemLabel2_11)); }
	inline Text_t356221433 * get_itemLabel2_11() const { return ___itemLabel2_11; }
	inline Text_t356221433 ** get_address_of_itemLabel2_11() { return &___itemLabel2_11; }
	inline void set_itemLabel2_11(Text_t356221433 * value)
	{
		___itemLabel2_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemLabel2_11, value);
	}

	inline static int32_t get_offset_of_itemDescription2_12() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemDescription2_12)); }
	inline Text_t356221433 * get_itemDescription2_12() const { return ___itemDescription2_12; }
	inline Text_t356221433 ** get_address_of_itemDescription2_12() { return &___itemDescription2_12; }
	inline void set_itemDescription2_12(Text_t356221433 * value)
	{
		___itemDescription2_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemDescription2_12, value);
	}

	inline static int32_t get_offset_of_itemCount2_13() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemCount2_13)); }
	inline Text_t356221433 * get_itemCount2_13() const { return ___itemCount2_13; }
	inline Text_t356221433 ** get_address_of_itemCount2_13() { return &___itemCount2_13; }
	inline void set_itemCount2_13(Text_t356221433 * value)
	{
		___itemCount2_13 = value;
		Il2CppCodeGenWriteBarrier(&___itemCount2_13, value);
	}

	inline static int32_t get_offset_of_itemImage2_14() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemImage2_14)); }
	inline Image_t2042527209 * get_itemImage2_14() const { return ___itemImage2_14; }
	inline Image_t2042527209 ** get_address_of_itemImage2_14() { return &___itemImage2_14; }
	inline void set_itemImage2_14(Image_t2042527209 * value)
	{
		___itemImage2_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage2_14, value);
	}

	inline static int32_t get_offset_of_itemCost2_15() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemCost2_15)); }
	inline Text_t356221433 * get_itemCost2_15() const { return ___itemCost2_15; }
	inline Text_t356221433 ** get_address_of_itemCost2_15() { return &___itemCost2_15; }
	inline void set_itemCost2_15(Text_t356221433 * value)
	{
		___itemCost2_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemCost2_15, value);
	}

	inline static int32_t get_offset_of_itemAction2_16() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemAction2_16)); }
	inline Text_t356221433 * get_itemAction2_16() const { return ___itemAction2_16; }
	inline Text_t356221433 ** get_address_of_itemAction2_16() { return &___itemAction2_16; }
	inline void set_itemAction2_16(Text_t356221433 * value)
	{
		___itemAction2_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemAction2_16, value);
	}

	inline static int32_t get_offset_of_UseBtn2_17() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___UseBtn2_17)); }
	inline GameObject_t1756533147 * get_UseBtn2_17() const { return ___UseBtn2_17; }
	inline GameObject_t1756533147 ** get_address_of_UseBtn2_17() { return &___UseBtn2_17; }
	inline void set_UseBtn2_17(GameObject_t1756533147 * value)
	{
		___UseBtn2_17 = value;
		Il2CppCodeGenWriteBarrier(&___UseBtn2_17, value);
	}

	inline static int32_t get_offset_of_itemId2_18() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___itemId2_18)); }
	inline String_t* get_itemId2_18() const { return ___itemId2_18; }
	inline String_t** get_address_of_itemId2_18() { return &___itemId2_18; }
	inline void set_itemId2_18(String_t* value)
	{
		___itemId2_18 = value;
		Il2CppCodeGenWriteBarrier(&___itemId2_18, value);
	}

	inline static int32_t get_offset_of_constantId2_19() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___constantId2_19)); }
	inline String_t* get_constantId2_19() const { return ___constantId2_19; }
	inline String_t** get_address_of_constantId2_19() { return &___constantId2_19; }
	inline void set_constantId2_19(String_t* value)
	{
		___constantId2_19 = value;
		Il2CppCodeGenWriteBarrier(&___constantId2_19, value);
	}

	inline static int32_t get_offset_of_owner_20() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___owner_20)); }
	inline Il2CppObject * get_owner_20() const { return ___owner_20; }
	inline Il2CppObject ** get_address_of_owner_20() { return &___owner_20; }
	inline void set_owner_20(Il2CppObject * value)
	{
		___owner_20 = value;
		Il2CppCodeGenWriteBarrier(&___owner_20, value);
	}

	inline static int32_t get_offset_of_type_21() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___type_21)); }
	inline String_t* get_type_21() const { return ___type_21; }
	inline String_t** get_address_of_type_21() { return &___type_21; }
	inline void set_type_21(String_t* value)
	{
		___type_21 = value;
		Il2CppCodeGenWriteBarrier(&___type_21, value);
	}

	inline static int32_t get_offset_of_evendId_22() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216, ___evendId_22)); }
	inline int64_t get_evendId_22() const { return ___evendId_22; }
	inline int64_t* get_address_of_evendId_22() { return &___evendId_22; }
	inline void set_evendId_22(int64_t value)
	{
		___evendId_22 = value;
	}
};

struct ShopTableCell_t4262618216_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableCell::<>f__switch$map1B
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1B_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ShopTableCell::<>f__switch$map1C
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1C_24;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1B_23() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216_StaticFields, ___U3CU3Ef__switchU24map1B_23)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1B_23() const { return ___U3CU3Ef__switchU24map1B_23; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1B_23() { return &___U3CU3Ef__switchU24map1B_23; }
	inline void set_U3CU3Ef__switchU24map1B_23(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1B_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1B_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1C_24() { return static_cast<int32_t>(offsetof(ShopTableCell_t4262618216_StaticFields, ___U3CU3Ef__switchU24map1C_24)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1C_24() const { return ___U3CU3Ef__switchU24map1C_24; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1C_24() { return &___U3CU3Ef__switchU24map1C_24; }
	inline void set_U3CU3Ef__switchU24map1C_24(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1C_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1C_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
