﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.PermissionSet
struct PermissionSet_t1941658161;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Type
struct Type_t;
// System.Security.SecurityElement
struct SecurityElement_t2325568386;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t43919632;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Security_Permissions_PermissionSta3557289502.h"
#include "mscorlib_System_Security_PermissionSet1941658161.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Security_SecurityElement2325568386.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Security_Policy_PolicyLevel43919632.h"

// System.Void System.Security.PermissionSet::.ctor()
extern "C"  void PermissionSet__ctor_m3440100522 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.String)
extern "C"  void PermissionSet__ctor_m4099742268 (PermissionSet_t1941658161 * __this, String_t* ___xml0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.Security.Permissions.PermissionState)
extern "C"  void PermissionSet__ctor_m3118504839 (PermissionSet_t1941658161 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.Security.PermissionSet)
extern "C"  void PermissionSet__ctor_m3936421588 (PermissionSet_t1941658161 * __this, PermissionSet_t1941658161 * ___permSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionSet::AddPermission(System.Security.IPermission)
extern "C"  Il2CppObject * PermissionSet_AddPermission_m105089679 (PermissionSet_t1941658161 * __this, Il2CppObject * ___perm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Assert()
extern "C"  void PermissionSet_Assert_m2399301258 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Copy()
extern "C"  PermissionSet_t1941658161 * PermissionSet_Copy_m2800465550 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Demand()
extern "C"  void PermissionSet_Demand_m2709283193 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::PermitOnly()
extern "C"  void PermissionSet_PermitOnly_m3170220053 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionSet::GetPermission(System.Type)
extern "C"  Il2CppObject * PermissionSet_GetPermission_m609165172 (PermissionSet_t1941658161 * __this, Type_t * ___permClass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Intersect(System.Security.PermissionSet)
extern "C"  PermissionSet_t1941658161 * PermissionSet_Intersect_m1732433896 (PermissionSet_t1941658161 * __this, PermissionSet_t1941658161 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Deny()
extern "C"  void PermissionSet_Deny_m618499554 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::FromXml(System.Security.SecurityElement)
extern "C"  void PermissionSet_FromXml_m2882734554 (PermissionSet_t1941658161 * __this, SecurityElement_t2325568386 * ___et0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::CopyTo(System.Array,System.Int32)
extern "C"  void PermissionSet_CopyTo_m3028011007 (PermissionSet_t1941658161 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.PermissionSet::ToXml()
extern "C"  SecurityElement_t2325568386 * PermissionSet_ToXml_m3312410464 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsSubsetOf(System.Security.PermissionSet)
extern "C"  bool PermissionSet_IsSubsetOf_m535103249 (PermissionSet_t1941658161 * __this, PermissionSet_t1941658161 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::SetReadOnly(System.Boolean)
extern "C"  void PermissionSet_SetReadOnly_m1151866929 (PermissionSet_t1941658161 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsUnrestricted()
extern "C"  bool PermissionSet_IsUnrestricted_m3966666674 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Union(System.Security.PermissionSet)
extern "C"  PermissionSet_t1941658161 * PermissionSet_Union_m2363188946 (PermissionSet_t1941658161 * __this, PermissionSet_t1941658161 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.PermissionSet::GetEnumerator()
extern "C"  Il2CppObject * PermissionSet_GetEnumerator_m4272244758 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.PermissionSet::get_Resolver()
extern "C"  PolicyLevel_t43919632 * PermissionSet_get_Resolver_m2271756985 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::set_Resolver(System.Security.Policy.PolicyLevel)
extern "C"  void PermissionSet_set_Resolver_m3778792926 (PermissionSet_t1941658161 * __this, PolicyLevel_t43919632 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::get_DeclarativeSecurity()
extern "C"  bool PermissionSet_get_DeclarativeSecurity_m394677187 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::set_DeclarativeSecurity(System.Boolean)
extern "C"  void PermissionSet_set_DeclarativeSecurity_m3993468766 (PermissionSet_t1941658161 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsEmpty()
extern "C"  bool PermissionSet_IsEmpty_m1041996493 (PermissionSet_t1941658161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::CreateFromBinaryFormat(System.Byte[])
extern "C"  PermissionSet_t1941658161 * PermissionSet_CreateFromBinaryFormat_m981771376 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
