﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConstructionPitManager
struct ConstructionPitManager_t99488877;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ConstructionPitManager::.ctor()
extern "C"  void ConstructionPitManager__ctor_m3031616884 (ConstructionPitManager_t99488877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionPitManager::OnEnable()
extern "C"  void ConstructionPitManager_OnEnable_m3861696620 (ConstructionPitManager_t99488877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionPitManager::UpdatePitState(System.Object,System.String)
extern "C"  void ConstructionPitManager_UpdatePitState_m1513024019 (ConstructionPitManager_t99488877 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionPitManager::OnMouseUpAsButton()
extern "C"  void ConstructionPitManager_OnMouseUpAsButton_m690950721 (ConstructionPitManager_t99488877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionPitManager::DisablePit()
extern "C"  void ConstructionPitManager_DisablePit_m4263015157 (ConstructionPitManager_t99488877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionPitManager::EnablePit()
extern "C"  void ConstructionPitManager_EnablePit_m2122439484 (ConstructionPitManager_t99488877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
