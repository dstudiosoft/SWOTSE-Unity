﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32_FIXED_INFO
struct Win32_FIXED_INFO_t1371335919;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
struct Win32_FIXED_INFO_t1371335919_marshaled_pinvoke;
struct Win32_FIXED_INFO_t1371335919_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.Win32_FIXED_INFO::.ctor()
extern "C"  void Win32_FIXED_INFO__ctor_m1139211445 (Win32_FIXED_INFO_t1371335919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32_FIXED_INFO::GetNetworkParams(System.Byte[],System.Int32&)
extern "C"  int32_t Win32_FIXED_INFO_GetNetworkParams_m1190983035 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t* ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.Win32_FIXED_INFO System.Net.NetworkInformation.Win32_FIXED_INFO::get_Instance()
extern "C"  Win32_FIXED_INFO_t1371335919 * Win32_FIXED_INFO_get_Instance_m1048441613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.Win32_FIXED_INFO System.Net.NetworkInformation.Win32_FIXED_INFO::GetInstance()
extern "C"  Win32_FIXED_INFO_t1371335919 * Win32_FIXED_INFO_GetInstance_m1739170628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Win32_FIXED_INFO_t1371335919;
struct Win32_FIXED_INFO_t1371335919_marshaled_pinvoke;

extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke(const Win32_FIXED_INFO_t1371335919& unmarshaled, Win32_FIXED_INFO_t1371335919_marshaled_pinvoke& marshaled);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke_back(const Win32_FIXED_INFO_t1371335919_marshaled_pinvoke& marshaled, Win32_FIXED_INFO_t1371335919& unmarshaled);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke_cleanup(Win32_FIXED_INFO_t1371335919_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Win32_FIXED_INFO_t1371335919;
struct Win32_FIXED_INFO_t1371335919_marshaled_com;

extern "C" void Win32_FIXED_INFO_t1371335919_marshal_com(const Win32_FIXED_INFO_t1371335919& unmarshaled, Win32_FIXED_INFO_t1371335919_marshaled_com& marshaled);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_com_back(const Win32_FIXED_INFO_t1371335919_marshaled_com& marshaled, Win32_FIXED_INFO_t1371335919& unmarshaled);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_com_cleanup(Win32_FIXED_INFO_t1371335919_marshaled_com& marshaled);
