﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResourceManager
struct ResourceManager_t4136494783;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"

// System.Void ResourceManager::.ctor()
extern "C"  void ResourceManager__ctor_m1852942078 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourceManager::add_onGetInitialResources(SimpleEvent)
extern "C"  void ResourceManager_add_onGetInitialResources_m3337047516 (ResourceManager_t4136494783 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourceManager::remove_onGetInitialResources(SimpleEvent)
extern "C"  void ResourceManager_remove_onGetInitialResources_m872191765 (ResourceManager_t4136494783 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::GetInitialReourcesForCity()
extern "C"  Il2CppObject * ResourceManager_GetInitialReourcesForCity_m1584975212 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourceManager::OnApplicationPause(System.Boolean)
extern "C"  void ResourceManager_OnApplicationPause_m1804041324 (ResourceManager_t4136494783 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResourceManager::StartResources()
extern "C"  void ResourceManager_StartResources_m683664587 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::getResources()
extern "C"  Il2CppObject * ResourceManager_getResources_m2908542555 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewArmyEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewArmyEvent_m2859265107 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewBuildingEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewBuildingEvent_m2697163278 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewUnitEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewUnitEvent_m24287732 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewCustomEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewCustomEvent_m463574923 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewWorldMapEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewWorldMapEvent_m2739534220 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsNewCityEvent()
extern "C"  Il2CppObject * ResourceManager_ReadIsNewCityEvent_m870841943 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadArmyEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadArmyEvents_m2096173601 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadBuildingEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadBuildingEvents_m3106817530 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadUnitEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadUnitEvents_m3571107772 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadCustomEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadCustomEvents_m2759525805 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadWorldMapEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadWorldMapEvents_m639305212 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResourceManager::ReadIsUnreadNewCityEvents()
extern "C"  Il2CppObject * ResourceManager_ReadIsUnreadNewCityEvents_m3506570133 (ResourceManager_t4136494783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
