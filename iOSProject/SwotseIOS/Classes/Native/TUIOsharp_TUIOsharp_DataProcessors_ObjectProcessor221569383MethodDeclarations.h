﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TUIOsharp.DataProcessors.ObjectProcessor
struct ObjectProcessor_t221569383;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct EventHandler_1_t402050513;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscMessage2764280154.h"

// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectAdded_m4092744429 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectAdded_m3070580308 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectUpdated_m2549856334 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectUpdated_m3226679647 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectRemoved_m3544020203 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectRemoved_m2344088226 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::set_FrameNumber(System.Int32)
extern "C"  void ObjectProcessor_set_FrameNumber_m3936044951 (ObjectProcessor_t221569383 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::.ctor()
extern "C"  void ObjectProcessor__ctor_m2046048849 (ObjectProcessor_t221569383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void ObjectProcessor_ProcessMessage_m1510623726 (ObjectProcessor_t221569383 * __this, OscMessage_t2764280154 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
