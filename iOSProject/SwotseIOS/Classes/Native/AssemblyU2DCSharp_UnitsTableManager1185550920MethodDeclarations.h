﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitsTableManager
struct UnitsTableManager_t1185550920;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnitsTableManager::.ctor()
extern "C"  void UnitsTableManager__ctor_m2014078127 (UnitsTableManager_t1185550920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsTableManager::OnEnable()
extern "C"  void UnitsTableManager_OnEnable_m1233376867 (UnitsTableManager_t1185550920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsTableManager::FixedUpdate()
extern "C"  void UnitsTableManager_FixedUpdate_m3876605608 (UnitsTableManager_t1185550920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnitsTableManager::Format(System.Int64)
extern "C"  String_t* UnitsTableManager_Format_m1107852103 (UnitsTableManager_t1185550920 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
