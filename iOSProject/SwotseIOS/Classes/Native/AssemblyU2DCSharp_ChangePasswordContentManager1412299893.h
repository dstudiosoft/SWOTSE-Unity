﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangePasswordContentManager
struct  ChangePasswordContentManager_t1412299893  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField ChangePasswordContentManager::oldPassword
	InputField_t1631627530 * ___oldPassword_2;
	// UnityEngine.UI.InputField ChangePasswordContentManager::newPassword
	InputField_t1631627530 * ___newPassword_3;
	// UnityEngine.UI.InputField ChangePasswordContentManager::confirmPassword
	InputField_t1631627530 * ___confirmPassword_4;

public:
	inline static int32_t get_offset_of_oldPassword_2() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t1412299893, ___oldPassword_2)); }
	inline InputField_t1631627530 * get_oldPassword_2() const { return ___oldPassword_2; }
	inline InputField_t1631627530 ** get_address_of_oldPassword_2() { return &___oldPassword_2; }
	inline void set_oldPassword_2(InputField_t1631627530 * value)
	{
		___oldPassword_2 = value;
		Il2CppCodeGenWriteBarrier(&___oldPassword_2, value);
	}

	inline static int32_t get_offset_of_newPassword_3() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t1412299893, ___newPassword_3)); }
	inline InputField_t1631627530 * get_newPassword_3() const { return ___newPassword_3; }
	inline InputField_t1631627530 ** get_address_of_newPassword_3() { return &___newPassword_3; }
	inline void set_newPassword_3(InputField_t1631627530 * value)
	{
		___newPassword_3 = value;
		Il2CppCodeGenWriteBarrier(&___newPassword_3, value);
	}

	inline static int32_t get_offset_of_confirmPassword_4() { return static_cast<int32_t>(offsetof(ChangePasswordContentManager_t1412299893, ___confirmPassword_4)); }
	inline InputField_t1631627530 * get_confirmPassword_4() const { return ___confirmPassword_4; }
	inline InputField_t1631627530 ** get_address_of_confirmPassword_4() { return &___confirmPassword_4; }
	inline void set_confirmPassword_4(InputField_t1631627530 * value)
	{
		___confirmPassword_4 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPassword_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
