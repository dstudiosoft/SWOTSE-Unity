﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.CameraLayer
struct CameraLayer_t464507322;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_HitTest_ObjectHi3057876522.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void TouchScript.Layers.CameraLayer::.ctor()
extern "C"  void CameraLayer__ctor_m1060667631 (CameraLayer_t464507322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayer::Awake()
extern "C"  void CameraLayer_Awake_m2943725122 (CameraLayer_t464507322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.CameraLayer::castRay(UnityEngine.Ray,TouchScript.Hit.TouchHit&)
extern "C"  int32_t CameraLayer_castRay_m54286803 (CameraLayer_t464507322 * __this, Ray_t2469606224  ___ray0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.HitTest/ObjectHitResult TouchScript.Layers.CameraLayer::doHit(UnityEngine.RaycastHit,TouchScript.Hit.TouchHit&)
extern "C"  int32_t CameraLayer_doHit_m2711338518 (CameraLayer_t464507322 * __this, RaycastHit_t87180320  ___raycastHit0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayer::sortHits(UnityEngine.RaycastHit[])
extern "C"  void CameraLayer_sortHits_m1914132082 (CameraLayer_t464507322 * __this, RaycastHitU5BU5D_t1214023521* ___hits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
