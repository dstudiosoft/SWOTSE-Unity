﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.LinuxGatewayIPAddressInformationCollection
struct LinuxGatewayIPAddressInformationCollection_t2323845969;
// System.Net.NetworkInformation.IPAddressCollection
struct IPAddressCollection_t2986660307;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_IPAddressColl2986660307.h"

// System.Void System.Net.NetworkInformation.LinuxGatewayIPAddressInformationCollection::.ctor(System.Boolean)
extern "C"  void LinuxGatewayIPAddressInformationCollection__ctor_m2547854322 (LinuxGatewayIPAddressInformationCollection_t2323845969 * __this, bool ___isReadOnly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.LinuxGatewayIPAddressInformationCollection::.ctor(System.Net.NetworkInformation.IPAddressCollection)
extern "C"  void LinuxGatewayIPAddressInformationCollection__ctor_m165845022 (LinuxGatewayIPAddressInformationCollection_t2323845969 * __this, IPAddressCollection_t2986660307 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.LinuxGatewayIPAddressInformationCollection::.cctor()
extern "C"  void LinuxGatewayIPAddressInformationCollection__cctor_m3656361370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.LinuxGatewayIPAddressInformationCollection::get_IsReadOnly()
extern "C"  bool LinuxGatewayIPAddressInformationCollection_get_IsReadOnly_m3171501324 (LinuxGatewayIPAddressInformationCollection_t2323845969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
