﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Ping
struct Ping_t3611444498;
// System.Net.NetworkInformation.PingCompletedEventHandler
struct PingCompletedEventHandler_t1044526567;
// System.Net.NetworkInformation.PingCompletedEventArgs
struct PingCompletedEventArgs_t841787612;
// System.Net.NetworkInformation.PingReply
struct PingReply_t1978568794;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Net.NetworkInformation.PingOptions
struct PingOptions_t1261881264;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_PingCompleted1044526567.h"
#include "System_System_Net_NetworkInformation_Ping_cap_user1012002510.h"
#include "System_System_Net_NetworkInformation_Ping_cap_user2417848831.h"
#include "System_System_Net_NetworkInformation_PingCompletedE841787612.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_NetworkInformation_PingOptions1261881264.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.NetworkInformation.Ping::.ctor()
extern "C"  void Ping__ctor_m1925477326 (Ping_t3611444498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::.cctor()
extern "C"  void Ping__cctor_m2539700127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::add_PingCompleted(System.Net.NetworkInformation.PingCompletedEventHandler)
extern "C"  void Ping_add_PingCompleted_m2647862356 (Ping_t3611444498 * __this, PingCompletedEventHandler_t1044526567 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::remove_PingCompleted(System.Net.NetworkInformation.PingCompletedEventHandler)
extern "C"  void Ping_remove_PingCompleted_m1847751419 (Ping_t3611444498 * __this, PingCompletedEventHandler_t1044526567 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::System.IDisposable.Dispose()
extern "C"  void Ping_System_IDisposable_Dispose_m3184217403 (Ping_t3611444498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Ping::capget(System.Net.NetworkInformation.Ping/cap_user_header_t&,System.Net.NetworkInformation.Ping/cap_user_data_t&)
extern "C"  int32_t Ping_capget_m502211985 (Il2CppObject * __this /* static, unused */, cap_user_header_t_t1012002510 * ___header0, cap_user_data_t_t2417848831 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::CheckLinuxCapabilities()
extern "C"  void Ping_CheckLinuxCapabilities_m207690028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::OnPingCompleted(System.Net.NetworkInformation.PingCompletedEventArgs)
extern "C"  void Ping_OnPingCompleted_m3567368730 (Ping_t3611444498 * __this, PingCompletedEventArgs_t841787612 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.Net.IPAddress)
extern "C"  PingReply_t1978568794 * Ping_Send_m3045095248 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.Net.IPAddress,System.Int32)
extern "C"  PingReply_t1978568794 * Ping_Send_m670222487 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.Net.IPAddress,System.Int32,System.Byte[])
extern "C"  PingReply_t1978568794 * Ping_Send_m3957161110 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.String)
extern "C"  PingReply_t1978568794 * Ping_Send_m2520883769 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.String,System.Int32)
extern "C"  PingReply_t1978568794 * Ping_Send_m430078730 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.String,System.Int32,System.Byte[])
extern "C"  PingReply_t1978568794 * Ping_Send_m2863308795 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.String,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions)
extern "C"  PingReply_t1978568794 * Ping_Send_m3904038265 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.Ping::GetNonLoopbackIP()
extern "C"  IPAddress_t1399971723 * Ping_GetNonLoopbackIP_m1361729525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::Send(System.Net.IPAddress,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions)
extern "C"  PingReply_t1978568794 * Ping_Send_m1870041180 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::SendPrivileged(System.Net.IPAddress,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions)
extern "C"  PingReply_t1978568794 * Ping_SendPrivileged_m4224171413 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.Ping::SendUnprivileged(System.Net.IPAddress,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions)
extern "C"  PingReply_t1978568794 * Ping_SendUnprivileged_m782055654 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.Net.IPAddress,System.Int32,System.Byte[],System.Object)
extern "C"  void Ping_SendAsync_m191990203 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.Net.IPAddress,System.Int32,System.Object)
extern "C"  void Ping_SendAsync_m615991308 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.Net.IPAddress,System.Object)
extern "C"  void Ping_SendAsync_m2397591729 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.String,System.Int32,System.Byte[],System.Object)
extern "C"  void Ping_SendAsync_m1874741654 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.String,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions,System.Object)
extern "C"  void Ping_SendAsync_m3737224288 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, Il2CppObject * ___userToken4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.String,System.Int32,System.Object)
extern "C"  void Ping_SendAsync_m3495096443 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, int32_t ___timeout1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.String,System.Object)
extern "C"  void Ping_SendAsync_m3066604804 (Ping_t3611444498 * __this, String_t* ___hostNameOrAddress0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsync(System.Net.IPAddress,System.Int32,System.Byte[],System.Net.NetworkInformation.PingOptions,System.Object)
extern "C"  void Ping_SendAsync_m1654762621 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, ByteU5BU5D_t3397334013* ___buffer2, PingOptions_t1261881264 * ___options3, Il2CppObject * ___userToken4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.Ping::SendAsyncCancel()
extern "C"  void Ping_SendAsyncCancel_m3674919158 (Ping_t3611444498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkInformation.Ping::BuildPingArgs(System.Net.IPAddress,System.Int32,System.Net.NetworkInformation.PingOptions)
extern "C"  String_t* Ping_BuildPingArgs_m2981963704 (Ping_t3611444498 * __this, IPAddress_t1399971723 * ___address0, int32_t ___timeout1, PingOptions_t1261881264 * ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
