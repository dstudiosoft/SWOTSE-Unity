﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.StringStringMap
struct StringStringMap_t3841415930;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t1360691296;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t3471835840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.StringStringMap/StringStringMapEnumerator
struct StringStringMapEnumerator_t2508848358;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_StringStringMap3841415930.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"

// System.Void Firebase.StringStringMap::.ctor(System.IntPtr,System.Boolean)
extern "C"  void StringStringMap__ctor_m1528426347 (StringStringMap_t3841415930 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::.ctor()
extern "C"  void StringStringMap__ctor_m294485360 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::.ctor(Firebase.StringStringMap)
extern "C"  void StringStringMap__ctor_m3680557453 (StringStringMap_t3841415930 * __this, StringStringMap_t3841415930 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.StringStringMap::getCPtr(Firebase.StringStringMap)
extern "C"  HandleRef_t2419939847  StringStringMap_getCPtr_m2689010030 (Il2CppObject * __this /* static, unused */, StringStringMap_t3841415930 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::Finalize()
extern "C"  void StringStringMap_Finalize_m866199736 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::Dispose()
extern "C"  void StringStringMap_Dispose_m1864036961 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringStringMap::get_Item(System.String)
extern "C"  String_t* StringStringMap_get_Item_m240457957 (StringStringMap_t3841415930 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::set_Item(System.String,System.String)
extern "C"  void StringStringMap_set_Item_m2249582586 (StringStringMap_t3841415930 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::TryGetValue(System.String,System.String&)
extern "C"  bool StringStringMap_TryGetValue_m4192204004 (StringStringMap_t3841415930 * __this, String_t* ___key0, String_t** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.StringStringMap::get_Count()
extern "C"  int32_t StringStringMap_get_Count_m2007738010 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::get_IsReadOnly()
extern "C"  bool StringStringMap_get_IsReadOnly_m1434826059 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Keys()
extern "C"  Il2CppObject* StringStringMap_get_Keys_m4236543489 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Values()
extern "C"  Il2CppObject* StringStringMap_get_Values_m2067770953 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  void StringStringMap_Add_m2252192898 (StringStringMap_t3841415930 * __this, KeyValuePair_2_t1701344717  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  bool StringStringMap_Remove_m1965945777 (StringStringMap_t3841415930 * __this, KeyValuePair_2_t1701344717  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern "C"  bool StringStringMap_Contains_m3923290856 (StringStringMap_t3841415930 * __this, KeyValuePair_2_t1701344717  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.String>[])
extern "C"  void StringStringMap_CopyTo_m1706911831 (StringStringMap_t3841415930 * __this, KeyValuePair_2U5BU5D_t1360691296* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.String>[],System.Int32)
extern "C"  void StringStringMap_CopyTo_m1925273462 (StringStringMap_t3841415930 * __this, KeyValuePair_2U5BU5D_t1360691296* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Firebase.StringStringMap::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<string,string>>.GetEnumerator()
extern "C"  Il2CppObject* StringStringMap_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CstringU3EU3E_GetEnumerator_m1211929826 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.StringStringMap::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * StringStringMap_System_Collections_IEnumerable_GetEnumerator_m545187047 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.StringStringMap/StringStringMapEnumerator Firebase.StringStringMap::GetEnumerator()
extern "C"  StringStringMapEnumerator_t2508848358 * StringStringMap_GetEnumerator_m4088880379 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.StringStringMap::size()
extern "C"  uint32_t StringStringMap_size_m1210509766 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::empty()
extern "C"  bool StringStringMap_empty_m3732117951 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::Clear()
extern "C"  void StringStringMap_Clear_m3640059667 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringStringMap::getitem(System.String)
extern "C"  String_t* StringStringMap_getitem_m1111612490 (StringStringMap_t3841415930 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::setitem(System.String,System.String)
extern "C"  void StringStringMap_setitem_m468299065 (StringStringMap_t3841415930 * __this, String_t* ___key0, String_t* ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::ContainsKey(System.String)
extern "C"  bool StringStringMap_ContainsKey_m4076023916 (StringStringMap_t3841415930 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::Add(System.String,System.String)
extern "C"  void StringStringMap_Add_m1670427519 (StringStringMap_t3841415930 * __this, String_t* ___key0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringStringMap::Remove(System.String)
extern "C"  bool StringStringMap_Remove_m3765976846 (StringStringMap_t3841415930 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.StringStringMap::create_iterator_begin()
extern "C"  IntPtr_t StringStringMap_create_iterator_begin_m4055175206 (StringStringMap_t3841415930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringStringMap::get_next_key(System.IntPtr)
extern "C"  String_t* StringStringMap_get_next_key_m423500327 (StringStringMap_t3841415930 * __this, IntPtr_t ___swigiterator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringStringMap::destroy_iterator(System.IntPtr)
extern "C"  void StringStringMap_destroy_iterator_m1696580431 (StringStringMap_t3841415930 * __this, IntPtr_t ___swigiterator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
