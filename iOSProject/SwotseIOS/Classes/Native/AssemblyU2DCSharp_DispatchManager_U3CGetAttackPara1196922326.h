﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnitsModel
struct UnitsModel_t1926818124;
// ResourcesModel
struct ResourcesModel_t2978985958;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// DispatchManager
struct DispatchManager_t870178121;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DispatchManager/<GetAttackParams>c__IteratorA
struct  U3CGetAttackParamsU3Ec__IteratorA_t1196922326  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm DispatchManager/<GetAttackParams>c__IteratorA::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// System.String DispatchManager/<GetAttackParams>c__IteratorA::attcakType
	String_t* ___attcakType_1;
	// UnitsModel DispatchManager/<GetAttackParams>c__IteratorA::army
	UnitsModel_t1926818124 * ___army_2;
	// ResourcesModel DispatchManager/<GetAttackParams>c__IteratorA::resources
	ResourcesModel_t2978985958 * ___resources_3;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::destX
	int64_t ___destX_4;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::destY
	int64_t ___destY_5;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::knightId
	int64_t ___knightId_6;
	// UnityEngine.Networking.UnityWebRequest DispatchManager/<GetAttackParams>c__IteratorA::<request>__1
	UnityWebRequest_t254341728 * ___U3CrequestU3E__1_7;
	// JSONObject DispatchManager/<GetAttackParams>c__IteratorA::<obj>__2
	JSONObject_t1971882247 * ___U3CobjU3E__2_8;
	// System.Int32 DispatchManager/<GetAttackParams>c__IteratorA::$PC
	int32_t ___U24PC_9;
	// System.Object DispatchManager/<GetAttackParams>c__IteratorA::$current
	Il2CppObject * ___U24current_10;
	// System.String DispatchManager/<GetAttackParams>c__IteratorA::<$>attcakType
	String_t* ___U3CU24U3EattcakType_11;
	// UnitsModel DispatchManager/<GetAttackParams>c__IteratorA::<$>army
	UnitsModel_t1926818124 * ___U3CU24U3Earmy_12;
	// ResourcesModel DispatchManager/<GetAttackParams>c__IteratorA::<$>resources
	ResourcesModel_t2978985958 * ___U3CU24U3Eresources_13;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::<$>destX
	int64_t ___U3CU24U3EdestX_14;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::<$>destY
	int64_t ___U3CU24U3EdestY_15;
	// System.Int64 DispatchManager/<GetAttackParams>c__IteratorA::<$>knightId
	int64_t ___U3CU24U3EknightId_16;
	// DispatchManager DispatchManager/<GetAttackParams>c__IteratorA::<>f__this
	DispatchManager_t870178121 * ___U3CU3Ef__this_17;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_attcakType_1() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___attcakType_1)); }
	inline String_t* get_attcakType_1() const { return ___attcakType_1; }
	inline String_t** get_address_of_attcakType_1() { return &___attcakType_1; }
	inline void set_attcakType_1(String_t* value)
	{
		___attcakType_1 = value;
		Il2CppCodeGenWriteBarrier(&___attcakType_1, value);
	}

	inline static int32_t get_offset_of_army_2() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___army_2)); }
	inline UnitsModel_t1926818124 * get_army_2() const { return ___army_2; }
	inline UnitsModel_t1926818124 ** get_address_of_army_2() { return &___army_2; }
	inline void set_army_2(UnitsModel_t1926818124 * value)
	{
		___army_2 = value;
		Il2CppCodeGenWriteBarrier(&___army_2, value);
	}

	inline static int32_t get_offset_of_resources_3() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___resources_3)); }
	inline ResourcesModel_t2978985958 * get_resources_3() const { return ___resources_3; }
	inline ResourcesModel_t2978985958 ** get_address_of_resources_3() { return &___resources_3; }
	inline void set_resources_3(ResourcesModel_t2978985958 * value)
	{
		___resources_3 = value;
		Il2CppCodeGenWriteBarrier(&___resources_3, value);
	}

	inline static int32_t get_offset_of_destX_4() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___destX_4)); }
	inline int64_t get_destX_4() const { return ___destX_4; }
	inline int64_t* get_address_of_destX_4() { return &___destX_4; }
	inline void set_destX_4(int64_t value)
	{
		___destX_4 = value;
	}

	inline static int32_t get_offset_of_destY_5() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___destY_5)); }
	inline int64_t get_destY_5() const { return ___destY_5; }
	inline int64_t* get_address_of_destY_5() { return &___destY_5; }
	inline void set_destY_5(int64_t value)
	{
		___destY_5 = value;
	}

	inline static int32_t get_offset_of_knightId_6() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___knightId_6)); }
	inline int64_t get_knightId_6() const { return ___knightId_6; }
	inline int64_t* get_address_of_knightId_6() { return &___knightId_6; }
	inline void set_knightId_6(int64_t value)
	{
		___knightId_6 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_7() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CrequestU3E__1_7)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__1_7() const { return ___U3CrequestU3E__1_7; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__1_7() { return &___U3CrequestU3E__1_7; }
	inline void set_U3CrequestU3E__1_7(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_7, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_8() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CobjU3E__2_8)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__2_8() const { return ___U3CobjU3E__2_8; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__2_8() { return &___U3CobjU3E__2_8; }
	inline void set_U3CobjU3E__2_8(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EattcakType_11() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3EattcakType_11)); }
	inline String_t* get_U3CU24U3EattcakType_11() const { return ___U3CU24U3EattcakType_11; }
	inline String_t** get_address_of_U3CU24U3EattcakType_11() { return &___U3CU24U3EattcakType_11; }
	inline void set_U3CU24U3EattcakType_11(String_t* value)
	{
		___U3CU24U3EattcakType_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EattcakType_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earmy_12() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3Earmy_12)); }
	inline UnitsModel_t1926818124 * get_U3CU24U3Earmy_12() const { return ___U3CU24U3Earmy_12; }
	inline UnitsModel_t1926818124 ** get_address_of_U3CU24U3Earmy_12() { return &___U3CU24U3Earmy_12; }
	inline void set_U3CU24U3Earmy_12(UnitsModel_t1926818124 * value)
	{
		___U3CU24U3Earmy_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Earmy_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eresources_13() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3Eresources_13)); }
	inline ResourcesModel_t2978985958 * get_U3CU24U3Eresources_13() const { return ___U3CU24U3Eresources_13; }
	inline ResourcesModel_t2978985958 ** get_address_of_U3CU24U3Eresources_13() { return &___U3CU24U3Eresources_13; }
	inline void set_U3CU24U3Eresources_13(ResourcesModel_t2978985958 * value)
	{
		___U3CU24U3Eresources_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eresources_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EdestX_14() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3EdestX_14)); }
	inline int64_t get_U3CU24U3EdestX_14() const { return ___U3CU24U3EdestX_14; }
	inline int64_t* get_address_of_U3CU24U3EdestX_14() { return &___U3CU24U3EdestX_14; }
	inline void set_U3CU24U3EdestX_14(int64_t value)
	{
		___U3CU24U3EdestX_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EdestY_15() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3EdestY_15)); }
	inline int64_t get_U3CU24U3EdestY_15() const { return ___U3CU24U3EdestY_15; }
	inline int64_t* get_address_of_U3CU24U3EdestY_15() { return &___U3CU24U3EdestY_15; }
	inline void set_U3CU24U3EdestY_15(int64_t value)
	{
		___U3CU24U3EdestY_15 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EknightId_16() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU24U3EknightId_16)); }
	inline int64_t get_U3CU24U3EknightId_16() const { return ___U3CU24U3EknightId_16; }
	inline int64_t* get_address_of_U3CU24U3EknightId_16() { return &___U3CU24U3EknightId_16; }
	inline void set_U3CU24U3EknightId_16(int64_t value)
	{
		___U3CU24U3EknightId_16 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_17() { return static_cast<int32_t>(offsetof(U3CGetAttackParamsU3Ec__IteratorA_t1196922326, ___U3CU3Ef__this_17)); }
	inline DispatchManager_t870178121 * get_U3CU3Ef__this_17() const { return ___U3CU3Ef__this_17; }
	inline DispatchManager_t870178121 ** get_address_of_U3CU3Ef__this_17() { return &___U3CU3Ef__this_17; }
	inline void set_U3CU3Ef__this_17(DispatchManager_t870178121 * value)
	{
		___U3CU3Ef__this_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
