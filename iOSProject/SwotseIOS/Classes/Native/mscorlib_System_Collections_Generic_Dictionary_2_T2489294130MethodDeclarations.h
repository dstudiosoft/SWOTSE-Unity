﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Transform_1_t2489294130;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m231675116_gshared (Transform_1_t2489294130 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m231675116(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2489294130 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m231675116_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Invoke(TKey,TValue)
extern "C"  TouchState_t2732082299  Transform_1_Invoke_m3360472388_gshared (Transform_1_t2489294130 * __this, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3360472388(__this, ___key0, ___value1, method) ((  TouchState_t2732082299  (*) (Transform_1_t2489294130 *, int32_t, TouchState_t2732082299 , const MethodInfo*))Transform_1_Invoke_m3360472388_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2097386423_gshared (Transform_1_t2489294130 * __this, int32_t ___key0, TouchState_t2732082299  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2097386423(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2489294130 *, int32_t, TouchState_t2732082299 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2097386423_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::EndInvoke(System.IAsyncResult)
extern "C"  TouchState_t2732082299  Transform_1_EndInvoke_m348860290_gshared (Transform_1_t2489294130 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m348860290(__this, ___result0, method) ((  TouchState_t2732082299  (*) (Transform_1_t2489294130 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m348860290_gshared)(__this, ___result0, method)
