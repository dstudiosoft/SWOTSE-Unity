﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Object>
struct U3CContinueWithU3Ec__AnonStorey1_t2359228914;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1__ctor_m2130157112_gshared (U3CContinueWithU3Ec__AnonStorey1_t2359228914 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1__ctor_m2130157112(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t2359228914 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1__ctor_m2130157112_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Object>::<>m__0()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m3237545381_gshared (U3CContinueWithU3Ec__AnonStorey1_t2359228914 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m3237545381(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t2359228914 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m3237545381_gshared)(__this, method)
