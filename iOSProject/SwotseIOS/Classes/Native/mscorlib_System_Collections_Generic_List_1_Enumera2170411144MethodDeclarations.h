﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2476527979(__this, ___l0, method) ((  void (*) (Enumerator_t2170411144 *, List_1_t2635681470 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1891475647(__this, method) ((  void (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m694932643(__this, method) ((  Il2CppObject * (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::Dispose()
#define Enumerator_Dispose_m485910830(__this, method) ((  void (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::VerifyState()
#define Enumerator_VerifyState_m3961488149(__this, method) ((  void (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::MoveNext()
#define Enumerator_MoveNext_m1054574395(__this, method) ((  bool (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TouchScript.InputSources.IInputSource>::get_Current()
#define Enumerator_get_Current_m1944886760(__this, method) ((  Il2CppObject * (*) (Enumerator_t2170411144 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
