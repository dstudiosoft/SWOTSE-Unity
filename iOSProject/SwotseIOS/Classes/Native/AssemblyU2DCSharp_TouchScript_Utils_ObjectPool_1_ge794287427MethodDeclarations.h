﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_g2042738142MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::.ctor(System.Int32,TouchScript.Utils.ObjectPool`1/UnityFunc`1<T,T>,UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m2311904092(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method) ((  void (*) (ObjectPool_1_t794287427 *, int32_t, UnityFunc_1_t4236629549 *, UnityAction_1_t2807584331 *, UnityAction_1_t2807584331 *, const MethodInfo*))ObjectPool_1__ctor_m3649530527_gshared)(__this, ___capacity0, ___actionNew1, ___actionOnGet2, ___actionOnRelease3, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_CountAll()
#define ObjectPool_1_get_CountAll_m214581132(__this, method) ((  int32_t (*) (ObjectPool_1_t794287427 *, const MethodInfo*))ObjectPool_1_get_CountAll_m1681014435_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::set_CountAll(System.Int32)
#define ObjectPool_1_set_CountAll_m2572987169(__this, ___value0, method) ((  void (*) (ObjectPool_1_t794287427 *, int32_t, const MethodInfo*))ObjectPool_1_set_CountAll_m122827726_gshared)(__this, ___value0, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_CountActive()
#define ObjectPool_1_get_CountActive_m1031830843(__this, method) ((  int32_t (*) (ObjectPool_1_t794287427 *, const MethodInfo*))ObjectPool_1_get_CountActive_m3063653714_gshared)(__this, method)
// System.Int32 TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_CountInactive()
#define ObjectPool_1_get_CountInactive_m3858041578(__this, method) ((  int32_t (*) (ObjectPool_1_t794287427 *, const MethodInfo*))ObjectPool_1_get_CountInactive_m1369084401_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::WarmUp(System.Int32)
#define ObjectPool_1_WarmUp_m1551379396(__this, ___count0, method) ((  void (*) (ObjectPool_1_t794287427 *, int32_t, const MethodInfo*))ObjectPool_1_WarmUp_m1349419031_gshared)(__this, ___count0, method)
// T TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Get()
#define ObjectPool_1_Get_m1620771188(__this, method) ((  List_1_t1440998580 * (*) (ObjectPool_1_t794287427 *, const MethodInfo*))ObjectPool_1_Get_m2843288903_gshared)(__this, method)
// System.Void TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Release(T)
#define ObjectPool_1_Release_m2960345078(__this, ___element0, method) ((  void (*) (ObjectPool_1_t794287427 *, List_1_t1440998580 *, const MethodInfo*))ObjectPool_1_Release_m1491729219_gshared)(__this, ___element0, method)
