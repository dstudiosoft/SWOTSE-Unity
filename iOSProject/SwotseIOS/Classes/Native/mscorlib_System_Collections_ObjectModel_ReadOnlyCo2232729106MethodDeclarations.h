﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m450008424(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2526456796(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m485513656(__this, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4158301343(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1713113175(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m626208387(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m36892483(__this, ___index0, method) ((  TuioBlob_t2046943414 * (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3521227958(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m606455102(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3681447217(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m199253178(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3828536213(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1199541745(__this, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2217384801(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1143357795(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m583720228(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1183668678(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1940001882(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3042281661(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2399573617(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2207073740(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1981862593(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2998128296(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3411896347(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::Contains(T)
#define ReadOnlyCollection_1_Contains_m916610266(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2232729106 *, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m2179892592(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2232729106 *, TuioBlobU5BU5D_t3918161843*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m4172272277(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1398834050(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2232729106 *, TuioBlob_t2046943414 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::get_Count()
#define ReadOnlyCollection_1_get_Count_m714752789(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2232729106 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<TUIOsharp.Entities.TuioBlob>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m2668196359(__this, ___index0, method) ((  TuioBlob_t2046943414 * (*) (ReadOnlyCollection_1_t2232729106 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
