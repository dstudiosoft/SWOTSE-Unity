﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UpgradeBuildingTableController
struct UpgradeBuildingTableController_t49696016;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeBuildingTableLine
struct  UpgradeBuildingTableLine_t750928550  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Image UpgradeBuildingTableLine::buildingIcon
	Image_t2042527209 * ___buildingIcon_2;
	// UnityEngine.UI.Text UpgradeBuildingTableLine::buildingName
	Text_t356221433 * ___buildingName_3;
	// UnityEngine.UI.Text UpgradeBuildingTableLine::buildingLevel
	Text_t356221433 * ___buildingLevel_4;
	// UpgradeBuildingTableController UpgradeBuildingTableLine::owner
	UpgradeBuildingTableController_t49696016 * ___owner_5;
	// System.Int64 UpgradeBuildingTableLine::id
	int64_t ___id_6;

public:
	inline static int32_t get_offset_of_buildingIcon_2() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t750928550, ___buildingIcon_2)); }
	inline Image_t2042527209 * get_buildingIcon_2() const { return ___buildingIcon_2; }
	inline Image_t2042527209 ** get_address_of_buildingIcon_2() { return &___buildingIcon_2; }
	inline void set_buildingIcon_2(Image_t2042527209 * value)
	{
		___buildingIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___buildingIcon_2, value);
	}

	inline static int32_t get_offset_of_buildingName_3() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t750928550, ___buildingName_3)); }
	inline Text_t356221433 * get_buildingName_3() const { return ___buildingName_3; }
	inline Text_t356221433 ** get_address_of_buildingName_3() { return &___buildingName_3; }
	inline void set_buildingName_3(Text_t356221433 * value)
	{
		___buildingName_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildingName_3, value);
	}

	inline static int32_t get_offset_of_buildingLevel_4() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t750928550, ___buildingLevel_4)); }
	inline Text_t356221433 * get_buildingLevel_4() const { return ___buildingLevel_4; }
	inline Text_t356221433 ** get_address_of_buildingLevel_4() { return &___buildingLevel_4; }
	inline void set_buildingLevel_4(Text_t356221433 * value)
	{
		___buildingLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___buildingLevel_4, value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t750928550, ___owner_5)); }
	inline UpgradeBuildingTableController_t49696016 * get_owner_5() const { return ___owner_5; }
	inline UpgradeBuildingTableController_t49696016 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(UpgradeBuildingTableController_t49696016 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t750928550, ___id_6)); }
	inline int64_t get_id_6() const { return ___id_6; }
	inline int64_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int64_t value)
	{
		___id_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
