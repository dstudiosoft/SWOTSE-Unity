﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelKnightTableLine
struct PanelKnightTableLine_t1314777363;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PanelKnightTableLine::.ctor()
extern "C"  void PanelKnightTableLine__ctor_m678672342 (PanelKnightTableLine_t1314777363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightTableLine::SetKnightName(System.String)
extern "C"  void PanelKnightTableLine_SetKnightName_m333136966 (PanelKnightTableLine_t1314777363 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightTableLine::SetLevel(System.Int64)
extern "C"  void PanelKnightTableLine_SetLevel_m1183415138 (PanelKnightTableLine_t1314777363 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightTableLine::SetStatus(System.String)
extern "C"  void PanelKnightTableLine_SetStatus_m4130167506 (PanelKnightTableLine_t1314777363 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightTableLine::SetKnightId(System.Int64)
extern "C"  void PanelKnightTableLine_SetKnightId_m349873290 (PanelKnightTableLine_t1314777363 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelKnightTableLine::OpenKnightStats()
extern "C"  void PanelKnightTableLine_OpenKnightStats_m2341835682 (PanelKnightTableLine_t1314777363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
