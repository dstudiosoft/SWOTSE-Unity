﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>
struct ReadOnlyCollection_1_t4249694466;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit2D>
struct IList_1_t309882079;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit2D>
struct IEnumerator_1_t1539432601;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3269834215_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3269834215(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3269834215_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m961437445_gshared (ReadOnlyCollection_1_t4249694466 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m961437445(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m961437445_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3503639537_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3503639537(__this, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3503639537_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3385003434_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3385003434(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3385003434_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m447955944_gshared (ReadOnlyCollection_1_t4249694466 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m447955944(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m447955944_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3062183518_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3062183518(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3062183518_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  RaycastHit2D_t4063908774  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3430269834_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3430269834(__this, ___index0, method) ((  RaycastHit2D_t4063908774  (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3430269834_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2206501133_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2206501133(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2206501133_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m182117897_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m182117897(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m182117897_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m338207996_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m338207996(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m338207996_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m285149361_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m285149361(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m285149361_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3766846208_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3766846208(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3766846208_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m768824086_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m768824086(__this, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m768824086_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m319299554_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m319299554(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m319299554_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2862941746_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2862941746(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2862941746_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2751350717_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2751350717(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2751350717_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1236124165_gshared (ReadOnlyCollection_1_t4249694466 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1236124165(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1236124165_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m907789179_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m907789179(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m907789179_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2135280196_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2135280196(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2135280196_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3283439554_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3283439554(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3283439554_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2294010233_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2294010233(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2294010233_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1252885632_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1252885632(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1252885632_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m120358229_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m120358229(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m120358229_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2940528262_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2940528262(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2940528262_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m973497687_gshared (ReadOnlyCollection_1_t4249694466 * __this, RaycastHit2D_t4063908774  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m973497687(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4249694466 *, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_Contains_m973497687_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m821462113_gshared (ReadOnlyCollection_1_t4249694466 * __this, RaycastHit2DU5BU5D_t4176517891* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m821462113(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4249694466 *, RaycastHit2DU5BU5D_t4176517891*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m821462113_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m118166868_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m118166868(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m118166868_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4111253649_gshared (ReadOnlyCollection_1_t4249694466 * __this, RaycastHit2D_t4063908774  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4111253649(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4249694466 *, RaycastHit2D_t4063908774 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4111253649_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3473707584_gshared (ReadOnlyCollection_1_t4249694466 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3473707584(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4249694466 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3473707584_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit2D>::get_Item(System.Int32)
extern "C"  RaycastHit2D_t4063908774  ReadOnlyCollection_1_get_Item_m3610244886_gshared (ReadOnlyCollection_1_t4249694466 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3610244886(__this, ___index0, method) ((  RaycastHit2D_t4063908774  (*) (ReadOnlyCollection_1_t4249694466 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3610244886_gshared)(__this, ___index0, method)
