﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailContentChange
struct  MailContentChange_t1171635520  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MailContentChange::inboxPanel
	GameObject_t1756533147 * ___inboxPanel_2;
	// UnityEngine.GameObject MailContentChange::reportsPanel
	GameObject_t1756533147 * ___reportsPanel_3;
	// UnityEngine.GameObject MailContentChange::systemPanel
	GameObject_t1756533147 * ___systemPanel_4;
	// UnityEngine.GameObject MailContentChange::invitePanel
	GameObject_t1756533147 * ___invitePanel_5;
	// UnityEngine.UI.Text MailContentChange::panelName
	Text_t356221433 * ___panelName_6;
	// UnityEngine.UI.Image[] MailContentChange::tabs
	ImageU5BU5D_t590162004* ___tabs_7;
	// UnityEngine.Sprite MailContentChange::tabSelectedSprite
	Sprite_t309593783 * ___tabSelectedSprite_8;
	// UnityEngine.Sprite MailContentChange::tabUnselectedSprite
	Sprite_t309593783 * ___tabUnselectedSprite_9;

public:
	inline static int32_t get_offset_of_inboxPanel_2() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___inboxPanel_2)); }
	inline GameObject_t1756533147 * get_inboxPanel_2() const { return ___inboxPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_inboxPanel_2() { return &___inboxPanel_2; }
	inline void set_inboxPanel_2(GameObject_t1756533147 * value)
	{
		___inboxPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___inboxPanel_2, value);
	}

	inline static int32_t get_offset_of_reportsPanel_3() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___reportsPanel_3)); }
	inline GameObject_t1756533147 * get_reportsPanel_3() const { return ___reportsPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_reportsPanel_3() { return &___reportsPanel_3; }
	inline void set_reportsPanel_3(GameObject_t1756533147 * value)
	{
		___reportsPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___reportsPanel_3, value);
	}

	inline static int32_t get_offset_of_systemPanel_4() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___systemPanel_4)); }
	inline GameObject_t1756533147 * get_systemPanel_4() const { return ___systemPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_systemPanel_4() { return &___systemPanel_4; }
	inline void set_systemPanel_4(GameObject_t1756533147 * value)
	{
		___systemPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___systemPanel_4, value);
	}

	inline static int32_t get_offset_of_invitePanel_5() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___invitePanel_5)); }
	inline GameObject_t1756533147 * get_invitePanel_5() const { return ___invitePanel_5; }
	inline GameObject_t1756533147 ** get_address_of_invitePanel_5() { return &___invitePanel_5; }
	inline void set_invitePanel_5(GameObject_t1756533147 * value)
	{
		___invitePanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___invitePanel_5, value);
	}

	inline static int32_t get_offset_of_panelName_6() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___panelName_6)); }
	inline Text_t356221433 * get_panelName_6() const { return ___panelName_6; }
	inline Text_t356221433 ** get_address_of_panelName_6() { return &___panelName_6; }
	inline void set_panelName_6(Text_t356221433 * value)
	{
		___panelName_6 = value;
		Il2CppCodeGenWriteBarrier(&___panelName_6, value);
	}

	inline static int32_t get_offset_of_tabs_7() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___tabs_7)); }
	inline ImageU5BU5D_t590162004* get_tabs_7() const { return ___tabs_7; }
	inline ImageU5BU5D_t590162004** get_address_of_tabs_7() { return &___tabs_7; }
	inline void set_tabs_7(ImageU5BU5D_t590162004* value)
	{
		___tabs_7 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_7, value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_8() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___tabSelectedSprite_8)); }
	inline Sprite_t309593783 * get_tabSelectedSprite_8() const { return ___tabSelectedSprite_8; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedSprite_8() { return &___tabSelectedSprite_8; }
	inline void set_tabSelectedSprite_8(Sprite_t309593783 * value)
	{
		___tabSelectedSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedSprite_8, value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_9() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520, ___tabUnselectedSprite_9)); }
	inline Sprite_t309593783 * get_tabUnselectedSprite_9() const { return ___tabUnselectedSprite_9; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedSprite_9() { return &___tabUnselectedSprite_9; }
	inline void set_tabUnselectedSprite_9(Sprite_t309593783 * value)
	{
		___tabUnselectedSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedSprite_9, value);
	}
};

struct MailContentChange_t1171635520_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MailContentChange::<>f__switch$mapA
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapA_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_10() { return static_cast<int32_t>(offsetof(MailContentChange_t1171635520_StaticFields, ___U3CU3Ef__switchU24mapA_10)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapA_10() const { return ___U3CU3Ef__switchU24mapA_10; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapA_10() { return &___U3CU3Ef__switchU24mapA_10; }
	inline void set_U3CU3Ef__switchU24mapA_10(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
