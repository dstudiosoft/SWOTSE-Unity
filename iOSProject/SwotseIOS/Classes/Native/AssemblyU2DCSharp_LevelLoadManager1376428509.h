﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ADManager
struct ADManager_t1411927088;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLoadManager
struct  LevelLoadManager_t1376428509  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject LevelLoadManager::mainUI
	GameObject_t1756533147 * ___mainUI_2;
	// UnityEngine.GameObject LevelLoadManager::touchManager
	GameObject_t1756533147 * ___touchManager_3;
	// UnityEngine.GameObject LevelLoadManager::zoomMenu
	GameObject_t1756533147 * ___zoomMenu_4;
	// UnityEngine.GameObject LevelLoadManager::coordsMenu
	GameObject_t1756533147 * ___coordsMenu_5;
	// ADManager LevelLoadManager::adManager
	ADManager_t1411927088 * ___adManager_6;

public:
	inline static int32_t get_offset_of_mainUI_2() { return static_cast<int32_t>(offsetof(LevelLoadManager_t1376428509, ___mainUI_2)); }
	inline GameObject_t1756533147 * get_mainUI_2() const { return ___mainUI_2; }
	inline GameObject_t1756533147 ** get_address_of_mainUI_2() { return &___mainUI_2; }
	inline void set_mainUI_2(GameObject_t1756533147 * value)
	{
		___mainUI_2 = value;
		Il2CppCodeGenWriteBarrier(&___mainUI_2, value);
	}

	inline static int32_t get_offset_of_touchManager_3() { return static_cast<int32_t>(offsetof(LevelLoadManager_t1376428509, ___touchManager_3)); }
	inline GameObject_t1756533147 * get_touchManager_3() const { return ___touchManager_3; }
	inline GameObject_t1756533147 ** get_address_of_touchManager_3() { return &___touchManager_3; }
	inline void set_touchManager_3(GameObject_t1756533147 * value)
	{
		___touchManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___touchManager_3, value);
	}

	inline static int32_t get_offset_of_zoomMenu_4() { return static_cast<int32_t>(offsetof(LevelLoadManager_t1376428509, ___zoomMenu_4)); }
	inline GameObject_t1756533147 * get_zoomMenu_4() const { return ___zoomMenu_4; }
	inline GameObject_t1756533147 ** get_address_of_zoomMenu_4() { return &___zoomMenu_4; }
	inline void set_zoomMenu_4(GameObject_t1756533147 * value)
	{
		___zoomMenu_4 = value;
		Il2CppCodeGenWriteBarrier(&___zoomMenu_4, value);
	}

	inline static int32_t get_offset_of_coordsMenu_5() { return static_cast<int32_t>(offsetof(LevelLoadManager_t1376428509, ___coordsMenu_5)); }
	inline GameObject_t1756533147 * get_coordsMenu_5() const { return ___coordsMenu_5; }
	inline GameObject_t1756533147 ** get_address_of_coordsMenu_5() { return &___coordsMenu_5; }
	inline void set_coordsMenu_5(GameObject_t1756533147 * value)
	{
		___coordsMenu_5 = value;
		Il2CppCodeGenWriteBarrier(&___coordsMenu_5, value);
	}

	inline static int32_t get_offset_of_adManager_6() { return static_cast<int32_t>(offsetof(LevelLoadManager_t1376428509, ___adManager_6)); }
	inline ADManager_t1411927088 * get_adManager_6() const { return ___adManager_6; }
	inline ADManager_t1411927088 ** get_address_of_adManager_6() { return &___adManager_6; }
	inline void set_adManager_6(ADManager_t1411927088 * value)
	{
		___adManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___adManager_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
