﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.TableViewCell::.ctor()
extern "C"  void TableViewCell__ctor_m4043242162 (TableViewCell_t1276614623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Tacticsoft.TableViewCell::get_reuseIdentifier()
extern "C"  String_t* TableViewCell_get_reuseIdentifier_m363184953 (TableViewCell_t1276614623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
