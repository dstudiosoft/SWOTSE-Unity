﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>
struct UnityEvent_2_t174962048;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Single>
struct UnityAction_2_t2587731426;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::.ctor()
extern "C"  void UnityEvent_2__ctor_m2939878020_gshared (UnityEvent_2_t174962048 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m2939878020(__this, method) ((  void (*) (UnityEvent_2_t174962048 *, const MethodInfo*))UnityEvent_2__ctor_m2939878020_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m1189260655_gshared (UnityEvent_2_t174962048 * __this, UnityAction_2_t2587731426 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m1189260655(__this, ___call0, method) ((  void (*) (UnityEvent_2_t174962048 *, UnityAction_2_t2587731426 *, const MethodInfo*))UnityEvent_2_AddListener_m1189260655_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m25014404_gshared (UnityEvent_2_t174962048 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m25014404(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t174962048 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m25014404_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m649497706_gshared (UnityEvent_2_t174962048 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m649497706(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_2_t174962048 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m649497706_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m962743757_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t2587731426 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m962743757(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2587731426 *, const MethodInfo*))UnityEvent_2_GetDelegate_m962743757_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::Invoke(T0,T1)
extern "C"  void UnityEvent_2_Invoke_m3012962327_gshared (UnityEvent_2_t174962048 * __this, int32_t ___arg00, float ___arg11, const MethodInfo* method);
#define UnityEvent_2_Invoke_m3012962327(__this, ___arg00, ___arg11, method) ((  void (*) (UnityEvent_2_t174962048 *, int32_t, float, const MethodInfo*))UnityEvent_2_Invoke_m3012962327_gshared)(__this, ___arg00, ___arg11, method)
