﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IReloadable
struct IReloadable_t861412162;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldTimeContentManager
struct  ShieldTimeContentManager_t603808652  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ShieldTimeContentManager::hour
	bool ___hour_2;
	// IReloadable ShieldTimeContentManager::owner
	Il2CppObject * ___owner_3;
	// System.String ShieldTimeContentManager::itemId
	String_t* ___itemId_4;
	// UnityEngine.UI.Text ShieldTimeContentManager::timeLabel
	Text_t356221433 * ___timeLabel_5;
	// UnityEngine.UI.Dropdown ShieldTimeContentManager::timeValue
	Dropdown_t1985816271 * ___timeValue_6;

public:
	inline static int32_t get_offset_of_hour_2() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652, ___hour_2)); }
	inline bool get_hour_2() const { return ___hour_2; }
	inline bool* get_address_of_hour_2() { return &___hour_2; }
	inline void set_hour_2(bool value)
	{
		___hour_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652, ___owner_3)); }
	inline Il2CppObject * get_owner_3() const { return ___owner_3; }
	inline Il2CppObject ** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(Il2CppObject * value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier(&___owner_3, value);
	}

	inline static int32_t get_offset_of_itemId_4() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652, ___itemId_4)); }
	inline String_t* get_itemId_4() const { return ___itemId_4; }
	inline String_t** get_address_of_itemId_4() { return &___itemId_4; }
	inline void set_itemId_4(String_t* value)
	{
		___itemId_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemId_4, value);
	}

	inline static int32_t get_offset_of_timeLabel_5() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652, ___timeLabel_5)); }
	inline Text_t356221433 * get_timeLabel_5() const { return ___timeLabel_5; }
	inline Text_t356221433 ** get_address_of_timeLabel_5() { return &___timeLabel_5; }
	inline void set_timeLabel_5(Text_t356221433 * value)
	{
		___timeLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_5, value);
	}

	inline static int32_t get_offset_of_timeValue_6() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652, ___timeValue_6)); }
	inline Dropdown_t1985816271 * get_timeValue_6() const { return ___timeValue_6; }
	inline Dropdown_t1985816271 ** get_address_of_timeValue_6() { return &___timeValue_6; }
	inline void set_timeValue_6(Dropdown_t1985816271 * value)
	{
		___timeValue_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeValue_6, value);
	}
};

struct ShieldTimeContentManager_t603808652_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> ShieldTimeContentManager::days
	List_1_t1398341365 * ___days_7;

public:
	inline static int32_t get_offset_of_days_7() { return static_cast<int32_t>(offsetof(ShieldTimeContentManager_t603808652_StaticFields, ___days_7)); }
	inline List_1_t1398341365 * get_days_7() const { return ___days_7; }
	inline List_1_t1398341365 ** get_address_of_days_7() { return &___days_7; }
	inline void set_days_7(List_1_t1398341365 * value)
	{
		___days_7 = value;
		Il2CppCodeGenWriteBarrier(&___days_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
