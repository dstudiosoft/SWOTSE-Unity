﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AppDomainManager
struct AppDomainManager_t54965696;
// System.Security.SecurityState
struct SecurityState_t3186615999;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_SecurityState3186615999.h"

// System.Boolean System.AppDomainManager::CheckSecuritySettings(System.Security.SecurityState)
extern "C"  bool AppDomainManager_CheckSecuritySettings_m2538479216 (AppDomainManager_t54965696 * __this, SecurityState_t3186615999 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
