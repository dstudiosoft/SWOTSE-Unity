﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandCenterArmyTableLine
struct CommandCenterArmyTableLine_t1553486509;
// System.String
struct String_t;
// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2450766557;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CommandCenterArmiesTableControll2450766557.h"

// System.Void CommandCenterArmyTableLine::.ctor()
extern "C"  void CommandCenterArmyTableLine__ctor_m2329696296 (CommandCenterArmyTableLine_t1553486509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmyTableLine::SetKnightName(System.String)
extern "C"  void CommandCenterArmyTableLine_SetKnightName_m2988312436 (CommandCenterArmyTableLine_t1553486509 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmyTableLine::SetStatus(System.String)
extern "C"  void CommandCenterArmyTableLine_SetStatus_m466621852 (CommandCenterArmyTableLine_t1553486509 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmyTableLine::SetArmyId(System.Int64)
extern "C"  void CommandCenterArmyTableLine_SetArmyId_m976969904 (CommandCenterArmyTableLine_t1553486509 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmyTableLine::SetOwner(CommandCenterArmiesTableController)
extern "C"  void CommandCenterArmyTableLine_SetOwner_m1590429616 (CommandCenterArmyTableLine_t1553486509 * __this, CommandCenterArmiesTableController_t2450766557 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmyTableLine::ShowDetails()
extern "C"  void CommandCenterArmyTableLine_ShowDetails_m1322112637 (CommandCenterArmyTableLine_t1553486509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
