﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IcmpV6Statistics
struct IcmpV6Statistics_t86748584;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IcmpV6Statistics::.ctor()
extern "C"  void IcmpV6Statistics__ctor_m2955825516 (IcmpV6Statistics_t86748584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
