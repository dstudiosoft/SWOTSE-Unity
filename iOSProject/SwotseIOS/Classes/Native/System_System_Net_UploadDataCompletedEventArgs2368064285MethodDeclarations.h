﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t2368064285;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.UploadDataCompletedEventArgs::.ctor(System.Byte[],System.Exception,System.Boolean,System.Object)
extern "C"  void UploadDataCompletedEventArgs__ctor_m4175522995 (UploadDataCompletedEventArgs_t2368064285 * __this, ByteU5BU5D_t3397334013* ___result0, Exception_t1927440687 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.UploadDataCompletedEventArgs::get_Result()
extern "C"  ByteU5BU5D_t3397334013* UploadDataCompletedEventArgs_get_Result_m146183913 (UploadDataCompletedEventArgs_t2368064285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
