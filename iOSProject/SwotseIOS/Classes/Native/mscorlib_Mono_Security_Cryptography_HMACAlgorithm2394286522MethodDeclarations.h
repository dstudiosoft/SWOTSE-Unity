﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.HMACAlgorithm
struct HMACAlgorithm_t2394286522;
// System.String
struct String_t;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.Cryptography.HMACAlgorithm::.ctor(System.String)
extern "C"  void HMACAlgorithm__ctor_m1302822079 (HMACAlgorithm_t2394286522 * __this, String_t* ___algoName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Finalize()
extern "C"  void HMACAlgorithm_Finalize_m3117049531 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::CreateHash(System.String)
extern "C"  void HMACAlgorithm_CreateHash_m2829508909 (HMACAlgorithm_t2394286522 * __this, String_t* ___algoName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Dispose()
extern "C"  void HMACAlgorithm_Dispose_m2714715030 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMACAlgorithm::get_Algo()
extern "C"  HashAlgorithm_t2624936259 * HMACAlgorithm_get_Algo_m3199654642 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Cryptography.HMACAlgorithm::get_HashName()
extern "C"  String_t* HMACAlgorithm_get_HashName_m4117196548 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::set_HashName(System.String)
extern "C"  void HMACAlgorithm_set_HashName_m2543203883 (HMACAlgorithm_t2394286522 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::get_Key()
extern "C"  ByteU5BU5D_t3397334013* HMACAlgorithm_get_Key_m2971390543 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::set_Key(System.Byte[])
extern "C"  void HMACAlgorithm_set_Key_m2677256634 (HMACAlgorithm_t2394286522 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Initialize()
extern "C"  void HMACAlgorithm_Initialize_m2977474669 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::KeySetup(System.Byte[],System.Byte)
extern "C"  ByteU5BU5D_t3397334013* HMACAlgorithm_KeySetup_m2737301027 (HMACAlgorithm_t2394286522 * __this, ByteU5BU5D_t3397334013* ___key0, uint8_t ___padding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Core(System.Byte[],System.Int32,System.Int32)
extern "C"  void HMACAlgorithm_Core_m3480858913 (HMACAlgorithm_t2394286522 * __this, ByteU5BU5D_t3397334013* ___rgb0, int32_t ___ib1, int32_t ___cb2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::Final()
extern "C"  ByteU5BU5D_t3397334013* HMACAlgorithm_Final_m253081009 (HMACAlgorithm_t2394286522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
