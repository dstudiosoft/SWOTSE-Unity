﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ResponseStream
struct ResponseStream_t3958753779;
// System.IO.Stream
struct Stream_t3255436806;
// System.Net.HttpListenerResponse
struct HttpListenerResponse_t3926738757;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_Net_HttpListenerResponse3926738757.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.Void System.Net.ResponseStream::.ctor(System.IO.Stream,System.Net.HttpListenerResponse,System.Boolean)
extern "C"  void ResponseStream__ctor_m1613466406 (ResponseStream_t3958753779 * __this, Stream_t3255436806 * ___stream0, HttpListenerResponse_t3926738757 * ___response1, bool ___ignore_errors2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::.cctor()
extern "C"  void ResponseStream__cctor_m4198081490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ResponseStream::get_CanRead()
extern "C"  bool ResponseStream_get_CanRead_m1241574638 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ResponseStream::get_CanSeek()
extern "C"  bool ResponseStream_get_CanSeek_m2095196902 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ResponseStream::get_CanWrite()
extern "C"  bool ResponseStream_get_CanWrite_m1391318679 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.ResponseStream::get_Length()
extern "C"  int64_t ResponseStream_get_Length_m435154619 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.ResponseStream::get_Position()
extern "C"  int64_t ResponseStream_get_Position_m1177470654 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::set_Position(System.Int64)
extern "C"  void ResponseStream_set_Position_m2247896805 (ResponseStream_t3958753779 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::Close()
extern "C"  void ResponseStream_Close_m1710367177 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream System.Net.ResponseStream::GetHeaders(System.Boolean)
extern "C"  MemoryStream_t743994179 * ResponseStream_GetHeaders_m4028648575 (ResponseStream_t3958753779 * __this, bool ___closing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::Flush()
extern "C"  void ResponseStream_Flush_m1710184951 (ResponseStream_t3958753779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.ResponseStream::GetChunkSizeBytes(System.Int32,System.Boolean)
extern "C"  ByteU5BU5D_t3397334013* ResponseStream_GetChunkSizeBytes_m3429439288 (Il2CppObject * __this /* static, unused */, int32_t ___size0, bool ___final1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::InternalWrite(System.Byte[],System.Int32,System.Int32)
extern "C"  void ResponseStream_InternalWrite_m516087524 (ResponseStream_t3958753779 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ResponseStream_Write_m3066786893 (ResponseStream_t3958753779 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.ResponseStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResponseStream_BeginWrite_m2303874118 (ResponseStream_t3958753779 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::EndWrite(System.IAsyncResult)
extern "C"  void ResponseStream_EndWrite_m3961121892 (ResponseStream_t3958753779 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ResponseStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ResponseStream_Read_m1890147504 (ResponseStream_t3958753779 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.ResponseStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResponseStream_BeginRead_m4287244565 (ResponseStream_t3958753779 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ResponseStream::EndRead(System.IAsyncResult)
extern "C"  int32_t ResponseStream_EndRead_m1729046089 (ResponseStream_t3958753779 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.ResponseStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ResponseStream_Seek_m4134197295 (ResponseStream_t3958753779 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ResponseStream::SetLength(System.Int64)
extern "C"  void ResponseStream_SetLength_m1282025419 (ResponseStream_t3958753779 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
