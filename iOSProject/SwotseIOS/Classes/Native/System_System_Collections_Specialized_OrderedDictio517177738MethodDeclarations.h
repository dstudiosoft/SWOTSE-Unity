﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator
struct OrderedEntryCollectionEnumerator_t517177738;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::.ctor(System.Collections.IEnumerator)
extern "C"  void OrderedEntryCollectionEnumerator__ctor_m2695706059 (OrderedEntryCollectionEnumerator_t517177738 * __this, Il2CppObject * ___listEnumerator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::MoveNext()
extern "C"  bool OrderedEntryCollectionEnumerator_MoveNext_m3445732396 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::Reset()
extern "C"  void OrderedEntryCollectionEnumerator_Reset_m913127103 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::get_Current()
extern "C"  Il2CppObject * OrderedEntryCollectionEnumerator_get_Current_m3902277143 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::get_Entry()
extern "C"  DictionaryEntry_t3048875398  OrderedEntryCollectionEnumerator_get_Entry_m728327312 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::get_Key()
extern "C"  Il2CppObject * OrderedEntryCollectionEnumerator_get_Key_m2360713669 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary/OrderedEntryCollectionEnumerator::get_Value()
extern "C"  Il2CppObject * OrderedEntryCollectionEnumerator_get_Value_m3652297933 (OrderedEntryCollectionEnumerator_t517177738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
