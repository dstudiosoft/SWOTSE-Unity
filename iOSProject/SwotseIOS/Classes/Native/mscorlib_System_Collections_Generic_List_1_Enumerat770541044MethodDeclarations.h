﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4168038091(__this, ___l0, method) ((  void (*) (Enumerator_t770541044 *, List_1_t1235811370 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2241165351(__this, method) ((  void (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2514391223(__this, method) ((  Il2CppObject * (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::Dispose()
#define Enumerator_Dispose_m844847810(__this, method) ((  void (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::VerifyState()
#define Enumerator_VerifyState_m2597019841(__this, method) ((  void (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::MoveNext()
#define Enumerator_MoveNext_m3733746893(__this, method) ((  bool (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TashkeelLocation>::get_Current()
#define Enumerator_get_Current_m3573437625(__this, method) ((  TashkeelLocation_t1866690238 * (*) (Enumerator_t770541044 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
