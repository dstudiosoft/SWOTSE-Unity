﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceValleysTableLine
struct ResidenceValleysTableLine_t3572173254;
// ResidenceValleysTableController
struct ResidenceValleysTableController_t2038149672;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResidenceValleysTableController2038149672.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResidenceValleysTableLine::.ctor()
extern "C"  void ResidenceValleysTableLine__ctor_m671108335 (ResidenceValleysTableLine_t3572173254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetOwner(ResidenceValleysTableController)
extern "C"  void ResidenceValleysTableLine_SetOwner_m2018073608 (ResidenceValleysTableLine_t3572173254 * __this, ResidenceValleysTableController_t2038149672 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetValleyName(System.String)
extern "C"  void ResidenceValleysTableLine_SetValleyName_m1825632407 (ResidenceValleysTableLine_t3572173254 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetLevel(System.Int64)
extern "C"  void ResidenceValleysTableLine_SetLevel_m2650763813 (ResidenceValleysTableLine_t3572173254 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetCoords(System.String)
extern "C"  void ResidenceValleysTableLine_SetCoords_m521749253 (ResidenceValleysTableLine_t3572173254 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetResource(System.String)
extern "C"  void ResidenceValleysTableLine_SetResource_m4133996201 (ResidenceValleysTableLine_t3572173254 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetRegion(System.String)
extern "C"  void ResidenceValleysTableLine_SetRegion_m3467028451 (ResidenceValleysTableLine_t3572173254 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::SetProduction(System.Int64)
extern "C"  void ResidenceValleysTableLine_SetProduction_m65843398 (ResidenceValleysTableLine_t3572173254 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceValleysTableLine::OpenBuildingWindow()
extern "C"  void ResidenceValleysTableLine_OpenBuildingWindow_m1456639645 (ResidenceValleysTableLine_t3572173254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
