﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.FirebaseNotification
struct FirebaseNotification_t531124606;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Firebase.Messaging.FirebaseNotification::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseNotification__ctor_m3109927032 (FirebaseNotification_t531124606 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseNotification::Finalize()
extern "C"  void FirebaseNotification_Finalize_m2995729439 (FirebaseNotification_t531124606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseNotification::Dispose()
extern "C"  void FirebaseNotification_Dispose_m2999443704 (FirebaseNotification_t531124606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseNotification::get_Body()
extern "C"  String_t* FirebaseNotification_get_Body_m150060641 (FirebaseNotification_t531124606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
