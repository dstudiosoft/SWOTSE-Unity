﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t1789388632;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginRegistrationContentManager
struct  LoginRegistrationContentManager_t511879792  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject LoginRegistrationContentManager::loginContent
	GameObject_t1756533147 * ___loginContent_2;
	// UnityEngine.GameObject LoginRegistrationContentManager::registrationContent
	GameObject_t1756533147 * ___registrationContent_3;
	// UnityEngine.GameObject LoginRegistrationContentManager::forgotPasswordContent
	GameObject_t1756533147 * ___forgotPasswordContent_4;
	// UnityEngine.GameObject LoginRegistrationContentManager::resetPasswordContent
	GameObject_t1756533147 * ___resetPasswordContent_5;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::loginUsername
	InputField_t1631627530 * ___loginUsername_6;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::loginPassword
	InputField_t1631627530 * ___loginPassword_7;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerUsername
	InputField_t1631627530 * ___registerUsername_8;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerPassword
	InputField_t1631627530 * ___registerPassword_9;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerConfirmPassword
	InputField_t1631627530 * ___registerConfirmPassword_10;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::registerEmail
	InputField_t1631627530 * ___registerEmail_11;
	// UnityEngine.UI.Dropdown LoginRegistrationContentManager::registerEmperors
	Dropdown_t1985816271 * ___registerEmperors_12;
	// UnityEngine.UI.Dropdown LoginRegistrationContentManager::registerLanguage
	Dropdown_t1985816271 * ___registerLanguage_13;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::forgotPasswordEmail
	InputField_t1631627530 * ___forgotPasswordEmail_14;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::resetSecretKey
	InputField_t1631627530 * ___resetSecretKey_15;
	// UnityEngine.UI.InputField LoginRegistrationContentManager::resetNewPassword
	InputField_t1631627530 * ___resetNewPassword_16;
	// UnityEngine.GameObject LoginRegistrationContentManager::directLoginButton
	GameObject_t1756533147 * ___directLoginButton_17;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> LoginRegistrationContentManager::emperorsList
	List_1_t1789388632 * ___emperorsList_18;

public:
	inline static int32_t get_offset_of_loginContent_2() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___loginContent_2)); }
	inline GameObject_t1756533147 * get_loginContent_2() const { return ___loginContent_2; }
	inline GameObject_t1756533147 ** get_address_of_loginContent_2() { return &___loginContent_2; }
	inline void set_loginContent_2(GameObject_t1756533147 * value)
	{
		___loginContent_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginContent_2, value);
	}

	inline static int32_t get_offset_of_registrationContent_3() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registrationContent_3)); }
	inline GameObject_t1756533147 * get_registrationContent_3() const { return ___registrationContent_3; }
	inline GameObject_t1756533147 ** get_address_of_registrationContent_3() { return &___registrationContent_3; }
	inline void set_registrationContent_3(GameObject_t1756533147 * value)
	{
		___registrationContent_3 = value;
		Il2CppCodeGenWriteBarrier(&___registrationContent_3, value);
	}

	inline static int32_t get_offset_of_forgotPasswordContent_4() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___forgotPasswordContent_4)); }
	inline GameObject_t1756533147 * get_forgotPasswordContent_4() const { return ___forgotPasswordContent_4; }
	inline GameObject_t1756533147 ** get_address_of_forgotPasswordContent_4() { return &___forgotPasswordContent_4; }
	inline void set_forgotPasswordContent_4(GameObject_t1756533147 * value)
	{
		___forgotPasswordContent_4 = value;
		Il2CppCodeGenWriteBarrier(&___forgotPasswordContent_4, value);
	}

	inline static int32_t get_offset_of_resetPasswordContent_5() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___resetPasswordContent_5)); }
	inline GameObject_t1756533147 * get_resetPasswordContent_5() const { return ___resetPasswordContent_5; }
	inline GameObject_t1756533147 ** get_address_of_resetPasswordContent_5() { return &___resetPasswordContent_5; }
	inline void set_resetPasswordContent_5(GameObject_t1756533147 * value)
	{
		___resetPasswordContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___resetPasswordContent_5, value);
	}

	inline static int32_t get_offset_of_loginUsername_6() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___loginUsername_6)); }
	inline InputField_t1631627530 * get_loginUsername_6() const { return ___loginUsername_6; }
	inline InputField_t1631627530 ** get_address_of_loginUsername_6() { return &___loginUsername_6; }
	inline void set_loginUsername_6(InputField_t1631627530 * value)
	{
		___loginUsername_6 = value;
		Il2CppCodeGenWriteBarrier(&___loginUsername_6, value);
	}

	inline static int32_t get_offset_of_loginPassword_7() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___loginPassword_7)); }
	inline InputField_t1631627530 * get_loginPassword_7() const { return ___loginPassword_7; }
	inline InputField_t1631627530 ** get_address_of_loginPassword_7() { return &___loginPassword_7; }
	inline void set_loginPassword_7(InputField_t1631627530 * value)
	{
		___loginPassword_7 = value;
		Il2CppCodeGenWriteBarrier(&___loginPassword_7, value);
	}

	inline static int32_t get_offset_of_registerUsername_8() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerUsername_8)); }
	inline InputField_t1631627530 * get_registerUsername_8() const { return ___registerUsername_8; }
	inline InputField_t1631627530 ** get_address_of_registerUsername_8() { return &___registerUsername_8; }
	inline void set_registerUsername_8(InputField_t1631627530 * value)
	{
		___registerUsername_8 = value;
		Il2CppCodeGenWriteBarrier(&___registerUsername_8, value);
	}

	inline static int32_t get_offset_of_registerPassword_9() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerPassword_9)); }
	inline InputField_t1631627530 * get_registerPassword_9() const { return ___registerPassword_9; }
	inline InputField_t1631627530 ** get_address_of_registerPassword_9() { return &___registerPassword_9; }
	inline void set_registerPassword_9(InputField_t1631627530 * value)
	{
		___registerPassword_9 = value;
		Il2CppCodeGenWriteBarrier(&___registerPassword_9, value);
	}

	inline static int32_t get_offset_of_registerConfirmPassword_10() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerConfirmPassword_10)); }
	inline InputField_t1631627530 * get_registerConfirmPassword_10() const { return ___registerConfirmPassword_10; }
	inline InputField_t1631627530 ** get_address_of_registerConfirmPassword_10() { return &___registerConfirmPassword_10; }
	inline void set_registerConfirmPassword_10(InputField_t1631627530 * value)
	{
		___registerConfirmPassword_10 = value;
		Il2CppCodeGenWriteBarrier(&___registerConfirmPassword_10, value);
	}

	inline static int32_t get_offset_of_registerEmail_11() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerEmail_11)); }
	inline InputField_t1631627530 * get_registerEmail_11() const { return ___registerEmail_11; }
	inline InputField_t1631627530 ** get_address_of_registerEmail_11() { return &___registerEmail_11; }
	inline void set_registerEmail_11(InputField_t1631627530 * value)
	{
		___registerEmail_11 = value;
		Il2CppCodeGenWriteBarrier(&___registerEmail_11, value);
	}

	inline static int32_t get_offset_of_registerEmperors_12() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerEmperors_12)); }
	inline Dropdown_t1985816271 * get_registerEmperors_12() const { return ___registerEmperors_12; }
	inline Dropdown_t1985816271 ** get_address_of_registerEmperors_12() { return &___registerEmperors_12; }
	inline void set_registerEmperors_12(Dropdown_t1985816271 * value)
	{
		___registerEmperors_12 = value;
		Il2CppCodeGenWriteBarrier(&___registerEmperors_12, value);
	}

	inline static int32_t get_offset_of_registerLanguage_13() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___registerLanguage_13)); }
	inline Dropdown_t1985816271 * get_registerLanguage_13() const { return ___registerLanguage_13; }
	inline Dropdown_t1985816271 ** get_address_of_registerLanguage_13() { return &___registerLanguage_13; }
	inline void set_registerLanguage_13(Dropdown_t1985816271 * value)
	{
		___registerLanguage_13 = value;
		Il2CppCodeGenWriteBarrier(&___registerLanguage_13, value);
	}

	inline static int32_t get_offset_of_forgotPasswordEmail_14() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___forgotPasswordEmail_14)); }
	inline InputField_t1631627530 * get_forgotPasswordEmail_14() const { return ___forgotPasswordEmail_14; }
	inline InputField_t1631627530 ** get_address_of_forgotPasswordEmail_14() { return &___forgotPasswordEmail_14; }
	inline void set_forgotPasswordEmail_14(InputField_t1631627530 * value)
	{
		___forgotPasswordEmail_14 = value;
		Il2CppCodeGenWriteBarrier(&___forgotPasswordEmail_14, value);
	}

	inline static int32_t get_offset_of_resetSecretKey_15() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___resetSecretKey_15)); }
	inline InputField_t1631627530 * get_resetSecretKey_15() const { return ___resetSecretKey_15; }
	inline InputField_t1631627530 ** get_address_of_resetSecretKey_15() { return &___resetSecretKey_15; }
	inline void set_resetSecretKey_15(InputField_t1631627530 * value)
	{
		___resetSecretKey_15 = value;
		Il2CppCodeGenWriteBarrier(&___resetSecretKey_15, value);
	}

	inline static int32_t get_offset_of_resetNewPassword_16() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___resetNewPassword_16)); }
	inline InputField_t1631627530 * get_resetNewPassword_16() const { return ___resetNewPassword_16; }
	inline InputField_t1631627530 ** get_address_of_resetNewPassword_16() { return &___resetNewPassword_16; }
	inline void set_resetNewPassword_16(InputField_t1631627530 * value)
	{
		___resetNewPassword_16 = value;
		Il2CppCodeGenWriteBarrier(&___resetNewPassword_16, value);
	}

	inline static int32_t get_offset_of_directLoginButton_17() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___directLoginButton_17)); }
	inline GameObject_t1756533147 * get_directLoginButton_17() const { return ___directLoginButton_17; }
	inline GameObject_t1756533147 ** get_address_of_directLoginButton_17() { return &___directLoginButton_17; }
	inline void set_directLoginButton_17(GameObject_t1756533147 * value)
	{
		___directLoginButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___directLoginButton_17, value);
	}

	inline static int32_t get_offset_of_emperorsList_18() { return static_cast<int32_t>(offsetof(LoginRegistrationContentManager_t511879792, ___emperorsList_18)); }
	inline List_1_t1789388632 * get_emperorsList_18() const { return ___emperorsList_18; }
	inline List_1_t1789388632 ** get_address_of_emperorsList_18() { return &___emperorsList_18; }
	inline void set_emperorsList_18(List_1_t1789388632 * value)
	{
		___emperorsList_18 = value;
		Il2CppCodeGenWriteBarrier(&___emperorsList_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
