﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyGeneralModel
struct  ArmyGeneralModel_t609759248  : public Il2CppObject
{
public:
	// System.Int64 ArmyGeneralModel::id
	int64_t ___id_0;
	// System.String ArmyGeneralModel::name
	String_t* ___name_1;
	// System.Int64 ArmyGeneralModel::cityID
	int64_t ___cityID_2;
	// System.Int64 ArmyGeneralModel::politics
	int64_t ___politics_3;
	// System.Int64 ArmyGeneralModel::marches
	int64_t ___marches_4;
	// System.Int64 ArmyGeneralModel::speed
	int64_t ___speed_5;
	// System.Int64 ArmyGeneralModel::loyalty
	int64_t ___loyalty_6;
	// System.Int64 ArmyGeneralModel::level
	int64_t ___level_7;
	// System.Int64 ArmyGeneralModel::salary
	int64_t ___salary_8;
	// System.Int64 ArmyGeneralModel::experience
	int64_t ___experience_9;
	// System.Int64 ArmyGeneralModel::number_of_battles
	int64_t ___number_of_battles_10;
	// System.String ArmyGeneralModel::status
	String_t* ___status_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_cityID_2() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___cityID_2)); }
	inline int64_t get_cityID_2() const { return ___cityID_2; }
	inline int64_t* get_address_of_cityID_2() { return &___cityID_2; }
	inline void set_cityID_2(int64_t value)
	{
		___cityID_2 = value;
	}

	inline static int32_t get_offset_of_politics_3() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___politics_3)); }
	inline int64_t get_politics_3() const { return ___politics_3; }
	inline int64_t* get_address_of_politics_3() { return &___politics_3; }
	inline void set_politics_3(int64_t value)
	{
		___politics_3 = value;
	}

	inline static int32_t get_offset_of_marches_4() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___marches_4)); }
	inline int64_t get_marches_4() const { return ___marches_4; }
	inline int64_t* get_address_of_marches_4() { return &___marches_4; }
	inline void set_marches_4(int64_t value)
	{
		___marches_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___speed_5)); }
	inline int64_t get_speed_5() const { return ___speed_5; }
	inline int64_t* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(int64_t value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_loyalty_6() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___loyalty_6)); }
	inline int64_t get_loyalty_6() const { return ___loyalty_6; }
	inline int64_t* get_address_of_loyalty_6() { return &___loyalty_6; }
	inline void set_loyalty_6(int64_t value)
	{
		___loyalty_6 = value;
	}

	inline static int32_t get_offset_of_level_7() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___level_7)); }
	inline int64_t get_level_7() const { return ___level_7; }
	inline int64_t* get_address_of_level_7() { return &___level_7; }
	inline void set_level_7(int64_t value)
	{
		___level_7 = value;
	}

	inline static int32_t get_offset_of_salary_8() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___salary_8)); }
	inline int64_t get_salary_8() const { return ___salary_8; }
	inline int64_t* get_address_of_salary_8() { return &___salary_8; }
	inline void set_salary_8(int64_t value)
	{
		___salary_8 = value;
	}

	inline static int32_t get_offset_of_experience_9() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___experience_9)); }
	inline int64_t get_experience_9() const { return ___experience_9; }
	inline int64_t* get_address_of_experience_9() { return &___experience_9; }
	inline void set_experience_9(int64_t value)
	{
		___experience_9 = value;
	}

	inline static int32_t get_offset_of_number_of_battles_10() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___number_of_battles_10)); }
	inline int64_t get_number_of_battles_10() const { return ___number_of_battles_10; }
	inline int64_t* get_address_of_number_of_battles_10() { return &___number_of_battles_10; }
	inline void set_number_of_battles_10(int64_t value)
	{
		___number_of_battles_10 = value;
	}

	inline static int32_t get_offset_of_status_11() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t609759248, ___status_11)); }
	inline String_t* get_status_11() const { return ___status_11; }
	inline String_t** get_address_of_status_11() { return &___status_11; }
	inline void set_status_11(String_t* value)
	{
		___status_11 = value;
		Il2CppCodeGenWriteBarrier(&___status_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
