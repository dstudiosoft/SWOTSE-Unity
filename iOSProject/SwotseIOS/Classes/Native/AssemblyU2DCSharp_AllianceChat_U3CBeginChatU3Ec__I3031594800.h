﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocket
struct WebSocket_t1213274227;
// JSONObject
struct JSONObject_t1971882247;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object
struct Il2CppObject;
// AllianceChat
struct AllianceChat_t2362063621;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceChat/<BeginChat>c__Iterator4
struct  U3CBeginChatU3Ec__Iterator4_t3031594800  : public Il2CppObject
{
public:
	// WebSocket AllianceChat/<BeginChat>c__Iterator4::<w>__0
	WebSocket_t1213274227 * ___U3CwU3E__0_0;
	// JSONObject AllianceChat/<BeginChat>c__Iterator4::<initial>__1
	JSONObject_t1971882247 * ___U3CinitialU3E__1_1;
	// System.String AllianceChat/<BeginChat>c__Iterator4::<reply>__2
	String_t* ___U3CreplyU3E__2_2;
	// System.String[] AllianceChat/<BeginChat>c__Iterator4::<parts>__3
	StringU5BU5D_t1642385972* ___U3CpartsU3E__3_3;
	// System.Char[] AllianceChat/<BeginChat>c__Iterator4::<arabic>__4
	CharU5BU5D_t1328083999* ___U3CarabicU3E__4_4;
	// JSONObject AllianceChat/<BeginChat>c__Iterator4::<obj>__5
	JSONObject_t1971882247 * ___U3CobjU3E__5_5;
	// System.Int32 AllianceChat/<BeginChat>c__Iterator4::$PC
	int32_t ___U24PC_6;
	// System.Object AllianceChat/<BeginChat>c__Iterator4::$current
	Il2CppObject * ___U24current_7;
	// AllianceChat AllianceChat/<BeginChat>c__Iterator4::<>f__this
	AllianceChat_t2362063621 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CwU3E__0_0)); }
	inline WebSocket_t1213274227 * get_U3CwU3E__0_0() const { return ___U3CwU3E__0_0; }
	inline WebSocket_t1213274227 ** get_address_of_U3CwU3E__0_0() { return &___U3CwU3E__0_0; }
	inline void set_U3CwU3E__0_0(WebSocket_t1213274227 * value)
	{
		___U3CwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CinitialU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CinitialU3E__1_1)); }
	inline JSONObject_t1971882247 * get_U3CinitialU3E__1_1() const { return ___U3CinitialU3E__1_1; }
	inline JSONObject_t1971882247 ** get_address_of_U3CinitialU3E__1_1() { return &___U3CinitialU3E__1_1; }
	inline void set_U3CinitialU3E__1_1(JSONObject_t1971882247 * value)
	{
		___U3CinitialU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinitialU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CreplyU3E__2_2() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CreplyU3E__2_2)); }
	inline String_t* get_U3CreplyU3E__2_2() const { return ___U3CreplyU3E__2_2; }
	inline String_t** get_address_of_U3CreplyU3E__2_2() { return &___U3CreplyU3E__2_2; }
	inline void set_U3CreplyU3E__2_2(String_t* value)
	{
		___U3CreplyU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreplyU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CpartsU3E__3_3() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CpartsU3E__3_3)); }
	inline StringU5BU5D_t1642385972* get_U3CpartsU3E__3_3() const { return ___U3CpartsU3E__3_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpartsU3E__3_3() { return &___U3CpartsU3E__3_3; }
	inline void set_U3CpartsU3E__3_3(StringU5BU5D_t1642385972* value)
	{
		___U3CpartsU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CarabicU3E__4_4() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CarabicU3E__4_4)); }
	inline CharU5BU5D_t1328083999* get_U3CarabicU3E__4_4() const { return ___U3CarabicU3E__4_4; }
	inline CharU5BU5D_t1328083999** get_address_of_U3CarabicU3E__4_4() { return &___U3CarabicU3E__4_4; }
	inline void set_U3CarabicU3E__4_4(CharU5BU5D_t1328083999* value)
	{
		___U3CarabicU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CarabicU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_5() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CobjU3E__5_5)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__5_5() const { return ___U3CobjU3E__5_5; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__5_5() { return &___U3CobjU3E__5_5; }
	inline void set_U3CobjU3E__5_5(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CBeginChatU3Ec__Iterator4_t3031594800, ___U3CU3Ef__this_8)); }
	inline AllianceChat_t2362063621 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline AllianceChat_t2362063621 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(AllianceChat_t2362063621 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
