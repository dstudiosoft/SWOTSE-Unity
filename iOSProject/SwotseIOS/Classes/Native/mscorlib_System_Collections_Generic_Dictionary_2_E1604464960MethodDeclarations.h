﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2613007475(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1604464960 *, Dictionary_2_t284440258 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2026776498(__this, method) ((  Il2CppObject * (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1227152886(__this, method) ((  void (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2220740913(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2468873644(__this, method) ((  Il2CppObject * (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2105263654(__this, method) ((  Il2CppObject * (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::MoveNext()
#define Enumerator_MoveNext_m2520029774(__this, method) ((  bool (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::get_Current()
#define Enumerator_get_Current_m1095536422(__this, method) ((  KeyValuePair_2_t2336752776  (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3157074431(__this, method) ((  int32_t (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2928980063(__this, method) ((  TableViewCell_t1276614623 * (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::Reset()
#define Enumerator_Reset_m3600499121(__this, method) ((  void (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::VerifyState()
#define Enumerator_VerifyState_m566036282(__this, method) ((  void (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m597337606(__this, method) ((  void (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Tacticsoft.TableViewCell>::Dispose()
#define Enumerator_Dispose_m702032727(__this, method) ((  void (*) (Enumerator_t1604464960 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
