﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2450766557;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CommandCenterArmiesTableController::.ctor()
extern "C"  void CommandCenterArmiesTableController__ctor_m2181541358 (CommandCenterArmiesTableController_t2450766557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmiesTableController::Start()
extern "C"  void CommandCenterArmiesTableController_Start_m2935548542 (CommandCenterArmiesTableController_t2450766557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CommandCenterArmiesTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t CommandCenterArmiesTableController_GetNumberOfRowsForTableView_m2176806168 (CommandCenterArmiesTableController_t2450766557 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CommandCenterArmiesTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float CommandCenterArmiesTableController_GetHeightForRowInTableView_m563345328 (CommandCenterArmiesTableController_t2450766557 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell CommandCenterArmiesTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * CommandCenterArmiesTableController_GetCellForRowInTableView_m3947784997 (CommandCenterArmiesTableController_t2450766557 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmiesTableController::ReloadData(System.Object,System.String)
extern "C"  void CommandCenterArmiesTableController_ReloadData_m3248574909 (CommandCenterArmiesTableController_t2450766557 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandCenterArmiesTableController::GotArmies(System.Object,System.String)
extern "C"  void CommandCenterArmiesTableController_GotArmies_m3785155517 (CommandCenterArmiesTableController_t2450766557 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
