﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Utils.ExceptionEventArgs
struct ExceptionEventArgs_t892713536;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void OSCsharp.Utils.ExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void ExceptionEventArgs_set_Exception_m3571541630 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Utils.ExceptionEventArgs::.ctor(System.Exception)
extern "C"  void ExceptionEventArgs__ctor_m1196714556 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
