﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceColoniesTableLine
struct ResidenceColoniesTableLine_t1284105164;
// ResidenceColoniesTableController
struct ResidenceColoniesTableController_t2206090190;
// System.String
struct String_t;
// ResourcesModel
struct ResourcesModel_t2978985958;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResidenceColoniesTableController2206090190.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ResourcesModel2978985958.h"

// System.Void ResidenceColoniesTableLine::.ctor()
extern "C"  void ResidenceColoniesTableLine__ctor_m1278662429 (ResidenceColoniesTableLine_t1284105164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetOwner(ResidenceColoniesTableController)
extern "C"  void ResidenceColoniesTableLine_SetOwner_m3104966658 (ResidenceColoniesTableLine_t1284105164 * __this, ResidenceColoniesTableController_t2206090190 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetCityName(System.String)
extern "C"  void ResidenceColoniesTableLine_SetCityName_m674331637 (ResidenceColoniesTableLine_t1284105164 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetPlayerName(System.String)
extern "C"  void ResidenceColoniesTableLine_SetPlayerName_m3857889625 (ResidenceColoniesTableLine_t1284105164 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetMayerLevel(System.Int64)
extern "C"  void ResidenceColoniesTableLine_SetMayerLevel_m3108850049 (ResidenceColoniesTableLine_t1284105164 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetCoords(System.String)
extern "C"  void ResidenceColoniesTableLine_SetCoords_m3027989871 (ResidenceColoniesTableLine_t1284105164 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetRegion(System.String)
extern "C"  void ResidenceColoniesTableLine_SetRegion_m3352674609 (ResidenceColoniesTableLine_t1284105164 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::SetResources(ResourcesModel)
extern "C"  void ResidenceColoniesTableLine_SetResources_m4270281072 (ResidenceColoniesTableLine_t1284105164 * __this, ResourcesModel_t2978985958 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableLine::OpenTributes()
extern "C"  void ResidenceColoniesTableLine_OpenTributes_m2862072791 (ResidenceColoniesTableLine_t1284105164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
