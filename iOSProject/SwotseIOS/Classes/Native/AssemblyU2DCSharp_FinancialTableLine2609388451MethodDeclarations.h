﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinancialTableLine
struct FinancialTableLine_t2609388451;
// System.String
struct String_t;
// FinancialContentChanger
struct FinancialContentChanger_t2753898366;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FinancialContentChanger2753898366.h"

// System.Void FinancialTableLine::.ctor()
extern "C"  void FinancialTableLine__ctor_m1493218078 (FinancialTableLine_t2609388451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetImage(System.String)
extern "C"  void FinancialTableLine_SetImage_m2375826141 (FinancialTableLine_t2609388451 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetName(System.String)
extern "C"  void FinancialTableLine_SetName_m1094046009 (FinancialTableLine_t2609388451 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetQuantity(System.Int64)
extern "C"  void FinancialTableLine_SetQuantity_m2582500185 (FinancialTableLine_t2609388451 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetPrice(System.Int64)
extern "C"  void FinancialTableLine_SetPrice_m3305297259 (FinancialTableLine_t2609388451 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetDate(System.String)
extern "C"  void FinancialTableLine_SetDate_m2563834840 (FinancialTableLine_t2609388451 * __this, String_t* ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetFromUser(System.String)
extern "C"  void FinancialTableLine_SetFromUser_m1101810581 (FinancialTableLine_t2609388451 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetOwner(FinancialContentChanger)
extern "C"  void FinancialTableLine_SetOwner_m1388096445 (FinancialTableLine_t2609388451 * __this, FinancialContentChanger_t2753898366 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetLotId(System.String)
extern "C"  void FinancialTableLine_SetLotId_m127792748 (FinancialTableLine_t2609388451 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::SetCancelable(System.Boolean)
extern "C"  void FinancialTableLine_SetCancelable_m603888771 (FinancialTableLine_t2609388451 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialTableLine::CancelOffer()
extern "C"  void FinancialTableLine_CancelOffer_m2002971066 (FinancialTableLine_t2609388451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
