﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.DynamicHeightCell
struct DynamicHeightCell_t1164024910;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.Examples.DynamicHeightCell::.ctor()
extern "C"  void DynamicHeightCell__ctor_m10863036 (DynamicHeightCell_t1164024910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.Examples.DynamicHeightCell::get_rowNumber()
extern "C"  int32_t DynamicHeightCell_get_rowNumber_m466223632 (DynamicHeightCell_t1164024910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightCell::set_rowNumber(System.Int32)
extern "C"  void DynamicHeightCell_set_rowNumber_m4180326543 (DynamicHeightCell_t1164024910 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightCell::Update()
extern "C"  void DynamicHeightCell_Update_m465148967 (DynamicHeightCell_t1164024910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightCell::SliderValueChanged(System.Single)
extern "C"  void DynamicHeightCell_SliderValueChanged_m1826873995 (DynamicHeightCell_t1164024910 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.Examples.DynamicHeightCell::get_height()
extern "C"  float DynamicHeightCell_get_height_m1825328330 (DynamicHeightCell_t1164024910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.DynamicHeightCell::set_height(System.Single)
extern "C"  void DynamicHeightCell_set_height_m3623761101 (DynamicHeightCell_t1164024910 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
