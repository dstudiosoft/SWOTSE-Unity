﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// Firebase.AppOptions
struct AppOptions_t1641189195;
// System.Uri
struct Uri_t19570940;
// Firebase.FirebaseApp/CreateDelegate
struct CreateDelegate_t413676709;
// System.Threading.Tasks.Task`1<Firebase.DependencyStatus>
struct Task_1_t1872448422;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// Firebase.AppOptionsInternal
struct AppOptionsInternal_t2434721572;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_App_Firebase_AppOptions1641189195.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "System_System_Uri19570940.h"
#include "Firebase_App_Firebase_FirebaseApp_CreateDelegate413676709.h"
#include "Firebase_App_Firebase_DependencyStatus2752419415.h"
#include "Firebase_App_Firebase_AppOptionsInternal2434721572.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseApp__ctor_m3662482775 (FirebaseApp_t210707726 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern "C"  HandleRef_t2419939847  FirebaseApp_getCPtr_m4255293038 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::Finalize()
extern "C"  void FirebaseApp_Finalize_m3651549934 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::Dispose()
extern "C"  void FirebaseApp_Dispose_m3098692549 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FirebaseApp::get_DependencyNotFoundErrorMessage()
extern "C"  String_t* FirebaseApp_get_DependencyNotFoundErrorMessage_m1901001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FirebaseApp::get_DllNotFoundExceptionErrorMessage()
extern "C"  String_t* FirebaseApp_get_DllNotFoundExceptionErrorMessage_m4105836705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
extern "C"  void FirebaseApp_TranslateDllNotFoundException_m3870437601 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___closureToExecute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::add_Disposed(System.EventHandler`1<System.EventArgs>)
extern "C"  void FirebaseApp_add_Disposed_m3513322427 (FirebaseApp_t210707726 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::remove_Disposed(System.EventHandler`1<System.EventArgs>)
extern "C"  void FirebaseApp_remove_Disposed_m2508927096 (FirebaseApp_t210707726 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern "C"  FirebaseApp_t210707726 * FirebaseApp_get_DefaultInstance_m465202029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_GetInstance_m4058350271 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern "C"  FirebaseApp_t210707726 * FirebaseApp_Create_m4111948162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::Create(Firebase.AppOptions)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_Create_m1391218050 (Il2CppObject * __this /* static, unused */, AppOptions_t1641189195 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::Create(Firebase.AppOptions,System.String)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_Create_m2029142954 (Il2CppObject * __this /* static, unused */, AppOptions_t1641189195 * ___options0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::SetLogLevel(Firebase.LogLevel)
extern "C"  void FirebaseApp_SetLogLevel_m3781890123 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
extern "C"  int32_t FirebaseApp_get_LogLevel_m3485723495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::set_LogLevel(Firebase.LogLevel)
extern "C"  void FirebaseApp_set_LogLevel_m1203159754 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::RemoveReference(Firebase.FirebaseApp)
extern "C"  void FirebaseApp_RemoveReference_m1475253910 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::EmptyAppDictionaries()
extern "C"  void FirebaseApp_EmptyAppDictionaries_m4102170024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Firebase.FirebaseApp::UrlStringToUri(System.String)
extern "C"  Uri_t19570940 * FirebaseApp_UrlStringToUri_m3126757169 (Il2CppObject * __this /* static, unused */, String_t* ___urlString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FirebaseApp::UriToUrlString(System.Uri)
extern "C"  String_t* FirebaseApp_UriToUrlString_m3424912909 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateAndTrack_m2952140369 (Il2CppObject * __this /* static, unused */, CreateDelegate_t413676709 * ___createDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::CheckDependenciesAsync()
extern "C"  Task_1_t1872448422 * FirebaseApp_CheckDependenciesAsync_m3084998877 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::CheckAndFixDependenciesAsync()
extern "C"  Task_1_t1872448422 * FirebaseApp_CheckAndFixDependenciesAsync_m2714249249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.DependencyStatus Firebase.FirebaseApp::CheckDependencies()
extern "C"  int32_t FirebaseApp_CheckDependencies_m3057655094 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.DependencyStatus Firebase.FirebaseApp::CheckDependenciesInternal()
extern "C"  int32_t FirebaseApp_CheckDependenciesInternal_m656726489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task Firebase.FirebaseApp::FixDependenciesAsync()
extern "C"  Task_t1843236107 * FirebaseApp_FixDependenciesAsync_m1443395599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::ResetDefaultAppCPtr()
extern "C"  void FirebaseApp_ResetDefaultAppCPtr_m4219420728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.AppOptions Firebase.FirebaseApp::get_Options()
extern "C"  AppOptions_t1641189195 * FirebaseApp_get_Options_m763126714 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.AppOptionsInternal Firebase.FirebaseApp::options()
extern "C"  AppOptionsInternal_t2434721572 * FirebaseApp_options_m2872390136 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FirebaseApp::get_Name()
extern "C"  String_t* FirebaseApp_get_Name_m3737532545 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateInternal_m3582107925 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal(Firebase.AppOptionsInternal)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateInternal_m1012730626 (Il2CppObject * __this /* static, unused */, AppOptionsInternal_t2434721572 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal(Firebase.AppOptionsInternal,System.String)
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateInternal_m527880490 (Il2CppObject * __this /* static, unused */, AppOptionsInternal_t2434721572 * ___options0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::SetLogLevelInternal(Firebase.LogLevel)
extern "C"  void FirebaseApp_SetLogLevelInternal_m2426332144 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.LogLevel Firebase.FirebaseApp::GetLogLevelInternal()
extern "C"  int32_t FirebaseApp_GetLogLevelInternal_m2427150631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FirebaseApp::get_DefaultName()
extern "C"  String_t* FirebaseApp_get_DefaultName_m2031394344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::.cctor()
extern "C"  void FirebaseApp__cctor_m2391020073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::<Dispose>m__0()
extern "C"  void FirebaseApp_U3CDisposeU3Em__0_m3004522492 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::<Create>m__1()
extern "C"  FirebaseApp_t210707726 * FirebaseApp_U3CCreateU3Em__1_m64761912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.DependencyStatus Firebase.FirebaseApp::<CheckDependenciesAsync>m__2()
extern "C"  int32_t FirebaseApp_U3CCheckDependenciesAsyncU3Em__2_m635190313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::<CheckAndFixDependenciesAsync>m__3(System.Threading.Tasks.Task`1<Firebase.DependencyStatus>)
extern "C"  Task_1_t1872448422 * FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__3_m3697096582 (Il2CppObject * __this /* static, unused */, Task_1_t1872448422 * ___checkTask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.DependencyStatus Firebase.FirebaseApp::<CheckAndFixDependenciesAsync>m__4(System.Threading.Tasks.Task)
extern "C"  int32_t FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__4_m3659431453 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
