﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<Firebase.DependencyStatus>
struct U3CContinueWithU3Ec__AnonStorey1_t2422199034;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<Firebase.DependencyStatus>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1__ctor_m3382395324_gshared (U3CContinueWithU3Ec__AnonStorey1_t2422199034 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1__ctor_m3382395324(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t2422199034 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1__ctor_m3382395324_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<Firebase.DependencyStatus>::<>m__0()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m2111482263_gshared (U3CContinueWithU3Ec__AnonStorey1_t2422199034 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m2111482263(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t2422199034 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m2111482263_gshared)(__this, method)
