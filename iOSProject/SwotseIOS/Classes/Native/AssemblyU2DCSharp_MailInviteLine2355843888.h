﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// MailInviteTableController
struct MailInviteTableController_t1221845970;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInviteLine
struct  MailInviteLine_t2355843888  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text MailInviteLine::senderName
	Text_t356221433 * ___senderName_2;
	// UnityEngine.UI.Text MailInviteLine::date
	Text_t356221433 * ___date_3;
	// MailInviteTableController MailInviteLine::owner
	MailInviteTableController_t1221845970 * ___owner_4;
	// System.Int64 MailInviteLine::inviteId
	int64_t ___inviteId_5;

public:
	inline static int32_t get_offset_of_senderName_2() { return static_cast<int32_t>(offsetof(MailInviteLine_t2355843888, ___senderName_2)); }
	inline Text_t356221433 * get_senderName_2() const { return ___senderName_2; }
	inline Text_t356221433 ** get_address_of_senderName_2() { return &___senderName_2; }
	inline void set_senderName_2(Text_t356221433 * value)
	{
		___senderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___senderName_2, value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(MailInviteLine_t2355843888, ___date_3)); }
	inline Text_t356221433 * get_date_3() const { return ___date_3; }
	inline Text_t356221433 ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(Text_t356221433 * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier(&___date_3, value);
	}

	inline static int32_t get_offset_of_owner_4() { return static_cast<int32_t>(offsetof(MailInviteLine_t2355843888, ___owner_4)); }
	inline MailInviteTableController_t1221845970 * get_owner_4() const { return ___owner_4; }
	inline MailInviteTableController_t1221845970 ** get_address_of_owner_4() { return &___owner_4; }
	inline void set_owner_4(MailInviteTableController_t1221845970 * value)
	{
		___owner_4 = value;
		Il2CppCodeGenWriteBarrier(&___owner_4, value);
	}

	inline static int32_t get_offset_of_inviteId_5() { return static_cast<int32_t>(offsetof(MailInviteLine_t2355843888, ___inviteId_5)); }
	inline int64_t get_inviteId_5() const { return ___inviteId_5; }
	inline int64_t* get_address_of_inviteId_5() { return &___inviteId_5; }
	inline void set_inviteId_5(int64_t value)
	{
		___inviteId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
