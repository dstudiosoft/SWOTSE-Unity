﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1077033874(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t302337493 *, Dictionary_2_t1599277650 *, const MethodInfo*))ValueCollection__ctor_m2077882560_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2648898936(__this, ___item0, method) ((  void (*) (ValueCollection_t302337493 *, ArmyGeneralModel_t609759248 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1072541475(__this, method) ((  void (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m276027276(__this, ___item0, method) ((  bool (*) (ValueCollection_t302337493 *, ArmyGeneralModel_t609759248 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4058548678_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2528147803(__this, ___item0, method) ((  bool (*) (ValueCollection_t302337493 *, ArmyGeneralModel_t609759248 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2140066781(__this, method) ((  Il2CppObject* (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1223126429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3235271825(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t302337493 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3768245709_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1529520192(__this, method) ((  Il2CppObject * (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m674376046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2069194167(__this, method) ((  bool (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3628342391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3966319405(__this, method) ((  bool (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4219624793_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1456506177(__this, method) ((  Il2CppObject * (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m546954107(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t302337493 *, ArmyGeneralModelU5BU5D_t2507881905*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1607943379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3172887412(__this, method) ((  Enumerator_t3285810414  (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_GetEnumerator_m1386936904_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,ArmyGeneralModel>::get_Count()
#define ValueCollection_get_Count_m925198477(__this, method) ((  int32_t (*) (ValueCollection_t302337493 *, const MethodInfo*))ValueCollection_get_Count_m2322833661_gshared)(__this, method)
