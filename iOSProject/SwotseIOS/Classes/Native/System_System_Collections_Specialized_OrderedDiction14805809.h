﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary
struct  OrderedDictionary_t14805809  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Collections.Specialized.OrderedDictionary::list
	ArrayList_t4252133567 * ___list_0;
	// System.Collections.Hashtable System.Collections.Specialized.OrderedDictionary::hash
	Hashtable_t909839986 * ___hash_1;
	// System.Boolean System.Collections.Specialized.OrderedDictionary::readOnly
	bool ___readOnly_2;
	// System.Int32 System.Collections.Specialized.OrderedDictionary::initialCapacity
	int32_t ___initialCapacity_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.OrderedDictionary::serializationInfo
	SerializationInfo_t228987430 * ___serializationInfo_4;
	// System.Collections.IEqualityComparer System.Collections.Specialized.OrderedDictionary::comparer
	Il2CppObject * ___comparer_5;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___list_0)); }
	inline ArrayList_t4252133567 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4252133567 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4252133567 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___hash_1)); }
	inline Hashtable_t909839986 * get_hash_1() const { return ___hash_1; }
	inline Hashtable_t909839986 ** get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(Hashtable_t909839986 * value)
	{
		___hash_1 = value;
		Il2CppCodeGenWriteBarrier(&___hash_1, value);
	}

	inline static int32_t get_offset_of_readOnly_2() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___readOnly_2)); }
	inline bool get_readOnly_2() const { return ___readOnly_2; }
	inline bool* get_address_of_readOnly_2() { return &___readOnly_2; }
	inline void set_readOnly_2(bool value)
	{
		___readOnly_2 = value;
	}

	inline static int32_t get_offset_of_initialCapacity_3() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___initialCapacity_3)); }
	inline int32_t get_initialCapacity_3() const { return ___initialCapacity_3; }
	inline int32_t* get_address_of_initialCapacity_3() { return &___initialCapacity_3; }
	inline void set_initialCapacity_3(int32_t value)
	{
		___initialCapacity_3 = value;
	}

	inline static int32_t get_offset_of_serializationInfo_4() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___serializationInfo_4)); }
	inline SerializationInfo_t228987430 * get_serializationInfo_4() const { return ___serializationInfo_4; }
	inline SerializationInfo_t228987430 ** get_address_of_serializationInfo_4() { return &___serializationInfo_4; }
	inline void set_serializationInfo_4(SerializationInfo_t228987430 * value)
	{
		___serializationInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___serializationInfo_4, value);
	}

	inline static int32_t get_offset_of_comparer_5() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ___comparer_5)); }
	inline Il2CppObject * get_comparer_5() const { return ___comparer_5; }
	inline Il2CppObject ** get_address_of_comparer_5() { return &___comparer_5; }
	inline void set_comparer_5(Il2CppObject * value)
	{
		___comparer_5 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
