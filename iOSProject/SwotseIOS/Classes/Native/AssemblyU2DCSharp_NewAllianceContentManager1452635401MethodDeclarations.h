﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewAllianceContentManager
struct NewAllianceContentManager_t1452635401;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NewAllianceContentManager::.ctor()
extern "C"  void NewAllianceContentManager__ctor_m4155575362 (NewAllianceContentManager_t1452635401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewAllianceContentManager::SendNewAllianceRequest()
extern "C"  void NewAllianceContentManager_SendNewAllianceRequest_m4227420982 (NewAllianceContentManager_t1452635401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewAllianceContentManager::RequestSent(System.Object,System.String)
extern "C"  void NewAllianceContentManager_RequestSent_m3845995221 (NewAllianceContentManager_t1452635401 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
