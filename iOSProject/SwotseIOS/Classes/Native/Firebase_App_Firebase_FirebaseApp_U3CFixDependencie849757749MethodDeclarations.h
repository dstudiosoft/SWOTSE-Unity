﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6
struct U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6::.ctor()
extern "C"  void U3CFixDependenciesAsyncU3Ec__AnonStorey6__ctor_m1174158344 (U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6::<>m__0()
extern "C"  void U3CFixDependenciesAsyncU3Ec__AnonStorey6_U3CU3Em__0_m2446835109 (U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6::<>m__1(System.Threading.Tasks.Task)
extern "C"  void U3CFixDependenciesAsyncU3Ec__AnonStorey6_U3CU3Em__1_m3922710028 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
