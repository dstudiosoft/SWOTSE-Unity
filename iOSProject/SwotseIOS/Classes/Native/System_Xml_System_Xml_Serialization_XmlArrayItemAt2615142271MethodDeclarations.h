﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlArrayItemAttribute
struct XmlArrayItemAttribute_t2615142271;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Xml.Serialization.XmlArrayItemAttribute::.ctor(System.String)
extern "C"  void XmlArrayItemAttribute__ctor_m688420164 (XmlArrayItemAttribute_t2615142271 * __this, String_t* ___elementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
