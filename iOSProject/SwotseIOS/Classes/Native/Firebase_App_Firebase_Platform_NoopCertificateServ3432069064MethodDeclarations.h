﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.NoopCertificateService
struct NoopCertificateService_t3432069064;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Platform.NoopCertificateService::.ctor()
extern "C"  void NoopCertificateService__ctor_m3870237845 (NoopCertificateService_t3432069064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.NoopCertificateService Firebase.Platform.NoopCertificateService::get_Instance()
extern "C"  NoopCertificateService_t3432069064 * NoopCertificateService_get_Instance_m1732268838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Platform.NoopCertificateService::Install(Firebase.FirebaseApp)
extern "C"  X509CertificateCollection_t1197680765 * NoopCertificateService_Install_m2887829784 (NoopCertificateService_t3432069064 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.NoopCertificateService::.cctor()
extern "C"  void NoopCertificateService__cctor_m2347295600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
