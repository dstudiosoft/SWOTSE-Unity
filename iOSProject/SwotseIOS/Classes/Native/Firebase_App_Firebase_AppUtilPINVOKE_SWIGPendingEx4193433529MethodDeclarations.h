﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppUtilPINVOKE/SWIGPendingException
struct SWIGPendingException_t4193433529;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.ctor()
extern "C"  void SWIGPendingException__ctor_m3036447238 (SWIGPendingException_t4193433529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern "C"  bool SWIGPendingException_get_Pending_m1368465104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern "C"  void SWIGPendingException_Set_m944203304 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t1927440687 * SWIGPendingException_Retrieve_m197352211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.cctor()
extern "C"  void SWIGPendingException__cctor_m4006952997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
