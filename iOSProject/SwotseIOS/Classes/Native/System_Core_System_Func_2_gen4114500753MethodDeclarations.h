﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2888474301MethodDeclarations.h"

// System.Void System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3195459772(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t4114500753 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1039604338_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>::Invoke(T)
#define Func_2_Invoke_m3527150048(__this, ___arg10, method) ((  int32_t (*) (Func_2_t4114500753 *, Task_t1843236107 *, const MethodInfo*))Func_2_Invoke_m1697416160_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m811231899(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t4114500753 *, Task_t1843236107 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2330127843_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2927799678(__this, ___result0, method) ((  int32_t (*) (Func_2_t4114500753 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1634188630_gshared)(__this, ___result0, method)
