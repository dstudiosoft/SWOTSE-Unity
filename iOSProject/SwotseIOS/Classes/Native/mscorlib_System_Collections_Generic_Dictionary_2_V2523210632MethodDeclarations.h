﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2728742885(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2523210632 *, Dictionary_2_t3820150789 *, const MethodInfo*))ValueCollection__ctor_m2077882560_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1009170547(__this, ___item0, method) ((  void (*) (ValueCollection_t2523210632 *, BuildingModel_t2830632387 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4292717438(__this, method) ((  void (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2574097693(__this, ___item0, method) ((  bool (*) (ValueCollection_t2523210632 *, BuildingModel_t2830632387 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4058548678_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3660863580(__this, ___item0, method) ((  bool (*) (ValueCollection_t2523210632 *, BuildingModel_t2830632387 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3055073908(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1223126429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1823878710(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2523210632 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3768245709_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2494306403(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m674376046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1380858320(__this, method) ((  bool (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3628342391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m352118146(__this, method) ((  bool (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4219624793_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1829513356(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m889435482(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2523210632 *, BuildingModelU5BU5D_t3179303890*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1607943379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2253385135(__this, method) ((  Enumerator_t1211716257  (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_GetEnumerator_m1386936904_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,BuildingModel>::get_Count()
#define ValueCollection_get_Count_m4141966730(__this, method) ((  int32_t (*) (ValueCollection_t2523210632 *, const MethodInfo*))ValueCollection_get_Count_m2322833661_gshared)(__this, method)
