﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.RequestStream
struct RequestStream_t550151197;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"

// System.Void System.Net.RequestStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void RequestStream__ctor_m3950690693 (RequestStream_t550151197 * __this, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32,System.Int64)
extern "C"  void RequestStream__ctor_m339144099 (RequestStream_t550151197 * __this, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___buffer1, int32_t ___offset2, int32_t ___length3, int64_t ___contentlength4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.RequestStream::get_CanRead()
extern "C"  bool RequestStream_get_CanRead_m3910040742 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.RequestStream::get_CanSeek()
extern "C"  bool RequestStream_get_CanSeek_m543118406 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.RequestStream::get_CanWrite()
extern "C"  bool RequestStream_get_CanWrite_m1955894529 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.RequestStream::get_Length()
extern "C"  int64_t RequestStream_get_Length_m43220493 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.RequestStream::get_Position()
extern "C"  int64_t RequestStream_get_Position_m4105570648 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::set_Position(System.Int64)
extern "C"  void RequestStream_set_Position_m1303894463 (RequestStream_t550151197 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::Close()
extern "C"  void RequestStream_Close_m2386180031 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::Flush()
extern "C"  void RequestStream_Flush_m400711417 (RequestStream_t550151197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.RequestStream::FillFromBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t RequestStream_FillFromBuffer_m2790718501 (RequestStream_t550151197 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___off1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.RequestStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t RequestStream_Read_m755536568 (RequestStream_t550151197 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.RequestStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestStream_BeginRead_m3760409363 (RequestStream_t550151197 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.RequestStream::EndRead(System.IAsyncResult)
extern "C"  int32_t RequestStream_EndRead_m735474159 (RequestStream_t550151197 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.RequestStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t RequestStream_Seek_m1829581133 (RequestStream_t550151197 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::SetLength(System.Int64)
extern "C"  void RequestStream_SetLength_m595897389 (RequestStream_t550151197 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void RequestStream_Write_m60189219 (RequestStream_t550151197 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.RequestStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestStream_BeginWrite_m1549198466 (RequestStream_t550151197 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.RequestStream::EndWrite(System.IAsyncResult)
extern "C"  void RequestStream_EndWrite_m3944994820 (RequestStream_t550151197 * __this, Il2CppObject * ___async_result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
