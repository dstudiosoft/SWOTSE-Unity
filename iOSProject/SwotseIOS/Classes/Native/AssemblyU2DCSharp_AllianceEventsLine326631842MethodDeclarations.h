﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceEventsLine
struct AllianceEventsLine_t326631842;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceEventsLine::.ctor()
extern "C"  void AllianceEventsLine__ctor_m3301546919 (AllianceEventsLine_t326631842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsLine::SetDate(System.String)
extern "C"  void AllianceEventsLine_SetDate_m3126921703 (AllianceEventsLine_t326631842 * __this, String_t* ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsLine::SetRemark(System.String)
extern "C"  void AllianceEventsLine_SetRemark_m2526207081 (AllianceEventsLine_t326631842 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
