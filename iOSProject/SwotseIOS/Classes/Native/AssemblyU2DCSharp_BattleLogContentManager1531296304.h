﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleLogContentManager
struct  BattleLogContentManager_t1531296304  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text BattleLogContentManager::content
	Text_t356221433 * ___content_2;
	// System.String BattleLogContentManager::log
	String_t* ___log_3;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(BattleLogContentManager_t1531296304, ___content_2)); }
	inline Text_t356221433 * get_content_2() const { return ___content_2; }
	inline Text_t356221433 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Text_t356221433 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier(&___content_2, value);
	}

	inline static int32_t get_offset_of_log_3() { return static_cast<int32_t>(offsetof(BattleLogContentManager_t1531296304, ___log_3)); }
	inline String_t* get_log_3() const { return ___log_3; }
	inline String_t** get_address_of_log_3() { return &___log_3; }
	inline void set_log_3(String_t* value)
	{
		___log_3 = value;
		Il2CppCodeGenWriteBarrier(&___log_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
