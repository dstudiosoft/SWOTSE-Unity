﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PanelKnightTableLine
struct PanelKnightTableLine_t1314777363;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelKnightsTableController
struct  PanelKnightsTableController_t1723197164  : public MonoBehaviour_t1158329972
{
public:
	// PanelKnightTableLine PanelKnightsTableController::m_cellPrefab
	PanelKnightTableLine_t1314777363 * ___m_cellPrefab_2;
	// Tacticsoft.TableView PanelKnightsTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.ArrayList PanelKnightsTableController::knightList
	ArrayList_t4252133567 * ___knightList_4;
	// System.Int32 PanelKnightsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t1723197164, ___m_cellPrefab_2)); }
	inline PanelKnightTableLine_t1314777363 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline PanelKnightTableLine_t1314777363 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(PanelKnightTableLine_t1314777363 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t1723197164, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_knightList_4() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t1723197164, ___knightList_4)); }
	inline ArrayList_t4252133567 * get_knightList_4() const { return ___knightList_4; }
	inline ArrayList_t4252133567 ** get_address_of_knightList_4() { return &___knightList_4; }
	inline void set_knightList_4(ArrayList_t4252133567 * value)
	{
		___knightList_4 = value;
		Il2CppCodeGenWriteBarrier(&___knightList_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t1723197164, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
