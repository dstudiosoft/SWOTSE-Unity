﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketSellTableController
struct MarketSellTableController_t4072176656;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void MarketSellTableController::.ctor()
extern "C"  void MarketSellTableController__ctor_m4227451303 (MarketSellTableController_t4072176656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableController::Start()
extern "C"  void MarketSellTableController_Start_m2034397483 (MarketSellTableController_t4072176656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MarketSellTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t MarketSellTableController_GetNumberOfRowsForTableView_m703416435 (MarketSellTableController_t4072176656 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MarketSellTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float MarketSellTableController_GetHeightForRowInTableView_m163274123 (MarketSellTableController_t4072176656 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell MarketSellTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * MarketSellTableController_GetCellForRowInTableView_m2487218410 (MarketSellTableController_t4072176656 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketSellTableController::ReloadTable(System.Collections.ArrayList)
extern "C"  void MarketSellTableController_ReloadTable_m2048566895 (MarketSellTableController_t4072176656 * __this, ArrayList_t4252133567 * ___managerLots0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
