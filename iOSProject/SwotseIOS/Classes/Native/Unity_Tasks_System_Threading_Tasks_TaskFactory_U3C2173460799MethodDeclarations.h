﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<Firebase.DependencyStatus>
struct U3CStartNewU3Ec__AnonStorey0_1_t2173460799;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<Firebase.DependencyStatus>::.ctor()
extern "C"  void U3CStartNewU3Ec__AnonStorey0_1__ctor_m1113851255_gshared (U3CStartNewU3Ec__AnonStorey0_1_t2173460799 * __this, const MethodInfo* method);
#define U3CStartNewU3Ec__AnonStorey0_1__ctor_m1113851255(__this, method) ((  void (*) (U3CStartNewU3Ec__AnonStorey0_1_t2173460799 *, const MethodInfo*))U3CStartNewU3Ec__AnonStorey0_1__ctor_m1113851255_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<Firebase.DependencyStatus>::<>m__0()
extern "C"  void U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2046468458_gshared (U3CStartNewU3Ec__AnonStorey0_1_t2173460799 * __this, const MethodInfo* method);
#define U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2046468458(__this, method) ((  void (*) (U3CStartNewU3Ec__AnonStorey0_1_t2173460799 *, const MethodInfo*))U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2046468458_gshared)(__this, method)
