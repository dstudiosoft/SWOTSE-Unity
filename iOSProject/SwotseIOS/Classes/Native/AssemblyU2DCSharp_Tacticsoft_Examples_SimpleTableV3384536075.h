﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.Examples.VisibleCounterCell
struct VisibleCounterCell_t1953764842;
// Tacticsoft.TableView
struct TableView_t3179510217;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.SimpleTableViewController
struct  SimpleTableViewController_t3384536075  : public MonoBehaviour_t1158329972
{
public:
	// Tacticsoft.Examples.VisibleCounterCell Tacticsoft.Examples.SimpleTableViewController::m_cellPrefab
	VisibleCounterCell_t1953764842 * ___m_cellPrefab_2;
	// Tacticsoft.TableView Tacticsoft.Examples.SimpleTableViewController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Int32 Tacticsoft.Examples.SimpleTableViewController::m_numRows
	int32_t ___m_numRows_4;
	// System.Int32 Tacticsoft.Examples.SimpleTableViewController::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t3384536075, ___m_cellPrefab_2)); }
	inline VisibleCounterCell_t1953764842 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline VisibleCounterCell_t1953764842 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(VisibleCounterCell_t1953764842 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t3384536075, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t3384536075, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_5() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t3384536075, ___m_numInstancesCreated_5)); }
	inline int32_t get_m_numInstancesCreated_5() const { return ___m_numInstancesCreated_5; }
	inline int32_t* get_address_of_m_numInstancesCreated_5() { return &___m_numInstancesCreated_5; }
	inline void set_m_numInstancesCreated_5(int32_t value)
	{
		___m_numInstancesCreated_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
