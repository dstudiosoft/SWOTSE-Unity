﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Chain
struct X509Chain_t1938971908;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472866;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t324051958;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3592472865.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl2843686920.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.X509.X509Chain::.ctor()
extern "C"  void X509Chain__ctor_m3522797144 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain__ctor_m1113875360 (X509Chain_t1938971908 * __this, X509CertificateCollection_t3592472866 * ___chain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_Chain()
extern "C"  X509CertificateCollection_t3592472866 * X509Chain_get_Chain_m2439162593 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::get_Root()
extern "C"  X509Certificate_t324051958 * X509Chain_get_Root_m223448854 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::get_Status()
extern "C"  int32_t X509Chain_get_Status_m2726750609 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_TrustAnchors()
extern "C"  X509CertificateCollection_t3592472866 * X509Chain_get_TrustAnchors_m930867182 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::set_TrustAnchors(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_set_TrustAnchors_m1074874977 (X509Chain_t1938971908 * __this, X509CertificateCollection_t3592472866 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificate(Mono.Security.X509.X509Certificate)
extern "C"  void X509Chain_LoadCertificate_m685234143 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___x5090, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificates(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_LoadCertificates_m159768270 (X509Chain_t1938971908 * __this, X509CertificateCollection_t3592472866 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindByIssuerName(System.String)
extern "C"  X509Certificate_t324051958 * X509Chain_FindByIssuerName_m788696423 (X509Chain_t1938971908 * __this, String_t* ___issuerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::Build(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_Build_m175470596 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___leaf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::Reset()
extern "C"  void X509Chain_Reset_m2723628639 (X509Chain_t1938971908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsValid(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsValid_m4184995162 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___cert0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateParent(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t324051958 * X509Chain_FindCertificateParent_m3122224229 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___child0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateRoot(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t324051958 * X509Chain_FindCertificateRoot_m589250107 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___potentialRoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsTrusted(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsTrusted_m2834338719 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___potentialTrusted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsParent(Mono.Security.X509.X509Certificate,Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsParent_m2578316632 (X509Chain_t1938971908 * __this, X509Certificate_t324051958 * ___child0, X509Certificate_t324051958 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
