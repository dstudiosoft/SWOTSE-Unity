﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Single>
struct IDictionary_2_t3378386284;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>[]
struct KeyValuePair_2U5BU5D_t1855990392;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>
struct IEnumerator_1_t612171912;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Single>
struct KeyCollection_t3567833338;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Single>
struct ValueCollection_t4082362706;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23136648085.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2404360269.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor()
extern "C"  void Dictionary_2__ctor_m3391555206_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3391555206(__this, method) ((  void (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2__ctor_m3391555206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2762487618_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2762487618(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2762487618_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m243166345_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m243166345(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m243166345_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3902907682_gshared (Dictionary_2_t1084335567 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3902907682(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3902907682_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2234185842_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2234185842(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2234185842_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2454433297_gshared (Dictionary_2_t1084335567 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2454433297(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2454433297_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m4241926932_gshared (Dictionary_2_t1084335567 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m4241926932(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1084335567 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m4241926932_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3213273385_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3213273385(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3213273385_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2168410329_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2168410329(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2168410329_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2792069534_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2792069534(__this, method) ((  bool (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2792069534_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1095735509_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1095735509(__this, method) ((  bool (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1095735509_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3733234379_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3733234379(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3733234379_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3530112576_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3530112576(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3530112576_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3669910321_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3669910321(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3669910321_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1964787373_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1964787373(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1964787373_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2275228196_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2275228196(__this, ___key0, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2275228196_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1502719723_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1502719723(__this, method) ((  bool (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1502719723_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2411451247_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2411451247(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2411451247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2958643449_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2958643449(__this, method) ((  bool (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2958643449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1682994678_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2_t3136648085  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1682994678(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1084335567 *, KeyValuePair_2_t3136648085 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1682994678_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2672066098_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2_t3136648085  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2672066098(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1084335567 *, KeyValuePair_2_t3136648085 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2672066098_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1462135666_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2U5BU5D_t1855990392* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1462135666(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1084335567 *, KeyValuePair_2U5BU5D_t1855990392*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1462135666_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344380959_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2_t3136648085  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344380959(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1084335567 *, KeyValuePair_2_t3136648085 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344380959_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m511049427_gshared (Dictionary_2_t1084335567 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m511049427(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m511049427_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3116378508_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3116378508(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3116378508_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2053885001_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2053885001(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2053885001_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3699676118_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3699676118(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3699676118_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1494160147_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1494160147(__this, method) ((  int32_t (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_get_Count_m1494160147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Item(TKey)
extern "C"  float Dictionary_2_get_Item_m1874253002_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1874253002(__this, ___key0, method) ((  float (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1874253002_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m407818537_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m407818537(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, float, const MethodInfo*))Dictionary_2_set_Item_m407818537_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m565436815_gshared (Dictionary_2_t1084335567 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m565436815(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m565436815_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3562917158_gshared (Dictionary_2_t1084335567 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3562917158(__this, ___size0, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3562917158_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1952139820_gshared (Dictionary_2_t1084335567 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1952139820(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1952139820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3136648085  Dictionary_2_make_pair_m1109600854_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1109600854(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3136648085  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_make_pair_m1109600854_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1403885096_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1403885096(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_pick_key_m1403885096_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::pick_value(TKey,TValue)
extern "C"  float Dictionary_2_pick_value_m619118760_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m619118760(__this /* static, unused */, ___key0, ___value1, method) ((  float (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_pick_value_m619118760_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m799944123_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2U5BU5D_t1855990392* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m799944123(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1084335567 *, KeyValuePair_2U5BU5D_t1855990392*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m799944123_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Resize()
extern "C"  void Dictionary_2_Resize_m3327383201_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3327383201(__this, method) ((  void (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_Resize_m3327383201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m632248438_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m632248438(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, float, const MethodInfo*))Dictionary_2_Add_m632248438_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Clear()
extern "C"  void Dictionary_2_Clear_m2229272950_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2229272950(__this, method) ((  void (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_Clear_m2229272950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1843953173_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1843953173(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1843953173_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1710382666_gshared (Dictionary_2_t1084335567 * __this, float ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1710382666(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1084335567 *, float, const MethodInfo*))Dictionary_2_ContainsValue_m1710382666_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1590014775_gshared (Dictionary_2_t1084335567 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1590014775(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1084335567 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1590014775_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3489848399_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3489848399(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3489848399_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2480175618_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2480175618(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2480175618_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m426431463_gshared (Dictionary_2_t1084335567 * __this, int32_t ___key0, float* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m426431463(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1084335567 *, int32_t, float*, const MethodInfo*))Dictionary_2_TryGetValue_m426431463_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Keys()
extern "C"  KeyCollection_t3567833338 * Dictionary_2_get_Keys_m1009732810_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1009732810(__this, method) ((  KeyCollection_t3567833338 * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_get_Keys_m1009732810_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Values()
extern "C"  ValueCollection_t4082362706 * Dictionary_2_get_Values_m624177066_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m624177066(__this, method) ((  ValueCollection_t4082362706 * (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_get_Values_m624177066_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1098192681_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1098192681(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1098192681_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::ToTValue(System.Object)
extern "C"  float Dictionary_2_ToTValue_m3613493577_gshared (Dictionary_2_t1084335567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3613493577(__this, ___value0, method) ((  float (*) (Dictionary_2_t1084335567 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3613493577_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1029786775_gshared (Dictionary_2_t1084335567 * __this, KeyValuePair_2_t3136648085  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1029786775(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1084335567 *, KeyValuePair_2_t3136648085 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1029786775_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::GetEnumerator()
extern "C"  Enumerator_t2404360269  Dictionary_2_GetEnumerator_m2688787604_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2688787604(__this, method) ((  Enumerator_t2404360269  (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2688787604_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3352221017_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3352221017(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3352221017_gshared)(__this /* static, unused */, ___key0, ___value1, method)
