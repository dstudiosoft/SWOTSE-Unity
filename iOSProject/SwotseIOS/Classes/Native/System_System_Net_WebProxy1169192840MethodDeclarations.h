﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebProxy
struct WebProxy_t1169192840;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void System.Net.WebProxy::.ctor()
extern "C"  void WebProxy__ctor_m2857702508 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String)
extern "C"  void WebProxy__ctor_m2341959666 (WebProxy_t1169192840 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri)
extern "C"  void WebProxy__ctor_m2133188609 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean)
extern "C"  void WebProxy__ctor_m1042737453 (WebProxy_t1169192840 * __this, String_t* ___address0, bool ___bypassOnLocal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Int32)
extern "C"  void WebProxy__ctor_m942734599 (WebProxy_t1169192840 * __this, String_t* ___host0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean)
extern "C"  void WebProxy__ctor_m1670938306 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___address0, bool ___bypassOnLocal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean,System.String[])
extern "C"  void WebProxy__ctor_m1061607961 (WebProxy_t1169192840 * __this, String_t* ___address0, bool ___bypassOnLocal1, StringU5BU5D_t1642385972* ___bypassList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[])
extern "C"  void WebProxy__ctor_m1440251310 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___address0, bool ___bypassOnLocal1, StringU5BU5D_t1642385972* ___bypassList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean,System.String[],System.Net.ICredentials)
extern "C"  void WebProxy__ctor_m2154589834 (WebProxy_t1169192840 * __this, String_t* ___address0, bool ___bypassOnLocal1, StringU5BU5D_t1642385972* ___bypassList2, Il2CppObject * ___credentials3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[],System.Net.ICredentials)
extern "C"  void WebProxy__ctor_m3679722173 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___address0, bool ___bypassOnLocal1, StringU5BU5D_t1642385972* ___bypassList2, Il2CppObject * ___credentials3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy__ctor_m2450728151 (WebProxy_t1169192840 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m1342312083 (WebProxy_t1169192840 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::get_Address()
extern "C"  Uri_t19570940 * WebProxy_get_Address_m3346416281 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_Address(System.Uri)
extern "C"  void WebProxy_set_Address_m1913416788 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Net.WebProxy::get_BypassArrayList()
extern "C"  ArrayList_t4252133567 * WebProxy_get_BypassArrayList_m754857652 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Net.WebProxy::get_BypassList()
extern "C"  StringU5BU5D_t1642385972* WebProxy_get_BypassList_m220297508 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_BypassList(System.String[])
extern "C"  void WebProxy_set_BypassList_m3379969141 (WebProxy_t1169192840 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::get_BypassProxyOnLocal()
extern "C"  bool WebProxy_get_BypassProxyOnLocal_m3684827135 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_BypassProxyOnLocal(System.Boolean)
extern "C"  void WebProxy_set_BypassProxyOnLocal_m4265945506 (WebProxy_t1169192840 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.WebProxy::get_Credentials()
extern "C"  Il2CppObject * WebProxy_get_Credentials_m476852361 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_Credentials(System.Net.ICredentials)
extern "C"  void WebProxy_set_Credentials_m4188037574 (WebProxy_t1169192840 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::get_UseDefaultCredentials()
extern "C"  bool WebProxy_get_UseDefaultCredentials_m2426255561 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_UseDefaultCredentials(System.Boolean)
extern "C"  void WebProxy_set_UseDefaultCredentials_m3782955912 (WebProxy_t1169192840 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebProxy System.Net.WebProxy::GetDefaultProxy()
extern "C"  WebProxy_t1169192840 * WebProxy_GetDefaultProxy_m1796308050 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::GetProxy(System.Uri)
extern "C"  Uri_t19570940 * WebProxy_GetProxy_m1249556681 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::IsBypassed(System.Uri)
extern "C"  bool WebProxy_IsBypassed_m907120600 (WebProxy_t1169192840 * __this, Uri_t19570940 * ___host0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy_GetObjectData_m2019316656 (WebProxy_t1169192840 * __this, SerializationInfo_t228987430 * ___serializationInfo0, StreamingContext_t1417235061  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::CheckBypassList()
extern "C"  void WebProxy_CheckBypassList_m565639068 (WebProxy_t1169192840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::ToUri(System.String)
extern "C"  Uri_t19570940 * WebProxy_ToUri_m1143740517 (Il2CppObject * __this /* static, unused */, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
