﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingConstantModel
struct  BuildingConstantModel_t2546650549  : public Il2CppObject
{
public:
	// System.String BuildingConstantModel::id
	String_t* ___id_0;
	// System.String BuildingConstantModel::building_name
	String_t* ___building_name_1;
	// System.String BuildingConstantModel::description
	String_t* ___description_2;
	// System.Collections.ArrayList BuildingConstantModel::building_prices
	ArrayList_t4252133567 * ___building_prices_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2546650549, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_building_name_1() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2546650549, ___building_name_1)); }
	inline String_t* get_building_name_1() const { return ___building_name_1; }
	inline String_t** get_address_of_building_name_1() { return &___building_name_1; }
	inline void set_building_name_1(String_t* value)
	{
		___building_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___building_name_1, value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2546650549, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier(&___description_2, value);
	}

	inline static int32_t get_offset_of_building_prices_3() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2546650549, ___building_prices_3)); }
	inline ArrayList_t4252133567 * get_building_prices_3() const { return ___building_prices_3; }
	inline ArrayList_t4252133567 ** get_address_of_building_prices_3() { return &___building_prices_3; }
	inline void set_building_prices_3(ArrayList_t4252133567 * value)
	{
		___building_prices_3 = value;
		Il2CppCodeGenWriteBarrier(&___building_prices_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
