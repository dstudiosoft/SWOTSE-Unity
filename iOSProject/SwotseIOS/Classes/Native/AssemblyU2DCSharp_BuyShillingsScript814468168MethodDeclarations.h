﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyShillingsScript
struct BuyShillingsScript_t814468168;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BuyShillingsScript::.ctor()
extern "C"  void BuyShillingsScript__ctor_m2131589643 (BuyShillingsScript_t814468168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyShillingsScript::BuyShillings()
extern "C"  void BuyShillingsScript_BuyShillings_m2111095016 (BuyShillingsScript_t814468168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuyShillingsScript::SendBuyRequest(System.String)
extern "C"  Il2CppObject * BuyShillingsScript_SendBuyRequest_m3656588746 (BuyShillingsScript_t814468168 * __this, String_t* ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
