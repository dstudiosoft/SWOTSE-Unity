﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingInformationManager
struct BuildingInformationManager_t1741266979;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BuildingInformationManager::.ctor()
extern "C"  void BuildingInformationManager__ctor_m2170063868 (BuildingInformationManager_t1741266979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::OnEnable()
extern "C"  void BuildingInformationManager_OnEnable_m3873365072 (BuildingInformationManager_t1741266979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::Upgrade()
extern "C"  void BuildingInformationManager_Upgrade_m411772832 (BuildingInformationManager_t1741266979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::Downgrade()
extern "C"  void BuildingInformationManager_Downgrade_m1966260557 (BuildingInformationManager_t1741266979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::HandleUpdate(System.Object,System.String)
extern "C"  void BuildingInformationManager_HandleUpdate_m270463177 (BuildingInformationManager_t1741266979 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::UpdateTax()
extern "C"  void BuildingInformationManager_UpdateTax_m3119333022 (BuildingInformationManager_t1741266979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingInformationManager::TaxUpdated(System.Object,System.String)
extern "C"  void BuildingInformationManager_TaxUpdated_m2491798326 (BuildingInformationManager_t1741266979 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
