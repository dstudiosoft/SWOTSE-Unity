﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceContentChanger
struct ResidenceContentChanger_t3315848035;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void ResidenceContentChanger::.ctor()
extern "C"  void ResidenceContentChanger__ctor_m2593849524 (ResidenceContentChanger_t3315848035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::OnEnable()
extern "C"  void ResidenceContentChanger_OnEnable_m959791880 (ResidenceContentChanger_t3315848035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::openContent(System.String)
extern "C"  void ResidenceContentChanger_openContent_m3366098249 (ResidenceContentChanger_t3315848035 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::closeAllContent()
extern "C"  void ResidenceContentChanger_closeAllContent_m2054661860 (ResidenceContentChanger_t3315848035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::SwitchIcon(System.Boolean)
extern "C"  void ResidenceContentChanger_SwitchIcon_m4010571618 (ResidenceContentChanger_t3315848035 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::ChangeNameAndIcon()
extern "C"  void ResidenceContentChanger_ChangeNameAndIcon_m2390730343 (ResidenceContentChanger_t3315848035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceContentChanger::NameAndIconChanged(System.Object,System.String)
extern "C"  void ResidenceContentChanger_NameAndIconChanged_m1368967027 (ResidenceContentChanger_t3315848035 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
