﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleUserModel
struct SimpleUserModel_t3156372614;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleUserModel::.ctor()
extern "C"  void SimpleUserModel__ctor_m95348899 (SimpleUserModel_t3156372614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
