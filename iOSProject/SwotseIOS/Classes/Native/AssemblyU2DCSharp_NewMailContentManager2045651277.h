﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailContentManager
struct  NewMailContentManager_t2045651277  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField NewMailContentManager::subject
	InputField_t1631627530 * ___subject_2;
	// UnityEngine.UI.InputField NewMailContentManager::message
	InputField_t1631627530 * ___message_3;

public:
	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(NewMailContentManager_t2045651277, ___subject_2)); }
	inline InputField_t1631627530 * get_subject_2() const { return ___subject_2; }
	inline InputField_t1631627530 ** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(InputField_t1631627530 * value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier(&___subject_2, value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(NewMailContentManager_t2045651277, ___message_3)); }
	inline InputField_t1631627530 * get_message_3() const { return ___message_3; }
	inline InputField_t1631627530 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(InputField_t1631627530 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier(&___message_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
