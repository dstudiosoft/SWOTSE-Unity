﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void SimpleEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void SimpleEvent__ctor_m2819253125 (SimpleEvent_t1509017202 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleEvent::Invoke(System.Object,System.String)
extern "C"  void SimpleEvent_Invoke_m3382763921 (SimpleEvent_t1509017202 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SimpleEvent::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SimpleEvent_BeginInvoke_m1555842162 (SimpleEvent_t1509017202 * __this, Il2CppObject * ___sender0, String_t* ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleEvent::EndInvoke(System.IAsyncResult)
extern "C"  void SimpleEvent_EndInvoke_m3744247739 (SimpleEvent_t1509017202 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
