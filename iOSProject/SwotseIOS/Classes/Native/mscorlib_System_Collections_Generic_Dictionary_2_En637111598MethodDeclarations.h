﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3926476145(__this, ___dictionary0, method) ((  void (*) (Enumerator_t637111598 *, Dictionary_2_t3612054192 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1044294636(__this, method) ((  Il2CppObject * (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3472618036(__this, method) ((  void (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3013389967(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4252939506(__this, method) ((  Il2CppObject * (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3128560556(__this, method) ((  Il2CppObject * (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m921466520(__this, method) ((  bool (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Current()
#define Enumerator_get_Current_m2439240900(__this, method) ((  KeyValuePair_2_t1369399414  (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2886657885(__this, method) ((  int32_t (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2157167485(__this, method) ((  Dictionary_2_t309261261 * (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Reset()
#define Enumerator_Reset_m1224638187(__this, method) ((  void (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m2590593780(__this, method) ((  void (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1537962124(__this, method) ((  void (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Dispose()
#define Enumerator_Dispose_m159491665(__this, method) ((  void (*) (Enumerator_t637111598 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
