﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResourcesModel
struct ResourcesModel_t2978985958;

#include "codegen/il2cpp-codegen.h"

// System.Void ResourcesModel::.ctor()
extern "C"  void ResourcesModel__ctor_m2124009229 (ResourcesModel_t2978985958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
