﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingsManager
struct BuildingsManager_t1345368432;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BuildingsManager::.ctor()
extern "C"  void BuildingsManager__ctor_m2516685017 (BuildingsManager_t1345368432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::add_onBuildingUpdate(SimpleEvent)
extern "C"  void BuildingsManager_add_onBuildingUpdate_m509923183 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::remove_onBuildingUpdate(SimpleEvent)
extern "C"  void BuildingsManager_remove_onBuildingUpdate_m2866106638 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::add_onGotValleys(SimpleEvent)
extern "C"  void BuildingsManager_add_onGotValleys_m2118877196 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::remove_onGotValleys(SimpleEvent)
extern "C"  void BuildingsManager_remove_onGotValleys_m3475866579 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::add_onBuildingCancelled(SimpleEvent)
extern "C"  void BuildingsManager_add_onBuildingCancelled_m332398725 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingsManager::remove_onBuildingCancelled(SimpleEvent)
extern "C"  void BuildingsManager_remove_onBuildingCancelled_m2222033032 (BuildingsManager_t1345368432 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::GetInitialCityBuildings()
extern "C"  Il2CppObject * BuildingsManager_GetInitialCityBuildings_m725122521 (BuildingsManager_t1345368432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::UpdateBuildingsAndFieldsState()
extern "C"  Il2CppObject * BuildingsManager_UpdateBuildingsAndFieldsState_m3823613340 (BuildingsManager_t1345368432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::GetInitialCityFields()
extern "C"  Il2CppObject * BuildingsManager_GetInitialCityFields_m2793959197 (BuildingsManager_t1345368432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::GetInitialCityValleys(System.Boolean)
extern "C"  Il2CppObject * BuildingsManager_GetInitialCityValleys_m3460430015 (BuildingsManager_t1345368432 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::UpdateBuilding(System.Int64,System.Int64,System.String,System.String)
extern "C"  Il2CppObject * BuildingsManager_UpdateBuilding_m2128886800 (BuildingsManager_t1345368432 * __this, int64_t ___city0, int64_t ___pit_id1, String_t* ___command2, String_t* ___location3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::UpgradeValley(System.Int64)
extern "C"  Il2CppObject * BuildingsManager_UpgradeValley_m523051044 (BuildingsManager_t1345368432 * __this, int64_t ___valley_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::DowngradeValley(System.Int64)
extern "C"  Il2CppObject * BuildingsManager_DowngradeValley_m1773729353 (BuildingsManager_t1345368432 * __this, int64_t ___valley_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::AddBuilding(System.String,System.Int64,System.Int64,System.String)
extern "C"  Il2CppObject * BuildingsManager_AddBuilding_m1981557002 (BuildingsManager_t1345368432 * __this, String_t* ___name0, int64_t ___city_id1, int64_t ___pit_id2, String_t* ___location3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::DestroyBuilding(System.Int64)
extern "C"  Il2CppObject * BuildingsManager_DestroyBuilding_m582787273 (BuildingsManager_t1345368432 * __this, int64_t ___buildingID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::CancelBuilding(System.Int64)
extern "C"  Il2CppObject * BuildingsManager_CancelBuilding_m3173073977 (BuildingsManager_t1345368432 * __this, int64_t ___event_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BuildingsManager::CancelValley(System.Int64)
extern "C"  Il2CppObject * BuildingsManager_CancelValley_m760081002 (BuildingsManager_t1345368432 * __this, int64_t ___event_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuildingsManager::IsBuildingPresent(System.String)
extern "C"  bool BuildingsManager_IsBuildingPresent_m1838061448 (BuildingsManager_t1345368432 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 BuildingsManager::BuildingsLevelSum(System.String)
extern "C"  int64_t BuildingsManager_BuildingsLevelSum_m2605711596 (BuildingsManager_t1345368432 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
