﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<System.Object>
struct U3CStartNewU3Ec__AnonStorey0_1_t2110490679;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<System.Object>::.ctor()
extern "C"  void U3CStartNewU3Ec__AnonStorey0_1__ctor_m2879731233_gshared (U3CStartNewU3Ec__AnonStorey0_1_t2110490679 * __this, const MethodInfo* method);
#define U3CStartNewU3Ec__AnonStorey0_1__ctor_m2879731233(__this, method) ((  void (*) (U3CStartNewU3Ec__AnonStorey0_1_t2110490679 *, const MethodInfo*))U3CStartNewU3Ec__AnonStorey0_1__ctor_m2879731233_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskFactory/<StartNew>c__AnonStorey0`1<System.Object>::<>m__0()
extern "C"  void U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2683955830_gshared (U3CStartNewU3Ec__AnonStorey0_1_t2110490679 * __this, const MethodInfo* method);
#define U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2683955830(__this, method) ((  void (*) (U3CStartNewU3Ec__AnonStorey0_1_t2110490679 *, const MethodInfo*))U3CStartNewU3Ec__AnonStorey0_1_U3CU3Em__0_m2683955830_gshared)(__this, method)
