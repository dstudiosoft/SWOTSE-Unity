﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;

#include "codegen/il2cpp-codegen.h"

// System.Void ArmyGeneralModel::.ctor()
extern "C"  void ArmyGeneralModel__ctor_m4148750949 (ArmyGeneralModel_t609759248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
