﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketLotModel
struct  MarketLotModel_t272455820  : public Il2CppObject
{
public:
	// System.Int64 MarketLotModel::id
	int64_t ___id_0;
	// System.Int64 MarketLotModel::owner
	int64_t ___owner_1;
	// System.Int64 MarketLotModel::city
	int64_t ___city_2;
	// System.String MarketLotModel::resource_type
	String_t* ___resource_type_3;
	// System.Int64 MarketLotModel::count
	int64_t ___count_4;
	// System.Int64 MarketLotModel::price
	int64_t ___price_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___owner_1)); }
	inline int64_t get_owner_1() const { return ___owner_1; }
	inline int64_t* get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(int64_t value)
	{
		___owner_1 = value;
	}

	inline static int32_t get_offset_of_city_2() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___city_2)); }
	inline int64_t get_city_2() const { return ___city_2; }
	inline int64_t* get_address_of_city_2() { return &___city_2; }
	inline void set_city_2(int64_t value)
	{
		___city_2 = value;
	}

	inline static int32_t get_offset_of_resource_type_3() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___resource_type_3)); }
	inline String_t* get_resource_type_3() const { return ___resource_type_3; }
	inline String_t** get_address_of_resource_type_3() { return &___resource_type_3; }
	inline void set_resource_type_3(String_t* value)
	{
		___resource_type_3 = value;
		Il2CppCodeGenWriteBarrier(&___resource_type_3, value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___count_4)); }
	inline int64_t get_count_4() const { return ___count_4; }
	inline int64_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int64_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_price_5() { return static_cast<int32_t>(offsetof(MarketLotModel_t272455820, ___price_5)); }
	inline int64_t get_price_5() const { return ___price_5; }
	inline int64_t* get_address_of_price_5() { return &___price_5; }
	inline void set_price_5(int64_t value)
	{
		___price_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
