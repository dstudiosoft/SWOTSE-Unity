﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldFieldModel
struct WorldFieldModel_t3469935653;

#include "codegen/il2cpp-codegen.h"

// System.Void WorldFieldModel::.ctor()
extern "C"  void WorldFieldModel__ctor_m2579220044 (WorldFieldModel_t3469935653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
