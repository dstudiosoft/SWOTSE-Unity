﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Platform.Default.UnityConfigExtensions
struct UnityConfigExtensions_t1763038139;
// Firebase.Platform.IAppConfigExtensions
struct IAppConfigExtensions_t273226812;
// System.String
struct String_t;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Platform.Default.UnityConfigExtensions::.ctor()
extern "C"  void UnityConfigExtensions__ctor_m2786937769 (UnityConfigExtensions_t1763038139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::get_DefaultInstance()
extern "C"  Il2CppObject * UnityConfigExtensions_get_DefaultInstance_m571534099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Platform.Default.UnityConfigExtensions::GetWriteablePath(Firebase.FirebaseApp)
extern "C"  String_t* UnityConfigExtensions_GetWriteablePath_m3633497331 (UnityConfigExtensions_t1763038139 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Platform.Default.UnityConfigExtensions::.cctor()
extern "C"  void UnityConfigExtensions__cctor_m5432252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
