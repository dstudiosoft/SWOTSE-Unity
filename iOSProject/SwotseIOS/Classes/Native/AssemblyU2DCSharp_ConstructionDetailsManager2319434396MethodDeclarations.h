﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConstructionDetailsManager
struct ConstructionDetailsManager_t2319434396;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ConstructionDetailsManager::.ctor()
extern "C"  void ConstructionDetailsManager__ctor_m1533433679 (ConstructionDetailsManager_t2319434396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionDetailsManager::Start()
extern "C"  void ConstructionDetailsManager_Start_m509373979 (ConstructionDetailsManager_t2319434396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionDetailsManager::Build(System.String)
extern "C"  void ConstructionDetailsManager_Build_m1054049439 (ConstructionDetailsManager_t2319434396 * __this, String_t* ___buildingName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionDetailsManager::HandleBuildingRequested(System.Object,System.String)
extern "C"  void ConstructionDetailsManager_HandleBuildingRequested_m2632672719 (ConstructionDetailsManager_t2319434396 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstructionDetailsManager::EnableBuilding(UnityEngine.GameObject)
extern "C"  void ConstructionDetailsManager_EnableBuilding_m2140616962 (ConstructionDetailsManager_t2319434396 * __this, GameObject_t1756533147 * ___building0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
