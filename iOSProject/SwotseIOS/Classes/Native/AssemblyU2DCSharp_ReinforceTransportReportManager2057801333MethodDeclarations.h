﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReinforceTransportReportManager
struct ReinforceTransportReportManager_t2057801333;

#include "codegen/il2cpp-codegen.h"

// System.Void ReinforceTransportReportManager::.ctor()
extern "C"  void ReinforceTransportReportManager__ctor_m3157765866 (ReinforceTransportReportManager_t2057801333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReinforceTransportReportManager::OnEnable()
extern "C"  void ReinforceTransportReportManager_OnEnable_m939798682 (ReinforceTransportReportManager_t2057801333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReinforceTransportReportManager::CloseReport()
extern "C"  void ReinforceTransportReportManager_CloseReport_m2824414412 (ReinforceTransportReportManager_t2057801333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
