﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DispatchManager/<GetAttackParams>c__IteratorA
struct U3CGetAttackParamsU3Ec__IteratorA_t1196922326;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DispatchManager/<GetAttackParams>c__IteratorA::.ctor()
extern "C"  void U3CGetAttackParamsU3Ec__IteratorA__ctor_m2322802919 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DispatchManager/<GetAttackParams>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetAttackParamsU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1406877265 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DispatchManager/<GetAttackParams>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAttackParamsU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m2008465321 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DispatchManager/<GetAttackParams>c__IteratorA::MoveNext()
extern "C"  bool U3CGetAttackParamsU3Ec__IteratorA_MoveNext_m474597785 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager/<GetAttackParams>c__IteratorA::Dispose()
extern "C"  void U3CGetAttackParamsU3Ec__IteratorA_Dispose_m2882961464 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DispatchManager/<GetAttackParams>c__IteratorA::Reset()
extern "C"  void U3CGetAttackParamsU3Ec__IteratorA_Reset_m2911782074 (U3CGetAttackParamsU3Ec__IteratorA_t1196922326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
