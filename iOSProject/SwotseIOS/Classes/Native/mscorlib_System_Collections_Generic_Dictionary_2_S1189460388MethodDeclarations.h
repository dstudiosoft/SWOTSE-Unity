﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>
struct ShimEnumerator_t1189460388;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m734440920_gshared (ShimEnumerator_t1189460388 * __this, Dictionary_2_t1084335567 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m734440920(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1189460388 *, Dictionary_2_t1084335567 *, const MethodInfo*))ShimEnumerator__ctor_m734440920_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m708477937_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m708477937(__this, method) ((  bool (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_MoveNext_m708477937_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1391674669_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1391674669(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_get_Entry_m1391674669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2961219720_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2961219720(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_get_Key_m2961219720_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m842006762_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m842006762(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_get_Value_m842006762_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4065379218_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4065379218(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_get_Current_m4065379218_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m3651345998_gshared (ShimEnumerator_t1189460388 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3651345998(__this, method) ((  void (*) (ShimEnumerator_t1189460388 *, const MethodInfo*))ShimEnumerator_Reset_m3651345998_gshared)(__this, method)
