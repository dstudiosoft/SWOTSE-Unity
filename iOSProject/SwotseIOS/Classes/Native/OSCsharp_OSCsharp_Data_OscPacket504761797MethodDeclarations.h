﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Boolean OSCsharp.Data.OscPacket::get_LittleEndianByteOrder()
extern "C"  bool OscPacket_get_LittleEndianByteOrder_m1882071268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OSCsharp.Data.OscPacket::get_Address()
extern "C"  String_t* OscPacket_get_Address_m1502094218 (OscPacket_t504761797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscPacket::set_Address(System.String)
extern "C"  void OscPacket_set_Address_m1646762965 (OscPacket_t504761797 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  Il2CppObject* OscPacket_get_Data_m65815223 (OscPacket_t504761797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscPacket::.cctor()
extern "C"  void OscPacket__cctor_m2285063083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscPacket::.ctor(System.String)
extern "C"  void OscPacket__ctor_m3636146750 (OscPacket_t504761797 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[])
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m476730285 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m3135720259 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
