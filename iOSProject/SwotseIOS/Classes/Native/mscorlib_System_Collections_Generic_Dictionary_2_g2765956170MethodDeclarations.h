﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor()
#define Dictionary_2__ctor_m2704133334(__this, method) ((  void (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2__ctor_m2974019358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m775708394(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m5447305(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2602799901_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3323985510(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2765956170 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m355726966(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3143729840_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m525695185(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2765956170 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2391180541_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1983277204(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2765956170 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4091265601(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m673000885_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1966034217(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1552474645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3257212678(__this, method) ((  bool (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m286716188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2342421229(__this, method) ((  bool (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m114053137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m875538015(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m432079528(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m76028465(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m3579378645(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3329567436(__this, ___key0, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1990362567(__this, method) ((  bool (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3529962011(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m998355849(__this, method) ((  bool (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2975150774(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2765956170 *, KeyValuePair_2_t523301392 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1911656670(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2765956170 *, KeyValuePair_2_t523301392 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2652469938(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2765956170 *, KeyValuePair_2U5BU5D_t3417125809*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1791240187(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2765956170 *, KeyValuePair_2_t523301392 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3020140543(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2388707000(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m830219065(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2210615422(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Count()
#define Dictionary_2_get_Count_m3778383679(__this, method) ((  int32_t (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Item(TKey)
#define Dictionary_2_get_Item_m1276551578(__this, ___key0, method) ((  ProjectionParams_t2712959773 * (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m61460347(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_set_Item_m782556999_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2327123539(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2765956170 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1837789730(__this, ___size0, method) ((  void (*) (Dictionary_2_t2765956170 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3944322068(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1256322282(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t523301392  (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m2505977612(__this /* static, unused */, ___key0, ___value1, method) ((  Canvas_t209405766 * (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3600969548(__this /* static, unused */, ___key0, ___value1, method) ((  ProjectionParams_t2712959773 * (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2225335095(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2765956170 *, KeyValuePair_2U5BU5D_t3417125809*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Resize()
#define Dictionary_2_Resize_m1948398009(__this, method) ((  void (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Add(TKey,TValue)
#define Dictionary_2_Add_m2125931262(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Clear()
#define Dictionary_2_Clear_m2558050482(__this, method) ((  void (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2502669394(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m3394341538(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2765956170 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1162882683(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2765956170 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m1973062987(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::Remove(TKey)
#define Dictionary_2_Remove_m364035238(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m880728349(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2765956170 *, Canvas_t209405766 *, ProjectionParams_t2712959773 **, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Keys()
#define Dictionary_2_get_Keys_m2000478202(__this, method) ((  KeyCollection_t954486645 * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::get_Values()
#define Dictionary_2_get_Values_m3463524186(__this, method) ((  ValueCollection_t1469016013 * (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3090569113(__this, ___key0, method) ((  Canvas_t209405766 * (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2186046969(__this, ___value0, method) ((  ProjectionParams_t2712959773 * (*) (Dictionary_2_t2765956170 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m2693585835(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2765956170 *, KeyValuePair_2_t523301392 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2491241768(__this, method) ((  Enumerator_t4085980872  (*) (Dictionary_2_t2765956170 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m103972593(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, ProjectionParams_t2712959773 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
