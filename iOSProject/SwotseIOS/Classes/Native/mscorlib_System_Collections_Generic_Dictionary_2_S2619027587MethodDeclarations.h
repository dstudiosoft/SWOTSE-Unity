﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>
struct ShimEnumerator_t2619027587;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3590362371_gshared (ShimEnumerator_t2619027587 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3590362371(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2619027587 *, Dictionary_2_t2513902766 *, const MethodInfo*))ShimEnumerator__ctor_m3590362371_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1814671474_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1814671474(__this, method) ((  bool (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_MoveNext_m1814671474_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2470154576_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2470154576(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_get_Entry_m2470154576_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3155610519_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3155610519(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_get_Key_m3155610519_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m497904711_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m497904711(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_get_Value_m497904711_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3862045777_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3862045777(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_get_Current_m3862045777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m246181529_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m246181529(__this, method) ((  void (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))ShimEnumerator_Reset_m246181529_gshared)(__this, method)
