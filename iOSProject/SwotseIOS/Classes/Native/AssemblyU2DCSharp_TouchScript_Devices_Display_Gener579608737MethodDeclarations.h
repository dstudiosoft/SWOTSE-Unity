﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Devices.Display.GenericDisplayDevice
struct GenericDisplayDevice_t579608737;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.Devices.Display.GenericDisplayDevice::.ctor()
extern "C"  void GenericDisplayDevice__ctor_m3880715949 (GenericDisplayDevice_t579608737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Devices.Display.GenericDisplayDevice::.cctor()
extern "C"  void GenericDisplayDevice__cctor_m502747488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Devices.Display.GenericDisplayDevice::get_INTERNAL_IsLaptop()
extern "C"  bool GenericDisplayDevice_get_INTERNAL_IsLaptop_m510324410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Devices.Display.GenericDisplayDevice::OnEnable()
extern "C"  void GenericDisplayDevice_OnEnable_m3991164901 (GenericDisplayDevice_t579608737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
