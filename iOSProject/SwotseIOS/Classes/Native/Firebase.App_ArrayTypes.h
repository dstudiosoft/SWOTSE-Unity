﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Firebase.Variant
struct Variant_t4275788079;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// Firebase.FutureVoid/Action
struct Action_t2847228577;
// Firebase.FutureString/Action
struct Action_t3062064994;

#include "mscorlib_System_Array3829468939.h"
#include "Firebase_App_Firebase_Variant4275788079.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FutureVoid_Action2847228577.h"
#include "Firebase_App_Firebase_FutureString_Action3062064994.h"

#pragma once
// Firebase.Variant[]
struct VariantU5BU5D_t268110134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Variant_t4275788079 * m_Items[1];

public:
	inline Variant_t4275788079 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Variant_t4275788079 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Variant_t4275788079 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Firebase.FirebaseApp[]
struct FirebaseAppU5BU5D_t194219771  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FirebaseApp_t210707726 * m_Items[1];

public:
	inline FirebaseApp_t210707726 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FirebaseApp_t210707726 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FirebaseApp_t210707726 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Firebase.FutureVoid/Action[]
struct ActionU5BU5D_t2406736316  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t2847228577 * m_Items[1];

public:
	inline Action_t2847228577 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t2847228577 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t2847228577 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Firebase.FutureString/Action[]
struct ActionU5BU5D_t4152980695  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3062064994 * m_Items[1];

public:
	inline Action_t3062064994 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3062064994 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3062064994 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
