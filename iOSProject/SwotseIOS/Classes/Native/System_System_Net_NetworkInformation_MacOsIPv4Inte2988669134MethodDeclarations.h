﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.MacOsIPv4InterfaceProperties
struct MacOsIPv4InterfaceProperties_t2988669134;
// System.Net.NetworkInformation.MacOsNetworkInterface
struct MacOsNetworkInterface_t1454185290;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1454185290.h"

// System.Void System.Net.NetworkInformation.MacOsIPv4InterfaceProperties::.ctor(System.Net.NetworkInformation.MacOsNetworkInterface)
extern "C"  void MacOsIPv4InterfaceProperties__ctor_m1569035652 (MacOsIPv4InterfaceProperties_t2988669134 * __this, MacOsNetworkInterface_t1454185290 * ___iface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.MacOsIPv4InterfaceProperties::get_IsForwardingEnabled()
extern "C"  bool MacOsIPv4InterfaceProperties_get_IsForwardingEnabled_m353913537 (MacOsIPv4InterfaceProperties_t2988669134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.MacOsIPv4InterfaceProperties::get_Mtu()
extern "C"  int32_t MacOsIPv4InterfaceProperties_get_Mtu_m2928093919 (MacOsIPv4InterfaceProperties_t2988669134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
