﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.CameraLayerBase
struct CameraLayerBase_t3857323967;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"

// System.Void TouchScript.Layers.CameraLayerBase::.ctor()
extern "C"  void CameraLayerBase__ctor_m96856466 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask TouchScript.Layers.CameraLayerBase::get_LayerMask()
extern "C"  LayerMask_t3188175821  CameraLayerBase_get_LayerMask_m826025531 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayerBase::set_LayerMask(UnityEngine.LayerMask)
extern "C"  void CameraLayerBase_set_LayerMask_m513125562 (CameraLayerBase_t3857323967 * __this, LayerMask_t3188175821  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.CameraLayerBase::get_WorldProjectionNormal()
extern "C"  Vector3_t2243707580  CameraLayerBase_get_WorldProjectionNormal_m3154762153 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.CameraLayerBase::Hit(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  int32_t CameraLayerBase_Hit_m3434505001 (CameraLayerBase_t3857323967 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayerBase::Awake()
extern "C"  void CameraLayerBase_Awake_m3764580647 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayerBase::setName()
extern "C"  void CameraLayerBase_setName_m2476604923 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.Layers.CameraLayerBase::createProjectionParams()
extern "C"  ProjectionParams_t2712959773 * CameraLayerBase_createProjectionParams_m328102931 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.CameraLayerBase::updateCamera()
extern "C"  void CameraLayerBase_updateCamera_m3542525990 (CameraLayerBase_t3857323967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
