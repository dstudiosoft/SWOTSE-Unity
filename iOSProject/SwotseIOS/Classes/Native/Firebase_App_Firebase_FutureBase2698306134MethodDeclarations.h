﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FutureBase
struct FutureBase_t2698306134;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_FutureBase2698306134.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_FutureStatus4011176069.h"

// System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FutureBase__ctor_m3054944159 (FutureBase_t2698306134 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureBase::.ctor()
extern "C"  void FutureBase__ctor_m390963742 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureBase::.ctor(Firebase.FutureBase)
extern "C"  void FutureBase__ctor_m1301203053 (FutureBase_t2698306134 * __this, FutureBase_t2698306134 * ___rhs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.FutureBase::getCPtr(Firebase.FutureBase)
extern "C"  HandleRef_t2419939847  FutureBase_getCPtr_m1560776750 (Il2CppObject * __this /* static, unused */, FutureBase_t2698306134 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureBase::Finalize()
extern "C"  void FutureBase_Finalize_m1464906966 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureBase::Dispose()
extern "C"  void FutureBase_Dispose_m1042966601 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureBase::Release()
extern "C"  void FutureBase_Release_m1175993543 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FutureStatus Firebase.FutureBase::status()
extern "C"  int32_t FutureBase_status_m542538063 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.FutureBase::error()
extern "C"  int32_t FutureBase_error_m3959169962 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.FutureBase::error_message()
extern "C"  String_t* FutureBase_error_message_m524560799 (FutureBase_t2698306134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
