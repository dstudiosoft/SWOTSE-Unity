﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserModel
struct UserModel_t3025569216;

#include "codegen/il2cpp-codegen.h"

// System.Void UserModel::.ctor()
extern "C"  void UserModel__ctor_m4229686825 (UserModel_t3025569216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
