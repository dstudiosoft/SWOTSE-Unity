﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;
// IReloadable
struct IReloadable_t861412162;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyItemContentManager
struct  BuyItemContentManager_t58540967  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text BuyItemContentManager::itemName
	Text_t356221433 * ___itemName_2;
	// UnityEngine.UI.Image BuyItemContentManager::itemImage
	Image_t2042527209 * ___itemImage_3;
	// UnityEngine.UI.Text BuyItemContentManager::itemsOwned
	Text_t356221433 * ___itemsOwned_4;
	// UnityEngine.UI.Text BuyItemContentManager::unitPrice
	Text_t356221433 * ___unitPrice_5;
	// UnityEngine.UI.InputField BuyItemContentManager::quantityInput
	InputField_t1631627530 * ___quantityInput_6;
	// UnityEngine.UI.Text BuyItemContentManager::totalShillings
	Text_t356221433 * ___totalShillings_7;
	// System.String BuyItemContentManager::itemId
	String_t* ___itemId_8;
	// System.Int64 BuyItemContentManager::unitCost
	int64_t ___unitCost_9;
	// System.Int64 BuyItemContentManager::itemcount
	int64_t ___itemcount_10;
	// System.Int64 BuyItemContentManager::toBuyCount
	int64_t ___toBuyCount_11;
	// IReloadable BuyItemContentManager::owner
	Il2CppObject * ___owner_12;
	// System.Boolean BuyItemContentManager::buy
	bool ___buy_13;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___itemName_2)); }
	inline Text_t356221433 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t356221433 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t356221433 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_2, value);
	}

	inline static int32_t get_offset_of_itemImage_3() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___itemImage_3)); }
	inline Image_t2042527209 * get_itemImage_3() const { return ___itemImage_3; }
	inline Image_t2042527209 ** get_address_of_itemImage_3() { return &___itemImage_3; }
	inline void set_itemImage_3(Image_t2042527209 * value)
	{
		___itemImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_3, value);
	}

	inline static int32_t get_offset_of_itemsOwned_4() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___itemsOwned_4)); }
	inline Text_t356221433 * get_itemsOwned_4() const { return ___itemsOwned_4; }
	inline Text_t356221433 ** get_address_of_itemsOwned_4() { return &___itemsOwned_4; }
	inline void set_itemsOwned_4(Text_t356221433 * value)
	{
		___itemsOwned_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemsOwned_4, value);
	}

	inline static int32_t get_offset_of_unitPrice_5() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___unitPrice_5)); }
	inline Text_t356221433 * get_unitPrice_5() const { return ___unitPrice_5; }
	inline Text_t356221433 ** get_address_of_unitPrice_5() { return &___unitPrice_5; }
	inline void set_unitPrice_5(Text_t356221433 * value)
	{
		___unitPrice_5 = value;
		Il2CppCodeGenWriteBarrier(&___unitPrice_5, value);
	}

	inline static int32_t get_offset_of_quantityInput_6() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___quantityInput_6)); }
	inline InputField_t1631627530 * get_quantityInput_6() const { return ___quantityInput_6; }
	inline InputField_t1631627530 ** get_address_of_quantityInput_6() { return &___quantityInput_6; }
	inline void set_quantityInput_6(InputField_t1631627530 * value)
	{
		___quantityInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___quantityInput_6, value);
	}

	inline static int32_t get_offset_of_totalShillings_7() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___totalShillings_7)); }
	inline Text_t356221433 * get_totalShillings_7() const { return ___totalShillings_7; }
	inline Text_t356221433 ** get_address_of_totalShillings_7() { return &___totalShillings_7; }
	inline void set_totalShillings_7(Text_t356221433 * value)
	{
		___totalShillings_7 = value;
		Il2CppCodeGenWriteBarrier(&___totalShillings_7, value);
	}

	inline static int32_t get_offset_of_itemId_8() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___itemId_8)); }
	inline String_t* get_itemId_8() const { return ___itemId_8; }
	inline String_t** get_address_of_itemId_8() { return &___itemId_8; }
	inline void set_itemId_8(String_t* value)
	{
		___itemId_8 = value;
		Il2CppCodeGenWriteBarrier(&___itemId_8, value);
	}

	inline static int32_t get_offset_of_unitCost_9() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___unitCost_9)); }
	inline int64_t get_unitCost_9() const { return ___unitCost_9; }
	inline int64_t* get_address_of_unitCost_9() { return &___unitCost_9; }
	inline void set_unitCost_9(int64_t value)
	{
		___unitCost_9 = value;
	}

	inline static int32_t get_offset_of_itemcount_10() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___itemcount_10)); }
	inline int64_t get_itemcount_10() const { return ___itemcount_10; }
	inline int64_t* get_address_of_itemcount_10() { return &___itemcount_10; }
	inline void set_itemcount_10(int64_t value)
	{
		___itemcount_10 = value;
	}

	inline static int32_t get_offset_of_toBuyCount_11() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___toBuyCount_11)); }
	inline int64_t get_toBuyCount_11() const { return ___toBuyCount_11; }
	inline int64_t* get_address_of_toBuyCount_11() { return &___toBuyCount_11; }
	inline void set_toBuyCount_11(int64_t value)
	{
		___toBuyCount_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___owner_12)); }
	inline Il2CppObject * get_owner_12() const { return ___owner_12; }
	inline Il2CppObject ** get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(Il2CppObject * value)
	{
		___owner_12 = value;
		Il2CppCodeGenWriteBarrier(&___owner_12, value);
	}

	inline static int32_t get_offset_of_buy_13() { return static_cast<int32_t>(offsetof(BuyItemContentManager_t58540967, ___buy_13)); }
	inline bool get_buy_13() const { return ___buy_13; }
	inline bool* get_address_of_buy_13() { return &___buy_13; }
	inline void set_buy_13(bool value)
	{
		___buy_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
