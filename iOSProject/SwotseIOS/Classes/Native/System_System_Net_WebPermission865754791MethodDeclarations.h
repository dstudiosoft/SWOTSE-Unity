﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermission
struct WebPermission_t865754791;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Net.WebPermissionInfo
struct WebPermissionInfo_t257172749;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Security.SecurityElement
struct SecurityElement_t2325568386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionSta3557289502.h"
#include "System_System_Net_NetworkAccess774279706.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Net_WebPermissionInfo257172749.h"
#include "System_System_Net_WebPermission865754791.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Security_SecurityElement2325568386.h"

// System.Void System.Net.WebPermission::.ctor()
extern "C"  void WebPermission__ctor_m2700391523 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C"  void WebPermission__ctor_m1972622522 (WebPermission_t865754791 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Net.NetworkAccess,System.String)
extern "C"  void WebPermission__ctor_m3890049757 (WebPermission_t865754791 * __this, int32_t ___access0, String_t* ___uriString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Net.NetworkAccess,System.Text.RegularExpressions.Regex)
extern "C"  void WebPermission__ctor_m897999211 (WebPermission_t865754791 * __this, int32_t ___access0, Regex_t1803876613 * ___uriRegex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.WebPermission::get_AcceptList()
extern "C"  Il2CppObject * WebPermission_get_AcceptList_m2940262372 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.WebPermission::get_ConnectList()
extern "C"  Il2CppObject * WebPermission_get_ConnectList_m2838692844 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.String)
extern "C"  void WebPermission_AddPermission_m2549423283 (WebPermission_t865754791 * __this, int32_t ___access0, String_t* ___uriString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.Text.RegularExpressions.Regex)
extern "C"  void WebPermission_AddPermission_m4110752713 (WebPermission_t865754791 * __this, int32_t ___access0, Regex_t1803876613 * ___uriRegex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.Net.WebPermissionInfo)
extern "C"  void WebPermission_AddPermission_m1586382036 (WebPermission_t865754791 * __this, int32_t ___access0, WebPermissionInfo_t257172749 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Copy()
extern "C"  Il2CppObject * WebPermission_Copy_m1459059492 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Intersect(System.Security.IPermission)
extern "C"  Il2CppObject * WebPermission_Intersect_m2663250965 (WebPermission_t865754791 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IntersectEmpty(System.Net.WebPermission)
extern "C"  bool WebPermission_IntersectEmpty_m498930964 (WebPermission_t865754791 * __this, WebPermission_t865754791 * ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::Intersect(System.Collections.ArrayList,System.Collections.ArrayList,System.Collections.ArrayList)
extern "C"  void WebPermission_Intersect_m4089143025 (WebPermission_t865754791 * __this, ArrayList_t4252133567 * ___list10, ArrayList_t4252133567 * ___list21, ArrayList_t4252133567 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsSubsetOf(System.Security.IPermission)
extern "C"  bool WebPermission_IsSubsetOf_m984468403 (WebPermission_t865754791 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsSubsetOf(System.Collections.ArrayList,System.Collections.ArrayList)
extern "C"  bool WebPermission_IsSubsetOf_m381877004 (WebPermission_t865754791 * __this, ArrayList_t4252133567 * ___list10, ArrayList_t4252133567 * ___list21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsUnrestricted()
extern "C"  bool WebPermission_IsUnrestricted_m3127452455 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Net.WebPermission::ToXml()
extern "C"  SecurityElement_t2325568386 * WebPermission_ToXml_m2585535305 (WebPermission_t865754791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::ToXml(System.Security.SecurityElement,System.String,System.Collections.IEnumerator)
extern "C"  void WebPermission_ToXml_m2953737581 (WebPermission_t865754791 * __this, SecurityElement_t2325568386 * ___root0, String_t* ___childName1, Il2CppObject * ___enumerator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::FromXml(System.Security.SecurityElement)
extern "C"  void WebPermission_FromXml_m1746963763 (WebPermission_t865754791 * __this, SecurityElement_t2325568386 * ___securityElement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::FromXml(System.Collections.ArrayList,System.Net.NetworkAccess)
extern "C"  void WebPermission_FromXml_m2737545587 (WebPermission_t865754791 * __this, ArrayList_t4252133567 * ___endpoints0, int32_t ___access1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Union(System.Security.IPermission)
extern "C"  Il2CppObject * WebPermission_Union_m51588967 (WebPermission_t865754791 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
