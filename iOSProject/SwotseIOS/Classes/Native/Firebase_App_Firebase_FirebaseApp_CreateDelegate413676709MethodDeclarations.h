﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/CreateDelegate
struct CreateDelegate_t413676709;
// System.Object
struct Il2CppObject;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateDelegate__ctor_m3440641418 (CreateDelegate_t413676709 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern "C"  FirebaseApp_t210707726 * CreateDelegate_Invoke_m3558955498 (CreateDelegate_t413676709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Firebase.FirebaseApp/CreateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CreateDelegate_BeginInvoke_m3861139009 (CreateDelegate_t413676709 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  FirebaseApp_t210707726 * CreateDelegate_EndInvoke_m3008445990 (CreateDelegate_t413676709 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
