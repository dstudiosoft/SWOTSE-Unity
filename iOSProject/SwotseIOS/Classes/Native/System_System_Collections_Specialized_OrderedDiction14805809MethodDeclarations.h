﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.OrderedDictionary
struct OrderedDictionary_t14805809;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Array
struct Il2CppArray;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Specialized.OrderedDictionary::.ctor()
extern "C"  void OrderedDictionary__ctor_m4225720632 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void OrderedDictionary__ctor_m938194519 (OrderedDictionary_t14805809 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C"  void OrderedDictionary_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1457654302 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.OrderedDictionary::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedDictionary_System_Collections_IEnumerable_GetEnumerator_m3079519561 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool OrderedDictionary_System_Collections_ICollection_get_IsSynchronized_m2685867516 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * OrderedDictionary_System_Collections_ICollection_get_SyncRoot_m767408392 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool OrderedDictionary_System_Collections_IDictionary_get_IsFixedSize_m2085161075 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::OnDeserialization(System.Object)
extern "C"  void OrderedDictionary_OnDeserialization_m2956246762 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void OrderedDictionary_GetObjectData_m2076924836 (OrderedDictionary_t14805809 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.OrderedDictionary::get_Count()
extern "C"  int32_t OrderedDictionary_get_Count_m701047796 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::CopyTo(System.Array,System.Int32)
extern "C"  void OrderedDictionary_CopyTo_m3301097501 (OrderedDictionary_t14805809 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary::get_IsReadOnly()
extern "C"  bool OrderedDictionary_get_IsReadOnly_m2908153761 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary::get_Item(System.Object)
extern "C"  Il2CppObject * OrderedDictionary_get_Item_m444540411 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::set_Item(System.Object,System.Object)
extern "C"  void OrderedDictionary_set_Item_m3840150098 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.Specialized.OrderedDictionary::get_Keys()
extern "C"  Il2CppObject * OrderedDictionary_get_Keys_m3345260237 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.Specialized.OrderedDictionary::get_Values()
extern "C"  Il2CppObject * OrderedDictionary_get_Values_m2700027973 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::Add(System.Object,System.Object)
extern "C"  void OrderedDictionary_Add_m2136325481 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::Clear()
extern "C"  void OrderedDictionary_Clear_m2484510301 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary::Contains(System.Object)
extern "C"  bool OrderedDictionary_Contains_m1740314097 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Collections.Specialized.OrderedDictionary::GetEnumerator()
extern "C"  Il2CppObject * OrderedDictionary_GetEnumerator_m2874361000 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::Remove(System.Object)
extern "C"  void OrderedDictionary_Remove_m1906215950 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.OrderedDictionary::FindListEntry(System.Object)
extern "C"  int32_t OrderedDictionary_FindListEntry_m1883692983 (OrderedDictionary_t14805809 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary::WriteCheck()
extern "C"  void OrderedDictionary_WriteCheck_m2618528947 (OrderedDictionary_t14805809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
