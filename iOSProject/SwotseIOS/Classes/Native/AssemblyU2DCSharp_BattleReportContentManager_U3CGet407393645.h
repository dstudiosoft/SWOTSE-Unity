﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// BattleReportContentManager
struct BattleReportContentManager_t4271590472;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportContentManager/<GetBattleReport>c__Iterator7
struct  U3CGetBattleReportU3Ec__Iterator7_t407393645  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BattleReportContentManager/<GetBattleReport>c__Iterator7::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// JSONObject BattleReportContentManager/<GetBattleReport>c__Iterator7::<temp>__1
	JSONObject_t1971882247 * ___U3CtempU3E__1_1;
	// System.Int32 BattleReportContentManager/<GetBattleReport>c__Iterator7::$PC
	int32_t ___U24PC_2;
	// System.Object BattleReportContentManager/<GetBattleReport>c__Iterator7::$current
	Il2CppObject * ___U24current_3;
	// BattleReportContentManager BattleReportContentManager/<GetBattleReport>c__Iterator7::<>f__this
	BattleReportContentManager_t4271590472 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator7_t407393645, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtempU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator7_t407393645, ___U3CtempU3E__1_1)); }
	inline JSONObject_t1971882247 * get_U3CtempU3E__1_1() const { return ___U3CtempU3E__1_1; }
	inline JSONObject_t1971882247 ** get_address_of_U3CtempU3E__1_1() { return &___U3CtempU3E__1_1; }
	inline void set_U3CtempU3E__1_1(JSONObject_t1971882247 * value)
	{
		___U3CtempU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator7_t407393645, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator7_t407393645, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetBattleReportU3Ec__Iterator7_t407393645, ___U3CU3Ef__this_4)); }
	inline BattleReportContentManager_t4271590472 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline BattleReportContentManager_t4271590472 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(BattleReportContentManager_t4271590472 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
