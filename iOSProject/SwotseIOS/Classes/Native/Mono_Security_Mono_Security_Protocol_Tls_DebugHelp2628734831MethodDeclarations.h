﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.DebugHelper
struct DebugHelper_t2628734831;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Mono.Security.Protocol.Tls.DebugHelper::.ctor()
extern "C"  void DebugHelper__ctor_m1480968687 (DebugHelper_t2628734831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::Initialize()
extern "C"  void DebugHelper_Initialize_m1914117403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String,System.Object[])
extern "C"  void DebugHelper_WriteLine_m2563530486 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String)
extern "C"  void DebugHelper_WriteLine_m1240198088 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String,System.Byte[])
extern "C"  void DebugHelper_WriteLine_m1994039977 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ByteU5BU5D_t3397334013* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteBuffer(System.Byte[])
extern "C"  void DebugHelper_WriteBuffer_m2257888565 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  void DebugHelper_WriteBuffer_m1871987209 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___index1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
