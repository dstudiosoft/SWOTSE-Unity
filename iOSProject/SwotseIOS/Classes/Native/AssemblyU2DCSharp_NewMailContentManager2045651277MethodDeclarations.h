﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewMailContentManager
struct NewMailContentManager_t2045651277;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NewMailContentManager::.ctor()
extern "C"  void NewMailContentManager__ctor_m932212680 (NewMailContentManager_t2045651277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailContentManager::SendUserMessage(System.Int64)
extern "C"  void NewMailContentManager_SendUserMessage_m1987240920 (NewMailContentManager_t2045651277 * __this, int64_t ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewMailContentManager::UserMessageSent(System.Object,System.String)
extern "C"  void NewMailContentManager_UserMessageSent_m434605700 (NewMailContentManager_t2045651277 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
