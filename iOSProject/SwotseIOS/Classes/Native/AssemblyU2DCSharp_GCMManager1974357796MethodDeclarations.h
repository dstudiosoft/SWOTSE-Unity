﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GCMManager
struct GCMManager_t1974357796;
// System.Object
struct Il2CppObject;
// Firebase.Messaging.TokenReceivedEventArgs
struct TokenReceivedEventArgs_t1675589073;
// Firebase.Messaging.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t2752427815;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Messaging_Firebase_Messaging_TokenReceive1675589073.h"
#include "Firebase_Messaging_Firebase_Messaging_MessageRecei2752427815.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GCMManager::.ctor()
extern "C"  void GCMManager__ctor_m3957555581 (GCMManager_t1974357796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GCMManager::Start()
extern "C"  void GCMManager_Start_m857854661 (GCMManager_t1974357796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GCMManager::OnTokenRecieved(System.Object,Firebase.Messaging.TokenReceivedEventArgs)
extern "C"  void GCMManager_OnTokenRecieved_m788399074 (GCMManager_t1974357796 * __this, Il2CppObject * ___sender0, TokenReceivedEventArgs_t1675589073 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GCMManager::OnMessageRecieved(System.Object,Firebase.Messaging.MessageReceivedEventArgs)
extern "C"  void GCMManager_OnMessageRecieved_m728344450 (GCMManager_t1974357796 * __this, Il2CppObject * ___sender0, MessageReceivedEventArgs_t2752427815 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GCMManager::RegisterDeviceToken(System.String)
extern "C"  Il2CppObject * GCMManager_RegisterDeviceToken_m3348995603 (GCMManager_t1974357796 * __this, String_t* ___tokenString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
