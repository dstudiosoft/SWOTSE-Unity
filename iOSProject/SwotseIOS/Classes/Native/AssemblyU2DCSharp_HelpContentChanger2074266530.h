﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpContentChanger
struct  HelpContentChanger_t2074266530  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HelpContentChanger::homeContent
	GameObject_t1756533147 * ___homeContent_2;
	// UnityEngine.GameObject HelpContentChanger::unitsContent
	GameObject_t1756533147 * ___unitsContent_3;
	// UnityEngine.GameObject HelpContentChanger::hierarchyContent
	GameObject_t1756533147 * ___hierarchyContent_4;
	// UnityEngine.GameObject HelpContentChanger::promotionContent
	GameObject_t1756533147 * ___promotionContent_5;
	// UnityEngine.GameObject HelpContentChanger::engSteps
	GameObject_t1756533147 * ___engSteps_6;
	// UnityEngine.GameObject HelpContentChanger::arSteps
	GameObject_t1756533147 * ___arSteps_7;
	// UnityEngine.UI.Image HelpContentChanger::homeButtonImage
	Image_t2042527209 * ___homeButtonImage_8;
	// UnityEngine.UI.Image HelpContentChanger::unitsButtonImage
	Image_t2042527209 * ___unitsButtonImage_9;
	// UnityEngine.UI.Image HelpContentChanger::hierarchyButtonImage
	Image_t2042527209 * ___hierarchyButtonImage_10;
	// UnityEngine.UI.Image HelpContentChanger::promotionButtonImage
	Image_t2042527209 * ___promotionButtonImage_11;
	// UnityEngine.Sprite HelpContentChanger::tabSelected
	Sprite_t309593783 * ___tabSelected_12;
	// UnityEngine.Sprite HelpContentChanger::tabUnselected
	Sprite_t309593783 * ___tabUnselected_13;

public:
	inline static int32_t get_offset_of_homeContent_2() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___homeContent_2)); }
	inline GameObject_t1756533147 * get_homeContent_2() const { return ___homeContent_2; }
	inline GameObject_t1756533147 ** get_address_of_homeContent_2() { return &___homeContent_2; }
	inline void set_homeContent_2(GameObject_t1756533147 * value)
	{
		___homeContent_2 = value;
		Il2CppCodeGenWriteBarrier(&___homeContent_2, value);
	}

	inline static int32_t get_offset_of_unitsContent_3() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___unitsContent_3)); }
	inline GameObject_t1756533147 * get_unitsContent_3() const { return ___unitsContent_3; }
	inline GameObject_t1756533147 ** get_address_of_unitsContent_3() { return &___unitsContent_3; }
	inline void set_unitsContent_3(GameObject_t1756533147 * value)
	{
		___unitsContent_3 = value;
		Il2CppCodeGenWriteBarrier(&___unitsContent_3, value);
	}

	inline static int32_t get_offset_of_hierarchyContent_4() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___hierarchyContent_4)); }
	inline GameObject_t1756533147 * get_hierarchyContent_4() const { return ___hierarchyContent_4; }
	inline GameObject_t1756533147 ** get_address_of_hierarchyContent_4() { return &___hierarchyContent_4; }
	inline void set_hierarchyContent_4(GameObject_t1756533147 * value)
	{
		___hierarchyContent_4 = value;
		Il2CppCodeGenWriteBarrier(&___hierarchyContent_4, value);
	}

	inline static int32_t get_offset_of_promotionContent_5() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___promotionContent_5)); }
	inline GameObject_t1756533147 * get_promotionContent_5() const { return ___promotionContent_5; }
	inline GameObject_t1756533147 ** get_address_of_promotionContent_5() { return &___promotionContent_5; }
	inline void set_promotionContent_5(GameObject_t1756533147 * value)
	{
		___promotionContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___promotionContent_5, value);
	}

	inline static int32_t get_offset_of_engSteps_6() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___engSteps_6)); }
	inline GameObject_t1756533147 * get_engSteps_6() const { return ___engSteps_6; }
	inline GameObject_t1756533147 ** get_address_of_engSteps_6() { return &___engSteps_6; }
	inline void set_engSteps_6(GameObject_t1756533147 * value)
	{
		___engSteps_6 = value;
		Il2CppCodeGenWriteBarrier(&___engSteps_6, value);
	}

	inline static int32_t get_offset_of_arSteps_7() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___arSteps_7)); }
	inline GameObject_t1756533147 * get_arSteps_7() const { return ___arSteps_7; }
	inline GameObject_t1756533147 ** get_address_of_arSteps_7() { return &___arSteps_7; }
	inline void set_arSteps_7(GameObject_t1756533147 * value)
	{
		___arSteps_7 = value;
		Il2CppCodeGenWriteBarrier(&___arSteps_7, value);
	}

	inline static int32_t get_offset_of_homeButtonImage_8() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___homeButtonImage_8)); }
	inline Image_t2042527209 * get_homeButtonImage_8() const { return ___homeButtonImage_8; }
	inline Image_t2042527209 ** get_address_of_homeButtonImage_8() { return &___homeButtonImage_8; }
	inline void set_homeButtonImage_8(Image_t2042527209 * value)
	{
		___homeButtonImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___homeButtonImage_8, value);
	}

	inline static int32_t get_offset_of_unitsButtonImage_9() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___unitsButtonImage_9)); }
	inline Image_t2042527209 * get_unitsButtonImage_9() const { return ___unitsButtonImage_9; }
	inline Image_t2042527209 ** get_address_of_unitsButtonImage_9() { return &___unitsButtonImage_9; }
	inline void set_unitsButtonImage_9(Image_t2042527209 * value)
	{
		___unitsButtonImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___unitsButtonImage_9, value);
	}

	inline static int32_t get_offset_of_hierarchyButtonImage_10() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___hierarchyButtonImage_10)); }
	inline Image_t2042527209 * get_hierarchyButtonImage_10() const { return ___hierarchyButtonImage_10; }
	inline Image_t2042527209 ** get_address_of_hierarchyButtonImage_10() { return &___hierarchyButtonImage_10; }
	inline void set_hierarchyButtonImage_10(Image_t2042527209 * value)
	{
		___hierarchyButtonImage_10 = value;
		Il2CppCodeGenWriteBarrier(&___hierarchyButtonImage_10, value);
	}

	inline static int32_t get_offset_of_promotionButtonImage_11() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___promotionButtonImage_11)); }
	inline Image_t2042527209 * get_promotionButtonImage_11() const { return ___promotionButtonImage_11; }
	inline Image_t2042527209 ** get_address_of_promotionButtonImage_11() { return &___promotionButtonImage_11; }
	inline void set_promotionButtonImage_11(Image_t2042527209 * value)
	{
		___promotionButtonImage_11 = value;
		Il2CppCodeGenWriteBarrier(&___promotionButtonImage_11, value);
	}

	inline static int32_t get_offset_of_tabSelected_12() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___tabSelected_12)); }
	inline Sprite_t309593783 * get_tabSelected_12() const { return ___tabSelected_12; }
	inline Sprite_t309593783 ** get_address_of_tabSelected_12() { return &___tabSelected_12; }
	inline void set_tabSelected_12(Sprite_t309593783 * value)
	{
		___tabSelected_12 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelected_12, value);
	}

	inline static int32_t get_offset_of_tabUnselected_13() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530, ___tabUnselected_13)); }
	inline Sprite_t309593783 * get_tabUnselected_13() const { return ___tabUnselected_13; }
	inline Sprite_t309593783 ** get_address_of_tabUnselected_13() { return &___tabUnselected_13; }
	inline void set_tabUnselected_13(Sprite_t309593783 * value)
	{
		___tabUnselected_13 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselected_13, value);
	}
};

struct HelpContentChanger_t2074266530_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HelpContentChanger::<>f__switch$map9
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map9_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_14() { return static_cast<int32_t>(offsetof(HelpContentChanger_t2074266530_StaticFields, ___U3CU3Ef__switchU24map9_14)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map9_14() const { return ___U3CU3Ef__switchU24map9_14; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map9_14() { return &___U3CU3Ef__switchU24map9_14; }
	inline void set_U3CU3Ef__switchU24map9_14(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map9_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
