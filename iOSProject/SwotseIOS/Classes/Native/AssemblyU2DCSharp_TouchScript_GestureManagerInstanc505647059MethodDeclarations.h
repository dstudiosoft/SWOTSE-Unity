﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t505647059;
// TouchScript.IGestureManager
struct IGestureManager_t4266705231;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t4252506175;
// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// System.Action`1<UnityEngine.Transform>
struct Action_1_t3076917440;
// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>>
struct Action_2_t2136039064;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1721427117;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;
// TouchScript.TouchEventArgs
struct TouchEventArgs_t1917927166;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t328750215;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Ges2128095272.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture2352305985.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "AssemblyU2DCSharp_TouchScript_TouchEventArgs1917927166.h"

// System.Void TouchScript.GestureManagerInstance::.ctor()
extern "C"  void GestureManagerInstance__ctor_m1972349126 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::.cctor()
extern "C"  void GestureManagerInstance__cctor_m1994856937 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.IGestureManager TouchScript.GestureManagerInstance::get_Instance()
extern "C"  Il2CppObject * GestureManagerInstance_get_Instance_m3753593026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.IGestureDelegate TouchScript.GestureManagerInstance::get_GlobalGestureDelegate()
extern "C"  Il2CppObject * GestureManagerInstance_get_GlobalGestureDelegate_m3640609482 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::set_GlobalGestureDelegate(TouchScript.IGestureDelegate)
extern "C"  void GestureManagerInstance_set_GlobalGestureDelegate_m1129101053 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::Awake()
extern "C"  void GestureManagerInstance_Awake_m3857603035 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::OnEnable()
extern "C"  void GestureManagerInstance_OnEnable_m1312310010 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::OnDisable()
extern "C"  void GestureManagerInstance_OnDisable_m2483318527 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::OnApplicationQuit()
extern "C"  void GestureManagerInstance_OnApplicationQuit_m686051728 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/GestureState TouchScript.GestureManagerInstance::INTERNAL_GestureChangeState(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture/GestureState)
extern "C"  int32_t GestureManagerInstance_INTERNAL_GestureChangeState_m1996248918 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::doUpdateBegan(TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void GestureManagerInstance_doUpdateBegan_m1553516450 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, Il2CppObject* ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::doUpdateMoved(TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void GestureManagerInstance_doUpdateMoved_m3196152968 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, Il2CppObject* ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::doUpdateEnded(TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void GestureManagerInstance_doUpdateEnded_m3198214937 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, Il2CppObject* ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::doUpdateCancelled(TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void GestureManagerInstance_doUpdateCancelled_m3517215732 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, Il2CppObject* ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::update(System.Collections.Generic.IList`1<TouchScript.TouchPoint>,System.Action`1<UnityEngine.Transform>,System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>>)
extern "C"  void GestureManagerInstance_update_m1974054294 (GestureManagerInstance_t505647059 * __this, Il2CppObject* ___touches0, Action_1_t3076917440 * ___process1, Action_2_t2136039064 * ___dispatch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::processTarget(UnityEngine.Transform)
extern "C"  void GestureManagerInstance_processTarget_m1528850531 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::processTargetBegan(UnityEngine.Transform)
extern "C"  void GestureManagerInstance_processTargetBegan_m597500926 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::resetGestures()
extern "C"  void GestureManagerInstance_resetGestures_m3046176739 (GestureManagerInstance_t505647059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::getHierarchyEndingWith(UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>)
extern "C"  void GestureManagerInstance_getHierarchyEndingWith_m2107594614 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, List_1_t1721427117 * ___outputList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::getHierarchyBeginningWith(UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Boolean)
extern "C"  void GestureManagerInstance_getHierarchyBeginningWith_m3287668355 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, List_1_t1721427117 * ___outputList1, bool ___includeSelf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::getHierarchyContaining(UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>)
extern "C"  void GestureManagerInstance_getHierarchyContaining_m3577968703 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, List_1_t1721427117 * ___outputList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::getEnabledGesturesOnTarget(UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>)
extern "C"  void GestureManagerInstance_getEnabledGesturesOnTarget_m1385324393 (GestureManagerInstance_t505647059 * __this, Transform_t3275118058 * ___target0, List_1_t1721427117 * ___outputList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.GestureManagerInstance::gestureIsActive(TouchScript.Gestures.Gesture)
extern "C"  bool GestureManagerInstance_gestureIsActive_m1881449192 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.GestureManagerInstance::recognizeGestureIfNotPrevented(TouchScript.Gestures.Gesture)
extern "C"  bool GestureManagerInstance_recognizeGestureIfNotPrevented_m725481717 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::failGesture(TouchScript.Gestures.Gesture)
extern "C"  void GestureManagerInstance_failGesture_m4036360212 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.GestureManagerInstance::shouldReceiveTouch(TouchScript.Gestures.Gesture,TouchScript.TouchPoint)
extern "C"  bool GestureManagerInstance_shouldReceiveTouch_m4004047953 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, TouchPoint_t959629083 * ___touch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.GestureManagerInstance::shouldBegin(TouchScript.Gestures.Gesture)
extern "C"  bool GestureManagerInstance_shouldBegin_m895667561 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.GestureManagerInstance::canPreventGesture(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture)
extern "C"  bool GestureManagerInstance_canPreventGesture_m2483383411 (GestureManagerInstance_t505647059 * __this, Gesture_t2352305985 * ___first0, Gesture_t2352305985 * ___second1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::frameFinishedHandler(System.Object,System.EventArgs)
extern "C"  void GestureManagerInstance_frameFinishedHandler_m1743100011 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___eventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::frameStartedHandler(System.Object,System.EventArgs)
extern "C"  void GestureManagerInstance_frameStartedHandler_m209612860 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___eventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::touchesBeganHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void GestureManagerInstance_touchesBeganHandler_m1663832822 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::touchesMovedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void GestureManagerInstance_touchesMovedHandler_m1819134652 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::touchesEndedHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void GestureManagerInstance_touchesEndedHandler_m2045481343 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::touchesCancelledHandler(System.Object,TouchScript.TouchEventArgs)
extern "C"  void GestureManagerInstance_touchesCancelledHandler_m4199637464 (GestureManagerInstance_t505647059 * __this, Il2CppObject * ___sender0, TouchEventArgs_t1917927166 * ___touchEventArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::<gestureListPool>m__18()
extern "C"  List_1_t1721427117 * GestureManagerInstance_U3CgestureListPoolU3Em__18_m192252009 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::<gestureListPool>m__19(System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>)
extern "C"  void GestureManagerInstance_U3CgestureListPoolU3Em__19_m3821321747 (Il2CppObject * __this /* static, unused */, List_1_t1721427117 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.GestureManagerInstance::<touchListPool>m__1A()
extern "C"  List_1_t328750215 * GestureManagerInstance_U3CtouchListPoolU3Em__1A_m3135792488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::<touchListPool>m__1B(System.Collections.Generic.List`1<TouchScript.TouchPoint>)
extern "C"  void GestureManagerInstance_U3CtouchListPoolU3Em__1B_m1886822116 (Il2CppObject * __this /* static, unused */, List_1_t328750215 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::<transformListPool>m__1C()
extern "C"  List_1_t2644239190 * GestureManagerInstance_U3CtransformListPoolU3Em__1C_m3993899075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.GestureManagerInstance::<transformListPool>m__1D(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern "C"  void GestureManagerInstance_U3CtransformListPoolU3Em__1D_m3676351923 (Il2CppObject * __this /* static, unused */, List_1_t2644239190 * ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
