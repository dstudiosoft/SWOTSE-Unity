﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingTimer
struct BuildingTimer_t787360079;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildingTimer::.ctor()
extern "C"  void BuildingTimer__ctor_m468304548 (BuildingTimer_t787360079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
