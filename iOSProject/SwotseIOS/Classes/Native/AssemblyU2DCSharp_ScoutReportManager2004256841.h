﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// BattleReportModel
struct BattleReportModel_t2786384729;
// BattleReportUnitTableController
struct BattleReportUnitTableController_t2722587206;
// BattleReportResourcesTableController
struct BattleReportResourcesTableController_t4005631621;
// BattleReportItemsTableController
struct BattleReportItemsTableController_t2343543308;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutReportManager
struct  ScoutReportManager_t2004256841  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ScoutReportManager::attackerName
	Text_t356221433 * ___attackerName_2;
	// UnityEngine.UI.Image ScoutReportManager::attackerImage
	Image_t2042527209 * ___attackerImage_3;
	// UnityEngine.UI.Text ScoutReportManager::attackerRank
	Text_t356221433 * ___attackerRank_4;
	// UnityEngine.UI.Text ScoutReportManager::attackerAllianceName
	Text_t356221433 * ___attackerAllianceName_5;
	// UnityEngine.UI.Text ScoutReportManager::attackerAlliancePosition
	Text_t356221433 * ___attackerAlliancePosition_6;
	// UnityEngine.UI.Text ScoutReportManager::attackerCityName
	Text_t356221433 * ___attackerCityName_7;
	// UnityEngine.UI.Image ScoutReportManager::attackerCityImage
	Image_t2042527209 * ___attackerCityImage_8;
	// UnityEngine.UI.Text ScoutReportManager::attackerExperiance
	Text_t356221433 * ___attackerExperiance_9;
	// UnityEngine.UI.Text ScoutReportManager::attackerCityRegion
	Text_t356221433 * ___attackerCityRegion_10;
	// UnityEngine.UI.Text ScoutReportManager::defenderName
	Text_t356221433 * ___defenderName_11;
	// UnityEngine.UI.Image ScoutReportManager::defenderImage
	Image_t2042527209 * ___defenderImage_12;
	// UnityEngine.UI.Text ScoutReportManager::defenderRank
	Text_t356221433 * ___defenderRank_13;
	// UnityEngine.UI.Text ScoutReportManager::defenderAllianceName
	Text_t356221433 * ___defenderAllianceName_14;
	// UnityEngine.UI.Text ScoutReportManager::defenderAlliancePosition
	Text_t356221433 * ___defenderAlliancePosition_15;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityName
	Text_t356221433 * ___defenderCityName_16;
	// UnityEngine.UI.Image ScoutReportManager::defenderCityImage
	Image_t2042527209 * ___defenderCityImage_17;
	// UnityEngine.UI.Text ScoutReportManager::defenderExperiance
	Text_t356221433 * ___defenderExperiance_18;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityRegion
	Text_t356221433 * ___defenderCityRegion_19;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityHappiness
	Text_t356221433 * ___defenderCityHappiness_20;
	// UnityEngine.UI.Text ScoutReportManager::defenderCityCourage
	Text_t356221433 * ___defenderCityCourage_21;
	// UnityEngine.UI.Text ScoutReportManager::attackerResult
	Text_t356221433 * ___attackerResult_22;
	// UnityEngine.UI.Text ScoutReportManager::defenderResult
	Text_t356221433 * ___defenderResult_23;
	// BattleReportModel ScoutReportManager::report
	BattleReportModel_t2786384729 * ___report_24;
	// BattleReportUnitTableController ScoutReportManager::attackerUnitsController
	BattleReportUnitTableController_t2722587206 * ___attackerUnitsController_25;
	// BattleReportUnitTableController ScoutReportManager::defenderUnitsController
	BattleReportUnitTableController_t2722587206 * ___defenderUnitsController_26;
	// BattleReportResourcesTableController ScoutReportManager::defenderResourcesController
	BattleReportResourcesTableController_t4005631621 * ___defenderResourcesController_27;
	// BattleReportItemsTableController ScoutReportManager::defendersItemsController
	BattleReportItemsTableController_t2343543308 * ___defendersItemsController_28;
	// System.Collections.Generic.List`1<System.String> ScoutReportManager::attackerUnitNames
	List_1_t1398341365 * ___attackerUnitNames_29;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::attackerBeforeCount
	List_1_t278199169 * ___attackerBeforeCount_30;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::attackerAfterCount
	List_1_t278199169 * ___attackerAfterCount_31;
	// System.Collections.Generic.List`1<System.String> ScoutReportManager::defenderUnitNames
	List_1_t1398341365 * ___defenderUnitNames_32;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::defenderBeforeCount
	List_1_t278199169 * ___defenderBeforeCount_33;
	// System.Collections.Generic.List`1<System.Int64> ScoutReportManager::defenderAfterCount
	List_1_t278199169 * ___defenderAfterCount_34;

public:
	inline static int32_t get_offset_of_attackerName_2() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerName_2)); }
	inline Text_t356221433 * get_attackerName_2() const { return ___attackerName_2; }
	inline Text_t356221433 ** get_address_of_attackerName_2() { return &___attackerName_2; }
	inline void set_attackerName_2(Text_t356221433 * value)
	{
		___attackerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___attackerName_2, value);
	}

	inline static int32_t get_offset_of_attackerImage_3() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerImage_3)); }
	inline Image_t2042527209 * get_attackerImage_3() const { return ___attackerImage_3; }
	inline Image_t2042527209 ** get_address_of_attackerImage_3() { return &___attackerImage_3; }
	inline void set_attackerImage_3(Image_t2042527209 * value)
	{
		___attackerImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___attackerImage_3, value);
	}

	inline static int32_t get_offset_of_attackerRank_4() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerRank_4)); }
	inline Text_t356221433 * get_attackerRank_4() const { return ___attackerRank_4; }
	inline Text_t356221433 ** get_address_of_attackerRank_4() { return &___attackerRank_4; }
	inline void set_attackerRank_4(Text_t356221433 * value)
	{
		___attackerRank_4 = value;
		Il2CppCodeGenWriteBarrier(&___attackerRank_4, value);
	}

	inline static int32_t get_offset_of_attackerAllianceName_5() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerAllianceName_5)); }
	inline Text_t356221433 * get_attackerAllianceName_5() const { return ___attackerAllianceName_5; }
	inline Text_t356221433 ** get_address_of_attackerAllianceName_5() { return &___attackerAllianceName_5; }
	inline void set_attackerAllianceName_5(Text_t356221433 * value)
	{
		___attackerAllianceName_5 = value;
		Il2CppCodeGenWriteBarrier(&___attackerAllianceName_5, value);
	}

	inline static int32_t get_offset_of_attackerAlliancePosition_6() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerAlliancePosition_6)); }
	inline Text_t356221433 * get_attackerAlliancePosition_6() const { return ___attackerAlliancePosition_6; }
	inline Text_t356221433 ** get_address_of_attackerAlliancePosition_6() { return &___attackerAlliancePosition_6; }
	inline void set_attackerAlliancePosition_6(Text_t356221433 * value)
	{
		___attackerAlliancePosition_6 = value;
		Il2CppCodeGenWriteBarrier(&___attackerAlliancePosition_6, value);
	}

	inline static int32_t get_offset_of_attackerCityName_7() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerCityName_7)); }
	inline Text_t356221433 * get_attackerCityName_7() const { return ___attackerCityName_7; }
	inline Text_t356221433 ** get_address_of_attackerCityName_7() { return &___attackerCityName_7; }
	inline void set_attackerCityName_7(Text_t356221433 * value)
	{
		___attackerCityName_7 = value;
		Il2CppCodeGenWriteBarrier(&___attackerCityName_7, value);
	}

	inline static int32_t get_offset_of_attackerCityImage_8() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerCityImage_8)); }
	inline Image_t2042527209 * get_attackerCityImage_8() const { return ___attackerCityImage_8; }
	inline Image_t2042527209 ** get_address_of_attackerCityImage_8() { return &___attackerCityImage_8; }
	inline void set_attackerCityImage_8(Image_t2042527209 * value)
	{
		___attackerCityImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___attackerCityImage_8, value);
	}

	inline static int32_t get_offset_of_attackerExperiance_9() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerExperiance_9)); }
	inline Text_t356221433 * get_attackerExperiance_9() const { return ___attackerExperiance_9; }
	inline Text_t356221433 ** get_address_of_attackerExperiance_9() { return &___attackerExperiance_9; }
	inline void set_attackerExperiance_9(Text_t356221433 * value)
	{
		___attackerExperiance_9 = value;
		Il2CppCodeGenWriteBarrier(&___attackerExperiance_9, value);
	}

	inline static int32_t get_offset_of_attackerCityRegion_10() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerCityRegion_10)); }
	inline Text_t356221433 * get_attackerCityRegion_10() const { return ___attackerCityRegion_10; }
	inline Text_t356221433 ** get_address_of_attackerCityRegion_10() { return &___attackerCityRegion_10; }
	inline void set_attackerCityRegion_10(Text_t356221433 * value)
	{
		___attackerCityRegion_10 = value;
		Il2CppCodeGenWriteBarrier(&___attackerCityRegion_10, value);
	}

	inline static int32_t get_offset_of_defenderName_11() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderName_11)); }
	inline Text_t356221433 * get_defenderName_11() const { return ___defenderName_11; }
	inline Text_t356221433 ** get_address_of_defenderName_11() { return &___defenderName_11; }
	inline void set_defenderName_11(Text_t356221433 * value)
	{
		___defenderName_11 = value;
		Il2CppCodeGenWriteBarrier(&___defenderName_11, value);
	}

	inline static int32_t get_offset_of_defenderImage_12() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderImage_12)); }
	inline Image_t2042527209 * get_defenderImage_12() const { return ___defenderImage_12; }
	inline Image_t2042527209 ** get_address_of_defenderImage_12() { return &___defenderImage_12; }
	inline void set_defenderImage_12(Image_t2042527209 * value)
	{
		___defenderImage_12 = value;
		Il2CppCodeGenWriteBarrier(&___defenderImage_12, value);
	}

	inline static int32_t get_offset_of_defenderRank_13() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderRank_13)); }
	inline Text_t356221433 * get_defenderRank_13() const { return ___defenderRank_13; }
	inline Text_t356221433 ** get_address_of_defenderRank_13() { return &___defenderRank_13; }
	inline void set_defenderRank_13(Text_t356221433 * value)
	{
		___defenderRank_13 = value;
		Il2CppCodeGenWriteBarrier(&___defenderRank_13, value);
	}

	inline static int32_t get_offset_of_defenderAllianceName_14() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderAllianceName_14)); }
	inline Text_t356221433 * get_defenderAllianceName_14() const { return ___defenderAllianceName_14; }
	inline Text_t356221433 ** get_address_of_defenderAllianceName_14() { return &___defenderAllianceName_14; }
	inline void set_defenderAllianceName_14(Text_t356221433 * value)
	{
		___defenderAllianceName_14 = value;
		Il2CppCodeGenWriteBarrier(&___defenderAllianceName_14, value);
	}

	inline static int32_t get_offset_of_defenderAlliancePosition_15() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderAlliancePosition_15)); }
	inline Text_t356221433 * get_defenderAlliancePosition_15() const { return ___defenderAlliancePosition_15; }
	inline Text_t356221433 ** get_address_of_defenderAlliancePosition_15() { return &___defenderAlliancePosition_15; }
	inline void set_defenderAlliancePosition_15(Text_t356221433 * value)
	{
		___defenderAlliancePosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___defenderAlliancePosition_15, value);
	}

	inline static int32_t get_offset_of_defenderCityName_16() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderCityName_16)); }
	inline Text_t356221433 * get_defenderCityName_16() const { return ___defenderCityName_16; }
	inline Text_t356221433 ** get_address_of_defenderCityName_16() { return &___defenderCityName_16; }
	inline void set_defenderCityName_16(Text_t356221433 * value)
	{
		___defenderCityName_16 = value;
		Il2CppCodeGenWriteBarrier(&___defenderCityName_16, value);
	}

	inline static int32_t get_offset_of_defenderCityImage_17() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderCityImage_17)); }
	inline Image_t2042527209 * get_defenderCityImage_17() const { return ___defenderCityImage_17; }
	inline Image_t2042527209 ** get_address_of_defenderCityImage_17() { return &___defenderCityImage_17; }
	inline void set_defenderCityImage_17(Image_t2042527209 * value)
	{
		___defenderCityImage_17 = value;
		Il2CppCodeGenWriteBarrier(&___defenderCityImage_17, value);
	}

	inline static int32_t get_offset_of_defenderExperiance_18() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderExperiance_18)); }
	inline Text_t356221433 * get_defenderExperiance_18() const { return ___defenderExperiance_18; }
	inline Text_t356221433 ** get_address_of_defenderExperiance_18() { return &___defenderExperiance_18; }
	inline void set_defenderExperiance_18(Text_t356221433 * value)
	{
		___defenderExperiance_18 = value;
		Il2CppCodeGenWriteBarrier(&___defenderExperiance_18, value);
	}

	inline static int32_t get_offset_of_defenderCityRegion_19() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderCityRegion_19)); }
	inline Text_t356221433 * get_defenderCityRegion_19() const { return ___defenderCityRegion_19; }
	inline Text_t356221433 ** get_address_of_defenderCityRegion_19() { return &___defenderCityRegion_19; }
	inline void set_defenderCityRegion_19(Text_t356221433 * value)
	{
		___defenderCityRegion_19 = value;
		Il2CppCodeGenWriteBarrier(&___defenderCityRegion_19, value);
	}

	inline static int32_t get_offset_of_defenderCityHappiness_20() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderCityHappiness_20)); }
	inline Text_t356221433 * get_defenderCityHappiness_20() const { return ___defenderCityHappiness_20; }
	inline Text_t356221433 ** get_address_of_defenderCityHappiness_20() { return &___defenderCityHappiness_20; }
	inline void set_defenderCityHappiness_20(Text_t356221433 * value)
	{
		___defenderCityHappiness_20 = value;
		Il2CppCodeGenWriteBarrier(&___defenderCityHappiness_20, value);
	}

	inline static int32_t get_offset_of_defenderCityCourage_21() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderCityCourage_21)); }
	inline Text_t356221433 * get_defenderCityCourage_21() const { return ___defenderCityCourage_21; }
	inline Text_t356221433 ** get_address_of_defenderCityCourage_21() { return &___defenderCityCourage_21; }
	inline void set_defenderCityCourage_21(Text_t356221433 * value)
	{
		___defenderCityCourage_21 = value;
		Il2CppCodeGenWriteBarrier(&___defenderCityCourage_21, value);
	}

	inline static int32_t get_offset_of_attackerResult_22() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerResult_22)); }
	inline Text_t356221433 * get_attackerResult_22() const { return ___attackerResult_22; }
	inline Text_t356221433 ** get_address_of_attackerResult_22() { return &___attackerResult_22; }
	inline void set_attackerResult_22(Text_t356221433 * value)
	{
		___attackerResult_22 = value;
		Il2CppCodeGenWriteBarrier(&___attackerResult_22, value);
	}

	inline static int32_t get_offset_of_defenderResult_23() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderResult_23)); }
	inline Text_t356221433 * get_defenderResult_23() const { return ___defenderResult_23; }
	inline Text_t356221433 ** get_address_of_defenderResult_23() { return &___defenderResult_23; }
	inline void set_defenderResult_23(Text_t356221433 * value)
	{
		___defenderResult_23 = value;
		Il2CppCodeGenWriteBarrier(&___defenderResult_23, value);
	}

	inline static int32_t get_offset_of_report_24() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___report_24)); }
	inline BattleReportModel_t2786384729 * get_report_24() const { return ___report_24; }
	inline BattleReportModel_t2786384729 ** get_address_of_report_24() { return &___report_24; }
	inline void set_report_24(BattleReportModel_t2786384729 * value)
	{
		___report_24 = value;
		Il2CppCodeGenWriteBarrier(&___report_24, value);
	}

	inline static int32_t get_offset_of_attackerUnitsController_25() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerUnitsController_25)); }
	inline BattleReportUnitTableController_t2722587206 * get_attackerUnitsController_25() const { return ___attackerUnitsController_25; }
	inline BattleReportUnitTableController_t2722587206 ** get_address_of_attackerUnitsController_25() { return &___attackerUnitsController_25; }
	inline void set_attackerUnitsController_25(BattleReportUnitTableController_t2722587206 * value)
	{
		___attackerUnitsController_25 = value;
		Il2CppCodeGenWriteBarrier(&___attackerUnitsController_25, value);
	}

	inline static int32_t get_offset_of_defenderUnitsController_26() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderUnitsController_26)); }
	inline BattleReportUnitTableController_t2722587206 * get_defenderUnitsController_26() const { return ___defenderUnitsController_26; }
	inline BattleReportUnitTableController_t2722587206 ** get_address_of_defenderUnitsController_26() { return &___defenderUnitsController_26; }
	inline void set_defenderUnitsController_26(BattleReportUnitTableController_t2722587206 * value)
	{
		___defenderUnitsController_26 = value;
		Il2CppCodeGenWriteBarrier(&___defenderUnitsController_26, value);
	}

	inline static int32_t get_offset_of_defenderResourcesController_27() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderResourcesController_27)); }
	inline BattleReportResourcesTableController_t4005631621 * get_defenderResourcesController_27() const { return ___defenderResourcesController_27; }
	inline BattleReportResourcesTableController_t4005631621 ** get_address_of_defenderResourcesController_27() { return &___defenderResourcesController_27; }
	inline void set_defenderResourcesController_27(BattleReportResourcesTableController_t4005631621 * value)
	{
		___defenderResourcesController_27 = value;
		Il2CppCodeGenWriteBarrier(&___defenderResourcesController_27, value);
	}

	inline static int32_t get_offset_of_defendersItemsController_28() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defendersItemsController_28)); }
	inline BattleReportItemsTableController_t2343543308 * get_defendersItemsController_28() const { return ___defendersItemsController_28; }
	inline BattleReportItemsTableController_t2343543308 ** get_address_of_defendersItemsController_28() { return &___defendersItemsController_28; }
	inline void set_defendersItemsController_28(BattleReportItemsTableController_t2343543308 * value)
	{
		___defendersItemsController_28 = value;
		Il2CppCodeGenWriteBarrier(&___defendersItemsController_28, value);
	}

	inline static int32_t get_offset_of_attackerUnitNames_29() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerUnitNames_29)); }
	inline List_1_t1398341365 * get_attackerUnitNames_29() const { return ___attackerUnitNames_29; }
	inline List_1_t1398341365 ** get_address_of_attackerUnitNames_29() { return &___attackerUnitNames_29; }
	inline void set_attackerUnitNames_29(List_1_t1398341365 * value)
	{
		___attackerUnitNames_29 = value;
		Il2CppCodeGenWriteBarrier(&___attackerUnitNames_29, value);
	}

	inline static int32_t get_offset_of_attackerBeforeCount_30() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerBeforeCount_30)); }
	inline List_1_t278199169 * get_attackerBeforeCount_30() const { return ___attackerBeforeCount_30; }
	inline List_1_t278199169 ** get_address_of_attackerBeforeCount_30() { return &___attackerBeforeCount_30; }
	inline void set_attackerBeforeCount_30(List_1_t278199169 * value)
	{
		___attackerBeforeCount_30 = value;
		Il2CppCodeGenWriteBarrier(&___attackerBeforeCount_30, value);
	}

	inline static int32_t get_offset_of_attackerAfterCount_31() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___attackerAfterCount_31)); }
	inline List_1_t278199169 * get_attackerAfterCount_31() const { return ___attackerAfterCount_31; }
	inline List_1_t278199169 ** get_address_of_attackerAfterCount_31() { return &___attackerAfterCount_31; }
	inline void set_attackerAfterCount_31(List_1_t278199169 * value)
	{
		___attackerAfterCount_31 = value;
		Il2CppCodeGenWriteBarrier(&___attackerAfterCount_31, value);
	}

	inline static int32_t get_offset_of_defenderUnitNames_32() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderUnitNames_32)); }
	inline List_1_t1398341365 * get_defenderUnitNames_32() const { return ___defenderUnitNames_32; }
	inline List_1_t1398341365 ** get_address_of_defenderUnitNames_32() { return &___defenderUnitNames_32; }
	inline void set_defenderUnitNames_32(List_1_t1398341365 * value)
	{
		___defenderUnitNames_32 = value;
		Il2CppCodeGenWriteBarrier(&___defenderUnitNames_32, value);
	}

	inline static int32_t get_offset_of_defenderBeforeCount_33() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderBeforeCount_33)); }
	inline List_1_t278199169 * get_defenderBeforeCount_33() const { return ___defenderBeforeCount_33; }
	inline List_1_t278199169 ** get_address_of_defenderBeforeCount_33() { return &___defenderBeforeCount_33; }
	inline void set_defenderBeforeCount_33(List_1_t278199169 * value)
	{
		___defenderBeforeCount_33 = value;
		Il2CppCodeGenWriteBarrier(&___defenderBeforeCount_33, value);
	}

	inline static int32_t get_offset_of_defenderAfterCount_34() { return static_cast<int32_t>(offsetof(ScoutReportManager_t2004256841, ___defenderAfterCount_34)); }
	inline List_1_t278199169 * get_defenderAfterCount_34() const { return ___defenderAfterCount_34; }
	inline List_1_t278199169 ** get_address_of_defenderAfterCount_34() { return &___defenderAfterCount_34; }
	inline void set_defenderAfterCount_34(List_1_t278199169 * value)
	{
		___defenderAfterCount_34 = value;
		Il2CppCodeGenWriteBarrier(&___defenderAfterCount_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
