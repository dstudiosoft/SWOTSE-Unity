﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/CountryName
struct CountryName_t1203253298;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/CountryName::.ctor()
extern "C"  void CountryName__ctor_m3364563718 (CountryName_t1203253298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
