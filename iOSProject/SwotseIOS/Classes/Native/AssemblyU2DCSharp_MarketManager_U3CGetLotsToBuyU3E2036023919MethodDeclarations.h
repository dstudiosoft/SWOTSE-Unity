﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager/<GetLotsToBuy>c__IteratorE
struct U3CGetLotsToBuyU3Ec__IteratorE_t2036023919;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketManager/<GetLotsToBuy>c__IteratorE::.ctor()
extern "C"  void U3CGetLotsToBuyU3Ec__IteratorE__ctor_m4082182630 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<GetLotsToBuy>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetLotsToBuyU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2138103518 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<GetLotsToBuy>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetLotsToBuyU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3698494870 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketManager/<GetLotsToBuy>c__IteratorE::MoveNext()
extern "C"  bool U3CGetLotsToBuyU3Ec__IteratorE_MoveNext_m1500833934 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<GetLotsToBuy>c__IteratorE::Dispose()
extern "C"  void U3CGetLotsToBuyU3Ec__IteratorE_Dispose_m2343154621 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<GetLotsToBuy>c__IteratorE::Reset()
extern "C"  void U3CGetLotsToBuyU3Ec__IteratorE_Reset_m858452583 (U3CGetLotsToBuyU3Ec__IteratorE_t2036023919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
