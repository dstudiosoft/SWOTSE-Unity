﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m865052752(__this, ___l0, method) ((  void (*) (Enumerator_t2911006355 *, List_1_t3376276681 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m672765826(__this, method) ((  void (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m981375826(__this, method) ((  Il2CppObject * (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::Dispose()
#define Enumerator_Dispose_m3250919549(__this, method) ((  void (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::VerifyState()
#define Enumerator_VerifyState_m2067823662(__this, method) ((  void (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::MoveNext()
#define Enumerator_MoveNext_m2694083798(__this, method) ((  bool (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Net.NetworkInformation.Win32IPGlobalProperties/Win32_MIB_TCP6ROW>::get_Current()
#define Enumerator_get_Current_m2043317741(__this, method) ((  Win32_MIB_TCP6ROW_t4007155549 * (*) (Enumerator_t2911006355 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
