﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.LinuxUnicastIPAddressInformation
struct LinuxUnicastIPAddressInformation_t870378058;
// System.Net.IPAddress
struct IPAddress_t1399971723;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_NetworkInformation_DuplicateAddr3969562151.h"
#include "System_System_Net_NetworkInformation_PrefixOrigin2896511548.h"
#include "System_System_Net_NetworkInformation_SuffixOrigin2288602937.h"

// System.Void System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::.ctor(System.Net.IPAddress)
extern "C"  void LinuxUnicastIPAddressInformation__ctor_m2878236655 (LinuxUnicastIPAddressInformation_t870378058 * __this, IPAddress_t1399971723 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_Address()
extern "C"  IPAddress_t1399971723 * LinuxUnicastIPAddressInformation_get_Address_m3888621915 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_IsDnsEligible()
extern "C"  bool LinuxUnicastIPAddressInformation_get_IsDnsEligible_m3877875601 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_IsTransient()
extern "C"  bool LinuxUnicastIPAddressInformation_get_IsTransient_m2253865985 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_AddressPreferredLifetime()
extern "C"  int64_t LinuxUnicastIPAddressInformation_get_AddressPreferredLifetime_m3659061278 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_AddressValidLifetime()
extern "C"  int64_t LinuxUnicastIPAddressInformation_get_AddressValidLifetime_m2643415969 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_DhcpLeaseLifetime()
extern "C"  int64_t LinuxUnicastIPAddressInformation_get_DhcpLeaseLifetime_m4129451082 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.DuplicateAddressDetectionState System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_DuplicateAddressDetectionState()
extern "C"  int32_t LinuxUnicastIPAddressInformation_get_DuplicateAddressDetectionState_m2117221258 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_IPv4Mask()
extern "C"  IPAddress_t1399971723 * LinuxUnicastIPAddressInformation_get_IPv4Mask_m2730788940 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PrefixOrigin System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_PrefixOrigin()
extern "C"  int32_t LinuxUnicastIPAddressInformation_get_PrefixOrigin_m57173032 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.SuffixOrigin System.Net.NetworkInformation.LinuxUnicastIPAddressInformation::get_SuffixOrigin()
extern "C"  int32_t LinuxUnicastIPAddressInformation_get_SuffixOrigin_m1148039026 (LinuxUnicastIPAddressInformation_t870378058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
