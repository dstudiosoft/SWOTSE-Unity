﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceInfoTableContoller
struct AllianceInfoTableContoller_t4166874261;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceInfoTableContoller::.ctor()
extern "C"  void AllianceInfoTableContoller__ctor_m2058276202 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::Start()
extern "C"  void AllianceInfoTableContoller_Start_m3747264994 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::OnEnable()
extern "C"  void AllianceInfoTableContoller_OnEnable_m3514525250 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllianceInfoTableContoller::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t AllianceInfoTableContoller_GetNumberOfRowsForTableView_m2427804732 (AllianceInfoTableContoller_t4166874261 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AllianceInfoTableContoller::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float AllianceInfoTableContoller_GetHeightForRowInTableView_m1743896284 (AllianceInfoTableContoller_t4166874261 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell AllianceInfoTableContoller::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * AllianceInfoTableContoller_GetCellForRowInTableView_m1876639897 (AllianceInfoTableContoller_t4166874261 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::GotAlliances(System.Object,System.String)
extern "C"  void AllianceInfoTableContoller_GotAlliances_m1176883444 (AllianceInfoTableContoller_t4166874261 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::ShowAlly()
extern "C"  void AllianceInfoTableContoller_ShowAlly_m3223696631 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::ShowEnemy()
extern "C"  void AllianceInfoTableContoller_ShowEnemy_m1334059925 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::ShowNeutral()
extern "C"  void AllianceInfoTableContoller_ShowNeutral_m1932296586 (AllianceInfoTableContoller_t4166874261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MakeAlly(System.Int64)
extern "C"  void AllianceInfoTableContoller_MakeAlly_m2939657004 (AllianceInfoTableContoller_t4166874261 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MadeAlly(System.Object,System.String)
extern "C"  void AllianceInfoTableContoller_MadeAlly_m866996117 (AllianceInfoTableContoller_t4166874261 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MakeEnemy(System.Int64)
extern "C"  void AllianceInfoTableContoller_MakeEnemy_m3393661188 (AllianceInfoTableContoller_t4166874261 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MadeEnemy(System.Object,System.String)
extern "C"  void AllianceInfoTableContoller_MadeEnemy_m3429616215 (AllianceInfoTableContoller_t4166874261 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MakeNeutral(System.Int64)
extern "C"  void AllianceInfoTableContoller_MakeNeutral_m3763794749 (AllianceInfoTableContoller_t4166874261 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoTableContoller::MadeNeutral(System.Object,System.String)
extern "C"  void AllianceInfoTableContoller_MadeNeutral_m632472626 (AllianceInfoTableContoller_t4166874261 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
