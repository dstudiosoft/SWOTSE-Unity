﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// GuestFeastingTableController
struct GuestFeastingTableController_t3137115560;
// NewMailPlayersTableController
struct NewMailPlayersTableController_t2252158828;
// ShopTableController
struct ShopTableController_t1404998102;
// CityModel
struct CityModel_t1286289939;
// SimpleUserModel
struct SimpleUserModel_t455560495;
// AllianceEventsTableController
struct AllianceEventsTableController_t2478949448;
// AllianceInviteTableController
struct AllianceInviteTableController_t1458639565;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// ArmyGeneralModel
struct ArmyGeneralModel_t737750221;
// WorldFieldModel
struct WorldFieldModel_t2417974361;
// ShopItemModel
struct ShopItemModel_t334704368;
// ResidenceColoniesTableController
struct ResidenceColoniesTableController_t3519043985;
// ResourcesModel
struct ResourcesModel_t2533508513;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// BuildingModel
struct BuildingModel_t2283411500;
// UserModel
struct UserModel_t1353931605;
// MyCityModel
struct MyCityModel_t3961736920;
// System.Collections.Generic.List`1<CityTreasureModel>
struct List_1_t3391946265;
// JSONObject
struct JSONObject_t1339445639;
// AllianceModel
struct AllianceModel_t2995969982;
// ResidenceBuildingsTableLine
struct ResidenceBuildingsTableLine_t2264296562;
// Tacticsoft.TableView
struct TableView_t4228429533;
// ResidenceCitiesTableLine
struct ResidenceCitiesTableLine_t3242262530;
// ResidenceColoniesTableLine
struct ResidenceColoniesTableLine_t998651070;
// SimpleEvent
struct SimpleEvent_t129249603;
// ResidenceValleysTableLine
struct ResidenceValleysTableLine_t1359679749;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject[0...,0...]
struct GameObjectU5B0___U2C0___U5D_t3328599147;
// UpgradeBuildingTableLine
struct UpgradeBuildingTableLine_t2892907218;
// IReloadable
struct IReloadable_t494058401;
// MarketManager
struct MarketManager_t880414981;
// MarketBuyTableLine
struct MarketBuyTableLine_t264875335;
// PanelTableLine
struct PanelTableLine_t11631953;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// AllianceAlliancesLine
struct AllianceAlliancesLine_t2112943226;
// AllianceModel[]
struct AllianceModelU5BU5D_t4240941963;
// AllianceEventsLine
struct AllianceEventsLine_t1491641488;
// GuestHouseTableLine
struct GuestHouseTableLine_t228100067;
// System.Collections.Generic.List`1<ArmyGeneralModel>
struct List_1_t2209824963;
// ConfirmationEvent
struct ConfirmationEvent_t890979749;
// ShopTableCell
struct ShopTableCell_t1369591435;
// PanelTableController
struct PanelTableController_t3691279756;
// FinancialContentChanger
struct FinancialContentChanger_t4019109810;
// FinancialTableLine
struct FinancialTableLine_t3445994354;
// AllianceInfoAlliancesLine
struct AllianceInfoAlliancesLine_t1537220308;
// System.Collections.Generic.List`1<AllianceModel>
struct List_1_t173077428;
// BattleReportResourcesLine
struct BattleReportResourcesLine_t3745703875;
// System.Collections.Generic.List`1<ResourcesModel>
struct List_1_t4005583255;
// BattleReportUnitsLine
struct BattleReportUnitsLine_t2550456670;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t913674750;
// CommandCenterArmyTableLine
struct CommandCenterArmyTableLine_t1166067843;
// System.Collections.Generic.List`1<ArmyModel>
struct List_1_t3540747847;
// AllianceInviteLine
struct AllianceInviteLine_t1691417839;
// System.Collections.Generic.List`1<UserModel>
struct List_1_t2826006347;
// AllianceMembersPermissionsLine
struct AllianceMembersPermissionsLine_t2063553011;
// UserModel[]
struct UserModelU5BU5D_t104655800;
// BattleReportItemsLine
struct BattleReportItemsLine_t1544447758;
// SmartLocalization.LanguageManager
struct LanguageManager_t2767934455;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// MailInboxLine
struct MailInboxLine_t3110909470;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// MessageHeader[]
struct MessageHeaderU5BU5D_t1916243966;
// PanelKnightTableLine
struct PanelKnightTableLine_t48687488;
// NewMailContentManager
struct NewMailContentManager_t1552843396;
// NewMailPlayerLine
struct NewMailPlayerLine_t272747317;
// MarketSellTableLine
struct MarketSellTableLine_t228365181;
// MailSystemLine
struct MailSystemLine_t2170509778;
// SystemReportModel[]
struct SystemReportModelU5BU5D_t826745815;
// MailInviteLine
struct MailInviteLine_t4144392430;
// InviteReportModel[]
struct InviteReportModelU5BU5D_t3007066497;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// MailAllianceReportTableLine
struct MailAllianceReportTableLine_t2381192320;
// BattleReportHeader[]
struct BattleReportHeaderU5BU5D_t3016397654;
// ResidenceValleysTableController
struct ResidenceValleysTableController_t655612456;
// UpgradeBuildingTableController
struct UpgradeBuildingTableController_t2757797628;
// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>
struct EventHandler_1_t766498446;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t765533606;
// TouchScript.ITouchManager
struct ITouchManager_t2123341201;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t2345966477;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1245047080;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t1334725428;
// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint>
struct ReadOnlyCollection_1_t2086468022;
// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>
struct TimedSequence_1_t1745006843;
// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t955425494;
// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2609779151;
// MailAllianceReportTableController
struct MailAllianceReportTableController_t2367082911;
// AllianceAlliancesTableController
struct AllianceAlliancesTableController_t3866927190;
// AllianceInfoTableContoller
struct AllianceInfoTableContoller_t4165916044;
// ResidenceCitiesTableController
struct ResidenceCitiesTableController_t4096198820;
// ResidenceBuildingsTableController
struct ResidenceBuildingsTableController_t4099106534;
// MailInboxTableController
struct MailInboxTableController_t2667561573;
// MailInviteTableController
struct MailInviteTableController_t1579929683;
// UnityEngine.Material
struct Material_t340375123;
// TestWorldChunksManager
struct TestWorldChunksManager_t1200850834;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SHOPITEMMODEL_T334704368_H
#define SHOPITEMMODEL_T334704368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopItemModel
struct  ShopItemModel_t334704368  : public RuntimeObject
{
public:
	// System.String ShopItemModel::id
	String_t* ___id_0;
	// System.String ShopItemModel::name
	String_t* ___name_1;
	// System.String ShopItemModel::rank
	String_t* ___rank_2;
	// System.String ShopItemModel::description
	String_t* ___description_3;
	// System.Int64 ShopItemModel::price_sh
	int64_t ___price_sh_4;
	// System.String ShopItemModel::type
	String_t* ___type_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier((&___rank_2), value);
	}

	inline static int32_t get_offset_of_description_3() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___description_3)); }
	inline String_t* get_description_3() const { return ___description_3; }
	inline String_t** get_address_of_description_3() { return &___description_3; }
	inline void set_description_3(String_t* value)
	{
		___description_3 = value;
		Il2CppCodeGenWriteBarrier((&___description_3), value);
	}

	inline static int32_t get_offset_of_price_sh_4() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___price_sh_4)); }
	inline int64_t get_price_sh_4() const { return ___price_sh_4; }
	inline int64_t* get_address_of_price_sh_4() { return &___price_sh_4; }
	inline void set_price_sh_4(int64_t value)
	{
		___price_sh_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ShopItemModel_t334704368, ___type_5)); }
	inline String_t* get_type_5() const { return ___type_5; }
	inline String_t** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(String_t* value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPITEMMODEL_T334704368_H
#ifndef SIMPLEUSERMODEL_T455560495_H
#define SIMPLEUSERMODEL_T455560495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleUserModel
struct  SimpleUserModel_t455560495  : public RuntimeObject
{
public:
	// System.Int64 SimpleUserModel::id
	int64_t ___id_0;
	// System.String SimpleUserModel::username
	String_t* ___username_1;
	// System.String SimpleUserModel::rank
	String_t* ___rank_2;
	// System.Int64 SimpleUserModel::emperor_id
	int64_t ___emperor_id_3;
	// System.String SimpleUserModel::status
	String_t* ___status_4;
	// System.Int64 SimpleUserModel::experience
	int64_t ___experience_5;
	// System.String SimpleUserModel::alliance_name
	String_t* ___alliance_name_6;
	// System.Int64 SimpleUserModel::allianceID
	int64_t ___allianceID_7;
	// System.Int64 SimpleUserModel::capitalID
	int64_t ___capitalID_8;
	// System.String SimpleUserModel::image_name
	String_t* ___image_name_9;
	// System.String SimpleUserModel::flag
	String_t* ___flag_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier((&___rank_2), value);
	}

	inline static int32_t get_offset_of_emperor_id_3() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___emperor_id_3)); }
	inline int64_t get_emperor_id_3() const { return ___emperor_id_3; }
	inline int64_t* get_address_of_emperor_id_3() { return &___emperor_id_3; }
	inline void set_emperor_id_3(int64_t value)
	{
		___emperor_id_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___status_4)); }
	inline String_t* get_status_4() const { return ___status_4; }
	inline String_t** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(String_t* value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier((&___status_4), value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___experience_5)); }
	inline int64_t get_experience_5() const { return ___experience_5; }
	inline int64_t* get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(int64_t value)
	{
		___experience_5 = value;
	}

	inline static int32_t get_offset_of_alliance_name_6() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___alliance_name_6)); }
	inline String_t* get_alliance_name_6() const { return ___alliance_name_6; }
	inline String_t** get_address_of_alliance_name_6() { return &___alliance_name_6; }
	inline void set_alliance_name_6(String_t* value)
	{
		___alliance_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_name_6), value);
	}

	inline static int32_t get_offset_of_allianceID_7() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___allianceID_7)); }
	inline int64_t get_allianceID_7() const { return ___allianceID_7; }
	inline int64_t* get_address_of_allianceID_7() { return &___allianceID_7; }
	inline void set_allianceID_7(int64_t value)
	{
		___allianceID_7 = value;
	}

	inline static int32_t get_offset_of_capitalID_8() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___capitalID_8)); }
	inline int64_t get_capitalID_8() const { return ___capitalID_8; }
	inline int64_t* get_address_of_capitalID_8() { return &___capitalID_8; }
	inline void set_capitalID_8(int64_t value)
	{
		___capitalID_8 = value;
	}

	inline static int32_t get_offset_of_image_name_9() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___image_name_9)); }
	inline String_t* get_image_name_9() const { return ___image_name_9; }
	inline String_t** get_address_of_image_name_9() { return &___image_name_9; }
	inline void set_image_name_9(String_t* value)
	{
		___image_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(SimpleUserModel_t455560495, ___flag_10)); }
	inline String_t* get_flag_10() const { return ___flag_10; }
	inline String_t** get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(String_t* value)
	{
		___flag_10 = value;
		Il2CppCodeGenWriteBarrier((&___flag_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEUSERMODEL_T455560495_H
#ifndef U3CGETALLAVAILABLEGENERALSU3EC__ITERATOR0_T3806474765_H
#define U3CGETALLAVAILABLEGENERALSU3EC__ITERATOR0_T3806474765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0
struct  U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::<postforn>__0
	WWWForm_t4064702195 * ___U3CpostfornU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// GuestFeastingTableController GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::$this
	GuestFeastingTableController_t3137115560 * ___U24this_2;
	// System.Object GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GuestFeastingTableController/<GetAllAvailableGenerals>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CpostfornU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U3CpostfornU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CpostfornU3E__0_0() const { return ___U3CpostfornU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CpostfornU3E__0_0() { return &___U3CpostfornU3E__0_0; }
	inline void set_U3CpostfornU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CpostfornU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostfornU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U24this_2)); }
	inline GuestFeastingTableController_t3137115560 * get_U24this_2() const { return ___U24this_2; }
	inline GuestFeastingTableController_t3137115560 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GuestFeastingTableController_t3137115560 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLAVAILABLEGENERALSU3EC__ITERATOR0_T3806474765_H
#ifndef MARKETLOTMODEL_T1127536123_H
#define MARKETLOTMODEL_T1127536123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketLotModel
struct  MarketLotModel_t1127536123  : public RuntimeObject
{
public:
	// System.Int64 MarketLotModel::id
	int64_t ___id_0;
	// System.Int64 MarketLotModel::owner
	int64_t ___owner_1;
	// System.Int64 MarketLotModel::city
	int64_t ___city_2;
	// System.String MarketLotModel::resource_type
	String_t* ___resource_type_3;
	// System.Int64 MarketLotModel::count
	int64_t ___count_4;
	// System.Int64 MarketLotModel::price
	int64_t ___price_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___owner_1)); }
	inline int64_t get_owner_1() const { return ___owner_1; }
	inline int64_t* get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(int64_t value)
	{
		___owner_1 = value;
	}

	inline static int32_t get_offset_of_city_2() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___city_2)); }
	inline int64_t get_city_2() const { return ___city_2; }
	inline int64_t* get_address_of_city_2() { return &___city_2; }
	inline void set_city_2(int64_t value)
	{
		___city_2 = value;
	}

	inline static int32_t get_offset_of_resource_type_3() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___resource_type_3)); }
	inline String_t* get_resource_type_3() const { return ___resource_type_3; }
	inline String_t** get_address_of_resource_type_3() { return &___resource_type_3; }
	inline void set_resource_type_3(String_t* value)
	{
		___resource_type_3 = value;
		Il2CppCodeGenWriteBarrier((&___resource_type_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___count_4)); }
	inline int64_t get_count_4() const { return ___count_4; }
	inline int64_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int64_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_price_5() { return static_cast<int32_t>(offsetof(MarketLotModel_t1127536123, ___price_5)); }
	inline int64_t get_price_5() const { return ___price_5; }
	inline int64_t* get_address_of_price_5() { return &___price_5; }
	inline void set_price_5(int64_t value)
	{
		___price_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETLOTMODEL_T1127536123_H
#ifndef U3CGETALLUSERSU3EC__ITERATOR0_T3310400646_H
#define U3CGETALLUSERSU3EC__ITERATOR0_T3310400646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailPlayersTableController/<GetAllUsers>c__Iterator0
struct  U3CGetAllUsersU3Ec__Iterator0_t3310400646  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest NewMailPlayersTableController/<GetAllUsers>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// NewMailPlayersTableController NewMailPlayersTableController/<GetAllUsers>c__Iterator0::$this
	NewMailPlayersTableController_t2252158828 * ___U24this_1;
	// System.Object NewMailPlayersTableController/<GetAllUsers>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean NewMailPlayersTableController/<GetAllUsers>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 NewMailPlayersTableController/<GetAllUsers>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3310400646, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3310400646, ___U24this_1)); }
	inline NewMailPlayersTableController_t2252158828 * get_U24this_1() const { return ___U24this_1; }
	inline NewMailPlayersTableController_t2252158828 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(NewMailPlayersTableController_t2252158828 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3310400646, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3310400646, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3310400646, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLUSERSU3EC__ITERATOR0_T3310400646_H
#ifndef MYCITYMODEL_T3961736920_H
#define MYCITYMODEL_T3961736920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyCityModel
struct  MyCityModel_t3961736920  : public RuntimeObject
{
public:
	// System.Int64 MyCityModel::id
	int64_t ___id_0;
	// System.String MyCityModel::city_name
	String_t* ___city_name_1;
	// System.Int64 MyCityModel::posX
	int64_t ___posX_2;
	// System.Int64 MyCityModel::posY
	int64_t ___posY_3;
	// System.String MyCityModel::region
	String_t* ___region_4;
	// System.String MyCityModel::image_name
	String_t* ___image_name_5;
	// System.Int64 MyCityModel::max_population
	int64_t ___max_population_6;
	// System.Int64 MyCityModel::current_population
	int64_t ___current_population_7;
	// System.Int64 MyCityModel::happiness
	int64_t ___happiness_8;
	// System.Int64 MyCityModel::courage
	int64_t ___courage_9;
	// System.Boolean MyCityModel::isExpantion
	bool ___isExpantion_10;
	// System.Boolean MyCityModel::reinforce_flag
	bool ___reinforce_flag_11;
	// System.Int64 MyCityModel::gold_tax_rate
	int64_t ___gold_tax_rate_12;
	// System.Int64 MyCityModel::fields_count
	int64_t ___fields_count_13;
	// System.Int64 MyCityModel::valleys_count
	int64_t ___valleys_count_14;
	// System.Int64 MyCityModel::mayer_residence_level
	int64_t ___mayer_residence_level_15;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_city_name_1() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___city_name_1)); }
	inline String_t* get_city_name_1() const { return ___city_name_1; }
	inline String_t** get_address_of_city_name_1() { return &___city_name_1; }
	inline void set_city_name_1(String_t* value)
	{
		___city_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___city_name_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___posX_2)); }
	inline int64_t get_posX_2() const { return ___posX_2; }
	inline int64_t* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(int64_t value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___posY_3)); }
	inline int64_t get_posY_3() const { return ___posY_3; }
	inline int64_t* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(int64_t value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___region_4)); }
	inline String_t* get_region_4() const { return ___region_4; }
	inline String_t** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(String_t* value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_image_name_5() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___image_name_5)); }
	inline String_t* get_image_name_5() const { return ___image_name_5; }
	inline String_t** get_address_of_image_name_5() { return &___image_name_5; }
	inline void set_image_name_5(String_t* value)
	{
		___image_name_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_5), value);
	}

	inline static int32_t get_offset_of_max_population_6() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___max_population_6)); }
	inline int64_t get_max_population_6() const { return ___max_population_6; }
	inline int64_t* get_address_of_max_population_6() { return &___max_population_6; }
	inline void set_max_population_6(int64_t value)
	{
		___max_population_6 = value;
	}

	inline static int32_t get_offset_of_current_population_7() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___current_population_7)); }
	inline int64_t get_current_population_7() const { return ___current_population_7; }
	inline int64_t* get_address_of_current_population_7() { return &___current_population_7; }
	inline void set_current_population_7(int64_t value)
	{
		___current_population_7 = value;
	}

	inline static int32_t get_offset_of_happiness_8() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___happiness_8)); }
	inline int64_t get_happiness_8() const { return ___happiness_8; }
	inline int64_t* get_address_of_happiness_8() { return &___happiness_8; }
	inline void set_happiness_8(int64_t value)
	{
		___happiness_8 = value;
	}

	inline static int32_t get_offset_of_courage_9() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___courage_9)); }
	inline int64_t get_courage_9() const { return ___courage_9; }
	inline int64_t* get_address_of_courage_9() { return &___courage_9; }
	inline void set_courage_9(int64_t value)
	{
		___courage_9 = value;
	}

	inline static int32_t get_offset_of_isExpantion_10() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___isExpantion_10)); }
	inline bool get_isExpantion_10() const { return ___isExpantion_10; }
	inline bool* get_address_of_isExpantion_10() { return &___isExpantion_10; }
	inline void set_isExpantion_10(bool value)
	{
		___isExpantion_10 = value;
	}

	inline static int32_t get_offset_of_reinforce_flag_11() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___reinforce_flag_11)); }
	inline bool get_reinforce_flag_11() const { return ___reinforce_flag_11; }
	inline bool* get_address_of_reinforce_flag_11() { return &___reinforce_flag_11; }
	inline void set_reinforce_flag_11(bool value)
	{
		___reinforce_flag_11 = value;
	}

	inline static int32_t get_offset_of_gold_tax_rate_12() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___gold_tax_rate_12)); }
	inline int64_t get_gold_tax_rate_12() const { return ___gold_tax_rate_12; }
	inline int64_t* get_address_of_gold_tax_rate_12() { return &___gold_tax_rate_12; }
	inline void set_gold_tax_rate_12(int64_t value)
	{
		___gold_tax_rate_12 = value;
	}

	inline static int32_t get_offset_of_fields_count_13() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___fields_count_13)); }
	inline int64_t get_fields_count_13() const { return ___fields_count_13; }
	inline int64_t* get_address_of_fields_count_13() { return &___fields_count_13; }
	inline void set_fields_count_13(int64_t value)
	{
		___fields_count_13 = value;
	}

	inline static int32_t get_offset_of_valleys_count_14() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___valleys_count_14)); }
	inline int64_t get_valleys_count_14() const { return ___valleys_count_14; }
	inline int64_t* get_address_of_valleys_count_14() { return &___valleys_count_14; }
	inline void set_valleys_count_14(int64_t value)
	{
		___valleys_count_14 = value;
	}

	inline static int32_t get_offset_of_mayer_residence_level_15() { return static_cast<int32_t>(offsetof(MyCityModel_t3961736920, ___mayer_residence_level_15)); }
	inline int64_t get_mayer_residence_level_15() const { return ___mayer_residence_level_15; }
	inline int64_t* get_address_of_mayer_residence_level_15() { return &___mayer_residence_level_15; }
	inline void set_mayer_residence_level_15(int64_t value)
	{
		___mayer_residence_level_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYCITYMODEL_T3961736920_H
#ifndef TUTORIALPAGEMODEL_T200695960_H
#define TUTORIALPAGEMODEL_T200695960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPageModel
struct  TutorialPageModel_t200695960  : public RuntimeObject
{
public:
	// System.Int64 TutorialPageModel::id
	int64_t ___id_0;
	// System.String TutorialPageModel::title
	String_t* ___title_1;
	// System.Int64 TutorialPageModel::page
	int64_t ___page_2;
	// System.String TutorialPageModel::text
	String_t* ___text_3;
	// System.String TutorialPageModel::image
	String_t* ___image_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TutorialPageModel_t200695960, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(TutorialPageModel_t200695960, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_page_2() { return static_cast<int32_t>(offsetof(TutorialPageModel_t200695960, ___page_2)); }
	inline int64_t get_page_2() const { return ___page_2; }
	inline int64_t* get_address_of_page_2() { return &___page_2; }
	inline void set_page_2(int64_t value)
	{
		___page_2 = value;
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(TutorialPageModel_t200695960, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(TutorialPageModel_t200695960, ___image_4)); }
	inline String_t* get_image_4() const { return ___image_4; }
	inline String_t** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(String_t* value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALPAGEMODEL_T200695960_H
#ifndef U3CGETPLAYERSHOPITEMSU3EC__ITERATOR1_T3108955028_H
#define U3CGETPLAYERSHOPITEMSU3EC__ITERATOR1_T3108955028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableController/<GetPlayerShopItems>c__Iterator1
struct  U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ShopTableController/<GetPlayerShopItems>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ShopTableController ShopTableController/<GetPlayerShopItems>c__Iterator1::$this
	ShopTableController_t1404998102 * ___U24this_1;
	// System.Object ShopTableController/<GetPlayerShopItems>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ShopTableController/<GetPlayerShopItems>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ShopTableController/<GetPlayerShopItems>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028, ___U24this_1)); }
	inline ShopTableController_t1404998102 * get_U24this_1() const { return ___U24this_1; }
	inline ShopTableController_t1404998102 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShopTableController_t1404998102 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPLAYERSHOPITEMSU3EC__ITERATOR1_T3108955028_H
#ifndef WORLDFIELDMODEL_T2417974361_H
#define WORLDFIELDMODEL_T2417974361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldModel
struct  WorldFieldModel_t2417974361  : public RuntimeObject
{
public:
	// System.Int64 WorldFieldModel::id
	int64_t ___id_0;
	// System.Int64 WorldFieldModel::posX
	int64_t ___posX_1;
	// System.Int64 WorldFieldModel::posY
	int64_t ___posY_2;
	// System.String WorldFieldModel::cityType
	String_t* ___cityType_3;
	// CityModel WorldFieldModel::city
	CityModel_t1286289939 * ___city_4;
	// SimpleUserModel WorldFieldModel::owner
	SimpleUserModel_t455560495 * ___owner_5;
	// System.Int64 WorldFieldModel::level
	int64_t ___level_6;
	// System.Int64 WorldFieldModel::production
	int64_t ___production_7;
	// System.String WorldFieldModel::region
	String_t* ___region_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_posX_1() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___posX_1)); }
	inline int64_t get_posX_1() const { return ___posX_1; }
	inline int64_t* get_address_of_posX_1() { return &___posX_1; }
	inline void set_posX_1(int64_t value)
	{
		___posX_1 = value;
	}

	inline static int32_t get_offset_of_posY_2() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___posY_2)); }
	inline int64_t get_posY_2() const { return ___posY_2; }
	inline int64_t* get_address_of_posY_2() { return &___posY_2; }
	inline void set_posY_2(int64_t value)
	{
		___posY_2 = value;
	}

	inline static int32_t get_offset_of_cityType_3() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___cityType_3)); }
	inline String_t* get_cityType_3() const { return ___cityType_3; }
	inline String_t** get_address_of_cityType_3() { return &___cityType_3; }
	inline void set_cityType_3(String_t* value)
	{
		___cityType_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityType_3), value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___city_4)); }
	inline CityModel_t1286289939 * get_city_4() const { return ___city_4; }
	inline CityModel_t1286289939 ** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(CityModel_t1286289939 * value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier((&___city_4), value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___owner_5)); }
	inline SimpleUserModel_t455560495 * get_owner_5() const { return ___owner_5; }
	inline SimpleUserModel_t455560495 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(SimpleUserModel_t455560495 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}

	inline static int32_t get_offset_of_level_6() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___level_6)); }
	inline int64_t get_level_6() const { return ___level_6; }
	inline int64_t* get_address_of_level_6() { return &___level_6; }
	inline void set_level_6(int64_t value)
	{
		___level_6 = value;
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___production_7)); }
	inline int64_t get_production_7() const { return ___production_7; }
	inline int64_t* get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(int64_t value)
	{
		___production_7 = value;
	}

	inline static int32_t get_offset_of_region_8() { return static_cast<int32_t>(offsetof(WorldFieldModel_t2417974361, ___region_8)); }
	inline String_t* get_region_8() const { return ___region_8; }
	inline String_t** get_address_of_region_8() { return &___region_8; }
	inline void set_region_8(String_t* value)
	{
		___region_8 = value;
		Il2CppCodeGenWriteBarrier((&___region_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDFIELDMODEL_T2417974361_H
#ifndef U3CGETSHOPITEMSU3EC__ITERATOR0_T332572680_H
#define U3CGETSHOPITEMSU3EC__ITERATOR0_T332572680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableController/<GetShopItems>c__Iterator0
struct  U3CGetShopItemsU3Ec__Iterator0_t332572680  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ShopTableController/<GetShopItems>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ShopTableController ShopTableController/<GetShopItems>c__Iterator0::$this
	ShopTableController_t1404998102 * ___U24this_1;
	// System.Object ShopTableController/<GetShopItems>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ShopTableController/<GetShopItems>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ShopTableController/<GetShopItems>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetShopItemsU3Ec__Iterator0_t332572680, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetShopItemsU3Ec__Iterator0_t332572680, ___U24this_1)); }
	inline ShopTableController_t1404998102 * get_U24this_1() const { return ___U24this_1; }
	inline ShopTableController_t1404998102 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShopTableController_t1404998102 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetShopItemsU3Ec__Iterator0_t332572680, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetShopItemsU3Ec__Iterator0_t332572680, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetShopItemsU3Ec__Iterator0_t332572680, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSHOPITEMSU3EC__ITERATOR0_T332572680_H
#ifndef U3CGETALLIANCEEVENTSU3EC__ITERATOR0_T939093064_H
#define U3CGETALLIANCEEVENTSU3EC__ITERATOR0_T939093064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventsTableController/<GetAllianceEvents>c__Iterator0
struct  U3CGetAllianceEventsU3Ec__Iterator0_t939093064  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest AllianceEventsTableController/<GetAllianceEvents>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// AllianceEventsTableController AllianceEventsTableController/<GetAllianceEvents>c__Iterator0::$this
	AllianceEventsTableController_t2478949448 * ___U24this_1;
	// System.Object AllianceEventsTableController/<GetAllianceEvents>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AllianceEventsTableController/<GetAllianceEvents>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 AllianceEventsTableController/<GetAllianceEvents>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAllianceEventsU3Ec__Iterator0_t939093064, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAllianceEventsU3Ec__Iterator0_t939093064, ___U24this_1)); }
	inline AllianceEventsTableController_t2478949448 * get_U24this_1() const { return ___U24this_1; }
	inline AllianceEventsTableController_t2478949448 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AllianceEventsTableController_t2478949448 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetAllianceEventsU3Ec__Iterator0_t939093064, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetAllianceEventsU3Ec__Iterator0_t939093064, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetAllianceEventsU3Ec__Iterator0_t939093064, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLIANCEEVENTSU3EC__ITERATOR0_T939093064_H
#ifndef UNITCONSTANTMODEL_T3582189408_H
#define UNITCONSTANTMODEL_T3582189408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitConstantModel
struct  UnitConstantModel_t3582189408  : public RuntimeObject
{
public:
	// System.String UnitConstantModel::name
	String_t* ___name_0;
	// System.String UnitConstantModel::unit_name
	String_t* ___unit_name_1;
	// System.Int64 UnitConstantModel::food_price
	int64_t ___food_price_2;
	// System.Int64 UnitConstantModel::wood_price
	int64_t ___wood_price_3;
	// System.Int64 UnitConstantModel::stone_price
	int64_t ___stone_price_4;
	// System.Int64 UnitConstantModel::iron_price
	int64_t ___iron_price_5;
	// System.Int64 UnitConstantModel::cooper_price
	int64_t ___cooper_price_6;
	// System.Int64 UnitConstantModel::gold_price
	int64_t ___gold_price_7;
	// System.Int64 UnitConstantModel::silver_price
	int64_t ___silver_price_8;
	// System.Int64 UnitConstantModel::time_seconds
	int64_t ___time_seconds_9;
	// System.Int64 UnitConstantModel::population_price
	int64_t ___population_price_10;
	// System.Int64 UnitConstantModel::speed
	int64_t ___speed_11;
	// System.Int64 UnitConstantModel::range
	int64_t ___range_12;
	// System.Int64 UnitConstantModel::attack
	int64_t ___attack_13;
	// System.Int64 UnitConstantModel::defence
	int64_t ___defence_14;
	// System.Int64 UnitConstantModel::life
	int64_t ___life_15;
	// System.Int64 UnitConstantModel::capacity
	int64_t ___capacity_16;
	// System.String UnitConstantModel::description
	String_t* ___description_17;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_unit_name_1() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___unit_name_1)); }
	inline String_t* get_unit_name_1() const { return ___unit_name_1; }
	inline String_t** get_address_of_unit_name_1() { return &___unit_name_1; }
	inline void set_unit_name_1(String_t* value)
	{
		___unit_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___unit_name_1), value);
	}

	inline static int32_t get_offset_of_food_price_2() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___food_price_2)); }
	inline int64_t get_food_price_2() const { return ___food_price_2; }
	inline int64_t* get_address_of_food_price_2() { return &___food_price_2; }
	inline void set_food_price_2(int64_t value)
	{
		___food_price_2 = value;
	}

	inline static int32_t get_offset_of_wood_price_3() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___wood_price_3)); }
	inline int64_t get_wood_price_3() const { return ___wood_price_3; }
	inline int64_t* get_address_of_wood_price_3() { return &___wood_price_3; }
	inline void set_wood_price_3(int64_t value)
	{
		___wood_price_3 = value;
	}

	inline static int32_t get_offset_of_stone_price_4() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___stone_price_4)); }
	inline int64_t get_stone_price_4() const { return ___stone_price_4; }
	inline int64_t* get_address_of_stone_price_4() { return &___stone_price_4; }
	inline void set_stone_price_4(int64_t value)
	{
		___stone_price_4 = value;
	}

	inline static int32_t get_offset_of_iron_price_5() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___iron_price_5)); }
	inline int64_t get_iron_price_5() const { return ___iron_price_5; }
	inline int64_t* get_address_of_iron_price_5() { return &___iron_price_5; }
	inline void set_iron_price_5(int64_t value)
	{
		___iron_price_5 = value;
	}

	inline static int32_t get_offset_of_cooper_price_6() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___cooper_price_6)); }
	inline int64_t get_cooper_price_6() const { return ___cooper_price_6; }
	inline int64_t* get_address_of_cooper_price_6() { return &___cooper_price_6; }
	inline void set_cooper_price_6(int64_t value)
	{
		___cooper_price_6 = value;
	}

	inline static int32_t get_offset_of_gold_price_7() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___gold_price_7)); }
	inline int64_t get_gold_price_7() const { return ___gold_price_7; }
	inline int64_t* get_address_of_gold_price_7() { return &___gold_price_7; }
	inline void set_gold_price_7(int64_t value)
	{
		___gold_price_7 = value;
	}

	inline static int32_t get_offset_of_silver_price_8() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___silver_price_8)); }
	inline int64_t get_silver_price_8() const { return ___silver_price_8; }
	inline int64_t* get_address_of_silver_price_8() { return &___silver_price_8; }
	inline void set_silver_price_8(int64_t value)
	{
		___silver_price_8 = value;
	}

	inline static int32_t get_offset_of_time_seconds_9() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___time_seconds_9)); }
	inline int64_t get_time_seconds_9() const { return ___time_seconds_9; }
	inline int64_t* get_address_of_time_seconds_9() { return &___time_seconds_9; }
	inline void set_time_seconds_9(int64_t value)
	{
		___time_seconds_9 = value;
	}

	inline static int32_t get_offset_of_population_price_10() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___population_price_10)); }
	inline int64_t get_population_price_10() const { return ___population_price_10; }
	inline int64_t* get_address_of_population_price_10() { return &___population_price_10; }
	inline void set_population_price_10(int64_t value)
	{
		___population_price_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___speed_11)); }
	inline int64_t get_speed_11() const { return ___speed_11; }
	inline int64_t* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(int64_t value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_range_12() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___range_12)); }
	inline int64_t get_range_12() const { return ___range_12; }
	inline int64_t* get_address_of_range_12() { return &___range_12; }
	inline void set_range_12(int64_t value)
	{
		___range_12 = value;
	}

	inline static int32_t get_offset_of_attack_13() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___attack_13)); }
	inline int64_t get_attack_13() const { return ___attack_13; }
	inline int64_t* get_address_of_attack_13() { return &___attack_13; }
	inline void set_attack_13(int64_t value)
	{
		___attack_13 = value;
	}

	inline static int32_t get_offset_of_defence_14() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___defence_14)); }
	inline int64_t get_defence_14() const { return ___defence_14; }
	inline int64_t* get_address_of_defence_14() { return &___defence_14; }
	inline void set_defence_14(int64_t value)
	{
		___defence_14 = value;
	}

	inline static int32_t get_offset_of_life_15() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___life_15)); }
	inline int64_t get_life_15() const { return ___life_15; }
	inline int64_t* get_address_of_life_15() { return &___life_15; }
	inline void set_life_15(int64_t value)
	{
		___life_15 = value;
	}

	inline static int32_t get_offset_of_capacity_16() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___capacity_16)); }
	inline int64_t get_capacity_16() const { return ___capacity_16; }
	inline int64_t* get_address_of_capacity_16() { return &___capacity_16; }
	inline void set_capacity_16(int64_t value)
	{
		___capacity_16 = value;
	}

	inline static int32_t get_offset_of_description_17() { return static_cast<int32_t>(offsetof(UnitConstantModel_t3582189408, ___description_17)); }
	inline String_t* get_description_17() const { return ___description_17; }
	inline String_t** get_address_of_description_17() { return &___description_17; }
	inline void set_description_17(String_t* value)
	{
		___description_17 = value;
		Il2CppCodeGenWriteBarrier((&___description_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITCONSTANTMODEL_T3582189408_H
#ifndef UNITSMODEL_T3847956398_H
#define UNITSMODEL_T3847956398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsModel
struct  UnitsModel_t3847956398  : public RuntimeObject
{
public:
	// System.Int64 UnitsModel::city
	int64_t ___city_0;
	// System.Int64 UnitsModel::workers_count
	int64_t ___workers_count_1;
	// System.Int64 UnitsModel::spy_count
	int64_t ___spy_count_2;
	// System.Int64 UnitsModel::swards_man_count
	int64_t ___swards_man_count_3;
	// System.Int64 UnitsModel::spear_man_count
	int64_t ___spear_man_count_4;
	// System.Int64 UnitsModel::pike_man_count
	int64_t ___pike_man_count_5;
	// System.Int64 UnitsModel::scout_rider_count
	int64_t ___scout_rider_count_6;
	// System.Int64 UnitsModel::light_cavalry_count
	int64_t ___light_cavalry_count_7;
	// System.Int64 UnitsModel::heavy_cavalry_count
	int64_t ___heavy_cavalry_count_8;
	// System.Int64 UnitsModel::archer_count
	int64_t ___archer_count_9;
	// System.Int64 UnitsModel::archer_rider_count
	int64_t ___archer_rider_count_10;
	// System.Int64 UnitsModel::holly_man_count
	int64_t ___holly_man_count_11;
	// System.Int64 UnitsModel::wagon_count
	int64_t ___wagon_count_12;
	// System.Int64 UnitsModel::trebuchet_count
	int64_t ___trebuchet_count_13;
	// System.Int64 UnitsModel::siege_towers_count
	int64_t ___siege_towers_count_14;
	// System.Int64 UnitsModel::battering_ram_count
	int64_t ___battering_ram_count_15;
	// System.Int64 UnitsModel::ballista_count
	int64_t ___ballista_count_16;
	// System.String UnitsModel::army_status
	String_t* ___army_status_17;

public:
	inline static int32_t get_offset_of_city_0() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___city_0)); }
	inline int64_t get_city_0() const { return ___city_0; }
	inline int64_t* get_address_of_city_0() { return &___city_0; }
	inline void set_city_0(int64_t value)
	{
		___city_0 = value;
	}

	inline static int32_t get_offset_of_workers_count_1() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___workers_count_1)); }
	inline int64_t get_workers_count_1() const { return ___workers_count_1; }
	inline int64_t* get_address_of_workers_count_1() { return &___workers_count_1; }
	inline void set_workers_count_1(int64_t value)
	{
		___workers_count_1 = value;
	}

	inline static int32_t get_offset_of_spy_count_2() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___spy_count_2)); }
	inline int64_t get_spy_count_2() const { return ___spy_count_2; }
	inline int64_t* get_address_of_spy_count_2() { return &___spy_count_2; }
	inline void set_spy_count_2(int64_t value)
	{
		___spy_count_2 = value;
	}

	inline static int32_t get_offset_of_swards_man_count_3() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___swards_man_count_3)); }
	inline int64_t get_swards_man_count_3() const { return ___swards_man_count_3; }
	inline int64_t* get_address_of_swards_man_count_3() { return &___swards_man_count_3; }
	inline void set_swards_man_count_3(int64_t value)
	{
		___swards_man_count_3 = value;
	}

	inline static int32_t get_offset_of_spear_man_count_4() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___spear_man_count_4)); }
	inline int64_t get_spear_man_count_4() const { return ___spear_man_count_4; }
	inline int64_t* get_address_of_spear_man_count_4() { return &___spear_man_count_4; }
	inline void set_spear_man_count_4(int64_t value)
	{
		___spear_man_count_4 = value;
	}

	inline static int32_t get_offset_of_pike_man_count_5() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___pike_man_count_5)); }
	inline int64_t get_pike_man_count_5() const { return ___pike_man_count_5; }
	inline int64_t* get_address_of_pike_man_count_5() { return &___pike_man_count_5; }
	inline void set_pike_man_count_5(int64_t value)
	{
		___pike_man_count_5 = value;
	}

	inline static int32_t get_offset_of_scout_rider_count_6() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___scout_rider_count_6)); }
	inline int64_t get_scout_rider_count_6() const { return ___scout_rider_count_6; }
	inline int64_t* get_address_of_scout_rider_count_6() { return &___scout_rider_count_6; }
	inline void set_scout_rider_count_6(int64_t value)
	{
		___scout_rider_count_6 = value;
	}

	inline static int32_t get_offset_of_light_cavalry_count_7() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___light_cavalry_count_7)); }
	inline int64_t get_light_cavalry_count_7() const { return ___light_cavalry_count_7; }
	inline int64_t* get_address_of_light_cavalry_count_7() { return &___light_cavalry_count_7; }
	inline void set_light_cavalry_count_7(int64_t value)
	{
		___light_cavalry_count_7 = value;
	}

	inline static int32_t get_offset_of_heavy_cavalry_count_8() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___heavy_cavalry_count_8)); }
	inline int64_t get_heavy_cavalry_count_8() const { return ___heavy_cavalry_count_8; }
	inline int64_t* get_address_of_heavy_cavalry_count_8() { return &___heavy_cavalry_count_8; }
	inline void set_heavy_cavalry_count_8(int64_t value)
	{
		___heavy_cavalry_count_8 = value;
	}

	inline static int32_t get_offset_of_archer_count_9() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___archer_count_9)); }
	inline int64_t get_archer_count_9() const { return ___archer_count_9; }
	inline int64_t* get_address_of_archer_count_9() { return &___archer_count_9; }
	inline void set_archer_count_9(int64_t value)
	{
		___archer_count_9 = value;
	}

	inline static int32_t get_offset_of_archer_rider_count_10() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___archer_rider_count_10)); }
	inline int64_t get_archer_rider_count_10() const { return ___archer_rider_count_10; }
	inline int64_t* get_address_of_archer_rider_count_10() { return &___archer_rider_count_10; }
	inline void set_archer_rider_count_10(int64_t value)
	{
		___archer_rider_count_10 = value;
	}

	inline static int32_t get_offset_of_holly_man_count_11() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___holly_man_count_11)); }
	inline int64_t get_holly_man_count_11() const { return ___holly_man_count_11; }
	inline int64_t* get_address_of_holly_man_count_11() { return &___holly_man_count_11; }
	inline void set_holly_man_count_11(int64_t value)
	{
		___holly_man_count_11 = value;
	}

	inline static int32_t get_offset_of_wagon_count_12() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___wagon_count_12)); }
	inline int64_t get_wagon_count_12() const { return ___wagon_count_12; }
	inline int64_t* get_address_of_wagon_count_12() { return &___wagon_count_12; }
	inline void set_wagon_count_12(int64_t value)
	{
		___wagon_count_12 = value;
	}

	inline static int32_t get_offset_of_trebuchet_count_13() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___trebuchet_count_13)); }
	inline int64_t get_trebuchet_count_13() const { return ___trebuchet_count_13; }
	inline int64_t* get_address_of_trebuchet_count_13() { return &___trebuchet_count_13; }
	inline void set_trebuchet_count_13(int64_t value)
	{
		___trebuchet_count_13 = value;
	}

	inline static int32_t get_offset_of_siege_towers_count_14() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___siege_towers_count_14)); }
	inline int64_t get_siege_towers_count_14() const { return ___siege_towers_count_14; }
	inline int64_t* get_address_of_siege_towers_count_14() { return &___siege_towers_count_14; }
	inline void set_siege_towers_count_14(int64_t value)
	{
		___siege_towers_count_14 = value;
	}

	inline static int32_t get_offset_of_battering_ram_count_15() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___battering_ram_count_15)); }
	inline int64_t get_battering_ram_count_15() const { return ___battering_ram_count_15; }
	inline int64_t* get_address_of_battering_ram_count_15() { return &___battering_ram_count_15; }
	inline void set_battering_ram_count_15(int64_t value)
	{
		___battering_ram_count_15 = value;
	}

	inline static int32_t get_offset_of_ballista_count_16() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___ballista_count_16)); }
	inline int64_t get_ballista_count_16() const { return ___ballista_count_16; }
	inline int64_t* get_address_of_ballista_count_16() { return &___ballista_count_16; }
	inline void set_ballista_count_16(int64_t value)
	{
		___ballista_count_16 = value;
	}

	inline static int32_t get_offset_of_army_status_17() { return static_cast<int32_t>(offsetof(UnitsModel_t3847956398, ___army_status_17)); }
	inline String_t* get_army_status_17() const { return ___army_status_17; }
	inline String_t** get_address_of_army_status_17() { return &___army_status_17; }
	inline void set_army_status_17(String_t* value)
	{
		___army_status_17 = value;
		Il2CppCodeGenWriteBarrier((&___army_status_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSMODEL_T3847956398_H
#ifndef U3CGETALLUSERSU3EC__ITERATOR0_T3081675446_H
#define U3CGETALLUSERSU3EC__ITERATOR0_T3081675446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInviteTableController/<GetAllUsers>c__Iterator0
struct  U3CGetAllUsersU3Ec__Iterator0_t3081675446  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest AllianceInviteTableController/<GetAllUsers>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// AllianceInviteTableController AllianceInviteTableController/<GetAllUsers>c__Iterator0::$this
	AllianceInviteTableController_t1458639565 * ___U24this_1;
	// System.Object AllianceInviteTableController/<GetAllUsers>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AllianceInviteTableController/<GetAllUsers>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 AllianceInviteTableController/<GetAllUsers>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3081675446, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3081675446, ___U24this_1)); }
	inline AllianceInviteTableController_t1458639565 * get_U24this_1() const { return ___U24this_1; }
	inline AllianceInviteTableController_t1458639565 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AllianceInviteTableController_t1458639565 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3081675446, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3081675446, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetAllUsersU3Ec__Iterator0_t3081675446, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLUSERSU3EC__ITERATOR0_T3081675446_H
#ifndef BUILDINGCONSTANTMODEL_T2655945000_H
#define BUILDINGCONSTANTMODEL_T2655945000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingConstantModel
struct  BuildingConstantModel_t2655945000  : public RuntimeObject
{
public:
	// System.String BuildingConstantModel::id
	String_t* ___id_0;
	// System.String BuildingConstantModel::building_name
	String_t* ___building_name_1;
	// System.String BuildingConstantModel::description
	String_t* ___description_2;
	// System.Collections.ArrayList BuildingConstantModel::building_prices
	ArrayList_t2718874744 * ___building_prices_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2655945000, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_building_name_1() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2655945000, ___building_name_1)); }
	inline String_t* get_building_name_1() const { return ___building_name_1; }
	inline String_t** get_address_of_building_name_1() { return &___building_name_1; }
	inline void set_building_name_1(String_t* value)
	{
		___building_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___building_name_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2655945000, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_building_prices_3() { return static_cast<int32_t>(offsetof(BuildingConstantModel_t2655945000, ___building_prices_3)); }
	inline ArrayList_t2718874744 * get_building_prices_3() const { return ___building_prices_3; }
	inline ArrayList_t2718874744 ** get_address_of_building_prices_3() { return &___building_prices_3; }
	inline void set_building_prices_3(ArrayList_t2718874744 * value)
	{
		___building_prices_3 = value;
		Il2CppCodeGenWriteBarrier((&___building_prices_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGCONSTANTMODEL_T2655945000_H
#ifndef BUILDINGMODEL_T2283411500_H
#define BUILDINGMODEL_T2283411500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingModel
struct  BuildingModel_t2283411500  : public RuntimeObject
{
public:
	// System.Int64 BuildingModel::id
	int64_t ___id_0;
	// System.String BuildingModel::building
	String_t* ___building_1;
	// System.Int64 BuildingModel::level
	int64_t ___level_2;
	// System.Int64 BuildingModel::pit_id
	int64_t ___pit_id_3;
	// System.Int64 BuildingModel::city
	int64_t ___city_4;
	// System.String BuildingModel::status
	String_t* ___status_5;
	// System.Int64 BuildingModel::population
	int64_t ___population_6;
	// System.String BuildingModel::location
	String_t* ___location_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_building_1() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___building_1)); }
	inline String_t* get_building_1() const { return ___building_1; }
	inline String_t** get_address_of_building_1() { return &___building_1; }
	inline void set_building_1(String_t* value)
	{
		___building_1 = value;
		Il2CppCodeGenWriteBarrier((&___building_1), value);
	}

	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___level_2)); }
	inline int64_t get_level_2() const { return ___level_2; }
	inline int64_t* get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(int64_t value)
	{
		___level_2 = value;
	}

	inline static int32_t get_offset_of_pit_id_3() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___pit_id_3)); }
	inline int64_t get_pit_id_3() const { return ___pit_id_3; }
	inline int64_t* get_address_of_pit_id_3() { return &___pit_id_3; }
	inline void set_pit_id_3(int64_t value)
	{
		___pit_id_3 = value;
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___city_4)); }
	inline int64_t get_city_4() const { return ___city_4; }
	inline int64_t* get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(int64_t value)
	{
		___city_4 = value;
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___status_5)); }
	inline String_t* get_status_5() const { return ___status_5; }
	inline String_t** get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(String_t* value)
	{
		___status_5 = value;
		Il2CppCodeGenWriteBarrier((&___status_5), value);
	}

	inline static int32_t get_offset_of_population_6() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___population_6)); }
	inline int64_t get_population_6() const { return ___population_6; }
	inline int64_t* get_address_of_population_6() { return &___population_6; }
	inline void set_population_6(int64_t value)
	{
		___population_6 = value;
	}

	inline static int32_t get_offset_of_location_7() { return static_cast<int32_t>(offsetof(BuildingModel_t2283411500, ___location_7)); }
	inline String_t* get_location_7() const { return ___location_7; }
	inline String_t** get_address_of_location_7() { return &___location_7; }
	inline void set_location_7(String_t* value)
	{
		___location_7 = value;
		Il2CppCodeGenWriteBarrier((&___location_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGMODEL_T2283411500_H
#ifndef BUILDINGPRICE_T1241848265_H
#define BUILDINGPRICE_T1241848265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingPrice
struct  BuildingPrice_t1241848265  : public RuntimeObject
{
public:
	// System.String BuildingPrice::building
	String_t* ___building_0;
	// System.Int64 BuildingPrice::level
	int64_t ___level_1;
	// System.Int64 BuildingPrice::food_price
	int64_t ___food_price_2;
	// System.Int64 BuildingPrice::wood_price
	int64_t ___wood_price_3;
	// System.Int64 BuildingPrice::stone_price
	int64_t ___stone_price_4;
	// System.Int64 BuildingPrice::iron_price
	int64_t ___iron_price_5;
	// System.Int64 BuildingPrice::cooper_price
	int64_t ___cooper_price_6;
	// System.Int64 BuildingPrice::gold_price
	int64_t ___gold_price_7;
	// System.Int64 BuildingPrice::silver_price
	int64_t ___silver_price_8;
	// System.Int64 BuildingPrice::time_seconds
	int64_t ___time_seconds_9;
	// System.Int64 BuildingPrice::population_price
	int64_t ___population_price_10;

public:
	inline static int32_t get_offset_of_building_0() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___building_0)); }
	inline String_t* get_building_0() const { return ___building_0; }
	inline String_t** get_address_of_building_0() { return &___building_0; }
	inline void set_building_0(String_t* value)
	{
		___building_0 = value;
		Il2CppCodeGenWriteBarrier((&___building_0), value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___level_1)); }
	inline int64_t get_level_1() const { return ___level_1; }
	inline int64_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int64_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_food_price_2() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___food_price_2)); }
	inline int64_t get_food_price_2() const { return ___food_price_2; }
	inline int64_t* get_address_of_food_price_2() { return &___food_price_2; }
	inline void set_food_price_2(int64_t value)
	{
		___food_price_2 = value;
	}

	inline static int32_t get_offset_of_wood_price_3() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___wood_price_3)); }
	inline int64_t get_wood_price_3() const { return ___wood_price_3; }
	inline int64_t* get_address_of_wood_price_3() { return &___wood_price_3; }
	inline void set_wood_price_3(int64_t value)
	{
		___wood_price_3 = value;
	}

	inline static int32_t get_offset_of_stone_price_4() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___stone_price_4)); }
	inline int64_t get_stone_price_4() const { return ___stone_price_4; }
	inline int64_t* get_address_of_stone_price_4() { return &___stone_price_4; }
	inline void set_stone_price_4(int64_t value)
	{
		___stone_price_4 = value;
	}

	inline static int32_t get_offset_of_iron_price_5() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___iron_price_5)); }
	inline int64_t get_iron_price_5() const { return ___iron_price_5; }
	inline int64_t* get_address_of_iron_price_5() { return &___iron_price_5; }
	inline void set_iron_price_5(int64_t value)
	{
		___iron_price_5 = value;
	}

	inline static int32_t get_offset_of_cooper_price_6() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___cooper_price_6)); }
	inline int64_t get_cooper_price_6() const { return ___cooper_price_6; }
	inline int64_t* get_address_of_cooper_price_6() { return &___cooper_price_6; }
	inline void set_cooper_price_6(int64_t value)
	{
		___cooper_price_6 = value;
	}

	inline static int32_t get_offset_of_gold_price_7() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___gold_price_7)); }
	inline int64_t get_gold_price_7() const { return ___gold_price_7; }
	inline int64_t* get_address_of_gold_price_7() { return &___gold_price_7; }
	inline void set_gold_price_7(int64_t value)
	{
		___gold_price_7 = value;
	}

	inline static int32_t get_offset_of_silver_price_8() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___silver_price_8)); }
	inline int64_t get_silver_price_8() const { return ___silver_price_8; }
	inline int64_t* get_address_of_silver_price_8() { return &___silver_price_8; }
	inline void set_silver_price_8(int64_t value)
	{
		___silver_price_8 = value;
	}

	inline static int32_t get_offset_of_time_seconds_9() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___time_seconds_9)); }
	inline int64_t get_time_seconds_9() const { return ___time_seconds_9; }
	inline int64_t* get_address_of_time_seconds_9() { return &___time_seconds_9; }
	inline void set_time_seconds_9(int64_t value)
	{
		___time_seconds_9 = value;
	}

	inline static int32_t get_offset_of_population_price_10() { return static_cast<int32_t>(offsetof(BuildingPrice_t1241848265, ___population_price_10)); }
	inline int64_t get_population_price_10() const { return ___population_price_10; }
	inline int64_t* get_address_of_population_price_10() { return &___population_price_10; }
	inline void set_population_price_10(int64_t value)
	{
		___population_price_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGPRICE_T1241848265_H
#ifndef ARMYGENERALMODEL_T737750221_H
#define ARMYGENERALMODEL_T737750221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyGeneralModel
struct  ArmyGeneralModel_t737750221  : public RuntimeObject
{
public:
	// System.Int64 ArmyGeneralModel::id
	int64_t ___id_0;
	// System.String ArmyGeneralModel::name
	String_t* ___name_1;
	// System.Int64 ArmyGeneralModel::cityID
	int64_t ___cityID_2;
	// System.Int64 ArmyGeneralModel::politics
	int64_t ___politics_3;
	// System.Int64 ArmyGeneralModel::marches
	int64_t ___marches_4;
	// System.Int64 ArmyGeneralModel::speed
	int64_t ___speed_5;
	// System.Int64 ArmyGeneralModel::loyalty
	int64_t ___loyalty_6;
	// System.Int64 ArmyGeneralModel::level
	int64_t ___level_7;
	// System.Int64 ArmyGeneralModel::salary
	int64_t ___salary_8;
	// System.Int64 ArmyGeneralModel::experience
	int64_t ___experience_9;
	// System.Int64 ArmyGeneralModel::number_of_battles
	int64_t ___number_of_battles_10;
	// System.String ArmyGeneralModel::status
	String_t* ___status_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_cityID_2() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___cityID_2)); }
	inline int64_t get_cityID_2() const { return ___cityID_2; }
	inline int64_t* get_address_of_cityID_2() { return &___cityID_2; }
	inline void set_cityID_2(int64_t value)
	{
		___cityID_2 = value;
	}

	inline static int32_t get_offset_of_politics_3() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___politics_3)); }
	inline int64_t get_politics_3() const { return ___politics_3; }
	inline int64_t* get_address_of_politics_3() { return &___politics_3; }
	inline void set_politics_3(int64_t value)
	{
		___politics_3 = value;
	}

	inline static int32_t get_offset_of_marches_4() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___marches_4)); }
	inline int64_t get_marches_4() const { return ___marches_4; }
	inline int64_t* get_address_of_marches_4() { return &___marches_4; }
	inline void set_marches_4(int64_t value)
	{
		___marches_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___speed_5)); }
	inline int64_t get_speed_5() const { return ___speed_5; }
	inline int64_t* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(int64_t value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_loyalty_6() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___loyalty_6)); }
	inline int64_t get_loyalty_6() const { return ___loyalty_6; }
	inline int64_t* get_address_of_loyalty_6() { return &___loyalty_6; }
	inline void set_loyalty_6(int64_t value)
	{
		___loyalty_6 = value;
	}

	inline static int32_t get_offset_of_level_7() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___level_7)); }
	inline int64_t get_level_7() const { return ___level_7; }
	inline int64_t* get_address_of_level_7() { return &___level_7; }
	inline void set_level_7(int64_t value)
	{
		___level_7 = value;
	}

	inline static int32_t get_offset_of_salary_8() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___salary_8)); }
	inline int64_t get_salary_8() const { return ___salary_8; }
	inline int64_t* get_address_of_salary_8() { return &___salary_8; }
	inline void set_salary_8(int64_t value)
	{
		___salary_8 = value;
	}

	inline static int32_t get_offset_of_experience_9() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___experience_9)); }
	inline int64_t get_experience_9() const { return ___experience_9; }
	inline int64_t* get_address_of_experience_9() { return &___experience_9; }
	inline void set_experience_9(int64_t value)
	{
		___experience_9 = value;
	}

	inline static int32_t get_offset_of_number_of_battles_10() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___number_of_battles_10)); }
	inline int64_t get_number_of_battles_10() const { return ___number_of_battles_10; }
	inline int64_t* get_address_of_number_of_battles_10() { return &___number_of_battles_10; }
	inline void set_number_of_battles_10(int64_t value)
	{
		___number_of_battles_10 = value;
	}

	inline static int32_t get_offset_of_status_11() { return static_cast<int32_t>(offsetof(ArmyGeneralModel_t737750221, ___status_11)); }
	inline String_t* get_status_11() const { return ___status_11; }
	inline String_t** get_address_of_status_11() { return &___status_11; }
	inline void set_status_11(String_t* value)
	{
		___status_11 = value;
		Il2CppCodeGenWriteBarrier((&___status_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMYGENERALMODEL_T737750221_H
#ifndef ARMYMODEL_T2068673105_H
#define ARMYMODEL_T2068673105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyModel
struct  ArmyModel_t2068673105  : public RuntimeObject
{
public:
	// System.Int64 ArmyModel::id
	int64_t ___id_0;
	// System.Int64 ArmyModel::worldMap
	int64_t ___worldMap_1;
	// ArmyGeneralModel ArmyModel::hero
	ArmyGeneralModel_t737750221 * ___hero_2;
	// WorldFieldModel ArmyModel::destination_point
	WorldFieldModel_t2417974361 * ___destination_point_3;
	// System.Int64 ArmyModel::workers_count
	int64_t ___workers_count_4;
	// System.Int64 ArmyModel::spy_count
	int64_t ___spy_count_5;
	// System.Int64 ArmyModel::swards_man_count
	int64_t ___swards_man_count_6;
	// System.Int64 ArmyModel::spear_man_count
	int64_t ___spear_man_count_7;
	// System.Int64 ArmyModel::pike_man_count
	int64_t ___pike_man_count_8;
	// System.Int64 ArmyModel::scout_rider_count
	int64_t ___scout_rider_count_9;
	// System.Int64 ArmyModel::light_cavalry_count
	int64_t ___light_cavalry_count_10;
	// System.Int64 ArmyModel::heavy_cavalry_count
	int64_t ___heavy_cavalry_count_11;
	// System.Int64 ArmyModel::archer_count
	int64_t ___archer_count_12;
	// System.Int64 ArmyModel::archer_rider_count
	int64_t ___archer_rider_count_13;
	// System.Int64 ArmyModel::holly_man_count
	int64_t ___holly_man_count_14;
	// System.Int64 ArmyModel::wagon_count
	int64_t ___wagon_count_15;
	// System.Int64 ArmyModel::trebuchet_count
	int64_t ___trebuchet_count_16;
	// System.Int64 ArmyModel::siege_towers_count
	int64_t ___siege_towers_count_17;
	// System.Int64 ArmyModel::battering_ram_count
	int64_t ___battering_ram_count_18;
	// System.Int64 ArmyModel::ballista_count
	int64_t ___ballista_count_19;
	// System.String ArmyModel::army_status
	String_t* ___army_status_20;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_worldMap_1() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___worldMap_1)); }
	inline int64_t get_worldMap_1() const { return ___worldMap_1; }
	inline int64_t* get_address_of_worldMap_1() { return &___worldMap_1; }
	inline void set_worldMap_1(int64_t value)
	{
		___worldMap_1 = value;
	}

	inline static int32_t get_offset_of_hero_2() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___hero_2)); }
	inline ArmyGeneralModel_t737750221 * get_hero_2() const { return ___hero_2; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_hero_2() { return &___hero_2; }
	inline void set_hero_2(ArmyGeneralModel_t737750221 * value)
	{
		___hero_2 = value;
		Il2CppCodeGenWriteBarrier((&___hero_2), value);
	}

	inline static int32_t get_offset_of_destination_point_3() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___destination_point_3)); }
	inline WorldFieldModel_t2417974361 * get_destination_point_3() const { return ___destination_point_3; }
	inline WorldFieldModel_t2417974361 ** get_address_of_destination_point_3() { return &___destination_point_3; }
	inline void set_destination_point_3(WorldFieldModel_t2417974361 * value)
	{
		___destination_point_3 = value;
		Il2CppCodeGenWriteBarrier((&___destination_point_3), value);
	}

	inline static int32_t get_offset_of_workers_count_4() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___workers_count_4)); }
	inline int64_t get_workers_count_4() const { return ___workers_count_4; }
	inline int64_t* get_address_of_workers_count_4() { return &___workers_count_4; }
	inline void set_workers_count_4(int64_t value)
	{
		___workers_count_4 = value;
	}

	inline static int32_t get_offset_of_spy_count_5() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___spy_count_5)); }
	inline int64_t get_spy_count_5() const { return ___spy_count_5; }
	inline int64_t* get_address_of_spy_count_5() { return &___spy_count_5; }
	inline void set_spy_count_5(int64_t value)
	{
		___spy_count_5 = value;
	}

	inline static int32_t get_offset_of_swards_man_count_6() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___swards_man_count_6)); }
	inline int64_t get_swards_man_count_6() const { return ___swards_man_count_6; }
	inline int64_t* get_address_of_swards_man_count_6() { return &___swards_man_count_6; }
	inline void set_swards_man_count_6(int64_t value)
	{
		___swards_man_count_6 = value;
	}

	inline static int32_t get_offset_of_spear_man_count_7() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___spear_man_count_7)); }
	inline int64_t get_spear_man_count_7() const { return ___spear_man_count_7; }
	inline int64_t* get_address_of_spear_man_count_7() { return &___spear_man_count_7; }
	inline void set_spear_man_count_7(int64_t value)
	{
		___spear_man_count_7 = value;
	}

	inline static int32_t get_offset_of_pike_man_count_8() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___pike_man_count_8)); }
	inline int64_t get_pike_man_count_8() const { return ___pike_man_count_8; }
	inline int64_t* get_address_of_pike_man_count_8() { return &___pike_man_count_8; }
	inline void set_pike_man_count_8(int64_t value)
	{
		___pike_man_count_8 = value;
	}

	inline static int32_t get_offset_of_scout_rider_count_9() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___scout_rider_count_9)); }
	inline int64_t get_scout_rider_count_9() const { return ___scout_rider_count_9; }
	inline int64_t* get_address_of_scout_rider_count_9() { return &___scout_rider_count_9; }
	inline void set_scout_rider_count_9(int64_t value)
	{
		___scout_rider_count_9 = value;
	}

	inline static int32_t get_offset_of_light_cavalry_count_10() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___light_cavalry_count_10)); }
	inline int64_t get_light_cavalry_count_10() const { return ___light_cavalry_count_10; }
	inline int64_t* get_address_of_light_cavalry_count_10() { return &___light_cavalry_count_10; }
	inline void set_light_cavalry_count_10(int64_t value)
	{
		___light_cavalry_count_10 = value;
	}

	inline static int32_t get_offset_of_heavy_cavalry_count_11() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___heavy_cavalry_count_11)); }
	inline int64_t get_heavy_cavalry_count_11() const { return ___heavy_cavalry_count_11; }
	inline int64_t* get_address_of_heavy_cavalry_count_11() { return &___heavy_cavalry_count_11; }
	inline void set_heavy_cavalry_count_11(int64_t value)
	{
		___heavy_cavalry_count_11 = value;
	}

	inline static int32_t get_offset_of_archer_count_12() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___archer_count_12)); }
	inline int64_t get_archer_count_12() const { return ___archer_count_12; }
	inline int64_t* get_address_of_archer_count_12() { return &___archer_count_12; }
	inline void set_archer_count_12(int64_t value)
	{
		___archer_count_12 = value;
	}

	inline static int32_t get_offset_of_archer_rider_count_13() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___archer_rider_count_13)); }
	inline int64_t get_archer_rider_count_13() const { return ___archer_rider_count_13; }
	inline int64_t* get_address_of_archer_rider_count_13() { return &___archer_rider_count_13; }
	inline void set_archer_rider_count_13(int64_t value)
	{
		___archer_rider_count_13 = value;
	}

	inline static int32_t get_offset_of_holly_man_count_14() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___holly_man_count_14)); }
	inline int64_t get_holly_man_count_14() const { return ___holly_man_count_14; }
	inline int64_t* get_address_of_holly_man_count_14() { return &___holly_man_count_14; }
	inline void set_holly_man_count_14(int64_t value)
	{
		___holly_man_count_14 = value;
	}

	inline static int32_t get_offset_of_wagon_count_15() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___wagon_count_15)); }
	inline int64_t get_wagon_count_15() const { return ___wagon_count_15; }
	inline int64_t* get_address_of_wagon_count_15() { return &___wagon_count_15; }
	inline void set_wagon_count_15(int64_t value)
	{
		___wagon_count_15 = value;
	}

	inline static int32_t get_offset_of_trebuchet_count_16() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___trebuchet_count_16)); }
	inline int64_t get_trebuchet_count_16() const { return ___trebuchet_count_16; }
	inline int64_t* get_address_of_trebuchet_count_16() { return &___trebuchet_count_16; }
	inline void set_trebuchet_count_16(int64_t value)
	{
		___trebuchet_count_16 = value;
	}

	inline static int32_t get_offset_of_siege_towers_count_17() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___siege_towers_count_17)); }
	inline int64_t get_siege_towers_count_17() const { return ___siege_towers_count_17; }
	inline int64_t* get_address_of_siege_towers_count_17() { return &___siege_towers_count_17; }
	inline void set_siege_towers_count_17(int64_t value)
	{
		___siege_towers_count_17 = value;
	}

	inline static int32_t get_offset_of_battering_ram_count_18() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___battering_ram_count_18)); }
	inline int64_t get_battering_ram_count_18() const { return ___battering_ram_count_18; }
	inline int64_t* get_address_of_battering_ram_count_18() { return &___battering_ram_count_18; }
	inline void set_battering_ram_count_18(int64_t value)
	{
		___battering_ram_count_18 = value;
	}

	inline static int32_t get_offset_of_ballista_count_19() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___ballista_count_19)); }
	inline int64_t get_ballista_count_19() const { return ___ballista_count_19; }
	inline int64_t* get_address_of_ballista_count_19() { return &___ballista_count_19; }
	inline void set_ballista_count_19(int64_t value)
	{
		___ballista_count_19 = value;
	}

	inline static int32_t get_offset_of_army_status_20() { return static_cast<int32_t>(offsetof(ArmyModel_t2068673105, ___army_status_20)); }
	inline String_t* get_army_status_20() const { return ___army_status_20; }
	inline String_t** get_address_of_army_status_20() { return &___army_status_20; }
	inline void set_army_status_20(String_t* value)
	{
		___army_status_20 = value;
		Il2CppCodeGenWriteBarrier((&___army_status_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMYMODEL_T2068673105_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CITYTREASUREMODEL_T1919871523_H
#define CITYTREASUREMODEL_T1919871523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityTreasureModel
struct  CityTreasureModel_t1919871523  : public RuntimeObject
{
public:
	// System.String CityTreasureModel::id
	String_t* ___id_0;
	// System.Int64 CityTreasureModel::city
	int64_t ___city_1;
	// ShopItemModel CityTreasureModel::item_constant
	ShopItemModel_t334704368 * ___item_constant_2;
	// System.Int64 CityTreasureModel::count
	int64_t ___count_3;
	// System.Int64 CityTreasureModel::price
	int64_t ___price_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1919871523, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_city_1() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1919871523, ___city_1)); }
	inline int64_t get_city_1() const { return ___city_1; }
	inline int64_t* get_address_of_city_1() { return &___city_1; }
	inline void set_city_1(int64_t value)
	{
		___city_1 = value;
	}

	inline static int32_t get_offset_of_item_constant_2() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1919871523, ___item_constant_2)); }
	inline ShopItemModel_t334704368 * get_item_constant_2() const { return ___item_constant_2; }
	inline ShopItemModel_t334704368 ** get_address_of_item_constant_2() { return &___item_constant_2; }
	inline void set_item_constant_2(ShopItemModel_t334704368 * value)
	{
		___item_constant_2 = value;
		Il2CppCodeGenWriteBarrier((&___item_constant_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1919871523, ___count_3)); }
	inline int64_t get_count_3() const { return ___count_3; }
	inline int64_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int64_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_price_4() { return static_cast<int32_t>(offsetof(CityTreasureModel_t1919871523, ___price_4)); }
	inline int64_t get_price_4() const { return ___price_4; }
	inline int64_t* get_address_of_price_4() { return &___price_4; }
	inline void set_price_4(int64_t value)
	{
		___price_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYTREASUREMODEL_T1919871523_H
#ifndef U3CGETCITYCOLONIESU3EC__ITERATOR0_T525247041_H
#define U3CGETCITYCOLONIESU3EC__ITERATOR0_T525247041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceColoniesTableController/<GetCityColonies>c__Iterator0
struct  U3CGetCityColoniesU3Ec__Iterator0_t525247041  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ResidenceColoniesTableController/<GetCityColonies>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ResidenceColoniesTableController ResidenceColoniesTableController/<GetCityColonies>c__Iterator0::$this
	ResidenceColoniesTableController_t3519043985 * ___U24this_1;
	// System.Object ResidenceColoniesTableController/<GetCityColonies>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ResidenceColoniesTableController/<GetCityColonies>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ResidenceColoniesTableController/<GetCityColonies>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCityColoniesU3Ec__Iterator0_t525247041, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetCityColoniesU3Ec__Iterator0_t525247041, ___U24this_1)); }
	inline ResidenceColoniesTableController_t3519043985 * get_U24this_1() const { return ___U24this_1; }
	inline ResidenceColoniesTableController_t3519043985 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ResidenceColoniesTableController_t3519043985 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetCityColoniesU3Ec__Iterator0_t525247041, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetCityColoniesU3Ec__Iterator0_t525247041, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetCityColoniesU3Ec__Iterator0_t525247041, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCITYCOLONIESU3EC__ITERATOR0_T525247041_H
#ifndef COLONYMODEL_T4002254264_H
#define COLONYMODEL_T4002254264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColonyModel
struct  ColonyModel_t4002254264  : public RuntimeObject
{
public:
	// System.Int64 ColonyModel::id
	int64_t ___id_0;
	// System.String ColonyModel::city_name
	String_t* ___city_name_1;
	// System.String ColonyModel::username
	String_t* ___username_2;
	// System.Int64 ColonyModel::posX
	int64_t ___posX_3;
	// System.Int64 ColonyModel::posY
	int64_t ___posY_4;
	// System.String ColonyModel::region
	String_t* ___region_5;
	// System.Int64 ColonyModel::mayer_residence_level
	int64_t ___mayer_residence_level_6;
	// ResourcesModel ColonyModel::production
	ResourcesModel_t2533508513 * ___production_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_city_name_1() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___city_name_1)); }
	inline String_t* get_city_name_1() const { return ___city_name_1; }
	inline String_t** get_address_of_city_name_1() { return &___city_name_1; }
	inline void set_city_name_1(String_t* value)
	{
		___city_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___city_name_1), value);
	}

	inline static int32_t get_offset_of_username_2() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___username_2)); }
	inline String_t* get_username_2() const { return ___username_2; }
	inline String_t** get_address_of_username_2() { return &___username_2; }
	inline void set_username_2(String_t* value)
	{
		___username_2 = value;
		Il2CppCodeGenWriteBarrier((&___username_2), value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___posX_3)); }
	inline int64_t get_posX_3() const { return ___posX_3; }
	inline int64_t* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(int64_t value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___posY_4)); }
	inline int64_t get_posY_4() const { return ___posY_4; }
	inline int64_t* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(int64_t value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_region_5() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___region_5)); }
	inline String_t* get_region_5() const { return ___region_5; }
	inline String_t** get_address_of_region_5() { return &___region_5; }
	inline void set_region_5(String_t* value)
	{
		___region_5 = value;
		Il2CppCodeGenWriteBarrier((&___region_5), value);
	}

	inline static int32_t get_offset_of_mayer_residence_level_6() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___mayer_residence_level_6)); }
	inline int64_t get_mayer_residence_level_6() const { return ___mayer_residence_level_6; }
	inline int64_t* get_address_of_mayer_residence_level_6() { return &___mayer_residence_level_6; }
	inline void set_mayer_residence_level_6(int64_t value)
	{
		___mayer_residence_level_6 = value;
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(ColonyModel_t4002254264, ___production_7)); }
	inline ResourcesModel_t2533508513 * get_production_7() const { return ___production_7; }
	inline ResourcesModel_t2533508513 ** get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(ResourcesModel_t2533508513 * value)
	{
		___production_7 = value;
		Il2CppCodeGenWriteBarrier((&___production_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLONYMODEL_T4002254264_H
#ifndef CITYMODEL_T1286289939_H
#define CITYMODEL_T1286289939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityModel
struct  CityModel_t1286289939  : public RuntimeObject
{
public:
	// System.Int64 CityModel::id
	int64_t ___id_0;
	// System.Int64 CityModel::owner
	int64_t ___owner_1;
	// System.String CityModel::city_name
	String_t* ___city_name_2;
	// System.String CityModel::image_name
	String_t* ___image_name_3;
	// System.String CityModel::occupantCityOwnerName
	String_t* ___occupantCityOwnerName_4;
	// System.String CityModel::occupantCityName
	String_t* ___occupantCityName_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___owner_1)); }
	inline int64_t get_owner_1() const { return ___owner_1; }
	inline int64_t* get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(int64_t value)
	{
		___owner_1 = value;
	}

	inline static int32_t get_offset_of_city_name_2() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___city_name_2)); }
	inline String_t* get_city_name_2() const { return ___city_name_2; }
	inline String_t** get_address_of_city_name_2() { return &___city_name_2; }
	inline void set_city_name_2(String_t* value)
	{
		___city_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___city_name_2), value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_3), value);
	}

	inline static int32_t get_offset_of_occupantCityOwnerName_4() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___occupantCityOwnerName_4)); }
	inline String_t* get_occupantCityOwnerName_4() const { return ___occupantCityOwnerName_4; }
	inline String_t** get_address_of_occupantCityOwnerName_4() { return &___occupantCityOwnerName_4; }
	inline void set_occupantCityOwnerName_4(String_t* value)
	{
		___occupantCityOwnerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___occupantCityOwnerName_4), value);
	}

	inline static int32_t get_offset_of_occupantCityName_5() { return static_cast<int32_t>(offsetof(CityModel_t1286289939, ___occupantCityName_5)); }
	inline String_t* get_occupantCityName_5() const { return ___occupantCityName_5; }
	inline String_t** get_address_of_occupantCityName_5() { return &___occupantCityName_5; }
	inline void set_occupantCityName_5(String_t* value)
	{
		___occupantCityName_5 = value;
		Il2CppCodeGenWriteBarrier((&___occupantCityName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYMODEL_T1286289939_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef TRANSFORMTYPE_T3234482893_H
#define TRANSFORMTYPE_T3234482893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase/TransformType
struct  TransformType_t3234482893 
{
public:
	// System.Int32 TouchScript.Gestures.Base.TransformGestureBase/TransformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformType_t3234482893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMTYPE_T3234482893_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef GESTURESTATE_T1922660398_H
#define GESTURESTATE_T1922660398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/GestureState
struct  GestureState_t1922660398 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/GestureState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureState_t1922660398, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURESTATE_T1922660398_H
#ifndef TOUCHESNUMSTATE_T401176838_H
#define TOUCHESNUMSTATE_T401176838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/TouchesNumState
struct  TouchesNumState_t401176838 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/TouchesNumState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchesNumState_t401176838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHESNUMSTATE_T401176838_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef UNITTIMER_T3121550770_H
#define UNITTIMER_T3121550770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitTimer
struct  UnitTimer_t3121550770  : public RuntimeObject
{
public:
	// System.Int64 UnitTimer::id
	int64_t ___id_0;
	// System.String UnitTimer::unit
	String_t* ___unit_1;
	// System.Int64 UnitTimer::event_id
	int64_t ___event_id_2;
	// System.DateTime UnitTimer::start_time
	DateTime_t3738529785  ___start_time_3;
	// System.DateTime UnitTimer::finish_time
	DateTime_t3738529785  ___finish_time_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UnitTimer_t3121550770, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(UnitTimer_t3121550770, ___unit_1)); }
	inline String_t* get_unit_1() const { return ___unit_1; }
	inline String_t** get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(String_t* value)
	{
		___unit_1 = value;
		Il2CppCodeGenWriteBarrier((&___unit_1), value);
	}

	inline static int32_t get_offset_of_event_id_2() { return static_cast<int32_t>(offsetof(UnitTimer_t3121550770, ___event_id_2)); }
	inline int64_t get_event_id_2() const { return ___event_id_2; }
	inline int64_t* get_address_of_event_id_2() { return &___event_id_2; }
	inline void set_event_id_2(int64_t value)
	{
		___event_id_2 = value;
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(UnitTimer_t3121550770, ___start_time_3)); }
	inline DateTime_t3738529785  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t3738529785 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t3738529785  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(UnitTimer_t3121550770, ___finish_time_4)); }
	inline DateTime_t3738529785  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t3738529785 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t3738529785  value)
	{
		___finish_time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITTIMER_T3121550770_H
#ifndef FINANCEREPORTMODEL_T1598071249_H
#define FINANCEREPORTMODEL_T1598071249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinanceReportModel
struct  FinanceReportModel_t1598071249  : public RuntimeObject
{
public:
	// System.String FinanceReportModel::id
	String_t* ___id_0;
	// System.Int64 FinanceReportModel::user
	int64_t ___user_1;
	// System.Int64 FinanceReportModel::city
	int64_t ___city_2;
	// ShopItemModel FinanceReportModel::item_constant
	ShopItemModel_t334704368 * ___item_constant_3;
	// System.Int64 FinanceReportModel::count
	int64_t ___count_4;
	// System.DateTime FinanceReportModel::date
	DateTime_t3738529785  ___date_5;
	// System.Int64 FinanceReportModel::price
	int64_t ___price_6;
	// System.String FinanceReportModel::type
	String_t* ___type_7;
	// System.String FinanceReportModel::fromUser
	String_t* ___fromUser_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_user_1() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___user_1)); }
	inline int64_t get_user_1() const { return ___user_1; }
	inline int64_t* get_address_of_user_1() { return &___user_1; }
	inline void set_user_1(int64_t value)
	{
		___user_1 = value;
	}

	inline static int32_t get_offset_of_city_2() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___city_2)); }
	inline int64_t get_city_2() const { return ___city_2; }
	inline int64_t* get_address_of_city_2() { return &___city_2; }
	inline void set_city_2(int64_t value)
	{
		___city_2 = value;
	}

	inline static int32_t get_offset_of_item_constant_3() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___item_constant_3)); }
	inline ShopItemModel_t334704368 * get_item_constant_3() const { return ___item_constant_3; }
	inline ShopItemModel_t334704368 ** get_address_of_item_constant_3() { return &___item_constant_3; }
	inline void set_item_constant_3(ShopItemModel_t334704368 * value)
	{
		___item_constant_3 = value;
		Il2CppCodeGenWriteBarrier((&___item_constant_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___count_4)); }
	inline int64_t get_count_4() const { return ___count_4; }
	inline int64_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int64_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_date_5() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___date_5)); }
	inline DateTime_t3738529785  get_date_5() const { return ___date_5; }
	inline DateTime_t3738529785 * get_address_of_date_5() { return &___date_5; }
	inline void set_date_5(DateTime_t3738529785  value)
	{
		___date_5 = value;
	}

	inline static int32_t get_offset_of_price_6() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___price_6)); }
	inline int64_t get_price_6() const { return ___price_6; }
	inline int64_t* get_address_of_price_6() { return &___price_6; }
	inline void set_price_6(int64_t value)
	{
		___price_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___type_7)); }
	inline String_t* get_type_7() const { return ___type_7; }
	inline String_t** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(String_t* value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier((&___type_7), value);
	}

	inline static int32_t get_offset_of_fromUser_8() { return static_cast<int32_t>(offsetof(FinanceReportModel_t1598071249, ___fromUser_8)); }
	inline String_t* get_fromUser_8() const { return ___fromUser_8; }
	inline String_t** get_address_of_fromUser_8() { return &___fromUser_8; }
	inline void set_fromUser_8(String_t* value)
	{
		___fromUser_8 = value;
		Il2CppCodeGenWriteBarrier((&___fromUser_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINANCEREPORTMODEL_T1598071249_H
#ifndef INVITEREPORTMODEL_T637635712_H
#define INVITEREPORTMODEL_T637635712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InviteReportModel
struct  InviteReportModel_t637635712  : public RuntimeObject
{
public:
	// System.Int64 InviteReportModel::id
	int64_t ___id_0;
	// System.Int64 InviteReportModel::inviteFromId
	int64_t ___inviteFromId_1;
	// System.String InviteReportModel::inviteFromName
	String_t* ___inviteFromName_2;
	// System.Int64 InviteReportModel::inviteToId
	int64_t ___inviteToId_3;
	// System.String InviteReportModel::inviteToName
	String_t* ___inviteToName_4;
	// System.Boolean InviteReportModel::isRead
	bool ___isRead_5;
	// System.Int64 InviteReportModel::allianceId
	int64_t ___allianceId_6;
	// System.String InviteReportModel::allianceName
	String_t* ___allianceName_7;
	// System.Int64 InviteReportModel::allianceRank
	int64_t ___allianceRank_8;
	// System.String InviteReportModel::type
	String_t* ___type_9;
	// System.DateTime InviteReportModel::creationDate
	DateTime_t3738529785  ___creationDate_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_inviteFromId_1() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___inviteFromId_1)); }
	inline int64_t get_inviteFromId_1() const { return ___inviteFromId_1; }
	inline int64_t* get_address_of_inviteFromId_1() { return &___inviteFromId_1; }
	inline void set_inviteFromId_1(int64_t value)
	{
		___inviteFromId_1 = value;
	}

	inline static int32_t get_offset_of_inviteFromName_2() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___inviteFromName_2)); }
	inline String_t* get_inviteFromName_2() const { return ___inviteFromName_2; }
	inline String_t** get_address_of_inviteFromName_2() { return &___inviteFromName_2; }
	inline void set_inviteFromName_2(String_t* value)
	{
		___inviteFromName_2 = value;
		Il2CppCodeGenWriteBarrier((&___inviteFromName_2), value);
	}

	inline static int32_t get_offset_of_inviteToId_3() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___inviteToId_3)); }
	inline int64_t get_inviteToId_3() const { return ___inviteToId_3; }
	inline int64_t* get_address_of_inviteToId_3() { return &___inviteToId_3; }
	inline void set_inviteToId_3(int64_t value)
	{
		___inviteToId_3 = value;
	}

	inline static int32_t get_offset_of_inviteToName_4() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___inviteToName_4)); }
	inline String_t* get_inviteToName_4() const { return ___inviteToName_4; }
	inline String_t** get_address_of_inviteToName_4() { return &___inviteToName_4; }
	inline void set_inviteToName_4(String_t* value)
	{
		___inviteToName_4 = value;
		Il2CppCodeGenWriteBarrier((&___inviteToName_4), value);
	}

	inline static int32_t get_offset_of_isRead_5() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___isRead_5)); }
	inline bool get_isRead_5() const { return ___isRead_5; }
	inline bool* get_address_of_isRead_5() { return &___isRead_5; }
	inline void set_isRead_5(bool value)
	{
		___isRead_5 = value;
	}

	inline static int32_t get_offset_of_allianceId_6() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___allianceId_6)); }
	inline int64_t get_allianceId_6() const { return ___allianceId_6; }
	inline int64_t* get_address_of_allianceId_6() { return &___allianceId_6; }
	inline void set_allianceId_6(int64_t value)
	{
		___allianceId_6 = value;
	}

	inline static int32_t get_offset_of_allianceName_7() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___allianceName_7)); }
	inline String_t* get_allianceName_7() const { return ___allianceName_7; }
	inline String_t** get_address_of_allianceName_7() { return &___allianceName_7; }
	inline void set_allianceName_7(String_t* value)
	{
		___allianceName_7 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_7), value);
	}

	inline static int32_t get_offset_of_allianceRank_8() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___allianceRank_8)); }
	inline int64_t get_allianceRank_8() const { return ___allianceRank_8; }
	inline int64_t* get_address_of_allianceRank_8() { return &___allianceRank_8; }
	inline void set_allianceRank_8(int64_t value)
	{
		___allianceRank_8 = value;
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___type_9)); }
	inline String_t* get_type_9() const { return ___type_9; }
	inline String_t** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(String_t* value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier((&___type_9), value);
	}

	inline static int32_t get_offset_of_creationDate_10() { return static_cast<int32_t>(offsetof(InviteReportModel_t637635712, ___creationDate_10)); }
	inline DateTime_t3738529785  get_creationDate_10() const { return ___creationDate_10; }
	inline DateTime_t3738529785 * get_address_of_creationDate_10() { return &___creationDate_10; }
	inline void set_creationDate_10(DateTime_t3738529785  value)
	{
		___creationDate_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVITEREPORTMODEL_T637635712_H
#ifndef RESOURCESMODEL_T2533508513_H
#define RESOURCESMODEL_T2533508513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesModel
struct  ResourcesModel_t2533508513  : public RuntimeObject
{
public:
	// System.Int64 ResourcesModel::city
	int64_t ___city_0;
	// System.Int64 ResourcesModel::food
	int64_t ___food_1;
	// System.Int64 ResourcesModel::wood
	int64_t ___wood_2;
	// System.Int64 ResourcesModel::stone
	int64_t ___stone_3;
	// System.Int64 ResourcesModel::iron
	int64_t ___iron_4;
	// System.Int64 ResourcesModel::cooper
	int64_t ___cooper_5;
	// System.Int64 ResourcesModel::gold
	int64_t ___gold_6;
	// System.Int64 ResourcesModel::silver
	int64_t ___silver_7;
	// System.Int64 ResourcesModel::population
	int64_t ___population_8;
	// System.DateTime ResourcesModel::last_update
	DateTime_t3738529785  ___last_update_9;

public:
	inline static int32_t get_offset_of_city_0() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___city_0)); }
	inline int64_t get_city_0() const { return ___city_0; }
	inline int64_t* get_address_of_city_0() { return &___city_0; }
	inline void set_city_0(int64_t value)
	{
		___city_0 = value;
	}

	inline static int32_t get_offset_of_food_1() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___food_1)); }
	inline int64_t get_food_1() const { return ___food_1; }
	inline int64_t* get_address_of_food_1() { return &___food_1; }
	inline void set_food_1(int64_t value)
	{
		___food_1 = value;
	}

	inline static int32_t get_offset_of_wood_2() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___wood_2)); }
	inline int64_t get_wood_2() const { return ___wood_2; }
	inline int64_t* get_address_of_wood_2() { return &___wood_2; }
	inline void set_wood_2(int64_t value)
	{
		___wood_2 = value;
	}

	inline static int32_t get_offset_of_stone_3() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___stone_3)); }
	inline int64_t get_stone_3() const { return ___stone_3; }
	inline int64_t* get_address_of_stone_3() { return &___stone_3; }
	inline void set_stone_3(int64_t value)
	{
		___stone_3 = value;
	}

	inline static int32_t get_offset_of_iron_4() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___iron_4)); }
	inline int64_t get_iron_4() const { return ___iron_4; }
	inline int64_t* get_address_of_iron_4() { return &___iron_4; }
	inline void set_iron_4(int64_t value)
	{
		___iron_4 = value;
	}

	inline static int32_t get_offset_of_cooper_5() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___cooper_5)); }
	inline int64_t get_cooper_5() const { return ___cooper_5; }
	inline int64_t* get_address_of_cooper_5() { return &___cooper_5; }
	inline void set_cooper_5(int64_t value)
	{
		___cooper_5 = value;
	}

	inline static int32_t get_offset_of_gold_6() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___gold_6)); }
	inline int64_t get_gold_6() const { return ___gold_6; }
	inline int64_t* get_address_of_gold_6() { return &___gold_6; }
	inline void set_gold_6(int64_t value)
	{
		___gold_6 = value;
	}

	inline static int32_t get_offset_of_silver_7() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___silver_7)); }
	inline int64_t get_silver_7() const { return ___silver_7; }
	inline int64_t* get_address_of_silver_7() { return &___silver_7; }
	inline void set_silver_7(int64_t value)
	{
		___silver_7 = value;
	}

	inline static int32_t get_offset_of_population_8() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___population_8)); }
	inline int64_t get_population_8() const { return ___population_8; }
	inline int64_t* get_address_of_population_8() { return &___population_8; }
	inline void set_population_8(int64_t value)
	{
		___population_8 = value;
	}

	inline static int32_t get_offset_of_last_update_9() { return static_cast<int32_t>(offsetof(ResourcesModel_t2533508513, ___last_update_9)); }
	inline DateTime_t3738529785  get_last_update_9() const { return ___last_update_9; }
	inline DateTime_t3738529785 * get_address_of_last_update_9() { return &___last_update_9; }
	inline void set_last_update_9(DateTime_t3738529785  value)
	{
		___last_update_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCESMODEL_T2533508513_H
#ifndef MESSAGEHEADER_T3475772423_H
#define MESSAGEHEADER_T3475772423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageHeader
struct  MessageHeader_t3475772423  : public RuntimeObject
{
public:
	// System.Int64 MessageHeader::id
	int64_t ___id_0;
	// System.String MessageHeader::fromUserName
	String_t* ___fromUserName_1;
	// System.String MessageHeader::subject
	String_t* ___subject_2;
	// System.Boolean MessageHeader::isRead
	bool ___isRead_3;
	// System.DateTime MessageHeader::creation_date
	DateTime_t3738529785  ___creation_date_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MessageHeader_t3475772423, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_fromUserName_1() { return static_cast<int32_t>(offsetof(MessageHeader_t3475772423, ___fromUserName_1)); }
	inline String_t* get_fromUserName_1() const { return ___fromUserName_1; }
	inline String_t** get_address_of_fromUserName_1() { return &___fromUserName_1; }
	inline void set_fromUserName_1(String_t* value)
	{
		___fromUserName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fromUserName_1), value);
	}

	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(MessageHeader_t3475772423, ___subject_2)); }
	inline String_t* get_subject_2() const { return ___subject_2; }
	inline String_t** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(String_t* value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_isRead_3() { return static_cast<int32_t>(offsetof(MessageHeader_t3475772423, ___isRead_3)); }
	inline bool get_isRead_3() const { return ___isRead_3; }
	inline bool* get_address_of_isRead_3() { return &___isRead_3; }
	inline void set_isRead_3(bool value)
	{
		___isRead_3 = value;
	}

	inline static int32_t get_offset_of_creation_date_4() { return static_cast<int32_t>(offsetof(MessageHeader_t3475772423, ___creation_date_4)); }
	inline DateTime_t3738529785  get_creation_date_4() const { return ___creation_date_4; }
	inline DateTime_t3738529785 * get_address_of_creation_date_4() { return &___creation_date_4; }
	inline void set_creation_date_4(DateTime_t3738529785  value)
	{
		___creation_date_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEHEADER_T3475772423_H
#ifndef SYSTEMREPORTMODEL_T2947163234_H
#define SYSTEMREPORTMODEL_T2947163234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemReportModel
struct  SystemReportModel_t2947163234  : public RuntimeObject
{
public:
	// System.Int64 SystemReportModel::id
	int64_t ___id_0;
	// System.String SystemReportModel::subject
	String_t* ___subject_1;
	// System.String SystemReportModel::message
	String_t* ___message_2;
	// System.Boolean SystemReportModel::isRead
	bool ___isRead_3;
	// System.DateTime SystemReportModel::creation_date
	DateTime_t3738529785  ___creation_date_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SystemReportModel_t2947163234, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(SystemReportModel_t2947163234, ___subject_1)); }
	inline String_t* get_subject_1() const { return ___subject_1; }
	inline String_t** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(String_t* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier((&___subject_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(SystemReportModel_t2947163234, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_isRead_3() { return static_cast<int32_t>(offsetof(SystemReportModel_t2947163234, ___isRead_3)); }
	inline bool get_isRead_3() const { return ___isRead_3; }
	inline bool* get_address_of_isRead_3() { return &___isRead_3; }
	inline void set_isRead_3(bool value)
	{
		___isRead_3 = value;
	}

	inline static int32_t get_offset_of_creation_date_4() { return static_cast<int32_t>(offsetof(SystemReportModel_t2947163234, ___creation_date_4)); }
	inline DateTime_t3738529785  get_creation_date_4() const { return ___creation_date_4; }
	inline DateTime_t3738529785 * get_address_of_creation_date_4() { return &___creation_date_4; }
	inline void set_creation_date_4(DateTime_t3738529785  value)
	{
		___creation_date_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMREPORTMODEL_T2947163234_H
#ifndef ITEMTIMER_T2318236575_H
#define ITEMTIMER_T2318236575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemTimer
struct  ItemTimer_t2318236575  : public RuntimeObject
{
public:
	// System.Int64 ItemTimer::id
	int64_t ___id_0;
	// System.Int64 ItemTimer::event_id
	int64_t ___event_id_1;
	// System.String ItemTimer::itemID
	String_t* ___itemID_2;
	// System.DateTime ItemTimer::start_time
	DateTime_t3738529785  ___start_time_3;
	// System.DateTime ItemTimer::finish_time
	DateTime_t3738529785  ___finish_time_4;
	// System.String ItemTimer::name
	String_t* ___name_5;
	// System.String ItemTimer::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_event_id_1() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___event_id_1)); }
	inline int64_t get_event_id_1() const { return ___event_id_1; }
	inline int64_t* get_address_of_event_id_1() { return &___event_id_1; }
	inline void set_event_id_1(int64_t value)
	{
		___event_id_1 = value;
	}

	inline static int32_t get_offset_of_itemID_2() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___itemID_2)); }
	inline String_t* get_itemID_2() const { return ___itemID_2; }
	inline String_t** get_address_of_itemID_2() { return &___itemID_2; }
	inline void set_itemID_2(String_t* value)
	{
		___itemID_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemID_2), value);
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___start_time_3)); }
	inline DateTime_t3738529785  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t3738529785 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t3738529785  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___finish_time_4)); }
	inline DateTime_t3738529785  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t3738529785 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t3738529785  value)
	{
		___finish_time_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(ItemTimer_t2318236575, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTIMER_T2318236575_H
#ifndef BUILDINGTIMER_T374074310_H
#define BUILDINGTIMER_T374074310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingTimer
struct  BuildingTimer_t374074310  : public RuntimeObject
{
public:
	// System.Int64 BuildingTimer::id
	int64_t ___id_0;
	// BuildingModel BuildingTimer::building
	BuildingModel_t2283411500 * ___building_1;
	// System.String BuildingTimer::status
	String_t* ___status_2;
	// System.Int64 BuildingTimer::event_id
	int64_t ___event_id_3;
	// System.DateTime BuildingTimer::start_time
	DateTime_t3738529785  ___start_time_4;
	// System.DateTime BuildingTimer::finish_time
	DateTime_t3738529785  ___finish_time_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_building_1() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___building_1)); }
	inline BuildingModel_t2283411500 * get_building_1() const { return ___building_1; }
	inline BuildingModel_t2283411500 ** get_address_of_building_1() { return &___building_1; }
	inline void set_building_1(BuildingModel_t2283411500 * value)
	{
		___building_1 = value;
		Il2CppCodeGenWriteBarrier((&___building_1), value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_event_id_3() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___event_id_3)); }
	inline int64_t get_event_id_3() const { return ___event_id_3; }
	inline int64_t* get_address_of_event_id_3() { return &___event_id_3; }
	inline void set_event_id_3(int64_t value)
	{
		___event_id_3 = value;
	}

	inline static int32_t get_offset_of_start_time_4() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___start_time_4)); }
	inline DateTime_t3738529785  get_start_time_4() const { return ___start_time_4; }
	inline DateTime_t3738529785 * get_address_of_start_time_4() { return &___start_time_4; }
	inline void set_start_time_4(DateTime_t3738529785  value)
	{
		___start_time_4 = value;
	}

	inline static int32_t get_offset_of_finish_time_5() { return static_cast<int32_t>(offsetof(BuildingTimer_t374074310, ___finish_time_5)); }
	inline DateTime_t3738529785  get_finish_time_5() const { return ___finish_time_5; }
	inline DateTime_t3738529785 * get_address_of_finish_time_5() { return &___finish_time_5; }
	inline void set_finish_time_5(DateTime_t3738529785  value)
	{
		___finish_time_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGTIMER_T374074310_H
#ifndef BATTLEREPORTHEADER_T3025158543_H
#define BATTLEREPORTHEADER_T3025158543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportHeader
struct  BattleReportHeader_t3025158543  : public RuntimeObject
{
public:
	// System.Int64 BattleReportHeader::id
	int64_t ___id_0;
	// System.String BattleReportHeader::attackUserName
	String_t* ___attackUserName_1;
	// System.String BattleReportHeader::defenceUserName
	String_t* ___defenceUserName_2;
	// System.Boolean BattleReportHeader::isSuccessAttack
	bool ___isSuccessAttack_3;
	// System.DateTime BattleReportHeader::creation_date
	DateTime_t3738529785  ___creation_date_4;
	// System.Boolean BattleReportHeader::isReadByAttacker
	bool ___isReadByAttacker_5;
	// System.Boolean BattleReportHeader::isReadByDefender
	bool ___isReadByDefender_6;
	// System.String BattleReportHeader::attack_type
	String_t* ___attack_type_7;
	// System.String BattleReportHeader::defenceCityName
	String_t* ___defenceCityName_8;
	// System.String BattleReportHeader::attackCityName
	String_t* ___attackCityName_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_attackUserName_1() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___attackUserName_1)); }
	inline String_t* get_attackUserName_1() const { return ___attackUserName_1; }
	inline String_t** get_address_of_attackUserName_1() { return &___attackUserName_1; }
	inline void set_attackUserName_1(String_t* value)
	{
		___attackUserName_1 = value;
		Il2CppCodeGenWriteBarrier((&___attackUserName_1), value);
	}

	inline static int32_t get_offset_of_defenceUserName_2() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___defenceUserName_2)); }
	inline String_t* get_defenceUserName_2() const { return ___defenceUserName_2; }
	inline String_t** get_address_of_defenceUserName_2() { return &___defenceUserName_2; }
	inline void set_defenceUserName_2(String_t* value)
	{
		___defenceUserName_2 = value;
		Il2CppCodeGenWriteBarrier((&___defenceUserName_2), value);
	}

	inline static int32_t get_offset_of_isSuccessAttack_3() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___isSuccessAttack_3)); }
	inline bool get_isSuccessAttack_3() const { return ___isSuccessAttack_3; }
	inline bool* get_address_of_isSuccessAttack_3() { return &___isSuccessAttack_3; }
	inline void set_isSuccessAttack_3(bool value)
	{
		___isSuccessAttack_3 = value;
	}

	inline static int32_t get_offset_of_creation_date_4() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___creation_date_4)); }
	inline DateTime_t3738529785  get_creation_date_4() const { return ___creation_date_4; }
	inline DateTime_t3738529785 * get_address_of_creation_date_4() { return &___creation_date_4; }
	inline void set_creation_date_4(DateTime_t3738529785  value)
	{
		___creation_date_4 = value;
	}

	inline static int32_t get_offset_of_isReadByAttacker_5() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___isReadByAttacker_5)); }
	inline bool get_isReadByAttacker_5() const { return ___isReadByAttacker_5; }
	inline bool* get_address_of_isReadByAttacker_5() { return &___isReadByAttacker_5; }
	inline void set_isReadByAttacker_5(bool value)
	{
		___isReadByAttacker_5 = value;
	}

	inline static int32_t get_offset_of_isReadByDefender_6() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___isReadByDefender_6)); }
	inline bool get_isReadByDefender_6() const { return ___isReadByDefender_6; }
	inline bool* get_address_of_isReadByDefender_6() { return &___isReadByDefender_6; }
	inline void set_isReadByDefender_6(bool value)
	{
		___isReadByDefender_6 = value;
	}

	inline static int32_t get_offset_of_attack_type_7() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___attack_type_7)); }
	inline String_t* get_attack_type_7() const { return ___attack_type_7; }
	inline String_t** get_address_of_attack_type_7() { return &___attack_type_7; }
	inline void set_attack_type_7(String_t* value)
	{
		___attack_type_7 = value;
		Il2CppCodeGenWriteBarrier((&___attack_type_7), value);
	}

	inline static int32_t get_offset_of_defenceCityName_8() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___defenceCityName_8)); }
	inline String_t* get_defenceCityName_8() const { return ___defenceCityName_8; }
	inline String_t** get_address_of_defenceCityName_8() { return &___defenceCityName_8; }
	inline void set_defenceCityName_8(String_t* value)
	{
		___defenceCityName_8 = value;
		Il2CppCodeGenWriteBarrier((&___defenceCityName_8), value);
	}

	inline static int32_t get_offset_of_attackCityName_9() { return static_cast<int32_t>(offsetof(BattleReportHeader_t3025158543, ___attackCityName_9)); }
	inline String_t* get_attackCityName_9() const { return ___attackCityName_9; }
	inline String_t** get_address_of_attackCityName_9() { return &___attackCityName_9; }
	inline void set_attackCityName_9(String_t* value)
	{
		___attackCityName_9 = value;
		Il2CppCodeGenWriteBarrier((&___attackCityName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTHEADER_T3025158543_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef WORLDMAPTIMER_T60483741_H
#define WORLDMAPTIMER_T60483741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapTimer
struct  WorldMapTimer_t60483741  : public RuntimeObject
{
public:
	// System.Int64 WorldMapTimer::id
	int64_t ___id_0;
	// System.Int64 WorldMapTimer::worldMap
	int64_t ___worldMap_1;
	// System.String WorldMapTimer::status
	String_t* ___status_2;
	// System.Int64 WorldMapTimer::event_id
	int64_t ___event_id_3;
	// System.DateTime WorldMapTimer::start_time
	DateTime_t3738529785  ___start_time_4;
	// System.DateTime WorldMapTimer::finish_time
	DateTime_t3738529785  ___finish_time_5;
	// System.String WorldMapTimer::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_worldMap_1() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___worldMap_1)); }
	inline int64_t get_worldMap_1() const { return ___worldMap_1; }
	inline int64_t* get_address_of_worldMap_1() { return &___worldMap_1; }
	inline void set_worldMap_1(int64_t value)
	{
		___worldMap_1 = value;
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_event_id_3() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___event_id_3)); }
	inline int64_t get_event_id_3() const { return ___event_id_3; }
	inline int64_t* get_address_of_event_id_3() { return &___event_id_3; }
	inline void set_event_id_3(int64_t value)
	{
		___event_id_3 = value;
	}

	inline static int32_t get_offset_of_start_time_4() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___start_time_4)); }
	inline DateTime_t3738529785  get_start_time_4() const { return ___start_time_4; }
	inline DateTime_t3738529785 * get_address_of_start_time_4() { return &___start_time_4; }
	inline void set_start_time_4(DateTime_t3738529785  value)
	{
		___start_time_4 = value;
	}

	inline static int32_t get_offset_of_finish_time_5() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___finish_time_5)); }
	inline DateTime_t3738529785  get_finish_time_5() const { return ___finish_time_5; }
	inline DateTime_t3738529785 * get_address_of_finish_time_5() { return &___finish_time_5; }
	inline void set_finish_time_5(DateTime_t3738529785  value)
	{
		___finish_time_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(WorldMapTimer_t60483741, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDMAPTIMER_T60483741_H
#ifndef ARMYTIMER_T3282851823_H
#define ARMYTIMER_T3282851823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyTimer
struct  ArmyTimer_t3282851823  : public RuntimeObject
{
public:
	// System.Int64 ArmyTimer::id
	int64_t ___id_0;
	// System.Int64 ArmyTimer::army
	int64_t ___army_1;
	// System.Int64 ArmyTimer::event_id
	int64_t ___event_id_2;
	// System.DateTime ArmyTimer::start_time
	DateTime_t3738529785  ___start_time_3;
	// System.DateTime ArmyTimer::finish_time
	DateTime_t3738529785  ___finish_time_4;
	// System.String ArmyTimer::army_status
	String_t* ___army_status_5;
	// System.Boolean ArmyTimer::enemy
	bool ___enemy_6;
	// System.String ArmyTimer::targetType
	String_t* ___targetType_7;
	// System.String ArmyTimer::old_army_status
	String_t* ___old_army_status_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_army_1() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___army_1)); }
	inline int64_t get_army_1() const { return ___army_1; }
	inline int64_t* get_address_of_army_1() { return &___army_1; }
	inline void set_army_1(int64_t value)
	{
		___army_1 = value;
	}

	inline static int32_t get_offset_of_event_id_2() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___event_id_2)); }
	inline int64_t get_event_id_2() const { return ___event_id_2; }
	inline int64_t* get_address_of_event_id_2() { return &___event_id_2; }
	inline void set_event_id_2(int64_t value)
	{
		___event_id_2 = value;
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___start_time_3)); }
	inline DateTime_t3738529785  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t3738529785 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t3738529785  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___finish_time_4)); }
	inline DateTime_t3738529785  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t3738529785 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t3738529785  value)
	{
		___finish_time_4 = value;
	}

	inline static int32_t get_offset_of_army_status_5() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___army_status_5)); }
	inline String_t* get_army_status_5() const { return ___army_status_5; }
	inline String_t** get_address_of_army_status_5() { return &___army_status_5; }
	inline void set_army_status_5(String_t* value)
	{
		___army_status_5 = value;
		Il2CppCodeGenWriteBarrier((&___army_status_5), value);
	}

	inline static int32_t get_offset_of_enemy_6() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___enemy_6)); }
	inline bool get_enemy_6() const { return ___enemy_6; }
	inline bool* get_address_of_enemy_6() { return &___enemy_6; }
	inline void set_enemy_6(bool value)
	{
		___enemy_6 = value;
	}

	inline static int32_t get_offset_of_targetType_7() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___targetType_7)); }
	inline String_t* get_targetType_7() const { return ___targetType_7; }
	inline String_t** get_address_of_targetType_7() { return &___targetType_7; }
	inline void set_targetType_7(String_t* value)
	{
		___targetType_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetType_7), value);
	}

	inline static int32_t get_offset_of_old_army_status_8() { return static_cast<int32_t>(offsetof(ArmyTimer_t3282851823, ___old_army_status_8)); }
	inline String_t* get_old_army_status_8() const { return ___old_army_status_8; }
	inline String_t** get_address_of_old_army_status_8() { return &___old_army_status_8; }
	inline void set_old_army_status_8(String_t* value)
	{
		___old_army_status_8 = value;
		Il2CppCodeGenWriteBarrier((&___old_army_status_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMYTIMER_T3282851823_H
#ifndef USERMESSAGEREPORTMODEL_T4065405063_H
#define USERMESSAGEREPORTMODEL_T4065405063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserMessageReportModel
struct  UserMessageReportModel_t4065405063  : public RuntimeObject
{
public:
	// System.Int64 UserMessageReportModel::id
	int64_t ___id_0;
	// UserModel UserMessageReportModel::fromUser
	UserModel_t1353931605 * ___fromUser_1;
	// System.String UserMessageReportModel::subject
	String_t* ___subject_2;
	// System.String UserMessageReportModel::message
	String_t* ___message_3;
	// System.Boolean UserMessageReportModel::isRead
	bool ___isRead_4;
	// System.DateTime UserMessageReportModel::creation_date
	DateTime_t3738529785  ___creation_date_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_fromUser_1() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___fromUser_1)); }
	inline UserModel_t1353931605 * get_fromUser_1() const { return ___fromUser_1; }
	inline UserModel_t1353931605 ** get_address_of_fromUser_1() { return &___fromUser_1; }
	inline void set_fromUser_1(UserModel_t1353931605 * value)
	{
		___fromUser_1 = value;
		Il2CppCodeGenWriteBarrier((&___fromUser_1), value);
	}

	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___subject_2)); }
	inline String_t* get_subject_2() const { return ___subject_2; }
	inline String_t** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(String_t* value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_isRead_4() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___isRead_4)); }
	inline bool get_isRead_4() const { return ___isRead_4; }
	inline bool* get_address_of_isRead_4() { return &___isRead_4; }
	inline void set_isRead_4(bool value)
	{
		___isRead_4 = value;
	}

	inline static int32_t get_offset_of_creation_date_5() { return static_cast<int32_t>(offsetof(UserMessageReportModel_t4065405063, ___creation_date_5)); }
	inline DateTime_t3738529785  get_creation_date_5() const { return ___creation_date_5; }
	inline DateTime_t3738529785 * get_address_of_creation_date_5() { return &___creation_date_5; }
	inline void set_creation_date_5(DateTime_t3738529785  value)
	{
		___creation_date_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERMESSAGEREPORTMODEL_T4065405063_H
#ifndef BATTLEREPORTMODEL_T4250794538_H
#define BATTLEREPORTMODEL_T4250794538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportModel
struct  BattleReportModel_t4250794538  : public RuntimeObject
{
public:
	// System.Int64 BattleReportModel::id
	int64_t ___id_0;
	// UserModel BattleReportModel::attackUser
	UserModel_t1353931605 * ___attackUser_1;
	// UserModel BattleReportModel::defenceUser
	UserModel_t1353931605 * ___defenceUser_2;
	// MyCityModel BattleReportModel::attackersCity
	MyCityModel_t3961736920 * ___attackersCity_3;
	// MyCityModel BattleReportModel::defendersCity
	MyCityModel_t3961736920 * ___defendersCity_4;
	// ResourcesModel BattleReportModel::attackArmyResources
	ResourcesModel_t2533508513 * ___attackArmyResources_5;
	// ResourcesModel BattleReportModel::defenceArmyResources
	ResourcesModel_t2533508513 * ___defenceArmyResources_6;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportModel::stolenCityTreasures
	List_1_t3391946265 * ___stolenCityTreasures_7;
	// JSONObject BattleReportModel::attackArmyBeforeBattle
	JSONObject_t1339445639 * ___attackArmyBeforeBattle_8;
	// JSONObject BattleReportModel::defenceArmyBeforeBattle
	JSONObject_t1339445639 * ___defenceArmyBeforeBattle_9;
	// JSONObject BattleReportModel::attackArmyAfterBattle
	JSONObject_t1339445639 * ___attackArmyAfterBattle_10;
	// JSONObject BattleReportModel::defenceArmyAfterBattle
	JSONObject_t1339445639 * ___defenceArmyAfterBattle_11;
	// ResourcesModel BattleReportModel::defendersResources
	ResourcesModel_t2533508513 * ___defendersResources_12;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportModel::defendersCityItems
	List_1_t3391946265 * ___defendersCityItems_13;
	// System.Boolean BattleReportModel::isSuccessAttack
	bool ___isSuccessAttack_14;
	// System.DateTime BattleReportModel::creation_date
	DateTime_t3738529785  ___creation_date_15;
	// System.String BattleReportModel::attack_type
	String_t* ___attack_type_16;
	// System.String BattleReportModel::log
	String_t* ___log_17;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_attackUser_1() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attackUser_1)); }
	inline UserModel_t1353931605 * get_attackUser_1() const { return ___attackUser_1; }
	inline UserModel_t1353931605 ** get_address_of_attackUser_1() { return &___attackUser_1; }
	inline void set_attackUser_1(UserModel_t1353931605 * value)
	{
		___attackUser_1 = value;
		Il2CppCodeGenWriteBarrier((&___attackUser_1), value);
	}

	inline static int32_t get_offset_of_defenceUser_2() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defenceUser_2)); }
	inline UserModel_t1353931605 * get_defenceUser_2() const { return ___defenceUser_2; }
	inline UserModel_t1353931605 ** get_address_of_defenceUser_2() { return &___defenceUser_2; }
	inline void set_defenceUser_2(UserModel_t1353931605 * value)
	{
		___defenceUser_2 = value;
		Il2CppCodeGenWriteBarrier((&___defenceUser_2), value);
	}

	inline static int32_t get_offset_of_attackersCity_3() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attackersCity_3)); }
	inline MyCityModel_t3961736920 * get_attackersCity_3() const { return ___attackersCity_3; }
	inline MyCityModel_t3961736920 ** get_address_of_attackersCity_3() { return &___attackersCity_3; }
	inline void set_attackersCity_3(MyCityModel_t3961736920 * value)
	{
		___attackersCity_3 = value;
		Il2CppCodeGenWriteBarrier((&___attackersCity_3), value);
	}

	inline static int32_t get_offset_of_defendersCity_4() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defendersCity_4)); }
	inline MyCityModel_t3961736920 * get_defendersCity_4() const { return ___defendersCity_4; }
	inline MyCityModel_t3961736920 ** get_address_of_defendersCity_4() { return &___defendersCity_4; }
	inline void set_defendersCity_4(MyCityModel_t3961736920 * value)
	{
		___defendersCity_4 = value;
		Il2CppCodeGenWriteBarrier((&___defendersCity_4), value);
	}

	inline static int32_t get_offset_of_attackArmyResources_5() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attackArmyResources_5)); }
	inline ResourcesModel_t2533508513 * get_attackArmyResources_5() const { return ___attackArmyResources_5; }
	inline ResourcesModel_t2533508513 ** get_address_of_attackArmyResources_5() { return &___attackArmyResources_5; }
	inline void set_attackArmyResources_5(ResourcesModel_t2533508513 * value)
	{
		___attackArmyResources_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackArmyResources_5), value);
	}

	inline static int32_t get_offset_of_defenceArmyResources_6() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defenceArmyResources_6)); }
	inline ResourcesModel_t2533508513 * get_defenceArmyResources_6() const { return ___defenceArmyResources_6; }
	inline ResourcesModel_t2533508513 ** get_address_of_defenceArmyResources_6() { return &___defenceArmyResources_6; }
	inline void set_defenceArmyResources_6(ResourcesModel_t2533508513 * value)
	{
		___defenceArmyResources_6 = value;
		Il2CppCodeGenWriteBarrier((&___defenceArmyResources_6), value);
	}

	inline static int32_t get_offset_of_stolenCityTreasures_7() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___stolenCityTreasures_7)); }
	inline List_1_t3391946265 * get_stolenCityTreasures_7() const { return ___stolenCityTreasures_7; }
	inline List_1_t3391946265 ** get_address_of_stolenCityTreasures_7() { return &___stolenCityTreasures_7; }
	inline void set_stolenCityTreasures_7(List_1_t3391946265 * value)
	{
		___stolenCityTreasures_7 = value;
		Il2CppCodeGenWriteBarrier((&___stolenCityTreasures_7), value);
	}

	inline static int32_t get_offset_of_attackArmyBeforeBattle_8() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attackArmyBeforeBattle_8)); }
	inline JSONObject_t1339445639 * get_attackArmyBeforeBattle_8() const { return ___attackArmyBeforeBattle_8; }
	inline JSONObject_t1339445639 ** get_address_of_attackArmyBeforeBattle_8() { return &___attackArmyBeforeBattle_8; }
	inline void set_attackArmyBeforeBattle_8(JSONObject_t1339445639 * value)
	{
		___attackArmyBeforeBattle_8 = value;
		Il2CppCodeGenWriteBarrier((&___attackArmyBeforeBattle_8), value);
	}

	inline static int32_t get_offset_of_defenceArmyBeforeBattle_9() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defenceArmyBeforeBattle_9)); }
	inline JSONObject_t1339445639 * get_defenceArmyBeforeBattle_9() const { return ___defenceArmyBeforeBattle_9; }
	inline JSONObject_t1339445639 ** get_address_of_defenceArmyBeforeBattle_9() { return &___defenceArmyBeforeBattle_9; }
	inline void set_defenceArmyBeforeBattle_9(JSONObject_t1339445639 * value)
	{
		___defenceArmyBeforeBattle_9 = value;
		Il2CppCodeGenWriteBarrier((&___defenceArmyBeforeBattle_9), value);
	}

	inline static int32_t get_offset_of_attackArmyAfterBattle_10() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attackArmyAfterBattle_10)); }
	inline JSONObject_t1339445639 * get_attackArmyAfterBattle_10() const { return ___attackArmyAfterBattle_10; }
	inline JSONObject_t1339445639 ** get_address_of_attackArmyAfterBattle_10() { return &___attackArmyAfterBattle_10; }
	inline void set_attackArmyAfterBattle_10(JSONObject_t1339445639 * value)
	{
		___attackArmyAfterBattle_10 = value;
		Il2CppCodeGenWriteBarrier((&___attackArmyAfterBattle_10), value);
	}

	inline static int32_t get_offset_of_defenceArmyAfterBattle_11() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defenceArmyAfterBattle_11)); }
	inline JSONObject_t1339445639 * get_defenceArmyAfterBattle_11() const { return ___defenceArmyAfterBattle_11; }
	inline JSONObject_t1339445639 ** get_address_of_defenceArmyAfterBattle_11() { return &___defenceArmyAfterBattle_11; }
	inline void set_defenceArmyAfterBattle_11(JSONObject_t1339445639 * value)
	{
		___defenceArmyAfterBattle_11 = value;
		Il2CppCodeGenWriteBarrier((&___defenceArmyAfterBattle_11), value);
	}

	inline static int32_t get_offset_of_defendersResources_12() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defendersResources_12)); }
	inline ResourcesModel_t2533508513 * get_defendersResources_12() const { return ___defendersResources_12; }
	inline ResourcesModel_t2533508513 ** get_address_of_defendersResources_12() { return &___defendersResources_12; }
	inline void set_defendersResources_12(ResourcesModel_t2533508513 * value)
	{
		___defendersResources_12 = value;
		Il2CppCodeGenWriteBarrier((&___defendersResources_12), value);
	}

	inline static int32_t get_offset_of_defendersCityItems_13() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___defendersCityItems_13)); }
	inline List_1_t3391946265 * get_defendersCityItems_13() const { return ___defendersCityItems_13; }
	inline List_1_t3391946265 ** get_address_of_defendersCityItems_13() { return &___defendersCityItems_13; }
	inline void set_defendersCityItems_13(List_1_t3391946265 * value)
	{
		___defendersCityItems_13 = value;
		Il2CppCodeGenWriteBarrier((&___defendersCityItems_13), value);
	}

	inline static int32_t get_offset_of_isSuccessAttack_14() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___isSuccessAttack_14)); }
	inline bool get_isSuccessAttack_14() const { return ___isSuccessAttack_14; }
	inline bool* get_address_of_isSuccessAttack_14() { return &___isSuccessAttack_14; }
	inline void set_isSuccessAttack_14(bool value)
	{
		___isSuccessAttack_14 = value;
	}

	inline static int32_t get_offset_of_creation_date_15() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___creation_date_15)); }
	inline DateTime_t3738529785  get_creation_date_15() const { return ___creation_date_15; }
	inline DateTime_t3738529785 * get_address_of_creation_date_15() { return &___creation_date_15; }
	inline void set_creation_date_15(DateTime_t3738529785  value)
	{
		___creation_date_15 = value;
	}

	inline static int32_t get_offset_of_attack_type_16() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___attack_type_16)); }
	inline String_t* get_attack_type_16() const { return ___attack_type_16; }
	inline String_t** get_address_of_attack_type_16() { return &___attack_type_16; }
	inline void set_attack_type_16(String_t* value)
	{
		___attack_type_16 = value;
		Il2CppCodeGenWriteBarrier((&___attack_type_16), value);
	}

	inline static int32_t get_offset_of_log_17() { return static_cast<int32_t>(offsetof(BattleReportModel_t4250794538, ___log_17)); }
	inline String_t* get_log_17() const { return ___log_17; }
	inline String_t** get_address_of_log_17() { return &___log_17; }
	inline void set_log_17(String_t* value)
	{
		___log_17 = value;
		Il2CppCodeGenWriteBarrier((&___log_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTMODEL_T4250794538_H
#ifndef USERMODEL_T1353931605_H
#define USERMODEL_T1353931605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserModel
struct  UserModel_t1353931605  : public RuntimeObject
{
public:
	// System.Int64 UserModel::id
	int64_t ___id_0;
	// System.String UserModel::username
	String_t* ___username_1;
	// System.String UserModel::status
	String_t* ___status_2;
	// System.String UserModel::image_name
	String_t* ___image_name_3;
	// System.String UserModel::rank
	String_t* ___rank_4;
	// System.Int64 UserModel::emperor_id
	int64_t ___emperor_id_5;
	// System.String UserModel::email
	String_t* ___email_6;
	// System.Int64 UserModel::experience
	int64_t ___experience_7;
	// System.String UserModel::alliance_rank
	String_t* ___alliance_rank_8;
	// System.DateTime UserModel::last_login
	DateTime_t3738529785  ___last_login_9;
	// System.Int64 UserModel::city_count
	int64_t ___city_count_10;
	// AllianceModel UserModel::alliance
	AllianceModel_t2995969982 * ___alliance_11;
	// MyCityModel UserModel::capital
	MyCityModel_t3961736920 * ___capital_12;
	// System.Int64 UserModel::shillings
	int64_t ___shillings_13;
	// System.String UserModel::language
	String_t* ___language_14;
	// System.String UserModel::flag
	String_t* ___flag_15;
	// System.Int64 UserModel::players_wins
	int64_t ___players_wins_16;
	// System.Int64 UserModel::players_losses
	int64_t ___players_losses_17;
	// System.Int64 UserModel::barbarians_wins
	int64_t ___barbarians_wins_18;
	// System.Int64 UserModel::barbarians_losses
	int64_t ___barbarians_losses_19;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___status_2)); }
	inline String_t* get_status_2() const { return ___status_2; }
	inline String_t** get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(String_t* value)
	{
		___status_2 = value;
		Il2CppCodeGenWriteBarrier((&___status_2), value);
	}

	inline static int32_t get_offset_of_image_name_3() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___image_name_3)); }
	inline String_t* get_image_name_3() const { return ___image_name_3; }
	inline String_t** get_address_of_image_name_3() { return &___image_name_3; }
	inline void set_image_name_3(String_t* value)
	{
		___image_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_name_3), value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___rank_4)); }
	inline String_t* get_rank_4() const { return ___rank_4; }
	inline String_t** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(String_t* value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier((&___rank_4), value);
	}

	inline static int32_t get_offset_of_emperor_id_5() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___emperor_id_5)); }
	inline int64_t get_emperor_id_5() const { return ___emperor_id_5; }
	inline int64_t* get_address_of_emperor_id_5() { return &___emperor_id_5; }
	inline void set_emperor_id_5(int64_t value)
	{
		___emperor_id_5 = value;
	}

	inline static int32_t get_offset_of_email_6() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___email_6)); }
	inline String_t* get_email_6() const { return ___email_6; }
	inline String_t** get_address_of_email_6() { return &___email_6; }
	inline void set_email_6(String_t* value)
	{
		___email_6 = value;
		Il2CppCodeGenWriteBarrier((&___email_6), value);
	}

	inline static int32_t get_offset_of_experience_7() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___experience_7)); }
	inline int64_t get_experience_7() const { return ___experience_7; }
	inline int64_t* get_address_of_experience_7() { return &___experience_7; }
	inline void set_experience_7(int64_t value)
	{
		___experience_7 = value;
	}

	inline static int32_t get_offset_of_alliance_rank_8() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___alliance_rank_8)); }
	inline String_t* get_alliance_rank_8() const { return ___alliance_rank_8; }
	inline String_t** get_address_of_alliance_rank_8() { return &___alliance_rank_8; }
	inline void set_alliance_rank_8(String_t* value)
	{
		___alliance_rank_8 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_rank_8), value);
	}

	inline static int32_t get_offset_of_last_login_9() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___last_login_9)); }
	inline DateTime_t3738529785  get_last_login_9() const { return ___last_login_9; }
	inline DateTime_t3738529785 * get_address_of_last_login_9() { return &___last_login_9; }
	inline void set_last_login_9(DateTime_t3738529785  value)
	{
		___last_login_9 = value;
	}

	inline static int32_t get_offset_of_city_count_10() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___city_count_10)); }
	inline int64_t get_city_count_10() const { return ___city_count_10; }
	inline int64_t* get_address_of_city_count_10() { return &___city_count_10; }
	inline void set_city_count_10(int64_t value)
	{
		___city_count_10 = value;
	}

	inline static int32_t get_offset_of_alliance_11() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___alliance_11)); }
	inline AllianceModel_t2995969982 * get_alliance_11() const { return ___alliance_11; }
	inline AllianceModel_t2995969982 ** get_address_of_alliance_11() { return &___alliance_11; }
	inline void set_alliance_11(AllianceModel_t2995969982 * value)
	{
		___alliance_11 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_11), value);
	}

	inline static int32_t get_offset_of_capital_12() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___capital_12)); }
	inline MyCityModel_t3961736920 * get_capital_12() const { return ___capital_12; }
	inline MyCityModel_t3961736920 ** get_address_of_capital_12() { return &___capital_12; }
	inline void set_capital_12(MyCityModel_t3961736920 * value)
	{
		___capital_12 = value;
		Il2CppCodeGenWriteBarrier((&___capital_12), value);
	}

	inline static int32_t get_offset_of_shillings_13() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___shillings_13)); }
	inline int64_t get_shillings_13() const { return ___shillings_13; }
	inline int64_t* get_address_of_shillings_13() { return &___shillings_13; }
	inline void set_shillings_13(int64_t value)
	{
		___shillings_13 = value;
	}

	inline static int32_t get_offset_of_language_14() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___language_14)); }
	inline String_t* get_language_14() const { return ___language_14; }
	inline String_t** get_address_of_language_14() { return &___language_14; }
	inline void set_language_14(String_t* value)
	{
		___language_14 = value;
		Il2CppCodeGenWriteBarrier((&___language_14), value);
	}

	inline static int32_t get_offset_of_flag_15() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___flag_15)); }
	inline String_t* get_flag_15() const { return ___flag_15; }
	inline String_t** get_address_of_flag_15() { return &___flag_15; }
	inline void set_flag_15(String_t* value)
	{
		___flag_15 = value;
		Il2CppCodeGenWriteBarrier((&___flag_15), value);
	}

	inline static int32_t get_offset_of_players_wins_16() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___players_wins_16)); }
	inline int64_t get_players_wins_16() const { return ___players_wins_16; }
	inline int64_t* get_address_of_players_wins_16() { return &___players_wins_16; }
	inline void set_players_wins_16(int64_t value)
	{
		___players_wins_16 = value;
	}

	inline static int32_t get_offset_of_players_losses_17() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___players_losses_17)); }
	inline int64_t get_players_losses_17() const { return ___players_losses_17; }
	inline int64_t* get_address_of_players_losses_17() { return &___players_losses_17; }
	inline void set_players_losses_17(int64_t value)
	{
		___players_losses_17 = value;
	}

	inline static int32_t get_offset_of_barbarians_wins_18() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___barbarians_wins_18)); }
	inline int64_t get_barbarians_wins_18() const { return ___barbarians_wins_18; }
	inline int64_t* get_address_of_barbarians_wins_18() { return &___barbarians_wins_18; }
	inline void set_barbarians_wins_18(int64_t value)
	{
		___barbarians_wins_18 = value;
	}

	inline static int32_t get_offset_of_barbarians_losses_19() { return static_cast<int32_t>(offsetof(UserModel_t1353931605, ___barbarians_losses_19)); }
	inline int64_t get_barbarians_losses_19() const { return ___barbarians_losses_19; }
	inline int64_t* get_address_of_barbarians_losses_19() { return &___barbarians_losses_19; }
	inline void set_barbarians_losses_19(int64_t value)
	{
		___barbarians_losses_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERMODEL_T1353931605_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RESIDENCEBUILDINGSTABLECONTROLLER_T4099106534_H
#define RESIDENCEBUILDINGSTABLECONTROLLER_T4099106534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceBuildingsTableController
struct  ResidenceBuildingsTableController_t4099106534  : public MonoBehaviour_t3962482529
{
public:
	// ResidenceBuildingsTableLine ResidenceBuildingsTableController::m_cellPrefab
	ResidenceBuildingsTableLine_t2264296562 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceBuildingsTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Boolean ResidenceBuildingsTableController::buildings
	bool ___buildings_4;
	// System.Collections.ArrayList ResidenceBuildingsTableController::buildingsList
	ArrayList_t2718874744 * ___buildingsList_5;
	// System.Int32 ResidenceBuildingsTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t4099106534, ___m_cellPrefab_2)); }
	inline ResidenceBuildingsTableLine_t2264296562 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceBuildingsTableLine_t2264296562 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceBuildingsTableLine_t2264296562 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t4099106534, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_buildings_4() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t4099106534, ___buildings_4)); }
	inline bool get_buildings_4() const { return ___buildings_4; }
	inline bool* get_address_of_buildings_4() { return &___buildings_4; }
	inline void set_buildings_4(bool value)
	{
		___buildings_4 = value;
	}

	inline static int32_t get_offset_of_buildingsList_5() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t4099106534, ___buildingsList_5)); }
	inline ArrayList_t2718874744 * get_buildingsList_5() const { return ___buildingsList_5; }
	inline ArrayList_t2718874744 ** get_address_of_buildingsList_5() { return &___buildingsList_5; }
	inline void set_buildingsList_5(ArrayList_t2718874744 * value)
	{
		___buildingsList_5 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsList_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableController_t4099106534, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCEBUILDINGSTABLECONTROLLER_T4099106534_H
#ifndef DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#define DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.DebuggableMonoBehaviour
struct  DebuggableMonoBehaviour_t3692909384  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGABLEMONOBEHAVIOUR_T3692909384_H
#ifndef RESIDENCECITIESTABLECONTROLLER_T4096198820_H
#define RESIDENCECITIESTABLECONTROLLER_T4096198820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceCitiesTableController
struct  ResidenceCitiesTableController_t4096198820  : public MonoBehaviour_t3962482529
{
public:
	// ResidenceCitiesTableLine ResidenceCitiesTableController::m_cellPrefab
	ResidenceCitiesTableLine_t3242262530 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceCitiesTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.ArrayList ResidenceCitiesTableController::citiesList
	ArrayList_t2718874744 * ___citiesList_4;
	// System.Int32 ResidenceCitiesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t4096198820, ___m_cellPrefab_2)); }
	inline ResidenceCitiesTableLine_t3242262530 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceCitiesTableLine_t3242262530 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceCitiesTableLine_t3242262530 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t4096198820, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_citiesList_4() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t4096198820, ___citiesList_4)); }
	inline ArrayList_t2718874744 * get_citiesList_4() const { return ___citiesList_4; }
	inline ArrayList_t2718874744 ** get_address_of_citiesList_4() { return &___citiesList_4; }
	inline void set_citiesList_4(ArrayList_t2718874744 * value)
	{
		___citiesList_4 = value;
		Il2CppCodeGenWriteBarrier((&___citiesList_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t4096198820, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCECITIESTABLECONTROLLER_T4096198820_H
#ifndef RESIDENCECOLONIESTABLECONTROLLER_T3519043985_H
#define RESIDENCECOLONIESTABLECONTROLLER_T3519043985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceColoniesTableController
struct  ResidenceColoniesTableController_t3519043985  : public MonoBehaviour_t3962482529
{
public:
	// ResidenceColoniesTableLine ResidenceColoniesTableController::m_cellPrefab
	ResidenceColoniesTableLine_t998651070 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceColoniesTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.ArrayList ResidenceColoniesTableController::citiesList
	ArrayList_t2718874744 * ___citiesList_4;
	// SimpleEvent ResidenceColoniesTableController::gotCityColonies
	SimpleEvent_t129249603 * ___gotCityColonies_5;
	// System.Int32 ResidenceColoniesTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableController_t3519043985, ___m_cellPrefab_2)); }
	inline ResidenceColoniesTableLine_t998651070 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceColoniesTableLine_t998651070 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceColoniesTableLine_t998651070 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableController_t3519043985, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_citiesList_4() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableController_t3519043985, ___citiesList_4)); }
	inline ArrayList_t2718874744 * get_citiesList_4() const { return ___citiesList_4; }
	inline ArrayList_t2718874744 ** get_address_of_citiesList_4() { return &___citiesList_4; }
	inline void set_citiesList_4(ArrayList_t2718874744 * value)
	{
		___citiesList_4 = value;
		Il2CppCodeGenWriteBarrier((&___citiesList_4), value);
	}

	inline static int32_t get_offset_of_gotCityColonies_5() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableController_t3519043985, ___gotCityColonies_5)); }
	inline SimpleEvent_t129249603 * get_gotCityColonies_5() const { return ___gotCityColonies_5; }
	inline SimpleEvent_t129249603 ** get_address_of_gotCityColonies_5() { return &___gotCityColonies_5; }
	inline void set_gotCityColonies_5(SimpleEvent_t129249603 * value)
	{
		___gotCityColonies_5 = value;
		Il2CppCodeGenWriteBarrier((&___gotCityColonies_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableController_t3519043985, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCECOLONIESTABLECONTROLLER_T3519043985_H
#ifndef RESIDENCEVALLEYSTABLECONTROLLER_T655612456_H
#define RESIDENCEVALLEYSTABLECONTROLLER_T655612456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceValleysTableController
struct  ResidenceValleysTableController_t655612456  : public MonoBehaviour_t3962482529
{
public:
	// ResidenceValleysTableLine ResidenceValleysTableController::m_cellPrefab
	ResidenceValleysTableLine_t1359679749 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceValleysTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.ArrayList ResidenceValleysTableController::buildingsList
	ArrayList_t2718874744 * ___buildingsList_4;
	// System.Int32 ResidenceValleysTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t655612456, ___m_cellPrefab_2)); }
	inline ResidenceValleysTableLine_t1359679749 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceValleysTableLine_t1359679749 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceValleysTableLine_t1359679749 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t655612456, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_buildingsList_4() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t655612456, ___buildingsList_4)); }
	inline ArrayList_t2718874744 * get_buildingsList_4() const { return ___buildingsList_4; }
	inline ArrayList_t2718874744 ** get_address_of_buildingsList_4() { return &___buildingsList_4; }
	inline void set_buildingsList_4(ArrayList_t2718874744 * value)
	{
		___buildingsList_4 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsList_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(ResidenceValleysTableController_t655612456, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCEVALLEYSTABLECONTROLLER_T655612456_H
#ifndef CITYITEMLINE_T1380899866_H
#define CITYITEMLINE_T1380899866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityItemLine
struct  CityItemLine_t1380899866  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CityItemLine::cityImage
	Image_t2670269651 * ___cityImage_2;
	// UnityEngine.UI.Image CityItemLine::backGround
	Image_t2670269651 * ___backGround_3;
	// UnityEngine.UI.Text CityItemLine::coords
	Text_t1901882714 * ___coords_4;
	// System.Int64 CityItemLine::cityId
	int64_t ___cityId_5;
	// System.Boolean CityItemLine::currentCity
	bool ___currentCity_6;

public:
	inline static int32_t get_offset_of_cityImage_2() { return static_cast<int32_t>(offsetof(CityItemLine_t1380899866, ___cityImage_2)); }
	inline Image_t2670269651 * get_cityImage_2() const { return ___cityImage_2; }
	inline Image_t2670269651 ** get_address_of_cityImage_2() { return &___cityImage_2; }
	inline void set_cityImage_2(Image_t2670269651 * value)
	{
		___cityImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityImage_2), value);
	}

	inline static int32_t get_offset_of_backGround_3() { return static_cast<int32_t>(offsetof(CityItemLine_t1380899866, ___backGround_3)); }
	inline Image_t2670269651 * get_backGround_3() const { return ___backGround_3; }
	inline Image_t2670269651 ** get_address_of_backGround_3() { return &___backGround_3; }
	inline void set_backGround_3(Image_t2670269651 * value)
	{
		___backGround_3 = value;
		Il2CppCodeGenWriteBarrier((&___backGround_3), value);
	}

	inline static int32_t get_offset_of_coords_4() { return static_cast<int32_t>(offsetof(CityItemLine_t1380899866, ___coords_4)); }
	inline Text_t1901882714 * get_coords_4() const { return ___coords_4; }
	inline Text_t1901882714 ** get_address_of_coords_4() { return &___coords_4; }
	inline void set_coords_4(Text_t1901882714 * value)
	{
		___coords_4 = value;
		Il2CppCodeGenWriteBarrier((&___coords_4), value);
	}

	inline static int32_t get_offset_of_cityId_5() { return static_cast<int32_t>(offsetof(CityItemLine_t1380899866, ___cityId_5)); }
	inline int64_t get_cityId_5() const { return ___cityId_5; }
	inline int64_t* get_address_of_cityId_5() { return &___cityId_5; }
	inline void set_cityId_5(int64_t value)
	{
		___cityId_5 = value;
	}

	inline static int32_t get_offset_of_currentCity_6() { return static_cast<int32_t>(offsetof(CityItemLine_t1380899866, ___currentCity_6)); }
	inline bool get_currentCity_6() const { return ___currentCity_6; }
	inline bool* get_address_of_currentCity_6() { return &___currentCity_6; }
	inline void set_currentCity_6(bool value)
	{
		___currentCity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYITEMLINE_T1380899866_H
#ifndef DIPLOMACYTABLELINE_T1854275108_H
#define DIPLOMACYTABLELINE_T1854275108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DiplomacyTableLine
struct  DiplomacyTableLine_t1854275108  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DiplomacyTableLine::nickName
	Text_t1901882714 * ___nickName_2;
	// UnityEngine.UI.Text DiplomacyTableLine::knight
	Text_t1901882714 * ___knight_3;
	// UnityEngine.UI.Text DiplomacyTableLine::city
	Text_t1901882714 * ___city_4;
	// UnityEngine.UI.Text DiplomacyTableLine::coord
	Text_t1901882714 * ___coord_5;
	// UnityEngine.UI.Text DiplomacyTableLine::date
	Text_t1901882714 * ___date_6;

public:
	inline static int32_t get_offset_of_nickName_2() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1854275108, ___nickName_2)); }
	inline Text_t1901882714 * get_nickName_2() const { return ___nickName_2; }
	inline Text_t1901882714 ** get_address_of_nickName_2() { return &___nickName_2; }
	inline void set_nickName_2(Text_t1901882714 * value)
	{
		___nickName_2 = value;
		Il2CppCodeGenWriteBarrier((&___nickName_2), value);
	}

	inline static int32_t get_offset_of_knight_3() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1854275108, ___knight_3)); }
	inline Text_t1901882714 * get_knight_3() const { return ___knight_3; }
	inline Text_t1901882714 ** get_address_of_knight_3() { return &___knight_3; }
	inline void set_knight_3(Text_t1901882714 * value)
	{
		___knight_3 = value;
		Il2CppCodeGenWriteBarrier((&___knight_3), value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1854275108, ___city_4)); }
	inline Text_t1901882714 * get_city_4() const { return ___city_4; }
	inline Text_t1901882714 ** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(Text_t1901882714 * value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier((&___city_4), value);
	}

	inline static int32_t get_offset_of_coord_5() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1854275108, ___coord_5)); }
	inline Text_t1901882714 * get_coord_5() const { return ___coord_5; }
	inline Text_t1901882714 ** get_address_of_coord_5() { return &___coord_5; }
	inline void set_coord_5(Text_t1901882714 * value)
	{
		___coord_5 = value;
		Il2CppCodeGenWriteBarrier((&___coord_5), value);
	}

	inline static int32_t get_offset_of_date_6() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1854275108, ___date_6)); }
	inline Text_t1901882714 * get_date_6() const { return ___date_6; }
	inline Text_t1901882714 ** get_address_of_date_6() { return &___date_6; }
	inline void set_date_6(Text_t1901882714 * value)
	{
		___date_6 = value;
		Il2CppCodeGenWriteBarrier((&___date_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIPLOMACYTABLELINE_T1854275108_H
#ifndef TESTWORLDCHUNKSMANAGER_T1200850834_H
#define TESTWORLDCHUNKSMANAGER_T1200850834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestWorldChunksManager
struct  TestWorldChunksManager_t1200850834  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[0...,0...] TestWorldChunksManager::worldChunks
	GameObjectU5B0___U2C0___U5D_t3328599147* ___worldChunks_2;
	// System.Int64 TestWorldChunksManager::targetX
	int64_t ___targetX_3;
	// System.Int64 TestWorldChunksManager::targetY
	int64_t ___targetY_4;

public:
	inline static int32_t get_offset_of_worldChunks_2() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___worldChunks_2)); }
	inline GameObjectU5B0___U2C0___U5D_t3328599147* get_worldChunks_2() const { return ___worldChunks_2; }
	inline GameObjectU5B0___U2C0___U5D_t3328599147** get_address_of_worldChunks_2() { return &___worldChunks_2; }
	inline void set_worldChunks_2(GameObjectU5B0___U2C0___U5D_t3328599147* value)
	{
		___worldChunks_2 = value;
		Il2CppCodeGenWriteBarrier((&___worldChunks_2), value);
	}

	inline static int32_t get_offset_of_targetX_3() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___targetX_3)); }
	inline int64_t get_targetX_3() const { return ___targetX_3; }
	inline int64_t* get_address_of_targetX_3() { return &___targetX_3; }
	inline void set_targetX_3(int64_t value)
	{
		___targetX_3 = value;
	}

	inline static int32_t get_offset_of_targetY_4() { return static_cast<int32_t>(offsetof(TestWorldChunksManager_t1200850834, ___targetY_4)); }
	inline int64_t get_targetY_4() const { return ___targetY_4; }
	inline int64_t* get_address_of_targetY_4() { return &___targetY_4; }
	inline void set_targetY_4(int64_t value)
	{
		___targetY_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTWORLDCHUNKSMANAGER_T1200850834_H
#ifndef UPGRADEBUILDINGTABLECONTROLLER_T2757797628_H
#define UPGRADEBUILDINGTABLECONTROLLER_T2757797628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeBuildingTableController
struct  UpgradeBuildingTableController_t2757797628  : public MonoBehaviour_t3962482529
{
public:
	// UpgradeBuildingTableLine UpgradeBuildingTableController::m_cellPrefab
	UpgradeBuildingTableLine_t2892907218 * ___m_cellPrefab_2;
	// Tacticsoft.TableView UpgradeBuildingTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.String UpgradeBuildingTableController::content
	String_t* ___content_4;
	// System.Collections.ArrayList UpgradeBuildingTableController::buildingsList
	ArrayList_t2718874744 * ___buildingsList_5;
	// System.Int64 UpgradeBuildingTableController::item_id
	int64_t ___item_id_6;
	// IReloadable UpgradeBuildingTableController::owner
	RuntimeObject* ___owner_7;
	// System.Int64 UpgradeBuildingTableController::building_id
	int64_t ___building_id_8;
	// System.Int32 UpgradeBuildingTableController::m_numRows
	int32_t ___m_numRows_9;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___m_cellPrefab_2)); }
	inline UpgradeBuildingTableLine_t2892907218 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline UpgradeBuildingTableLine_t2892907218 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(UpgradeBuildingTableLine_t2892907218 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___content_4)); }
	inline String_t* get_content_4() const { return ___content_4; }
	inline String_t** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(String_t* value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_4), value);
	}

	inline static int32_t get_offset_of_buildingsList_5() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___buildingsList_5)); }
	inline ArrayList_t2718874744 * get_buildingsList_5() const { return ___buildingsList_5; }
	inline ArrayList_t2718874744 ** get_address_of_buildingsList_5() { return &___buildingsList_5; }
	inline void set_buildingsList_5(ArrayList_t2718874744 * value)
	{
		___buildingsList_5 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsList_5), value);
	}

	inline static int32_t get_offset_of_item_id_6() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___item_id_6)); }
	inline int64_t get_item_id_6() const { return ___item_id_6; }
	inline int64_t* get_address_of_item_id_6() { return &___item_id_6; }
	inline void set_item_id_6(int64_t value)
	{
		___item_id_6 = value;
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___owner_7)); }
	inline RuntimeObject* get_owner_7() const { return ___owner_7; }
	inline RuntimeObject** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(RuntimeObject* value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier((&___owner_7), value);
	}

	inline static int32_t get_offset_of_building_id_8() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___building_id_8)); }
	inline int64_t get_building_id_8() const { return ___building_id_8; }
	inline int64_t* get_address_of_building_id_8() { return &___building_id_8; }
	inline void set_building_id_8(int64_t value)
	{
		___building_id_8 = value;
	}

	inline static int32_t get_offset_of_m_numRows_9() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t2757797628, ___m_numRows_9)); }
	inline int32_t get_m_numRows_9() const { return ___m_numRows_9; }
	inline int32_t* get_address_of_m_numRows_9() { return &___m_numRows_9; }
	inline void set_m_numRows_9(int32_t value)
	{
		___m_numRows_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPGRADEBUILDINGTABLECONTROLLER_T2757797628_H
#ifndef MARKETBUYTABLECONTROLLER_T892139868_H
#define MARKETBUYTABLECONTROLLER_T892139868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketBuyTableController
struct  MarketBuyTableController_t892139868  : public MonoBehaviour_t3962482529
{
public:
	// MarketManager MarketBuyTableController::owner
	MarketManager_t880414981 * ___owner_2;
	// MarketBuyTableLine MarketBuyTableController::m_cellPrefab
	MarketBuyTableLine_t264875335 * ___m_cellPrefab_3;
	// Tacticsoft.TableView MarketBuyTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_4;
	// System.Collections.ArrayList MarketBuyTableController::lots
	ArrayList_t2718874744 * ___lots_5;
	// System.Int32 MarketBuyTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t892139868, ___owner_2)); }
	inline MarketManager_t880414981 * get_owner_2() const { return ___owner_2; }
	inline MarketManager_t880414981 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(MarketManager_t880414981 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier((&___owner_2), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t892139868, ___m_cellPrefab_3)); }
	inline MarketBuyTableLine_t264875335 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline MarketBuyTableLine_t264875335 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(MarketBuyTableLine_t264875335 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_3), value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t892139868, ___m_tableView_4)); }
	inline TableView_t4228429533 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t4228429533 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_4), value);
	}

	inline static int32_t get_offset_of_lots_5() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t892139868, ___lots_5)); }
	inline ArrayList_t2718874744 * get_lots_5() const { return ___lots_5; }
	inline ArrayList_t2718874744 ** get_address_of_lots_5() { return &___lots_5; }
	inline void set_lots_5(ArrayList_t2718874744 * value)
	{
		___lots_5 = value;
		Il2CppCodeGenWriteBarrier((&___lots_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(MarketBuyTableController_t892139868, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETBUYTABLECONTROLLER_T892139868_H
#ifndef TABLEVIEWCELL_T777944023_H
#define TABLEVIEWCELL_T777944023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableViewCell
struct  TableViewCell_t777944023  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEVIEWCELL_T777944023_H
#ifndef PANELTABLECONTROLLER_T3691279756_H
#define PANELTABLECONTROLLER_T3691279756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTableController
struct  PanelTableController_t3691279756  : public MonoBehaviour_t3962482529
{
public:
	// PanelTableLine PanelTableController::m_cellPrefab
	PanelTableLine_t11631953 * ___m_cellPrefab_2;
	// Tacticsoft.TableView PanelTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.ArrayList PanelTableController::actionList
	ArrayList_t2718874744 * ___actionList_4;
	// System.String PanelTableController::content
	String_t* ___content_5;
	// System.Int32 PanelTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(PanelTableController_t3691279756, ___m_cellPrefab_2)); }
	inline PanelTableLine_t11631953 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline PanelTableLine_t11631953 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(PanelTableLine_t11631953 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(PanelTableController_t3691279756, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_actionList_4() { return static_cast<int32_t>(offsetof(PanelTableController_t3691279756, ___actionList_4)); }
	inline ArrayList_t2718874744 * get_actionList_4() const { return ___actionList_4; }
	inline ArrayList_t2718874744 ** get_address_of_actionList_4() { return &___actionList_4; }
	inline void set_actionList_4(ArrayList_t2718874744 * value)
	{
		___actionList_4 = value;
		Il2CppCodeGenWriteBarrier((&___actionList_4), value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(PanelTableController_t3691279756, ___content_5)); }
	inline String_t* get_content_5() const { return ___content_5; }
	inline String_t** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(String_t* value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier((&___content_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(PanelTableController_t3691279756, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELTABLECONTROLLER_T3691279756_H
#ifndef SOUNDMANAGER_T2102329059_H
#define SOUNDMANAGER_T2102329059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2102329059  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource SoundManager::music
	AudioSource_t3935305588 * ___music_3;

public:
	inline static int32_t get_offset_of_music_3() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___music_3)); }
	inline AudioSource_t3935305588 * get_music_3() const { return ___music_3; }
	inline AudioSource_t3935305588 ** get_address_of_music_3() { return &___music_3; }
	inline void set_music_3(AudioSource_t3935305588 * value)
	{
		___music_3 = value;
		Il2CppCodeGenWriteBarrier((&___music_3), value);
	}
};

struct SoundManager_t2102329059_StaticFields
{
public:
	// SoundManager SoundManager::instance
	SoundManager_t2102329059 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059_StaticFields, ___instance_2)); }
	inline SoundManager_t2102329059 * get_instance_2() const { return ___instance_2; }
	inline SoundManager_t2102329059 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SoundManager_t2102329059 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T2102329059_H
#ifndef ALLIANCEALLIANCESTABLECONTROLLER_T3866927190_H
#define ALLIANCEALLIANCESTABLECONTROLLER_T3866927190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceAlliancesTableController
struct  AllianceAlliancesTableController_t3866927190  : public MonoBehaviour_t3962482529
{
public:
	// AllianceAlliancesLine AllianceAlliancesTableController::m_cellPrefab
	AllianceAlliancesLine_t2112943226 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceAlliancesTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// AllianceModel[] AllianceAlliancesTableController::alliances
	AllianceModelU5BU5D_t4240941963* ___alliances_4;
	// System.Int32 AllianceAlliancesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t3866927190, ___m_cellPrefab_2)); }
	inline AllianceAlliancesLine_t2112943226 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceAlliancesLine_t2112943226 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceAlliancesLine_t2112943226 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t3866927190, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_alliances_4() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t3866927190, ___alliances_4)); }
	inline AllianceModelU5BU5D_t4240941963* get_alliances_4() const { return ___alliances_4; }
	inline AllianceModelU5BU5D_t4240941963** get_address_of_alliances_4() { return &___alliances_4; }
	inline void set_alliances_4(AllianceModelU5BU5D_t4240941963* value)
	{
		___alliances_4 = value;
		Il2CppCodeGenWriteBarrier((&___alliances_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceAlliancesTableController_t3866927190, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEALLIANCESTABLECONTROLLER_T3866927190_H
#ifndef ALLIANCEEVENTSTABLECONTROLLER_T2478949448_H
#define ALLIANCEEVENTSTABLECONTROLLER_T2478949448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventsTableController
struct  AllianceEventsTableController_t2478949448  : public MonoBehaviour_t3962482529
{
public:
	// AllianceEventsLine AllianceEventsTableController::m_cellPrefab
	AllianceEventsLine_t1491641488 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceEventsTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Int32 AllianceEventsTableController::m_numRows
	int32_t ___m_numRows_4;
	// System.Collections.ArrayList AllianceEventsTableController::allianceEvents
	ArrayList_t2718874744 * ___allianceEvents_5;
	// SimpleEvent AllianceEventsTableController::onGetAllianceEvents
	SimpleEvent_t129249603 * ___onGetAllianceEvents_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t2478949448, ___m_cellPrefab_2)); }
	inline AllianceEventsLine_t1491641488 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceEventsLine_t1491641488 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceEventsLine_t1491641488 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t2478949448, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t2478949448, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_allianceEvents_5() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t2478949448, ___allianceEvents_5)); }
	inline ArrayList_t2718874744 * get_allianceEvents_5() const { return ___allianceEvents_5; }
	inline ArrayList_t2718874744 ** get_address_of_allianceEvents_5() { return &___allianceEvents_5; }
	inline void set_allianceEvents_5(ArrayList_t2718874744 * value)
	{
		___allianceEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___allianceEvents_5), value);
	}

	inline static int32_t get_offset_of_onGetAllianceEvents_6() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t2478949448, ___onGetAllianceEvents_6)); }
	inline SimpleEvent_t129249603 * get_onGetAllianceEvents_6() const { return ___onGetAllianceEvents_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAllianceEvents_6() { return &___onGetAllianceEvents_6; }
	inline void set_onGetAllianceEvents_6(SimpleEvent_t129249603 * value)
	{
		___onGetAllianceEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAllianceEvents_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEEVENTSTABLECONTROLLER_T2478949448_H
#ifndef GUESTFEASTINGTABLECONTROLLER_T3137115560_H
#define GUESTFEASTINGTABLECONTROLLER_T3137115560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuestFeastingTableController
struct  GuestFeastingTableController_t3137115560  : public MonoBehaviour_t3962482529
{
public:
	// GuestHouseTableLine GuestFeastingTableController::m_cellPrefab
	GuestHouseTableLine_t228100067 * ___m_cellPrefab_2;
	// Tacticsoft.TableView GuestFeastingTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Boolean GuestFeastingTableController::guest
	bool ___guest_4;
	// System.Collections.Generic.List`1<ArmyGeneralModel> GuestFeastingTableController::generalsContainer
	List_1_t2209824963 * ___generalsContainer_5;
	// ArmyGeneralModel GuestFeastingTableController::requestedGeneral
	ArmyGeneralModel_t737750221 * ___requestedGeneral_6;
	// System.Int32 GuestFeastingTableController::displayedHeroesCount
	int32_t ___displayedHeroesCount_7;
	// ConfirmationEvent GuestFeastingTableController::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_8;
	// System.Int32 GuestFeastingTableController::m_numRows
	int32_t ___m_numRows_9;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___m_cellPrefab_2)); }
	inline GuestHouseTableLine_t228100067 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline GuestHouseTableLine_t228100067 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(GuestHouseTableLine_t228100067 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_guest_4() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___guest_4)); }
	inline bool get_guest_4() const { return ___guest_4; }
	inline bool* get_address_of_guest_4() { return &___guest_4; }
	inline void set_guest_4(bool value)
	{
		___guest_4 = value;
	}

	inline static int32_t get_offset_of_generalsContainer_5() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___generalsContainer_5)); }
	inline List_1_t2209824963 * get_generalsContainer_5() const { return ___generalsContainer_5; }
	inline List_1_t2209824963 ** get_address_of_generalsContainer_5() { return &___generalsContainer_5; }
	inline void set_generalsContainer_5(List_1_t2209824963 * value)
	{
		___generalsContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___generalsContainer_5), value);
	}

	inline static int32_t get_offset_of_requestedGeneral_6() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___requestedGeneral_6)); }
	inline ArmyGeneralModel_t737750221 * get_requestedGeneral_6() const { return ___requestedGeneral_6; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_requestedGeneral_6() { return &___requestedGeneral_6; }
	inline void set_requestedGeneral_6(ArmyGeneralModel_t737750221 * value)
	{
		___requestedGeneral_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestedGeneral_6), value);
	}

	inline static int32_t get_offset_of_displayedHeroesCount_7() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___displayedHeroesCount_7)); }
	inline int32_t get_displayedHeroesCount_7() const { return ___displayedHeroesCount_7; }
	inline int32_t* get_address_of_displayedHeroesCount_7() { return &___displayedHeroesCount_7; }
	inline void set_displayedHeroesCount_7(int32_t value)
	{
		___displayedHeroesCount_7 = value;
	}

	inline static int32_t get_offset_of_confirmed_8() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___confirmed_8)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_8() const { return ___confirmed_8; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_8() { return &___confirmed_8; }
	inline void set_confirmed_8(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_8 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_8), value);
	}

	inline static int32_t get_offset_of_m_numRows_9() { return static_cast<int32_t>(offsetof(GuestFeastingTableController_t3137115560, ___m_numRows_9)); }
	inline int32_t get_m_numRows_9() const { return ___m_numRows_9; }
	inline int32_t* get_address_of_m_numRows_9() { return &___m_numRows_9; }
	inline void set_m_numRows_9(int32_t value)
	{
		___m_numRows_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUESTFEASTINGTABLECONTROLLER_T3137115560_H
#ifndef SPEEDUPTABLECONTROLLER_T3709478523_H
#define SPEEDUPTABLECONTROLLER_T3709478523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedUpTableController
struct  SpeedUpTableController_t3709478523  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.ArrayList SpeedUpTableController::currentSourceArray
	ArrayList_t2718874744 * ___currentSourceArray_2;
	// ShopTableCell SpeedUpTableController::m_cellPrefab
	ShopTableCell_t1369591435 * ___m_cellPrefab_3;
	// Tacticsoft.TableView SpeedUpTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_4;
	// UnityEngine.UI.Image SpeedUpTableController::itemImage
	Image_t2670269651 * ___itemImage_5;
	// UnityEngine.UI.Text SpeedUpTableController::timeLeft
	Text_t1901882714 * ___timeLeft_6;
	// System.Int64 SpeedUpTableController::eventId
	int64_t ___eventId_7;
	// System.DateTime SpeedUpTableController::startTime
	DateTime_t3738529785  ___startTime_8;
	// System.DateTime SpeedUpTableController::finishTime
	DateTime_t3738529785  ___finishTime_9;
	// PanelTableController SpeedUpTableController::owner
	PanelTableController_t3691279756 * ___owner_10;
	// System.TimeSpan SpeedUpTableController::timespan
	TimeSpan_t881159249  ___timespan_11;

public:
	inline static int32_t get_offset_of_currentSourceArray_2() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___currentSourceArray_2)); }
	inline ArrayList_t2718874744 * get_currentSourceArray_2() const { return ___currentSourceArray_2; }
	inline ArrayList_t2718874744 ** get_address_of_currentSourceArray_2() { return &___currentSourceArray_2; }
	inline void set_currentSourceArray_2(ArrayList_t2718874744 * value)
	{
		___currentSourceArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentSourceArray_2), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___m_cellPrefab_3)); }
	inline ShopTableCell_t1369591435 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline ShopTableCell_t1369591435 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(ShopTableCell_t1369591435 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_3), value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___m_tableView_4)); }
	inline TableView_t4228429533 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t4228429533 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_4), value);
	}

	inline static int32_t get_offset_of_itemImage_5() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___itemImage_5)); }
	inline Image_t2670269651 * get_itemImage_5() const { return ___itemImage_5; }
	inline Image_t2670269651 ** get_address_of_itemImage_5() { return &___itemImage_5; }
	inline void set_itemImage_5(Image_t2670269651 * value)
	{
		___itemImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage_5), value);
	}

	inline static int32_t get_offset_of_timeLeft_6() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___timeLeft_6)); }
	inline Text_t1901882714 * get_timeLeft_6() const { return ___timeLeft_6; }
	inline Text_t1901882714 ** get_address_of_timeLeft_6() { return &___timeLeft_6; }
	inline void set_timeLeft_6(Text_t1901882714 * value)
	{
		___timeLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeLeft_6), value);
	}

	inline static int32_t get_offset_of_eventId_7() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___eventId_7)); }
	inline int64_t get_eventId_7() const { return ___eventId_7; }
	inline int64_t* get_address_of_eventId_7() { return &___eventId_7; }
	inline void set_eventId_7(int64_t value)
	{
		___eventId_7 = value;
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___startTime_8)); }
	inline DateTime_t3738529785  get_startTime_8() const { return ___startTime_8; }
	inline DateTime_t3738529785 * get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(DateTime_t3738529785  value)
	{
		___startTime_8 = value;
	}

	inline static int32_t get_offset_of_finishTime_9() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___finishTime_9)); }
	inline DateTime_t3738529785  get_finishTime_9() const { return ___finishTime_9; }
	inline DateTime_t3738529785 * get_address_of_finishTime_9() { return &___finishTime_9; }
	inline void set_finishTime_9(DateTime_t3738529785  value)
	{
		___finishTime_9 = value;
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___owner_10)); }
	inline PanelTableController_t3691279756 * get_owner_10() const { return ___owner_10; }
	inline PanelTableController_t3691279756 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(PanelTableController_t3691279756 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier((&___owner_10), value);
	}

	inline static int32_t get_offset_of_timespan_11() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t3709478523, ___timespan_11)); }
	inline TimeSpan_t881159249  get_timespan_11() const { return ___timespan_11; }
	inline TimeSpan_t881159249 * get_address_of_timespan_11() { return &___timespan_11; }
	inline void set_timespan_11(TimeSpan_t881159249  value)
	{
		___timespan_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDUPTABLECONTROLLER_T3709478523_H
#ifndef FINANCETABLECONTROLLER_T3435833683_H
#define FINANCETABLECONTROLLER_T3435833683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinanceTableController
struct  FinanceTableController_t3435833683  : public MonoBehaviour_t3962482529
{
public:
	// FinancialContentChanger FinanceTableController::owner
	FinancialContentChanger_t4019109810 * ___owner_2;
	// System.Collections.ArrayList FinanceTableController::currentSourceArray
	ArrayList_t2718874744 * ___currentSourceArray_3;
	// FinancialTableLine FinanceTableController::m_cellPrefab
	FinancialTableLine_t3445994354 * ___m_cellPrefab_4;
	// Tacticsoft.TableView FinanceTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_5;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(FinanceTableController_t3435833683, ___owner_2)); }
	inline FinancialContentChanger_t4019109810 * get_owner_2() const { return ___owner_2; }
	inline FinancialContentChanger_t4019109810 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(FinancialContentChanger_t4019109810 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier((&___owner_2), value);
	}

	inline static int32_t get_offset_of_currentSourceArray_3() { return static_cast<int32_t>(offsetof(FinanceTableController_t3435833683, ___currentSourceArray_3)); }
	inline ArrayList_t2718874744 * get_currentSourceArray_3() const { return ___currentSourceArray_3; }
	inline ArrayList_t2718874744 ** get_address_of_currentSourceArray_3() { return &___currentSourceArray_3; }
	inline void set_currentSourceArray_3(ArrayList_t2718874744 * value)
	{
		___currentSourceArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentSourceArray_3), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_4() { return static_cast<int32_t>(offsetof(FinanceTableController_t3435833683, ___m_cellPrefab_4)); }
	inline FinancialTableLine_t3445994354 * get_m_cellPrefab_4() const { return ___m_cellPrefab_4; }
	inline FinancialTableLine_t3445994354 ** get_address_of_m_cellPrefab_4() { return &___m_cellPrefab_4; }
	inline void set_m_cellPrefab_4(FinancialTableLine_t3445994354 * value)
	{
		___m_cellPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_4), value);
	}

	inline static int32_t get_offset_of_m_tableView_5() { return static_cast<int32_t>(offsetof(FinanceTableController_t3435833683, ___m_tableView_5)); }
	inline TableView_t4228429533 * get_m_tableView_5() const { return ___m_tableView_5; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_5() { return &___m_tableView_5; }
	inline void set_m_tableView_5(TableView_t4228429533 * value)
	{
		___m_tableView_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINANCETABLECONTROLLER_T3435833683_H
#ifndef ALLIANCEINFOTABLECONTOLLER_T4165916044_H
#define ALLIANCEINFOTABLECONTOLLER_T4165916044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInfoTableContoller
struct  AllianceInfoTableContoller_t4165916044  : public MonoBehaviour_t3962482529
{
public:
	// AllianceInfoAlliancesLine AllianceInfoTableContoller::m_cellPrefab
	AllianceInfoAlliancesLine_t1537220308 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceInfoTableContoller::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Boolean AllianceInfoTableContoller::neutral
	bool ___neutral_4;
	// System.Boolean AllianceInfoTableContoller::ally
	bool ___ally_5;
	// System.Boolean AllianceInfoTableContoller::enemy
	bool ___enemy_6;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::neutralAlliances
	List_1_t173077428 * ___neutralAlliances_7;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::allyAlliances
	List_1_t173077428 * ___allyAlliances_8;
	// System.Collections.Generic.List`1<AllianceModel> AllianceInfoTableContoller::enemyAlliances
	List_1_t173077428 * ___enemyAlliances_9;
	// System.Int32 AllianceInfoTableContoller::m_numRows
	int32_t ___m_numRows_10;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___m_cellPrefab_2)); }
	inline AllianceInfoAlliancesLine_t1537220308 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceInfoAlliancesLine_t1537220308 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceInfoAlliancesLine_t1537220308 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_neutral_4() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___neutral_4)); }
	inline bool get_neutral_4() const { return ___neutral_4; }
	inline bool* get_address_of_neutral_4() { return &___neutral_4; }
	inline void set_neutral_4(bool value)
	{
		___neutral_4 = value;
	}

	inline static int32_t get_offset_of_ally_5() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___ally_5)); }
	inline bool get_ally_5() const { return ___ally_5; }
	inline bool* get_address_of_ally_5() { return &___ally_5; }
	inline void set_ally_5(bool value)
	{
		___ally_5 = value;
	}

	inline static int32_t get_offset_of_enemy_6() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___enemy_6)); }
	inline bool get_enemy_6() const { return ___enemy_6; }
	inline bool* get_address_of_enemy_6() { return &___enemy_6; }
	inline void set_enemy_6(bool value)
	{
		___enemy_6 = value;
	}

	inline static int32_t get_offset_of_neutralAlliances_7() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___neutralAlliances_7)); }
	inline List_1_t173077428 * get_neutralAlliances_7() const { return ___neutralAlliances_7; }
	inline List_1_t173077428 ** get_address_of_neutralAlliances_7() { return &___neutralAlliances_7; }
	inline void set_neutralAlliances_7(List_1_t173077428 * value)
	{
		___neutralAlliances_7 = value;
		Il2CppCodeGenWriteBarrier((&___neutralAlliances_7), value);
	}

	inline static int32_t get_offset_of_allyAlliances_8() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___allyAlliances_8)); }
	inline List_1_t173077428 * get_allyAlliances_8() const { return ___allyAlliances_8; }
	inline List_1_t173077428 ** get_address_of_allyAlliances_8() { return &___allyAlliances_8; }
	inline void set_allyAlliances_8(List_1_t173077428 * value)
	{
		___allyAlliances_8 = value;
		Il2CppCodeGenWriteBarrier((&___allyAlliances_8), value);
	}

	inline static int32_t get_offset_of_enemyAlliances_9() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___enemyAlliances_9)); }
	inline List_1_t173077428 * get_enemyAlliances_9() const { return ___enemyAlliances_9; }
	inline List_1_t173077428 ** get_address_of_enemyAlliances_9() { return &___enemyAlliances_9; }
	inline void set_enemyAlliances_9(List_1_t173077428 * value)
	{
		___enemyAlliances_9 = value;
		Il2CppCodeGenWriteBarrier((&___enemyAlliances_9), value);
	}

	inline static int32_t get_offset_of_m_numRows_10() { return static_cast<int32_t>(offsetof(AllianceInfoTableContoller_t4165916044, ___m_numRows_10)); }
	inline int32_t get_m_numRows_10() const { return ___m_numRows_10; }
	inline int32_t* get_address_of_m_numRows_10() { return &___m_numRows_10; }
	inline void set_m_numRows_10(int32_t value)
	{
		___m_numRows_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEINFOTABLECONTOLLER_T4165916044_H
#ifndef BATTLEREPORTRESOURCESTABLECONTROLLER_T1929107072_H
#define BATTLEREPORTRESOURCESTABLECONTROLLER_T1929107072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportResourcesTableController
struct  BattleReportResourcesTableController_t1929107072  : public MonoBehaviour_t3962482529
{
public:
	// BattleReportResourcesLine BattleReportResourcesTableController::m_cellPrefab
	BattleReportResourcesLine_t3745703875 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportResourcesTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.Generic.List`1<ResourcesModel> BattleReportResourcesTableController::armyResources
	List_1_t4005583255 * ___armyResources_4;
	// System.Int32 BattleReportResourcesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t1929107072, ___m_cellPrefab_2)); }
	inline BattleReportResourcesLine_t3745703875 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportResourcesLine_t3745703875 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportResourcesLine_t3745703875 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t1929107072, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_armyResources_4() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t1929107072, ___armyResources_4)); }
	inline List_1_t4005583255 * get_armyResources_4() const { return ___armyResources_4; }
	inline List_1_t4005583255 ** get_address_of_armyResources_4() { return &___armyResources_4; }
	inline void set_armyResources_4(List_1_t4005583255 * value)
	{
		___armyResources_4 = value;
		Il2CppCodeGenWriteBarrier((&___armyResources_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(BattleReportResourcesTableController_t1929107072, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTRESOURCESTABLECONTROLLER_T1929107072_H
#ifndef BATTLEREPORTUNITTABLECONTROLLER_T1840302685_H
#define BATTLEREPORTUNITTABLECONTROLLER_T1840302685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportUnitTableController
struct  BattleReportUnitTableController_t1840302685  : public MonoBehaviour_t3962482529
{
public:
	// BattleReportUnitsLine BattleReportUnitTableController::m_cellPrefab
	BattleReportUnitsLine_t2550456670 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportUnitTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.Generic.List`1<System.String> BattleReportUnitTableController::unitNames
	List_1_t3319525431 * ___unitNames_4;
	// System.Collections.Generic.List`1<System.Int64> BattleReportUnitTableController::unitBeforeCount
	List_1_t913674750 * ___unitBeforeCount_5;
	// System.Collections.Generic.List`1<System.Int64> BattleReportUnitTableController::unitAfterCount
	List_1_t913674750 * ___unitAfterCount_6;
	// System.Int32 BattleReportUnitTableController::m_numRows
	int32_t ___m_numRows_7;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___m_cellPrefab_2)); }
	inline BattleReportUnitsLine_t2550456670 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportUnitsLine_t2550456670 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportUnitsLine_t2550456670 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_unitNames_4() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___unitNames_4)); }
	inline List_1_t3319525431 * get_unitNames_4() const { return ___unitNames_4; }
	inline List_1_t3319525431 ** get_address_of_unitNames_4() { return &___unitNames_4; }
	inline void set_unitNames_4(List_1_t3319525431 * value)
	{
		___unitNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitNames_4), value);
	}

	inline static int32_t get_offset_of_unitBeforeCount_5() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___unitBeforeCount_5)); }
	inline List_1_t913674750 * get_unitBeforeCount_5() const { return ___unitBeforeCount_5; }
	inline List_1_t913674750 ** get_address_of_unitBeforeCount_5() { return &___unitBeforeCount_5; }
	inline void set_unitBeforeCount_5(List_1_t913674750 * value)
	{
		___unitBeforeCount_5 = value;
		Il2CppCodeGenWriteBarrier((&___unitBeforeCount_5), value);
	}

	inline static int32_t get_offset_of_unitAfterCount_6() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___unitAfterCount_6)); }
	inline List_1_t913674750 * get_unitAfterCount_6() const { return ___unitAfterCount_6; }
	inline List_1_t913674750 ** get_address_of_unitAfterCount_6() { return &___unitAfterCount_6; }
	inline void set_unitAfterCount_6(List_1_t913674750 * value)
	{
		___unitAfterCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___unitAfterCount_6), value);
	}

	inline static int32_t get_offset_of_m_numRows_7() { return static_cast<int32_t>(offsetof(BattleReportUnitTableController_t1840302685, ___m_numRows_7)); }
	inline int32_t get_m_numRows_7() const { return ___m_numRows_7; }
	inline int32_t* get_address_of_m_numRows_7() { return &___m_numRows_7; }
	inline void set_m_numRows_7(int32_t value)
	{
		___m_numRows_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTUNITTABLECONTROLLER_T1840302685_H
#ifndef COMMANDCENTERARMIESTABLECONTROLLER_T2609779151_H
#define COMMANDCENTERARMIESTABLECONTROLLER_T2609779151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterArmiesTableController
struct  CommandCenterArmiesTableController_t2609779151  : public MonoBehaviour_t3962482529
{
public:
	// CommandCenterArmyTableLine CommandCenterArmiesTableController::m_cellPrefab
	CommandCenterArmyTableLine_t1166067843 * ___m_cellPrefab_2;
	// Tacticsoft.TableView CommandCenterArmiesTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.Generic.List`1<ArmyModel> CommandCenterArmiesTableController::cityArmies
	List_1_t3540747847 * ___cityArmies_4;
	// System.Int32 CommandCenterArmiesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2609779151, ___m_cellPrefab_2)); }
	inline CommandCenterArmyTableLine_t1166067843 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline CommandCenterArmyTableLine_t1166067843 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(CommandCenterArmyTableLine_t1166067843 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2609779151, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_cityArmies_4() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2609779151, ___cityArmies_4)); }
	inline List_1_t3540747847 * get_cityArmies_4() const { return ___cityArmies_4; }
	inline List_1_t3540747847 ** get_address_of_cityArmies_4() { return &___cityArmies_4; }
	inline void set_cityArmies_4(List_1_t3540747847 * value)
	{
		___cityArmies_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityArmies_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2609779151, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDCENTERARMIESTABLECONTROLLER_T2609779151_H
#ifndef ALLIANCEINVITETABLECONTROLLER_T1458639565_H
#define ALLIANCEINVITETABLECONTROLLER_T1458639565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInviteTableController
struct  AllianceInviteTableController_t1458639565  : public MonoBehaviour_t3962482529
{
public:
	// AllianceInviteLine AllianceInviteTableController::m_cellPrefab
	AllianceInviteLine_t1691417839 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceInviteTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.Generic.List`1<UserModel> AllianceInviteTableController::allUsers
	List_1_t2826006347 * ___allUsers_4;
	// System.Int32 AllianceInviteTableController::m_numRows
	int32_t ___m_numRows_5;
	// SimpleEvent AllianceInviteTableController::onGetAllUsers
	SimpleEvent_t129249603 * ___onGetAllUsers_6;
	// System.String AllianceInviteTableController::dateFormat
	String_t* ___dateFormat_7;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___m_cellPrefab_2)); }
	inline AllianceInviteLine_t1691417839 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceInviteLine_t1691417839 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceInviteLine_t1691417839 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_allUsers_4() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___allUsers_4)); }
	inline List_1_t2826006347 * get_allUsers_4() const { return ___allUsers_4; }
	inline List_1_t2826006347 ** get_address_of_allUsers_4() { return &___allUsers_4; }
	inline void set_allUsers_4(List_1_t2826006347 * value)
	{
		___allUsers_4 = value;
		Il2CppCodeGenWriteBarrier((&___allUsers_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}

	inline static int32_t get_offset_of_onGetAllUsers_6() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___onGetAllUsers_6)); }
	inline SimpleEvent_t129249603 * get_onGetAllUsers_6() const { return ___onGetAllUsers_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAllUsers_6() { return &___onGetAllUsers_6; }
	inline void set_onGetAllUsers_6(SimpleEvent_t129249603 * value)
	{
		___onGetAllUsers_6 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAllUsers_6), value);
	}

	inline static int32_t get_offset_of_dateFormat_7() { return static_cast<int32_t>(offsetof(AllianceInviteTableController_t1458639565, ___dateFormat_7)); }
	inline String_t* get_dateFormat_7() const { return ___dateFormat_7; }
	inline String_t** get_address_of_dateFormat_7() { return &___dateFormat_7; }
	inline void set_dateFormat_7(String_t* value)
	{
		___dateFormat_7 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEINVITETABLECONTROLLER_T1458639565_H
#ifndef ALLIANCEMEMBERSPERMISIONSTABLECONTROLLER_T3675389298_H
#define ALLIANCEMEMBERSPERMISIONSTABLECONTROLLER_T3675389298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceMembersPermisionsTableController
struct  AllianceMembersPermisionsTableController_t3675389298  : public MonoBehaviour_t3962482529
{
public:
	// AllianceMembersPermissionsLine AllianceMembersPermisionsTableController::m_cellPrefab
	AllianceMembersPermissionsLine_t2063553011 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceMembersPermisionsTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// UserModel[] AllianceMembersPermisionsTableController::allianceMembers
	UserModelU5BU5D_t104655800* ___allianceMembers_4;
	// System.Int32 AllianceMembersPermisionsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t3675389298, ___m_cellPrefab_2)); }
	inline AllianceMembersPermissionsLine_t2063553011 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceMembersPermissionsLine_t2063553011 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceMembersPermissionsLine_t2063553011 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t3675389298, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_allianceMembers_4() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t3675389298, ___allianceMembers_4)); }
	inline UserModelU5BU5D_t104655800* get_allianceMembers_4() const { return ___allianceMembers_4; }
	inline UserModelU5BU5D_t104655800** get_address_of_allianceMembers_4() { return &___allianceMembers_4; }
	inline void set_allianceMembers_4(UserModelU5BU5D_t104655800* value)
	{
		___allianceMembers_4 = value;
		Il2CppCodeGenWriteBarrier((&___allianceMembers_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(AllianceMembersPermisionsTableController_t3675389298, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEMEMBERSPERMISIONSTABLECONTROLLER_T3675389298_H
#ifndef BATTLEREPORTITEMSTABLECONTROLLER_T14710115_H
#define BATTLEREPORTITEMSTABLECONTROLLER_T14710115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportItemsTableController
struct  BattleReportItemsTableController_t14710115  : public MonoBehaviour_t3962482529
{
public:
	// BattleReportItemsLine BattleReportItemsTableController::m_cellPrefab
	BattleReportItemsLine_t1544447758 * ___m_cellPrefab_2;
	// Tacticsoft.TableView BattleReportItemsTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportItemsTableController::items
	List_1_t3391946265 * ___items_4;
	// System.Int32 BattleReportItemsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t14710115, ___m_cellPrefab_2)); }
	inline BattleReportItemsLine_t1544447758 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline BattleReportItemsLine_t1544447758 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(BattleReportItemsLine_t1544447758 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t14710115, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_items_4() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t14710115, ___items_4)); }
	inline List_1_t3391946265 * get_items_4() const { return ___items_4; }
	inline List_1_t3391946265 ** get_address_of_items_4() { return &___items_4; }
	inline void set_items_4(List_1_t3391946265 * value)
	{
		___items_4 = value;
		Il2CppCodeGenWriteBarrier((&___items_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(BattleReportItemsTableController_t14710115, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTITEMSTABLECONTROLLER_T14710115_H
#ifndef SHOPTABLECONTROLLER_T1404998102_H
#define SHOPTABLECONTROLLER_T1404998102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableController
struct  ShopTableController_t1404998102  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.ArrayList ShopTableController::scriptShopCells
	ArrayList_t2718874744 * ___scriptShopCells_2;
	// System.Collections.ArrayList ShopTableController::speedUpShopCells
	ArrayList_t2718874744 * ___speedUpShopCells_3;
	// System.Collections.ArrayList ShopTableController::productionShopCells
	ArrayList_t2718874744 * ___productionShopCells_4;
	// System.Collections.ArrayList ShopTableController::diamondShopCells
	ArrayList_t2718874744 * ___diamondShopCells_5;
	// System.Collections.ArrayList ShopTableController::promotionsShopCells
	ArrayList_t2718874744 * ___promotionsShopCells_6;
	// System.Collections.ArrayList ShopTableController::armyShopCells
	ArrayList_t2718874744 * ___armyShopCells_7;
	// System.Collections.ArrayList ShopTableController::currentSourceArray
	ArrayList_t2718874744 * ___currentSourceArray_8;
	// System.Boolean ShopTableController::_shop
	bool ____shop_9;
	// System.Int64 ShopTableController::cityId
	int64_t ___cityId_10;
	// System.String ShopTableController::content
	String_t* ___content_11;
	// SmartLocalization.LanguageManager ShopTableController::langManager
	LanguageManager_t2767934455 * ___langManager_12;
	// SimpleEvent ShopTableController::onGetShopItems
	SimpleEvent_t129249603 * ___onGetShopItems_13;
	// ShopTableCell ShopTableController::m_cellPrefab
	ShopTableCell_t1369591435 * ___m_cellPrefab_14;
	// Tacticsoft.TableView ShopTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_15;
	// UnityEngine.UI.Text ShopTableController::Label
	Text_t1901882714 * ___Label_16;
	// UnityEngine.UI.Text ShopTableController::shillingsCount
	Text_t1901882714 * ___shillingsCount_17;
	// UnityEngine.UI.Image ShopTableController::icon
	Image_t2670269651 * ___icon_18;
	// UnityEngine.Sprite ShopTableController::tabSelectedTop
	Sprite_t280657092 * ___tabSelectedTop_19;
	// UnityEngine.Sprite ShopTableController::tabSelectedBottom
	Sprite_t280657092 * ___tabSelectedBottom_20;
	// UnityEngine.Sprite ShopTableController::tabUnselectedTop
	Sprite_t280657092 * ___tabUnselectedTop_21;
	// UnityEngine.Sprite ShopTableController::tabUnselectedBottom
	Sprite_t280657092 * ___tabUnselectedBottom_22;
	// UnityEngine.Sprite ShopTableController::shopIcon
	Sprite_t280657092 * ___shopIcon_23;
	// UnityEngine.Sprite ShopTableController::treasureIcon
	Sprite_t280657092 * ___treasureIcon_24;
	// UnityEngine.GameObject ShopTableController::buyMoreBtn
	GameObject_t1113636619 * ___buyMoreBtn_25;
	// UnityEngine.GameObject ShopTableController::buyMoreTxt
	GameObject_t1113636619 * ___buyMoreTxt_26;
	// UnityEngine.UI.Image[] ShopTableController::tabs
	ImageU5BU5D_t2439009922* ___tabs_27;

public:
	inline static int32_t get_offset_of_scriptShopCells_2() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___scriptShopCells_2)); }
	inline ArrayList_t2718874744 * get_scriptShopCells_2() const { return ___scriptShopCells_2; }
	inline ArrayList_t2718874744 ** get_address_of_scriptShopCells_2() { return &___scriptShopCells_2; }
	inline void set_scriptShopCells_2(ArrayList_t2718874744 * value)
	{
		___scriptShopCells_2 = value;
		Il2CppCodeGenWriteBarrier((&___scriptShopCells_2), value);
	}

	inline static int32_t get_offset_of_speedUpShopCells_3() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___speedUpShopCells_3)); }
	inline ArrayList_t2718874744 * get_speedUpShopCells_3() const { return ___speedUpShopCells_3; }
	inline ArrayList_t2718874744 ** get_address_of_speedUpShopCells_3() { return &___speedUpShopCells_3; }
	inline void set_speedUpShopCells_3(ArrayList_t2718874744 * value)
	{
		___speedUpShopCells_3 = value;
		Il2CppCodeGenWriteBarrier((&___speedUpShopCells_3), value);
	}

	inline static int32_t get_offset_of_productionShopCells_4() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___productionShopCells_4)); }
	inline ArrayList_t2718874744 * get_productionShopCells_4() const { return ___productionShopCells_4; }
	inline ArrayList_t2718874744 ** get_address_of_productionShopCells_4() { return &___productionShopCells_4; }
	inline void set_productionShopCells_4(ArrayList_t2718874744 * value)
	{
		___productionShopCells_4 = value;
		Il2CppCodeGenWriteBarrier((&___productionShopCells_4), value);
	}

	inline static int32_t get_offset_of_diamondShopCells_5() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___diamondShopCells_5)); }
	inline ArrayList_t2718874744 * get_diamondShopCells_5() const { return ___diamondShopCells_5; }
	inline ArrayList_t2718874744 ** get_address_of_diamondShopCells_5() { return &___diamondShopCells_5; }
	inline void set_diamondShopCells_5(ArrayList_t2718874744 * value)
	{
		___diamondShopCells_5 = value;
		Il2CppCodeGenWriteBarrier((&___diamondShopCells_5), value);
	}

	inline static int32_t get_offset_of_promotionsShopCells_6() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___promotionsShopCells_6)); }
	inline ArrayList_t2718874744 * get_promotionsShopCells_6() const { return ___promotionsShopCells_6; }
	inline ArrayList_t2718874744 ** get_address_of_promotionsShopCells_6() { return &___promotionsShopCells_6; }
	inline void set_promotionsShopCells_6(ArrayList_t2718874744 * value)
	{
		___promotionsShopCells_6 = value;
		Il2CppCodeGenWriteBarrier((&___promotionsShopCells_6), value);
	}

	inline static int32_t get_offset_of_armyShopCells_7() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___armyShopCells_7)); }
	inline ArrayList_t2718874744 * get_armyShopCells_7() const { return ___armyShopCells_7; }
	inline ArrayList_t2718874744 ** get_address_of_armyShopCells_7() { return &___armyShopCells_7; }
	inline void set_armyShopCells_7(ArrayList_t2718874744 * value)
	{
		___armyShopCells_7 = value;
		Il2CppCodeGenWriteBarrier((&___armyShopCells_7), value);
	}

	inline static int32_t get_offset_of_currentSourceArray_8() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___currentSourceArray_8)); }
	inline ArrayList_t2718874744 * get_currentSourceArray_8() const { return ___currentSourceArray_8; }
	inline ArrayList_t2718874744 ** get_address_of_currentSourceArray_8() { return &___currentSourceArray_8; }
	inline void set_currentSourceArray_8(ArrayList_t2718874744 * value)
	{
		___currentSourceArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentSourceArray_8), value);
	}

	inline static int32_t get_offset_of__shop_9() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ____shop_9)); }
	inline bool get__shop_9() const { return ____shop_9; }
	inline bool* get_address_of__shop_9() { return &____shop_9; }
	inline void set__shop_9(bool value)
	{
		____shop_9 = value;
	}

	inline static int32_t get_offset_of_cityId_10() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___cityId_10)); }
	inline int64_t get_cityId_10() const { return ___cityId_10; }
	inline int64_t* get_address_of_cityId_10() { return &___cityId_10; }
	inline void set_cityId_10(int64_t value)
	{
		___cityId_10 = value;
	}

	inline static int32_t get_offset_of_content_11() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___content_11)); }
	inline String_t* get_content_11() const { return ___content_11; }
	inline String_t** get_address_of_content_11() { return &___content_11; }
	inline void set_content_11(String_t* value)
	{
		___content_11 = value;
		Il2CppCodeGenWriteBarrier((&___content_11), value);
	}

	inline static int32_t get_offset_of_langManager_12() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___langManager_12)); }
	inline LanguageManager_t2767934455 * get_langManager_12() const { return ___langManager_12; }
	inline LanguageManager_t2767934455 ** get_address_of_langManager_12() { return &___langManager_12; }
	inline void set_langManager_12(LanguageManager_t2767934455 * value)
	{
		___langManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___langManager_12), value);
	}

	inline static int32_t get_offset_of_onGetShopItems_13() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___onGetShopItems_13)); }
	inline SimpleEvent_t129249603 * get_onGetShopItems_13() const { return ___onGetShopItems_13; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetShopItems_13() { return &___onGetShopItems_13; }
	inline void set_onGetShopItems_13(SimpleEvent_t129249603 * value)
	{
		___onGetShopItems_13 = value;
		Il2CppCodeGenWriteBarrier((&___onGetShopItems_13), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_14() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___m_cellPrefab_14)); }
	inline ShopTableCell_t1369591435 * get_m_cellPrefab_14() const { return ___m_cellPrefab_14; }
	inline ShopTableCell_t1369591435 ** get_address_of_m_cellPrefab_14() { return &___m_cellPrefab_14; }
	inline void set_m_cellPrefab_14(ShopTableCell_t1369591435 * value)
	{
		___m_cellPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_14), value);
	}

	inline static int32_t get_offset_of_m_tableView_15() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___m_tableView_15)); }
	inline TableView_t4228429533 * get_m_tableView_15() const { return ___m_tableView_15; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_15() { return &___m_tableView_15; }
	inline void set_m_tableView_15(TableView_t4228429533 * value)
	{
		___m_tableView_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_15), value);
	}

	inline static int32_t get_offset_of_Label_16() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___Label_16)); }
	inline Text_t1901882714 * get_Label_16() const { return ___Label_16; }
	inline Text_t1901882714 ** get_address_of_Label_16() { return &___Label_16; }
	inline void set_Label_16(Text_t1901882714 * value)
	{
		___Label_16 = value;
		Il2CppCodeGenWriteBarrier((&___Label_16), value);
	}

	inline static int32_t get_offset_of_shillingsCount_17() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___shillingsCount_17)); }
	inline Text_t1901882714 * get_shillingsCount_17() const { return ___shillingsCount_17; }
	inline Text_t1901882714 ** get_address_of_shillingsCount_17() { return &___shillingsCount_17; }
	inline void set_shillingsCount_17(Text_t1901882714 * value)
	{
		___shillingsCount_17 = value;
		Il2CppCodeGenWriteBarrier((&___shillingsCount_17), value);
	}

	inline static int32_t get_offset_of_icon_18() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___icon_18)); }
	inline Image_t2670269651 * get_icon_18() const { return ___icon_18; }
	inline Image_t2670269651 ** get_address_of_icon_18() { return &___icon_18; }
	inline void set_icon_18(Image_t2670269651 * value)
	{
		___icon_18 = value;
		Il2CppCodeGenWriteBarrier((&___icon_18), value);
	}

	inline static int32_t get_offset_of_tabSelectedTop_19() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___tabSelectedTop_19)); }
	inline Sprite_t280657092 * get_tabSelectedTop_19() const { return ___tabSelectedTop_19; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedTop_19() { return &___tabSelectedTop_19; }
	inline void set_tabSelectedTop_19(Sprite_t280657092 * value)
	{
		___tabSelectedTop_19 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedTop_19), value);
	}

	inline static int32_t get_offset_of_tabSelectedBottom_20() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___tabSelectedBottom_20)); }
	inline Sprite_t280657092 * get_tabSelectedBottom_20() const { return ___tabSelectedBottom_20; }
	inline Sprite_t280657092 ** get_address_of_tabSelectedBottom_20() { return &___tabSelectedBottom_20; }
	inline void set_tabSelectedBottom_20(Sprite_t280657092 * value)
	{
		___tabSelectedBottom_20 = value;
		Il2CppCodeGenWriteBarrier((&___tabSelectedBottom_20), value);
	}

	inline static int32_t get_offset_of_tabUnselectedTop_21() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___tabUnselectedTop_21)); }
	inline Sprite_t280657092 * get_tabUnselectedTop_21() const { return ___tabUnselectedTop_21; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedTop_21() { return &___tabUnselectedTop_21; }
	inline void set_tabUnselectedTop_21(Sprite_t280657092 * value)
	{
		___tabUnselectedTop_21 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedTop_21), value);
	}

	inline static int32_t get_offset_of_tabUnselectedBottom_22() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___tabUnselectedBottom_22)); }
	inline Sprite_t280657092 * get_tabUnselectedBottom_22() const { return ___tabUnselectedBottom_22; }
	inline Sprite_t280657092 ** get_address_of_tabUnselectedBottom_22() { return &___tabUnselectedBottom_22; }
	inline void set_tabUnselectedBottom_22(Sprite_t280657092 * value)
	{
		___tabUnselectedBottom_22 = value;
		Il2CppCodeGenWriteBarrier((&___tabUnselectedBottom_22), value);
	}

	inline static int32_t get_offset_of_shopIcon_23() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___shopIcon_23)); }
	inline Sprite_t280657092 * get_shopIcon_23() const { return ___shopIcon_23; }
	inline Sprite_t280657092 ** get_address_of_shopIcon_23() { return &___shopIcon_23; }
	inline void set_shopIcon_23(Sprite_t280657092 * value)
	{
		___shopIcon_23 = value;
		Il2CppCodeGenWriteBarrier((&___shopIcon_23), value);
	}

	inline static int32_t get_offset_of_treasureIcon_24() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___treasureIcon_24)); }
	inline Sprite_t280657092 * get_treasureIcon_24() const { return ___treasureIcon_24; }
	inline Sprite_t280657092 ** get_address_of_treasureIcon_24() { return &___treasureIcon_24; }
	inline void set_treasureIcon_24(Sprite_t280657092 * value)
	{
		___treasureIcon_24 = value;
		Il2CppCodeGenWriteBarrier((&___treasureIcon_24), value);
	}

	inline static int32_t get_offset_of_buyMoreBtn_25() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___buyMoreBtn_25)); }
	inline GameObject_t1113636619 * get_buyMoreBtn_25() const { return ___buyMoreBtn_25; }
	inline GameObject_t1113636619 ** get_address_of_buyMoreBtn_25() { return &___buyMoreBtn_25; }
	inline void set_buyMoreBtn_25(GameObject_t1113636619 * value)
	{
		___buyMoreBtn_25 = value;
		Il2CppCodeGenWriteBarrier((&___buyMoreBtn_25), value);
	}

	inline static int32_t get_offset_of_buyMoreTxt_26() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___buyMoreTxt_26)); }
	inline GameObject_t1113636619 * get_buyMoreTxt_26() const { return ___buyMoreTxt_26; }
	inline GameObject_t1113636619 ** get_address_of_buyMoreTxt_26() { return &___buyMoreTxt_26; }
	inline void set_buyMoreTxt_26(GameObject_t1113636619 * value)
	{
		___buyMoreTxt_26 = value;
		Il2CppCodeGenWriteBarrier((&___buyMoreTxt_26), value);
	}

	inline static int32_t get_offset_of_tabs_27() { return static_cast<int32_t>(offsetof(ShopTableController_t1404998102, ___tabs_27)); }
	inline ImageU5BU5D_t2439009922* get_tabs_27() const { return ___tabs_27; }
	inline ImageU5BU5D_t2439009922** get_address_of_tabs_27() { return &___tabs_27; }
	inline void set_tabs_27(ImageU5BU5D_t2439009922* value)
	{
		___tabs_27 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPTABLECONTROLLER_T1404998102_H
#ifndef MAILINBOXTABLECONTROLLER_T2667561573_H
#define MAILINBOXTABLECONTROLLER_T2667561573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInboxTableController
struct  MailInboxTableController_t2667561573  : public MonoBehaviour_t3962482529
{
public:
	// MailInboxLine MailInboxTableController::m_cellPrefab
	MailInboxLine_t3110909470 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailInboxTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// UnityEngine.UI.InputField MailInboxTableController::username
	InputField_t3762917431 * ___username_4;
	// MessageHeader[] MailInboxTableController::allMessages
	MessageHeaderU5BU5D_t1916243966* ___allMessages_5;
	// System.Collections.ArrayList MailInboxTableController::searchResults
	ArrayList_t2718874744 * ___searchResults_6;
	// System.Boolean MailInboxTableController::search
	bool ___search_7;
	// System.Int32 MailInboxTableController::m_numRows
	int32_t ___m_numRows_8;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___m_cellPrefab_2)); }
	inline MailInboxLine_t3110909470 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailInboxLine_t3110909470 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailInboxLine_t3110909470 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___username_4)); }
	inline InputField_t3762917431 * get_username_4() const { return ___username_4; }
	inline InputField_t3762917431 ** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(InputField_t3762917431 * value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier((&___username_4), value);
	}

	inline static int32_t get_offset_of_allMessages_5() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___allMessages_5)); }
	inline MessageHeaderU5BU5D_t1916243966* get_allMessages_5() const { return ___allMessages_5; }
	inline MessageHeaderU5BU5D_t1916243966** get_address_of_allMessages_5() { return &___allMessages_5; }
	inline void set_allMessages_5(MessageHeaderU5BU5D_t1916243966* value)
	{
		___allMessages_5 = value;
		Il2CppCodeGenWriteBarrier((&___allMessages_5), value);
	}

	inline static int32_t get_offset_of_searchResults_6() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___searchResults_6)); }
	inline ArrayList_t2718874744 * get_searchResults_6() const { return ___searchResults_6; }
	inline ArrayList_t2718874744 ** get_address_of_searchResults_6() { return &___searchResults_6; }
	inline void set_searchResults_6(ArrayList_t2718874744 * value)
	{
		___searchResults_6 = value;
		Il2CppCodeGenWriteBarrier((&___searchResults_6), value);
	}

	inline static int32_t get_offset_of_search_7() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___search_7)); }
	inline bool get_search_7() const { return ___search_7; }
	inline bool* get_address_of_search_7() { return &___search_7; }
	inline void set_search_7(bool value)
	{
		___search_7 = value;
	}

	inline static int32_t get_offset_of_m_numRows_8() { return static_cast<int32_t>(offsetof(MailInboxTableController_t2667561573, ___m_numRows_8)); }
	inline int32_t get_m_numRows_8() const { return ___m_numRows_8; }
	inline int32_t* get_address_of_m_numRows_8() { return &___m_numRows_8; }
	inline void set_m_numRows_8(int32_t value)
	{
		___m_numRows_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILINBOXTABLECONTROLLER_T2667561573_H
#ifndef PANELKNIGHTSTABLECONTROLLER_T3023536378_H
#define PANELKNIGHTSTABLECONTROLLER_T3023536378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelKnightsTableController
struct  PanelKnightsTableController_t3023536378  : public MonoBehaviour_t3962482529
{
public:
	// PanelKnightTableLine PanelKnightsTableController::m_cellPrefab
	PanelKnightTableLine_t48687488 * ___m_cellPrefab_2;
	// Tacticsoft.TableView PanelKnightsTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Collections.ArrayList PanelKnightsTableController::knightList
	ArrayList_t2718874744 * ___knightList_4;
	// System.Int32 PanelKnightsTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t3023536378, ___m_cellPrefab_2)); }
	inline PanelKnightTableLine_t48687488 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline PanelKnightTableLine_t48687488 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(PanelKnightTableLine_t48687488 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t3023536378, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_knightList_4() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t3023536378, ___knightList_4)); }
	inline ArrayList_t2718874744 * get_knightList_4() const { return ___knightList_4; }
	inline ArrayList_t2718874744 ** get_address_of_knightList_4() { return &___knightList_4; }
	inline void set_knightList_4(ArrayList_t2718874744 * value)
	{
		___knightList_4 = value;
		Il2CppCodeGenWriteBarrier((&___knightList_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(PanelKnightsTableController_t3023536378, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELKNIGHTSTABLECONTROLLER_T3023536378_H
#ifndef NEWMAILPLAYERSTABLECONTROLLER_T2252158828_H
#define NEWMAILPLAYERSTABLECONTROLLER_T2252158828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailPlayersTableController
struct  NewMailPlayersTableController_t2252158828  : public MonoBehaviour_t3962482529
{
public:
	// NewMailContentManager NewMailPlayersTableController::owner
	NewMailContentManager_t1552843396 * ___owner_2;
	// NewMailPlayerLine NewMailPlayersTableController::m_cellPrefab
	NewMailPlayerLine_t272747317 * ___m_cellPrefab_3;
	// Tacticsoft.TableView NewMailPlayersTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_4;
	// System.Collections.Generic.List`1<UserModel> NewMailPlayersTableController::allUsers
	List_1_t2826006347 * ___allUsers_5;
	// System.Int32 NewMailPlayersTableController::m_numRows
	int32_t ___m_numRows_6;
	// SimpleEvent NewMailPlayersTableController::onGetAllUsers
	SimpleEvent_t129249603 * ___onGetAllUsers_7;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___owner_2)); }
	inline NewMailContentManager_t1552843396 * get_owner_2() const { return ___owner_2; }
	inline NewMailContentManager_t1552843396 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(NewMailContentManager_t1552843396 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier((&___owner_2), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___m_cellPrefab_3)); }
	inline NewMailPlayerLine_t272747317 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline NewMailPlayerLine_t272747317 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(NewMailPlayerLine_t272747317 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_3), value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___m_tableView_4)); }
	inline TableView_t4228429533 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t4228429533 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_4), value);
	}

	inline static int32_t get_offset_of_allUsers_5() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___allUsers_5)); }
	inline List_1_t2826006347 * get_allUsers_5() const { return ___allUsers_5; }
	inline List_1_t2826006347 ** get_address_of_allUsers_5() { return &___allUsers_5; }
	inline void set_allUsers_5(List_1_t2826006347 * value)
	{
		___allUsers_5 = value;
		Il2CppCodeGenWriteBarrier((&___allUsers_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}

	inline static int32_t get_offset_of_onGetAllUsers_7() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t2252158828, ___onGetAllUsers_7)); }
	inline SimpleEvent_t129249603 * get_onGetAllUsers_7() const { return ___onGetAllUsers_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAllUsers_7() { return &___onGetAllUsers_7; }
	inline void set_onGetAllUsers_7(SimpleEvent_t129249603 * value)
	{
		___onGetAllUsers_7 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAllUsers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWMAILPLAYERSTABLECONTROLLER_T2252158828_H
#ifndef MARKETSELLTABLECONTROLLER_T878431470_H
#define MARKETSELLTABLECONTROLLER_T878431470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketSellTableController
struct  MarketSellTableController_t878431470  : public MonoBehaviour_t3962482529
{
public:
	// MarketManager MarketSellTableController::owner
	MarketManager_t880414981 * ___owner_2;
	// MarketSellTableLine MarketSellTableController::m_cellPrefab
	MarketSellTableLine_t228365181 * ___m_cellPrefab_3;
	// Tacticsoft.TableView MarketSellTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_4;
	// System.Collections.ArrayList MarketSellTableController::lots
	ArrayList_t2718874744 * ___lots_5;
	// System.Int32 MarketSellTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(MarketSellTableController_t878431470, ___owner_2)); }
	inline MarketManager_t880414981 * get_owner_2() const { return ___owner_2; }
	inline MarketManager_t880414981 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(MarketManager_t880414981 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier((&___owner_2), value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(MarketSellTableController_t878431470, ___m_cellPrefab_3)); }
	inline MarketSellTableLine_t228365181 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline MarketSellTableLine_t228365181 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(MarketSellTableLine_t228365181 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_3), value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(MarketSellTableController_t878431470, ___m_tableView_4)); }
	inline TableView_t4228429533 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t4228429533 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_4), value);
	}

	inline static int32_t get_offset_of_lots_5() { return static_cast<int32_t>(offsetof(MarketSellTableController_t878431470, ___lots_5)); }
	inline ArrayList_t2718874744 * get_lots_5() const { return ___lots_5; }
	inline ArrayList_t2718874744 ** get_address_of_lots_5() { return &___lots_5; }
	inline void set_lots_5(ArrayList_t2718874744 * value)
	{
		___lots_5 = value;
		Il2CppCodeGenWriteBarrier((&___lots_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(MarketSellTableController_t878431470, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETSELLTABLECONTROLLER_T878431470_H
#ifndef MAILSYSTEMTABLECONTROLLER_T2081682146_H
#define MAILSYSTEMTABLECONTROLLER_T2081682146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailSystemTableController
struct  MailSystemTableController_t2081682146  : public MonoBehaviour_t3962482529
{
public:
	// MailSystemLine MailSystemTableController::m_cellPrefab
	MailSystemLine_t2170509778 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailSystemTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// SystemReportModel[] MailSystemTableController::allSystem
	SystemReportModelU5BU5D_t826745815* ___allSystem_4;
	// System.Int32 MailSystemTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailSystemTableController_t2081682146, ___m_cellPrefab_2)); }
	inline MailSystemLine_t2170509778 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailSystemLine_t2170509778 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailSystemLine_t2170509778 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailSystemTableController_t2081682146, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_allSystem_4() { return static_cast<int32_t>(offsetof(MailSystemTableController_t2081682146, ___allSystem_4)); }
	inline SystemReportModelU5BU5D_t826745815* get_allSystem_4() const { return ___allSystem_4; }
	inline SystemReportModelU5BU5D_t826745815** get_address_of_allSystem_4() { return &___allSystem_4; }
	inline void set_allSystem_4(SystemReportModelU5BU5D_t826745815* value)
	{
		___allSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___allSystem_4), value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(MailSystemTableController_t2081682146, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILSYSTEMTABLECONTROLLER_T2081682146_H
#ifndef MAILINVITETABLECONTROLLER_T1579929683_H
#define MAILINVITETABLECONTROLLER_T1579929683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInviteTableController
struct  MailInviteTableController_t1579929683  : public MonoBehaviour_t3962482529
{
public:
	// MailInviteLine MailInviteTableController::m_cellPrefab
	MailInviteLine_t4144392430 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailInviteTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// InviteReportModel[] MailInviteTableController::allInvites
	InviteReportModelU5BU5D_t3007066497* ___allInvites_4;
	// System.Boolean MailInviteTableController::mail
	bool ___mail_5;
	// ConfirmationEvent MailInviteTableController::confirmed
	ConfirmationEvent_t890979749 * ___confirmed_6;
	// System.Int64 MailInviteTableController::inviteId
	int64_t ___inviteId_7;
	// System.Int32 MailInviteTableController::m_numRows
	int32_t ___m_numRows_8;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___m_cellPrefab_2)); }
	inline MailInviteLine_t4144392430 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailInviteLine_t4144392430 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailInviteLine_t4144392430 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_allInvites_4() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___allInvites_4)); }
	inline InviteReportModelU5BU5D_t3007066497* get_allInvites_4() const { return ___allInvites_4; }
	inline InviteReportModelU5BU5D_t3007066497** get_address_of_allInvites_4() { return &___allInvites_4; }
	inline void set_allInvites_4(InviteReportModelU5BU5D_t3007066497* value)
	{
		___allInvites_4 = value;
		Il2CppCodeGenWriteBarrier((&___allInvites_4), value);
	}

	inline static int32_t get_offset_of_mail_5() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___mail_5)); }
	inline bool get_mail_5() const { return ___mail_5; }
	inline bool* get_address_of_mail_5() { return &___mail_5; }
	inline void set_mail_5(bool value)
	{
		___mail_5 = value;
	}

	inline static int32_t get_offset_of_confirmed_6() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___confirmed_6)); }
	inline ConfirmationEvent_t890979749 * get_confirmed_6() const { return ___confirmed_6; }
	inline ConfirmationEvent_t890979749 ** get_address_of_confirmed_6() { return &___confirmed_6; }
	inline void set_confirmed_6(ConfirmationEvent_t890979749 * value)
	{
		___confirmed_6 = value;
		Il2CppCodeGenWriteBarrier((&___confirmed_6), value);
	}

	inline static int32_t get_offset_of_inviteId_7() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___inviteId_7)); }
	inline int64_t get_inviteId_7() const { return ___inviteId_7; }
	inline int64_t* get_address_of_inviteId_7() { return &___inviteId_7; }
	inline void set_inviteId_7(int64_t value)
	{
		___inviteId_7 = value;
	}

	inline static int32_t get_offset_of_m_numRows_8() { return static_cast<int32_t>(offsetof(MailInviteTableController_t1579929683, ___m_numRows_8)); }
	inline int32_t get_m_numRows_8() const { return ___m_numRows_8; }
	inline int32_t* get_address_of_m_numRows_8() { return &___m_numRows_8; }
	inline void set_m_numRows_8(int32_t value)
	{
		___m_numRows_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILINVITETABLECONTROLLER_T1579929683_H
#ifndef RIGHTPANEL_T145460902_H
#define RIGHTPANEL_T145460902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RightPanel
struct  RightPanel_t145460902  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform RightPanel::RightButtonsTransform
	RectTransform_t3704657025 * ___RightButtonsTransform_3;
	// UnityEngine.UI.Image RightPanel::cityIcon
	Image_t2670269651 * ___cityIcon_4;
	// UnityEngine.UI.Text RightPanel::cityCoords
	Text_t1901882714 * ___cityCoords_5;
	// UnityEngine.UI.Text RightPanel::citiesCount
	Text_t1901882714 * ___citiesCount_6;
	// System.Boolean RightPanel::isOpened
	bool ___isOpened_7;

public:
	inline static int32_t get_offset_of_RightButtonsTransform_3() { return static_cast<int32_t>(offsetof(RightPanel_t145460902, ___RightButtonsTransform_3)); }
	inline RectTransform_t3704657025 * get_RightButtonsTransform_3() const { return ___RightButtonsTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_RightButtonsTransform_3() { return &___RightButtonsTransform_3; }
	inline void set_RightButtonsTransform_3(RectTransform_t3704657025 * value)
	{
		___RightButtonsTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___RightButtonsTransform_3), value);
	}

	inline static int32_t get_offset_of_cityIcon_4() { return static_cast<int32_t>(offsetof(RightPanel_t145460902, ___cityIcon_4)); }
	inline Image_t2670269651 * get_cityIcon_4() const { return ___cityIcon_4; }
	inline Image_t2670269651 ** get_address_of_cityIcon_4() { return &___cityIcon_4; }
	inline void set_cityIcon_4(Image_t2670269651 * value)
	{
		___cityIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityIcon_4), value);
	}

	inline static int32_t get_offset_of_cityCoords_5() { return static_cast<int32_t>(offsetof(RightPanel_t145460902, ___cityCoords_5)); }
	inline Text_t1901882714 * get_cityCoords_5() const { return ___cityCoords_5; }
	inline Text_t1901882714 ** get_address_of_cityCoords_5() { return &___cityCoords_5; }
	inline void set_cityCoords_5(Text_t1901882714 * value)
	{
		___cityCoords_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityCoords_5), value);
	}

	inline static int32_t get_offset_of_citiesCount_6() { return static_cast<int32_t>(offsetof(RightPanel_t145460902, ___citiesCount_6)); }
	inline Text_t1901882714 * get_citiesCount_6() const { return ___citiesCount_6; }
	inline Text_t1901882714 ** get_address_of_citiesCount_6() { return &___citiesCount_6; }
	inline void set_citiesCount_6(Text_t1901882714 * value)
	{
		___citiesCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___citiesCount_6), value);
	}

	inline static int32_t get_offset_of_isOpened_7() { return static_cast<int32_t>(offsetof(RightPanel_t145460902, ___isOpened_7)); }
	inline bool get_isOpened_7() const { return ___isOpened_7; }
	inline bool* get_address_of_isOpened_7() { return &___isOpened_7; }
	inline void set_isOpened_7(bool value)
	{
		___isOpened_7 = value;
	}
};

struct RightPanel_t145460902_StaticFields
{
public:
	// RightPanel RightPanel::instance
	RightPanel_t145460902 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(RightPanel_t145460902_StaticFields, ___instance_2)); }
	inline RightPanel_t145460902 * get_instance_2() const { return ___instance_2; }
	inline RightPanel_t145460902 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RightPanel_t145460902 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTPANEL_T145460902_H
#ifndef SERVERTIMEUPDATER_T1591320137_H
#define SERVERTIMEUPDATER_T1591320137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerTimeUpdater
struct  ServerTimeUpdater_t1591320137  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ServerTimeUpdater::currentTime
	Text_t1901882714 * ___currentTime_3;
	// System.DateTime ServerTimeUpdater::currentDate
	DateTime_t3738529785  ___currentDate_4;

public:
	inline static int32_t get_offset_of_currentTime_3() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t1591320137, ___currentTime_3)); }
	inline Text_t1901882714 * get_currentTime_3() const { return ___currentTime_3; }
	inline Text_t1901882714 ** get_address_of_currentTime_3() { return &___currentTime_3; }
	inline void set_currentTime_3(Text_t1901882714 * value)
	{
		___currentTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentTime_3), value);
	}

	inline static int32_t get_offset_of_currentDate_4() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t1591320137, ___currentDate_4)); }
	inline DateTime_t3738529785  get_currentDate_4() const { return ___currentDate_4; }
	inline DateTime_t3738529785 * get_address_of_currentDate_4() { return &___currentDate_4; }
	inline void set_currentDate_4(DateTime_t3738529785  value)
	{
		___currentDate_4 = value;
	}
};

struct ServerTimeUpdater_t1591320137_StaticFields
{
public:
	// ServerTimeUpdater ServerTimeUpdater::instance
	ServerTimeUpdater_t1591320137 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ServerTimeUpdater_t1591320137_StaticFields, ___instance_2)); }
	inline ServerTimeUpdater_t1591320137 * get_instance_2() const { return ___instance_2; }
	inline ServerTimeUpdater_t1591320137 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ServerTimeUpdater_t1591320137 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERTIMEUPDATER_T1591320137_H
#ifndef OPENLINK_T536774666_H
#define OPENLINK_T536774666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenLink
struct  OpenLink_t536774666  : public MonoBehaviour_t3962482529
{
public:
	// System.String OpenLink::link
	String_t* ___link_2;

public:
	inline static int32_t get_offset_of_link_2() { return static_cast<int32_t>(offsetof(OpenLink_t536774666, ___link_2)); }
	inline String_t* get_link_2() const { return ___link_2; }
	inline String_t** get_address_of_link_2() { return &___link_2; }
	inline void set_link_2(String_t* value)
	{
		___link_2 = value;
		Il2CppCodeGenWriteBarrier((&___link_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENLINK_T536774666_H
#ifndef RANKICONUPDATER_T798492598_H
#define RANKICONUPDATER_T798492598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankIconUpdater
struct  RankIconUpdater_t798492598  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image RankIconUpdater::rankIcon
	Image_t2670269651 * ___rankIcon_2;
	// System.String RankIconUpdater::rank
	String_t* ___rank_3;

public:
	inline static int32_t get_offset_of_rankIcon_2() { return static_cast<int32_t>(offsetof(RankIconUpdater_t798492598, ___rankIcon_2)); }
	inline Image_t2670269651 * get_rankIcon_2() const { return ___rankIcon_2; }
	inline Image_t2670269651 ** get_address_of_rankIcon_2() { return &___rankIcon_2; }
	inline void set_rankIcon_2(Image_t2670269651 * value)
	{
		___rankIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___rankIcon_2), value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(RankIconUpdater_t798492598, ___rank_3)); }
	inline String_t* get_rank_3() const { return ___rank_3; }
	inline String_t** get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(String_t* value)
	{
		___rank_3 = value;
		Il2CppCodeGenWriteBarrier((&___rank_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKICONUPDATER_T798492598_H
#ifndef MAILALLIANCEREPORTTABLECONTROLLER_T2367082911_H
#define MAILALLIANCEREPORTTABLECONTROLLER_T2367082911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailAllianceReportTableController
struct  MailAllianceReportTableController_t2367082911  : public MonoBehaviour_t3962482529
{
public:
	// MailAllianceReportTableLine MailAllianceReportTableController::m_cellPrefab
	MailAllianceReportTableLine_t2381192320 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailAllianceReportTableController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Boolean MailAllianceReportTableController::alliance
	bool ___alliance_4;
	// BattleReportHeader[] MailAllianceReportTableController::allReports
	BattleReportHeaderU5BU5D_t3016397654* ___allReports_5;
	// System.Int32 MailAllianceReportTableController::m_numRows
	int32_t ___m_numRows_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t2367082911, ___m_cellPrefab_2)); }
	inline MailAllianceReportTableLine_t2381192320 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailAllianceReportTableLine_t2381192320 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailAllianceReportTableLine_t2381192320 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t2367082911, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_alliance_4() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t2367082911, ___alliance_4)); }
	inline bool get_alliance_4() const { return ___alliance_4; }
	inline bool* get_address_of_alliance_4() { return &___alliance_4; }
	inline void set_alliance_4(bool value)
	{
		___alliance_4 = value;
	}

	inline static int32_t get_offset_of_allReports_5() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t2367082911, ___allReports_5)); }
	inline BattleReportHeaderU5BU5D_t3016397654* get_allReports_5() const { return ___allReports_5; }
	inline BattleReportHeaderU5BU5D_t3016397654** get_address_of_allReports_5() { return &___allReports_5; }
	inline void set_allReports_5(BattleReportHeaderU5BU5D_t3016397654* value)
	{
		___allReports_5 = value;
		Il2CppCodeGenWriteBarrier((&___allReports_5), value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(MailAllianceReportTableController_t2367082911, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILALLIANCEREPORTTABLECONTROLLER_T2367082911_H
#ifndef RESIDENCEVALLEYSTABLELINE_T1359679749_H
#define RESIDENCEVALLEYSTABLELINE_T1359679749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceValleysTableLine
struct  ResidenceValleysTableLine_t1359679749  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyName
	Text_t1901882714 * ___valleyName_2;
	// UnityEngine.UI.Text ResidenceValleysTableLine::resource
	Text_t1901882714 * ___resource_3;
	// UnityEngine.UI.Text ResidenceValleysTableLine::region
	Text_t1901882714 * ___region_4;
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyCoords
	Text_t1901882714 * ___valleyCoords_5;
	// UnityEngine.UI.Text ResidenceValleysTableLine::valleyLevel
	Text_t1901882714 * ___valleyLevel_6;
	// UnityEngine.UI.Text ResidenceValleysTableLine::production
	Text_t1901882714 * ___production_7;
	// WorldFieldModel ResidenceValleysTableLine::model
	WorldFieldModel_t2417974361 * ___model_8;
	// ResidenceValleysTableController ResidenceValleysTableLine::owner
	ResidenceValleysTableController_t655612456 * ___owner_9;

public:
	inline static int32_t get_offset_of_valleyName_2() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___valleyName_2)); }
	inline Text_t1901882714 * get_valleyName_2() const { return ___valleyName_2; }
	inline Text_t1901882714 ** get_address_of_valleyName_2() { return &___valleyName_2; }
	inline void set_valleyName_2(Text_t1901882714 * value)
	{
		___valleyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___valleyName_2), value);
	}

	inline static int32_t get_offset_of_resource_3() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___resource_3)); }
	inline Text_t1901882714 * get_resource_3() const { return ___resource_3; }
	inline Text_t1901882714 ** get_address_of_resource_3() { return &___resource_3; }
	inline void set_resource_3(Text_t1901882714 * value)
	{
		___resource_3 = value;
		Il2CppCodeGenWriteBarrier((&___resource_3), value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___region_4)); }
	inline Text_t1901882714 * get_region_4() const { return ___region_4; }
	inline Text_t1901882714 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t1901882714 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_valleyCoords_5() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___valleyCoords_5)); }
	inline Text_t1901882714 * get_valleyCoords_5() const { return ___valleyCoords_5; }
	inline Text_t1901882714 ** get_address_of_valleyCoords_5() { return &___valleyCoords_5; }
	inline void set_valleyCoords_5(Text_t1901882714 * value)
	{
		___valleyCoords_5 = value;
		Il2CppCodeGenWriteBarrier((&___valleyCoords_5), value);
	}

	inline static int32_t get_offset_of_valleyLevel_6() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___valleyLevel_6)); }
	inline Text_t1901882714 * get_valleyLevel_6() const { return ___valleyLevel_6; }
	inline Text_t1901882714 ** get_address_of_valleyLevel_6() { return &___valleyLevel_6; }
	inline void set_valleyLevel_6(Text_t1901882714 * value)
	{
		___valleyLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&___valleyLevel_6), value);
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___production_7)); }
	inline Text_t1901882714 * get_production_7() const { return ___production_7; }
	inline Text_t1901882714 ** get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(Text_t1901882714 * value)
	{
		___production_7 = value;
		Il2CppCodeGenWriteBarrier((&___production_7), value);
	}

	inline static int32_t get_offset_of_model_8() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___model_8)); }
	inline WorldFieldModel_t2417974361 * get_model_8() const { return ___model_8; }
	inline WorldFieldModel_t2417974361 ** get_address_of_model_8() { return &___model_8; }
	inline void set_model_8(WorldFieldModel_t2417974361 * value)
	{
		___model_8 = value;
		Il2CppCodeGenWriteBarrier((&___model_8), value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(ResidenceValleysTableLine_t1359679749, ___owner_9)); }
	inline ResidenceValleysTableController_t655612456 * get_owner_9() const { return ___owner_9; }
	inline ResidenceValleysTableController_t655612456 ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(ResidenceValleysTableController_t655612456 * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier((&___owner_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCEVALLEYSTABLELINE_T1359679749_H
#ifndef UPGRADEBUILDINGTABLELINE_T2892907218_H
#define UPGRADEBUILDINGTABLELINE_T2892907218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeBuildingTableLine
struct  UpgradeBuildingTableLine_t2892907218  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Image UpgradeBuildingTableLine::buildingIcon
	Image_t2670269651 * ___buildingIcon_2;
	// UnityEngine.UI.Text UpgradeBuildingTableLine::buildingName
	Text_t1901882714 * ___buildingName_3;
	// UnityEngine.UI.Text UpgradeBuildingTableLine::buildingLevel
	Text_t1901882714 * ___buildingLevel_4;
	// UpgradeBuildingTableController UpgradeBuildingTableLine::owner
	UpgradeBuildingTableController_t2757797628 * ___owner_5;
	// System.Int64 UpgradeBuildingTableLine::id
	int64_t ___id_6;

public:
	inline static int32_t get_offset_of_buildingIcon_2() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t2892907218, ___buildingIcon_2)); }
	inline Image_t2670269651 * get_buildingIcon_2() const { return ___buildingIcon_2; }
	inline Image_t2670269651 ** get_address_of_buildingIcon_2() { return &___buildingIcon_2; }
	inline void set_buildingIcon_2(Image_t2670269651 * value)
	{
		___buildingIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildingIcon_2), value);
	}

	inline static int32_t get_offset_of_buildingName_3() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t2892907218, ___buildingName_3)); }
	inline Text_t1901882714 * get_buildingName_3() const { return ___buildingName_3; }
	inline Text_t1901882714 ** get_address_of_buildingName_3() { return &___buildingName_3; }
	inline void set_buildingName_3(Text_t1901882714 * value)
	{
		___buildingName_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingName_3), value);
	}

	inline static int32_t get_offset_of_buildingLevel_4() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t2892907218, ___buildingLevel_4)); }
	inline Text_t1901882714 * get_buildingLevel_4() const { return ___buildingLevel_4; }
	inline Text_t1901882714 ** get_address_of_buildingLevel_4() { return &___buildingLevel_4; }
	inline void set_buildingLevel_4(Text_t1901882714 * value)
	{
		___buildingLevel_4 = value;
		Il2CppCodeGenWriteBarrier((&___buildingLevel_4), value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t2892907218, ___owner_5)); }
	inline UpgradeBuildingTableController_t2757797628 * get_owner_5() const { return ___owner_5; }
	inline UpgradeBuildingTableController_t2757797628 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(UpgradeBuildingTableController_t2757797628 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableLine_t2892907218, ___id_6)); }
	inline int64_t get_id_6() const { return ___id_6; }
	inline int64_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int64_t value)
	{
		___id_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPGRADEBUILDINGTABLELINE_T2892907218_H
#ifndef SHOPTABLECELL_T1369591435_H
#define SHOPTABLECELL_T1369591435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopTableCell
struct  ShopTableCell_t1369591435  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text ShopTableCell::itemLabel1
	Text_t1901882714 * ___itemLabel1_2;
	// UnityEngine.UI.Text ShopTableCell::itemDescription1
	Text_t1901882714 * ___itemDescription1_3;
	// UnityEngine.UI.Text ShopTableCell::itemCount1
	Text_t1901882714 * ___itemCount1_4;
	// UnityEngine.UI.Image ShopTableCell::itemImage1
	Image_t2670269651 * ___itemImage1_5;
	// UnityEngine.UI.Text ShopTableCell::itemCost1
	Text_t1901882714 * ___itemCost1_6;
	// UnityEngine.UI.Text ShopTableCell::itemAction1
	Text_t1901882714 * ___itemAction1_7;
	// UnityEngine.GameObject ShopTableCell::UseBtn1
	GameObject_t1113636619 * ___UseBtn1_8;
	// System.String ShopTableCell::itemId1
	String_t* ___itemId1_9;
	// System.String ShopTableCell::constantId1
	String_t* ___constantId1_10;
	// UnityEngine.UI.Text ShopTableCell::itemLabel2
	Text_t1901882714 * ___itemLabel2_11;
	// UnityEngine.UI.Text ShopTableCell::itemDescription2
	Text_t1901882714 * ___itemDescription2_12;
	// UnityEngine.UI.Text ShopTableCell::itemCount2
	Text_t1901882714 * ___itemCount2_13;
	// UnityEngine.UI.Image ShopTableCell::itemImage2
	Image_t2670269651 * ___itemImage2_14;
	// UnityEngine.UI.Text ShopTableCell::itemCost2
	Text_t1901882714 * ___itemCost2_15;
	// UnityEngine.UI.Text ShopTableCell::itemAction2
	Text_t1901882714 * ___itemAction2_16;
	// UnityEngine.GameObject ShopTableCell::UseBtn2
	GameObject_t1113636619 * ___UseBtn2_17;
	// System.String ShopTableCell::itemId2
	String_t* ___itemId2_18;
	// System.String ShopTableCell::constantId2
	String_t* ___constantId2_19;
	// IReloadable ShopTableCell::owner
	RuntimeObject* ___owner_20;
	// System.String ShopTableCell::type
	String_t* ___type_21;
	// System.Int64 ShopTableCell::evendId
	int64_t ___evendId_22;

public:
	inline static int32_t get_offset_of_itemLabel1_2() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemLabel1_2)); }
	inline Text_t1901882714 * get_itemLabel1_2() const { return ___itemLabel1_2; }
	inline Text_t1901882714 ** get_address_of_itemLabel1_2() { return &___itemLabel1_2; }
	inline void set_itemLabel1_2(Text_t1901882714 * value)
	{
		___itemLabel1_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemLabel1_2), value);
	}

	inline static int32_t get_offset_of_itemDescription1_3() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemDescription1_3)); }
	inline Text_t1901882714 * get_itemDescription1_3() const { return ___itemDescription1_3; }
	inline Text_t1901882714 ** get_address_of_itemDescription1_3() { return &___itemDescription1_3; }
	inline void set_itemDescription1_3(Text_t1901882714 * value)
	{
		___itemDescription1_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemDescription1_3), value);
	}

	inline static int32_t get_offset_of_itemCount1_4() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemCount1_4)); }
	inline Text_t1901882714 * get_itemCount1_4() const { return ___itemCount1_4; }
	inline Text_t1901882714 ** get_address_of_itemCount1_4() { return &___itemCount1_4; }
	inline void set_itemCount1_4(Text_t1901882714 * value)
	{
		___itemCount1_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemCount1_4), value);
	}

	inline static int32_t get_offset_of_itemImage1_5() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemImage1_5)); }
	inline Image_t2670269651 * get_itemImage1_5() const { return ___itemImage1_5; }
	inline Image_t2670269651 ** get_address_of_itemImage1_5() { return &___itemImage1_5; }
	inline void set_itemImage1_5(Image_t2670269651 * value)
	{
		___itemImage1_5 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage1_5), value);
	}

	inline static int32_t get_offset_of_itemCost1_6() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemCost1_6)); }
	inline Text_t1901882714 * get_itemCost1_6() const { return ___itemCost1_6; }
	inline Text_t1901882714 ** get_address_of_itemCost1_6() { return &___itemCost1_6; }
	inline void set_itemCost1_6(Text_t1901882714 * value)
	{
		___itemCost1_6 = value;
		Il2CppCodeGenWriteBarrier((&___itemCost1_6), value);
	}

	inline static int32_t get_offset_of_itemAction1_7() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemAction1_7)); }
	inline Text_t1901882714 * get_itemAction1_7() const { return ___itemAction1_7; }
	inline Text_t1901882714 ** get_address_of_itemAction1_7() { return &___itemAction1_7; }
	inline void set_itemAction1_7(Text_t1901882714 * value)
	{
		___itemAction1_7 = value;
		Il2CppCodeGenWriteBarrier((&___itemAction1_7), value);
	}

	inline static int32_t get_offset_of_UseBtn1_8() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___UseBtn1_8)); }
	inline GameObject_t1113636619 * get_UseBtn1_8() const { return ___UseBtn1_8; }
	inline GameObject_t1113636619 ** get_address_of_UseBtn1_8() { return &___UseBtn1_8; }
	inline void set_UseBtn1_8(GameObject_t1113636619 * value)
	{
		___UseBtn1_8 = value;
		Il2CppCodeGenWriteBarrier((&___UseBtn1_8), value);
	}

	inline static int32_t get_offset_of_itemId1_9() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemId1_9)); }
	inline String_t* get_itemId1_9() const { return ___itemId1_9; }
	inline String_t** get_address_of_itemId1_9() { return &___itemId1_9; }
	inline void set_itemId1_9(String_t* value)
	{
		___itemId1_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemId1_9), value);
	}

	inline static int32_t get_offset_of_constantId1_10() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___constantId1_10)); }
	inline String_t* get_constantId1_10() const { return ___constantId1_10; }
	inline String_t** get_address_of_constantId1_10() { return &___constantId1_10; }
	inline void set_constantId1_10(String_t* value)
	{
		___constantId1_10 = value;
		Il2CppCodeGenWriteBarrier((&___constantId1_10), value);
	}

	inline static int32_t get_offset_of_itemLabel2_11() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemLabel2_11)); }
	inline Text_t1901882714 * get_itemLabel2_11() const { return ___itemLabel2_11; }
	inline Text_t1901882714 ** get_address_of_itemLabel2_11() { return &___itemLabel2_11; }
	inline void set_itemLabel2_11(Text_t1901882714 * value)
	{
		___itemLabel2_11 = value;
		Il2CppCodeGenWriteBarrier((&___itemLabel2_11), value);
	}

	inline static int32_t get_offset_of_itemDescription2_12() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemDescription2_12)); }
	inline Text_t1901882714 * get_itemDescription2_12() const { return ___itemDescription2_12; }
	inline Text_t1901882714 ** get_address_of_itemDescription2_12() { return &___itemDescription2_12; }
	inline void set_itemDescription2_12(Text_t1901882714 * value)
	{
		___itemDescription2_12 = value;
		Il2CppCodeGenWriteBarrier((&___itemDescription2_12), value);
	}

	inline static int32_t get_offset_of_itemCount2_13() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemCount2_13)); }
	inline Text_t1901882714 * get_itemCount2_13() const { return ___itemCount2_13; }
	inline Text_t1901882714 ** get_address_of_itemCount2_13() { return &___itemCount2_13; }
	inline void set_itemCount2_13(Text_t1901882714 * value)
	{
		___itemCount2_13 = value;
		Il2CppCodeGenWriteBarrier((&___itemCount2_13), value);
	}

	inline static int32_t get_offset_of_itemImage2_14() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemImage2_14)); }
	inline Image_t2670269651 * get_itemImage2_14() const { return ___itemImage2_14; }
	inline Image_t2670269651 ** get_address_of_itemImage2_14() { return &___itemImage2_14; }
	inline void set_itemImage2_14(Image_t2670269651 * value)
	{
		___itemImage2_14 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage2_14), value);
	}

	inline static int32_t get_offset_of_itemCost2_15() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemCost2_15)); }
	inline Text_t1901882714 * get_itemCost2_15() const { return ___itemCost2_15; }
	inline Text_t1901882714 ** get_address_of_itemCost2_15() { return &___itemCost2_15; }
	inline void set_itemCost2_15(Text_t1901882714 * value)
	{
		___itemCost2_15 = value;
		Il2CppCodeGenWriteBarrier((&___itemCost2_15), value);
	}

	inline static int32_t get_offset_of_itemAction2_16() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemAction2_16)); }
	inline Text_t1901882714 * get_itemAction2_16() const { return ___itemAction2_16; }
	inline Text_t1901882714 ** get_address_of_itemAction2_16() { return &___itemAction2_16; }
	inline void set_itemAction2_16(Text_t1901882714 * value)
	{
		___itemAction2_16 = value;
		Il2CppCodeGenWriteBarrier((&___itemAction2_16), value);
	}

	inline static int32_t get_offset_of_UseBtn2_17() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___UseBtn2_17)); }
	inline GameObject_t1113636619 * get_UseBtn2_17() const { return ___UseBtn2_17; }
	inline GameObject_t1113636619 ** get_address_of_UseBtn2_17() { return &___UseBtn2_17; }
	inline void set_UseBtn2_17(GameObject_t1113636619 * value)
	{
		___UseBtn2_17 = value;
		Il2CppCodeGenWriteBarrier((&___UseBtn2_17), value);
	}

	inline static int32_t get_offset_of_itemId2_18() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___itemId2_18)); }
	inline String_t* get_itemId2_18() const { return ___itemId2_18; }
	inline String_t** get_address_of_itemId2_18() { return &___itemId2_18; }
	inline void set_itemId2_18(String_t* value)
	{
		___itemId2_18 = value;
		Il2CppCodeGenWriteBarrier((&___itemId2_18), value);
	}

	inline static int32_t get_offset_of_constantId2_19() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___constantId2_19)); }
	inline String_t* get_constantId2_19() const { return ___constantId2_19; }
	inline String_t** get_address_of_constantId2_19() { return &___constantId2_19; }
	inline void set_constantId2_19(String_t* value)
	{
		___constantId2_19 = value;
		Il2CppCodeGenWriteBarrier((&___constantId2_19), value);
	}

	inline static int32_t get_offset_of_owner_20() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___owner_20)); }
	inline RuntimeObject* get_owner_20() const { return ___owner_20; }
	inline RuntimeObject** get_address_of_owner_20() { return &___owner_20; }
	inline void set_owner_20(RuntimeObject* value)
	{
		___owner_20 = value;
		Il2CppCodeGenWriteBarrier((&___owner_20), value);
	}

	inline static int32_t get_offset_of_type_21() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___type_21)); }
	inline String_t* get_type_21() const { return ___type_21; }
	inline String_t** get_address_of_type_21() { return &___type_21; }
	inline void set_type_21(String_t* value)
	{
		___type_21 = value;
		Il2CppCodeGenWriteBarrier((&___type_21), value);
	}

	inline static int32_t get_offset_of_evendId_22() { return static_cast<int32_t>(offsetof(ShopTableCell_t1369591435, ___evendId_22)); }
	inline int64_t get_evendId_22() const { return ___evendId_22; }
	inline int64_t* get_address_of_evendId_22() { return &___evendId_22; }
	inline void set_evendId_22(int64_t value)
	{
		___evendId_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPTABLECELL_T1369591435_H
#ifndef GESTURE_T4067939634_H
#define GESTURE_T4067939634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture
struct  Gesture_t4067939634  : public DebuggableMonoBehaviour_t3692909384
{
public:
	// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs> TouchScript.Gestures.Gesture::stateChangedInvoker
	EventHandler_1_t766498446 * ___stateChangedInvoker_4;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Gesture::cancelledInvoker
	EventHandler_1_t1515976428 * ___cancelledInvoker_5;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_6;
	// TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_7;
	// TouchScript.ITouchManager TouchScript.Gestures.Gesture::<touchManager>k__BackingField
	RuntimeObject* ___U3CtouchManagerU3Ek__BackingField_8;
	// TouchScript.Gestures.Gesture/TouchesNumState TouchScript.Gestures.Gesture::<touchesNumState>k__BackingField
	int32_t ___U3CtouchesNumStateU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::activeTouches
	List_1_t2345966477 * ___activeTouches_10;
	// UnityEngine.Transform TouchScript.Gestures.Gesture::cachedTransform
	Transform_t3600365921 * ___cachedTransform_11;
	// System.Boolean TouchScript.Gestures.Gesture::advancedProps
	bool ___advancedProps_12;
	// System.Int32 TouchScript.Gestures.Gesture::minTouches
	int32_t ___minTouches_13;
	// System.Int32 TouchScript.Gestures.Gesture::maxTouches
	int32_t ___maxTouches_14;
	// System.Boolean TouchScript.Gestures.Gesture::combineTouches
	bool ___combineTouches_15;
	// System.Single TouchScript.Gestures.Gesture::combineTouchesInterval
	float ___combineTouchesInterval_16;
	// System.Boolean TouchScript.Gestures.Gesture::useSendMessage
	bool ___useSendMessage_17;
	// System.Boolean TouchScript.Gestures.Gesture::sendStateChangeMessages
	bool ___sendStateChangeMessages_18;
	// UnityEngine.GameObject TouchScript.Gestures.Gesture::sendMessageTarget
	GameObject_t1113636619 * ___sendMessageTarget_19;
	// TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::requireGestureToFail
	Gesture_t4067939634 * ___requireGestureToFail_20;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Gestures.Gesture::friendlyGestures
	List_1_t1245047080 * ___friendlyGestures_21;
	// System.Int32 TouchScript.Gestures.Gesture::numTouches
	int32_t ___numTouches_22;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.Gesture::layer
	TouchLayer_t1334725428 * ___layer_23;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::readonlyActiveTouches
	ReadOnlyCollection_1_t2086468022 * ___readonlyActiveTouches_24;
	// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::touchSequence
	TimedSequence_1_t1745006843 * ___touchSequence_25;
	// TouchScript.GestureManagerInstance TouchScript.Gestures.Gesture::gestureManagerInstance
	GestureManagerInstance_t955425494 * ___gestureManagerInstance_26;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::delayedStateChange
	int32_t ___delayedStateChange_27;
	// System.Boolean TouchScript.Gestures.Gesture::requiredGestureFailed
	bool ___requiredGestureFailed_28;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::state
	int32_t ___state_29;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedScreenPosition
	Vector2_t2156229523  ___cachedScreenPosition_30;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedPreviousScreenPosition
	Vector2_t2156229523  ___cachedPreviousScreenPosition_31;

public:
	inline static int32_t get_offset_of_stateChangedInvoker_4() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___stateChangedInvoker_4)); }
	inline EventHandler_1_t766498446 * get_stateChangedInvoker_4() const { return ___stateChangedInvoker_4; }
	inline EventHandler_1_t766498446 ** get_address_of_stateChangedInvoker_4() { return &___stateChangedInvoker_4; }
	inline void set_stateChangedInvoker_4(EventHandler_1_t766498446 * value)
	{
		___stateChangedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((&___stateChangedInvoker_4), value);
	}

	inline static int32_t get_offset_of_cancelledInvoker_5() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cancelledInvoker_5)); }
	inline EventHandler_1_t1515976428 * get_cancelledInvoker_5() const { return ___cancelledInvoker_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_cancelledInvoker_5() { return &___cancelledInvoker_5; }
	inline void set_cancelledInvoker_5(EventHandler_1_t1515976428 * value)
	{
		___cancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((&___cancelledInvoker_5), value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CPreviousStateU3Ek__BackingField_6)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_6() const { return ___U3CPreviousStateU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_6() { return &___U3CPreviousStateU3Ek__BackingField_6; }
	inline void set_U3CPreviousStateU3Ek__BackingField_6(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CDelegateU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_7() const { return ___U3CDelegateU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_7() { return &___U3CDelegateU3Ek__BackingField_7; }
	inline void set_U3CDelegateU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CtouchManagerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CtouchManagerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CtouchManagerU3Ek__BackingField_8() const { return ___U3CtouchManagerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CtouchManagerU3Ek__BackingField_8() { return &___U3CtouchManagerU3Ek__BackingField_8; }
	inline void set_U3CtouchManagerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CtouchManagerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchManagerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CtouchesNumStateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___U3CtouchesNumStateU3Ek__BackingField_9)); }
	inline int32_t get_U3CtouchesNumStateU3Ek__BackingField_9() const { return ___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CtouchesNumStateU3Ek__BackingField_9() { return &___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline void set_U3CtouchesNumStateU3Ek__BackingField_9(int32_t value)
	{
		___U3CtouchesNumStateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_activeTouches_10() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___activeTouches_10)); }
	inline List_1_t2345966477 * get_activeTouches_10() const { return ___activeTouches_10; }
	inline List_1_t2345966477 ** get_address_of_activeTouches_10() { return &___activeTouches_10; }
	inline void set_activeTouches_10(List_1_t2345966477 * value)
	{
		___activeTouches_10 = value;
		Il2CppCodeGenWriteBarrier((&___activeTouches_10), value);
	}

	inline static int32_t get_offset_of_cachedTransform_11() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedTransform_11)); }
	inline Transform_t3600365921 * get_cachedTransform_11() const { return ___cachedTransform_11; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_11() { return &___cachedTransform_11; }
	inline void set_cachedTransform_11(Transform_t3600365921 * value)
	{
		___cachedTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_11), value);
	}

	inline static int32_t get_offset_of_advancedProps_12() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___advancedProps_12)); }
	inline bool get_advancedProps_12() const { return ___advancedProps_12; }
	inline bool* get_address_of_advancedProps_12() { return &___advancedProps_12; }
	inline void set_advancedProps_12(bool value)
	{
		___advancedProps_12 = value;
	}

	inline static int32_t get_offset_of_minTouches_13() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___minTouches_13)); }
	inline int32_t get_minTouches_13() const { return ___minTouches_13; }
	inline int32_t* get_address_of_minTouches_13() { return &___minTouches_13; }
	inline void set_minTouches_13(int32_t value)
	{
		___minTouches_13 = value;
	}

	inline static int32_t get_offset_of_maxTouches_14() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___maxTouches_14)); }
	inline int32_t get_maxTouches_14() const { return ___maxTouches_14; }
	inline int32_t* get_address_of_maxTouches_14() { return &___maxTouches_14; }
	inline void set_maxTouches_14(int32_t value)
	{
		___maxTouches_14 = value;
	}

	inline static int32_t get_offset_of_combineTouches_15() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___combineTouches_15)); }
	inline bool get_combineTouches_15() const { return ___combineTouches_15; }
	inline bool* get_address_of_combineTouches_15() { return &___combineTouches_15; }
	inline void set_combineTouches_15(bool value)
	{
		___combineTouches_15 = value;
	}

	inline static int32_t get_offset_of_combineTouchesInterval_16() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___combineTouchesInterval_16)); }
	inline float get_combineTouchesInterval_16() const { return ___combineTouchesInterval_16; }
	inline float* get_address_of_combineTouchesInterval_16() { return &___combineTouchesInterval_16; }
	inline void set_combineTouchesInterval_16(float value)
	{
		___combineTouchesInterval_16 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_17() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___useSendMessage_17)); }
	inline bool get_useSendMessage_17() const { return ___useSendMessage_17; }
	inline bool* get_address_of_useSendMessage_17() { return &___useSendMessage_17; }
	inline void set_useSendMessage_17(bool value)
	{
		___useSendMessage_17 = value;
	}

	inline static int32_t get_offset_of_sendStateChangeMessages_18() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___sendStateChangeMessages_18)); }
	inline bool get_sendStateChangeMessages_18() const { return ___sendStateChangeMessages_18; }
	inline bool* get_address_of_sendStateChangeMessages_18() { return &___sendStateChangeMessages_18; }
	inline void set_sendStateChangeMessages_18(bool value)
	{
		___sendStateChangeMessages_18 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_19() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___sendMessageTarget_19)); }
	inline GameObject_t1113636619 * get_sendMessageTarget_19() const { return ___sendMessageTarget_19; }
	inline GameObject_t1113636619 ** get_address_of_sendMessageTarget_19() { return &___sendMessageTarget_19; }
	inline void set_sendMessageTarget_19(GameObject_t1113636619 * value)
	{
		___sendMessageTarget_19 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_19), value);
	}

	inline static int32_t get_offset_of_requireGestureToFail_20() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___requireGestureToFail_20)); }
	inline Gesture_t4067939634 * get_requireGestureToFail_20() const { return ___requireGestureToFail_20; }
	inline Gesture_t4067939634 ** get_address_of_requireGestureToFail_20() { return &___requireGestureToFail_20; }
	inline void set_requireGestureToFail_20(Gesture_t4067939634 * value)
	{
		___requireGestureToFail_20 = value;
		Il2CppCodeGenWriteBarrier((&___requireGestureToFail_20), value);
	}

	inline static int32_t get_offset_of_friendlyGestures_21() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___friendlyGestures_21)); }
	inline List_1_t1245047080 * get_friendlyGestures_21() const { return ___friendlyGestures_21; }
	inline List_1_t1245047080 ** get_address_of_friendlyGestures_21() { return &___friendlyGestures_21; }
	inline void set_friendlyGestures_21(List_1_t1245047080 * value)
	{
		___friendlyGestures_21 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyGestures_21), value);
	}

	inline static int32_t get_offset_of_numTouches_22() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___numTouches_22)); }
	inline int32_t get_numTouches_22() const { return ___numTouches_22; }
	inline int32_t* get_address_of_numTouches_22() { return &___numTouches_22; }
	inline void set_numTouches_22(int32_t value)
	{
		___numTouches_22 = value;
	}

	inline static int32_t get_offset_of_layer_23() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___layer_23)); }
	inline TouchLayer_t1334725428 * get_layer_23() const { return ___layer_23; }
	inline TouchLayer_t1334725428 ** get_address_of_layer_23() { return &___layer_23; }
	inline void set_layer_23(TouchLayer_t1334725428 * value)
	{
		___layer_23 = value;
		Il2CppCodeGenWriteBarrier((&___layer_23), value);
	}

	inline static int32_t get_offset_of_readonlyActiveTouches_24() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___readonlyActiveTouches_24)); }
	inline ReadOnlyCollection_1_t2086468022 * get_readonlyActiveTouches_24() const { return ___readonlyActiveTouches_24; }
	inline ReadOnlyCollection_1_t2086468022 ** get_address_of_readonlyActiveTouches_24() { return &___readonlyActiveTouches_24; }
	inline void set_readonlyActiveTouches_24(ReadOnlyCollection_1_t2086468022 * value)
	{
		___readonlyActiveTouches_24 = value;
		Il2CppCodeGenWriteBarrier((&___readonlyActiveTouches_24), value);
	}

	inline static int32_t get_offset_of_touchSequence_25() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___touchSequence_25)); }
	inline TimedSequence_1_t1745006843 * get_touchSequence_25() const { return ___touchSequence_25; }
	inline TimedSequence_1_t1745006843 ** get_address_of_touchSequence_25() { return &___touchSequence_25; }
	inline void set_touchSequence_25(TimedSequence_1_t1745006843 * value)
	{
		___touchSequence_25 = value;
		Il2CppCodeGenWriteBarrier((&___touchSequence_25), value);
	}

	inline static int32_t get_offset_of_gestureManagerInstance_26() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___gestureManagerInstance_26)); }
	inline GestureManagerInstance_t955425494 * get_gestureManagerInstance_26() const { return ___gestureManagerInstance_26; }
	inline GestureManagerInstance_t955425494 ** get_address_of_gestureManagerInstance_26() { return &___gestureManagerInstance_26; }
	inline void set_gestureManagerInstance_26(GestureManagerInstance_t955425494 * value)
	{
		___gestureManagerInstance_26 = value;
		Il2CppCodeGenWriteBarrier((&___gestureManagerInstance_26), value);
	}

	inline static int32_t get_offset_of_delayedStateChange_27() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___delayedStateChange_27)); }
	inline int32_t get_delayedStateChange_27() const { return ___delayedStateChange_27; }
	inline int32_t* get_address_of_delayedStateChange_27() { return &___delayedStateChange_27; }
	inline void set_delayedStateChange_27(int32_t value)
	{
		___delayedStateChange_27 = value;
	}

	inline static int32_t get_offset_of_requiredGestureFailed_28() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___requiredGestureFailed_28)); }
	inline bool get_requiredGestureFailed_28() const { return ___requiredGestureFailed_28; }
	inline bool* get_address_of_requiredGestureFailed_28() { return &___requiredGestureFailed_28; }
	inline void set_requiredGestureFailed_28(bool value)
	{
		___requiredGestureFailed_28 = value;
	}

	inline static int32_t get_offset_of_state_29() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___state_29)); }
	inline int32_t get_state_29() const { return ___state_29; }
	inline int32_t* get_address_of_state_29() { return &___state_29; }
	inline void set_state_29(int32_t value)
	{
		___state_29 = value;
	}

	inline static int32_t get_offset_of_cachedScreenPosition_30() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedScreenPosition_30)); }
	inline Vector2_t2156229523  get_cachedScreenPosition_30() const { return ___cachedScreenPosition_30; }
	inline Vector2_t2156229523 * get_address_of_cachedScreenPosition_30() { return &___cachedScreenPosition_30; }
	inline void set_cachedScreenPosition_30(Vector2_t2156229523  value)
	{
		___cachedScreenPosition_30 = value;
	}

	inline static int32_t get_offset_of_cachedPreviousScreenPosition_31() { return static_cast<int32_t>(offsetof(Gesture_t4067939634, ___cachedPreviousScreenPosition_31)); }
	inline Vector2_t2156229523  get_cachedPreviousScreenPosition_31() const { return ___cachedPreviousScreenPosition_31; }
	inline Vector2_t2156229523 * get_address_of_cachedPreviousScreenPosition_31() { return &___cachedPreviousScreenPosition_31; }
	inline void set_cachedPreviousScreenPosition_31(Vector2_t2156229523  value)
	{
		___cachedPreviousScreenPosition_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_T4067939634_H
#ifndef ALLIANCEINVITELINE_T1691417839_H
#define ALLIANCEINVITELINE_T1691417839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInviteLine
struct  AllianceInviteLine_t1691417839  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text AllianceInviteLine::memberName
	Text_t1901882714 * ___memberName_2;
	// UnityEngine.UI.Text AllianceInviteLine::allianceName
	Text_t1901882714 * ___allianceName_3;
	// UnityEngine.UI.Text AllianceInviteLine::rank
	Text_t1901882714 * ___rank_4;
	// UnityEngine.UI.Text AllianceInviteLine::experience
	Text_t1901882714 * ___experience_5;
	// UnityEngine.UI.Text AllianceInviteLine::cities
	Text_t1901882714 * ___cities_6;
	// UnityEngine.UI.Text AllianceInviteLine::capitalName
	Text_t1901882714 * ___capitalName_7;
	// UnityEngine.UI.Text AllianceInviteLine::lastOnline
	Text_t1901882714 * ___lastOnline_8;
	// UserModel AllianceInviteLine::user
	UserModel_t1353931605 * ___user_9;
	// AllianceInviteTableController AllianceInviteLine::owner
	AllianceInviteTableController_t1458639565 * ___owner_10;

public:
	inline static int32_t get_offset_of_memberName_2() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___memberName_2)); }
	inline Text_t1901882714 * get_memberName_2() const { return ___memberName_2; }
	inline Text_t1901882714 ** get_address_of_memberName_2() { return &___memberName_2; }
	inline void set_memberName_2(Text_t1901882714 * value)
	{
		___memberName_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberName_2), value);
	}

	inline static int32_t get_offset_of_allianceName_3() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___allianceName_3)); }
	inline Text_t1901882714 * get_allianceName_3() const { return ___allianceName_3; }
	inline Text_t1901882714 ** get_address_of_allianceName_3() { return &___allianceName_3; }
	inline void set_allianceName_3(Text_t1901882714 * value)
	{
		___allianceName_3 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_3), value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___rank_4)); }
	inline Text_t1901882714 * get_rank_4() const { return ___rank_4; }
	inline Text_t1901882714 ** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(Text_t1901882714 * value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier((&___rank_4), value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___experience_5)); }
	inline Text_t1901882714 * get_experience_5() const { return ___experience_5; }
	inline Text_t1901882714 ** get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(Text_t1901882714 * value)
	{
		___experience_5 = value;
		Il2CppCodeGenWriteBarrier((&___experience_5), value);
	}

	inline static int32_t get_offset_of_cities_6() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___cities_6)); }
	inline Text_t1901882714 * get_cities_6() const { return ___cities_6; }
	inline Text_t1901882714 ** get_address_of_cities_6() { return &___cities_6; }
	inline void set_cities_6(Text_t1901882714 * value)
	{
		___cities_6 = value;
		Il2CppCodeGenWriteBarrier((&___cities_6), value);
	}

	inline static int32_t get_offset_of_capitalName_7() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___capitalName_7)); }
	inline Text_t1901882714 * get_capitalName_7() const { return ___capitalName_7; }
	inline Text_t1901882714 ** get_address_of_capitalName_7() { return &___capitalName_7; }
	inline void set_capitalName_7(Text_t1901882714 * value)
	{
		___capitalName_7 = value;
		Il2CppCodeGenWriteBarrier((&___capitalName_7), value);
	}

	inline static int32_t get_offset_of_lastOnline_8() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___lastOnline_8)); }
	inline Text_t1901882714 * get_lastOnline_8() const { return ___lastOnline_8; }
	inline Text_t1901882714 ** get_address_of_lastOnline_8() { return &___lastOnline_8; }
	inline void set_lastOnline_8(Text_t1901882714 * value)
	{
		___lastOnline_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnline_8), value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___user_9)); }
	inline UserModel_t1353931605 * get_user_9() const { return ___user_9; }
	inline UserModel_t1353931605 ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(UserModel_t1353931605 * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier((&___user_9), value);
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(AllianceInviteLine_t1691417839, ___owner_10)); }
	inline AllianceInviteTableController_t1458639565 * get_owner_10() const { return ___owner_10; }
	inline AllianceInviteTableController_t1458639565 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(AllianceInviteTableController_t1458639565 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier((&___owner_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEINVITELINE_T1691417839_H
#ifndef RESIDENCECOLONIESTABLELINE_T998651070_H
#define RESIDENCECOLONIESTABLELINE_T998651070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceColoniesTableLine
struct  ResidenceColoniesTableLine_t998651070  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text ResidenceColoniesTableLine::playerName
	Text_t1901882714 * ___playerName_2;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::cityName
	Text_t1901882714 * ___cityName_3;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::region
	Text_t1901882714 * ___region_4;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::cityCoords
	Text_t1901882714 * ___cityCoords_5;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::mayerLevel
	Text_t1901882714 * ___mayerLevel_6;
	// ResidenceColoniesTableController ResidenceColoniesTableLine::owner
	ResidenceColoniesTableController_t3519043985 * ___owner_7;
	// ResourcesModel ResidenceColoniesTableLine::tributes
	ResourcesModel_t2533508513 * ___tributes_8;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___playerName_2)); }
	inline Text_t1901882714 * get_playerName_2() const { return ___playerName_2; }
	inline Text_t1901882714 ** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(Text_t1901882714 * value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_2), value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___cityName_3)); }
	inline Text_t1901882714 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t1901882714 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t1901882714 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_3), value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___region_4)); }
	inline Text_t1901882714 * get_region_4() const { return ___region_4; }
	inline Text_t1901882714 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t1901882714 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_cityCoords_5() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___cityCoords_5)); }
	inline Text_t1901882714 * get_cityCoords_5() const { return ___cityCoords_5; }
	inline Text_t1901882714 ** get_address_of_cityCoords_5() { return &___cityCoords_5; }
	inline void set_cityCoords_5(Text_t1901882714 * value)
	{
		___cityCoords_5 = value;
		Il2CppCodeGenWriteBarrier((&___cityCoords_5), value);
	}

	inline static int32_t get_offset_of_mayerLevel_6() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___mayerLevel_6)); }
	inline Text_t1901882714 * get_mayerLevel_6() const { return ___mayerLevel_6; }
	inline Text_t1901882714 ** get_address_of_mayerLevel_6() { return &___mayerLevel_6; }
	inline void set_mayerLevel_6(Text_t1901882714 * value)
	{
		___mayerLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&___mayerLevel_6), value);
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___owner_7)); }
	inline ResidenceColoniesTableController_t3519043985 * get_owner_7() const { return ___owner_7; }
	inline ResidenceColoniesTableController_t3519043985 ** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(ResidenceColoniesTableController_t3519043985 * value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier((&___owner_7), value);
	}

	inline static int32_t get_offset_of_tributes_8() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t998651070, ___tributes_8)); }
	inline ResourcesModel_t2533508513 * get_tributes_8() const { return ___tributes_8; }
	inline ResourcesModel_t2533508513 ** get_address_of_tributes_8() { return &___tributes_8; }
	inline void set_tributes_8(ResourcesModel_t2533508513 * value)
	{
		___tributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___tributes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCECOLONIESTABLELINE_T998651070_H
#ifndef COMMANDCENTERARMYTABLELINE_T1166067843_H
#define COMMANDCENTERARMYTABLELINE_T1166067843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterArmyTableLine
struct  CommandCenterArmyTableLine_t1166067843  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text CommandCenterArmyTableLine::knightName
	Text_t1901882714 * ___knightName_2;
	// UnityEngine.UI.Text CommandCenterArmyTableLine::status
	Text_t1901882714 * ___status_3;
	// System.Int64 CommandCenterArmyTableLine::armyId
	int64_t ___armyId_4;
	// CommandCenterArmiesTableController CommandCenterArmyTableLine::owner
	CommandCenterArmiesTableController_t2609779151 * ___owner_5;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1166067843, ___knightName_2)); }
	inline Text_t1901882714 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t1901882714 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t1901882714 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier((&___knightName_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1166067843, ___status_3)); }
	inline Text_t1901882714 * get_status_3() const { return ___status_3; }
	inline Text_t1901882714 ** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(Text_t1901882714 * value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_armyId_4() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1166067843, ___armyId_4)); }
	inline int64_t get_armyId_4() const { return ___armyId_4; }
	inline int64_t* get_address_of_armyId_4() { return &___armyId_4; }
	inline void set_armyId_4(int64_t value)
	{
		___armyId_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(CommandCenterArmyTableLine_t1166067843, ___owner_5)); }
	inline CommandCenterArmiesTableController_t2609779151 * get_owner_5() const { return ___owner_5; }
	inline CommandCenterArmiesTableController_t2609779151 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(CommandCenterArmiesTableController_t2609779151 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDCENTERARMYTABLELINE_T1166067843_H
#ifndef BATTLEREPORTUNITSLINE_T2550456670_H
#define BATTLEREPORTUNITSLINE_T2550456670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportUnitsLine
struct  BattleReportUnitsLine_t2550456670  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text BattleReportUnitsLine::unitName
	Text_t1901882714 * ___unitName_2;
	// UnityEngine.UI.Text BattleReportUnitsLine::beforeCount
	Text_t1901882714 * ___beforeCount_3;
	// UnityEngine.UI.Text BattleReportUnitsLine::lostCount
	Text_t1901882714 * ___lostCount_4;
	// UnityEngine.UI.Text BattleReportUnitsLine::remainingCount
	Text_t1901882714 * ___remainingCount_5;

public:
	inline static int32_t get_offset_of_unitName_2() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t2550456670, ___unitName_2)); }
	inline Text_t1901882714 * get_unitName_2() const { return ___unitName_2; }
	inline Text_t1901882714 ** get_address_of_unitName_2() { return &___unitName_2; }
	inline void set_unitName_2(Text_t1901882714 * value)
	{
		___unitName_2 = value;
		Il2CppCodeGenWriteBarrier((&___unitName_2), value);
	}

	inline static int32_t get_offset_of_beforeCount_3() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t2550456670, ___beforeCount_3)); }
	inline Text_t1901882714 * get_beforeCount_3() const { return ___beforeCount_3; }
	inline Text_t1901882714 ** get_address_of_beforeCount_3() { return &___beforeCount_3; }
	inline void set_beforeCount_3(Text_t1901882714 * value)
	{
		___beforeCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___beforeCount_3), value);
	}

	inline static int32_t get_offset_of_lostCount_4() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t2550456670, ___lostCount_4)); }
	inline Text_t1901882714 * get_lostCount_4() const { return ___lostCount_4; }
	inline Text_t1901882714 ** get_address_of_lostCount_4() { return &___lostCount_4; }
	inline void set_lostCount_4(Text_t1901882714 * value)
	{
		___lostCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___lostCount_4), value);
	}

	inline static int32_t get_offset_of_remainingCount_5() { return static_cast<int32_t>(offsetof(BattleReportUnitsLine_t2550456670, ___remainingCount_5)); }
	inline Text_t1901882714 * get_remainingCount_5() const { return ___remainingCount_5; }
	inline Text_t1901882714 ** get_address_of_remainingCount_5() { return &___remainingCount_5; }
	inline void set_remainingCount_5(Text_t1901882714 * value)
	{
		___remainingCount_5 = value;
		Il2CppCodeGenWriteBarrier((&___remainingCount_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTUNITSLINE_T2550456670_H
#ifndef FINANCIALTABLELINE_T3445994354_H
#define FINANCIALTABLELINE_T3445994354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialTableLine
struct  FinancialTableLine_t3445994354  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Image FinancialTableLine::productImage
	Image_t2670269651 * ___productImage_2;
	// UnityEngine.UI.Text FinancialTableLine::productName
	Text_t1901882714 * ___productName_3;
	// UnityEngine.UI.Text FinancialTableLine::quantity
	Text_t1901882714 * ___quantity_4;
	// UnityEngine.UI.Text FinancialTableLine::price
	Text_t1901882714 * ___price_5;
	// UnityEngine.UI.Text FinancialTableLine::date
	Text_t1901882714 * ___date_6;
	// UnityEngine.UI.Text FinancialTableLine::fromUser
	Text_t1901882714 * ___fromUser_7;
	// UnityEngine.GameObject FinancialTableLine::cancelOfferBtn
	GameObject_t1113636619 * ___cancelOfferBtn_8;
	// FinancialContentChanger FinancialTableLine::owner
	FinancialContentChanger_t4019109810 * ___owner_9;
	// System.String FinancialTableLine::lotId
	String_t* ___lotId_10;

public:
	inline static int32_t get_offset_of_productImage_2() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___productImage_2)); }
	inline Image_t2670269651 * get_productImage_2() const { return ___productImage_2; }
	inline Image_t2670269651 ** get_address_of_productImage_2() { return &___productImage_2; }
	inline void set_productImage_2(Image_t2670269651 * value)
	{
		___productImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___productImage_2), value);
	}

	inline static int32_t get_offset_of_productName_3() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___productName_3)); }
	inline Text_t1901882714 * get_productName_3() const { return ___productName_3; }
	inline Text_t1901882714 ** get_address_of_productName_3() { return &___productName_3; }
	inline void set_productName_3(Text_t1901882714 * value)
	{
		___productName_3 = value;
		Il2CppCodeGenWriteBarrier((&___productName_3), value);
	}

	inline static int32_t get_offset_of_quantity_4() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___quantity_4)); }
	inline Text_t1901882714 * get_quantity_4() const { return ___quantity_4; }
	inline Text_t1901882714 ** get_address_of_quantity_4() { return &___quantity_4; }
	inline void set_quantity_4(Text_t1901882714 * value)
	{
		___quantity_4 = value;
		Il2CppCodeGenWriteBarrier((&___quantity_4), value);
	}

	inline static int32_t get_offset_of_price_5() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___price_5)); }
	inline Text_t1901882714 * get_price_5() const { return ___price_5; }
	inline Text_t1901882714 ** get_address_of_price_5() { return &___price_5; }
	inline void set_price_5(Text_t1901882714 * value)
	{
		___price_5 = value;
		Il2CppCodeGenWriteBarrier((&___price_5), value);
	}

	inline static int32_t get_offset_of_date_6() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___date_6)); }
	inline Text_t1901882714 * get_date_6() const { return ___date_6; }
	inline Text_t1901882714 ** get_address_of_date_6() { return &___date_6; }
	inline void set_date_6(Text_t1901882714 * value)
	{
		___date_6 = value;
		Il2CppCodeGenWriteBarrier((&___date_6), value);
	}

	inline static int32_t get_offset_of_fromUser_7() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___fromUser_7)); }
	inline Text_t1901882714 * get_fromUser_7() const { return ___fromUser_7; }
	inline Text_t1901882714 ** get_address_of_fromUser_7() { return &___fromUser_7; }
	inline void set_fromUser_7(Text_t1901882714 * value)
	{
		___fromUser_7 = value;
		Il2CppCodeGenWriteBarrier((&___fromUser_7), value);
	}

	inline static int32_t get_offset_of_cancelOfferBtn_8() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___cancelOfferBtn_8)); }
	inline GameObject_t1113636619 * get_cancelOfferBtn_8() const { return ___cancelOfferBtn_8; }
	inline GameObject_t1113636619 ** get_address_of_cancelOfferBtn_8() { return &___cancelOfferBtn_8; }
	inline void set_cancelOfferBtn_8(GameObject_t1113636619 * value)
	{
		___cancelOfferBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___cancelOfferBtn_8), value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___owner_9)); }
	inline FinancialContentChanger_t4019109810 * get_owner_9() const { return ___owner_9; }
	inline FinancialContentChanger_t4019109810 ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(FinancialContentChanger_t4019109810 * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier((&___owner_9), value);
	}

	inline static int32_t get_offset_of_lotId_10() { return static_cast<int32_t>(offsetof(FinancialTableLine_t3445994354, ___lotId_10)); }
	inline String_t* get_lotId_10() const { return ___lotId_10; }
	inline String_t** get_address_of_lotId_10() { return &___lotId_10; }
	inline void set_lotId_10(String_t* value)
	{
		___lotId_10 = value;
		Il2CppCodeGenWriteBarrier((&___lotId_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINANCIALTABLELINE_T3445994354_H
#ifndef MAILALLIANCEREPORTTABLELINE_T2381192320_H
#define MAILALLIANCEREPORTTABLELINE_T2381192320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailAllianceReportTableLine
struct  MailAllianceReportTableLine_t2381192320  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MailAllianceReportTableLine::userName
	Text_t1901882714 * ___userName_2;
	// UnityEngine.UI.Text MailAllianceReportTableLine::type
	Text_t1901882714 * ___type_3;
	// UnityEngine.UI.Text MailAllianceReportTableLine::source
	Text_t1901882714 * ___source_4;
	// UnityEngine.UI.Text MailAllianceReportTableLine::target
	Text_t1901882714 * ___target_5;
	// UnityEngine.UI.Text MailAllianceReportTableLine::time
	Text_t1901882714 * ___time_6;
	// System.Int64 MailAllianceReportTableLine::reportId
	int64_t ___reportId_7;
	// MailAllianceReportTableController MailAllianceReportTableLine::owner
	MailAllianceReportTableController_t2367082911 * ___owner_8;

public:
	inline static int32_t get_offset_of_userName_2() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___userName_2)); }
	inline Text_t1901882714 * get_userName_2() const { return ___userName_2; }
	inline Text_t1901882714 ** get_address_of_userName_2() { return &___userName_2; }
	inline void set_userName_2(Text_t1901882714 * value)
	{
		___userName_2 = value;
		Il2CppCodeGenWriteBarrier((&___userName_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___type_3)); }
	inline Text_t1901882714 * get_type_3() const { return ___type_3; }
	inline Text_t1901882714 ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Text_t1901882714 * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___source_4)); }
	inline Text_t1901882714 * get_source_4() const { return ___source_4; }
	inline Text_t1901882714 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(Text_t1901882714 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___target_5)); }
	inline Text_t1901882714 * get_target_5() const { return ___target_5; }
	inline Text_t1901882714 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Text_t1901882714 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_time_6() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___time_6)); }
	inline Text_t1901882714 * get_time_6() const { return ___time_6; }
	inline Text_t1901882714 ** get_address_of_time_6() { return &___time_6; }
	inline void set_time_6(Text_t1901882714 * value)
	{
		___time_6 = value;
		Il2CppCodeGenWriteBarrier((&___time_6), value);
	}

	inline static int32_t get_offset_of_reportId_7() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___reportId_7)); }
	inline int64_t get_reportId_7() const { return ___reportId_7; }
	inline int64_t* get_address_of_reportId_7() { return &___reportId_7; }
	inline void set_reportId_7(int64_t value)
	{
		___reportId_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t2381192320, ___owner_8)); }
	inline MailAllianceReportTableController_t2367082911 * get_owner_8() const { return ___owner_8; }
	inline MailAllianceReportTableController_t2367082911 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(MailAllianceReportTableController_t2367082911 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILALLIANCEREPORTTABLELINE_T2381192320_H
#ifndef GUESTHOUSETABLELINE_T228100067_H
#define GUESTHOUSETABLELINE_T228100067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuestHouseTableLine
struct  GuestHouseTableLine_t228100067  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text GuestHouseTableLine::knightName
	Text_t1901882714 * ___knightName_2;
	// UnityEngine.UI.Text GuestHouseTableLine::politicsLevel
	Text_t1901882714 * ___politicsLevel_3;
	// UnityEngine.UI.Text GuestHouseTableLine::marchesLevel
	Text_t1901882714 * ___marchesLevel_4;
	// UnityEngine.UI.Text GuestHouseTableLine::speedLevel
	Text_t1901882714 * ___speedLevel_5;
	// UnityEngine.UI.Text GuestHouseTableLine::loyaltyLevel
	Text_t1901882714 * ___loyaltyLevel_6;
	// UnityEngine.UI.Text GuestHouseTableLine::knightLevel
	Text_t1901882714 * ___knightLevel_7;
	// UnityEngine.UI.Text GuestHouseTableLine::salary
	Text_t1901882714 * ___salary_8;
	// UnityEngine.UI.Text GuestHouseTableLine::status
	Text_t1901882714 * ___status_9;
	// UnityEngine.UI.Text GuestHouseTableLine::actionText
	Text_t1901882714 * ___actionText_10;
	// UnityEngine.GameObject GuestHouseTableLine::nameBackground
	GameObject_t1113636619 * ___nameBackground_11;
	// GuestFeastingTableController GuestHouseTableLine::owner
	GuestFeastingTableController_t3137115560 * ___owner_12;
	// ArmyGeneralModel GuestHouseTableLine::general
	ArmyGeneralModel_t737750221 * ___general_13;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___knightName_2)); }
	inline Text_t1901882714 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t1901882714 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t1901882714 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier((&___knightName_2), value);
	}

	inline static int32_t get_offset_of_politicsLevel_3() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___politicsLevel_3)); }
	inline Text_t1901882714 * get_politicsLevel_3() const { return ___politicsLevel_3; }
	inline Text_t1901882714 ** get_address_of_politicsLevel_3() { return &___politicsLevel_3; }
	inline void set_politicsLevel_3(Text_t1901882714 * value)
	{
		___politicsLevel_3 = value;
		Il2CppCodeGenWriteBarrier((&___politicsLevel_3), value);
	}

	inline static int32_t get_offset_of_marchesLevel_4() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___marchesLevel_4)); }
	inline Text_t1901882714 * get_marchesLevel_4() const { return ___marchesLevel_4; }
	inline Text_t1901882714 ** get_address_of_marchesLevel_4() { return &___marchesLevel_4; }
	inline void set_marchesLevel_4(Text_t1901882714 * value)
	{
		___marchesLevel_4 = value;
		Il2CppCodeGenWriteBarrier((&___marchesLevel_4), value);
	}

	inline static int32_t get_offset_of_speedLevel_5() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___speedLevel_5)); }
	inline Text_t1901882714 * get_speedLevel_5() const { return ___speedLevel_5; }
	inline Text_t1901882714 ** get_address_of_speedLevel_5() { return &___speedLevel_5; }
	inline void set_speedLevel_5(Text_t1901882714 * value)
	{
		___speedLevel_5 = value;
		Il2CppCodeGenWriteBarrier((&___speedLevel_5), value);
	}

	inline static int32_t get_offset_of_loyaltyLevel_6() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___loyaltyLevel_6)); }
	inline Text_t1901882714 * get_loyaltyLevel_6() const { return ___loyaltyLevel_6; }
	inline Text_t1901882714 ** get_address_of_loyaltyLevel_6() { return &___loyaltyLevel_6; }
	inline void set_loyaltyLevel_6(Text_t1901882714 * value)
	{
		___loyaltyLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&___loyaltyLevel_6), value);
	}

	inline static int32_t get_offset_of_knightLevel_7() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___knightLevel_7)); }
	inline Text_t1901882714 * get_knightLevel_7() const { return ___knightLevel_7; }
	inline Text_t1901882714 ** get_address_of_knightLevel_7() { return &___knightLevel_7; }
	inline void set_knightLevel_7(Text_t1901882714 * value)
	{
		___knightLevel_7 = value;
		Il2CppCodeGenWriteBarrier((&___knightLevel_7), value);
	}

	inline static int32_t get_offset_of_salary_8() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___salary_8)); }
	inline Text_t1901882714 * get_salary_8() const { return ___salary_8; }
	inline Text_t1901882714 ** get_address_of_salary_8() { return &___salary_8; }
	inline void set_salary_8(Text_t1901882714 * value)
	{
		___salary_8 = value;
		Il2CppCodeGenWriteBarrier((&___salary_8), value);
	}

	inline static int32_t get_offset_of_status_9() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___status_9)); }
	inline Text_t1901882714 * get_status_9() const { return ___status_9; }
	inline Text_t1901882714 ** get_address_of_status_9() { return &___status_9; }
	inline void set_status_9(Text_t1901882714 * value)
	{
		___status_9 = value;
		Il2CppCodeGenWriteBarrier((&___status_9), value);
	}

	inline static int32_t get_offset_of_actionText_10() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___actionText_10)); }
	inline Text_t1901882714 * get_actionText_10() const { return ___actionText_10; }
	inline Text_t1901882714 ** get_address_of_actionText_10() { return &___actionText_10; }
	inline void set_actionText_10(Text_t1901882714 * value)
	{
		___actionText_10 = value;
		Il2CppCodeGenWriteBarrier((&___actionText_10), value);
	}

	inline static int32_t get_offset_of_nameBackground_11() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___nameBackground_11)); }
	inline GameObject_t1113636619 * get_nameBackground_11() const { return ___nameBackground_11; }
	inline GameObject_t1113636619 ** get_address_of_nameBackground_11() { return &___nameBackground_11; }
	inline void set_nameBackground_11(GameObject_t1113636619 * value)
	{
		___nameBackground_11 = value;
		Il2CppCodeGenWriteBarrier((&___nameBackground_11), value);
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___owner_12)); }
	inline GuestFeastingTableController_t3137115560 * get_owner_12() const { return ___owner_12; }
	inline GuestFeastingTableController_t3137115560 ** get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(GuestFeastingTableController_t3137115560 * value)
	{
		___owner_12 = value;
		Il2CppCodeGenWriteBarrier((&___owner_12), value);
	}

	inline static int32_t get_offset_of_general_13() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t228100067, ___general_13)); }
	inline ArmyGeneralModel_t737750221 * get_general_13() const { return ___general_13; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_general_13() { return &___general_13; }
	inline void set_general_13(ArmyGeneralModel_t737750221 * value)
	{
		___general_13 = value;
		Il2CppCodeGenWriteBarrier((&___general_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUESTHOUSETABLELINE_T228100067_H
#ifndef BATTLEREPORTRESOURCESLINE_T3745703875_H
#define BATTLEREPORTRESOURCESLINE_T3745703875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportResourcesLine
struct  BattleReportResourcesLine_t3745703875  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text BattleReportResourcesLine::gold
	Text_t1901882714 * ___gold_2;
	// UnityEngine.UI.Text BattleReportResourcesLine::silver
	Text_t1901882714 * ___silver_3;
	// UnityEngine.UI.Text BattleReportResourcesLine::food
	Text_t1901882714 * ___food_4;
	// UnityEngine.UI.Text BattleReportResourcesLine::wood
	Text_t1901882714 * ___wood_5;
	// UnityEngine.UI.Text BattleReportResourcesLine::iron
	Text_t1901882714 * ___iron_6;
	// UnityEngine.UI.Text BattleReportResourcesLine::copper
	Text_t1901882714 * ___copper_7;
	// UnityEngine.UI.Text BattleReportResourcesLine::stone
	Text_t1901882714 * ___stone_8;

public:
	inline static int32_t get_offset_of_gold_2() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___gold_2)); }
	inline Text_t1901882714 * get_gold_2() const { return ___gold_2; }
	inline Text_t1901882714 ** get_address_of_gold_2() { return &___gold_2; }
	inline void set_gold_2(Text_t1901882714 * value)
	{
		___gold_2 = value;
		Il2CppCodeGenWriteBarrier((&___gold_2), value);
	}

	inline static int32_t get_offset_of_silver_3() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___silver_3)); }
	inline Text_t1901882714 * get_silver_3() const { return ___silver_3; }
	inline Text_t1901882714 ** get_address_of_silver_3() { return &___silver_3; }
	inline void set_silver_3(Text_t1901882714 * value)
	{
		___silver_3 = value;
		Il2CppCodeGenWriteBarrier((&___silver_3), value);
	}

	inline static int32_t get_offset_of_food_4() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___food_4)); }
	inline Text_t1901882714 * get_food_4() const { return ___food_4; }
	inline Text_t1901882714 ** get_address_of_food_4() { return &___food_4; }
	inline void set_food_4(Text_t1901882714 * value)
	{
		___food_4 = value;
		Il2CppCodeGenWriteBarrier((&___food_4), value);
	}

	inline static int32_t get_offset_of_wood_5() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___wood_5)); }
	inline Text_t1901882714 * get_wood_5() const { return ___wood_5; }
	inline Text_t1901882714 ** get_address_of_wood_5() { return &___wood_5; }
	inline void set_wood_5(Text_t1901882714 * value)
	{
		___wood_5 = value;
		Il2CppCodeGenWriteBarrier((&___wood_5), value);
	}

	inline static int32_t get_offset_of_iron_6() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___iron_6)); }
	inline Text_t1901882714 * get_iron_6() const { return ___iron_6; }
	inline Text_t1901882714 ** get_address_of_iron_6() { return &___iron_6; }
	inline void set_iron_6(Text_t1901882714 * value)
	{
		___iron_6 = value;
		Il2CppCodeGenWriteBarrier((&___iron_6), value);
	}

	inline static int32_t get_offset_of_copper_7() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___copper_7)); }
	inline Text_t1901882714 * get_copper_7() const { return ___copper_7; }
	inline Text_t1901882714 ** get_address_of_copper_7() { return &___copper_7; }
	inline void set_copper_7(Text_t1901882714 * value)
	{
		___copper_7 = value;
		Il2CppCodeGenWriteBarrier((&___copper_7), value);
	}

	inline static int32_t get_offset_of_stone_8() { return static_cast<int32_t>(offsetof(BattleReportResourcesLine_t3745703875, ___stone_8)); }
	inline Text_t1901882714 * get_stone_8() const { return ___stone_8; }
	inline Text_t1901882714 ** get_address_of_stone_8() { return &___stone_8; }
	inline void set_stone_8(Text_t1901882714 * value)
	{
		___stone_8 = value;
		Il2CppCodeGenWriteBarrier((&___stone_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTRESOURCESLINE_T3745703875_H
#ifndef ALLIANCEEVENTSLINE_T1491641488_H
#define ALLIANCEEVENTSLINE_T1491641488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventsLine
struct  AllianceEventsLine_t1491641488  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text AllianceEventsLine::date
	Text_t1901882714 * ___date_2;
	// UnityEngine.UI.Text AllianceEventsLine::remark
	Text_t1901882714 * ___remark_3;

public:
	inline static int32_t get_offset_of_date_2() { return static_cast<int32_t>(offsetof(AllianceEventsLine_t1491641488, ___date_2)); }
	inline Text_t1901882714 * get_date_2() const { return ___date_2; }
	inline Text_t1901882714 ** get_address_of_date_2() { return &___date_2; }
	inline void set_date_2(Text_t1901882714 * value)
	{
		___date_2 = value;
		Il2CppCodeGenWriteBarrier((&___date_2), value);
	}

	inline static int32_t get_offset_of_remark_3() { return static_cast<int32_t>(offsetof(AllianceEventsLine_t1491641488, ___remark_3)); }
	inline Text_t1901882714 * get_remark_3() const { return ___remark_3; }
	inline Text_t1901882714 ** get_address_of_remark_3() { return &___remark_3; }
	inline void set_remark_3(Text_t1901882714 * value)
	{
		___remark_3 = value;
		Il2CppCodeGenWriteBarrier((&___remark_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEEVENTSLINE_T1491641488_H
#ifndef ALLIANCEALLIANCESLINE_T2112943226_H
#define ALLIANCEALLIANCESLINE_T2112943226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceAlliancesLine
struct  AllianceAlliancesLine_t2112943226  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text AllianceAlliancesLine::allianceName
	Text_t1901882714 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceAlliancesLine::founder
	Text_t1901882714 * ___founder_3;
	// UnityEngine.UI.Text AllianceAlliancesLine::leader
	Text_t1901882714 * ___leader_4;
	// UnityEngine.UI.Text AllianceAlliancesLine::experience
	Text_t1901882714 * ___experience_5;
	// UnityEngine.UI.Text AllianceAlliancesLine::rank
	Text_t1901882714 * ___rank_6;
	// UnityEngine.UI.Text AllianceAlliancesLine::members
	Text_t1901882714 * ___members_7;
	// UnityEngine.GameObject AllianceAlliancesLine::joinBtn
	GameObject_t1113636619 * ___joinBtn_8;
	// System.Int64 AllianceAlliancesLine::allianceId
	int64_t ___allianceId_9;
	// AllianceAlliancesTableController AllianceAlliancesLine::owner
	AllianceAlliancesTableController_t3866927190 * ___owner_10;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___allianceName_2)); }
	inline Text_t1901882714 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t1901882714 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t1901882714 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_2), value);
	}

	inline static int32_t get_offset_of_founder_3() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___founder_3)); }
	inline Text_t1901882714 * get_founder_3() const { return ___founder_3; }
	inline Text_t1901882714 ** get_address_of_founder_3() { return &___founder_3; }
	inline void set_founder_3(Text_t1901882714 * value)
	{
		___founder_3 = value;
		Il2CppCodeGenWriteBarrier((&___founder_3), value);
	}

	inline static int32_t get_offset_of_leader_4() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___leader_4)); }
	inline Text_t1901882714 * get_leader_4() const { return ___leader_4; }
	inline Text_t1901882714 ** get_address_of_leader_4() { return &___leader_4; }
	inline void set_leader_4(Text_t1901882714 * value)
	{
		___leader_4 = value;
		Il2CppCodeGenWriteBarrier((&___leader_4), value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___experience_5)); }
	inline Text_t1901882714 * get_experience_5() const { return ___experience_5; }
	inline Text_t1901882714 ** get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(Text_t1901882714 * value)
	{
		___experience_5 = value;
		Il2CppCodeGenWriteBarrier((&___experience_5), value);
	}

	inline static int32_t get_offset_of_rank_6() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___rank_6)); }
	inline Text_t1901882714 * get_rank_6() const { return ___rank_6; }
	inline Text_t1901882714 ** get_address_of_rank_6() { return &___rank_6; }
	inline void set_rank_6(Text_t1901882714 * value)
	{
		___rank_6 = value;
		Il2CppCodeGenWriteBarrier((&___rank_6), value);
	}

	inline static int32_t get_offset_of_members_7() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___members_7)); }
	inline Text_t1901882714 * get_members_7() const { return ___members_7; }
	inline Text_t1901882714 ** get_address_of_members_7() { return &___members_7; }
	inline void set_members_7(Text_t1901882714 * value)
	{
		___members_7 = value;
		Il2CppCodeGenWriteBarrier((&___members_7), value);
	}

	inline static int32_t get_offset_of_joinBtn_8() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___joinBtn_8)); }
	inline GameObject_t1113636619 * get_joinBtn_8() const { return ___joinBtn_8; }
	inline GameObject_t1113636619 ** get_address_of_joinBtn_8() { return &___joinBtn_8; }
	inline void set_joinBtn_8(GameObject_t1113636619 * value)
	{
		___joinBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___joinBtn_8), value);
	}

	inline static int32_t get_offset_of_allianceId_9() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___allianceId_9)); }
	inline int64_t get_allianceId_9() const { return ___allianceId_9; }
	inline int64_t* get_address_of_allianceId_9() { return &___allianceId_9; }
	inline void set_allianceId_9(int64_t value)
	{
		___allianceId_9 = value;
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t2112943226, ___owner_10)); }
	inline AllianceAlliancesTableController_t3866927190 * get_owner_10() const { return ___owner_10; }
	inline AllianceAlliancesTableController_t3866927190 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(AllianceAlliancesTableController_t3866927190 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier((&___owner_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEALLIANCESLINE_T2112943226_H
#ifndef ALLIANCEINFOALLIANCESLINE_T1537220308_H
#define ALLIANCEINFOALLIANCESLINE_T1537220308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInfoAlliancesLine
struct  AllianceInfoAlliancesLine_t1537220308  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::allianceName
	Text_t1901882714 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::rank
	Text_t1901882714 * ___rank_3;
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::members
	Text_t1901882714 * ___members_4;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::neutralBTN
	GameObject_t1113636619 * ___neutralBTN_5;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::allyBTN
	GameObject_t1113636619 * ___allyBTN_6;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::enemyBTN
	GameObject_t1113636619 * ___enemyBTN_7;
	// AllianceInfoTableContoller AllianceInfoAlliancesLine::owner
	AllianceInfoTableContoller_t4165916044 * ___owner_8;
	// System.Int64 AllianceInfoAlliancesLine::allianceId
	int64_t ___allianceId_9;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___allianceName_2)); }
	inline Text_t1901882714 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t1901882714 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t1901882714 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier((&___allianceName_2), value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___rank_3)); }
	inline Text_t1901882714 * get_rank_3() const { return ___rank_3; }
	inline Text_t1901882714 ** get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(Text_t1901882714 * value)
	{
		___rank_3 = value;
		Il2CppCodeGenWriteBarrier((&___rank_3), value);
	}

	inline static int32_t get_offset_of_members_4() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___members_4)); }
	inline Text_t1901882714 * get_members_4() const { return ___members_4; }
	inline Text_t1901882714 ** get_address_of_members_4() { return &___members_4; }
	inline void set_members_4(Text_t1901882714 * value)
	{
		___members_4 = value;
		Il2CppCodeGenWriteBarrier((&___members_4), value);
	}

	inline static int32_t get_offset_of_neutralBTN_5() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___neutralBTN_5)); }
	inline GameObject_t1113636619 * get_neutralBTN_5() const { return ___neutralBTN_5; }
	inline GameObject_t1113636619 ** get_address_of_neutralBTN_5() { return &___neutralBTN_5; }
	inline void set_neutralBTN_5(GameObject_t1113636619 * value)
	{
		___neutralBTN_5 = value;
		Il2CppCodeGenWriteBarrier((&___neutralBTN_5), value);
	}

	inline static int32_t get_offset_of_allyBTN_6() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___allyBTN_6)); }
	inline GameObject_t1113636619 * get_allyBTN_6() const { return ___allyBTN_6; }
	inline GameObject_t1113636619 ** get_address_of_allyBTN_6() { return &___allyBTN_6; }
	inline void set_allyBTN_6(GameObject_t1113636619 * value)
	{
		___allyBTN_6 = value;
		Il2CppCodeGenWriteBarrier((&___allyBTN_6), value);
	}

	inline static int32_t get_offset_of_enemyBTN_7() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___enemyBTN_7)); }
	inline GameObject_t1113636619 * get_enemyBTN_7() const { return ___enemyBTN_7; }
	inline GameObject_t1113636619 ** get_address_of_enemyBTN_7() { return &___enemyBTN_7; }
	inline void set_enemyBTN_7(GameObject_t1113636619 * value)
	{
		___enemyBTN_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemyBTN_7), value);
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___owner_8)); }
	inline AllianceInfoTableContoller_t4165916044 * get_owner_8() const { return ___owner_8; }
	inline AllianceInfoTableContoller_t4165916044 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(AllianceInfoTableContoller_t4165916044 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_allianceId_9() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t1537220308, ___allianceId_9)); }
	inline int64_t get_allianceId_9() const { return ___allianceId_9; }
	inline int64_t* get_address_of_allianceId_9() { return &___allianceId_9; }
	inline void set_allianceId_9(int64_t value)
	{
		___allianceId_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEINFOALLIANCESLINE_T1537220308_H
#ifndef BATTLEREPORTITEMSLINE_T1544447758_H
#define BATTLEREPORTITEMSLINE_T1544447758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportItemsLine
struct  BattleReportItemsLine_t1544447758  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text BattleReportItemsLine::itemName
	Text_t1901882714 * ___itemName_2;
	// UnityEngine.UI.Text BattleReportItemsLine::itemPrice
	Text_t1901882714 * ___itemPrice_3;
	// UnityEngine.UI.Text BattleReportItemsLine::itemCount
	Text_t1901882714 * ___itemCount_4;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t1544447758, ___itemName_2)); }
	inline Text_t1901882714 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t1901882714 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t1901882714 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemName_2), value);
	}

	inline static int32_t get_offset_of_itemPrice_3() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t1544447758, ___itemPrice_3)); }
	inline Text_t1901882714 * get_itemPrice_3() const { return ___itemPrice_3; }
	inline Text_t1901882714 ** get_address_of_itemPrice_3() { return &___itemPrice_3; }
	inline void set_itemPrice_3(Text_t1901882714 * value)
	{
		___itemPrice_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrice_3), value);
	}

	inline static int32_t get_offset_of_itemCount_4() { return static_cast<int32_t>(offsetof(BattleReportItemsLine_t1544447758, ___itemCount_4)); }
	inline Text_t1901882714 * get_itemCount_4() const { return ___itemCount_4; }
	inline Text_t1901882714 ** get_address_of_itemCount_4() { return &___itemCount_4; }
	inline void set_itemCount_4(Text_t1901882714 * value)
	{
		___itemCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREPORTITEMSLINE_T1544447758_H
#ifndef ALLIANCEMEMBERSPERMISSIONSLINE_T2063553011_H
#define ALLIANCEMEMBERSPERMISSIONSLINE_T2063553011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceMembersPermissionsLine
struct  AllianceMembersPermissionsLine_t2063553011  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::memberName
	Text_t1901882714 * ___memberName_2;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::position
	Text_t1901882714 * ___position_3;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::rank
	Text_t1901882714 * ___rank_4;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::experience
	Text_t1901882714 * ___experience_5;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::cities
	Text_t1901882714 * ___cities_6;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::capitalName
	Text_t1901882714 * ___capitalName_7;
	// UnityEngine.UI.Text AllianceMembersPermissionsLine::lastOnline
	Text_t1901882714 * ___lastOnline_8;
	// UserModel AllianceMembersPermissionsLine::user
	UserModel_t1353931605 * ___user_9;

public:
	inline static int32_t get_offset_of_memberName_2() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___memberName_2)); }
	inline Text_t1901882714 * get_memberName_2() const { return ___memberName_2; }
	inline Text_t1901882714 ** get_address_of_memberName_2() { return &___memberName_2; }
	inline void set_memberName_2(Text_t1901882714 * value)
	{
		___memberName_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberName_2), value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___position_3)); }
	inline Text_t1901882714 * get_position_3() const { return ___position_3; }
	inline Text_t1901882714 ** get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Text_t1901882714 * value)
	{
		___position_3 = value;
		Il2CppCodeGenWriteBarrier((&___position_3), value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___rank_4)); }
	inline Text_t1901882714 * get_rank_4() const { return ___rank_4; }
	inline Text_t1901882714 ** get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(Text_t1901882714 * value)
	{
		___rank_4 = value;
		Il2CppCodeGenWriteBarrier((&___rank_4), value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___experience_5)); }
	inline Text_t1901882714 * get_experience_5() const { return ___experience_5; }
	inline Text_t1901882714 ** get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(Text_t1901882714 * value)
	{
		___experience_5 = value;
		Il2CppCodeGenWriteBarrier((&___experience_5), value);
	}

	inline static int32_t get_offset_of_cities_6() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___cities_6)); }
	inline Text_t1901882714 * get_cities_6() const { return ___cities_6; }
	inline Text_t1901882714 ** get_address_of_cities_6() { return &___cities_6; }
	inline void set_cities_6(Text_t1901882714 * value)
	{
		___cities_6 = value;
		Il2CppCodeGenWriteBarrier((&___cities_6), value);
	}

	inline static int32_t get_offset_of_capitalName_7() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___capitalName_7)); }
	inline Text_t1901882714 * get_capitalName_7() const { return ___capitalName_7; }
	inline Text_t1901882714 ** get_address_of_capitalName_7() { return &___capitalName_7; }
	inline void set_capitalName_7(Text_t1901882714 * value)
	{
		___capitalName_7 = value;
		Il2CppCodeGenWriteBarrier((&___capitalName_7), value);
	}

	inline static int32_t get_offset_of_lastOnline_8() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___lastOnline_8)); }
	inline Text_t1901882714 * get_lastOnline_8() const { return ___lastOnline_8; }
	inline Text_t1901882714 ** get_address_of_lastOnline_8() { return &___lastOnline_8; }
	inline void set_lastOnline_8(Text_t1901882714 * value)
	{
		___lastOnline_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnline_8), value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(AllianceMembersPermissionsLine_t2063553011, ___user_9)); }
	inline UserModel_t1353931605 * get_user_9() const { return ___user_9; }
	inline UserModel_t1353931605 ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(UserModel_t1353931605 * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier((&___user_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEMEMBERSPERMISSIONSLINE_T2063553011_H
#ifndef PANELKNIGHTTABLELINE_T48687488_H
#define PANELKNIGHTTABLELINE_T48687488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelKnightTableLine
struct  PanelKnightTableLine_t48687488  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text PanelKnightTableLine::knightName
	Text_t1901882714 * ___knightName_2;
	// UnityEngine.UI.Text PanelKnightTableLine::knightLevel
	Text_t1901882714 * ___knightLevel_3;
	// UnityEngine.UI.Text PanelKnightTableLine::status
	Text_t1901882714 * ___status_4;
	// System.Int64 PanelKnightTableLine::knightId
	int64_t ___knightId_5;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t48687488, ___knightName_2)); }
	inline Text_t1901882714 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t1901882714 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t1901882714 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier((&___knightName_2), value);
	}

	inline static int32_t get_offset_of_knightLevel_3() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t48687488, ___knightLevel_3)); }
	inline Text_t1901882714 * get_knightLevel_3() const { return ___knightLevel_3; }
	inline Text_t1901882714 ** get_address_of_knightLevel_3() { return &___knightLevel_3; }
	inline void set_knightLevel_3(Text_t1901882714 * value)
	{
		___knightLevel_3 = value;
		Il2CppCodeGenWriteBarrier((&___knightLevel_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t48687488, ___status_4)); }
	inline Text_t1901882714 * get_status_4() const { return ___status_4; }
	inline Text_t1901882714 ** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(Text_t1901882714 * value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier((&___status_4), value);
	}

	inline static int32_t get_offset_of_knightId_5() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t48687488, ___knightId_5)); }
	inline int64_t get_knightId_5() const { return ___knightId_5; }
	inline int64_t* get_address_of_knightId_5() { return &___knightId_5; }
	inline void set_knightId_5(int64_t value)
	{
		___knightId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELKNIGHTTABLELINE_T48687488_H
#ifndef NEWMAILPLAYERLINE_T272747317_H
#define NEWMAILPLAYERLINE_T272747317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailPlayerLine
struct  NewMailPlayerLine_t272747317  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text NewMailPlayerLine::playeName
	Text_t1901882714 * ___playeName_2;
	// System.Int64 NewMailPlayerLine::playerId
	int64_t ___playerId_3;
	// NewMailContentManager NewMailPlayerLine::manager
	NewMailContentManager_t1552843396 * ___manager_4;

public:
	inline static int32_t get_offset_of_playeName_2() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t272747317, ___playeName_2)); }
	inline Text_t1901882714 * get_playeName_2() const { return ___playeName_2; }
	inline Text_t1901882714 ** get_address_of_playeName_2() { return &___playeName_2; }
	inline void set_playeName_2(Text_t1901882714 * value)
	{
		___playeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___playeName_2), value);
	}

	inline static int32_t get_offset_of_playerId_3() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t272747317, ___playerId_3)); }
	inline int64_t get_playerId_3() const { return ___playerId_3; }
	inline int64_t* get_address_of_playerId_3() { return &___playerId_3; }
	inline void set_playerId_3(int64_t value)
	{
		___playerId_3 = value;
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(NewMailPlayerLine_t272747317, ___manager_4)); }
	inline NewMailContentManager_t1552843396 * get_manager_4() const { return ___manager_4; }
	inline NewMailContentManager_t1552843396 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(NewMailContentManager_t1552843396 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWMAILPLAYERLINE_T272747317_H
#ifndef PANELTABLELINE_T11631953_H
#define PANELTABLELINE_T11631953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTableLine
struct  PanelTableLine_t11631953  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Image PanelTableLine::actionImage
	Image_t2670269651 * ___actionImage_2;
	// UnityEngine.UI.Image PanelTableLine::itemImage
	Image_t2670269651 * ___itemImage_3;
	// UnityEngine.UI.Text PanelTableLine::itemText
	Text_t1901882714 * ___itemText_4;
	// UnityEngine.UI.Text PanelTableLine::itemTimer
	Text_t1901882714 * ___itemTimer_5;
	// UnityEngine.GameObject PanelTableLine::speedUpBTN
	GameObject_t1113636619 * ___speedUpBTN_6;
	// UnityEngine.GameObject PanelTableLine::cancelBTN
	GameObject_t1113636619 * ___cancelBTN_7;
	// UnityEngine.RectTransform PanelTableLine::progressBar
	RectTransform_t3704657025 * ___progressBar_8;
	// System.Int64 PanelTableLine::eventId
	int64_t ___eventId_9;
	// System.Int64 PanelTableLine::itemId
	int64_t ___itemId_10;
	// PanelTableController PanelTableLine::owner
	PanelTableController_t3691279756 * ___owner_11;
	// System.DateTime PanelTableLine::startTime
	DateTime_t3738529785  ___startTime_12;
	// System.DateTime PanelTableLine::finishTime
	DateTime_t3738529785  ___finishTime_13;
	// System.Boolean PanelTableLine::cityBuilding
	bool ___cityBuilding_14;
	// System.TimeSpan PanelTableLine::timespan
	TimeSpan_t881159249  ___timespan_15;
	// System.Single PanelTableLine::valuePerSecond
	float ___valuePerSecond_16;

public:
	inline static int32_t get_offset_of_actionImage_2() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___actionImage_2)); }
	inline Image_t2670269651 * get_actionImage_2() const { return ___actionImage_2; }
	inline Image_t2670269651 ** get_address_of_actionImage_2() { return &___actionImage_2; }
	inline void set_actionImage_2(Image_t2670269651 * value)
	{
		___actionImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___actionImage_2), value);
	}

	inline static int32_t get_offset_of_itemImage_3() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___itemImage_3)); }
	inline Image_t2670269651 * get_itemImage_3() const { return ___itemImage_3; }
	inline Image_t2670269651 ** get_address_of_itemImage_3() { return &___itemImage_3; }
	inline void set_itemImage_3(Image_t2670269651 * value)
	{
		___itemImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage_3), value);
	}

	inline static int32_t get_offset_of_itemText_4() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___itemText_4)); }
	inline Text_t1901882714 * get_itemText_4() const { return ___itemText_4; }
	inline Text_t1901882714 ** get_address_of_itemText_4() { return &___itemText_4; }
	inline void set_itemText_4(Text_t1901882714 * value)
	{
		___itemText_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemText_4), value);
	}

	inline static int32_t get_offset_of_itemTimer_5() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___itemTimer_5)); }
	inline Text_t1901882714 * get_itemTimer_5() const { return ___itemTimer_5; }
	inline Text_t1901882714 ** get_address_of_itemTimer_5() { return &___itemTimer_5; }
	inline void set_itemTimer_5(Text_t1901882714 * value)
	{
		___itemTimer_5 = value;
		Il2CppCodeGenWriteBarrier((&___itemTimer_5), value);
	}

	inline static int32_t get_offset_of_speedUpBTN_6() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___speedUpBTN_6)); }
	inline GameObject_t1113636619 * get_speedUpBTN_6() const { return ___speedUpBTN_6; }
	inline GameObject_t1113636619 ** get_address_of_speedUpBTN_6() { return &___speedUpBTN_6; }
	inline void set_speedUpBTN_6(GameObject_t1113636619 * value)
	{
		___speedUpBTN_6 = value;
		Il2CppCodeGenWriteBarrier((&___speedUpBTN_6), value);
	}

	inline static int32_t get_offset_of_cancelBTN_7() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___cancelBTN_7)); }
	inline GameObject_t1113636619 * get_cancelBTN_7() const { return ___cancelBTN_7; }
	inline GameObject_t1113636619 ** get_address_of_cancelBTN_7() { return &___cancelBTN_7; }
	inline void set_cancelBTN_7(GameObject_t1113636619 * value)
	{
		___cancelBTN_7 = value;
		Il2CppCodeGenWriteBarrier((&___cancelBTN_7), value);
	}

	inline static int32_t get_offset_of_progressBar_8() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___progressBar_8)); }
	inline RectTransform_t3704657025 * get_progressBar_8() const { return ___progressBar_8; }
	inline RectTransform_t3704657025 ** get_address_of_progressBar_8() { return &___progressBar_8; }
	inline void set_progressBar_8(RectTransform_t3704657025 * value)
	{
		___progressBar_8 = value;
		Il2CppCodeGenWriteBarrier((&___progressBar_8), value);
	}

	inline static int32_t get_offset_of_eventId_9() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___eventId_9)); }
	inline int64_t get_eventId_9() const { return ___eventId_9; }
	inline int64_t* get_address_of_eventId_9() { return &___eventId_9; }
	inline void set_eventId_9(int64_t value)
	{
		___eventId_9 = value;
	}

	inline static int32_t get_offset_of_itemId_10() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___itemId_10)); }
	inline int64_t get_itemId_10() const { return ___itemId_10; }
	inline int64_t* get_address_of_itemId_10() { return &___itemId_10; }
	inline void set_itemId_10(int64_t value)
	{
		___itemId_10 = value;
	}

	inline static int32_t get_offset_of_owner_11() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___owner_11)); }
	inline PanelTableController_t3691279756 * get_owner_11() const { return ___owner_11; }
	inline PanelTableController_t3691279756 ** get_address_of_owner_11() { return &___owner_11; }
	inline void set_owner_11(PanelTableController_t3691279756 * value)
	{
		___owner_11 = value;
		Il2CppCodeGenWriteBarrier((&___owner_11), value);
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___startTime_12)); }
	inline DateTime_t3738529785  get_startTime_12() const { return ___startTime_12; }
	inline DateTime_t3738529785 * get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(DateTime_t3738529785  value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_finishTime_13() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___finishTime_13)); }
	inline DateTime_t3738529785  get_finishTime_13() const { return ___finishTime_13; }
	inline DateTime_t3738529785 * get_address_of_finishTime_13() { return &___finishTime_13; }
	inline void set_finishTime_13(DateTime_t3738529785  value)
	{
		___finishTime_13 = value;
	}

	inline static int32_t get_offset_of_cityBuilding_14() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___cityBuilding_14)); }
	inline bool get_cityBuilding_14() const { return ___cityBuilding_14; }
	inline bool* get_address_of_cityBuilding_14() { return &___cityBuilding_14; }
	inline void set_cityBuilding_14(bool value)
	{
		___cityBuilding_14 = value;
	}

	inline static int32_t get_offset_of_timespan_15() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___timespan_15)); }
	inline TimeSpan_t881159249  get_timespan_15() const { return ___timespan_15; }
	inline TimeSpan_t881159249 * get_address_of_timespan_15() { return &___timespan_15; }
	inline void set_timespan_15(TimeSpan_t881159249  value)
	{
		___timespan_15 = value;
	}

	inline static int32_t get_offset_of_valuePerSecond_16() { return static_cast<int32_t>(offsetof(PanelTableLine_t11631953, ___valuePerSecond_16)); }
	inline float get_valuePerSecond_16() const { return ___valuePerSecond_16; }
	inline float* get_address_of_valuePerSecond_16() { return &___valuePerSecond_16; }
	inline void set_valuePerSecond_16(float value)
	{
		___valuePerSecond_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELTABLELINE_T11631953_H
#ifndef RESIDENCECITIESTABLELINE_T3242262530_H
#define RESIDENCECITIESTABLELINE_T3242262530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceCitiesTableLine
struct  ResidenceCitiesTableLine_t3242262530  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text ResidenceCitiesTableLine::cityName
	Text_t1901882714 * ___cityName_2;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::region
	Text_t1901882714 * ___region_3;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::cityCoords
	Text_t1901882714 * ___cityCoords_4;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::mayerLevel
	Text_t1901882714 * ___mayerLevel_5;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::fieldsCount
	Text_t1901882714 * ___fieldsCount_6;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::valleysCount
	Text_t1901882714 * ___valleysCount_7;
	// ResidenceCitiesTableController ResidenceCitiesTableLine::owner
	ResidenceCitiesTableController_t4096198820 * ___owner_8;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___cityName_2)); }
	inline Text_t1901882714 * get_cityName_2() const { return ___cityName_2; }
	inline Text_t1901882714 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(Text_t1901882714 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_2), value);
	}

	inline static int32_t get_offset_of_region_3() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___region_3)); }
	inline Text_t1901882714 * get_region_3() const { return ___region_3; }
	inline Text_t1901882714 ** get_address_of_region_3() { return &___region_3; }
	inline void set_region_3(Text_t1901882714 * value)
	{
		___region_3 = value;
		Il2CppCodeGenWriteBarrier((&___region_3), value);
	}

	inline static int32_t get_offset_of_cityCoords_4() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___cityCoords_4)); }
	inline Text_t1901882714 * get_cityCoords_4() const { return ___cityCoords_4; }
	inline Text_t1901882714 ** get_address_of_cityCoords_4() { return &___cityCoords_4; }
	inline void set_cityCoords_4(Text_t1901882714 * value)
	{
		___cityCoords_4 = value;
		Il2CppCodeGenWriteBarrier((&___cityCoords_4), value);
	}

	inline static int32_t get_offset_of_mayerLevel_5() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___mayerLevel_5)); }
	inline Text_t1901882714 * get_mayerLevel_5() const { return ___mayerLevel_5; }
	inline Text_t1901882714 ** get_address_of_mayerLevel_5() { return &___mayerLevel_5; }
	inline void set_mayerLevel_5(Text_t1901882714 * value)
	{
		___mayerLevel_5 = value;
		Il2CppCodeGenWriteBarrier((&___mayerLevel_5), value);
	}

	inline static int32_t get_offset_of_fieldsCount_6() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___fieldsCount_6)); }
	inline Text_t1901882714 * get_fieldsCount_6() const { return ___fieldsCount_6; }
	inline Text_t1901882714 ** get_address_of_fieldsCount_6() { return &___fieldsCount_6; }
	inline void set_fieldsCount_6(Text_t1901882714 * value)
	{
		___fieldsCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___fieldsCount_6), value);
	}

	inline static int32_t get_offset_of_valleysCount_7() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___valleysCount_7)); }
	inline Text_t1901882714 * get_valleysCount_7() const { return ___valleysCount_7; }
	inline Text_t1901882714 ** get_address_of_valleysCount_7() { return &___valleysCount_7; }
	inline void set_valleysCount_7(Text_t1901882714 * value)
	{
		___valleysCount_7 = value;
		Il2CppCodeGenWriteBarrier((&___valleysCount_7), value);
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t3242262530, ___owner_8)); }
	inline ResidenceCitiesTableController_t4096198820 * get_owner_8() const { return ___owner_8; }
	inline ResidenceCitiesTableController_t4096198820 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(ResidenceCitiesTableController_t4096198820 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCECITIESTABLELINE_T3242262530_H
#ifndef RESIDENCEBUILDINGSTABLELINE_T2264296562_H
#define RESIDENCEBUILDINGSTABLELINE_T2264296562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceBuildingsTableLine
struct  ResidenceBuildingsTableLine_t2264296562  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Image ResidenceBuildingsTableLine::buildingIcon
	Image_t2670269651 * ___buildingIcon_2;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::buildingName
	Text_t1901882714 * ___buildingName_3;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::buildingLevel
	Text_t1901882714 * ___buildingLevel_4;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::status
	Text_t1901882714 * ___status_5;
	// System.String ResidenceBuildingsTableLine::type
	String_t* ___type_6;
	// ResidenceBuildingsTableController ResidenceBuildingsTableLine::owner
	ResidenceBuildingsTableController_t4099106534 * ___owner_7;
	// System.Int64 ResidenceBuildingsTableLine::pitId
	int64_t ___pitId_8;

public:
	inline static int32_t get_offset_of_buildingIcon_2() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___buildingIcon_2)); }
	inline Image_t2670269651 * get_buildingIcon_2() const { return ___buildingIcon_2; }
	inline Image_t2670269651 ** get_address_of_buildingIcon_2() { return &___buildingIcon_2; }
	inline void set_buildingIcon_2(Image_t2670269651 * value)
	{
		___buildingIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildingIcon_2), value);
	}

	inline static int32_t get_offset_of_buildingName_3() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___buildingName_3)); }
	inline Text_t1901882714 * get_buildingName_3() const { return ___buildingName_3; }
	inline Text_t1901882714 ** get_address_of_buildingName_3() { return &___buildingName_3; }
	inline void set_buildingName_3(Text_t1901882714 * value)
	{
		___buildingName_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingName_3), value);
	}

	inline static int32_t get_offset_of_buildingLevel_4() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___buildingLevel_4)); }
	inline Text_t1901882714 * get_buildingLevel_4() const { return ___buildingLevel_4; }
	inline Text_t1901882714 ** get_address_of_buildingLevel_4() { return &___buildingLevel_4; }
	inline void set_buildingLevel_4(Text_t1901882714 * value)
	{
		___buildingLevel_4 = value;
		Il2CppCodeGenWriteBarrier((&___buildingLevel_4), value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___status_5)); }
	inline Text_t1901882714 * get_status_5() const { return ___status_5; }
	inline Text_t1901882714 ** get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(Text_t1901882714 * value)
	{
		___status_5 = value;
		Il2CppCodeGenWriteBarrier((&___status_5), value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___owner_7)); }
	inline ResidenceBuildingsTableController_t4099106534 * get_owner_7() const { return ___owner_7; }
	inline ResidenceBuildingsTableController_t4099106534 ** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(ResidenceBuildingsTableController_t4099106534 * value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier((&___owner_7), value);
	}

	inline static int32_t get_offset_of_pitId_8() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t2264296562, ___pitId_8)); }
	inline int64_t get_pitId_8() const { return ___pitId_8; }
	inline int64_t* get_address_of_pitId_8() { return &___pitId_8; }
	inline void set_pitId_8(int64_t value)
	{
		___pitId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIDENCEBUILDINGSTABLELINE_T2264296562_H
#ifndef MARKETSELLTABLELINE_T228365181_H
#define MARKETSELLTABLELINE_T228365181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketSellTableLine
struct  MarketSellTableLine_t228365181  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MarketSellTableLine::quantity
	Text_t1901882714 * ___quantity_2;
	// UnityEngine.UI.Text MarketSellTableLine::unitPrice
	Text_t1901882714 * ___unitPrice_3;
	// System.Int64 MarketSellTableLine::lotId
	int64_t ___lotId_4;
	// MarketManager MarketSellTableLine::owner
	MarketManager_t880414981 * ___owner_5;

public:
	inline static int32_t get_offset_of_quantity_2() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t228365181, ___quantity_2)); }
	inline Text_t1901882714 * get_quantity_2() const { return ___quantity_2; }
	inline Text_t1901882714 ** get_address_of_quantity_2() { return &___quantity_2; }
	inline void set_quantity_2(Text_t1901882714 * value)
	{
		___quantity_2 = value;
		Il2CppCodeGenWriteBarrier((&___quantity_2), value);
	}

	inline static int32_t get_offset_of_unitPrice_3() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t228365181, ___unitPrice_3)); }
	inline Text_t1901882714 * get_unitPrice_3() const { return ___unitPrice_3; }
	inline Text_t1901882714 ** get_address_of_unitPrice_3() { return &___unitPrice_3; }
	inline void set_unitPrice_3(Text_t1901882714 * value)
	{
		___unitPrice_3 = value;
		Il2CppCodeGenWriteBarrier((&___unitPrice_3), value);
	}

	inline static int32_t get_offset_of_lotId_4() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t228365181, ___lotId_4)); }
	inline int64_t get_lotId_4() const { return ___lotId_4; }
	inline int64_t* get_address_of_lotId_4() { return &___lotId_4; }
	inline void set_lotId_4(int64_t value)
	{
		___lotId_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t228365181, ___owner_5)); }
	inline MarketManager_t880414981 * get_owner_5() const { return ___owner_5; }
	inline MarketManager_t880414981 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(MarketManager_t880414981 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETSELLTABLELINE_T228365181_H
#ifndef MAILINBOXLINE_T3110909470_H
#define MAILINBOXLINE_T3110909470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInboxLine
struct  MailInboxLine_t3110909470  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MailInboxLine::playerName
	Text_t1901882714 * ___playerName_2;
	// UnityEngine.UI.Text MailInboxLine::subject
	Text_t1901882714 * ___subject_3;
	// UnityEngine.UI.Text MailInboxLine::date
	Text_t1901882714 * ___date_4;
	// System.Int64 MailInboxLine::messageId
	int64_t ___messageId_5;
	// MailInboxTableController MailInboxLine::owner
	MailInboxTableController_t2667561573 * ___owner_6;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(MailInboxLine_t3110909470, ___playerName_2)); }
	inline Text_t1901882714 * get_playerName_2() const { return ___playerName_2; }
	inline Text_t1901882714 ** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(Text_t1901882714 * value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_2), value);
	}

	inline static int32_t get_offset_of_subject_3() { return static_cast<int32_t>(offsetof(MailInboxLine_t3110909470, ___subject_3)); }
	inline Text_t1901882714 * get_subject_3() const { return ___subject_3; }
	inline Text_t1901882714 ** get_address_of_subject_3() { return &___subject_3; }
	inline void set_subject_3(Text_t1901882714 * value)
	{
		___subject_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_3), value);
	}

	inline static int32_t get_offset_of_date_4() { return static_cast<int32_t>(offsetof(MailInboxLine_t3110909470, ___date_4)); }
	inline Text_t1901882714 * get_date_4() const { return ___date_4; }
	inline Text_t1901882714 ** get_address_of_date_4() { return &___date_4; }
	inline void set_date_4(Text_t1901882714 * value)
	{
		___date_4 = value;
		Il2CppCodeGenWriteBarrier((&___date_4), value);
	}

	inline static int32_t get_offset_of_messageId_5() { return static_cast<int32_t>(offsetof(MailInboxLine_t3110909470, ___messageId_5)); }
	inline int64_t get_messageId_5() const { return ___messageId_5; }
	inline int64_t* get_address_of_messageId_5() { return &___messageId_5; }
	inline void set_messageId_5(int64_t value)
	{
		___messageId_5 = value;
	}

	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(MailInboxLine_t3110909470, ___owner_6)); }
	inline MailInboxTableController_t2667561573 * get_owner_6() const { return ___owner_6; }
	inline MailInboxTableController_t2667561573 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(MailInboxTableController_t2667561573 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier((&___owner_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILINBOXLINE_T3110909470_H
#ifndef MAILINVITELINE_T4144392430_H
#define MAILINVITELINE_T4144392430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInviteLine
struct  MailInviteLine_t4144392430  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MailInviteLine::senderName
	Text_t1901882714 * ___senderName_2;
	// UnityEngine.UI.Text MailInviteLine::date
	Text_t1901882714 * ___date_3;
	// MailInviteTableController MailInviteLine::owner
	MailInviteTableController_t1579929683 * ___owner_4;
	// System.Int64 MailInviteLine::inviteId
	int64_t ___inviteId_5;

public:
	inline static int32_t get_offset_of_senderName_2() { return static_cast<int32_t>(offsetof(MailInviteLine_t4144392430, ___senderName_2)); }
	inline Text_t1901882714 * get_senderName_2() const { return ___senderName_2; }
	inline Text_t1901882714 ** get_address_of_senderName_2() { return &___senderName_2; }
	inline void set_senderName_2(Text_t1901882714 * value)
	{
		___senderName_2 = value;
		Il2CppCodeGenWriteBarrier((&___senderName_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(MailInviteLine_t4144392430, ___date_3)); }
	inline Text_t1901882714 * get_date_3() const { return ___date_3; }
	inline Text_t1901882714 ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(Text_t1901882714 * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier((&___date_3), value);
	}

	inline static int32_t get_offset_of_owner_4() { return static_cast<int32_t>(offsetof(MailInviteLine_t4144392430, ___owner_4)); }
	inline MailInviteTableController_t1579929683 * get_owner_4() const { return ___owner_4; }
	inline MailInviteTableController_t1579929683 ** get_address_of_owner_4() { return &___owner_4; }
	inline void set_owner_4(MailInviteTableController_t1579929683 * value)
	{
		___owner_4 = value;
		Il2CppCodeGenWriteBarrier((&___owner_4), value);
	}

	inline static int32_t get_offset_of_inviteId_5() { return static_cast<int32_t>(offsetof(MailInviteLine_t4144392430, ___inviteId_5)); }
	inline int64_t get_inviteId_5() const { return ___inviteId_5; }
	inline int64_t* get_address_of_inviteId_5() { return &___inviteId_5; }
	inline void set_inviteId_5(int64_t value)
	{
		___inviteId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILINVITELINE_T4144392430_H
#ifndef MAILSYSTEMLINE_T2170509778_H
#define MAILSYSTEMLINE_T2170509778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailSystemLine
struct  MailSystemLine_t2170509778  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MailSystemLine::subject
	Text_t1901882714 * ___subject_2;
	// UnityEngine.UI.Text MailSystemLine::message
	Text_t1901882714 * ___message_3;

public:
	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(MailSystemLine_t2170509778, ___subject_2)); }
	inline Text_t1901882714 * get_subject_2() const { return ___subject_2; }
	inline Text_t1901882714 ** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(Text_t1901882714 * value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(MailSystemLine_t2170509778, ___message_3)); }
	inline Text_t1901882714 * get_message_3() const { return ___message_3; }
	inline Text_t1901882714 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(Text_t1901882714 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILSYSTEMLINE_T2170509778_H
#ifndef MARKETBUYTABLELINE_T264875335_H
#define MARKETBUYTABLELINE_T264875335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketBuyTableLine
struct  MarketBuyTableLine_t264875335  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text MarketBuyTableLine::quantity
	Text_t1901882714 * ___quantity_2;
	// UnityEngine.UI.Text MarketBuyTableLine::unitPrice
	Text_t1901882714 * ___unitPrice_3;

public:
	inline static int32_t get_offset_of_quantity_2() { return static_cast<int32_t>(offsetof(MarketBuyTableLine_t264875335, ___quantity_2)); }
	inline Text_t1901882714 * get_quantity_2() const { return ___quantity_2; }
	inline Text_t1901882714 ** get_address_of_quantity_2() { return &___quantity_2; }
	inline void set_quantity_2(Text_t1901882714 * value)
	{
		___quantity_2 = value;
		Il2CppCodeGenWriteBarrier((&___quantity_2), value);
	}

	inline static int32_t get_offset_of_unitPrice_3() { return static_cast<int32_t>(offsetof(MarketBuyTableLine_t264875335, ___unitPrice_3)); }
	inline Text_t1901882714 * get_unitPrice_3() const { return ___unitPrice_3; }
	inline Text_t1901882714 ** get_address_of_unitPrice_3() { return &___unitPrice_3; }
	inline void set_unitPrice_3(Text_t1901882714 * value)
	{
		___unitPrice_3 = value;
		Il2CppCodeGenWriteBarrier((&___unitPrice_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETBUYTABLELINE_T264875335_H
#ifndef TRANSFORMGESTUREBASE_T2879339428_H
#define TRANSFORMGESTUREBASE_T2879339428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase
struct  TransformGestureBase_t2879339428  : public Gesture_t4067939634
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformStartedInvoker
	EventHandler_1_t1515976428 * ___transformStartedInvoker_35;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformedInvoker
	EventHandler_1_t1515976428 * ___transformedInvoker_36;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformCompletedInvoker
	EventHandler_1_t1515976428 * ___transformCompletedInvoker_37;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistance
	float ___minScreenPointsPixelDistance_38;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistanceSquared
	float ___minScreenPointsPixelDistanceSquared_39;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThreshold
	float ___screenTransformPixelThreshold_40;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThresholdSquared
	float ___screenTransformPixelThresholdSquared_41;
	// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::deltaPosition
	Vector3_t3722313464  ___deltaPosition_42;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaRotation
	float ___deltaRotation_43;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaScale
	float ___deltaScale_44;
	// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::screenPixelTranslationBuffer
	Vector2_t2156229523  ___screenPixelTranslationBuffer_45;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelRotationBuffer
	float ___screenPixelRotationBuffer_46;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::angleBuffer
	float ___angleBuffer_47;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelScalingBuffer
	float ___screenPixelScalingBuffer_48;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::scaleBuffer
	float ___scaleBuffer_49;
	// System.Boolean TouchScript.Gestures.Base.TransformGestureBase::isTransforming
	bool ___isTransforming_50;
	// TouchScript.Gestures.Base.TransformGestureBase/TransformType TouchScript.Gestures.Base.TransformGestureBase::type
	int32_t ___type_51;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsDistance
	float ___minScreenPointsDistance_52;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformThreshold
	float ___screenTransformThreshold_53;

public:
	inline static int32_t get_offset_of_transformStartedInvoker_35() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformStartedInvoker_35)); }
	inline EventHandler_1_t1515976428 * get_transformStartedInvoker_35() const { return ___transformStartedInvoker_35; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformStartedInvoker_35() { return &___transformStartedInvoker_35; }
	inline void set_transformStartedInvoker_35(EventHandler_1_t1515976428 * value)
	{
		___transformStartedInvoker_35 = value;
		Il2CppCodeGenWriteBarrier((&___transformStartedInvoker_35), value);
	}

	inline static int32_t get_offset_of_transformedInvoker_36() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformedInvoker_36)); }
	inline EventHandler_1_t1515976428 * get_transformedInvoker_36() const { return ___transformedInvoker_36; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformedInvoker_36() { return &___transformedInvoker_36; }
	inline void set_transformedInvoker_36(EventHandler_1_t1515976428 * value)
	{
		___transformedInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___transformedInvoker_36), value);
	}

	inline static int32_t get_offset_of_transformCompletedInvoker_37() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___transformCompletedInvoker_37)); }
	inline EventHandler_1_t1515976428 * get_transformCompletedInvoker_37() const { return ___transformCompletedInvoker_37; }
	inline EventHandler_1_t1515976428 ** get_address_of_transformCompletedInvoker_37() { return &___transformCompletedInvoker_37; }
	inline void set_transformCompletedInvoker_37(EventHandler_1_t1515976428 * value)
	{
		___transformCompletedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___transformCompletedInvoker_37), value);
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistance_38() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsPixelDistance_38)); }
	inline float get_minScreenPointsPixelDistance_38() const { return ___minScreenPointsPixelDistance_38; }
	inline float* get_address_of_minScreenPointsPixelDistance_38() { return &___minScreenPointsPixelDistance_38; }
	inline void set_minScreenPointsPixelDistance_38(float value)
	{
		___minScreenPointsPixelDistance_38 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistanceSquared_39() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsPixelDistanceSquared_39)); }
	inline float get_minScreenPointsPixelDistanceSquared_39() const { return ___minScreenPointsPixelDistanceSquared_39; }
	inline float* get_address_of_minScreenPointsPixelDistanceSquared_39() { return &___minScreenPointsPixelDistanceSquared_39; }
	inline void set_minScreenPointsPixelDistanceSquared_39(float value)
	{
		___minScreenPointsPixelDistanceSquared_39 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThreshold_40() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformPixelThreshold_40)); }
	inline float get_screenTransformPixelThreshold_40() const { return ___screenTransformPixelThreshold_40; }
	inline float* get_address_of_screenTransformPixelThreshold_40() { return &___screenTransformPixelThreshold_40; }
	inline void set_screenTransformPixelThreshold_40(float value)
	{
		___screenTransformPixelThreshold_40 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThresholdSquared_41() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformPixelThresholdSquared_41)); }
	inline float get_screenTransformPixelThresholdSquared_41() const { return ___screenTransformPixelThresholdSquared_41; }
	inline float* get_address_of_screenTransformPixelThresholdSquared_41() { return &___screenTransformPixelThresholdSquared_41; }
	inline void set_screenTransformPixelThresholdSquared_41(float value)
	{
		___screenTransformPixelThresholdSquared_41 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_42() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaPosition_42)); }
	inline Vector3_t3722313464  get_deltaPosition_42() const { return ___deltaPosition_42; }
	inline Vector3_t3722313464 * get_address_of_deltaPosition_42() { return &___deltaPosition_42; }
	inline void set_deltaPosition_42(Vector3_t3722313464  value)
	{
		___deltaPosition_42 = value;
	}

	inline static int32_t get_offset_of_deltaRotation_43() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaRotation_43)); }
	inline float get_deltaRotation_43() const { return ___deltaRotation_43; }
	inline float* get_address_of_deltaRotation_43() { return &___deltaRotation_43; }
	inline void set_deltaRotation_43(float value)
	{
		___deltaRotation_43 = value;
	}

	inline static int32_t get_offset_of_deltaScale_44() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___deltaScale_44)); }
	inline float get_deltaScale_44() const { return ___deltaScale_44; }
	inline float* get_address_of_deltaScale_44() { return &___deltaScale_44; }
	inline void set_deltaScale_44(float value)
	{
		___deltaScale_44 = value;
	}

	inline static int32_t get_offset_of_screenPixelTranslationBuffer_45() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelTranslationBuffer_45)); }
	inline Vector2_t2156229523  get_screenPixelTranslationBuffer_45() const { return ___screenPixelTranslationBuffer_45; }
	inline Vector2_t2156229523 * get_address_of_screenPixelTranslationBuffer_45() { return &___screenPixelTranslationBuffer_45; }
	inline void set_screenPixelTranslationBuffer_45(Vector2_t2156229523  value)
	{
		___screenPixelTranslationBuffer_45 = value;
	}

	inline static int32_t get_offset_of_screenPixelRotationBuffer_46() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelRotationBuffer_46)); }
	inline float get_screenPixelRotationBuffer_46() const { return ___screenPixelRotationBuffer_46; }
	inline float* get_address_of_screenPixelRotationBuffer_46() { return &___screenPixelRotationBuffer_46; }
	inline void set_screenPixelRotationBuffer_46(float value)
	{
		___screenPixelRotationBuffer_46 = value;
	}

	inline static int32_t get_offset_of_angleBuffer_47() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___angleBuffer_47)); }
	inline float get_angleBuffer_47() const { return ___angleBuffer_47; }
	inline float* get_address_of_angleBuffer_47() { return &___angleBuffer_47; }
	inline void set_angleBuffer_47(float value)
	{
		___angleBuffer_47 = value;
	}

	inline static int32_t get_offset_of_screenPixelScalingBuffer_48() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenPixelScalingBuffer_48)); }
	inline float get_screenPixelScalingBuffer_48() const { return ___screenPixelScalingBuffer_48; }
	inline float* get_address_of_screenPixelScalingBuffer_48() { return &___screenPixelScalingBuffer_48; }
	inline void set_screenPixelScalingBuffer_48(float value)
	{
		___screenPixelScalingBuffer_48 = value;
	}

	inline static int32_t get_offset_of_scaleBuffer_49() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___scaleBuffer_49)); }
	inline float get_scaleBuffer_49() const { return ___scaleBuffer_49; }
	inline float* get_address_of_scaleBuffer_49() { return &___scaleBuffer_49; }
	inline void set_scaleBuffer_49(float value)
	{
		___scaleBuffer_49 = value;
	}

	inline static int32_t get_offset_of_isTransforming_50() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___isTransforming_50)); }
	inline bool get_isTransforming_50() const { return ___isTransforming_50; }
	inline bool* get_address_of_isTransforming_50() { return &___isTransforming_50; }
	inline void set_isTransforming_50(bool value)
	{
		___isTransforming_50 = value;
	}

	inline static int32_t get_offset_of_type_51() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___type_51)); }
	inline int32_t get_type_51() const { return ___type_51; }
	inline int32_t* get_address_of_type_51() { return &___type_51; }
	inline void set_type_51(int32_t value)
	{
		___type_51 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsDistance_52() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___minScreenPointsDistance_52)); }
	inline float get_minScreenPointsDistance_52() const { return ___minScreenPointsDistance_52; }
	inline float* get_address_of_minScreenPointsDistance_52() { return &___minScreenPointsDistance_52; }
	inline void set_minScreenPointsDistance_52(float value)
	{
		___minScreenPointsDistance_52 = value;
	}

	inline static int32_t get_offset_of_screenTransformThreshold_53() { return static_cast<int32_t>(offsetof(TransformGestureBase_t2879339428, ___screenTransformThreshold_53)); }
	inline float get_screenTransformThreshold_53() const { return ___screenTransformThreshold_53; }
	inline float* get_address_of_screenTransformThreshold_53() { return &___screenTransformThreshold_53; }
	inline void set_screenTransformThreshold_53(float value)
	{
		___screenTransformThreshold_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMGESTUREBASE_T2879339428_H
#ifndef ORTHOGRAPHICCAMERATRANSFORMGESTURE_T1804876297_H
#define ORTHOGRAPHICCAMERATRANSFORMGESTURE_T1804876297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.OrthographicCameraTransformGesture
struct  OrthographicCameraTransformGesture_t1804876297  : public TransformGestureBase_t2879339428
{
public:
	// System.Boolean TouchScript.Gestures.OrthographicCameraTransformGesture::worldMap
	bool ___worldMap_54;
	// UnityEngine.Material TouchScript.Gestures.OrthographicCameraTransformGesture::mapBackground
	Material_t340375123 * ___mapBackground_55;
	// UnityEngine.GameObject TouchScript.Gestures.OrthographicCameraTransformGesture::mapGameWorld
	GameObject_t1113636619 * ___mapGameWorld_56;
	// TestWorldChunksManager TouchScript.Gestures.OrthographicCameraTransformGesture::chuncksManager
	TestWorldChunksManager_t1200850834 * ___chuncksManager_57;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::leftBottom
	Vector2_t2156229523  ___leftBottom_58;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::rightTop
	Vector2_t2156229523  ___rightTop_59;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::actualLeftBottom
	Vector2_t2156229523  ___actualLeftBottom_60;
	// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::actualRightTop
	Vector2_t2156229523  ___actualRightTop_61;

public:
	inline static int32_t get_offset_of_worldMap_54() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___worldMap_54)); }
	inline bool get_worldMap_54() const { return ___worldMap_54; }
	inline bool* get_address_of_worldMap_54() { return &___worldMap_54; }
	inline void set_worldMap_54(bool value)
	{
		___worldMap_54 = value;
	}

	inline static int32_t get_offset_of_mapBackground_55() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___mapBackground_55)); }
	inline Material_t340375123 * get_mapBackground_55() const { return ___mapBackground_55; }
	inline Material_t340375123 ** get_address_of_mapBackground_55() { return &___mapBackground_55; }
	inline void set_mapBackground_55(Material_t340375123 * value)
	{
		___mapBackground_55 = value;
		Il2CppCodeGenWriteBarrier((&___mapBackground_55), value);
	}

	inline static int32_t get_offset_of_mapGameWorld_56() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___mapGameWorld_56)); }
	inline GameObject_t1113636619 * get_mapGameWorld_56() const { return ___mapGameWorld_56; }
	inline GameObject_t1113636619 ** get_address_of_mapGameWorld_56() { return &___mapGameWorld_56; }
	inline void set_mapGameWorld_56(GameObject_t1113636619 * value)
	{
		___mapGameWorld_56 = value;
		Il2CppCodeGenWriteBarrier((&___mapGameWorld_56), value);
	}

	inline static int32_t get_offset_of_chuncksManager_57() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___chuncksManager_57)); }
	inline TestWorldChunksManager_t1200850834 * get_chuncksManager_57() const { return ___chuncksManager_57; }
	inline TestWorldChunksManager_t1200850834 ** get_address_of_chuncksManager_57() { return &___chuncksManager_57; }
	inline void set_chuncksManager_57(TestWorldChunksManager_t1200850834 * value)
	{
		___chuncksManager_57 = value;
		Il2CppCodeGenWriteBarrier((&___chuncksManager_57), value);
	}

	inline static int32_t get_offset_of_leftBottom_58() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___leftBottom_58)); }
	inline Vector2_t2156229523  get_leftBottom_58() const { return ___leftBottom_58; }
	inline Vector2_t2156229523 * get_address_of_leftBottom_58() { return &___leftBottom_58; }
	inline void set_leftBottom_58(Vector2_t2156229523  value)
	{
		___leftBottom_58 = value;
	}

	inline static int32_t get_offset_of_rightTop_59() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___rightTop_59)); }
	inline Vector2_t2156229523  get_rightTop_59() const { return ___rightTop_59; }
	inline Vector2_t2156229523 * get_address_of_rightTop_59() { return &___rightTop_59; }
	inline void set_rightTop_59(Vector2_t2156229523  value)
	{
		___rightTop_59 = value;
	}

	inline static int32_t get_offset_of_actualLeftBottom_60() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___actualLeftBottom_60)); }
	inline Vector2_t2156229523  get_actualLeftBottom_60() const { return ___actualLeftBottom_60; }
	inline Vector2_t2156229523 * get_address_of_actualLeftBottom_60() { return &___actualLeftBottom_60; }
	inline void set_actualLeftBottom_60(Vector2_t2156229523  value)
	{
		___actualLeftBottom_60 = value;
	}

	inline static int32_t get_offset_of_actualRightTop_61() { return static_cast<int32_t>(offsetof(OrthographicCameraTransformGesture_t1804876297, ___actualRightTop_61)); }
	inline Vector2_t2156229523  get_actualRightTop_61() const { return ___actualRightTop_61; }
	inline Vector2_t2156229523 * get_address_of_actualRightTop_61() { return &___actualRightTop_61; }
	inline void set_actualRightTop_61(Vector2_t2156229523  value)
	{
		___actualRightTop_61 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORTHOGRAPHICCAMERATRANSFORMGESTURE_T1804876297_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (ArmyGeneralModel_t737750221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[12] = 
{
	ArmyGeneralModel_t737750221::get_offset_of_id_0(),
	ArmyGeneralModel_t737750221::get_offset_of_name_1(),
	ArmyGeneralModel_t737750221::get_offset_of_cityID_2(),
	ArmyGeneralModel_t737750221::get_offset_of_politics_3(),
	ArmyGeneralModel_t737750221::get_offset_of_marches_4(),
	ArmyGeneralModel_t737750221::get_offset_of_speed_5(),
	ArmyGeneralModel_t737750221::get_offset_of_loyalty_6(),
	ArmyGeneralModel_t737750221::get_offset_of_level_7(),
	ArmyGeneralModel_t737750221::get_offset_of_salary_8(),
	ArmyGeneralModel_t737750221::get_offset_of_experience_9(),
	ArmyGeneralModel_t737750221::get_offset_of_number_of_battles_10(),
	ArmyGeneralModel_t737750221::get_offset_of_status_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (ArmyModel_t2068673105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[21] = 
{
	ArmyModel_t2068673105::get_offset_of_id_0(),
	ArmyModel_t2068673105::get_offset_of_worldMap_1(),
	ArmyModel_t2068673105::get_offset_of_hero_2(),
	ArmyModel_t2068673105::get_offset_of_destination_point_3(),
	ArmyModel_t2068673105::get_offset_of_workers_count_4(),
	ArmyModel_t2068673105::get_offset_of_spy_count_5(),
	ArmyModel_t2068673105::get_offset_of_swards_man_count_6(),
	ArmyModel_t2068673105::get_offset_of_spear_man_count_7(),
	ArmyModel_t2068673105::get_offset_of_pike_man_count_8(),
	ArmyModel_t2068673105::get_offset_of_scout_rider_count_9(),
	ArmyModel_t2068673105::get_offset_of_light_cavalry_count_10(),
	ArmyModel_t2068673105::get_offset_of_heavy_cavalry_count_11(),
	ArmyModel_t2068673105::get_offset_of_archer_count_12(),
	ArmyModel_t2068673105::get_offset_of_archer_rider_count_13(),
	ArmyModel_t2068673105::get_offset_of_holly_man_count_14(),
	ArmyModel_t2068673105::get_offset_of_wagon_count_15(),
	ArmyModel_t2068673105::get_offset_of_trebuchet_count_16(),
	ArmyModel_t2068673105::get_offset_of_siege_towers_count_17(),
	ArmyModel_t2068673105::get_offset_of_battering_ram_count_18(),
	ArmyModel_t2068673105::get_offset_of_ballista_count_19(),
	ArmyModel_t2068673105::get_offset_of_army_status_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (ArmyTimer_t3282851823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[9] = 
{
	ArmyTimer_t3282851823::get_offset_of_id_0(),
	ArmyTimer_t3282851823::get_offset_of_army_1(),
	ArmyTimer_t3282851823::get_offset_of_event_id_2(),
	ArmyTimer_t3282851823::get_offset_of_start_time_3(),
	ArmyTimer_t3282851823::get_offset_of_finish_time_4(),
	ArmyTimer_t3282851823::get_offset_of_army_status_5(),
	ArmyTimer_t3282851823::get_offset_of_enemy_6(),
	ArmyTimer_t3282851823::get_offset_of_targetType_7(),
	ArmyTimer_t3282851823::get_offset_of_old_army_status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (BattleReportHeader_t3025158543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3503[10] = 
{
	BattleReportHeader_t3025158543::get_offset_of_id_0(),
	BattleReportHeader_t3025158543::get_offset_of_attackUserName_1(),
	BattleReportHeader_t3025158543::get_offset_of_defenceUserName_2(),
	BattleReportHeader_t3025158543::get_offset_of_isSuccessAttack_3(),
	BattleReportHeader_t3025158543::get_offset_of_creation_date_4(),
	BattleReportHeader_t3025158543::get_offset_of_isReadByAttacker_5(),
	BattleReportHeader_t3025158543::get_offset_of_isReadByDefender_6(),
	BattleReportHeader_t3025158543::get_offset_of_attack_type_7(),
	BattleReportHeader_t3025158543::get_offset_of_defenceCityName_8(),
	BattleReportHeader_t3025158543::get_offset_of_attackCityName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (BattleReportModel_t4250794538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[18] = 
{
	BattleReportModel_t4250794538::get_offset_of_id_0(),
	BattleReportModel_t4250794538::get_offset_of_attackUser_1(),
	BattleReportModel_t4250794538::get_offset_of_defenceUser_2(),
	BattleReportModel_t4250794538::get_offset_of_attackersCity_3(),
	BattleReportModel_t4250794538::get_offset_of_defendersCity_4(),
	BattleReportModel_t4250794538::get_offset_of_attackArmyResources_5(),
	BattleReportModel_t4250794538::get_offset_of_defenceArmyResources_6(),
	BattleReportModel_t4250794538::get_offset_of_stolenCityTreasures_7(),
	BattleReportModel_t4250794538::get_offset_of_attackArmyBeforeBattle_8(),
	BattleReportModel_t4250794538::get_offset_of_defenceArmyBeforeBattle_9(),
	BattleReportModel_t4250794538::get_offset_of_attackArmyAfterBattle_10(),
	BattleReportModel_t4250794538::get_offset_of_defenceArmyAfterBattle_11(),
	BattleReportModel_t4250794538::get_offset_of_defendersResources_12(),
	BattleReportModel_t4250794538::get_offset_of_defendersCityItems_13(),
	BattleReportModel_t4250794538::get_offset_of_isSuccessAttack_14(),
	BattleReportModel_t4250794538::get_offset_of_creation_date_15(),
	BattleReportModel_t4250794538::get_offset_of_attack_type_16(),
	BattleReportModel_t4250794538::get_offset_of_log_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (BuildingConstantModel_t2655945000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[4] = 
{
	BuildingConstantModel_t2655945000::get_offset_of_id_0(),
	BuildingConstantModel_t2655945000::get_offset_of_building_name_1(),
	BuildingConstantModel_t2655945000::get_offset_of_description_2(),
	BuildingConstantModel_t2655945000::get_offset_of_building_prices_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (BuildingModel_t2283411500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3506[8] = 
{
	BuildingModel_t2283411500::get_offset_of_id_0(),
	BuildingModel_t2283411500::get_offset_of_building_1(),
	BuildingModel_t2283411500::get_offset_of_level_2(),
	BuildingModel_t2283411500::get_offset_of_pit_id_3(),
	BuildingModel_t2283411500::get_offset_of_city_4(),
	BuildingModel_t2283411500::get_offset_of_status_5(),
	BuildingModel_t2283411500::get_offset_of_population_6(),
	BuildingModel_t2283411500::get_offset_of_location_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (BuildingPrice_t1241848265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3507[11] = 
{
	BuildingPrice_t1241848265::get_offset_of_building_0(),
	BuildingPrice_t1241848265::get_offset_of_level_1(),
	BuildingPrice_t1241848265::get_offset_of_food_price_2(),
	BuildingPrice_t1241848265::get_offset_of_wood_price_3(),
	BuildingPrice_t1241848265::get_offset_of_stone_price_4(),
	BuildingPrice_t1241848265::get_offset_of_iron_price_5(),
	BuildingPrice_t1241848265::get_offset_of_cooper_price_6(),
	BuildingPrice_t1241848265::get_offset_of_gold_price_7(),
	BuildingPrice_t1241848265::get_offset_of_silver_price_8(),
	BuildingPrice_t1241848265::get_offset_of_time_seconds_9(),
	BuildingPrice_t1241848265::get_offset_of_population_price_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (BuildingTimer_t374074310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3508[6] = 
{
	BuildingTimer_t374074310::get_offset_of_id_0(),
	BuildingTimer_t374074310::get_offset_of_building_1(),
	BuildingTimer_t374074310::get_offset_of_status_2(),
	BuildingTimer_t374074310::get_offset_of_event_id_3(),
	BuildingTimer_t374074310::get_offset_of_start_time_4(),
	BuildingTimer_t374074310::get_offset_of_finish_time_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (CityModel_t1286289939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3509[6] = 
{
	CityModel_t1286289939::get_offset_of_id_0(),
	CityModel_t1286289939::get_offset_of_owner_1(),
	CityModel_t1286289939::get_offset_of_city_name_2(),
	CityModel_t1286289939::get_offset_of_image_name_3(),
	CityModel_t1286289939::get_offset_of_occupantCityOwnerName_4(),
	CityModel_t1286289939::get_offset_of_occupantCityName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (CityTreasureModel_t1919871523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[5] = 
{
	CityTreasureModel_t1919871523::get_offset_of_id_0(),
	CityTreasureModel_t1919871523::get_offset_of_city_1(),
	CityTreasureModel_t1919871523::get_offset_of_item_constant_2(),
	CityTreasureModel_t1919871523::get_offset_of_count_3(),
	CityTreasureModel_t1919871523::get_offset_of_price_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (ColonyModel_t4002254264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[8] = 
{
	ColonyModel_t4002254264::get_offset_of_id_0(),
	ColonyModel_t4002254264::get_offset_of_city_name_1(),
	ColonyModel_t4002254264::get_offset_of_username_2(),
	ColonyModel_t4002254264::get_offset_of_posX_3(),
	ColonyModel_t4002254264::get_offset_of_posY_4(),
	ColonyModel_t4002254264::get_offset_of_region_5(),
	ColonyModel_t4002254264::get_offset_of_mayer_residence_level_6(),
	ColonyModel_t4002254264::get_offset_of_production_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (FinanceReportModel_t1598071249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3512[9] = 
{
	FinanceReportModel_t1598071249::get_offset_of_id_0(),
	FinanceReportModel_t1598071249::get_offset_of_user_1(),
	FinanceReportModel_t1598071249::get_offset_of_city_2(),
	FinanceReportModel_t1598071249::get_offset_of_item_constant_3(),
	FinanceReportModel_t1598071249::get_offset_of_count_4(),
	FinanceReportModel_t1598071249::get_offset_of_date_5(),
	FinanceReportModel_t1598071249::get_offset_of_price_6(),
	FinanceReportModel_t1598071249::get_offset_of_type_7(),
	FinanceReportModel_t1598071249::get_offset_of_fromUser_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (InviteReportModel_t637635712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3513[11] = 
{
	InviteReportModel_t637635712::get_offset_of_id_0(),
	InviteReportModel_t637635712::get_offset_of_inviteFromId_1(),
	InviteReportModel_t637635712::get_offset_of_inviteFromName_2(),
	InviteReportModel_t637635712::get_offset_of_inviteToId_3(),
	InviteReportModel_t637635712::get_offset_of_inviteToName_4(),
	InviteReportModel_t637635712::get_offset_of_isRead_5(),
	InviteReportModel_t637635712::get_offset_of_allianceId_6(),
	InviteReportModel_t637635712::get_offset_of_allianceName_7(),
	InviteReportModel_t637635712::get_offset_of_allianceRank_8(),
	InviteReportModel_t637635712::get_offset_of_type_9(),
	InviteReportModel_t637635712::get_offset_of_creationDate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (ItemTimer_t2318236575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[7] = 
{
	ItemTimer_t2318236575::get_offset_of_id_0(),
	ItemTimer_t2318236575::get_offset_of_event_id_1(),
	ItemTimer_t2318236575::get_offset_of_itemID_2(),
	ItemTimer_t2318236575::get_offset_of_start_time_3(),
	ItemTimer_t2318236575::get_offset_of_finish_time_4(),
	ItemTimer_t2318236575::get_offset_of_name_5(),
	ItemTimer_t2318236575::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (MarketLotModel_t1127536123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[6] = 
{
	MarketLotModel_t1127536123::get_offset_of_id_0(),
	MarketLotModel_t1127536123::get_offset_of_owner_1(),
	MarketLotModel_t1127536123::get_offset_of_city_2(),
	MarketLotModel_t1127536123::get_offset_of_resource_type_3(),
	MarketLotModel_t1127536123::get_offset_of_count_4(),
	MarketLotModel_t1127536123::get_offset_of_price_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (MessageHeader_t3475772423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3516[5] = 
{
	MessageHeader_t3475772423::get_offset_of_id_0(),
	MessageHeader_t3475772423::get_offset_of_fromUserName_1(),
	MessageHeader_t3475772423::get_offset_of_subject_2(),
	MessageHeader_t3475772423::get_offset_of_isRead_3(),
	MessageHeader_t3475772423::get_offset_of_creation_date_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (MyCityModel_t3961736920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3517[16] = 
{
	MyCityModel_t3961736920::get_offset_of_id_0(),
	MyCityModel_t3961736920::get_offset_of_city_name_1(),
	MyCityModel_t3961736920::get_offset_of_posX_2(),
	MyCityModel_t3961736920::get_offset_of_posY_3(),
	MyCityModel_t3961736920::get_offset_of_region_4(),
	MyCityModel_t3961736920::get_offset_of_image_name_5(),
	MyCityModel_t3961736920::get_offset_of_max_population_6(),
	MyCityModel_t3961736920::get_offset_of_current_population_7(),
	MyCityModel_t3961736920::get_offset_of_happiness_8(),
	MyCityModel_t3961736920::get_offset_of_courage_9(),
	MyCityModel_t3961736920::get_offset_of_isExpantion_10(),
	MyCityModel_t3961736920::get_offset_of_reinforce_flag_11(),
	MyCityModel_t3961736920::get_offset_of_gold_tax_rate_12(),
	MyCityModel_t3961736920::get_offset_of_fields_count_13(),
	MyCityModel_t3961736920::get_offset_of_valleys_count_14(),
	MyCityModel_t3961736920::get_offset_of_mayer_residence_level_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (ResourcesModel_t2533508513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3518[10] = 
{
	ResourcesModel_t2533508513::get_offset_of_city_0(),
	ResourcesModel_t2533508513::get_offset_of_food_1(),
	ResourcesModel_t2533508513::get_offset_of_wood_2(),
	ResourcesModel_t2533508513::get_offset_of_stone_3(),
	ResourcesModel_t2533508513::get_offset_of_iron_4(),
	ResourcesModel_t2533508513::get_offset_of_cooper_5(),
	ResourcesModel_t2533508513::get_offset_of_gold_6(),
	ResourcesModel_t2533508513::get_offset_of_silver_7(),
	ResourcesModel_t2533508513::get_offset_of_population_8(),
	ResourcesModel_t2533508513::get_offset_of_last_update_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (ShopItemModel_t334704368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[6] = 
{
	ShopItemModel_t334704368::get_offset_of_id_0(),
	ShopItemModel_t334704368::get_offset_of_name_1(),
	ShopItemModel_t334704368::get_offset_of_rank_2(),
	ShopItemModel_t334704368::get_offset_of_description_3(),
	ShopItemModel_t334704368::get_offset_of_price_sh_4(),
	ShopItemModel_t334704368::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (SimpleUserModel_t455560495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3520[11] = 
{
	SimpleUserModel_t455560495::get_offset_of_id_0(),
	SimpleUserModel_t455560495::get_offset_of_username_1(),
	SimpleUserModel_t455560495::get_offset_of_rank_2(),
	SimpleUserModel_t455560495::get_offset_of_emperor_id_3(),
	SimpleUserModel_t455560495::get_offset_of_status_4(),
	SimpleUserModel_t455560495::get_offset_of_experience_5(),
	SimpleUserModel_t455560495::get_offset_of_alliance_name_6(),
	SimpleUserModel_t455560495::get_offset_of_allianceID_7(),
	SimpleUserModel_t455560495::get_offset_of_capitalID_8(),
	SimpleUserModel_t455560495::get_offset_of_image_name_9(),
	SimpleUserModel_t455560495::get_offset_of_flag_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (SystemReportModel_t2947163234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3521[5] = 
{
	SystemReportModel_t2947163234::get_offset_of_id_0(),
	SystemReportModel_t2947163234::get_offset_of_subject_1(),
	SystemReportModel_t2947163234::get_offset_of_message_2(),
	SystemReportModel_t2947163234::get_offset_of_isRead_3(),
	SystemReportModel_t2947163234::get_offset_of_creation_date_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (TutorialPageModel_t200695960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[5] = 
{
	TutorialPageModel_t200695960::get_offset_of_id_0(),
	TutorialPageModel_t200695960::get_offset_of_title_1(),
	TutorialPageModel_t200695960::get_offset_of_page_2(),
	TutorialPageModel_t200695960::get_offset_of_text_3(),
	TutorialPageModel_t200695960::get_offset_of_image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (UnitConstantModel_t3582189408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[18] = 
{
	UnitConstantModel_t3582189408::get_offset_of_name_0(),
	UnitConstantModel_t3582189408::get_offset_of_unit_name_1(),
	UnitConstantModel_t3582189408::get_offset_of_food_price_2(),
	UnitConstantModel_t3582189408::get_offset_of_wood_price_3(),
	UnitConstantModel_t3582189408::get_offset_of_stone_price_4(),
	UnitConstantModel_t3582189408::get_offset_of_iron_price_5(),
	UnitConstantModel_t3582189408::get_offset_of_cooper_price_6(),
	UnitConstantModel_t3582189408::get_offset_of_gold_price_7(),
	UnitConstantModel_t3582189408::get_offset_of_silver_price_8(),
	UnitConstantModel_t3582189408::get_offset_of_time_seconds_9(),
	UnitConstantModel_t3582189408::get_offset_of_population_price_10(),
	UnitConstantModel_t3582189408::get_offset_of_speed_11(),
	UnitConstantModel_t3582189408::get_offset_of_range_12(),
	UnitConstantModel_t3582189408::get_offset_of_attack_13(),
	UnitConstantModel_t3582189408::get_offset_of_defence_14(),
	UnitConstantModel_t3582189408::get_offset_of_life_15(),
	UnitConstantModel_t3582189408::get_offset_of_capacity_16(),
	UnitConstantModel_t3582189408::get_offset_of_description_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (UnitsModel_t3847956398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[18] = 
{
	UnitsModel_t3847956398::get_offset_of_city_0(),
	UnitsModel_t3847956398::get_offset_of_workers_count_1(),
	UnitsModel_t3847956398::get_offset_of_spy_count_2(),
	UnitsModel_t3847956398::get_offset_of_swards_man_count_3(),
	UnitsModel_t3847956398::get_offset_of_spear_man_count_4(),
	UnitsModel_t3847956398::get_offset_of_pike_man_count_5(),
	UnitsModel_t3847956398::get_offset_of_scout_rider_count_6(),
	UnitsModel_t3847956398::get_offset_of_light_cavalry_count_7(),
	UnitsModel_t3847956398::get_offset_of_heavy_cavalry_count_8(),
	UnitsModel_t3847956398::get_offset_of_archer_count_9(),
	UnitsModel_t3847956398::get_offset_of_archer_rider_count_10(),
	UnitsModel_t3847956398::get_offset_of_holly_man_count_11(),
	UnitsModel_t3847956398::get_offset_of_wagon_count_12(),
	UnitsModel_t3847956398::get_offset_of_trebuchet_count_13(),
	UnitsModel_t3847956398::get_offset_of_siege_towers_count_14(),
	UnitsModel_t3847956398::get_offset_of_battering_ram_count_15(),
	UnitsModel_t3847956398::get_offset_of_ballista_count_16(),
	UnitsModel_t3847956398::get_offset_of_army_status_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (UnitTimer_t3121550770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[5] = 
{
	UnitTimer_t3121550770::get_offset_of_id_0(),
	UnitTimer_t3121550770::get_offset_of_unit_1(),
	UnitTimer_t3121550770::get_offset_of_event_id_2(),
	UnitTimer_t3121550770::get_offset_of_start_time_3(),
	UnitTimer_t3121550770::get_offset_of_finish_time_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (UserMessageReportModel_t4065405063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3526[6] = 
{
	UserMessageReportModel_t4065405063::get_offset_of_id_0(),
	UserMessageReportModel_t4065405063::get_offset_of_fromUser_1(),
	UserMessageReportModel_t4065405063::get_offset_of_subject_2(),
	UserMessageReportModel_t4065405063::get_offset_of_message_3(),
	UserMessageReportModel_t4065405063::get_offset_of_isRead_4(),
	UserMessageReportModel_t4065405063::get_offset_of_creation_date_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (UserModel_t1353931605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[20] = 
{
	UserModel_t1353931605::get_offset_of_id_0(),
	UserModel_t1353931605::get_offset_of_username_1(),
	UserModel_t1353931605::get_offset_of_status_2(),
	UserModel_t1353931605::get_offset_of_image_name_3(),
	UserModel_t1353931605::get_offset_of_rank_4(),
	UserModel_t1353931605::get_offset_of_emperor_id_5(),
	UserModel_t1353931605::get_offset_of_email_6(),
	UserModel_t1353931605::get_offset_of_experience_7(),
	UserModel_t1353931605::get_offset_of_alliance_rank_8(),
	UserModel_t1353931605::get_offset_of_last_login_9(),
	UserModel_t1353931605::get_offset_of_city_count_10(),
	UserModel_t1353931605::get_offset_of_alliance_11(),
	UserModel_t1353931605::get_offset_of_capital_12(),
	UserModel_t1353931605::get_offset_of_shillings_13(),
	UserModel_t1353931605::get_offset_of_language_14(),
	UserModel_t1353931605::get_offset_of_flag_15(),
	UserModel_t1353931605::get_offset_of_players_wins_16(),
	UserModel_t1353931605::get_offset_of_players_losses_17(),
	UserModel_t1353931605::get_offset_of_barbarians_wins_18(),
	UserModel_t1353931605::get_offset_of_barbarians_losses_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (WorldFieldModel_t2417974361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[9] = 
{
	WorldFieldModel_t2417974361::get_offset_of_id_0(),
	WorldFieldModel_t2417974361::get_offset_of_posX_1(),
	WorldFieldModel_t2417974361::get_offset_of_posY_2(),
	WorldFieldModel_t2417974361::get_offset_of_cityType_3(),
	WorldFieldModel_t2417974361::get_offset_of_city_4(),
	WorldFieldModel_t2417974361::get_offset_of_owner_5(),
	WorldFieldModel_t2417974361::get_offset_of_level_6(),
	WorldFieldModel_t2417974361::get_offset_of_production_7(),
	WorldFieldModel_t2417974361::get_offset_of_region_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (WorldMapTimer_t60483741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3529[7] = 
{
	WorldMapTimer_t60483741::get_offset_of_id_0(),
	WorldMapTimer_t60483741::get_offset_of_worldMap_1(),
	WorldMapTimer_t60483741::get_offset_of_status_2(),
	WorldMapTimer_t60483741::get_offset_of_event_id_3(),
	WorldMapTimer_t60483741::get_offset_of_start_time_4(),
	WorldMapTimer_t60483741::get_offset_of_finish_time_5(),
	WorldMapTimer_t60483741::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (OpenLink_t536774666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[1] = 
{
	OpenLink_t536774666::get_offset_of_link_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (OrthographicCameraTransformGesture_t1804876297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3531[8] = 
{
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_worldMap_54(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_mapBackground_55(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_mapGameWorld_56(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_chuncksManager_57(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_leftBottom_58(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_rightTop_59(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_actualLeftBottom_60(),
	OrthographicCameraTransformGesture_t1804876297::get_offset_of_actualRightTop_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (RankIconUpdater_t798492598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[2] = 
{
	RankIconUpdater_t798492598::get_offset_of_rankIcon_2(),
	RankIconUpdater_t798492598::get_offset_of_rank_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (RightPanel_t145460902), -1, sizeof(RightPanel_t145460902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3533[6] = 
{
	RightPanel_t145460902_StaticFields::get_offset_of_instance_2(),
	RightPanel_t145460902::get_offset_of_RightButtonsTransform_3(),
	RightPanel_t145460902::get_offset_of_cityIcon_4(),
	RightPanel_t145460902::get_offset_of_cityCoords_5(),
	RightPanel_t145460902::get_offset_of_citiesCount_6(),
	RightPanel_t145460902::get_offset_of_isOpened_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (ServerTimeUpdater_t1591320137), -1, sizeof(ServerTimeUpdater_t1591320137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3534[3] = 
{
	ServerTimeUpdater_t1591320137_StaticFields::get_offset_of_instance_2(),
	ServerTimeUpdater_t1591320137::get_offset_of_currentTime_3(),
	ServerTimeUpdater_t1591320137::get_offset_of_currentDate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (ShopTableCell_t1369591435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3536[21] = 
{
	ShopTableCell_t1369591435::get_offset_of_itemLabel1_2(),
	ShopTableCell_t1369591435::get_offset_of_itemDescription1_3(),
	ShopTableCell_t1369591435::get_offset_of_itemCount1_4(),
	ShopTableCell_t1369591435::get_offset_of_itemImage1_5(),
	ShopTableCell_t1369591435::get_offset_of_itemCost1_6(),
	ShopTableCell_t1369591435::get_offset_of_itemAction1_7(),
	ShopTableCell_t1369591435::get_offset_of_UseBtn1_8(),
	ShopTableCell_t1369591435::get_offset_of_itemId1_9(),
	ShopTableCell_t1369591435::get_offset_of_constantId1_10(),
	ShopTableCell_t1369591435::get_offset_of_itemLabel2_11(),
	ShopTableCell_t1369591435::get_offset_of_itemDescription2_12(),
	ShopTableCell_t1369591435::get_offset_of_itemCount2_13(),
	ShopTableCell_t1369591435::get_offset_of_itemImage2_14(),
	ShopTableCell_t1369591435::get_offset_of_itemCost2_15(),
	ShopTableCell_t1369591435::get_offset_of_itemAction2_16(),
	ShopTableCell_t1369591435::get_offset_of_UseBtn2_17(),
	ShopTableCell_t1369591435::get_offset_of_itemId2_18(),
	ShopTableCell_t1369591435::get_offset_of_constantId2_19(),
	ShopTableCell_t1369591435::get_offset_of_owner_20(),
	ShopTableCell_t1369591435::get_offset_of_type_21(),
	ShopTableCell_t1369591435::get_offset_of_evendId_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (ShopTableController_t1404998102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3537[26] = 
{
	ShopTableController_t1404998102::get_offset_of_scriptShopCells_2(),
	ShopTableController_t1404998102::get_offset_of_speedUpShopCells_3(),
	ShopTableController_t1404998102::get_offset_of_productionShopCells_4(),
	ShopTableController_t1404998102::get_offset_of_diamondShopCells_5(),
	ShopTableController_t1404998102::get_offset_of_promotionsShopCells_6(),
	ShopTableController_t1404998102::get_offset_of_armyShopCells_7(),
	ShopTableController_t1404998102::get_offset_of_currentSourceArray_8(),
	ShopTableController_t1404998102::get_offset_of__shop_9(),
	ShopTableController_t1404998102::get_offset_of_cityId_10(),
	ShopTableController_t1404998102::get_offset_of_content_11(),
	ShopTableController_t1404998102::get_offset_of_langManager_12(),
	ShopTableController_t1404998102::get_offset_of_onGetShopItems_13(),
	ShopTableController_t1404998102::get_offset_of_m_cellPrefab_14(),
	ShopTableController_t1404998102::get_offset_of_m_tableView_15(),
	ShopTableController_t1404998102::get_offset_of_Label_16(),
	ShopTableController_t1404998102::get_offset_of_shillingsCount_17(),
	ShopTableController_t1404998102::get_offset_of_icon_18(),
	ShopTableController_t1404998102::get_offset_of_tabSelectedTop_19(),
	ShopTableController_t1404998102::get_offset_of_tabSelectedBottom_20(),
	ShopTableController_t1404998102::get_offset_of_tabUnselectedTop_21(),
	ShopTableController_t1404998102::get_offset_of_tabUnselectedBottom_22(),
	ShopTableController_t1404998102::get_offset_of_shopIcon_23(),
	ShopTableController_t1404998102::get_offset_of_treasureIcon_24(),
	ShopTableController_t1404998102::get_offset_of_buyMoreBtn_25(),
	ShopTableController_t1404998102::get_offset_of_buyMoreTxt_26(),
	ShopTableController_t1404998102::get_offset_of_tabs_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (U3CGetShopItemsU3Ec__Iterator0_t332572680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3538[5] = 
{
	U3CGetShopItemsU3Ec__Iterator0_t332572680::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetShopItemsU3Ec__Iterator0_t332572680::get_offset_of_U24this_1(),
	U3CGetShopItemsU3Ec__Iterator0_t332572680::get_offset_of_U24current_2(),
	U3CGetShopItemsU3Ec__Iterator0_t332572680::get_offset_of_U24disposing_3(),
	U3CGetShopItemsU3Ec__Iterator0_t332572680::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[5] = 
{
	U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028::get_offset_of_U24this_1(),
	U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028::get_offset_of_U24current_2(),
	U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028::get_offset_of_U24disposing_3(),
	U3CGetPlayerShopItemsU3Ec__Iterator1_t3108955028::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (SpeedUpTableController_t3709478523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[10] = 
{
	SpeedUpTableController_t3709478523::get_offset_of_currentSourceArray_2(),
	SpeedUpTableController_t3709478523::get_offset_of_m_cellPrefab_3(),
	SpeedUpTableController_t3709478523::get_offset_of_m_tableView_4(),
	SpeedUpTableController_t3709478523::get_offset_of_itemImage_5(),
	SpeedUpTableController_t3709478523::get_offset_of_timeLeft_6(),
	SpeedUpTableController_t3709478523::get_offset_of_eventId_7(),
	SpeedUpTableController_t3709478523::get_offset_of_startTime_8(),
	SpeedUpTableController_t3709478523::get_offset_of_finishTime_9(),
	SpeedUpTableController_t3709478523::get_offset_of_owner_10(),
	SpeedUpTableController_t3709478523::get_offset_of_timespan_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (SoundManager_t2102329059), -1, sizeof(SoundManager_t2102329059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3541[2] = 
{
	SoundManager_t2102329059_StaticFields::get_offset_of_instance_2(),
	SoundManager_t2102329059::get_offset_of_music_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (AllianceAlliancesTableController_t3866927190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[4] = 
{
	AllianceAlliancesTableController_t3866927190::get_offset_of_m_cellPrefab_2(),
	AllianceAlliancesTableController_t3866927190::get_offset_of_m_tableView_3(),
	AllianceAlliancesTableController_t3866927190::get_offset_of_alliances_4(),
	AllianceAlliancesTableController_t3866927190::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (AllianceEventsTableController_t2478949448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[5] = 
{
	AllianceEventsTableController_t2478949448::get_offset_of_m_cellPrefab_2(),
	AllianceEventsTableController_t2478949448::get_offset_of_m_tableView_3(),
	AllianceEventsTableController_t2478949448::get_offset_of_m_numRows_4(),
	AllianceEventsTableController_t2478949448::get_offset_of_allianceEvents_5(),
	AllianceEventsTableController_t2478949448::get_offset_of_onGetAllianceEvents_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (U3CGetAllianceEventsU3Ec__Iterator0_t939093064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3544[5] = 
{
	U3CGetAllianceEventsU3Ec__Iterator0_t939093064::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetAllianceEventsU3Ec__Iterator0_t939093064::get_offset_of_U24this_1(),
	U3CGetAllianceEventsU3Ec__Iterator0_t939093064::get_offset_of_U24current_2(),
	U3CGetAllianceEventsU3Ec__Iterator0_t939093064::get_offset_of_U24disposing_3(),
	U3CGetAllianceEventsU3Ec__Iterator0_t939093064::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (AllianceInfoTableContoller_t4165916044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3545[9] = 
{
	AllianceInfoTableContoller_t4165916044::get_offset_of_m_cellPrefab_2(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_m_tableView_3(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_neutral_4(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_ally_5(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_enemy_6(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_neutralAlliances_7(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_allyAlliances_8(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_enemyAlliances_9(),
	AllianceInfoTableContoller_t4165916044::get_offset_of_m_numRows_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (AllianceInviteTableController_t1458639565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3546[6] = 
{
	AllianceInviteTableController_t1458639565::get_offset_of_m_cellPrefab_2(),
	AllianceInviteTableController_t1458639565::get_offset_of_m_tableView_3(),
	AllianceInviteTableController_t1458639565::get_offset_of_allUsers_4(),
	AllianceInviteTableController_t1458639565::get_offset_of_m_numRows_5(),
	AllianceInviteTableController_t1458639565::get_offset_of_onGetAllUsers_6(),
	AllianceInviteTableController_t1458639565::get_offset_of_dateFormat_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (U3CGetAllUsersU3Ec__Iterator0_t3081675446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3547[5] = 
{
	U3CGetAllUsersU3Ec__Iterator0_t3081675446::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetAllUsersU3Ec__Iterator0_t3081675446::get_offset_of_U24this_1(),
	U3CGetAllUsersU3Ec__Iterator0_t3081675446::get_offset_of_U24current_2(),
	U3CGetAllUsersU3Ec__Iterator0_t3081675446::get_offset_of_U24disposing_3(),
	U3CGetAllUsersU3Ec__Iterator0_t3081675446::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (AllianceMembersPermisionsTableController_t3675389298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[4] = 
{
	AllianceMembersPermisionsTableController_t3675389298::get_offset_of_m_cellPrefab_2(),
	AllianceMembersPermisionsTableController_t3675389298::get_offset_of_m_tableView_3(),
	AllianceMembersPermisionsTableController_t3675389298::get_offset_of_allianceMembers_4(),
	AllianceMembersPermisionsTableController_t3675389298::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (BattleReportItemsTableController_t14710115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[4] = 
{
	BattleReportItemsTableController_t14710115::get_offset_of_m_cellPrefab_2(),
	BattleReportItemsTableController_t14710115::get_offset_of_m_tableView_3(),
	BattleReportItemsTableController_t14710115::get_offset_of_items_4(),
	BattleReportItemsTableController_t14710115::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (BattleReportResourcesTableController_t1929107072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[4] = 
{
	BattleReportResourcesTableController_t1929107072::get_offset_of_m_cellPrefab_2(),
	BattleReportResourcesTableController_t1929107072::get_offset_of_m_tableView_3(),
	BattleReportResourcesTableController_t1929107072::get_offset_of_armyResources_4(),
	BattleReportResourcesTableController_t1929107072::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (BattleReportUnitTableController_t1840302685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3551[6] = 
{
	BattleReportUnitTableController_t1840302685::get_offset_of_m_cellPrefab_2(),
	BattleReportUnitTableController_t1840302685::get_offset_of_m_tableView_3(),
	BattleReportUnitTableController_t1840302685::get_offset_of_unitNames_4(),
	BattleReportUnitTableController_t1840302685::get_offset_of_unitBeforeCount_5(),
	BattleReportUnitTableController_t1840302685::get_offset_of_unitAfterCount_6(),
	BattleReportUnitTableController_t1840302685::get_offset_of_m_numRows_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (CommandCenterArmiesTableController_t2609779151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3552[4] = 
{
	CommandCenterArmiesTableController_t2609779151::get_offset_of_m_cellPrefab_2(),
	CommandCenterArmiesTableController_t2609779151::get_offset_of_m_tableView_3(),
	CommandCenterArmiesTableController_t2609779151::get_offset_of_cityArmies_4(),
	CommandCenterArmiesTableController_t2609779151::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (FinanceTableController_t3435833683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[4] = 
{
	FinanceTableController_t3435833683::get_offset_of_owner_2(),
	FinanceTableController_t3435833683::get_offset_of_currentSourceArray_3(),
	FinanceTableController_t3435833683::get_offset_of_m_cellPrefab_4(),
	FinanceTableController_t3435833683::get_offset_of_m_tableView_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (GuestFeastingTableController_t3137115560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[8] = 
{
	GuestFeastingTableController_t3137115560::get_offset_of_m_cellPrefab_2(),
	GuestFeastingTableController_t3137115560::get_offset_of_m_tableView_3(),
	GuestFeastingTableController_t3137115560::get_offset_of_guest_4(),
	GuestFeastingTableController_t3137115560::get_offset_of_generalsContainer_5(),
	GuestFeastingTableController_t3137115560::get_offset_of_requestedGeneral_6(),
	GuestFeastingTableController_t3137115560::get_offset_of_displayedHeroesCount_7(),
	GuestFeastingTableController_t3137115560::get_offset_of_confirmed_8(),
	GuestFeastingTableController_t3137115560::get_offset_of_m_numRows_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3555[6] = 
{
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U3CpostfornU3E__0_0(),
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U3CrequestU3E__0_1(),
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U24this_2(),
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U24current_3(),
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U24disposing_4(),
	U3CGetAllAvailableGeneralsU3Ec__Iterator0_t3806474765::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (MailAllianceReportTableController_t2367082911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3556[5] = 
{
	MailAllianceReportTableController_t2367082911::get_offset_of_m_cellPrefab_2(),
	MailAllianceReportTableController_t2367082911::get_offset_of_m_tableView_3(),
	MailAllianceReportTableController_t2367082911::get_offset_of_alliance_4(),
	MailAllianceReportTableController_t2367082911::get_offset_of_allReports_5(),
	MailAllianceReportTableController_t2367082911::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (MailInboxTableController_t2667561573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[7] = 
{
	MailInboxTableController_t2667561573::get_offset_of_m_cellPrefab_2(),
	MailInboxTableController_t2667561573::get_offset_of_m_tableView_3(),
	MailInboxTableController_t2667561573::get_offset_of_username_4(),
	MailInboxTableController_t2667561573::get_offset_of_allMessages_5(),
	MailInboxTableController_t2667561573::get_offset_of_searchResults_6(),
	MailInboxTableController_t2667561573::get_offset_of_search_7(),
	MailInboxTableController_t2667561573::get_offset_of_m_numRows_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (MailInviteTableController_t1579929683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[7] = 
{
	MailInviteTableController_t1579929683::get_offset_of_m_cellPrefab_2(),
	MailInviteTableController_t1579929683::get_offset_of_m_tableView_3(),
	MailInviteTableController_t1579929683::get_offset_of_allInvites_4(),
	MailInviteTableController_t1579929683::get_offset_of_mail_5(),
	MailInviteTableController_t1579929683::get_offset_of_confirmed_6(),
	MailInviteTableController_t1579929683::get_offset_of_inviteId_7(),
	MailInviteTableController_t1579929683::get_offset_of_m_numRows_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (MailSystemTableController_t2081682146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3559[4] = 
{
	MailSystemTableController_t2081682146::get_offset_of_m_cellPrefab_2(),
	MailSystemTableController_t2081682146::get_offset_of_m_tableView_3(),
	MailSystemTableController_t2081682146::get_offset_of_allSystem_4(),
	MailSystemTableController_t2081682146::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (MarketBuyTableController_t892139868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[5] = 
{
	MarketBuyTableController_t892139868::get_offset_of_owner_2(),
	MarketBuyTableController_t892139868::get_offset_of_m_cellPrefab_3(),
	MarketBuyTableController_t892139868::get_offset_of_m_tableView_4(),
	MarketBuyTableController_t892139868::get_offset_of_lots_5(),
	MarketBuyTableController_t892139868::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (MarketSellTableController_t878431470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[5] = 
{
	MarketSellTableController_t878431470::get_offset_of_owner_2(),
	MarketSellTableController_t878431470::get_offset_of_m_cellPrefab_3(),
	MarketSellTableController_t878431470::get_offset_of_m_tableView_4(),
	MarketSellTableController_t878431470::get_offset_of_lots_5(),
	MarketSellTableController_t878431470::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (NewMailPlayersTableController_t2252158828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[6] = 
{
	NewMailPlayersTableController_t2252158828::get_offset_of_owner_2(),
	NewMailPlayersTableController_t2252158828::get_offset_of_m_cellPrefab_3(),
	NewMailPlayersTableController_t2252158828::get_offset_of_m_tableView_4(),
	NewMailPlayersTableController_t2252158828::get_offset_of_allUsers_5(),
	NewMailPlayersTableController_t2252158828::get_offset_of_m_numRows_6(),
	NewMailPlayersTableController_t2252158828::get_offset_of_onGetAllUsers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (U3CGetAllUsersU3Ec__Iterator0_t3310400646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[5] = 
{
	U3CGetAllUsersU3Ec__Iterator0_t3310400646::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetAllUsersU3Ec__Iterator0_t3310400646::get_offset_of_U24this_1(),
	U3CGetAllUsersU3Ec__Iterator0_t3310400646::get_offset_of_U24current_2(),
	U3CGetAllUsersU3Ec__Iterator0_t3310400646::get_offset_of_U24disposing_3(),
	U3CGetAllUsersU3Ec__Iterator0_t3310400646::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (PanelKnightsTableController_t3023536378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[4] = 
{
	PanelKnightsTableController_t3023536378::get_offset_of_m_cellPrefab_2(),
	PanelKnightsTableController_t3023536378::get_offset_of_m_tableView_3(),
	PanelKnightsTableController_t3023536378::get_offset_of_knightList_4(),
	PanelKnightsTableController_t3023536378::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (PanelTableController_t3691279756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3565[5] = 
{
	PanelTableController_t3691279756::get_offset_of_m_cellPrefab_2(),
	PanelTableController_t3691279756::get_offset_of_m_tableView_3(),
	PanelTableController_t3691279756::get_offset_of_actionList_4(),
	PanelTableController_t3691279756::get_offset_of_content_5(),
	PanelTableController_t3691279756::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (ResidenceBuildingsTableController_t4099106534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3566[5] = 
{
	ResidenceBuildingsTableController_t4099106534::get_offset_of_m_cellPrefab_2(),
	ResidenceBuildingsTableController_t4099106534::get_offset_of_m_tableView_3(),
	ResidenceBuildingsTableController_t4099106534::get_offset_of_buildings_4(),
	ResidenceBuildingsTableController_t4099106534::get_offset_of_buildingsList_5(),
	ResidenceBuildingsTableController_t4099106534::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (ResidenceCitiesTableController_t4096198820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3567[4] = 
{
	ResidenceCitiesTableController_t4096198820::get_offset_of_m_cellPrefab_2(),
	ResidenceCitiesTableController_t4096198820::get_offset_of_m_tableView_3(),
	ResidenceCitiesTableController_t4096198820::get_offset_of_citiesList_4(),
	ResidenceCitiesTableController_t4096198820::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (ResidenceColoniesTableController_t3519043985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3568[5] = 
{
	ResidenceColoniesTableController_t3519043985::get_offset_of_m_cellPrefab_2(),
	ResidenceColoniesTableController_t3519043985::get_offset_of_m_tableView_3(),
	ResidenceColoniesTableController_t3519043985::get_offset_of_citiesList_4(),
	ResidenceColoniesTableController_t3519043985::get_offset_of_gotCityColonies_5(),
	ResidenceColoniesTableController_t3519043985::get_offset_of_m_numRows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (U3CGetCityColoniesU3Ec__Iterator0_t525247041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[5] = 
{
	U3CGetCityColoniesU3Ec__Iterator0_t525247041::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetCityColoniesU3Ec__Iterator0_t525247041::get_offset_of_U24this_1(),
	U3CGetCityColoniesU3Ec__Iterator0_t525247041::get_offset_of_U24current_2(),
	U3CGetCityColoniesU3Ec__Iterator0_t525247041::get_offset_of_U24disposing_3(),
	U3CGetCityColoniesU3Ec__Iterator0_t525247041::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (ResidenceValleysTableController_t655612456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[4] = 
{
	ResidenceValleysTableController_t655612456::get_offset_of_m_cellPrefab_2(),
	ResidenceValleysTableController_t655612456::get_offset_of_m_tableView_3(),
	ResidenceValleysTableController_t655612456::get_offset_of_buildingsList_4(),
	ResidenceValleysTableController_t655612456::get_offset_of_m_numRows_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (UpgradeBuildingTableController_t2757797628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[8] = 
{
	UpgradeBuildingTableController_t2757797628::get_offset_of_m_cellPrefab_2(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_m_tableView_3(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_content_4(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_buildingsList_5(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_item_id_6(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_owner_7(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_building_id_8(),
	UpgradeBuildingTableController_t2757797628::get_offset_of_m_numRows_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (AllianceAlliancesLine_t2112943226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3572[9] = 
{
	AllianceAlliancesLine_t2112943226::get_offset_of_allianceName_2(),
	AllianceAlliancesLine_t2112943226::get_offset_of_founder_3(),
	AllianceAlliancesLine_t2112943226::get_offset_of_leader_4(),
	AllianceAlliancesLine_t2112943226::get_offset_of_experience_5(),
	AllianceAlliancesLine_t2112943226::get_offset_of_rank_6(),
	AllianceAlliancesLine_t2112943226::get_offset_of_members_7(),
	AllianceAlliancesLine_t2112943226::get_offset_of_joinBtn_8(),
	AllianceAlliancesLine_t2112943226::get_offset_of_allianceId_9(),
	AllianceAlliancesLine_t2112943226::get_offset_of_owner_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (AllianceEventsLine_t1491641488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[2] = 
{
	AllianceEventsLine_t1491641488::get_offset_of_date_2(),
	AllianceEventsLine_t1491641488::get_offset_of_remark_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (AllianceInfoAlliancesLine_t1537220308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[8] = 
{
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_allianceName_2(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_rank_3(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_members_4(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_neutralBTN_5(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_allyBTN_6(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_enemyBTN_7(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_owner_8(),
	AllianceInfoAlliancesLine_t1537220308::get_offset_of_allianceId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (AllianceInviteLine_t1691417839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[9] = 
{
	AllianceInviteLine_t1691417839::get_offset_of_memberName_2(),
	AllianceInviteLine_t1691417839::get_offset_of_allianceName_3(),
	AllianceInviteLine_t1691417839::get_offset_of_rank_4(),
	AllianceInviteLine_t1691417839::get_offset_of_experience_5(),
	AllianceInviteLine_t1691417839::get_offset_of_cities_6(),
	AllianceInviteLine_t1691417839::get_offset_of_capitalName_7(),
	AllianceInviteLine_t1691417839::get_offset_of_lastOnline_8(),
	AllianceInviteLine_t1691417839::get_offset_of_user_9(),
	AllianceInviteLine_t1691417839::get_offset_of_owner_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (AllianceMembersPermissionsLine_t2063553011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3576[8] = 
{
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_memberName_2(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_position_3(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_rank_4(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_experience_5(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_cities_6(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_capitalName_7(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_lastOnline_8(),
	AllianceMembersPermissionsLine_t2063553011::get_offset_of_user_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (BattleReportItemsLine_t1544447758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[3] = 
{
	BattleReportItemsLine_t1544447758::get_offset_of_itemName_2(),
	BattleReportItemsLine_t1544447758::get_offset_of_itemPrice_3(),
	BattleReportItemsLine_t1544447758::get_offset_of_itemCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (BattleReportResourcesLine_t3745703875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[7] = 
{
	BattleReportResourcesLine_t3745703875::get_offset_of_gold_2(),
	BattleReportResourcesLine_t3745703875::get_offset_of_silver_3(),
	BattleReportResourcesLine_t3745703875::get_offset_of_food_4(),
	BattleReportResourcesLine_t3745703875::get_offset_of_wood_5(),
	BattleReportResourcesLine_t3745703875::get_offset_of_iron_6(),
	BattleReportResourcesLine_t3745703875::get_offset_of_copper_7(),
	BattleReportResourcesLine_t3745703875::get_offset_of_stone_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (BattleReportUnitsLine_t2550456670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[4] = 
{
	BattleReportUnitsLine_t2550456670::get_offset_of_unitName_2(),
	BattleReportUnitsLine_t2550456670::get_offset_of_beforeCount_3(),
	BattleReportUnitsLine_t2550456670::get_offset_of_lostCount_4(),
	BattleReportUnitsLine_t2550456670::get_offset_of_remainingCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (CityItemLine_t1380899866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[5] = 
{
	CityItemLine_t1380899866::get_offset_of_cityImage_2(),
	CityItemLine_t1380899866::get_offset_of_backGround_3(),
	CityItemLine_t1380899866::get_offset_of_coords_4(),
	CityItemLine_t1380899866::get_offset_of_cityId_5(),
	CityItemLine_t1380899866::get_offset_of_currentCity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (CommandCenterArmyTableLine_t1166067843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[4] = 
{
	CommandCenterArmyTableLine_t1166067843::get_offset_of_knightName_2(),
	CommandCenterArmyTableLine_t1166067843::get_offset_of_status_3(),
	CommandCenterArmyTableLine_t1166067843::get_offset_of_armyId_4(),
	CommandCenterArmyTableLine_t1166067843::get_offset_of_owner_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (DiplomacyTableLine_t1854275108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[5] = 
{
	DiplomacyTableLine_t1854275108::get_offset_of_nickName_2(),
	DiplomacyTableLine_t1854275108::get_offset_of_knight_3(),
	DiplomacyTableLine_t1854275108::get_offset_of_city_4(),
	DiplomacyTableLine_t1854275108::get_offset_of_coord_5(),
	DiplomacyTableLine_t1854275108::get_offset_of_date_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (FinancialTableLine_t3445994354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[9] = 
{
	FinancialTableLine_t3445994354::get_offset_of_productImage_2(),
	FinancialTableLine_t3445994354::get_offset_of_productName_3(),
	FinancialTableLine_t3445994354::get_offset_of_quantity_4(),
	FinancialTableLine_t3445994354::get_offset_of_price_5(),
	FinancialTableLine_t3445994354::get_offset_of_date_6(),
	FinancialTableLine_t3445994354::get_offset_of_fromUser_7(),
	FinancialTableLine_t3445994354::get_offset_of_cancelOfferBtn_8(),
	FinancialTableLine_t3445994354::get_offset_of_owner_9(),
	FinancialTableLine_t3445994354::get_offset_of_lotId_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (GuestHouseTableLine_t228100067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[12] = 
{
	GuestHouseTableLine_t228100067::get_offset_of_knightName_2(),
	GuestHouseTableLine_t228100067::get_offset_of_politicsLevel_3(),
	GuestHouseTableLine_t228100067::get_offset_of_marchesLevel_4(),
	GuestHouseTableLine_t228100067::get_offset_of_speedLevel_5(),
	GuestHouseTableLine_t228100067::get_offset_of_loyaltyLevel_6(),
	GuestHouseTableLine_t228100067::get_offset_of_knightLevel_7(),
	GuestHouseTableLine_t228100067::get_offset_of_salary_8(),
	GuestHouseTableLine_t228100067::get_offset_of_status_9(),
	GuestHouseTableLine_t228100067::get_offset_of_actionText_10(),
	GuestHouseTableLine_t228100067::get_offset_of_nameBackground_11(),
	GuestHouseTableLine_t228100067::get_offset_of_owner_12(),
	GuestHouseTableLine_t228100067::get_offset_of_general_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (MailAllianceReportTableLine_t2381192320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3585[7] = 
{
	MailAllianceReportTableLine_t2381192320::get_offset_of_userName_2(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_type_3(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_source_4(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_target_5(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_time_6(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_reportId_7(),
	MailAllianceReportTableLine_t2381192320::get_offset_of_owner_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (MailInboxLine_t3110909470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3586[5] = 
{
	MailInboxLine_t3110909470::get_offset_of_playerName_2(),
	MailInboxLine_t3110909470::get_offset_of_subject_3(),
	MailInboxLine_t3110909470::get_offset_of_date_4(),
	MailInboxLine_t3110909470::get_offset_of_messageId_5(),
	MailInboxLine_t3110909470::get_offset_of_owner_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (MailInviteLine_t4144392430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3587[4] = 
{
	MailInviteLine_t4144392430::get_offset_of_senderName_2(),
	MailInviteLine_t4144392430::get_offset_of_date_3(),
	MailInviteLine_t4144392430::get_offset_of_owner_4(),
	MailInviteLine_t4144392430::get_offset_of_inviteId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (MailSystemLine_t2170509778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3588[2] = 
{
	MailSystemLine_t2170509778::get_offset_of_subject_2(),
	MailSystemLine_t2170509778::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (MarketBuyTableLine_t264875335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3589[2] = 
{
	MarketBuyTableLine_t264875335::get_offset_of_quantity_2(),
	MarketBuyTableLine_t264875335::get_offset_of_unitPrice_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (MarketSellTableLine_t228365181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3590[4] = 
{
	MarketSellTableLine_t228365181::get_offset_of_quantity_2(),
	MarketSellTableLine_t228365181::get_offset_of_unitPrice_3(),
	MarketSellTableLine_t228365181::get_offset_of_lotId_4(),
	MarketSellTableLine_t228365181::get_offset_of_owner_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (NewMailPlayerLine_t272747317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3591[3] = 
{
	NewMailPlayerLine_t272747317::get_offset_of_playeName_2(),
	NewMailPlayerLine_t272747317::get_offset_of_playerId_3(),
	NewMailPlayerLine_t272747317::get_offset_of_manager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (PanelKnightTableLine_t48687488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[4] = 
{
	PanelKnightTableLine_t48687488::get_offset_of_knightName_2(),
	PanelKnightTableLine_t48687488::get_offset_of_knightLevel_3(),
	PanelKnightTableLine_t48687488::get_offset_of_status_4(),
	PanelKnightTableLine_t48687488::get_offset_of_knightId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (PanelTableLine_t11631953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3593[15] = 
{
	PanelTableLine_t11631953::get_offset_of_actionImage_2(),
	PanelTableLine_t11631953::get_offset_of_itemImage_3(),
	PanelTableLine_t11631953::get_offset_of_itemText_4(),
	PanelTableLine_t11631953::get_offset_of_itemTimer_5(),
	PanelTableLine_t11631953::get_offset_of_speedUpBTN_6(),
	PanelTableLine_t11631953::get_offset_of_cancelBTN_7(),
	PanelTableLine_t11631953::get_offset_of_progressBar_8(),
	PanelTableLine_t11631953::get_offset_of_eventId_9(),
	PanelTableLine_t11631953::get_offset_of_itemId_10(),
	PanelTableLine_t11631953::get_offset_of_owner_11(),
	PanelTableLine_t11631953::get_offset_of_startTime_12(),
	PanelTableLine_t11631953::get_offset_of_finishTime_13(),
	PanelTableLine_t11631953::get_offset_of_cityBuilding_14(),
	PanelTableLine_t11631953::get_offset_of_timespan_15(),
	PanelTableLine_t11631953::get_offset_of_valuePerSecond_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (ResidenceBuildingsTableLine_t2264296562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3594[7] = 
{
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_buildingIcon_2(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_buildingName_3(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_buildingLevel_4(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_status_5(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_type_6(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_owner_7(),
	ResidenceBuildingsTableLine_t2264296562::get_offset_of_pitId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (ResidenceCitiesTableLine_t3242262530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[7] = 
{
	ResidenceCitiesTableLine_t3242262530::get_offset_of_cityName_2(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_region_3(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_cityCoords_4(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_mayerLevel_5(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_fieldsCount_6(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_valleysCount_7(),
	ResidenceCitiesTableLine_t3242262530::get_offset_of_owner_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (ResidenceColoniesTableLine_t998651070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[7] = 
{
	ResidenceColoniesTableLine_t998651070::get_offset_of_playerName_2(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_cityName_3(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_region_4(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_cityCoords_5(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_mayerLevel_6(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_owner_7(),
	ResidenceColoniesTableLine_t998651070::get_offset_of_tributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (ResidenceValleysTableLine_t1359679749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[8] = 
{
	ResidenceValleysTableLine_t1359679749::get_offset_of_valleyName_2(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_resource_3(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_region_4(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_valleyCoords_5(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_valleyLevel_6(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_production_7(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_model_8(),
	ResidenceValleysTableLine_t1359679749::get_offset_of_owner_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (UpgradeBuildingTableLine_t2892907218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[5] = 
{
	UpgradeBuildingTableLine_t2892907218::get_offset_of_buildingIcon_2(),
	UpgradeBuildingTableLine_t2892907218::get_offset_of_buildingName_3(),
	UpgradeBuildingTableLine_t2892907218::get_offset_of_buildingLevel_4(),
	UpgradeBuildingTableLine_t2892907218::get_offset_of_owner_5(),
	UpgradeBuildingTableLine_t2892907218::get_offset_of_id_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (TestWorldChunksManager_t1200850834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3599[3] = 
{
	TestWorldChunksManager_t1200850834::get_offset_of_worldChunks_2(),
	TestWorldChunksManager_t1200850834::get_offset_of_targetX_3(),
	TestWorldChunksManager_t1200850834::get_offset_of_targetY_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
