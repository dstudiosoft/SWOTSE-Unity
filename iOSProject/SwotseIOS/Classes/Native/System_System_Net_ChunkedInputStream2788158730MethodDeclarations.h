﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ChunkedInputStream
struct ChunkedInputStream_t2788158730;
// System.Net.HttpListenerContext
struct HttpListenerContext_t506453093;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.ChunkStream
struct ChunkStream_t91719323;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpListenerContext506453093.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_Net_ChunkStream91719323.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.ChunkedInputStream::.ctor(System.Net.HttpListenerContext,System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void ChunkedInputStream__ctor_m1245273655 (ChunkedInputStream_t2788158730 * __this, HttpListenerContext_t506453093 * ___context0, Stream_t3255436806 * ___stream1, ByteU5BU5D_t3397334013* ___buffer2, int32_t ___offset3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ChunkStream System.Net.ChunkedInputStream::get_Decoder()
extern "C"  ChunkStream_t91719323 * ChunkedInputStream_get_Decoder_m2324588957 (ChunkedInputStream_t2788158730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::set_Decoder(System.Net.ChunkStream)
extern "C"  void ChunkedInputStream_set_Decoder_m1643405704 (ChunkedInputStream_t2788158730 * __this, ChunkStream_t91719323 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ChunkedInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkedInputStream_Read_m1400151555 (ChunkedInputStream_t2788158730 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.ChunkedInputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ChunkedInputStream_BeginRead_m1733124662 (ChunkedInputStream_t2788158730 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::OnRead(System.IAsyncResult)
extern "C"  void ChunkedInputStream_OnRead_m2843187866 (ChunkedInputStream_t2788158730 * __this, Il2CppObject * ___base_ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ChunkedInputStream::EndRead(System.IAsyncResult)
extern "C"  int32_t ChunkedInputStream_EndRead_m1128143328 (ChunkedInputStream_t2788158730 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::Close()
extern "C"  void ChunkedInputStream_Close_m1869211834 (ChunkedInputStream_t2788158730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
