﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldTimeContentManager
struct ShieldTimeContentManager_t603808652;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ShieldTimeContentManager::.ctor()
extern "C"  void ShieldTimeContentManager__ctor_m209144915 (ShieldTimeContentManager_t603808652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldTimeContentManager::.cctor()
extern "C"  void ShieldTimeContentManager__cctor_m2270391174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldTimeContentManager::OnEnable()
extern "C"  void ShieldTimeContentManager_OnEnable_m654846711 (ShieldTimeContentManager_t603808652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldTimeContentManager::ItemUsed(System.Object,System.String)
extern "C"  void ShieldTimeContentManager_ItemUsed_m1720211501 (ShieldTimeContentManager_t603808652 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldTimeContentManager::Ok()
extern "C"  void ShieldTimeContentManager_Ok_m2057345243 (ShieldTimeContentManager_t603808652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldTimeContentManager::Cancel()
extern "C"  void ShieldTimeContentManager_Cancel_m501679365 (ShieldTimeContentManager_t603808652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
