﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,ItemTimer,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2021753797(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t137826909 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3373533409_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,ItemTimer,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2233998201(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t137826909 *, int64_t, ItemTimer_t3839766396 *, const MethodInfo*))Transform_1_Invoke_m1299483797_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,ItemTimer,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1698296424(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t137826909 *, int64_t, ItemTimer_t3839766396 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3448776694_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,ItemTimer,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3156171867(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t137826909 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3006940059_gshared)(__this, ___result0, method)
