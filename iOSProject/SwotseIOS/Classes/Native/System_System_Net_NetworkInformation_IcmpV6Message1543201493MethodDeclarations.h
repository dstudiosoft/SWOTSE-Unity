﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IcmpV6MessageTypes
struct IcmpV6MessageTypes_t1543201493;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IcmpV6MessageTypes::.ctor()
extern "C"  void IcmpV6MessageTypes__ctor_m1415630527 (IcmpV6MessageTypes_t1543201493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
