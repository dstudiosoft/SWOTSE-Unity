﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ReportManager
struct ReportManager_t1736915555;
// AllianceManager
struct AllianceManager_t3412736524;
// LoginManager
struct LoginManager_t973619992;
// UserModel
struct UserModel_t3025569216;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1596653588  : public Il2CppObject
{
public:
	// ReportManager PlayerManager::reportManager
	ReportManager_t1736915555 * ___reportManager_0;
	// AllianceManager PlayerManager::allianceManager
	AllianceManager_t3412736524 * ___allianceManager_1;
	// LoginManager PlayerManager::loginManager
	LoginManager_t973619992 * ___loginManager_2;
	// UserModel PlayerManager::user
	UserModel_t3025569216 * ___user_3;
	// SimpleEvent PlayerManager::onGetMyUserInfo
	SimpleEvent_t1509017202 * ___onGetMyUserInfo_4;
	// SimpleEvent PlayerManager::onLanguageChanged
	SimpleEvent_t1509017202 * ___onLanguageChanged_5;
	// SimpleEvent PlayerManager::onPasswordChanged
	SimpleEvent_t1509017202 * ___onPasswordChanged_6;
	// SimpleEvent PlayerManager::onResetPasswordRequested
	SimpleEvent_t1509017202 * ___onResetPasswordRequested_7;
	// SimpleEvent PlayerManager::onPasswordReset
	SimpleEvent_t1509017202 * ___onPasswordReset_8;

public:
	inline static int32_t get_offset_of_reportManager_0() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___reportManager_0)); }
	inline ReportManager_t1736915555 * get_reportManager_0() const { return ___reportManager_0; }
	inline ReportManager_t1736915555 ** get_address_of_reportManager_0() { return &___reportManager_0; }
	inline void set_reportManager_0(ReportManager_t1736915555 * value)
	{
		___reportManager_0 = value;
		Il2CppCodeGenWriteBarrier(&___reportManager_0, value);
	}

	inline static int32_t get_offset_of_allianceManager_1() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___allianceManager_1)); }
	inline AllianceManager_t3412736524 * get_allianceManager_1() const { return ___allianceManager_1; }
	inline AllianceManager_t3412736524 ** get_address_of_allianceManager_1() { return &___allianceManager_1; }
	inline void set_allianceManager_1(AllianceManager_t3412736524 * value)
	{
		___allianceManager_1 = value;
		Il2CppCodeGenWriteBarrier(&___allianceManager_1, value);
	}

	inline static int32_t get_offset_of_loginManager_2() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___loginManager_2)); }
	inline LoginManager_t973619992 * get_loginManager_2() const { return ___loginManager_2; }
	inline LoginManager_t973619992 ** get_address_of_loginManager_2() { return &___loginManager_2; }
	inline void set_loginManager_2(LoginManager_t973619992 * value)
	{
		___loginManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginManager_2, value);
	}

	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___user_3)); }
	inline UserModel_t3025569216 * get_user_3() const { return ___user_3; }
	inline UserModel_t3025569216 ** get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(UserModel_t3025569216 * value)
	{
		___user_3 = value;
		Il2CppCodeGenWriteBarrier(&___user_3, value);
	}

	inline static int32_t get_offset_of_onGetMyUserInfo_4() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___onGetMyUserInfo_4)); }
	inline SimpleEvent_t1509017202 * get_onGetMyUserInfo_4() const { return ___onGetMyUserInfo_4; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetMyUserInfo_4() { return &___onGetMyUserInfo_4; }
	inline void set_onGetMyUserInfo_4(SimpleEvent_t1509017202 * value)
	{
		___onGetMyUserInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___onGetMyUserInfo_4, value);
	}

	inline static int32_t get_offset_of_onLanguageChanged_5() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___onLanguageChanged_5)); }
	inline SimpleEvent_t1509017202 * get_onLanguageChanged_5() const { return ___onLanguageChanged_5; }
	inline SimpleEvent_t1509017202 ** get_address_of_onLanguageChanged_5() { return &___onLanguageChanged_5; }
	inline void set_onLanguageChanged_5(SimpleEvent_t1509017202 * value)
	{
		___onLanguageChanged_5 = value;
		Il2CppCodeGenWriteBarrier(&___onLanguageChanged_5, value);
	}

	inline static int32_t get_offset_of_onPasswordChanged_6() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___onPasswordChanged_6)); }
	inline SimpleEvent_t1509017202 * get_onPasswordChanged_6() const { return ___onPasswordChanged_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onPasswordChanged_6() { return &___onPasswordChanged_6; }
	inline void set_onPasswordChanged_6(SimpleEvent_t1509017202 * value)
	{
		___onPasswordChanged_6 = value;
		Il2CppCodeGenWriteBarrier(&___onPasswordChanged_6, value);
	}

	inline static int32_t get_offset_of_onResetPasswordRequested_7() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___onResetPasswordRequested_7)); }
	inline SimpleEvent_t1509017202 * get_onResetPasswordRequested_7() const { return ___onResetPasswordRequested_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onResetPasswordRequested_7() { return &___onResetPasswordRequested_7; }
	inline void set_onResetPasswordRequested_7(SimpleEvent_t1509017202 * value)
	{
		___onResetPasswordRequested_7 = value;
		Il2CppCodeGenWriteBarrier(&___onResetPasswordRequested_7, value);
	}

	inline static int32_t get_offset_of_onPasswordReset_8() { return static_cast<int32_t>(offsetof(PlayerManager_t1596653588, ___onPasswordReset_8)); }
	inline SimpleEvent_t1509017202 * get_onPasswordReset_8() const { return ___onPasswordReset_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onPasswordReset_8() { return &___onPasswordReset_8; }
	inline void set_onPasswordReset_8(SimpleEvent_t1509017202 * value)
	{
		___onPasswordReset_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPasswordReset_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
