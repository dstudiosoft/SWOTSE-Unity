﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IPv4InterfaceProperties
struct IPv4InterfaceProperties_t1411071681;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IPv4InterfaceProperties::.ctor()
extern "C"  void IPv4InterfaceProperties__ctor_m3167868243 (IPv4InterfaceProperties_t1411071681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
