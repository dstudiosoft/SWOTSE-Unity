﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>
struct DefaultComparer_t4125990770;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::.ctor()
extern "C"  void DefaultComparer__ctor_m580747638_gshared (DefaultComparer_t4125990770 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m580747638(__this, method) ((  void (*) (DefaultComparer_t4125990770 *, const MethodInfo*))DefaultComparer__ctor_m580747638_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2460290037_gshared (DefaultComparer_t4125990770 * __this, IntPtr_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2460290037(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4125990770 *, IntPtr_t, const MethodInfo*))DefaultComparer_GetHashCode_m2460290037_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m524689061_gshared (DefaultComparer_t4125990770 * __this, IntPtr_t ___x0, IntPtr_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m524689061(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4125990770 *, IntPtr_t, IntPtr_t, const MethodInfo*))DefaultComparer_Equals_m524689061_gshared)(__this, ___x0, ___y1, method)
