﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceMembersPermisionsTableController
struct AllianceMembersPermisionsTableController_t2920559745;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceMembersPermisionsTableController::.ctor()
extern "C"  void AllianceMembersPermisionsTableController__ctor_m2275244382 (AllianceMembersPermisionsTableController_t2920559745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermisionsTableController::Start()
extern "C"  void AllianceMembersPermisionsTableController_Start_m1083741326 (AllianceMembersPermisionsTableController_t2920559745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermisionsTableController::OnEnable()
extern "C"  void AllianceMembersPermisionsTableController_OnEnable_m2219339630 (AllianceMembersPermisionsTableController_t2920559745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllianceMembersPermisionsTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t AllianceMembersPermisionsTableController_GetNumberOfRowsForTableView_m3942812408 (AllianceMembersPermisionsTableController_t2920559745 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AllianceMembersPermisionsTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float AllianceMembersPermisionsTableController_GetHeightForRowInTableView_m2217340368 (AllianceMembersPermisionsTableController_t2920559745 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell AllianceMembersPermisionsTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * AllianceMembersPermisionsTableController_GetCellForRowInTableView_m1819400701 (AllianceMembersPermisionsTableController_t2920559745 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceMembersPermisionsTableController::GotAllianceMembers(System.Object,System.String)
extern "C"  void AllianceMembersPermisionsTableController_GotAllianceMembers_m4123899130 (AllianceMembersPermisionsTableController_t2920559745 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
