﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActionManager
struct ActionManager_t1367723175;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ActionManager::.ctor()
extern "C"  void ActionManager__ctor_m1864532176 (ActionManager_t1367723175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::closePanel(System.String)
extern "C"  void ActionManager_closePanel_m3912889714 (ActionManager_t1367723175 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
