﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.StringList
struct StringList_t2332200693;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.StringList/StringListEnumerator
struct StringListEnumerator_t2590584790;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_StringList2332200693.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.StringList::.ctor(System.IntPtr,System.Boolean)
extern "C"  void StringList__ctor_m312512202 (StringList_t2332200693 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::.ctor(System.Collections.ICollection)
extern "C"  void StringList__ctor_m421882526 (StringList_t2332200693 * __this, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::.ctor()
extern "C"  void StringList__ctor_m1333009117 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::.ctor(Firebase.StringList)
extern "C"  void StringList__ctor_m2960140685 (StringList_t2332200693 * __this, StringList_t2332200693 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::.ctor(System.Int32)
extern "C"  void StringList__ctor_m3297838902 (StringList_t2332200693 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.StringList::getCPtr(Firebase.StringList)
extern "C"  HandleRef_t2419939847  StringList_getCPtr_m4152710572 (Il2CppObject * __this /* static, unused */, StringList_t2332200693 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Finalize()
extern "C"  void StringList_Finalize_m1566185715 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Dispose()
extern "C"  void StringList_Dispose_m3761705322 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList::get_IsFixedSize()
extern "C"  bool StringList_get_IsFixedSize_m449198137 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList::get_IsReadOnly()
extern "C"  bool StringList_get_IsReadOnly_m1053641238 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringList::get_Item(System.Int32)
extern "C"  String_t* StringList_get_Item_m2089203073 (StringList_t2332200693 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::set_Item(System.Int32,System.String)
extern "C"  void StringList_set_Item_m2014646324 (StringList_t2332200693 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.StringList::get_Capacity()
extern "C"  int32_t StringList_get_Capacity_m3494411444 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::set_Capacity(System.Int32)
extern "C"  void StringList_set_Capacity_m2851413165 (StringList_t2332200693 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.StringList::get_Count()
extern "C"  int32_t StringList_get_Count_m1252159681 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList::get_IsSynchronized()
extern "C"  bool StringList_get_IsSynchronized_m2013772498 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::CopyTo(System.String[])
extern "C"  void StringList_CopyTo_m1551877279 (StringList_t2332200693 * __this, StringU5BU5D_t1642385972* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::CopyTo(System.String[],System.Int32)
extern "C"  void StringList_CopyTo_m3537217454 (StringList_t2332200693 * __this, StringU5BU5D_t1642385972* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::CopyTo(System.Int32,System.String[],System.Int32,System.Int32)
extern "C"  void StringList_CopyTo_m471804692 (StringList_t2332200693 * __this, int32_t ___index0, StringU5BU5D_t1642385972* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> Firebase.StringList::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* StringList_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3103021806 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.StringList::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * StringList_System_Collections_IEnumerable_GetEnumerator_m3600205018 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.StringList/StringListEnumerator Firebase.StringList::GetEnumerator()
extern "C"  StringListEnumerator_t2590584790 * StringList_GetEnumerator_m1549350096 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Clear()
extern "C"  void StringList_Clear_m3922655398 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Add(System.String)
extern "C"  void StringList_Add_m181872658 (StringList_t2332200693 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.StringList::size()
extern "C"  uint32_t StringList_size_m284418231 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.StringList::capacity()
extern "C"  uint32_t StringList_capacity_m534791840 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::reserve(System.UInt32)
extern "C"  void StringList_reserve_m629687933 (StringList_t2332200693 * __this, uint32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringList::getitemcopy(System.Int32)
extern "C"  String_t* StringList_getitemcopy_m734988147 (StringList_t2332200693 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringList::getitem(System.Int32)
extern "C"  String_t* StringList_getitem_m373570060 (StringList_t2332200693 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::setitem(System.Int32,System.String)
extern "C"  void StringList_setitem_m1957559437 (StringList_t2332200693 * __this, int32_t ___index0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::AddRange(Firebase.StringList)
extern "C"  void StringList_AddRange_m2944720515 (StringList_t2332200693 * __this, StringList_t2332200693 * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.StringList Firebase.StringList::GetRange(System.Int32,System.Int32)
extern "C"  StringList_t2332200693 * StringList_GetRange_m3983604855 (StringList_t2332200693 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Insert(System.Int32,System.String)
extern "C"  void StringList_Insert_m4001241125 (StringList_t2332200693 * __this, int32_t ___index0, String_t* ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::InsertRange(System.Int32,Firebase.StringList)
extern "C"  void StringList_InsertRange_m2529573588 (StringList_t2332200693 * __this, int32_t ___index0, StringList_t2332200693 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::RemoveAt(System.Int32)
extern "C"  void StringList_RemoveAt_m1259654511 (StringList_t2332200693 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::RemoveRange(System.Int32,System.Int32)
extern "C"  void StringList_RemoveRange_m2279351578 (StringList_t2332200693 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.StringList Firebase.StringList::Repeat(System.String,System.Int32)
extern "C"  StringList_t2332200693 * StringList_Repeat_m3099891106 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Reverse()
extern "C"  void StringList_Reverse_m2435476385 (StringList_t2332200693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::Reverse(System.Int32,System.Int32)
extern "C"  void StringList_Reverse_m3828820421 (StringList_t2332200693 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList::SetRange(System.Int32,Firebase.StringList)
extern "C"  void StringList_SetRange_m2990413085 (StringList_t2332200693 * __this, int32_t ___index0, StringList_t2332200693 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList::Contains(System.String)
extern "C"  bool StringList_Contains_m1697658242 (StringList_t2332200693 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.StringList::IndexOf(System.String)
extern "C"  int32_t StringList_IndexOf_m1796869658 (StringList_t2332200693 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.StringList::LastIndexOf(System.String)
extern "C"  int32_t StringList_LastIndexOf_m939689462 (StringList_t2332200693 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList::Remove(System.String)
extern "C"  bool StringList_Remove_m536881733 (StringList_t2332200693 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
