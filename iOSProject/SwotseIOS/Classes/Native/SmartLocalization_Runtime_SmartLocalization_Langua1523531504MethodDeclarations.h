﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0
struct U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t1523531504;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"

// System.Void SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::.ctor()
extern "C"  void U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0__ctor_m3629953774 (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t1523531504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager/<GetDeviceCultureIfSupported>c__AnonStorey0::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_U3CU3Em__0_m3221246192 (U3CGetDeviceCultureIfSupportedU3Ec__AnonStorey0_t1523531504 * __this, SmartCultureInfo_t2361725737 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
