﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResidenceCitiesTableLine
struct ResidenceCitiesTableLine_t1138332587;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceCitiesTableController
struct  ResidenceCitiesTableController_t1287619937  : public MonoBehaviour_t1158329972
{
public:
	// ResidenceCitiesTableLine ResidenceCitiesTableController::m_cellPrefab
	ResidenceCitiesTableLine_t1138332587 * ___m_cellPrefab_2;
	// Tacticsoft.TableView ResidenceCitiesTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.ArrayList ResidenceCitiesTableController::citiesList
	ArrayList_t4252133567 * ___citiesList_4;
	// System.Int32 ResidenceCitiesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t1287619937, ___m_cellPrefab_2)); }
	inline ResidenceCitiesTableLine_t1138332587 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline ResidenceCitiesTableLine_t1138332587 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(ResidenceCitiesTableLine_t1138332587 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t1287619937, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_citiesList_4() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t1287619937, ___citiesList_4)); }
	inline ArrayList_t4252133567 * get_citiesList_4() const { return ___citiesList_4; }
	inline ArrayList_t4252133567 ** get_address_of_citiesList_4() { return &___citiesList_4; }
	inline void set_citiesList_4(ArrayList_t4252133567 * value)
	{
		___citiesList_4 = value;
		Il2CppCodeGenWriteBarrier(&___citiesList_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableController_t1287619937, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
