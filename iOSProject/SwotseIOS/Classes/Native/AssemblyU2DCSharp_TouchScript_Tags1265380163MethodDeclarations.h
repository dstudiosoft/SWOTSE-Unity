﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Tags
struct Tags_t1265380163;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TouchScript.Tags::.ctor(TouchScript.Tags,System.Collections.Generic.IEnumerable`1<System.String>)
extern "C"  void Tags__ctor_m217605332 (Tags_t1265380163 * __this, Tags_t1265380163 * ___tags0, Il2CppObject* ___add1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor(TouchScript.Tags,System.String)
extern "C"  void Tags__ctor_m2547129877 (Tags_t1265380163 * __this, Tags_t1265380163 * ___tags0, String_t* ___add1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor(TouchScript.Tags)
extern "C"  void Tags__ctor_m3969474187 (Tags_t1265380163 * __this, Tags_t1265380163 * ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C"  void Tags__ctor_m2264509847 (Tags_t1265380163 * __this, Il2CppObject* ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor(System.String[])
extern "C"  void Tags__ctor_m3441749056 (Tags_t1265380163 * __this, StringU5BU5D_t1642385972* ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor(System.String)
extern "C"  void Tags__ctor_m2292420680 (Tags_t1265380163 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.ctor()
extern "C"  void Tags__ctor_m2321599242 (Tags_t1265380163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::.cctor()
extern "C"  void Tags__cctor_m3760229913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Tags::HasTag(System.String)
extern "C"  bool Tags_HasTag_m3733923232 (Tags_t1265380163 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::OnBeforeSerialize()
extern "C"  void Tags_OnBeforeSerialize_m708808922 (Tags_t1265380163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Tags::OnAfterDeserialize()
extern "C"  void Tags_OnAfterDeserialize_m4047363300 (Tags_t1265380163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TouchScript.Tags::ToString()
extern "C"  String_t* Tags_ToString_m820978881 (Tags_t1265380163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
