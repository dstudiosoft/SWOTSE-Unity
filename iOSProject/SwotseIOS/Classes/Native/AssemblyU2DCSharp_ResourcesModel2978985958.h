﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourcesModel
struct  ResourcesModel_t2978985958  : public Il2CppObject
{
public:
	// System.Int64 ResourcesModel::city
	int64_t ___city_0;
	// System.Int64 ResourcesModel::food
	int64_t ___food_1;
	// System.Int64 ResourcesModel::wood
	int64_t ___wood_2;
	// System.Int64 ResourcesModel::stone
	int64_t ___stone_3;
	// System.Int64 ResourcesModel::iron
	int64_t ___iron_4;
	// System.Int64 ResourcesModel::cooper
	int64_t ___cooper_5;
	// System.Int64 ResourcesModel::gold
	int64_t ___gold_6;
	// System.Int64 ResourcesModel::silver
	int64_t ___silver_7;
	// System.Int64 ResourcesModel::population
	int64_t ___population_8;
	// System.DateTime ResourcesModel::last_update
	DateTime_t693205669  ___last_update_9;

public:
	inline static int32_t get_offset_of_city_0() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___city_0)); }
	inline int64_t get_city_0() const { return ___city_0; }
	inline int64_t* get_address_of_city_0() { return &___city_0; }
	inline void set_city_0(int64_t value)
	{
		___city_0 = value;
	}

	inline static int32_t get_offset_of_food_1() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___food_1)); }
	inline int64_t get_food_1() const { return ___food_1; }
	inline int64_t* get_address_of_food_1() { return &___food_1; }
	inline void set_food_1(int64_t value)
	{
		___food_1 = value;
	}

	inline static int32_t get_offset_of_wood_2() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___wood_2)); }
	inline int64_t get_wood_2() const { return ___wood_2; }
	inline int64_t* get_address_of_wood_2() { return &___wood_2; }
	inline void set_wood_2(int64_t value)
	{
		___wood_2 = value;
	}

	inline static int32_t get_offset_of_stone_3() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___stone_3)); }
	inline int64_t get_stone_3() const { return ___stone_3; }
	inline int64_t* get_address_of_stone_3() { return &___stone_3; }
	inline void set_stone_3(int64_t value)
	{
		___stone_3 = value;
	}

	inline static int32_t get_offset_of_iron_4() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___iron_4)); }
	inline int64_t get_iron_4() const { return ___iron_4; }
	inline int64_t* get_address_of_iron_4() { return &___iron_4; }
	inline void set_iron_4(int64_t value)
	{
		___iron_4 = value;
	}

	inline static int32_t get_offset_of_cooper_5() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___cooper_5)); }
	inline int64_t get_cooper_5() const { return ___cooper_5; }
	inline int64_t* get_address_of_cooper_5() { return &___cooper_5; }
	inline void set_cooper_5(int64_t value)
	{
		___cooper_5 = value;
	}

	inline static int32_t get_offset_of_gold_6() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___gold_6)); }
	inline int64_t get_gold_6() const { return ___gold_6; }
	inline int64_t* get_address_of_gold_6() { return &___gold_6; }
	inline void set_gold_6(int64_t value)
	{
		___gold_6 = value;
	}

	inline static int32_t get_offset_of_silver_7() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___silver_7)); }
	inline int64_t get_silver_7() const { return ___silver_7; }
	inline int64_t* get_address_of_silver_7() { return &___silver_7; }
	inline void set_silver_7(int64_t value)
	{
		___silver_7 = value;
	}

	inline static int32_t get_offset_of_population_8() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___population_8)); }
	inline int64_t get_population_8() const { return ___population_8; }
	inline int64_t* get_address_of_population_8() { return &___population_8; }
	inline void set_population_8(int64_t value)
	{
		___population_8 = value;
	}

	inline static int32_t get_offset_of_last_update_9() { return static_cast<int32_t>(offsetof(ResourcesModel_t2978985958, ___last_update_9)); }
	inline DateTime_t693205669  get_last_update_9() const { return ___last_update_9; }
	inline DateTime_t693205669 * get_address_of_last_update_9() { return &___last_update_9; }
	inline void set_last_update_9(DateTime_t693205669  value)
	{
		___last_update_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
