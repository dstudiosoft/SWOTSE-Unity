﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3995400347.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23136648085.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m116589272_gshared (InternalEnumerator_1_t3995400347 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m116589272(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3995400347 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m116589272_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2135901264_gshared (InternalEnumerator_1_t3995400347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2135901264(__this, method) ((  void (*) (InternalEnumerator_1_t3995400347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2135901264_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2400903942_gshared (InternalEnumerator_1_t3995400347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2400903942(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3995400347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2400903942_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2039284427_gshared (InternalEnumerator_1_t3995400347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2039284427(__this, method) ((  void (*) (InternalEnumerator_1_t3995400347 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2039284427_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2217542768_gshared (InternalEnumerator_1_t3995400347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2217542768(__this, method) ((  bool (*) (InternalEnumerator_1_t3995400347 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2217542768_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t3136648085  InternalEnumerator_1_get_Current_m1187319135_gshared (InternalEnumerator_1_t3995400347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1187319135(__this, method) ((  KeyValuePair_2_t3136648085  (*) (InternalEnumerator_1_t3995400347 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1187319135_gshared)(__this, method)
