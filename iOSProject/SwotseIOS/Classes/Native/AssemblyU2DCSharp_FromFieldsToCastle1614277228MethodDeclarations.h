﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FromFieldsToCastle
struct FromFieldsToCastle_t1614277228;

#include "codegen/il2cpp-codegen.h"

// System.Void FromFieldsToCastle::.ctor()
extern "C"  void FromFieldsToCastle__ctor_m1005745715 (FromFieldsToCastle_t1614277228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FromFieldsToCastle::OnMouseUpAsButton()
extern "C"  void FromFieldsToCastle_OnMouseUpAsButton_m352459010 (FromFieldsToCastle_t1614277228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
