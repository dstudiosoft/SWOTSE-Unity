﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2404360269.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23136648085.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1064629015_gshared (Enumerator_t2404360269 * __this, Dictionary_2_t1084335567 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1064629015(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2404360269 *, Dictionary_2_t1084335567 *, const MethodInfo*))Enumerator__ctor_m1064629015_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1646077222_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1646077222(__this, method) ((  Il2CppObject * (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1646077222_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1279338360_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1279338360(__this, method) ((  void (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1279338360_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2259390945_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2259390945(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2259390945_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1814757932_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1814757932(__this, method) ((  Il2CppObject * (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1814757932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3763261310_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3763261310(__this, method) ((  Il2CppObject * (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3763261310_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m234597384_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m234597384(__this, method) ((  bool (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_MoveNext_m234597384_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t3136648085  Enumerator_get_Current_m1040844624_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1040844624(__this, method) ((  KeyValuePair_2_t3136648085  (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_get_Current_m1040844624_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m877324075_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m877324075(__this, method) ((  int32_t (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_get_CurrentKey_m877324075_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m1802181963_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1802181963(__this, method) ((  float (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_get_CurrentValue_m1802181963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m2351897729_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2351897729(__this, method) ((  void (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_Reset_m2351897729_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2264595884_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2264595884(__this, method) ((  void (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_VerifyState_m2264595884_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2558223424_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2558223424(__this, method) ((  void (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_VerifyCurrent_m2558223424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m941222243_gshared (Enumerator_t2404360269 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m941222243(__this, method) ((  void (*) (Enumerator_t2404360269 *, const MethodInfo*))Enumerator_Dispose_m941222243_gshared)(__this, method)
