﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3688466362;
// UniWebView
struct UniWebView_t941983939;
// Tacticsoft.Examples.ScrollingEventsHandler
struct ScrollingEventsHandler_t2846363661;
// SizeAndTransitionSceneManager
struct SizeAndTransitionSceneManager_t3659808263;
// System.Action
struct Action_t1264377477;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Text.StringBuilder
struct StringBuilder_t;
// TouchScript.TouchManagerInstance
struct TouchManagerInstance_t3252675660;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Transform
struct Transform_t3600365921;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t1334725428;
// TouchScript.InputSources.IInputSource
struct IInputSource_t3790135087;
// TouchScript.Tags
struct Tags_t3560929397;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UniWebViewEdgeInsets
struct UniWebViewEdgeInsets_t3496354243;
// Tacticsoft.Examples.VisibleCounterCell
struct VisibleCounterCell_t1279458109;
// Tacticsoft.TableView
struct TableView_t4228429533;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// Tacticsoft.Examples.DynamicHeightCell
struct DynamicHeightCell_t2796155881;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t285980105;
// Tacticsoft.TableView/CellVisibilityChangeEvent
struct CellVisibilityChangeEvent_t2201436281;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t3219259043;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t923838031;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>
struct Dictionary_2_t3961624650;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>
struct Dictionary_2_t3697813627;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UniWebView/LoadCompleteDelegate
struct LoadCompleteDelegate_t2989787613;
// UniWebView/LoadBeginDelegate
struct LoadBeginDelegate_t3414845260;
// UniWebView/ReceivedMessageDelegate
struct ReceivedMessageDelegate_t188390413;
// UniWebView/EvalJavaScriptFinishedDelegate
struct EvalJavaScriptFinishedDelegate_t3947010167;
// UniWebView/WebViewShouldCloseDelegate
struct WebViewShouldCloseDelegate_t3305779967;
// UniWebView/ReceivedKeyCodeDelegate
struct ReceivedKeyCodeDelegate_t1391812367;
// UniWebView/InsetsForScreenOreitationDelegate
struct InsetsForScreenOreitationDelegate_t202380036;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t3046220461;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent
struct CellHeightChangedEvent_t954279626;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745559_H
#define U3CMODULEU3E_T692745559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745559 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745559_H
#ifndef U3CLOADFROMJARPACKAGEU3EC__ITERATOR0_T3671744877_H
#define U3CLOADFROMJARPACKAGEU3EC__ITERATOR0_T3671744877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/<LoadFromJarPackage>c__Iterator0
struct  U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877  : public RuntimeObject
{
public:
	// System.String UniWebView/<LoadFromJarPackage>c__Iterator0::jarFilePath
	String_t* ___jarFilePath_0;
	// UnityEngine.WWW UniWebView/<LoadFromJarPackage>c__Iterator0::<stream>__0
	WWW_t3688466362 * ___U3CstreamU3E__0_1;
	// UniWebView UniWebView/<LoadFromJarPackage>c__Iterator0::$this
	UniWebView_t941983939 * ___U24this_2;
	// System.Object UniWebView/<LoadFromJarPackage>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UniWebView/<LoadFromJarPackage>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UniWebView/<LoadFromJarPackage>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_jarFilePath_0() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___jarFilePath_0)); }
	inline String_t* get_jarFilePath_0() const { return ___jarFilePath_0; }
	inline String_t** get_address_of_jarFilePath_0() { return &___jarFilePath_0; }
	inline void set_jarFilePath_0(String_t* value)
	{
		___jarFilePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___jarFilePath_0), value);
	}

	inline static int32_t get_offset_of_U3CstreamU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___U3CstreamU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CstreamU3E__0_1() const { return ___U3CstreamU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CstreamU3E__0_1() { return &___U3CstreamU3E__0_1; }
	inline void set_U3CstreamU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CstreamU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___U24this_2)); }
	inline UniWebView_t941983939 * get_U24this_2() const { return ___U24this_2; }
	inline UniWebView_t941983939 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UniWebView_t941983939 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMJARPACKAGEU3EC__ITERATOR0_T3671744877_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CANIMATETOSCROLLYU3EC__ITERATOR0_T2778569968_H
#define U3CANIMATETOSCROLLYU3EC__ITERATOR0_T2778569968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0
struct  U3CAnimateToScrollYU3Ec__Iterator0_t2778569968  : public RuntimeObject
{
public:
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<startScrollY>__0
	float ___U3CstartScrollYU3E__0_1;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::time
	float ___time_2;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<endTime>__0
	float ___U3CendTimeU3E__0_3;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<relativeProgress>__1
	float ___U3CrelativeProgressU3E__1_4;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::scrollY
	float ___scrollY_5;
	// Tacticsoft.Examples.ScrollingEventsHandler Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$this
	ScrollingEventsHandler_t2846363661 * ___U24this_6;
	// System.Object Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartScrollYU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U3CstartScrollYU3E__0_1)); }
	inline float get_U3CstartScrollYU3E__0_1() const { return ___U3CstartScrollYU3E__0_1; }
	inline float* get_address_of_U3CstartScrollYU3E__0_1() { return &___U3CstartScrollYU3E__0_1; }
	inline void set_U3CstartScrollYU3E__0_1(float value)
	{
		___U3CstartScrollYU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CendTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U3CendTimeU3E__0_3)); }
	inline float get_U3CendTimeU3E__0_3() const { return ___U3CendTimeU3E__0_3; }
	inline float* get_address_of_U3CendTimeU3E__0_3() { return &___U3CendTimeU3E__0_3; }
	inline void set_U3CendTimeU3E__0_3(float value)
	{
		___U3CendTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CrelativeProgressU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U3CrelativeProgressU3E__1_4)); }
	inline float get_U3CrelativeProgressU3E__1_4() const { return ___U3CrelativeProgressU3E__1_4; }
	inline float* get_address_of_U3CrelativeProgressU3E__1_4() { return &___U3CrelativeProgressU3E__1_4; }
	inline void set_U3CrelativeProgressU3E__1_4(float value)
	{
		___U3CrelativeProgressU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_scrollY_5() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___scrollY_5)); }
	inline float get_scrollY_5() const { return ___scrollY_5; }
	inline float* get_address_of_scrollY_5() { return &___scrollY_5; }
	inline void set_scrollY_5(float value)
	{
		___scrollY_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U24this_6)); }
	inline ScrollingEventsHandler_t2846363661 * get_U24this_6() const { return ___U24this_6; }
	inline ScrollingEventsHandler_t2846363661 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ScrollingEventsHandler_t2846363661 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t2778569968, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETOSCROLLYU3EC__ITERATOR0_T2778569968_H
#ifndef UNIWEBVIEWHELPER_T1459904593_H
#define UNIWEBVIEWHELPER_T1459904593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewHelper
struct  UniWebViewHelper_t1459904593  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWHELPER_T1459904593_H
#ifndef UNIWEBVIEWEDGEINSETS_T3496354243_H
#define UNIWEBVIEWEDGEINSETS_T3496354243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewEdgeInsets
struct  UniWebViewEdgeInsets_t3496354243  : public RuntimeObject
{
public:
	// System.Int32 UniWebViewEdgeInsets::top
	int32_t ___top_0;
	// System.Int32 UniWebViewEdgeInsets::left
	int32_t ___left_1;
	// System.Int32 UniWebViewEdgeInsets::bottom
	int32_t ___bottom_2;
	// System.Int32 UniWebViewEdgeInsets::right
	int32_t ___right_3;

public:
	inline static int32_t get_offset_of_top_0() { return static_cast<int32_t>(offsetof(UniWebViewEdgeInsets_t3496354243, ___top_0)); }
	inline int32_t get_top_0() const { return ___top_0; }
	inline int32_t* get_address_of_top_0() { return &___top_0; }
	inline void set_top_0(int32_t value)
	{
		___top_0 = value;
	}

	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(UniWebViewEdgeInsets_t3496354243, ___left_1)); }
	inline int32_t get_left_1() const { return ___left_1; }
	inline int32_t* get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(int32_t value)
	{
		___left_1 = value;
	}

	inline static int32_t get_offset_of_bottom_2() { return static_cast<int32_t>(offsetof(UniWebViewEdgeInsets_t3496354243, ___bottom_2)); }
	inline int32_t get_bottom_2() const { return ___bottom_2; }
	inline int32_t* get_address_of_bottom_2() { return &___bottom_2; }
	inline void set_bottom_2(int32_t value)
	{
		___bottom_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(UniWebViewEdgeInsets_t3496354243, ___right_3)); }
	inline int32_t get_right_3() const { return ___right_3; }
	inline int32_t* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(int32_t value)
	{
		___right_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWEDGEINSETS_T3496354243_H
#ifndef RANGEEXTENSIONS_T531368024_H
#define RANGEEXTENSIONS_T531368024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.RangeExtensions
struct  RangeExtensions_t531368024  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEEXTENSIONS_T531368024_H
#ifndef U3CCREATEWEBVIEWU3EC__ANONSTOREY0_T2301772004_H
#define U3CCREATEWEBVIEWU3EC__ANONSTOREY0_T2301772004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SizeAndTransitionSceneManager/<CreateWebView>c__AnonStorey0
struct  U3CCreateWebViewU3Ec__AnonStorey0_t2301772004  : public RuntimeObject
{
public:
	// UniWebView SizeAndTransitionSceneManager/<CreateWebView>c__AnonStorey0::webView
	UniWebView_t941983939 * ___webView_0;
	// SizeAndTransitionSceneManager SizeAndTransitionSceneManager/<CreateWebView>c__AnonStorey0::$this
	SizeAndTransitionSceneManager_t3659808263 * ___U24this_1;

public:
	inline static int32_t get_offset_of_webView_0() { return static_cast<int32_t>(offsetof(U3CCreateWebViewU3Ec__AnonStorey0_t2301772004, ___webView_0)); }
	inline UniWebView_t941983939 * get_webView_0() const { return ___webView_0; }
	inline UniWebView_t941983939 ** get_address_of_webView_0() { return &___webView_0; }
	inline void set_webView_0(UniWebView_t941983939 * value)
	{
		___webView_0 = value;
		Il2CppCodeGenWriteBarrier((&___webView_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCreateWebViewU3Ec__AnonStorey0_t2301772004, ___U24this_1)); }
	inline SizeAndTransitionSceneManager_t3659808263 * get_U24this_1() const { return ___U24this_1; }
	inline SizeAndTransitionSceneManager_t3659808263 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SizeAndTransitionSceneManager_t3659808263 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

struct U3CCreateWebViewU3Ec__AnonStorey0_t2301772004_StaticFields
{
public:
	// System.Action SizeAndTransitionSceneManager/<CreateWebView>c__AnonStorey0::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3CCreateWebViewU3Ec__AnonStorey0_t2301772004_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEWEBVIEWU3EC__ANONSTOREY0_T2301772004_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef TWOD_T1747174160_H
#define TWOD_T1747174160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Geom.TwoD
struct  TwoD_t1747174160  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOD_T1747174160_H
#ifndef PROJECTIONUTILS_T3292001777_H
#define PROJECTIONUTILS_T3292001777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.ProjectionUtils
struct  ProjectionUtils_t3292001777  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONUTILS_T3292001777_H
#ifndef CLUSTERUTILS_T2231362195_H
#define CLUSTERUTILS_T2231362195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.ClusterUtils
struct  ClusterUtils_t2231362195  : public RuntimeObject
{
public:

public:
};

struct ClusterUtils_t2231362195_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.ClusterUtils::hashString
	StringBuilder_t * ___hashString_0;

public:
	inline static int32_t get_offset_of_hashString_0() { return static_cast<int32_t>(offsetof(ClusterUtils_t2231362195_StaticFields, ___hashString_0)); }
	inline StringBuilder_t * get_hashString_0() const { return ___hashString_0; }
	inline StringBuilder_t ** get_address_of_hashString_0() { return &___hashString_0; }
	inline void set_hashString_0(StringBuilder_t * value)
	{
		___hashString_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERUTILS_T2231362195_H
#ifndef EVENTHANDLEREXTENSIONS_T1390733735_H
#define EVENTHANDLEREXTENSIONS_T1390733735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.EventHandlerExtensions
struct  EventHandlerExtensions_t1390733735  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLEREXTENSIONS_T1390733735_H
#ifndef TOUCHUTILS_T2322363280_H
#define TOUCHUTILS_T2322363280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.TouchUtils
struct  TouchUtils_t2322363280  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUTILS_T2322363280_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T2130520576_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T2130520576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t2130520576  : public RuntimeObject
{
public:
	// TouchScript.TouchManagerInstance TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$this
	TouchManagerInstance_t3252675660 * ___U24this_0;
	// System.Object TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t2130520576, ___U24this_0)); }
	inline TouchManagerInstance_t3252675660 * get_U24this_0() const { return ___U24this_0; }
	inline TouchManagerInstance_t3252675660 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TouchManagerInstance_t3252675660 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t2130520576, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t2130520576, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t2130520576, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T2130520576_H
#ifndef TRANSFORMUTILS_T4150736043_H
#define TRANSFORMUTILS_T4150736043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.TransformUtils
struct  TransformUtils_t4150736043  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMUTILS_T4150736043_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UNIWEBVIEWMESSAGE_T2441068380_H
#define UNIWEBVIEWMESSAGE_T2441068380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewMessage
struct  UniWebViewMessage_t2441068380 
{
public:
	union
	{
		struct
		{
			// System.String UniWebViewMessage::<rawMessage>k__BackingField
			String_t* ___U3CrawMessageU3Ek__BackingField_0;
			// System.String UniWebViewMessage::<scheme>k__BackingField
			String_t* ___U3CschemeU3Ek__BackingField_1;
			// System.String UniWebViewMessage::<path>k__BackingField
			String_t* ___U3CpathU3Ek__BackingField_2;
			// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::<args>k__BackingField
			Dictionary_2_t1632706988 * ___U3CargsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CrawMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CrawMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CrawMessageU3Ek__BackingField_0() const { return ___U3CrawMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CrawMessageU3Ek__BackingField_0() { return &___U3CrawMessageU3Ek__BackingField_0; }
	inline void set_U3CrawMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CrawMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CschemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CschemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CschemeU3Ek__BackingField_1() const { return ___U3CschemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CschemeU3Ek__BackingField_1() { return &___U3CschemeU3Ek__BackingField_1; }
	inline void set_U3CschemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CschemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CschemeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CpathU3Ek__BackingField_2)); }
	inline String_t* get_U3CpathU3Ek__BackingField_2() const { return ___U3CpathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CpathU3Ek__BackingField_2() { return &___U3CpathU3Ek__BackingField_2; }
	inline void set_U3CpathU3Ek__BackingField_2(String_t* value)
	{
		___U3CpathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CargsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CargsU3Ek__BackingField_3)); }
	inline Dictionary_2_t1632706988 * get_U3CargsU3Ek__BackingField_3() const { return ___U3CargsU3Ek__BackingField_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CargsU3Ek__BackingField_3() { return &___U3CargsU3Ek__BackingField_3; }
	inline void set_U3CargsU3Ek__BackingField_3(Dictionary_2_t1632706988 * value)
	{
		___U3CargsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CargsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CrawMessageU3Ek__BackingField_0;
			char* ___U3CschemeU3Ek__BackingField_1;
			char* ___U3CpathU3Ek__BackingField_2;
			Dictionary_2_t1632706988 * ___U3CargsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};
};
// Native definition for COM marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CrawMessageU3Ek__BackingField_0;
			Il2CppChar* ___U3CschemeU3Ek__BackingField_1;
			Il2CppChar* ___U3CpathU3Ek__BackingField_2;
			Dictionary_2_t1632706988 * ___U3CargsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};
};
#endif // UNIWEBVIEWMESSAGE_T2441068380_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef U24ARRAYTYPEU3D80_T183802871_H
#define U24ARRAYTYPEU3D80_T183802871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=80
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D80_t183802871 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D80_t183802871__padding[80];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D80_T183802871_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef RANGE_T173988048_H
#define RANGE_T173988048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.Range
struct  Range_t173988048 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.Range::from
	int32_t ___from_0;
	// System.Int32 UnityEngine.SocialPlatforms.Range::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(Range_t173988048, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(Range_t173988048, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T173988048_H
#ifndef UNITYEVENT_2_T3762691837_H
#define UNITYEVENT_2_T3762691837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Boolean>
struct  UnityEvent_2_t3762691837  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t3762691837, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T3762691837_H
#ifndef UNITYEVENT_2_T767703350_H
#define UNITYEVENT_2_T767703350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>
struct  UnityEvent_2_t767703350  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t767703350, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T767703350_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef UNIWEBVIEWORIENTATION_T1746230176_H
#define UNIWEBVIEWORIENTATION_T1746230176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewOrientation
struct  UniWebViewOrientation_t1746230176 
{
public:
	// System.Int32 UniWebViewOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UniWebViewOrientation_t1746230176, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWORIENTATION_T1746230176_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef TOUCHHITTYPE_T1165203098_H
#define TOUCHHITTYPE_T1165203098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit/TouchHitType
struct  TouchHitType_t1165203098 
{
public:
	// System.Int32 TouchScript.Hit.TouchHit/TouchHitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchHitType_t1165203098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHITTYPE_T1165203098_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#define UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewTransitionEdge
struct  UniWebViewTransitionEdge_t2354894166 
{
public:
	// System.Int32 UniWebViewTransitionEdge::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UniWebViewTransitionEdge_t2354894166, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-87137E630C3837BC71D4E9D0A4EB1E7CF85C9909
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0;
	// <PrivateImplementationDetails>/$ArrayType=80 <PrivateImplementationDetails>::$field-D06AFECACD30B599375506E0891369AE6874C159
	U24ArrayTypeU3D80_t183802871  ___U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0() const { return ___U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0() { return &___U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0; }
	inline void set_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1)); }
	inline U24ArrayTypeU3D80_t183802871  get_U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1() const { return ___U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1; }
	inline U24ArrayTypeU3D80_t183802871 * get_address_of_U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1() { return &___U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1; }
	inline void set_U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1(U24ArrayTypeU3D80_t183802871  value)
	{
		___U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef NULLTOGGLEATTRIBUTE_T3228924299_H
#define NULLTOGGLEATTRIBUTE_T3228924299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Attributes.NullToggleAttribute
struct  NullToggleAttribute_t3228924299  : public PropertyAttribute_t3677895545
{
public:
	// System.Int32 TouchScript.Utils.Attributes.NullToggleAttribute::NullIntValue
	int32_t ___NullIntValue_0;
	// System.Single TouchScript.Utils.Attributes.NullToggleAttribute::NullFloatValue
	float ___NullFloatValue_1;
	// UnityEngine.Object TouchScript.Utils.Attributes.NullToggleAttribute::NullObjectValue
	Object_t631007953 * ___NullObjectValue_2;

public:
	inline static int32_t get_offset_of_NullIntValue_0() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t3228924299, ___NullIntValue_0)); }
	inline int32_t get_NullIntValue_0() const { return ___NullIntValue_0; }
	inline int32_t* get_address_of_NullIntValue_0() { return &___NullIntValue_0; }
	inline void set_NullIntValue_0(int32_t value)
	{
		___NullIntValue_0 = value;
	}

	inline static int32_t get_offset_of_NullFloatValue_1() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t3228924299, ___NullFloatValue_1)); }
	inline float get_NullFloatValue_1() const { return ___NullFloatValue_1; }
	inline float* get_address_of_NullFloatValue_1() { return &___NullFloatValue_1; }
	inline void set_NullFloatValue_1(float value)
	{
		___NullFloatValue_1 = value;
	}

	inline static int32_t get_offset_of_NullObjectValue_2() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t3228924299, ___NullObjectValue_2)); }
	inline Object_t631007953 * get_NullObjectValue_2() const { return ___NullObjectValue_2; }
	inline Object_t631007953 ** get_address_of_NullObjectValue_2() { return &___NullObjectValue_2; }
	inline void set_NullObjectValue_2(Object_t631007953 * value)
	{
		___NullObjectValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___NullObjectValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLTOGGLEATTRIBUTE_T3228924299_H
#ifndef CELLVISIBILITYCHANGEEVENT_T2201436281_H
#define CELLVISIBILITYCHANGEEVENT_T2201436281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView/CellVisibilityChangeEvent
struct  CellVisibilityChangeEvent_t2201436281  : public UnityEvent_2_t3762691837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLVISIBILITYCHANGEEVENT_T2201436281_H
#ifndef CELLHEIGHTCHANGEDEVENT_T954279626_H
#define CELLHEIGHTCHANGEDEVENT_T954279626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent
struct  CellHeightChangedEvent_t954279626  : public UnityEvent_2_t767703350
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLHEIGHTCHANGEDEVENT_T954279626_H
#ifndef TOGGLELEFTATTRIBUTE_T2082752021_H
#define TOGGLELEFTATTRIBUTE_T2082752021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Attributes.ToggleLeftAttribute
struct  ToggleLeftAttribute_t2082752021  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLELEFTATTRIBUTE_T2082752021_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TOUCHHIT_T1165636103_H
#define TOUCHHIT_T1165636103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit
struct  TouchHit_t1165636103 
{
public:
	// TouchScript.Hit.TouchHit/TouchHitType TouchScript.Hit.TouchHit::type
	int32_t ___type_0;
	// UnityEngine.Transform TouchScript.Hit.TouchHit::transform
	Transform_t3600365921 * ___transform_1;
	// UnityEngine.RaycastHit TouchScript.Hit.TouchHit::raycastHit
	RaycastHit_t1056001966  ___raycastHit_2;
	// UnityEngine.RaycastHit2D TouchScript.Hit.TouchHit::raycastHit2D
	RaycastHit2D_t2279581989  ___raycastHit2D_3;
	// UnityEngine.EventSystems.RaycastResult TouchScript.Hit.TouchHit::raycastResult
	RaycastResult_t3360306849  ___raycastResult_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___transform_1)); }
	inline Transform_t3600365921 * get_transform_1() const { return ___transform_1; }
	inline Transform_t3600365921 ** get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Transform_t3600365921 * value)
	{
		___transform_1 = value;
		Il2CppCodeGenWriteBarrier((&___transform_1), value);
	}

	inline static int32_t get_offset_of_raycastHit_2() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastHit_2)); }
	inline RaycastHit_t1056001966  get_raycastHit_2() const { return ___raycastHit_2; }
	inline RaycastHit_t1056001966 * get_address_of_raycastHit_2() { return &___raycastHit_2; }
	inline void set_raycastHit_2(RaycastHit_t1056001966  value)
	{
		___raycastHit_2 = value;
	}

	inline static int32_t get_offset_of_raycastHit2D_3() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastHit2D_3)); }
	inline RaycastHit2D_t2279581989  get_raycastHit2D_3() const { return ___raycastHit2D_3; }
	inline RaycastHit2D_t2279581989 * get_address_of_raycastHit2D_3() { return &___raycastHit2D_3; }
	inline void set_raycastHit2D_3(RaycastHit2D_t2279581989  value)
	{
		___raycastHit2D_3 = value;
	}

	inline static int32_t get_offset_of_raycastResult_4() { return static_cast<int32_t>(offsetof(TouchHit_t1165636103, ___raycastResult_4)); }
	inline RaycastResult_t3360306849  get_raycastResult_4() const { return ___raycastResult_4; }
	inline RaycastResult_t3360306849 * get_address_of_raycastResult_4() { return &___raycastResult_4; }
	inline void set_raycastResult_4(RaycastResult_t3360306849  value)
	{
		___raycastResult_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t1165636103_marshaled_pinvoke
{
	int32_t ___type_0;
	Transform_t3600365921 * ___transform_1;
	RaycastHit_t1056001966_marshaled_pinvoke ___raycastHit_2;
	RaycastHit2D_t2279581989_marshaled_pinvoke ___raycastHit2D_3;
	RaycastResult_t3360306849_marshaled_pinvoke ___raycastResult_4;
};
// Native definition for COM marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t1165636103_marshaled_com
{
	int32_t ___type_0;
	Transform_t3600365921 * ___transform_1;
	RaycastHit_t1056001966_marshaled_com ___raycastHit_2;
	RaycastHit2D_t2279581989_marshaled_com ___raycastHit2D_3;
	RaycastResult_t3360306849_marshaled_com ___raycastResult_4;
};
#endif // TOUCHHIT_T1165636103_H
#ifndef TOUCHPOINT_T873891735_H
#define TOUCHPOINT_T873891735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchPoint
struct  TouchPoint_t873891735  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchPoint::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// UnityEngine.Transform TouchScript.TouchPoint::<Target>k__BackingField
	Transform_t3600365921 * ___U3CTargetU3Ek__BackingField_1;
	// UnityEngine.Vector2 TouchScript.TouchPoint::<PreviousPosition>k__BackingField
	Vector2_t2156229523  ___U3CPreviousPositionU3Ek__BackingField_2;
	// TouchScript.Hit.TouchHit TouchScript.TouchPoint::<Hit>k__BackingField
	TouchHit_t1165636103  ___U3CHitU3Ek__BackingField_3;
	// TouchScript.Layers.TouchLayer TouchScript.TouchPoint::<Layer>k__BackingField
	TouchLayer_t1334725428 * ___U3CLayerU3Ek__BackingField_4;
	// TouchScript.InputSources.IInputSource TouchScript.TouchPoint::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_5;
	// TouchScript.Tags TouchScript.TouchPoint::<Tags>k__BackingField
	Tags_t3560929397 * ___U3CTagsU3Ek__BackingField_6;
	// System.Int32 TouchScript.TouchPoint::refCount
	int32_t ___refCount_7;
	// UnityEngine.Vector2 TouchScript.TouchPoint::position
	Vector2_t2156229523  ___position_8;
	// UnityEngine.Vector2 TouchScript.TouchPoint::newPosition
	Vector2_t2156229523  ___newPosition_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TouchScript.TouchPoint::properties
	Dictionary_2_t2865362463 * ___properties_10;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CTargetU3Ek__BackingField_1)); }
	inline Transform_t3600365921 * get_U3CTargetU3Ek__BackingField_1() const { return ___U3CTargetU3Ek__BackingField_1; }
	inline Transform_t3600365921 ** get_address_of_U3CTargetU3Ek__BackingField_1() { return &___U3CTargetU3Ek__BackingField_1; }
	inline void set_U3CTargetU3Ek__BackingField_1(Transform_t3600365921 * value)
	{
		___U3CTargetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPreviousPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CPreviousPositionU3Ek__BackingField_2)); }
	inline Vector2_t2156229523  get_U3CPreviousPositionU3Ek__BackingField_2() const { return ___U3CPreviousPositionU3Ek__BackingField_2; }
	inline Vector2_t2156229523 * get_address_of_U3CPreviousPositionU3Ek__BackingField_2() { return &___U3CPreviousPositionU3Ek__BackingField_2; }
	inline void set_U3CPreviousPositionU3Ek__BackingField_2(Vector2_t2156229523  value)
	{
		___U3CPreviousPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CHitU3Ek__BackingField_3)); }
	inline TouchHit_t1165636103  get_U3CHitU3Ek__BackingField_3() const { return ___U3CHitU3Ek__BackingField_3; }
	inline TouchHit_t1165636103 * get_address_of_U3CHitU3Ek__BackingField_3() { return &___U3CHitU3Ek__BackingField_3; }
	inline void set_U3CHitU3Ek__BackingField_3(TouchHit_t1165636103  value)
	{
		___U3CHitU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLayerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CLayerU3Ek__BackingField_4)); }
	inline TouchLayer_t1334725428 * get_U3CLayerU3Ek__BackingField_4() const { return ___U3CLayerU3Ek__BackingField_4; }
	inline TouchLayer_t1334725428 ** get_address_of_U3CLayerU3Ek__BackingField_4() { return &___U3CLayerU3Ek__BackingField_4; }
	inline void set_U3CLayerU3Ek__BackingField_4(TouchLayer_t1334725428 * value)
	{
		___U3CLayerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLayerU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CInputSourceU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_5() const { return ___U3CInputSourceU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_5() { return &___U3CInputSourceU3Ek__BackingField_5; }
	inline void set_U3CInputSourceU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputSourceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___U3CTagsU3Ek__BackingField_6)); }
	inline Tags_t3560929397 * get_U3CTagsU3Ek__BackingField_6() const { return ___U3CTagsU3Ek__BackingField_6; }
	inline Tags_t3560929397 ** get_address_of_U3CTagsU3Ek__BackingField_6() { return &___U3CTagsU3Ek__BackingField_6; }
	inline void set_U3CTagsU3Ek__BackingField_6(Tags_t3560929397 * value)
	{
		___U3CTagsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_refCount_7() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___refCount_7)); }
	inline int32_t get_refCount_7() const { return ___refCount_7; }
	inline int32_t* get_address_of_refCount_7() { return &___refCount_7; }
	inline void set_refCount_7(int32_t value)
	{
		___refCount_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___position_8)); }
	inline Vector2_t2156229523  get_position_8() const { return ___position_8; }
	inline Vector2_t2156229523 * get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(Vector2_t2156229523  value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_newPosition_9() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___newPosition_9)); }
	inline Vector2_t2156229523  get_newPosition_9() const { return ___newPosition_9; }
	inline Vector2_t2156229523 * get_address_of_newPosition_9() { return &___newPosition_9; }
	inline void set_newPosition_9(Vector2_t2156229523  value)
	{
		___newPosition_9 = value;
	}

	inline static int32_t get_offset_of_properties_10() { return static_cast<int32_t>(offsetof(TouchPoint_t873891735, ___properties_10)); }
	inline Dictionary_2_t2865362463 * get_properties_10() const { return ___properties_10; }
	inline Dictionary_2_t2865362463 ** get_address_of_properties_10() { return &___properties_10; }
	inline void set_properties_10(Dictionary_2_t2865362463 * value)
	{
		___properties_10 = value;
		Il2CppCodeGenWriteBarrier((&___properties_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPOINT_T873891735_H
#ifndef RECEIVEDKEYCODEDELEGATE_T1391812367_H
#define RECEIVEDKEYCODEDELEGATE_T1391812367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/ReceivedKeyCodeDelegate
struct  ReceivedKeyCodeDelegate_t1391812367  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVEDKEYCODEDELEGATE_T1391812367_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RECEIVEDMESSAGEDELEGATE_T188390413_H
#define RECEIVEDMESSAGEDELEGATE_T188390413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/ReceivedMessageDelegate
struct  ReceivedMessageDelegate_t188390413  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVEDMESSAGEDELEGATE_T188390413_H
#ifndef LOADBEGINDELEGATE_T3414845260_H
#define LOADBEGINDELEGATE_T3414845260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/LoadBeginDelegate
struct  LoadBeginDelegate_t3414845260  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADBEGINDELEGATE_T3414845260_H
#ifndef LOADCOMPLETEDELEGATE_T2989787613_H
#define LOADCOMPLETEDELEGATE_T2989787613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/LoadCompleteDelegate
struct  LoadCompleteDelegate_t2989787613  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCOMPLETEDELEGATE_T2989787613_H
#ifndef WEBVIEWSHOULDCLOSEDELEGATE_T3305779967_H
#define WEBVIEWSHOULDCLOSEDELEGATE_T3305779967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/WebViewShouldCloseDelegate
struct  WebViewShouldCloseDelegate_t3305779967  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBVIEWSHOULDCLOSEDELEGATE_T3305779967_H
#ifndef INSETSFORSCREENOREITATIONDELEGATE_T202380036_H
#define INSETSFORSCREENOREITATIONDELEGATE_T202380036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/InsetsForScreenOreitationDelegate
struct  InsetsForScreenOreitationDelegate_t202380036  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSETSFORSCREENOREITATIONDELEGATE_T202380036_H
#ifndef EVALJAVASCRIPTFINISHEDDELEGATE_T3947010167_H
#define EVALJAVASCRIPTFINISHEDDELEGATE_T3947010167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/EvalJavaScriptFinishedDelegate
struct  EvalJavaScriptFinishedDelegate_t3947010167  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVALJAVASCRIPTFINISHEDDELEGATE_T3947010167_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SIMPLETABLEVIEWCONTROLLER_T2092983076_H
#define SIMPLETABLEVIEWCONTROLLER_T2092983076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.SimpleTableViewController
struct  SimpleTableViewController_t2092983076  : public MonoBehaviour_t3962482529
{
public:
	// Tacticsoft.Examples.VisibleCounterCell Tacticsoft.Examples.SimpleTableViewController::m_cellPrefab
	VisibleCounterCell_t1279458109 * ___m_cellPrefab_2;
	// Tacticsoft.TableView Tacticsoft.Examples.SimpleTableViewController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Int32 Tacticsoft.Examples.SimpleTableViewController::m_numRows
	int32_t ___m_numRows_4;
	// System.Int32 Tacticsoft.Examples.SimpleTableViewController::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t2092983076, ___m_cellPrefab_2)); }
	inline VisibleCounterCell_t1279458109 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline VisibleCounterCell_t1279458109 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(VisibleCounterCell_t1279458109 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t2092983076, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t2092983076, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_5() { return static_cast<int32_t>(offsetof(SimpleTableViewController_t2092983076, ___m_numInstancesCreated_5)); }
	inline int32_t get_m_numInstancesCreated_5() const { return ___m_numInstancesCreated_5; }
	inline int32_t* get_address_of_m_numInstancesCreated_5() { return &___m_numInstancesCreated_5; }
	inline void set_m_numInstancesCreated_5(int32_t value)
	{
		___m_numInstancesCreated_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETABLEVIEWCONTROLLER_T2092983076_H
#ifndef USEWITHCODESCENEMANAGER_T1728296987_H
#define USEWITHCODESCENEMANAGER_T1728296987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UseWithCodeSceneManager
struct  UseWithCodeSceneManager_t1728296987  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField UseWithCodeSceneManager::urlInput
	InputField_t3762917431 * ___urlInput_2;

public:
	inline static int32_t get_offset_of_urlInput_2() { return static_cast<int32_t>(offsetof(UseWithCodeSceneManager_t1728296987, ___urlInput_2)); }
	inline InputField_t3762917431 * get_urlInput_2() const { return ___urlInput_2; }
	inline InputField_t3762917431 ** get_address_of_urlInput_2() { return &___urlInput_2; }
	inline void set_urlInput_2(InputField_t3762917431 * value)
	{
		___urlInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___urlInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEWITHCODESCENEMANAGER_T1728296987_H
#ifndef DYNAMICHEIGHTTABLEVIEWCONTROLLER_T2429124891_H
#define DYNAMICHEIGHTTABLEVIEWCONTROLLER_T2429124891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.DynamicHeightTableViewController
struct  DynamicHeightTableViewController_t2429124891  : public MonoBehaviour_t3962482529
{
public:
	// Tacticsoft.Examples.DynamicHeightCell Tacticsoft.Examples.DynamicHeightTableViewController::m_cellPrefab
	DynamicHeightCell_t2796155881 * ___m_cellPrefab_2;
	// Tacticsoft.TableView Tacticsoft.Examples.DynamicHeightTableViewController::m_tableView
	TableView_t4228429533 * ___m_tableView_3;
	// System.Int32 Tacticsoft.Examples.DynamicHeightTableViewController::m_numRows
	int32_t ___m_numRows_4;
	// System.Int32 Tacticsoft.Examples.DynamicHeightTableViewController::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Tacticsoft.Examples.DynamicHeightTableViewController::m_customRowHeights
	Dictionary_2_t285980105 * ___m_customRowHeights_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t2429124891, ___m_cellPrefab_2)); }
	inline DynamicHeightCell_t2796155881 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline DynamicHeightCell_t2796155881 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(DynamicHeightCell_t2796155881 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t2429124891, ___m_tableView_3)); }
	inline TableView_t4228429533 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t4228429533 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_3), value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t2429124891, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_5() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t2429124891, ___m_numInstancesCreated_5)); }
	inline int32_t get_m_numInstancesCreated_5() const { return ___m_numInstancesCreated_5; }
	inline int32_t* get_address_of_m_numInstancesCreated_5() { return &___m_numInstancesCreated_5; }
	inline void set_m_numInstancesCreated_5(int32_t value)
	{
		___m_numInstancesCreated_5 = value;
	}

	inline static int32_t get_offset_of_m_customRowHeights_6() { return static_cast<int32_t>(offsetof(DynamicHeightTableViewController_t2429124891, ___m_customRowHeights_6)); }
	inline Dictionary_2_t285980105 * get_m_customRowHeights_6() const { return ___m_customRowHeights_6; }
	inline Dictionary_2_t285980105 ** get_address_of_m_customRowHeights_6() { return &___m_customRowHeights_6; }
	inline void set_m_customRowHeights_6(Dictionary_2_t285980105 * value)
	{
		___m_customRowHeights_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_customRowHeights_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICHEIGHTTABLEVIEWCONTROLLER_T2429124891_H
#ifndef SCROLLINGEVENTSHANDLER_T2846363661_H
#define SCROLLINGEVENTSHANDLER_T2846363661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.ScrollingEventsHandler
struct  ScrollingEventsHandler_t2846363661  : public MonoBehaviour_t3962482529
{
public:
	// Tacticsoft.TableView Tacticsoft.Examples.ScrollingEventsHandler::m_tableView
	TableView_t4228429533 * ___m_tableView_2;

public:
	inline static int32_t get_offset_of_m_tableView_2() { return static_cast<int32_t>(offsetof(ScrollingEventsHandler_t2846363661, ___m_tableView_2)); }
	inline TableView_t4228429533 * get_m_tableView_2() const { return ___m_tableView_2; }
	inline TableView_t4228429533 ** get_address_of_m_tableView_2() { return &___m_tableView_2; }
	inline void set_m_tableView_2(TableView_t4228429533 * value)
	{
		___m_tableView_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_tableView_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGEVENTSHANDLER_T2846363661_H
#ifndef TABLEVIEW_T4228429533_H
#define TABLEVIEW_T4228429533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView
struct  TableView_t4228429533  : public MonoBehaviour_t3962482529
{
public:
	// Tacticsoft.TableView/CellVisibilityChangeEvent Tacticsoft.TableView::onCellVisibilityChanged
	CellVisibilityChangeEvent_t2201436281 * ___onCellVisibilityChanged_2;
	// System.Boolean Tacticsoft.TableView::<isEmpty>k__BackingField
	bool ___U3CisEmptyU3Ek__BackingField_3;
	// Tacticsoft.ITableViewDataSource Tacticsoft.TableView::m_dataSource
	RuntimeObject* ___m_dataSource_4;
	// System.Boolean Tacticsoft.TableView::m_requiresReload
	bool ___m_requiresReload_5;
	// UnityEngine.UI.VerticalLayoutGroup Tacticsoft.TableView::m_verticalLayoutGroup
	VerticalLayoutGroup_t923838031 * ___m_verticalLayoutGroup_6;
	// UnityEngine.UI.ScrollRect Tacticsoft.TableView::m_scrollRect
	ScrollRect_t4137855814 * ___m_scrollRect_7;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_topPadding
	LayoutElement_t1785403678 * ___m_topPadding_8;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_bottomPadding
	LayoutElement_t1785403678 * ___m_bottomPadding_9;
	// System.Single[] Tacticsoft.TableView::m_rowHeights
	SingleU5BU5D_t1444911251* ___m_rowHeights_10;
	// System.Single[] Tacticsoft.TableView::m_cumulativeRowHeights
	SingleU5BU5D_t1444911251* ___m_cumulativeRowHeights_11;
	// System.Int32 Tacticsoft.TableView::m_cleanCumulativeIndex
	int32_t ___m_cleanCumulativeIndex_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell> Tacticsoft.TableView::m_visibleCells
	Dictionary_2_t3961624650 * ___m_visibleCells_13;
	// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::m_visibleRowRange
	Range_t173988048  ___m_visibleRowRange_14;
	// UnityEngine.RectTransform Tacticsoft.TableView::m_reusableCellContainer
	RectTransform_t3704657025 * ___m_reusableCellContainer_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>> Tacticsoft.TableView::m_reusableCells
	Dictionary_2_t3697813627 * ___m_reusableCells_16;
	// System.Single Tacticsoft.TableView::m_scrollY
	float ___m_scrollY_17;
	// System.Boolean Tacticsoft.TableView::m_requiresRefresh
	bool ___m_requiresRefresh_18;

public:
	inline static int32_t get_offset_of_onCellVisibilityChanged_2() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___onCellVisibilityChanged_2)); }
	inline CellVisibilityChangeEvent_t2201436281 * get_onCellVisibilityChanged_2() const { return ___onCellVisibilityChanged_2; }
	inline CellVisibilityChangeEvent_t2201436281 ** get_address_of_onCellVisibilityChanged_2() { return &___onCellVisibilityChanged_2; }
	inline void set_onCellVisibilityChanged_2(CellVisibilityChangeEvent_t2201436281 * value)
	{
		___onCellVisibilityChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCellVisibilityChanged_2), value);
	}

	inline static int32_t get_offset_of_U3CisEmptyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___U3CisEmptyU3Ek__BackingField_3)); }
	inline bool get_U3CisEmptyU3Ek__BackingField_3() const { return ___U3CisEmptyU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CisEmptyU3Ek__BackingField_3() { return &___U3CisEmptyU3Ek__BackingField_3; }
	inline void set_U3CisEmptyU3Ek__BackingField_3(bool value)
	{
		___U3CisEmptyU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_dataSource_4() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_dataSource_4)); }
	inline RuntimeObject* get_m_dataSource_4() const { return ___m_dataSource_4; }
	inline RuntimeObject** get_address_of_m_dataSource_4() { return &___m_dataSource_4; }
	inline void set_m_dataSource_4(RuntimeObject* value)
	{
		___m_dataSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_dataSource_4), value);
	}

	inline static int32_t get_offset_of_m_requiresReload_5() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_requiresReload_5)); }
	inline bool get_m_requiresReload_5() const { return ___m_requiresReload_5; }
	inline bool* get_address_of_m_requiresReload_5() { return &___m_requiresReload_5; }
	inline void set_m_requiresReload_5(bool value)
	{
		___m_requiresReload_5 = value;
	}

	inline static int32_t get_offset_of_m_verticalLayoutGroup_6() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_verticalLayoutGroup_6)); }
	inline VerticalLayoutGroup_t923838031 * get_m_verticalLayoutGroup_6() const { return ___m_verticalLayoutGroup_6; }
	inline VerticalLayoutGroup_t923838031 ** get_address_of_m_verticalLayoutGroup_6() { return &___m_verticalLayoutGroup_6; }
	inline void set_m_verticalLayoutGroup_6(VerticalLayoutGroup_t923838031 * value)
	{
		___m_verticalLayoutGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalLayoutGroup_6), value);
	}

	inline static int32_t get_offset_of_m_scrollRect_7() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_scrollRect_7)); }
	inline ScrollRect_t4137855814 * get_m_scrollRect_7() const { return ___m_scrollRect_7; }
	inline ScrollRect_t4137855814 ** get_address_of_m_scrollRect_7() { return &___m_scrollRect_7; }
	inline void set_m_scrollRect_7(ScrollRect_t4137855814 * value)
	{
		___m_scrollRect_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_scrollRect_7), value);
	}

	inline static int32_t get_offset_of_m_topPadding_8() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_topPadding_8)); }
	inline LayoutElement_t1785403678 * get_m_topPadding_8() const { return ___m_topPadding_8; }
	inline LayoutElement_t1785403678 ** get_address_of_m_topPadding_8() { return &___m_topPadding_8; }
	inline void set_m_topPadding_8(LayoutElement_t1785403678 * value)
	{
		___m_topPadding_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_topPadding_8), value);
	}

	inline static int32_t get_offset_of_m_bottomPadding_9() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_bottomPadding_9)); }
	inline LayoutElement_t1785403678 * get_m_bottomPadding_9() const { return ___m_bottomPadding_9; }
	inline LayoutElement_t1785403678 ** get_address_of_m_bottomPadding_9() { return &___m_bottomPadding_9; }
	inline void set_m_bottomPadding_9(LayoutElement_t1785403678 * value)
	{
		___m_bottomPadding_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_bottomPadding_9), value);
	}

	inline static int32_t get_offset_of_m_rowHeights_10() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_rowHeights_10)); }
	inline SingleU5BU5D_t1444911251* get_m_rowHeights_10() const { return ___m_rowHeights_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_rowHeights_10() { return &___m_rowHeights_10; }
	inline void set_m_rowHeights_10(SingleU5BU5D_t1444911251* value)
	{
		___m_rowHeights_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_rowHeights_10), value);
	}

	inline static int32_t get_offset_of_m_cumulativeRowHeights_11() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_cumulativeRowHeights_11)); }
	inline SingleU5BU5D_t1444911251* get_m_cumulativeRowHeights_11() const { return ___m_cumulativeRowHeights_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_cumulativeRowHeights_11() { return &___m_cumulativeRowHeights_11; }
	inline void set_m_cumulativeRowHeights_11(SingleU5BU5D_t1444911251* value)
	{
		___m_cumulativeRowHeights_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_cumulativeRowHeights_11), value);
	}

	inline static int32_t get_offset_of_m_cleanCumulativeIndex_12() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_cleanCumulativeIndex_12)); }
	inline int32_t get_m_cleanCumulativeIndex_12() const { return ___m_cleanCumulativeIndex_12; }
	inline int32_t* get_address_of_m_cleanCumulativeIndex_12() { return &___m_cleanCumulativeIndex_12; }
	inline void set_m_cleanCumulativeIndex_12(int32_t value)
	{
		___m_cleanCumulativeIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_visibleCells_13() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_visibleCells_13)); }
	inline Dictionary_2_t3961624650 * get_m_visibleCells_13() const { return ___m_visibleCells_13; }
	inline Dictionary_2_t3961624650 ** get_address_of_m_visibleCells_13() { return &___m_visibleCells_13; }
	inline void set_m_visibleCells_13(Dictionary_2_t3961624650 * value)
	{
		___m_visibleCells_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_visibleCells_13), value);
	}

	inline static int32_t get_offset_of_m_visibleRowRange_14() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_visibleRowRange_14)); }
	inline Range_t173988048  get_m_visibleRowRange_14() const { return ___m_visibleRowRange_14; }
	inline Range_t173988048 * get_address_of_m_visibleRowRange_14() { return &___m_visibleRowRange_14; }
	inline void set_m_visibleRowRange_14(Range_t173988048  value)
	{
		___m_visibleRowRange_14 = value;
	}

	inline static int32_t get_offset_of_m_reusableCellContainer_15() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_reusableCellContainer_15)); }
	inline RectTransform_t3704657025 * get_m_reusableCellContainer_15() const { return ___m_reusableCellContainer_15; }
	inline RectTransform_t3704657025 ** get_address_of_m_reusableCellContainer_15() { return &___m_reusableCellContainer_15; }
	inline void set_m_reusableCellContainer_15(RectTransform_t3704657025 * value)
	{
		___m_reusableCellContainer_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_reusableCellContainer_15), value);
	}

	inline static int32_t get_offset_of_m_reusableCells_16() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_reusableCells_16)); }
	inline Dictionary_2_t3697813627 * get_m_reusableCells_16() const { return ___m_reusableCells_16; }
	inline Dictionary_2_t3697813627 ** get_address_of_m_reusableCells_16() { return &___m_reusableCells_16; }
	inline void set_m_reusableCells_16(Dictionary_2_t3697813627 * value)
	{
		___m_reusableCells_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_reusableCells_16), value);
	}

	inline static int32_t get_offset_of_m_scrollY_17() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_scrollY_17)); }
	inline float get_m_scrollY_17() const { return ___m_scrollY_17; }
	inline float* get_address_of_m_scrollY_17() { return &___m_scrollY_17; }
	inline void set_m_scrollY_17(float value)
	{
		___m_scrollY_17 = value;
	}

	inline static int32_t get_offset_of_m_requiresRefresh_18() { return static_cast<int32_t>(offsetof(TableView_t4228429533, ___m_requiresRefresh_18)); }
	inline bool get_m_requiresRefresh_18() const { return ___m_requiresRefresh_18; }
	inline bool* get_address_of_m_requiresRefresh_18() { return &___m_requiresRefresh_18; }
	inline void set_m_requiresRefresh_18(bool value)
	{
		___m_requiresRefresh_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEVIEW_T4228429533_H
#ifndef LOCALHTMLSCENEMANAGER_T868756294_H
#define LOCALHTMLSCENEMANAGER_T868756294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalHTMLSceneManager
struct  LocalHTMLSceneManager_t868756294  : public MonoBehaviour_t3962482529
{
public:
	// System.String LocalHTMLSceneManager::fileName
	String_t* ___fileName_2;
	// System.String LocalHTMLSceneManager::htmlText
	String_t* ___htmlText_3;

public:
	inline static int32_t get_offset_of_fileName_2() { return static_cast<int32_t>(offsetof(LocalHTMLSceneManager_t868756294, ___fileName_2)); }
	inline String_t* get_fileName_2() const { return ___fileName_2; }
	inline String_t** get_address_of_fileName_2() { return &___fileName_2; }
	inline void set_fileName_2(String_t* value)
	{
		___fileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_2), value);
	}

	inline static int32_t get_offset_of_htmlText_3() { return static_cast<int32_t>(offsetof(LocalHTMLSceneManager_t868756294, ___htmlText_3)); }
	inline String_t* get_htmlText_3() const { return ___htmlText_3; }
	inline String_t** get_address_of_htmlText_3() { return &___htmlText_3; }
	inline void set_htmlText_3(String_t* value)
	{
		___htmlText_3 = value;
		Il2CppCodeGenWriteBarrier((&___htmlText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALHTMLSCENEMANAGER_T868756294_H
#ifndef RUNJAVASCRIPTINWEBSCENEMANAGER_T3721445575_H
#define RUNJAVASCRIPTINWEBSCENEMANAGER_T3721445575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunJavaScriptInWebSceneManager
struct  RunJavaScriptInWebSceneManager_t3721445575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text RunJavaScriptInWebSceneManager::result
	Text_t1901882714 * ___result_2;
	// UniWebView RunJavaScriptInWebSceneManager::_webView
	UniWebView_t941983939 * ____webView_3;
	// System.String RunJavaScriptInWebSceneManager::_fileName
	String_t* ____fileName_4;

public:
	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(RunJavaScriptInWebSceneManager_t3721445575, ___result_2)); }
	inline Text_t1901882714 * get_result_2() const { return ___result_2; }
	inline Text_t1901882714 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Text_t1901882714 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of__webView_3() { return static_cast<int32_t>(offsetof(RunJavaScriptInWebSceneManager_t3721445575, ____webView_3)); }
	inline UniWebView_t941983939 * get__webView_3() const { return ____webView_3; }
	inline UniWebView_t941983939 ** get_address_of__webView_3() { return &____webView_3; }
	inline void set__webView_3(UniWebView_t941983939 * value)
	{
		____webView_3 = value;
		Il2CppCodeGenWriteBarrier((&____webView_3), value);
	}

	inline static int32_t get_offset_of__fileName_4() { return static_cast<int32_t>(offsetof(RunJavaScriptInWebSceneManager_t3721445575, ____fileName_4)); }
	inline String_t* get__fileName_4() const { return ____fileName_4; }
	inline String_t** get_address_of__fileName_4() { return &____fileName_4; }
	inline void set__fileName_4(String_t* value)
	{
		____fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&____fileName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNJAVASCRIPTINWEBSCENEMANAGER_T3721445575_H
#ifndef SIZEANDTRANSITIONSCENEMANAGER_T3659808263_H
#define SIZEANDTRANSITIONSCENEMANAGER_T3659808263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SizeAndTransitionSceneManager
struct  SizeAndTransitionSceneManager_t3659808263  : public MonoBehaviour_t3962482529
{
public:
	// UniWebView SizeAndTransitionSceneManager::_webView
	UniWebView_t941983939 * ____webView_2;
	// System.Boolean SizeAndTransitionSceneManager::_webViewReady
	bool ____webViewReady_3;
	// System.Boolean SizeAndTransitionSceneManager::<fade>k__BackingField
	bool ___U3CfadeU3Ek__BackingField_4;
	// System.Int32 SizeAndTransitionSceneManager::<transitionEdge>k__BackingField
	int32_t ___U3CtransitionEdgeU3Ek__BackingField_5;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::top
	InputField_t3762917431 * ___top_6;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::left
	InputField_t3762917431 * ___left_7;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::bottom
	InputField_t3762917431 * ___bottom_8;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::right
	InputField_t3762917431 * ___right_9;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::x
	InputField_t3762917431 * ___x_10;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::y
	InputField_t3762917431 * ___y_11;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::width
	InputField_t3762917431 * ___width_12;
	// UnityEngine.UI.InputField SizeAndTransitionSceneManager::height
	InputField_t3762917431 * ___height_13;

public:
	inline static int32_t get_offset_of__webView_2() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ____webView_2)); }
	inline UniWebView_t941983939 * get__webView_2() const { return ____webView_2; }
	inline UniWebView_t941983939 ** get_address_of__webView_2() { return &____webView_2; }
	inline void set__webView_2(UniWebView_t941983939 * value)
	{
		____webView_2 = value;
		Il2CppCodeGenWriteBarrier((&____webView_2), value);
	}

	inline static int32_t get_offset_of__webViewReady_3() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ____webViewReady_3)); }
	inline bool get__webViewReady_3() const { return ____webViewReady_3; }
	inline bool* get_address_of__webViewReady_3() { return &____webViewReady_3; }
	inline void set__webViewReady_3(bool value)
	{
		____webViewReady_3 = value;
	}

	inline static int32_t get_offset_of_U3CfadeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___U3CfadeU3Ek__BackingField_4)); }
	inline bool get_U3CfadeU3Ek__BackingField_4() const { return ___U3CfadeU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CfadeU3Ek__BackingField_4() { return &___U3CfadeU3Ek__BackingField_4; }
	inline void set_U3CfadeU3Ek__BackingField_4(bool value)
	{
		___U3CfadeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CtransitionEdgeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___U3CtransitionEdgeU3Ek__BackingField_5)); }
	inline int32_t get_U3CtransitionEdgeU3Ek__BackingField_5() const { return ___U3CtransitionEdgeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CtransitionEdgeU3Ek__BackingField_5() { return &___U3CtransitionEdgeU3Ek__BackingField_5; }
	inline void set_U3CtransitionEdgeU3Ek__BackingField_5(int32_t value)
	{
		___U3CtransitionEdgeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_top_6() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___top_6)); }
	inline InputField_t3762917431 * get_top_6() const { return ___top_6; }
	inline InputField_t3762917431 ** get_address_of_top_6() { return &___top_6; }
	inline void set_top_6(InputField_t3762917431 * value)
	{
		___top_6 = value;
		Il2CppCodeGenWriteBarrier((&___top_6), value);
	}

	inline static int32_t get_offset_of_left_7() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___left_7)); }
	inline InputField_t3762917431 * get_left_7() const { return ___left_7; }
	inline InputField_t3762917431 ** get_address_of_left_7() { return &___left_7; }
	inline void set_left_7(InputField_t3762917431 * value)
	{
		___left_7 = value;
		Il2CppCodeGenWriteBarrier((&___left_7), value);
	}

	inline static int32_t get_offset_of_bottom_8() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___bottom_8)); }
	inline InputField_t3762917431 * get_bottom_8() const { return ___bottom_8; }
	inline InputField_t3762917431 ** get_address_of_bottom_8() { return &___bottom_8; }
	inline void set_bottom_8(InputField_t3762917431 * value)
	{
		___bottom_8 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_8), value);
	}

	inline static int32_t get_offset_of_right_9() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___right_9)); }
	inline InputField_t3762917431 * get_right_9() const { return ___right_9; }
	inline InputField_t3762917431 ** get_address_of_right_9() { return &___right_9; }
	inline void set_right_9(InputField_t3762917431 * value)
	{
		___right_9 = value;
		Il2CppCodeGenWriteBarrier((&___right_9), value);
	}

	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___x_10)); }
	inline InputField_t3762917431 * get_x_10() const { return ___x_10; }
	inline InputField_t3762917431 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(InputField_t3762917431 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___y_11)); }
	inline InputField_t3762917431 * get_y_11() const { return ___y_11; }
	inline InputField_t3762917431 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(InputField_t3762917431 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier((&___y_11), value);
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___width_12)); }
	inline InputField_t3762917431 * get_width_12() const { return ___width_12; }
	inline InputField_t3762917431 ** get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(InputField_t3762917431 * value)
	{
		___width_12 = value;
		Il2CppCodeGenWriteBarrier((&___width_12), value);
	}

	inline static int32_t get_offset_of_height_13() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263, ___height_13)); }
	inline InputField_t3762917431 * get_height_13() const { return ___height_13; }
	inline InputField_t3762917431 ** get_address_of_height_13() { return &___height_13; }
	inline void set_height_13(InputField_t3762917431 * value)
	{
		___height_13 = value;
		Il2CppCodeGenWriteBarrier((&___height_13), value);
	}
};

struct SizeAndTransitionSceneManager_t3659808263_StaticFields
{
public:
	// System.Action SizeAndTransitionSceneManager::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(SizeAndTransitionSceneManager_t3659808263_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZEANDTRANSITIONSCENEMANAGER_T3659808263_H
#ifndef NAVIGATOR_T1365902112_H
#define NAVIGATOR_T1365902112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Navigator
struct  Navigator_t1365902112  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATOR_T1365902112_H
#ifndef CALLBACKFROMWEBSCENEMANAGER_T2952333088_H
#define CALLBACKFROMWEBSCENEMANAGER_T2952333088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CallbackFromWebSceneManager
struct  CallbackFromWebSceneManager_t2952333088  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CallbackFromWebSceneManager::result
	Text_t1901882714 * ___result_2;
	// UniWebView CallbackFromWebSceneManager::_webView
	UniWebView_t941983939 * ____webView_3;
	// System.String CallbackFromWebSceneManager::_fileName
	String_t* ____fileName_4;

public:
	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(CallbackFromWebSceneManager_t2952333088, ___result_2)); }
	inline Text_t1901882714 * get_result_2() const { return ___result_2; }
	inline Text_t1901882714 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Text_t1901882714 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of__webView_3() { return static_cast<int32_t>(offsetof(CallbackFromWebSceneManager_t2952333088, ____webView_3)); }
	inline UniWebView_t941983939 * get__webView_3() const { return ____webView_3; }
	inline UniWebView_t941983939 ** get_address_of__webView_3() { return &____webView_3; }
	inline void set__webView_3(UniWebView_t941983939 * value)
	{
		____webView_3 = value;
		Il2CppCodeGenWriteBarrier((&____webView_3), value);
	}

	inline static int32_t get_offset_of__fileName_4() { return static_cast<int32_t>(offsetof(CallbackFromWebSceneManager_t2952333088, ____fileName_4)); }
	inline String_t* get__fileName_4() const { return ____fileName_4; }
	inline String_t** get_address_of__fileName_4() { return &____fileName_4; }
	inline void set__fileName_4(String_t* value)
	{
		____fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&____fileName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKFROMWEBSCENEMANAGER_T2952333088_H
#ifndef FIXGUITEXTJS_T3341698330_H
#define FIXGUITEXTJS_T3341698330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixGUITextJS
struct  FixGUITextJS_t3341698330  : public MonoBehaviour_t3962482529
{
public:
	// System.String FixGUITextJS::text
	String_t* ___text_2;
	// System.Boolean FixGUITextJS::tashkeel
	bool ___tashkeel_3;
	// System.Boolean FixGUITextJS::hinduNumbers
	bool ___hinduNumbers_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(FixGUITextJS_t3341698330, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_tashkeel_3() { return static_cast<int32_t>(offsetof(FixGUITextJS_t3341698330, ___tashkeel_3)); }
	inline bool get_tashkeel_3() const { return ___tashkeel_3; }
	inline bool* get_address_of_tashkeel_3() { return &___tashkeel_3; }
	inline void set_tashkeel_3(bool value)
	{
		___tashkeel_3 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_4() { return static_cast<int32_t>(offsetof(FixGUITextJS_t3341698330, ___hinduNumbers_4)); }
	inline bool get_hinduNumbers_4() const { return ___hinduNumbers_4; }
	inline bool* get_address_of_hinduNumbers_4() { return &___hinduNumbers_4; }
	inline void set_hinduNumbers_4(bool value)
	{
		___hinduNumbers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXGUITEXTJS_T3341698330_H
#ifndef TABLEVIEWCELL_T777944023_H
#define TABLEVIEWCELL_T777944023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableViewCell
struct  TableViewCell_t777944023  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEVIEWCELL_T777944023_H
#ifndef UNIWEBVIEW_T941983939_H
#define UNIWEBVIEW_T941983939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView
struct  UniWebView_t941983939  : public MonoBehaviour_t3962482529
{
public:
	// UniWebView/LoadCompleteDelegate UniWebView::OnLoadComplete
	LoadCompleteDelegate_t2989787613 * ___OnLoadComplete_2;
	// UniWebView/LoadBeginDelegate UniWebView::OnLoadBegin
	LoadBeginDelegate_t3414845260 * ___OnLoadBegin_3;
	// UniWebView/ReceivedMessageDelegate UniWebView::OnReceivedMessage
	ReceivedMessageDelegate_t188390413 * ___OnReceivedMessage_4;
	// UniWebView/EvalJavaScriptFinishedDelegate UniWebView::OnEvalJavaScriptFinished
	EvalJavaScriptFinishedDelegate_t3947010167 * ___OnEvalJavaScriptFinished_5;
	// UniWebView/WebViewShouldCloseDelegate UniWebView::OnWebViewShouldClose
	WebViewShouldCloseDelegate_t3305779967 * ___OnWebViewShouldClose_6;
	// UniWebView/ReceivedKeyCodeDelegate UniWebView::OnReceivedKeyCode
	ReceivedKeyCodeDelegate_t1391812367 * ___OnReceivedKeyCode_7;
	// UniWebView/InsetsForScreenOreitationDelegate UniWebView::InsetsForScreenOreitation
	InsetsForScreenOreitationDelegate_t202380036 * ___InsetsForScreenOreitation_8;
	// UniWebViewEdgeInsets UniWebView::_insets
	UniWebViewEdgeInsets_t3496354243 * ____insets_9;
	// System.String UniWebView::url
	String_t* ___url_10;
	// System.Boolean UniWebView::loadOnStart
	bool ___loadOnStart_11;
	// System.Boolean UniWebView::autoShowWhenLoadComplete
	bool ___autoShowWhenLoadComplete_12;
	// System.Boolean UniWebView::_backButtonEnable
	bool ____backButtonEnable_13;
	// System.Boolean UniWebView::_bouncesEnable
	bool ____bouncesEnable_14;
	// System.Boolean UniWebView::_zoomEnable
	bool ____zoomEnable_15;
	// System.String UniWebView::_currentGUID
	String_t* ____currentGUID_16;
	// System.Int32 UniWebView::_lastScreenHeight
	int32_t ____lastScreenHeight_17;
	// System.Boolean UniWebView::_immersiveMode
	bool ____immersiveMode_18;
	// System.Action UniWebView::_showTransitionAction
	Action_t1264377477 * ____showTransitionAction_19;
	// System.Action UniWebView::_hideTransitionAction
	Action_t1264377477 * ____hideTransitionAction_20;
	// System.Boolean UniWebView::toolBarShow
	bool ___toolBarShow_21;

public:
	inline static int32_t get_offset_of_OnLoadComplete_2() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnLoadComplete_2)); }
	inline LoadCompleteDelegate_t2989787613 * get_OnLoadComplete_2() const { return ___OnLoadComplete_2; }
	inline LoadCompleteDelegate_t2989787613 ** get_address_of_OnLoadComplete_2() { return &___OnLoadComplete_2; }
	inline void set_OnLoadComplete_2(LoadCompleteDelegate_t2989787613 * value)
	{
		___OnLoadComplete_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadComplete_2), value);
	}

	inline static int32_t get_offset_of_OnLoadBegin_3() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnLoadBegin_3)); }
	inline LoadBeginDelegate_t3414845260 * get_OnLoadBegin_3() const { return ___OnLoadBegin_3; }
	inline LoadBeginDelegate_t3414845260 ** get_address_of_OnLoadBegin_3() { return &___OnLoadBegin_3; }
	inline void set_OnLoadBegin_3(LoadBeginDelegate_t3414845260 * value)
	{
		___OnLoadBegin_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnLoadBegin_3), value);
	}

	inline static int32_t get_offset_of_OnReceivedMessage_4() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnReceivedMessage_4)); }
	inline ReceivedMessageDelegate_t188390413 * get_OnReceivedMessage_4() const { return ___OnReceivedMessage_4; }
	inline ReceivedMessageDelegate_t188390413 ** get_address_of_OnReceivedMessage_4() { return &___OnReceivedMessage_4; }
	inline void set_OnReceivedMessage_4(ReceivedMessageDelegate_t188390413 * value)
	{
		___OnReceivedMessage_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceivedMessage_4), value);
	}

	inline static int32_t get_offset_of_OnEvalJavaScriptFinished_5() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnEvalJavaScriptFinished_5)); }
	inline EvalJavaScriptFinishedDelegate_t3947010167 * get_OnEvalJavaScriptFinished_5() const { return ___OnEvalJavaScriptFinished_5; }
	inline EvalJavaScriptFinishedDelegate_t3947010167 ** get_address_of_OnEvalJavaScriptFinished_5() { return &___OnEvalJavaScriptFinished_5; }
	inline void set_OnEvalJavaScriptFinished_5(EvalJavaScriptFinishedDelegate_t3947010167 * value)
	{
		___OnEvalJavaScriptFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnEvalJavaScriptFinished_5), value);
	}

	inline static int32_t get_offset_of_OnWebViewShouldClose_6() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnWebViewShouldClose_6)); }
	inline WebViewShouldCloseDelegate_t3305779967 * get_OnWebViewShouldClose_6() const { return ___OnWebViewShouldClose_6; }
	inline WebViewShouldCloseDelegate_t3305779967 ** get_address_of_OnWebViewShouldClose_6() { return &___OnWebViewShouldClose_6; }
	inline void set_OnWebViewShouldClose_6(WebViewShouldCloseDelegate_t3305779967 * value)
	{
		___OnWebViewShouldClose_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnWebViewShouldClose_6), value);
	}

	inline static int32_t get_offset_of_OnReceivedKeyCode_7() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnReceivedKeyCode_7)); }
	inline ReceivedKeyCodeDelegate_t1391812367 * get_OnReceivedKeyCode_7() const { return ___OnReceivedKeyCode_7; }
	inline ReceivedKeyCodeDelegate_t1391812367 ** get_address_of_OnReceivedKeyCode_7() { return &___OnReceivedKeyCode_7; }
	inline void set_OnReceivedKeyCode_7(ReceivedKeyCodeDelegate_t1391812367 * value)
	{
		___OnReceivedKeyCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceivedKeyCode_7), value);
	}

	inline static int32_t get_offset_of_InsetsForScreenOreitation_8() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___InsetsForScreenOreitation_8)); }
	inline InsetsForScreenOreitationDelegate_t202380036 * get_InsetsForScreenOreitation_8() const { return ___InsetsForScreenOreitation_8; }
	inline InsetsForScreenOreitationDelegate_t202380036 ** get_address_of_InsetsForScreenOreitation_8() { return &___InsetsForScreenOreitation_8; }
	inline void set_InsetsForScreenOreitation_8(InsetsForScreenOreitationDelegate_t202380036 * value)
	{
		___InsetsForScreenOreitation_8 = value;
		Il2CppCodeGenWriteBarrier((&___InsetsForScreenOreitation_8), value);
	}

	inline static int32_t get_offset_of__insets_9() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____insets_9)); }
	inline UniWebViewEdgeInsets_t3496354243 * get__insets_9() const { return ____insets_9; }
	inline UniWebViewEdgeInsets_t3496354243 ** get_address_of__insets_9() { return &____insets_9; }
	inline void set__insets_9(UniWebViewEdgeInsets_t3496354243 * value)
	{
		____insets_9 = value;
		Il2CppCodeGenWriteBarrier((&____insets_9), value);
	}

	inline static int32_t get_offset_of_url_10() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___url_10)); }
	inline String_t* get_url_10() const { return ___url_10; }
	inline String_t** get_address_of_url_10() { return &___url_10; }
	inline void set_url_10(String_t* value)
	{
		___url_10 = value;
		Il2CppCodeGenWriteBarrier((&___url_10), value);
	}

	inline static int32_t get_offset_of_loadOnStart_11() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___loadOnStart_11)); }
	inline bool get_loadOnStart_11() const { return ___loadOnStart_11; }
	inline bool* get_address_of_loadOnStart_11() { return &___loadOnStart_11; }
	inline void set_loadOnStart_11(bool value)
	{
		___loadOnStart_11 = value;
	}

	inline static int32_t get_offset_of_autoShowWhenLoadComplete_12() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___autoShowWhenLoadComplete_12)); }
	inline bool get_autoShowWhenLoadComplete_12() const { return ___autoShowWhenLoadComplete_12; }
	inline bool* get_address_of_autoShowWhenLoadComplete_12() { return &___autoShowWhenLoadComplete_12; }
	inline void set_autoShowWhenLoadComplete_12(bool value)
	{
		___autoShowWhenLoadComplete_12 = value;
	}

	inline static int32_t get_offset_of__backButtonEnable_13() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____backButtonEnable_13)); }
	inline bool get__backButtonEnable_13() const { return ____backButtonEnable_13; }
	inline bool* get_address_of__backButtonEnable_13() { return &____backButtonEnable_13; }
	inline void set__backButtonEnable_13(bool value)
	{
		____backButtonEnable_13 = value;
	}

	inline static int32_t get_offset_of__bouncesEnable_14() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____bouncesEnable_14)); }
	inline bool get__bouncesEnable_14() const { return ____bouncesEnable_14; }
	inline bool* get_address_of__bouncesEnable_14() { return &____bouncesEnable_14; }
	inline void set__bouncesEnable_14(bool value)
	{
		____bouncesEnable_14 = value;
	}

	inline static int32_t get_offset_of__zoomEnable_15() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____zoomEnable_15)); }
	inline bool get__zoomEnable_15() const { return ____zoomEnable_15; }
	inline bool* get_address_of__zoomEnable_15() { return &____zoomEnable_15; }
	inline void set__zoomEnable_15(bool value)
	{
		____zoomEnable_15 = value;
	}

	inline static int32_t get_offset_of__currentGUID_16() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____currentGUID_16)); }
	inline String_t* get__currentGUID_16() const { return ____currentGUID_16; }
	inline String_t** get_address_of__currentGUID_16() { return &____currentGUID_16; }
	inline void set__currentGUID_16(String_t* value)
	{
		____currentGUID_16 = value;
		Il2CppCodeGenWriteBarrier((&____currentGUID_16), value);
	}

	inline static int32_t get_offset_of__lastScreenHeight_17() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____lastScreenHeight_17)); }
	inline int32_t get__lastScreenHeight_17() const { return ____lastScreenHeight_17; }
	inline int32_t* get_address_of__lastScreenHeight_17() { return &____lastScreenHeight_17; }
	inline void set__lastScreenHeight_17(int32_t value)
	{
		____lastScreenHeight_17 = value;
	}

	inline static int32_t get_offset_of__immersiveMode_18() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____immersiveMode_18)); }
	inline bool get__immersiveMode_18() const { return ____immersiveMode_18; }
	inline bool* get_address_of__immersiveMode_18() { return &____immersiveMode_18; }
	inline void set__immersiveMode_18(bool value)
	{
		____immersiveMode_18 = value;
	}

	inline static int32_t get_offset_of__showTransitionAction_19() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____showTransitionAction_19)); }
	inline Action_t1264377477 * get__showTransitionAction_19() const { return ____showTransitionAction_19; }
	inline Action_t1264377477 ** get_address_of__showTransitionAction_19() { return &____showTransitionAction_19; }
	inline void set__showTransitionAction_19(Action_t1264377477 * value)
	{
		____showTransitionAction_19 = value;
		Il2CppCodeGenWriteBarrier((&____showTransitionAction_19), value);
	}

	inline static int32_t get_offset_of__hideTransitionAction_20() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ____hideTransitionAction_20)); }
	inline Action_t1264377477 * get__hideTransitionAction_20() const { return ____hideTransitionAction_20; }
	inline Action_t1264377477 ** get_address_of__hideTransitionAction_20() { return &____hideTransitionAction_20; }
	inline void set__hideTransitionAction_20(Action_t1264377477 * value)
	{
		____hideTransitionAction_20 = value;
		Il2CppCodeGenWriteBarrier((&____hideTransitionAction_20), value);
	}

	inline static int32_t get_offset_of_toolBarShow_21() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___toolBarShow_21)); }
	inline bool get_toolBarShow_21() const { return ___toolBarShow_21; }
	inline bool* get_address_of_toolBarShow_21() { return &___toolBarShow_21; }
	inline void set_toolBarShow_21(bool value)
	{
		___toolBarShow_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEW_T941983939_H
#ifndef FIX3DTEXTJS_T2811811012_H
#define FIX3DTEXTJS_T2811811012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fix3dTextJS
struct  Fix3dTextJS_t2811811012  : public MonoBehaviour_t3962482529
{
public:
	// System.String Fix3dTextJS::text
	String_t* ___text_2;
	// System.Boolean Fix3dTextJS::tashkeel
	bool ___tashkeel_3;
	// System.Boolean Fix3dTextJS::hinduNumbers
	bool ___hinduNumbers_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t2811811012, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_tashkeel_3() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t2811811012, ___tashkeel_3)); }
	inline bool get_tashkeel_3() const { return ___tashkeel_3; }
	inline bool* get_address_of_tashkeel_3() { return &___tashkeel_3; }
	inline void set_tashkeel_3(bool value)
	{
		___tashkeel_3 = value;
	}

	inline static int32_t get_offset_of_hinduNumbers_4() { return static_cast<int32_t>(offsetof(Fix3dTextJS_t2811811012, ___hinduNumbers_4)); }
	inline bool get_hinduNumbers_4() const { return ___hinduNumbers_4; }
	inline bool* get_address_of_hinduNumbers_4() { return &___hinduNumbers_4; }
	inline void set_hinduNumbers_4(bool value)
	{
		___hinduNumbers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIX3DTEXTJS_T2811811012_H
#ifndef TOPSCENEMANAGER_T988329550_H
#define TOPSCENEMANAGER_T988329550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopSceneManager
struct  TopSceneManager_t988329550  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TopSceneManager::webview
	GameObject_t1113636619 * ___webview_2;
	// UnityEngine.UI.Text TopSceneManager::countDownText
	Text_t1901882714 * ___countDownText_3;
	// System.Int32 TopSceneManager::countDown
	int32_t ___countDown_4;

public:
	inline static int32_t get_offset_of_webview_2() { return static_cast<int32_t>(offsetof(TopSceneManager_t988329550, ___webview_2)); }
	inline GameObject_t1113636619 * get_webview_2() const { return ___webview_2; }
	inline GameObject_t1113636619 ** get_address_of_webview_2() { return &___webview_2; }
	inline void set_webview_2(GameObject_t1113636619 * value)
	{
		___webview_2 = value;
		Il2CppCodeGenWriteBarrier((&___webview_2), value);
	}

	inline static int32_t get_offset_of_countDownText_3() { return static_cast<int32_t>(offsetof(TopSceneManager_t988329550, ___countDownText_3)); }
	inline Text_t1901882714 * get_countDownText_3() const { return ___countDownText_3; }
	inline Text_t1901882714 ** get_address_of_countDownText_3() { return &___countDownText_3; }
	inline void set_countDownText_3(Text_t1901882714 * value)
	{
		___countDownText_3 = value;
		Il2CppCodeGenWriteBarrier((&___countDownText_3), value);
	}

	inline static int32_t get_offset_of_countDown_4() { return static_cast<int32_t>(offsetof(TopSceneManager_t988329550, ___countDown_4)); }
	inline int32_t get_countDown_4() const { return ___countDown_4; }
	inline int32_t* get_address_of_countDown_4() { return &___countDown_4; }
	inline void set_countDown_4(int32_t value)
	{
		___countDown_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPSCENEMANAGER_T988329550_H
#ifndef GRIDTABLEVIEW_T3636592800_H
#define GRIDTABLEVIEW_T3636592800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.GridTableView
struct  GridTableView_t3636592800  : public TableView_t4228429533
{
public:
	// UnityEngine.UI.GridLayoutGroup Tacticsoft.GridTableView::m_gridLayoutGroup
	GridLayoutGroup_t3046220461 * ___m_gridLayoutGroup_19;

public:
	inline static int32_t get_offset_of_m_gridLayoutGroup_19() { return static_cast<int32_t>(offsetof(GridTableView_t3636592800, ___m_gridLayoutGroup_19)); }
	inline GridLayoutGroup_t3046220461 * get_m_gridLayoutGroup_19() const { return ___m_gridLayoutGroup_19; }
	inline GridLayoutGroup_t3046220461 ** get_address_of_m_gridLayoutGroup_19() { return &___m_gridLayoutGroup_19; }
	inline void set_m_gridLayoutGroup_19(GridLayoutGroup_t3046220461 * value)
	{
		___m_gridLayoutGroup_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_gridLayoutGroup_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDTABLEVIEW_T3636592800_H
#ifndef VISIBLECOUNTERCELL_T1279458109_H
#define VISIBLECOUNTERCELL_T1279458109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.VisibleCounterCell
struct  VisibleCounterCell_t1279458109  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text Tacticsoft.Examples.VisibleCounterCell::m_rowNumberText
	Text_t1901882714 * ___m_rowNumberText_2;
	// UnityEngine.UI.Text Tacticsoft.Examples.VisibleCounterCell::m_visibleCountText
	Text_t1901882714 * ___m_visibleCountText_3;
	// System.Int32 Tacticsoft.Examples.VisibleCounterCell::m_numTimesBecameVisible
	int32_t ___m_numTimesBecameVisible_4;

public:
	inline static int32_t get_offset_of_m_rowNumberText_2() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1279458109, ___m_rowNumberText_2)); }
	inline Text_t1901882714 * get_m_rowNumberText_2() const { return ___m_rowNumberText_2; }
	inline Text_t1901882714 ** get_address_of_m_rowNumberText_2() { return &___m_rowNumberText_2; }
	inline void set_m_rowNumberText_2(Text_t1901882714 * value)
	{
		___m_rowNumberText_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_rowNumberText_2), value);
	}

	inline static int32_t get_offset_of_m_visibleCountText_3() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1279458109, ___m_visibleCountText_3)); }
	inline Text_t1901882714 * get_m_visibleCountText_3() const { return ___m_visibleCountText_3; }
	inline Text_t1901882714 ** get_address_of_m_visibleCountText_3() { return &___m_visibleCountText_3; }
	inline void set_m_visibleCountText_3(Text_t1901882714 * value)
	{
		___m_visibleCountText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_visibleCountText_3), value);
	}

	inline static int32_t get_offset_of_m_numTimesBecameVisible_4() { return static_cast<int32_t>(offsetof(VisibleCounterCell_t1279458109, ___m_numTimesBecameVisible_4)); }
	inline int32_t get_m_numTimesBecameVisible_4() const { return ___m_numTimesBecameVisible_4; }
	inline int32_t* get_address_of_m_numTimesBecameVisible_4() { return &___m_numTimesBecameVisible_4; }
	inline void set_m_numTimesBecameVisible_4(int32_t value)
	{
		___m_numTimesBecameVisible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBLECOUNTERCELL_T1279458109_H
#ifndef DYNAMICHEIGHTCELL_T2796155881_H
#define DYNAMICHEIGHTCELL_T2796155881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.DynamicHeightCell
struct  DynamicHeightCell_t2796155881  : public TableViewCell_t777944023
{
public:
	// UnityEngine.UI.Text Tacticsoft.Examples.DynamicHeightCell::m_rowNumberText
	Text_t1901882714 * ___m_rowNumberText_2;
	// UnityEngine.UI.Slider Tacticsoft.Examples.DynamicHeightCell::m_cellHeightSlider
	Slider_t3903728902 * ___m_cellHeightSlider_3;
	// System.Int32 Tacticsoft.Examples.DynamicHeightCell::<rowNumber>k__BackingField
	int32_t ___U3CrowNumberU3Ek__BackingField_4;
	// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent Tacticsoft.Examples.DynamicHeightCell::onCellHeightChanged
	CellHeightChangedEvent_t954279626 * ___onCellHeightChanged_5;

public:
	inline static int32_t get_offset_of_m_rowNumberText_2() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t2796155881, ___m_rowNumberText_2)); }
	inline Text_t1901882714 * get_m_rowNumberText_2() const { return ___m_rowNumberText_2; }
	inline Text_t1901882714 ** get_address_of_m_rowNumberText_2() { return &___m_rowNumberText_2; }
	inline void set_m_rowNumberText_2(Text_t1901882714 * value)
	{
		___m_rowNumberText_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_rowNumberText_2), value);
	}

	inline static int32_t get_offset_of_m_cellHeightSlider_3() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t2796155881, ___m_cellHeightSlider_3)); }
	inline Slider_t3903728902 * get_m_cellHeightSlider_3() const { return ___m_cellHeightSlider_3; }
	inline Slider_t3903728902 ** get_address_of_m_cellHeightSlider_3() { return &___m_cellHeightSlider_3; }
	inline void set_m_cellHeightSlider_3(Slider_t3903728902 * value)
	{
		___m_cellHeightSlider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_cellHeightSlider_3), value);
	}

	inline static int32_t get_offset_of_U3CrowNumberU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t2796155881, ___U3CrowNumberU3Ek__BackingField_4)); }
	inline int32_t get_U3CrowNumberU3Ek__BackingField_4() const { return ___U3CrowNumberU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CrowNumberU3Ek__BackingField_4() { return &___U3CrowNumberU3Ek__BackingField_4; }
	inline void set_U3CrowNumberU3Ek__BackingField_4(int32_t value)
	{
		___U3CrowNumberU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_onCellHeightChanged_5() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t2796155881, ___onCellHeightChanged_5)); }
	inline CellHeightChangedEvent_t954279626 * get_onCellHeightChanged_5() const { return ___onCellHeightChanged_5; }
	inline CellHeightChangedEvent_t954279626 ** get_address_of_onCellHeightChanged_5() { return &___onCellHeightChanged_5; }
	inline void set_onCellHeightChanged_5(CellHeightChangedEvent_t954279626 * value)
	{
		___onCellHeightChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCellHeightChanged_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICHEIGHTCELL_T2796155881_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t2130520576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t2130520576::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t2130520576::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t2130520576::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t2130520576::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (TouchPoint_t873891735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[11] = 
{
	TouchPoint_t873891735::get_offset_of_U3CIdU3Ek__BackingField_0(),
	TouchPoint_t873891735::get_offset_of_U3CTargetU3Ek__BackingField_1(),
	TouchPoint_t873891735::get_offset_of_U3CPreviousPositionU3Ek__BackingField_2(),
	TouchPoint_t873891735::get_offset_of_U3CHitU3Ek__BackingField_3(),
	TouchPoint_t873891735::get_offset_of_U3CLayerU3Ek__BackingField_4(),
	TouchPoint_t873891735::get_offset_of_U3CInputSourceU3Ek__BackingField_5(),
	TouchPoint_t873891735::get_offset_of_U3CTagsU3Ek__BackingField_6(),
	TouchPoint_t873891735::get_offset_of_refCount_7(),
	TouchPoint_t873891735::get_offset_of_position_8(),
	TouchPoint_t873891735::get_offset_of_newPosition_9(),
	TouchPoint_t873891735::get_offset_of_properties_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (NullToggleAttribute_t3228924299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[3] = 
{
	NullToggleAttribute_t3228924299::get_offset_of_NullIntValue_0(),
	NullToggleAttribute_t3228924299::get_offset_of_NullFloatValue_1(),
	NullToggleAttribute_t3228924299::get_offset_of_NullObjectValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (ToggleLeftAttribute_t2082752021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (ClusterUtils_t2231362195), -1, sizeof(ClusterUtils_t2231362195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3704[1] = 
{
	ClusterUtils_t2231362195_StaticFields::get_offset_of_hashString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (EventHandlerExtensions_t1390733735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (TwoD_t1747174160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (ProjectionUtils_t3292001777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (TouchUtils_t2322363280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (TransformUtils_t4150736043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (DynamicHeightCell_t2796155881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[4] = 
{
	DynamicHeightCell_t2796155881::get_offset_of_m_rowNumberText_2(),
	DynamicHeightCell_t2796155881::get_offset_of_m_cellHeightSlider_3(),
	DynamicHeightCell_t2796155881::get_offset_of_U3CrowNumberU3Ek__BackingField_4(),
	DynamicHeightCell_t2796155881::get_offset_of_onCellHeightChanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (CellHeightChangedEvent_t954279626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (DynamicHeightTableViewController_t2429124891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[5] = 
{
	DynamicHeightTableViewController_t2429124891::get_offset_of_m_cellPrefab_2(),
	DynamicHeightTableViewController_t2429124891::get_offset_of_m_tableView_3(),
	DynamicHeightTableViewController_t2429124891::get_offset_of_m_numRows_4(),
	DynamicHeightTableViewController_t2429124891::get_offset_of_m_numInstancesCreated_5(),
	DynamicHeightTableViewController_t2429124891::get_offset_of_m_customRowHeights_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (ScrollingEventsHandler_t2846363661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[1] = 
{
	ScrollingEventsHandler_t2846363661::get_offset_of_m_tableView_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (U3CAnimateToScrollYU3Ec__Iterator0_t2778569968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[10] = 
{
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U3CstartScrollYU3E__0_1(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_time_2(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U3CendTimeU3E__0_3(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U3CrelativeProgressU3E__1_4(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_scrollY_5(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U24this_6(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U24current_7(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U24disposing_8(),
	U3CAnimateToScrollYU3Ec__Iterator0_t2778569968::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (SimpleTableViewController_t2092983076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3718[4] = 
{
	SimpleTableViewController_t2092983076::get_offset_of_m_cellPrefab_2(),
	SimpleTableViewController_t2092983076::get_offset_of_m_tableView_3(),
	SimpleTableViewController_t2092983076::get_offset_of_m_numRows_4(),
	SimpleTableViewController_t2092983076::get_offset_of_m_numInstancesCreated_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (VisibleCounterCell_t1279458109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[3] = 
{
	VisibleCounterCell_t1279458109::get_offset_of_m_rowNumberText_2(),
	VisibleCounterCell_t1279458109::get_offset_of_m_visibleCountText_3(),
	VisibleCounterCell_t1279458109::get_offset_of_m_numTimesBecameVisible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (GridTableView_t3636592800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[1] = 
{
	GridTableView_t3636592800::get_offset_of_m_gridLayoutGroup_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (TableView_t4228429533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[17] = 
{
	TableView_t4228429533::get_offset_of_onCellVisibilityChanged_2(),
	TableView_t4228429533::get_offset_of_U3CisEmptyU3Ek__BackingField_3(),
	TableView_t4228429533::get_offset_of_m_dataSource_4(),
	TableView_t4228429533::get_offset_of_m_requiresReload_5(),
	TableView_t4228429533::get_offset_of_m_verticalLayoutGroup_6(),
	TableView_t4228429533::get_offset_of_m_scrollRect_7(),
	TableView_t4228429533::get_offset_of_m_topPadding_8(),
	TableView_t4228429533::get_offset_of_m_bottomPadding_9(),
	TableView_t4228429533::get_offset_of_m_rowHeights_10(),
	TableView_t4228429533::get_offset_of_m_cumulativeRowHeights_11(),
	TableView_t4228429533::get_offset_of_m_cleanCumulativeIndex_12(),
	TableView_t4228429533::get_offset_of_m_visibleCells_13(),
	TableView_t4228429533::get_offset_of_m_visibleRowRange_14(),
	TableView_t4228429533::get_offset_of_m_reusableCellContainer_15(),
	TableView_t4228429533::get_offset_of_m_reusableCells_16(),
	TableView_t4228429533::get_offset_of_m_scrollY_17(),
	TableView_t4228429533::get_offset_of_m_requiresRefresh_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (CellVisibilityChangeEvent_t2201436281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (RangeExtensions_t531368024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (TableViewCell_t777944023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (TopSceneManager_t988329550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[3] = 
{
	TopSceneManager_t988329550::get_offset_of_webview_2(),
	TopSceneManager_t988329550::get_offset_of_countDownText_3(),
	TopSceneManager_t988329550::get_offset_of_countDown_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (UseWithCodeSceneManager_t1728296987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[1] = 
{
	UseWithCodeSceneManager_t1728296987::get_offset_of_urlInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (LocalHTMLSceneManager_t868756294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[2] = 
{
	LocalHTMLSceneManager_t868756294::get_offset_of_fileName_2(),
	LocalHTMLSceneManager_t868756294::get_offset_of_htmlText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (SizeAndTransitionSceneManager_t3659808263), -1, sizeof(SizeAndTransitionSceneManager_t3659808263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3729[13] = 
{
	SizeAndTransitionSceneManager_t3659808263::get_offset_of__webView_2(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of__webViewReady_3(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_U3CfadeU3Ek__BackingField_4(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_U3CtransitionEdgeU3Ek__BackingField_5(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_top_6(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_left_7(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_bottom_8(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_right_9(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_x_10(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_y_11(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_width_12(),
	SizeAndTransitionSceneManager_t3659808263::get_offset_of_height_13(),
	SizeAndTransitionSceneManager_t3659808263_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (U3CCreateWebViewU3Ec__AnonStorey0_t2301772004), -1, sizeof(U3CCreateWebViewU3Ec__AnonStorey0_t2301772004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3730[3] = 
{
	U3CCreateWebViewU3Ec__AnonStorey0_t2301772004::get_offset_of_webView_0(),
	U3CCreateWebViewU3Ec__AnonStorey0_t2301772004::get_offset_of_U24this_1(),
	U3CCreateWebViewU3Ec__AnonStorey0_t2301772004_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (CallbackFromWebSceneManager_t2952333088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[3] = 
{
	CallbackFromWebSceneManager_t2952333088::get_offset_of_result_2(),
	CallbackFromWebSceneManager_t2952333088::get_offset_of__webView_3(),
	CallbackFromWebSceneManager_t2952333088::get_offset_of__fileName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (RunJavaScriptInWebSceneManager_t3721445575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[3] = 
{
	RunJavaScriptInWebSceneManager_t3721445575::get_offset_of_result_2(),
	RunJavaScriptInWebSceneManager_t3721445575::get_offset_of__webView_3(),
	RunJavaScriptInWebSceneManager_t3721445575::get_offset_of__fileName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (Navigator_t1365902112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (UniWebViewHelper_t1459904593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (UniWebView_t941983939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3735[20] = 
{
	UniWebView_t941983939::get_offset_of_OnLoadComplete_2(),
	UniWebView_t941983939::get_offset_of_OnLoadBegin_3(),
	UniWebView_t941983939::get_offset_of_OnReceivedMessage_4(),
	UniWebView_t941983939::get_offset_of_OnEvalJavaScriptFinished_5(),
	UniWebView_t941983939::get_offset_of_OnWebViewShouldClose_6(),
	UniWebView_t941983939::get_offset_of_OnReceivedKeyCode_7(),
	UniWebView_t941983939::get_offset_of_InsetsForScreenOreitation_8(),
	UniWebView_t941983939::get_offset_of__insets_9(),
	UniWebView_t941983939::get_offset_of_url_10(),
	UniWebView_t941983939::get_offset_of_loadOnStart_11(),
	UniWebView_t941983939::get_offset_of_autoShowWhenLoadComplete_12(),
	UniWebView_t941983939::get_offset_of__backButtonEnable_13(),
	UniWebView_t941983939::get_offset_of__bouncesEnable_14(),
	UniWebView_t941983939::get_offset_of__zoomEnable_15(),
	UniWebView_t941983939::get_offset_of__currentGUID_16(),
	UniWebView_t941983939::get_offset_of__lastScreenHeight_17(),
	UniWebView_t941983939::get_offset_of__immersiveMode_18(),
	UniWebView_t941983939::get_offset_of__showTransitionAction_19(),
	UniWebView_t941983939::get_offset_of__hideTransitionAction_20(),
	UniWebView_t941983939::get_offset_of_toolBarShow_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (LoadCompleteDelegate_t2989787613), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (LoadBeginDelegate_t3414845260), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (ReceivedMessageDelegate_t188390413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (EvalJavaScriptFinishedDelegate_t3947010167), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (WebViewShouldCloseDelegate_t3305779967), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (ReceivedKeyCodeDelegate_t1391812367), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (InsetsForScreenOreitationDelegate_t202380036), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3743[6] = 
{
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_jarFilePath_0(),
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_U3CstreamU3E__0_1(),
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_U24this_2(),
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_U24current_3(),
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_U24disposing_4(),
	U3CLoadFromJarPackageU3Ec__Iterator0_t3671744877::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (UniWebViewEdgeInsets_t3496354243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[4] = 
{
	UniWebViewEdgeInsets_t3496354243::get_offset_of_top_0(),
	UniWebViewEdgeInsets_t3496354243::get_offset_of_left_1(),
	UniWebViewEdgeInsets_t3496354243::get_offset_of_bottom_2(),
	UniWebViewEdgeInsets_t3496354243::get_offset_of_right_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (UniWebViewMessage_t2441068380)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3745[4] = 
{
	UniWebViewMessage_t2441068380::get_offset_of_U3CrawMessageU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CschemeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CpathU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CargsU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (UniWebViewOrientation_t1746230176)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3746[3] = 
{
	UniWebViewOrientation_t1746230176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (UniWebViewTransitionEdge_t2354894166)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3747[6] = 
{
	UniWebViewTransitionEdge_t2354894166::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3748[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD06AFECACD30B599375506E0891369AE6874C159_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (U24ArrayTypeU3D80_t183802871)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D80_t183802871 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (U3CModuleU3E_t692745559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (Fix3dTextJS_t2811811012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[3] = 
{
	Fix3dTextJS_t2811811012::get_offset_of_text_2(),
	Fix3dTextJS_t2811811012::get_offset_of_tashkeel_3(),
	Fix3dTextJS_t2811811012::get_offset_of_hinduNumbers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (FixGUITextJS_t3341698330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[3] = 
{
	FixGUITextJS_t3341698330::get_offset_of_text_2(),
	FixGUITextJS_t3341698330::get_offset_of_tashkeel_3(),
	FixGUITextJS_t3341698330::get_offset_of_hinduNumbers_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
