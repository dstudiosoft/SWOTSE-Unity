﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>
struct UnityFunc_1_t1709969761;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityFunc_1__ctor_m3483258383_gshared (UnityFunc_1_t1709969761 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define UnityFunc_1__ctor_m3483258383(__this, ___object0, ___method1, method) ((  void (*) (UnityFunc_1_t1709969761 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityFunc_1__ctor_m3483258383_gshared)(__this, ___object0, ___method1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>::Invoke()
extern "C"  Il2CppObject * UnityFunc_1_Invoke_m4133775042_gshared (UnityFunc_1_t1709969761 * __this, const MethodInfo* method);
#define UnityFunc_1_Invoke_m4133775042(__this, method) ((  Il2CppObject * (*) (UnityFunc_1_t1709969761 *, const MethodInfo*))UnityFunc_1_Invoke_m4133775042_gshared)(__this, method)
// System.IAsyncResult TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityFunc_1_BeginInvoke_m3032171082_gshared (UnityFunc_1_t1709969761 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define UnityFunc_1_BeginInvoke_m3032171082(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (UnityFunc_1_t1709969761 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_BeginInvoke_m3032171082_gshared)(__this, ___callback0, ___object1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * UnityFunc_1_EndInvoke_m455851222_gshared (UnityFunc_1_t1709969761 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define UnityFunc_1_EndInvoke_m455851222(__this, ___result0, method) ((  Il2CppObject * (*) (UnityFunc_1_t1709969761 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_EndInvoke_m455851222_gshared)(__this, ___result0, method)
