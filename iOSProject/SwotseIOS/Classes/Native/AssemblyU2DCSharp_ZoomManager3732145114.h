﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomManager
struct  ZoomManager_t3732145114  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera ZoomManager::mainCamera
	Camera_t189460977 * ___mainCamera_2;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(ZoomManager_t3732145114, ___mainCamera_2)); }
	inline Camera_t189460977 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Camera_t189460977 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Camera_t189460977 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
