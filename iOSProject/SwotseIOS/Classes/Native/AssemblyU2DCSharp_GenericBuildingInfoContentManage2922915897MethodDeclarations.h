﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenericBuildingInfoContentManager
struct GenericBuildingInfoContentManager_t2922915897;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void GenericBuildingInfoContentManager::.ctor()
extern "C"  void GenericBuildingInfoContentManager__ctor_m4009580560 (GenericBuildingInfoContentManager_t2922915897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::add_confirmed(ConfirmationEvent)
extern "C"  void GenericBuildingInfoContentManager_add_confirmed_m1516995108 (GenericBuildingInfoContentManager_t2922915897 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::remove_confirmed(ConfirmationEvent)
extern "C"  void GenericBuildingInfoContentManager_remove_confirmed_m671927625 (GenericBuildingInfoContentManager_t2922915897 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::SetInitialInfo(System.String,System.Int64)
extern "C"  void GenericBuildingInfoContentManager_SetInitialInfo_m725314570 (GenericBuildingInfoContentManager_t2922915897 * __this, String_t* ___type0, int64_t ___pitId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::Upgrade()
extern "C"  void GenericBuildingInfoContentManager_Upgrade_m1867720944 (GenericBuildingInfoContentManager_t2922915897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::Downgrade()
extern "C"  void GenericBuildingInfoContentManager_Downgrade_m1097783487 (GenericBuildingInfoContentManager_t2922915897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::Demolish()
extern "C"  void GenericBuildingInfoContentManager_Demolish_m4177747345 (GenericBuildingInfoContentManager_t2922915897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::DemolishConfirmed(System.Boolean)
extern "C"  void GenericBuildingInfoContentManager_DemolishConfirmed_m2832146585 (GenericBuildingInfoContentManager_t2922915897 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::Demolished(System.Object,System.String)
extern "C"  void GenericBuildingInfoContentManager_Demolished_m3312092672 (GenericBuildingInfoContentManager_t2922915897 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericBuildingInfoContentManager::HandleUpdate(System.Object,System.String)
extern "C"  void GenericBuildingInfoContentManager_HandleUpdate_m1602839399 (GenericBuildingInfoContentManager_t2922915897 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
