﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey1`1<Firebase.DependencyStatus,System.Object>
struct U3CContinueWithU3Ec__AnonStorey1_1_t1119576783;
// System.Object
struct Il2CppObject;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey1`1<Firebase.DependencyStatus,System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1_1__ctor_m2731057834_gshared (U3CContinueWithU3Ec__AnonStorey1_1_t1119576783 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1_1__ctor_m2731057834(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_1_t1119576783 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1_1__ctor_m2731057834_gshared)(__this, method)
// TResult System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey1`1<Firebase.DependencyStatus,System.Object>::<>m__0(System.Threading.Tasks.Task)
extern "C"  Il2CppObject * U3CContinueWithU3Ec__AnonStorey1_1_U3CU3Em__0_m1725259553_gshared (U3CContinueWithU3Ec__AnonStorey1_1_t1119576783 * __this, Task_t1843236107 * ___t0, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1_1_U3CU3Em__0_m1725259553(__this, ___t0, method) ((  Il2CppObject * (*) (U3CContinueWithU3Ec__AnonStorey1_1_t1119576783 *, Task_t1843236107 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1_1_U3CU3Em__0_m1725259553_gshared)(__this, ___t0, method)
