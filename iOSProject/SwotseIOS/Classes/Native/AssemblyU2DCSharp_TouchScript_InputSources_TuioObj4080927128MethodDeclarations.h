﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.TuioObjectMapping
struct TuioObjectMapping_t4080927128;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.InputSources.TuioObjectMapping::.ctor()
extern "C"  void TuioObjectMapping__ctor_m243547575 (TuioObjectMapping_t4080927128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
