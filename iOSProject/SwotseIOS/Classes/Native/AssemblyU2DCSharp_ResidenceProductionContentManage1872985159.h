﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceProductionContentManager
struct  ResidenceProductionContentManager_t1872985159  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::laborForces
	TextU5BU5D_t4216439300* ___laborForces_2;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::basicProduction
	TextU5BU5D_t4216439300* ___basicProduction_3;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::coloniesTributes
	TextU5BU5D_t4216439300* ___coloniesTributes_4;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::valleysProduction
	TextU5BU5D_t4216439300* ___valleysProduction_5;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::mayorPlus
	TextU5BU5D_t4216439300* ___mayorPlus_6;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::tributesPaid
	TextU5BU5D_t4216439300* ___tributesPaid_7;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::troopUpkeep
	TextU5BU5D_t4216439300* ___troopUpkeep_8;
	// UnityEngine.UI.Text[] ResidenceProductionContentManager::netProduction
	TextU5BU5D_t4216439300* ___netProduction_9;

public:
	inline static int32_t get_offset_of_laborForces_2() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___laborForces_2)); }
	inline TextU5BU5D_t4216439300* get_laborForces_2() const { return ___laborForces_2; }
	inline TextU5BU5D_t4216439300** get_address_of_laborForces_2() { return &___laborForces_2; }
	inline void set_laborForces_2(TextU5BU5D_t4216439300* value)
	{
		___laborForces_2 = value;
		Il2CppCodeGenWriteBarrier(&___laborForces_2, value);
	}

	inline static int32_t get_offset_of_basicProduction_3() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___basicProduction_3)); }
	inline TextU5BU5D_t4216439300* get_basicProduction_3() const { return ___basicProduction_3; }
	inline TextU5BU5D_t4216439300** get_address_of_basicProduction_3() { return &___basicProduction_3; }
	inline void set_basicProduction_3(TextU5BU5D_t4216439300* value)
	{
		___basicProduction_3 = value;
		Il2CppCodeGenWriteBarrier(&___basicProduction_3, value);
	}

	inline static int32_t get_offset_of_coloniesTributes_4() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___coloniesTributes_4)); }
	inline TextU5BU5D_t4216439300* get_coloniesTributes_4() const { return ___coloniesTributes_4; }
	inline TextU5BU5D_t4216439300** get_address_of_coloniesTributes_4() { return &___coloniesTributes_4; }
	inline void set_coloniesTributes_4(TextU5BU5D_t4216439300* value)
	{
		___coloniesTributes_4 = value;
		Il2CppCodeGenWriteBarrier(&___coloniesTributes_4, value);
	}

	inline static int32_t get_offset_of_valleysProduction_5() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___valleysProduction_5)); }
	inline TextU5BU5D_t4216439300* get_valleysProduction_5() const { return ___valleysProduction_5; }
	inline TextU5BU5D_t4216439300** get_address_of_valleysProduction_5() { return &___valleysProduction_5; }
	inline void set_valleysProduction_5(TextU5BU5D_t4216439300* value)
	{
		___valleysProduction_5 = value;
		Il2CppCodeGenWriteBarrier(&___valleysProduction_5, value);
	}

	inline static int32_t get_offset_of_mayorPlus_6() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___mayorPlus_6)); }
	inline TextU5BU5D_t4216439300* get_mayorPlus_6() const { return ___mayorPlus_6; }
	inline TextU5BU5D_t4216439300** get_address_of_mayorPlus_6() { return &___mayorPlus_6; }
	inline void set_mayorPlus_6(TextU5BU5D_t4216439300* value)
	{
		___mayorPlus_6 = value;
		Il2CppCodeGenWriteBarrier(&___mayorPlus_6, value);
	}

	inline static int32_t get_offset_of_tributesPaid_7() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___tributesPaid_7)); }
	inline TextU5BU5D_t4216439300* get_tributesPaid_7() const { return ___tributesPaid_7; }
	inline TextU5BU5D_t4216439300** get_address_of_tributesPaid_7() { return &___tributesPaid_7; }
	inline void set_tributesPaid_7(TextU5BU5D_t4216439300* value)
	{
		___tributesPaid_7 = value;
		Il2CppCodeGenWriteBarrier(&___tributesPaid_7, value);
	}

	inline static int32_t get_offset_of_troopUpkeep_8() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___troopUpkeep_8)); }
	inline TextU5BU5D_t4216439300* get_troopUpkeep_8() const { return ___troopUpkeep_8; }
	inline TextU5BU5D_t4216439300** get_address_of_troopUpkeep_8() { return &___troopUpkeep_8; }
	inline void set_troopUpkeep_8(TextU5BU5D_t4216439300* value)
	{
		___troopUpkeep_8 = value;
		Il2CppCodeGenWriteBarrier(&___troopUpkeep_8, value);
	}

	inline static int32_t get_offset_of_netProduction_9() { return static_cast<int32_t>(offsetof(ResidenceProductionContentManager_t1872985159, ___netProduction_9)); }
	inline TextU5BU5D_t4216439300* get_netProduction_9() const { return ___netProduction_9; }
	inline TextU5BU5D_t4216439300** get_address_of_netProduction_9() { return &___netProduction_9; }
	inline void set_netProduction_9(TextU5BU5D_t4216439300* value)
	{
		___netProduction_9 = value;
		Il2CppCodeGenWriteBarrier(&___netProduction_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
