﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t2116608150;
// System.Xml.XmlNamespaceManager/NsScope[]
struct NsScopeU5BU5D_t382374428;
// System.String
struct String_t;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t1073099671;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t2958275022;
// Mono.Xml.DTDElementAutomata
struct DTDElementAutomata_t1050190167;
// Mono.Xml.DTDEmptyAutomata
struct DTDEmptyAutomata_t465590953;
// Mono.Xml.DTDAnyAutomata
struct DTDAnyAutomata_t3633486160;
// Mono.Xml.DTDInvalidAutomata
struct DTDInvalidAutomata_t1406553220;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t222313714;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t2220366188;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t2844734410;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t2250844513;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t959292105;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t139615908;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t1082692789;
// System.Xml.XmlParserInput
struct XmlParserInput_t2182411204;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Text.StringBuilder
struct StringBuilder_t;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t1729680289;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t2708887342;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1437094927;
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t3368185270;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Xml.XmlParserInput/XmlParserInputSource
struct XmlParserInputSource_t3533005609;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Xml.XmlImplementation
struct XmlImplementation_t254178875;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t2890546907;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t934654762;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t1533444722;
// System.Xml.XmlElement
struct XmlElement_t561603118;
// Mono.Xml.DTDNode
struct DTDNode_t858560093;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Text.Decoder
struct Decoder_t2204182725;
// System.IO.Stream
struct Stream_t1273022909;
// System.Xml.XmlException
struct XmlException_t1761730631;
// Mono.Xml.DTDAutomata
struct DTDAutomata_t781538777;
// System.Uri
struct Uri_t100236324;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t3264596611;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t491982174;
// System.Type
struct Type_t;
// Mono.Xml.EntityResolvingXmlReader
struct EntityResolvingXmlReader_t1267732406;
// System.Xml.XmlTextReader
struct XmlTextReader_t4233384356;
// System.Xml.XmlValidatingReader
struct XmlValidatingReader_t1719295192;
// Mono.Xml.DTDValidatingReader/AttributeSlot[]
struct AttributeSlotU5BU5D_t600906362;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct List_1_t218596005;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t2821286253;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t2316283784;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Xml.XmlInputStream
struct XmlInputStream_t1691369434;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlParserContext
struct XmlParserContext_t2544895291;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t322714710;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t2798820000;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t52754249;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLNAMESPACEMANAGER_T418790500_H
#define XMLNAMESPACEMANAGER_T418790500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t418790500  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_t2116608150* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.Xml.XmlNamespaceManager/NsScope[] System.Xml.XmlNamespaceManager::scopes
	NsScopeU5BU5D_t382374428* ___scopes_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopePos
	int32_t ___scopePos_3;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_4;
	// System.Int32 System.Xml.XmlNamespaceManager::count
	int32_t ___count_5;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t71772148 * ___nameTable_6;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_7;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___decls_0)); }
	inline NsDeclU5BU5D_t2116608150* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_t2116608150** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_t2116608150* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_scopes_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___scopes_2)); }
	inline NsScopeU5BU5D_t382374428* get_scopes_2() const { return ___scopes_2; }
	inline NsScopeU5BU5D_t382374428** get_address_of_scopes_2() { return &___scopes_2; }
	inline void set_scopes_2(NsScopeU5BU5D_t382374428* value)
	{
		___scopes_2 = value;
		Il2CppCodeGenWriteBarrier((&___scopes_2), value);
	}

	inline static int32_t get_offset_of_scopePos_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___scopePos_3)); }
	inline int32_t get_scopePos_3() const { return ___scopePos_3; }
	inline int32_t* get_address_of_scopePos_3() { return &___scopePos_3; }
	inline void set_scopePos_3(int32_t value)
	{
		___scopePos_3 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___defaultNamespace_4)); }
	inline String_t* get_defaultNamespace_4() const { return ___defaultNamespace_4; }
	inline String_t** get_address_of_defaultNamespace_4() { return &___defaultNamespace_4; }
	inline void set_defaultNamespace_4(String_t* value)
	{
		___defaultNamespace_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_nameTable_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___nameTable_6)); }
	inline XmlNameTable_t71772148 * get_nameTable_6() const { return ___nameTable_6; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_6() { return &___nameTable_6; }
	inline void set_nameTable_6(XmlNameTable_t71772148 * value)
	{
		___nameTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_6), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500, ___internalAtomizedNames_7)); }
	inline bool get_internalAtomizedNames_7() const { return ___internalAtomizedNames_7; }
	inline bool* get_address_of_internalAtomizedNames_7() { return &___internalAtomizedNames_7; }
	inline void set_internalAtomizedNames_7(bool value)
	{
		___internalAtomizedNames_7 = value;
	}
};

struct XmlNamespaceManager_t418790500_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switch$map28
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map28_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_8() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t418790500_StaticFields, ___U3CU3Ef__switchU24map28_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map28_8() const { return ___U3CU3Ef__switchU24map28_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map28_8() { return &___U3CU3Ef__switchU24map28_8; }
	inline void set_U3CU3Ef__switchU24map28_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map28_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map28_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T418790500_H
#ifndef XMLNAMEDNODEMAP_T2821286253_H
#define XMLNAMEDNODEMAP_T2821286253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t2821286253  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t3767805227 * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t2718874744 * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___parent_1)); }
	inline XmlNode_t3767805227 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t3767805227 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t3767805227 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___nodeList_2)); }
	inline ArrayList_t2718874744 * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t2718874744 ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t2718874744 * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_t2821286253_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t2821286253_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T2821286253_H
#ifndef XMLNAMEENTRYCACHE_T2890546907_H
#define XMLNAMEENTRYCACHE_T2890546907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntryCache
struct  XmlNameEntryCache_t2890546907  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlNameEntryCache::table
	Hashtable_t1853889766 * ___table_0;
	// System.Xml.XmlNameTable System.Xml.XmlNameEntryCache::nameTable
	XmlNameTable_t71772148 * ___nameTable_1;
	// System.Xml.XmlNameEntry System.Xml.XmlNameEntryCache::dummy
	XmlNameEntry_t1073099671 * ___dummy_2;
	// System.Char[] System.Xml.XmlNameEntryCache::cacheBuffer
	CharU5BU5D_t3528271667* ___cacheBuffer_3;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___table_0)); }
	inline Hashtable_t1853889766 * get_table_0() const { return ___table_0; }
	inline Hashtable_t1853889766 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_t1853889766 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___nameTable_1)); }
	inline XmlNameTable_t71772148 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t71772148 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_dummy_2() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___dummy_2)); }
	inline XmlNameEntry_t1073099671 * get_dummy_2() const { return ___dummy_2; }
	inline XmlNameEntry_t1073099671 ** get_address_of_dummy_2() { return &___dummy_2; }
	inline void set_dummy_2(XmlNameEntry_t1073099671 * value)
	{
		___dummy_2 = value;
		Il2CppCodeGenWriteBarrier((&___dummy_2), value);
	}

	inline static int32_t get_offset_of_cacheBuffer_3() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t2890546907, ___cacheBuffer_3)); }
	inline CharU5BU5D_t3528271667* get_cacheBuffer_3() const { return ___cacheBuffer_3; }
	inline CharU5BU5D_t3528271667** get_address_of_cacheBuffer_3() { return &___cacheBuffer_3; }
	inline void set_cacheBuffer_3(CharU5BU5D_t3528271667* value)
	{
		___cacheBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___cacheBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRYCACHE_T2890546907_H
#ifndef XMLNAMETABLE_T71772148_H
#define XMLNAMETABLE_T71772148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t71772148  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T71772148_H
#ifndef DTDOBJECTMODEL_T1729680289_H
#define DTDOBJECTMODEL_T1729680289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDObjectModel
struct  DTDObjectModel_t1729680289  : public RuntimeObject
{
public:
	// Mono.Xml.DTDAutomataFactory Mono.Xml.DTDObjectModel::factory
	DTDAutomataFactory_t2958275022 * ___factory_0;
	// Mono.Xml.DTDElementAutomata Mono.Xml.DTDObjectModel::rootAutomata
	DTDElementAutomata_t1050190167 * ___rootAutomata_1;
	// Mono.Xml.DTDEmptyAutomata Mono.Xml.DTDObjectModel::emptyAutomata
	DTDEmptyAutomata_t465590953 * ___emptyAutomata_2;
	// Mono.Xml.DTDAnyAutomata Mono.Xml.DTDObjectModel::anyAutomata
	DTDAnyAutomata_t3633486160 * ___anyAutomata_3;
	// Mono.Xml.DTDInvalidAutomata Mono.Xml.DTDObjectModel::invalidAutomata
	DTDInvalidAutomata_t1406553220 * ___invalidAutomata_4;
	// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::elementDecls
	DTDElementDeclarationCollection_t222313714 * ___elementDecls_5;
	// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::attListDecls
	DTDAttListDeclarationCollection_t2220366188 * ___attListDecls_6;
	// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::peDecls
	DTDParameterEntityDeclarationCollection_t2844734410 * ___peDecls_7;
	// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::entityDecls
	DTDEntityDeclarationCollection_t2250844513 * ___entityDecls_8;
	// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::notationDecls
	DTDNotationDeclarationCollection_t959292105 * ___notationDecls_9;
	// System.Collections.ArrayList Mono.Xml.DTDObjectModel::validationErrors
	ArrayList_t2718874744 * ___validationErrors_10;
	// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::resolver
	XmlResolver_t626023767 * ___resolver_11;
	// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::nameTable
	XmlNameTable_t71772148 * ___nameTable_12;
	// System.Collections.Hashtable Mono.Xml.DTDObjectModel::externalResources
	Hashtable_t1853889766 * ___externalResources_13;
	// System.String Mono.Xml.DTDObjectModel::baseURI
	String_t* ___baseURI_14;
	// System.String Mono.Xml.DTDObjectModel::name
	String_t* ___name_15;
	// System.String Mono.Xml.DTDObjectModel::publicId
	String_t* ___publicId_16;
	// System.String Mono.Xml.DTDObjectModel::systemId
	String_t* ___systemId_17;
	// System.String Mono.Xml.DTDObjectModel::intSubset
	String_t* ___intSubset_18;
	// System.Boolean Mono.Xml.DTDObjectModel::intSubsetHasPERef
	bool ___intSubsetHasPERef_19;
	// System.Boolean Mono.Xml.DTDObjectModel::isStandalone
	bool ___isStandalone_20;
	// System.Int32 Mono.Xml.DTDObjectModel::lineNumber
	int32_t ___lineNumber_21;
	// System.Int32 Mono.Xml.DTDObjectModel::linePosition
	int32_t ___linePosition_22;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___factory_0)); }
	inline DTDAutomataFactory_t2958275022 * get_factory_0() const { return ___factory_0; }
	inline DTDAutomataFactory_t2958275022 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(DTDAutomataFactory_t2958275022 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___factory_0), value);
	}

	inline static int32_t get_offset_of_rootAutomata_1() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___rootAutomata_1)); }
	inline DTDElementAutomata_t1050190167 * get_rootAutomata_1() const { return ___rootAutomata_1; }
	inline DTDElementAutomata_t1050190167 ** get_address_of_rootAutomata_1() { return &___rootAutomata_1; }
	inline void set_rootAutomata_1(DTDElementAutomata_t1050190167 * value)
	{
		___rootAutomata_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootAutomata_1), value);
	}

	inline static int32_t get_offset_of_emptyAutomata_2() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___emptyAutomata_2)); }
	inline DTDEmptyAutomata_t465590953 * get_emptyAutomata_2() const { return ___emptyAutomata_2; }
	inline DTDEmptyAutomata_t465590953 ** get_address_of_emptyAutomata_2() { return &___emptyAutomata_2; }
	inline void set_emptyAutomata_2(DTDEmptyAutomata_t465590953 * value)
	{
		___emptyAutomata_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAutomata_2), value);
	}

	inline static int32_t get_offset_of_anyAutomata_3() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___anyAutomata_3)); }
	inline DTDAnyAutomata_t3633486160 * get_anyAutomata_3() const { return ___anyAutomata_3; }
	inline DTDAnyAutomata_t3633486160 ** get_address_of_anyAutomata_3() { return &___anyAutomata_3; }
	inline void set_anyAutomata_3(DTDAnyAutomata_t3633486160 * value)
	{
		___anyAutomata_3 = value;
		Il2CppCodeGenWriteBarrier((&___anyAutomata_3), value);
	}

	inline static int32_t get_offset_of_invalidAutomata_4() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___invalidAutomata_4)); }
	inline DTDInvalidAutomata_t1406553220 * get_invalidAutomata_4() const { return ___invalidAutomata_4; }
	inline DTDInvalidAutomata_t1406553220 ** get_address_of_invalidAutomata_4() { return &___invalidAutomata_4; }
	inline void set_invalidAutomata_4(DTDInvalidAutomata_t1406553220 * value)
	{
		___invalidAutomata_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidAutomata_4), value);
	}

	inline static int32_t get_offset_of_elementDecls_5() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___elementDecls_5)); }
	inline DTDElementDeclarationCollection_t222313714 * get_elementDecls_5() const { return ___elementDecls_5; }
	inline DTDElementDeclarationCollection_t222313714 ** get_address_of_elementDecls_5() { return &___elementDecls_5; }
	inline void set_elementDecls_5(DTDElementDeclarationCollection_t222313714 * value)
	{
		___elementDecls_5 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_5), value);
	}

	inline static int32_t get_offset_of_attListDecls_6() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___attListDecls_6)); }
	inline DTDAttListDeclarationCollection_t2220366188 * get_attListDecls_6() const { return ___attListDecls_6; }
	inline DTDAttListDeclarationCollection_t2220366188 ** get_address_of_attListDecls_6() { return &___attListDecls_6; }
	inline void set_attListDecls_6(DTDAttListDeclarationCollection_t2220366188 * value)
	{
		___attListDecls_6 = value;
		Il2CppCodeGenWriteBarrier((&___attListDecls_6), value);
	}

	inline static int32_t get_offset_of_peDecls_7() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___peDecls_7)); }
	inline DTDParameterEntityDeclarationCollection_t2844734410 * get_peDecls_7() const { return ___peDecls_7; }
	inline DTDParameterEntityDeclarationCollection_t2844734410 ** get_address_of_peDecls_7() { return &___peDecls_7; }
	inline void set_peDecls_7(DTDParameterEntityDeclarationCollection_t2844734410 * value)
	{
		___peDecls_7 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_7), value);
	}

	inline static int32_t get_offset_of_entityDecls_8() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___entityDecls_8)); }
	inline DTDEntityDeclarationCollection_t2250844513 * get_entityDecls_8() const { return ___entityDecls_8; }
	inline DTDEntityDeclarationCollection_t2250844513 ** get_address_of_entityDecls_8() { return &___entityDecls_8; }
	inline void set_entityDecls_8(DTDEntityDeclarationCollection_t2250844513 * value)
	{
		___entityDecls_8 = value;
		Il2CppCodeGenWriteBarrier((&___entityDecls_8), value);
	}

	inline static int32_t get_offset_of_notationDecls_9() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___notationDecls_9)); }
	inline DTDNotationDeclarationCollection_t959292105 * get_notationDecls_9() const { return ___notationDecls_9; }
	inline DTDNotationDeclarationCollection_t959292105 ** get_address_of_notationDecls_9() { return &___notationDecls_9; }
	inline void set_notationDecls_9(DTDNotationDeclarationCollection_t959292105 * value)
	{
		___notationDecls_9 = value;
		Il2CppCodeGenWriteBarrier((&___notationDecls_9), value);
	}

	inline static int32_t get_offset_of_validationErrors_10() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___validationErrors_10)); }
	inline ArrayList_t2718874744 * get_validationErrors_10() const { return ___validationErrors_10; }
	inline ArrayList_t2718874744 ** get_address_of_validationErrors_10() { return &___validationErrors_10; }
	inline void set_validationErrors_10(ArrayList_t2718874744 * value)
	{
		___validationErrors_10 = value;
		Il2CppCodeGenWriteBarrier((&___validationErrors_10), value);
	}

	inline static int32_t get_offset_of_resolver_11() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___resolver_11)); }
	inline XmlResolver_t626023767 * get_resolver_11() const { return ___resolver_11; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_11() { return &___resolver_11; }
	inline void set_resolver_11(XmlResolver_t626023767 * value)
	{
		___resolver_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___nameTable_12)); }
	inline XmlNameTable_t71772148 * get_nameTable_12() const { return ___nameTable_12; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(XmlNameTable_t71772148 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}

	inline static int32_t get_offset_of_externalResources_13() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___externalResources_13)); }
	inline Hashtable_t1853889766 * get_externalResources_13() const { return ___externalResources_13; }
	inline Hashtable_t1853889766 ** get_address_of_externalResources_13() { return &___externalResources_13; }
	inline void set_externalResources_13(Hashtable_t1853889766 * value)
	{
		___externalResources_13 = value;
		Il2CppCodeGenWriteBarrier((&___externalResources_13), value);
	}

	inline static int32_t get_offset_of_baseURI_14() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___baseURI_14)); }
	inline String_t* get_baseURI_14() const { return ___baseURI_14; }
	inline String_t** get_address_of_baseURI_14() { return &___baseURI_14; }
	inline void set_baseURI_14(String_t* value)
	{
		___baseURI_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_14), value);
	}

	inline static int32_t get_offset_of_name_15() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___name_15)); }
	inline String_t* get_name_15() const { return ___name_15; }
	inline String_t** get_address_of_name_15() { return &___name_15; }
	inline void set_name_15(String_t* value)
	{
		___name_15 = value;
		Il2CppCodeGenWriteBarrier((&___name_15), value);
	}

	inline static int32_t get_offset_of_publicId_16() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___publicId_16)); }
	inline String_t* get_publicId_16() const { return ___publicId_16; }
	inline String_t** get_address_of_publicId_16() { return &___publicId_16; }
	inline void set_publicId_16(String_t* value)
	{
		___publicId_16 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_16), value);
	}

	inline static int32_t get_offset_of_systemId_17() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___systemId_17)); }
	inline String_t* get_systemId_17() const { return ___systemId_17; }
	inline String_t** get_address_of_systemId_17() { return &___systemId_17; }
	inline void set_systemId_17(String_t* value)
	{
		___systemId_17 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_17), value);
	}

	inline static int32_t get_offset_of_intSubset_18() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___intSubset_18)); }
	inline String_t* get_intSubset_18() const { return ___intSubset_18; }
	inline String_t** get_address_of_intSubset_18() { return &___intSubset_18; }
	inline void set_intSubset_18(String_t* value)
	{
		___intSubset_18 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_18), value);
	}

	inline static int32_t get_offset_of_intSubsetHasPERef_19() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___intSubsetHasPERef_19)); }
	inline bool get_intSubsetHasPERef_19() const { return ___intSubsetHasPERef_19; }
	inline bool* get_address_of_intSubsetHasPERef_19() { return &___intSubsetHasPERef_19; }
	inline void set_intSubsetHasPERef_19(bool value)
	{
		___intSubsetHasPERef_19 = value;
	}

	inline static int32_t get_offset_of_isStandalone_20() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___isStandalone_20)); }
	inline bool get_isStandalone_20() const { return ___isStandalone_20; }
	inline bool* get_address_of_isStandalone_20() { return &___isStandalone_20; }
	inline void set_isStandalone_20(bool value)
	{
		___isStandalone_20 = value;
	}

	inline static int32_t get_offset_of_lineNumber_21() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___lineNumber_21)); }
	inline int32_t get_lineNumber_21() const { return ___lineNumber_21; }
	inline int32_t* get_address_of_lineNumber_21() { return &___lineNumber_21; }
	inline void set_lineNumber_21(int32_t value)
	{
		___lineNumber_21 = value;
	}

	inline static int32_t get_offset_of_linePosition_22() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1729680289, ___linePosition_22)); }
	inline int32_t get_linePosition_22() const { return ___linePosition_22; }
	inline int32_t* get_address_of_linePosition_22() { return &___linePosition_22; }
	inline void set_linePosition_22(int32_t value)
	{
		___linePosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOBJECTMODEL_T1729680289_H
#ifndef XMLNODELIST_T2551693786_H
#define XMLNODELIST_T2551693786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_t2551693786  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_T2551693786_H
#ifndef ENTRY_T3052280359_H
#define ENTRY_T3052280359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_t3052280359  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hash
	int32_t ___hash_1;
	// System.Int32 System.Xml.NameTable/Entry::len
	int32_t ___len_2;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_t3052280359 * ___next_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(Entry_t3052280359, ___next_3)); }
	inline Entry_t3052280359 * get_next_3() const { return ___next_3; }
	inline Entry_t3052280359 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(Entry_t3052280359 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3052280359_H
#ifndef XMLNODE_T3767805227_H
#define XMLNODE_T3767805227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t3767805227  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t3767805227 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t1082692789 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___ownerDocument_1)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___parentNode_2)); }
	inline XmlNode_t3767805227 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t3767805227 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t3767805227 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227, ___childNodes_3)); }
	inline XmlNodeListChildren_t1082692789 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t1082692789 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t1082692789 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t3767805227_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t139615908 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map44_4;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t139615908 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t139615908 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t139615908 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t3767805227_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T3767805227_H
#ifndef DTDREADER_T386081180_H
#define DTDREADER_T386081180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DTDReader
struct  DTDReader_t386081180  : public RuntimeObject
{
public:
	// System.Xml.XmlParserInput System.Xml.DTDReader::currentInput
	XmlParserInput_t2182411204 * ___currentInput_0;
	// System.Collections.Stack System.Xml.DTDReader::parserInputStack
	Stack_t2329662280 * ___parserInputStack_1;
	// System.Char[] System.Xml.DTDReader::nameBuffer
	CharU5BU5D_t3528271667* ___nameBuffer_2;
	// System.Int32 System.Xml.DTDReader::nameLength
	int32_t ___nameLength_3;
	// System.Int32 System.Xml.DTDReader::nameCapacity
	int32_t ___nameCapacity_4;
	// System.Text.StringBuilder System.Xml.DTDReader::valueBuffer
	StringBuilder_t * ___valueBuffer_5;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_6;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_7;
	// System.Int32 System.Xml.DTDReader::dtdIncludeSect
	int32_t ___dtdIncludeSect_8;
	// System.Boolean System.Xml.DTDReader::normalization
	bool ___normalization_9;
	// System.Boolean System.Xml.DTDReader::processingInternalSubset
	bool ___processingInternalSubset_10;
	// System.String System.Xml.DTDReader::cachedPublicId
	String_t* ___cachedPublicId_11;
	// System.String System.Xml.DTDReader::cachedSystemId
	String_t* ___cachedSystemId_12;
	// Mono.Xml.DTDObjectModel System.Xml.DTDReader::DTD
	DTDObjectModel_t1729680289 * ___DTD_13;

public:
	inline static int32_t get_offset_of_currentInput_0() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentInput_0)); }
	inline XmlParserInput_t2182411204 * get_currentInput_0() const { return ___currentInput_0; }
	inline XmlParserInput_t2182411204 ** get_address_of_currentInput_0() { return &___currentInput_0; }
	inline void set_currentInput_0(XmlParserInput_t2182411204 * value)
	{
		___currentInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentInput_0), value);
	}

	inline static int32_t get_offset_of_parserInputStack_1() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___parserInputStack_1)); }
	inline Stack_t2329662280 * get_parserInputStack_1() const { return ___parserInputStack_1; }
	inline Stack_t2329662280 ** get_address_of_parserInputStack_1() { return &___parserInputStack_1; }
	inline void set_parserInputStack_1(Stack_t2329662280 * value)
	{
		___parserInputStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___parserInputStack_1), value);
	}

	inline static int32_t get_offset_of_nameBuffer_2() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameBuffer_2)); }
	inline CharU5BU5D_t3528271667* get_nameBuffer_2() const { return ___nameBuffer_2; }
	inline CharU5BU5D_t3528271667** get_address_of_nameBuffer_2() { return &___nameBuffer_2; }
	inline void set_nameBuffer_2(CharU5BU5D_t3528271667* value)
	{
		___nameBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameBuffer_2), value);
	}

	inline static int32_t get_offset_of_nameLength_3() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameLength_3)); }
	inline int32_t get_nameLength_3() const { return ___nameLength_3; }
	inline int32_t* get_address_of_nameLength_3() { return &___nameLength_3; }
	inline void set_nameLength_3(int32_t value)
	{
		___nameLength_3 = value;
	}

	inline static int32_t get_offset_of_nameCapacity_4() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___nameCapacity_4)); }
	inline int32_t get_nameCapacity_4() const { return ___nameCapacity_4; }
	inline int32_t* get_address_of_nameCapacity_4() { return &___nameCapacity_4; }
	inline void set_nameCapacity_4(int32_t value)
	{
		___nameCapacity_4 = value;
	}

	inline static int32_t get_offset_of_valueBuffer_5() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___valueBuffer_5)); }
	inline StringBuilder_t * get_valueBuffer_5() const { return ___valueBuffer_5; }
	inline StringBuilder_t ** get_address_of_valueBuffer_5() { return &___valueBuffer_5; }
	inline void set_valueBuffer_5(StringBuilder_t * value)
	{
		___valueBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_5), value);
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_6() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentLinkedNodeLineNumber_6)); }
	inline int32_t get_currentLinkedNodeLineNumber_6() const { return ___currentLinkedNodeLineNumber_6; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_6() { return &___currentLinkedNodeLineNumber_6; }
	inline void set_currentLinkedNodeLineNumber_6(int32_t value)
	{
		___currentLinkedNodeLineNumber_6 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_7() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___currentLinkedNodeLinePosition_7)); }
	inline int32_t get_currentLinkedNodeLinePosition_7() const { return ___currentLinkedNodeLinePosition_7; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_7() { return &___currentLinkedNodeLinePosition_7; }
	inline void set_currentLinkedNodeLinePosition_7(int32_t value)
	{
		___currentLinkedNodeLinePosition_7 = value;
	}

	inline static int32_t get_offset_of_dtdIncludeSect_8() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___dtdIncludeSect_8)); }
	inline int32_t get_dtdIncludeSect_8() const { return ___dtdIncludeSect_8; }
	inline int32_t* get_address_of_dtdIncludeSect_8() { return &___dtdIncludeSect_8; }
	inline void set_dtdIncludeSect_8(int32_t value)
	{
		___dtdIncludeSect_8 = value;
	}

	inline static int32_t get_offset_of_normalization_9() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___normalization_9)); }
	inline bool get_normalization_9() const { return ___normalization_9; }
	inline bool* get_address_of_normalization_9() { return &___normalization_9; }
	inline void set_normalization_9(bool value)
	{
		___normalization_9 = value;
	}

	inline static int32_t get_offset_of_processingInternalSubset_10() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___processingInternalSubset_10)); }
	inline bool get_processingInternalSubset_10() const { return ___processingInternalSubset_10; }
	inline bool* get_address_of_processingInternalSubset_10() { return &___processingInternalSubset_10; }
	inline void set_processingInternalSubset_10(bool value)
	{
		___processingInternalSubset_10 = value;
	}

	inline static int32_t get_offset_of_cachedPublicId_11() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___cachedPublicId_11)); }
	inline String_t* get_cachedPublicId_11() const { return ___cachedPublicId_11; }
	inline String_t** get_address_of_cachedPublicId_11() { return &___cachedPublicId_11; }
	inline void set_cachedPublicId_11(String_t* value)
	{
		___cachedPublicId_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPublicId_11), value);
	}

	inline static int32_t get_offset_of_cachedSystemId_12() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___cachedSystemId_12)); }
	inline String_t* get_cachedSystemId_12() const { return ___cachedSystemId_12; }
	inline String_t** get_address_of_cachedSystemId_12() { return &___cachedSystemId_12; }
	inline void set_cachedSystemId_12(String_t* value)
	{
		___cachedSystemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedSystemId_12), value);
	}

	inline static int32_t get_offset_of_DTD_13() { return static_cast<int32_t>(offsetof(DTDReader_t386081180, ___DTD_13)); }
	inline DTDObjectModel_t1729680289 * get_DTD_13() const { return ___DTD_13; }
	inline DTDObjectModel_t1729680289 ** get_address_of_DTD_13() { return &___DTD_13; }
	inline void set_DTD_13(DTDObjectModel_t1729680289 * value)
	{
		___DTD_13 = value;
		Il2CppCodeGenWriteBarrier((&___DTD_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDREADER_T386081180_H
#ifndef XMLCHAR_T3816087079_H
#define XMLCHAR_T3816087079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t3816087079  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t3816087079_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t3528271667* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t4116647657* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t4116647657* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_t2770800703* ___nameBitmap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlChar::<>f__switch$map47
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map47_4;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t3528271667* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t3528271667* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t4116647657* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t4116647657* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t4116647657* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t4116647657* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_t2770800703* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_t2770800703** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_t2770800703* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map47_4() { return static_cast<int32_t>(offsetof(XmlChar_t3816087079_StaticFields, ___U3CU3Ef__switchU24map47_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map47_4() const { return ___U3CU3Ef__switchU24map47_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map47_4() { return &___U3CU3Ef__switchU24map47_4; }
	inline void set_U3CU3Ef__switchU24map47_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map47_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map47_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T3816087079_H
#ifndef XQUERYCONVERT_T2841811029_H
#define XQUERYCONVERT_T2841811029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XQueryConvert
struct  XQueryConvert_t2841811029  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XQUERYCONVERT_T2841811029_H
#ifndef ATTRIBUTESLOT_T3985135163_H
#define ATTRIBUTESLOT_T3985135163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader/AttributeSlot
struct  AttributeSlot_t3985135163  : public RuntimeObject
{
public:
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Name
	String_t* ___Name_0;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::NS
	String_t* ___NS_2;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Prefix
	String_t* ___Prefix_3;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Value
	String_t* ___Value_4;
	// System.Boolean Mono.Xml.DTDValidatingReader/AttributeSlot::IsDefault
	bool ___IsDefault_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Prefix_3() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___Prefix_3)); }
	inline String_t* get_Prefix_3() const { return ___Prefix_3; }
	inline String_t** get_address_of_Prefix_3() { return &___Prefix_3; }
	inline void set_Prefix_3(String_t* value)
	{
		___Prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_3), value);
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___Value_4)); }
	inline String_t* get_Value_4() const { return ___Value_4; }
	inline String_t** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(String_t* value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_IsDefault_5() { return static_cast<int32_t>(offsetof(AttributeSlot_t3985135163, ___IsDefault_5)); }
	inline bool get_IsDefault_5() const { return ___IsDefault_5; }
	inline bool* get_address_of_IsDefault_5() { return &___IsDefault_5; }
	inline void set_IsDefault_5(bool value)
	{
		___IsDefault_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTESLOT_T3985135163_H
#ifndef DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#define DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct  DTDParameterEntityDeclarationCollection_t2844734410  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.DTDParameterEntityDeclarationCollection::peDecls
	Hashtable_t1853889766 * ___peDecls_0;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDParameterEntityDeclarationCollection::root
	DTDObjectModel_t1729680289 * ___root_1;

public:
	inline static int32_t get_offset_of_peDecls_0() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t2844734410, ___peDecls_0)); }
	inline Hashtable_t1853889766 * get_peDecls_0() const { return ___peDecls_0; }
	inline Hashtable_t1853889766 ** get_address_of_peDecls_0() { return &___peDecls_0; }
	inline void set_peDecls_0(Hashtable_t1853889766 * value)
	{
		___peDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t2844734410, ___root_1)); }
	inline DTDObjectModel_t1729680289 * get_root_1() const { return ___root_1; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(DTDObjectModel_t1729680289 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATIONCOLLECTION_T2844734410_H
#ifndef DTDNODE_T858560093_H
#define DTDNODE_T858560093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNode
struct  DTDNode_t858560093  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::root
	DTDObjectModel_t1729680289 * ___root_0;
	// System.Boolean Mono.Xml.DTDNode::isInternalSubset
	bool ___isInternalSubset_1;
	// System.String Mono.Xml.DTDNode::baseURI
	String_t* ___baseURI_2;
	// System.Int32 Mono.Xml.DTDNode::lineNumber
	int32_t ___lineNumber_3;
	// System.Int32 Mono.Xml.DTDNode::linePosition
	int32_t ___linePosition_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___root_0)); }
	inline DTDObjectModel_t1729680289 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1729680289 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_isInternalSubset_1() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___isInternalSubset_1)); }
	inline bool get_isInternalSubset_1() const { return ___isInternalSubset_1; }
	inline bool* get_address_of_isInternalSubset_1() { return &___isInternalSubset_1; }
	inline void set_isInternalSubset_1(bool value)
	{
		___isInternalSubset_1 = value;
	}

	inline static int32_t get_offset_of_baseURI_2() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___baseURI_2)); }
	inline String_t* get_baseURI_2() const { return ___baseURI_2; }
	inline String_t** get_address_of_baseURI_2() { return &___baseURI_2; }
	inline void set_baseURI_2(String_t* value)
	{
		___baseURI_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_2), value);
	}

	inline static int32_t get_offset_of_lineNumber_3() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___lineNumber_3)); }
	inline int32_t get_lineNumber_3() const { return ___lineNumber_3; }
	inline int32_t* get_address_of_lineNumber_3() { return &___lineNumber_3; }
	inline void set_lineNumber_3(int32_t value)
	{
		___lineNumber_3 = value;
	}

	inline static int32_t get_offset_of_linePosition_4() { return static_cast<int32_t>(offsetof(DTDNode_t858560093, ___linePosition_4)); }
	inline int32_t get_linePosition_4() const { return ___linePosition_4; }
	inline int32_t* get_address_of_linePosition_4() { return &___linePosition_4; }
	inline void set_linePosition_4(int32_t value)
	{
		___linePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNODE_T858560093_H
#ifndef DTDCONTENTMODELCOLLECTION_T2798820000_H
#define DTDCONTENTMODELCOLLECTION_T2798820000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t2798820000  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t2718874744 * ___contentModel_0;

public:
	inline static int32_t get_offset_of_contentModel_0() { return static_cast<int32_t>(offsetof(DTDContentModelCollection_t2798820000, ___contentModel_0)); }
	inline ArrayList_t2718874744 * get_contentModel_0() const { return ___contentModel_0; }
	inline ArrayList_t2718874744 ** get_address_of_contentModel_0() { return &___contentModel_0; }
	inline void set_contentModel_0(ArrayList_t2718874744 * value)
	{
		___contentModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODELCOLLECTION_T2798820000_H
#ifndef XMLIMPLEMENTATION_T254178875_H
#define XMLIMPLEMENTATION_T254178875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t254178875  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_t71772148 * ___InternalNameTable_0;

public:
	inline static int32_t get_offset_of_InternalNameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t254178875, ___InternalNameTable_0)); }
	inline XmlNameTable_t71772148 * get_InternalNameTable_0() const { return ___InternalNameTable_0; }
	inline XmlNameTable_t71772148 ** get_address_of_InternalNameTable_0() { return &___InternalNameTable_0; }
	inline void set_InternalNameTable_0(XmlNameTable_t71772148 * value)
	{
		___InternalNameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___InternalNameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T254178875_H
#ifndef XMLNAMEENTRY_T1073099671_H
#define XMLNAMEENTRY_T1073099671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntry
struct  XmlNameEntry_t1073099671  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlNameEntry::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNameEntry::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlNameEntry::NS
	String_t* ___NS_2;
	// System.Int32 System.Xml.XmlNameEntry::Hash
	int32_t ___Hash_3;
	// System.String System.Xml.XmlNameEntry::prefixed_name_cache
	String_t* ___prefixed_name_cache_4;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Hash_3() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___Hash_3)); }
	inline int32_t get_Hash_3() const { return ___Hash_3; }
	inline int32_t* get_address_of_Hash_3() { return &___Hash_3; }
	inline void set_Hash_3(int32_t value)
	{
		___Hash_3 = value;
	}

	inline static int32_t get_offset_of_prefixed_name_cache_4() { return static_cast<int32_t>(offsetof(XmlNameEntry_t1073099671, ___prefixed_name_cache_4)); }
	inline String_t* get_prefixed_name_cache_4() const { return ___prefixed_name_cache_4; }
	inline String_t** get_address_of_prefixed_name_cache_4() { return &___prefixed_name_cache_4; }
	inline void set_prefixed_name_cache_4(String_t* value)
	{
		___prefixed_name_cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefixed_name_cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRY_T1073099671_H
#ifndef ENUMERATOR_T97922292_H
#define ENUMERATOR_T97922292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren/Enumerator
struct  Enumerator_t97922292  : public RuntimeObject
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren/Enumerator::parent
	RuntimeObject* ___parent_0;
	// System.Xml.XmlLinkedNode System.Xml.XmlNodeListChildren/Enumerator::currentChild
	XmlLinkedNode_t1437094927 * ___currentChild_1;
	// System.Boolean System.Xml.XmlNodeListChildren/Enumerator::passedLastNode
	bool ___passedLastNode_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_currentChild_1() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___currentChild_1)); }
	inline XmlLinkedNode_t1437094927 * get_currentChild_1() const { return ___currentChild_1; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_currentChild_1() { return &___currentChild_1; }
	inline void set_currentChild_1(XmlLinkedNode_t1437094927 * value)
	{
		___currentChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChild_1), value);
	}

	inline static int32_t get_offset_of_passedLastNode_2() { return static_cast<int32_t>(offsetof(Enumerator_t97922292, ___passedLastNode_2)); }
	inline bool get_passedLastNode_2() const { return ___passedLastNode_2; }
	inline bool* get_address_of_passedLastNode_2() { return &___passedLastNode_2; }
	inline void set_passedLastNode_2(bool value)
	{
		___passedLastNode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T97922292_H
#ifndef LIST_1_T218596005_H
#define LIST_1_T218596005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  List_1_t218596005  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t3368185270* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t3368185270* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t3368185270** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t3368185270* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t218596005, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t218596005_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t3368185270* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t218596005_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t3368185270* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t3368185270** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t3368185270* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T218596005_H
#ifndef TEXTREADER_T283511965_H
#define TEXTREADER_T283511965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t283511965  : public RuntimeObject
{
public:

public:
};

struct TextReader_t283511965_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t283511965 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t283511965_StaticFields, ___Null_0)); }
	inline TextReader_t283511965 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t283511965 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t283511965 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T283511965_H
#ifndef DTDAUTOMATAFACTORY_T2958275022_H
#define DTDAUTOMATAFACTORY_T2958275022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomataFactory
struct  DTDAutomataFactory_t2958275022  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomataFactory::root
	DTDObjectModel_t1729680289 * ___root_0;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::choiceTable
	Hashtable_t1853889766 * ___choiceTable_1;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::sequenceTable
	Hashtable_t1853889766 * ___sequenceTable_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___root_0)); }
	inline DTDObjectModel_t1729680289 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1729680289 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_choiceTable_1() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___choiceTable_1)); }
	inline Hashtable_t1853889766 * get_choiceTable_1() const { return ___choiceTable_1; }
	inline Hashtable_t1853889766 ** get_address_of_choiceTable_1() { return &___choiceTable_1; }
	inline void set_choiceTable_1(Hashtable_t1853889766 * value)
	{
		___choiceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceTable_1), value);
	}

	inline static int32_t get_offset_of_sequenceTable_2() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t2958275022, ___sequenceTable_2)); }
	inline Hashtable_t1853889766 * get_sequenceTable_2() const { return ___sequenceTable_2; }
	inline Hashtable_t1853889766 ** get_address_of_sequenceTable_2() { return &___sequenceTable_2; }
	inline void set_sequenceTable_2(Hashtable_t1853889766 * value)
	{
		___sequenceTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATAFACTORY_T2958275022_H
#ifndef XMLSERIALIZERNAMESPACES_T2702737953_H
#define XMLSERIALIZERNAMESPACES_T2702737953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t2702737953  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_t1624492310 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t2702737953, ___namespaces_0)); }
	inline ListDictionary_t1624492310 * get_namespaces_0() const { return ___namespaces_0; }
	inline ListDictionary_t1624492310 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(ListDictionary_t1624492310 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T2702737953_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_0)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_1)); }
	inline XmlReaderSettings_t2186285234 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t2186285234 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef XMLPARSERINPUT_T2182411204_H
#define XMLPARSERINPUT_T2182411204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput
struct  XmlParserInput_t2182411204  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Xml.XmlParserInput::sourceStack
	Stack_t2329662280 * ___sourceStack_0;
	// System.Xml.XmlParserInput/XmlParserInputSource System.Xml.XmlParserInput::source
	XmlParserInputSource_t3533005609 * ___source_1;
	// System.Boolean System.Xml.XmlParserInput::has_peek
	bool ___has_peek_2;
	// System.Int32 System.Xml.XmlParserInput::peek_char
	int32_t ___peek_char_3;
	// System.Boolean System.Xml.XmlParserInput::allowTextDecl
	bool ___allowTextDecl_4;

public:
	inline static int32_t get_offset_of_sourceStack_0() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___sourceStack_0)); }
	inline Stack_t2329662280 * get_sourceStack_0() const { return ___sourceStack_0; }
	inline Stack_t2329662280 ** get_address_of_sourceStack_0() { return &___sourceStack_0; }
	inline void set_sourceStack_0(Stack_t2329662280 * value)
	{
		___sourceStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceStack_0), value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___source_1)); }
	inline XmlParserInputSource_t3533005609 * get_source_1() const { return ___source_1; }
	inline XmlParserInputSource_t3533005609 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(XmlParserInputSource_t3533005609 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_has_peek_2() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___has_peek_2)); }
	inline bool get_has_peek_2() const { return ___has_peek_2; }
	inline bool* get_address_of_has_peek_2() { return &___has_peek_2; }
	inline void set_has_peek_2(bool value)
	{
		___has_peek_2 = value;
	}

	inline static int32_t get_offset_of_peek_char_3() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___peek_char_3)); }
	inline int32_t get_peek_char_3() const { return ___peek_char_3; }
	inline int32_t* get_address_of_peek_char_3() { return &___peek_char_3; }
	inline void set_peek_char_3(int32_t value)
	{
		___peek_char_3 = value;
	}

	inline static int32_t get_offset_of_allowTextDecl_4() { return static_cast<int32_t>(offsetof(XmlParserInput_t2182411204, ___allowTextDecl_4)); }
	inline bool get_allowTextDecl_4() const { return ___allowTextDecl_4; }
	inline bool* get_address_of_allowTextDecl_4() { return &___allowTextDecl_4; }
	inline void set_allowTextDecl_4(bool value)
	{
		___allowTextDecl_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUT_T2182411204_H
#ifndef XPATHITEM_T4250588140_H
#define XPATHITEM_T4250588140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t4250588140  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T4250588140_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef DTDAUTOMATA_T781538777_H
#define DTDAUTOMATA_T781538777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomata
struct  DTDAutomata_t781538777  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomata::root
	DTDObjectModel_t1729680289 * ___root_0;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomata_t781538777, ___root_0)); }
	inline DTDObjectModel_t1729680289 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1729680289 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATA_T781538777_H
#ifndef XMLDOCUMENTFRAGMENT_T1323348855_H
#define XMLDOCUMENTFRAGMENT_T1323348855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t1323348855  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_5;

public:
	inline static int32_t get_offset_of_lastLinkedChild_5() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t1323348855, ___lastLinkedChild_5)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_5() const { return ___lastLinkedChild_5; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_5() { return &___lastLinkedChild_5; }
	inline void set_lastLinkedChild_5(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T1323348855_H
#ifndef XMLDOCUMENT_T2837193595_H
#define XMLDOCUMENT_T2837193595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t2837193595  : public XmlNode_t3767805227
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_6;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_7;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_t71772148 * ___nameTable_8;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_9;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t254178875 * ___implementation_10;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_11;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t626023767 * ___resolver_12;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_t1853889766 * ___idTable_13;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t2890546907 * ___nameCache_14;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_15;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	RuntimeObject* ___schemaInfo_16;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_17;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_t1533444722 * ___NodeChanged_18;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_t1533444722 * ___NodeChanging_19;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_t1533444722 * ___NodeInserted_20;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_t1533444722 * ___NodeInserting_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_t1533444722 * ___NodeRemoved_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_t1533444722 * ___NodeRemoving_23;

public:
	inline static int32_t get_offset_of_optimal_create_element_6() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___optimal_create_element_6)); }
	inline bool get_optimal_create_element_6() const { return ___optimal_create_element_6; }
	inline bool* get_address_of_optimal_create_element_6() { return &___optimal_create_element_6; }
	inline void set_optimal_create_element_6(bool value)
	{
		___optimal_create_element_6 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_7() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___optimal_create_attribute_7)); }
	inline bool get_optimal_create_attribute_7() const { return ___optimal_create_attribute_7; }
	inline bool* get_address_of_optimal_create_attribute_7() { return &___optimal_create_attribute_7; }
	inline void set_optimal_create_attribute_7(bool value)
	{
		___optimal_create_attribute_7 = value;
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___nameTable_8)); }
	inline XmlNameTable_t71772148 * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_t71772148 * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_baseURI_9() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___baseURI_9)); }
	inline String_t* get_baseURI_9() const { return ___baseURI_9; }
	inline String_t** get_address_of_baseURI_9() { return &___baseURI_9; }
	inline void set_baseURI_9(String_t* value)
	{
		___baseURI_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_9), value);
	}

	inline static int32_t get_offset_of_implementation_10() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___implementation_10)); }
	inline XmlImplementation_t254178875 * get_implementation_10() const { return ___implementation_10; }
	inline XmlImplementation_t254178875 ** get_address_of_implementation_10() { return &___implementation_10; }
	inline void set_implementation_10(XmlImplementation_t254178875 * value)
	{
		___implementation_10 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_10), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_11() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___preserveWhitespace_11)); }
	inline bool get_preserveWhitespace_11() const { return ___preserveWhitespace_11; }
	inline bool* get_address_of_preserveWhitespace_11() { return &___preserveWhitespace_11; }
	inline void set_preserveWhitespace_11(bool value)
	{
		___preserveWhitespace_11 = value;
	}

	inline static int32_t get_offset_of_resolver_12() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___resolver_12)); }
	inline XmlResolver_t626023767 * get_resolver_12() const { return ___resolver_12; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_12() { return &___resolver_12; }
	inline void set_resolver_12(XmlResolver_t626023767 * value)
	{
		___resolver_12 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_12), value);
	}

	inline static int32_t get_offset_of_idTable_13() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___idTable_13)); }
	inline Hashtable_t1853889766 * get_idTable_13() const { return ___idTable_13; }
	inline Hashtable_t1853889766 ** get_address_of_idTable_13() { return &___idTable_13; }
	inline void set_idTable_13(Hashtable_t1853889766 * value)
	{
		___idTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___idTable_13), value);
	}

	inline static int32_t get_offset_of_nameCache_14() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___nameCache_14)); }
	inline XmlNameEntryCache_t2890546907 * get_nameCache_14() const { return ___nameCache_14; }
	inline XmlNameEntryCache_t2890546907 ** get_address_of_nameCache_14() { return &___nameCache_14; }
	inline void set_nameCache_14(XmlNameEntryCache_t2890546907 * value)
	{
		___nameCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_14), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_15() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___lastLinkedChild_15)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_15() const { return ___lastLinkedChild_15; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_15() { return &___lastLinkedChild_15; }
	inline void set_lastLinkedChild_15(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_15 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_15), value);
	}

	inline static int32_t get_offset_of_schemaInfo_16() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___schemaInfo_16)); }
	inline RuntimeObject* get_schemaInfo_16() const { return ___schemaInfo_16; }
	inline RuntimeObject** get_address_of_schemaInfo_16() { return &___schemaInfo_16; }
	inline void set_schemaInfo_16(RuntimeObject* value)
	{
		___schemaInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_16), value);
	}

	inline static int32_t get_offset_of_loadMode_17() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___loadMode_17)); }
	inline bool get_loadMode_17() const { return ___loadMode_17; }
	inline bool* get_address_of_loadMode_17() { return &___loadMode_17; }
	inline void set_loadMode_17(bool value)
	{
		___loadMode_17 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_18() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeChanged_18)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeChanged_18() const { return ___NodeChanged_18; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeChanged_18() { return &___NodeChanged_18; }
	inline void set_NodeChanged_18(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanged_18), value);
	}

	inline static int32_t get_offset_of_NodeChanging_19() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeChanging_19)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeChanging_19() const { return ___NodeChanging_19; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeChanging_19() { return &___NodeChanging_19; }
	inline void set_NodeChanging_19(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeChanging_19 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanging_19), value);
	}

	inline static int32_t get_offset_of_NodeInserted_20() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeInserted_20)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeInserted_20() const { return ___NodeInserted_20; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeInserted_20() { return &___NodeInserted_20; }
	inline void set_NodeInserted_20(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeInserted_20 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserted_20), value);
	}

	inline static int32_t get_offset_of_NodeInserting_21() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeInserting_21)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeInserting_21() const { return ___NodeInserting_21; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeInserting_21() { return &___NodeInserting_21; }
	inline void set_NodeInserting_21(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeInserting_21 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserting_21), value);
	}

	inline static int32_t get_offset_of_NodeRemoved_22() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeRemoved_22)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeRemoved_22() const { return ___NodeRemoved_22; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeRemoved_22() { return &___NodeRemoved_22; }
	inline void set_NodeRemoved_22(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeRemoved_22 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoved_22), value);
	}

	inline static int32_t get_offset_of_NodeRemoving_23() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595, ___NodeRemoving_23)); }
	inline XmlNodeChangedEventHandler_t1533444722 * get_NodeRemoving_23() const { return ___NodeRemoving_23; }
	inline XmlNodeChangedEventHandler_t1533444722 ** get_address_of_NodeRemoving_23() { return &___NodeRemoving_23; }
	inline void set_NodeRemoving_23(XmlNodeChangedEventHandler_t1533444722 * value)
	{
		___NodeRemoving_23 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoving_23), value);
	}
};

struct XmlDocument_t2837193595_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_t3940880105* ___optimal_create_types_5;

public:
	inline static int32_t get_offset_of_optimal_create_types_5() { return static_cast<int32_t>(offsetof(XmlDocument_t2837193595_StaticFields, ___optimal_create_types_5)); }
	inline TypeU5BU5D_t3940880105* get_optimal_create_types_5() const { return ___optimal_create_types_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_optimal_create_types_5() { return &___optimal_create_types_5; }
	inline void set_optimal_create_types_5(TypeU5BU5D_t3940880105* value)
	{
		___optimal_create_types_5 = value;
		Il2CppCodeGenWriteBarrier((&___optimal_create_types_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T2837193595_H
#ifndef NSSCOPE_T3958624705_H
#define NSSCOPE_T3958624705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsScope
struct  NsScope_t3958624705 
{
public:
	// System.Int32 System.Xml.XmlNamespaceManager/NsScope::DeclCount
	int32_t ___DeclCount_0;
	// System.String System.Xml.XmlNamespaceManager/NsScope::DefaultNamespace
	String_t* ___DefaultNamespace_1;

public:
	inline static int32_t get_offset_of_DeclCount_0() { return static_cast<int32_t>(offsetof(NsScope_t3958624705, ___DeclCount_0)); }
	inline int32_t get_DeclCount_0() const { return ___DeclCount_0; }
	inline int32_t* get_address_of_DeclCount_0() { return &___DeclCount_0; }
	inline void set_DeclCount_0(int32_t value)
	{
		___DeclCount_0 = value;
	}

	inline static int32_t get_offset_of_DefaultNamespace_1() { return static_cast<int32_t>(offsetof(NsScope_t3958624705, ___DefaultNamespace_1)); }
	inline String_t* get_DefaultNamespace_1() const { return ___DefaultNamespace_1; }
	inline String_t** get_address_of_DefaultNamespace_1() { return &___DefaultNamespace_1; }
	inline void set_DefaultNamespace_1(String_t* value)
	{
		___DefaultNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultNamespace_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t3958624705_marshaled_pinvoke
{
	int32_t ___DeclCount_0;
	char* ___DefaultNamespace_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t3958624705_marshaled_com
{
	int32_t ___DeclCount_0;
	Il2CppChar* ___DefaultNamespace_1;
};
#endif // NSSCOPE_T3958624705_H
#ifndef XMLATTRIBUTE_T1173852259_H
#define XMLATTRIBUTE_T1173852259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t1173852259  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t1073099671 * ___name_5;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_7;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	RuntimeObject* ___schemaInfo_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___name_5)); }
	inline XmlNameEntry_t1073099671 * get_name_5() const { return ___name_5; }
	inline XmlNameEntry_t1073099671 ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XmlNameEntry_t1073099671 * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_isDefault_6() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___isDefault_6)); }
	inline bool get_isDefault_6() const { return ___isDefault_6; }
	inline bool* get_address_of_isDefault_6() { return &___isDefault_6; }
	inline void set_isDefault_6(bool value)
	{
		___isDefault_6 = value;
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}

	inline static int32_t get_offset_of_schemaInfo_8() { return static_cast<int32_t>(offsetof(XmlAttribute_t1173852259, ___schemaInfo_8)); }
	inline RuntimeObject* get_schemaInfo_8() const { return ___schemaInfo_8; }
	inline RuntimeObject** get_address_of_schemaInfo_8() { return &___schemaInfo_8; }
	inline void set_schemaInfo_8(RuntimeObject* value)
	{
		___schemaInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T1173852259_H
#ifndef XMLARRAYATTRIBUTE_T1484124842_H
#define XMLARRAYATTRIBUTE_T1484124842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlArrayAttribute
struct  XmlArrayAttribute_t1484124842  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlArrayAttribute::elementName
	String_t* ___elementName_0;
	// System.Int32 System.Xml.Serialization.XmlArrayAttribute::order
	int32_t ___order_1;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t1484124842, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t1484124842, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLARRAYATTRIBUTE_T1484124842_H
#ifndef XMLATTRIBUTECOLLECTION_T2316283784_H
#define XMLATTRIBUTECOLLECTION_T2316283784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t2316283784  : public XmlNamedNodeMap_t2821286253
{
public:
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t561603118 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t2837193595 * ___ownerDocument_5;

public:
	inline static int32_t get_offset_of_ownerElement_4() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t2316283784, ___ownerElement_4)); }
	inline XmlElement_t561603118 * get_ownerElement_4() const { return ___ownerElement_4; }
	inline XmlElement_t561603118 ** get_address_of_ownerElement_4() { return &___ownerElement_4; }
	inline void set_ownerElement_4(XmlElement_t561603118 * value)
	{
		___ownerElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElement_4), value);
	}

	inline static int32_t get_offset_of_ownerDocument_5() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t2316283784, ___ownerDocument_5)); }
	inline XmlDocument_t2837193595 * get_ownerDocument_5() const { return ___ownerDocument_5; }
	inline XmlDocument_t2837193595 ** get_address_of_ownerDocument_5() { return &___ownerDocument_5; }
	inline void set_ownerDocument_5(XmlDocument_t2837193595 * value)
	{
		___ownerDocument_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T2316283784_H
#ifndef NSDECL_T3938094415_H
#define NSDECL_T3938094415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NsDecl
struct  NsDecl_t3938094415 
{
public:
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Uri
	String_t* ___Uri_1;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(NsDecl_t3938094415, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_Uri_1() { return static_cast<int32_t>(offsetof(NsDecl_t3938094415, ___Uri_1)); }
	inline String_t* get_Uri_1() const { return ___Uri_1; }
	inline String_t** get_address_of_Uri_1() { return &___Uri_1; }
	inline void set_Uri_1(String_t* value)
	{
		___Uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t3938094415_marshaled_pinvoke
{
	char* ___Prefix_0;
	char* ___Uri_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t3938094415_marshaled_com
{
	Il2CppChar* ___Prefix_0;
	Il2CppChar* ___Uri_1;
};
#endif // NSDECL_T3938094415_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef EMPTYNODELIST_T139615908_H
#define EMPTYNODELIST_T139615908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode/EmptyNodeList
struct  EmptyNodeList_t139615908  : public XmlNodeList_t2551693786
{
public:

public:
};

struct EmptyNodeList_t139615908_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNode/EmptyNodeList::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(EmptyNodeList_t139615908_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYNODELIST_T139615908_H
#ifndef XMLNODELISTCHILDREN_T1082692789_H
#define XMLNODELISTCHILDREN_T1082692789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren
struct  XmlNodeListChildren_t1082692789  : public XmlNodeList_t2551693786
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren::parent
	RuntimeObject* ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNodeListChildren_t1082692789, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELISTCHILDREN_T1082692789_H
#ifndef XMLNOTATION_T1476580686_H
#define XMLNOTATION_T1476580686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t1476580686  : public XmlNode_t3767805227
{
public:
	// System.String System.Xml.XmlNotation::localName
	String_t* ___localName_5;
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_6;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_7;
	// System.String System.Xml.XmlNotation::prefix
	String_t* ___prefix_8;

public:
	inline static int32_t get_offset_of_localName_5() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___localName_5)); }
	inline String_t* get_localName_5() const { return ___localName_5; }
	inline String_t** get_address_of_localName_5() { return &___localName_5; }
	inline void set_localName_5(String_t* value)
	{
		___localName_5 = value;
		Il2CppCodeGenWriteBarrier((&___localName_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(XmlNotation_t1476580686, ___prefix_8)); }
	inline String_t* get_prefix_8() const { return ___prefix_8; }
	inline String_t** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(String_t* value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNOTATION_T1476580686_H
#ifndef KEYVALUEPAIR_2_T3041488559_H
#define KEYVALUEPAIR_2_T3041488559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t3041488559 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t858560093 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3041488559, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3041488559, ___value_1)); }
	inline DTDNode_t858560093 * get_value_1() const { return ___value_1; }
	inline DTDNode_t858560093 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(DTDNode_t858560093 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3041488559_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef XMLENTITY_T3308518401_H
#define XMLENTITY_T3308518401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t3308518401  : public XmlNode_t3767805227
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_5;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_6;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_7;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_8;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_9;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_10;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_NDATA_6() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___NDATA_6)); }
	inline String_t* get_NDATA_6() const { return ___NDATA_6; }
	inline String_t** get_address_of_NDATA_6() { return &___NDATA_6; }
	inline void set_NDATA_6(String_t* value)
	{
		___NDATA_6 = value;
		Il2CppCodeGenWriteBarrier((&___NDATA_6), value);
	}

	inline static int32_t get_offset_of_publicId_7() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___publicId_7)); }
	inline String_t* get_publicId_7() const { return ___publicId_7; }
	inline String_t** get_address_of_publicId_7() { return &___publicId_7; }
	inline void set_publicId_7(String_t* value)
	{
		___publicId_7 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_7), value);
	}

	inline static int32_t get_offset_of_systemId_8() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___systemId_8)); }
	inline String_t* get_systemId_8() const { return ___systemId_8; }
	inline String_t** get_address_of_systemId_8() { return &___systemId_8; }
	inline void set_systemId_8(String_t* value)
	{
		___systemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_8), value);
	}

	inline static int32_t get_offset_of_baseUri_9() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___baseUri_9)); }
	inline String_t* get_baseUri_9() const { return ___baseUri_9; }
	inline String_t** get_address_of_baseUri_9() { return &___baseUri_9; }
	inline void set_baseUri_9(String_t* value)
	{
		___baseUri_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_9), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_10() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___lastLinkedChild_10)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_10() const { return ___lastLinkedChild_10; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_10() { return &___lastLinkedChild_10; }
	inline void set_lastLinkedChild_10(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_10), value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_11() { return static_cast<int32_t>(offsetof(XmlEntity_t3308518401, ___contentAlreadySet_11)); }
	inline bool get_contentAlreadySet_11() const { return ___contentAlreadySet_11; }
	inline bool* get_address_of_contentAlreadySet_11() { return &___contentAlreadySet_11; }
	inline void set_contentAlreadySet_11(bool value)
	{
		___contentAlreadySet_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T3308518401_H
#ifndef NONBLOCKINGSTREAMREADER_T2495303928_H
#define NONBLOCKINGSTREAMREADER_T2495303928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NonBlockingStreamReader
struct  NonBlockingStreamReader_t2495303928  : public TextReader_t283511965
{
public:
	// System.Byte[] System.Xml.NonBlockingStreamReader::input_buffer
	ByteU5BU5D_t4116647657* ___input_buffer_1;
	// System.Char[] System.Xml.NonBlockingStreamReader::decoded_buffer
	CharU5BU5D_t3528271667* ___decoded_buffer_2;
	// System.Int32 System.Xml.NonBlockingStreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.Xml.NonBlockingStreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.Xml.NonBlockingStreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Text.Encoding System.Xml.NonBlockingStreamReader::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Text.Decoder System.Xml.NonBlockingStreamReader::decoder
	Decoder_t2204182725 * ___decoder_7;
	// System.IO.Stream System.Xml.NonBlockingStreamReader::base_stream
	Stream_t1273022909 * ___base_stream_8;
	// System.Boolean System.Xml.NonBlockingStreamReader::mayBlock
	bool ___mayBlock_9;
	// System.Text.StringBuilder System.Xml.NonBlockingStreamReader::line_builder
	StringBuilder_t * ___line_builder_10;
	// System.Boolean System.Xml.NonBlockingStreamReader::foundCR
	bool ___foundCR_11;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___input_buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoded_buffer_2)); }
	inline CharU5BU5D_t3528271667* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t3528271667** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t3528271667* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_decoder_7() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___decoder_7)); }
	inline Decoder_t2204182725 * get_decoder_7() const { return ___decoder_7; }
	inline Decoder_t2204182725 ** get_address_of_decoder_7() { return &___decoder_7; }
	inline void set_decoder_7(Decoder_t2204182725 * value)
	{
		___decoder_7 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_7), value);
	}

	inline static int32_t get_offset_of_base_stream_8() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___base_stream_8)); }
	inline Stream_t1273022909 * get_base_stream_8() const { return ___base_stream_8; }
	inline Stream_t1273022909 ** get_address_of_base_stream_8() { return &___base_stream_8; }
	inline void set_base_stream_8(Stream_t1273022909 * value)
	{
		___base_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_8), value);
	}

	inline static int32_t get_offset_of_mayBlock_9() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___mayBlock_9)); }
	inline bool get_mayBlock_9() const { return ___mayBlock_9; }
	inline bool* get_address_of_mayBlock_9() { return &___mayBlock_9; }
	inline void set_mayBlock_9(bool value)
	{
		___mayBlock_9 = value;
	}

	inline static int32_t get_offset_of_line_builder_10() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___line_builder_10)); }
	inline StringBuilder_t * get_line_builder_10() const { return ___line_builder_10; }
	inline StringBuilder_t ** get_address_of_line_builder_10() { return &___line_builder_10; }
	inline void set_line_builder_10(StringBuilder_t * value)
	{
		___line_builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_10), value);
	}

	inline static int32_t get_offset_of_foundCR_11() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t2495303928, ___foundCR_11)); }
	inline bool get_foundCR_11() const { return ___foundCR_11; }
	inline bool* get_address_of_foundCR_11() { return &___foundCR_11; }
	inline void set_foundCR_11(bool value)
	{
		___foundCR_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBLOCKINGSTREAMREADER_T2495303928_H
#ifndef XMLLINKEDNODE_T1437094927_H
#define XMLLINKEDNODE_T1437094927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1437094927  : public XmlNode_t3767805227
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1437094927 * ___nextSibling_5;

public:
	inline static int32_t get_offset_of_nextSibling_5() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1437094927, ___nextSibling_5)); }
	inline XmlLinkedNode_t1437094927 * get_nextSibling_5() const { return ___nextSibling_5; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_nextSibling_5() { return &___nextSibling_5; }
	inline void set_nextSibling_5(XmlLinkedNode_t1437094927 * value)
	{
		___nextSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1437094927_H
#ifndef XMLINPUTSTREAM_T1691369434_H
#define XMLINPUTSTREAM_T1691369434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlInputStream
struct  XmlInputStream_t1691369434  : public Stream_t1273022909
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t1523322056 * ___enc_2;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_t1273022909 * ___stream_3;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_4;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_5;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_6;

public:
	inline static int32_t get_offset_of_enc_2() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___enc_2)); }
	inline Encoding_t1523322056 * get_enc_2() const { return ___enc_2; }
	inline Encoding_t1523322056 ** get_address_of_enc_2() { return &___enc_2; }
	inline void set_enc_2(Encoding_t1523322056 * value)
	{
		___enc_2 = value;
		Il2CppCodeGenWriteBarrier((&___enc_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___stream_3)); }
	inline Stream_t1273022909 * get_stream_3() const { return ___stream_3; }
	inline Stream_t1273022909 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t1273022909 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___buffer_4)); }
	inline ByteU5BU5D_t4116647657* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_t4116647657* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_bufLength_5() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___bufLength_5)); }
	inline int32_t get_bufLength_5() const { return ___bufLength_5; }
	inline int32_t* get_address_of_bufLength_5() { return &___bufLength_5; }
	inline void set_bufLength_5(int32_t value)
	{
		___bufLength_5 = value;
	}

	inline static int32_t get_offset_of_bufPos_6() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434, ___bufPos_6)); }
	inline int32_t get_bufPos_6() const { return ___bufPos_6; }
	inline int32_t* get_address_of_bufPos_6() { return &___bufPos_6; }
	inline void set_bufPos_6(int32_t value)
	{
		___bufPos_6 = value;
	}
};

struct XmlInputStream_t1691369434_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t1523322056 * ___StrictUTF8_1;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_t1761730631 * ___encodingException_7;

public:
	inline static int32_t get_offset_of_StrictUTF8_1() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434_StaticFields, ___StrictUTF8_1)); }
	inline Encoding_t1523322056 * get_StrictUTF8_1() const { return ___StrictUTF8_1; }
	inline Encoding_t1523322056 ** get_address_of_StrictUTF8_1() { return &___StrictUTF8_1; }
	inline void set_StrictUTF8_1(Encoding_t1523322056 * value)
	{
		___StrictUTF8_1 = value;
		Il2CppCodeGenWriteBarrier((&___StrictUTF8_1), value);
	}

	inline static int32_t get_offset_of_encodingException_7() { return static_cast<int32_t>(offsetof(XmlInputStream_t1691369434_StaticFields, ___encodingException_7)); }
	inline XmlException_t1761730631 * get_encodingException_7() const { return ___encodingException_7; }
	inline XmlException_t1761730631 ** get_address_of_encodingException_7() { return &___encodingException_7; }
	inline void set_encodingException_7(XmlException_t1761730631 * value)
	{
		___encodingException_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodingException_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINPUTSTREAM_T1691369434_H
#ifndef DTDONEORMOREAUTOMATA_T1192707047_H
#define DTDONEORMOREAUTOMATA_T1192707047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOneOrMoreAutomata
struct  DTDOneOrMoreAutomata_t1192707047  : public DTDAutomata_t781538777
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDOneOrMoreAutomata::children
	DTDAutomata_t781538777 * ___children_1;

public:
	inline static int32_t get_offset_of_children_1() { return static_cast<int32_t>(offsetof(DTDOneOrMoreAutomata_t1192707047, ___children_1)); }
	inline DTDAutomata_t781538777 * get_children_1() const { return ___children_1; }
	inline DTDAutomata_t781538777 ** get_address_of_children_1() { return &___children_1; }
	inline void set_children_1(DTDAutomata_t781538777 * value)
	{
		___children_1 = value;
		Il2CppCodeGenWriteBarrier((&___children_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDONEORMOREAUTOMATA_T1192707047_H
#ifndef DTDEMPTYAUTOMATA_T465590953_H
#define DTDEMPTYAUTOMATA_T465590953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEmptyAutomata
struct  DTDEmptyAutomata_t465590953  : public DTDAutomata_t781538777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDEMPTYAUTOMATA_T465590953_H
#ifndef DTDNOTATIONDECLARATION_T3702682588_H
#define DTDNOTATIONDECLARATION_T3702682588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t3702682588  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t3702682588, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATION_T3702682588_H
#ifndef DTDELEMENTAUTOMATA_T1050190167_H
#define DTDELEMENTAUTOMATA_T1050190167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementAutomata
struct  DTDElementAutomata_t1050190167  : public DTDAutomata_t781538777
{
public:
	// System.String Mono.Xml.DTDElementAutomata::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DTDElementAutomata_t1050190167, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTAUTOMATA_T1050190167_H
#ifndef DTDCHOICEAUTOMATA_T3129343723_H
#define DTDCHOICEAUTOMATA_T3129343723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDChoiceAutomata
struct  DTDChoiceAutomata_t3129343723  : public DTDAutomata_t781538777
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::left
	DTDAutomata_t781538777 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::right
	DTDAutomata_t781538777 * ___right_2;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t3129343723, ___left_1)); }
	inline DTDAutomata_t781538777 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t781538777 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t781538777 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t3129343723, ___right_2)); }
	inline DTDAutomata_t781538777 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t781538777 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t781538777 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t3129343723, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t3129343723, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCHOICEAUTOMATA_T3129343723_H
#ifndef DTDSEQUENCEAUTOMATA_T2865847115_H
#define DTDSEQUENCEAUTOMATA_T2865847115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDSequenceAutomata
struct  DTDSequenceAutomata_t2865847115  : public DTDAutomata_t781538777
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::left
	DTDAutomata_t781538777 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::right
	DTDAutomata_t781538777 * ___right_2;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t2865847115, ___left_1)); }
	inline DTDAutomata_t781538777 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t781538777 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t781538777 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t2865847115, ___right_2)); }
	inline DTDAutomata_t781538777 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t781538777 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t781538777 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t2865847115, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t2865847115, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDSEQUENCEAUTOMATA_T2865847115_H
#ifndef DTDENTITYBASE_T1228162861_H
#define DTDENTITYBASE_T1228162861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityBase
struct  DTDEntityBase_t1228162861  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDEntityBase::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDEntityBase::publicId
	String_t* ___publicId_6;
	// System.String Mono.Xml.DTDEntityBase::systemId
	String_t* ___systemId_7;
	// System.String Mono.Xml.DTDEntityBase::literalValue
	String_t* ___literalValue_8;
	// System.String Mono.Xml.DTDEntityBase::replacementText
	String_t* ___replacementText_9;
	// System.String Mono.Xml.DTDEntityBase::uriString
	String_t* ___uriString_10;
	// System.Uri Mono.Xml.DTDEntityBase::absUri
	Uri_t100236324 * ___absUri_11;
	// System.Boolean Mono.Xml.DTDEntityBase::isInvalid
	bool ___isInvalid_12;
	// System.Boolean Mono.Xml.DTDEntityBase::loadFailed
	bool ___loadFailed_13;
	// System.Xml.XmlResolver Mono.Xml.DTDEntityBase::resolver
	XmlResolver_t626023767 * ___resolver_14;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_literalValue_8() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___literalValue_8)); }
	inline String_t* get_literalValue_8() const { return ___literalValue_8; }
	inline String_t** get_address_of_literalValue_8() { return &___literalValue_8; }
	inline void set_literalValue_8(String_t* value)
	{
		___literalValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___literalValue_8), value);
	}

	inline static int32_t get_offset_of_replacementText_9() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___replacementText_9)); }
	inline String_t* get_replacementText_9() const { return ___replacementText_9; }
	inline String_t** get_address_of_replacementText_9() { return &___replacementText_9; }
	inline void set_replacementText_9(String_t* value)
	{
		___replacementText_9 = value;
		Il2CppCodeGenWriteBarrier((&___replacementText_9), value);
	}

	inline static int32_t get_offset_of_uriString_10() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___uriString_10)); }
	inline String_t* get_uriString_10() const { return ___uriString_10; }
	inline String_t** get_address_of_uriString_10() { return &___uriString_10; }
	inline void set_uriString_10(String_t* value)
	{
		___uriString_10 = value;
		Il2CppCodeGenWriteBarrier((&___uriString_10), value);
	}

	inline static int32_t get_offset_of_absUri_11() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___absUri_11)); }
	inline Uri_t100236324 * get_absUri_11() const { return ___absUri_11; }
	inline Uri_t100236324 ** get_address_of_absUri_11() { return &___absUri_11; }
	inline void set_absUri_11(Uri_t100236324 * value)
	{
		___absUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___absUri_11), value);
	}

	inline static int32_t get_offset_of_isInvalid_12() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___isInvalid_12)); }
	inline bool get_isInvalid_12() const { return ___isInvalid_12; }
	inline bool* get_address_of_isInvalid_12() { return &___isInvalid_12; }
	inline void set_isInvalid_12(bool value)
	{
		___isInvalid_12 = value;
	}

	inline static int32_t get_offset_of_loadFailed_13() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___loadFailed_13)); }
	inline bool get_loadFailed_13() const { return ___loadFailed_13; }
	inline bool* get_address_of_loadFailed_13() { return &___loadFailed_13; }
	inline void set_loadFailed_13(bool value)
	{
		___loadFailed_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(DTDEntityBase_t1228162861, ___resolver_14)); }
	inline XmlResolver_t626023767 * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_t626023767 * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYBASE_T1228162861_H
#ifndef DTDATTLISTDECLARATION_T3593159715_H
#define DTDATTLISTDECLARATION_T3593159715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t3593159715  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_t1853889766 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t2718874744 * ___attributes_7;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_attributeOrders_6() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___attributeOrders_6)); }
	inline Hashtable_t1853889766 * get_attributeOrders_6() const { return ___attributeOrders_6; }
	inline Hashtable_t1853889766 ** get_address_of_attributeOrders_6() { return &___attributeOrders_6; }
	inline void set_attributeOrders_6(Hashtable_t1853889766 * value)
	{
		___attributeOrders_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOrders_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t3593159715, ___attributes_7)); }
	inline ArrayList_t2718874744 * get_attributes_7() const { return ___attributes_7; }
	inline ArrayList_t2718874744 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(ArrayList_t2718874744 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATION_T3593159715_H
#ifndef DTDELEMENTDECLARATION_T1830540991_H
#define DTDELEMENTDECLARATION_T1830540991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclaration
struct  DTDElementDeclaration_t1830540991  : public DTDNode_t858560093
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDElementDeclaration::root
	DTDObjectModel_t1729680289 * ___root_5;
	// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::contentModel
	DTDContentModel_t3264596611 * ___contentModel_6;
	// System.String Mono.Xml.DTDElementDeclaration::name
	String_t* ___name_7;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isEmpty
	bool ___isEmpty_8;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isAny
	bool ___isAny_9;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isMixedContent
	bool ___isMixedContent_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_contentModel_6() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___contentModel_6)); }
	inline DTDContentModel_t3264596611 * get_contentModel_6() const { return ___contentModel_6; }
	inline DTDContentModel_t3264596611 ** get_address_of_contentModel_6() { return &___contentModel_6; }
	inline void set_contentModel_6(DTDContentModel_t3264596611 * value)
	{
		___contentModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isEmpty_8() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isEmpty_8)); }
	inline bool get_isEmpty_8() const { return ___isEmpty_8; }
	inline bool* get_address_of_isEmpty_8() { return &___isEmpty_8; }
	inline void set_isEmpty_8(bool value)
	{
		___isEmpty_8 = value;
	}

	inline static int32_t get_offset_of_isAny_9() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isAny_9)); }
	inline bool get_isAny_9() const { return ___isAny_9; }
	inline bool* get_address_of_isAny_9() { return &___isAny_9; }
	inline void set_isAny_9(bool value)
	{
		___isAny_9 = value;
	}

	inline static int32_t get_offset_of_isMixedContent_10() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t1830540991, ___isMixedContent_10)); }
	inline bool get_isMixedContent_10() const { return ___isMixedContent_10; }
	inline bool* get_address_of_isMixedContent_10() { return &___isMixedContent_10; }
	inline void set_isMixedContent_10(bool value)
	{
		___isMixedContent_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATION_T1830540991_H
#ifndef DTDANYAUTOMATA_T3633486160_H
#define DTDANYAUTOMATA_T3633486160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAnyAutomata
struct  DTDAnyAutomata_t3633486160  : public DTDAutomata_t781538777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDANYAUTOMATA_T3633486160_H
#ifndef DTDINVALIDAUTOMATA_T1406553220_H
#define DTDINVALIDAUTOMATA_T1406553220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDInvalidAutomata
struct  DTDInvalidAutomata_t1406553220  : public DTDAutomata_t781538777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINVALIDAUTOMATA_T1406553220_H
#ifndef DICTIONARYBASE_T52754249_H
#define DICTIONARYBASE_T52754249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase
struct  DictionaryBase_t52754249  : public List_1_t218596005
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBASE_T52754249_H
#ifndef XMLTEXTATTRIBUTE_T499390083_H
#define XMLTEXTATTRIBUTE_T499390083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t499390083  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T499390083_H
#ifndef XMLIGNOREATTRIBUTE_T1428424057_H
#define XMLIGNOREATTRIBUTE_T1428424057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t1428424057  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T1428424057_H
#ifndef NAMETABLE_T3178203267_H
#define NAMETABLE_T3178203267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t3178203267  : public XmlNameTable_t71772148
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t491982174* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___buckets_1)); }
	inline EntryU5BU5D_t491982174* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t491982174** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t491982174* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t3178203267, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T3178203267_H
#ifndef XMLELEMENTATTRIBUTE_T17472343_H
#define XMLELEMENTATTRIBUTE_T17472343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t17472343  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t17472343, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T17472343_H
#ifndef XMLATTRIBUTEATTRIBUTE_T2511360870_H
#define XMLATTRIBUTEATTRIBUTE_T2511360870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_t2511360870  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t2511360870, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t2511360870, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_T2511360870_H
#ifndef XMLARRAYITEMATTRIBUTE_T3521089969_H
#define XMLARRAYITEMATTRIBUTE_T3521089969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlArrayItemAttribute
struct  XmlArrayItemAttribute_t3521089969  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::elementName
	String_t* ___elementName_0;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_t3521089969, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLARRAYITEMATTRIBUTE_T3521089969_H
#ifndef XMLENUMATTRIBUTE_T106705320_H
#define XMLENUMATTRIBUTE_T106705320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t106705320  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t106705320, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T106705320_H
#ifndef XMLROOTATTRIBUTE_T2306097217_H
#define XMLROOTATTRIBUTE_T2306097217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t2306097217  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t2306097217, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T2306097217_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_t966425202  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_T966425202_H
#ifndef DTDVALIDATINGREADER_T3946379043_H
#define DTDVALIDATINGREADER_T3946379043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader
struct  DTDValidatingReader_t3946379043  : public XmlReader_t3121518892
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.DTDValidatingReader::reader
	EntityResolvingXmlReader_t1267732406 * ___reader_2;
	// System.Xml.XmlTextReader Mono.Xml.DTDValidatingReader::sourceTextReader
	XmlTextReader_t4233384356 * ___sourceTextReader_3;
	// System.Xml.XmlValidatingReader Mono.Xml.DTDValidatingReader::validatingReader
	XmlValidatingReader_t1719295192 * ___validatingReader_4;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDValidatingReader::dtd
	DTDObjectModel_t1729680289 * ___dtd_5;
	// System.Xml.XmlResolver Mono.Xml.DTDValidatingReader::resolver
	XmlResolver_t626023767 * ___resolver_6;
	// System.String Mono.Xml.DTDValidatingReader::currentElement
	String_t* ___currentElement_7;
	// Mono.Xml.DTDValidatingReader/AttributeSlot[] Mono.Xml.DTDValidatingReader::attributes
	AttributeSlotU5BU5D_t600906362* ___attributes_8;
	// System.Int32 Mono.Xml.DTDValidatingReader::attributeCount
	int32_t ___attributeCount_9;
	// System.Int32 Mono.Xml.DTDValidatingReader::currentAttribute
	int32_t ___currentAttribute_10;
	// System.Boolean Mono.Xml.DTDValidatingReader::consumedAttribute
	bool ___consumedAttribute_11;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::elementStack
	Stack_t2329662280 * ___elementStack_12;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::automataStack
	Stack_t2329662280 * ___automataStack_13;
	// System.Boolean Mono.Xml.DTDValidatingReader::popScope
	bool ___popScope_14;
	// System.Boolean Mono.Xml.DTDValidatingReader::isStandalone
	bool ___isStandalone_15;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::currentAutomata
	DTDAutomata_t781538777 * ___currentAutomata_16;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::previousAutomata
	DTDAutomata_t781538777 * ___previousAutomata_17;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::idList
	ArrayList_t2718874744 * ___idList_18;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::missingIDReferences
	ArrayList_t2718874744 * ___missingIDReferences_19;
	// System.Xml.XmlNamespaceManager Mono.Xml.DTDValidatingReader::nsmgr
	XmlNamespaceManager_t418790500 * ___nsmgr_20;
	// System.String Mono.Xml.DTDValidatingReader::currentTextValue
	String_t* ___currentTextValue_21;
	// System.String Mono.Xml.DTDValidatingReader::constructingTextValue
	String_t* ___constructingTextValue_22;
	// System.Boolean Mono.Xml.DTDValidatingReader::shouldResetCurrentTextValue
	bool ___shouldResetCurrentTextValue_23;
	// System.Boolean Mono.Xml.DTDValidatingReader::isSignificantWhitespace
	bool ___isSignificantWhitespace_24;
	// System.Boolean Mono.Xml.DTDValidatingReader::isWhitespace
	bool ___isWhitespace_25;
	// System.Boolean Mono.Xml.DTDValidatingReader::isText
	bool ___isText_26;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::attributeValueEntityStack
	Stack_t2329662280 * ___attributeValueEntityStack_27;
	// System.Text.StringBuilder Mono.Xml.DTDValidatingReader::valueBuilder
	StringBuilder_t * ___valueBuilder_28;
	// System.Char[] Mono.Xml.DTDValidatingReader::whitespaceChars
	CharU5BU5D_t3528271667* ___whitespaceChars_29;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___reader_2)); }
	inline EntityResolvingXmlReader_t1267732406 * get_reader_2() const { return ___reader_2; }
	inline EntityResolvingXmlReader_t1267732406 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(EntityResolvingXmlReader_t1267732406 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_sourceTextReader_3() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___sourceTextReader_3)); }
	inline XmlTextReader_t4233384356 * get_sourceTextReader_3() const { return ___sourceTextReader_3; }
	inline XmlTextReader_t4233384356 ** get_address_of_sourceTextReader_3() { return &___sourceTextReader_3; }
	inline void set_sourceTextReader_3(XmlTextReader_t4233384356 * value)
	{
		___sourceTextReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextReader_3), value);
	}

	inline static int32_t get_offset_of_validatingReader_4() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___validatingReader_4)); }
	inline XmlValidatingReader_t1719295192 * get_validatingReader_4() const { return ___validatingReader_4; }
	inline XmlValidatingReader_t1719295192 ** get_address_of_validatingReader_4() { return &___validatingReader_4; }
	inline void set_validatingReader_4(XmlValidatingReader_t1719295192 * value)
	{
		___validatingReader_4 = value;
		Il2CppCodeGenWriteBarrier((&___validatingReader_4), value);
	}

	inline static int32_t get_offset_of_dtd_5() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___dtd_5)); }
	inline DTDObjectModel_t1729680289 * get_dtd_5() const { return ___dtd_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_dtd_5() { return &___dtd_5; }
	inline void set_dtd_5(DTDObjectModel_t1729680289 * value)
	{
		___dtd_5 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_5), value);
	}

	inline static int32_t get_offset_of_resolver_6() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___resolver_6)); }
	inline XmlResolver_t626023767 * get_resolver_6() const { return ___resolver_6; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_6() { return &___resolver_6; }
	inline void set_resolver_6(XmlResolver_t626023767 * value)
	{
		___resolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_6), value);
	}

	inline static int32_t get_offset_of_currentElement_7() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___currentElement_7)); }
	inline String_t* get_currentElement_7() const { return ___currentElement_7; }
	inline String_t** get_address_of_currentElement_7() { return &___currentElement_7; }
	inline void set_currentElement_7(String_t* value)
	{
		___currentElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentElement_7), value);
	}

	inline static int32_t get_offset_of_attributes_8() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___attributes_8)); }
	inline AttributeSlotU5BU5D_t600906362* get_attributes_8() const { return ___attributes_8; }
	inline AttributeSlotU5BU5D_t600906362** get_address_of_attributes_8() { return &___attributes_8; }
	inline void set_attributes_8(AttributeSlotU5BU5D_t600906362* value)
	{
		___attributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_8), value);
	}

	inline static int32_t get_offset_of_attributeCount_9() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___attributeCount_9)); }
	inline int32_t get_attributeCount_9() const { return ___attributeCount_9; }
	inline int32_t* get_address_of_attributeCount_9() { return &___attributeCount_9; }
	inline void set_attributeCount_9(int32_t value)
	{
		___attributeCount_9 = value;
	}

	inline static int32_t get_offset_of_currentAttribute_10() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___currentAttribute_10)); }
	inline int32_t get_currentAttribute_10() const { return ___currentAttribute_10; }
	inline int32_t* get_address_of_currentAttribute_10() { return &___currentAttribute_10; }
	inline void set_currentAttribute_10(int32_t value)
	{
		___currentAttribute_10 = value;
	}

	inline static int32_t get_offset_of_consumedAttribute_11() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___consumedAttribute_11)); }
	inline bool get_consumedAttribute_11() const { return ___consumedAttribute_11; }
	inline bool* get_address_of_consumedAttribute_11() { return &___consumedAttribute_11; }
	inline void set_consumedAttribute_11(bool value)
	{
		___consumedAttribute_11 = value;
	}

	inline static int32_t get_offset_of_elementStack_12() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___elementStack_12)); }
	inline Stack_t2329662280 * get_elementStack_12() const { return ___elementStack_12; }
	inline Stack_t2329662280 ** get_address_of_elementStack_12() { return &___elementStack_12; }
	inline void set_elementStack_12(Stack_t2329662280 * value)
	{
		___elementStack_12 = value;
		Il2CppCodeGenWriteBarrier((&___elementStack_12), value);
	}

	inline static int32_t get_offset_of_automataStack_13() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___automataStack_13)); }
	inline Stack_t2329662280 * get_automataStack_13() const { return ___automataStack_13; }
	inline Stack_t2329662280 ** get_address_of_automataStack_13() { return &___automataStack_13; }
	inline void set_automataStack_13(Stack_t2329662280 * value)
	{
		___automataStack_13 = value;
		Il2CppCodeGenWriteBarrier((&___automataStack_13), value);
	}

	inline static int32_t get_offset_of_popScope_14() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___popScope_14)); }
	inline bool get_popScope_14() const { return ___popScope_14; }
	inline bool* get_address_of_popScope_14() { return &___popScope_14; }
	inline void set_popScope_14(bool value)
	{
		___popScope_14 = value;
	}

	inline static int32_t get_offset_of_isStandalone_15() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___isStandalone_15)); }
	inline bool get_isStandalone_15() const { return ___isStandalone_15; }
	inline bool* get_address_of_isStandalone_15() { return &___isStandalone_15; }
	inline void set_isStandalone_15(bool value)
	{
		___isStandalone_15 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_16() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___currentAutomata_16)); }
	inline DTDAutomata_t781538777 * get_currentAutomata_16() const { return ___currentAutomata_16; }
	inline DTDAutomata_t781538777 ** get_address_of_currentAutomata_16() { return &___currentAutomata_16; }
	inline void set_currentAutomata_16(DTDAutomata_t781538777 * value)
	{
		___currentAutomata_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_16), value);
	}

	inline static int32_t get_offset_of_previousAutomata_17() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___previousAutomata_17)); }
	inline DTDAutomata_t781538777 * get_previousAutomata_17() const { return ___previousAutomata_17; }
	inline DTDAutomata_t781538777 ** get_address_of_previousAutomata_17() { return &___previousAutomata_17; }
	inline void set_previousAutomata_17(DTDAutomata_t781538777 * value)
	{
		___previousAutomata_17 = value;
		Il2CppCodeGenWriteBarrier((&___previousAutomata_17), value);
	}

	inline static int32_t get_offset_of_idList_18() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___idList_18)); }
	inline ArrayList_t2718874744 * get_idList_18() const { return ___idList_18; }
	inline ArrayList_t2718874744 ** get_address_of_idList_18() { return &___idList_18; }
	inline void set_idList_18(ArrayList_t2718874744 * value)
	{
		___idList_18 = value;
		Il2CppCodeGenWriteBarrier((&___idList_18), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_19() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___missingIDReferences_19)); }
	inline ArrayList_t2718874744 * get_missingIDReferences_19() const { return ___missingIDReferences_19; }
	inline ArrayList_t2718874744 ** get_address_of_missingIDReferences_19() { return &___missingIDReferences_19; }
	inline void set_missingIDReferences_19(ArrayList_t2718874744 * value)
	{
		___missingIDReferences_19 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_19), value);
	}

	inline static int32_t get_offset_of_nsmgr_20() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___nsmgr_20)); }
	inline XmlNamespaceManager_t418790500 * get_nsmgr_20() const { return ___nsmgr_20; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsmgr_20() { return &___nsmgr_20; }
	inline void set_nsmgr_20(XmlNamespaceManager_t418790500 * value)
	{
		___nsmgr_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_20), value);
	}

	inline static int32_t get_offset_of_currentTextValue_21() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___currentTextValue_21)); }
	inline String_t* get_currentTextValue_21() const { return ___currentTextValue_21; }
	inline String_t** get_address_of_currentTextValue_21() { return &___currentTextValue_21; }
	inline void set_currentTextValue_21(String_t* value)
	{
		___currentTextValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentTextValue_21), value);
	}

	inline static int32_t get_offset_of_constructingTextValue_22() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___constructingTextValue_22)); }
	inline String_t* get_constructingTextValue_22() const { return ___constructingTextValue_22; }
	inline String_t** get_address_of_constructingTextValue_22() { return &___constructingTextValue_22; }
	inline void set_constructingTextValue_22(String_t* value)
	{
		___constructingTextValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___constructingTextValue_22), value);
	}

	inline static int32_t get_offset_of_shouldResetCurrentTextValue_23() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___shouldResetCurrentTextValue_23)); }
	inline bool get_shouldResetCurrentTextValue_23() const { return ___shouldResetCurrentTextValue_23; }
	inline bool* get_address_of_shouldResetCurrentTextValue_23() { return &___shouldResetCurrentTextValue_23; }
	inline void set_shouldResetCurrentTextValue_23(bool value)
	{
		___shouldResetCurrentTextValue_23 = value;
	}

	inline static int32_t get_offset_of_isSignificantWhitespace_24() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___isSignificantWhitespace_24)); }
	inline bool get_isSignificantWhitespace_24() const { return ___isSignificantWhitespace_24; }
	inline bool* get_address_of_isSignificantWhitespace_24() { return &___isSignificantWhitespace_24; }
	inline void set_isSignificantWhitespace_24(bool value)
	{
		___isSignificantWhitespace_24 = value;
	}

	inline static int32_t get_offset_of_isWhitespace_25() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___isWhitespace_25)); }
	inline bool get_isWhitespace_25() const { return ___isWhitespace_25; }
	inline bool* get_address_of_isWhitespace_25() { return &___isWhitespace_25; }
	inline void set_isWhitespace_25(bool value)
	{
		___isWhitespace_25 = value;
	}

	inline static int32_t get_offset_of_isText_26() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___isText_26)); }
	inline bool get_isText_26() const { return ___isText_26; }
	inline bool* get_address_of_isText_26() { return &___isText_26; }
	inline void set_isText_26(bool value)
	{
		___isText_26 = value;
	}

	inline static int32_t get_offset_of_attributeValueEntityStack_27() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___attributeValueEntityStack_27)); }
	inline Stack_t2329662280 * get_attributeValueEntityStack_27() const { return ___attributeValueEntityStack_27; }
	inline Stack_t2329662280 ** get_address_of_attributeValueEntityStack_27() { return &___attributeValueEntityStack_27; }
	inline void set_attributeValueEntityStack_27(Stack_t2329662280 * value)
	{
		___attributeValueEntityStack_27 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueEntityStack_27), value);
	}

	inline static int32_t get_offset_of_valueBuilder_28() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___valueBuilder_28)); }
	inline StringBuilder_t * get_valueBuilder_28() const { return ___valueBuilder_28; }
	inline StringBuilder_t ** get_address_of_valueBuilder_28() { return &___valueBuilder_28; }
	inline void set_valueBuilder_28(StringBuilder_t * value)
	{
		___valueBuilder_28 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuilder_28), value);
	}

	inline static int32_t get_offset_of_whitespaceChars_29() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043, ___whitespaceChars_29)); }
	inline CharU5BU5D_t3528271667* get_whitespaceChars_29() const { return ___whitespaceChars_29; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaceChars_29() { return &___whitespaceChars_29; }
	inline void set_whitespaceChars_29(CharU5BU5D_t3528271667* value)
	{
		___whitespaceChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceChars_29), value);
	}
};

struct DTDValidatingReader_t3946379043_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.DTDValidatingReader::<>f__switch$map43
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map43_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map43_30() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t3946379043_StaticFields, ___U3CU3Ef__switchU24map43_30)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map43_30() const { return ___U3CU3Ef__switchU24map43_30; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map43_30() { return &___U3CU3Ef__switchU24map43_30; }
	inline void set_U3CU3Ef__switchU24map43_30(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map43_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map43_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDVALIDATINGREADER_T3946379043_H
#ifndef XMLSPACE_T3324193251_H
#define XMLSPACE_T3324193251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t3324193251 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t3324193251, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T3324193251_H
#ifndef WRITESTATE_T3983380671_H
#define WRITESTATE_T3983380671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t3983380671 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t3983380671, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T3983380671_H
#ifndef DTDCOLLECTIONBASE_T3926218464_H
#define DTDCOLLECTIONBASE_T3926218464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_t3926218464  : public DictionaryBase_t52754249
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t1729680289 * ___root_5;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDCollectionBase_t3926218464, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCOLLECTIONBASE_T3926218464_H
#ifndef XMLNODETYPE_T1672767151_H
#define XMLNODETYPE_T1672767151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t1672767151 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t1672767151, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T1672767151_H
#ifndef ENUMERATOR_T2107839882_H
#define ENUMERATOR_T2107839882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  Enumerator_t2107839882 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t218596005 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t3041488559  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___l_0)); }
	inline List_1_t218596005 * get_l_0() const { return ___l_0; }
	inline List_1_t218596005 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t218596005 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2107839882, ___current_3)); }
	inline KeyValuePair_2_t3041488559  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3041488559 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3041488559  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2107839882_H
#ifndef DATETIMESTYLES_T840957420_H
#define DATETIMESTYLES_T840957420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t840957420 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t840957420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T840957420_H
#ifndef XMLNODECHANGEDACTION_T3227731597_H
#define XMLNODECHANGEDACTION_T3227731597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_t3227731597 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_t3227731597, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_T3227731597_H
#ifndef CONFORMANCELEVEL_T3899847875_H
#define CONFORMANCELEVEL_T3899847875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3899847875 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3899847875, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3899847875_H
#ifndef XMLDOCUMENTTYPE_T4112370061_H
#define XMLDOCUMENTTYPE_T4112370061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t4112370061  : public XmlLinkedNode_t1437094927
{
public:
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t2821286253 * ___entities_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t2821286253 * ___notations_7;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t1729680289 * ___dtd_8;

public:
	inline static int32_t get_offset_of_entities_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___entities_6)); }
	inline XmlNamedNodeMap_t2821286253 * get_entities_6() const { return ___entities_6; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_entities_6() { return &___entities_6; }
	inline void set_entities_6(XmlNamedNodeMap_t2821286253 * value)
	{
		___entities_6 = value;
		Il2CppCodeGenWriteBarrier((&___entities_6), value);
	}

	inline static int32_t get_offset_of_notations_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___notations_7)); }
	inline XmlNamedNodeMap_t2821286253 * get_notations_7() const { return ___notations_7; }
	inline XmlNamedNodeMap_t2821286253 ** get_address_of_notations_7() { return &___notations_7; }
	inline void set_notations_7(XmlNamedNodeMap_t2821286253 * value)
	{
		___notations_7 = value;
		Il2CppCodeGenWriteBarrier((&___notations_7), value);
	}

	inline static int32_t get_offset_of_dtd_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t4112370061, ___dtd_8)); }
	inline DTDObjectModel_t1729680289 * get_dtd_8() const { return ___dtd_8; }
	inline DTDObjectModel_t1729680289 ** get_address_of_dtd_8() { return &___dtd_8; }
	inline void set_dtd_8(DTDObjectModel_t1729680289 * value)
	{
		___dtd_8 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T4112370061_H
#ifndef DTDPARAMETERENTITYDECLARATION_T3796253422_H
#define DTDPARAMETERENTITYDECLARATION_T3796253422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_t3796253422  : public DTDEntityBase_t1228162861
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATION_T3796253422_H
#ifndef XMLCHARACTERDATA_T1167807131_H
#define XMLCHARACTERDATA_T1167807131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t1167807131  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(XmlCharacterData_t1167807131, ___data_6)); }
	inline String_t* get_data_6() const { return ___data_6; }
	inline String_t** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(String_t* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T1167807131_H
#ifndef XMLELEMENT_T561603118_H
#define XMLELEMENT_T561603118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t561603118  : public XmlLinkedNode_t1437094927
{
public:
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t2316283784 * ___attributes_6;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t1073099671 * ___name_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_8;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_9;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	RuntimeObject* ___schemaInfo_10;

public:
	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___attributes_6)); }
	inline XmlAttributeCollection_t2316283784 * get_attributes_6() const { return ___attributes_6; }
	inline XmlAttributeCollection_t2316283784 ** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(XmlAttributeCollection_t2316283784 * value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___name_7)); }
	inline XmlNameEntry_t1073099671 * get_name_7() const { return ___name_7; }
	inline XmlNameEntry_t1073099671 ** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(XmlNameEntry_t1073099671 * value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}

	inline static int32_t get_offset_of_isNotEmpty_9() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___isNotEmpty_9)); }
	inline bool get_isNotEmpty_9() const { return ___isNotEmpty_9; }
	inline bool* get_address_of_isNotEmpty_9() { return &___isNotEmpty_9; }
	inline void set_isNotEmpty_9(bool value)
	{
		___isNotEmpty_9 = value;
	}

	inline static int32_t get_offset_of_schemaInfo_10() { return static_cast<int32_t>(offsetof(XmlElement_t561603118, ___schemaInfo_10)); }
	inline RuntimeObject* get_schemaInfo_10() const { return ___schemaInfo_10; }
	inline RuntimeObject** get_address_of_schemaInfo_10() { return &___schemaInfo_10; }
	inline void set_schemaInfo_10(RuntimeObject* value)
	{
		___schemaInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T561603118_H
#ifndef ENTITYHANDLING_T1047276436_H
#define ENTITYHANDLING_T1047276436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t1047276436 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t1047276436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T1047276436_H
#ifndef XMLDECLARATION_T679870411_H
#define XMLDECLARATION_T679870411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t679870411  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_6;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_7;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_8;

public:
	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___encoding_6)); }
	inline String_t* get_encoding_6() const { return ___encoding_6; }
	inline String_t** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(String_t* value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_standalone_7() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___standalone_7)); }
	inline String_t* get_standalone_7() const { return ___standalone_7; }
	inline String_t** get_address_of_standalone_7() { return &___standalone_7; }
	inline void set_standalone_7(String_t* value)
	{
		___standalone_7 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}
};

struct XmlDeclaration_t679870411_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4A_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t679870411_StaticFields, ___U3CU3Ef__switchU24map4A_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4A_9() const { return ___U3CU3Ef__switchU24map4A_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4A_9() { return &___U3CU3Ef__switchU24map4A_9; }
	inline void set_U3CU3Ef__switchU24map4A_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4A_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4A_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T679870411_H
#ifndef DTDATTRIBUTEOCCURENCETYPE_T2323614041_H
#define DTDATTRIBUTEOCCURENCETYPE_T2323614041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeOccurenceType
struct  DTDAttributeOccurenceType_t2323614041 
{
public:
	// System.Int32 Mono.Xml.DTDAttributeOccurenceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDAttributeOccurenceType_t2323614041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEOCCURENCETYPE_T2323614041_H
#ifndef DTDOCCURENCE_T3140866896_H
#define DTDOCCURENCE_T3140866896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOccurence
struct  DTDOccurence_t3140866896 
{
public:
	// System.Int32 Mono.Xml.DTDOccurence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDOccurence_t3140866896, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOCCURENCE_T3140866896_H
#ifndef DTDCONTENTORDERTYPE_T1195786655_H
#define DTDCONTENTORDERTYPE_T1195786655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentOrderType
struct  DTDContentOrderType_t1195786655 
{
public:
	// System.Int32 Mono.Xml.DTDContentOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDContentOrderType_t1195786655, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTORDERTYPE_T1195786655_H
#ifndef XMLEXCEPTION_T1761730631_H
#define XMLEXCEPTION_T1761730631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t1761730631  : public SystemException_t176217640
{
public:
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.XmlException::res
	String_t* ___res_14;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t1281789340* ___messages_15;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_13), value);
	}

	inline static int32_t get_offset_of_res_14() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___res_14)); }
	inline String_t* get_res_14() const { return ___res_14; }
	inline String_t** get_address_of_res_14() { return &___res_14; }
	inline void set_res_14(String_t* value)
	{
		___res_14 = value;
		Il2CppCodeGenWriteBarrier((&___res_14), value);
	}

	inline static int32_t get_offset_of_messages_15() { return static_cast<int32_t>(offsetof(XmlException_t1761730631, ___messages_15)); }
	inline StringU5BU5D_t1281789340* get_messages_15() const { return ___messages_15; }
	inline StringU5BU5D_t1281789340** get_address_of_messages_15() { return &___messages_15; }
	inline void set_messages_15(StringU5BU5D_t1281789340* value)
	{
		___messages_15 = value;
		Il2CppCodeGenWriteBarrier((&___messages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_T1761730631_H
#ifndef NEWLINEHANDLING_T850339274_H
#define NEWLINEHANDLING_T850339274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t850339274 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t850339274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T850339274_H
#ifndef VALIDATIONTYPE_T4049928607_H
#define VALIDATIONTYPE_T4049928607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t4049928607 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t4049928607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T4049928607_H
#ifndef XMLENTITYREFERENCE_T1966808559_H
#define XMLENTITYREFERENCE_T1966808559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t1966808559  : public XmlLinkedNode_t1437094927
{
public:
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t1437094927 * ___lastLinkedChild_7;

public:
	inline static int32_t get_offset_of_entityName_6() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___entityName_6)); }
	inline String_t* get_entityName_6() const { return ___entityName_6; }
	inline String_t** get_address_of_entityName_6() { return &___entityName_6; }
	inline void set_entityName_6(String_t* value)
	{
		___entityName_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_6), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlEntityReference_t1966808559, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1437094927 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1437094927 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1437094927 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T1966808559_H
#ifndef NAMESPACEHANDLING_T4087553436_H
#define NAMESPACEHANDLING_T4087553436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t4087553436 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t4087553436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T4087553436_H
#ifndef DTDENTITYDECLARATION_T811637416_H
#define DTDENTITYDECLARATION_T811637416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t811637416  : public DTDEntityBase_t1228162861
{
public:
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t2718874744 * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;

public:
	inline static int32_t get_offset_of_entityValue_15() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___entityValue_15)); }
	inline String_t* get_entityValue_15() const { return ___entityValue_15; }
	inline String_t** get_address_of_entityValue_15() { return &___entityValue_15; }
	inline void set_entityValue_15(String_t* value)
	{
		___entityValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___entityValue_15), value);
	}

	inline static int32_t get_offset_of_notationName_16() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___notationName_16)); }
	inline String_t* get_notationName_16() const { return ___notationName_16; }
	inline String_t** get_address_of_notationName_16() { return &___notationName_16; }
	inline void set_notationName_16(String_t* value)
	{
		___notationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_16), value);
	}

	inline static int32_t get_offset_of_ReferencingEntities_17() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___ReferencingEntities_17)); }
	inline ArrayList_t2718874744 * get_ReferencingEntities_17() const { return ___ReferencingEntities_17; }
	inline ArrayList_t2718874744 ** get_address_of_ReferencingEntities_17() { return &___ReferencingEntities_17; }
	inline void set_ReferencingEntities_17(ArrayList_t2718874744 * value)
	{
		___ReferencingEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencingEntities_17), value);
	}

	inline static int32_t get_offset_of_scanned_18() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___scanned_18)); }
	inline bool get_scanned_18() const { return ___scanned_18; }
	inline bool* get_address_of_scanned_18() { return &___scanned_18; }
	inline void set_scanned_18(bool value)
	{
		___scanned_18 = value;
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_hasExternalReference_20() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t811637416, ___hasExternalReference_20)); }
	inline bool get_hasExternalReference_20() const { return ___hasExternalReference_20; }
	inline bool* get_address_of_hasExternalReference_20() { return &___hasExternalReference_20; }
	inline void set_hasExternalReference_20(bool value)
	{
		___hasExternalReference_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATION_T811637416_H
#ifndef READSTATE_T944984020_H
#define READSTATE_T944984020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t944984020 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t944984020, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T944984020_H
#ifndef XMLSTREAMREADER_T727818754_H
#define XMLSTREAMREADER_T727818754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t727818754  : public NonBlockingStreamReader_t2495303928
{
public:
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_t1691369434 * ___input_12;

public:
	inline static int32_t get_offset_of_input_12() { return static_cast<int32_t>(offsetof(XmlStreamReader_t727818754, ___input_12)); }
	inline XmlInputStream_t1691369434 * get_input_12() const { return ___input_12; }
	inline XmlInputStream_t1691369434 ** get_address_of_input_12() { return &___input_12; }
	inline void set_input_12(XmlInputStream_t1691369434 * value)
	{
		___input_12 = value;
		Il2CppCodeGenWriteBarrier((&___input_12), value);
	}
};

struct XmlStreamReader_t727818754_StaticFields
{
public:
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_t1761730631 * ___invalidDataException_13;

public:
	inline static int32_t get_offset_of_invalidDataException_13() { return static_cast<int32_t>(offsetof(XmlStreamReader_t727818754_StaticFields, ___invalidDataException_13)); }
	inline XmlException_t1761730631 * get_invalidDataException_13() const { return ___invalidDataException_13; }
	inline XmlException_t1761730631 ** get_address_of_invalidDataException_13() { return &___invalidDataException_13; }
	inline void set_invalidDataException_13(XmlException_t1761730631 * value)
	{
		___invalidDataException_13 = value;
		Il2CppCodeGenWriteBarrier((&___invalidDataException_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTREAMREADER_T727818754_H
#ifndef WHITESPACEHANDLING_T784045650_H
#define WHITESPACEHANDLING_T784045650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t784045650 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t784045650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T784045650_H
#ifndef DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#define DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_t959292105  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATIONCOLLECTION_T959292105_H
#ifndef ENTITYRESOLVINGXMLREADER_T1267732406_H
#define ENTITYRESOLVINGXMLREADER_T1267732406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.EntityResolvingXmlReader
struct  EntityResolvingXmlReader_t1267732406  : public XmlReader_t3121518892
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.EntityResolvingXmlReader::entity
	EntityResolvingXmlReader_t1267732406 * ___entity_2;
	// System.Xml.XmlReader Mono.Xml.EntityResolvingXmlReader::source
	XmlReader_t3121518892 * ___source_3;
	// System.Xml.XmlParserContext Mono.Xml.EntityResolvingXmlReader::context
	XmlParserContext_t2544895291 * ___context_4;
	// System.Xml.XmlResolver Mono.Xml.EntityResolvingXmlReader::resolver
	XmlResolver_t626023767 * ___resolver_5;
	// System.Xml.EntityHandling Mono.Xml.EntityResolvingXmlReader::entity_handling
	int32_t ___entity_handling_6;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::entity_inside_attr
	bool ___entity_inside_attr_7;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::inside_attr
	bool ___inside_attr_8;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::do_resolve
	bool ___do_resolve_9;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___entity_2)); }
	inline EntityResolvingXmlReader_t1267732406 * get_entity_2() const { return ___entity_2; }
	inline EntityResolvingXmlReader_t1267732406 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(EntityResolvingXmlReader_t1267732406 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___source_3)); }
	inline XmlReader_t3121518892 * get_source_3() const { return ___source_3; }
	inline XmlReader_t3121518892 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlReader_t3121518892 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___context_4)); }
	inline XmlParserContext_t2544895291 * get_context_4() const { return ___context_4; }
	inline XmlParserContext_t2544895291 ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(XmlParserContext_t2544895291 * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier((&___context_4), value);
	}

	inline static int32_t get_offset_of_resolver_5() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___resolver_5)); }
	inline XmlResolver_t626023767 * get_resolver_5() const { return ___resolver_5; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_5() { return &___resolver_5; }
	inline void set_resolver_5(XmlResolver_t626023767 * value)
	{
		___resolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_5), value);
	}

	inline static int32_t get_offset_of_entity_handling_6() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___entity_handling_6)); }
	inline int32_t get_entity_handling_6() const { return ___entity_handling_6; }
	inline int32_t* get_address_of_entity_handling_6() { return &___entity_handling_6; }
	inline void set_entity_handling_6(int32_t value)
	{
		___entity_handling_6 = value;
	}

	inline static int32_t get_offset_of_entity_inside_attr_7() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___entity_inside_attr_7)); }
	inline bool get_entity_inside_attr_7() const { return ___entity_inside_attr_7; }
	inline bool* get_address_of_entity_inside_attr_7() { return &___entity_inside_attr_7; }
	inline void set_entity_inside_attr_7(bool value)
	{
		___entity_inside_attr_7 = value;
	}

	inline static int32_t get_offset_of_inside_attr_8() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___inside_attr_8)); }
	inline bool get_inside_attr_8() const { return ___inside_attr_8; }
	inline bool* get_address_of_inside_attr_8() { return &___inside_attr_8; }
	inline void set_inside_attr_8(bool value)
	{
		___inside_attr_8 = value;
	}

	inline static int32_t get_offset_of_do_resolve_9() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t1267732406, ___do_resolve_9)); }
	inline bool get_do_resolve_9() const { return ___do_resolve_9; }
	inline bool* get_address_of_do_resolve_9() { return &___do_resolve_9; }
	inline void set_do_resolve_9(bool value)
	{
		___do_resolve_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYRESOLVINGXMLREADER_T1267732406_H
#ifndef XMLCDATASECTION_T3267478366_H
#define XMLCDATASECTION_T3267478366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t3267478366  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T3267478366_H
#ifndef DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#define DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclarationCollection
struct  DTDElementDeclarationCollection_t222313714  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATIONCOLLECTION_T222313714_H
#ifndef DTDATTRIBUTEDEFINITION_T3434905422_H
#define DTDATTRIBUTEDEFINITION_T3434905422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeDefinition
struct  DTDAttributeDefinition_t3434905422  : public DTDNode_t858560093
{
public:
	// System.String Mono.Xml.DTDAttributeDefinition::name
	String_t* ___name_5;
	// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::datatype
	XmlSchemaDatatype_t322714710 * ___datatype_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedLiterals
	ArrayList_t2718874744 * ___enumeratedLiterals_7;
	// System.String Mono.Xml.DTDAttributeDefinition::unresolvedDefault
	String_t* ___unresolvedDefault_8;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedNotations
	ArrayList_t2718874744 * ___enumeratedNotations_9;
	// Mono.Xml.DTDAttributeOccurenceType Mono.Xml.DTDAttributeDefinition::occurenceType
	int32_t ___occurenceType_10;
	// System.String Mono.Xml.DTDAttributeDefinition::resolvedDefaultValue
	String_t* ___resolvedDefaultValue_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_datatype_6() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___datatype_6)); }
	inline XmlSchemaDatatype_t322714710 * get_datatype_6() const { return ___datatype_6; }
	inline XmlSchemaDatatype_t322714710 ** get_address_of_datatype_6() { return &___datatype_6; }
	inline void set_datatype_6(XmlSchemaDatatype_t322714710 * value)
	{
		___datatype_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_6), value);
	}

	inline static int32_t get_offset_of_enumeratedLiterals_7() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___enumeratedLiterals_7)); }
	inline ArrayList_t2718874744 * get_enumeratedLiterals_7() const { return ___enumeratedLiterals_7; }
	inline ArrayList_t2718874744 ** get_address_of_enumeratedLiterals_7() { return &___enumeratedLiterals_7; }
	inline void set_enumeratedLiterals_7(ArrayList_t2718874744 * value)
	{
		___enumeratedLiterals_7 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedLiterals_7), value);
	}

	inline static int32_t get_offset_of_unresolvedDefault_8() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___unresolvedDefault_8)); }
	inline String_t* get_unresolvedDefault_8() const { return ___unresolvedDefault_8; }
	inline String_t** get_address_of_unresolvedDefault_8() { return &___unresolvedDefault_8; }
	inline void set_unresolvedDefault_8(String_t* value)
	{
		___unresolvedDefault_8 = value;
		Il2CppCodeGenWriteBarrier((&___unresolvedDefault_8), value);
	}

	inline static int32_t get_offset_of_enumeratedNotations_9() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___enumeratedNotations_9)); }
	inline ArrayList_t2718874744 * get_enumeratedNotations_9() const { return ___enumeratedNotations_9; }
	inline ArrayList_t2718874744 ** get_address_of_enumeratedNotations_9() { return &___enumeratedNotations_9; }
	inline void set_enumeratedNotations_9(ArrayList_t2718874744 * value)
	{
		___enumeratedNotations_9 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedNotations_9), value);
	}

	inline static int32_t get_offset_of_occurenceType_10() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___occurenceType_10)); }
	inline int32_t get_occurenceType_10() const { return ___occurenceType_10; }
	inline int32_t* get_address_of_occurenceType_10() { return &___occurenceType_10; }
	inline void set_occurenceType_10(int32_t value)
	{
		___occurenceType_10 = value;
	}

	inline static int32_t get_offset_of_resolvedDefaultValue_11() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3434905422, ___resolvedDefaultValue_11)); }
	inline String_t* get_resolvedDefaultValue_11() const { return ___resolvedDefaultValue_11; }
	inline String_t** get_address_of_resolvedDefaultValue_11() { return &___resolvedDefaultValue_11; }
	inline void set_resolvedDefaultValue_11(String_t* value)
	{
		___resolvedDefaultValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolvedDefaultValue_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEDEFINITION_T3434905422_H
#ifndef DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#define DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclarationCollection
struct  DTDAttListDeclarationCollection_t2220366188  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATIONCOLLECTION_T2220366188_H
#ifndef DTDCONTENTMODEL_T3264596611_H
#define DTDCONTENTMODEL_T3264596611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModel
struct  DTDContentModel_t3264596611  : public DTDNode_t858560093
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDContentModel::root
	DTDObjectModel_t1729680289 * ___root_5;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDContentModel::compiledAutomata
	DTDAutomata_t781538777 * ___compiledAutomata_6;
	// System.String Mono.Xml.DTDContentModel::ownerElementName
	String_t* ___ownerElementName_7;
	// System.String Mono.Xml.DTDContentModel::elementName
	String_t* ___elementName_8;
	// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::orderType
	int32_t ___orderType_9;
	// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::childModels
	DTDContentModelCollection_t2798820000 * ___childModels_10;
	// Mono.Xml.DTDOccurence Mono.Xml.DTDContentModel::occurence
	int32_t ___occurence_11;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___root_5)); }
	inline DTDObjectModel_t1729680289 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1729680289 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1729680289 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_compiledAutomata_6() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___compiledAutomata_6)); }
	inline DTDAutomata_t781538777 * get_compiledAutomata_6() const { return ___compiledAutomata_6; }
	inline DTDAutomata_t781538777 ** get_address_of_compiledAutomata_6() { return &___compiledAutomata_6; }
	inline void set_compiledAutomata_6(DTDAutomata_t781538777 * value)
	{
		___compiledAutomata_6 = value;
		Il2CppCodeGenWriteBarrier((&___compiledAutomata_6), value);
	}

	inline static int32_t get_offset_of_ownerElementName_7() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___ownerElementName_7)); }
	inline String_t* get_ownerElementName_7() const { return ___ownerElementName_7; }
	inline String_t** get_address_of_ownerElementName_7() { return &___ownerElementName_7; }
	inline void set_ownerElementName_7(String_t* value)
	{
		___ownerElementName_7 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElementName_7), value);
	}

	inline static int32_t get_offset_of_elementName_8() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___elementName_8)); }
	inline String_t* get_elementName_8() const { return ___elementName_8; }
	inline String_t** get_address_of_elementName_8() { return &___elementName_8; }
	inline void set_elementName_8(String_t* value)
	{
		___elementName_8 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_8), value);
	}

	inline static int32_t get_offset_of_orderType_9() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___orderType_9)); }
	inline int32_t get_orderType_9() const { return ___orderType_9; }
	inline int32_t* get_address_of_orderType_9() { return &___orderType_9; }
	inline void set_orderType_9(int32_t value)
	{
		___orderType_9 = value;
	}

	inline static int32_t get_offset_of_childModels_10() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___childModels_10)); }
	inline DTDContentModelCollection_t2798820000 * get_childModels_10() const { return ___childModels_10; }
	inline DTDContentModelCollection_t2798820000 ** get_address_of_childModels_10() { return &___childModels_10; }
	inline void set_childModels_10(DTDContentModelCollection_t2798820000 * value)
	{
		___childModels_10 = value;
		Il2CppCodeGenWriteBarrier((&___childModels_10), value);
	}

	inline static int32_t get_offset_of_occurence_11() { return static_cast<int32_t>(offsetof(DTDContentModel_t3264596611, ___occurence_11)); }
	inline int32_t get_occurence_11() const { return ___occurence_11; }
	inline int32_t* get_address_of_occurence_11() { return &___occurence_11; }
	inline void set_occurence_11(int32_t value)
	{
		___occurence_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODEL_T3264596611_H
#ifndef DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#define DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t2250844513  : public DTDCollectionBase_t3926218464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATIONCOLLECTION_T2250844513_H
#ifndef U3CU3EC__ITERATOR3_T2072931442_H
#define U3CU3EC__ITERATOR3_T2072931442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct  U3CU3Ec__Iterator3_t2072931442  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase/<>c__Iterator3::<$s_431>__0
	Enumerator_t2107839882  ___U3CU24s_431U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::<p>__1
	KeyValuePair_2_t3041488559  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase/<>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::$current
	DTDNode_t858560093 * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase/<>c__Iterator3::<>f__this
	DictionaryBase_t52754249 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_431U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CU24s_431U3E__0_0)); }
	inline Enumerator_t2107839882  get_U3CU24s_431U3E__0_0() const { return ___U3CU24s_431U3E__0_0; }
	inline Enumerator_t2107839882 * get_address_of_U3CU24s_431U3E__0_0() { return &___U3CU24s_431U3E__0_0; }
	inline void set_U3CU24s_431U3E__0_0(Enumerator_t2107839882  value)
	{
		___U3CU24s_431U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t3041488559  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t3041488559 * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t3041488559  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U24current_3)); }
	inline DTDNode_t858560093 * get_U24current_3() const { return ___U24current_3; }
	inline DTDNode_t858560093 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(DTDNode_t858560093 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t2072931442, ___U3CU3Ef__this_4)); }
	inline DictionaryBase_t52754249 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline DictionaryBase_t52754249 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(DictionaryBase_t52754249 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR3_T2072931442_H
#ifndef XMLCONVERT_T1981561327_H
#define XMLCONVERT_T1981561327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1981561327  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1981561327_StaticFields
{
public:
	// System.String[] System.Xml.XmlConvert::datetimeFormats
	StringU5BU5D_t1281789340* ___datetimeFormats_0;
	// System.String[] System.Xml.XmlConvert::defaultDateTimeFormats
	StringU5BU5D_t1281789340* ___defaultDateTimeFormats_1;
	// System.String[] System.Xml.XmlConvert::roundtripDateTimeFormats
	StringU5BU5D_t1281789340* ___roundtripDateTimeFormats_2;
	// System.String[] System.Xml.XmlConvert::localDateTimeFormats
	StringU5BU5D_t1281789340* ___localDateTimeFormats_3;
	// System.String[] System.Xml.XmlConvert::utcDateTimeFormats
	StringU5BU5D_t1281789340* ___utcDateTimeFormats_4;
	// System.String[] System.Xml.XmlConvert::unspecifiedDateTimeFormats
	StringU5BU5D_t1281789340* ___unspecifiedDateTimeFormats_5;
	// System.Globalization.DateTimeStyles System.Xml.XmlConvert::_defaultStyle
	int32_t ____defaultStyle_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlConvert::<>f__switch$map49
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map49_7;

public:
	inline static int32_t get_offset_of_datetimeFormats_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___datetimeFormats_0)); }
	inline StringU5BU5D_t1281789340* get_datetimeFormats_0() const { return ___datetimeFormats_0; }
	inline StringU5BU5D_t1281789340** get_address_of_datetimeFormats_0() { return &___datetimeFormats_0; }
	inline void set_datetimeFormats_0(StringU5BU5D_t1281789340* value)
	{
		___datetimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___datetimeFormats_0), value);
	}

	inline static int32_t get_offset_of_defaultDateTimeFormats_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___defaultDateTimeFormats_1)); }
	inline StringU5BU5D_t1281789340* get_defaultDateTimeFormats_1() const { return ___defaultDateTimeFormats_1; }
	inline StringU5BU5D_t1281789340** get_address_of_defaultDateTimeFormats_1() { return &___defaultDateTimeFormats_1; }
	inline void set_defaultDateTimeFormats_1(StringU5BU5D_t1281789340* value)
	{
		___defaultDateTimeFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDateTimeFormats_1), value);
	}

	inline static int32_t get_offset_of_roundtripDateTimeFormats_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___roundtripDateTimeFormats_2)); }
	inline StringU5BU5D_t1281789340* get_roundtripDateTimeFormats_2() const { return ___roundtripDateTimeFormats_2; }
	inline StringU5BU5D_t1281789340** get_address_of_roundtripDateTimeFormats_2() { return &___roundtripDateTimeFormats_2; }
	inline void set_roundtripDateTimeFormats_2(StringU5BU5D_t1281789340* value)
	{
		___roundtripDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundtripDateTimeFormats_2), value);
	}

	inline static int32_t get_offset_of_localDateTimeFormats_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___localDateTimeFormats_3)); }
	inline StringU5BU5D_t1281789340* get_localDateTimeFormats_3() const { return ___localDateTimeFormats_3; }
	inline StringU5BU5D_t1281789340** get_address_of_localDateTimeFormats_3() { return &___localDateTimeFormats_3; }
	inline void set_localDateTimeFormats_3(StringU5BU5D_t1281789340* value)
	{
		___localDateTimeFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___localDateTimeFormats_3), value);
	}

	inline static int32_t get_offset_of_utcDateTimeFormats_4() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___utcDateTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_utcDateTimeFormats_4() const { return ___utcDateTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_utcDateTimeFormats_4() { return &___utcDateTimeFormats_4; }
	inline void set_utcDateTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___utcDateTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___utcDateTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_unspecifiedDateTimeFormats_5() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___unspecifiedDateTimeFormats_5)); }
	inline StringU5BU5D_t1281789340* get_unspecifiedDateTimeFormats_5() const { return ___unspecifiedDateTimeFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_unspecifiedDateTimeFormats_5() { return &___unspecifiedDateTimeFormats_5; }
	inline void set_unspecifiedDateTimeFormats_5(StringU5BU5D_t1281789340* value)
	{
		___unspecifiedDateTimeFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedDateTimeFormats_5), value);
	}

	inline static int32_t get_offset_of__defaultStyle_6() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ____defaultStyle_6)); }
	inline int32_t get__defaultStyle_6() const { return ____defaultStyle_6; }
	inline int32_t* get_address_of__defaultStyle_6() { return &____defaultStyle_6; }
	inline void set__defaultStyle_6(int32_t value)
	{
		____defaultStyle_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map49_7() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___U3CU3Ef__switchU24map49_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map49_7() const { return ___U3CU3Ef__switchU24map49_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map49_7() { return &___U3CU3Ef__switchU24map49_7; }
	inline void set_U3CU3Ef__switchU24map49_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map49_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map49_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1981561327_H
#ifndef XMLCOMMENT_T2476947920_H
#define XMLCOMMENT_T2476947920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t2476947920  : public XmlCharacterData_t1167807131
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T2476947920_H
#ifndef CONTEXTITEM_T3112052795_H
#define CONTEXTITEM_T3112052795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext/ContextItem
struct  ContextItem_t3112052795  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext/ContextItem::BaseURI
	String_t* ___BaseURI_0;
	// System.String System.Xml.XmlParserContext/ContextItem::XmlLang
	String_t* ___XmlLang_1;
	// System.Xml.XmlSpace System.Xml.XmlParserContext/ContextItem::XmlSpace
	int32_t ___XmlSpace_2;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_XmlLang_1() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___XmlLang_1)); }
	inline String_t* get_XmlLang_1() const { return ___XmlLang_1; }
	inline String_t** get_address_of_XmlLang_1() { return &___XmlLang_1; }
	inline void set_XmlLang_1(String_t* value)
	{
		___XmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_1), value);
	}

	inline static int32_t get_offset_of_XmlSpace_2() { return static_cast<int32_t>(offsetof(ContextItem_t3112052795, ___XmlSpace_2)); }
	inline int32_t get_XmlSpace_2() const { return ___XmlSpace_2; }
	inline int32_t* get_address_of_XmlSpace_2() { return &___XmlSpace_2; }
	inline void set_XmlSpace_2(int32_t value)
	{
		___XmlSpace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTITEM_T3112052795_H
#ifndef XMLNODECHANGEDEVENTARGS_T2486095928_H
#define XMLNODECHANGEDEVENTARGS_T2486095928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t2486095928  : public EventArgs_t3591816995
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_oldParent
	XmlNode_t3767805227 * ____oldParent_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_newParent
	XmlNode_t3767805227 * ____newParent_2;
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::_action
	int32_t ____action_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_node
	XmlNode_t3767805227 * ____node_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::_oldValue
	String_t* ____oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::_newValue
	String_t* ____newValue_6;

public:
	inline static int32_t get_offset_of__oldParent_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____oldParent_1)); }
	inline XmlNode_t3767805227 * get__oldParent_1() const { return ____oldParent_1; }
	inline XmlNode_t3767805227 ** get_address_of__oldParent_1() { return &____oldParent_1; }
	inline void set__oldParent_1(XmlNode_t3767805227 * value)
	{
		____oldParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____oldParent_1), value);
	}

	inline static int32_t get_offset_of__newParent_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____newParent_2)); }
	inline XmlNode_t3767805227 * get__newParent_2() const { return ____newParent_2; }
	inline XmlNode_t3767805227 ** get_address_of__newParent_2() { return &____newParent_2; }
	inline void set__newParent_2(XmlNode_t3767805227 * value)
	{
		____newParent_2 = value;
		Il2CppCodeGenWriteBarrier((&____newParent_2), value);
	}

	inline static int32_t get_offset_of__action_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____action_3)); }
	inline int32_t get__action_3() const { return ____action_3; }
	inline int32_t* get_address_of__action_3() { return &____action_3; }
	inline void set__action_3(int32_t value)
	{
		____action_3 = value;
	}

	inline static int32_t get_offset_of__node_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____node_4)); }
	inline XmlNode_t3767805227 * get__node_4() const { return ____node_4; }
	inline XmlNode_t3767805227 ** get_address_of__node_4() { return &____node_4; }
	inline void set__node_4(XmlNode_t3767805227 * value)
	{
		____node_4 = value;
		Il2CppCodeGenWriteBarrier((&____node_4), value);
	}

	inline static int32_t get_offset_of__oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____oldValue_5)); }
	inline String_t* get__oldValue_5() const { return ____oldValue_5; }
	inline String_t** get_address_of__oldValue_5() { return &____oldValue_5; }
	inline void set__oldValue_5(String_t* value)
	{
		____oldValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____oldValue_5), value);
	}

	inline static int32_t get_offset_of__newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t2486095928, ____newValue_6)); }
	inline String_t* get__newValue_6() const { return ____newValue_6; }
	inline String_t** get_address_of__newValue_6() { return &____newValue_6; }
	inline void set__newValue_6(String_t* value)
	{
		____newValue_6 = value;
		Il2CppCodeGenWriteBarrier((&____newValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTARGS_T2486095928_H
#ifndef XMLPARSERCONTEXT_T2544895291_H
#define XMLPARSERCONTEXT_T2544895291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_t2544895291  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext::baseURI
	String_t* ___baseURI_0;
	// System.String System.Xml.XmlParserContext::docTypeName
	String_t* ___docTypeName_1;
	// System.Text.Encoding System.Xml.XmlParserContext::encoding
	Encoding_t1523322056 * ___encoding_2;
	// System.String System.Xml.XmlParserContext::internalSubset
	String_t* ___internalSubset_3;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::namespaceManager
	XmlNamespaceManager_t418790500 * ___namespaceManager_4;
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::nameTable
	XmlNameTable_t71772148 * ___nameTable_5;
	// System.String System.Xml.XmlParserContext::publicID
	String_t* ___publicID_6;
	// System.String System.Xml.XmlParserContext::systemID
	String_t* ___systemID_7;
	// System.String System.Xml.XmlParserContext::xmlLang
	String_t* ___xmlLang_8;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::xmlSpace
	int32_t ___xmlSpace_9;
	// System.Collections.ArrayList System.Xml.XmlParserContext::contextItems
	ArrayList_t2718874744 * ___contextItems_10;
	// System.Int32 System.Xml.XmlParserContext::contextItemCount
	int32_t ___contextItemCount_11;
	// Mono.Xml.DTDObjectModel System.Xml.XmlParserContext::dtd
	DTDObjectModel_t1729680289 * ___dtd_12;

public:
	inline static int32_t get_offset_of_baseURI_0() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___baseURI_0)); }
	inline String_t* get_baseURI_0() const { return ___baseURI_0; }
	inline String_t** get_address_of_baseURI_0() { return &___baseURI_0; }
	inline void set_baseURI_0(String_t* value)
	{
		___baseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_0), value);
	}

	inline static int32_t get_offset_of_docTypeName_1() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___docTypeName_1)); }
	inline String_t* get_docTypeName_1() const { return ___docTypeName_1; }
	inline String_t** get_address_of_docTypeName_1() { return &___docTypeName_1; }
	inline void set_docTypeName_1(String_t* value)
	{
		___docTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeName_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___encoding_2)); }
	inline Encoding_t1523322056 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t1523322056 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t1523322056 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_internalSubset_3() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___internalSubset_3)); }
	inline String_t* get_internalSubset_3() const { return ___internalSubset_3; }
	inline String_t** get_address_of_internalSubset_3() { return &___internalSubset_3; }
	inline void set_internalSubset_3(String_t* value)
	{
		___internalSubset_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t418790500 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t418790500 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___nameTable_5)); }
	inline XmlNameTable_t71772148 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t71772148 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_publicID_6() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___publicID_6)); }
	inline String_t* get_publicID_6() const { return ___publicID_6; }
	inline String_t** get_address_of_publicID_6() { return &___publicID_6; }
	inline void set_publicID_6(String_t* value)
	{
		___publicID_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicID_6), value);
	}

	inline static int32_t get_offset_of_systemID_7() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___systemID_7)); }
	inline String_t* get_systemID_7() const { return ___systemID_7; }
	inline String_t** get_address_of_systemID_7() { return &___systemID_7; }
	inline void set_systemID_7(String_t* value)
	{
		___systemID_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemID_7), value);
	}

	inline static int32_t get_offset_of_xmlLang_8() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___xmlLang_8)); }
	inline String_t* get_xmlLang_8() const { return ___xmlLang_8; }
	inline String_t** get_address_of_xmlLang_8() { return &___xmlLang_8; }
	inline void set_xmlLang_8(String_t* value)
	{
		___xmlLang_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_8), value);
	}

	inline static int32_t get_offset_of_xmlSpace_9() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___xmlSpace_9)); }
	inline int32_t get_xmlSpace_9() const { return ___xmlSpace_9; }
	inline int32_t* get_address_of_xmlSpace_9() { return &___xmlSpace_9; }
	inline void set_xmlSpace_9(int32_t value)
	{
		___xmlSpace_9 = value;
	}

	inline static int32_t get_offset_of_contextItems_10() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___contextItems_10)); }
	inline ArrayList_t2718874744 * get_contextItems_10() const { return ___contextItems_10; }
	inline ArrayList_t2718874744 ** get_address_of_contextItems_10() { return &___contextItems_10; }
	inline void set_contextItems_10(ArrayList_t2718874744 * value)
	{
		___contextItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___contextItems_10), value);
	}

	inline static int32_t get_offset_of_contextItemCount_11() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___contextItemCount_11)); }
	inline int32_t get_contextItemCount_11() const { return ___contextItemCount_11; }
	inline int32_t* get_address_of_contextItemCount_11() { return &___contextItemCount_11; }
	inline void set_contextItemCount_11(int32_t value)
	{
		___contextItemCount_11 = value;
	}

	inline static int32_t get_offset_of_dtd_12() { return static_cast<int32_t>(offsetof(XmlParserContext_t2544895291, ___dtd_12)); }
	inline DTDObjectModel_t1729680289 * get_dtd_12() const { return ___dtd_12; }
	inline DTDObjectModel_t1729680289 ** get_address_of_dtd_12() { return &___dtd_12; }
	inline void set_dtd_12(DTDObjectModel_t1729680289 * value)
	{
		___dtd_12 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_T2544895291_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (XmlArrayAttribute_t1484124842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[2] = 
{
	XmlArrayAttribute_t1484124842::get_offset_of_elementName_0(),
	XmlArrayAttribute_t1484124842::get_offset_of_order_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (XmlArrayItemAttribute_t3521089969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[1] = 
{
	XmlArrayItemAttribute_t3521089969::get_offset_of_elementName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (XmlAttributeAttribute_t2511360870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	XmlAttributeAttribute_t2511360870::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t2511360870::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (XmlElementAttribute_t17472343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[3] = 
{
	XmlElementAttribute_t17472343::get_offset_of_elementName_0(),
	XmlElementAttribute_t17472343::get_offset_of_type_1(),
	XmlElementAttribute_t17472343::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (XmlEnumAttribute_t106705320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[1] = 
{
	XmlEnumAttribute_t106705320::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (XmlIgnoreAttribute_t1428424057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (XmlNamespaceDeclarationsAttribute_t966425202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (XmlRootAttribute_t2306097217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[3] = 
{
	XmlRootAttribute_t2306097217::get_offset_of_elementName_0(),
	XmlRootAttribute_t2306097217::get_offset_of_isNullable_1(),
	XmlRootAttribute_t2306097217::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (XmlSerializerNamespaces_t2702737953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	XmlSerializerNamespaces_t2702737953::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (XmlTextAttribute_t499390083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (XPathItem_t4250588140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (ConformanceLevel_t3899847875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[4] = 
{
	ConformanceLevel_t3899847875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (DTDAutomataFactory_t2958275022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[3] = 
{
	DTDAutomataFactory_t2958275022::get_offset_of_root_0(),
	DTDAutomataFactory_t2958275022::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t2958275022::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (DTDAutomata_t781538777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	DTDAutomata_t781538777::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (DTDElementAutomata_t1050190167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[1] = 
{
	DTDElementAutomata_t1050190167::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (DTDChoiceAutomata_t3129343723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[4] = 
{
	DTDChoiceAutomata_t3129343723::get_offset_of_left_1(),
	DTDChoiceAutomata_t3129343723::get_offset_of_right_2(),
	DTDChoiceAutomata_t3129343723::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t3129343723::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (DTDSequenceAutomata_t2865847115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	DTDSequenceAutomata_t2865847115::get_offset_of_left_1(),
	DTDSequenceAutomata_t2865847115::get_offset_of_right_2(),
	DTDSequenceAutomata_t2865847115::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t2865847115::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (DTDOneOrMoreAutomata_t1192707047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[1] = 
{
	DTDOneOrMoreAutomata_t1192707047::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (DTDEmptyAutomata_t465590953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (DTDAnyAutomata_t3633486160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (DTDInvalidAutomata_t1406553220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (DTDObjectModel_t1729680289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[23] = 
{
	DTDObjectModel_t1729680289::get_offset_of_factory_0(),
	DTDObjectModel_t1729680289::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1729680289::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1729680289::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1729680289::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1729680289::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1729680289::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1729680289::get_offset_of_peDecls_7(),
	DTDObjectModel_t1729680289::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1729680289::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1729680289::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1729680289::get_offset_of_resolver_11(),
	DTDObjectModel_t1729680289::get_offset_of_nameTable_12(),
	DTDObjectModel_t1729680289::get_offset_of_externalResources_13(),
	DTDObjectModel_t1729680289::get_offset_of_baseURI_14(),
	DTDObjectModel_t1729680289::get_offset_of_name_15(),
	DTDObjectModel_t1729680289::get_offset_of_publicId_16(),
	DTDObjectModel_t1729680289::get_offset_of_systemId_17(),
	DTDObjectModel_t1729680289::get_offset_of_intSubset_18(),
	DTDObjectModel_t1729680289::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1729680289::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1729680289::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1729680289::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (DictionaryBase_t52754249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (U3CU3Ec__Iterator3_t2072931442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[5] = 
{
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (DTDCollectionBase_t3926218464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[1] = 
{
	DTDCollectionBase_t3926218464::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (DTDElementDeclarationCollection_t222313714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (DTDAttListDeclarationCollection_t2220366188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (DTDEntityDeclarationCollection_t2250844513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (DTDNotationDeclarationCollection_t959292105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (DTDContentModel_t3264596611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[7] = 
{
	DTDContentModel_t3264596611::get_offset_of_root_5(),
	DTDContentModel_t3264596611::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t3264596611::get_offset_of_ownerElementName_7(),
	DTDContentModel_t3264596611::get_offset_of_elementName_8(),
	DTDContentModel_t3264596611::get_offset_of_orderType_9(),
	DTDContentModel_t3264596611::get_offset_of_childModels_10(),
	DTDContentModel_t3264596611::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (DTDContentModelCollection_t2798820000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	DTDContentModelCollection_t2798820000::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (DTDNode_t858560093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[5] = 
{
	DTDNode_t858560093::get_offset_of_root_0(),
	DTDNode_t858560093::get_offset_of_isInternalSubset_1(),
	DTDNode_t858560093::get_offset_of_baseURI_2(),
	DTDNode_t858560093::get_offset_of_lineNumber_3(),
	DTDNode_t858560093::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (DTDElementDeclaration_t1830540991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[6] = 
{
	DTDElementDeclaration_t1830540991::get_offset_of_root_5(),
	DTDElementDeclaration_t1830540991::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t1830540991::get_offset_of_name_7(),
	DTDElementDeclaration_t1830540991::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t1830540991::get_offset_of_isAny_9(),
	DTDElementDeclaration_t1830540991::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (DTDAttributeDefinition_t3434905422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[7] = 
{
	DTDAttributeDefinition_t3434905422::get_offset_of_name_5(),
	DTDAttributeDefinition_t3434905422::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3434905422::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_t3434905422::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_t3434905422::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_t3434905422::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_t3434905422::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (DTDAttListDeclaration_t3593159715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[3] = 
{
	DTDAttListDeclaration_t3593159715::get_offset_of_name_5(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (DTDEntityBase_t1228162861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[10] = 
{
	DTDEntityBase_t1228162861::get_offset_of_name_5(),
	DTDEntityBase_t1228162861::get_offset_of_publicId_6(),
	DTDEntityBase_t1228162861::get_offset_of_systemId_7(),
	DTDEntityBase_t1228162861::get_offset_of_literalValue_8(),
	DTDEntityBase_t1228162861::get_offset_of_replacementText_9(),
	DTDEntityBase_t1228162861::get_offset_of_uriString_10(),
	DTDEntityBase_t1228162861::get_offset_of_absUri_11(),
	DTDEntityBase_t1228162861::get_offset_of_isInvalid_12(),
	DTDEntityBase_t1228162861::get_offset_of_loadFailed_13(),
	DTDEntityBase_t1228162861::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (DTDEntityDeclaration_t811637416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[6] = 
{
	DTDEntityDeclaration_t811637416::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t811637416::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t811637416::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t811637416::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t811637416::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t811637416::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (DTDNotationDeclaration_t3702682588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	DTDNotationDeclaration_t3702682588::get_offset_of_name_5(),
	DTDNotationDeclaration_t3702682588::get_offset_of_localName_6(),
	DTDNotationDeclaration_t3702682588::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t3702682588::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t3702682588::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (DTDParameterEntityDeclarationCollection_t2844734410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[2] = 
{
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (DTDParameterEntityDeclaration_t3796253422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (DTDContentOrderType_t1195786655)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2741[4] = 
{
	DTDContentOrderType_t1195786655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (DTDAttributeOccurenceType_t2323614041)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[5] = 
{
	DTDAttributeOccurenceType_t2323614041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (DTDOccurence_t3140866896)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[5] = 
{
	DTDOccurence_t3140866896::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (DTDReader_t386081180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[14] = 
{
	DTDReader_t386081180::get_offset_of_currentInput_0(),
	DTDReader_t386081180::get_offset_of_parserInputStack_1(),
	DTDReader_t386081180::get_offset_of_nameBuffer_2(),
	DTDReader_t386081180::get_offset_of_nameLength_3(),
	DTDReader_t386081180::get_offset_of_nameCapacity_4(),
	DTDReader_t386081180::get_offset_of_valueBuffer_5(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t386081180::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t386081180::get_offset_of_normalization_9(),
	DTDReader_t386081180::get_offset_of_processingInternalSubset_10(),
	DTDReader_t386081180::get_offset_of_cachedPublicId_11(),
	DTDReader_t386081180::get_offset_of_cachedSystemId_12(),
	DTDReader_t386081180::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (DTDValidatingReader_t3946379043), -1, sizeof(DTDValidatingReader_t3946379043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[29] = 
{
	DTDValidatingReader_t3946379043::get_offset_of_reader_2(),
	DTDValidatingReader_t3946379043::get_offset_of_sourceTextReader_3(),
	DTDValidatingReader_t3946379043::get_offset_of_validatingReader_4(),
	DTDValidatingReader_t3946379043::get_offset_of_dtd_5(),
	DTDValidatingReader_t3946379043::get_offset_of_resolver_6(),
	DTDValidatingReader_t3946379043::get_offset_of_currentElement_7(),
	DTDValidatingReader_t3946379043::get_offset_of_attributes_8(),
	DTDValidatingReader_t3946379043::get_offset_of_attributeCount_9(),
	DTDValidatingReader_t3946379043::get_offset_of_currentAttribute_10(),
	DTDValidatingReader_t3946379043::get_offset_of_consumedAttribute_11(),
	DTDValidatingReader_t3946379043::get_offset_of_elementStack_12(),
	DTDValidatingReader_t3946379043::get_offset_of_automataStack_13(),
	DTDValidatingReader_t3946379043::get_offset_of_popScope_14(),
	DTDValidatingReader_t3946379043::get_offset_of_isStandalone_15(),
	DTDValidatingReader_t3946379043::get_offset_of_currentAutomata_16(),
	DTDValidatingReader_t3946379043::get_offset_of_previousAutomata_17(),
	DTDValidatingReader_t3946379043::get_offset_of_idList_18(),
	DTDValidatingReader_t3946379043::get_offset_of_missingIDReferences_19(),
	DTDValidatingReader_t3946379043::get_offset_of_nsmgr_20(),
	DTDValidatingReader_t3946379043::get_offset_of_currentTextValue_21(),
	DTDValidatingReader_t3946379043::get_offset_of_constructingTextValue_22(),
	DTDValidatingReader_t3946379043::get_offset_of_shouldResetCurrentTextValue_23(),
	DTDValidatingReader_t3946379043::get_offset_of_isSignificantWhitespace_24(),
	DTDValidatingReader_t3946379043::get_offset_of_isWhitespace_25(),
	DTDValidatingReader_t3946379043::get_offset_of_isText_26(),
	DTDValidatingReader_t3946379043::get_offset_of_attributeValueEntityStack_27(),
	DTDValidatingReader_t3946379043::get_offset_of_valueBuilder_28(),
	DTDValidatingReader_t3946379043::get_offset_of_whitespaceChars_29(),
	DTDValidatingReader_t3946379043_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (AttributeSlot_t3985135163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[6] = 
{
	AttributeSlot_t3985135163::get_offset_of_Name_0(),
	AttributeSlot_t3985135163::get_offset_of_LocalName_1(),
	AttributeSlot_t3985135163::get_offset_of_NS_2(),
	AttributeSlot_t3985135163::get_offset_of_Prefix_3(),
	AttributeSlot_t3985135163::get_offset_of_Value_4(),
	AttributeSlot_t3985135163::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (EntityHandling_t1047276436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[3] = 
{
	EntityHandling_t1047276436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (EntityResolvingXmlReader_t1267732406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[8] = 
{
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_2(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_source_3(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_context_4(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_resolver_5(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_handling_6(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_inside_attr_7(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_inside_attr_8(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_do_resolve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (NameTable_t3178203267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[3] = 
{
	NameTable_t3178203267::get_offset_of_count_0(),
	NameTable_t3178203267::get_offset_of_buckets_1(),
	NameTable_t3178203267::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (Entry_t3052280359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[4] = 
{
	Entry_t3052280359::get_offset_of_str_0(),
	Entry_t3052280359::get_offset_of_hash_1(),
	Entry_t3052280359::get_offset_of_len_2(),
	Entry_t3052280359::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (NamespaceHandling_t4087553436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2754[3] = 
{
	NamespaceHandling_t4087553436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (NewLineHandling_t850339274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2755[4] = 
{
	NewLineHandling_t850339274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (ReadState_t944984020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[6] = 
{
	ReadState_t944984020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (ValidationType_t4049928607)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[6] = 
{
	ValidationType_t4049928607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (WhitespaceHandling_t784045650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2758[4] = 
{
	WhitespaceHandling_t784045650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (WriteState_t3983380671)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2759[8] = 
{
	WriteState_t3983380671::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (XQueryConvert_t2841811029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (XmlAttribute_t1173852259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[4] = 
{
	XmlAttribute_t1173852259::get_offset_of_name_5(),
	XmlAttribute_t1173852259::get_offset_of_isDefault_6(),
	XmlAttribute_t1173852259::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t1173852259::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (XmlAttributeCollection_t2316283784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[2] = 
{
	XmlAttributeCollection_t2316283784::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t2316283784::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (XmlCDataSection_t3267478366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (XmlChar_t3816087079), -1, sizeof(XmlChar_t3816087079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2764[5] = 
{
	XmlChar_t3816087079_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t3816087079_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t3816087079_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t3816087079_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t3816087079_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (XmlCharacterData_t1167807131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[1] = 
{
	XmlCharacterData_t1167807131::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (XmlComment_t2476947920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (XmlConvert_t1981561327), -1, sizeof(XmlConvert_t1981561327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2767[8] = 
{
	XmlConvert_t1981561327_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1981561327_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (XmlDeclaration_t679870411), -1, sizeof(XmlDeclaration_t679870411_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[4] = 
{
	XmlDeclaration_t679870411::get_offset_of_encoding_6(),
	XmlDeclaration_t679870411::get_offset_of_standalone_7(),
	XmlDeclaration_t679870411::get_offset_of_version_8(),
	XmlDeclaration_t679870411_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (XmlDocument_t2837193595), -1, sizeof(XmlDocument_t2837193595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2769[19] = 
{
	XmlDocument_t2837193595_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_element_6(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t2837193595::get_offset_of_nameTable_8(),
	XmlDocument_t2837193595::get_offset_of_baseURI_9(),
	XmlDocument_t2837193595::get_offset_of_implementation_10(),
	XmlDocument_t2837193595::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t2837193595::get_offset_of_resolver_12(),
	XmlDocument_t2837193595::get_offset_of_idTable_13(),
	XmlDocument_t2837193595::get_offset_of_nameCache_14(),
	XmlDocument_t2837193595::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t2837193595::get_offset_of_schemaInfo_16(),
	XmlDocument_t2837193595::get_offset_of_loadMode_17(),
	XmlDocument_t2837193595::get_offset_of_NodeChanged_18(),
	XmlDocument_t2837193595::get_offset_of_NodeChanging_19(),
	XmlDocument_t2837193595::get_offset_of_NodeInserted_20(),
	XmlDocument_t2837193595::get_offset_of_NodeInserting_21(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoved_22(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoving_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (XmlDocumentFragment_t1323348855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[1] = 
{
	XmlDocumentFragment_t1323348855::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (XmlDocumentType_t4112370061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[3] = 
{
	XmlDocumentType_t4112370061::get_offset_of_entities_6(),
	XmlDocumentType_t4112370061::get_offset_of_notations_7(),
	XmlDocumentType_t4112370061::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (XmlElement_t561603118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[5] = 
{
	XmlElement_t561603118::get_offset_of_attributes_6(),
	XmlElement_t561603118::get_offset_of_name_7(),
	XmlElement_t561603118::get_offset_of_lastLinkedChild_8(),
	XmlElement_t561603118::get_offset_of_isNotEmpty_9(),
	XmlElement_t561603118::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (XmlEntity_t3308518401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[7] = 
{
	XmlEntity_t3308518401::get_offset_of_name_5(),
	XmlEntity_t3308518401::get_offset_of_NDATA_6(),
	XmlEntity_t3308518401::get_offset_of_publicId_7(),
	XmlEntity_t3308518401::get_offset_of_systemId_8(),
	XmlEntity_t3308518401::get_offset_of_baseUri_9(),
	XmlEntity_t3308518401::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t3308518401::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (XmlEntityReference_t1966808559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[2] = 
{
	XmlEntityReference_t1966808559::get_offset_of_entityName_6(),
	XmlEntityReference_t1966808559::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (XmlException_t1761730631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[5] = 
{
	XmlException_t1761730631::get_offset_of_lineNumber_11(),
	XmlException_t1761730631::get_offset_of_linePosition_12(),
	XmlException_t1761730631::get_offset_of_sourceUri_13(),
	XmlException_t1761730631::get_offset_of_res_14(),
	XmlException_t1761730631::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (XmlImplementation_t254178875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[1] = 
{
	XmlImplementation_t254178875::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (XmlStreamReader_t727818754), -1, sizeof(XmlStreamReader_t727818754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[2] = 
{
	XmlStreamReader_t727818754::get_offset_of_input_12(),
	XmlStreamReader_t727818754_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (NonBlockingStreamReader_t2495303928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[11] = 
{
	NonBlockingStreamReader_t2495303928::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t2495303928::get_offset_of_pos_4(),
	NonBlockingStreamReader_t2495303928::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t2495303928::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t2495303928::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t2495303928::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t2495303928::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t2495303928::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (XmlInputStream_t1691369434), -1, sizeof(XmlInputStream_t1691369434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2779[7] = 
{
	XmlInputStream_t1691369434_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t1691369434::get_offset_of_enc_2(),
	XmlInputStream_t1691369434::get_offset_of_stream_3(),
	XmlInputStream_t1691369434::get_offset_of_buffer_4(),
	XmlInputStream_t1691369434::get_offset_of_bufLength_5(),
	XmlInputStream_t1691369434::get_offset_of_bufPos_6(),
	XmlInputStream_t1691369434_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (XmlLinkedNode_t1437094927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[1] = 
{
	XmlLinkedNode_t1437094927::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (XmlNameEntry_t1073099671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[5] = 
{
	XmlNameEntry_t1073099671::get_offset_of_Prefix_0(),
	XmlNameEntry_t1073099671::get_offset_of_LocalName_1(),
	XmlNameEntry_t1073099671::get_offset_of_NS_2(),
	XmlNameEntry_t1073099671::get_offset_of_Hash_3(),
	XmlNameEntry_t1073099671::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (XmlNameEntryCache_t2890546907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[4] = 
{
	XmlNameEntryCache_t2890546907::get_offset_of_table_0(),
	XmlNameEntryCache_t2890546907::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t2890546907::get_offset_of_dummy_2(),
	XmlNameEntryCache_t2890546907::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (XmlNameTable_t71772148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (XmlNamedNodeMap_t2821286253), -1, sizeof(XmlNamedNodeMap_t2821286253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2784[4] = 
{
	XmlNamedNodeMap_t2821286253_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t2821286253::get_offset_of_parent_1(),
	XmlNamedNodeMap_t2821286253::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t2821286253::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (XmlNamespaceManager_t418790500), -1, sizeof(XmlNamespaceManager_t418790500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2785[9] = 
{
	XmlNamespaceManager_t418790500::get_offset_of_decls_0(),
	XmlNamespaceManager_t418790500::get_offset_of_declPos_1(),
	XmlNamespaceManager_t418790500::get_offset_of_scopes_2(),
	XmlNamespaceManager_t418790500::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t418790500::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t418790500::get_offset_of_count_5(),
	XmlNamespaceManager_t418790500::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t418790500::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t418790500_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (NsDecl_t3938094415)+ sizeof (RuntimeObject), sizeof(NsDecl_t3938094415_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2786[2] = 
{
	NsDecl_t3938094415::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsDecl_t3938094415::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (NsScope_t3958624705)+ sizeof (RuntimeObject), sizeof(NsScope_t3958624705_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2787[2] = 
{
	NsScope_t3958624705::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsScope_t3958624705::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (XmlNode_t3767805227), -1, sizeof(XmlNode_t3767805227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2788[5] = 
{
	XmlNode_t3767805227_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t3767805227::get_offset_of_ownerDocument_1(),
	XmlNode_t3767805227::get_offset_of_parentNode_2(),
	XmlNode_t3767805227::get_offset_of_childNodes_3(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (EmptyNodeList_t139615908), -1, sizeof(EmptyNodeList_t139615908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[1] = 
{
	EmptyNodeList_t139615908_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (XmlNodeChangedAction_t3227731597)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[4] = 
{
	XmlNodeChangedAction_t3227731597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (XmlNodeChangedEventArgs_t2486095928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[6] = 
{
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (XmlNodeList_t2551693786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (XmlNodeListChildren_t1082692789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	XmlNodeListChildren_t1082692789::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (Enumerator_t97922292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[3] = 
{
	Enumerator_t97922292::get_offset_of_parent_0(),
	Enumerator_t97922292::get_offset_of_currentChild_1(),
	Enumerator_t97922292::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (XmlNodeType_t1672767151)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2795[19] = 
{
	XmlNodeType_t1672767151::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (XmlNotation_t1476580686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[4] = 
{
	XmlNotation_t1476580686::get_offset_of_localName_5(),
	XmlNotation_t1476580686::get_offset_of_publicId_6(),
	XmlNotation_t1476580686::get_offset_of_systemId_7(),
	XmlNotation_t1476580686::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (XmlParserContext_t2544895291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[13] = 
{
	XmlParserContext_t2544895291::get_offset_of_baseURI_0(),
	XmlParserContext_t2544895291::get_offset_of_docTypeName_1(),
	XmlParserContext_t2544895291::get_offset_of_encoding_2(),
	XmlParserContext_t2544895291::get_offset_of_internalSubset_3(),
	XmlParserContext_t2544895291::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2544895291::get_offset_of_nameTable_5(),
	XmlParserContext_t2544895291::get_offset_of_publicID_6(),
	XmlParserContext_t2544895291::get_offset_of_systemID_7(),
	XmlParserContext_t2544895291::get_offset_of_xmlLang_8(),
	XmlParserContext_t2544895291::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2544895291::get_offset_of_contextItems_10(),
	XmlParserContext_t2544895291::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2544895291::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (ContextItem_t3112052795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[3] = 
{
	ContextItem_t3112052795::get_offset_of_BaseURI_0(),
	ContextItem_t3112052795::get_offset_of_XmlLang_1(),
	ContextItem_t3112052795::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (XmlParserInput_t2182411204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[5] = 
{
	XmlParserInput_t2182411204::get_offset_of_sourceStack_0(),
	XmlParserInput_t2182411204::get_offset_of_source_1(),
	XmlParserInput_t2182411204::get_offset_of_has_peek_2(),
	XmlParserInput_t2182411204::get_offset_of_peek_char_3(),
	XmlParserInput_t2182411204::get_offset_of_allowTextDecl_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
