﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemTimer
struct  ItemTimer_t3839766396  : public Il2CppObject
{
public:
	// System.Int64 ItemTimer::id
	int64_t ___id_0;
	// System.Int64 ItemTimer::event_id
	int64_t ___event_id_1;
	// System.String ItemTimer::itemID
	String_t* ___itemID_2;
	// System.DateTime ItemTimer::start_time
	DateTime_t693205669  ___start_time_3;
	// System.DateTime ItemTimer::finish_time
	DateTime_t693205669  ___finish_time_4;
	// System.String ItemTimer::name
	String_t* ___name_5;
	// System.String ItemTimer::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_event_id_1() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___event_id_1)); }
	inline int64_t get_event_id_1() const { return ___event_id_1; }
	inline int64_t* get_address_of_event_id_1() { return &___event_id_1; }
	inline void set_event_id_1(int64_t value)
	{
		___event_id_1 = value;
	}

	inline static int32_t get_offset_of_itemID_2() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___itemID_2)); }
	inline String_t* get_itemID_2() const { return ___itemID_2; }
	inline String_t** get_address_of_itemID_2() { return &___itemID_2; }
	inline void set_itemID_2(String_t* value)
	{
		___itemID_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemID_2, value);
	}

	inline static int32_t get_offset_of_start_time_3() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___start_time_3)); }
	inline DateTime_t693205669  get_start_time_3() const { return ___start_time_3; }
	inline DateTime_t693205669 * get_address_of_start_time_3() { return &___start_time_3; }
	inline void set_start_time_3(DateTime_t693205669  value)
	{
		___start_time_3 = value;
	}

	inline static int32_t get_offset_of_finish_time_4() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___finish_time_4)); }
	inline DateTime_t693205669  get_finish_time_4() const { return ___finish_time_4; }
	inline DateTime_t693205669 * get_address_of_finish_time_4() { return &___finish_time_4; }
	inline void set_finish_time_4(DateTime_t693205669  value)
	{
		___finish_time_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(ItemTimer_t3839766396, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier(&___type_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
