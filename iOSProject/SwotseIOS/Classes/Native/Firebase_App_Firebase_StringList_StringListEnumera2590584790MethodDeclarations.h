﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.StringList/StringListEnumerator
struct StringListEnumerator_t2590584790;
// Firebase.StringList
struct StringList_t2332200693;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_StringList2332200693.h"

// System.Void Firebase.StringList/StringListEnumerator::.ctor(Firebase.StringList)
extern "C"  void StringListEnumerator__ctor_m2709358997 (StringListEnumerator_t2590584790 * __this, StringList_t2332200693 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.StringList/StringListEnumerator::get_Current()
extern "C"  String_t* StringListEnumerator_get_Current_m1868622004 (StringListEnumerator_t2590584790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.StringList/StringListEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * StringListEnumerator_System_Collections_IEnumerator_get_Current_m4106985007 (StringListEnumerator_t2590584790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.StringList/StringListEnumerator::MoveNext()
extern "C"  bool StringListEnumerator_MoveNext_m1858710423 (StringListEnumerator_t2590584790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList/StringListEnumerator::Reset()
extern "C"  void StringListEnumerator_Reset_m1079173106 (StringListEnumerator_t2590584790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.StringList/StringListEnumerator::Dispose()
extern "C"  void StringListEnumerator_Dispose_m4065242420 (StringListEnumerator_t2590584790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
