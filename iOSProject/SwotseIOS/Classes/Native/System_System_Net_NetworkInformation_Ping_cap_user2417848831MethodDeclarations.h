﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Ping/cap_user_data_t
struct cap_user_data_t_t2417848831;
struct cap_user_data_t_t2417848831_marshaled_pinvoke;
struct cap_user_data_t_t2417848831_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct cap_user_data_t_t2417848831;
struct cap_user_data_t_t2417848831_marshaled_pinvoke;

extern "C" void cap_user_data_t_t2417848831_marshal_pinvoke(const cap_user_data_t_t2417848831& unmarshaled, cap_user_data_t_t2417848831_marshaled_pinvoke& marshaled);
extern "C" void cap_user_data_t_t2417848831_marshal_pinvoke_back(const cap_user_data_t_t2417848831_marshaled_pinvoke& marshaled, cap_user_data_t_t2417848831& unmarshaled);
extern "C" void cap_user_data_t_t2417848831_marshal_pinvoke_cleanup(cap_user_data_t_t2417848831_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct cap_user_data_t_t2417848831;
struct cap_user_data_t_t2417848831_marshaled_com;

extern "C" void cap_user_data_t_t2417848831_marshal_com(const cap_user_data_t_t2417848831& unmarshaled, cap_user_data_t_t2417848831_marshaled_com& marshaled);
extern "C" void cap_user_data_t_t2417848831_marshal_com_back(const cap_user_data_t_t2417848831_marshaled_com& marshaled, cap_user_data_t_t2417848831& unmarshaled);
extern "C" void cap_user_data_t_t2417848831_marshal_com_cleanup(cap_user_data_t_t2417848831_marshaled_com& marshaled);
