﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceContentChanger
struct  AllianceContentChanger_t4213706348  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AllianceContentChanger::informationSection
	GameObject_t1756533147 * ___informationSection_2;
	// UnityEngine.GameObject AllianceContentChanger::contentSection
	GameObject_t1756533147 * ___contentSection_3;
	// UnityEngine.UI.Text AllianceContentChanger::contentLabel
	Text_t356221433 * ___contentLabel_4;
	// UnityEngine.GameObject AllianceContentChanger::membersContent
	GameObject_t1756533147 * ___membersContent_5;
	// UnityEngine.GameObject AllianceContentChanger::reportsContent
	GameObject_t1756533147 * ___reportsContent_6;
	// UnityEngine.GameObject AllianceContentChanger::eventsContent
	GameObject_t1756533147 * ___eventsContent_7;
	// UnityEngine.GameObject AllianceContentChanger::alliancesContent
	GameObject_t1756533147 * ___alliancesContent_8;
	// UnityEngine.GameObject AllianceContentChanger::inviteContent
	GameObject_t1756533147 * ___inviteContent_9;
	// UnityEngine.GameObject AllianceContentChanger::applicationsContent
	GameObject_t1756533147 * ___applicationsContent_10;
	// UnityEngine.GameObject AllianceContentChanger::permissionsContent
	GameObject_t1756533147 * ___permissionsContent_11;
	// UnityEngine.GameObject AllianceContentChanger::guidelinesContent
	GameObject_t1756533147 * ___guidelinesContent_12;
	// UnityEngine.GameObject AllianceContentChanger::messageContent
	GameObject_t1756533147 * ___messageContent_13;
	// UnityEngine.GameObject[] AllianceContentChanger::contents
	GameObjectU5BU5D_t3057952154* ___contents_14;
	// ConfirmationEvent AllianceContentChanger::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_15;

public:
	inline static int32_t get_offset_of_informationSection_2() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___informationSection_2)); }
	inline GameObject_t1756533147 * get_informationSection_2() const { return ___informationSection_2; }
	inline GameObject_t1756533147 ** get_address_of_informationSection_2() { return &___informationSection_2; }
	inline void set_informationSection_2(GameObject_t1756533147 * value)
	{
		___informationSection_2 = value;
		Il2CppCodeGenWriteBarrier(&___informationSection_2, value);
	}

	inline static int32_t get_offset_of_contentSection_3() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___contentSection_3)); }
	inline GameObject_t1756533147 * get_contentSection_3() const { return ___contentSection_3; }
	inline GameObject_t1756533147 ** get_address_of_contentSection_3() { return &___contentSection_3; }
	inline void set_contentSection_3(GameObject_t1756533147 * value)
	{
		___contentSection_3 = value;
		Il2CppCodeGenWriteBarrier(&___contentSection_3, value);
	}

	inline static int32_t get_offset_of_contentLabel_4() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___contentLabel_4)); }
	inline Text_t356221433 * get_contentLabel_4() const { return ___contentLabel_4; }
	inline Text_t356221433 ** get_address_of_contentLabel_4() { return &___contentLabel_4; }
	inline void set_contentLabel_4(Text_t356221433 * value)
	{
		___contentLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___contentLabel_4, value);
	}

	inline static int32_t get_offset_of_membersContent_5() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___membersContent_5)); }
	inline GameObject_t1756533147 * get_membersContent_5() const { return ___membersContent_5; }
	inline GameObject_t1756533147 ** get_address_of_membersContent_5() { return &___membersContent_5; }
	inline void set_membersContent_5(GameObject_t1756533147 * value)
	{
		___membersContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___membersContent_5, value);
	}

	inline static int32_t get_offset_of_reportsContent_6() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___reportsContent_6)); }
	inline GameObject_t1756533147 * get_reportsContent_6() const { return ___reportsContent_6; }
	inline GameObject_t1756533147 ** get_address_of_reportsContent_6() { return &___reportsContent_6; }
	inline void set_reportsContent_6(GameObject_t1756533147 * value)
	{
		___reportsContent_6 = value;
		Il2CppCodeGenWriteBarrier(&___reportsContent_6, value);
	}

	inline static int32_t get_offset_of_eventsContent_7() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___eventsContent_7)); }
	inline GameObject_t1756533147 * get_eventsContent_7() const { return ___eventsContent_7; }
	inline GameObject_t1756533147 ** get_address_of_eventsContent_7() { return &___eventsContent_7; }
	inline void set_eventsContent_7(GameObject_t1756533147 * value)
	{
		___eventsContent_7 = value;
		Il2CppCodeGenWriteBarrier(&___eventsContent_7, value);
	}

	inline static int32_t get_offset_of_alliancesContent_8() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___alliancesContent_8)); }
	inline GameObject_t1756533147 * get_alliancesContent_8() const { return ___alliancesContent_8; }
	inline GameObject_t1756533147 ** get_address_of_alliancesContent_8() { return &___alliancesContent_8; }
	inline void set_alliancesContent_8(GameObject_t1756533147 * value)
	{
		___alliancesContent_8 = value;
		Il2CppCodeGenWriteBarrier(&___alliancesContent_8, value);
	}

	inline static int32_t get_offset_of_inviteContent_9() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___inviteContent_9)); }
	inline GameObject_t1756533147 * get_inviteContent_9() const { return ___inviteContent_9; }
	inline GameObject_t1756533147 ** get_address_of_inviteContent_9() { return &___inviteContent_9; }
	inline void set_inviteContent_9(GameObject_t1756533147 * value)
	{
		___inviteContent_9 = value;
		Il2CppCodeGenWriteBarrier(&___inviteContent_9, value);
	}

	inline static int32_t get_offset_of_applicationsContent_10() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___applicationsContent_10)); }
	inline GameObject_t1756533147 * get_applicationsContent_10() const { return ___applicationsContent_10; }
	inline GameObject_t1756533147 ** get_address_of_applicationsContent_10() { return &___applicationsContent_10; }
	inline void set_applicationsContent_10(GameObject_t1756533147 * value)
	{
		___applicationsContent_10 = value;
		Il2CppCodeGenWriteBarrier(&___applicationsContent_10, value);
	}

	inline static int32_t get_offset_of_permissionsContent_11() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___permissionsContent_11)); }
	inline GameObject_t1756533147 * get_permissionsContent_11() const { return ___permissionsContent_11; }
	inline GameObject_t1756533147 ** get_address_of_permissionsContent_11() { return &___permissionsContent_11; }
	inline void set_permissionsContent_11(GameObject_t1756533147 * value)
	{
		___permissionsContent_11 = value;
		Il2CppCodeGenWriteBarrier(&___permissionsContent_11, value);
	}

	inline static int32_t get_offset_of_guidelinesContent_12() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___guidelinesContent_12)); }
	inline GameObject_t1756533147 * get_guidelinesContent_12() const { return ___guidelinesContent_12; }
	inline GameObject_t1756533147 ** get_address_of_guidelinesContent_12() { return &___guidelinesContent_12; }
	inline void set_guidelinesContent_12(GameObject_t1756533147 * value)
	{
		___guidelinesContent_12 = value;
		Il2CppCodeGenWriteBarrier(&___guidelinesContent_12, value);
	}

	inline static int32_t get_offset_of_messageContent_13() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___messageContent_13)); }
	inline GameObject_t1756533147 * get_messageContent_13() const { return ___messageContent_13; }
	inline GameObject_t1756533147 ** get_address_of_messageContent_13() { return &___messageContent_13; }
	inline void set_messageContent_13(GameObject_t1756533147 * value)
	{
		___messageContent_13 = value;
		Il2CppCodeGenWriteBarrier(&___messageContent_13, value);
	}

	inline static int32_t get_offset_of_contents_14() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___contents_14)); }
	inline GameObjectU5BU5D_t3057952154* get_contents_14() const { return ___contents_14; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_contents_14() { return &___contents_14; }
	inline void set_contents_14(GameObjectU5BU5D_t3057952154* value)
	{
		___contents_14 = value;
		Il2CppCodeGenWriteBarrier(&___contents_14, value);
	}

	inline static int32_t get_offset_of_confirmed_15() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348, ___confirmed_15)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_15() const { return ___confirmed_15; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_15() { return &___confirmed_15; }
	inline void set_confirmed_15(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_15 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_15, value);
	}
};

struct AllianceContentChanger_t4213706348_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AllianceContentChanger::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_16() { return static_cast<int32_t>(offsetof(AllianceContentChanger_t4213706348_StaticFields, ___U3CU3Ef__switchU24map4_16)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_16() const { return ___U3CU3Ef__switchU24map4_16; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_16() { return &___U3CU3Ef__switchU24map4_16; }
	inline void set_U3CU3Ef__switchU24map4_16(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
