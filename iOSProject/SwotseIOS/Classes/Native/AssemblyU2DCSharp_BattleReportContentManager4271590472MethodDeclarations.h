﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportContentManager
struct BattleReportContentManager_t4271590472;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_JSONObject1971882247.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BattleReportContentManager::.ctor()
extern "C"  void BattleReportContentManager__ctor_m2838330329 (BattleReportContentManager_t4271590472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::add_onGetBattleReport(SimpleEvent)
extern "C"  void BattleReportContentManager_add_onGetBattleReport_m1270111024 (BattleReportContentManager_t4271590472 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::remove_onGetBattleReport(SimpleEvent)
extern "C"  void BattleReportContentManager_remove_onGetBattleReport_m272821061 (BattleReportContentManager_t4271590472 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::OnEnable()
extern "C"  void BattleReportContentManager_OnEnable_m4033734089 (BattleReportContentManager_t4271590472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::CloseReport()
extern "C"  void BattleReportContentManager_CloseReport_m1552344597 (BattleReportContentManager_t4271590472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BattleReportContentManager::GetBattleReport()
extern "C"  Il2CppObject * BattleReportContentManager_GetBattleReport_m3792656377 (BattleReportContentManager_t4271590472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::ParseBattleReport(JSONObject)
extern "C"  void BattleReportContentManager_ParseBattleReport_m2495112669 (BattleReportContentManager_t4271590472 * __this, JSONObject_t1971882247 * ___battleReport0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::GotBattleReport(System.Object,System.String)
extern "C"  void BattleReportContentManager_GotBattleReport_m980592293 (BattleReportContentManager_t4271590472 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager::OpenBattleLog()
extern "C"  void BattleReportContentManager_OpenBattleLog_m1162918667 (BattleReportContentManager_t4271590472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
