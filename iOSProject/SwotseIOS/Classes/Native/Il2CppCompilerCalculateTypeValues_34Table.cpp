﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// ResourceManager
struct ResourceManager_t484397614;
// ReportManager
struct ReportManager_t1122460921;
// JSONObject
struct JSONObject_t1339445639;
// WebSocket
struct WebSocket_t1645401340;
// System.String
struct String_t;
// System.Collections.Specialized.OrderedDictionary
struct OrderedDictionary_t2617496293;
// SimpleEvent
struct SimpleEvent_t129249603;
// UnitsManager
struct UnitsManager_t4062574081;
// ArmyGeneralModel
struct ArmyGeneralModel_t737750221;
// UnitsModel
struct UnitsModel_t3847956398;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t913674750;
// System.Collections.Generic.Dictionary`2<System.Int64,ArmyGeneralModel>
struct Dictionary_2_t1800390005;
// TreasureManager
struct TreasureManager_t110095690;
// CityManager
struct CityManager_t2587329200;
// BattleManager
struct BattleManager_t4022130644;
// LevelLoadManager
struct LevelLoadManager_t362334468;
// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel>
struct Dictionary_2_t2441201299;
// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel>
struct Dictionary_2_t3367445707;
// System.Collections.Generic.List`1<TutorialPageModel>
struct List_1_t1672770702;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// GameManager
struct GameManager_t1536523654;
// BuildingsManager
struct BuildingsManager_t3721263023;
// MyCityModel
struct MyCityModel_t3961736920;
// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel>
struct Dictionary_2_t729409408;
// AllianceManager
struct AllianceManager_t344187419;
// LoginManager
struct LoginManager_t1249555276;
// UserModel
struct UserModel_t1353931605;
// System.Collections.Generic.Dictionary`2<System.Int64,InviteReportModel>
struct Dictionary_2_t1700275496;
// System.Collections.Generic.Dictionary`2<System.Int64,BattleReportHeader>
struct Dictionary_2_t4087798327;
// System.Collections.Generic.Dictionary`2<System.Int64,MessageHeader>
struct Dictionary_2_t243444911;
// System.Collections.Generic.Dictionary`2<System.Int64,SystemReportModel>
struct Dictionary_2_t4009803018;
// System.Collections.Generic.Dictionary`2<System.Int64,UnitTimer>
struct Dictionary_2_t4184190554;
// System.Collections.Generic.Dictionary`2<System.Int64,BuildingTimer>
struct Dictionary_2_t1436714094;
// System.Collections.Generic.Dictionary`2<System.Int64,ArmyTimer>
struct Dictionary_2_t50524311;
// System.Collections.Generic.Dictionary`2<System.Int64,ItemTimer>
struct Dictionary_2_t3380876359;
// System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer>
struct Dictionary_2_t1123123525;
// PlayerManager
struct PlayerManager_t1349889689;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// TutorialPageModel
struct TutorialPageModel_t200695960;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// WorldFieldModel
struct WorldFieldModel_t2417974361;
// ResourcesModel
struct ResourcesModel_t2533508513;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// ADManager
struct ADManager_t261308543;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t3521823603;
// GCMManager
struct GCMManager_t2058212271;
// WindowInstanceManager
struct WindowInstanceManager_t3234774687;
// CityChangerContentManager
struct CityChangerContentManager_t1075607021;
// PanelKnightsTableController
struct PanelKnightsTableController_t3023536378;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CREADISNEWWORLDMAPEVENTU3EC__ITERATOR6_T1752417698_H
#define U3CREADISNEWWORLDMAPEVENTU3EC__ITERATOR6_T1752417698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6
struct  U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewWorldMapEvent>c__Iterator6::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWWORLDMAPEVENTU3EC__ITERATOR6_T1752417698_H
#ifndef U3CREADISNEWCITYEVENTU3EC__ITERATOR7_T3353020914_H
#define U3CREADISNEWCITYEVENTU3EC__ITERATOR7_T3353020914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewCityEvent>c__Iterator7
struct  U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewCityEvent>c__Iterator7::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewCityEvent>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewCityEvent>c__Iterator7::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewCityEvent>c__Iterator7::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewCityEvent>c__Iterator7::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewCityEvent>c__Iterator7::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWCITYEVENTU3EC__ITERATOR7_T3353020914_H
#ifndef U3CREADISNEWCUSTOMEVENTU3EC__ITERATOR5_T2961040443_H
#define U3CREADISNEWCUSTOMEVENTU3EC__ITERATOR5_T2961040443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewCustomEvent>c__Iterator5
struct  U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewCustomEvent>c__Iterator5::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWCUSTOMEVENTU3EC__ITERATOR5_T2961040443_H
#ifndef U3CREADISNEWBUILDINGEVENTU3EC__ITERATOR3_T2099043782_H
#define U3CREADISNEWBUILDINGEVENTU3EC__ITERATOR3_T2099043782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3
struct  U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewBuildingEvent>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWBUILDINGEVENTU3EC__ITERATOR3_T2099043782_H
#ifndef U3CREADISNEWUNITEVENTU3EC__ITERATOR4_T182794934_H
#define U3CREADISNEWUNITEVENTU3EC__ITERATOR4_T182794934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewUnitEvent>c__Iterator4
struct  U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewUnitEvent>c__Iterator4::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWUNITEVENTU3EC__ITERATOR4_T182794934_H
#ifndef U3CREADISUNREADARMYEVENTSU3EC__ITERATOR8_T1817838790_H
#define U3CREADISUNREADARMYEVENTSU3EC__ITERATOR8_T1817838790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8
struct  U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadArmyEvents>c__Iterator8::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADARMYEVENTSU3EC__ITERATOR8_T1817838790_H
#ifndef U3CREADISUNREADWORLDMAPEVENTSU3EC__ITERATORC_T3586856441_H
#define U3CREADISUNREADWORLDMAPEVENTSU3EC__ITERATORC_T3586856441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC
struct  U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadWorldMapEvents>c__IteratorC::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADWORLDMAPEVENTSU3EC__ITERATORC_T3586856441_H
#ifndef U3CREADISUNREADNEWCITYEVENTSU3EC__ITERATORD_T1396976694_H
#define U3CREADISUNREADNEWCITYEVENTSU3EC__ITERATORD_T1396976694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD
struct  U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadNewCityEvents>c__IteratorD::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADNEWCITYEVENTSU3EC__ITERATORD_T1396976694_H
#ifndef U3CREADISUNREADCUSTOMEVENTSU3EC__ITERATORB_T3770294009_H
#define U3CREADISUNREADCUSTOMEVENTSU3EC__ITERATORB_T3770294009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB
struct  U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadCustomEvents>c__IteratorB::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADCUSTOMEVENTSU3EC__ITERATORB_T3770294009_H
#ifndef U3CREADISUNREADBUILDINGEVENTSU3EC__ITERATOR9_T2308996025_H
#define U3CREADISUNREADBUILDINGEVENTSU3EC__ITERATOR9_T2308996025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9
struct  U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadBuildingEvents>c__Iterator9::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADBUILDINGEVENTSU3EC__ITERATOR9_T2308996025_H
#ifndef U3CREADISUNREADUNITEVENTSU3EC__ITERATORA_T3412693977_H
#define U3CREADISUNREADUNITEVENTSU3EC__ITERATORA_T3412693977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA
struct  U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsUnreadUnitEvents>c__IteratorA::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISUNREADUNITEVENTSU3EC__ITERATORA_T3412693977_H
#ifndef U3CREADISNEWARMYEVENTU3EC__ITERATOR2_T2625897388_H
#define U3CREADISNEWARMYEVENTU3EC__ITERATOR2_T2625897388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<ReadIsNewArmyEvent>c__Iterator2
struct  U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// ResourceManager ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::$this
	ResourceManager_t484397614 * ___U24this_2;
	// System.Object ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 ResourceManager/<ReadIsNewArmyEvent>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U24this_2)); }
	inline ResourceManager_t484397614 * get_U24this_2() const { return ___U24this_2; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ResourceManager_t484397614 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADISNEWARMYEVENTU3EC__ITERATOR2_T2625897388_H
#ifndef U3CGETARMYTIMERSU3EC__ITERATOR8_T2156596326_H
#define U3CGETARMYTIMERSU3EC__ITERATOR8_T2156596326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetArmyTimers>c__Iterator8
struct  U3CGetArmyTimersU3Ec__Iterator8_t2156596326  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetArmyTimers>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean ReportManager/<GetArmyTimers>c__Iterator8::initial
	bool ___initial_1;
	// ReportManager ReportManager/<GetArmyTimers>c__Iterator8::$this
	ReportManager_t1122460921 * ___U24this_2;
	// System.Object ReportManager/<GetArmyTimers>c__Iterator8::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<GetArmyTimers>c__Iterator8::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<GetArmyTimers>c__Iterator8::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___U24this_2)); }
	inline ReportManager_t1122460921 * get_U24this_2() const { return ___U24this_2; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ReportManager_t1122460921 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetArmyTimersU3Ec__Iterator8_t2156596326, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETARMYTIMERSU3EC__ITERATOR8_T2156596326_H
#ifndef U3CGETITEMTIMERSU3EC__ITERATOR9_T2913865839_H
#define U3CGETITEMTIMERSU3EC__ITERATOR9_T2913865839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetItemTimers>c__Iterator9
struct  U3CGetItemTimersU3Ec__Iterator9_t2913865839  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetItemTimers>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean ReportManager/<GetItemTimers>c__Iterator9::initial
	bool ___initial_1;
	// ReportManager ReportManager/<GetItemTimers>c__Iterator9::$this
	ReportManager_t1122460921 * ___U24this_2;
	// System.Object ReportManager/<GetItemTimers>c__Iterator9::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<GetItemTimers>c__Iterator9::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<GetItemTimers>c__Iterator9::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___U24this_2)); }
	inline ReportManager_t1122460921 * get_U24this_2() const { return ___U24this_2; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ReportManager_t1122460921 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetItemTimersU3Ec__Iterator9_t2913865839, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETITEMTIMERSU3EC__ITERATOR9_T2913865839_H
#ifndef U3CGETBUILDINGTIMERSU3EC__ITERATOR7_T589680364_H
#define U3CGETBUILDINGTIMERSU3EC__ITERATOR7_T589680364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetBuildingTimers>c__Iterator7
struct  U3CGetBuildingTimersU3Ec__Iterator7_t589680364  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetBuildingTimers>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean ReportManager/<GetBuildingTimers>c__Iterator7::initial
	bool ___initial_1;
	// ReportManager ReportManager/<GetBuildingTimers>c__Iterator7::$this
	ReportManager_t1122460921 * ___U24this_2;
	// System.Object ReportManager/<GetBuildingTimers>c__Iterator7::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<GetBuildingTimers>c__Iterator7::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<GetBuildingTimers>c__Iterator7::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___U24this_2)); }
	inline ReportManager_t1122460921 * get_U24this_2() const { return ___U24this_2; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ReportManager_t1122460921 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetBuildingTimersU3Ec__Iterator7_t589680364, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBUILDINGTIMERSU3EC__ITERATOR7_T589680364_H
#ifndef U3CGETUNREADALLIANCEBATTLEREPORTSU3EC__ITERATOR5_T385495556_H
#define U3CGETUNREADALLIANCEBATTLEREPORTSU3EC__ITERATOR5_T385495556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5
struct  U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ReportManager ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5::$this
	ReportManager_t1122460921 * ___U24this_1;
	// System.Object ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5::$disposing
	bool ___U24disposing_3;
	// System.Int32 ReportManager/<GetUnreadAllianceBattleReports>c__Iterator5::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556, ___U24this_1)); }
	inline ReportManager_t1122460921 * get_U24this_1() const { return ___U24this_1; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ReportManager_t1122460921 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETUNREADALLIANCEBATTLEREPORTSU3EC__ITERATOR5_T385495556_H
#ifndef U3CGETUNITTIMERSU3EC__ITERATOR6_T3416549667_H
#define U3CGETUNITTIMERSU3EC__ITERATOR6_T3416549667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetUnitTimers>c__Iterator6
struct  U3CGetUnitTimersU3Ec__Iterator6_t3416549667  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetUnitTimers>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean ReportManager/<GetUnitTimers>c__Iterator6::initial
	bool ___initial_1;
	// ReportManager ReportManager/<GetUnitTimers>c__Iterator6::$this
	ReportManager_t1122460921 * ___U24this_2;
	// System.Object ReportManager/<GetUnitTimers>c__Iterator6::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<GetUnitTimers>c__Iterator6::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<GetUnitTimers>c__Iterator6::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___U24this_2)); }
	inline ReportManager_t1122460921 * get_U24this_2() const { return ___U24this_2; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ReportManager_t1122460921 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetUnitTimersU3Ec__Iterator6_t3416549667, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETUNITTIMERSU3EC__ITERATOR6_T3416549667_H
#ifndef U3CGETWORLDMAPTIMERSU3EC__ITERATORA_T706540611_H
#define U3CGETWORLDMAPTIMERSU3EC__ITERATORA_T706540611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetWorldMapTimers>c__IteratorA
struct  U3CGetWorldMapTimersU3Ec__IteratorA_t706540611  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetWorldMapTimers>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean ReportManager/<GetWorldMapTimers>c__IteratorA::initial
	bool ___initial_1;
	// ReportManager ReportManager/<GetWorldMapTimers>c__IteratorA::$this
	ReportManager_t1122460921 * ___U24this_2;
	// System.Object ReportManager/<GetWorldMapTimers>c__IteratorA::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<GetWorldMapTimers>c__IteratorA::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<GetWorldMapTimers>c__IteratorA::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___U24this_2)); }
	inline ReportManager_t1122460921 * get_U24this_2() const { return ___U24this_2; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ReportManager_t1122460921 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetWorldMapTimersU3Ec__IteratorA_t706540611, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETWORLDMAPTIMERSU3EC__ITERATORA_T706540611_H
#ifndef U3CGETINITIALREOURCESFORCITYU3EC__ITERATOR0_T2189768509_H
#define U3CGETINITIALREOURCESFORCITYU3EC__ITERATOR0_T2189768509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<GetInitialReourcesForCity>c__Iterator0
struct  U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ResourceManager/<GetInitialReourcesForCity>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ResourceManager ResourceManager/<GetInitialReourcesForCity>c__Iterator0::$this
	ResourceManager_t484397614 * ___U24this_1;
	// System.Object ResourceManager/<GetInitialReourcesForCity>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ResourceManager/<GetInitialReourcesForCity>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ResourceManager/<GetInitialReourcesForCity>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509, ___U24this_1)); }
	inline ResourceManager_t484397614 * get_U24this_1() const { return ___U24this_1; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ResourceManager_t484397614 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALREOURCESFORCITYU3EC__ITERATOR0_T2189768509_H
#ifndef U3CGETRESOURCESU3EC__ITERATOR1_T486193133_H
#define U3CGETRESOURCESU3EC__ITERATOR1_T486193133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/<getResources>c__Iterator1
struct  U3CgetResourcesU3Ec__Iterator1_t486193133  : public RuntimeObject
{
public:
	// JSONObject ResourceManager/<getResources>c__Iterator1::<obj>__0
	JSONObject_t1339445639 * ___U3CobjU3E__0_0;
	// WebSocket ResourceManager/<getResources>c__Iterator1::<w>__0
	WebSocket_t1645401340 * ___U3CwU3E__0_1;
	// System.String ResourceManager/<getResources>c__Iterator1::<reply>__1
	String_t* ___U3CreplyU3E__1_2;
	// ResourceManager ResourceManager/<getResources>c__Iterator1::$this
	ResourceManager_t484397614 * ___U24this_3;
	// System.Object ResourceManager/<getResources>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ResourceManager/<getResources>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 ResourceManager/<getResources>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CobjU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U3CobjU3E__0_0)); }
	inline JSONObject_t1339445639 * get_U3CobjU3E__0_0() const { return ___U3CobjU3E__0_0; }
	inline JSONObject_t1339445639 ** get_address_of_U3CobjU3E__0_0() { return &___U3CobjU3E__0_0; }
	inline void set_U3CobjU3E__0_0(JSONObject_t1339445639 * value)
	{
		___U3CobjU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U3CwU3E__0_1)); }
	inline WebSocket_t1645401340 * get_U3CwU3E__0_1() const { return ___U3CwU3E__0_1; }
	inline WebSocket_t1645401340 ** get_address_of_U3CwU3E__0_1() { return &___U3CwU3E__0_1; }
	inline void set_U3CwU3E__0_1(WebSocket_t1645401340 * value)
	{
		___U3CwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CreplyU3E__1_2() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U3CreplyU3E__1_2)); }
	inline String_t* get_U3CreplyU3E__1_2() const { return ___U3CreplyU3E__1_2; }
	inline String_t** get_address_of_U3CreplyU3E__1_2() { return &___U3CreplyU3E__1_2; }
	inline void set_U3CreplyU3E__1_2(String_t* value)
	{
		___U3CreplyU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreplyU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U24this_3)); }
	inline ResourceManager_t484397614 * get_U24this_3() const { return ___U24this_3; }
	inline ResourceManager_t484397614 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ResourceManager_t484397614 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CgetResourcesU3Ec__Iterator1_t486193133, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETRESOURCESU3EC__ITERATOR1_T486193133_H
#ifndef U3CDELETESCOUTREPORTU3EC__ITERATORD_T2460383078_H
#define U3CDELETESCOUTREPORTU3EC__ITERATORD_T2460383078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<DeleteScoutReport>c__IteratorD
struct  U3CDeleteScoutReportU3Ec__IteratorD_t2460383078  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<DeleteScoutReport>c__IteratorD::<deleteForm>__0
	WWWForm_t4064702195 * ___U3CdeleteFormU3E__0_0;
	// System.Int64 ReportManager/<DeleteScoutReport>c__IteratorD::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<DeleteScoutReport>c__IteratorD::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// ReportManager ReportManager/<DeleteScoutReport>c__IteratorD::$this
	ReportManager_t1122460921 * ___U24this_3;
	// System.Object ReportManager/<DeleteScoutReport>c__IteratorD::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ReportManager/<DeleteScoutReport>c__IteratorD::$disposing
	bool ___U24disposing_5;
	// System.Int32 ReportManager/<DeleteScoutReport>c__IteratorD::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdeleteFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U3CdeleteFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdeleteFormU3E__0_0() const { return ___U3CdeleteFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdeleteFormU3E__0_0() { return &___U3CdeleteFormU3E__0_0; }
	inline void set_U3CdeleteFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdeleteFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdeleteFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U24this_3)); }
	inline ReportManager_t1122460921 * get_U24this_3() const { return ___U24this_3; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ReportManager_t1122460921 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDeleteScoutReportU3Ec__IteratorD_t2460383078, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETESCOUTREPORTU3EC__ITERATORD_T2460383078_H
#ifndef U3CDELETEMESSAGEREPORTU3EC__ITERATORB_T871656197_H
#define U3CDELETEMESSAGEREPORTU3EC__ITERATORB_T871656197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<DeleteMessageReport>c__IteratorB
struct  U3CDeleteMessageReportU3Ec__IteratorB_t871656197  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<DeleteMessageReport>c__IteratorB::<deleteForm>__0
	WWWForm_t4064702195 * ___U3CdeleteFormU3E__0_0;
	// System.Int64 ReportManager/<DeleteMessageReport>c__IteratorB::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<DeleteMessageReport>c__IteratorB::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// ReportManager ReportManager/<DeleteMessageReport>c__IteratorB::$this
	ReportManager_t1122460921 * ___U24this_3;
	// System.Object ReportManager/<DeleteMessageReport>c__IteratorB::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ReportManager/<DeleteMessageReport>c__IteratorB::$disposing
	bool ___U24disposing_5;
	// System.Int32 ReportManager/<DeleteMessageReport>c__IteratorB::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdeleteFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U3CdeleteFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdeleteFormU3E__0_0() const { return ___U3CdeleteFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdeleteFormU3E__0_0() { return &___U3CdeleteFormU3E__0_0; }
	inline void set_U3CdeleteFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdeleteFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdeleteFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U24this_3)); }
	inline ReportManager_t1122460921 * get_U24this_3() const { return ___U24this_3; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ReportManager_t1122460921 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDeleteMessageReportU3Ec__IteratorB_t871656197, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEMESSAGEREPORTU3EC__ITERATORB_T871656197_H
#ifndef U3CDELETEBATTLEREPORTU3EC__ITERATORC_T3087337682_H
#define U3CDELETEBATTLEREPORTU3EC__ITERATORC_T3087337682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<DeleteBattleReport>c__IteratorC
struct  U3CDeleteBattleReportU3Ec__IteratorC_t3087337682  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<DeleteBattleReport>c__IteratorC::<deleteForm>__0
	WWWForm_t4064702195 * ___U3CdeleteFormU3E__0_0;
	// System.Int64 ReportManager/<DeleteBattleReport>c__IteratorC::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<DeleteBattleReport>c__IteratorC::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// ReportManager ReportManager/<DeleteBattleReport>c__IteratorC::$this
	ReportManager_t1122460921 * ___U24this_3;
	// System.Object ReportManager/<DeleteBattleReport>c__IteratorC::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ReportManager/<DeleteBattleReport>c__IteratorC::$disposing
	bool ___U24disposing_5;
	// System.Int32 ReportManager/<DeleteBattleReport>c__IteratorC::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdeleteFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U3CdeleteFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdeleteFormU3E__0_0() const { return ___U3CdeleteFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdeleteFormU3E__0_0() { return &___U3CdeleteFormU3E__0_0; }
	inline void set_U3CdeleteFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdeleteFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdeleteFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U24this_3)); }
	inline ReportManager_t1122460921 * get_U24this_3() const { return ___U24this_3; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ReportManager_t1122460921 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDeleteBattleReportU3Ec__IteratorC_t3087337682, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEBATTLEREPORTU3EC__ITERATORC_T3087337682_H
#ifndef TREASUREMANAGER_T110095690_H
#define TREASUREMANAGER_T110095690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager
struct  TreasureManager_t110095690  : public RuntimeObject
{
public:
	// System.Collections.Specialized.OrderedDictionary TreasureManager::scriptTreasures
	OrderedDictionary_t2617496293 * ___scriptTreasures_0;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::speedUpTreasures
	OrderedDictionary_t2617496293 * ___speedUpTreasures_1;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::productionTreasures
	OrderedDictionary_t2617496293 * ___productionTreasures_2;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::diamondTreasures
	OrderedDictionary_t2617496293 * ___diamondTreasures_3;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::promotionsTreasures
	OrderedDictionary_t2617496293 * ___promotionsTreasures_4;
	// System.Collections.Specialized.OrderedDictionary TreasureManager::armyTreasures
	OrderedDictionary_t2617496293 * ___armyTreasures_5;
	// SimpleEvent TreasureManager::onGetItems
	SimpleEvent_t129249603 * ___onGetItems_6;
	// SimpleEvent TreasureManager::onItemBought
	SimpleEvent_t129249603 * ___onItemBought_7;
	// SimpleEvent TreasureManager::onItemSold
	SimpleEvent_t129249603 * ___onItemSold_8;
	// SimpleEvent TreasureManager::onItemGifted
	SimpleEvent_t129249603 * ___onItemGifted_9;
	// SimpleEvent TreasureManager::onItemUsed
	SimpleEvent_t129249603 * ___onItemUsed_10;

public:
	inline static int32_t get_offset_of_scriptTreasures_0() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___scriptTreasures_0)); }
	inline OrderedDictionary_t2617496293 * get_scriptTreasures_0() const { return ___scriptTreasures_0; }
	inline OrderedDictionary_t2617496293 ** get_address_of_scriptTreasures_0() { return &___scriptTreasures_0; }
	inline void set_scriptTreasures_0(OrderedDictionary_t2617496293 * value)
	{
		___scriptTreasures_0 = value;
		Il2CppCodeGenWriteBarrier((&___scriptTreasures_0), value);
	}

	inline static int32_t get_offset_of_speedUpTreasures_1() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___speedUpTreasures_1)); }
	inline OrderedDictionary_t2617496293 * get_speedUpTreasures_1() const { return ___speedUpTreasures_1; }
	inline OrderedDictionary_t2617496293 ** get_address_of_speedUpTreasures_1() { return &___speedUpTreasures_1; }
	inline void set_speedUpTreasures_1(OrderedDictionary_t2617496293 * value)
	{
		___speedUpTreasures_1 = value;
		Il2CppCodeGenWriteBarrier((&___speedUpTreasures_1), value);
	}

	inline static int32_t get_offset_of_productionTreasures_2() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___productionTreasures_2)); }
	inline OrderedDictionary_t2617496293 * get_productionTreasures_2() const { return ___productionTreasures_2; }
	inline OrderedDictionary_t2617496293 ** get_address_of_productionTreasures_2() { return &___productionTreasures_2; }
	inline void set_productionTreasures_2(OrderedDictionary_t2617496293 * value)
	{
		___productionTreasures_2 = value;
		Il2CppCodeGenWriteBarrier((&___productionTreasures_2), value);
	}

	inline static int32_t get_offset_of_diamondTreasures_3() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___diamondTreasures_3)); }
	inline OrderedDictionary_t2617496293 * get_diamondTreasures_3() const { return ___diamondTreasures_3; }
	inline OrderedDictionary_t2617496293 ** get_address_of_diamondTreasures_3() { return &___diamondTreasures_3; }
	inline void set_diamondTreasures_3(OrderedDictionary_t2617496293 * value)
	{
		___diamondTreasures_3 = value;
		Il2CppCodeGenWriteBarrier((&___diamondTreasures_3), value);
	}

	inline static int32_t get_offset_of_promotionsTreasures_4() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___promotionsTreasures_4)); }
	inline OrderedDictionary_t2617496293 * get_promotionsTreasures_4() const { return ___promotionsTreasures_4; }
	inline OrderedDictionary_t2617496293 ** get_address_of_promotionsTreasures_4() { return &___promotionsTreasures_4; }
	inline void set_promotionsTreasures_4(OrderedDictionary_t2617496293 * value)
	{
		___promotionsTreasures_4 = value;
		Il2CppCodeGenWriteBarrier((&___promotionsTreasures_4), value);
	}

	inline static int32_t get_offset_of_armyTreasures_5() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___armyTreasures_5)); }
	inline OrderedDictionary_t2617496293 * get_armyTreasures_5() const { return ___armyTreasures_5; }
	inline OrderedDictionary_t2617496293 ** get_address_of_armyTreasures_5() { return &___armyTreasures_5; }
	inline void set_armyTreasures_5(OrderedDictionary_t2617496293 * value)
	{
		___armyTreasures_5 = value;
		Il2CppCodeGenWriteBarrier((&___armyTreasures_5), value);
	}

	inline static int32_t get_offset_of_onGetItems_6() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___onGetItems_6)); }
	inline SimpleEvent_t129249603 * get_onGetItems_6() const { return ___onGetItems_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetItems_6() { return &___onGetItems_6; }
	inline void set_onGetItems_6(SimpleEvent_t129249603 * value)
	{
		___onGetItems_6 = value;
		Il2CppCodeGenWriteBarrier((&___onGetItems_6), value);
	}

	inline static int32_t get_offset_of_onItemBought_7() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___onItemBought_7)); }
	inline SimpleEvent_t129249603 * get_onItemBought_7() const { return ___onItemBought_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onItemBought_7() { return &___onItemBought_7; }
	inline void set_onItemBought_7(SimpleEvent_t129249603 * value)
	{
		___onItemBought_7 = value;
		Il2CppCodeGenWriteBarrier((&___onItemBought_7), value);
	}

	inline static int32_t get_offset_of_onItemSold_8() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___onItemSold_8)); }
	inline SimpleEvent_t129249603 * get_onItemSold_8() const { return ___onItemSold_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onItemSold_8() { return &___onItemSold_8; }
	inline void set_onItemSold_8(SimpleEvent_t129249603 * value)
	{
		___onItemSold_8 = value;
		Il2CppCodeGenWriteBarrier((&___onItemSold_8), value);
	}

	inline static int32_t get_offset_of_onItemGifted_9() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___onItemGifted_9)); }
	inline SimpleEvent_t129249603 * get_onItemGifted_9() const { return ___onItemGifted_9; }
	inline SimpleEvent_t129249603 ** get_address_of_onItemGifted_9() { return &___onItemGifted_9; }
	inline void set_onItemGifted_9(SimpleEvent_t129249603 * value)
	{
		___onItemGifted_9 = value;
		Il2CppCodeGenWriteBarrier((&___onItemGifted_9), value);
	}

	inline static int32_t get_offset_of_onItemUsed_10() { return static_cast<int32_t>(offsetof(TreasureManager_t110095690, ___onItemUsed_10)); }
	inline SimpleEvent_t129249603 * get_onItemUsed_10() const { return ___onItemUsed_10; }
	inline SimpleEvent_t129249603 ** get_address_of_onItemUsed_10() { return &___onItemUsed_10; }
	inline void set_onItemUsed_10(SimpleEvent_t129249603 * value)
	{
		___onItemUsed_10 = value;
		Il2CppCodeGenWriteBarrier((&___onItemUsed_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREASUREMANAGER_T110095690_H
#ifndef U3CUPDATECITYGENERALSU3EC__ITERATOR3_T2358464504_H
#define U3CUPDATECITYGENERALSU3EC__ITERATOR3_T2358464504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<UpdateCityGenerals>c__Iterator3
struct  U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<UpdateCityGenerals>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// UnitsManager UnitsManager/<UpdateCityGenerals>c__Iterator3::$this
	UnitsManager_t4062574081 * ___U24this_1;
	// System.Object UnitsManager/<UpdateCityGenerals>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnitsManager/<UpdateCityGenerals>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnitsManager/<UpdateCityGenerals>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504, ___U24this_1)); }
	inline UnitsManager_t4062574081 * get_U24this_1() const { return ___U24this_1; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnitsManager_t4062574081 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECITYGENERALSU3EC__ITERATOR3_T2358464504_H
#ifndef U3CHIREGENERALU3EC__ITERATOR4_T1375040851_H
#define U3CHIREGENERALU3EC__ITERATOR4_T1375040851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<HireGeneral>c__Iterator4
struct  U3CHireGeneralU3Ec__Iterator4_t1375040851  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<HireGeneral>c__Iterator4::<hireForm>__0
	WWWForm_t4064702195 * ___U3ChireFormU3E__0_0;
	// ArmyGeneralModel UnitsManager/<HireGeneral>c__Iterator4::general
	ArmyGeneralModel_t737750221 * ___general_1;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<HireGeneral>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// UnitsManager UnitsManager/<HireGeneral>c__Iterator4::$this
	UnitsManager_t4062574081 * ___U24this_3;
	// System.Object UnitsManager/<HireGeneral>c__Iterator4::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnitsManager/<HireGeneral>c__Iterator4::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnitsManager/<HireGeneral>c__Iterator4::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3ChireFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U3ChireFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3ChireFormU3E__0_0() const { return ___U3ChireFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3ChireFormU3E__0_0() { return &___U3ChireFormU3E__0_0; }
	inline void set_U3ChireFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3ChireFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChireFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_general_1() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___general_1)); }
	inline ArmyGeneralModel_t737750221 * get_general_1() const { return ___general_1; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_general_1() { return &___general_1; }
	inline void set_general_1(ArmyGeneralModel_t737750221 * value)
	{
		___general_1 = value;
		Il2CppCodeGenWriteBarrier((&___general_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U24this_3)); }
	inline UnitsManager_t4062574081 * get_U24this_3() const { return ___U24this_3; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UnitsManager_t4062574081 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CHireGeneralU3Ec__Iterator4_t1375040851, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIREGENERALU3EC__ITERATOR4_T1375040851_H
#ifndef U3CUPDATECITYUNITSU3EC__ITERATOR2_T1596891963_H
#define U3CUPDATECITYUNITSU3EC__ITERATOR2_T1596891963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<UpdateCityUnits>c__Iterator2
struct  U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<UpdateCityUnits>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// UnitsManager UnitsManager/<UpdateCityUnits>c__Iterator2::$this
	UnitsManager_t4062574081 * ___U24this_1;
	// System.Object UnitsManager/<UpdateCityUnits>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnitsManager/<UpdateCityUnits>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnitsManager/<UpdateCityUnits>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963, ___U24this_1)); }
	inline UnitsManager_t4062574081 * get_U24this_1() const { return ___U24this_1; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnitsManager_t4062574081 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECITYUNITSU3EC__ITERATOR2_T1596891963_H
#ifndef U3CGETINITIALCITYUNITSU3EC__ITERATOR0_T367879573_H
#define U3CGETINITIALCITYUNITSU3EC__ITERATOR0_T367879573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<GetInitialCityUnits>c__Iterator0
struct  U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<GetInitialCityUnits>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// UnitsManager UnitsManager/<GetInitialCityUnits>c__Iterator0::$this
	UnitsManager_t4062574081 * ___U24this_1;
	// System.Object UnitsManager/<GetInitialCityUnits>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnitsManager/<GetInitialCityUnits>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnitsManager/<GetInitialCityUnits>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573, ___U24this_1)); }
	inline UnitsManager_t4062574081 * get_U24this_1() const { return ___U24this_1; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnitsManager_t4062574081 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITYUNITSU3EC__ITERATOR0_T367879573_H
#ifndef U3CGETINITIALCITYGENERALSU3EC__ITERATOR1_T4092798502_H
#define U3CGETINITIALCITYGENERALSU3EC__ITERATOR1_T4092798502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<GetInitialCityGenerals>c__Iterator1
struct  U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<GetInitialCityGenerals>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// UnitsManager UnitsManager/<GetInitialCityGenerals>c__Iterator1::$this
	UnitsManager_t4062574081 * ___U24this_1;
	// System.Object UnitsManager/<GetInitialCityGenerals>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnitsManager/<GetInitialCityGenerals>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnitsManager/<GetInitialCityGenerals>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502, ___U24this_1)); }
	inline UnitsManager_t4062574081 * get_U24this_1() const { return ___U24this_1; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnitsManager_t4062574081 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITYGENERALSU3EC__ITERATOR1_T4092798502_H
#ifndef U3CDISMISSGENERALU3EC__ITERATOR5_T2234259606_H
#define U3CDISMISSGENERALU3EC__ITERATOR5_T2234259606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<DismissGeneral>c__Iterator5
struct  U3CDismissGeneralU3Ec__Iterator5_t2234259606  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<DismissGeneral>c__Iterator5::<dismissForm>__0
	WWWForm_t4064702195 * ___U3CdismissFormU3E__0_0;
	// ArmyGeneralModel UnitsManager/<DismissGeneral>c__Iterator5::general
	ArmyGeneralModel_t737750221 * ___general_1;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<DismissGeneral>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// UnitsManager UnitsManager/<DismissGeneral>c__Iterator5::$this
	UnitsManager_t4062574081 * ___U24this_3;
	// System.Object UnitsManager/<DismissGeneral>c__Iterator5::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnitsManager/<DismissGeneral>c__Iterator5::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnitsManager/<DismissGeneral>c__Iterator5::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdismissFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U3CdismissFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdismissFormU3E__0_0() const { return ___U3CdismissFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdismissFormU3E__0_0() { return &___U3CdismissFormU3E__0_0; }
	inline void set_U3CdismissFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdismissFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdismissFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_general_1() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___general_1)); }
	inline ArmyGeneralModel_t737750221 * get_general_1() const { return ___general_1; }
	inline ArmyGeneralModel_t737750221 ** get_address_of_general_1() { return &___general_1; }
	inline void set_general_1(ArmyGeneralModel_t737750221 * value)
	{
		___general_1 = value;
		Il2CppCodeGenWriteBarrier((&___general_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U24this_3)); }
	inline UnitsManager_t4062574081 * get_U24this_3() const { return ___U24this_3; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UnitsManager_t4062574081 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDismissGeneralU3Ec__Iterator5_t2234259606, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISMISSGENERALU3EC__ITERATOR5_T2234259606_H
#ifndef U3CDISMISSUNITSU3EC__ITERATOR9_T617410052_H
#define U3CDISMISSUNITSU3EC__ITERATOR9_T617410052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<DismissUnits>c__Iterator9
struct  U3CDismissUnitsU3Ec__Iterator9_t617410052  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<DismissUnits>c__Iterator9::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// UnitsModel UnitsManager/<DismissUnits>c__Iterator9::army
	UnitsModel_t3847956398 * ___army_1;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<DismissUnits>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// UnitsManager UnitsManager/<DismissUnits>c__Iterator9::$this
	UnitsManager_t4062574081 * ___U24this_3;
	// System.Object UnitsManager/<DismissUnits>c__Iterator9::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnitsManager/<DismissUnits>c__Iterator9::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnitsManager/<DismissUnits>c__Iterator9::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_army_1() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___army_1)); }
	inline UnitsModel_t3847956398 * get_army_1() const { return ___army_1; }
	inline UnitsModel_t3847956398 ** get_address_of_army_1() { return &___army_1; }
	inline void set_army_1(UnitsModel_t3847956398 * value)
	{
		___army_1 = value;
		Il2CppCodeGenWriteBarrier((&___army_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U24this_3)); }
	inline UnitsManager_t4062574081 * get_U24this_3() const { return ___U24this_3; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UnitsManager_t4062574081 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDismissUnitsU3Ec__Iterator9_t617410052, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISMISSUNITSU3EC__ITERATOR9_T617410052_H
#ifndef ALLIANCEMODEL_T2995969982_H
#define ALLIANCEMODEL_T2995969982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceModel
struct  AllianceModel_t2995969982  : public RuntimeObject
{
public:
	// System.Int64 AllianceModel::id
	int64_t ___id_0;
	// System.String AllianceModel::alliance_name
	String_t* ___alliance_name_1;
	// System.Int64 AllianceModel::founderId
	int64_t ___founderId_2;
	// System.String AllianceModel::founderName
	String_t* ___founderName_3;
	// System.Int64 AllianceModel::leaderId
	int64_t ___leaderId_4;
	// System.String AllianceModel::leaderName
	String_t* ___leaderName_5;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::allies
	List_1_t913674750 * ___allies_6;
	// System.Collections.Generic.List`1<System.Int64> AllianceModel::enemies
	List_1_t913674750 * ___enemies_7;
	// System.Int64 AllianceModel::rank
	int64_t ___rank_8;
	// System.Int64 AllianceModel::members_count
	int64_t ___members_count_9;
	// System.Int64 AllianceModel::alliance_experience
	int64_t ___alliance_experience_10;
	// System.String AllianceModel::guideline
	String_t* ___guideline_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_alliance_name_1() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___alliance_name_1)); }
	inline String_t* get_alliance_name_1() const { return ___alliance_name_1; }
	inline String_t** get_address_of_alliance_name_1() { return &___alliance_name_1; }
	inline void set_alliance_name_1(String_t* value)
	{
		___alliance_name_1 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_name_1), value);
	}

	inline static int32_t get_offset_of_founderId_2() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___founderId_2)); }
	inline int64_t get_founderId_2() const { return ___founderId_2; }
	inline int64_t* get_address_of_founderId_2() { return &___founderId_2; }
	inline void set_founderId_2(int64_t value)
	{
		___founderId_2 = value;
	}

	inline static int32_t get_offset_of_founderName_3() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___founderName_3)); }
	inline String_t* get_founderName_3() const { return ___founderName_3; }
	inline String_t** get_address_of_founderName_3() { return &___founderName_3; }
	inline void set_founderName_3(String_t* value)
	{
		___founderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___founderName_3), value);
	}

	inline static int32_t get_offset_of_leaderId_4() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___leaderId_4)); }
	inline int64_t get_leaderId_4() const { return ___leaderId_4; }
	inline int64_t* get_address_of_leaderId_4() { return &___leaderId_4; }
	inline void set_leaderId_4(int64_t value)
	{
		___leaderId_4 = value;
	}

	inline static int32_t get_offset_of_leaderName_5() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___leaderName_5)); }
	inline String_t* get_leaderName_5() const { return ___leaderName_5; }
	inline String_t** get_address_of_leaderName_5() { return &___leaderName_5; }
	inline void set_leaderName_5(String_t* value)
	{
		___leaderName_5 = value;
		Il2CppCodeGenWriteBarrier((&___leaderName_5), value);
	}

	inline static int32_t get_offset_of_allies_6() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___allies_6)); }
	inline List_1_t913674750 * get_allies_6() const { return ___allies_6; }
	inline List_1_t913674750 ** get_address_of_allies_6() { return &___allies_6; }
	inline void set_allies_6(List_1_t913674750 * value)
	{
		___allies_6 = value;
		Il2CppCodeGenWriteBarrier((&___allies_6), value);
	}

	inline static int32_t get_offset_of_enemies_7() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___enemies_7)); }
	inline List_1_t913674750 * get_enemies_7() const { return ___enemies_7; }
	inline List_1_t913674750 ** get_address_of_enemies_7() { return &___enemies_7; }
	inline void set_enemies_7(List_1_t913674750 * value)
	{
		___enemies_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemies_7), value);
	}

	inline static int32_t get_offset_of_rank_8() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___rank_8)); }
	inline int64_t get_rank_8() const { return ___rank_8; }
	inline int64_t* get_address_of_rank_8() { return &___rank_8; }
	inline void set_rank_8(int64_t value)
	{
		___rank_8 = value;
	}

	inline static int32_t get_offset_of_members_count_9() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___members_count_9)); }
	inline int64_t get_members_count_9() const { return ___members_count_9; }
	inline int64_t* get_address_of_members_count_9() { return &___members_count_9; }
	inline void set_members_count_9(int64_t value)
	{
		___members_count_9 = value;
	}

	inline static int32_t get_offset_of_alliance_experience_10() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___alliance_experience_10)); }
	inline int64_t get_alliance_experience_10() const { return ___alliance_experience_10; }
	inline int64_t* get_address_of_alliance_experience_10() { return &___alliance_experience_10; }
	inline void set_alliance_experience_10(int64_t value)
	{
		___alliance_experience_10 = value;
	}

	inline static int32_t get_offset_of_guideline_11() { return static_cast<int32_t>(offsetof(AllianceModel_t2995969982, ___guideline_11)); }
	inline String_t* get_guideline_11() const { return ___guideline_11; }
	inline String_t** get_address_of_guideline_11() { return &___guideline_11; }
	inline void set_guideline_11(String_t* value)
	{
		___guideline_11 = value;
		Il2CppCodeGenWriteBarrier((&___guideline_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEMODEL_T2995969982_H
#ifndef U3CCANCELTRAININGU3EC__ITERATOR8_T585120171_H
#define U3CCANCELTRAININGU3EC__ITERATOR8_T585120171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<CancelTraining>c__Iterator8
struct  U3CCancelTrainingU3Ec__Iterator8_t585120171  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<CancelTraining>c__Iterator8::<cancelForm>__0
	WWWForm_t4064702195 * ___U3CcancelFormU3E__0_0;
	// System.Int64 UnitsManager/<CancelTraining>c__Iterator8::event_id
	int64_t ___event_id_1;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<CancelTraining>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// UnitsManager UnitsManager/<CancelTraining>c__Iterator8::$this
	UnitsManager_t4062574081 * ___U24this_3;
	// System.Object UnitsManager/<CancelTraining>c__Iterator8::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnitsManager/<CancelTraining>c__Iterator8::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnitsManager/<CancelTraining>c__Iterator8::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcancelFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U3CcancelFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CcancelFormU3E__0_0() const { return ___U3CcancelFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CcancelFormU3E__0_0() { return &___U3CcancelFormU3E__0_0; }
	inline void set_U3CcancelFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CcancelFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcancelFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_event_id_1() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___event_id_1)); }
	inline int64_t get_event_id_1() const { return ___event_id_1; }
	inline int64_t* get_address_of_event_id_1() { return &___event_id_1; }
	inline void set_event_id_1(int64_t value)
	{
		___event_id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U24this_3)); }
	inline UnitsManager_t4062574081 * get_U24this_3() const { return ___U24this_3; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(UnitsManager_t4062574081 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelTrainingU3Ec__Iterator8_t585120171, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELTRAININGU3EC__ITERATOR8_T585120171_H
#ifndef U3CHIREUNITSU3EC__ITERATOR6_T1010860969_H
#define U3CHIREUNITSU3EC__ITERATOR6_T1010860969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<HireUnits>c__Iterator6
struct  U3CHireUnitsU3Ec__Iterator6_t1010860969  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<HireUnits>c__Iterator6::<unitForm>__0
	WWWForm_t4064702195 * ___U3CunitFormU3E__0_0;
	// System.String UnitsManager/<HireUnits>c__Iterator6::name
	String_t* ___name_1;
	// System.Int64 UnitsManager/<HireUnits>c__Iterator6::count
	int64_t ___count_2;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<HireUnits>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// UnitsManager UnitsManager/<HireUnits>c__Iterator6::$this
	UnitsManager_t4062574081 * ___U24this_4;
	// System.Object UnitsManager/<HireUnits>c__Iterator6::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UnitsManager/<HireUnits>c__Iterator6::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnitsManager/<HireUnits>c__Iterator6::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CunitFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U3CunitFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CunitFormU3E__0_0() const { return ___U3CunitFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CunitFormU3E__0_0() { return &___U3CunitFormU3E__0_0; }
	inline void set_U3CunitFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CunitFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunitFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___count_2)); }
	inline int64_t get_count_2() const { return ___count_2; }
	inline int64_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int64_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U24this_4)); }
	inline UnitsManager_t4062574081 * get_U24this_4() const { return ___U24this_4; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(UnitsManager_t4062574081 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CHireUnitsU3Ec__Iterator6_t1010860969, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIREUNITSU3EC__ITERATOR6_T1010860969_H
#ifndef U3CUPGRADEGENERALU3EC__ITERATOR7_T4280997162_H
#define U3CUPGRADEGENERALU3EC__ITERATOR7_T4280997162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager/<UpgradeGeneral>c__Iterator7
struct  U3CUpgradeGeneralU3Ec__Iterator7_t4280997162  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm UnitsManager/<UpgradeGeneral>c__Iterator7::<upgradeForm>__0
	WWWForm_t4064702195 * ___U3CupgradeFormU3E__0_0;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::heroId
	int64_t ___heroId_1;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::march
	int64_t ___march_2;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::politics
	int64_t ___politics_3;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::speed
	int64_t ___speed_4;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::salary
	int64_t ___salary_5;
	// System.Int64 UnitsManager/<UpgradeGeneral>c__Iterator7::loyalty
	int64_t ___loyalty_6;
	// UnityEngine.Networking.UnityWebRequest UnitsManager/<UpgradeGeneral>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_7;
	// UnitsManager UnitsManager/<UpgradeGeneral>c__Iterator7::$this
	UnitsManager_t4062574081 * ___U24this_8;
	// System.Object UnitsManager/<UpgradeGeneral>c__Iterator7::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean UnitsManager/<UpgradeGeneral>c__Iterator7::$disposing
	bool ___U24disposing_10;
	// System.Int32 UnitsManager/<UpgradeGeneral>c__Iterator7::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CupgradeFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U3CupgradeFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CupgradeFormU3E__0_0() const { return ___U3CupgradeFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CupgradeFormU3E__0_0() { return &___U3CupgradeFormU3E__0_0; }
	inline void set_U3CupgradeFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CupgradeFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupgradeFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_heroId_1() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___heroId_1)); }
	inline int64_t get_heroId_1() const { return ___heroId_1; }
	inline int64_t* get_address_of_heroId_1() { return &___heroId_1; }
	inline void set_heroId_1(int64_t value)
	{
		___heroId_1 = value;
	}

	inline static int32_t get_offset_of_march_2() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___march_2)); }
	inline int64_t get_march_2() const { return ___march_2; }
	inline int64_t* get_address_of_march_2() { return &___march_2; }
	inline void set_march_2(int64_t value)
	{
		___march_2 = value;
	}

	inline static int32_t get_offset_of_politics_3() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___politics_3)); }
	inline int64_t get_politics_3() const { return ___politics_3; }
	inline int64_t* get_address_of_politics_3() { return &___politics_3; }
	inline void set_politics_3(int64_t value)
	{
		___politics_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___speed_4)); }
	inline int64_t get_speed_4() const { return ___speed_4; }
	inline int64_t* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(int64_t value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_salary_5() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___salary_5)); }
	inline int64_t get_salary_5() const { return ___salary_5; }
	inline int64_t* get_address_of_salary_5() { return &___salary_5; }
	inline void set_salary_5(int64_t value)
	{
		___salary_5 = value;
	}

	inline static int32_t get_offset_of_loyalty_6() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___loyalty_6)); }
	inline int64_t get_loyalty_6() const { return ___loyalty_6; }
	inline int64_t* get_address_of_loyalty_6() { return &___loyalty_6; }
	inline void set_loyalty_6(int64_t value)
	{
		___loyalty_6 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_7() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U3CrequestU3E__0_7)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_7() const { return ___U3CrequestU3E__0_7; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_7() { return &___U3CrequestU3E__0_7; }
	inline void set_U3CrequestU3E__0_7(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U24this_8)); }
	inline UnitsManager_t4062574081 * get_U24this_8() const { return ___U24this_8; }
	inline UnitsManager_t4062574081 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(UnitsManager_t4062574081 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CUpgradeGeneralU3Ec__Iterator7_t4280997162, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPGRADEGENERALU3EC__ITERATOR7_T4280997162_H
#ifndef UNITSMANAGER_T4062574081_H
#define UNITSMANAGER_T4062574081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager
struct  UnitsManager_t4062574081  : public RuntimeObject
{
public:
	// UnitsModel UnitsManager::cityUnits
	UnitsModel_t3847956398 * ___cityUnits_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,ArmyGeneralModel> UnitsManager::cityGenerals
	Dictionary_2_t1800390005 * ___cityGenerals_1;
	// SimpleEvent UnitsManager::onUnitsHired
	SimpleEvent_t129249603 * ___onUnitsHired_2;
	// SimpleEvent UnitsManager::onGeneralHired
	SimpleEvent_t129249603 * ___onGeneralHired_3;
	// SimpleEvent UnitsManager::onGeneralDismissed
	SimpleEvent_t129249603 * ___onGeneralDismissed_4;
	// SimpleEvent UnitsManager::onUnitsUpdated
	SimpleEvent_t129249603 * ___onUnitsUpdated_5;
	// SimpleEvent UnitsManager::onGeneralUpgraded
	SimpleEvent_t129249603 * ___onGeneralUpgraded_6;
	// SimpleEvent UnitsManager::onTrainingCancelled
	SimpleEvent_t129249603 * ___onTrainingCancelled_7;
	// SimpleEvent UnitsManager::onUnitsDismissed
	SimpleEvent_t129249603 * ___onUnitsDismissed_8;
	// SimpleEvent UnitsManager::onGetGenerals
	SimpleEvent_t129249603 * ___onGetGenerals_9;

public:
	inline static int32_t get_offset_of_cityUnits_0() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___cityUnits_0)); }
	inline UnitsModel_t3847956398 * get_cityUnits_0() const { return ___cityUnits_0; }
	inline UnitsModel_t3847956398 ** get_address_of_cityUnits_0() { return &___cityUnits_0; }
	inline void set_cityUnits_0(UnitsModel_t3847956398 * value)
	{
		___cityUnits_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityUnits_0), value);
	}

	inline static int32_t get_offset_of_cityGenerals_1() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___cityGenerals_1)); }
	inline Dictionary_2_t1800390005 * get_cityGenerals_1() const { return ___cityGenerals_1; }
	inline Dictionary_2_t1800390005 ** get_address_of_cityGenerals_1() { return &___cityGenerals_1; }
	inline void set_cityGenerals_1(Dictionary_2_t1800390005 * value)
	{
		___cityGenerals_1 = value;
		Il2CppCodeGenWriteBarrier((&___cityGenerals_1), value);
	}

	inline static int32_t get_offset_of_onUnitsHired_2() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onUnitsHired_2)); }
	inline SimpleEvent_t129249603 * get_onUnitsHired_2() const { return ___onUnitsHired_2; }
	inline SimpleEvent_t129249603 ** get_address_of_onUnitsHired_2() { return &___onUnitsHired_2; }
	inline void set_onUnitsHired_2(SimpleEvent_t129249603 * value)
	{
		___onUnitsHired_2 = value;
		Il2CppCodeGenWriteBarrier((&___onUnitsHired_2), value);
	}

	inline static int32_t get_offset_of_onGeneralHired_3() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onGeneralHired_3)); }
	inline SimpleEvent_t129249603 * get_onGeneralHired_3() const { return ___onGeneralHired_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onGeneralHired_3() { return &___onGeneralHired_3; }
	inline void set_onGeneralHired_3(SimpleEvent_t129249603 * value)
	{
		___onGeneralHired_3 = value;
		Il2CppCodeGenWriteBarrier((&___onGeneralHired_3), value);
	}

	inline static int32_t get_offset_of_onGeneralDismissed_4() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onGeneralDismissed_4)); }
	inline SimpleEvent_t129249603 * get_onGeneralDismissed_4() const { return ___onGeneralDismissed_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGeneralDismissed_4() { return &___onGeneralDismissed_4; }
	inline void set_onGeneralDismissed_4(SimpleEvent_t129249603 * value)
	{
		___onGeneralDismissed_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGeneralDismissed_4), value);
	}

	inline static int32_t get_offset_of_onUnitsUpdated_5() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onUnitsUpdated_5)); }
	inline SimpleEvent_t129249603 * get_onUnitsUpdated_5() const { return ___onUnitsUpdated_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onUnitsUpdated_5() { return &___onUnitsUpdated_5; }
	inline void set_onUnitsUpdated_5(SimpleEvent_t129249603 * value)
	{
		___onUnitsUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___onUnitsUpdated_5), value);
	}

	inline static int32_t get_offset_of_onGeneralUpgraded_6() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onGeneralUpgraded_6)); }
	inline SimpleEvent_t129249603 * get_onGeneralUpgraded_6() const { return ___onGeneralUpgraded_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onGeneralUpgraded_6() { return &___onGeneralUpgraded_6; }
	inline void set_onGeneralUpgraded_6(SimpleEvent_t129249603 * value)
	{
		___onGeneralUpgraded_6 = value;
		Il2CppCodeGenWriteBarrier((&___onGeneralUpgraded_6), value);
	}

	inline static int32_t get_offset_of_onTrainingCancelled_7() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onTrainingCancelled_7)); }
	inline SimpleEvent_t129249603 * get_onTrainingCancelled_7() const { return ___onTrainingCancelled_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onTrainingCancelled_7() { return &___onTrainingCancelled_7; }
	inline void set_onTrainingCancelled_7(SimpleEvent_t129249603 * value)
	{
		___onTrainingCancelled_7 = value;
		Il2CppCodeGenWriteBarrier((&___onTrainingCancelled_7), value);
	}

	inline static int32_t get_offset_of_onUnitsDismissed_8() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onUnitsDismissed_8)); }
	inline SimpleEvent_t129249603 * get_onUnitsDismissed_8() const { return ___onUnitsDismissed_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onUnitsDismissed_8() { return &___onUnitsDismissed_8; }
	inline void set_onUnitsDismissed_8(SimpleEvent_t129249603 * value)
	{
		___onUnitsDismissed_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUnitsDismissed_8), value);
	}

	inline static int32_t get_offset_of_onGetGenerals_9() { return static_cast<int32_t>(offsetof(UnitsManager_t4062574081, ___onGetGenerals_9)); }
	inline SimpleEvent_t129249603 * get_onGetGenerals_9() const { return ___onGetGenerals_9; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetGenerals_9() { return &___onGetGenerals_9; }
	inline void set_onGetGenerals_9(SimpleEvent_t129249603 * value)
	{
		___onGetGenerals_9 = value;
		Il2CppCodeGenWriteBarrier((&___onGetGenerals_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSMANAGER_T4062574081_H
#ifndef U3CUSETELEPORTU3EC__ITERATOR3_T2451633368_H
#define U3CUSETELEPORTU3EC__ITERATOR3_T2451633368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseTeleport>c__Iterator3
struct  U3CUseTeleportU3Ec__Iterator3_t2451633368  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseTeleport>c__Iterator3::<useItemForm>__0
	WWWForm_t4064702195 * ___U3CuseItemFormU3E__0_0;
	// System.String TreasureManager/<UseTeleport>c__Iterator3::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<UseTeleport>c__Iterator3::x
	int64_t ___x_2;
	// System.Int64 TreasureManager/<UseTeleport>c__Iterator3::y
	int64_t ___y_3;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseTeleport>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// TreasureManager TreasureManager/<UseTeleport>c__Iterator3::$this
	TreasureManager_t110095690 * ___U24this_5;
	// System.Object TreasureManager/<UseTeleport>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TreasureManager/<UseTeleport>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 TreasureManager/<UseTeleport>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CuseItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U3CuseItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseItemFormU3E__0_0() const { return ___U3CuseItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseItemFormU3E__0_0() { return &___U3CuseItemFormU3E__0_0; }
	inline void set_U3CuseItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___x_2)); }
	inline int64_t get_x_2() const { return ___x_2; }
	inline int64_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int64_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___y_3)); }
	inline int64_t get_y_3() const { return ___y_3; }
	inline int64_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int64_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U24this_5)); }
	inline TreasureManager_t110095690 * get_U24this_5() const { return ___U24this_5; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TreasureManager_t110095690 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CUseTeleportU3Ec__Iterator3_t2451633368, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSETELEPORTU3EC__ITERATOR3_T2451633368_H
#ifndef U3CUSESHIELDHOURU3EC__ITERATOR4_T3002913516_H
#define U3CUSESHIELDHOURU3EC__ITERATOR4_T3002913516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseShieldHour>c__Iterator4
struct  U3CUseShieldHourU3Ec__Iterator4_t3002913516  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseShieldHour>c__Iterator4::<useItemForm>__0
	WWWForm_t4064702195 * ___U3CuseItemFormU3E__0_0;
	// System.String TreasureManager/<UseShieldHour>c__Iterator4::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<UseShieldHour>c__Iterator4::hour
	int64_t ___hour_2;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseShieldHour>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// TreasureManager TreasureManager/<UseShieldHour>c__Iterator4::$this
	TreasureManager_t110095690 * ___U24this_4;
	// System.Object TreasureManager/<UseShieldHour>c__Iterator4::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TreasureManager/<UseShieldHour>c__Iterator4::$disposing
	bool ___U24disposing_6;
	// System.Int32 TreasureManager/<UseShieldHour>c__Iterator4::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CuseItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U3CuseItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseItemFormU3E__0_0() const { return ___U3CuseItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseItemFormU3E__0_0() { return &___U3CuseItemFormU3E__0_0; }
	inline void set_U3CuseItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_hour_2() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___hour_2)); }
	inline int64_t get_hour_2() const { return ___hour_2; }
	inline int64_t* get_address_of_hour_2() { return &___hour_2; }
	inline void set_hour_2(int64_t value)
	{
		___hour_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U24this_4)); }
	inline TreasureManager_t110095690 * get_U24this_4() const { return ___U24this_4; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TreasureManager_t110095690 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CUseShieldHourU3Ec__Iterator4_t3002913516, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSESHIELDHOURU3EC__ITERATOR4_T3002913516_H
#ifndef U3CUSEUPGRADEU3EC__ITERATOR2_T864670046_H
#define U3CUSEUPGRADEU3EC__ITERATOR2_T864670046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseUpgrade>c__Iterator2
struct  U3CUseUpgradeU3Ec__Iterator2_t864670046  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseUpgrade>c__Iterator2::<useItemForm>__0
	WWWForm_t4064702195 * ___U3CuseItemFormU3E__0_0;
	// System.Int64 TreasureManager/<UseUpgrade>c__Iterator2::itemId
	int64_t ___itemId_1;
	// System.Boolean TreasureManager/<UseUpgrade>c__Iterator2::valley
	bool ___valley_2;
	// System.Int64 TreasureManager/<UseUpgrade>c__Iterator2::building_id
	int64_t ___building_id_3;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseUpgrade>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// TreasureManager TreasureManager/<UseUpgrade>c__Iterator2::$this
	TreasureManager_t110095690 * ___U24this_5;
	// System.Object TreasureManager/<UseUpgrade>c__Iterator2::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TreasureManager/<UseUpgrade>c__Iterator2::$disposing
	bool ___U24disposing_7;
	// System.Int32 TreasureManager/<UseUpgrade>c__Iterator2::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CuseItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U3CuseItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseItemFormU3E__0_0() const { return ___U3CuseItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseItemFormU3E__0_0() { return &___U3CuseItemFormU3E__0_0; }
	inline void set_U3CuseItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___itemId_1)); }
	inline int64_t get_itemId_1() const { return ___itemId_1; }
	inline int64_t* get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(int64_t value)
	{
		___itemId_1 = value;
	}

	inline static int32_t get_offset_of_valley_2() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___valley_2)); }
	inline bool get_valley_2() const { return ___valley_2; }
	inline bool* get_address_of_valley_2() { return &___valley_2; }
	inline void set_valley_2(bool value)
	{
		___valley_2 = value;
	}

	inline static int32_t get_offset_of_building_id_3() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___building_id_3)); }
	inline int64_t get_building_id_3() const { return ___building_id_3; }
	inline int64_t* get_address_of_building_id_3() { return &___building_id_3; }
	inline void set_building_id_3(int64_t value)
	{
		___building_id_3 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U24this_5)); }
	inline TreasureManager_t110095690 * get_U24this_5() const { return ___U24this_5; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TreasureManager_t110095690 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CUseUpgradeU3Ec__Iterator2_t864670046, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSEUPGRADEU3EC__ITERATOR2_T864670046_H
#ifndef U3CGETCITYTREASURESU3EC__ITERATOR0_T3928749663_H
#define U3CGETCITYTREASURESU3EC__ITERATOR0_T3928749663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<GetCityTreasures>c__Iterator0
struct  U3CGetCityTreasuresU3Ec__Iterator0_t3928749663  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<GetCityTreasures>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean TreasureManager/<GetCityTreasures>c__Iterator0::initial
	bool ___initial_1;
	// TreasureManager TreasureManager/<GetCityTreasures>c__Iterator0::$this
	TreasureManager_t110095690 * ___U24this_2;
	// System.Object TreasureManager/<GetCityTreasures>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean TreasureManager/<GetCityTreasures>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 TreasureManager/<GetCityTreasures>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___U24this_2)); }
	inline TreasureManager_t110095690 * get_U24this_2() const { return ___U24this_2; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TreasureManager_t110095690 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetCityTreasuresU3Ec__Iterator0_t3928749663, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCITYTREASURESU3EC__ITERATOR0_T3928749663_H
#ifndef U3CUSEITEMU3EC__ITERATOR1_T483476331_H
#define U3CUSEITEMU3EC__ITERATOR1_T483476331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseItem>c__Iterator1
struct  U3CUseItemU3Ec__Iterator1_t483476331  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseItem>c__Iterator1::<useItemForm>__0
	WWWForm_t4064702195 * ___U3CuseItemFormU3E__0_0;
	// System.String TreasureManager/<UseItem>c__Iterator1::itemId
	String_t* ___itemId_1;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseItem>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// TreasureManager TreasureManager/<UseItem>c__Iterator1::$this
	TreasureManager_t110095690 * ___U24this_3;
	// System.Object TreasureManager/<UseItem>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TreasureManager/<UseItem>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 TreasureManager/<UseItem>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CuseItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U3CuseItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseItemFormU3E__0_0() const { return ___U3CuseItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseItemFormU3E__0_0() { return &___U3CuseItemFormU3E__0_0; }
	inline void set_U3CuseItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U24this_3)); }
	inline TreasureManager_t110095690 * get_U24this_3() const { return ___U24this_3; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TreasureManager_t110095690 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUseItemU3Ec__Iterator1_t483476331, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSEITEMU3EC__ITERATOR1_T483476331_H
#ifndef U3CUSESHIELDDAYU3EC__ITERATOR5_T2970678129_H
#define U3CUSESHIELDDAYU3EC__ITERATOR5_T2970678129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseShieldDay>c__Iterator5
struct  U3CUseShieldDayU3Ec__Iterator5_t2970678129  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseShieldDay>c__Iterator5::<useItemForm>__0
	WWWForm_t4064702195 * ___U3CuseItemFormU3E__0_0;
	// System.String TreasureManager/<UseShieldDay>c__Iterator5::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<UseShieldDay>c__Iterator5::day
	int64_t ___day_2;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseShieldDay>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// TreasureManager TreasureManager/<UseShieldDay>c__Iterator5::$this
	TreasureManager_t110095690 * ___U24this_4;
	// System.Object TreasureManager/<UseShieldDay>c__Iterator5::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TreasureManager/<UseShieldDay>c__Iterator5::$disposing
	bool ___U24disposing_6;
	// System.Int32 TreasureManager/<UseShieldDay>c__Iterator5::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CuseItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U3CuseItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseItemFormU3E__0_0() const { return ___U3CuseItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseItemFormU3E__0_0() { return &___U3CuseItemFormU3E__0_0; }
	inline void set_U3CuseItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___day_2)); }
	inline int64_t get_day_2() const { return ___day_2; }
	inline int64_t* get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(int64_t value)
	{
		___day_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U24this_4)); }
	inline TreasureManager_t110095690 * get_U24this_4() const { return ___U24this_4; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TreasureManager_t110095690 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CUseShieldDayU3Ec__Iterator5_t2970678129, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSESHIELDDAYU3EC__ITERATOR5_T2970678129_H
#ifndef U3CSELLITEMU3EC__ITERATOR9_T3920914631_H
#define U3CSELLITEMU3EC__ITERATOR9_T3920914631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<SellItem>c__Iterator9
struct  U3CSellItemU3Ec__Iterator9_t3920914631  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<SellItem>c__Iterator9::<sellItemForm>__0
	WWWForm_t4064702195 * ___U3CsellItemFormU3E__0_0;
	// System.String TreasureManager/<SellItem>c__Iterator9::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<SellItem>c__Iterator9::count
	int64_t ___count_2;
	// System.Int64 TreasureManager/<SellItem>c__Iterator9::discount
	int64_t ___discount_3;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<SellItem>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// TreasureManager TreasureManager/<SellItem>c__Iterator9::$this
	TreasureManager_t110095690 * ___U24this_5;
	// System.Object TreasureManager/<SellItem>c__Iterator9::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TreasureManager/<SellItem>c__Iterator9::$disposing
	bool ___U24disposing_7;
	// System.Int32 TreasureManager/<SellItem>c__Iterator9::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CsellItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U3CsellItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CsellItemFormU3E__0_0() const { return ___U3CsellItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CsellItemFormU3E__0_0() { return &___U3CsellItemFormU3E__0_0; }
	inline void set_U3CsellItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CsellItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsellItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___count_2)); }
	inline int64_t get_count_2() const { return ___count_2; }
	inline int64_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int64_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_discount_3() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___discount_3)); }
	inline int64_t get_discount_3() const { return ___discount_3; }
	inline int64_t* get_address_of_discount_3() { return &___discount_3; }
	inline void set_discount_3(int64_t value)
	{
		___discount_3 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U24this_5)); }
	inline TreasureManager_t110095690 * get_U24this_5() const { return ___U24this_5; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TreasureManager_t110095690 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CSellItemU3Ec__Iterator9_t3920914631, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELLITEMU3EC__ITERATOR9_T3920914631_H
#ifndef U3CGIFTITEMU3EC__ITERATORA_T1993921738_H
#define U3CGIFTITEMU3EC__ITERATORA_T1993921738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<GiftItem>c__IteratorA
struct  U3CGiftItemU3Ec__IteratorA_t1993921738  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<GiftItem>c__IteratorA::<giftItemForm>__0
	WWWForm_t4064702195 * ___U3CgiftItemFormU3E__0_0;
	// System.Int64 TreasureManager/<GiftItem>c__IteratorA::cityID
	int64_t ___cityID_1;
	// System.String TreasureManager/<GiftItem>c__IteratorA::itemId
	String_t* ___itemId_2;
	// System.Int64 TreasureManager/<GiftItem>c__IteratorA::count
	int64_t ___count_3;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<GiftItem>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// TreasureManager TreasureManager/<GiftItem>c__IteratorA::$this
	TreasureManager_t110095690 * ___U24this_5;
	// System.Object TreasureManager/<GiftItem>c__IteratorA::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TreasureManager/<GiftItem>c__IteratorA::$disposing
	bool ___U24disposing_7;
	// System.Int32 TreasureManager/<GiftItem>c__IteratorA::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CgiftItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U3CgiftItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CgiftItemFormU3E__0_0() const { return ___U3CgiftItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CgiftItemFormU3E__0_0() { return &___U3CgiftItemFormU3E__0_0; }
	inline void set_U3CgiftItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CgiftItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgiftItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_cityID_1() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___cityID_1)); }
	inline int64_t get_cityID_1() const { return ___cityID_1; }
	inline int64_t* get_address_of_cityID_1() { return &___cityID_1; }
	inline void set_cityID_1(int64_t value)
	{
		___cityID_1 = value;
	}

	inline static int32_t get_offset_of_itemId_2() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___itemId_2)); }
	inline String_t* get_itemId_2() const { return ___itemId_2; }
	inline String_t** get_address_of_itemId_2() { return &___itemId_2; }
	inline void set_itemId_2(String_t* value)
	{
		___itemId_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___count_3)); }
	inline int64_t get_count_3() const { return ___count_3; }
	inline int64_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int64_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U24this_5)); }
	inline TreasureManager_t110095690 * get_U24this_5() const { return ___U24this_5; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TreasureManager_t110095690 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CGiftItemU3Ec__IteratorA_t1993921738, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGIFTITEMU3EC__ITERATORA_T1993921738_H
#ifndef U3CBUYPLAYERITEMU3EC__ITERATOR8_T2841192203_H
#define U3CBUYPLAYERITEMU3EC__ITERATOR8_T2841192203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<BuyPlayerItem>c__Iterator8
struct  U3CBuyPlayerItemU3Ec__Iterator8_t2841192203  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<BuyPlayerItem>c__Iterator8::<buyItemForm>__0
	WWWForm_t4064702195 * ___U3CbuyItemFormU3E__0_0;
	// System.String TreasureManager/<BuyPlayerItem>c__Iterator8::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<BuyPlayerItem>c__Iterator8::count
	int64_t ___count_2;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<BuyPlayerItem>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// TreasureManager TreasureManager/<BuyPlayerItem>c__Iterator8::$this
	TreasureManager_t110095690 * ___U24this_4;
	// System.Object TreasureManager/<BuyPlayerItem>c__Iterator8::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TreasureManager/<BuyPlayerItem>c__Iterator8::$disposing
	bool ___U24disposing_6;
	// System.Int32 TreasureManager/<BuyPlayerItem>c__Iterator8::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CbuyItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U3CbuyItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CbuyItemFormU3E__0_0() const { return ___U3CbuyItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CbuyItemFormU3E__0_0() { return &___U3CbuyItemFormU3E__0_0; }
	inline void set_U3CbuyItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CbuyItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuyItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___count_2)); }
	inline int64_t get_count_2() const { return ___count_2; }
	inline int64_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int64_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U24this_4)); }
	inline TreasureManager_t110095690 * get_U24this_4() const { return ___U24this_4; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TreasureManager_t110095690 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CBuyPlayerItemU3Ec__Iterator8_t2841192203, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUYPLAYERITEMU3EC__ITERATOR8_T2841192203_H
#ifndef U3CUSESPEEDUPU3EC__ITERATOR6_T780075305_H
#define U3CUSESPEEDUPU3EC__ITERATOR6_T780075305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<UseSpeedUp>c__Iterator6
struct  U3CUseSpeedUpU3Ec__Iterator6_t780075305  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<UseSpeedUp>c__Iterator6::<useSpeedUpForm>__0
	WWWForm_t4064702195 * ___U3CuseSpeedUpFormU3E__0_0;
	// System.String TreasureManager/<UseSpeedUp>c__Iterator6::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<UseSpeedUp>c__Iterator6::eventId
	int64_t ___eventId_2;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<UseSpeedUp>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// TreasureManager TreasureManager/<UseSpeedUp>c__Iterator6::$this
	TreasureManager_t110095690 * ___U24this_4;
	// System.Object TreasureManager/<UseSpeedUp>c__Iterator6::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TreasureManager/<UseSpeedUp>c__Iterator6::$disposing
	bool ___U24disposing_6;
	// System.Int32 TreasureManager/<UseSpeedUp>c__Iterator6::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CuseSpeedUpFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U3CuseSpeedUpFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuseSpeedUpFormU3E__0_0() const { return ___U3CuseSpeedUpFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuseSpeedUpFormU3E__0_0() { return &___U3CuseSpeedUpFormU3E__0_0; }
	inline void set_U3CuseSpeedUpFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuseSpeedUpFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseSpeedUpFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_eventId_2() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___eventId_2)); }
	inline int64_t get_eventId_2() const { return ___eventId_2; }
	inline int64_t* get_address_of_eventId_2() { return &___eventId_2; }
	inline void set_eventId_2(int64_t value)
	{
		___eventId_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U24this_4)); }
	inline TreasureManager_t110095690 * get_U24this_4() const { return ___U24this_4; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TreasureManager_t110095690 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CUseSpeedUpU3Ec__Iterator6_t780075305, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSESPEEDUPU3EC__ITERATOR6_T780075305_H
#ifndef U3CBUYITEMU3EC__ITERATOR7_T4213354092_H
#define U3CBUYITEMU3EC__ITERATOR7_T4213354092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureManager/<BuyItem>c__Iterator7
struct  U3CBuyItemU3Ec__Iterator7_t4213354092  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm TreasureManager/<BuyItem>c__Iterator7::<buyItemForm>__0
	WWWForm_t4064702195 * ___U3CbuyItemFormU3E__0_0;
	// System.String TreasureManager/<BuyItem>c__Iterator7::itemId
	String_t* ___itemId_1;
	// System.Int64 TreasureManager/<BuyItem>c__Iterator7::count
	int64_t ___count_2;
	// UnityEngine.Networking.UnityWebRequest TreasureManager/<BuyItem>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// TreasureManager TreasureManager/<BuyItem>c__Iterator7::$this
	TreasureManager_t110095690 * ___U24this_4;
	// System.Object TreasureManager/<BuyItem>c__Iterator7::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TreasureManager/<BuyItem>c__Iterator7::$disposing
	bool ___U24disposing_6;
	// System.Int32 TreasureManager/<BuyItem>c__Iterator7::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CbuyItemFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U3CbuyItemFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CbuyItemFormU3E__0_0() const { return ___U3CbuyItemFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CbuyItemFormU3E__0_0() { return &___U3CbuyItemFormU3E__0_0; }
	inline void set_U3CbuyItemFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CbuyItemFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuyItemFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_itemId_1() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___itemId_1)); }
	inline String_t* get_itemId_1() const { return ___itemId_1; }
	inline String_t** get_address_of_itemId_1() { return &___itemId_1; }
	inline void set_itemId_1(String_t* value)
	{
		___itemId_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemId_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___count_2)); }
	inline int64_t get_count_2() const { return ___count_2; }
	inline int64_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int64_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U24this_4)); }
	inline TreasureManager_t110095690 * get_U24this_4() const { return ___U24this_4; }
	inline TreasureManager_t110095690 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TreasureManager_t110095690 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CBuyItemU3Ec__Iterator7_t4213354092, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUYITEMU3EC__ITERATOR7_T4213354092_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public RuntimeObject
{
public:
	// CityManager GameManager::cityManager
	CityManager_t2587329200 * ___cityManager_0;
	// BattleManager GameManager::battleManager
	BattleManager_t4022130644 * ___battleManager_1;
	// LevelLoadManager GameManager::levelLoadManager
	LevelLoadManager_t362334468 * ___levelLoadManager_2;
	// System.Collections.Generic.Dictionary`2<System.String,BuildingConstantModel> GameManager::buildingConstants
	Dictionary_2_t2441201299 * ___buildingConstants_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel> GameManager::unitConstants
	Dictionary_2_t3367445707 * ___unitConstants_4;
	// System.Collections.Generic.List`1<TutorialPageModel> GameManager::tutorials
	List_1_t1672770702 * ___tutorials_5;
	// System.Int64[] GameManager::residenceFields
	Int64U5BU5D_t2559172825* ___residenceFields_6;
	// SimpleEvent GameManager::onGetBuildingNames
	SimpleEvent_t129249603 * ___onGetBuildingNames_7;
	// System.Collections.Generic.List`1<System.String> GameManager::buildingIds
	List_1_t3319525431 * ___buildingIds_8;

public:
	inline static int32_t get_offset_of_cityManager_0() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___cityManager_0)); }
	inline CityManager_t2587329200 * get_cityManager_0() const { return ___cityManager_0; }
	inline CityManager_t2587329200 ** get_address_of_cityManager_0() { return &___cityManager_0; }
	inline void set_cityManager_0(CityManager_t2587329200 * value)
	{
		___cityManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___cityManager_0), value);
	}

	inline static int32_t get_offset_of_battleManager_1() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___battleManager_1)); }
	inline BattleManager_t4022130644 * get_battleManager_1() const { return ___battleManager_1; }
	inline BattleManager_t4022130644 ** get_address_of_battleManager_1() { return &___battleManager_1; }
	inline void set_battleManager_1(BattleManager_t4022130644 * value)
	{
		___battleManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___battleManager_1), value);
	}

	inline static int32_t get_offset_of_levelLoadManager_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___levelLoadManager_2)); }
	inline LevelLoadManager_t362334468 * get_levelLoadManager_2() const { return ___levelLoadManager_2; }
	inline LevelLoadManager_t362334468 ** get_address_of_levelLoadManager_2() { return &___levelLoadManager_2; }
	inline void set_levelLoadManager_2(LevelLoadManager_t362334468 * value)
	{
		___levelLoadManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelLoadManager_2), value);
	}

	inline static int32_t get_offset_of_buildingConstants_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___buildingConstants_3)); }
	inline Dictionary_2_t2441201299 * get_buildingConstants_3() const { return ___buildingConstants_3; }
	inline Dictionary_2_t2441201299 ** get_address_of_buildingConstants_3() { return &___buildingConstants_3; }
	inline void set_buildingConstants_3(Dictionary_2_t2441201299 * value)
	{
		___buildingConstants_3 = value;
		Il2CppCodeGenWriteBarrier((&___buildingConstants_3), value);
	}

	inline static int32_t get_offset_of_unitConstants_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___unitConstants_4)); }
	inline Dictionary_2_t3367445707 * get_unitConstants_4() const { return ___unitConstants_4; }
	inline Dictionary_2_t3367445707 ** get_address_of_unitConstants_4() { return &___unitConstants_4; }
	inline void set_unitConstants_4(Dictionary_2_t3367445707 * value)
	{
		___unitConstants_4 = value;
		Il2CppCodeGenWriteBarrier((&___unitConstants_4), value);
	}

	inline static int32_t get_offset_of_tutorials_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___tutorials_5)); }
	inline List_1_t1672770702 * get_tutorials_5() const { return ___tutorials_5; }
	inline List_1_t1672770702 ** get_address_of_tutorials_5() { return &___tutorials_5; }
	inline void set_tutorials_5(List_1_t1672770702 * value)
	{
		___tutorials_5 = value;
		Il2CppCodeGenWriteBarrier((&___tutorials_5), value);
	}

	inline static int32_t get_offset_of_residenceFields_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___residenceFields_6)); }
	inline Int64U5BU5D_t2559172825* get_residenceFields_6() const { return ___residenceFields_6; }
	inline Int64U5BU5D_t2559172825** get_address_of_residenceFields_6() { return &___residenceFields_6; }
	inline void set_residenceFields_6(Int64U5BU5D_t2559172825* value)
	{
		___residenceFields_6 = value;
		Il2CppCodeGenWriteBarrier((&___residenceFields_6), value);
	}

	inline static int32_t get_offset_of_onGetBuildingNames_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___onGetBuildingNames_7)); }
	inline SimpleEvent_t129249603 * get_onGetBuildingNames_7() const { return ___onGetBuildingNames_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetBuildingNames_7() { return &___onGetBuildingNames_7; }
	inline void set_onGetBuildingNames_7(SimpleEvent_t129249603 * value)
	{
		___onGetBuildingNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___onGetBuildingNames_7), value);
	}

	inline static int32_t get_offset_of_buildingIds_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___buildingIds_8)); }
	inline List_1_t3319525431 * get_buildingIds_8() const { return ___buildingIds_8; }
	inline List_1_t3319525431 ** get_address_of_buildingIds_8() { return &___buildingIds_8; }
	inline void set_buildingIds_8(List_1_t3319525431 * value)
	{
		___buildingIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___buildingIds_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef U3CGETINITIALBUILDINGCONSTANTSU3EC__ITERATOR0_T263283755_H
#define U3CGETINITIALBUILDINGCONSTANTSU3EC__ITERATOR0_T263283755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<GetInitialBuildingConstants>c__Iterator0
struct  U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest GameManager/<GetInitialBuildingConstants>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// GameManager GameManager/<GetInitialBuildingConstants>c__Iterator0::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<GetInitialBuildingConstants>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<GetInitialBuildingConstants>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<GetInitialBuildingConstants>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALBUILDINGCONSTANTSU3EC__ITERATOR0_T263283755_H
#ifndef U3CSTARTREVOLUTIONU3EC__ITERATOR8_T3221568021_H
#define U3CSTARTREVOLUTIONU3EC__ITERATOR8_T3221568021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<StartRevolution>c__Iterator8
struct  U3CStartRevolutionU3Ec__Iterator8_t3221568021  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CityManager/<StartRevolution>c__Iterator8::<revoltForm>__0
	WWWForm_t4064702195 * ___U3CrevoltFormU3E__0_0;
	// System.Int64 CityManager/<StartRevolution>c__Iterator8::cityID
	int64_t ___cityID_1;
	// UnityEngine.Networking.UnityWebRequest CityManager/<StartRevolution>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object CityManager/<StartRevolution>c__Iterator8::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CityManager/<StartRevolution>c__Iterator8::$disposing
	bool ___U24disposing_4;
	// System.Int32 CityManager/<StartRevolution>c__Iterator8::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrevoltFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___U3CrevoltFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CrevoltFormU3E__0_0() const { return ___U3CrevoltFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CrevoltFormU3E__0_0() { return &___U3CrevoltFormU3E__0_0; }
	inline void set_U3CrevoltFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CrevoltFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrevoltFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_cityID_1() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___cityID_1)); }
	inline int64_t get_cityID_1() const { return ___cityID_1; }
	inline int64_t* get_address_of_cityID_1() { return &___cityID_1; }
	inline void set_cityID_1(int64_t value)
	{
		___cityID_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartRevolutionU3Ec__Iterator8_t3221568021, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTREVOLUTIONU3EC__ITERATOR8_T3221568021_H
#ifndef U3CSETALLOWTROOPSU3EC__ITERATOR6_T3482264946_H
#define U3CSETALLOWTROOPSU3EC__ITERATOR6_T3482264946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<SetAllowTroops>c__Iterator6
struct  U3CSetAllowTroopsU3Ec__Iterator6_t3482264946  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CityManager/<SetAllowTroops>c__Iterator6::<setForm>__0
	WWWForm_t4064702195 * ___U3CsetFormU3E__0_0;
	// System.Boolean CityManager/<SetAllowTroops>c__Iterator6::flag
	bool ___flag_1;
	// UnityEngine.Networking.UnityWebRequest CityManager/<SetAllowTroops>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// CityManager CityManager/<SetAllowTroops>c__Iterator6::$this
	CityManager_t2587329200 * ___U24this_3;
	// System.Object CityManager/<SetAllowTroops>c__Iterator6::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean CityManager/<SetAllowTroops>c__Iterator6::$disposing
	bool ___U24disposing_5;
	// System.Int32 CityManager/<SetAllowTroops>c__Iterator6::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CsetFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U3CsetFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CsetFormU3E__0_0() const { return ___U3CsetFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CsetFormU3E__0_0() { return &___U3CsetFormU3E__0_0; }
	inline void set_U3CsetFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CsetFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_flag_1() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___flag_1)); }
	inline bool get_flag_1() const { return ___flag_1; }
	inline bool* get_address_of_flag_1() { return &___flag_1; }
	inline void set_flag_1(bool value)
	{
		___flag_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U24this_3)); }
	inline CityManager_t2587329200 * get_U24this_3() const { return ___U24this_3; }
	inline CityManager_t2587329200 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CityManager_t2587329200 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSetAllowTroopsU3Ec__Iterator6_t3482264946, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETALLOWTROOPSU3EC__ITERATOR6_T3482264946_H
#ifndef U3CUPDATECITYTAXU3EC__ITERATOR7_T2241765318_H
#define U3CUPDATECITYTAXU3EC__ITERATOR7_T2241765318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<UpdateCityTax>c__Iterator7
struct  U3CUpdateCityTaxU3Ec__Iterator7_t2241765318  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CityManager/<UpdateCityTax>c__Iterator7::<setForm>__0
	WWWForm_t4064702195 * ___U3CsetFormU3E__0_0;
	// System.Int64 CityManager/<UpdateCityTax>c__Iterator7::taxRate
	int64_t ___taxRate_1;
	// UnityEngine.Networking.UnityWebRequest CityManager/<UpdateCityTax>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// CityManager CityManager/<UpdateCityTax>c__Iterator7::$this
	CityManager_t2587329200 * ___U24this_3;
	// System.Object CityManager/<UpdateCityTax>c__Iterator7::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean CityManager/<UpdateCityTax>c__Iterator7::$disposing
	bool ___U24disposing_5;
	// System.Int32 CityManager/<UpdateCityTax>c__Iterator7::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CsetFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U3CsetFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CsetFormU3E__0_0() const { return ___U3CsetFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CsetFormU3E__0_0() { return &___U3CsetFormU3E__0_0; }
	inline void set_U3CsetFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CsetFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_taxRate_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___taxRate_1)); }
	inline int64_t get_taxRate_1() const { return ___taxRate_1; }
	inline int64_t* get_address_of_taxRate_1() { return &___taxRate_1; }
	inline void set_taxRate_1(int64_t value)
	{
		___taxRate_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U24this_3)); }
	inline CityManager_t2587329200 * get_U24this_3() const { return ___U24this_3; }
	inline CityManager_t2587329200 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CityManager_t2587329200 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUpdateCityTaxU3Ec__Iterator7_t2241765318, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECITYTAXU3EC__ITERATOR7_T2241765318_H
#ifndef U3CGETBUILDINGINITIALCONSTANTFORNAMEU3EC__ITERATOR1_T1766459497_H
#define U3CGETBUILDINGINITIALCONSTANTFORNAMEU3EC__ITERATOR1_T1766459497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<GetBuildingInitialConstantForName>c__Iterator1
struct  U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497  : public RuntimeObject
{
public:
	// System.String GameManager/<GetBuildingInitialConstantForName>c__Iterator1::id
	String_t* ___id_0;
	// UnityEngine.Networking.UnityWebRequest GameManager/<GetBuildingInitialConstantForName>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// GameManager GameManager/<GetBuildingInitialConstantForName>c__Iterator1::$this
	GameManager_t1536523654 * ___U24this_2;
	// System.Object GameManager/<GetBuildingInitialConstantForName>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GameManager/<GetBuildingInitialConstantForName>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameManager/<GetBuildingInitialConstantForName>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___U24this_2)); }
	inline GameManager_t1536523654 * get_U24this_2() const { return ___U24this_2; }
	inline GameManager_t1536523654 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GameManager_t1536523654 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBUILDINGINITIALCONSTANTFORNAMEU3EC__ITERATOR1_T1766459497_H
#ifndef U3CREGISTERDEVICETOKENU3EC__ITERATOR0_T3352232145_H
#define U3CREGISTERDEVICETOKENU3EC__ITERATOR0_T3352232145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GCMManager/<RegisterDeviceToken>c__Iterator0
struct  U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145  : public RuntimeObject
{
public:
	// System.String GCMManager/<RegisterDeviceToken>c__Iterator0::<addressString>__0
	String_t* ___U3CaddressStringU3E__0_0;
	// UnityEngine.WWWForm GCMManager/<RegisterDeviceToken>c__Iterator0::<userForm>__0
	WWWForm_t4064702195 * ___U3CuserFormU3E__0_1;
	// System.String GCMManager/<RegisterDeviceToken>c__Iterator0::tokenString
	String_t* ___tokenString_2;
	// UnityEngine.Networking.UnityWebRequest GCMManager/<RegisterDeviceToken>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Object GCMManager/<RegisterDeviceToken>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean GCMManager/<RegisterDeviceToken>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 GCMManager/<RegisterDeviceToken>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CaddressStringU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U3CaddressStringU3E__0_0)); }
	inline String_t* get_U3CaddressStringU3E__0_0() const { return ___U3CaddressStringU3E__0_0; }
	inline String_t** get_address_of_U3CaddressStringU3E__0_0() { return &___U3CaddressStringU3E__0_0; }
	inline void set_U3CaddressStringU3E__0_0(String_t* value)
	{
		___U3CaddressStringU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddressStringU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CuserFormU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U3CuserFormU3E__0_1)); }
	inline WWWForm_t4064702195 * get_U3CuserFormU3E__0_1() const { return ___U3CuserFormU3E__0_1; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuserFormU3E__0_1() { return &___U3CuserFormU3E__0_1; }
	inline void set_U3CuserFormU3E__0_1(WWWForm_t4064702195 * value)
	{
		___U3CuserFormU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserFormU3E__0_1), value);
	}

	inline static int32_t get_offset_of_tokenString_2() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___tokenString_2)); }
	inline String_t* get_tokenString_2() const { return ___tokenString_2; }
	inline String_t** get_address_of_tokenString_2() { return &___tokenString_2; }
	inline void set_tokenString_2(String_t* value)
	{
		___tokenString_2 = value;
		Il2CppCodeGenWriteBarrier((&___tokenString_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREGISTERDEVICETOKENU3EC__ITERATOR0_T3352232145_H
#ifndef U3CUPGRADEVALLEYU3EC__ITERATOR5_T2222192536_H
#define U3CUPGRADEVALLEYU3EC__ITERATOR5_T2222192536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<UpgradeValley>c__Iterator5
struct  U3CUpgradeValleyU3Ec__Iterator5_t2222192536  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<UpgradeValley>c__Iterator5::<upgradeForm>__0
	WWWForm_t4064702195 * ___U3CupgradeFormU3E__0_0;
	// System.Int64 BuildingsManager/<UpgradeValley>c__Iterator5::valley_id
	int64_t ___valley_id_1;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<UpgradeValley>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BuildingsManager BuildingsManager/<UpgradeValley>c__Iterator5::$this
	BuildingsManager_t3721263023 * ___U24this_3;
	// System.Object BuildingsManager/<UpgradeValley>c__Iterator5::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BuildingsManager/<UpgradeValley>c__Iterator5::$disposing
	bool ___U24disposing_5;
	// System.Int32 BuildingsManager/<UpgradeValley>c__Iterator5::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CupgradeFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U3CupgradeFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CupgradeFormU3E__0_0() const { return ___U3CupgradeFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CupgradeFormU3E__0_0() { return &___U3CupgradeFormU3E__0_0; }
	inline void set_U3CupgradeFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CupgradeFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupgradeFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_valley_id_1() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___valley_id_1)); }
	inline int64_t get_valley_id_1() const { return ___valley_id_1; }
	inline int64_t* get_address_of_valley_id_1() { return &___valley_id_1; }
	inline void set_valley_id_1(int64_t value)
	{
		___valley_id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U24this_3)); }
	inline BuildingsManager_t3721263023 * get_U24this_3() const { return ___U24this_3; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BuildingsManager_t3721263023 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUpgradeValleyU3Ec__Iterator5_t2222192536, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPGRADEVALLEYU3EC__ITERATOR5_T2222192536_H
#ifndef U3CDOWNLOADTUTORIALIMAGEU3EC__ITERATOR4_T4140255223_H
#define U3CDOWNLOADTUTORIALIMAGEU3EC__ITERATOR4_T4140255223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<DownloadTutorialImage>c__Iterator4
struct  U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223  : public RuntimeObject
{
public:
	// System.String GameManager/<DownloadTutorialImage>c__Iterator4::url
	String_t* ___url_0;
	// UnityEngine.Networking.UnityWebRequest GameManager/<DownloadTutorialImage>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// System.Int64 GameManager/<DownloadTutorialImage>c__Iterator4::tutorialID
	int64_t ___tutorialID_2;
	// System.Object GameManager/<DownloadTutorialImage>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GameManager/<DownloadTutorialImage>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameManager/<DownloadTutorialImage>c__Iterator4::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_tutorialID_2() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___tutorialID_2)); }
	inline int64_t get_tutorialID_2() const { return ___tutorialID_2; }
	inline int64_t* get_address_of_tutorialID_2() { return &___tutorialID_2; }
	inline void set_tutorialID_2(int64_t value)
	{
		___tutorialID_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADTUTORIALIMAGEU3EC__ITERATOR4_T4140255223_H
#ifndef U3CGETINITIALUNITCONSTANTSU3EC__ITERATOR2_T3804170832_H
#define U3CGETINITIALUNITCONSTANTSU3EC__ITERATOR2_T3804170832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<GetInitialUnitConstants>c__Iterator2
struct  U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest GameManager/<GetInitialUnitConstants>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// GameManager GameManager/<GetInitialUnitConstants>c__Iterator2::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<GetInitialUnitConstants>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<GetInitialUnitConstants>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<GetInitialUnitConstants>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALUNITCONSTANTSU3EC__ITERATOR2_T3804170832_H
#ifndef U3CGETTUTORIALPAGESU3EC__ITERATOR3_T1358694543_H
#define U3CGETTUTORIALPAGESU3EC__ITERATOR3_T1358694543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<GetTutorialPages>c__Iterator3
struct  U3CGetTutorialPagesU3Ec__Iterator3_t1358694543  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest GameManager/<GetTutorialPages>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// GameManager GameManager/<GetTutorialPages>c__Iterator3::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<GetTutorialPages>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<GetTutorialPages>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<GetTutorialPages>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetTutorialPagesU3Ec__Iterator3_t1358694543, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetTutorialPagesU3Ec__Iterator3_t1358694543, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetTutorialPagesU3Ec__Iterator3_t1358694543, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetTutorialPagesU3Ec__Iterator3_t1358694543, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetTutorialPagesU3Ec__Iterator3_t1358694543, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTUTORIALPAGESU3EC__ITERATOR3_T1358694543_H
#ifndef U3CUPDATECITYALLOWTROOPSU3EC__ITERATOR5_T166018786_H
#define U3CUPDATECITYALLOWTROOPSU3EC__ITERATOR5_T166018786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<UpdateCityAllowTroops>c__Iterator5
struct  U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest CityManager/<UpdateCityAllowTroops>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// CityManager CityManager/<UpdateCityAllowTroops>c__Iterator5::$this
	CityManager_t2587329200 * ___U24this_1;
	// System.Object CityManager/<UpdateCityAllowTroops>c__Iterator5::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CityManager/<UpdateCityAllowTroops>c__Iterator5::$disposing
	bool ___U24disposing_3;
	// System.Int32 CityManager/<UpdateCityAllowTroops>c__Iterator5::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786, ___U24this_1)); }
	inline CityManager_t2587329200 * get_U24this_1() const { return ___U24this_1; }
	inline CityManager_t2587329200 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CityManager_t2587329200 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECITYALLOWTROOPSU3EC__ITERATOR5_T166018786_H
#ifndef U3CCANCELBUILDINGU3EC__ITERATOR9_T1915633598_H
#define U3CCANCELBUILDINGU3EC__ITERATOR9_T1915633598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<CancelBuilding>c__Iterator9
struct  U3CCancelBuildingU3Ec__Iterator9_t1915633598  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<CancelBuilding>c__Iterator9::<cancelForm>__0
	WWWForm_t4064702195 * ___U3CcancelFormU3E__0_0;
	// System.Int64 BuildingsManager/<CancelBuilding>c__Iterator9::event_id
	int64_t ___event_id_1;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<CancelBuilding>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BuildingsManager BuildingsManager/<CancelBuilding>c__Iterator9::$this
	BuildingsManager_t3721263023 * ___U24this_3;
	// System.Object BuildingsManager/<CancelBuilding>c__Iterator9::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BuildingsManager/<CancelBuilding>c__Iterator9::$disposing
	bool ___U24disposing_5;
	// System.Int32 BuildingsManager/<CancelBuilding>c__Iterator9::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcancelFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U3CcancelFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CcancelFormU3E__0_0() const { return ___U3CcancelFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CcancelFormU3E__0_0() { return &___U3CcancelFormU3E__0_0; }
	inline void set_U3CcancelFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CcancelFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcancelFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_event_id_1() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___event_id_1)); }
	inline int64_t get_event_id_1() const { return ___event_id_1; }
	inline int64_t* get_address_of_event_id_1() { return &___event_id_1; }
	inline void set_event_id_1(int64_t value)
	{
		___event_id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U24this_3)); }
	inline BuildingsManager_t3721263023 * get_U24this_3() const { return ___U24this_3; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BuildingsManager_t3721263023 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelBuildingU3Ec__Iterator9_t1915633598, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELBUILDINGU3EC__ITERATOR9_T1915633598_H
#ifndef U3CCANCELVALLEYU3EC__ITERATORA_T3260069651_H
#define U3CCANCELVALLEYU3EC__ITERATORA_T3260069651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<CancelValley>c__IteratorA
struct  U3CCancelValleyU3Ec__IteratorA_t3260069651  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<CancelValley>c__IteratorA::<cancelForm>__0
	WWWForm_t4064702195 * ___U3CcancelFormU3E__0_0;
	// System.Int64 BuildingsManager/<CancelValley>c__IteratorA::event_id
	int64_t ___event_id_1;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<CancelValley>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BuildingsManager BuildingsManager/<CancelValley>c__IteratorA::$this
	BuildingsManager_t3721263023 * ___U24this_3;
	// System.Object BuildingsManager/<CancelValley>c__IteratorA::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BuildingsManager/<CancelValley>c__IteratorA::$disposing
	bool ___U24disposing_5;
	// System.Int32 BuildingsManager/<CancelValley>c__IteratorA::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcancelFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U3CcancelFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CcancelFormU3E__0_0() const { return ___U3CcancelFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CcancelFormU3E__0_0() { return &___U3CcancelFormU3E__0_0; }
	inline void set_U3CcancelFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CcancelFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcancelFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_event_id_1() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___event_id_1)); }
	inline int64_t get_event_id_1() const { return ___event_id_1; }
	inline int64_t* get_address_of_event_id_1() { return &___event_id_1; }
	inline void set_event_id_1(int64_t value)
	{
		___event_id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U24this_3)); }
	inline BuildingsManager_t3721263023 * get_U24this_3() const { return ___U24this_3; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BuildingsManager_t3721263023 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCancelValleyU3Ec__IteratorA_t3260069651, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANCELVALLEYU3EC__ITERATORA_T3260069651_H
#ifndef U3CDESTROYBUILDINGU3EC__ITERATOR8_T3296167326_H
#define U3CDESTROYBUILDINGU3EC__ITERATOR8_T3296167326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<DestroyBuilding>c__Iterator8
struct  U3CDestroyBuildingU3Ec__Iterator8_t3296167326  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<DestroyBuilding>c__Iterator8::<destroyForm>__0
	WWWForm_t4064702195 * ___U3CdestroyFormU3E__0_0;
	// System.Int64 BuildingsManager/<DestroyBuilding>c__Iterator8::buildingID
	int64_t ___buildingID_1;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<DestroyBuilding>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BuildingsManager BuildingsManager/<DestroyBuilding>c__Iterator8::$this
	BuildingsManager_t3721263023 * ___U24this_3;
	// System.Object BuildingsManager/<DestroyBuilding>c__Iterator8::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BuildingsManager/<DestroyBuilding>c__Iterator8::$disposing
	bool ___U24disposing_5;
	// System.Int32 BuildingsManager/<DestroyBuilding>c__Iterator8::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdestroyFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U3CdestroyFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdestroyFormU3E__0_0() const { return ___U3CdestroyFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdestroyFormU3E__0_0() { return &___U3CdestroyFormU3E__0_0; }
	inline void set_U3CdestroyFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdestroyFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdestroyFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_buildingID_1() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___buildingID_1)); }
	inline int64_t get_buildingID_1() const { return ___buildingID_1; }
	inline int64_t* get_address_of_buildingID_1() { return &___buildingID_1; }
	inline void set_buildingID_1(int64_t value)
	{
		___buildingID_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U24this_3)); }
	inline BuildingsManager_t3721263023 * get_U24this_3() const { return ___U24this_3; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BuildingsManager_t3721263023 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDestroyBuildingU3Ec__Iterator8_t3296167326, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBUILDINGU3EC__ITERATOR8_T3296167326_H
#ifndef U3CDOWNGRADEVALLEYU3EC__ITERATOR6_T309172924_H
#define U3CDOWNGRADEVALLEYU3EC__ITERATOR6_T309172924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<DowngradeValley>c__Iterator6
struct  U3CDowngradeValleyU3Ec__Iterator6_t309172924  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<DowngradeValley>c__Iterator6::<downgradeForm>__0
	WWWForm_t4064702195 * ___U3CdowngradeFormU3E__0_0;
	// System.Int64 BuildingsManager/<DowngradeValley>c__Iterator6::valley_id
	int64_t ___valley_id_1;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<DowngradeValley>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// BuildingsManager BuildingsManager/<DowngradeValley>c__Iterator6::$this
	BuildingsManager_t3721263023 * ___U24this_3;
	// System.Object BuildingsManager/<DowngradeValley>c__Iterator6::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BuildingsManager/<DowngradeValley>c__Iterator6::$disposing
	bool ___U24disposing_5;
	// System.Int32 BuildingsManager/<DowngradeValley>c__Iterator6::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdowngradeFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U3CdowngradeFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CdowngradeFormU3E__0_0() const { return ___U3CdowngradeFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CdowngradeFormU3E__0_0() { return &___U3CdowngradeFormU3E__0_0; }
	inline void set_U3CdowngradeFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CdowngradeFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdowngradeFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_valley_id_1() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___valley_id_1)); }
	inline int64_t get_valley_id_1() const { return ___valley_id_1; }
	inline int64_t* get_address_of_valley_id_1() { return &___valley_id_1; }
	inline void set_valley_id_1(int64_t value)
	{
		___valley_id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U24this_3)); }
	inline BuildingsManager_t3721263023 * get_U24this_3() const { return ___U24this_3; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(BuildingsManager_t3721263023 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDowngradeValleyU3Ec__Iterator6_t309172924, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNGRADEVALLEYU3EC__ITERATOR6_T309172924_H
#ifndef U3CADDBUILDINGU3EC__ITERATOR7_T3864423245_H
#define U3CADDBUILDINGU3EC__ITERATOR7_T3864423245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager/<AddBuilding>c__Iterator7
struct  U3CAddBuildingU3Ec__Iterator7_t3864423245  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm BuildingsManager/<AddBuilding>c__Iterator7::<buildForm>__0
	WWWForm_t4064702195 * ___U3CbuildFormU3E__0_0;
	// System.String BuildingsManager/<AddBuilding>c__Iterator7::name
	String_t* ___name_1;
	// System.Int64 BuildingsManager/<AddBuilding>c__Iterator7::city_id
	int64_t ___city_id_2;
	// System.Int64 BuildingsManager/<AddBuilding>c__Iterator7::pit_id
	int64_t ___pit_id_3;
	// System.String BuildingsManager/<AddBuilding>c__Iterator7::location
	String_t* ___location_4;
	// UnityEngine.Networking.UnityWebRequest BuildingsManager/<AddBuilding>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// BuildingsManager BuildingsManager/<AddBuilding>c__Iterator7::$this
	BuildingsManager_t3721263023 * ___U24this_6;
	// System.Object BuildingsManager/<AddBuilding>c__Iterator7::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean BuildingsManager/<AddBuilding>c__Iterator7::$disposing
	bool ___U24disposing_8;
	// System.Int32 BuildingsManager/<AddBuilding>c__Iterator7::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CbuildFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U3CbuildFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CbuildFormU3E__0_0() const { return ___U3CbuildFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CbuildFormU3E__0_0() { return &___U3CbuildFormU3E__0_0; }
	inline void set_U3CbuildFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CbuildFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuildFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_city_id_2() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___city_id_2)); }
	inline int64_t get_city_id_2() const { return ___city_id_2; }
	inline int64_t* get_address_of_city_id_2() { return &___city_id_2; }
	inline void set_city_id_2(int64_t value)
	{
		___city_id_2 = value;
	}

	inline static int32_t get_offset_of_pit_id_3() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___pit_id_3)); }
	inline int64_t get_pit_id_3() const { return ___pit_id_3; }
	inline int64_t* get_address_of_pit_id_3() { return &___pit_id_3; }
	inline void set_pit_id_3(int64_t value)
	{
		___pit_id_3 = value;
	}

	inline static int32_t get_offset_of_location_4() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___location_4)); }
	inline String_t* get_location_4() const { return ___location_4; }
	inline String_t** get_address_of_location_4() { return &___location_4; }
	inline void set_location_4(String_t* value)
	{
		___location_4 = value;
		Il2CppCodeGenWriteBarrier((&___location_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U24this_6)); }
	inline BuildingsManager_t3721263023 * get_U24this_6() const { return ___U24this_6; }
	inline BuildingsManager_t3721263023 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(BuildingsManager_t3721263023 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAddBuildingU3Ec__Iterator7_t3864423245, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDBUILDINGU3EC__ITERATOR7_T3864423245_H
#ifndef CITYMANAGER_T2587329200_H
#define CITYMANAGER_T2587329200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager
struct  CityManager_t2587329200  : public RuntimeObject
{
public:
	// ResourceManager CityManager::resourceManager
	ResourceManager_t484397614 * ___resourceManager_0;
	// UnitsManager CityManager::unitsManager
	UnitsManager_t4062574081 * ___unitsManager_1;
	// BuildingsManager CityManager::buildingsManager
	BuildingsManager_t3721263023 * ___buildingsManager_2;
	// TreasureManager CityManager::treasureManager
	TreasureManager_t110095690 * ___treasureManager_3;
	// SimpleEvent CityManager::onCityBuilt
	SimpleEvent_t129249603 * ___onCityBuilt_4;
	// SimpleEvent CityManager::onGotNewCity
	SimpleEvent_t129249603 * ___onGotNewCity_5;
	// SimpleEvent CityManager::onCityNameAndIconChanged
	SimpleEvent_t129249603 * ___onCityNameAndIconChanged_6;
	// SimpleEvent CityManager::cityChangerUpdateImage
	SimpleEvent_t129249603 * ___cityChangerUpdateImage_7;
	// SimpleEvent CityManager::gotAllowTroopsState
	SimpleEvent_t129249603 * ___gotAllowTroopsState_8;
	// SimpleEvent CityManager::onTaxUpdated
	SimpleEvent_t129249603 * ___onTaxUpdated_9;
	// MyCityModel CityManager::currentCity
	MyCityModel_t3961736920 * ___currentCity_10;
	// System.Collections.Generic.Dictionary`2<System.Int64,MyCityModel> CityManager::myCities
	Dictionary_2_t729409408 * ___myCities_11;

public:
	inline static int32_t get_offset_of_resourceManager_0() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___resourceManager_0)); }
	inline ResourceManager_t484397614 * get_resourceManager_0() const { return ___resourceManager_0; }
	inline ResourceManager_t484397614 ** get_address_of_resourceManager_0() { return &___resourceManager_0; }
	inline void set_resourceManager_0(ResourceManager_t484397614 * value)
	{
		___resourceManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourceManager_0), value);
	}

	inline static int32_t get_offset_of_unitsManager_1() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___unitsManager_1)); }
	inline UnitsManager_t4062574081 * get_unitsManager_1() const { return ___unitsManager_1; }
	inline UnitsManager_t4062574081 ** get_address_of_unitsManager_1() { return &___unitsManager_1; }
	inline void set_unitsManager_1(UnitsManager_t4062574081 * value)
	{
		___unitsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___unitsManager_1), value);
	}

	inline static int32_t get_offset_of_buildingsManager_2() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___buildingsManager_2)); }
	inline BuildingsManager_t3721263023 * get_buildingsManager_2() const { return ___buildingsManager_2; }
	inline BuildingsManager_t3721263023 ** get_address_of_buildingsManager_2() { return &___buildingsManager_2; }
	inline void set_buildingsManager_2(BuildingsManager_t3721263023 * value)
	{
		___buildingsManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildingsManager_2), value);
	}

	inline static int32_t get_offset_of_treasureManager_3() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___treasureManager_3)); }
	inline TreasureManager_t110095690 * get_treasureManager_3() const { return ___treasureManager_3; }
	inline TreasureManager_t110095690 ** get_address_of_treasureManager_3() { return &___treasureManager_3; }
	inline void set_treasureManager_3(TreasureManager_t110095690 * value)
	{
		___treasureManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___treasureManager_3), value);
	}

	inline static int32_t get_offset_of_onCityBuilt_4() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onCityBuilt_4)); }
	inline SimpleEvent_t129249603 * get_onCityBuilt_4() const { return ___onCityBuilt_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onCityBuilt_4() { return &___onCityBuilt_4; }
	inline void set_onCityBuilt_4(SimpleEvent_t129249603 * value)
	{
		___onCityBuilt_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCityBuilt_4), value);
	}

	inline static int32_t get_offset_of_onGotNewCity_5() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onGotNewCity_5)); }
	inline SimpleEvent_t129249603 * get_onGotNewCity_5() const { return ___onGotNewCity_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onGotNewCity_5() { return &___onGotNewCity_5; }
	inline void set_onGotNewCity_5(SimpleEvent_t129249603 * value)
	{
		___onGotNewCity_5 = value;
		Il2CppCodeGenWriteBarrier((&___onGotNewCity_5), value);
	}

	inline static int32_t get_offset_of_onCityNameAndIconChanged_6() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onCityNameAndIconChanged_6)); }
	inline SimpleEvent_t129249603 * get_onCityNameAndIconChanged_6() const { return ___onCityNameAndIconChanged_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onCityNameAndIconChanged_6() { return &___onCityNameAndIconChanged_6; }
	inline void set_onCityNameAndIconChanged_6(SimpleEvent_t129249603 * value)
	{
		___onCityNameAndIconChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___onCityNameAndIconChanged_6), value);
	}

	inline static int32_t get_offset_of_cityChangerUpdateImage_7() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___cityChangerUpdateImage_7)); }
	inline SimpleEvent_t129249603 * get_cityChangerUpdateImage_7() const { return ___cityChangerUpdateImage_7; }
	inline SimpleEvent_t129249603 ** get_address_of_cityChangerUpdateImage_7() { return &___cityChangerUpdateImage_7; }
	inline void set_cityChangerUpdateImage_7(SimpleEvent_t129249603 * value)
	{
		___cityChangerUpdateImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___cityChangerUpdateImage_7), value);
	}

	inline static int32_t get_offset_of_gotAllowTroopsState_8() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___gotAllowTroopsState_8)); }
	inline SimpleEvent_t129249603 * get_gotAllowTroopsState_8() const { return ___gotAllowTroopsState_8; }
	inline SimpleEvent_t129249603 ** get_address_of_gotAllowTroopsState_8() { return &___gotAllowTroopsState_8; }
	inline void set_gotAllowTroopsState_8(SimpleEvent_t129249603 * value)
	{
		___gotAllowTroopsState_8 = value;
		Il2CppCodeGenWriteBarrier((&___gotAllowTroopsState_8), value);
	}

	inline static int32_t get_offset_of_onTaxUpdated_9() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___onTaxUpdated_9)); }
	inline SimpleEvent_t129249603 * get_onTaxUpdated_9() const { return ___onTaxUpdated_9; }
	inline SimpleEvent_t129249603 ** get_address_of_onTaxUpdated_9() { return &___onTaxUpdated_9; }
	inline void set_onTaxUpdated_9(SimpleEvent_t129249603 * value)
	{
		___onTaxUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTaxUpdated_9), value);
	}

	inline static int32_t get_offset_of_currentCity_10() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___currentCity_10)); }
	inline MyCityModel_t3961736920 * get_currentCity_10() const { return ___currentCity_10; }
	inline MyCityModel_t3961736920 ** get_address_of_currentCity_10() { return &___currentCity_10; }
	inline void set_currentCity_10(MyCityModel_t3961736920 * value)
	{
		___currentCity_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentCity_10), value);
	}

	inline static int32_t get_offset_of_myCities_11() { return static_cast<int32_t>(offsetof(CityManager_t2587329200, ___myCities_11)); }
	inline Dictionary_2_t729409408 * get_myCities_11() const { return ___myCities_11; }
	inline Dictionary_2_t729409408 ** get_address_of_myCities_11() { return &___myCities_11; }
	inline void set_myCities_11(Dictionary_2_t729409408 * value)
	{
		___myCities_11 = value;
		Il2CppCodeGenWriteBarrier((&___myCities_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITYMANAGER_T2587329200_H
#ifndef U3CUPDATECURRENTCITYCOORDSU3EC__ITERATOR3_T3359773593_H
#define U3CUPDATECURRENTCITYCOORDSU3EC__ITERATOR3_T3359773593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<UpdateCurrentCityCoords>c__Iterator3
struct  U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest CityManager/<UpdateCurrentCityCoords>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// CityManager CityManager/<UpdateCurrentCityCoords>c__Iterator3::$this
	CityManager_t2587329200 * ___U24this_1;
	// System.Object CityManager/<UpdateCurrentCityCoords>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CityManager/<UpdateCurrentCityCoords>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 CityManager/<UpdateCurrentCityCoords>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593, ___U24this_1)); }
	inline CityManager_t2587329200 * get_U24this_1() const { return ___U24this_1; }
	inline CityManager_t2587329200 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CityManager_t2587329200 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECURRENTCITYCOORDSU3EC__ITERATOR3_T3359773593_H
#ifndef U3CUPDATECITYEXPANTIONU3EC__ITERATOR4_T3076708588_H
#define U3CUPDATECITYEXPANTIONU3EC__ITERATOR4_T3076708588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<UpdateCityExpantion>c__Iterator4
struct  U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest CityManager/<UpdateCityExpantion>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// CityManager CityManager/<UpdateCityExpantion>c__Iterator4::$this
	CityManager_t2587329200 * ___U24this_1;
	// System.Object CityManager/<UpdateCityExpantion>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CityManager/<UpdateCityExpantion>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 CityManager/<UpdateCityExpantion>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588, ___U24this_1)); }
	inline CityManager_t2587329200 * get_U24this_1() const { return ___U24this_1; }
	inline CityManager_t2587329200 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CityManager_t2587329200 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECITYEXPANTIONU3EC__ITERATOR4_T3076708588_H
#ifndef U3CCHANGENAMEANDIMAGEU3EC__ITERATOR2_T708341679_H
#define U3CCHANGENAMEANDIMAGEU3EC__ITERATOR2_T708341679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<ChangeNameAndImage>c__Iterator2
struct  U3CChangeNameAndImageU3Ec__Iterator2_t708341679  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CityManager/<ChangeNameAndImage>c__Iterator2::<changeForm>__0
	WWWForm_t4064702195 * ___U3CchangeFormU3E__0_0;
	// System.String CityManager/<ChangeNameAndImage>c__Iterator2::cityName
	String_t* ___cityName_1;
	// System.String CityManager/<ChangeNameAndImage>c__Iterator2::imageName
	String_t* ___imageName_2;
	// UnityEngine.Networking.UnityWebRequest CityManager/<ChangeNameAndImage>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// CityManager CityManager/<ChangeNameAndImage>c__Iterator2::$this
	CityManager_t2587329200 * ___U24this_4;
	// System.Object CityManager/<ChangeNameAndImage>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean CityManager/<ChangeNameAndImage>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 CityManager/<ChangeNameAndImage>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CchangeFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U3CchangeFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CchangeFormU3E__0_0() const { return ___U3CchangeFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CchangeFormU3E__0_0() { return &___U3CchangeFormU3E__0_0; }
	inline void set_U3CchangeFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CchangeFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchangeFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_cityName_1() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___cityName_1)); }
	inline String_t* get_cityName_1() const { return ___cityName_1; }
	inline String_t** get_address_of_cityName_1() { return &___cityName_1; }
	inline void set_cityName_1(String_t* value)
	{
		___cityName_1 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_1), value);
	}

	inline static int32_t get_offset_of_imageName_2() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___imageName_2)); }
	inline String_t* get_imageName_2() const { return ___imageName_2; }
	inline String_t** get_address_of_imageName_2() { return &___imageName_2; }
	inline void set_imageName_2(String_t* value)
	{
		___imageName_2 = value;
		Il2CppCodeGenWriteBarrier((&___imageName_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U24this_4)); }
	inline CityManager_t2587329200 * get_U24this_4() const { return ___U24this_4; }
	inline CityManager_t2587329200 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CityManager_t2587329200 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CChangeNameAndImageU3Ec__Iterator2_t708341679, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGENAMEANDIMAGEU3EC__ITERATOR2_T708341679_H
#ifndef U3CGETINITIALCITIESU3EC__ITERATOR0_T423452736_H
#define U3CGETINITIALCITIESU3EC__ITERATOR0_T423452736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<GetInitialCities>c__Iterator0
struct  U3CGetInitialCitiesU3Ec__Iterator0_t423452736  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest CityManager/<GetInitialCities>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// CityManager CityManager/<GetInitialCities>c__Iterator0::$this
	CityManager_t2587329200 * ___U24this_1;
	// System.Object CityManager/<GetInitialCities>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CityManager/<GetInitialCities>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CityManager/<GetInitialCities>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetInitialCitiesU3Ec__Iterator0_t423452736, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetInitialCitiesU3Ec__Iterator0_t423452736, ___U24this_1)); }
	inline CityManager_t2587329200 * get_U24this_1() const { return ___U24this_1; }
	inline CityManager_t2587329200 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CityManager_t2587329200 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetInitialCitiesU3Ec__Iterator0_t423452736, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetInitialCitiesU3Ec__Iterator0_t423452736, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetInitialCitiesU3Ec__Iterator0_t423452736, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETINITIALCITIESU3EC__ITERATOR0_T423452736_H
#ifndef U3CBUILDCITYU3EC__ITERATOR1_T2749806825_H
#define U3CBUILDCITYU3EC__ITERATOR1_T2749806825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityManager/<BuildCity>c__Iterator1
struct  U3CBuildCityU3Ec__Iterator1_t2749806825  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CityManager/<BuildCity>c__Iterator1::<newCityForm>__0
	WWWForm_t4064702195 * ___U3CnewCityFormU3E__0_0;
	// System.Int64 CityManager/<BuildCity>c__Iterator1::x
	int64_t ___x_1;
	// System.Int64 CityManager/<BuildCity>c__Iterator1::y
	int64_t ___y_2;
	// System.String CityManager/<BuildCity>c__Iterator1::cityName
	String_t* ___cityName_3;
	// UnityEngine.Networking.UnityWebRequest CityManager/<BuildCity>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// CityManager CityManager/<BuildCity>c__Iterator1::$this
	CityManager_t2587329200 * ___U24this_5;
	// System.Object CityManager/<BuildCity>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean CityManager/<BuildCity>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 CityManager/<BuildCity>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CnewCityFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U3CnewCityFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CnewCityFormU3E__0_0() const { return ___U3CnewCityFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CnewCityFormU3E__0_0() { return &___U3CnewCityFormU3E__0_0; }
	inline void set_U3CnewCityFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CnewCityFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewCityFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___x_1)); }
	inline int64_t get_x_1() const { return ___x_1; }
	inline int64_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int64_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___y_2)); }
	inline int64_t get_y_2() const { return ___y_2; }
	inline int64_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int64_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___cityName_3)); }
	inline String_t* get_cityName_3() const { return ___cityName_3; }
	inline String_t** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(String_t* value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U24this_5)); }
	inline CityManager_t2587329200 * get_U24this_5() const { return ___U24this_5; }
	inline CityManager_t2587329200 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CityManager_t2587329200 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CBuildCityU3Ec__Iterator1_t2749806825, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDCITYU3EC__ITERATOR1_T2749806825_H
#ifndef PLAYERMANAGER_T1349889689_H
#define PLAYERMANAGER_T1349889689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1349889689  : public RuntimeObject
{
public:
	// ReportManager PlayerManager::reportManager
	ReportManager_t1122460921 * ___reportManager_0;
	// AllianceManager PlayerManager::allianceManager
	AllianceManager_t344187419 * ___allianceManager_1;
	// LoginManager PlayerManager::loginManager
	LoginManager_t1249555276 * ___loginManager_2;
	// UserModel PlayerManager::user
	UserModel_t1353931605 * ___user_3;
	// SimpleEvent PlayerManager::onGetMyUserInfo
	SimpleEvent_t129249603 * ___onGetMyUserInfo_4;
	// SimpleEvent PlayerManager::onLanguageChanged
	SimpleEvent_t129249603 * ___onLanguageChanged_5;
	// SimpleEvent PlayerManager::onPasswordChanged
	SimpleEvent_t129249603 * ___onPasswordChanged_6;
	// SimpleEvent PlayerManager::onResetPasswordRequested
	SimpleEvent_t129249603 * ___onResetPasswordRequested_7;
	// SimpleEvent PlayerManager::onPasswordReset
	SimpleEvent_t129249603 * ___onPasswordReset_8;

public:
	inline static int32_t get_offset_of_reportManager_0() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___reportManager_0)); }
	inline ReportManager_t1122460921 * get_reportManager_0() const { return ___reportManager_0; }
	inline ReportManager_t1122460921 ** get_address_of_reportManager_0() { return &___reportManager_0; }
	inline void set_reportManager_0(ReportManager_t1122460921 * value)
	{
		___reportManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___reportManager_0), value);
	}

	inline static int32_t get_offset_of_allianceManager_1() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___allianceManager_1)); }
	inline AllianceManager_t344187419 * get_allianceManager_1() const { return ___allianceManager_1; }
	inline AllianceManager_t344187419 ** get_address_of_allianceManager_1() { return &___allianceManager_1; }
	inline void set_allianceManager_1(AllianceManager_t344187419 * value)
	{
		___allianceManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___allianceManager_1), value);
	}

	inline static int32_t get_offset_of_loginManager_2() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___loginManager_2)); }
	inline LoginManager_t1249555276 * get_loginManager_2() const { return ___loginManager_2; }
	inline LoginManager_t1249555276 ** get_address_of_loginManager_2() { return &___loginManager_2; }
	inline void set_loginManager_2(LoginManager_t1249555276 * value)
	{
		___loginManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginManager_2), value);
	}

	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___user_3)); }
	inline UserModel_t1353931605 * get_user_3() const { return ___user_3; }
	inline UserModel_t1353931605 ** get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(UserModel_t1353931605 * value)
	{
		___user_3 = value;
		Il2CppCodeGenWriteBarrier((&___user_3), value);
	}

	inline static int32_t get_offset_of_onGetMyUserInfo_4() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onGetMyUserInfo_4)); }
	inline SimpleEvent_t129249603 * get_onGetMyUserInfo_4() const { return ___onGetMyUserInfo_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetMyUserInfo_4() { return &___onGetMyUserInfo_4; }
	inline void set_onGetMyUserInfo_4(SimpleEvent_t129249603 * value)
	{
		___onGetMyUserInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___onGetMyUserInfo_4), value);
	}

	inline static int32_t get_offset_of_onLanguageChanged_5() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onLanguageChanged_5)); }
	inline SimpleEvent_t129249603 * get_onLanguageChanged_5() const { return ___onLanguageChanged_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onLanguageChanged_5() { return &___onLanguageChanged_5; }
	inline void set_onLanguageChanged_5(SimpleEvent_t129249603 * value)
	{
		___onLanguageChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___onLanguageChanged_5), value);
	}

	inline static int32_t get_offset_of_onPasswordChanged_6() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onPasswordChanged_6)); }
	inline SimpleEvent_t129249603 * get_onPasswordChanged_6() const { return ___onPasswordChanged_6; }
	inline SimpleEvent_t129249603 ** get_address_of_onPasswordChanged_6() { return &___onPasswordChanged_6; }
	inline void set_onPasswordChanged_6(SimpleEvent_t129249603 * value)
	{
		___onPasswordChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPasswordChanged_6), value);
	}

	inline static int32_t get_offset_of_onResetPasswordRequested_7() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onResetPasswordRequested_7)); }
	inline SimpleEvent_t129249603 * get_onResetPasswordRequested_7() const { return ___onResetPasswordRequested_7; }
	inline SimpleEvent_t129249603 ** get_address_of_onResetPasswordRequested_7() { return &___onResetPasswordRequested_7; }
	inline void set_onResetPasswordRequested_7(SimpleEvent_t129249603 * value)
	{
		___onResetPasswordRequested_7 = value;
		Il2CppCodeGenWriteBarrier((&___onResetPasswordRequested_7), value);
	}

	inline static int32_t get_offset_of_onPasswordReset_8() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___onPasswordReset_8)); }
	inline SimpleEvent_t129249603 * get_onPasswordReset_8() const { return ___onPasswordReset_8; }
	inline SimpleEvent_t129249603 ** get_address_of_onPasswordReset_8() { return &___onPasswordReset_8; }
	inline void set_onPasswordReset_8(SimpleEvent_t129249603 * value)
	{
		___onPasswordReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPasswordReset_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T1349889689_H
#ifndef REPORTMANAGER_T1122460921_H
#define REPORTMANAGER_T1122460921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager
struct  ReportManager_t1122460921  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,InviteReportModel> ReportManager::invites
	Dictionary_2_t1700275496 * ___invites_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,InviteReportModel> ReportManager::allianceRequsets
	Dictionary_2_t1700275496 * ___allianceRequsets_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,BattleReportHeader> ReportManager::battles
	Dictionary_2_t4087798327 * ___battles_2;
	// System.Collections.Generic.Dictionary`2<System.Int64,BattleReportHeader> ReportManager::allianceBattles
	Dictionary_2_t4087798327 * ___allianceBattles_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,MessageHeader> ReportManager::inbox
	Dictionary_2_t243444911 * ___inbox_4;
	// System.Collections.Generic.Dictionary`2<System.Int64,SystemReportModel> ReportManager::systemReports
	Dictionary_2_t4009803018 * ___systemReports_5;
	// System.Collections.Generic.Dictionary`2<System.Int64,UnitTimer> ReportManager::unitTimers
	Dictionary_2_t4184190554 * ___unitTimers_6;
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingTimer> ReportManager::buildingTimers
	Dictionary_2_t1436714094 * ___buildingTimers_7;
	// System.Collections.Generic.Dictionary`2<System.Int64,ArmyTimer> ReportManager::armyTimers
	Dictionary_2_t50524311 * ___armyTimers_8;
	// System.Collections.Generic.Dictionary`2<System.Int64,ItemTimer> ReportManager::itemTimers
	Dictionary_2_t3380876359 * ___itemTimers_9;
	// System.Collections.Generic.Dictionary`2<System.Int64,WorldMapTimer> ReportManager::worldMapTimers
	Dictionary_2_t1123123525 * ___worldMapTimers_10;
	// SimpleEvent ReportManager::onGetUnreadReports
	SimpleEvent_t129249603 * ___onGetUnreadReports_11;
	// SimpleEvent ReportManager::onGetAllianceBattles
	SimpleEvent_t129249603 * ___onGetAllianceBattles_12;
	// SimpleEvent ReportManager::onUserMessageSent
	SimpleEvent_t129249603 * ___onUserMessageSent_13;
	// SimpleEvent ReportManager::onGetUnitTimers
	SimpleEvent_t129249603 * ___onGetUnitTimers_14;
	// SimpleEvent ReportManager::onGetBuildingTimers
	SimpleEvent_t129249603 * ___onGetBuildingTimers_15;
	// SimpleEvent ReportManager::onGetArmyTimers
	SimpleEvent_t129249603 * ___onGetArmyTimers_16;
	// SimpleEvent ReportManager::onGetItemTimers
	SimpleEvent_t129249603 * ___onGetItemTimers_17;
	// SimpleEvent ReportManager::onGetWorldMapTimers
	SimpleEvent_t129249603 * ___onGetWorldMapTimers_18;
	// SimpleEvent ReportManager::onMessageDeleted
	SimpleEvent_t129249603 * ___onMessageDeleted_19;
	// SimpleEvent ReportManager::onBattleReportDeleted
	SimpleEvent_t129249603 * ___onBattleReportDeleted_20;
	// System.Int64 ReportManager::unreadInboxCount
	int64_t ___unreadInboxCount_21;
	// System.Int64 ReportManager::unreadBattleCount
	int64_t ___unreadBattleCount_22;
	// System.Int64 ReportManager::unreadSystemCount
	int64_t ___unreadSystemCount_23;
	// System.String ReportManager::dateFormat
	String_t* ___dateFormat_24;

public:
	inline static int32_t get_offset_of_invites_0() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___invites_0)); }
	inline Dictionary_2_t1700275496 * get_invites_0() const { return ___invites_0; }
	inline Dictionary_2_t1700275496 ** get_address_of_invites_0() { return &___invites_0; }
	inline void set_invites_0(Dictionary_2_t1700275496 * value)
	{
		___invites_0 = value;
		Il2CppCodeGenWriteBarrier((&___invites_0), value);
	}

	inline static int32_t get_offset_of_allianceRequsets_1() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___allianceRequsets_1)); }
	inline Dictionary_2_t1700275496 * get_allianceRequsets_1() const { return ___allianceRequsets_1; }
	inline Dictionary_2_t1700275496 ** get_address_of_allianceRequsets_1() { return &___allianceRequsets_1; }
	inline void set_allianceRequsets_1(Dictionary_2_t1700275496 * value)
	{
		___allianceRequsets_1 = value;
		Il2CppCodeGenWriteBarrier((&___allianceRequsets_1), value);
	}

	inline static int32_t get_offset_of_battles_2() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___battles_2)); }
	inline Dictionary_2_t4087798327 * get_battles_2() const { return ___battles_2; }
	inline Dictionary_2_t4087798327 ** get_address_of_battles_2() { return &___battles_2; }
	inline void set_battles_2(Dictionary_2_t4087798327 * value)
	{
		___battles_2 = value;
		Il2CppCodeGenWriteBarrier((&___battles_2), value);
	}

	inline static int32_t get_offset_of_allianceBattles_3() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___allianceBattles_3)); }
	inline Dictionary_2_t4087798327 * get_allianceBattles_3() const { return ___allianceBattles_3; }
	inline Dictionary_2_t4087798327 ** get_address_of_allianceBattles_3() { return &___allianceBattles_3; }
	inline void set_allianceBattles_3(Dictionary_2_t4087798327 * value)
	{
		___allianceBattles_3 = value;
		Il2CppCodeGenWriteBarrier((&___allianceBattles_3), value);
	}

	inline static int32_t get_offset_of_inbox_4() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___inbox_4)); }
	inline Dictionary_2_t243444911 * get_inbox_4() const { return ___inbox_4; }
	inline Dictionary_2_t243444911 ** get_address_of_inbox_4() { return &___inbox_4; }
	inline void set_inbox_4(Dictionary_2_t243444911 * value)
	{
		___inbox_4 = value;
		Il2CppCodeGenWriteBarrier((&___inbox_4), value);
	}

	inline static int32_t get_offset_of_systemReports_5() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___systemReports_5)); }
	inline Dictionary_2_t4009803018 * get_systemReports_5() const { return ___systemReports_5; }
	inline Dictionary_2_t4009803018 ** get_address_of_systemReports_5() { return &___systemReports_5; }
	inline void set_systemReports_5(Dictionary_2_t4009803018 * value)
	{
		___systemReports_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemReports_5), value);
	}

	inline static int32_t get_offset_of_unitTimers_6() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___unitTimers_6)); }
	inline Dictionary_2_t4184190554 * get_unitTimers_6() const { return ___unitTimers_6; }
	inline Dictionary_2_t4184190554 ** get_address_of_unitTimers_6() { return &___unitTimers_6; }
	inline void set_unitTimers_6(Dictionary_2_t4184190554 * value)
	{
		___unitTimers_6 = value;
		Il2CppCodeGenWriteBarrier((&___unitTimers_6), value);
	}

	inline static int32_t get_offset_of_buildingTimers_7() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___buildingTimers_7)); }
	inline Dictionary_2_t1436714094 * get_buildingTimers_7() const { return ___buildingTimers_7; }
	inline Dictionary_2_t1436714094 ** get_address_of_buildingTimers_7() { return &___buildingTimers_7; }
	inline void set_buildingTimers_7(Dictionary_2_t1436714094 * value)
	{
		___buildingTimers_7 = value;
		Il2CppCodeGenWriteBarrier((&___buildingTimers_7), value);
	}

	inline static int32_t get_offset_of_armyTimers_8() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___armyTimers_8)); }
	inline Dictionary_2_t50524311 * get_armyTimers_8() const { return ___armyTimers_8; }
	inline Dictionary_2_t50524311 ** get_address_of_armyTimers_8() { return &___armyTimers_8; }
	inline void set_armyTimers_8(Dictionary_2_t50524311 * value)
	{
		___armyTimers_8 = value;
		Il2CppCodeGenWriteBarrier((&___armyTimers_8), value);
	}

	inline static int32_t get_offset_of_itemTimers_9() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___itemTimers_9)); }
	inline Dictionary_2_t3380876359 * get_itemTimers_9() const { return ___itemTimers_9; }
	inline Dictionary_2_t3380876359 ** get_address_of_itemTimers_9() { return &___itemTimers_9; }
	inline void set_itemTimers_9(Dictionary_2_t3380876359 * value)
	{
		___itemTimers_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemTimers_9), value);
	}

	inline static int32_t get_offset_of_worldMapTimers_10() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___worldMapTimers_10)); }
	inline Dictionary_2_t1123123525 * get_worldMapTimers_10() const { return ___worldMapTimers_10; }
	inline Dictionary_2_t1123123525 ** get_address_of_worldMapTimers_10() { return &___worldMapTimers_10; }
	inline void set_worldMapTimers_10(Dictionary_2_t1123123525 * value)
	{
		___worldMapTimers_10 = value;
		Il2CppCodeGenWriteBarrier((&___worldMapTimers_10), value);
	}

	inline static int32_t get_offset_of_onGetUnreadReports_11() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetUnreadReports_11)); }
	inline SimpleEvent_t129249603 * get_onGetUnreadReports_11() const { return ___onGetUnreadReports_11; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetUnreadReports_11() { return &___onGetUnreadReports_11; }
	inline void set_onGetUnreadReports_11(SimpleEvent_t129249603 * value)
	{
		___onGetUnreadReports_11 = value;
		Il2CppCodeGenWriteBarrier((&___onGetUnreadReports_11), value);
	}

	inline static int32_t get_offset_of_onGetAllianceBattles_12() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetAllianceBattles_12)); }
	inline SimpleEvent_t129249603 * get_onGetAllianceBattles_12() const { return ___onGetAllianceBattles_12; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetAllianceBattles_12() { return &___onGetAllianceBattles_12; }
	inline void set_onGetAllianceBattles_12(SimpleEvent_t129249603 * value)
	{
		___onGetAllianceBattles_12 = value;
		Il2CppCodeGenWriteBarrier((&___onGetAllianceBattles_12), value);
	}

	inline static int32_t get_offset_of_onUserMessageSent_13() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onUserMessageSent_13)); }
	inline SimpleEvent_t129249603 * get_onUserMessageSent_13() const { return ___onUserMessageSent_13; }
	inline SimpleEvent_t129249603 ** get_address_of_onUserMessageSent_13() { return &___onUserMessageSent_13; }
	inline void set_onUserMessageSent_13(SimpleEvent_t129249603 * value)
	{
		___onUserMessageSent_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUserMessageSent_13), value);
	}

	inline static int32_t get_offset_of_onGetUnitTimers_14() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetUnitTimers_14)); }
	inline SimpleEvent_t129249603 * get_onGetUnitTimers_14() const { return ___onGetUnitTimers_14; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetUnitTimers_14() { return &___onGetUnitTimers_14; }
	inline void set_onGetUnitTimers_14(SimpleEvent_t129249603 * value)
	{
		___onGetUnitTimers_14 = value;
		Il2CppCodeGenWriteBarrier((&___onGetUnitTimers_14), value);
	}

	inline static int32_t get_offset_of_onGetBuildingTimers_15() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetBuildingTimers_15)); }
	inline SimpleEvent_t129249603 * get_onGetBuildingTimers_15() const { return ___onGetBuildingTimers_15; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetBuildingTimers_15() { return &___onGetBuildingTimers_15; }
	inline void set_onGetBuildingTimers_15(SimpleEvent_t129249603 * value)
	{
		___onGetBuildingTimers_15 = value;
		Il2CppCodeGenWriteBarrier((&___onGetBuildingTimers_15), value);
	}

	inline static int32_t get_offset_of_onGetArmyTimers_16() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetArmyTimers_16)); }
	inline SimpleEvent_t129249603 * get_onGetArmyTimers_16() const { return ___onGetArmyTimers_16; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetArmyTimers_16() { return &___onGetArmyTimers_16; }
	inline void set_onGetArmyTimers_16(SimpleEvent_t129249603 * value)
	{
		___onGetArmyTimers_16 = value;
		Il2CppCodeGenWriteBarrier((&___onGetArmyTimers_16), value);
	}

	inline static int32_t get_offset_of_onGetItemTimers_17() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetItemTimers_17)); }
	inline SimpleEvent_t129249603 * get_onGetItemTimers_17() const { return ___onGetItemTimers_17; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetItemTimers_17() { return &___onGetItemTimers_17; }
	inline void set_onGetItemTimers_17(SimpleEvent_t129249603 * value)
	{
		___onGetItemTimers_17 = value;
		Il2CppCodeGenWriteBarrier((&___onGetItemTimers_17), value);
	}

	inline static int32_t get_offset_of_onGetWorldMapTimers_18() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onGetWorldMapTimers_18)); }
	inline SimpleEvent_t129249603 * get_onGetWorldMapTimers_18() const { return ___onGetWorldMapTimers_18; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetWorldMapTimers_18() { return &___onGetWorldMapTimers_18; }
	inline void set_onGetWorldMapTimers_18(SimpleEvent_t129249603 * value)
	{
		___onGetWorldMapTimers_18 = value;
		Il2CppCodeGenWriteBarrier((&___onGetWorldMapTimers_18), value);
	}

	inline static int32_t get_offset_of_onMessageDeleted_19() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onMessageDeleted_19)); }
	inline SimpleEvent_t129249603 * get_onMessageDeleted_19() const { return ___onMessageDeleted_19; }
	inline SimpleEvent_t129249603 ** get_address_of_onMessageDeleted_19() { return &___onMessageDeleted_19; }
	inline void set_onMessageDeleted_19(SimpleEvent_t129249603 * value)
	{
		___onMessageDeleted_19 = value;
		Il2CppCodeGenWriteBarrier((&___onMessageDeleted_19), value);
	}

	inline static int32_t get_offset_of_onBattleReportDeleted_20() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___onBattleReportDeleted_20)); }
	inline SimpleEvent_t129249603 * get_onBattleReportDeleted_20() const { return ___onBattleReportDeleted_20; }
	inline SimpleEvent_t129249603 ** get_address_of_onBattleReportDeleted_20() { return &___onBattleReportDeleted_20; }
	inline void set_onBattleReportDeleted_20(SimpleEvent_t129249603 * value)
	{
		___onBattleReportDeleted_20 = value;
		Il2CppCodeGenWriteBarrier((&___onBattleReportDeleted_20), value);
	}

	inline static int32_t get_offset_of_unreadInboxCount_21() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___unreadInboxCount_21)); }
	inline int64_t get_unreadInboxCount_21() const { return ___unreadInboxCount_21; }
	inline int64_t* get_address_of_unreadInboxCount_21() { return &___unreadInboxCount_21; }
	inline void set_unreadInboxCount_21(int64_t value)
	{
		___unreadInboxCount_21 = value;
	}

	inline static int32_t get_offset_of_unreadBattleCount_22() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___unreadBattleCount_22)); }
	inline int64_t get_unreadBattleCount_22() const { return ___unreadBattleCount_22; }
	inline int64_t* get_address_of_unreadBattleCount_22() { return &___unreadBattleCount_22; }
	inline void set_unreadBattleCount_22(int64_t value)
	{
		___unreadBattleCount_22 = value;
	}

	inline static int32_t get_offset_of_unreadSystemCount_23() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___unreadSystemCount_23)); }
	inline int64_t get_unreadSystemCount_23() const { return ___unreadSystemCount_23; }
	inline int64_t* get_address_of_unreadSystemCount_23() { return &___unreadSystemCount_23; }
	inline void set_unreadSystemCount_23(int64_t value)
	{
		___unreadSystemCount_23 = value;
	}

	inline static int32_t get_offset_of_dateFormat_24() { return static_cast<int32_t>(offsetof(ReportManager_t1122460921, ___dateFormat_24)); }
	inline String_t* get_dateFormat_24() const { return ___dateFormat_24; }
	inline String_t** get_address_of_dateFormat_24() { return &___dateFormat_24; }
	inline void set_dateFormat_24(String_t* value)
	{
		___dateFormat_24 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTMANAGER_T1122460921_H
#ifndef U3CGETEMPERORSU3EC__ITERATOR3_T4053754095_H
#define U3CGETEMPERORSU3EC__ITERATOR3_T4053754095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager/<GetEmperors>c__Iterator3
struct  U3CGetEmperorsU3Ec__Iterator3_t4053754095  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest LoginManager/<GetEmperors>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// LoginManager LoginManager/<GetEmperors>c__Iterator3::$this
	LoginManager_t1249555276 * ___U24this_1;
	// System.Object LoginManager/<GetEmperors>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoginManager/<GetEmperors>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoginManager/<GetEmperors>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEmperorsU3Ec__Iterator3_t4053754095, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetEmperorsU3Ec__Iterator3_t4053754095, ___U24this_1)); }
	inline LoginManager_t1249555276 * get_U24this_1() const { return ___U24this_1; }
	inline LoginManager_t1249555276 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoginManager_t1249555276 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetEmperorsU3Ec__Iterator3_t4053754095, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetEmperorsU3Ec__Iterator3_t4053754095, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetEmperorsU3Ec__Iterator3_t4053754095, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETEMPERORSU3EC__ITERATOR3_T4053754095_H
#ifndef U3CREADBATTLEREPORTU3EC__ITERATOR0_T558420421_H
#define U3CREADBATTLEREPORTU3EC__ITERATOR0_T558420421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<ReadBattleReport>c__Iterator0
struct  U3CReadBattleReportU3Ec__Iterator0_t558420421  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<ReadBattleReport>c__Iterator0::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// System.Int64 ReportManager/<ReadBattleReport>c__Iterator0::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<ReadBattleReport>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object ReportManager/<ReadBattleReport>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<ReadBattleReport>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<ReadBattleReport>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadBattleReportU3Ec__Iterator0_t558420421, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBATTLEREPORTU3EC__ITERATOR0_T558420421_H
#ifndef U3CCHANGEUSERINFOU3EC__ITERATOR1_T3940304722_H
#define U3CCHANGEUSERINFOU3EC__ITERATOR1_T3940304722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<ChangeUserInfo>c__Iterator1
struct  U3CChangeUserInfoU3Ec__Iterator1_t3940304722  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerManager/<ChangeUserInfo>c__Iterator1::<userForm>__0
	WWWForm_t4064702195 * ___U3CuserFormU3E__0_0;
	// System.String PlayerManager/<ChangeUserInfo>c__Iterator1::username
	String_t* ___username_1;
	// System.String PlayerManager/<ChangeUserInfo>c__Iterator1::email
	String_t* ___email_2;
	// System.String PlayerManager/<ChangeUserInfo>c__Iterator1::image
	String_t* ___image_3;
	// System.String PlayerManager/<ChangeUserInfo>c__Iterator1::flag
	String_t* ___flag_4;
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<ChangeUserInfo>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// PlayerManager PlayerManager/<ChangeUserInfo>c__Iterator1::$this
	PlayerManager_t1349889689 * ___U24this_6;
	// System.Object PlayerManager/<ChangeUserInfo>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean PlayerManager/<ChangeUserInfo>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 PlayerManager/<ChangeUserInfo>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CuserFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U3CuserFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuserFormU3E__0_0() const { return ___U3CuserFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuserFormU3E__0_0() { return &___U3CuserFormU3E__0_0; }
	inline void set_U3CuserFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuserFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_email_2() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___email_2)); }
	inline String_t* get_email_2() const { return ___email_2; }
	inline String_t** get_address_of_email_2() { return &___email_2; }
	inline void set_email_2(String_t* value)
	{
		___email_2 = value;
		Il2CppCodeGenWriteBarrier((&___email_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___image_3)); }
	inline String_t* get_image_3() const { return ___image_3; }
	inline String_t** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(String_t* value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}

	inline static int32_t get_offset_of_flag_4() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___flag_4)); }
	inline String_t* get_flag_4() const { return ___flag_4; }
	inline String_t** get_address_of_flag_4() { return &___flag_4; }
	inline void set_flag_4(String_t* value)
	{
		___flag_4 = value;
		Il2CppCodeGenWriteBarrier((&___flag_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U24this_6)); }
	inline PlayerManager_t1349889689 * get_U24this_6() const { return ___U24this_6; }
	inline PlayerManager_t1349889689 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(PlayerManager_t1349889689 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CChangeUserInfoU3Ec__Iterator1_t3940304722, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEUSERINFOU3EC__ITERATOR1_T3940304722_H
#ifndef U3CCHANGELANGUAGEU3EC__ITERATOR3_T1756740330_H
#define U3CCHANGELANGUAGEU3EC__ITERATOR3_T1756740330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<ChangeLanguage>c__Iterator3
struct  U3CChangeLanguageU3Ec__Iterator3_t1756740330  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerManager/<ChangeLanguage>c__Iterator3::<userForm>__0
	WWWForm_t4064702195 * ___U3CuserFormU3E__0_0;
	// System.String PlayerManager/<ChangeLanguage>c__Iterator3::lang
	String_t* ___lang_1;
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<ChangeLanguage>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// PlayerManager PlayerManager/<ChangeLanguage>c__Iterator3::$this
	PlayerManager_t1349889689 * ___U24this_3;
	// System.Object PlayerManager/<ChangeLanguage>c__Iterator3::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerManager/<ChangeLanguage>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerManager/<ChangeLanguage>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CuserFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U3CuserFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuserFormU3E__0_0() const { return ___U3CuserFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuserFormU3E__0_0() { return &___U3CuserFormU3E__0_0; }
	inline void set_U3CuserFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuserFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_lang_1() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___lang_1)); }
	inline String_t* get_lang_1() const { return ___lang_1; }
	inline String_t** get_address_of_lang_1() { return &___lang_1; }
	inline void set_lang_1(String_t* value)
	{
		___lang_1 = value;
		Il2CppCodeGenWriteBarrier((&___lang_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U24this_3)); }
	inline PlayerManager_t1349889689 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerManager_t1349889689 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerManager_t1349889689 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CChangeLanguageU3Ec__Iterator3_t1756740330, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGELANGUAGEU3EC__ITERATOR3_T1756740330_H
#ifndef U3CCHANGEUSERPASSWORDU3EC__ITERATOR4_T543598374_H
#define U3CCHANGEUSERPASSWORDU3EC__ITERATOR4_T543598374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<ChangeUserPassword>c__Iterator4
struct  U3CChangeUserPasswordU3Ec__Iterator4_t543598374  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerManager/<ChangeUserPassword>c__Iterator4::<passForm>__0
	WWWForm_t4064702195 * ___U3CpassFormU3E__0_0;
	// System.String PlayerManager/<ChangeUserPassword>c__Iterator4::oldPass
	String_t* ___oldPass_1;
	// System.String PlayerManager/<ChangeUserPassword>c__Iterator4::newPass
	String_t* ___newPass_2;
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<ChangeUserPassword>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// PlayerManager PlayerManager/<ChangeUserPassword>c__Iterator4::$this
	PlayerManager_t1349889689 * ___U24this_4;
	// System.Object PlayerManager/<ChangeUserPassword>c__Iterator4::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean PlayerManager/<ChangeUserPassword>c__Iterator4::$disposing
	bool ___U24disposing_6;
	// System.Int32 PlayerManager/<ChangeUserPassword>c__Iterator4::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CpassFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U3CpassFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CpassFormU3E__0_0() const { return ___U3CpassFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CpassFormU3E__0_0() { return &___U3CpassFormU3E__0_0; }
	inline void set_U3CpassFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CpassFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpassFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_oldPass_1() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___oldPass_1)); }
	inline String_t* get_oldPass_1() const { return ___oldPass_1; }
	inline String_t** get_address_of_oldPass_1() { return &___oldPass_1; }
	inline void set_oldPass_1(String_t* value)
	{
		___oldPass_1 = value;
		Il2CppCodeGenWriteBarrier((&___oldPass_1), value);
	}

	inline static int32_t get_offset_of_newPass_2() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___newPass_2)); }
	inline String_t* get_newPass_2() const { return ___newPass_2; }
	inline String_t** get_address_of_newPass_2() { return &___newPass_2; }
	inline void set_newPass_2(String_t* value)
	{
		___newPass_2 = value;
		Il2CppCodeGenWriteBarrier((&___newPass_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U24this_4)); }
	inline PlayerManager_t1349889689 * get_U24this_4() const { return ___U24this_4; }
	inline PlayerManager_t1349889689 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(PlayerManager_t1349889689 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CChangeUserPasswordU3Ec__Iterator4_t543598374, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEUSERPASSWORDU3EC__ITERATOR4_T543598374_H
#ifndef U3CNOMINATEUSERU3EC__ITERATOR2_T1177340224_H
#define U3CNOMINATEUSERU3EC__ITERATOR2_T1177340224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<NominateUser>c__Iterator2
struct  U3CNominateUserU3Ec__Iterator2_t1177340224  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerManager/<NominateUser>c__Iterator2::<userForm>__0
	WWWForm_t4064702195 * ___U3CuserFormU3E__0_0;
	// System.Int64 PlayerManager/<NominateUser>c__Iterator2::userId
	int64_t ___userId_1;
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<NominateUser>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object PlayerManager/<NominateUser>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PlayerManager/<NominateUser>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 PlayerManager/<NominateUser>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CuserFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___U3CuserFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuserFormU3E__0_0() const { return ___U3CuserFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuserFormU3E__0_0() { return &___U3CuserFormU3E__0_0; }
	inline void set_U3CuserFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuserFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___userId_1)); }
	inline int64_t get_userId_1() const { return ___userId_1; }
	inline int64_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int64_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CNominateUserU3Ec__Iterator2_t1177340224, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNOMINATEUSERU3EC__ITERATOR2_T1177340224_H
#ifndef U3CRESETPASSWORDU3EC__ITERATOR5_T2616237299_H
#define U3CRESETPASSWORDU3EC__ITERATOR5_T2616237299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<ResetPassword>c__Iterator5
struct  U3CResetPasswordU3Ec__Iterator5_t2616237299  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerManager/<ResetPassword>c__Iterator5::<passwordForm>__0
	WWWForm_t4064702195 * ___U3CpasswordFormU3E__0_0;
	// System.String PlayerManager/<ResetPassword>c__Iterator5::email
	String_t* ___email_1;
	// System.String PlayerManager/<ResetPassword>c__Iterator5::secretKey
	String_t* ___secretKey_2;
	// System.String PlayerManager/<ResetPassword>c__Iterator5::newPassword
	String_t* ___newPassword_3;
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<ResetPassword>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// PlayerManager PlayerManager/<ResetPassword>c__Iterator5::$this
	PlayerManager_t1349889689 * ___U24this_5;
	// System.Object PlayerManager/<ResetPassword>c__Iterator5::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean PlayerManager/<ResetPassword>c__Iterator5::$disposing
	bool ___U24disposing_7;
	// System.Int32 PlayerManager/<ResetPassword>c__Iterator5::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CpasswordFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U3CpasswordFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CpasswordFormU3E__0_0() const { return ___U3CpasswordFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CpasswordFormU3E__0_0() { return &___U3CpasswordFormU3E__0_0; }
	inline void set_U3CpasswordFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CpasswordFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpasswordFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_email_1() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___email_1)); }
	inline String_t* get_email_1() const { return ___email_1; }
	inline String_t** get_address_of_email_1() { return &___email_1; }
	inline void set_email_1(String_t* value)
	{
		___email_1 = value;
		Il2CppCodeGenWriteBarrier((&___email_1), value);
	}

	inline static int32_t get_offset_of_secretKey_2() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___secretKey_2)); }
	inline String_t* get_secretKey_2() const { return ___secretKey_2; }
	inline String_t** get_address_of_secretKey_2() { return &___secretKey_2; }
	inline void set_secretKey_2(String_t* value)
	{
		___secretKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___secretKey_2), value);
	}

	inline static int32_t get_offset_of_newPassword_3() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___newPassword_3)); }
	inline String_t* get_newPassword_3() const { return ___newPassword_3; }
	inline String_t** get_address_of_newPassword_3() { return &___newPassword_3; }
	inline void set_newPassword_3(String_t* value)
	{
		___newPassword_3 = value;
		Il2CppCodeGenWriteBarrier((&___newPassword_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U24this_5)); }
	inline PlayerManager_t1349889689 * get_U24this_5() const { return ___U24this_5; }
	inline PlayerManager_t1349889689 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PlayerManager_t1349889689 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CResetPasswordU3Ec__Iterator5_t2616237299, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETPASSWORDU3EC__ITERATOR5_T2616237299_H
#ifndef U3CLOGINREQUESTU3EC__ITERATOR2_T2287010221_H
#define U3CLOGINREQUESTU3EC__ITERATOR2_T2287010221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager/<LoginRequest>c__Iterator2
struct  U3CLoginRequestU3Ec__Iterator2_t2287010221  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm LoginManager/<LoginRequest>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String LoginManager/<LoginRequest>c__Iterator2::username
	String_t* ___username_1;
	// System.String LoginManager/<LoginRequest>c__Iterator2::password
	String_t* ___password_2;
	// UnityEngine.Networking.UnityWebRequest LoginManager/<LoginRequest>c__Iterator2::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_3;
	// LoginManager LoginManager/<LoginRequest>c__Iterator2::$this
	LoginManager_t1249555276 * ___U24this_4;
	// System.Object LoginManager/<LoginRequest>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean LoginManager/<LoginRequest>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 LoginManager/<LoginRequest>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier((&___password_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U3CwwwU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U24this_4)); }
	inline LoginManager_t1249555276 * get_U24this_4() const { return ___U24this_4; }
	inline LoginManager_t1249555276 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(LoginManager_t1249555276 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoginRequestU3Ec__Iterator2_t2287010221, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGINREQUESTU3EC__ITERATOR2_T2287010221_H
#ifndef U3CSENDUSERMESSAGEU3EC__ITERATOR3_T3221806769_H
#define U3CSENDUSERMESSAGEU3EC__ITERATOR3_T3221806769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<SendUserMessage>c__Iterator3
struct  U3CSendUserMessageU3Ec__Iterator3_t3221806769  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<SendUserMessage>c__Iterator3::<sendForm>__0
	WWWForm_t4064702195 * ___U3CsendFormU3E__0_0;
	// System.Int64 ReportManager/<SendUserMessage>c__Iterator3::userId
	int64_t ___userId_1;
	// System.String ReportManager/<SendUserMessage>c__Iterator3::subject
	String_t* ___subject_2;
	// System.String ReportManager/<SendUserMessage>c__Iterator3::message
	String_t* ___message_3;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<SendUserMessage>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// ReportManager ReportManager/<SendUserMessage>c__Iterator3::$this
	ReportManager_t1122460921 * ___U24this_5;
	// System.Object ReportManager/<SendUserMessage>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean ReportManager/<SendUserMessage>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 ReportManager/<SendUserMessage>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CsendFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U3CsendFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CsendFormU3E__0_0() const { return ___U3CsendFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CsendFormU3E__0_0() { return &___U3CsendFormU3E__0_0; }
	inline void set_U3CsendFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CsendFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsendFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___userId_1)); }
	inline int64_t get_userId_1() const { return ___userId_1; }
	inline int64_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int64_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___subject_2)); }
	inline String_t* get_subject_2() const { return ___subject_2; }
	inline String_t** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(String_t* value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier((&___subject_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U24this_5)); }
	inline ReportManager_t1122460921 * get_U24this_5() const { return ___U24this_5; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ReportManager_t1122460921 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CSendUserMessageU3Ec__Iterator3_t3221806769, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDUSERMESSAGEU3EC__ITERATOR3_T3221806769_H
#ifndef U3CLOADCOROUTINEU3EC__ITERATOR0_T3811190666_H
#define U3CLOADCOROUTINEU3EC__ITERATOR0_T3811190666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalGOScript/<LoadCoroutine>c__Iterator0
struct  U3CLoadCoroutineU3Ec__Iterator0_t3811190666  : public RuntimeObject
{
public:
	// System.Object GlobalGOScript/<LoadCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean GlobalGOScript/<LoadCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 GlobalGOScript/<LoadCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3811190666, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3811190666, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3811190666, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCOROUTINEU3EC__ITERATOR0_T3811190666_H
#ifndef U3CGETMYUSERINFOU3EC__ITERATOR0_T4123769329_H
#define U3CGETMYUSERINFOU3EC__ITERATOR0_T4123769329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager/<getMyUserInfo>c__Iterator0
struct  U3CgetMyUserInfoU3Ec__Iterator0_t4123769329  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest PlayerManager/<getMyUserInfo>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Boolean PlayerManager/<getMyUserInfo>c__Iterator0::initial
	bool ___initial_1;
	// PlayerManager PlayerManager/<getMyUserInfo>c__Iterator0::$this
	PlayerManager_t1349889689 * ___U24this_2;
	// System.Object PlayerManager/<getMyUserInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PlayerManager/<getMyUserInfo>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PlayerManager/<getMyUserInfo>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_initial_1() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___initial_1)); }
	inline bool get_initial_1() const { return ___initial_1; }
	inline bool* get_address_of_initial_1() { return &___initial_1; }
	inline void set_initial_1(bool value)
	{
		___initial_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___U24this_2)); }
	inline PlayerManager_t1349889689 * get_U24this_2() const { return ___U24this_2; }
	inline PlayerManager_t1349889689 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PlayerManager_t1349889689 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CgetMyUserInfoU3Ec__Iterator0_t4123769329, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETMYUSERINFOU3EC__ITERATOR0_T4123769329_H
#ifndef U3CGETUNREADREPORTSU3EC__ITERATOR4_T2377312974_H
#define U3CGETUNREADREPORTSU3EC__ITERATOR4_T2377312974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<GetUnreadReports>c__Iterator4
struct  U3CGetUnreadReportsU3Ec__Iterator4_t2377312974  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest ReportManager/<GetUnreadReports>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// ReportManager ReportManager/<GetUnreadReports>c__Iterator4::$this
	ReportManager_t1122460921 * ___U24this_1;
	// System.Object ReportManager/<GetUnreadReports>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ReportManager/<GetUnreadReports>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 ReportManager/<GetUnreadReports>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetUnreadReportsU3Ec__Iterator4_t2377312974, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetUnreadReportsU3Ec__Iterator4_t2377312974, ___U24this_1)); }
	inline ReportManager_t1122460921 * get_U24this_1() const { return ___U24this_1; }
	inline ReportManager_t1122460921 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ReportManager_t1122460921 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetUnreadReportsU3Ec__Iterator4_t2377312974, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetUnreadReportsU3Ec__Iterator4_t2377312974, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetUnreadReportsU3Ec__Iterator4_t2377312974, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETUNREADREPORTSU3EC__ITERATOR4_T2377312974_H
#ifndef U3CREADSYSTEMREPORTU3EC__ITERATOR2_T1664821444_H
#define U3CREADSYSTEMREPORTU3EC__ITERATOR2_T1664821444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<ReadSystemReport>c__Iterator2
struct  U3CReadSystemReportU3Ec__Iterator2_t1664821444  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<ReadSystemReport>c__Iterator2::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// System.Int64 ReportManager/<ReadSystemReport>c__Iterator2::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<ReadSystemReport>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object ReportManager/<ReadSystemReport>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<ReadSystemReport>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<ReadSystemReport>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadSystemReportU3Ec__Iterator2_t1664821444, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADSYSTEMREPORTU3EC__ITERATOR2_T1664821444_H
#ifndef U3CREGISTERREQUESTU3EC__ITERATOR0_T2344562202_H
#define U3CREGISTERREQUESTU3EC__ITERATOR0_T2344562202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager/<RegisterRequest>c__Iterator0
struct  U3CRegisterRequestU3Ec__Iterator0_t2344562202  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm LoginManager/<RegisterRequest>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String LoginManager/<RegisterRequest>c__Iterator0::username
	String_t* ___username_1;
	// System.String LoginManager/<RegisterRequest>c__Iterator0::password
	String_t* ___password_2;
	// System.String LoginManager/<RegisterRequest>c__Iterator0::email
	String_t* ___email_3;
	// System.String LoginManager/<RegisterRequest>c__Iterator0::emperor_name
	String_t* ___emperor_name_4;
	// System.String LoginManager/<RegisterRequest>c__Iterator0::language
	String_t* ___language_5;
	// UnityEngine.Networking.UnityWebRequest LoginManager/<RegisterRequest>c__Iterator0::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_6;
	// LoginManager LoginManager/<RegisterRequest>c__Iterator0::$this
	LoginManager_t1249555276 * ___U24this_7;
	// System.Object LoginManager/<RegisterRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean LoginManager/<RegisterRequest>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 LoginManager/<RegisterRequest>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier((&___username_1), value);
	}

	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier((&___password_2), value);
	}

	inline static int32_t get_offset_of_email_3() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___email_3)); }
	inline String_t* get_email_3() const { return ___email_3; }
	inline String_t** get_address_of_email_3() { return &___email_3; }
	inline void set_email_3(String_t* value)
	{
		___email_3 = value;
		Il2CppCodeGenWriteBarrier((&___email_3), value);
	}

	inline static int32_t get_offset_of_emperor_name_4() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___emperor_name_4)); }
	inline String_t* get_emperor_name_4() const { return ___emperor_name_4; }
	inline String_t** get_address_of_emperor_name_4() { return &___emperor_name_4; }
	inline void set_emperor_name_4(String_t* value)
	{
		___emperor_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___emperor_name_4), value);
	}

	inline static int32_t get_offset_of_language_5() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___language_5)); }
	inline String_t* get_language_5() const { return ___language_5; }
	inline String_t** get_address_of_language_5() { return &___language_5; }
	inline void set_language_5(String_t* value)
	{
		___language_5 = value;
		Il2CppCodeGenWriteBarrier((&___language_5), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_6() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U3CwwwU3E__0_6)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_6() const { return ___U3CwwwU3E__0_6; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_6() { return &___U3CwwwU3E__0_6; }
	inline void set_U3CwwwU3E__0_6(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U24this_7)); }
	inline LoginManager_t1249555276 * get_U24this_7() const { return ___U24this_7; }
	inline LoginManager_t1249555276 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(LoginManager_t1249555276 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CRegisterRequestU3Ec__Iterator0_t2344562202, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREGISTERREQUESTU3EC__ITERATOR0_T2344562202_H
#ifndef U3CFORGOTPASSWORDU3EC__ITERATOR1_T3222112050_H
#define U3CFORGOTPASSWORDU3EC__ITERATOR1_T3222112050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager/<ForgotPassword>c__Iterator1
struct  U3CForgotPasswordU3Ec__Iterator1_t3222112050  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm LoginManager/<ForgotPassword>c__Iterator1::<emailForm>__0
	WWWForm_t4064702195 * ___U3CemailFormU3E__0_0;
	// System.String LoginManager/<ForgotPassword>c__Iterator1::email
	String_t* ___email_1;
	// UnityEngine.Networking.UnityWebRequest LoginManager/<ForgotPassword>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object LoginManager/<ForgotPassword>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LoginManager/<ForgotPassword>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 LoginManager/<ForgotPassword>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CemailFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___U3CemailFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CemailFormU3E__0_0() const { return ___U3CemailFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CemailFormU3E__0_0() { return &___U3CemailFormU3E__0_0; }
	inline void set_U3CemailFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CemailFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CemailFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_email_1() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___email_1)); }
	inline String_t* get_email_1() const { return ___email_1; }
	inline String_t** get_address_of_email_1() { return &___email_1; }
	inline void set_email_1(String_t* value)
	{
		___email_1 = value;
		Il2CppCodeGenWriteBarrier((&___email_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CForgotPasswordU3Ec__Iterator1_t3222112050, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFORGOTPASSWORDU3EC__ITERATOR1_T3222112050_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CREADREPORTMESSAGEU3EC__ITERATOR1_T4000497501_H
#define U3CREADREPORTMESSAGEU3EC__ITERATOR1_T4000497501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReportManager/<ReadReportMessage>c__Iterator1
struct  U3CReadReportMessageU3Ec__Iterator1_t4000497501  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm ReportManager/<ReadReportMessage>c__Iterator1::<readForm>__0
	WWWForm_t4064702195 * ___U3CreadFormU3E__0_0;
	// System.Int64 ReportManager/<ReadReportMessage>c__Iterator1::id
	int64_t ___id_1;
	// UnityEngine.Networking.UnityWebRequest ReportManager/<ReadReportMessage>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Object ReportManager/<ReadReportMessage>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ReportManager/<ReadReportMessage>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 ReportManager/<ReadReportMessage>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CreadFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___U3CreadFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CreadFormU3E__0_0() const { return ___U3CreadFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CreadFormU3E__0_0() { return &___U3CreadFormU3E__0_0; }
	inline void set_U3CreadFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CreadFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreadFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReadReportMessageU3Ec__Iterator1_t4000497501, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADREPORTMESSAGEU3EC__ITERATOR1_T4000497501_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUMERATOR_T3562014579_H
#define ENUMERATOR_T3562014579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<TutorialPageModel>
struct  Enumerator_t3562014579 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1672770702 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	TutorialPageModel_t200695960 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3562014579, ___l_0)); }
	inline List_1_t1672770702 * get_l_0() const { return ___l_0; }
	inline List_1_t1672770702 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1672770702 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3562014579, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3562014579, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3562014579, ___current_3)); }
	inline TutorialPageModel_t200695960 * get_current_3() const { return ___current_3; }
	inline TutorialPageModel_t200695960 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TutorialPageModel_t200695960 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3562014579_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CLOADMAINSCENEU3EC__ITERATOR4_T338545386_H
#define U3CLOADMAINSCENEU3EC__ITERATOR4_T338545386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager/<loadMainScene>c__Iterator4
struct  U3CloadMainSceneU3Ec__Iterator4_t338545386  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<TutorialPageModel> LoginManager/<loadMainScene>c__Iterator4::$locvar0
	Enumerator_t3562014579  ___U24locvar0_0;
	// LoginManager LoginManager/<loadMainScene>c__Iterator4::$this
	LoginManager_t1249555276 * ___U24this_1;
	// System.Object LoginManager/<loadMainScene>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoginManager/<loadMainScene>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoginManager/<loadMainScene>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CloadMainSceneU3Ec__Iterator4_t338545386, ___U24locvar0_0)); }
	inline Enumerator_t3562014579  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t3562014579 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t3562014579  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CloadMainSceneU3Ec__Iterator4_t338545386, ___U24this_1)); }
	inline LoginManager_t1249555276 * get_U24this_1() const { return ___U24this_1; }
	inline LoginManager_t1249555276 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoginManager_t1249555276 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CloadMainSceneU3Ec__Iterator4_t338545386, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CloadMainSceneU3Ec__Iterator4_t338545386, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CloadMainSceneU3Ec__Iterator4_t338545386, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMAINSCENEU3EC__ITERATOR4_T338545386_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CONFIRMATIONEVENT_T890979749_H
#define CONFIRMATIONEVENT_T890979749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfirmationEvent
struct  ConfirmationEvent_t890979749  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIRMATIONEVENT_T890979749_H
#ifndef SIMPLEEVENT_T129249603_H
#define SIMPLEEVENT_T129249603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleEvent
struct  SimpleEvent_t129249603  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEEVENT_T129249603_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ALLIANCEEVENTMODEL_T1723640982_H
#define ALLIANCEEVENTMODEL_T1723640982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventModel
struct  AllianceEventModel_t1723640982  : public RuntimeObject
{
public:
	// System.Int64 AllianceEventModel::id
	int64_t ___id_0;
	// System.Int64 AllianceEventModel::alliance
	int64_t ___alliance_1;
	// System.String AllianceEventModel::message
	String_t* ___message_2;
	// System.DateTime AllianceEventModel::date
	DateTime_t3738529785  ___date_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1723640982, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_alliance_1() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1723640982, ___alliance_1)); }
	inline int64_t get_alliance_1() const { return ___alliance_1; }
	inline int64_t* get_address_of_alliance_1() { return &___alliance_1; }
	inline void set_alliance_1(int64_t value)
	{
		___alliance_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1723640982, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(AllianceEventModel_t1723640982, ___date_3)); }
	inline DateTime_t3738529785  get_date_3() const { return ___date_3; }
	inline DateTime_t3738529785 * get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DateTime_t3738529785  value)
	{
		___date_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLIANCEEVENTMODEL_T1723640982_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MAPCITYINFOMANAGER_T1539510113_H
#define MAPCITYINFOMANAGER_T1539510113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCityInfoManager
struct  MapCityInfoManager_t1539510113  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MapCityInfoManager::rank
	Text_t1901882714 * ___rank_2;
	// UnityEngine.UI.Text MapCityInfoManager::experience
	Text_t1901882714 * ___experience_3;
	// UnityEngine.UI.Text MapCityInfoManager::region
	Text_t1901882714 * ___region_4;
	// UnityEngine.UI.Text MapCityInfoManager::coords
	Text_t1901882714 * ___coords_5;
	// UnityEngine.UI.Text MapCityInfoManager::alliance
	Text_t1901882714 * ___alliance_6;
	// UnityEngine.UI.Text MapCityInfoManager::status
	Text_t1901882714 * ___status_7;
	// UnityEngine.UI.Text MapCityInfoManager::cityName
	Text_t1901882714 * ___cityName_8;
	// UnityEngine.UI.Text MapCityInfoManager::playerName
	Text_t1901882714 * ___playerName_9;
	// UnityEngine.UI.Text MapCityInfoManager::occupationLabel
	Text_t1901882714 * ___occupationLabel_10;
	// UnityEngine.GameObject MapCityInfoManager::marchBtn
	GameObject_t1113636619 * ___marchBtn_11;
	// UnityEngine.GameObject MapCityInfoManager::revoltBtn
	GameObject_t1113636619 * ___revoltBtn_12;
	// UnityEngine.GameObject MapCityInfoManager::nominateBtn
	GameObject_t1113636619 * ___nominateBtn_13;
	// UnityEngine.GameObject MapCityInfoManager::shopBtn
	GameObject_t1113636619 * ___shopBtn_14;
	// UnityEngine.GameObject MapCityInfoManager::giftBtn
	GameObject_t1113636619 * ___giftBtn_15;
	// UnityEngine.UI.Image MapCityInfoManager::playerImage
	Image_t2670269651 * ___playerImage_16;
	// UnityEngine.UI.Image MapCityInfoManager::cityImage
	Image_t2670269651 * ___cityImage_17;
	// WorldFieldModel MapCityInfoManager::field
	WorldFieldModel_t2417974361 * ___field_18;

public:
	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___rank_2)); }
	inline Text_t1901882714 * get_rank_2() const { return ___rank_2; }
	inline Text_t1901882714 ** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(Text_t1901882714 * value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier((&___rank_2), value);
	}

	inline static int32_t get_offset_of_experience_3() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___experience_3)); }
	inline Text_t1901882714 * get_experience_3() const { return ___experience_3; }
	inline Text_t1901882714 ** get_address_of_experience_3() { return &___experience_3; }
	inline void set_experience_3(Text_t1901882714 * value)
	{
		___experience_3 = value;
		Il2CppCodeGenWriteBarrier((&___experience_3), value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___region_4)); }
	inline Text_t1901882714 * get_region_4() const { return ___region_4; }
	inline Text_t1901882714 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t1901882714 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier((&___region_4), value);
	}

	inline static int32_t get_offset_of_coords_5() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___coords_5)); }
	inline Text_t1901882714 * get_coords_5() const { return ___coords_5; }
	inline Text_t1901882714 ** get_address_of_coords_5() { return &___coords_5; }
	inline void set_coords_5(Text_t1901882714 * value)
	{
		___coords_5 = value;
		Il2CppCodeGenWriteBarrier((&___coords_5), value);
	}

	inline static int32_t get_offset_of_alliance_6() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___alliance_6)); }
	inline Text_t1901882714 * get_alliance_6() const { return ___alliance_6; }
	inline Text_t1901882714 ** get_address_of_alliance_6() { return &___alliance_6; }
	inline void set_alliance_6(Text_t1901882714 * value)
	{
		___alliance_6 = value;
		Il2CppCodeGenWriteBarrier((&___alliance_6), value);
	}

	inline static int32_t get_offset_of_status_7() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___status_7)); }
	inline Text_t1901882714 * get_status_7() const { return ___status_7; }
	inline Text_t1901882714 ** get_address_of_status_7() { return &___status_7; }
	inline void set_status_7(Text_t1901882714 * value)
	{
		___status_7 = value;
		Il2CppCodeGenWriteBarrier((&___status_7), value);
	}

	inline static int32_t get_offset_of_cityName_8() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___cityName_8)); }
	inline Text_t1901882714 * get_cityName_8() const { return ___cityName_8; }
	inline Text_t1901882714 ** get_address_of_cityName_8() { return &___cityName_8; }
	inline void set_cityName_8(Text_t1901882714 * value)
	{
		___cityName_8 = value;
		Il2CppCodeGenWriteBarrier((&___cityName_8), value);
	}

	inline static int32_t get_offset_of_playerName_9() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___playerName_9)); }
	inline Text_t1901882714 * get_playerName_9() const { return ___playerName_9; }
	inline Text_t1901882714 ** get_address_of_playerName_9() { return &___playerName_9; }
	inline void set_playerName_9(Text_t1901882714 * value)
	{
		___playerName_9 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_9), value);
	}

	inline static int32_t get_offset_of_occupationLabel_10() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___occupationLabel_10)); }
	inline Text_t1901882714 * get_occupationLabel_10() const { return ___occupationLabel_10; }
	inline Text_t1901882714 ** get_address_of_occupationLabel_10() { return &___occupationLabel_10; }
	inline void set_occupationLabel_10(Text_t1901882714 * value)
	{
		___occupationLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___occupationLabel_10), value);
	}

	inline static int32_t get_offset_of_marchBtn_11() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___marchBtn_11)); }
	inline GameObject_t1113636619 * get_marchBtn_11() const { return ___marchBtn_11; }
	inline GameObject_t1113636619 ** get_address_of_marchBtn_11() { return &___marchBtn_11; }
	inline void set_marchBtn_11(GameObject_t1113636619 * value)
	{
		___marchBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&___marchBtn_11), value);
	}

	inline static int32_t get_offset_of_revoltBtn_12() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___revoltBtn_12)); }
	inline GameObject_t1113636619 * get_revoltBtn_12() const { return ___revoltBtn_12; }
	inline GameObject_t1113636619 ** get_address_of_revoltBtn_12() { return &___revoltBtn_12; }
	inline void set_revoltBtn_12(GameObject_t1113636619 * value)
	{
		___revoltBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&___revoltBtn_12), value);
	}

	inline static int32_t get_offset_of_nominateBtn_13() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___nominateBtn_13)); }
	inline GameObject_t1113636619 * get_nominateBtn_13() const { return ___nominateBtn_13; }
	inline GameObject_t1113636619 ** get_address_of_nominateBtn_13() { return &___nominateBtn_13; }
	inline void set_nominateBtn_13(GameObject_t1113636619 * value)
	{
		___nominateBtn_13 = value;
		Il2CppCodeGenWriteBarrier((&___nominateBtn_13), value);
	}

	inline static int32_t get_offset_of_shopBtn_14() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___shopBtn_14)); }
	inline GameObject_t1113636619 * get_shopBtn_14() const { return ___shopBtn_14; }
	inline GameObject_t1113636619 ** get_address_of_shopBtn_14() { return &___shopBtn_14; }
	inline void set_shopBtn_14(GameObject_t1113636619 * value)
	{
		___shopBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&___shopBtn_14), value);
	}

	inline static int32_t get_offset_of_giftBtn_15() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___giftBtn_15)); }
	inline GameObject_t1113636619 * get_giftBtn_15() const { return ___giftBtn_15; }
	inline GameObject_t1113636619 ** get_address_of_giftBtn_15() { return &___giftBtn_15; }
	inline void set_giftBtn_15(GameObject_t1113636619 * value)
	{
		___giftBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&___giftBtn_15), value);
	}

	inline static int32_t get_offset_of_playerImage_16() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___playerImage_16)); }
	inline Image_t2670269651 * get_playerImage_16() const { return ___playerImage_16; }
	inline Image_t2670269651 ** get_address_of_playerImage_16() { return &___playerImage_16; }
	inline void set_playerImage_16(Image_t2670269651 * value)
	{
		___playerImage_16 = value;
		Il2CppCodeGenWriteBarrier((&___playerImage_16), value);
	}

	inline static int32_t get_offset_of_cityImage_17() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___cityImage_17)); }
	inline Image_t2670269651 * get_cityImage_17() const { return ___cityImage_17; }
	inline Image_t2670269651 ** get_address_of_cityImage_17() { return &___cityImage_17; }
	inline void set_cityImage_17(Image_t2670269651 * value)
	{
		___cityImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___cityImage_17), value);
	}

	inline static int32_t get_offset_of_field_18() { return static_cast<int32_t>(offsetof(MapCityInfoManager_t1539510113, ___field_18)); }
	inline WorldFieldModel_t2417974361 * get_field_18() const { return ___field_18; }
	inline WorldFieldModel_t2417974361 ** get_address_of_field_18() { return &___field_18; }
	inline void set_field_18(WorldFieldModel_t2417974361 * value)
	{
		___field_18 = value;
		Il2CppCodeGenWriteBarrier((&___field_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPCITYINFOMANAGER_T1539510113_H
#ifndef LOCALIZEDBUILDINGNAME_T3861673193_H
#define LOCALIZEDBUILDINGNAME_T3861673193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizedBuildingName
struct  LocalizedBuildingName_t3861673193  : public MonoBehaviour_t3962482529
{
public:
	// System.String LocalizedBuildingName::key
	String_t* ___key_2;

public:
	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(LocalizedBuildingName_t3861673193, ___key_2)); }
	inline String_t* get_key_2() const { return ___key_2; }
	inline String_t** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(String_t* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDBUILDINGNAME_T3861673193_H
#ifndef RESOURCEMANAGER_T484397614_H
#define RESOURCEMANAGER_T484397614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_t484397614  : public MonoBehaviour_t3962482529
{
public:
	// ResourcesModel ResourceManager::cityResources
	ResourcesModel_t2533508513 * ___cityResources_2;
	// SimpleEvent ResourceManager::onGetInitialResources
	SimpleEvent_t129249603 * ___onGetInitialResources_3;
	// UnityEngine.Coroutine ResourceManager::resourceCoroutine
	Coroutine_t3829159415 * ___resourceCoroutine_4;
	// System.Boolean ResourceManager::resourcesWorking
	bool ___resourcesWorking_5;
	// System.Boolean ResourceManager::buildingEvents
	bool ___buildingEvents_6;
	// System.Boolean ResourceManager::unitEvents
	bool ___unitEvents_7;
	// System.Boolean ResourceManager::armyEvents
	bool ___armyEvents_8;
	// System.Boolean ResourceManager::itemEvents
	bool ___itemEvents_9;
	// System.Boolean ResourceManager::worldMapEvents
	bool ___worldMapEvents_10;
	// System.Boolean ResourceManager::newCityEvents
	bool ___newCityEvents_11;
	// System.Boolean ResourceManager::buildingTimers
	bool ___buildingTimers_12;
	// System.Boolean ResourceManager::unitTimers
	bool ___unitTimers_13;
	// System.Boolean ResourceManager::armyTimers
	bool ___armyTimers_14;
	// System.Boolean ResourceManager::itemTimers
	bool ___itemTimers_15;
	// System.Boolean ResourceManager::worldMapTimers
	bool ___worldMapTimers_16;
	// System.Boolean ResourceManager::newCityTimers
	bool ___newCityTimers_17;
	// System.String ResourceManager::dateFormat
	String_t* ___dateFormat_18;

public:
	inline static int32_t get_offset_of_cityResources_2() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___cityResources_2)); }
	inline ResourcesModel_t2533508513 * get_cityResources_2() const { return ___cityResources_2; }
	inline ResourcesModel_t2533508513 ** get_address_of_cityResources_2() { return &___cityResources_2; }
	inline void set_cityResources_2(ResourcesModel_t2533508513 * value)
	{
		___cityResources_2 = value;
		Il2CppCodeGenWriteBarrier((&___cityResources_2), value);
	}

	inline static int32_t get_offset_of_onGetInitialResources_3() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___onGetInitialResources_3)); }
	inline SimpleEvent_t129249603 * get_onGetInitialResources_3() const { return ___onGetInitialResources_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetInitialResources_3() { return &___onGetInitialResources_3; }
	inline void set_onGetInitialResources_3(SimpleEvent_t129249603 * value)
	{
		___onGetInitialResources_3 = value;
		Il2CppCodeGenWriteBarrier((&___onGetInitialResources_3), value);
	}

	inline static int32_t get_offset_of_resourceCoroutine_4() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___resourceCoroutine_4)); }
	inline Coroutine_t3829159415 * get_resourceCoroutine_4() const { return ___resourceCoroutine_4; }
	inline Coroutine_t3829159415 ** get_address_of_resourceCoroutine_4() { return &___resourceCoroutine_4; }
	inline void set_resourceCoroutine_4(Coroutine_t3829159415 * value)
	{
		___resourceCoroutine_4 = value;
		Il2CppCodeGenWriteBarrier((&___resourceCoroutine_4), value);
	}

	inline static int32_t get_offset_of_resourcesWorking_5() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___resourcesWorking_5)); }
	inline bool get_resourcesWorking_5() const { return ___resourcesWorking_5; }
	inline bool* get_address_of_resourcesWorking_5() { return &___resourcesWorking_5; }
	inline void set_resourcesWorking_5(bool value)
	{
		___resourcesWorking_5 = value;
	}

	inline static int32_t get_offset_of_buildingEvents_6() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___buildingEvents_6)); }
	inline bool get_buildingEvents_6() const { return ___buildingEvents_6; }
	inline bool* get_address_of_buildingEvents_6() { return &___buildingEvents_6; }
	inline void set_buildingEvents_6(bool value)
	{
		___buildingEvents_6 = value;
	}

	inline static int32_t get_offset_of_unitEvents_7() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___unitEvents_7)); }
	inline bool get_unitEvents_7() const { return ___unitEvents_7; }
	inline bool* get_address_of_unitEvents_7() { return &___unitEvents_7; }
	inline void set_unitEvents_7(bool value)
	{
		___unitEvents_7 = value;
	}

	inline static int32_t get_offset_of_armyEvents_8() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___armyEvents_8)); }
	inline bool get_armyEvents_8() const { return ___armyEvents_8; }
	inline bool* get_address_of_armyEvents_8() { return &___armyEvents_8; }
	inline void set_armyEvents_8(bool value)
	{
		___armyEvents_8 = value;
	}

	inline static int32_t get_offset_of_itemEvents_9() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___itemEvents_9)); }
	inline bool get_itemEvents_9() const { return ___itemEvents_9; }
	inline bool* get_address_of_itemEvents_9() { return &___itemEvents_9; }
	inline void set_itemEvents_9(bool value)
	{
		___itemEvents_9 = value;
	}

	inline static int32_t get_offset_of_worldMapEvents_10() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___worldMapEvents_10)); }
	inline bool get_worldMapEvents_10() const { return ___worldMapEvents_10; }
	inline bool* get_address_of_worldMapEvents_10() { return &___worldMapEvents_10; }
	inline void set_worldMapEvents_10(bool value)
	{
		___worldMapEvents_10 = value;
	}

	inline static int32_t get_offset_of_newCityEvents_11() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___newCityEvents_11)); }
	inline bool get_newCityEvents_11() const { return ___newCityEvents_11; }
	inline bool* get_address_of_newCityEvents_11() { return &___newCityEvents_11; }
	inline void set_newCityEvents_11(bool value)
	{
		___newCityEvents_11 = value;
	}

	inline static int32_t get_offset_of_buildingTimers_12() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___buildingTimers_12)); }
	inline bool get_buildingTimers_12() const { return ___buildingTimers_12; }
	inline bool* get_address_of_buildingTimers_12() { return &___buildingTimers_12; }
	inline void set_buildingTimers_12(bool value)
	{
		___buildingTimers_12 = value;
	}

	inline static int32_t get_offset_of_unitTimers_13() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___unitTimers_13)); }
	inline bool get_unitTimers_13() const { return ___unitTimers_13; }
	inline bool* get_address_of_unitTimers_13() { return &___unitTimers_13; }
	inline void set_unitTimers_13(bool value)
	{
		___unitTimers_13 = value;
	}

	inline static int32_t get_offset_of_armyTimers_14() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___armyTimers_14)); }
	inline bool get_armyTimers_14() const { return ___armyTimers_14; }
	inline bool* get_address_of_armyTimers_14() { return &___armyTimers_14; }
	inline void set_armyTimers_14(bool value)
	{
		___armyTimers_14 = value;
	}

	inline static int32_t get_offset_of_itemTimers_15() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___itemTimers_15)); }
	inline bool get_itemTimers_15() const { return ___itemTimers_15; }
	inline bool* get_address_of_itemTimers_15() { return &___itemTimers_15; }
	inline void set_itemTimers_15(bool value)
	{
		___itemTimers_15 = value;
	}

	inline static int32_t get_offset_of_worldMapTimers_16() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___worldMapTimers_16)); }
	inline bool get_worldMapTimers_16() const { return ___worldMapTimers_16; }
	inline bool* get_address_of_worldMapTimers_16() { return &___worldMapTimers_16; }
	inline void set_worldMapTimers_16(bool value)
	{
		___worldMapTimers_16 = value;
	}

	inline static int32_t get_offset_of_newCityTimers_17() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___newCityTimers_17)); }
	inline bool get_newCityTimers_17() const { return ___newCityTimers_17; }
	inline bool* get_address_of_newCityTimers_17() { return &___newCityTimers_17; }
	inline void set_newCityTimers_17(bool value)
	{
		___newCityTimers_17 = value;
	}

	inline static int32_t get_offset_of_dateFormat_18() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___dateFormat_18)); }
	inline String_t* get_dateFormat_18() const { return ___dateFormat_18; }
	inline String_t** get_address_of_dateFormat_18() { return &___dateFormat_18; }
	inline void set_dateFormat_18(String_t* value)
	{
		___dateFormat_18 = value;
		Il2CppCodeGenWriteBarrier((&___dateFormat_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEMANAGER_T484397614_H
#ifndef GCMMANAGER_T2058212271_H
#define GCMMANAGER_T2058212271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GCMManager
struct  GCMManager_t2058212271  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCMMANAGER_T2058212271_H
#ifndef LEVELLOADMANAGER_T362334468_H
#define LEVELLOADMANAGER_T362334468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLoadManager
struct  LevelLoadManager_t362334468  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LevelLoadManager::mainUI
	GameObject_t1113636619 * ___mainUI_2;
	// UnityEngine.GameObject LevelLoadManager::touchManager
	GameObject_t1113636619 * ___touchManager_3;
	// UnityEngine.GameObject LevelLoadManager::zoomMenu
	GameObject_t1113636619 * ___zoomMenu_4;
	// UnityEngine.GameObject LevelLoadManager::coordsMenu
	GameObject_t1113636619 * ___coordsMenu_5;
	// ADManager LevelLoadManager::adManager
	ADManager_t261308543 * ___adManager_6;

public:
	inline static int32_t get_offset_of_mainUI_2() { return static_cast<int32_t>(offsetof(LevelLoadManager_t362334468, ___mainUI_2)); }
	inline GameObject_t1113636619 * get_mainUI_2() const { return ___mainUI_2; }
	inline GameObject_t1113636619 ** get_address_of_mainUI_2() { return &___mainUI_2; }
	inline void set_mainUI_2(GameObject_t1113636619 * value)
	{
		___mainUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_2), value);
	}

	inline static int32_t get_offset_of_touchManager_3() { return static_cast<int32_t>(offsetof(LevelLoadManager_t362334468, ___touchManager_3)); }
	inline GameObject_t1113636619 * get_touchManager_3() const { return ___touchManager_3; }
	inline GameObject_t1113636619 ** get_address_of_touchManager_3() { return &___touchManager_3; }
	inline void set_touchManager_3(GameObject_t1113636619 * value)
	{
		___touchManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___touchManager_3), value);
	}

	inline static int32_t get_offset_of_zoomMenu_4() { return static_cast<int32_t>(offsetof(LevelLoadManager_t362334468, ___zoomMenu_4)); }
	inline GameObject_t1113636619 * get_zoomMenu_4() const { return ___zoomMenu_4; }
	inline GameObject_t1113636619 ** get_address_of_zoomMenu_4() { return &___zoomMenu_4; }
	inline void set_zoomMenu_4(GameObject_t1113636619 * value)
	{
		___zoomMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___zoomMenu_4), value);
	}

	inline static int32_t get_offset_of_coordsMenu_5() { return static_cast<int32_t>(offsetof(LevelLoadManager_t362334468, ___coordsMenu_5)); }
	inline GameObject_t1113636619 * get_coordsMenu_5() const { return ___coordsMenu_5; }
	inline GameObject_t1113636619 ** get_address_of_coordsMenu_5() { return &___coordsMenu_5; }
	inline void set_coordsMenu_5(GameObject_t1113636619 * value)
	{
		___coordsMenu_5 = value;
		Il2CppCodeGenWriteBarrier((&___coordsMenu_5), value);
	}

	inline static int32_t get_offset_of_adManager_6() { return static_cast<int32_t>(offsetof(LevelLoadManager_t362334468, ___adManager_6)); }
	inline ADManager_t261308543 * get_adManager_6() const { return ___adManager_6; }
	inline ADManager_t261308543 ** get_address_of_adManager_6() { return &___adManager_6; }
	inline void set_adManager_6(ADManager_t261308543 * value)
	{
		___adManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___adManager_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOADMANAGER_T362334468_H
#ifndef LOGINMANAGER_T1249555276_H
#define LOGINMANAGER_T1249555276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager
struct  LoginManager_t1249555276  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> LoginManager::emperorsNameToId
	Dictionary_2_t3521823603 * ___emperorsNameToId_2;
	// SimpleEvent LoginManager::onGetEmperors
	SimpleEvent_t129249603 * ___onGetEmperors_3;
	// SimpleEvent LoginManager::onRegister
	SimpleEvent_t129249603 * ___onRegister_4;
	// SimpleEvent LoginManager::onLogin
	SimpleEvent_t129249603 * ___onLogin_5;
	// System.Boolean LoginManager::gotBuildingConstants
	bool ___gotBuildingConstants_6;
	// System.Boolean LoginManager::gotUnitConstants
	bool ___gotUnitConstants_7;
	// System.Boolean LoginManager::gotTutorialPages
	bool ___gotTutorialPages_8;
	// System.Boolean LoginManager::gotPlayerInfo
	bool ___gotPlayerInfo_9;
	// System.Boolean LoginManager::gotPlayerCities
	bool ___gotPlayerCities_10;
	// System.Boolean LoginManager::gotCityResources
	bool ___gotCityResources_11;
	// System.Boolean LoginManager::gotCityBuildings
	bool ___gotCityBuildings_12;
	// System.Boolean LoginManager::gotCityFields
	bool ___gotCityFields_13;
	// System.Boolean LoginManager::gotCityValleys
	bool ___gotCityValleys_14;
	// System.Boolean LoginManager::gotCityUnits
	bool ___gotCityUnits_15;
	// System.Boolean LoginManager::gotCityGenerals
	bool ___gotCityGenerals_16;
	// System.Boolean LoginManager::gotCityTreasures
	bool ___gotCityTreasures_17;

public:
	inline static int32_t get_offset_of_emperorsNameToId_2() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___emperorsNameToId_2)); }
	inline Dictionary_2_t3521823603 * get_emperorsNameToId_2() const { return ___emperorsNameToId_2; }
	inline Dictionary_2_t3521823603 ** get_address_of_emperorsNameToId_2() { return &___emperorsNameToId_2; }
	inline void set_emperorsNameToId_2(Dictionary_2_t3521823603 * value)
	{
		___emperorsNameToId_2 = value;
		Il2CppCodeGenWriteBarrier((&___emperorsNameToId_2), value);
	}

	inline static int32_t get_offset_of_onGetEmperors_3() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___onGetEmperors_3)); }
	inline SimpleEvent_t129249603 * get_onGetEmperors_3() const { return ___onGetEmperors_3; }
	inline SimpleEvent_t129249603 ** get_address_of_onGetEmperors_3() { return &___onGetEmperors_3; }
	inline void set_onGetEmperors_3(SimpleEvent_t129249603 * value)
	{
		___onGetEmperors_3 = value;
		Il2CppCodeGenWriteBarrier((&___onGetEmperors_3), value);
	}

	inline static int32_t get_offset_of_onRegister_4() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___onRegister_4)); }
	inline SimpleEvent_t129249603 * get_onRegister_4() const { return ___onRegister_4; }
	inline SimpleEvent_t129249603 ** get_address_of_onRegister_4() { return &___onRegister_4; }
	inline void set_onRegister_4(SimpleEvent_t129249603 * value)
	{
		___onRegister_4 = value;
		Il2CppCodeGenWriteBarrier((&___onRegister_4), value);
	}

	inline static int32_t get_offset_of_onLogin_5() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___onLogin_5)); }
	inline SimpleEvent_t129249603 * get_onLogin_5() const { return ___onLogin_5; }
	inline SimpleEvent_t129249603 ** get_address_of_onLogin_5() { return &___onLogin_5; }
	inline void set_onLogin_5(SimpleEvent_t129249603 * value)
	{
		___onLogin_5 = value;
		Il2CppCodeGenWriteBarrier((&___onLogin_5), value);
	}

	inline static int32_t get_offset_of_gotBuildingConstants_6() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotBuildingConstants_6)); }
	inline bool get_gotBuildingConstants_6() const { return ___gotBuildingConstants_6; }
	inline bool* get_address_of_gotBuildingConstants_6() { return &___gotBuildingConstants_6; }
	inline void set_gotBuildingConstants_6(bool value)
	{
		___gotBuildingConstants_6 = value;
	}

	inline static int32_t get_offset_of_gotUnitConstants_7() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotUnitConstants_7)); }
	inline bool get_gotUnitConstants_7() const { return ___gotUnitConstants_7; }
	inline bool* get_address_of_gotUnitConstants_7() { return &___gotUnitConstants_7; }
	inline void set_gotUnitConstants_7(bool value)
	{
		___gotUnitConstants_7 = value;
	}

	inline static int32_t get_offset_of_gotTutorialPages_8() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotTutorialPages_8)); }
	inline bool get_gotTutorialPages_8() const { return ___gotTutorialPages_8; }
	inline bool* get_address_of_gotTutorialPages_8() { return &___gotTutorialPages_8; }
	inline void set_gotTutorialPages_8(bool value)
	{
		___gotTutorialPages_8 = value;
	}

	inline static int32_t get_offset_of_gotPlayerInfo_9() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotPlayerInfo_9)); }
	inline bool get_gotPlayerInfo_9() const { return ___gotPlayerInfo_9; }
	inline bool* get_address_of_gotPlayerInfo_9() { return &___gotPlayerInfo_9; }
	inline void set_gotPlayerInfo_9(bool value)
	{
		___gotPlayerInfo_9 = value;
	}

	inline static int32_t get_offset_of_gotPlayerCities_10() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotPlayerCities_10)); }
	inline bool get_gotPlayerCities_10() const { return ___gotPlayerCities_10; }
	inline bool* get_address_of_gotPlayerCities_10() { return &___gotPlayerCities_10; }
	inline void set_gotPlayerCities_10(bool value)
	{
		___gotPlayerCities_10 = value;
	}

	inline static int32_t get_offset_of_gotCityResources_11() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityResources_11)); }
	inline bool get_gotCityResources_11() const { return ___gotCityResources_11; }
	inline bool* get_address_of_gotCityResources_11() { return &___gotCityResources_11; }
	inline void set_gotCityResources_11(bool value)
	{
		___gotCityResources_11 = value;
	}

	inline static int32_t get_offset_of_gotCityBuildings_12() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityBuildings_12)); }
	inline bool get_gotCityBuildings_12() const { return ___gotCityBuildings_12; }
	inline bool* get_address_of_gotCityBuildings_12() { return &___gotCityBuildings_12; }
	inline void set_gotCityBuildings_12(bool value)
	{
		___gotCityBuildings_12 = value;
	}

	inline static int32_t get_offset_of_gotCityFields_13() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityFields_13)); }
	inline bool get_gotCityFields_13() const { return ___gotCityFields_13; }
	inline bool* get_address_of_gotCityFields_13() { return &___gotCityFields_13; }
	inline void set_gotCityFields_13(bool value)
	{
		___gotCityFields_13 = value;
	}

	inline static int32_t get_offset_of_gotCityValleys_14() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityValleys_14)); }
	inline bool get_gotCityValleys_14() const { return ___gotCityValleys_14; }
	inline bool* get_address_of_gotCityValleys_14() { return &___gotCityValleys_14; }
	inline void set_gotCityValleys_14(bool value)
	{
		___gotCityValleys_14 = value;
	}

	inline static int32_t get_offset_of_gotCityUnits_15() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityUnits_15)); }
	inline bool get_gotCityUnits_15() const { return ___gotCityUnits_15; }
	inline bool* get_address_of_gotCityUnits_15() { return &___gotCityUnits_15; }
	inline void set_gotCityUnits_15(bool value)
	{
		___gotCityUnits_15 = value;
	}

	inline static int32_t get_offset_of_gotCityGenerals_16() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityGenerals_16)); }
	inline bool get_gotCityGenerals_16() const { return ___gotCityGenerals_16; }
	inline bool* get_address_of_gotCityGenerals_16() { return &___gotCityGenerals_16; }
	inline void set_gotCityGenerals_16(bool value)
	{
		___gotCityGenerals_16 = value;
	}

	inline static int32_t get_offset_of_gotCityTreasures_17() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___gotCityTreasures_17)); }
	inline bool get_gotCityTreasures_17() const { return ___gotCityTreasures_17; }
	inline bool* get_address_of_gotCityTreasures_17() { return &___gotCityTreasures_17; }
	inline void set_gotCityTreasures_17(bool value)
	{
		___gotCityTreasures_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINMANAGER_T1249555276_H
#ifndef SETTINGSMANAGER_T2083239687_H
#define SETTINGSMANAGER_T2083239687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager
struct  SettingsManager_t2083239687  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T2083239687_H
#ifndef GLOBALGOSCRIPT_T724808191_H
#define GLOBALGOSCRIPT_T724808191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalGOScript
struct  GlobalGOScript_t724808191  : public MonoBehaviour_t3962482529
{
public:
	// PlayerManager GlobalGOScript::playerManager
	PlayerManager_t1349889689 * ___playerManager_3;
	// GameManager GlobalGOScript::gameManager
	GameManager_t1536523654 * ___gameManager_4;
	// GCMManager GlobalGOScript::messagesManager
	GCMManager_t2058212271 * ___messagesManager_5;
	// System.String GlobalGOScript::host
	String_t* ___host_6;
	// System.String GlobalGOScript::token
	String_t* ___token_7;
	// WindowInstanceManager GlobalGOScript::windowInstanceManager
	WindowInstanceManager_t3234774687 * ___windowInstanceManager_8;
	// CityChangerContentManager GlobalGOScript::cityChanger
	CityChangerContentManager_t1075607021 * ___cityChanger_9;
	// PanelKnightsTableController GlobalGOScript::knightsTable
	PanelKnightsTableController_t3023536378 * ___knightsTable_10;
	// UnityEngine.GameObject GlobalGOScript::newMailNotification
	GameObject_t1113636619 * ___newMailNotification_11;
	// UnityEngine.GameObject GlobalGOScript::underAttackNotification
	GameObject_t1113636619 * ___underAttackNotification_12;
	// UnityEngine.GameObject GlobalGOScript::adBonusNotification
	GameObject_t1113636619 * ___adBonusNotification_13;

public:
	inline static int32_t get_offset_of_playerManager_3() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___playerManager_3)); }
	inline PlayerManager_t1349889689 * get_playerManager_3() const { return ___playerManager_3; }
	inline PlayerManager_t1349889689 ** get_address_of_playerManager_3() { return &___playerManager_3; }
	inline void set_playerManager_3(PlayerManager_t1349889689 * value)
	{
		___playerManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerManager_3), value);
	}

	inline static int32_t get_offset_of_gameManager_4() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___gameManager_4)); }
	inline GameManager_t1536523654 * get_gameManager_4() const { return ___gameManager_4; }
	inline GameManager_t1536523654 ** get_address_of_gameManager_4() { return &___gameManager_4; }
	inline void set_gameManager_4(GameManager_t1536523654 * value)
	{
		___gameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_4), value);
	}

	inline static int32_t get_offset_of_messagesManager_5() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___messagesManager_5)); }
	inline GCMManager_t2058212271 * get_messagesManager_5() const { return ___messagesManager_5; }
	inline GCMManager_t2058212271 ** get_address_of_messagesManager_5() { return &___messagesManager_5; }
	inline void set_messagesManager_5(GCMManager_t2058212271 * value)
	{
		___messagesManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___messagesManager_5), value);
	}

	inline static int32_t get_offset_of_host_6() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___host_6)); }
	inline String_t* get_host_6() const { return ___host_6; }
	inline String_t** get_address_of_host_6() { return &___host_6; }
	inline void set_host_6(String_t* value)
	{
		___host_6 = value;
		Il2CppCodeGenWriteBarrier((&___host_6), value);
	}

	inline static int32_t get_offset_of_token_7() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___token_7)); }
	inline String_t* get_token_7() const { return ___token_7; }
	inline String_t** get_address_of_token_7() { return &___token_7; }
	inline void set_token_7(String_t* value)
	{
		___token_7 = value;
		Il2CppCodeGenWriteBarrier((&___token_7), value);
	}

	inline static int32_t get_offset_of_windowInstanceManager_8() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___windowInstanceManager_8)); }
	inline WindowInstanceManager_t3234774687 * get_windowInstanceManager_8() const { return ___windowInstanceManager_8; }
	inline WindowInstanceManager_t3234774687 ** get_address_of_windowInstanceManager_8() { return &___windowInstanceManager_8; }
	inline void set_windowInstanceManager_8(WindowInstanceManager_t3234774687 * value)
	{
		___windowInstanceManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___windowInstanceManager_8), value);
	}

	inline static int32_t get_offset_of_cityChanger_9() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___cityChanger_9)); }
	inline CityChangerContentManager_t1075607021 * get_cityChanger_9() const { return ___cityChanger_9; }
	inline CityChangerContentManager_t1075607021 ** get_address_of_cityChanger_9() { return &___cityChanger_9; }
	inline void set_cityChanger_9(CityChangerContentManager_t1075607021 * value)
	{
		___cityChanger_9 = value;
		Il2CppCodeGenWriteBarrier((&___cityChanger_9), value);
	}

	inline static int32_t get_offset_of_knightsTable_10() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___knightsTable_10)); }
	inline PanelKnightsTableController_t3023536378 * get_knightsTable_10() const { return ___knightsTable_10; }
	inline PanelKnightsTableController_t3023536378 ** get_address_of_knightsTable_10() { return &___knightsTable_10; }
	inline void set_knightsTable_10(PanelKnightsTableController_t3023536378 * value)
	{
		___knightsTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___knightsTable_10), value);
	}

	inline static int32_t get_offset_of_newMailNotification_11() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___newMailNotification_11)); }
	inline GameObject_t1113636619 * get_newMailNotification_11() const { return ___newMailNotification_11; }
	inline GameObject_t1113636619 ** get_address_of_newMailNotification_11() { return &___newMailNotification_11; }
	inline void set_newMailNotification_11(GameObject_t1113636619 * value)
	{
		___newMailNotification_11 = value;
		Il2CppCodeGenWriteBarrier((&___newMailNotification_11), value);
	}

	inline static int32_t get_offset_of_underAttackNotification_12() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___underAttackNotification_12)); }
	inline GameObject_t1113636619 * get_underAttackNotification_12() const { return ___underAttackNotification_12; }
	inline GameObject_t1113636619 ** get_address_of_underAttackNotification_12() { return &___underAttackNotification_12; }
	inline void set_underAttackNotification_12(GameObject_t1113636619 * value)
	{
		___underAttackNotification_12 = value;
		Il2CppCodeGenWriteBarrier((&___underAttackNotification_12), value);
	}

	inline static int32_t get_offset_of_adBonusNotification_13() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191, ___adBonusNotification_13)); }
	inline GameObject_t1113636619 * get_adBonusNotification_13() const { return ___adBonusNotification_13; }
	inline GameObject_t1113636619 ** get_address_of_adBonusNotification_13() { return &___adBonusNotification_13; }
	inline void set_adBonusNotification_13(GameObject_t1113636619 * value)
	{
		___adBonusNotification_13 = value;
		Il2CppCodeGenWriteBarrier((&___adBonusNotification_13), value);
	}
};

struct GlobalGOScript_t724808191_StaticFields
{
public:
	// GlobalGOScript GlobalGOScript::instance
	GlobalGOScript_t724808191 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GlobalGOScript_t724808191_StaticFields, ___instance_2)); }
	inline GlobalGOScript_t724808191 * get_instance_2() const { return ___instance_2; }
	inline GlobalGOScript_t724808191 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GlobalGOScript_t724808191 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALGOSCRIPT_T724808191_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (U3CUpgradeValleyU3Ec__Iterator5_t2222192536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[7] = 
{
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U3CupgradeFormU3E__0_0(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_valley_id_1(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U3CrequestU3E__0_2(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U24this_3(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U24current_4(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U24disposing_5(),
	U3CUpgradeValleyU3Ec__Iterator5_t2222192536::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (U3CDowngradeValleyU3Ec__Iterator6_t309172924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3401[7] = 
{
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U3CdowngradeFormU3E__0_0(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_valley_id_1(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U3CrequestU3E__0_2(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U24this_3(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U24current_4(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U24disposing_5(),
	U3CDowngradeValleyU3Ec__Iterator6_t309172924::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (U3CAddBuildingU3Ec__Iterator7_t3864423245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[10] = 
{
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U3CbuildFormU3E__0_0(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_name_1(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_city_id_2(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_pit_id_3(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_location_4(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U3CrequestU3E__0_5(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U24this_6(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U24current_7(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U24disposing_8(),
	U3CAddBuildingU3Ec__Iterator7_t3864423245::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (U3CDestroyBuildingU3Ec__Iterator8_t3296167326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3403[7] = 
{
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U3CdestroyFormU3E__0_0(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_buildingID_1(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U3CrequestU3E__0_2(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U24this_3(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U24current_4(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U24disposing_5(),
	U3CDestroyBuildingU3Ec__Iterator8_t3296167326::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (U3CCancelBuildingU3Ec__Iterator9_t1915633598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[7] = 
{
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U3CcancelFormU3E__0_0(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_event_id_1(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U24this_3(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U24current_4(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U24disposing_5(),
	U3CCancelBuildingU3Ec__Iterator9_t1915633598::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (U3CCancelValleyU3Ec__IteratorA_t3260069651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3405[7] = 
{
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U3CcancelFormU3E__0_0(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_event_id_1(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U24this_3(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U24current_4(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U24disposing_5(),
	U3CCancelValleyU3Ec__IteratorA_t3260069651::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (CityManager_t2587329200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[12] = 
{
	CityManager_t2587329200::get_offset_of_resourceManager_0(),
	CityManager_t2587329200::get_offset_of_unitsManager_1(),
	CityManager_t2587329200::get_offset_of_buildingsManager_2(),
	CityManager_t2587329200::get_offset_of_treasureManager_3(),
	CityManager_t2587329200::get_offset_of_onCityBuilt_4(),
	CityManager_t2587329200::get_offset_of_onGotNewCity_5(),
	CityManager_t2587329200::get_offset_of_onCityNameAndIconChanged_6(),
	CityManager_t2587329200::get_offset_of_cityChangerUpdateImage_7(),
	CityManager_t2587329200::get_offset_of_gotAllowTroopsState_8(),
	CityManager_t2587329200::get_offset_of_onTaxUpdated_9(),
	CityManager_t2587329200::get_offset_of_currentCity_10(),
	CityManager_t2587329200::get_offset_of_myCities_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (U3CGetInitialCitiesU3Ec__Iterator0_t423452736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[5] = 
{
	U3CGetInitialCitiesU3Ec__Iterator0_t423452736::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCitiesU3Ec__Iterator0_t423452736::get_offset_of_U24this_1(),
	U3CGetInitialCitiesU3Ec__Iterator0_t423452736::get_offset_of_U24current_2(),
	U3CGetInitialCitiesU3Ec__Iterator0_t423452736::get_offset_of_U24disposing_3(),
	U3CGetInitialCitiesU3Ec__Iterator0_t423452736::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (U3CBuildCityU3Ec__Iterator1_t2749806825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[9] = 
{
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U3CnewCityFormU3E__0_0(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_x_1(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_y_2(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_cityName_3(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U3CrequestU3E__0_4(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U24this_5(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U24current_6(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U24disposing_7(),
	U3CBuildCityU3Ec__Iterator1_t2749806825::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (U3CChangeNameAndImageU3Ec__Iterator2_t708341679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[8] = 
{
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U3CchangeFormU3E__0_0(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_cityName_1(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_imageName_2(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U3CrequestU3E__0_3(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U24this_4(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U24current_5(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U24disposing_6(),
	U3CChangeNameAndImageU3Ec__Iterator2_t708341679::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[5] = 
{
	U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593::get_offset_of_U24this_1(),
	U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593::get_offset_of_U24current_2(),
	U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593::get_offset_of_U24disposing_3(),
	U3CUpdateCurrentCityCoordsU3Ec__Iterator3_t3359773593::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[5] = 
{
	U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588::get_offset_of_U24this_1(),
	U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588::get_offset_of_U24current_2(),
	U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588::get_offset_of_U24disposing_3(),
	U3CUpdateCityExpantionU3Ec__Iterator4_t3076708588::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[5] = 
{
	U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786::get_offset_of_U24this_1(),
	U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786::get_offset_of_U24current_2(),
	U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786::get_offset_of_U24disposing_3(),
	U3CUpdateCityAllowTroopsU3Ec__Iterator5_t166018786::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (U3CSetAllowTroopsU3Ec__Iterator6_t3482264946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[7] = 
{
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U3CsetFormU3E__0_0(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_flag_1(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U3CrequestU3E__0_2(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U24this_3(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U24current_4(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U24disposing_5(),
	U3CSetAllowTroopsU3Ec__Iterator6_t3482264946::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (U3CUpdateCityTaxU3Ec__Iterator7_t2241765318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[7] = 
{
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U3CsetFormU3E__0_0(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_taxRate_1(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U3CrequestU3E__0_2(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U24this_3(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U24current_4(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U24disposing_5(),
	U3CUpdateCityTaxU3Ec__Iterator7_t2241765318::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (U3CStartRevolutionU3Ec__Iterator8_t3221568021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[6] = 
{
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_U3CrevoltFormU3E__0_0(),
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_cityID_1(),
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_U3CrequestU3E__0_2(),
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_U24current_3(),
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_U24disposing_4(),
	U3CStartRevolutionU3Ec__Iterator8_t3221568021::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (GameManager_t1536523654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[9] = 
{
	GameManager_t1536523654::get_offset_of_cityManager_0(),
	GameManager_t1536523654::get_offset_of_battleManager_1(),
	GameManager_t1536523654::get_offset_of_levelLoadManager_2(),
	GameManager_t1536523654::get_offset_of_buildingConstants_3(),
	GameManager_t1536523654::get_offset_of_unitConstants_4(),
	GameManager_t1536523654::get_offset_of_tutorials_5(),
	GameManager_t1536523654::get_offset_of_residenceFields_6(),
	GameManager_t1536523654::get_offset_of_onGetBuildingNames_7(),
	GameManager_t1536523654::get_offset_of_buildingIds_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3417[5] = 
{
	U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755::get_offset_of_U24this_1(),
	U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755::get_offset_of_U24current_2(),
	U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755::get_offset_of_U24disposing_3(),
	U3CGetInitialBuildingConstantsU3Ec__Iterator0_t263283755::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3418[6] = 
{
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_id_0(),
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_U3CrequestU3E__0_1(),
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_U24this_2(),
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_U24current_3(),
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_U24disposing_4(),
	U3CGetBuildingInitialConstantForNameU3Ec__Iterator1_t1766459497::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[5] = 
{
	U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832::get_offset_of_U24this_1(),
	U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832::get_offset_of_U24current_2(),
	U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832::get_offset_of_U24disposing_3(),
	U3CGetInitialUnitConstantsU3Ec__Iterator2_t3804170832::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (U3CGetTutorialPagesU3Ec__Iterator3_t1358694543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3420[5] = 
{
	U3CGetTutorialPagesU3Ec__Iterator3_t1358694543::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetTutorialPagesU3Ec__Iterator3_t1358694543::get_offset_of_U24this_1(),
	U3CGetTutorialPagesU3Ec__Iterator3_t1358694543::get_offset_of_U24current_2(),
	U3CGetTutorialPagesU3Ec__Iterator3_t1358694543::get_offset_of_U24disposing_3(),
	U3CGetTutorialPagesU3Ec__Iterator3_t1358694543::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3421[6] = 
{
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_url_0(),
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_U3CrequestU3E__0_1(),
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_tutorialID_2(),
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_U24current_3(),
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_U24disposing_4(),
	U3CDownloadTutorialImageU3Ec__Iterator4_t4140255223::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (GCMManager_t2058212271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[7] = 
{
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U3CaddressStringU3E__0_0(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U3CuserFormU3E__0_1(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_tokenString_2(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U3CrequestU3E__0_3(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U24current_4(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U24disposing_5(),
	U3CRegisterDeviceTokenU3Ec__Iterator0_t3352232145::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (SimpleEvent_t129249603), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (ConfirmationEvent_t890979749), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (GlobalGOScript_t724808191), -1, sizeof(GlobalGOScript_t724808191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3426[12] = 
{
	GlobalGOScript_t724808191_StaticFields::get_offset_of_instance_2(),
	GlobalGOScript_t724808191::get_offset_of_playerManager_3(),
	GlobalGOScript_t724808191::get_offset_of_gameManager_4(),
	GlobalGOScript_t724808191::get_offset_of_messagesManager_5(),
	GlobalGOScript_t724808191::get_offset_of_host_6(),
	GlobalGOScript_t724808191::get_offset_of_token_7(),
	GlobalGOScript_t724808191::get_offset_of_windowInstanceManager_8(),
	GlobalGOScript_t724808191::get_offset_of_cityChanger_9(),
	GlobalGOScript_t724808191::get_offset_of_knightsTable_10(),
	GlobalGOScript_t724808191::get_offset_of_newMailNotification_11(),
	GlobalGOScript_t724808191::get_offset_of_underAttackNotification_12(),
	GlobalGOScript_t724808191::get_offset_of_adBonusNotification_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (U3CLoadCoroutineU3Ec__Iterator0_t3811190666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[3] = 
{
	U3CLoadCoroutineU3Ec__Iterator0_t3811190666::get_offset_of_U24current_0(),
	U3CLoadCoroutineU3Ec__Iterator0_t3811190666::get_offset_of_U24disposing_1(),
	U3CLoadCoroutineU3Ec__Iterator0_t3811190666::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (LevelLoadManager_t362334468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3428[5] = 
{
	LevelLoadManager_t362334468::get_offset_of_mainUI_2(),
	LevelLoadManager_t362334468::get_offset_of_touchManager_3(),
	LevelLoadManager_t362334468::get_offset_of_zoomMenu_4(),
	LevelLoadManager_t362334468::get_offset_of_coordsMenu_5(),
	LevelLoadManager_t362334468::get_offset_of_adManager_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (LoginManager_t1249555276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[16] = 
{
	LoginManager_t1249555276::get_offset_of_emperorsNameToId_2(),
	LoginManager_t1249555276::get_offset_of_onGetEmperors_3(),
	LoginManager_t1249555276::get_offset_of_onRegister_4(),
	LoginManager_t1249555276::get_offset_of_onLogin_5(),
	LoginManager_t1249555276::get_offset_of_gotBuildingConstants_6(),
	LoginManager_t1249555276::get_offset_of_gotUnitConstants_7(),
	LoginManager_t1249555276::get_offset_of_gotTutorialPages_8(),
	LoginManager_t1249555276::get_offset_of_gotPlayerInfo_9(),
	LoginManager_t1249555276::get_offset_of_gotPlayerCities_10(),
	LoginManager_t1249555276::get_offset_of_gotCityResources_11(),
	LoginManager_t1249555276::get_offset_of_gotCityBuildings_12(),
	LoginManager_t1249555276::get_offset_of_gotCityFields_13(),
	LoginManager_t1249555276::get_offset_of_gotCityValleys_14(),
	LoginManager_t1249555276::get_offset_of_gotCityUnits_15(),
	LoginManager_t1249555276::get_offset_of_gotCityGenerals_16(),
	LoginManager_t1249555276::get_offset_of_gotCityTreasures_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (U3CRegisterRequestU3Ec__Iterator0_t2344562202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[11] = 
{
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U3CformU3E__0_0(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_username_1(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_password_2(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_email_3(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_emperor_name_4(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_language_5(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U3CwwwU3E__0_6(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U24this_7(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U24current_8(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U24disposing_9(),
	U3CRegisterRequestU3Ec__Iterator0_t2344562202::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (U3CForgotPasswordU3Ec__Iterator1_t3222112050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[6] = 
{
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_U3CemailFormU3E__0_0(),
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_email_1(),
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_U3CrequestU3E__0_2(),
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_U24current_3(),
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_U24disposing_4(),
	U3CForgotPasswordU3Ec__Iterator1_t3222112050::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (U3CLoginRequestU3Ec__Iterator2_t2287010221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[8] = 
{
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U3CformU3E__0_0(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_username_1(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_password_2(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U3CwwwU3E__0_3(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U24this_4(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U24current_5(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U24disposing_6(),
	U3CLoginRequestU3Ec__Iterator2_t2287010221::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (U3CGetEmperorsU3Ec__Iterator3_t4053754095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[5] = 
{
	U3CGetEmperorsU3Ec__Iterator3_t4053754095::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetEmperorsU3Ec__Iterator3_t4053754095::get_offset_of_U24this_1(),
	U3CGetEmperorsU3Ec__Iterator3_t4053754095::get_offset_of_U24current_2(),
	U3CGetEmperorsU3Ec__Iterator3_t4053754095::get_offset_of_U24disposing_3(),
	U3CGetEmperorsU3Ec__Iterator3_t4053754095::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (U3CloadMainSceneU3Ec__Iterator4_t338545386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[5] = 
{
	U3CloadMainSceneU3Ec__Iterator4_t338545386::get_offset_of_U24locvar0_0(),
	U3CloadMainSceneU3Ec__Iterator4_t338545386::get_offset_of_U24this_1(),
	U3CloadMainSceneU3Ec__Iterator4_t338545386::get_offset_of_U24current_2(),
	U3CloadMainSceneU3Ec__Iterator4_t338545386::get_offset_of_U24disposing_3(),
	U3CloadMainSceneU3Ec__Iterator4_t338545386::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (PlayerManager_t1349889689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[9] = 
{
	PlayerManager_t1349889689::get_offset_of_reportManager_0(),
	PlayerManager_t1349889689::get_offset_of_allianceManager_1(),
	PlayerManager_t1349889689::get_offset_of_loginManager_2(),
	PlayerManager_t1349889689::get_offset_of_user_3(),
	PlayerManager_t1349889689::get_offset_of_onGetMyUserInfo_4(),
	PlayerManager_t1349889689::get_offset_of_onLanguageChanged_5(),
	PlayerManager_t1349889689::get_offset_of_onPasswordChanged_6(),
	PlayerManager_t1349889689::get_offset_of_onResetPasswordRequested_7(),
	PlayerManager_t1349889689::get_offset_of_onPasswordReset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (U3CgetMyUserInfoU3Ec__Iterator0_t4123769329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3436[6] = 
{
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_U3CrequestU3E__0_0(),
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_initial_1(),
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_U24this_2(),
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_U24current_3(),
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_U24disposing_4(),
	U3CgetMyUserInfoU3Ec__Iterator0_t4123769329::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (U3CChangeUserInfoU3Ec__Iterator1_t3940304722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[10] = 
{
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U3CuserFormU3E__0_0(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_username_1(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_email_2(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_image_3(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_flag_4(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U3CrequestU3E__0_5(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U24this_6(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U24current_7(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U24disposing_8(),
	U3CChangeUserInfoU3Ec__Iterator1_t3940304722::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (U3CNominateUserU3Ec__Iterator2_t1177340224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[6] = 
{
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_U3CuserFormU3E__0_0(),
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_userId_1(),
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_U3CrequestU3E__0_2(),
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_U24current_3(),
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_U24disposing_4(),
	U3CNominateUserU3Ec__Iterator2_t1177340224::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (U3CChangeLanguageU3Ec__Iterator3_t1756740330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[7] = 
{
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U3CuserFormU3E__0_0(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_lang_1(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U3CrequestU3E__0_2(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U24this_3(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U24current_4(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U24disposing_5(),
	U3CChangeLanguageU3Ec__Iterator3_t1756740330::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (U3CChangeUserPasswordU3Ec__Iterator4_t543598374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[8] = 
{
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U3CpassFormU3E__0_0(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_oldPass_1(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_newPass_2(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U3CrequestU3E__0_3(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U24this_4(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U24current_5(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U24disposing_6(),
	U3CChangeUserPasswordU3Ec__Iterator4_t543598374::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (U3CResetPasswordU3Ec__Iterator5_t2616237299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3441[9] = 
{
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U3CpasswordFormU3E__0_0(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_email_1(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_secretKey_2(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_newPassword_3(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U3CrequestU3E__0_4(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U24this_5(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U24current_6(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U24disposing_7(),
	U3CResetPasswordU3Ec__Iterator5_t2616237299::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (ReportManager_t1122460921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[25] = 
{
	ReportManager_t1122460921::get_offset_of_invites_0(),
	ReportManager_t1122460921::get_offset_of_allianceRequsets_1(),
	ReportManager_t1122460921::get_offset_of_battles_2(),
	ReportManager_t1122460921::get_offset_of_allianceBattles_3(),
	ReportManager_t1122460921::get_offset_of_inbox_4(),
	ReportManager_t1122460921::get_offset_of_systemReports_5(),
	ReportManager_t1122460921::get_offset_of_unitTimers_6(),
	ReportManager_t1122460921::get_offset_of_buildingTimers_7(),
	ReportManager_t1122460921::get_offset_of_armyTimers_8(),
	ReportManager_t1122460921::get_offset_of_itemTimers_9(),
	ReportManager_t1122460921::get_offset_of_worldMapTimers_10(),
	ReportManager_t1122460921::get_offset_of_onGetUnreadReports_11(),
	ReportManager_t1122460921::get_offset_of_onGetAllianceBattles_12(),
	ReportManager_t1122460921::get_offset_of_onUserMessageSent_13(),
	ReportManager_t1122460921::get_offset_of_onGetUnitTimers_14(),
	ReportManager_t1122460921::get_offset_of_onGetBuildingTimers_15(),
	ReportManager_t1122460921::get_offset_of_onGetArmyTimers_16(),
	ReportManager_t1122460921::get_offset_of_onGetItemTimers_17(),
	ReportManager_t1122460921::get_offset_of_onGetWorldMapTimers_18(),
	ReportManager_t1122460921::get_offset_of_onMessageDeleted_19(),
	ReportManager_t1122460921::get_offset_of_onBattleReportDeleted_20(),
	ReportManager_t1122460921::get_offset_of_unreadInboxCount_21(),
	ReportManager_t1122460921::get_offset_of_unreadBattleCount_22(),
	ReportManager_t1122460921::get_offset_of_unreadSystemCount_23(),
	ReportManager_t1122460921::get_offset_of_dateFormat_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (U3CReadBattleReportU3Ec__Iterator0_t558420421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[6] = 
{
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_id_1(),
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_U3CrequestU3E__0_2(),
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_U24current_3(),
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_U24disposing_4(),
	U3CReadBattleReportU3Ec__Iterator0_t558420421::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (U3CReadReportMessageU3Ec__Iterator1_t4000497501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[6] = 
{
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_id_1(),
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_U3CrequestU3E__0_2(),
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_U24current_3(),
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_U24disposing_4(),
	U3CReadReportMessageU3Ec__Iterator1_t4000497501::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (U3CReadSystemReportU3Ec__Iterator2_t1664821444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[6] = 
{
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_id_1(),
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_U3CrequestU3E__0_2(),
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_U24current_3(),
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_U24disposing_4(),
	U3CReadSystemReportU3Ec__Iterator2_t1664821444::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (U3CSendUserMessageU3Ec__Iterator3_t3221806769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[9] = 
{
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U3CsendFormU3E__0_0(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_userId_1(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_subject_2(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_message_3(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U3CrequestU3E__0_4(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U24this_5(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U24current_6(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U24disposing_7(),
	U3CSendUserMessageU3Ec__Iterator3_t3221806769::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (U3CGetUnreadReportsU3Ec__Iterator4_t2377312974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[5] = 
{
	U3CGetUnreadReportsU3Ec__Iterator4_t2377312974::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetUnreadReportsU3Ec__Iterator4_t2377312974::get_offset_of_U24this_1(),
	U3CGetUnreadReportsU3Ec__Iterator4_t2377312974::get_offset_of_U24current_2(),
	U3CGetUnreadReportsU3Ec__Iterator4_t2377312974::get_offset_of_U24disposing_3(),
	U3CGetUnreadReportsU3Ec__Iterator4_t2377312974::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[5] = 
{
	U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556::get_offset_of_U24this_1(),
	U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556::get_offset_of_U24current_2(),
	U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556::get_offset_of_U24disposing_3(),
	U3CGetUnreadAllianceBattleReportsU3Ec__Iterator5_t385495556::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (U3CGetUnitTimersU3Ec__Iterator6_t3416549667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[6] = 
{
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_initial_1(),
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_U24this_2(),
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_U24current_3(),
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_U24disposing_4(),
	U3CGetUnitTimersU3Ec__Iterator6_t3416549667::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (U3CGetBuildingTimersU3Ec__Iterator7_t589680364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[6] = 
{
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_initial_1(),
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_U24this_2(),
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_U24current_3(),
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_U24disposing_4(),
	U3CGetBuildingTimersU3Ec__Iterator7_t589680364::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (U3CGetArmyTimersU3Ec__Iterator8_t2156596326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[6] = 
{
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_initial_1(),
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_U24this_2(),
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_U24current_3(),
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_U24disposing_4(),
	U3CGetArmyTimersU3Ec__Iterator8_t2156596326::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (U3CGetItemTimersU3Ec__Iterator9_t2913865839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[6] = 
{
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_initial_1(),
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_U24this_2(),
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_U24current_3(),
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_U24disposing_4(),
	U3CGetItemTimersU3Ec__Iterator9_t2913865839::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (U3CGetWorldMapTimersU3Ec__IteratorA_t706540611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[6] = 
{
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_initial_1(),
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_U24this_2(),
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_U24current_3(),
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_U24disposing_4(),
	U3CGetWorldMapTimersU3Ec__IteratorA_t706540611::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (U3CDeleteMessageReportU3Ec__IteratorB_t871656197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3454[7] = 
{
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U3CdeleteFormU3E__0_0(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_id_1(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U3CrequestU3E__0_2(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U24this_3(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U24current_4(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U24disposing_5(),
	U3CDeleteMessageReportU3Ec__IteratorB_t871656197::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (U3CDeleteBattleReportU3Ec__IteratorC_t3087337682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[7] = 
{
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U3CdeleteFormU3E__0_0(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_id_1(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U3CrequestU3E__0_2(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U24this_3(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U24current_4(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U24disposing_5(),
	U3CDeleteBattleReportU3Ec__IteratorC_t3087337682::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (U3CDeleteScoutReportU3Ec__IteratorD_t2460383078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3456[7] = 
{
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U3CdeleteFormU3E__0_0(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_id_1(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U3CrequestU3E__0_2(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U24this_3(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U24current_4(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U24disposing_5(),
	U3CDeleteScoutReportU3Ec__IteratorD_t2460383078::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (ResourceManager_t484397614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[17] = 
{
	ResourceManager_t484397614::get_offset_of_cityResources_2(),
	ResourceManager_t484397614::get_offset_of_onGetInitialResources_3(),
	ResourceManager_t484397614::get_offset_of_resourceCoroutine_4(),
	ResourceManager_t484397614::get_offset_of_resourcesWorking_5(),
	ResourceManager_t484397614::get_offset_of_buildingEvents_6(),
	ResourceManager_t484397614::get_offset_of_unitEvents_7(),
	ResourceManager_t484397614::get_offset_of_armyEvents_8(),
	ResourceManager_t484397614::get_offset_of_itemEvents_9(),
	ResourceManager_t484397614::get_offset_of_worldMapEvents_10(),
	ResourceManager_t484397614::get_offset_of_newCityEvents_11(),
	ResourceManager_t484397614::get_offset_of_buildingTimers_12(),
	ResourceManager_t484397614::get_offset_of_unitTimers_13(),
	ResourceManager_t484397614::get_offset_of_armyTimers_14(),
	ResourceManager_t484397614::get_offset_of_itemTimers_15(),
	ResourceManager_t484397614::get_offset_of_worldMapTimers_16(),
	ResourceManager_t484397614::get_offset_of_newCityTimers_17(),
	ResourceManager_t484397614::get_offset_of_dateFormat_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[5] = 
{
	U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509::get_offset_of_U24this_1(),
	U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509::get_offset_of_U24current_2(),
	U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509::get_offset_of_U24disposing_3(),
	U3CGetInitialReourcesForCityU3Ec__Iterator0_t2189768509::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (U3CgetResourcesU3Ec__Iterator1_t486193133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3459[7] = 
{
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U3CobjU3E__0_0(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U3CwU3E__0_1(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U3CreplyU3E__1_2(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U24this_3(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U24current_4(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U24disposing_5(),
	U3CgetResourcesU3Ec__Iterator1_t486193133::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[6] = 
{
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U24this_2(),
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U24current_3(),
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U24disposing_4(),
	U3CReadIsNewArmyEventU3Ec__Iterator2_t2625897388::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[6] = 
{
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U24this_2(),
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U24current_3(),
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U24disposing_4(),
	U3CReadIsNewBuildingEventU3Ec__Iterator3_t2099043782::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[6] = 
{
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U24this_2(),
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U24current_3(),
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U24disposing_4(),
	U3CReadIsNewUnitEventU3Ec__Iterator4_t182794934::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[6] = 
{
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U24this_2(),
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U24current_3(),
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U24disposing_4(),
	U3CReadIsNewCustomEventU3Ec__Iterator5_t2961040443::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3464[6] = 
{
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U24this_2(),
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U24current_3(),
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U24disposing_4(),
	U3CReadIsNewWorldMapEventU3Ec__Iterator6_t1752417698::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3465[6] = 
{
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U24this_2(),
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U24current_3(),
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U24disposing_4(),
	U3CReadIsNewCityEventU3Ec__Iterator7_t3353020914::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[6] = 
{
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U24this_2(),
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U24current_3(),
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadArmyEventsU3Ec__Iterator8_t1817838790::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[6] = 
{
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U24this_2(),
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U24current_3(),
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadBuildingEventsU3Ec__Iterator9_t2308996025::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[6] = 
{
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U24this_2(),
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U24current_3(),
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadUnitEventsU3Ec__IteratorA_t3412693977::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3469[6] = 
{
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U24this_2(),
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U24current_3(),
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadCustomEventsU3Ec__IteratorB_t3770294009::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[6] = 
{
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U24this_2(),
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U24current_3(),
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadWorldMapEventsU3Ec__IteratorC_t3586856441::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3471[6] = 
{
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U3CreadFormU3E__0_0(),
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U3CrequestU3E__0_1(),
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U24this_2(),
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U24current_3(),
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U24disposing_4(),
	U3CReadIsUnreadNewCityEventsU3Ec__IteratorD_t1396976694::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (SettingsManager_t2083239687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (TreasureManager_t110095690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[11] = 
{
	TreasureManager_t110095690::get_offset_of_scriptTreasures_0(),
	TreasureManager_t110095690::get_offset_of_speedUpTreasures_1(),
	TreasureManager_t110095690::get_offset_of_productionTreasures_2(),
	TreasureManager_t110095690::get_offset_of_diamondTreasures_3(),
	TreasureManager_t110095690::get_offset_of_promotionsTreasures_4(),
	TreasureManager_t110095690::get_offset_of_armyTreasures_5(),
	TreasureManager_t110095690::get_offset_of_onGetItems_6(),
	TreasureManager_t110095690::get_offset_of_onItemBought_7(),
	TreasureManager_t110095690::get_offset_of_onItemSold_8(),
	TreasureManager_t110095690::get_offset_of_onItemGifted_9(),
	TreasureManager_t110095690::get_offset_of_onItemUsed_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (U3CGetCityTreasuresU3Ec__Iterator0_t3928749663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[6] = 
{
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_initial_1(),
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_U24this_2(),
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_U24current_3(),
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_U24disposing_4(),
	U3CGetCityTreasuresU3Ec__Iterator0_t3928749663::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (U3CUseItemU3Ec__Iterator1_t483476331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3475[7] = 
{
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U3CuseItemFormU3E__0_0(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_itemId_1(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U3CrequestU3E__0_2(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U24this_3(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U24current_4(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U24disposing_5(),
	U3CUseItemU3Ec__Iterator1_t483476331::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (U3CUseUpgradeU3Ec__Iterator2_t864670046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3476[9] = 
{
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U3CuseItemFormU3E__0_0(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_itemId_1(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_valley_2(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_building_id_3(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U3CrequestU3E__0_4(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U24this_5(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U24current_6(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U24disposing_7(),
	U3CUseUpgradeU3Ec__Iterator2_t864670046::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (U3CUseTeleportU3Ec__Iterator3_t2451633368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[9] = 
{
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U3CuseItemFormU3E__0_0(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_itemId_1(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_x_2(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_y_3(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U3CrequestU3E__0_4(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U24this_5(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U24current_6(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U24disposing_7(),
	U3CUseTeleportU3Ec__Iterator3_t2451633368::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (U3CUseShieldHourU3Ec__Iterator4_t3002913516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[8] = 
{
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U3CuseItemFormU3E__0_0(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_itemId_1(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_hour_2(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U3CrequestU3E__0_3(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U24this_4(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U24current_5(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U24disposing_6(),
	U3CUseShieldHourU3Ec__Iterator4_t3002913516::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (U3CUseShieldDayU3Ec__Iterator5_t2970678129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3479[8] = 
{
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U3CuseItemFormU3E__0_0(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_itemId_1(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_day_2(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U3CrequestU3E__0_3(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U24this_4(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U24current_5(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U24disposing_6(),
	U3CUseShieldDayU3Ec__Iterator5_t2970678129::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (U3CUseSpeedUpU3Ec__Iterator6_t780075305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[8] = 
{
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U3CuseSpeedUpFormU3E__0_0(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_itemId_1(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_eventId_2(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U3CrequestU3E__0_3(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U24this_4(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U24current_5(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U24disposing_6(),
	U3CUseSpeedUpU3Ec__Iterator6_t780075305::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (U3CBuyItemU3Ec__Iterator7_t4213354092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[8] = 
{
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U3CbuyItemFormU3E__0_0(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_itemId_1(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_count_2(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U3CrequestU3E__0_3(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U24this_4(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U24current_5(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U24disposing_6(),
	U3CBuyItemU3Ec__Iterator7_t4213354092::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (U3CBuyPlayerItemU3Ec__Iterator8_t2841192203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[8] = 
{
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U3CbuyItemFormU3E__0_0(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_itemId_1(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_count_2(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U3CrequestU3E__0_3(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U24this_4(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U24current_5(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U24disposing_6(),
	U3CBuyPlayerItemU3Ec__Iterator8_t2841192203::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (U3CSellItemU3Ec__Iterator9_t3920914631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[9] = 
{
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U3CsellItemFormU3E__0_0(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_itemId_1(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_count_2(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_discount_3(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U3CrequestU3E__0_4(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U24this_5(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U24current_6(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U24disposing_7(),
	U3CSellItemU3Ec__Iterator9_t3920914631::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (U3CGiftItemU3Ec__IteratorA_t1993921738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[9] = 
{
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U3CgiftItemFormU3E__0_0(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_cityID_1(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_itemId_2(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_count_3(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U3CrequestU3E__0_4(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U24this_5(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U24current_6(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U24disposing_7(),
	U3CGiftItemU3Ec__IteratorA_t1993921738::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (UnitsManager_t4062574081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[10] = 
{
	UnitsManager_t4062574081::get_offset_of_cityUnits_0(),
	UnitsManager_t4062574081::get_offset_of_cityGenerals_1(),
	UnitsManager_t4062574081::get_offset_of_onUnitsHired_2(),
	UnitsManager_t4062574081::get_offset_of_onGeneralHired_3(),
	UnitsManager_t4062574081::get_offset_of_onGeneralDismissed_4(),
	UnitsManager_t4062574081::get_offset_of_onUnitsUpdated_5(),
	UnitsManager_t4062574081::get_offset_of_onGeneralUpgraded_6(),
	UnitsManager_t4062574081::get_offset_of_onTrainingCancelled_7(),
	UnitsManager_t4062574081::get_offset_of_onUnitsDismissed_8(),
	UnitsManager_t4062574081::get_offset_of_onGetGenerals_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3486[5] = 
{
	U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573::get_offset_of_U24this_1(),
	U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573::get_offset_of_U24current_2(),
	U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573::get_offset_of_U24disposing_3(),
	U3CGetInitialCityUnitsU3Ec__Iterator0_t367879573::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3487[5] = 
{
	U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502::get_offset_of_U24this_1(),
	U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502::get_offset_of_U24current_2(),
	U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502::get_offset_of_U24disposing_3(),
	U3CGetInitialCityGeneralsU3Ec__Iterator1_t4092798502::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[5] = 
{
	U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963::get_offset_of_U24this_1(),
	U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963::get_offset_of_U24current_2(),
	U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963::get_offset_of_U24disposing_3(),
	U3CUpdateCityUnitsU3Ec__Iterator2_t1596891963::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3489[5] = 
{
	U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504::get_offset_of_U3CrequestU3E__0_0(),
	U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504::get_offset_of_U24this_1(),
	U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504::get_offset_of_U24current_2(),
	U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504::get_offset_of_U24disposing_3(),
	U3CUpdateCityGeneralsU3Ec__Iterator3_t2358464504::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (U3CHireGeneralU3Ec__Iterator4_t1375040851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[7] = 
{
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U3ChireFormU3E__0_0(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_general_1(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U3CrequestU3E__0_2(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U24this_3(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U24current_4(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U24disposing_5(),
	U3CHireGeneralU3Ec__Iterator4_t1375040851::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (U3CDismissGeneralU3Ec__Iterator5_t2234259606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3491[7] = 
{
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U3CdismissFormU3E__0_0(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_general_1(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U3CrequestU3E__0_2(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U24this_3(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U24current_4(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U24disposing_5(),
	U3CDismissGeneralU3Ec__Iterator5_t2234259606::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (U3CHireUnitsU3Ec__Iterator6_t1010860969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3492[8] = 
{
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U3CunitFormU3E__0_0(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_name_1(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_count_2(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U3CrequestU3E__0_3(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U24this_4(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U24current_5(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U24disposing_6(),
	U3CHireUnitsU3Ec__Iterator6_t1010860969::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (U3CUpgradeGeneralU3Ec__Iterator7_t4280997162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[12] = 
{
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U3CupgradeFormU3E__0_0(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_heroId_1(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_march_2(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_politics_3(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_speed_4(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_salary_5(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_loyalty_6(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U3CrequestU3E__0_7(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U24this_8(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U24current_9(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U24disposing_10(),
	U3CUpgradeGeneralU3Ec__Iterator7_t4280997162::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (U3CCancelTrainingU3Ec__Iterator8_t585120171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3494[7] = 
{
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U3CcancelFormU3E__0_0(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_event_id_1(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U3CrequestU3E__0_2(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U24this_3(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U24current_4(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U24disposing_5(),
	U3CCancelTrainingU3Ec__Iterator8_t585120171::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (U3CDismissUnitsU3Ec__Iterator9_t617410052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3495[7] = 
{
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U3CformU3E__0_0(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_army_1(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U3CrequestU3E__0_2(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U24this_3(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U24current_4(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U24disposing_5(),
	U3CDismissUnitsU3Ec__Iterator9_t617410052::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (LocalizedBuildingName_t3861673193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[1] = 
{
	LocalizedBuildingName_t3861673193::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (MapCityInfoManager_t1539510113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[17] = 
{
	MapCityInfoManager_t1539510113::get_offset_of_rank_2(),
	MapCityInfoManager_t1539510113::get_offset_of_experience_3(),
	MapCityInfoManager_t1539510113::get_offset_of_region_4(),
	MapCityInfoManager_t1539510113::get_offset_of_coords_5(),
	MapCityInfoManager_t1539510113::get_offset_of_alliance_6(),
	MapCityInfoManager_t1539510113::get_offset_of_status_7(),
	MapCityInfoManager_t1539510113::get_offset_of_cityName_8(),
	MapCityInfoManager_t1539510113::get_offset_of_playerName_9(),
	MapCityInfoManager_t1539510113::get_offset_of_occupationLabel_10(),
	MapCityInfoManager_t1539510113::get_offset_of_marchBtn_11(),
	MapCityInfoManager_t1539510113::get_offset_of_revoltBtn_12(),
	MapCityInfoManager_t1539510113::get_offset_of_nominateBtn_13(),
	MapCityInfoManager_t1539510113::get_offset_of_shopBtn_14(),
	MapCityInfoManager_t1539510113::get_offset_of_giftBtn_15(),
	MapCityInfoManager_t1539510113::get_offset_of_playerImage_16(),
	MapCityInfoManager_t1539510113::get_offset_of_cityImage_17(),
	MapCityInfoManager_t1539510113::get_offset_of_field_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (AllianceEventModel_t1723640982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3498[4] = 
{
	AllianceEventModel_t1723640982::get_offset_of_id_0(),
	AllianceEventModel_t1723640982::get_offset_of_alliance_1(),
	AllianceEventModel_t1723640982::get_offset_of_message_2(),
	AllianceEventModel_t1723640982::get_offset_of_date_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (AllianceModel_t2995969982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3499[12] = 
{
	AllianceModel_t2995969982::get_offset_of_id_0(),
	AllianceModel_t2995969982::get_offset_of_alliance_name_1(),
	AllianceModel_t2995969982::get_offset_of_founderId_2(),
	AllianceModel_t2995969982::get_offset_of_founderName_3(),
	AllianceModel_t2995969982::get_offset_of_leaderId_4(),
	AllianceModel_t2995969982::get_offset_of_leaderName_5(),
	AllianceModel_t2995969982::get_offset_of_allies_6(),
	AllianceModel_t2995969982::get_offset_of_enemies_7(),
	AllianceModel_t2995969982::get_offset_of_rank_8(),
	AllianceModel_t2995969982::get_offset_of_members_count_9(),
	AllianceModel_t2995969982::get_offset_of_alliance_experience_10(),
	AllianceModel_t2995969982::get_offset_of_guideline_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
