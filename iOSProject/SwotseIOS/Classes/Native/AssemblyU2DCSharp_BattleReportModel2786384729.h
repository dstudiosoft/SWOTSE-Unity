﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserModel
struct UserModel_t3025569216;
// MyCityModel
struct MyCityModel_t1736786178;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.Collections.Generic.List`1<CityTreasureModel>
struct List_1_t877182185;
// JSONObject
struct JSONObject_t1971882247;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReportModel
struct  BattleReportModel_t2786384729  : public Il2CppObject
{
public:
	// System.Int64 BattleReportModel::id
	int64_t ___id_0;
	// UserModel BattleReportModel::attackUser
	UserModel_t3025569216 * ___attackUser_1;
	// UserModel BattleReportModel::defenceUser
	UserModel_t3025569216 * ___defenceUser_2;
	// MyCityModel BattleReportModel::attackersCity
	MyCityModel_t1736786178 * ___attackersCity_3;
	// MyCityModel BattleReportModel::defendersCity
	MyCityModel_t1736786178 * ___defendersCity_4;
	// ResourcesModel BattleReportModel::attackArmyResources
	ResourcesModel_t2978985958 * ___attackArmyResources_5;
	// ResourcesModel BattleReportModel::defenceArmyResources
	ResourcesModel_t2978985958 * ___defenceArmyResources_6;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportModel::stolenCityTreasures
	List_1_t877182185 * ___stolenCityTreasures_7;
	// JSONObject BattleReportModel::attackArmyBeforeBattle
	JSONObject_t1971882247 * ___attackArmyBeforeBattle_8;
	// JSONObject BattleReportModel::defenceArmyBeforeBattle
	JSONObject_t1971882247 * ___defenceArmyBeforeBattle_9;
	// JSONObject BattleReportModel::attackArmyAfterBattle
	JSONObject_t1971882247 * ___attackArmyAfterBattle_10;
	// JSONObject BattleReportModel::defenceArmyAfterBattle
	JSONObject_t1971882247 * ___defenceArmyAfterBattle_11;
	// ResourcesModel BattleReportModel::defendersResources
	ResourcesModel_t2978985958 * ___defendersResources_12;
	// System.Collections.Generic.List`1<CityTreasureModel> BattleReportModel::defendersCityItems
	List_1_t877182185 * ___defendersCityItems_13;
	// System.Boolean BattleReportModel::isSuccessAttack
	bool ___isSuccessAttack_14;
	// System.DateTime BattleReportModel::creation_date
	DateTime_t693205669  ___creation_date_15;
	// System.String BattleReportModel::attack_type
	String_t* ___attack_type_16;
	// System.String BattleReportModel::log
	String_t* ___log_17;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_attackUser_1() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attackUser_1)); }
	inline UserModel_t3025569216 * get_attackUser_1() const { return ___attackUser_1; }
	inline UserModel_t3025569216 ** get_address_of_attackUser_1() { return &___attackUser_1; }
	inline void set_attackUser_1(UserModel_t3025569216 * value)
	{
		___attackUser_1 = value;
		Il2CppCodeGenWriteBarrier(&___attackUser_1, value);
	}

	inline static int32_t get_offset_of_defenceUser_2() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defenceUser_2)); }
	inline UserModel_t3025569216 * get_defenceUser_2() const { return ___defenceUser_2; }
	inline UserModel_t3025569216 ** get_address_of_defenceUser_2() { return &___defenceUser_2; }
	inline void set_defenceUser_2(UserModel_t3025569216 * value)
	{
		___defenceUser_2 = value;
		Il2CppCodeGenWriteBarrier(&___defenceUser_2, value);
	}

	inline static int32_t get_offset_of_attackersCity_3() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attackersCity_3)); }
	inline MyCityModel_t1736786178 * get_attackersCity_3() const { return ___attackersCity_3; }
	inline MyCityModel_t1736786178 ** get_address_of_attackersCity_3() { return &___attackersCity_3; }
	inline void set_attackersCity_3(MyCityModel_t1736786178 * value)
	{
		___attackersCity_3 = value;
		Il2CppCodeGenWriteBarrier(&___attackersCity_3, value);
	}

	inline static int32_t get_offset_of_defendersCity_4() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defendersCity_4)); }
	inline MyCityModel_t1736786178 * get_defendersCity_4() const { return ___defendersCity_4; }
	inline MyCityModel_t1736786178 ** get_address_of_defendersCity_4() { return &___defendersCity_4; }
	inline void set_defendersCity_4(MyCityModel_t1736786178 * value)
	{
		___defendersCity_4 = value;
		Il2CppCodeGenWriteBarrier(&___defendersCity_4, value);
	}

	inline static int32_t get_offset_of_attackArmyResources_5() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attackArmyResources_5)); }
	inline ResourcesModel_t2978985958 * get_attackArmyResources_5() const { return ___attackArmyResources_5; }
	inline ResourcesModel_t2978985958 ** get_address_of_attackArmyResources_5() { return &___attackArmyResources_5; }
	inline void set_attackArmyResources_5(ResourcesModel_t2978985958 * value)
	{
		___attackArmyResources_5 = value;
		Il2CppCodeGenWriteBarrier(&___attackArmyResources_5, value);
	}

	inline static int32_t get_offset_of_defenceArmyResources_6() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defenceArmyResources_6)); }
	inline ResourcesModel_t2978985958 * get_defenceArmyResources_6() const { return ___defenceArmyResources_6; }
	inline ResourcesModel_t2978985958 ** get_address_of_defenceArmyResources_6() { return &___defenceArmyResources_6; }
	inline void set_defenceArmyResources_6(ResourcesModel_t2978985958 * value)
	{
		___defenceArmyResources_6 = value;
		Il2CppCodeGenWriteBarrier(&___defenceArmyResources_6, value);
	}

	inline static int32_t get_offset_of_stolenCityTreasures_7() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___stolenCityTreasures_7)); }
	inline List_1_t877182185 * get_stolenCityTreasures_7() const { return ___stolenCityTreasures_7; }
	inline List_1_t877182185 ** get_address_of_stolenCityTreasures_7() { return &___stolenCityTreasures_7; }
	inline void set_stolenCityTreasures_7(List_1_t877182185 * value)
	{
		___stolenCityTreasures_7 = value;
		Il2CppCodeGenWriteBarrier(&___stolenCityTreasures_7, value);
	}

	inline static int32_t get_offset_of_attackArmyBeforeBattle_8() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attackArmyBeforeBattle_8)); }
	inline JSONObject_t1971882247 * get_attackArmyBeforeBattle_8() const { return ___attackArmyBeforeBattle_8; }
	inline JSONObject_t1971882247 ** get_address_of_attackArmyBeforeBattle_8() { return &___attackArmyBeforeBattle_8; }
	inline void set_attackArmyBeforeBattle_8(JSONObject_t1971882247 * value)
	{
		___attackArmyBeforeBattle_8 = value;
		Il2CppCodeGenWriteBarrier(&___attackArmyBeforeBattle_8, value);
	}

	inline static int32_t get_offset_of_defenceArmyBeforeBattle_9() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defenceArmyBeforeBattle_9)); }
	inline JSONObject_t1971882247 * get_defenceArmyBeforeBattle_9() const { return ___defenceArmyBeforeBattle_9; }
	inline JSONObject_t1971882247 ** get_address_of_defenceArmyBeforeBattle_9() { return &___defenceArmyBeforeBattle_9; }
	inline void set_defenceArmyBeforeBattle_9(JSONObject_t1971882247 * value)
	{
		___defenceArmyBeforeBattle_9 = value;
		Il2CppCodeGenWriteBarrier(&___defenceArmyBeforeBattle_9, value);
	}

	inline static int32_t get_offset_of_attackArmyAfterBattle_10() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attackArmyAfterBattle_10)); }
	inline JSONObject_t1971882247 * get_attackArmyAfterBattle_10() const { return ___attackArmyAfterBattle_10; }
	inline JSONObject_t1971882247 ** get_address_of_attackArmyAfterBattle_10() { return &___attackArmyAfterBattle_10; }
	inline void set_attackArmyAfterBattle_10(JSONObject_t1971882247 * value)
	{
		___attackArmyAfterBattle_10 = value;
		Il2CppCodeGenWriteBarrier(&___attackArmyAfterBattle_10, value);
	}

	inline static int32_t get_offset_of_defenceArmyAfterBattle_11() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defenceArmyAfterBattle_11)); }
	inline JSONObject_t1971882247 * get_defenceArmyAfterBattle_11() const { return ___defenceArmyAfterBattle_11; }
	inline JSONObject_t1971882247 ** get_address_of_defenceArmyAfterBattle_11() { return &___defenceArmyAfterBattle_11; }
	inline void set_defenceArmyAfterBattle_11(JSONObject_t1971882247 * value)
	{
		___defenceArmyAfterBattle_11 = value;
		Il2CppCodeGenWriteBarrier(&___defenceArmyAfterBattle_11, value);
	}

	inline static int32_t get_offset_of_defendersResources_12() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defendersResources_12)); }
	inline ResourcesModel_t2978985958 * get_defendersResources_12() const { return ___defendersResources_12; }
	inline ResourcesModel_t2978985958 ** get_address_of_defendersResources_12() { return &___defendersResources_12; }
	inline void set_defendersResources_12(ResourcesModel_t2978985958 * value)
	{
		___defendersResources_12 = value;
		Il2CppCodeGenWriteBarrier(&___defendersResources_12, value);
	}

	inline static int32_t get_offset_of_defendersCityItems_13() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___defendersCityItems_13)); }
	inline List_1_t877182185 * get_defendersCityItems_13() const { return ___defendersCityItems_13; }
	inline List_1_t877182185 ** get_address_of_defendersCityItems_13() { return &___defendersCityItems_13; }
	inline void set_defendersCityItems_13(List_1_t877182185 * value)
	{
		___defendersCityItems_13 = value;
		Il2CppCodeGenWriteBarrier(&___defendersCityItems_13, value);
	}

	inline static int32_t get_offset_of_isSuccessAttack_14() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___isSuccessAttack_14)); }
	inline bool get_isSuccessAttack_14() const { return ___isSuccessAttack_14; }
	inline bool* get_address_of_isSuccessAttack_14() { return &___isSuccessAttack_14; }
	inline void set_isSuccessAttack_14(bool value)
	{
		___isSuccessAttack_14 = value;
	}

	inline static int32_t get_offset_of_creation_date_15() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___creation_date_15)); }
	inline DateTime_t693205669  get_creation_date_15() const { return ___creation_date_15; }
	inline DateTime_t693205669 * get_address_of_creation_date_15() { return &___creation_date_15; }
	inline void set_creation_date_15(DateTime_t693205669  value)
	{
		___creation_date_15 = value;
	}

	inline static int32_t get_offset_of_attack_type_16() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___attack_type_16)); }
	inline String_t* get_attack_type_16() const { return ___attack_type_16; }
	inline String_t** get_address_of_attack_type_16() { return &___attack_type_16; }
	inline void set_attack_type_16(String_t* value)
	{
		___attack_type_16 = value;
		Il2CppCodeGenWriteBarrier(&___attack_type_16, value);
	}

	inline static int32_t get_offset_of_log_17() { return static_cast<int32_t>(offsetof(BattleReportModel_t2786384729, ___log_17)); }
	inline String_t* get_log_17() const { return ___log_17; }
	inline String_t** get_address_of_log_17() { return &___log_17; }
	inline void set_log_17(String_t* value)
	{
		___log_17 = value;
		Il2CppCodeGenWriteBarrier(&___log_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
