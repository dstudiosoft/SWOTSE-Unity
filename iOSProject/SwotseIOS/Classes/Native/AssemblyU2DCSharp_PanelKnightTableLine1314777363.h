﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelKnightTableLine
struct  PanelKnightTableLine_t1314777363  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text PanelKnightTableLine::knightName
	Text_t356221433 * ___knightName_2;
	// UnityEngine.UI.Text PanelKnightTableLine::knightLevel
	Text_t356221433 * ___knightLevel_3;
	// UnityEngine.UI.Text PanelKnightTableLine::status
	Text_t356221433 * ___status_4;
	// System.Int64 PanelKnightTableLine::knightId
	int64_t ___knightId_5;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t1314777363, ___knightName_2)); }
	inline Text_t356221433 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t356221433 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t356221433 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier(&___knightName_2, value);
	}

	inline static int32_t get_offset_of_knightLevel_3() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t1314777363, ___knightLevel_3)); }
	inline Text_t356221433 * get_knightLevel_3() const { return ___knightLevel_3; }
	inline Text_t356221433 ** get_address_of_knightLevel_3() { return &___knightLevel_3; }
	inline void set_knightLevel_3(Text_t356221433 * value)
	{
		___knightLevel_3 = value;
		Il2CppCodeGenWriteBarrier(&___knightLevel_3, value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t1314777363, ___status_4)); }
	inline Text_t356221433 * get_status_4() const { return ___status_4; }
	inline Text_t356221433 ** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(Text_t356221433 * value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier(&___status_4, value);
	}

	inline static int32_t get_offset_of_knightId_5() { return static_cast<int32_t>(offsetof(PanelKnightTableLine_t1314777363, ___knightId_5)); }
	inline int64_t get_knightId_5() const { return ___knightId_5; }
	inline int64_t* get_address_of_knightId_5() { return &___knightId_5; }
	inline void set_knightId_5(int64_t value)
	{
		___knightId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
