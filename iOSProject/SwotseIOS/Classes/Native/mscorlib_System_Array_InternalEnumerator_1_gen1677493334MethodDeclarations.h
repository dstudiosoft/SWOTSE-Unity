﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1677493334.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4171994105_gshared (InternalEnumerator_1_t1677493334 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4171994105(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1677493334 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4171994105_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449692397_gshared (InternalEnumerator_1_t1677493334 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449692397(__this, method) ((  void (*) (InternalEnumerator_1_t1677493334 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449692397_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1237160357_gshared (InternalEnumerator_1_t1677493334 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1237160357(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1677493334 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1237160357_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m19337650_gshared (InternalEnumerator_1_t1677493334 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19337650(__this, method) ((  void (*) (InternalEnumerator_1_t1677493334 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19337650_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2676314501_gshared (InternalEnumerator_1_t1677493334 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2676314501(__this, method) ((  bool (*) (InternalEnumerator_1_t1677493334 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2676314501_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t818741072  InternalEnumerator_1_get_Current_m1922909696_gshared (InternalEnumerator_1_t1677493334 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1922909696(__this, method) ((  KeyValuePair_2_t818741072  (*) (InternalEnumerator_1_t1677493334 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1922909696_gshared)(__this, method)
