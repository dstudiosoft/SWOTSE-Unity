﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.UnixIPv4InterfaceProperties
struct UnixIPv4InterfaceProperties_t179777771;
// System.Net.NetworkInformation.UnixNetworkInterface
struct UnixNetworkInterface_t1000704527;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"

// System.Void System.Net.NetworkInformation.UnixIPv4InterfaceProperties::.ctor(System.Net.NetworkInformation.UnixNetworkInterface)
extern "C"  void UnixIPv4InterfaceProperties__ctor_m796527670 (UnixIPv4InterfaceProperties_t179777771 * __this, UnixNetworkInterface_t1000704527 * ___iface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.UnixIPv4InterfaceProperties::get_Index()
extern "C"  int32_t UnixIPv4InterfaceProperties_get_Index_m2935970524 (UnixIPv4InterfaceProperties_t179777771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPv4InterfaceProperties::get_IsAutomaticPrivateAddressingActive()
extern "C"  bool UnixIPv4InterfaceProperties_get_IsAutomaticPrivateAddressingActive_m997945974 (UnixIPv4InterfaceProperties_t179777771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPv4InterfaceProperties::get_IsAutomaticPrivateAddressingEnabled()
extern "C"  bool UnixIPv4InterfaceProperties_get_IsAutomaticPrivateAddressingEnabled_m493688961 (UnixIPv4InterfaceProperties_t179777771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPv4InterfaceProperties::get_IsDhcpEnabled()
extern "C"  bool UnixIPv4InterfaceProperties_get_IsDhcpEnabled_m1218923620 (UnixIPv4InterfaceProperties_t179777771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPv4InterfaceProperties::get_UsesWins()
extern "C"  bool UnixIPv4InterfaceProperties_get_UsesWins_m1680669455 (UnixIPv4InterfaceProperties_t179777771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
