﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyCityModel
struct  MyCityModel_t1736786178  : public Il2CppObject
{
public:
	// System.Int64 MyCityModel::id
	int64_t ___id_0;
	// System.String MyCityModel::city_name
	String_t* ___city_name_1;
	// System.Int64 MyCityModel::posX
	int64_t ___posX_2;
	// System.Int64 MyCityModel::posY
	int64_t ___posY_3;
	// System.String MyCityModel::region
	String_t* ___region_4;
	// System.String MyCityModel::image_name
	String_t* ___image_name_5;
	// System.Int64 MyCityModel::max_population
	int64_t ___max_population_6;
	// System.Int64 MyCityModel::current_population
	int64_t ___current_population_7;
	// System.Int64 MyCityModel::happiness
	int64_t ___happiness_8;
	// System.Int64 MyCityModel::courage
	int64_t ___courage_9;
	// System.Boolean MyCityModel::isExpantion
	bool ___isExpantion_10;
	// System.Boolean MyCityModel::reinforce_flag
	bool ___reinforce_flag_11;
	// System.Int64 MyCityModel::gold_tax_rate
	int64_t ___gold_tax_rate_12;
	// System.Int64 MyCityModel::fields_count
	int64_t ___fields_count_13;
	// System.Int64 MyCityModel::valleys_count
	int64_t ___valleys_count_14;
	// System.Int64 MyCityModel::mayer_residence_level
	int64_t ___mayer_residence_level_15;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_city_name_1() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___city_name_1)); }
	inline String_t* get_city_name_1() const { return ___city_name_1; }
	inline String_t** get_address_of_city_name_1() { return &___city_name_1; }
	inline void set_city_name_1(String_t* value)
	{
		___city_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___city_name_1, value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___posX_2)); }
	inline int64_t get_posX_2() const { return ___posX_2; }
	inline int64_t* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(int64_t value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___posY_3)); }
	inline int64_t get_posY_3() const { return ___posY_3; }
	inline int64_t* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(int64_t value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___region_4)); }
	inline String_t* get_region_4() const { return ___region_4; }
	inline String_t** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(String_t* value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier(&___region_4, value);
	}

	inline static int32_t get_offset_of_image_name_5() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___image_name_5)); }
	inline String_t* get_image_name_5() const { return ___image_name_5; }
	inline String_t** get_address_of_image_name_5() { return &___image_name_5; }
	inline void set_image_name_5(String_t* value)
	{
		___image_name_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_name_5, value);
	}

	inline static int32_t get_offset_of_max_population_6() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___max_population_6)); }
	inline int64_t get_max_population_6() const { return ___max_population_6; }
	inline int64_t* get_address_of_max_population_6() { return &___max_population_6; }
	inline void set_max_population_6(int64_t value)
	{
		___max_population_6 = value;
	}

	inline static int32_t get_offset_of_current_population_7() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___current_population_7)); }
	inline int64_t get_current_population_7() const { return ___current_population_7; }
	inline int64_t* get_address_of_current_population_7() { return &___current_population_7; }
	inline void set_current_population_7(int64_t value)
	{
		___current_population_7 = value;
	}

	inline static int32_t get_offset_of_happiness_8() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___happiness_8)); }
	inline int64_t get_happiness_8() const { return ___happiness_8; }
	inline int64_t* get_address_of_happiness_8() { return &___happiness_8; }
	inline void set_happiness_8(int64_t value)
	{
		___happiness_8 = value;
	}

	inline static int32_t get_offset_of_courage_9() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___courage_9)); }
	inline int64_t get_courage_9() const { return ___courage_9; }
	inline int64_t* get_address_of_courage_9() { return &___courage_9; }
	inline void set_courage_9(int64_t value)
	{
		___courage_9 = value;
	}

	inline static int32_t get_offset_of_isExpantion_10() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___isExpantion_10)); }
	inline bool get_isExpantion_10() const { return ___isExpantion_10; }
	inline bool* get_address_of_isExpantion_10() { return &___isExpantion_10; }
	inline void set_isExpantion_10(bool value)
	{
		___isExpantion_10 = value;
	}

	inline static int32_t get_offset_of_reinforce_flag_11() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___reinforce_flag_11)); }
	inline bool get_reinforce_flag_11() const { return ___reinforce_flag_11; }
	inline bool* get_address_of_reinforce_flag_11() { return &___reinforce_flag_11; }
	inline void set_reinforce_flag_11(bool value)
	{
		___reinforce_flag_11 = value;
	}

	inline static int32_t get_offset_of_gold_tax_rate_12() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___gold_tax_rate_12)); }
	inline int64_t get_gold_tax_rate_12() const { return ___gold_tax_rate_12; }
	inline int64_t* get_address_of_gold_tax_rate_12() { return &___gold_tax_rate_12; }
	inline void set_gold_tax_rate_12(int64_t value)
	{
		___gold_tax_rate_12 = value;
	}

	inline static int32_t get_offset_of_fields_count_13() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___fields_count_13)); }
	inline int64_t get_fields_count_13() const { return ___fields_count_13; }
	inline int64_t* get_address_of_fields_count_13() { return &___fields_count_13; }
	inline void set_fields_count_13(int64_t value)
	{
		___fields_count_13 = value;
	}

	inline static int32_t get_offset_of_valleys_count_14() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___valleys_count_14)); }
	inline int64_t get_valleys_count_14() const { return ___valleys_count_14; }
	inline int64_t* get_address_of_valleys_count_14() { return &___valleys_count_14; }
	inline void set_valleys_count_14(int64_t value)
	{
		___valleys_count_14 = value;
	}

	inline static int32_t get_offset_of_mayer_residence_level_15() { return static_cast<int32_t>(offsetof(MyCityModel_t1736786178, ___mayer_residence_level_15)); }
	inline int64_t get_mayer_residence_level_15() const { return ___mayer_residence_level_15; }
	inline int64_t* get_address_of_mayer_residence_level_15() { return &___mayer_residence_level_15; }
	inline void set_mayer_residence_level_15(int64_t value)
	{
		___mayer_residence_level_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
