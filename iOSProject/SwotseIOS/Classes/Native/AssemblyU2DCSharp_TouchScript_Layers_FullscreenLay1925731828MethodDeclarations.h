﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.FullscreenLayer
struct FullscreenLayer_t1925731828;
// UnityEngine.Camera
struct Camera_t189460977;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_FullscreenLaye777199562.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer_La1590288664.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"

// System.Void TouchScript.Layers.FullscreenLayer::.ctor()
extern "C"  void FullscreenLayer__ctor_m3916595617 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.FullscreenLayer/LayerType TouchScript.Layers.FullscreenLayer::get_Type()
extern "C"  int32_t FullscreenLayer_get_Type_m178615345 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::set_Type(TouchScript.Layers.FullscreenLayer/LayerType)
extern "C"  void FullscreenLayer_set_Type_m236525480 (FullscreenLayer_t1925731828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera TouchScript.Layers.FullscreenLayer::get_Camera()
extern "C"  Camera_t189460977 * FullscreenLayer_get_Camera_m219721486 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::set_Camera(UnityEngine.Camera)
extern "C"  void FullscreenLayer_set_Camera_m1126839547 (FullscreenLayer_t1925731828 * __this, Camera_t189460977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.FullscreenLayer::get_WorldProjectionNormal()
extern "C"  Vector3_t2243707580  FullscreenLayer_get_WorldProjectionNormal_m4144234920 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer/LayerHitResult TouchScript.Layers.FullscreenLayer::Hit(UnityEngine.Vector2,TouchScript.Hit.TouchHit&)
extern "C"  int32_t FullscreenLayer_Hit_m4014427400 (FullscreenLayer_t1925731828 * __this, Vector2_t2243707579  ___position0, TouchHit_t4186847494 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::Awake()
extern "C"  void FullscreenLayer_Awake_m252727580 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::setName()
extern "C"  void FullscreenLayer_setName_m4200496700 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.Layers.FullscreenLayer::createProjectionParams()
extern "C"  ProjectionParams_t2712959773 * FullscreenLayer_createProjectionParams_m426390250 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::updateCamera()
extern "C"  void FullscreenLayer_updateCamera_m549423581 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Layers.FullscreenLayer::cacheCameraTransform()
extern "C"  void FullscreenLayer_cacheCameraTransform_m727326060 (FullscreenLayer_t1925731828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
