﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.InputHandlers.TouchHandler
struct TouchHandler_t2521645213;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>
struct Func_4_t1475708928;
// System.Action`2<System.Int32,UnityEngine.Vector2>
struct Action_2_t1542075644;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void TouchScript.InputSources.InputHandlers.TouchHandler::.ctor(TouchScript.Tags,System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>,System.Action`2<System.Int32,UnityEngine.Vector2>,System.Action`1<System.Int32>,System.Action`1<System.Int32>)
extern "C"  void TouchHandler__ctor_m3841336190 (TouchHandler_t2521645213 * __this, Tags_t1265380163 * ___tags0, Func_4_t1475708928 * ___beginTouch1, Action_2_t1542075644 * ___moveTouch2, Action_1_t1873676830 * ___endTouch3, Action_1_t1873676830 * ___cancelTouch4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.InputSources.InputHandlers.TouchHandler::get_HasTouches()
extern "C"  bool TouchHandler_get_HasTouches_m3880949891 (TouchHandler_t2521645213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.TouchHandler::Update()
extern "C"  void TouchHandler_Update_m84976388 (TouchHandler_t2521645213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.InputSources.InputHandlers.TouchHandler::CancelTouch(TouchScript.TouchPoint,System.Boolean)
extern "C"  bool TouchHandler_CancelTouch_m765158768 (TouchHandler_t2521645213 * __this, TouchPoint_t959629083 * ___touch0, bool ___return1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.TouchHandler::Dispose()
extern "C"  void TouchHandler_Dispose_m2061346944 (TouchHandler_t2521645213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.InputSources.InputHandlers.TouchHandler::internalBeginTouch(UnityEngine.Vector2)
extern "C"  TouchPoint_t959629083 * TouchHandler_internalBeginTouch_m1108704808 (TouchHandler_t2521645213 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.TouchHandler::internalEndTouch(System.Int32)
extern "C"  void TouchHandler_internalEndTouch_m2505025283 (TouchHandler_t2521645213 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.InputSources.InputHandlers.TouchHandler::internalCancelTouch(System.Int32)
extern "C"  void TouchHandler_internalCancelTouch_m1572470690 (TouchHandler_t2521645213 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
