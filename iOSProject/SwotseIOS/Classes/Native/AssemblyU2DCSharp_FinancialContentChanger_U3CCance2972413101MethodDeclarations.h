﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinancialContentChanger/<CancelOfferRequest>c__IteratorD
struct U3CCancelOfferRequestU3Ec__IteratorD_t2972413101;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FinancialContentChanger/<CancelOfferRequest>c__IteratorD::.ctor()
extern "C"  void U3CCancelOfferRequestU3Ec__IteratorD__ctor_m822359828 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<CancelOfferRequest>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCancelOfferRequestU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1957742320 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<CancelOfferRequest>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCancelOfferRequestU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3788803752 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FinancialContentChanger/<CancelOfferRequest>c__IteratorD::MoveNext()
extern "C"  bool U3CCancelOfferRequestU3Ec__IteratorD_MoveNext_m2539413492 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<CancelOfferRequest>c__IteratorD::Dispose()
extern "C"  void U3CCancelOfferRequestU3Ec__IteratorD_Dispose_m2100079807 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<CancelOfferRequest>c__IteratorD::Reset()
extern "C"  void U3CCancelOfferRequestU3Ec__IteratorD_Reset_m3396138533 (U3CCancelOfferRequestU3Ec__IteratorD_t2972413101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
