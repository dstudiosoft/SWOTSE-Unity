﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.PingOptions
struct PingOptions_t1261881264;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.PingOptions::.ctor()
extern "C"  void PingOptions__ctor_m1440794514 (PingOptions_t1261881264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.PingOptions::.ctor(System.Int32,System.Boolean)
extern "C"  void PingOptions__ctor_m3631407070 (PingOptions_t1261881264 * __this, int32_t ___ttl0, bool ___dontFragment1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.PingOptions::get_DontFragment()
extern "C"  bool PingOptions_get_DontFragment_m1729750106 (PingOptions_t1261881264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.PingOptions::set_DontFragment(System.Boolean)
extern "C"  void PingOptions_set_DontFragment_m4221405291 (PingOptions_t1261881264 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.PingOptions::get_Ttl()
extern "C"  int32_t PingOptions_get_Ttl_m4280427503 (PingOptions_t1261881264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.PingOptions::set_Ttl(System.Int32)
extern "C"  void PingOptions_set_Ttl_m2643569608 (PingOptions_t1261881264 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
