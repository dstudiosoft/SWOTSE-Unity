﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// System.Collections.Generic.List`1<ArmyGeneralModel>
struct List_1_t4273847676;
// UnitsModel
struct UnitsModel_t1926818124;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DispatchManager
struct  DispatchManager_t870178121  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject DispatchManager::Page1
	GameObject_t1756533147 * ___Page1_2;
	// UnityEngine.GameObject DispatchManager::Page2
	GameObject_t1756533147 * ___Page2_3;
	// UnityEngine.UI.Text DispatchManager::cityFood
	Text_t356221433 * ___cityFood_4;
	// UnityEngine.UI.Text DispatchManager::cityWood
	Text_t356221433 * ___cityWood_5;
	// UnityEngine.UI.Text DispatchManager::cityIron
	Text_t356221433 * ___cityIron_6;
	// UnityEngine.UI.Text DispatchManager::cityStone
	Text_t356221433 * ___cityStone_7;
	// UnityEngine.UI.Text DispatchManager::cityCopper
	Text_t356221433 * ___cityCopper_8;
	// UnityEngine.UI.Text DispatchManager::citySilver
	Text_t356221433 * ___citySilver_9;
	// UnityEngine.UI.Text DispatchManager::cityGold
	Text_t356221433 * ___cityGold_10;
	// UnityEngine.UI.Text DispatchManager::cityWorker
	Text_t356221433 * ___cityWorker_11;
	// UnityEngine.UI.Text DispatchManager::citySpy
	Text_t356221433 * ___citySpy_12;
	// UnityEngine.UI.Text DispatchManager::citySwordsman
	Text_t356221433 * ___citySwordsman_13;
	// UnityEngine.UI.Text DispatchManager::citySpearman
	Text_t356221433 * ___citySpearman_14;
	// UnityEngine.UI.Text DispatchManager::cityPikeman
	Text_t356221433 * ___cityPikeman_15;
	// UnityEngine.UI.Text DispatchManager::cityScoutRider
	Text_t356221433 * ___cityScoutRider_16;
	// UnityEngine.UI.Text DispatchManager::cityLightCavalry
	Text_t356221433 * ___cityLightCavalry_17;
	// UnityEngine.UI.Text DispatchManager::cityHeavyCavalry
	Text_t356221433 * ___cityHeavyCavalry_18;
	// UnityEngine.UI.Text DispatchManager::cityArcher
	Text_t356221433 * ___cityArcher_19;
	// UnityEngine.UI.Text DispatchManager::cityArcherRider
	Text_t356221433 * ___cityArcherRider_20;
	// UnityEngine.UI.Text DispatchManager::cityHollyman
	Text_t356221433 * ___cityHollyman_21;
	// UnityEngine.UI.Text DispatchManager::cityWagon
	Text_t356221433 * ___cityWagon_22;
	// UnityEngine.UI.Text DispatchManager::cityTrebuchet
	Text_t356221433 * ___cityTrebuchet_23;
	// UnityEngine.UI.Text DispatchManager::citySiegeTower
	Text_t356221433 * ___citySiegeTower_24;
	// UnityEngine.UI.Text DispatchManager::cityBatteringRam
	Text_t356221433 * ___cityBatteringRam_25;
	// UnityEngine.UI.Text DispatchManager::cityBallista
	Text_t356221433 * ___cityBallista_26;
	// UnityEngine.UI.Text DispatchManager::cityWorkerName
	Text_t356221433 * ___cityWorkerName_27;
	// UnityEngine.UI.Text DispatchManager::citySpyName
	Text_t356221433 * ___citySpyName_28;
	// UnityEngine.UI.Text DispatchManager::citySwordsmanName
	Text_t356221433 * ___citySwordsmanName_29;
	// UnityEngine.UI.Text DispatchManager::citySpearmanName
	Text_t356221433 * ___citySpearmanName_30;
	// UnityEngine.UI.Text DispatchManager::cityPikemanName
	Text_t356221433 * ___cityPikemanName_31;
	// UnityEngine.UI.Text DispatchManager::cityScoutRiderName
	Text_t356221433 * ___cityScoutRiderName_32;
	// UnityEngine.UI.Text DispatchManager::cityLightCavalryName
	Text_t356221433 * ___cityLightCavalryName_33;
	// UnityEngine.UI.Text DispatchManager::cityHeavyCavalryName
	Text_t356221433 * ___cityHeavyCavalryName_34;
	// UnityEngine.UI.Text DispatchManager::cityArcherName
	Text_t356221433 * ___cityArcherName_35;
	// UnityEngine.UI.Text DispatchManager::cityArcherRiderName
	Text_t356221433 * ___cityArcherRiderName_36;
	// UnityEngine.UI.Text DispatchManager::cityHollymanName
	Text_t356221433 * ___cityHollymanName_37;
	// UnityEngine.UI.Text DispatchManager::cityWagonName
	Text_t356221433 * ___cityWagonName_38;
	// UnityEngine.UI.Text DispatchManager::cityTrebuchetName
	Text_t356221433 * ___cityTrebuchetName_39;
	// UnityEngine.UI.Text DispatchManager::citySiegeTowerName
	Text_t356221433 * ___citySiegeTowerName_40;
	// UnityEngine.UI.Text DispatchManager::cityBatteringRamName
	Text_t356221433 * ___cityBatteringRamName_41;
	// UnityEngine.UI.Text DispatchManager::cityBallistaName
	Text_t356221433 * ___cityBallistaName_42;
	// UnityEngine.UI.InputField DispatchManager::dispatchFood
	InputField_t1631627530 * ___dispatchFood_43;
	// UnityEngine.UI.InputField DispatchManager::dispatchWood
	InputField_t1631627530 * ___dispatchWood_44;
	// UnityEngine.UI.InputField DispatchManager::dispatchIron
	InputField_t1631627530 * ___dispatchIron_45;
	// UnityEngine.UI.InputField DispatchManager::dispatchStone
	InputField_t1631627530 * ___dispatchStone_46;
	// UnityEngine.UI.InputField DispatchManager::dispatchCopper
	InputField_t1631627530 * ___dispatchCopper_47;
	// UnityEngine.UI.InputField DispatchManager::dispatchSilver
	InputField_t1631627530 * ___dispatchSilver_48;
	// UnityEngine.UI.InputField DispatchManager::dispatchGold
	InputField_t1631627530 * ___dispatchGold_49;
	// UnityEngine.UI.InputField DispatchManager::dispatchWorker
	InputField_t1631627530 * ___dispatchWorker_50;
	// UnityEngine.UI.InputField DispatchManager::dispatchSpy
	InputField_t1631627530 * ___dispatchSpy_51;
	// UnityEngine.UI.InputField DispatchManager::dispatchSwordsman
	InputField_t1631627530 * ___dispatchSwordsman_52;
	// UnityEngine.UI.InputField DispatchManager::dispatchSpearman
	InputField_t1631627530 * ___dispatchSpearman_53;
	// UnityEngine.UI.InputField DispatchManager::dispatchPikeman
	InputField_t1631627530 * ___dispatchPikeman_54;
	// UnityEngine.UI.InputField DispatchManager::dispatchScoutRider
	InputField_t1631627530 * ___dispatchScoutRider_55;
	// UnityEngine.UI.InputField DispatchManager::dispatchLightCavalry
	InputField_t1631627530 * ___dispatchLightCavalry_56;
	// UnityEngine.UI.InputField DispatchManager::dispatchHeavyCavalry
	InputField_t1631627530 * ___dispatchHeavyCavalry_57;
	// UnityEngine.UI.InputField DispatchManager::dispatchArcher
	InputField_t1631627530 * ___dispatchArcher_58;
	// UnityEngine.UI.InputField DispatchManager::dispatchArcherRider
	InputField_t1631627530 * ___dispatchArcherRider_59;
	// UnityEngine.UI.InputField DispatchManager::dispatchHollyman
	InputField_t1631627530 * ___dispatchHollyman_60;
	// UnityEngine.UI.InputField DispatchManager::dispatchWagon
	InputField_t1631627530 * ___dispatchWagon_61;
	// UnityEngine.UI.InputField DispatchManager::dispatchTrebuchet
	InputField_t1631627530 * ___dispatchTrebuchet_62;
	// UnityEngine.UI.InputField DispatchManager::dispatchSiegeTower
	InputField_t1631627530 * ___dispatchSiegeTower_63;
	// UnityEngine.UI.InputField DispatchManager::dispatchBatteringRam
	InputField_t1631627530 * ___dispatchBatteringRam_64;
	// UnityEngine.UI.InputField DispatchManager::dispatchBallista
	InputField_t1631627530 * ___dispatchBallista_65;
	// UnityEngine.UI.Dropdown DispatchManager::attackType
	Dropdown_t1985816271 * ___attackType_66;
	// UnityEngine.UI.Dropdown DispatchManager::knights
	Dropdown_t1985816271 * ___knights_67;
	// UnityEngine.UI.InputField DispatchManager::dispatchHour
	InputField_t1631627530 * ___dispatchHour_68;
	// UnityEngine.UI.InputField DispatchManager::dispatchMinute
	InputField_t1631627530 * ___dispatchMinute_69;
	// UnityEngine.UI.Text DispatchManager::xCoord
	Text_t356221433 * ___xCoord_70;
	// UnityEngine.UI.Text DispatchManager::yCoord
	Text_t356221433 * ___yCoord_71;
	// UnityEngine.UI.InputField DispatchManager::xInput
	InputField_t1631627530 * ___xInput_72;
	// UnityEngine.UI.InputField DispatchManager::yInput
	InputField_t1631627530 * ___yInput_73;
	// UnityEngine.UI.InputField DispatchManager::foodPrice
	InputField_t1631627530 * ___foodPrice_74;
	// UnityEngine.UI.InputField DispatchManager::loadVacancy
	InputField_t1631627530 * ___loadVacancy_75;
	// UnityEngine.UI.InputField DispatchManager::tripTime
	InputField_t1631627530 * ___tripTime_76;
	// System.Boolean DispatchManager::fromMap
	bool ___fromMap_77;
	// WorldFieldModel DispatchManager::targetField
	WorldFieldModel_t3469935653 * ___targetField_78;
	// System.Collections.Generic.List`1<ArmyGeneralModel> DispatchManager::generals
	List_1_t4273847676 * ___generals_79;
	// UnitsModel DispatchManager::army
	UnitsModel_t1926818124 * ___army_80;
	// ResourcesModel DispatchManager::resources
	ResourcesModel_t2978985958 * ___resources_81;
	// System.Boolean DispatchManager::calculated
	bool ___calculated_82;
	// System.Collections.Generic.List`1<System.String> DispatchManager::attackTypes
	List_1_t1398341365 * ___attackTypes_83;
	// SimpleEvent DispatchManager::onGotParams
	SimpleEvent_t1509017202 * ___onGotParams_84;

public:
	inline static int32_t get_offset_of_Page1_2() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___Page1_2)); }
	inline GameObject_t1756533147 * get_Page1_2() const { return ___Page1_2; }
	inline GameObject_t1756533147 ** get_address_of_Page1_2() { return &___Page1_2; }
	inline void set_Page1_2(GameObject_t1756533147 * value)
	{
		___Page1_2 = value;
		Il2CppCodeGenWriteBarrier(&___Page1_2, value);
	}

	inline static int32_t get_offset_of_Page2_3() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___Page2_3)); }
	inline GameObject_t1756533147 * get_Page2_3() const { return ___Page2_3; }
	inline GameObject_t1756533147 ** get_address_of_Page2_3() { return &___Page2_3; }
	inline void set_Page2_3(GameObject_t1756533147 * value)
	{
		___Page2_3 = value;
		Il2CppCodeGenWriteBarrier(&___Page2_3, value);
	}

	inline static int32_t get_offset_of_cityFood_4() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityFood_4)); }
	inline Text_t356221433 * get_cityFood_4() const { return ___cityFood_4; }
	inline Text_t356221433 ** get_address_of_cityFood_4() { return &___cityFood_4; }
	inline void set_cityFood_4(Text_t356221433 * value)
	{
		___cityFood_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityFood_4, value);
	}

	inline static int32_t get_offset_of_cityWood_5() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityWood_5)); }
	inline Text_t356221433 * get_cityWood_5() const { return ___cityWood_5; }
	inline Text_t356221433 ** get_address_of_cityWood_5() { return &___cityWood_5; }
	inline void set_cityWood_5(Text_t356221433 * value)
	{
		___cityWood_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityWood_5, value);
	}

	inline static int32_t get_offset_of_cityIron_6() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityIron_6)); }
	inline Text_t356221433 * get_cityIron_6() const { return ___cityIron_6; }
	inline Text_t356221433 ** get_address_of_cityIron_6() { return &___cityIron_6; }
	inline void set_cityIron_6(Text_t356221433 * value)
	{
		___cityIron_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityIron_6, value);
	}

	inline static int32_t get_offset_of_cityStone_7() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityStone_7)); }
	inline Text_t356221433 * get_cityStone_7() const { return ___cityStone_7; }
	inline Text_t356221433 ** get_address_of_cityStone_7() { return &___cityStone_7; }
	inline void set_cityStone_7(Text_t356221433 * value)
	{
		___cityStone_7 = value;
		Il2CppCodeGenWriteBarrier(&___cityStone_7, value);
	}

	inline static int32_t get_offset_of_cityCopper_8() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityCopper_8)); }
	inline Text_t356221433 * get_cityCopper_8() const { return ___cityCopper_8; }
	inline Text_t356221433 ** get_address_of_cityCopper_8() { return &___cityCopper_8; }
	inline void set_cityCopper_8(Text_t356221433 * value)
	{
		___cityCopper_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityCopper_8, value);
	}

	inline static int32_t get_offset_of_citySilver_9() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySilver_9)); }
	inline Text_t356221433 * get_citySilver_9() const { return ___citySilver_9; }
	inline Text_t356221433 ** get_address_of_citySilver_9() { return &___citySilver_9; }
	inline void set_citySilver_9(Text_t356221433 * value)
	{
		___citySilver_9 = value;
		Il2CppCodeGenWriteBarrier(&___citySilver_9, value);
	}

	inline static int32_t get_offset_of_cityGold_10() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityGold_10)); }
	inline Text_t356221433 * get_cityGold_10() const { return ___cityGold_10; }
	inline Text_t356221433 ** get_address_of_cityGold_10() { return &___cityGold_10; }
	inline void set_cityGold_10(Text_t356221433 * value)
	{
		___cityGold_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityGold_10, value);
	}

	inline static int32_t get_offset_of_cityWorker_11() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityWorker_11)); }
	inline Text_t356221433 * get_cityWorker_11() const { return ___cityWorker_11; }
	inline Text_t356221433 ** get_address_of_cityWorker_11() { return &___cityWorker_11; }
	inline void set_cityWorker_11(Text_t356221433 * value)
	{
		___cityWorker_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorker_11, value);
	}

	inline static int32_t get_offset_of_citySpy_12() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySpy_12)); }
	inline Text_t356221433 * get_citySpy_12() const { return ___citySpy_12; }
	inline Text_t356221433 ** get_address_of_citySpy_12() { return &___citySpy_12; }
	inline void set_citySpy_12(Text_t356221433 * value)
	{
		___citySpy_12 = value;
		Il2CppCodeGenWriteBarrier(&___citySpy_12, value);
	}

	inline static int32_t get_offset_of_citySwordsman_13() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySwordsman_13)); }
	inline Text_t356221433 * get_citySwordsman_13() const { return ___citySwordsman_13; }
	inline Text_t356221433 ** get_address_of_citySwordsman_13() { return &___citySwordsman_13; }
	inline void set_citySwordsman_13(Text_t356221433 * value)
	{
		___citySwordsman_13 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsman_13, value);
	}

	inline static int32_t get_offset_of_citySpearman_14() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySpearman_14)); }
	inline Text_t356221433 * get_citySpearman_14() const { return ___citySpearman_14; }
	inline Text_t356221433 ** get_address_of_citySpearman_14() { return &___citySpearman_14; }
	inline void set_citySpearman_14(Text_t356221433 * value)
	{
		___citySpearman_14 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearman_14, value);
	}

	inline static int32_t get_offset_of_cityPikeman_15() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityPikeman_15)); }
	inline Text_t356221433 * get_cityPikeman_15() const { return ___cityPikeman_15; }
	inline Text_t356221433 ** get_address_of_cityPikeman_15() { return &___cityPikeman_15; }
	inline void set_cityPikeman_15(Text_t356221433 * value)
	{
		___cityPikeman_15 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikeman_15, value);
	}

	inline static int32_t get_offset_of_cityScoutRider_16() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityScoutRider_16)); }
	inline Text_t356221433 * get_cityScoutRider_16() const { return ___cityScoutRider_16; }
	inline Text_t356221433 ** get_address_of_cityScoutRider_16() { return &___cityScoutRider_16; }
	inline void set_cityScoutRider_16(Text_t356221433 * value)
	{
		___cityScoutRider_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRider_16, value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_17() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityLightCavalry_17)); }
	inline Text_t356221433 * get_cityLightCavalry_17() const { return ___cityLightCavalry_17; }
	inline Text_t356221433 ** get_address_of_cityLightCavalry_17() { return &___cityLightCavalry_17; }
	inline void set_cityLightCavalry_17(Text_t356221433 * value)
	{
		___cityLightCavalry_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalry_17, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_18() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityHeavyCavalry_18)); }
	inline Text_t356221433 * get_cityHeavyCavalry_18() const { return ___cityHeavyCavalry_18; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalry_18() { return &___cityHeavyCavalry_18; }
	inline void set_cityHeavyCavalry_18(Text_t356221433 * value)
	{
		___cityHeavyCavalry_18 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalry_18, value);
	}

	inline static int32_t get_offset_of_cityArcher_19() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityArcher_19)); }
	inline Text_t356221433 * get_cityArcher_19() const { return ___cityArcher_19; }
	inline Text_t356221433 ** get_address_of_cityArcher_19() { return &___cityArcher_19; }
	inline void set_cityArcher_19(Text_t356221433 * value)
	{
		___cityArcher_19 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcher_19, value);
	}

	inline static int32_t get_offset_of_cityArcherRider_20() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityArcherRider_20)); }
	inline Text_t356221433 * get_cityArcherRider_20() const { return ___cityArcherRider_20; }
	inline Text_t356221433 ** get_address_of_cityArcherRider_20() { return &___cityArcherRider_20; }
	inline void set_cityArcherRider_20(Text_t356221433 * value)
	{
		___cityArcherRider_20 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRider_20, value);
	}

	inline static int32_t get_offset_of_cityHollyman_21() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityHollyman_21)); }
	inline Text_t356221433 * get_cityHollyman_21() const { return ___cityHollyman_21; }
	inline Text_t356221433 ** get_address_of_cityHollyman_21() { return &___cityHollyman_21; }
	inline void set_cityHollyman_21(Text_t356221433 * value)
	{
		___cityHollyman_21 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollyman_21, value);
	}

	inline static int32_t get_offset_of_cityWagon_22() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityWagon_22)); }
	inline Text_t356221433 * get_cityWagon_22() const { return ___cityWagon_22; }
	inline Text_t356221433 ** get_address_of_cityWagon_22() { return &___cityWagon_22; }
	inline void set_cityWagon_22(Text_t356221433 * value)
	{
		___cityWagon_22 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagon_22, value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_23() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityTrebuchet_23)); }
	inline Text_t356221433 * get_cityTrebuchet_23() const { return ___cityTrebuchet_23; }
	inline Text_t356221433 ** get_address_of_cityTrebuchet_23() { return &___cityTrebuchet_23; }
	inline void set_cityTrebuchet_23(Text_t356221433 * value)
	{
		___cityTrebuchet_23 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchet_23, value);
	}

	inline static int32_t get_offset_of_citySiegeTower_24() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySiegeTower_24)); }
	inline Text_t356221433 * get_citySiegeTower_24() const { return ___citySiegeTower_24; }
	inline Text_t356221433 ** get_address_of_citySiegeTower_24() { return &___citySiegeTower_24; }
	inline void set_citySiegeTower_24(Text_t356221433 * value)
	{
		___citySiegeTower_24 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTower_24, value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_25() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityBatteringRam_25)); }
	inline Text_t356221433 * get_cityBatteringRam_25() const { return ___cityBatteringRam_25; }
	inline Text_t356221433 ** get_address_of_cityBatteringRam_25() { return &___cityBatteringRam_25; }
	inline void set_cityBatteringRam_25(Text_t356221433 * value)
	{
		___cityBatteringRam_25 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRam_25, value);
	}

	inline static int32_t get_offset_of_cityBallista_26() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityBallista_26)); }
	inline Text_t356221433 * get_cityBallista_26() const { return ___cityBallista_26; }
	inline Text_t356221433 ** get_address_of_cityBallista_26() { return &___cityBallista_26; }
	inline void set_cityBallista_26(Text_t356221433 * value)
	{
		___cityBallista_26 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallista_26, value);
	}

	inline static int32_t get_offset_of_cityWorkerName_27() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityWorkerName_27)); }
	inline Text_t356221433 * get_cityWorkerName_27() const { return ___cityWorkerName_27; }
	inline Text_t356221433 ** get_address_of_cityWorkerName_27() { return &___cityWorkerName_27; }
	inline void set_cityWorkerName_27(Text_t356221433 * value)
	{
		___cityWorkerName_27 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorkerName_27, value);
	}

	inline static int32_t get_offset_of_citySpyName_28() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySpyName_28)); }
	inline Text_t356221433 * get_citySpyName_28() const { return ___citySpyName_28; }
	inline Text_t356221433 ** get_address_of_citySpyName_28() { return &___citySpyName_28; }
	inline void set_citySpyName_28(Text_t356221433 * value)
	{
		___citySpyName_28 = value;
		Il2CppCodeGenWriteBarrier(&___citySpyName_28, value);
	}

	inline static int32_t get_offset_of_citySwordsmanName_29() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySwordsmanName_29)); }
	inline Text_t356221433 * get_citySwordsmanName_29() const { return ___citySwordsmanName_29; }
	inline Text_t356221433 ** get_address_of_citySwordsmanName_29() { return &___citySwordsmanName_29; }
	inline void set_citySwordsmanName_29(Text_t356221433 * value)
	{
		___citySwordsmanName_29 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsmanName_29, value);
	}

	inline static int32_t get_offset_of_citySpearmanName_30() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySpearmanName_30)); }
	inline Text_t356221433 * get_citySpearmanName_30() const { return ___citySpearmanName_30; }
	inline Text_t356221433 ** get_address_of_citySpearmanName_30() { return &___citySpearmanName_30; }
	inline void set_citySpearmanName_30(Text_t356221433 * value)
	{
		___citySpearmanName_30 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearmanName_30, value);
	}

	inline static int32_t get_offset_of_cityPikemanName_31() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityPikemanName_31)); }
	inline Text_t356221433 * get_cityPikemanName_31() const { return ___cityPikemanName_31; }
	inline Text_t356221433 ** get_address_of_cityPikemanName_31() { return &___cityPikemanName_31; }
	inline void set_cityPikemanName_31(Text_t356221433 * value)
	{
		___cityPikemanName_31 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikemanName_31, value);
	}

	inline static int32_t get_offset_of_cityScoutRiderName_32() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityScoutRiderName_32)); }
	inline Text_t356221433 * get_cityScoutRiderName_32() const { return ___cityScoutRiderName_32; }
	inline Text_t356221433 ** get_address_of_cityScoutRiderName_32() { return &___cityScoutRiderName_32; }
	inline void set_cityScoutRiderName_32(Text_t356221433 * value)
	{
		___cityScoutRiderName_32 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRiderName_32, value);
	}

	inline static int32_t get_offset_of_cityLightCavalryName_33() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityLightCavalryName_33)); }
	inline Text_t356221433 * get_cityLightCavalryName_33() const { return ___cityLightCavalryName_33; }
	inline Text_t356221433 ** get_address_of_cityLightCavalryName_33() { return &___cityLightCavalryName_33; }
	inline void set_cityLightCavalryName_33(Text_t356221433 * value)
	{
		___cityLightCavalryName_33 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalryName_33, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalryName_34() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityHeavyCavalryName_34)); }
	inline Text_t356221433 * get_cityHeavyCavalryName_34() const { return ___cityHeavyCavalryName_34; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalryName_34() { return &___cityHeavyCavalryName_34; }
	inline void set_cityHeavyCavalryName_34(Text_t356221433 * value)
	{
		___cityHeavyCavalryName_34 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalryName_34, value);
	}

	inline static int32_t get_offset_of_cityArcherName_35() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityArcherName_35)); }
	inline Text_t356221433 * get_cityArcherName_35() const { return ___cityArcherName_35; }
	inline Text_t356221433 ** get_address_of_cityArcherName_35() { return &___cityArcherName_35; }
	inline void set_cityArcherName_35(Text_t356221433 * value)
	{
		___cityArcherName_35 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherName_35, value);
	}

	inline static int32_t get_offset_of_cityArcherRiderName_36() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityArcherRiderName_36)); }
	inline Text_t356221433 * get_cityArcherRiderName_36() const { return ___cityArcherRiderName_36; }
	inline Text_t356221433 ** get_address_of_cityArcherRiderName_36() { return &___cityArcherRiderName_36; }
	inline void set_cityArcherRiderName_36(Text_t356221433 * value)
	{
		___cityArcherRiderName_36 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRiderName_36, value);
	}

	inline static int32_t get_offset_of_cityHollymanName_37() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityHollymanName_37)); }
	inline Text_t356221433 * get_cityHollymanName_37() const { return ___cityHollymanName_37; }
	inline Text_t356221433 ** get_address_of_cityHollymanName_37() { return &___cityHollymanName_37; }
	inline void set_cityHollymanName_37(Text_t356221433 * value)
	{
		___cityHollymanName_37 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollymanName_37, value);
	}

	inline static int32_t get_offset_of_cityWagonName_38() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityWagonName_38)); }
	inline Text_t356221433 * get_cityWagonName_38() const { return ___cityWagonName_38; }
	inline Text_t356221433 ** get_address_of_cityWagonName_38() { return &___cityWagonName_38; }
	inline void set_cityWagonName_38(Text_t356221433 * value)
	{
		___cityWagonName_38 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagonName_38, value);
	}

	inline static int32_t get_offset_of_cityTrebuchetName_39() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityTrebuchetName_39)); }
	inline Text_t356221433 * get_cityTrebuchetName_39() const { return ___cityTrebuchetName_39; }
	inline Text_t356221433 ** get_address_of_cityTrebuchetName_39() { return &___cityTrebuchetName_39; }
	inline void set_cityTrebuchetName_39(Text_t356221433 * value)
	{
		___cityTrebuchetName_39 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchetName_39, value);
	}

	inline static int32_t get_offset_of_citySiegeTowerName_40() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___citySiegeTowerName_40)); }
	inline Text_t356221433 * get_citySiegeTowerName_40() const { return ___citySiegeTowerName_40; }
	inline Text_t356221433 ** get_address_of_citySiegeTowerName_40() { return &___citySiegeTowerName_40; }
	inline void set_citySiegeTowerName_40(Text_t356221433 * value)
	{
		___citySiegeTowerName_40 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTowerName_40, value);
	}

	inline static int32_t get_offset_of_cityBatteringRamName_41() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityBatteringRamName_41)); }
	inline Text_t356221433 * get_cityBatteringRamName_41() const { return ___cityBatteringRamName_41; }
	inline Text_t356221433 ** get_address_of_cityBatteringRamName_41() { return &___cityBatteringRamName_41; }
	inline void set_cityBatteringRamName_41(Text_t356221433 * value)
	{
		___cityBatteringRamName_41 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRamName_41, value);
	}

	inline static int32_t get_offset_of_cityBallistaName_42() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___cityBallistaName_42)); }
	inline Text_t356221433 * get_cityBallistaName_42() const { return ___cityBallistaName_42; }
	inline Text_t356221433 ** get_address_of_cityBallistaName_42() { return &___cityBallistaName_42; }
	inline void set_cityBallistaName_42(Text_t356221433 * value)
	{
		___cityBallistaName_42 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallistaName_42, value);
	}

	inline static int32_t get_offset_of_dispatchFood_43() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchFood_43)); }
	inline InputField_t1631627530 * get_dispatchFood_43() const { return ___dispatchFood_43; }
	inline InputField_t1631627530 ** get_address_of_dispatchFood_43() { return &___dispatchFood_43; }
	inline void set_dispatchFood_43(InputField_t1631627530 * value)
	{
		___dispatchFood_43 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchFood_43, value);
	}

	inline static int32_t get_offset_of_dispatchWood_44() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchWood_44)); }
	inline InputField_t1631627530 * get_dispatchWood_44() const { return ___dispatchWood_44; }
	inline InputField_t1631627530 ** get_address_of_dispatchWood_44() { return &___dispatchWood_44; }
	inline void set_dispatchWood_44(InputField_t1631627530 * value)
	{
		___dispatchWood_44 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchWood_44, value);
	}

	inline static int32_t get_offset_of_dispatchIron_45() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchIron_45)); }
	inline InputField_t1631627530 * get_dispatchIron_45() const { return ___dispatchIron_45; }
	inline InputField_t1631627530 ** get_address_of_dispatchIron_45() { return &___dispatchIron_45; }
	inline void set_dispatchIron_45(InputField_t1631627530 * value)
	{
		___dispatchIron_45 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchIron_45, value);
	}

	inline static int32_t get_offset_of_dispatchStone_46() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchStone_46)); }
	inline InputField_t1631627530 * get_dispatchStone_46() const { return ___dispatchStone_46; }
	inline InputField_t1631627530 ** get_address_of_dispatchStone_46() { return &___dispatchStone_46; }
	inline void set_dispatchStone_46(InputField_t1631627530 * value)
	{
		___dispatchStone_46 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchStone_46, value);
	}

	inline static int32_t get_offset_of_dispatchCopper_47() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchCopper_47)); }
	inline InputField_t1631627530 * get_dispatchCopper_47() const { return ___dispatchCopper_47; }
	inline InputField_t1631627530 ** get_address_of_dispatchCopper_47() { return &___dispatchCopper_47; }
	inline void set_dispatchCopper_47(InputField_t1631627530 * value)
	{
		___dispatchCopper_47 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchCopper_47, value);
	}

	inline static int32_t get_offset_of_dispatchSilver_48() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchSilver_48)); }
	inline InputField_t1631627530 * get_dispatchSilver_48() const { return ___dispatchSilver_48; }
	inline InputField_t1631627530 ** get_address_of_dispatchSilver_48() { return &___dispatchSilver_48; }
	inline void set_dispatchSilver_48(InputField_t1631627530 * value)
	{
		___dispatchSilver_48 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSilver_48, value);
	}

	inline static int32_t get_offset_of_dispatchGold_49() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchGold_49)); }
	inline InputField_t1631627530 * get_dispatchGold_49() const { return ___dispatchGold_49; }
	inline InputField_t1631627530 ** get_address_of_dispatchGold_49() { return &___dispatchGold_49; }
	inline void set_dispatchGold_49(InputField_t1631627530 * value)
	{
		___dispatchGold_49 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchGold_49, value);
	}

	inline static int32_t get_offset_of_dispatchWorker_50() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchWorker_50)); }
	inline InputField_t1631627530 * get_dispatchWorker_50() const { return ___dispatchWorker_50; }
	inline InputField_t1631627530 ** get_address_of_dispatchWorker_50() { return &___dispatchWorker_50; }
	inline void set_dispatchWorker_50(InputField_t1631627530 * value)
	{
		___dispatchWorker_50 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchWorker_50, value);
	}

	inline static int32_t get_offset_of_dispatchSpy_51() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchSpy_51)); }
	inline InputField_t1631627530 * get_dispatchSpy_51() const { return ___dispatchSpy_51; }
	inline InputField_t1631627530 ** get_address_of_dispatchSpy_51() { return &___dispatchSpy_51; }
	inline void set_dispatchSpy_51(InputField_t1631627530 * value)
	{
		___dispatchSpy_51 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSpy_51, value);
	}

	inline static int32_t get_offset_of_dispatchSwordsman_52() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchSwordsman_52)); }
	inline InputField_t1631627530 * get_dispatchSwordsman_52() const { return ___dispatchSwordsman_52; }
	inline InputField_t1631627530 ** get_address_of_dispatchSwordsman_52() { return &___dispatchSwordsman_52; }
	inline void set_dispatchSwordsman_52(InputField_t1631627530 * value)
	{
		___dispatchSwordsman_52 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSwordsman_52, value);
	}

	inline static int32_t get_offset_of_dispatchSpearman_53() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchSpearman_53)); }
	inline InputField_t1631627530 * get_dispatchSpearman_53() const { return ___dispatchSpearman_53; }
	inline InputField_t1631627530 ** get_address_of_dispatchSpearman_53() { return &___dispatchSpearman_53; }
	inline void set_dispatchSpearman_53(InputField_t1631627530 * value)
	{
		___dispatchSpearman_53 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSpearman_53, value);
	}

	inline static int32_t get_offset_of_dispatchPikeman_54() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchPikeman_54)); }
	inline InputField_t1631627530 * get_dispatchPikeman_54() const { return ___dispatchPikeman_54; }
	inline InputField_t1631627530 ** get_address_of_dispatchPikeman_54() { return &___dispatchPikeman_54; }
	inline void set_dispatchPikeman_54(InputField_t1631627530 * value)
	{
		___dispatchPikeman_54 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchPikeman_54, value);
	}

	inline static int32_t get_offset_of_dispatchScoutRider_55() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchScoutRider_55)); }
	inline InputField_t1631627530 * get_dispatchScoutRider_55() const { return ___dispatchScoutRider_55; }
	inline InputField_t1631627530 ** get_address_of_dispatchScoutRider_55() { return &___dispatchScoutRider_55; }
	inline void set_dispatchScoutRider_55(InputField_t1631627530 * value)
	{
		___dispatchScoutRider_55 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchScoutRider_55, value);
	}

	inline static int32_t get_offset_of_dispatchLightCavalry_56() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchLightCavalry_56)); }
	inline InputField_t1631627530 * get_dispatchLightCavalry_56() const { return ___dispatchLightCavalry_56; }
	inline InputField_t1631627530 ** get_address_of_dispatchLightCavalry_56() { return &___dispatchLightCavalry_56; }
	inline void set_dispatchLightCavalry_56(InputField_t1631627530 * value)
	{
		___dispatchLightCavalry_56 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchLightCavalry_56, value);
	}

	inline static int32_t get_offset_of_dispatchHeavyCavalry_57() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchHeavyCavalry_57)); }
	inline InputField_t1631627530 * get_dispatchHeavyCavalry_57() const { return ___dispatchHeavyCavalry_57; }
	inline InputField_t1631627530 ** get_address_of_dispatchHeavyCavalry_57() { return &___dispatchHeavyCavalry_57; }
	inline void set_dispatchHeavyCavalry_57(InputField_t1631627530 * value)
	{
		___dispatchHeavyCavalry_57 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchHeavyCavalry_57, value);
	}

	inline static int32_t get_offset_of_dispatchArcher_58() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchArcher_58)); }
	inline InputField_t1631627530 * get_dispatchArcher_58() const { return ___dispatchArcher_58; }
	inline InputField_t1631627530 ** get_address_of_dispatchArcher_58() { return &___dispatchArcher_58; }
	inline void set_dispatchArcher_58(InputField_t1631627530 * value)
	{
		___dispatchArcher_58 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchArcher_58, value);
	}

	inline static int32_t get_offset_of_dispatchArcherRider_59() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchArcherRider_59)); }
	inline InputField_t1631627530 * get_dispatchArcherRider_59() const { return ___dispatchArcherRider_59; }
	inline InputField_t1631627530 ** get_address_of_dispatchArcherRider_59() { return &___dispatchArcherRider_59; }
	inline void set_dispatchArcherRider_59(InputField_t1631627530 * value)
	{
		___dispatchArcherRider_59 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchArcherRider_59, value);
	}

	inline static int32_t get_offset_of_dispatchHollyman_60() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchHollyman_60)); }
	inline InputField_t1631627530 * get_dispatchHollyman_60() const { return ___dispatchHollyman_60; }
	inline InputField_t1631627530 ** get_address_of_dispatchHollyman_60() { return &___dispatchHollyman_60; }
	inline void set_dispatchHollyman_60(InputField_t1631627530 * value)
	{
		___dispatchHollyman_60 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchHollyman_60, value);
	}

	inline static int32_t get_offset_of_dispatchWagon_61() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchWagon_61)); }
	inline InputField_t1631627530 * get_dispatchWagon_61() const { return ___dispatchWagon_61; }
	inline InputField_t1631627530 ** get_address_of_dispatchWagon_61() { return &___dispatchWagon_61; }
	inline void set_dispatchWagon_61(InputField_t1631627530 * value)
	{
		___dispatchWagon_61 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchWagon_61, value);
	}

	inline static int32_t get_offset_of_dispatchTrebuchet_62() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchTrebuchet_62)); }
	inline InputField_t1631627530 * get_dispatchTrebuchet_62() const { return ___dispatchTrebuchet_62; }
	inline InputField_t1631627530 ** get_address_of_dispatchTrebuchet_62() { return &___dispatchTrebuchet_62; }
	inline void set_dispatchTrebuchet_62(InputField_t1631627530 * value)
	{
		___dispatchTrebuchet_62 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchTrebuchet_62, value);
	}

	inline static int32_t get_offset_of_dispatchSiegeTower_63() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchSiegeTower_63)); }
	inline InputField_t1631627530 * get_dispatchSiegeTower_63() const { return ___dispatchSiegeTower_63; }
	inline InputField_t1631627530 ** get_address_of_dispatchSiegeTower_63() { return &___dispatchSiegeTower_63; }
	inline void set_dispatchSiegeTower_63(InputField_t1631627530 * value)
	{
		___dispatchSiegeTower_63 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSiegeTower_63, value);
	}

	inline static int32_t get_offset_of_dispatchBatteringRam_64() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchBatteringRam_64)); }
	inline InputField_t1631627530 * get_dispatchBatteringRam_64() const { return ___dispatchBatteringRam_64; }
	inline InputField_t1631627530 ** get_address_of_dispatchBatteringRam_64() { return &___dispatchBatteringRam_64; }
	inline void set_dispatchBatteringRam_64(InputField_t1631627530 * value)
	{
		___dispatchBatteringRam_64 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchBatteringRam_64, value);
	}

	inline static int32_t get_offset_of_dispatchBallista_65() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchBallista_65)); }
	inline InputField_t1631627530 * get_dispatchBallista_65() const { return ___dispatchBallista_65; }
	inline InputField_t1631627530 ** get_address_of_dispatchBallista_65() { return &___dispatchBallista_65; }
	inline void set_dispatchBallista_65(InputField_t1631627530 * value)
	{
		___dispatchBallista_65 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchBallista_65, value);
	}

	inline static int32_t get_offset_of_attackType_66() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___attackType_66)); }
	inline Dropdown_t1985816271 * get_attackType_66() const { return ___attackType_66; }
	inline Dropdown_t1985816271 ** get_address_of_attackType_66() { return &___attackType_66; }
	inline void set_attackType_66(Dropdown_t1985816271 * value)
	{
		___attackType_66 = value;
		Il2CppCodeGenWriteBarrier(&___attackType_66, value);
	}

	inline static int32_t get_offset_of_knights_67() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___knights_67)); }
	inline Dropdown_t1985816271 * get_knights_67() const { return ___knights_67; }
	inline Dropdown_t1985816271 ** get_address_of_knights_67() { return &___knights_67; }
	inline void set_knights_67(Dropdown_t1985816271 * value)
	{
		___knights_67 = value;
		Il2CppCodeGenWriteBarrier(&___knights_67, value);
	}

	inline static int32_t get_offset_of_dispatchHour_68() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchHour_68)); }
	inline InputField_t1631627530 * get_dispatchHour_68() const { return ___dispatchHour_68; }
	inline InputField_t1631627530 ** get_address_of_dispatchHour_68() { return &___dispatchHour_68; }
	inline void set_dispatchHour_68(InputField_t1631627530 * value)
	{
		___dispatchHour_68 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchHour_68, value);
	}

	inline static int32_t get_offset_of_dispatchMinute_69() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___dispatchMinute_69)); }
	inline InputField_t1631627530 * get_dispatchMinute_69() const { return ___dispatchMinute_69; }
	inline InputField_t1631627530 ** get_address_of_dispatchMinute_69() { return &___dispatchMinute_69; }
	inline void set_dispatchMinute_69(InputField_t1631627530 * value)
	{
		___dispatchMinute_69 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchMinute_69, value);
	}

	inline static int32_t get_offset_of_xCoord_70() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___xCoord_70)); }
	inline Text_t356221433 * get_xCoord_70() const { return ___xCoord_70; }
	inline Text_t356221433 ** get_address_of_xCoord_70() { return &___xCoord_70; }
	inline void set_xCoord_70(Text_t356221433 * value)
	{
		___xCoord_70 = value;
		Il2CppCodeGenWriteBarrier(&___xCoord_70, value);
	}

	inline static int32_t get_offset_of_yCoord_71() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___yCoord_71)); }
	inline Text_t356221433 * get_yCoord_71() const { return ___yCoord_71; }
	inline Text_t356221433 ** get_address_of_yCoord_71() { return &___yCoord_71; }
	inline void set_yCoord_71(Text_t356221433 * value)
	{
		___yCoord_71 = value;
		Il2CppCodeGenWriteBarrier(&___yCoord_71, value);
	}

	inline static int32_t get_offset_of_xInput_72() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___xInput_72)); }
	inline InputField_t1631627530 * get_xInput_72() const { return ___xInput_72; }
	inline InputField_t1631627530 ** get_address_of_xInput_72() { return &___xInput_72; }
	inline void set_xInput_72(InputField_t1631627530 * value)
	{
		___xInput_72 = value;
		Il2CppCodeGenWriteBarrier(&___xInput_72, value);
	}

	inline static int32_t get_offset_of_yInput_73() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___yInput_73)); }
	inline InputField_t1631627530 * get_yInput_73() const { return ___yInput_73; }
	inline InputField_t1631627530 ** get_address_of_yInput_73() { return &___yInput_73; }
	inline void set_yInput_73(InputField_t1631627530 * value)
	{
		___yInput_73 = value;
		Il2CppCodeGenWriteBarrier(&___yInput_73, value);
	}

	inline static int32_t get_offset_of_foodPrice_74() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___foodPrice_74)); }
	inline InputField_t1631627530 * get_foodPrice_74() const { return ___foodPrice_74; }
	inline InputField_t1631627530 ** get_address_of_foodPrice_74() { return &___foodPrice_74; }
	inline void set_foodPrice_74(InputField_t1631627530 * value)
	{
		___foodPrice_74 = value;
		Il2CppCodeGenWriteBarrier(&___foodPrice_74, value);
	}

	inline static int32_t get_offset_of_loadVacancy_75() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___loadVacancy_75)); }
	inline InputField_t1631627530 * get_loadVacancy_75() const { return ___loadVacancy_75; }
	inline InputField_t1631627530 ** get_address_of_loadVacancy_75() { return &___loadVacancy_75; }
	inline void set_loadVacancy_75(InputField_t1631627530 * value)
	{
		___loadVacancy_75 = value;
		Il2CppCodeGenWriteBarrier(&___loadVacancy_75, value);
	}

	inline static int32_t get_offset_of_tripTime_76() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___tripTime_76)); }
	inline InputField_t1631627530 * get_tripTime_76() const { return ___tripTime_76; }
	inline InputField_t1631627530 ** get_address_of_tripTime_76() { return &___tripTime_76; }
	inline void set_tripTime_76(InputField_t1631627530 * value)
	{
		___tripTime_76 = value;
		Il2CppCodeGenWriteBarrier(&___tripTime_76, value);
	}

	inline static int32_t get_offset_of_fromMap_77() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___fromMap_77)); }
	inline bool get_fromMap_77() const { return ___fromMap_77; }
	inline bool* get_address_of_fromMap_77() { return &___fromMap_77; }
	inline void set_fromMap_77(bool value)
	{
		___fromMap_77 = value;
	}

	inline static int32_t get_offset_of_targetField_78() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___targetField_78)); }
	inline WorldFieldModel_t3469935653 * get_targetField_78() const { return ___targetField_78; }
	inline WorldFieldModel_t3469935653 ** get_address_of_targetField_78() { return &___targetField_78; }
	inline void set_targetField_78(WorldFieldModel_t3469935653 * value)
	{
		___targetField_78 = value;
		Il2CppCodeGenWriteBarrier(&___targetField_78, value);
	}

	inline static int32_t get_offset_of_generals_79() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___generals_79)); }
	inline List_1_t4273847676 * get_generals_79() const { return ___generals_79; }
	inline List_1_t4273847676 ** get_address_of_generals_79() { return &___generals_79; }
	inline void set_generals_79(List_1_t4273847676 * value)
	{
		___generals_79 = value;
		Il2CppCodeGenWriteBarrier(&___generals_79, value);
	}

	inline static int32_t get_offset_of_army_80() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___army_80)); }
	inline UnitsModel_t1926818124 * get_army_80() const { return ___army_80; }
	inline UnitsModel_t1926818124 ** get_address_of_army_80() { return &___army_80; }
	inline void set_army_80(UnitsModel_t1926818124 * value)
	{
		___army_80 = value;
		Il2CppCodeGenWriteBarrier(&___army_80, value);
	}

	inline static int32_t get_offset_of_resources_81() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___resources_81)); }
	inline ResourcesModel_t2978985958 * get_resources_81() const { return ___resources_81; }
	inline ResourcesModel_t2978985958 ** get_address_of_resources_81() { return &___resources_81; }
	inline void set_resources_81(ResourcesModel_t2978985958 * value)
	{
		___resources_81 = value;
		Il2CppCodeGenWriteBarrier(&___resources_81, value);
	}

	inline static int32_t get_offset_of_calculated_82() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___calculated_82)); }
	inline bool get_calculated_82() const { return ___calculated_82; }
	inline bool* get_address_of_calculated_82() { return &___calculated_82; }
	inline void set_calculated_82(bool value)
	{
		___calculated_82 = value;
	}

	inline static int32_t get_offset_of_attackTypes_83() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___attackTypes_83)); }
	inline List_1_t1398341365 * get_attackTypes_83() const { return ___attackTypes_83; }
	inline List_1_t1398341365 ** get_address_of_attackTypes_83() { return &___attackTypes_83; }
	inline void set_attackTypes_83(List_1_t1398341365 * value)
	{
		___attackTypes_83 = value;
		Il2CppCodeGenWriteBarrier(&___attackTypes_83, value);
	}

	inline static int32_t get_offset_of_onGotParams_84() { return static_cast<int32_t>(offsetof(DispatchManager_t870178121, ___onGotParams_84)); }
	inline SimpleEvent_t1509017202 * get_onGotParams_84() const { return ___onGotParams_84; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGotParams_84() { return &___onGotParams_84; }
	inline void set_onGotParams_84(SimpleEvent_t1509017202 * value)
	{
		___onGotParams_84 = value;
		Il2CppCodeGenWriteBarrier(&___onGotParams_84, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
