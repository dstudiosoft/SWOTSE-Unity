﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitConstantModel
struct UnitConstantModel_t1807269633;

#include "codegen/il2cpp-codegen.h"

// System.Void UnitConstantModel::.ctor()
extern "C"  void UnitConstantModel__ctor_m3668609942 (UnitConstantModel_t1807269633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
