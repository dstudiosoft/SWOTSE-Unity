﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.VariantList/VariantListEnumerator
struct VariantListEnumerator_t3934991014;
// Firebase.VariantList
struct VariantList_t2408673687;
// Firebase.Variant
struct Variant_t4275788079;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_VariantList2408673687.h"

// System.Void Firebase.VariantList/VariantListEnumerator::.ctor(Firebase.VariantList)
extern "C"  void VariantListEnumerator__ctor_m2502458339 (VariantListEnumerator_t3934991014 * __this, VariantList_t2408673687 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantList/VariantListEnumerator::get_Current()
extern "C"  Variant_t4275788079 * VariantListEnumerator_get_Current_m491963112 (VariantListEnumerator_t3934991014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.VariantList/VariantListEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * VariantListEnumerator_System_Collections_IEnumerator_get_Current_m2200325183 (VariantListEnumerator_t3934991014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantList/VariantListEnumerator::MoveNext()
extern "C"  bool VariantListEnumerator_MoveNext_m3016580503 (VariantListEnumerator_t3934991014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList/VariantListEnumerator::Reset()
extern "C"  void VariantListEnumerator_Reset_m2194997970 (VariantListEnumerator_t3934991014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList/VariantListEnumerator::Dispose()
extern "C"  void VariantListEnumerator_Dispose_m2463558100 (VariantListEnumerator_t3934991014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
