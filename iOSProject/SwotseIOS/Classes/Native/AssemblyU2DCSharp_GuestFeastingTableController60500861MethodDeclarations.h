﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GuestFeastingTableController
struct GuestFeastingTableController_t60500861;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "AssemblyU2DCSharp_ArmyGeneralModel609759248.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GuestFeastingTableController::.ctor()
extern "C"  void GuestFeastingTableController__ctor_m391450986 (GuestFeastingTableController_t60500861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::add_confirmed(ConfirmationEvent)
extern "C"  void GuestFeastingTableController_add_confirmed_m1637918498 (GuestFeastingTableController_t60500861 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::remove_confirmed(ConfirmationEvent)
extern "C"  void GuestFeastingTableController_remove_confirmed_m1559631133 (GuestFeastingTableController_t60500861 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::Start()
extern "C"  void GuestFeastingTableController_Start_m1767532170 (GuestFeastingTableController_t60500861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GuestFeastingTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t GuestFeastingTableController_GetNumberOfRowsForTableView_m1993903172 (GuestFeastingTableController_t60500861 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GuestFeastingTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float GuestFeastingTableController_GetHeightForRowInTableView_m3493304732 (GuestFeastingTableController_t60500861 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell GuestFeastingTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * GuestFeastingTableController_GetCellForRowInTableView_m1328655809 (GuestFeastingTableController_t60500861 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::PerformAction(ArmyGeneralModel)
extern "C"  void GuestFeastingTableController_PerformAction_m2332948453 (GuestFeastingTableController_t60500861 * __this, ArmyGeneralModel_t609759248 * ___general0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::DismissConfrirmed(System.Boolean)
extern "C"  void GuestFeastingTableController_DismissConfrirmed_m3513498172 (GuestFeastingTableController_t60500861 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuestFeastingTableController::ActionPerformed(System.Object,System.String)
extern "C"  void GuestFeastingTableController_ActionPerformed_m1244311556 (GuestFeastingTableController_t60500861 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GuestFeastingTableController::GetAllAvailableGenerals()
extern "C"  Il2CppObject * GuestFeastingTableController_GetAllAvailableGenerals_m3530133317 (GuestFeastingTableController_t60500861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
