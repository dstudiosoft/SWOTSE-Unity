﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResourcesModel
struct ResourcesModel_t2978985958;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_t4136494783  : public MonoBehaviour_t1158329972
{
public:
	// ResourcesModel ResourceManager::cityResources
	ResourcesModel_t2978985958 * ___cityResources_2;
	// UnityEngine.Coroutine ResourceManager::resourceCoroutine
	Coroutine_t2299508840 * ___resourceCoroutine_3;
	// System.Boolean ResourceManager::resourcesWorking
	bool ___resourcesWorking_4;
	// System.Boolean ResourceManager::buildingEvents
	bool ___buildingEvents_5;
	// System.Boolean ResourceManager::unitEvents
	bool ___unitEvents_6;
	// System.Boolean ResourceManager::armyEvents
	bool ___armyEvents_7;
	// System.Boolean ResourceManager::itemEvents
	bool ___itemEvents_8;
	// System.Boolean ResourceManager::worldMapEvents
	bool ___worldMapEvents_9;
	// System.Boolean ResourceManager::newCityEvents
	bool ___newCityEvents_10;
	// System.Boolean ResourceManager::buildingTimers
	bool ___buildingTimers_11;
	// System.Boolean ResourceManager::unitTimers
	bool ___unitTimers_12;
	// System.Boolean ResourceManager::armyTimers
	bool ___armyTimers_13;
	// System.Boolean ResourceManager::itemTimers
	bool ___itemTimers_14;
	// System.Boolean ResourceManager::worldMapTimers
	bool ___worldMapTimers_15;
	// System.Boolean ResourceManager::newCityTimers
	bool ___newCityTimers_16;
	// System.String ResourceManager::dateFormat
	String_t* ___dateFormat_17;
	// SimpleEvent ResourceManager::onGetInitialResources
	SimpleEvent_t1509017202 * ___onGetInitialResources_18;

public:
	inline static int32_t get_offset_of_cityResources_2() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___cityResources_2)); }
	inline ResourcesModel_t2978985958 * get_cityResources_2() const { return ___cityResources_2; }
	inline ResourcesModel_t2978985958 ** get_address_of_cityResources_2() { return &___cityResources_2; }
	inline void set_cityResources_2(ResourcesModel_t2978985958 * value)
	{
		___cityResources_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityResources_2, value);
	}

	inline static int32_t get_offset_of_resourceCoroutine_3() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___resourceCoroutine_3)); }
	inline Coroutine_t2299508840 * get_resourceCoroutine_3() const { return ___resourceCoroutine_3; }
	inline Coroutine_t2299508840 ** get_address_of_resourceCoroutine_3() { return &___resourceCoroutine_3; }
	inline void set_resourceCoroutine_3(Coroutine_t2299508840 * value)
	{
		___resourceCoroutine_3 = value;
		Il2CppCodeGenWriteBarrier(&___resourceCoroutine_3, value);
	}

	inline static int32_t get_offset_of_resourcesWorking_4() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___resourcesWorking_4)); }
	inline bool get_resourcesWorking_4() const { return ___resourcesWorking_4; }
	inline bool* get_address_of_resourcesWorking_4() { return &___resourcesWorking_4; }
	inline void set_resourcesWorking_4(bool value)
	{
		___resourcesWorking_4 = value;
	}

	inline static int32_t get_offset_of_buildingEvents_5() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___buildingEvents_5)); }
	inline bool get_buildingEvents_5() const { return ___buildingEvents_5; }
	inline bool* get_address_of_buildingEvents_5() { return &___buildingEvents_5; }
	inline void set_buildingEvents_5(bool value)
	{
		___buildingEvents_5 = value;
	}

	inline static int32_t get_offset_of_unitEvents_6() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___unitEvents_6)); }
	inline bool get_unitEvents_6() const { return ___unitEvents_6; }
	inline bool* get_address_of_unitEvents_6() { return &___unitEvents_6; }
	inline void set_unitEvents_6(bool value)
	{
		___unitEvents_6 = value;
	}

	inline static int32_t get_offset_of_armyEvents_7() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___armyEvents_7)); }
	inline bool get_armyEvents_7() const { return ___armyEvents_7; }
	inline bool* get_address_of_armyEvents_7() { return &___armyEvents_7; }
	inline void set_armyEvents_7(bool value)
	{
		___armyEvents_7 = value;
	}

	inline static int32_t get_offset_of_itemEvents_8() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___itemEvents_8)); }
	inline bool get_itemEvents_8() const { return ___itemEvents_8; }
	inline bool* get_address_of_itemEvents_8() { return &___itemEvents_8; }
	inline void set_itemEvents_8(bool value)
	{
		___itemEvents_8 = value;
	}

	inline static int32_t get_offset_of_worldMapEvents_9() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___worldMapEvents_9)); }
	inline bool get_worldMapEvents_9() const { return ___worldMapEvents_9; }
	inline bool* get_address_of_worldMapEvents_9() { return &___worldMapEvents_9; }
	inline void set_worldMapEvents_9(bool value)
	{
		___worldMapEvents_9 = value;
	}

	inline static int32_t get_offset_of_newCityEvents_10() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___newCityEvents_10)); }
	inline bool get_newCityEvents_10() const { return ___newCityEvents_10; }
	inline bool* get_address_of_newCityEvents_10() { return &___newCityEvents_10; }
	inline void set_newCityEvents_10(bool value)
	{
		___newCityEvents_10 = value;
	}

	inline static int32_t get_offset_of_buildingTimers_11() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___buildingTimers_11)); }
	inline bool get_buildingTimers_11() const { return ___buildingTimers_11; }
	inline bool* get_address_of_buildingTimers_11() { return &___buildingTimers_11; }
	inline void set_buildingTimers_11(bool value)
	{
		___buildingTimers_11 = value;
	}

	inline static int32_t get_offset_of_unitTimers_12() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___unitTimers_12)); }
	inline bool get_unitTimers_12() const { return ___unitTimers_12; }
	inline bool* get_address_of_unitTimers_12() { return &___unitTimers_12; }
	inline void set_unitTimers_12(bool value)
	{
		___unitTimers_12 = value;
	}

	inline static int32_t get_offset_of_armyTimers_13() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___armyTimers_13)); }
	inline bool get_armyTimers_13() const { return ___armyTimers_13; }
	inline bool* get_address_of_armyTimers_13() { return &___armyTimers_13; }
	inline void set_armyTimers_13(bool value)
	{
		___armyTimers_13 = value;
	}

	inline static int32_t get_offset_of_itemTimers_14() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___itemTimers_14)); }
	inline bool get_itemTimers_14() const { return ___itemTimers_14; }
	inline bool* get_address_of_itemTimers_14() { return &___itemTimers_14; }
	inline void set_itemTimers_14(bool value)
	{
		___itemTimers_14 = value;
	}

	inline static int32_t get_offset_of_worldMapTimers_15() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___worldMapTimers_15)); }
	inline bool get_worldMapTimers_15() const { return ___worldMapTimers_15; }
	inline bool* get_address_of_worldMapTimers_15() { return &___worldMapTimers_15; }
	inline void set_worldMapTimers_15(bool value)
	{
		___worldMapTimers_15 = value;
	}

	inline static int32_t get_offset_of_newCityTimers_16() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___newCityTimers_16)); }
	inline bool get_newCityTimers_16() const { return ___newCityTimers_16; }
	inline bool* get_address_of_newCityTimers_16() { return &___newCityTimers_16; }
	inline void set_newCityTimers_16(bool value)
	{
		___newCityTimers_16 = value;
	}

	inline static int32_t get_offset_of_dateFormat_17() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___dateFormat_17)); }
	inline String_t* get_dateFormat_17() const { return ___dateFormat_17; }
	inline String_t** get_address_of_dateFormat_17() { return &___dateFormat_17; }
	inline void set_dateFormat_17(String_t* value)
	{
		___dateFormat_17 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_17, value);
	}

	inline static int32_t get_offset_of_onGetInitialResources_18() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___onGetInitialResources_18)); }
	inline SimpleEvent_t1509017202 * get_onGetInitialResources_18() const { return ___onGetInitialResources_18; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetInitialResources_18() { return &___onGetInitialResources_18; }
	inline void set_onGetInitialResources_18(SimpleEvent_t1509017202 * value)
	{
		___onGetInitialResources_18 = value;
		Il2CppCodeGenWriteBarrier(&___onGetInitialResources_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
