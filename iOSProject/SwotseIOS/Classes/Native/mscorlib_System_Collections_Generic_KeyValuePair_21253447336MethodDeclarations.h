﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3107318717(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1253447336 *, String_t*, LinkedList_1_t1581322852 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_Key()
#define KeyValuePair_2_get_Key_m3196295255(__this, method) ((  String_t* (*) (KeyValuePair_2_t1253447336 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1178004072(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1253447336 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_Value()
#define KeyValuePair_2_get_Value_m96529783(__this, method) ((  LinkedList_1_t1581322852 * (*) (KeyValuePair_2_t1253447336 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3671750296(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1253447336 *, LinkedList_1_t1581322852 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::ToString()
#define KeyValuePair_2_ToString_m1237312664(__this, method) ((  String_t* (*) (KeyValuePair_2_t1253447336 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
