﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.TouchEventArgs
struct TouchEventArgs_t1917927166;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.TouchEventArgs::.ctor()
extern "C"  void TouchEventArgs__ctor_m2633888109 (TouchEventArgs_t1917927166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint> TouchScript.TouchEventArgs::get_Touches()
extern "C"  Il2CppObject* TouchEventArgs_get_Touches_m2842359318 (TouchEventArgs_t1917927166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchEventArgs::set_Touches(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void TouchEventArgs_set_Touches_m3629754889 (TouchEventArgs_t1917927166 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchEventArgs TouchScript.TouchEventArgs::GetCachedEventArgs(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  TouchEventArgs_t1917927166 * TouchEventArgs_GetCachedEventArgs_m4037342651 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
