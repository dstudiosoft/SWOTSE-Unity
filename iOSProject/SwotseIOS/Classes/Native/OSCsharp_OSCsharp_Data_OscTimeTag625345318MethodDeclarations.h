﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Data.OscTimeTag
struct OscTimeTag_t625345318;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "OSCsharp_OSCsharp_Data_OscTimeTag625345318.h"
#include "mscorlib_System_Object2689449295.h"

// System.DateTime OSCsharp.Data.OscTimeTag::get_DateTime()
extern "C"  DateTime_t693205669  OscTimeTag_get_DateTime_m2172308342 (OscTimeTag_t625345318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.DateTime)
extern "C"  void OscTimeTag__ctor_m1655495837 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.Byte[])
extern "C"  void OscTimeTag__ctor_m183269774 (OscTimeTag_t625345318 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_Equality(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_Equality_m3410370307 (Il2CppObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_LessThan(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_LessThan_m1978965833 (Il2CppObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::IsValidTime(System.DateTime)
extern "C"  bool OscTimeTag_IsValidTime_m2988141586 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___timeStamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::Set(System.DateTime)
extern "C"  void OscTimeTag_Set_m4065929879 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::Equals(System.Object)
extern "C"  bool OscTimeTag_Equals_m3221546906 (OscTimeTag_t625345318 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OSCsharp.Data.OscTimeTag::GetHashCode()
extern "C"  int32_t OscTimeTag_GetHashCode_m1237726830 (OscTimeTag_t625345318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OSCsharp.Data.OscTimeTag::ToString()
extern "C"  String_t* OscTimeTag_ToString_m3425168866 (OscTimeTag_t625345318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::.cctor()
extern "C"  void OscTimeTag__cctor_m3227110810 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
