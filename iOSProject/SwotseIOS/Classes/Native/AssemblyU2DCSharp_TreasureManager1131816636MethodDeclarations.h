﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TreasureManager
struct TreasureManager_t1131816636;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TreasureManager::.ctor()
extern "C"  void TreasureManager__ctor_m1226084847 (TreasureManager_t1131816636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::add_onGetItems(SimpleEvent)
extern "C"  void TreasureManager_add_onGetItems_m490624534 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::remove_onGetItems(SimpleEvent)
extern "C"  void TreasureManager_remove_onGetItems_m4074057113 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::add_onItemBought(SimpleEvent)
extern "C"  void TreasureManager_add_onItemBought_m4114664602 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::remove_onItemBought(SimpleEvent)
extern "C"  void TreasureManager_remove_onItemBought_m1773038655 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::add_onItemSold(SimpleEvent)
extern "C"  void TreasureManager_add_onItemSold_m3457770763 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::remove_onItemSold(SimpleEvent)
extern "C"  void TreasureManager_remove_onItemSold_m3550453610 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::add_onItemGifted(SimpleEvent)
extern "C"  void TreasureManager_add_onItemGifted_m838873958 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::remove_onItemGifted(SimpleEvent)
extern "C"  void TreasureManager_remove_onItemGifted_m1359907285 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::add_onItemUsed(SimpleEvent)
extern "C"  void TreasureManager_add_onItemUsed_m1081448444 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TreasureManager::remove_onItemUsed(SimpleEvent)
extern "C"  void TreasureManager_remove_onItemUsed_m3557957199 (TreasureManager_t1131816636 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::GetCityTreasures(System.Boolean)
extern "C"  Il2CppObject * TreasureManager_GetCityTreasures_m3355538475 (TreasureManager_t1131816636 * __this, bool ___initial0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseItem(System.String)
extern "C"  Il2CppObject * TreasureManager_UseItem_m3107964797 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseUpgrade(System.Int64,System.Int64,System.Boolean)
extern "C"  Il2CppObject * TreasureManager_UseUpgrade_m4286394645 (TreasureManager_t1131816636 * __this, int64_t ___itemId0, int64_t ___building_id1, bool ___valley2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseTeleport(System.String,System.Int64,System.Int64)
extern "C"  Il2CppObject * TreasureManager_UseTeleport_m2696704201 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___x1, int64_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseShieldHour(System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_UseShieldHour_m648674569 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___hour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseShieldDay(System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_UseShieldDay_m1140047337 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___day1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::UseSpeedUp(System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_UseSpeedUp_m3900616404 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___eventId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::BuyItem(System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_BuyItem_m7725136 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::BuyPlayerItem(System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_BuyPlayerItem_m2616588297 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::SellItem(System.String,System.Int64,System.Int64)
extern "C"  Il2CppObject * TreasureManager_SellItem_m3998282214 (TreasureManager_t1131816636 * __this, String_t* ___itemId0, int64_t ___count1, int64_t ___discount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TreasureManager::GiftItem(System.Int64,System.String,System.Int64)
extern "C"  Il2CppObject * TreasureManager_GiftItem_m2009203908 (TreasureManager_t1131816636 * __this, int64_t ___cityID0, String_t* ___itemId1, int64_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
