﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "ArabicSupport_U3CPrivateImplementationDetailsU3EU73819911766.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}
struct  U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t619391775  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t619391775_StaticFields
{
public:
	// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18 <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}::$$method0x6000009-1
	__StaticArrayInitTypeSizeU3D18_t3819911766  ___U24U24method0x6000009U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000009U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t619391775_StaticFields, ___U24U24method0x6000009U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D18_t3819911766  get_U24U24method0x6000009U2D1_0() const { return ___U24U24method0x6000009U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D18_t3819911766 * get_address_of_U24U24method0x6000009U2D1_0() { return &___U24U24method0x6000009U2D1_0; }
	inline void set_U24U24method0x6000009U2D1_0(__StaticArrayInitTypeSizeU3D18_t3819911766  value)
	{
		___U24U24method0x6000009U2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
