﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// JSONObject
struct JSONObject_t1971882247;
// System.Object
struct Il2CppObject;
// MarketManager
struct MarketManager_t2047363881;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager/<BuyLots>c__Iterator12
struct  U3CBuyLotsU3Ec__Iterator12_t1560696684  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm MarketManager/<BuyLots>c__Iterator12::<buyForm>__0
	WWWForm_t3950226929 * ___U3CbuyFormU3E__0_0;
	// System.String MarketManager/<BuyLots>c__Iterator12::lotsDict
	String_t* ___lotsDict_1;
	// UnityEngine.Networking.UnityWebRequest MarketManager/<BuyLots>c__Iterator12::<request>__1
	UnityWebRequest_t254341728 * ___U3CrequestU3E__1_2;
	// JSONObject MarketManager/<BuyLots>c__Iterator12::<obj>__2
	JSONObject_t1971882247 * ___U3CobjU3E__2_3;
	// System.Int32 MarketManager/<BuyLots>c__Iterator12::$PC
	int32_t ___U24PC_4;
	// System.Object MarketManager/<BuyLots>c__Iterator12::$current
	Il2CppObject * ___U24current_5;
	// System.String MarketManager/<BuyLots>c__Iterator12::<$>lotsDict
	String_t* ___U3CU24U3ElotsDict_6;
	// MarketManager MarketManager/<BuyLots>c__Iterator12::<>f__this
	MarketManager_t2047363881 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CbuyFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U3CbuyFormU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CbuyFormU3E__0_0() const { return ___U3CbuyFormU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CbuyFormU3E__0_0() { return &___U3CbuyFormU3E__0_0; }
	inline void set_U3CbuyFormU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CbuyFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuyFormU3E__0_0, value);
	}

	inline static int32_t get_offset_of_lotsDict_1() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___lotsDict_1)); }
	inline String_t* get_lotsDict_1() const { return ___lotsDict_1; }
	inline String_t** get_address_of_lotsDict_1() { return &___lotsDict_1; }
	inline void set_lotsDict_1(String_t* value)
	{
		___lotsDict_1 = value;
		Il2CppCodeGenWriteBarrier(&___lotsDict_1, value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_2() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U3CrequestU3E__1_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__1_2() const { return ___U3CrequestU3E__1_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__1_2() { return &___U3CrequestU3E__1_2; }
	inline void set_U3CrequestU3E__1_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_3() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U3CobjU3E__2_3)); }
	inline JSONObject_t1971882247 * get_U3CobjU3E__2_3() const { return ___U3CobjU3E__2_3; }
	inline JSONObject_t1971882247 ** get_address_of_U3CobjU3E__2_3() { return &___U3CobjU3E__2_3; }
	inline void set_U3CobjU3E__2_3(JSONObject_t1971882247 * value)
	{
		___U3CobjU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ElotsDict_6() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U3CU24U3ElotsDict_6)); }
	inline String_t* get_U3CU24U3ElotsDict_6() const { return ___U3CU24U3ElotsDict_6; }
	inline String_t** get_address_of_U3CU24U3ElotsDict_6() { return &___U3CU24U3ElotsDict_6; }
	inline void set_U3CU24U3ElotsDict_6(String_t* value)
	{
		___U3CU24U3ElotsDict_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ElotsDict_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CBuyLotsU3Ec__Iterator12_t1560696684, ___U3CU3Ef__this_7)); }
	inline MarketManager_t2047363881 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline MarketManager_t2047363881 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(MarketManager_t2047363881 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
