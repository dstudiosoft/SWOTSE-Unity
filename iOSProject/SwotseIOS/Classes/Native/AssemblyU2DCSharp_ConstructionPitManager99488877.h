﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstructionPitManager
struct  ConstructionPitManager_t99488877  : public MonoBehaviour_t1158329972
{
public:
	// System.Int64 ConstructionPitManager::pitId
	int64_t ___pitId_2;
	// UnityEngine.GameObject ConstructionPitManager::building
	GameObject_t1756533147 * ___building_3;
	// System.String ConstructionPitManager::type
	String_t* ___type_4;

public:
	inline static int32_t get_offset_of_pitId_2() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t99488877, ___pitId_2)); }
	inline int64_t get_pitId_2() const { return ___pitId_2; }
	inline int64_t* get_address_of_pitId_2() { return &___pitId_2; }
	inline void set_pitId_2(int64_t value)
	{
		___pitId_2 = value;
	}

	inline static int32_t get_offset_of_building_3() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t99488877, ___building_3)); }
	inline GameObject_t1756533147 * get_building_3() const { return ___building_3; }
	inline GameObject_t1756533147 ** get_address_of_building_3() { return &___building_3; }
	inline void set_building_3(GameObject_t1756533147 * value)
	{
		___building_3 = value;
		Il2CppCodeGenWriteBarrier(&___building_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(ConstructionPitManager_t99488877, ___type_4)); }
	inline String_t* get_type_4() const { return ___type_4; }
	inline String_t** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(String_t* value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
