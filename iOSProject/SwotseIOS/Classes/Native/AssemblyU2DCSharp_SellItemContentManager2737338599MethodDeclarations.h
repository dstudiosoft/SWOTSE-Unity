﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SellItemContentManager
struct SellItemContentManager_t2737338599;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// IReloadable
struct IReloadable_t861412162;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SellItemContentManager::.ctor()
extern "C"  void SellItemContentManager__ctor_m2151886038 (SellItemContentManager_t2737338599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SellItemContentManager::SetContent(UnityEngine.Sprite,System.String,System.String,System.String,IReloadable)
extern "C"  void SellItemContentManager_SetContent_m2715827207 (SellItemContentManager_t2737338599 * __this, Sprite_t309593783 * ___image0, String_t* ___name1, String_t* ___count2, String_t* ___id3, Il2CppObject * ___controller4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SellItemContentManager::SellItems()
extern "C"  void SellItemContentManager_SellItems_m332423730 (SellItemContentManager_t2737338599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SellItemContentManager::ItemsSold(System.Object,System.String)
extern "C"  void SellItemContentManager_ItemsSold_m3214622772 (SellItemContentManager_t2737338599 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SellItemContentManager::Close(System.Object,System.String)
extern "C"  void SellItemContentManager_Close_m2455289542 (SellItemContentManager_t2737338599 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SellItemContentManager::Cancel()
extern "C"  void SellItemContentManager_Cancel_m1458681010 (SellItemContentManager_t2737338599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
