﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShopItemModel
struct  ShopItemModel_t96241056  : public Il2CppObject
{
public:
	// System.String ShopItemModel::id
	String_t* ___id_0;
	// System.String ShopItemModel::name
	String_t* ___name_1;
	// System.String ShopItemModel::rank
	String_t* ___rank_2;
	// System.String ShopItemModel::description
	String_t* ___description_3;
	// System.Int64 ShopItemModel::price_sh
	int64_t ___price_sh_4;
	// System.String ShopItemModel::type
	String_t* ___type_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___rank_2)); }
	inline String_t* get_rank_2() const { return ___rank_2; }
	inline String_t** get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(String_t* value)
	{
		___rank_2 = value;
		Il2CppCodeGenWriteBarrier(&___rank_2, value);
	}

	inline static int32_t get_offset_of_description_3() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___description_3)); }
	inline String_t* get_description_3() const { return ___description_3; }
	inline String_t** get_address_of_description_3() { return &___description_3; }
	inline void set_description_3(String_t* value)
	{
		___description_3 = value;
		Il2CppCodeGenWriteBarrier(&___description_3, value);
	}

	inline static int32_t get_offset_of_price_sh_4() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___price_sh_4)); }
	inline int64_t get_price_sh_4() const { return ___price_sh_4; }
	inline int64_t* get_address_of_price_sh_4() { return &___price_sh_4; }
	inline void set_price_sh_4(int64_t value)
	{
		___price_sh_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ShopItemModel_t96241056, ___type_5)); }
	inline String_t* get_type_5() const { return ___type_5; }
	inline String_t** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(String_t* value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
