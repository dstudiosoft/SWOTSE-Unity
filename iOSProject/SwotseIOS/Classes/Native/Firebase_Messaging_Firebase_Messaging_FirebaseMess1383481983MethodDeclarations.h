﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct EventHandler_1_t1343734987;
// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct EventHandler_1_t266896245;
// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t3038239007;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t1095792431;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMess3038239007.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMess1095792431.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Firebase.Messaging.FirebaseMessaging::.cctor()
extern "C"  void FirebaseMessaging__cctor_m393898133 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_MessageReceivedInternal_m647638879 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1343734987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_MessageReceivedInternal_m2065608280 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1343734987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_TokenReceivedInternal_m490100159 (Il2CppObject * __this /* static, unused */, EventHandler_1_t266896245 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_TokenReceivedInternal_m2517466424 (Il2CppObject * __this /* static, unused */, EventHandler_1_t266896245 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::CreateOrDestroyListener()
extern "C"  void FirebaseMessaging_CreateOrDestroyListener_m307114011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_MessageReceived(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_MessageReceived_m3873841432 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1343734987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_MessageReceived(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_MessageReceived_m2935686043 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1343734987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_TokenReceived(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_TokenReceived_m969755288 (Il2CppObject * __this /* static, unused */, EventHandler_1_t266896245 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_TokenReceived(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_TokenReceived_m1632162779 (Il2CppObject * __this /* static, unused */, EventHandler_1_t266896245 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessaging_SetListenerCallbacks_m1099223436 (Il2CppObject * __this /* static, unused */, MessageReceivedDelegate_t3038239007 * ___messageCallback0, TokenReceivedDelegate_t1095792431 * ___tokenCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessaging_SetListenerCallbacksEnabled_m2707683333 (Il2CppObject * __this /* static, unused */, bool ___message_callback_enabled0, bool ___token_callback_enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SendPendingEvents()
extern "C"  void FirebaseMessaging_SendPendingEvents_m275580946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.Messaging.FirebaseMessaging::MessageCopyNotification(System.IntPtr)
extern "C"  IntPtr_t FirebaseMessaging_MessageCopyNotification_m1066256730 (Il2CppObject * __this /* static, unused */, IntPtr_t ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
