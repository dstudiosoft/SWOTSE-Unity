﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<StringifyAsync>c__Iterator3
struct U3CStringifyAsyncU3Ec__Iterator3_t4037879553;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<StringifyAsync>c__Iterator3::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3__ctor_m3372576406 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1695515356 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2708799604 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m582030445 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1394338327 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<StringifyAsync>c__Iterator3::MoveNext()
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator3_MoveNext_m1475634618 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Dispose()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Dispose_m755628987 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Reset()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Reset_m3971269097 (U3CStringifyAsyncU3Ec__Iterator3_t4037879553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
