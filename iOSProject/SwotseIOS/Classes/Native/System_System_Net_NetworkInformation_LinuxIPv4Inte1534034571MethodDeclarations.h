﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.LinuxIPv4InterfaceProperties
struct LinuxIPv4InterfaceProperties_t1534034571;
// System.Net.NetworkInformation.LinuxNetworkInterface
struct LinuxNetworkInterface_t3864470295;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"

// System.Void System.Net.NetworkInformation.LinuxIPv4InterfaceProperties::.ctor(System.Net.NetworkInformation.LinuxNetworkInterface)
extern "C"  void LinuxIPv4InterfaceProperties__ctor_m1190628452 (LinuxIPv4InterfaceProperties_t1534034571 * __this, LinuxNetworkInterface_t3864470295 * ___iface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.LinuxIPv4InterfaceProperties::get_IsForwardingEnabled()
extern "C"  bool LinuxIPv4InterfaceProperties_get_IsForwardingEnabled_m336773314 (LinuxIPv4InterfaceProperties_t1534034571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.LinuxIPv4InterfaceProperties::get_Mtu()
extern "C"  int32_t LinuxIPv4InterfaceProperties_get_Mtu_m4036506946 (LinuxIPv4InterfaceProperties_t1534034571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
