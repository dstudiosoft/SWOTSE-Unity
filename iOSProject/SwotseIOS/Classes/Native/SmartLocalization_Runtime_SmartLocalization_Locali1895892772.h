﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;

#include "mscorlib_System_Object2689449295.h"
#include "SmartLocalization_Runtime_SmartLocalization_Locali3069815932.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LocalizedObject
struct  LocalizedObject_t1895892772  : public Il2CppObject
{
public:
	// SmartLocalization.LocalizedObjectType SmartLocalization.LocalizedObject::objectType
	int32_t ___objectType_2;
	// System.String SmartLocalization.LocalizedObject::textValue
	String_t* ___textValue_3;
	// UnityEngine.GameObject SmartLocalization.LocalizedObject::thisGameObject
	GameObject_t1756533147 * ___thisGameObject_4;
	// UnityEngine.AudioClip SmartLocalization.LocalizedObject::thisAudioClip
	AudioClip_t1932558630 * ___thisAudioClip_5;
	// UnityEngine.Texture SmartLocalization.LocalizedObject::thisTexture
	Texture_t2243626319 * ___thisTexture_6;
	// UnityEngine.TextAsset SmartLocalization.LocalizedObject::thisTextAsset
	TextAsset_t3973159845 * ___thisTextAsset_7;
	// System.Boolean SmartLocalization.LocalizedObject::overrideLocalizedObject
	bool ___overrideLocalizedObject_8;
	// System.String SmartLocalization.LocalizedObject::overrideObjectLanguageCode
	String_t* ___overrideObjectLanguageCode_9;

public:
	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___objectType_2)); }
	inline int32_t get_objectType_2() const { return ___objectType_2; }
	inline int32_t* get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(int32_t value)
	{
		___objectType_2 = value;
	}

	inline static int32_t get_offset_of_textValue_3() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___textValue_3)); }
	inline String_t* get_textValue_3() const { return ___textValue_3; }
	inline String_t** get_address_of_textValue_3() { return &___textValue_3; }
	inline void set_textValue_3(String_t* value)
	{
		___textValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___textValue_3, value);
	}

	inline static int32_t get_offset_of_thisGameObject_4() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___thisGameObject_4)); }
	inline GameObject_t1756533147 * get_thisGameObject_4() const { return ___thisGameObject_4; }
	inline GameObject_t1756533147 ** get_address_of_thisGameObject_4() { return &___thisGameObject_4; }
	inline void set_thisGameObject_4(GameObject_t1756533147 * value)
	{
		___thisGameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___thisGameObject_4, value);
	}

	inline static int32_t get_offset_of_thisAudioClip_5() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___thisAudioClip_5)); }
	inline AudioClip_t1932558630 * get_thisAudioClip_5() const { return ___thisAudioClip_5; }
	inline AudioClip_t1932558630 ** get_address_of_thisAudioClip_5() { return &___thisAudioClip_5; }
	inline void set_thisAudioClip_5(AudioClip_t1932558630 * value)
	{
		___thisAudioClip_5 = value;
		Il2CppCodeGenWriteBarrier(&___thisAudioClip_5, value);
	}

	inline static int32_t get_offset_of_thisTexture_6() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___thisTexture_6)); }
	inline Texture_t2243626319 * get_thisTexture_6() const { return ___thisTexture_6; }
	inline Texture_t2243626319 ** get_address_of_thisTexture_6() { return &___thisTexture_6; }
	inline void set_thisTexture_6(Texture_t2243626319 * value)
	{
		___thisTexture_6 = value;
		Il2CppCodeGenWriteBarrier(&___thisTexture_6, value);
	}

	inline static int32_t get_offset_of_thisTextAsset_7() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___thisTextAsset_7)); }
	inline TextAsset_t3973159845 * get_thisTextAsset_7() const { return ___thisTextAsset_7; }
	inline TextAsset_t3973159845 ** get_address_of_thisTextAsset_7() { return &___thisTextAsset_7; }
	inline void set_thisTextAsset_7(TextAsset_t3973159845 * value)
	{
		___thisTextAsset_7 = value;
		Il2CppCodeGenWriteBarrier(&___thisTextAsset_7, value);
	}

	inline static int32_t get_offset_of_overrideLocalizedObject_8() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___overrideLocalizedObject_8)); }
	inline bool get_overrideLocalizedObject_8() const { return ___overrideLocalizedObject_8; }
	inline bool* get_address_of_overrideLocalizedObject_8() { return &___overrideLocalizedObject_8; }
	inline void set_overrideLocalizedObject_8(bool value)
	{
		___overrideLocalizedObject_8 = value;
	}

	inline static int32_t get_offset_of_overrideObjectLanguageCode_9() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772, ___overrideObjectLanguageCode_9)); }
	inline String_t* get_overrideObjectLanguageCode_9() const { return ___overrideObjectLanguageCode_9; }
	inline String_t** get_address_of_overrideObjectLanguageCode_9() { return &___overrideObjectLanguageCode_9; }
	inline void set_overrideObjectLanguageCode_9(String_t* value)
	{
		___overrideObjectLanguageCode_9 = value;
		Il2CppCodeGenWriteBarrier(&___overrideObjectLanguageCode_9, value);
	}
};

struct LocalizedObject_t1895892772_StaticFields
{
public:
	// System.String SmartLocalization.LocalizedObject::keyTypeIdentifier
	String_t* ___keyTypeIdentifier_0;
	// System.String SmartLocalization.LocalizedObject::endBracket
	String_t* ___endBracket_1;

public:
	inline static int32_t get_offset_of_keyTypeIdentifier_0() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772_StaticFields, ___keyTypeIdentifier_0)); }
	inline String_t* get_keyTypeIdentifier_0() const { return ___keyTypeIdentifier_0; }
	inline String_t** get_address_of_keyTypeIdentifier_0() { return &___keyTypeIdentifier_0; }
	inline void set_keyTypeIdentifier_0(String_t* value)
	{
		___keyTypeIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier(&___keyTypeIdentifier_0, value);
	}

	inline static int32_t get_offset_of_endBracket_1() { return static_cast<int32_t>(offsetof(LocalizedObject_t1895892772_StaticFields, ___endBracket_1)); }
	inline String_t* get_endBracket_1() const { return ___endBracket_1; }
	inline String_t** get_address_of_endBracket_1() { return &___endBracket_1; }
	inline void set_endBracket_1(String_t* value)
	{
		___endBracket_1 = value;
		Il2CppCodeGenWriteBarrier(&___endBracket_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
