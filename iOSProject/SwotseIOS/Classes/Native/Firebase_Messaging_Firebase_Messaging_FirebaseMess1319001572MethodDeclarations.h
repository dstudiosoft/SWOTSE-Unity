﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t1319001572;
// Firebase.Messaging.FirebaseNotification
struct FirebaseNotification_t531124606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Firebase.Messaging.FirebaseMessage::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseMessage__ctor_m1714174642 (FirebaseMessage_t1319001572 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessage::Finalize()
extern "C"  void FirebaseMessage_Finalize_m2164429225 (FirebaseMessage_t1319001572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessage::Dispose()
extern "C"  void FirebaseMessage_Dispose_m4258025450 (FirebaseMessage_t1319001572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Messaging.FirebaseNotification Firebase.Messaging.FirebaseMessage::get_Notification()
extern "C"  FirebaseNotification_t531124606 * FirebaseMessage_get_Notification_m3315407199 (FirebaseMessage_t1319001572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
