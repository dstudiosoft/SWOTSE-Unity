﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// IReloadable
struct IReloadable_t861412162;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SellItemContentManager
struct  SellItemContentManager_t2737338599  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SellItemContentManager::itemName
	Text_t356221433 * ___itemName_2;
	// UnityEngine.UI.Text SellItemContentManager::itemsOwned
	Text_t356221433 * ___itemsOwned_3;
	// UnityEngine.UI.Image SellItemContentManager::itemImage
	Image_t2042527209 * ___itemImage_4;
	// UnityEngine.UI.Dropdown SellItemContentManager::discount
	Dropdown_t1985816271 * ___discount_5;
	// UnityEngine.UI.InputField SellItemContentManager::quantity
	InputField_t1631627530 * ___quantity_6;
	// IReloadable SellItemContentManager::owner
	Il2CppObject * ___owner_7;
	// System.Int64 SellItemContentManager::itemcount
	int64_t ___itemcount_8;
	// System.String SellItemContentManager::itemId
	String_t* ___itemId_9;
	// System.Int64 SellItemContentManager::toSellCount
	int64_t ___toSellCount_10;

public:
	inline static int32_t get_offset_of_itemName_2() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___itemName_2)); }
	inline Text_t356221433 * get_itemName_2() const { return ___itemName_2; }
	inline Text_t356221433 ** get_address_of_itemName_2() { return &___itemName_2; }
	inline void set_itemName_2(Text_t356221433 * value)
	{
		___itemName_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_2, value);
	}

	inline static int32_t get_offset_of_itemsOwned_3() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___itemsOwned_3)); }
	inline Text_t356221433 * get_itemsOwned_3() const { return ___itemsOwned_3; }
	inline Text_t356221433 ** get_address_of_itemsOwned_3() { return &___itemsOwned_3; }
	inline void set_itemsOwned_3(Text_t356221433 * value)
	{
		___itemsOwned_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemsOwned_3, value);
	}

	inline static int32_t get_offset_of_itemImage_4() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___itemImage_4)); }
	inline Image_t2042527209 * get_itemImage_4() const { return ___itemImage_4; }
	inline Image_t2042527209 ** get_address_of_itemImage_4() { return &___itemImage_4; }
	inline void set_itemImage_4(Image_t2042527209 * value)
	{
		___itemImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_4, value);
	}

	inline static int32_t get_offset_of_discount_5() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___discount_5)); }
	inline Dropdown_t1985816271 * get_discount_5() const { return ___discount_5; }
	inline Dropdown_t1985816271 ** get_address_of_discount_5() { return &___discount_5; }
	inline void set_discount_5(Dropdown_t1985816271 * value)
	{
		___discount_5 = value;
		Il2CppCodeGenWriteBarrier(&___discount_5, value);
	}

	inline static int32_t get_offset_of_quantity_6() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___quantity_6)); }
	inline InputField_t1631627530 * get_quantity_6() const { return ___quantity_6; }
	inline InputField_t1631627530 ** get_address_of_quantity_6() { return &___quantity_6; }
	inline void set_quantity_6(InputField_t1631627530 * value)
	{
		___quantity_6 = value;
		Il2CppCodeGenWriteBarrier(&___quantity_6, value);
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___owner_7)); }
	inline Il2CppObject * get_owner_7() const { return ___owner_7; }
	inline Il2CppObject ** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(Il2CppObject * value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier(&___owner_7, value);
	}

	inline static int32_t get_offset_of_itemcount_8() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___itemcount_8)); }
	inline int64_t get_itemcount_8() const { return ___itemcount_8; }
	inline int64_t* get_address_of_itemcount_8() { return &___itemcount_8; }
	inline void set_itemcount_8(int64_t value)
	{
		___itemcount_8 = value;
	}

	inline static int32_t get_offset_of_itemId_9() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___itemId_9)); }
	inline String_t* get_itemId_9() const { return ___itemId_9; }
	inline String_t** get_address_of_itemId_9() { return &___itemId_9; }
	inline void set_itemId_9(String_t* value)
	{
		___itemId_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemId_9, value);
	}

	inline static int32_t get_offset_of_toSellCount_10() { return static_cast<int32_t>(offsetof(SellItemContentManager_t2737338599, ___toSellCount_10)); }
	inline int64_t get_toSellCount_10() const { return ___toSellCount_10; }
	inline int64_t* get_address_of_toSellCount_10() { return &___toSellCount_10; }
	inline void set_toSellCount_10(int64_t value)
	{
		___toSellCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
