﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildNewCityContentManager
struct BuildNewCityContentManager_t3113853881;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BuildNewCityContentManager::.ctor()
extern "C"  void BuildNewCityContentManager__ctor_m2844477508 (BuildNewCityContentManager_t3113853881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildNewCityContentManager::OnEnable()
extern "C"  void BuildNewCityContentManager_OnEnable_m3195036452 (BuildNewCityContentManager_t3113853881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildNewCityContentManager::FixedUpdate()
extern "C"  void BuildNewCityContentManager_FixedUpdate_m3242600687 (BuildNewCityContentManager_t3113853881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildNewCityContentManager::BuildNewCity()
extern "C"  void BuildNewCityContentManager_BuildNewCity_m4063326377 (BuildNewCityContentManager_t3113853881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildNewCityContentManager::NewCityHandler(System.Object,System.String)
extern "C"  void BuildNewCityContentManager_NewCityHandler_m4150890847 (BuildNewCityContentManager_t3113853881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
