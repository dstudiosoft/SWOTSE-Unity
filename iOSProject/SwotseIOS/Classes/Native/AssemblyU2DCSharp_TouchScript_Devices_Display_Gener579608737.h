﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_TouchScript_Devices_Display_Disp1478475236.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Devices.Display.GenericDisplayDevice
struct  GenericDisplayDevice_t579608737  : public DisplayDevice_t1478475236
{
public:

public:
};

struct GenericDisplayDevice_t579608737_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> TouchScript.Devices.Display.GenericDisplayDevice::isLaptop
	Nullable_1_t2088641033  ___isLaptop_4;

public:
	inline static int32_t get_offset_of_isLaptop_4() { return static_cast<int32_t>(offsetof(GenericDisplayDevice_t579608737_StaticFields, ___isLaptop_4)); }
	inline Nullable_1_t2088641033  get_isLaptop_4() const { return ___isLaptop_4; }
	inline Nullable_1_t2088641033 * get_address_of_isLaptop_4() { return &___isLaptop_4; }
	inline void set_isLaptop_4(Nullable_1_t2088641033  value)
	{
		___isLaptop_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
