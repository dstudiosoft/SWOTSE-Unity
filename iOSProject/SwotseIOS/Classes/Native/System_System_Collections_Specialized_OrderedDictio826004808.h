﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator
struct  OrderedCollectionEnumerator_t826004808  : public Il2CppObject
{
public:
	// System.Boolean System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::isKeyList
	bool ___isKeyList_0;
	// System.Collections.IEnumerator System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::listEnumerator
	Il2CppObject * ___listEnumerator_1;

public:
	inline static int32_t get_offset_of_isKeyList_0() { return static_cast<int32_t>(offsetof(OrderedCollectionEnumerator_t826004808, ___isKeyList_0)); }
	inline bool get_isKeyList_0() const { return ___isKeyList_0; }
	inline bool* get_address_of_isKeyList_0() { return &___isKeyList_0; }
	inline void set_isKeyList_0(bool value)
	{
		___isKeyList_0 = value;
	}

	inline static int32_t get_offset_of_listEnumerator_1() { return static_cast<int32_t>(offsetof(OrderedCollectionEnumerator_t826004808, ___listEnumerator_1)); }
	inline Il2CppObject * get_listEnumerator_1() const { return ___listEnumerator_1; }
	inline Il2CppObject ** get_address_of_listEnumerator_1() { return &___listEnumerator_1; }
	inline void set_listEnumerator_1(Il2CppObject * value)
	{
		___listEnumerator_1 = value;
		Il2CppCodeGenWriteBarrier(&___listEnumerator_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
