﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceManager
struct AllianceManager_t3412736524;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceManager::.ctor()
extern "C"  void AllianceManager__ctor_m20403429 (AllianceManager_t3412736524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onGetAllianceMembers(SimpleEvent)
extern "C"  void AllianceManager_add_onGetAllianceMembers_m3287453590 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onGetAllianceMembers(SimpleEvent)
extern "C"  void AllianceManager_remove_onGetAllianceMembers_m3872730303 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onGetAlliances(SimpleEvent)
extern "C"  void AllianceManager_add_onGetAlliances_m1715129832 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onGetAlliances(SimpleEvent)
extern "C"  void AllianceManager_remove_onGetAlliances_m1952475971 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onExcludeUser(SimpleEvent)
extern "C"  void AllianceManager_add_onExcludeUser_m410176639 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onExcludeUser(SimpleEvent)
extern "C"  void AllianceManager_remove_onExcludeUser_m3014760798 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onInviteUser(SimpleEvent)
extern "C"  void AllianceManager_add_onInviteUser_m3627309008 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onInviteUser(SimpleEvent)
extern "C"  void AllianceManager_remove_onInviteUser_m1685613447 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onRequestJoin(SimpleEvent)
extern "C"  void AllianceManager_add_onRequestJoin_m246585267 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onRequestJoin(SimpleEvent)
extern "C"  void AllianceManager_remove_onRequestJoin_m4038272130 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onQuitAlliance(SimpleEvent)
extern "C"  void AllianceManager_add_onQuitAlliance_m2931837040 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onQuitAlliance(SimpleEvent)
extern "C"  void AllianceManager_remove_onQuitAlliance_m1854077215 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onUpdateGuideline(SimpleEvent)
extern "C"  void AllianceManager_add_onUpdateGuideline_m2316187617 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onUpdateGuideline(SimpleEvent)
extern "C"  void AllianceManager_remove_onUpdateGuideline_m2549690286 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onAcceptInvite(SimpleEvent)
extern "C"  void AllianceManager_add_onAcceptInvite_m3159087733 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onAcceptInvite(SimpleEvent)
extern "C"  void AllianceManager_remove_onAcceptInvite_m569748746 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onCancelInvite(SimpleEvent)
extern "C"  void AllianceManager_add_onCancelInvite_m3976226931 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onCancelInvite(SimpleEvent)
extern "C"  void AllianceManager_remove_onCancelInvite_m3034769540 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onAddAlly(SimpleEvent)
extern "C"  void AllianceManager_add_onAddAlly_m2515644769 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onAddAlly(SimpleEvent)
extern "C"  void AllianceManager_remove_onAddAlly_m2496994674 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onAddEnemy(SimpleEvent)
extern "C"  void AllianceManager_add_onAddEnemy_m968658035 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onAddEnemy(SimpleEvent)
extern "C"  void AllianceManager_remove_onAddEnemy_m3897373174 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onRemoveAlly(SimpleEvent)
extern "C"  void AllianceManager_add_onRemoveAlly_m3595328680 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onRemoveAlly(SimpleEvent)
extern "C"  void AllianceManager_remove_onRemoveAlly_m1185461585 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onRemoveEnemy(SimpleEvent)
extern "C"  void AllianceManager_add_onRemoveEnemy_m3383221124 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onRemoveEnemy(SimpleEvent)
extern "C"  void AllianceManager_remove_onRemoveEnemy_m675573923 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::add_onCreateNewAlliance(SimpleEvent)
extern "C"  void AllianceManager_add_onCreateNewAlliance_m4181138389 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceManager::remove_onCreateNewAlliance(SimpleEvent)
extern "C"  void AllianceManager_remove_onCreateNewAlliance_m3350098240 (AllianceManager_t3412736524 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::AllianceMemebers()
extern "C"  Il2CppObject * AllianceManager_AllianceMemebers_m2474319096 (AllianceManager_t3412736524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::ExcludeUser(System.Int64)
extern "C"  Il2CppObject * AllianceManager_ExcludeUser_m3805837518 (AllianceManager_t3412736524 * __this, int64_t ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::InviteUser(System.Int64)
extern "C"  Il2CppObject * AllianceManager_InviteUser_m2947700757 (AllianceManager_t3412736524 * __this, int64_t ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::RequestJoin(System.Int64)
extern "C"  Il2CppObject * AllianceManager_RequestJoin_m3856205610 (AllianceManager_t3412736524 * __this, int64_t ___allianceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::AllAlliances()
extern "C"  Il2CppObject * AllianceManager_AllAlliances_m1557920644 (AllianceManager_t3412736524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::QuitAlliance()
extern "C"  Il2CppObject * AllianceManager_QuitAlliance_m1607134575 (AllianceManager_t3412736524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::UpdateGuideline(System.String)
extern "C"  Il2CppObject * AllianceManager_UpdateGuideline_m3137152428 (AllianceManager_t3412736524 * __this, String_t* ___guideline0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::AcceptInvite(System.Int64)
extern "C"  Il2CppObject * AllianceManager_AcceptInvite_m3569919774 (AllianceManager_t3412736524 * __this, int64_t ___inviteId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::CancelInvite(System.Int64)
extern "C"  Il2CppObject * AllianceManager_CancelInvite_m2622107316 (AllianceManager_t3412736524 * __this, int64_t ___inviteId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::SetMemberRank(System.String,System.Int64)
extern "C"  Il2CppObject * AllianceManager_SetMemberRank_m744230187 (AllianceManager_t3412736524 * __this, String_t* ___rank0, int64_t ___userId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::ResignRank()
extern "C"  Il2CppObject * AllianceManager_ResignRank_m1715537125 (AllianceManager_t3412736524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::AddAlly(System.Int64)
extern "C"  Il2CppObject * AllianceManager_AddAlly_m3723156570 (AllianceManager_t3412736524 * __this, int64_t ___allianceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::RemoveAlly(System.Int64)
extern "C"  Il2CppObject * AllianceManager_RemoveAlly_m2626223931 (AllianceManager_t3412736524 * __this, int64_t ___allianceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::AddEnemy(System.Int64)
extern "C"  Il2CppObject * AllianceManager_AddEnemy_m3705855458 (AllianceManager_t3412736524 * __this, int64_t ___allianceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::RemoveEnemy(System.Int64)
extern "C"  Il2CppObject * AllianceManager_RemoveEnemy_m3940692521 (AllianceManager_t3412736524 * __this, int64_t ___allianceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceManager::CreateNewAllliance(System.String)
extern "C"  Il2CppObject * AllianceManager_CreateNewAllliance_m2095512852 (AllianceManager_t3412736524 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
