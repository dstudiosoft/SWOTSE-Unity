﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_U1709969761MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>::.ctor(System.Object,System.IntPtr)
#define UnityFunc_1__ctor_m1298516047(__this, ___object0, ___method1, method) ((  void (*) (UnityFunc_1_t1559192105 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityFunc_1__ctor_m3483258383_gshared)(__this, ___object0, ___method1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>::Invoke()
#define UnityFunc_1_Invoke_m3209975362(__this, method) ((  List_1_t1721427117 * (*) (UnityFunc_1_t1559192105 *, const MethodInfo*))UnityFunc_1_Invoke_m4133775042_gshared)(__this, method)
// System.IAsyncResult TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>::BeginInvoke(System.AsyncCallback,System.Object)
#define UnityFunc_1_BeginInvoke_m1560535050(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (UnityFunc_1_t1559192105 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_BeginInvoke_m3032171082_gshared)(__this, ___callback0, ___object1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>,System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>::EndInvoke(System.IAsyncResult)
#define UnityFunc_1_EndInvoke_m835051734(__this, ___result0, method) ((  List_1_t1721427117 * (*) (UnityFunc_1_t1559192105 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_EndInvoke_m455851222_gshared)(__this, ___result0, method)
