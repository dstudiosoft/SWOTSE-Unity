﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.Attributes.NullToggleAttribute
struct NullToggleAttribute_t2456566677;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.Utils.Attributes.NullToggleAttribute::.ctor()
extern "C"  void NullToggleAttribute__ctor_m3114558870 (NullToggleAttribute_t2456566677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
