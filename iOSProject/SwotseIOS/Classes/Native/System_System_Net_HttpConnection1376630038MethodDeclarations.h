﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpConnection
struct HttpConnection_t1376630038;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Net.EndPointListener
struct EndPointListener_t822992745;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.String
struct String_t;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.ListenerPrefix
struct ListenerPrefix_t577622550;
// System.Net.RequestStream
struct RequestStream_t550151197;
// System.Net.ResponseStream
struct ResponseStream_t3958753779;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_EndPointListener822992745.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_ListenerPrefix577622550.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"

// System.Void System.Net.HttpConnection::.ctor(System.Net.Sockets.Socket,System.Net.EndPointListener,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  void HttpConnection__ctor_m2145981891 (HttpConnection_t1376630038 * __this, Socket_t3821512045 * ___sock0, EndPointListener_t822992745 * ___epl1, bool ___secure2, X509Certificate2_t4056456767 * ___cert3, AsymmetricAlgorithm_t784058677 * ___key4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Net.HttpConnection::OnPVKSelection(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern "C"  AsymmetricAlgorithm_t784058677 * HttpConnection_OnPVKSelection_m450775607 (HttpConnection_t1376630038 * __this, X509Certificate_t283079845 * ___certificate0, String_t* ___targetHost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::Init()
extern "C"  void HttpConnection_Init_m855367422 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.HttpConnection::get_ChunkedUses()
extern "C"  int32_t HttpConnection_get_ChunkedUses_m1142074491 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.HttpConnection::get_LocalEndPoint()
extern "C"  IPEndPoint_t2615413766 * HttpConnection_get_LocalEndPoint_m3614905432 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.HttpConnection::get_RemoteEndPoint()
extern "C"  IPEndPoint_t2615413766 * HttpConnection_get_RemoteEndPoint_m3476668947 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpConnection::get_IsSecure()
extern "C"  bool HttpConnection_get_IsSecure_m3078193596 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ListenerPrefix System.Net.HttpConnection::get_Prefix()
extern "C"  ListenerPrefix_t577622550 * HttpConnection_get_Prefix_m3645793408 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::set_Prefix(System.Net.ListenerPrefix)
extern "C"  void HttpConnection_set_Prefix_m1415641135 (HttpConnection_t1376630038 * __this, ListenerPrefix_t577622550 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::BeginReadRequest()
extern "C"  void HttpConnection_BeginReadRequest_m2874455910 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.RequestStream System.Net.HttpConnection::GetRequestStream(System.Boolean,System.Int64)
extern "C"  RequestStream_t550151197 * HttpConnection_GetRequestStream_m3701805198 (HttpConnection_t1376630038 * __this, bool ___chunked0, int64_t ___contentlength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ResponseStream System.Net.HttpConnection::GetResponseStream()
extern "C"  ResponseStream_t3958753779 * HttpConnection_GetResponseStream_m3354895617 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::OnRead(System.IAsyncResult)
extern "C"  void HttpConnection_OnRead_m251837386 (HttpConnection_t1376630038 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpConnection::ProcessInput(System.IO.MemoryStream)
extern "C"  bool HttpConnection_ProcessInput_m3051748045 (HttpConnection_t1376630038 * __this, MemoryStream_t743994179 * ___ms0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpConnection::ReadLine(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  String_t* HttpConnection_ReadLine_m3774034191 (HttpConnection_t1376630038 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___len2, int32_t* ___used3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::SendError(System.String,System.Int32)
extern "C"  void HttpConnection_SendError_m3342435745 (HttpConnection_t1376630038 * __this, String_t* ___msg0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::SendError()
extern "C"  void HttpConnection_SendError_m1534489606 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::Unbind()
extern "C"  void HttpConnection_Unbind_m1035431688 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::Close()
extern "C"  void HttpConnection_Close_m195361522 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::CloseSocket()
extern "C"  void HttpConnection_CloseSocket_m3370445601 (HttpConnection_t1376630038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpConnection::Close(System.Boolean)
extern "C"  void HttpConnection_Close_m1334909767 (HttpConnection_t1376630038 * __this, bool ___force_close0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
