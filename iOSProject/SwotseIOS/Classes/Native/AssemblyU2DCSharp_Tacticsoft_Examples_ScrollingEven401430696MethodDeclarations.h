﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.Examples.ScrollingEventsHandler
struct ScrollingEventsHandler_t401430696;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.Examples.ScrollingEventsHandler::.ctor()
extern "C"  void ScrollingEventsHandler__ctor_m2600119198 (ScrollingEventsHandler_t401430696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.ScrollingEventsHandler::ScrollToTopAnimated()
extern "C"  void ScrollingEventsHandler_ScrollToTopAnimated_m3386406588 (ScrollingEventsHandler_t401430696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.ScrollingEventsHandler::ScrollToBottomImmediate()
extern "C"  void ScrollingEventsHandler_ScrollToBottomImmediate_m1925050258 (ScrollingEventsHandler_t401430696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.Examples.ScrollingEventsHandler::ScrollToRow10Animated()
extern "C"  void ScrollingEventsHandler_ScrollToRow10Animated_m1986091382 (ScrollingEventsHandler_t401430696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Tacticsoft.Examples.ScrollingEventsHandler::AnimateToScrollY(System.Single,System.Single)
extern "C"  Il2CppObject * ScrollingEventsHandler_AnimateToScrollY_m1197507140 (ScrollingEventsHandler_t401430696 * __this, float ___scrollY0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
