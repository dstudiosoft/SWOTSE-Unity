﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor()
#define List_1__ctor_m1986803350(__this, method) ((  void (*) (List_1_t1416064546 *, const MethodInfo*))List_1__ctor_m3040225017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1964409591(__this, ___collection0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor(System.Int32)
#define List_1__ctor_m1474807197(__this, ___capacity0, method) ((  void (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor(T[],System.Int32)
#define List_1__ctor_m3151876419(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1416064546 *, TuioBlobU5BU5D_t3918161843*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.cctor()
#define List_1__cctor_m2213066287(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2648430526(__this, method) ((  Il2CppObject* (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1463415624(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1416064546 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2239055149(__this, method) ((  Il2CppObject * (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4202244848(__this, ___item0, method) ((  int32_t (*) (List_1_t1416064546 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3537079646(__this, ___item0, method) ((  bool (*) (List_1_t1416064546 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1102904214(__this, ___item0, method) ((  int32_t (*) (List_1_t1416064546 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3903029181(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1416064546 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m441705269(__this, ___item0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2563331129(__this, method) ((  bool (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4249635096(__this, method) ((  bool (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3850335480(__this, method) ((  Il2CppObject * (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1884052177(__this, method) ((  bool (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2940852964(__this, method) ((  bool (*) (List_1_t1416064546 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m756304017(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3384389162(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1416064546 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Add(T)
#define List_1_Add_m2273408866(__this, ___item0, method) ((  void (*) (List_1_t1416064546 *, TuioBlob_t2046943414 *, const MethodInfo*))List_1_Add_m1194694229_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1344074810(__this, ___newCount0, method) ((  void (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2178036474(__this, ___collection0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3337458922(__this, ___enumerable0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3405688229(__this, ___collection0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::AsReadOnly()
#define List_1_AsReadOnly_m2328103420(__this, method) ((  ReadOnlyCollection_1_t2232729106 * (*) (List_1_t1416064546 *, const MethodInfo*))List_1_AsReadOnly_m1618299177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Clear()
#define List_1_Clear_m1175215109(__this, method) ((  void (*) (List_1_t1416064546 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Contains(T)
#define List_1_Contains_m729367983(__this, ___item0, method) ((  bool (*) (List_1_t1416064546 *, TuioBlob_t2046943414 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2704414169(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1416064546 *, TuioBlobU5BU5D_t3918161843*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Find(System.Predicate`1<T>)
#define List_1_Find_m684730019(__this, ___match0, method) ((  TuioBlob_t2046943414 * (*) (List_1_t1416064546 *, Predicate_1_t489913529 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2126279488(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t489913529 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3433429334(__this, ___match0, method) ((  List_1_t1416064546 * (*) (List_1_t1416064546 *, Predicate_1_t489913529 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3700509170(__this, ___match0, method) ((  List_1_t1416064546 * (*) (List_1_t1416064546 *, Predicate_1_t489913529 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3228651682(__this, ___match0, method) ((  List_1_t1416064546 * (*) (List_1_t1416064546 *, Predicate_1_t489913529 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3217081095(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1416064546 *, int32_t, int32_t, Predicate_1_t489913529 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2778409288(__this, ___action0, method) ((  void (*) (List_1_t1416064546 *, Action_1_t1848742796 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::GetEnumerator()
#define List_1_GetEnumerator_m77299632(__this, method) ((  Enumerator_t950794220  (*) (List_1_t1416064546 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::IndexOf(T)
#define List_1_IndexOf_m1747904525(__this, ___item0, method) ((  int32_t (*) (List_1_t1416064546 *, TuioBlob_t2046943414 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1249994580(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1416064546 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1372484405(__this, ___index0, method) ((  void (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Insert(System.Int32,T)
#define List_1_Insert_m3716712922(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1416064546 *, int32_t, TuioBlob_t2046943414 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m429094531(__this, ___collection0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Remove(T)
#define List_1_Remove_m2797026324(__this, ___item0, method) ((  bool (*) (List_1_t1416064546 *, TuioBlob_t2046943414 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2577279104(__this, ___match0, method) ((  int32_t (*) (List_1_t1416064546 *, Predicate_1_t489913529 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3422152870(__this, ___index0, method) ((  void (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Reverse()
#define List_1_Reverse_m2012987990(__this, method) ((  void (*) (List_1_t1416064546 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Sort()
#define List_1_Sort_m4013799134(__this, method) ((  void (*) (List_1_t1416064546 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m4169438842(__this, ___comparer0, method) ((  void (*) (List_1_t1416064546 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2743733847(__this, ___comparison0, method) ((  void (*) (List_1_t1416064546 *, Comparison_1_t3308682265 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::ToArray()
#define List_1_ToArray_m10817807(__this, method) ((  TuioBlobU5BU5D_t3918161843* (*) (List_1_t1416064546 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::TrimExcess()
#define List_1_TrimExcess_m3661900737(__this, method) ((  void (*) (List_1_t1416064546 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Capacity()
#define List_1_get_Capacity_m3403519471(__this, method) ((  int32_t (*) (List_1_t1416064546 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4245872314(__this, ___value0, method) ((  void (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Count()
#define List_1_get_Count_m2267760198(__this, method) ((  int32_t (*) (List_1_t1416064546 *, const MethodInfo*))List_1_get_Count_m1549410684_gshared)(__this, method)
// T System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Item(System.Int32)
#define List_1_get_Item_m3035119279(__this, ___index0, method) ((  TuioBlob_t2046943414 * (*) (List_1_t1416064546 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::set_Item(System.Int32,T)
#define List_1_set_Item_m1710345841(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1416064546 *, int32_t, TuioBlob_t2046943414 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
