﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelTableController
struct PanelTableController_t1117991214;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PanelTableController::.ctor()
extern "C"  void PanelTableController__ctor_m1782349747 (PanelTableController_t1117991214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableController::OnEnable()
extern "C"  void PanelTableController_OnEnable_m3704701039 (PanelTableController_t1117991214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PanelTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t PanelTableController_GetNumberOfRowsForTableView_m1147735495 (PanelTableController_t1117991214 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PanelTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float PanelTableController_GetHeightForRowInTableView_m26132959 (PanelTableController_t1117991214 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell PanelTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * PanelTableController_GetCellForRowInTableView_m1942923892 (PanelTableController_t1117991214 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableController::RemoveTimer(System.Int64)
extern "C"  void PanelTableController_RemoveTimer_m1432654504 (PanelTableController_t1117991214 * __this, int64_t ___eventId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableController::ReloadTable(System.Object,System.String)
extern "C"  void PanelTableController_ReloadTable_m86992710 (PanelTableController_t1117991214 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
