﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Mono.Math.BigInteger
struct BigInteger_t925946153;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t324051958;
// Mono.Security.X509.X509Extension
struct X509Extension_t1439760128;

#include "mscorlib_System_Array3829468939.h"
#include "Mono_Security_Mono_Math_BigInteger925946152.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1439760127.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"

#pragma once
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t3157146938  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BigInteger_t925946153 * m_Items[1];

public:
	inline BigInteger_t925946153 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BigInteger_t925946153 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BigInteger_t925946153 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Security.X509.X509Certificate[]
struct X509CertificateU5BU5D_t1996442776  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Certificate_t324051958 * m_Items[1];

public:
	inline X509Certificate_t324051958 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Certificate_t324051958 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Certificate_t324051958 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t2486010406  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Extension_t1439760128 * m_Items[1];

public:
	inline X509Extension_t1439760128 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Extension_t1439760128 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Extension_t1439760128 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct ClientCertificateTypeU5BU5D_t2397899623  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
