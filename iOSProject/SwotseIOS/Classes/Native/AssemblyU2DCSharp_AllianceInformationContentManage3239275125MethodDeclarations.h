﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceInformationContentManager
struct AllianceInformationContentManager_t3239275125;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConfirmationEvent4112571757.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceInformationContentManager::.ctor()
extern "C"  void AllianceInformationContentManager__ctor_m168502528 (AllianceInformationContentManager_t3239275125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::add_confirmed(ConfirmationEvent)
extern "C"  void AllianceInformationContentManager_add_confirmed_m1803326912 (AllianceInformationContentManager_t3239275125 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::remove_confirmed(ConfirmationEvent)
extern "C"  void AllianceInformationContentManager_remove_confirmed_m841485393 (AllianceInformationContentManager_t3239275125 * __this, ConfirmationEvent_t4112571757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::OnEnable()
extern "C"  void AllianceInformationContentManager_OnEnable_m4248556624 (AllianceInformationContentManager_t3239275125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::ResignRank()
extern "C"  void AllianceInformationContentManager_ResignRank_m1900817700 (AllianceInformationContentManager_t3239275125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::ResignConfirmed(System.Boolean)
extern "C"  void AllianceInformationContentManager_ResignConfirmed_m2700344984 (AllianceInformationContentManager_t3239275125 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::QuitAlliance()
extern "C"  void AllianceInformationContentManager_QuitAlliance_m1392079016 (AllianceInformationContentManager_t3239275125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::QuitConfirmed(System.Boolean)
extern "C"  void AllianceInformationContentManager_QuitConfirmed_m1777364025 (AllianceInformationContentManager_t3239275125 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInformationContentManager::OnQuitAlliance(System.Object,System.String)
extern "C"  void AllianceInformationContentManager_OnQuitAlliance_m2695648509 (AllianceInformationContentManager_t3239275125 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
