﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager/<BuyLots>c__Iterator12
struct U3CBuyLotsU3Ec__Iterator12_t1560696684;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketManager/<BuyLots>c__Iterator12::.ctor()
extern "C"  void U3CBuyLotsU3Ec__Iterator12__ctor_m1296632693 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<BuyLots>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBuyLotsU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2565556111 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<BuyLots>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBuyLotsU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3621740231 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketManager/<BuyLots>c__Iterator12::MoveNext()
extern "C"  bool U3CBuyLotsU3Ec__Iterator12_MoveNext_m2852221967 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<BuyLots>c__Iterator12::Dispose()
extern "C"  void U3CBuyLotsU3Ec__Iterator12_Dispose_m2059146014 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<BuyLots>c__Iterator12::Reset()
extern "C"  void U3CBuyLotsU3Ec__Iterator12_Reset_m3871779108 (U3CBuyLotsU3Ec__Iterator12_t1560696684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
