﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3097024156(__this, ___dictionary0, method) ((  void (*) (Enumerator_t747106301 *, Dictionary_2_t3722048895 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2670585291(__this, method) ((  Il2CppObject * (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2164088735(__this, method) ((  void (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2804880606(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m251713725(__this, method) ((  Il2CppObject * (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2952747213(__this, method) ((  Il2CppObject * (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::MoveNext()
#define Enumerator_MoveNext_m198855739(__this, method) ((  bool (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::get_Current()
#define Enumerator_get_Current_m2543408067(__this, method) ((  KeyValuePair_2_t1479394117  (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4120561996(__this, method) ((  String_t* (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1255164940(__this, method) ((  UnitConstantModel_t1807269633 * (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::Reset()
#define Enumerator_Reset_m940131034(__this, method) ((  void (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::VerifyState()
#define Enumerator_VerifyState_m4160913401(__this, method) ((  void (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1652024759(__this, method) ((  void (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnitConstantModel>::Dispose()
#define Enumerator_Dispose_m2659475984(__this, method) ((  void (*) (Enumerator_t747106301 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
