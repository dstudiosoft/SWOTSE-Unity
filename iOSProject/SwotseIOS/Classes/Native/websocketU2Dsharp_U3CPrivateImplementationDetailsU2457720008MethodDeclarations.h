﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{be551f8d-aed3-403c-a1e7-93fc616e7213}/$ArrayType=16
struct U24ArrayTypeU3D16_t2457720008;
struct U24ArrayTypeU3D16_t2457720008_marshaled_pinvoke;
struct U24ArrayTypeU3D16_t2457720008_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct U24ArrayTypeU3D16_t2457720008;
struct U24ArrayTypeU3D16_t2457720008_marshaled_pinvoke;

extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_pinvoke(const U24ArrayTypeU3D16_t2457720008& unmarshaled, U24ArrayTypeU3D16_t2457720008_marshaled_pinvoke& marshaled);
extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_pinvoke_back(const U24ArrayTypeU3D16_t2457720008_marshaled_pinvoke& marshaled, U24ArrayTypeU3D16_t2457720008& unmarshaled);
extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_pinvoke_cleanup(U24ArrayTypeU3D16_t2457720008_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct U24ArrayTypeU3D16_t2457720008;
struct U24ArrayTypeU3D16_t2457720008_marshaled_com;

extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_com(const U24ArrayTypeU3D16_t2457720008& unmarshaled, U24ArrayTypeU3D16_t2457720008_marshaled_com& marshaled);
extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_com_back(const U24ArrayTypeU3D16_t2457720008_marshaled_com& marshaled, U24ArrayTypeU3D16_t2457720008& unmarshaled);
extern "C" void U24ArrayTypeU3D16_t2457720008_marshal_com_cleanup(U24ArrayTypeU3D16_t2457720008_marshaled_com& marshaled);
