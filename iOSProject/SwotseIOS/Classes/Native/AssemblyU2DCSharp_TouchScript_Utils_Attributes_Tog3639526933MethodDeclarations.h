﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.Attributes.ToggleLeftAttribute
struct ToggleLeftAttribute_t3639526933;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.Utils.Attributes.ToggleLeftAttribute::.ctor()
extern "C"  void ToggleLeftAttribute__ctor_m1079425430 (ToggleLeftAttribute_t3639526933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
