﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6
struct U3CGetBarbariansArmyU3Ec__Iterator6_t634682975;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::.ctor()
extern "C"  void U3CGetBarbariansArmyU3Ec__Iterator6__ctor_m2970055264 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetBarbariansArmyU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2090715482 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetBarbariansArmyU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1237499698 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::MoveNext()
extern "C"  bool U3CGetBarbariansArmyU3Ec__Iterator6_MoveNext_m701699228 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::Dispose()
extern "C"  void U3CGetBarbariansArmyU3Ec__Iterator6_Dispose_m2768291281 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarbarianRecruitManager/<GetBarbariansArmy>c__Iterator6::Reset()
extern "C"  void U3CGetBarbariansArmyU3Ec__Iterator6_Reset_m3559042371 (U3CGetBarbariansArmyU3Ec__Iterator6_t634682975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
