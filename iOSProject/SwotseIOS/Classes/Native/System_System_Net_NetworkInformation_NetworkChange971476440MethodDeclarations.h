﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.NetworkChange
struct NetworkChange_t971476440;
// System.Net.NetworkInformation.NetworkAddressChangedEventHandler
struct NetworkAddressChangedEventHandler_t2528401652;
// System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler
struct NetworkAvailabilityChangedEventHandler_t3480120991;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_NetworkAddres2528401652.h"
#include "System_System_Net_NetworkInformation_NetworkAvaila3480120991.h"

// System.Void System.Net.NetworkInformation.NetworkChange::.ctor()
extern "C"  void NetworkChange__ctor_m2342786506 (NetworkChange_t971476440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkChange::add_NetworkAddressChanged(System.Net.NetworkInformation.NetworkAddressChangedEventHandler)
extern "C"  void NetworkChange_add_NetworkAddressChanged_m1715626088 (Il2CppObject * __this /* static, unused */, NetworkAddressChangedEventHandler_t2528401652 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkChange::remove_NetworkAddressChanged(System.Net.NetworkInformation.NetworkAddressChangedEventHandler)
extern "C"  void NetworkChange_remove_NetworkAddressChanged_m1155329143 (Il2CppObject * __this /* static, unused */, NetworkAddressChangedEventHandler_t2528401652 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkChange::add_NetworkAvailabilityChanged(System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler)
extern "C"  void NetworkChange_add_NetworkAvailabilityChanged_m1510511970 (Il2CppObject * __this /* static, unused */, NetworkAvailabilityChangedEventHandler_t3480120991 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkChange::remove_NetworkAvailabilityChanged(System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler)
extern "C"  void NetworkChange_remove_NetworkAvailabilityChanged_m4109781723 (Il2CppObject * __this /* static, unused */, NetworkAvailabilityChangedEventHandler_t3480120991 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
