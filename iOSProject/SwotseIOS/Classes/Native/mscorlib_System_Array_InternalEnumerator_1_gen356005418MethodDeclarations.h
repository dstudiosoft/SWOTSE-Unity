﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen356005418.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23792220452.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2864334282_gshared (InternalEnumerator_1_t356005418 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2864334282(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t356005418 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2864334282_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1066663010_gshared (InternalEnumerator_1_t356005418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1066663010(__this, method) ((  void (*) (InternalEnumerator_1_t356005418 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1066663010_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4005906526_gshared (InternalEnumerator_1_t356005418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4005906526(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t356005418 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4005906526_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m135626755_gshared (InternalEnumerator_1_t356005418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m135626755(__this, method) ((  void (*) (InternalEnumerator_1_t356005418 *, const MethodInfo*))InternalEnumerator_1_Dispose_m135626755_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3850744818_gshared (InternalEnumerator_1_t356005418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3850744818(__this, method) ((  bool (*) (InternalEnumerator_1_t356005418 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3850744818_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>::get_Current()
extern "C"  KeyValuePair_2_t3792220452  InternalEnumerator_1_get_Current_m1613294627_gshared (InternalEnumerator_1_t356005418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1613294627(__this, method) ((  KeyValuePair_2_t3792220452  (*) (InternalEnumerator_1_t356005418 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1613294627_gshared)(__this, method)
