﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Firebase.Platform.IClockService
struct IClockService_t1265908309;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Action`1<System.Threading.Tasks.Task>
struct Action_1_t3359742907;
// Firebase.Platform.IAppConfigExtensions
struct IAppConfigExtensions_t2982493820;
// Firebase.Platform.IAuthService
struct IAuthService_t11025045;
// Firebase.Platform.ICertificateService
struct ICertificateService_t2118531980;
// Firebase.Platform.IHttpFactoryService
struct IHttpFactoryService_t1869123900;
// Firebase.Platform.ILoggingService
struct ILoggingService_t1251626632;
// Firebase.FutureVoid
struct FutureVoid_t983301800;
// System.Threading.Tasks.TaskCompletionSource`1<System.Int32>
struct TaskCompletionSource_1_t1351668570;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t521420319;
// Firebase.AppOptions
struct AppOptions_t2535978413;
// Firebase.StringStringMap
struct StringStringMap_t4119824414;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t3662770472;
// Firebase.StringList
struct StringList_t2653214935;
// System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>
struct Tuple_2_t3253106730;
// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t1466989729;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1283030885;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Collections.Generic.Dictionary`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>
struct Dictionary_2_t1342557839;
// Firebase.CharVector
struct CharVector_t1896987243;
// Firebase.FutureString
struct FutureString_t4072148675;
// System.Threading.Tasks.TaskCompletionSource`1<System.String>
struct TaskCompletionSource_1_t248173506;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t467131861;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t1533780051;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1372108196;
// Firebase.AppUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1143654535;
// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t3256258918;
// System.Exception
struct Exception_t;
// System.Action
struct Action_t1264377477;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t2326897723;
// System.Threading.ThreadLocal`1<System.Int32>
struct ThreadLocal_1_t558034686;
// System.Action`1<System.Action>
struct Action_1_t1436845072;
// System.Collections.Generic.IList`1<System.Action`1<System.Threading.Tasks.Task>>
struct IList_1_t880095394;
// System.AggregateException
struct AggregateException_t3586243216;
// Firebase.VariantVariantMap
struct VariantVariantMap_t4053535076;
// System.Collections.Generic.IList`1<Firebase.Variant>
struct IList_1_t2979228591;
// Firebase.VariantList
struct VariantList_t2751083993;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t540272775;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception>
struct ReadOnlyCollection_1_t2649313536;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>
struct Queue_1_t3099366224;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent>
struct Dictionary_2_t3634922637;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t1196198384;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>
struct Dictionary_2_t2311544709;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>
struct Dictionary_2_t748154129;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t1172811472;
// Firebase.FirebaseApp/DestroyDelegate
struct DestroyDelegate_t2525580959;
// Firebase.FirebaseApp/CreateDelegate
struct CreateDelegate_t3131870060;
// System.Func`1<Firebase.DependencyStatus>
struct Func_1_t2062041240;
// System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Threading.Tasks.Task`1<Firebase.DependencyStatus>>
struct Func_2_t680774166;
// System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>
struct Func_2_t724047911;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureString/Action>
struct Dictionary_2_t1596674066;
// Firebase.FutureString/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t3186567461;
// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureVoid/Action>
struct Dictionary_2_t133523557;
// Firebase.FutureVoid/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t2224793779;
// Firebase.FirebaseApp
struct FirebaseApp_t2526288410;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.EventHandler`1<Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs>
struct EventHandler_1_t1880246940;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t657899700;




#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef SYSTEMCLOCK_T2480061955_H
#define SYSTEMCLOCK_T2480061955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.Default.SystemClock
struct  SystemClock_t2480061955  : public RuntimeObject
{
public:

public:
};

struct SystemClock_t2480061955_StaticFields
{
public:
	// Firebase.Platform.IClockService Firebase.Platform.Default.SystemClock::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(SystemClock_t2480061955_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMCLOCK_T2480061955_H
#ifndef U3CFIXDEPENDENCIESASYNCU3EC__ANONSTOREY6_T3943580417_H
#define U3CFIXDEPENDENCIESASYNCU3EC__ANONSTOREY6_T3943580417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6
struct  U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417  : public RuntimeObject
{
public:
	// System.Threading.Tasks.Task Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6::task
	Task_t3187275312 * ___task_0;

public:
	inline static int32_t get_offset_of_task_0() { return static_cast<int32_t>(offsetof(U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417, ___task_0)); }
	inline Task_t3187275312 * get_task_0() const { return ___task_0; }
	inline Task_t3187275312 ** get_address_of_task_0() { return &___task_0; }
	inline void set_task_0(Task_t3187275312 * value)
	{
		___task_0 = value;
		Il2CppCodeGenWriteBarrier((&___task_0), value);
	}
};

struct U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417_StaticFields
{
public:
	// System.Action`1<System.Threading.Tasks.Task> Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey6::<>f__am$cache0
	Action_1_t3359742907 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t3359742907 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t3359742907 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t3359742907 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFIXDEPENDENCIESASYNCU3EC__ANONSTOREY6_T3943580417_H
#ifndef NOOPCERTIFICATESERVICE_T402701936_H
#define NOOPCERTIFICATESERVICE_T402701936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.NoopCertificateService
struct  NoopCertificateService_t402701936  : public RuntimeObject
{
public:

public:
};

struct NoopCertificateService_t402701936_StaticFields
{
public:
	// Firebase.Platform.NoopCertificateService Firebase.Platform.NoopCertificateService::_instance
	NoopCertificateService_t402701936 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(NoopCertificateService_t402701936_StaticFields, ____instance_0)); }
	inline NoopCertificateService_t402701936 * get__instance_0() const { return ____instance_0; }
	inline NoopCertificateService_t402701936 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(NoopCertificateService_t402701936 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOPCERTIFICATESERVICE_T402701936_H
#ifndef DEBUGLOGGER_T16656067_H
#define DEBUGLOGGER_T16656067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.DebugLogger
struct  DebugLogger_t16656067  : public RuntimeObject
{
public:

public:
};

struct DebugLogger_t16656067_StaticFields
{
public:
	// Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::_instance
	DebugLogger_t16656067 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DebugLogger_t16656067_StaticFields, ____instance_0)); }
	inline DebugLogger_t16656067 * get__instance_0() const { return ____instance_0; }
	inline DebugLogger_t16656067 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DebugLogger_t16656067 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGGER_T16656067_H
#ifndef SERVICES_T463074608_H
#define SERVICES_T463074608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.Services
struct  Services_t463074608  : public RuntimeObject
{
public:

public:
};

struct Services_t463074608_StaticFields
{
public:
	// Firebase.Platform.IAppConfigExtensions Firebase.Platform.Services::<AppConfig>k__BackingField
	RuntimeObject* ___U3CAppConfigU3Ek__BackingField_0;
	// Firebase.Platform.IAuthService Firebase.Platform.Services::<Auth>k__BackingField
	RuntimeObject* ___U3CAuthU3Ek__BackingField_1;
	// Firebase.Platform.ICertificateService Firebase.Platform.Services::<RootCerts>k__BackingField
	RuntimeObject* ___U3CRootCertsU3Ek__BackingField_2;
	// Firebase.Platform.IClockService Firebase.Platform.Services::<Clock>k__BackingField
	RuntimeObject* ___U3CClockU3Ek__BackingField_3;
	// Firebase.Platform.IHttpFactoryService Firebase.Platform.Services::<HttpFactory>k__BackingField
	RuntimeObject* ___U3CHttpFactoryU3Ek__BackingField_4;
	// Firebase.Platform.ILoggingService Firebase.Platform.Services::<Logging>k__BackingField
	RuntimeObject* ___U3CLoggingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CAppConfigU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CAppConfigU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CAppConfigU3Ek__BackingField_0() const { return ___U3CAppConfigU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CAppConfigU3Ek__BackingField_0() { return &___U3CAppConfigU3Ek__BackingField_0; }
	inline void set_U3CAppConfigU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CAppConfigU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppConfigU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAuthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CAuthU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CAuthU3Ek__BackingField_1() const { return ___U3CAuthU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CAuthU3Ek__BackingField_1() { return &___U3CAuthU3Ek__BackingField_1; }
	inline void set_U3CAuthU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CAuthU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRootCertsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CRootCertsU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CRootCertsU3Ek__BackingField_2() const { return ___U3CRootCertsU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CRootCertsU3Ek__BackingField_2() { return &___U3CRootCertsU3Ek__BackingField_2; }
	inline void set_U3CRootCertsU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CRootCertsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootCertsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CClockU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CClockU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CClockU3Ek__BackingField_3() const { return ___U3CClockU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CClockU3Ek__BackingField_3() { return &___U3CClockU3Ek__BackingField_3; }
	inline void set_U3CClockU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CClockU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClockU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CHttpFactoryU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CHttpFactoryU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CHttpFactoryU3Ek__BackingField_4() const { return ___U3CHttpFactoryU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CHttpFactoryU3Ek__BackingField_4() { return &___U3CHttpFactoryU3Ek__BackingField_4; }
	inline void set_U3CHttpFactoryU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CHttpFactoryU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHttpFactoryU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Services_t463074608_StaticFields, ___U3CLoggingU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CLoggingU3Ek__BackingField_5() const { return ___U3CLoggingU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CLoggingU3Ek__BackingField_5() { return &___U3CLoggingU3Ek__BackingField_5; }
	inline void set_U3CLoggingU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CLoggingU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggingU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICES_T463074608_H
#ifndef BASEAUTHSERVICE_T3388892161_H
#define BASEAUTHSERVICE_T3388892161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.Default.BaseAuthService
struct  BaseAuthService_t3388892161  : public RuntimeObject
{
public:

public:
};

struct BaseAuthService_t3388892161_StaticFields
{
public:
	// Firebase.Platform.Default.BaseAuthService Firebase.Platform.Default.BaseAuthService::_instance
	BaseAuthService_t3388892161 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(BaseAuthService_t3388892161_StaticFields, ____instance_0)); }
	inline BaseAuthService_t3388892161 * get__instance_0() const { return ____instance_0; }
	inline BaseAuthService_t3388892161 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(BaseAuthService_t3388892161 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEAUTHSERVICE_T3388892161_H
#ifndef U3CGETTASKU3EC__ANONSTOREY0_T4029664161_H
#define U3CGETTASKU3EC__ANONSTOREY0_T4029664161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureVoid/<GetTask>c__AnonStorey0
struct  U3CGetTaskU3Ec__AnonStorey0_t4029664161  : public RuntimeObject
{
public:
	// Firebase.FutureVoid Firebase.FutureVoid/<GetTask>c__AnonStorey0::fu
	FutureVoid_t983301800 * ___fu_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Int32> Firebase.FutureVoid/<GetTask>c__AnonStorey0::tcs
	TaskCompletionSource_1_t1351668570 * ___tcs_1;

public:
	inline static int32_t get_offset_of_fu_0() { return static_cast<int32_t>(offsetof(U3CGetTaskU3Ec__AnonStorey0_t4029664161, ___fu_0)); }
	inline FutureVoid_t983301800 * get_fu_0() const { return ___fu_0; }
	inline FutureVoid_t983301800 ** get_address_of_fu_0() { return &___fu_0; }
	inline void set_fu_0(FutureVoid_t983301800 * value)
	{
		___fu_0 = value;
		Il2CppCodeGenWriteBarrier((&___fu_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CGetTaskU3Ec__AnonStorey0_t4029664161, ___tcs_1)); }
	inline TaskCompletionSource_1_t1351668570 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t1351668570 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t1351668570 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTASKU3EC__ANONSTOREY0_T4029664161_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef APPCONFIGEXTENSIONS_T2108028355_H
#define APPCONFIGEXTENSIONS_T2108028355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.Default.AppConfigExtensions
struct  AppConfigExtensions_t2108028355  : public RuntimeObject
{
public:

public:
};

struct AppConfigExtensions_t2108028355_StaticFields
{
public:
	// System.Uri Firebase.Platform.Default.AppConfigExtensions::DefaultUpdateUrl
	Uri_t100236324 * ___DefaultUpdateUrl_0;
	// System.String Firebase.Platform.Default.AppConfigExtensions::Default
	String_t* ___Default_1;
	// System.Object Firebase.Platform.Default.AppConfigExtensions::Sync
	RuntimeObject * ___Sync_2;
	// Firebase.Platform.Default.AppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::_instance
	AppConfigExtensions_t2108028355 * ____instance_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>> Firebase.Platform.Default.AppConfigExtensions::SStringState
	Dictionary_2_t521420319 * ___SStringState_4;

public:
	inline static int32_t get_offset_of_DefaultUpdateUrl_0() { return static_cast<int32_t>(offsetof(AppConfigExtensions_t2108028355_StaticFields, ___DefaultUpdateUrl_0)); }
	inline Uri_t100236324 * get_DefaultUpdateUrl_0() const { return ___DefaultUpdateUrl_0; }
	inline Uri_t100236324 ** get_address_of_DefaultUpdateUrl_0() { return &___DefaultUpdateUrl_0; }
	inline void set_DefaultUpdateUrl_0(Uri_t100236324 * value)
	{
		___DefaultUpdateUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultUpdateUrl_0), value);
	}

	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(AppConfigExtensions_t2108028355_StaticFields, ___Default_1)); }
	inline String_t* get_Default_1() const { return ___Default_1; }
	inline String_t** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(String_t* value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}

	inline static int32_t get_offset_of_Sync_2() { return static_cast<int32_t>(offsetof(AppConfigExtensions_t2108028355_StaticFields, ___Sync_2)); }
	inline RuntimeObject * get_Sync_2() const { return ___Sync_2; }
	inline RuntimeObject ** get_address_of_Sync_2() { return &___Sync_2; }
	inline void set_Sync_2(RuntimeObject * value)
	{
		___Sync_2 = value;
		Il2CppCodeGenWriteBarrier((&___Sync_2), value);
	}

	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(AppConfigExtensions_t2108028355_StaticFields, ____instance_3)); }
	inline AppConfigExtensions_t2108028355 * get__instance_3() const { return ____instance_3; }
	inline AppConfigExtensions_t2108028355 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(AppConfigExtensions_t2108028355 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier((&____instance_3), value);
	}

	inline static int32_t get_offset_of_SStringState_4() { return static_cast<int32_t>(offsetof(AppConfigExtensions_t2108028355_StaticFields, ___SStringState_4)); }
	inline Dictionary_2_t521420319 * get_SStringState_4() const { return ___SStringState_4; }
	inline Dictionary_2_t521420319 ** get_address_of_SStringState_4() { return &___SStringState_4; }
	inline void set_SStringState_4(Dictionary_2_t521420319 * value)
	{
		___SStringState_4 = value;
		Il2CppCodeGenWriteBarrier((&___SStringState_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPCONFIGEXTENSIONS_T2108028355_H
#ifndef U3CCREATEU3EC__ANONSTOREY1_T2838696591_H
#define U3CCREATEU3EC__ANONSTOREY1_T2838696591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<Create>c__AnonStorey1
struct  U3CCreateU3Ec__AnonStorey1_t2838696591  : public RuntimeObject
{
public:
	// Firebase.AppOptions Firebase.FirebaseApp/<Create>c__AnonStorey1::options
	AppOptions_t2535978413 * ___options_0;
	// System.String Firebase.FirebaseApp/<Create>c__AnonStorey1::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_options_0() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey1_t2838696591, ___options_0)); }
	inline AppOptions_t2535978413 * get_options_0() const { return ___options_0; }
	inline AppOptions_t2535978413 ** get_address_of_options_0() { return &___options_0; }
	inline void set_options_0(AppOptions_t2535978413 * value)
	{
		___options_0 = value;
		Il2CppCodeGenWriteBarrier((&___options_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey1_t2838696591, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEU3EC__ANONSTOREY1_T2838696591_H
#ifndef U3CCREATEU3EC__ANONSTOREY0_T109813236_H
#define U3CCREATEU3EC__ANONSTOREY0_T109813236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<Create>c__AnonStorey0
struct  U3CCreateU3Ec__AnonStorey0_t109813236  : public RuntimeObject
{
public:
	// Firebase.AppOptions Firebase.FirebaseApp/<Create>c__AnonStorey0::options
	AppOptions_t2535978413 * ___options_0;

public:
	inline static int32_t get_offset_of_options_0() { return static_cast<int32_t>(offsetof(U3CCreateU3Ec__AnonStorey0_t109813236, ___options_0)); }
	inline AppOptions_t2535978413 * get_options_0() const { return ___options_0; }
	inline AppOptions_t2535978413 ** get_address_of_options_0() { return &___options_0; }
	inline void set_options_0(AppOptions_t2535978413 * value)
	{
		___options_0 = value;
		Il2CppCodeGenWriteBarrier((&___options_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEU3EC__ANONSTOREY0_T109813236_H
#ifndef UNITYHTTPFACTORYSERVICE_T3235819319_H
#define UNITYHTTPFACTORYSERVICE_T3235819319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnityHttpFactoryService
struct  UnityHttpFactoryService_t3235819319  : public RuntimeObject
{
public:

public:
};

struct UnityHttpFactoryService_t3235819319_StaticFields
{
public:
	// Firebase.Unity.UnityHttpFactoryService Firebase.Unity.UnityHttpFactoryService::_instance
	UnityHttpFactoryService_t3235819319 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(UnityHttpFactoryService_t3235819319_StaticFields, ____instance_0)); }
	inline UnityHttpFactoryService_t3235819319 * get__instance_0() const { return ____instance_0; }
	inline UnityHttpFactoryService_t3235819319 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(UnityHttpFactoryService_t3235819319 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYHTTPFACTORYSERVICE_T3235819319_H
#ifndef STRINGSTRINGMAPENUMERATOR_T2843440744_H
#define STRINGSTRINGMAPENUMERATOR_T2843440744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.StringStringMap/StringStringMapEnumerator
struct  StringStringMapEnumerator_t2843440744  : public RuntimeObject
{
public:
	// Firebase.StringStringMap Firebase.StringStringMap/StringStringMapEnumerator::collectionRef
	StringStringMap_t4119824414 * ___collectionRef_0;
	// System.Collections.Generic.IList`1<System.String> Firebase.StringStringMap/StringStringMapEnumerator::keyCollection
	RuntimeObject* ___keyCollection_1;
	// System.Int32 Firebase.StringStringMap/StringStringMapEnumerator::currentIndex
	int32_t ___currentIndex_2;
	// System.Object Firebase.StringStringMap/StringStringMapEnumerator::currentObject
	RuntimeObject * ___currentObject_3;
	// System.Int32 Firebase.StringStringMap/StringStringMapEnumerator::currentSize
	int32_t ___currentSize_4;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(StringStringMapEnumerator_t2843440744, ___collectionRef_0)); }
	inline StringStringMap_t4119824414 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline StringStringMap_t4119824414 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(StringStringMap_t4119824414 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRef_0), value);
	}

	inline static int32_t get_offset_of_keyCollection_1() { return static_cast<int32_t>(offsetof(StringStringMapEnumerator_t2843440744, ___keyCollection_1)); }
	inline RuntimeObject* get_keyCollection_1() const { return ___keyCollection_1; }
	inline RuntimeObject** get_address_of_keyCollection_1() { return &___keyCollection_1; }
	inline void set_keyCollection_1(RuntimeObject* value)
	{
		___keyCollection_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyCollection_1), value);
	}

	inline static int32_t get_offset_of_currentIndex_2() { return static_cast<int32_t>(offsetof(StringStringMapEnumerator_t2843440744, ___currentIndex_2)); }
	inline int32_t get_currentIndex_2() const { return ___currentIndex_2; }
	inline int32_t* get_address_of_currentIndex_2() { return &___currentIndex_2; }
	inline void set_currentIndex_2(int32_t value)
	{
		___currentIndex_2 = value;
	}

	inline static int32_t get_offset_of_currentObject_3() { return static_cast<int32_t>(offsetof(StringStringMapEnumerator_t2843440744, ___currentObject_3)); }
	inline RuntimeObject * get_currentObject_3() const { return ___currentObject_3; }
	inline RuntimeObject ** get_address_of_currentObject_3() { return &___currentObject_3; }
	inline void set_currentObject_3(RuntimeObject * value)
	{
		___currentObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_3), value);
	}

	inline static int32_t get_offset_of_currentSize_4() { return static_cast<int32_t>(offsetof(StringStringMapEnumerator_t2843440744, ___currentSize_4)); }
	inline int32_t get_currentSize_4() const { return ___currentSize_4; }
	inline int32_t* get_address_of_currentSize_4() { return &___currentSize_4; }
	inline void set_currentSize_4(int32_t value)
	{
		___currentSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSTRINGMAPENUMERATOR_T2843440744_H
#ifndef U3CSENDCOROUTINEU3EC__ANONSTOREY2_T2871068643_H
#define U3CSENDCOROUTINEU3EC__ANONSTOREY2_T2871068643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct  U3CSendCoroutineU3Ec__AnonStorey2_t2871068643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDCOROUTINEU3EC__ANONSTOREY2_T2871068643_H
#ifndef STRINGLISTENUMERATOR_T435309527_H
#define STRINGLISTENUMERATOR_T435309527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.StringList/StringListEnumerator
struct  StringListEnumerator_t435309527  : public RuntimeObject
{
public:
	// Firebase.StringList Firebase.StringList/StringListEnumerator::collectionRef
	StringList_t2653214935 * ___collectionRef_0;
	// System.Int32 Firebase.StringList/StringListEnumerator::currentIndex
	int32_t ___currentIndex_1;
	// System.Object Firebase.StringList/StringListEnumerator::currentObject
	RuntimeObject * ___currentObject_2;
	// System.Int32 Firebase.StringList/StringListEnumerator::currentSize
	int32_t ___currentSize_3;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(StringListEnumerator_t435309527, ___collectionRef_0)); }
	inline StringList_t2653214935 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline StringList_t2653214935 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(StringList_t2653214935 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRef_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(StringListEnumerator_t435309527, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}

	inline static int32_t get_offset_of_currentObject_2() { return static_cast<int32_t>(offsetof(StringListEnumerator_t435309527, ___currentObject_2)); }
	inline RuntimeObject * get_currentObject_2() const { return ___currentObject_2; }
	inline RuntimeObject ** get_address_of_currentObject_2() { return &___currentObject_2; }
	inline void set_currentObject_2(RuntimeObject * value)
	{
		___currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_2), value);
	}

	inline static int32_t get_offset_of_currentSize_3() { return static_cast<int32_t>(offsetof(StringListEnumerator_t435309527, ___currentSize_3)); }
	inline int32_t get_currentSize_3() const { return ___currentSize_3; }
	inline int32_t* get_address_of_currentSize_3() { return &___currentSize_3; }
	inline void set_currentSize_3(int32_t value)
	{
		___currentSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGLISTENUMERATOR_T435309527_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1125778690_H
#define U3CSTARTU3EC__ITERATOR0_T1125778690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1125778690  : public RuntimeObject
{
public:
	// System.Tuple`2<System.Threading.SendOrPostCallback,System.Object> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::<entry>__0
	Tuple_2_t3253106730 * ___U3CentryU3E__0_0;
	// System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$locvar0
	RuntimeObject * ___U24locvar0_1;
	// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$this
	SynchronizationContextBehavoir_t1466989729 * ___U24this_2;
	// System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CentryU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U3CentryU3E__0_0)); }
	inline Tuple_2_t3253106730 * get_U3CentryU3E__0_0() const { return ___U3CentryU3E__0_0; }
	inline Tuple_2_t3253106730 ** get_address_of_U3CentryU3E__0_0() { return &___U3CentryU3E__0_0; }
	inline void set_U3CentryU3E__0_0(Tuple_2_t3253106730 * value)
	{
		___U3CentryU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentryU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U24locvar0_1)); }
	inline RuntimeObject * get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject ** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject * value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U24this_2)); }
	inline SynchronizationContextBehavoir_t1466989729 * get_U24this_2() const { return ___U24this_2; }
	inline SynchronizationContextBehavoir_t1466989729 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SynchronizationContextBehavoir_t1466989729 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1125778690, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1125778690_H
#ifndef U3CSIGNALEDCOROUTINEU3EC__ITERATOR0_T4124630109_H
#define U3CSIGNALEDCOROUTINEU3EC__ITERATOR0_T4124630109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0
struct  U3CSignaledCoroutineU3Ec__Iterator0_t4124630109  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::coroutine
	Func_1_t1283030885 * ___coroutine_0;
	// System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::newSignal
	ManualResetEvent_t451242010 * ___newSignal_1;
	// System.Object Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_t4124630109, ___coroutine_0)); }
	inline Func_1_t1283030885 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_1_t1283030885 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_1_t1283030885 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_newSignal_1() { return static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_t4124630109, ___newSignal_1)); }
	inline ManualResetEvent_t451242010 * get_newSignal_1() const { return ___newSignal_1; }
	inline ManualResetEvent_t451242010 ** get_address_of_newSignal_1() { return &___newSignal_1; }
	inline void set_newSignal_1(ManualResetEvent_t451242010 * value)
	{
		___newSignal_1 = value;
		Il2CppCodeGenWriteBarrier((&___newSignal_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_t4124630109, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_t4124630109, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_t4124630109, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIGNALEDCOROUTINEU3EC__ITERATOR0_T4124630109_H
#ifndef U3CSENDCOROUTINEU3EC__ANONSTOREY1_T914753507_H
#define U3CSENDCOROUTINEU3EC__ANONSTOREY1_T914753507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct  U3CSendCoroutineU3Ec__AnonStorey1_t914753507  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDCOROUTINEU3EC__ANONSTOREY1_T914753507_H
#ifndef U3CSENDU3EC__ANONSTOREY3_T2455143249_H
#define U3CSENDU3EC__ANONSTOREY3_T2455143249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3
struct  U3CSendU3Ec__AnonStorey3_t2455143249  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDU3EC__ANONSTOREY3_T2455143249_H
#ifndef UNITYPLATFORMSERVICES_T2540433941_H
#define UNITYPLATFORMSERVICES_T2540433941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnityPlatformServices
struct  UnityPlatformServices_t2540433941  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPLATFORMSERVICES_T2540433941_H
#ifndef UNITYLOGGINGSERVICE_T1821497488_H
#define UNITYLOGGINGSERVICE_T1821497488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnityLoggingService
struct  UnityLoggingService_t1821497488  : public RuntimeObject
{
public:

public:
};

struct UnityLoggingService_t1821497488_StaticFields
{
public:
	// Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::_instance
	UnityLoggingService_t1821497488 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(UnityLoggingService_t1821497488_StaticFields, ____instance_0)); }
	inline UnityLoggingService_t1821497488 * get__instance_0() const { return ____instance_0; }
	inline UnityLoggingService_t1821497488 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(UnityLoggingService_t1821497488 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYLOGGINGSERVICE_T1821497488_H
#ifndef INSTALLROOTCERTS_T2034015421_H
#define INSTALLROOTCERTS_T2034015421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.InstallRootCerts
struct  InstallRootCerts_t2034015421  : public RuntimeObject
{
public:
	// System.Boolean Firebase.Unity.InstallRootCerts::_needsCertificateWorkaround
	bool ____needsCertificateWorkaround_3;

public:
	inline static int32_t get_offset_of__needsCertificateWorkaround_3() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421, ____needsCertificateWorkaround_3)); }
	inline bool get__needsCertificateWorkaround_3() const { return ____needsCertificateWorkaround_3; }
	inline bool* get_address_of__needsCertificateWorkaround_3() { return &____needsCertificateWorkaround_3; }
	inline void set__needsCertificateWorkaround_3(bool value)
	{
		____needsCertificateWorkaround_3 = value;
	}
};

struct InstallRootCerts_t2034015421_StaticFields
{
public:
	// System.Object Firebase.Unity.InstallRootCerts::Sync
	RuntimeObject * ___Sync_0;
	// System.Collections.Generic.Dictionary`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection> Firebase.Unity.InstallRootCerts::_installedRoots
	Dictionary_2_t1342557839 * ____installedRoots_1;
	// Firebase.Unity.InstallRootCerts Firebase.Unity.InstallRootCerts::_instance
	InstallRootCerts_t2034015421 * ____instance_2;
	// System.String Firebase.Unity.InstallRootCerts::TrustedRoot
	String_t* ___TrustedRoot_4;
	// System.String Firebase.Unity.InstallRootCerts::IntermediateCA
	String_t* ___IntermediateCA_5;

public:
	inline static int32_t get_offset_of_Sync_0() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421_StaticFields, ___Sync_0)); }
	inline RuntimeObject * get_Sync_0() const { return ___Sync_0; }
	inline RuntimeObject ** get_address_of_Sync_0() { return &___Sync_0; }
	inline void set_Sync_0(RuntimeObject * value)
	{
		___Sync_0 = value;
		Il2CppCodeGenWriteBarrier((&___Sync_0), value);
	}

	inline static int32_t get_offset_of__installedRoots_1() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421_StaticFields, ____installedRoots_1)); }
	inline Dictionary_2_t1342557839 * get__installedRoots_1() const { return ____installedRoots_1; }
	inline Dictionary_2_t1342557839 ** get_address_of__installedRoots_1() { return &____installedRoots_1; }
	inline void set__installedRoots_1(Dictionary_2_t1342557839 * value)
	{
		____installedRoots_1 = value;
		Il2CppCodeGenWriteBarrier((&____installedRoots_1), value);
	}

	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421_StaticFields, ____instance_2)); }
	inline InstallRootCerts_t2034015421 * get__instance_2() const { return ____instance_2; }
	inline InstallRootCerts_t2034015421 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(InstallRootCerts_t2034015421 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_TrustedRoot_4() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421_StaticFields, ___TrustedRoot_4)); }
	inline String_t* get_TrustedRoot_4() const { return ___TrustedRoot_4; }
	inline String_t** get_address_of_TrustedRoot_4() { return &___TrustedRoot_4; }
	inline void set_TrustedRoot_4(String_t* value)
	{
		___TrustedRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&___TrustedRoot_4), value);
	}

	inline static int32_t get_offset_of_IntermediateCA_5() { return static_cast<int32_t>(offsetof(InstallRootCerts_t2034015421_StaticFields, ___IntermediateCA_5)); }
	inline String_t* get_IntermediateCA_5() const { return ___IntermediateCA_5; }
	inline String_t** get_address_of_IntermediateCA_5() { return &___IntermediateCA_5; }
	inline void set_IntermediateCA_5(String_t* value)
	{
		___IntermediateCA_5 = value;
		Il2CppCodeGenWriteBarrier((&___IntermediateCA_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLROOTCERTS_T2034015421_H
#ifndef CHARVECTORENUMERATOR_T1643981309_H
#define CHARVECTORENUMERATOR_T1643981309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.CharVector/CharVectorEnumerator
struct  CharVectorEnumerator_t1643981309  : public RuntimeObject
{
public:
	// Firebase.CharVector Firebase.CharVector/CharVectorEnumerator::collectionRef
	CharVector_t1896987243 * ___collectionRef_0;
	// System.Int32 Firebase.CharVector/CharVectorEnumerator::currentIndex
	int32_t ___currentIndex_1;
	// System.Object Firebase.CharVector/CharVectorEnumerator::currentObject
	RuntimeObject * ___currentObject_2;
	// System.Int32 Firebase.CharVector/CharVectorEnumerator::currentSize
	int32_t ___currentSize_3;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(CharVectorEnumerator_t1643981309, ___collectionRef_0)); }
	inline CharVector_t1896987243 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline CharVector_t1896987243 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(CharVector_t1896987243 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRef_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(CharVectorEnumerator_t1643981309, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}

	inline static int32_t get_offset_of_currentObject_2() { return static_cast<int32_t>(offsetof(CharVectorEnumerator_t1643981309, ___currentObject_2)); }
	inline RuntimeObject * get_currentObject_2() const { return ___currentObject_2; }
	inline RuntimeObject ** get_address_of_currentObject_2() { return &___currentObject_2; }
	inline void set_currentObject_2(RuntimeObject * value)
	{
		___currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_2), value);
	}

	inline static int32_t get_offset_of_currentSize_3() { return static_cast<int32_t>(offsetof(CharVectorEnumerator_t1643981309, ___currentSize_3)); }
	inline int32_t get_currentSize_3() const { return ___currentSize_3; }
	inline int32_t* get_address_of_currentSize_3() { return &___currentSize_3; }
	inline void set_currentSize_3(int32_t value)
	{
		___currentSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARVECTORENUMERATOR_T1643981309_H
#ifndef U3CSENDU3EC__ANONSTOREY4_T2051858722_H
#define U3CSENDU3EC__ANONSTOREY4_T2051858722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4
struct  U3CSendU3Ec__AnonStorey4_t2051858722  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDU3EC__ANONSTOREY4_T2051858722_H
#ifndef U3CGETTASKU3EC__ANONSTOREY0_T1537100348_H
#define U3CGETTASKU3EC__ANONSTOREY0_T1537100348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureString/<GetTask>c__AnonStorey0
struct  U3CGetTaskU3Ec__AnonStorey0_t1537100348  : public RuntimeObject
{
public:
	// Firebase.FutureString Firebase.FutureString/<GetTask>c__AnonStorey0::fu
	FutureString_t4072148675 * ___fu_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.String> Firebase.FutureString/<GetTask>c__AnonStorey0::tcs
	TaskCompletionSource_1_t248173506 * ___tcs_1;

public:
	inline static int32_t get_offset_of_fu_0() { return static_cast<int32_t>(offsetof(U3CGetTaskU3Ec__AnonStorey0_t1537100348, ___fu_0)); }
	inline FutureString_t4072148675 * get_fu_0() const { return ___fu_0; }
	inline FutureString_t4072148675 ** get_address_of_fu_0() { return &___fu_0; }
	inline void set_fu_0(FutureString_t4072148675 * value)
	{
		___fu_0 = value;
		Il2CppCodeGenWriteBarrier((&___fu_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CGetTaskU3Ec__AnonStorey0_t1537100348, ___tcs_1)); }
	inline TaskCompletionSource_1_t248173506 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t248173506 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t248173506 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTASKU3EC__ANONSTOREY0_T1537100348_H
#ifndef SYNCHRONIZATIONCONTEXT_T2326897723_H
#define SYNCHRONIZATIONCONTEXT_T2326897723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t2326897723  : public RuntimeObject
{
public:

public:
};

struct SynchronizationContext_t2326897723_ThreadStaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t2326897723 * ___currentContext_0;

public:
	inline static int32_t get_offset_of_currentContext_0() { return static_cast<int32_t>(offsetof(SynchronizationContext_t2326897723_ThreadStaticFields, ___currentContext_0)); }
	inline SynchronizationContext_t2326897723 * get_currentContext_0() const { return ___currentContext_0; }
	inline SynchronizationContext_t2326897723 ** get_address_of_currentContext_0() { return &___currentContext_0; }
	inline void set_currentContext_0(SynchronizationContext_t2326897723 * value)
	{
		___currentContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZATIONCONTEXT_T2326897723_H
#ifndef SWIGEXCEPTIONHELPER_T1372108196_H
#define SWIGEXCEPTIONHELPER_T1372108196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGExceptionHelper
struct  SWIGExceptionHelper_t1372108196  : public RuntimeObject
{
public:

public:
};

struct SWIGExceptionHelper_t1372108196_StaticFields
{
public:
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t467131861 * ___applicationDelegate_0;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t467131861 * ___arithmeticDelegate_1;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t467131861 * ___divideByZeroDelegate_2;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t467131861 * ___indexOutOfRangeDelegate_3;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t467131861 * ___invalidCastDelegate_4;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t467131861 * ___invalidOperationDelegate_5;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t467131861 * ___ioDelegate_6;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t467131861 * ___nullReferenceDelegate_7;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t467131861 * ___outOfMemoryDelegate_8;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t467131861 * ___overflowDelegate_9;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t467131861 * ___systemDelegate_10;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_t1533780051 * ___argumentDelegate_11;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_t1533780051 * ___argumentNullDelegate_12;
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.AppUtilPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_t1533780051 * ___argumentOutOfRangeDelegate_13;

public:
	inline static int32_t get_offset_of_applicationDelegate_0() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___applicationDelegate_0)); }
	inline ExceptionDelegate_t467131861 * get_applicationDelegate_0() const { return ___applicationDelegate_0; }
	inline ExceptionDelegate_t467131861 ** get_address_of_applicationDelegate_0() { return &___applicationDelegate_0; }
	inline void set_applicationDelegate_0(ExceptionDelegate_t467131861 * value)
	{
		___applicationDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___applicationDelegate_0), value);
	}

	inline static int32_t get_offset_of_arithmeticDelegate_1() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___arithmeticDelegate_1)); }
	inline ExceptionDelegate_t467131861 * get_arithmeticDelegate_1() const { return ___arithmeticDelegate_1; }
	inline ExceptionDelegate_t467131861 ** get_address_of_arithmeticDelegate_1() { return &___arithmeticDelegate_1; }
	inline void set_arithmeticDelegate_1(ExceptionDelegate_t467131861 * value)
	{
		___arithmeticDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___arithmeticDelegate_1), value);
	}

	inline static int32_t get_offset_of_divideByZeroDelegate_2() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___divideByZeroDelegate_2)); }
	inline ExceptionDelegate_t467131861 * get_divideByZeroDelegate_2() const { return ___divideByZeroDelegate_2; }
	inline ExceptionDelegate_t467131861 ** get_address_of_divideByZeroDelegate_2() { return &___divideByZeroDelegate_2; }
	inline void set_divideByZeroDelegate_2(ExceptionDelegate_t467131861 * value)
	{
		___divideByZeroDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___divideByZeroDelegate_2), value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeDelegate_3() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___indexOutOfRangeDelegate_3)); }
	inline ExceptionDelegate_t467131861 * get_indexOutOfRangeDelegate_3() const { return ___indexOutOfRangeDelegate_3; }
	inline ExceptionDelegate_t467131861 ** get_address_of_indexOutOfRangeDelegate_3() { return &___indexOutOfRangeDelegate_3; }
	inline void set_indexOutOfRangeDelegate_3(ExceptionDelegate_t467131861 * value)
	{
		___indexOutOfRangeDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRangeDelegate_3), value);
	}

	inline static int32_t get_offset_of_invalidCastDelegate_4() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___invalidCastDelegate_4)); }
	inline ExceptionDelegate_t467131861 * get_invalidCastDelegate_4() const { return ___invalidCastDelegate_4; }
	inline ExceptionDelegate_t467131861 ** get_address_of_invalidCastDelegate_4() { return &___invalidCastDelegate_4; }
	inline void set_invalidCastDelegate_4(ExceptionDelegate_t467131861 * value)
	{
		___invalidCastDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidCastDelegate_4), value);
	}

	inline static int32_t get_offset_of_invalidOperationDelegate_5() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___invalidOperationDelegate_5)); }
	inline ExceptionDelegate_t467131861 * get_invalidOperationDelegate_5() const { return ___invalidOperationDelegate_5; }
	inline ExceptionDelegate_t467131861 ** get_address_of_invalidOperationDelegate_5() { return &___invalidOperationDelegate_5; }
	inline void set_invalidOperationDelegate_5(ExceptionDelegate_t467131861 * value)
	{
		___invalidOperationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___invalidOperationDelegate_5), value);
	}

	inline static int32_t get_offset_of_ioDelegate_6() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___ioDelegate_6)); }
	inline ExceptionDelegate_t467131861 * get_ioDelegate_6() const { return ___ioDelegate_6; }
	inline ExceptionDelegate_t467131861 ** get_address_of_ioDelegate_6() { return &___ioDelegate_6; }
	inline void set_ioDelegate_6(ExceptionDelegate_t467131861 * value)
	{
		___ioDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___ioDelegate_6), value);
	}

	inline static int32_t get_offset_of_nullReferenceDelegate_7() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___nullReferenceDelegate_7)); }
	inline ExceptionDelegate_t467131861 * get_nullReferenceDelegate_7() const { return ___nullReferenceDelegate_7; }
	inline ExceptionDelegate_t467131861 ** get_address_of_nullReferenceDelegate_7() { return &___nullReferenceDelegate_7; }
	inline void set_nullReferenceDelegate_7(ExceptionDelegate_t467131861 * value)
	{
		___nullReferenceDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___nullReferenceDelegate_7), value);
	}

	inline static int32_t get_offset_of_outOfMemoryDelegate_8() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___outOfMemoryDelegate_8)); }
	inline ExceptionDelegate_t467131861 * get_outOfMemoryDelegate_8() const { return ___outOfMemoryDelegate_8; }
	inline ExceptionDelegate_t467131861 ** get_address_of_outOfMemoryDelegate_8() { return &___outOfMemoryDelegate_8; }
	inline void set_outOfMemoryDelegate_8(ExceptionDelegate_t467131861 * value)
	{
		___outOfMemoryDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___outOfMemoryDelegate_8), value);
	}

	inline static int32_t get_offset_of_overflowDelegate_9() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___overflowDelegate_9)); }
	inline ExceptionDelegate_t467131861 * get_overflowDelegate_9() const { return ___overflowDelegate_9; }
	inline ExceptionDelegate_t467131861 ** get_address_of_overflowDelegate_9() { return &___overflowDelegate_9; }
	inline void set_overflowDelegate_9(ExceptionDelegate_t467131861 * value)
	{
		___overflowDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___overflowDelegate_9), value);
	}

	inline static int32_t get_offset_of_systemDelegate_10() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___systemDelegate_10)); }
	inline ExceptionDelegate_t467131861 * get_systemDelegate_10() const { return ___systemDelegate_10; }
	inline ExceptionDelegate_t467131861 ** get_address_of_systemDelegate_10() { return &___systemDelegate_10; }
	inline void set_systemDelegate_10(ExceptionDelegate_t467131861 * value)
	{
		___systemDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemDelegate_10), value);
	}

	inline static int32_t get_offset_of_argumentDelegate_11() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___argumentDelegate_11)); }
	inline ExceptionArgumentDelegate_t1533780051 * get_argumentDelegate_11() const { return ___argumentDelegate_11; }
	inline ExceptionArgumentDelegate_t1533780051 ** get_address_of_argumentDelegate_11() { return &___argumentDelegate_11; }
	inline void set_argumentDelegate_11(ExceptionArgumentDelegate_t1533780051 * value)
	{
		___argumentDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___argumentDelegate_11), value);
	}

	inline static int32_t get_offset_of_argumentNullDelegate_12() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___argumentNullDelegate_12)); }
	inline ExceptionArgumentDelegate_t1533780051 * get_argumentNullDelegate_12() const { return ___argumentNullDelegate_12; }
	inline ExceptionArgumentDelegate_t1533780051 ** get_address_of_argumentNullDelegate_12() { return &___argumentNullDelegate_12; }
	inline void set_argumentNullDelegate_12(ExceptionArgumentDelegate_t1533780051 * value)
	{
		___argumentNullDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___argumentNullDelegate_12), value);
	}

	inline static int32_t get_offset_of_argumentOutOfRangeDelegate_13() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1372108196_StaticFields, ___argumentOutOfRangeDelegate_13)); }
	inline ExceptionArgumentDelegate_t1533780051 * get_argumentOutOfRangeDelegate_13() const { return ___argumentOutOfRangeDelegate_13; }
	inline ExceptionArgumentDelegate_t1533780051 ** get_address_of_argumentOutOfRangeDelegate_13() { return &___argumentOutOfRangeDelegate_13; }
	inline void set_argumentOutOfRangeDelegate_13(ExceptionArgumentDelegate_t1533780051 * value)
	{
		___argumentOutOfRangeDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___argumentOutOfRangeDelegate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGEXCEPTIONHELPER_T1372108196_H
#ifndef APPUTILPINVOKE_T1786794453_H
#define APPUTILPINVOKE_T1786794453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE
struct  AppUtilPINVOKE_t1786794453  : public RuntimeObject
{
public:

public:
};

struct AppUtilPINVOKE_t1786794453_StaticFields
{
public:
	// Firebase.AppUtilPINVOKE/SWIGExceptionHelper Firebase.AppUtilPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t1372108196 * ___swigExceptionHelper_0;
	// Firebase.AppUtilPINVOKE/SWIGStringHelper Firebase.AppUtilPINVOKE::swigStringHelper
	SWIGStringHelper_t1143654535 * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(AppUtilPINVOKE_t1786794453_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t1372108196 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t1372108196 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t1372108196 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier((&___swigExceptionHelper_0), value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(AppUtilPINVOKE_t1786794453_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t1143654535 * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t1143654535 ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t1143654535 * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier((&___swigStringHelper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPUTILPINVOKE_T1786794453_H
#ifndef APPUTIL_T2855004820_H
#define APPUTIL_T2855004820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtil
struct  AppUtil_t2855004820  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPUTIL_T2855004820_H
#ifndef SWIGSTRINGHELPER_T1143654535_H
#define SWIGSTRINGHELPER_T1143654535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGStringHelper
struct  SWIGStringHelper_t1143654535  : public RuntimeObject
{
public:

public:
};

struct SWIGStringHelper_t1143654535_StaticFields
{
public:
	// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.AppUtilPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t3256258918 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t1143654535_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_t3256258918 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_t3256258918 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_t3256258918 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGHELPER_T1143654535_H
#ifndef SWIGPENDINGEXCEPTION_T3190247900_H
#define SWIGPENDINGEXCEPTION_T3190247900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGPendingException
struct  SWIGPendingException_t3190247900  : public RuntimeObject
{
public:

public:
};

struct SWIGPendingException_t3190247900_StaticFields
{
public:
	// System.Int32 Firebase.AppUtilPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;

public:
	inline static int32_t get_offset_of_numExceptionsPending_1() { return static_cast<int32_t>(offsetof(SWIGPendingException_t3190247900_StaticFields, ___numExceptionsPending_1)); }
	inline int32_t get_numExceptionsPending_1() const { return ___numExceptionsPending_1; }
	inline int32_t* get_address_of_numExceptionsPending_1() { return &___numExceptionsPending_1; }
	inline void set_numExceptionsPending_1(int32_t value)
	{
		___numExceptionsPending_1 = value;
	}
};

struct SWIGPendingException_t3190247900_ThreadStaticFields
{
public:
	// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::pendingException
	Exception_t * ___pendingException_0;

public:
	inline static int32_t get_offset_of_pendingException_0() { return static_cast<int32_t>(offsetof(SWIGPendingException_t3190247900_ThreadStaticFields, ___pendingException_0)); }
	inline Exception_t * get_pendingException_0() const { return ___pendingException_0; }
	inline Exception_t ** get_address_of_pendingException_0() { return &___pendingException_0; }
	inline void set_pendingException_0(Exception_t * value)
	{
		___pendingException_0 = value;
		Il2CppCodeGenWriteBarrier((&___pendingException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGPENDINGEXCEPTION_T3190247900_H
#ifndef TASKEXTENSIONS_T2119891112_H
#define TASKEXTENSIONS_T2119891112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskExtensions
struct  TaskExtensions_t2119891112  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKEXTENSIONS_T2119891112_H
#ifndef U3CPOSTU3EC__ANONSTOREY0_T2101353913_H
#define U3CPOSTU3EC__ANONSTOREY0_T2101353913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskScheduler/<Post>c__AnonStorey0
struct  U3CPostU3Ec__AnonStorey0_t2101353913  : public RuntimeObject
{
public:
	// System.Action System.Threading.Tasks.TaskScheduler/<Post>c__AnonStorey0::action
	Action_t1264377477 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey0_t2101353913, ___action_0)); }
	inline Action_t1264377477 * get_action_0() const { return ___action_0; }
	inline Action_t1264377477 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t1264377477 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY0_T2101353913_H
#ifndef TASKSCHEDULER_T1196198384_H
#define TASKSCHEDULER_T1196198384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskScheduler
struct  TaskScheduler_t1196198384  : public RuntimeObject
{
public:
	// System.Threading.SynchronizationContext System.Threading.Tasks.TaskScheduler::context
	SynchronizationContext_t2326897723 * ___context_1;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(TaskScheduler_t1196198384, ___context_1)); }
	inline SynchronizationContext_t2326897723 * get_context_1() const { return ___context_1; }
	inline SynchronizationContext_t2326897723 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(SynchronizationContext_t2326897723 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}
};

struct TaskScheduler_t1196198384_StaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.Tasks.TaskScheduler::defaultContext
	SynchronizationContext_t2326897723 * ___defaultContext_0;

public:
	inline static int32_t get_offset_of_defaultContext_0() { return static_cast<int32_t>(offsetof(TaskScheduler_t1196198384_StaticFields, ___defaultContext_0)); }
	inline SynchronizationContext_t2326897723 * get_defaultContext_0() const { return ___defaultContext_0; }
	inline SynchronizationContext_t2326897723 ** get_address_of_defaultContext_0() { return &___defaultContext_0; }
	inline void set_defaultContext_0(SynchronizationContext_t2326897723 * value)
	{
		___defaultContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSCHEDULER_T1196198384_H
#ifndef CANCELLATIONTOKENSOURCE_T540272775_H
#define CANCELLATIONTOKENSOURCE_T540272775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationTokenSource
struct  CancellationTokenSource_t540272775  : public RuntimeObject
{
public:
	// System.Object System.Threading.CancellationTokenSource::mutex
	RuntimeObject * ___mutex_0;
	// System.Action System.Threading.CancellationTokenSource::actions
	Action_t1264377477 * ___actions_1;

public:
	inline static int32_t get_offset_of_mutex_0() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t540272775, ___mutex_0)); }
	inline RuntimeObject * get_mutex_0() const { return ___mutex_0; }
	inline RuntimeObject ** get_address_of_mutex_0() { return &___mutex_0; }
	inline void set_mutex_0(RuntimeObject * value)
	{
		___mutex_0 = value;
		Il2CppCodeGenWriteBarrier((&___mutex_0), value);
	}

	inline static int32_t get_offset_of_actions_1() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t540272775, ___actions_1)); }
	inline Action_t1264377477 * get_actions_1() const { return ___actions_1; }
	inline Action_t1264377477 ** get_address_of_actions_1() { return &___actions_1; }
	inline void set_actions_1(Action_t1264377477 * value)
	{
		___actions_1 = value;
		Il2CppCodeGenWriteBarrier((&___actions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCELLATIONTOKENSOURCE_T540272775_H
#ifndef U3CCONTINUEWITHU3EC__ANONSTOREY2_T408331350_H
#define U3CCONTINUEWITHU3EC__ANONSTOREY2_T408331350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey2
struct  U3CContinueWithU3Ec__AnonStorey2_t408331350  : public RuntimeObject
{
public:
	// System.Action`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey2::continuation
	Action_1_t3359742907 * ___continuation_0;

public:
	inline static int32_t get_offset_of_continuation_0() { return static_cast<int32_t>(offsetof(U3CContinueWithU3Ec__AnonStorey2_t408331350, ___continuation_0)); }
	inline Action_1_t3359742907 * get_continuation_0() const { return ___continuation_0; }
	inline Action_1_t3359742907 ** get_address_of_continuation_0() { return &___continuation_0; }
	inline void set_continuation_0(Action_1_t3359742907 * value)
	{
		___continuation_0 = value;
		Il2CppCodeGenWriteBarrier((&___continuation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTINUEWITHU3EC__ANONSTOREY2_T408331350_H
#ifndef TASK_T3187275312_H
#define TASK_T3187275312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task
struct  Task_t3187275312  : public RuntimeObject
{
public:
	// System.Object System.Threading.Tasks.Task::mutex
	RuntimeObject * ___mutex_2;
	// System.Collections.Generic.IList`1<System.Action`1<System.Threading.Tasks.Task>> System.Threading.Tasks.Task::continuations
	RuntimeObject* ___continuations_3;
	// System.AggregateException System.Threading.Tasks.Task::exception
	AggregateException_t3586243216 * ___exception_4;
	// System.Boolean System.Threading.Tasks.Task::isCanceled
	bool ___isCanceled_5;
	// System.Boolean System.Threading.Tasks.Task::isCompleted
	bool ___isCompleted_6;

public:
	inline static int32_t get_offset_of_mutex_2() { return static_cast<int32_t>(offsetof(Task_t3187275312, ___mutex_2)); }
	inline RuntimeObject * get_mutex_2() const { return ___mutex_2; }
	inline RuntimeObject ** get_address_of_mutex_2() { return &___mutex_2; }
	inline void set_mutex_2(RuntimeObject * value)
	{
		___mutex_2 = value;
		Il2CppCodeGenWriteBarrier((&___mutex_2), value);
	}

	inline static int32_t get_offset_of_continuations_3() { return static_cast<int32_t>(offsetof(Task_t3187275312, ___continuations_3)); }
	inline RuntimeObject* get_continuations_3() const { return ___continuations_3; }
	inline RuntimeObject** get_address_of_continuations_3() { return &___continuations_3; }
	inline void set_continuations_3(RuntimeObject* value)
	{
		___continuations_3 = value;
		Il2CppCodeGenWriteBarrier((&___continuations_3), value);
	}

	inline static int32_t get_offset_of_exception_4() { return static_cast<int32_t>(offsetof(Task_t3187275312, ___exception_4)); }
	inline AggregateException_t3586243216 * get_exception_4() const { return ___exception_4; }
	inline AggregateException_t3586243216 ** get_address_of_exception_4() { return &___exception_4; }
	inline void set_exception_4(AggregateException_t3586243216 * value)
	{
		___exception_4 = value;
		Il2CppCodeGenWriteBarrier((&___exception_4), value);
	}

	inline static int32_t get_offset_of_isCanceled_5() { return static_cast<int32_t>(offsetof(Task_t3187275312, ___isCanceled_5)); }
	inline bool get_isCanceled_5() const { return ___isCanceled_5; }
	inline bool* get_address_of_isCanceled_5() { return &___isCanceled_5; }
	inline void set_isCanceled_5(bool value)
	{
		___isCanceled_5 = value;
	}

	inline static int32_t get_offset_of_isCompleted_6() { return static_cast<int32_t>(offsetof(Task_t3187275312, ___isCompleted_6)); }
	inline bool get_isCompleted_6() const { return ___isCompleted_6; }
	inline bool* get_address_of_isCompleted_6() { return &___isCompleted_6; }
	inline void set_isCompleted_6(bool value)
	{
		___isCompleted_6 = value;
	}
};

struct Task_t3187275312_StaticFields
{
public:
	// System.Threading.ThreadLocal`1<System.Int32> System.Threading.Tasks.Task::executionDepth
	ThreadLocal_1_t558034686 * ___executionDepth_0;
	// System.Action`1<System.Action> System.Threading.Tasks.Task::immediateExecutor
	Action_1_t1436845072 * ___immediateExecutor_1;

public:
	inline static int32_t get_offset_of_executionDepth_0() { return static_cast<int32_t>(offsetof(Task_t3187275312_StaticFields, ___executionDepth_0)); }
	inline ThreadLocal_1_t558034686 * get_executionDepth_0() const { return ___executionDepth_0; }
	inline ThreadLocal_1_t558034686 ** get_address_of_executionDepth_0() { return &___executionDepth_0; }
	inline void set_executionDepth_0(ThreadLocal_1_t558034686 * value)
	{
		___executionDepth_0 = value;
		Il2CppCodeGenWriteBarrier((&___executionDepth_0), value);
	}

	inline static int32_t get_offset_of_immediateExecutor_1() { return static_cast<int32_t>(offsetof(Task_t3187275312_StaticFields, ___immediateExecutor_1)); }
	inline Action_1_t1436845072 * get_immediateExecutor_1() const { return ___immediateExecutor_1; }
	inline Action_1_t1436845072 ** get_address_of_immediateExecutor_1() { return &___immediateExecutor_1; }
	inline void set_immediateExecutor_1(Action_1_t1436845072 * value)
	{
		___immediateExecutor_1 = value;
		Il2CppCodeGenWriteBarrier((&___immediateExecutor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASK_T3187275312_H
#ifndef VARIANTVARIANTMAPENUMERATOR_T2068875317_H
#define VARIANTVARIANTMAPENUMERATOR_T2068875317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.VariantVariantMap/VariantVariantMapEnumerator
struct  VariantVariantMapEnumerator_t2068875317  : public RuntimeObject
{
public:
	// Firebase.VariantVariantMap Firebase.VariantVariantMap/VariantVariantMapEnumerator::collectionRef
	VariantVariantMap_t4053535076 * ___collectionRef_0;
	// System.Collections.Generic.IList`1<Firebase.Variant> Firebase.VariantVariantMap/VariantVariantMapEnumerator::keyCollection
	RuntimeObject* ___keyCollection_1;
	// System.Int32 Firebase.VariantVariantMap/VariantVariantMapEnumerator::currentIndex
	int32_t ___currentIndex_2;
	// System.Object Firebase.VariantVariantMap/VariantVariantMapEnumerator::currentObject
	RuntimeObject * ___currentObject_3;
	// System.Int32 Firebase.VariantVariantMap/VariantVariantMapEnumerator::currentSize
	int32_t ___currentSize_4;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(VariantVariantMapEnumerator_t2068875317, ___collectionRef_0)); }
	inline VariantVariantMap_t4053535076 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline VariantVariantMap_t4053535076 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(VariantVariantMap_t4053535076 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRef_0), value);
	}

	inline static int32_t get_offset_of_keyCollection_1() { return static_cast<int32_t>(offsetof(VariantVariantMapEnumerator_t2068875317, ___keyCollection_1)); }
	inline RuntimeObject* get_keyCollection_1() const { return ___keyCollection_1; }
	inline RuntimeObject** get_address_of_keyCollection_1() { return &___keyCollection_1; }
	inline void set_keyCollection_1(RuntimeObject* value)
	{
		___keyCollection_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyCollection_1), value);
	}

	inline static int32_t get_offset_of_currentIndex_2() { return static_cast<int32_t>(offsetof(VariantVariantMapEnumerator_t2068875317, ___currentIndex_2)); }
	inline int32_t get_currentIndex_2() const { return ___currentIndex_2; }
	inline int32_t* get_address_of_currentIndex_2() { return &___currentIndex_2; }
	inline void set_currentIndex_2(int32_t value)
	{
		___currentIndex_2 = value;
	}

	inline static int32_t get_offset_of_currentObject_3() { return static_cast<int32_t>(offsetof(VariantVariantMapEnumerator_t2068875317, ___currentObject_3)); }
	inline RuntimeObject * get_currentObject_3() const { return ___currentObject_3; }
	inline RuntimeObject ** get_address_of_currentObject_3() { return &___currentObject_3; }
	inline void set_currentObject_3(RuntimeObject * value)
	{
		___currentObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_3), value);
	}

	inline static int32_t get_offset_of_currentSize_4() { return static_cast<int32_t>(offsetof(VariantVariantMapEnumerator_t2068875317, ___currentSize_4)); }
	inline int32_t get_currentSize_4() const { return ___currentSize_4; }
	inline int32_t* get_address_of_currentSize_4() { return &___currentSize_4; }
	inline void set_currentSize_4(int32_t value)
	{
		___currentSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIANTVARIANTMAPENUMERATOR_T2068875317_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef VARIANTLISTENUMERATOR_T1966549472_H
#define VARIANTLISTENUMERATOR_T1966549472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.VariantList/VariantListEnumerator
struct  VariantListEnumerator_t1966549472  : public RuntimeObject
{
public:
	// Firebase.VariantList Firebase.VariantList/VariantListEnumerator::collectionRef
	VariantList_t2751083993 * ___collectionRef_0;
	// System.Int32 Firebase.VariantList/VariantListEnumerator::currentIndex
	int32_t ___currentIndex_1;
	// System.Object Firebase.VariantList/VariantListEnumerator::currentObject
	RuntimeObject * ___currentObject_2;
	// System.Int32 Firebase.VariantList/VariantListEnumerator::currentSize
	int32_t ___currentSize_3;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(VariantListEnumerator_t1966549472, ___collectionRef_0)); }
	inline VariantList_t2751083993 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline VariantList_t2751083993 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(VariantList_t2751083993 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRef_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(VariantListEnumerator_t1966549472, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}

	inline static int32_t get_offset_of_currentObject_2() { return static_cast<int32_t>(offsetof(VariantListEnumerator_t1966549472, ___currentObject_2)); }
	inline RuntimeObject * get_currentObject_2() const { return ___currentObject_2; }
	inline RuntimeObject ** get_address_of_currentObject_2() { return &___currentObject_2; }
	inline void set_currentObject_2(RuntimeObject * value)
	{
		___currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentObject_2), value);
	}

	inline static int32_t get_offset_of_currentSize_3() { return static_cast<int32_t>(offsetof(VariantListEnumerator_t1966549472, ___currentSize_3)); }
	inline int32_t get_currentSize_3() const { return ___currentSize_3; }
	inline int32_t* get_address_of_currentSize_3() { return &___currentSize_3; }
	inline void set_currentSize_3(int32_t value)
	{
		___currentSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIANTLISTENUMERATOR_T1966549472_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef CANCELLATIONTOKEN_T784455623_H
#define CANCELLATIONTOKEN_T784455623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationToken
struct  CancellationToken_t784455623 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::source
	CancellationTokenSource_t540272775 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t784455623, ___source_0)); }
	inline CancellationTokenSource_t540272775 * get_source_0() const { return ___source_0; }
	inline CancellationTokenSource_t540272775 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(CancellationTokenSource_t540272775 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_pinvoke
{
	CancellationTokenSource_t540272775 * ___source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_com
{
	CancellationTokenSource_t540272775 * ___source_0;
};
#endif // CANCELLATIONTOKEN_T784455623_H
#ifndef AGGREGATEEXCEPTION_T3586243216_H
#define AGGREGATEEXCEPTION_T3586243216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AggregateException
struct  AggregateException_t3586243216  : public Exception_t
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception> System.AggregateException::<InnerExceptions>k__BackingField
	ReadOnlyCollection_1_t2649313536 * ___U3CInnerExceptionsU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CInnerExceptionsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AggregateException_t3586243216, ___U3CInnerExceptionsU3Ek__BackingField_11)); }
	inline ReadOnlyCollection_1_t2649313536 * get_U3CInnerExceptionsU3Ek__BackingField_11() const { return ___U3CInnerExceptionsU3Ek__BackingField_11; }
	inline ReadOnlyCollection_1_t2649313536 ** get_address_of_U3CInnerExceptionsU3Ek__BackingField_11() { return &___U3CInnerExceptionsU3Ek__BackingField_11; }
	inline void set_U3CInnerExceptionsU3Ek__BackingField_11(ReadOnlyCollection_1_t2649313536 * value)
	{
		___U3CInnerExceptionsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInnerExceptionsU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATEEXCEPTION_T3586243216_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef CANCELLATIONTOKENREGISTRATION_T2813424904_H
#define CANCELLATIONTOKENREGISTRATION_T2813424904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationTokenRegistration
struct  CancellationTokenRegistration_t2813424904 
{
public:
	// System.Action System.Threading.CancellationTokenRegistration::action
	Action_t1264377477 * ___action_0;
	// System.Threading.CancellationTokenSource System.Threading.CancellationTokenRegistration::source
	CancellationTokenSource_t540272775 * ___source_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t2813424904, ___action_0)); }
	inline Action_t1264377477 * get_action_0() const { return ___action_0; }
	inline Action_t1264377477 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t1264377477 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_t2813424904, ___source_1)); }
	inline CancellationTokenSource_t540272775 * get_source_1() const { return ___source_1; }
	inline CancellationTokenSource_t540272775 ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(CancellationTokenSource_t540272775 * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t2813424904_marshaled_pinvoke
{
	Il2CppMethodPointer ___action_0;
	CancellationTokenSource_t540272775 * ___source_1;
};
// Native definition for COM marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_t2813424904_marshaled_com
{
	Il2CppMethodPointer ___action_0;
	CancellationTokenSource_t540272775 * ___source_1;
};
#endif // CANCELLATIONTOKENREGISTRATION_T2813424904_H
#ifndef UNITYSYNCHRONIZATIONCONTEXT_T705171362_H
#define UNITYSYNCHRONIZATIONCONTEXT_T705171362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext
struct  UnitySynchronizationContext_t705171362  : public SynchronizationContext_t2326897723
{
public:
	// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext::queue
	Queue_1_t3099366224 * ___queue_2;
	// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir Firebase.Unity.UnitySynchronizationContext::behavior
	SynchronizationContextBehavoir_t1466989729 * ___behavior_3;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext::mainThreadId
	int32_t ___mainThreadId_4;

public:
	inline static int32_t get_offset_of_queue_2() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t705171362, ___queue_2)); }
	inline Queue_1_t3099366224 * get_queue_2() const { return ___queue_2; }
	inline Queue_1_t3099366224 ** get_address_of_queue_2() { return &___queue_2; }
	inline void set_queue_2(Queue_1_t3099366224 * value)
	{
		___queue_2 = value;
		Il2CppCodeGenWriteBarrier((&___queue_2), value);
	}

	inline static int32_t get_offset_of_behavior_3() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t705171362, ___behavior_3)); }
	inline SynchronizationContextBehavoir_t1466989729 * get_behavior_3() const { return ___behavior_3; }
	inline SynchronizationContextBehavoir_t1466989729 ** get_address_of_behavior_3() { return &___behavior_3; }
	inline void set_behavior_3(SynchronizationContextBehavoir_t1466989729 * value)
	{
		___behavior_3 = value;
		Il2CppCodeGenWriteBarrier((&___behavior_3), value);
	}

	inline static int32_t get_offset_of_mainThreadId_4() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t705171362, ___mainThreadId_4)); }
	inline int32_t get_mainThreadId_4() const { return ___mainThreadId_4; }
	inline int32_t* get_address_of_mainThreadId_4() { return &___mainThreadId_4; }
	inline void set_mainThreadId_4(int32_t value)
	{
		___mainThreadId_4 = value;
	}
};

struct UnitySynchronizationContext_t705171362_StaticFields
{
public:
	// Firebase.Unity.UnitySynchronizationContext Firebase.Unity.UnitySynchronizationContext::_instance
	UnitySynchronizationContext_t705171362 * ____instance_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent> Firebase.Unity.UnitySynchronizationContext::signalDictionary
	Dictionary_2_t3634922637 * ___signalDictionary_5;

public:
	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t705171362_StaticFields, ____instance_1)); }
	inline UnitySynchronizationContext_t705171362 * get__instance_1() const { return ____instance_1; }
	inline UnitySynchronizationContext_t705171362 ** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(UnitySynchronizationContext_t705171362 * value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier((&____instance_1), value);
	}

	inline static int32_t get_offset_of_signalDictionary_5() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t705171362_StaticFields, ___signalDictionary_5)); }
	inline Dictionary_2_t3634922637 * get_signalDictionary_5() const { return ___signalDictionary_5; }
	inline Dictionary_2_t3634922637 ** get_address_of_signalDictionary_5() { return &___signalDictionary_5; }
	inline void set_signalDictionary_5(Dictionary_2_t3634922637 * value)
	{
		___signalDictionary_5 = value;
		Il2CppCodeGenWriteBarrier((&___signalDictionary_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSYNCHRONIZATIONCONTEXT_T705171362_H
#ifndef APPLICATIONFOCUSCHANGEDEVENTARGS_T3956087507_H
#define APPLICATIONFOCUSCHANGEDEVENTARGS_T3956087507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs
struct  ApplicationFocusChangedEventArgs_t3956087507  : public EventArgs_t3591816995
{
public:
	// System.Boolean Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs::<HasFocus>k__BackingField
	bool ___U3CHasFocusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CHasFocusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ApplicationFocusChangedEventArgs_t3956087507, ___U3CHasFocusU3Ek__BackingField_1)); }
	inline bool get_U3CHasFocusU3Ek__BackingField_1() const { return ___U3CHasFocusU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CHasFocusU3Ek__BackingField_1() { return &___U3CHasFocusU3Ek__BackingField_1; }
	inline void set_U3CHasFocusU3Ek__BackingField_1(bool value)
	{
		___U3CHasFocusU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONFOCUSCHANGEDEVENTARGS_T3956087507_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T124655523_H
#define MONOPINVOKECALLBACKATTRIBUTE_T124655523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t124655523  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T124655523_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T264469961_H
#define MONOPINVOKECALLBACKATTRIBUTE_T264469961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t264469961  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T264469961_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T1863609720_H
#define MONOPINVOKECALLBACKATTRIBUTE_T1863609720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureString/MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t1863609720  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T1863609720_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UNITYCONFIGEXTENSIONS_T3428627910_H
#define UNITYCONFIGEXTENSIONS_T3428627910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Platform.Default.UnityConfigExtensions
struct  UnityConfigExtensions_t3428627910  : public AppConfigExtensions_t2108028355
{
public:

public:
};

struct UnityConfigExtensions_t3428627910_StaticFields
{
public:
	// Firebase.Platform.Default.UnityConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::_instance
	UnityConfigExtensions_t3428627910 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(UnityConfigExtensions_t3428627910_StaticFields, ____instance_5)); }
	inline UnityConfigExtensions_t3428627910 * get__instance_5() const { return ____instance_5; }
	inline UnityConfigExtensions_t3428627910 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(UnityConfigExtensions_t3428627910 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((&____instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCONFIGEXTENSIONS_T3428627910_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T2381882871_H
#define MONOPINVOKECALLBACKATTRIBUTE_T2381882871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureVoid/MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t2381882871  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T2381882871_H
#ifndef DEPENDENCYSTATUS_T2632294593_H
#define DEPENDENCYSTATUS_T2632294593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.DependencyStatus
struct  DependencyStatus_t2632294593 
{
public:
	// System.Int32 Firebase.DependencyStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DependencyStatus_t2632294593, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPENDENCYSTATUS_T2632294593_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef HANDLEREF_T3745784362_H
#define HANDLEREF_T3745784362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784362 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T3745784362_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TYPE_T2611682988_H
#define TYPE_T2611682988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Variant/Type
struct  Type_t2611682988 
{
public:
	// System.Int32 Firebase.Variant/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2611682988, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2611682988_H
#ifndef INITRESULT_T3767652586_H
#define INITRESULT_T3767652586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.InitResult
struct  InitResult_t3767652586 
{
public:
	// System.Int32 Firebase.InitResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitResult_t3767652586, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITRESULT_T3767652586_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TASKFACTORY_T2660013028_H
#define TASKFACTORY_T2660013028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskFactory
struct  TaskFactory_t2660013028  : public RuntimeObject
{
public:
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.TaskFactory::scheduler
	TaskScheduler_t1196198384 * ___scheduler_0;
	// System.Threading.CancellationToken System.Threading.Tasks.TaskFactory::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(TaskFactory_t2660013028, ___scheduler_0)); }
	inline TaskScheduler_t1196198384 * get_scheduler_0() const { return ___scheduler_0; }
	inline TaskScheduler_t1196198384 ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(TaskScheduler_t1196198384 * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_0), value);
	}

	inline static int32_t get_offset_of_cancellationToken_1() { return static_cast<int32_t>(offsetof(TaskFactory_t2660013028, ___cancellationToken_1)); }
	inline CancellationToken_t784455623  get_cancellationToken_1() const { return ___cancellationToken_1; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_1() { return &___cancellationToken_1; }
	inline void set_cancellationToken_1(CancellationToken_t784455623  value)
	{
		___cancellationToken_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKFACTORY_T2660013028_H
#ifndef LOGLEVEL_T2506056272_H
#define LOGLEVEL_T2506056272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.LogLevel
struct  LogLevel_t2506056272 
{
public:
	// System.Int32 Firebase.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t2506056272, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T2506056272_H
#ifndef FUTURESTATUS_T854570584_H
#define FUTURESTATUS_T854570584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureStatus
struct  FutureStatus_t854570584 
{
public:
	// System.Int32 Firebase.FutureStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FutureStatus_t854570584, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTURESTATUS_T854570584_H
#ifndef GOOGLEPLAYSERVICESAVAILABILITY_T243528349_H
#define GOOGLEPLAYSERVICESAVAILABILITY_T243528349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.GooglePlayServicesAvailability
struct  GooglePlayServicesAvailability_t243528349 
{
public:
	// System.Int32 Firebase.GooglePlayServicesAvailability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GooglePlayServicesAvailability_t243528349, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYSERVICESAVAILABILITY_T243528349_H
#ifndef U3CCHECKDEPENDENCIESU3EC__ANONSTOREY5_T80273224_H
#define U3CCHECKDEPENDENCIESU3EC__ANONSTOREY5_T80273224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey5
struct  U3CCheckDependenciesU3Ec__AnonStorey5_t80273224  : public RuntimeObject
{
public:
	// Firebase.DependencyStatus Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey5::status
	int32_t ___status_0;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(U3CCheckDependenciesU3Ec__AnonStorey5_t80273224, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKDEPENDENCIESU3EC__ANONSTOREY5_T80273224_H
#ifndef U3CU3EC__ANONSTOREY4_T1009172721_H
#define U3CU3EC__ANONSTOREY4_T1009172721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<>c__AnonStorey4
struct  U3CU3Ec__AnonStorey4_t1009172721  : public RuntimeObject
{
public:
	// Firebase.LogLevel Firebase.FirebaseApp/<>c__AnonStorey4::value
	int32_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStorey4_t1009172721, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ANONSTOREY4_T1009172721_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef U3CU3EC__ANONSTOREY3_T1009172714_H
#define U3CU3EC__ANONSTOREY3_T1009172714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<>c__AnonStorey3
struct  U3CU3Ec__AnonStorey3_t1009172714  : public RuntimeObject
{
public:
	// Firebase.LogLevel Firebase.FirebaseApp/<>c__AnonStorey3::level
	int32_t ___level_0;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStorey3_t1009172714, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ANONSTOREY3_T1009172714_H
#ifndef VARIANTVARIANTMAP_T4053535076_H
#define VARIANTVARIANTMAP_T4053535076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.VariantVariantMap
struct  VariantVariantMap_t4053535076  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.VariantVariantMap::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.VariantVariantMap::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(VariantVariantMap_t4053535076, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(VariantVariantMap_t4053535076, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIANTVARIANTMAP_T4053535076_H
#ifndef FIREBASEAPP_T2526288410_H
#define FIREBASEAPP_T2526288410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp
struct  FirebaseApp_t2526288410  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.FirebaseApp::swigCMemOwn
	bool ___swigCMemOwn_1;
	// System.EventHandler`1<System.EventArgs> Firebase.FirebaseApp::Disposed
	EventHandler_1_t1515976428 * ___Disposed_8;
	// Firebase.FirebaseApp/DestroyDelegate Firebase.FirebaseApp::destroy
	DestroyDelegate_t2525580959 * ___destroy_12;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_Disposed_8() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___Disposed_8)); }
	inline EventHandler_1_t1515976428 * get_Disposed_8() const { return ___Disposed_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_Disposed_8() { return &___Disposed_8; }
	inline void set_Disposed_8(EventHandler_1_t1515976428 * value)
	{
		___Disposed_8 = value;
		Il2CppCodeGenWriteBarrier((&___Disposed_8), value);
	}

	inline static int32_t get_offset_of_destroy_12() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___destroy_12)); }
	inline DestroyDelegate_t2525580959 * get_destroy_12() const { return ___destroy_12; }
	inline DestroyDelegate_t2525580959 ** get_address_of_destroy_12() { return &___destroy_12; }
	inline void set_destroy_12(DestroyDelegate_t2525580959 * value)
	{
		___destroy_12 = value;
		Il2CppCodeGenWriteBarrier((&___destroy_12), value);
	}
};

struct FirebaseApp_t2526288410_StaticFields
{
public:
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_ANDROID
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2;
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_IOS
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3;
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_GENERIC
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_ANDROID
	String_t* ___DLL_NOT_FOUND_ERROR_ANDROID_5;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_IOS
	String_t* ___DLL_NOT_FOUND_ERROR_IOS_6;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_GENERIC
	String_t* ___DLL_NOT_FOUND_ERROR_GENERIC_7;
	// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp> Firebase.FirebaseApp::nameToProxy
	Dictionary_2_t2311544709 * ___nameToProxy_9;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp> Firebase.FirebaseApp::cPtrToProxy
	Dictionary_2_t748154129 * ___cPtrToProxy_10;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32> Firebase.FirebaseApp::cPtrRefCount
	Dictionary_2_t1172811472 * ___cPtrRefCount_11;
	// Firebase.FirebaseApp/CreateDelegate Firebase.FirebaseApp::<>f__am$cache0
	CreateDelegate_t3131870060 * ___U3CU3Ef__amU24cache0_13;
	// System.Func`1<Firebase.DependencyStatus> Firebase.FirebaseApp::<>f__am$cache1
	Func_1_t2062041240 * ___U3CU3Ef__amU24cache1_14;
	// System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Threading.Tasks.Task`1<Firebase.DependencyStatus>> Firebase.FirebaseApp::<>f__am$cache2
	Func_2_t680774166 * ___U3CU3Ef__amU24cache2_15;
	// System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus> Firebase.FirebaseApp::<>f__am$cache3
	Func_2_t724047911 * ___U3CU3Ef__amU24cache3_16;

public:
	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() const { return ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() { return &___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2), value);
	}

	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() const { return ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() { return &___DEPENDENCY_NOT_FOUND_ERROR_IOS_3; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_IOS_3(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_IOS_3 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_IOS_3), value);
	}

	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() const { return ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() { return &___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_ANDROID_5() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_ANDROID_5)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_ANDROID_5() const { return ___DLL_NOT_FOUND_ERROR_ANDROID_5; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_ANDROID_5() { return &___DLL_NOT_FOUND_ERROR_ANDROID_5; }
	inline void set_DLL_NOT_FOUND_ERROR_ANDROID_5(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_ANDROID_5 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_ANDROID_5), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_IOS_6() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_IOS_6)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_IOS_6() const { return ___DLL_NOT_FOUND_ERROR_IOS_6; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_IOS_6() { return &___DLL_NOT_FOUND_ERROR_IOS_6; }
	inline void set_DLL_NOT_FOUND_ERROR_IOS_6(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_IOS_6 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_IOS_6), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_GENERIC_7() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_GENERIC_7)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_GENERIC_7() const { return ___DLL_NOT_FOUND_ERROR_GENERIC_7; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_GENERIC_7() { return &___DLL_NOT_FOUND_ERROR_GENERIC_7; }
	inline void set_DLL_NOT_FOUND_ERROR_GENERIC_7(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_GENERIC_7 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_GENERIC_7), value);
	}

	inline static int32_t get_offset_of_nameToProxy_9() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___nameToProxy_9)); }
	inline Dictionary_2_t2311544709 * get_nameToProxy_9() const { return ___nameToProxy_9; }
	inline Dictionary_2_t2311544709 ** get_address_of_nameToProxy_9() { return &___nameToProxy_9; }
	inline void set_nameToProxy_9(Dictionary_2_t2311544709 * value)
	{
		___nameToProxy_9 = value;
		Il2CppCodeGenWriteBarrier((&___nameToProxy_9), value);
	}

	inline static int32_t get_offset_of_cPtrToProxy_10() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___cPtrToProxy_10)); }
	inline Dictionary_2_t748154129 * get_cPtrToProxy_10() const { return ___cPtrToProxy_10; }
	inline Dictionary_2_t748154129 ** get_address_of_cPtrToProxy_10() { return &___cPtrToProxy_10; }
	inline void set_cPtrToProxy_10(Dictionary_2_t748154129 * value)
	{
		___cPtrToProxy_10 = value;
		Il2CppCodeGenWriteBarrier((&___cPtrToProxy_10), value);
	}

	inline static int32_t get_offset_of_cPtrRefCount_11() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___cPtrRefCount_11)); }
	inline Dictionary_2_t1172811472 * get_cPtrRefCount_11() const { return ___cPtrRefCount_11; }
	inline Dictionary_2_t1172811472 ** get_address_of_cPtrRefCount_11() { return &___cPtrRefCount_11; }
	inline void set_cPtrRefCount_11(Dictionary_2_t1172811472 * value)
	{
		___cPtrRefCount_11 = value;
		Il2CppCodeGenWriteBarrier((&___cPtrRefCount_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline CreateDelegate_t3131870060 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline CreateDelegate_t3131870060 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(CreateDelegate_t3131870060 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_14() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache1_14)); }
	inline Func_1_t2062041240 * get_U3CU3Ef__amU24cache1_14() const { return ___U3CU3Ef__amU24cache1_14; }
	inline Func_1_t2062041240 ** get_address_of_U3CU3Ef__amU24cache1_14() { return &___U3CU3Ef__amU24cache1_14; }
	inline void set_U3CU3Ef__amU24cache1_14(Func_1_t2062041240 * value)
	{
		___U3CU3Ef__amU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_15() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache2_15)); }
	inline Func_2_t680774166 * get_U3CU3Ef__amU24cache2_15() const { return ___U3CU3Ef__amU24cache2_15; }
	inline Func_2_t680774166 ** get_address_of_U3CU3Ef__amU24cache2_15() { return &___U3CU3Ef__amU24cache2_15; }
	inline void set_U3CU3Ef__amU24cache2_15(Func_2_t680774166 * value)
	{
		___U3CU3Ef__amU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_16() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache3_16)); }
	inline Func_2_t724047911 * get_U3CU3Ef__amU24cache3_16() const { return ___U3CU3Ef__amU24cache3_16; }
	inline Func_2_t724047911 ** get_address_of_U3CU3Ef__amU24cache3_16() { return &___U3CU3Ef__amU24cache3_16; }
	inline void set_U3CU3Ef__amU24cache3_16(Func_2_t724047911 * value)
	{
		___U3CU3Ef__amU24cache3_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEAPP_T2526288410_H
#ifndef VARIANT_T1163908808_H
#define VARIANT_T1163908808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Variant
struct  Variant_t1163908808  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Variant::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.Variant::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Variant_t1163908808, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Variant_t1163908808, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIANT_T1163908808_H
#ifndef VARIANTLIST_T2751083993_H
#define VARIANTLIST_T2751083993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.VariantList
struct  VariantList_t2751083993  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.VariantList::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.VariantList::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(VariantList_t2751083993, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(VariantList_t2751083993, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIANTLIST_T2751083993_H
#ifndef CHARVECTOR_T1896987243_H
#define CHARVECTOR_T1896987243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.CharVector
struct  CharVector_t1896987243  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.CharVector::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.CharVector::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(CharVector_t1896987243, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(CharVector_t1896987243, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARVECTOR_T1896987243_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef STRINGLIST_T2653214935_H
#define STRINGLIST_T2653214935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.StringList
struct  StringList_t2653214935  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.StringList::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.StringList::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(StringList_t2653214935, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(StringList_t2653214935, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGLIST_T2653214935_H
#ifndef FUTUREBASE_T1024553797_H
#define FUTUREBASE_T1024553797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureBase
struct  FutureBase_t1024553797  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FutureBase::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.FutureBase::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FutureBase_t1024553797, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FutureBase_t1024553797, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTUREBASE_T1024553797_H
#ifndef STRINGSTRINGMAP_T4119824414_H
#define STRINGSTRINGMAP_T4119824414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.StringStringMap
struct  StringStringMap_t4119824414  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.StringStringMap::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.StringStringMap::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(StringStringMap_t4119824414, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(StringStringMap_t4119824414, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSTRINGMAP_T4119824414_H
#ifndef U3CSETLOGLEVELU3EC__ANONSTOREY2_T3050025917_H
#define U3CSETLOGLEVELU3EC__ANONSTOREY2_T3050025917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/<SetLogLevel>c__AnonStorey2
struct  U3CSetLogLevelU3Ec__AnonStorey2_t3050025917  : public RuntimeObject
{
public:
	// Firebase.LogLevel Firebase.FirebaseApp/<SetLogLevel>c__AnonStorey2::level
	int32_t ___level_0;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(U3CSetLogLevelU3Ec__AnonStorey2_t3050025917, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETLOGLEVELU3EC__ANONSTOREY2_T3050025917_H
#ifndef APPOPTIONSINTERNAL_T1980997696_H
#define APPOPTIONSINTERNAL_T1980997696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppOptionsInternal
struct  AppOptionsInternal_t1980997696  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.AppOptionsInternal::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.AppOptionsInternal::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(AppOptionsInternal_t1980997696, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(AppOptionsInternal_t1980997696, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPOPTIONSINTERNAL_T1980997696_H
#ifndef SWIGSTRINGDELEGATE_T3256258918_H
#define SWIGSTRINGDELEGATE_T3256258918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct  SWIGStringDelegate_t3256258918  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGDELEGATE_T3256258918_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef EXCEPTIONDELEGATE_T467131861_H
#define EXCEPTIONDELEGATE_T467131861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct  ExceptionDelegate_t467131861  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONDELEGATE_T467131861_H
#ifndef EXCEPTIONARGUMENTDELEGATE_T1533780051_H
#define EXCEPTIONARGUMENTDELEGATE_T1533780051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct  ExceptionArgumentDelegate_t1533780051  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONARGUMENTDELEGATE_T1533780051_H
#ifndef SWIG_COMPLETIONDELEGATE_T2224793779_H
#define SWIG_COMPLETIONDELEGATE_T2224793779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureVoid/SWIG_CompletionDelegate
struct  SWIG_CompletionDelegate_t2224793779  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIG_COMPLETIONDELEGATE_T2224793779_H
#ifndef ACTION_T1244810226_H
#define ACTION_T1244810226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureVoid/Action
struct  Action_t1244810226  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1244810226_H
#ifndef FUTURESTRING_T4072148675_H
#define FUTURESTRING_T4072148675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureString
struct  FutureString_t4072148675  : public FutureBase_t1024553797
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FutureString::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_2;
	// System.IntPtr Firebase.FutureString::callbackData
	intptr_t ___callbackData_6;
	// Firebase.FutureString/SWIG_CompletionDelegate Firebase.FutureString::SWIG_CompletionCB
	SWIG_CompletionDelegate_t3186567461 * ___SWIG_CompletionCB_7;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FutureString_t4072148675, ___swigCPtr_2)); }
	inline HandleRef_t3745784362  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784362  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_callbackData_6() { return static_cast<int32_t>(offsetof(FutureString_t4072148675, ___callbackData_6)); }
	inline intptr_t get_callbackData_6() const { return ___callbackData_6; }
	inline intptr_t* get_address_of_callbackData_6() { return &___callbackData_6; }
	inline void set_callbackData_6(intptr_t value)
	{
		___callbackData_6 = value;
	}

	inline static int32_t get_offset_of_SWIG_CompletionCB_7() { return static_cast<int32_t>(offsetof(FutureString_t4072148675, ___SWIG_CompletionCB_7)); }
	inline SWIG_CompletionDelegate_t3186567461 * get_SWIG_CompletionCB_7() const { return ___SWIG_CompletionCB_7; }
	inline SWIG_CompletionDelegate_t3186567461 ** get_address_of_SWIG_CompletionCB_7() { return &___SWIG_CompletionCB_7; }
	inline void set_SWIG_CompletionCB_7(SWIG_CompletionDelegate_t3186567461 * value)
	{
		___SWIG_CompletionCB_7 = value;
		Il2CppCodeGenWriteBarrier((&___SWIG_CompletionCB_7), value);
	}
};

struct FutureString_t4072148675_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureString/Action> Firebase.FutureString::Callbacks
	Dictionary_2_t1596674066 * ___Callbacks_3;
	// System.Int32 Firebase.FutureString::CallbackIndex
	int32_t ___CallbackIndex_4;
	// System.Object Firebase.FutureString::CallbackLock
	RuntimeObject * ___CallbackLock_5;

public:
	inline static int32_t get_offset_of_Callbacks_3() { return static_cast<int32_t>(offsetof(FutureString_t4072148675_StaticFields, ___Callbacks_3)); }
	inline Dictionary_2_t1596674066 * get_Callbacks_3() const { return ___Callbacks_3; }
	inline Dictionary_2_t1596674066 ** get_address_of_Callbacks_3() { return &___Callbacks_3; }
	inline void set_Callbacks_3(Dictionary_2_t1596674066 * value)
	{
		___Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callbacks_3), value);
	}

	inline static int32_t get_offset_of_CallbackIndex_4() { return static_cast<int32_t>(offsetof(FutureString_t4072148675_StaticFields, ___CallbackIndex_4)); }
	inline int32_t get_CallbackIndex_4() const { return ___CallbackIndex_4; }
	inline int32_t* get_address_of_CallbackIndex_4() { return &___CallbackIndex_4; }
	inline void set_CallbackIndex_4(int32_t value)
	{
		___CallbackIndex_4 = value;
	}

	inline static int32_t get_offset_of_CallbackLock_5() { return static_cast<int32_t>(offsetof(FutureString_t4072148675_StaticFields, ___CallbackLock_5)); }
	inline RuntimeObject * get_CallbackLock_5() const { return ___CallbackLock_5; }
	inline RuntimeObject ** get_address_of_CallbackLock_5() { return &___CallbackLock_5; }
	inline void set_CallbackLock_5(RuntimeObject * value)
	{
		___CallbackLock_5 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackLock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTURESTRING_T4072148675_H
#ifndef SWIG_COMPLETIONDELEGATE_T3186567461_H
#define SWIG_COMPLETIONDELEGATE_T3186567461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureString/SWIG_CompletionDelegate
struct  SWIG_CompletionDelegate_t3186567461  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIG_COMPLETIONDELEGATE_T3186567461_H
#ifndef ACTION_T2707960735_H
#define ACTION_T2707960735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureString/Action
struct  Action_t2707960735  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T2707960735_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef LOGMESSAGEDELEGATE_T657899700_H
#define LOGMESSAGEDELEGATE_T657899700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct  LogMessageDelegate_t657899700  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMESSAGEDELEGATE_T657899700_H
#ifndef DESTROYDELEGATE_T2525580959_H
#define DESTROYDELEGATE_T2525580959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/DestroyDelegate
struct  DestroyDelegate_t2525580959  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYDELEGATE_T2525580959_H
#ifndef FUTUREVOID_T983301800_H
#define FUTUREVOID_T983301800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FutureVoid
struct  FutureVoid_t983301800  : public FutureBase_t1024553797
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FutureVoid::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_2;
	// System.IntPtr Firebase.FutureVoid::callbackData
	intptr_t ___callbackData_6;
	// Firebase.FutureVoid/SWIG_CompletionDelegate Firebase.FutureVoid::SWIG_CompletionCB
	SWIG_CompletionDelegate_t2224793779 * ___SWIG_CompletionCB_7;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800, ___swigCPtr_2)); }
	inline HandleRef_t3745784362  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t3745784362  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_callbackData_6() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800, ___callbackData_6)); }
	inline intptr_t get_callbackData_6() const { return ___callbackData_6; }
	inline intptr_t* get_address_of_callbackData_6() { return &___callbackData_6; }
	inline void set_callbackData_6(intptr_t value)
	{
		___callbackData_6 = value;
	}

	inline static int32_t get_offset_of_SWIG_CompletionCB_7() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800, ___SWIG_CompletionCB_7)); }
	inline SWIG_CompletionDelegate_t2224793779 * get_SWIG_CompletionCB_7() const { return ___SWIG_CompletionCB_7; }
	inline SWIG_CompletionDelegate_t2224793779 ** get_address_of_SWIG_CompletionCB_7() { return &___SWIG_CompletionCB_7; }
	inline void set_SWIG_CompletionCB_7(SWIG_CompletionDelegate_t2224793779 * value)
	{
		___SWIG_CompletionCB_7 = value;
		Il2CppCodeGenWriteBarrier((&___SWIG_CompletionCB_7), value);
	}
};

struct FutureVoid_t983301800_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureVoid/Action> Firebase.FutureVoid::Callbacks
	Dictionary_2_t133523557 * ___Callbacks_3;
	// System.Int32 Firebase.FutureVoid::CallbackIndex
	int32_t ___CallbackIndex_4;
	// System.Object Firebase.FutureVoid::CallbackLock
	RuntimeObject * ___CallbackLock_5;

public:
	inline static int32_t get_offset_of_Callbacks_3() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800_StaticFields, ___Callbacks_3)); }
	inline Dictionary_2_t133523557 * get_Callbacks_3() const { return ___Callbacks_3; }
	inline Dictionary_2_t133523557 ** get_address_of_Callbacks_3() { return &___Callbacks_3; }
	inline void set_Callbacks_3(Dictionary_2_t133523557 * value)
	{
		___Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callbacks_3), value);
	}

	inline static int32_t get_offset_of_CallbackIndex_4() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800_StaticFields, ___CallbackIndex_4)); }
	inline int32_t get_CallbackIndex_4() const { return ___CallbackIndex_4; }
	inline int32_t* get_address_of_CallbackIndex_4() { return &___CallbackIndex_4; }
	inline void set_CallbackIndex_4(int32_t value)
	{
		___CallbackIndex_4 = value;
	}

	inline static int32_t get_offset_of_CallbackLock_5() { return static_cast<int32_t>(offsetof(FutureVoid_t983301800_StaticFields, ___CallbackLock_5)); }
	inline RuntimeObject * get_CallbackLock_5() const { return ___CallbackLock_5; }
	inline RuntimeObject ** get_address_of_CallbackLock_5() { return &___CallbackLock_5; }
	inline void set_CallbackLock_5(RuntimeObject * value)
	{
		___CallbackLock_5 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackLock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUTUREVOID_T983301800_H
#ifndef CREATEDELEGATE_T3131870060_H
#define CREATEDELEGATE_T3131870060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/CreateDelegate
struct  CreateDelegate_t3131870060  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEDELEGATE_T3131870060_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SYNCHRONIZATIONCONTEXTBEHAVOIR_T1466989729_H
#define SYNCHRONIZATIONCONTEXTBEHAVOIR_T1466989729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir
struct  SynchronizationContextBehavoir_t1466989729  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::callbackQueue
	Queue_1_t3099366224 * ___callbackQueue_2;

public:
	inline static int32_t get_offset_of_callbackQueue_2() { return static_cast<int32_t>(offsetof(SynchronizationContextBehavoir_t1466989729, ___callbackQueue_2)); }
	inline Queue_1_t3099366224 * get_callbackQueue_2() const { return ___callbackQueue_2; }
	inline Queue_1_t3099366224 ** get_address_of_callbackQueue_2() { return &___callbackQueue_2; }
	inline void set_callbackQueue_2(Queue_1_t3099366224 * value)
	{
		___callbackQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___callbackQueue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZATIONCONTEXTBEHAVOIR_T1466989729_H
#ifndef FIREBASEHANDLER_T321821895_H
#define FIREBASEHANDLER_T321821895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp/FirebaseHandler
struct  FirebaseHandler_t321821895  : public MonoBehaviour_t3962482529
{
public:
	// System.EventHandler`1<System.EventArgs> Firebase.FirebaseApp/FirebaseHandler::Updated
	EventHandler_1_t1515976428 * ___Updated_3;
	// System.EventHandler`1<Firebase.FirebaseApp/FirebaseHandler/ApplicationFocusChangedEventArgs> Firebase.FirebaseApp/FirebaseHandler::ApplicationFocusChanged
	EventHandler_1_t1880246940 * ___ApplicationFocusChanged_4;

public:
	inline static int32_t get_offset_of_Updated_3() { return static_cast<int32_t>(offsetof(FirebaseHandler_t321821895, ___Updated_3)); }
	inline EventHandler_1_t1515976428 * get_Updated_3() const { return ___Updated_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_Updated_3() { return &___Updated_3; }
	inline void set_Updated_3(EventHandler_1_t1515976428 * value)
	{
		___Updated_3 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_3), value);
	}

	inline static int32_t get_offset_of_ApplicationFocusChanged_4() { return static_cast<int32_t>(offsetof(FirebaseHandler_t321821895, ___ApplicationFocusChanged_4)); }
	inline EventHandler_1_t1880246940 * get_ApplicationFocusChanged_4() const { return ___ApplicationFocusChanged_4; }
	inline EventHandler_1_t1880246940 ** get_address_of_ApplicationFocusChanged_4() { return &___ApplicationFocusChanged_4; }
	inline void set_ApplicationFocusChanged_4(EventHandler_1_t1880246940 * value)
	{
		___ApplicationFocusChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ApplicationFocusChanged_4), value);
	}
};

struct FirebaseHandler_t321821895_StaticFields
{
public:
	// UnityEngine.GameObject Firebase.FirebaseApp/FirebaseHandler::firebaseHandler
	GameObject_t1113636619 * ___firebaseHandler_2;
	// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate Firebase.FirebaseApp/FirebaseHandler::<>f__mg$cache0
	LogMessageDelegate_t657899700 * ___U3CU3Ef__mgU24cache0_5;
	// System.Action Firebase.FirebaseApp/FirebaseHandler::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_firebaseHandler_2() { return static_cast<int32_t>(offsetof(FirebaseHandler_t321821895_StaticFields, ___firebaseHandler_2)); }
	inline GameObject_t1113636619 * get_firebaseHandler_2() const { return ___firebaseHandler_2; }
	inline GameObject_t1113636619 ** get_address_of_firebaseHandler_2() { return &___firebaseHandler_2; }
	inline void set_firebaseHandler_2(GameObject_t1113636619 * value)
	{
		___firebaseHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___firebaseHandler_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(FirebaseHandler_t321821895_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline LogMessageDelegate_t657899700 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline LogMessageDelegate_t657899700 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(LogMessageDelegate_t657899700 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(FirebaseHandler_t321821895_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEHANDLER_T321821895_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	WWW_t3688466362::get_offset_of__uwr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (TaskScheduler_t1196198384), -1, sizeof(TaskScheduler_t1196198384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2404[2] = 
{
	TaskScheduler_t1196198384_StaticFields::get_offset_of_defaultContext_0(),
	TaskScheduler_t1196198384::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (U3CPostU3Ec__AnonStorey0_t2101353913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	U3CPostU3Ec__AnonStorey0_t2101353913::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (TaskFactory_t2660013028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[2] = 
{
	TaskFactory_t2660013028::get_offset_of_scheduler_0(),
	TaskFactory_t2660013028::get_offset_of_cancellationToken_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (TaskExtensions_t2119891112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (Task_t3187275312), -1, sizeof(Task_t3187275312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2411[7] = 
{
	Task_t3187275312_StaticFields::get_offset_of_executionDepth_0(),
	Task_t3187275312_StaticFields::get_offset_of_immediateExecutor_1(),
	Task_t3187275312::get_offset_of_mutex_2(),
	Task_t3187275312::get_offset_of_continuations_3(),
	Task_t3187275312::get_offset_of_exception_4(),
	Task_t3187275312::get_offset_of_isCanceled_5(),
	Task_t3187275312::get_offset_of_isCompleted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (U3CContinueWithU3Ec__AnonStorey2_t408331350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[1] = 
{
	U3CContinueWithU3Ec__AnonStorey2_t408331350::get_offset_of_continuation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (CancellationTokenSource_t540272775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[2] = 
{
	CancellationTokenSource_t540272775::get_offset_of_mutex_0(),
	CancellationTokenSource_t540272775::get_offset_of_actions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (CancellationTokenRegistration_t2813424904)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	CancellationTokenRegistration_t2813424904::get_offset_of_action_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CancellationTokenRegistration_t2813424904::get_offset_of_source_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (CancellationToken_t784455623)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[1] = 
{
	CancellationToken_t784455623::get_offset_of_source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (AggregateException_t3586243216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	AggregateException_t3586243216::get_offset_of_U3CInnerExceptionsU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (MonoPInvokeCallbackAttribute_t264469961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (AppUtilPINVOKE_t1786794453), -1, sizeof(AppUtilPINVOKE_t1786794453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[2] = 
{
	AppUtilPINVOKE_t1786794453_StaticFields::get_offset_of_swigExceptionHelper_0(),
	AppUtilPINVOKE_t1786794453_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (SWIGExceptionHelper_t1372108196), -1, sizeof(SWIGExceptionHelper_t1372108196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2426[14] = 
{
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t1372108196_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (ExceptionDelegate_t467131861), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (ExceptionArgumentDelegate_t1533780051), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (SWIGPendingException_t3190247900), -1, sizeof(SWIGPendingException_t3190247900_StaticFields), sizeof(SWIGPendingException_t3190247900_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2429[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t3190247900_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (SWIGStringHelper_t1143654535), -1, sizeof(SWIGStringHelper_t1143654535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2430[1] = 
{
	SWIGStringHelper_t1143654535_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (SWIGStringDelegate_t3256258918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (AppUtil_t2855004820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (Variant_t1163908808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[2] = 
{
	Variant_t1163908808::get_offset_of_swigCPtr_0(),
	Variant_t1163908808::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (Type_t2611682988)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2434[11] = 
{
	Type_t2611682988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (VariantList_t2751083993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	VariantList_t2751083993::get_offset_of_swigCPtr_0(),
	VariantList_t2751083993::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (VariantListEnumerator_t1966549472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[4] = 
{
	VariantListEnumerator_t1966549472::get_offset_of_collectionRef_0(),
	VariantListEnumerator_t1966549472::get_offset_of_currentIndex_1(),
	VariantListEnumerator_t1966549472::get_offset_of_currentObject_2(),
	VariantListEnumerator_t1966549472::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (VariantVariantMap_t4053535076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[2] = 
{
	VariantVariantMap_t4053535076::get_offset_of_swigCPtr_0(),
	VariantVariantMap_t4053535076::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (VariantVariantMapEnumerator_t2068875317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[5] = 
{
	VariantVariantMapEnumerator_t2068875317::get_offset_of_collectionRef_0(),
	VariantVariantMapEnumerator_t2068875317::get_offset_of_keyCollection_1(),
	VariantVariantMapEnumerator_t2068875317::get_offset_of_currentIndex_2(),
	VariantVariantMapEnumerator_t2068875317::get_offset_of_currentObject_3(),
	VariantVariantMapEnumerator_t2068875317::get_offset_of_currentSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (GooglePlayServicesAvailability_t243528349)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2439[9] = 
{
	GooglePlayServicesAvailability_t243528349::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (LogLevel_t2506056272)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2440[7] = 
{
	LogLevel_t2506056272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (FirebaseApp_t2526288410), -1, sizeof(FirebaseApp_t2526288410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2441[17] = 
{
	FirebaseApp_t2526288410::get_offset_of_swigCPtr_0(),
	FirebaseApp_t2526288410::get_offset_of_swigCMemOwn_1(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_ANDROID_5(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_IOS_6(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_GENERIC_7(),
	FirebaseApp_t2526288410::get_offset_of_Disposed_8(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_nameToProxy_9(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_cPtrToProxy_10(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_cPtrRefCount_11(),
	FirebaseApp_t2526288410::get_offset_of_destroy_12(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	FirebaseApp_t2526288410_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (FirebaseHandler_t321821895), -1, sizeof(FirebaseHandler_t321821895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2442[5] = 
{
	FirebaseHandler_t321821895_StaticFields::get_offset_of_firebaseHandler_2(),
	FirebaseHandler_t321821895::get_offset_of_Updated_3(),
	FirebaseHandler_t321821895::get_offset_of_ApplicationFocusChanged_4(),
	FirebaseHandler_t321821895_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	FirebaseHandler_t321821895_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (LogMessageDelegate_t657899700), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (ApplicationFocusChangedEventArgs_t3956087507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[1] = 
{
	ApplicationFocusChangedEventArgs_t3956087507::get_offset_of_U3CHasFocusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (MonoPInvokeCallbackAttribute_t124655523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (DestroyDelegate_t2525580959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (CreateDelegate_t3131870060), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (U3CCreateU3Ec__AnonStorey0_t109813236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[1] = 
{
	U3CCreateU3Ec__AnonStorey0_t109813236::get_offset_of_options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (U3CCreateU3Ec__AnonStorey1_t2838696591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	U3CCreateU3Ec__AnonStorey1_t2838696591::get_offset_of_options_0(),
	U3CCreateU3Ec__AnonStorey1_t2838696591::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (U3CSetLogLevelU3Ec__AnonStorey2_t3050025917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[1] = 
{
	U3CSetLogLevelU3Ec__AnonStorey2_t3050025917::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (U3CU3Ec__AnonStorey3_t1009172714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[1] = 
{
	U3CU3Ec__AnonStorey3_t1009172714::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (U3CU3Ec__AnonStorey4_t1009172721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[1] = 
{
	U3CU3Ec__AnonStorey4_t1009172721::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (U3CCheckDependenciesU3Ec__AnonStorey5_t80273224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	U3CCheckDependenciesU3Ec__AnonStorey5_t80273224::get_offset_of_status_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417), -1, sizeof(U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[2] = 
{
	U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417::get_offset_of_task_0(),
	U3CFixDependenciesAsyncU3Ec__AnonStorey6_t3943580417_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (AppOptionsInternal_t1980997696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[2] = 
{
	AppOptionsInternal_t1980997696::get_offset_of_swigCPtr_0(),
	AppOptionsInternal_t1980997696::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (InitResult_t3767652586)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2456[3] = 
{
	InitResult_t3767652586::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (FutureVoid_t983301800), -1, sizeof(FutureVoid_t983301800_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[6] = 
{
	FutureVoid_t983301800::get_offset_of_swigCPtr_2(),
	FutureVoid_t983301800_StaticFields::get_offset_of_Callbacks_3(),
	FutureVoid_t983301800_StaticFields::get_offset_of_CallbackIndex_4(),
	FutureVoid_t983301800_StaticFields::get_offset_of_CallbackLock_5(),
	FutureVoid_t983301800::get_offset_of_callbackData_6(),
	FutureVoid_t983301800::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (Action_t1244810226), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (MonoPInvokeCallbackAttribute_t2381882871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (SWIG_CompletionDelegate_t2224793779), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t4029664161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t4029664161::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t4029664161::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (FutureString_t4072148675), -1, sizeof(FutureString_t4072148675_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[6] = 
{
	FutureString_t4072148675::get_offset_of_swigCPtr_2(),
	FutureString_t4072148675_StaticFields::get_offset_of_Callbacks_3(),
	FutureString_t4072148675_StaticFields::get_offset_of_CallbackIndex_4(),
	FutureString_t4072148675_StaticFields::get_offset_of_CallbackLock_5(),
	FutureString_t4072148675::get_offset_of_callbackData_6(),
	FutureString_t4072148675::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (Action_t2707960735), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (MonoPInvokeCallbackAttribute_t1863609720), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (SWIG_CompletionDelegate_t3186567461), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t1537100348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t1537100348::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t1537100348::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (CharVector_t1896987243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[2] = 
{
	CharVector_t1896987243::get_offset_of_swigCPtr_0(),
	CharVector_t1896987243::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (CharVectorEnumerator_t1643981309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[4] = 
{
	CharVectorEnumerator_t1643981309::get_offset_of_collectionRef_0(),
	CharVectorEnumerator_t1643981309::get_offset_of_currentIndex_1(),
	CharVectorEnumerator_t1643981309::get_offset_of_currentObject_2(),
	CharVectorEnumerator_t1643981309::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (StringList_t2653214935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[2] = 
{
	StringList_t2653214935::get_offset_of_swigCPtr_0(),
	StringList_t2653214935::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (StringListEnumerator_t435309527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[4] = 
{
	StringListEnumerator_t435309527::get_offset_of_collectionRef_0(),
	StringListEnumerator_t435309527::get_offset_of_currentIndex_1(),
	StringListEnumerator_t435309527::get_offset_of_currentObject_2(),
	StringListEnumerator_t435309527::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (StringStringMap_t4119824414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[2] = 
{
	StringStringMap_t4119824414::get_offset_of_swigCPtr_0(),
	StringStringMap_t4119824414::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (StringStringMapEnumerator_t2843440744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[5] = 
{
	StringStringMapEnumerator_t2843440744::get_offset_of_collectionRef_0(),
	StringStringMapEnumerator_t2843440744::get_offset_of_keyCollection_1(),
	StringStringMapEnumerator_t2843440744::get_offset_of_currentIndex_2(),
	StringStringMapEnumerator_t2843440744::get_offset_of_currentObject_3(),
	StringStringMapEnumerator_t2843440744::get_offset_of_currentSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (FutureBase_t1024553797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[2] = 
{
	FutureBase_t1024553797::get_offset_of_swigCPtr_0(),
	FutureBase_t1024553797::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (FutureStatus_t854570584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2474[4] = 
{
	FutureStatus_t854570584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (UnitySynchronizationContext_t705171362), -1, sizeof(UnitySynchronizationContext_t705171362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2475[5] = 
{
	UnitySynchronizationContext_t705171362_StaticFields::get_offset_of__instance_1(),
	UnitySynchronizationContext_t705171362::get_offset_of_queue_2(),
	UnitySynchronizationContext_t705171362::get_offset_of_behavior_3(),
	UnitySynchronizationContext_t705171362::get_offset_of_mainThreadId_4(),
	UnitySynchronizationContext_t705171362_StaticFields::get_offset_of_signalDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (SynchronizationContextBehavoir_t1466989729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[1] = 
{
	SynchronizationContextBehavoir_t1466989729::get_offset_of_callbackQueue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (U3CStartU3Ec__Iterator0_t1125778690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[6] = 
{
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U3CentryU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1125778690::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (U3CSignaledCoroutineU3Ec__Iterator0_t4124630109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[5] = 
{
	U3CSignaledCoroutineU3Ec__Iterator0_t4124630109::get_offset_of_coroutine_0(),
	U3CSignaledCoroutineU3Ec__Iterator0_t4124630109::get_offset_of_newSignal_1(),
	U3CSignaledCoroutineU3Ec__Iterator0_t4124630109::get_offset_of_U24current_2(),
	U3CSignaledCoroutineU3Ec__Iterator0_t4124630109::get_offset_of_U24disposing_3(),
	U3CSignaledCoroutineU3Ec__Iterator0_t4124630109::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey1_t914753507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey2_t2871068643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (U3CSendU3Ec__AnonStorey3_t2455143249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (U3CSendU3Ec__AnonStorey4_t2051858722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (UnityPlatformServices_t2540433941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (UnityLoggingService_t1821497488), -1, sizeof(UnityLoggingService_t1821497488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	UnityLoggingService_t1821497488_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (InstallRootCerts_t2034015421), -1, sizeof(InstallRootCerts_t2034015421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2485[6] = 
{
	InstallRootCerts_t2034015421_StaticFields::get_offset_of_Sync_0(),
	InstallRootCerts_t2034015421_StaticFields::get_offset_of__installedRoots_1(),
	InstallRootCerts_t2034015421_StaticFields::get_offset_of__instance_2(),
	InstallRootCerts_t2034015421::get_offset_of__needsCertificateWorkaround_3(),
	InstallRootCerts_t2034015421_StaticFields::get_offset_of_TrustedRoot_4(),
	InstallRootCerts_t2034015421_StaticFields::get_offset_of_IntermediateCA_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (UnityHttpFactoryService_t3235819319), -1, sizeof(UnityHttpFactoryService_t3235819319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	UnityHttpFactoryService_t3235819319_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (UnityConfigExtensions_t3428627910), -1, sizeof(UnityConfigExtensions_t3428627910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2487[1] = 
{
	UnityConfigExtensions_t3428627910_StaticFields::get_offset_of__instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (Services_t463074608), -1, sizeof(Services_t463074608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2488[6] = 
{
	Services_t463074608_StaticFields::get_offset_of_U3CAppConfigU3Ek__BackingField_0(),
	Services_t463074608_StaticFields::get_offset_of_U3CAuthU3Ek__BackingField_1(),
	Services_t463074608_StaticFields::get_offset_of_U3CRootCertsU3Ek__BackingField_2(),
	Services_t463074608_StaticFields::get_offset_of_U3CClockU3Ek__BackingField_3(),
	Services_t463074608_StaticFields::get_offset_of_U3CHttpFactoryU3Ek__BackingField_4(),
	Services_t463074608_StaticFields::get_offset_of_U3CLoggingU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (DebugLogger_t16656067), -1, sizeof(DebugLogger_t16656067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2490[1] = 
{
	DebugLogger_t16656067_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (NoopCertificateService_t402701936), -1, sizeof(NoopCertificateService_t402701936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2494[1] = 
{
	NoopCertificateService_t402701936_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (SystemClock_t2480061955), -1, sizeof(SystemClock_t2480061955_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2497[1] = 
{
	SystemClock_t2480061955_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (BaseAuthService_t3388892161), -1, sizeof(BaseAuthService_t3388892161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	BaseAuthService_t3388892161_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (AppConfigExtensions_t2108028355), -1, sizeof(AppConfigExtensions_t2108028355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2499[5] = 
{
	AppConfigExtensions_t2108028355_StaticFields::get_offset_of_DefaultUpdateUrl_0(),
	AppConfigExtensions_t2108028355_StaticFields::get_offset_of_Default_1(),
	AppConfigExtensions_t2108028355_StaticFields::get_offset_of_Sync_2(),
	AppConfigExtensions_t2108028355_StaticFields::get_offset_of__instance_3(),
	AppConfigExtensions_t2108028355_StaticFields::get_offset_of_SStringState_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
