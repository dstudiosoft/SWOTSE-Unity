﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppUtil
struct AppUtil_t2859790353;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t1988210674;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_GooglePlayServicesAvailabilit722840220.h"

// System.Void Firebase.AppUtil::.ctor()
extern "C"  void AppUtil__ctor_m3543230411 (AppUtil_t2859790353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::PollCallbacks()
extern "C"  void AppUtil_PollCallbacks_m1948862416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtil_AppEnableLogCallback_m1418645513 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.LogLevel Firebase.AppUtil::AppGetLogLevel()
extern "C"  int32_t AppUtil_AppGetLogLevel_m2540188292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtil_SetEnabledAllAppCallbacks_m2417423293 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtil_SetLogFunction_m1947305073 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.GooglePlayServicesAvailability Firebase.AppUtil::CheckAndroidDependencies()
extern "C"  int32_t AppUtil_CheckAndroidDependencies_m2373483093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task Firebase.AppUtil::FixAndroidDependenciesAsync()
extern "C"  Task_t1843236107 * AppUtil_FixAndroidDependenciesAsync_m1672186169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::InitializePlayServicesInternal()
extern "C"  void AppUtil_InitializePlayServicesInternal_m25221378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::TerminatePlayServicesInternal()
extern "C"  void AppUtil_TerminatePlayServicesInternal_m1908360659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
