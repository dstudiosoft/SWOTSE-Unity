﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IPAddressInformationImpl
struct IPAddressInformationImpl_t1429877825;
// System.Net.IPAddress
struct IPAddress_t1399971723;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"

// System.Void System.Net.NetworkInformation.IPAddressInformationImpl::.ctor(System.Net.IPAddress,System.Boolean,System.Boolean)
extern "C"  void IPAddressInformationImpl__ctor_m3339186200 (IPAddressInformationImpl_t1429877825 * __this, IPAddress_t1399971723 * ___address0, bool ___isDnsEligible1, bool ___isTransient2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.IPAddressInformationImpl::get_Address()
extern "C"  IPAddress_t1399971723 * IPAddressInformationImpl_get_Address_m3009922532 (IPAddressInformationImpl_t1429877825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.IPAddressInformationImpl::get_IsDnsEligible()
extern "C"  bool IPAddressInformationImpl_get_IsDnsEligible_m2077588632 (IPAddressInformationImpl_t1429877825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.IPAddressInformationImpl::get_IsTransient()
extern "C"  bool IPAddressInformationImpl_get_IsTransient_m27116856 (IPAddressInformationImpl_t1429877825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
