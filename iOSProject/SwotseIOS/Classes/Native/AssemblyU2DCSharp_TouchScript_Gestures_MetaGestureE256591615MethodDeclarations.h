﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.MetaGestureEventArgs
struct MetaGestureEventArgs_t256591615;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Gestures.MetaGestureEventArgs::.ctor(TouchScript.TouchPoint)
extern "C"  void MetaGestureEventArgs__ctor_m1129513157 (MetaGestureEventArgs_t256591615 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.TouchPoint TouchScript.Gestures.MetaGestureEventArgs::get_Touch()
extern "C"  TouchPoint_t959629083 * MetaGestureEventArgs_get_Touch_m920373360 (MetaGestureEventArgs_t256591615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.MetaGestureEventArgs::set_Touch(TouchScript.TouchPoint)
extern "C"  void MetaGestureEventArgs_set_Touch_m792179677 (MetaGestureEventArgs_t256591615 * __this, TouchPoint_t959629083 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
