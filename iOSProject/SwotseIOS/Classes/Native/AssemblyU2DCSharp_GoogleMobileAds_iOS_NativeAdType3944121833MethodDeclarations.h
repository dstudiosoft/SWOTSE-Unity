﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleMobileAds.iOS.NativeAdTypes
struct NativeAdTypes_t3944121833;
struct NativeAdTypes_t3944121833_marshaled_pinvoke;
struct NativeAdTypes_t3944121833_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct NativeAdTypes_t3944121833;
struct NativeAdTypes_t3944121833_marshaled_pinvoke;

extern "C" void NativeAdTypes_t3944121833_marshal_pinvoke(const NativeAdTypes_t3944121833& unmarshaled, NativeAdTypes_t3944121833_marshaled_pinvoke& marshaled);
extern "C" void NativeAdTypes_t3944121833_marshal_pinvoke_back(const NativeAdTypes_t3944121833_marshaled_pinvoke& marshaled, NativeAdTypes_t3944121833& unmarshaled);
extern "C" void NativeAdTypes_t3944121833_marshal_pinvoke_cleanup(NativeAdTypes_t3944121833_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NativeAdTypes_t3944121833;
struct NativeAdTypes_t3944121833_marshaled_com;

extern "C" void NativeAdTypes_t3944121833_marshal_com(const NativeAdTypes_t3944121833& unmarshaled, NativeAdTypes_t3944121833_marshaled_com& marshaled);
extern "C" void NativeAdTypes_t3944121833_marshal_com_back(const NativeAdTypes_t3944121833_marshaled_com& marshaled, NativeAdTypes_t3944121833& unmarshaled);
extern "C" void NativeAdTypes_t3944121833_marshal_com_cleanup(NativeAdTypes_t3944121833_marshaled_com& marshaled);
