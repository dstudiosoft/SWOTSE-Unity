﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1004715516MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1954976213(__this, ___dic0, method) ((  void (*) (Enumerator_t2533878127 *, SortedDictionary_2_t1165316627 *, const MethodInfo*))Enumerator__ctor_m1530705605_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2691418696(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3759775480_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1047360725(__this, method) ((  Il2CppObject * (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2082684101_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3829714765(__this, method) ((  Il2CppObject * (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8986413_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m768242303(__this, method) ((  Il2CppObject * (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3256116175_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2624986019(__this, method) ((  void (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1941930227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::get_Current()
#define Enumerator_get_Current_m1659156186(__this, method) ((  KeyValuePair_2_t1568017256  (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_get_Current_m2922944951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::MoveNext()
#define Enumerator_MoveNext_m752185619(__this, method) ((  bool (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_MoveNext_m1169387503_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::Dispose()
#define Enumerator_Dispose_m2859102530(__this, method) ((  void (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_Dispose_m427679586_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,SmartLocalization.LocalizedObject>::get_CurrentNode()
#define Enumerator_get_CurrentNode_m4197249989(__this, method) ((  Node_t4042612855 * (*) (Enumerator_t2533878127 *, const MethodInfo*))Enumerator_get_CurrentNode_m1168849237_gshared)(__this, method)
