﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.GenericIdentity
struct GenericIdentity_t607870731;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Security.Principal.GenericIdentity::.ctor(System.String,System.String)
extern "C"  void GenericIdentity__ctor_m2200963550 (GenericIdentity_t607870731 * __this, String_t* ___name0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
