﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2095618989(__this, ___host0, method) ((  void (*) (Enumerator_t1218945742 *, Dictionary_2_t2824409600 *, const MethodInfo*))Enumerator__ctor_m3400877137_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2150587466(__this, method) ((  Il2CppObject * (*) (Enumerator_t1218945742 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m247337542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m744674708(__this, method) ((  void (*) (Enumerator_t1218945742 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4166432760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::Dispose()
#define Enumerator_Dispose_m4086234613(__this, method) ((  void (*) (Enumerator_t1218945742 *, const MethodInfo*))Enumerator_Dispose_m3239242417_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::MoveNext()
#define Enumerator_MoveNext_m2875601051(__this, method) ((  bool (*) (Enumerator_t1218945742 *, const MethodInfo*))Enumerator_MoveNext_m1062748132_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,AllianceModel>::get_Current()
#define Enumerator_get_Current_m717440335(__this, method) ((  int64_t (*) (Enumerator_t1218945742 *, const MethodInfo*))Enumerator_get_Current_m3153023504_gshared)(__this, method)
