﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.NetTcpStyleUriParser
struct NetTcpStyleUriParser_t4124623308;

#include "codegen/il2cpp-codegen.h"

// System.Void System.NetTcpStyleUriParser::.ctor()
extern "C"  void NetTcpStyleUriParser__ctor_m2979298155 (NetTcpStyleUriParser_t4124623308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
