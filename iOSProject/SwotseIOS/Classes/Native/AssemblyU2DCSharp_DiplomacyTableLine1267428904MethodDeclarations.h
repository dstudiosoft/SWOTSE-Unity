﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiplomacyTableLine
struct DiplomacyTableLine_t1267428904;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DiplomacyTableLine::.ctor()
extern "C"  void DiplomacyTableLine__ctor_m1330466403 (DiplomacyTableLine_t1267428904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::SetNickName(System.String)
extern "C"  void DiplomacyTableLine_SetNickName_m4040680239 (DiplomacyTableLine_t1267428904 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::SetKnight(System.String)
extern "C"  void DiplomacyTableLine_SetKnight_m2275950904 (DiplomacyTableLine_t1267428904 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::SetCity(System.String)
extern "C"  void DiplomacyTableLine_SetCity_m3972185894 (DiplomacyTableLine_t1267428904 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::SetCoord(System.String)
extern "C"  void DiplomacyTableLine_SetCoord_m3111929496 (DiplomacyTableLine_t1267428904 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::SetDate(System.String)
extern "C"  void DiplomacyTableLine_SetDate_m3198075731 (DiplomacyTableLine_t1267428904 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::DismissTroops()
extern "C"  void DiplomacyTableLine_DismissTroops_m1915371162 (DiplomacyTableLine_t1267428904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyTableLine::ShowTroops()
extern "C"  void DiplomacyTableLine_ShowTroops_m3765417713 (DiplomacyTableLine_t1267428904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
