﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<SetLogLevel>c__AnonStorey2
struct U3CSetLogLevelU3Ec__AnonStorey2_t567363225;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/<SetLogLevel>c__AnonStorey2::.ctor()
extern "C"  void U3CSetLogLevelU3Ec__AnonStorey2__ctor_m3083539886 (U3CSetLogLevelU3Ec__AnonStorey2_t567363225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/<SetLogLevel>c__AnonStorey2::<>m__0()
extern "C"  void U3CSetLogLevelU3Ec__AnonStorey2_U3CU3Em__0_m2649439393 (U3CSetLogLevelU3Ec__AnonStorey2_t567363225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
