﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RankIconUpdater
struct  RankIconUpdater_t3495770272  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image RankIconUpdater::rankIcon
	Image_t2042527209 * ___rankIcon_2;
	// System.String RankIconUpdater::rank
	String_t* ___rank_3;

public:
	inline static int32_t get_offset_of_rankIcon_2() { return static_cast<int32_t>(offsetof(RankIconUpdater_t3495770272, ___rankIcon_2)); }
	inline Image_t2042527209 * get_rankIcon_2() const { return ___rankIcon_2; }
	inline Image_t2042527209 ** get_address_of_rankIcon_2() { return &___rankIcon_2; }
	inline void set_rankIcon_2(Image_t2042527209 * value)
	{
		___rankIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___rankIcon_2, value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(RankIconUpdater_t3495770272, ___rank_3)); }
	inline String_t* get_rank_3() const { return ___rank_3; }
	inline String_t** get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(String_t* value)
	{
		___rank_3 = value;
		Il2CppCodeGenWriteBarrier(&___rank_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
