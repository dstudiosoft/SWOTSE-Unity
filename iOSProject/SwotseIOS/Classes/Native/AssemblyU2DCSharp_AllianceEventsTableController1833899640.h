﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceEventsLine
struct AllianceEventsLine_t326631842;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventsTableController
struct  AllianceEventsTableController_t1833899640  : public MonoBehaviour_t1158329972
{
public:
	// AllianceEventsLine AllianceEventsTableController::m_cellPrefab
	AllianceEventsLine_t326631842 * ___m_cellPrefab_2;
	// Tacticsoft.TableView AllianceEventsTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Int32 AllianceEventsTableController::m_numRows
	int32_t ___m_numRows_4;
	// System.Collections.ArrayList AllianceEventsTableController::allianceEvents
	ArrayList_t4252133567 * ___allianceEvents_5;
	// SimpleEvent AllianceEventsTableController::onGetAllianceEvents
	SimpleEvent_t1509017202 * ___onGetAllianceEvents_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t1833899640, ___m_cellPrefab_2)); }
	inline AllianceEventsLine_t326631842 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline AllianceEventsLine_t326631842 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(AllianceEventsLine_t326631842 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t1833899640, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_m_numRows_4() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t1833899640, ___m_numRows_4)); }
	inline int32_t get_m_numRows_4() const { return ___m_numRows_4; }
	inline int32_t* get_address_of_m_numRows_4() { return &___m_numRows_4; }
	inline void set_m_numRows_4(int32_t value)
	{
		___m_numRows_4 = value;
	}

	inline static int32_t get_offset_of_allianceEvents_5() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t1833899640, ___allianceEvents_5)); }
	inline ArrayList_t4252133567 * get_allianceEvents_5() const { return ___allianceEvents_5; }
	inline ArrayList_t4252133567 ** get_address_of_allianceEvents_5() { return &___allianceEvents_5; }
	inline void set_allianceEvents_5(ArrayList_t4252133567 * value)
	{
		___allianceEvents_5 = value;
		Il2CppCodeGenWriteBarrier(&___allianceEvents_5, value);
	}

	inline static int32_t get_offset_of_onGetAllianceEvents_6() { return static_cast<int32_t>(offsetof(AllianceEventsTableController_t1833899640, ___onGetAllianceEvents_6)); }
	inline SimpleEvent_t1509017202 * get_onGetAllianceEvents_6() const { return ___onGetAllianceEvents_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetAllianceEvents_6() { return &___onGetAllianceEvents_6; }
	inline void set_onGetAllianceEvents_6(SimpleEvent_t1509017202 * value)
	{
		___onGetAllianceEvents_6 = value;
		Il2CppCodeGenWriteBarrier(&___onGetAllianceEvents_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
