﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct ValueCollection_t1591485453;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Collections.Generic.IEnumerator`1<TouchScript.Gestures.UI.UIGesture/TouchData>
struct IEnumerator_1_t1356123802;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// TouchScript.Gestures.UI.UIGesture/TouchData[]
struct TouchDataU5BU5D_t599316446;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va279991078.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4147791912_gshared (ValueCollection_t1591485453 * __this, Dictionary_2_t2888425610 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m4147791912(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1591485453 *, Dictionary_2_t2888425610 *, const MethodInfo*))ValueCollection__ctor_m4147791912_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4062473294_gshared (ValueCollection_t1591485453 * __this, TouchData_t3880599975  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4062473294(__this, ___item0, method) ((  void (*) (ValueCollection_t1591485453 *, TouchData_t3880599975 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4062473294_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3645469155_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3645469155(__this, method) ((  void (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3645469155_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2719571566_gshared (ValueCollection_t1591485453 * __this, TouchData_t3880599975  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2719571566(__this, ___item0, method) ((  bool (*) (ValueCollection_t1591485453 *, TouchData_t3880599975 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2719571566_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1339517963_gshared (ValueCollection_t1591485453 * __this, TouchData_t3880599975  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1339517963(__this, ___item0, method) ((  bool (*) (ValueCollection_t1591485453 *, TouchData_t3880599975 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1339517963_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4100708729_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4100708729(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4100708729_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2120712009_gshared (ValueCollection_t1591485453 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2120712009(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1591485453 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2120712009_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4019191914_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4019191914(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4019191914_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m752637583_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m752637583(__this, method) ((  bool (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m752637583_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m747628381_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m747628381(__this, method) ((  bool (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m747628381_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2728036309_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2728036309(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2728036309_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4209035835_gshared (ValueCollection_t1591485453 * __this, TouchDataU5BU5D_t599316446* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4209035835(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1591485453 *, TouchDataU5BU5D_t599316446*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4209035835_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::GetEnumerator()
extern "C"  Enumerator_t279991078  ValueCollection_GetEnumerator_m55379120_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m55379120(__this, method) ((  Enumerator_t279991078  (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_GetEnumerator_m55379120_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1953543257_gshared (ValueCollection_t1591485453 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1953543257(__this, method) ((  int32_t (*) (ValueCollection_t1591485453 *, const MethodInfo*))ValueCollection_get_Count_m1953543257_gshared)(__this, method)
