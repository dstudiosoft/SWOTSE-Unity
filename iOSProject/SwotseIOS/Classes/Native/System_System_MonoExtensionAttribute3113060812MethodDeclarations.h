﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MonoExtensionAttribute
struct MonoExtensionAttribute_t3113060812;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.MonoExtensionAttribute::.ctor(System.String)
extern "C"  void MonoExtensionAttribute__ctor_m2957649289 (MonoExtensionAttribute_t3113060812 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
