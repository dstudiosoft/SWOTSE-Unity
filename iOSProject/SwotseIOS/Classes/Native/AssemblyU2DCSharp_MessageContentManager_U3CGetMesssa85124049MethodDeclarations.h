﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageContentManager/<GetMesssageDetails>c__Iterator13
struct U3CGetMesssageDetailsU3Ec__Iterator13_t85124049;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageContentManager/<GetMesssageDetails>c__Iterator13::.ctor()
extern "C"  void U3CGetMesssageDetailsU3Ec__Iterator13__ctor_m904385636 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageContentManager/<GetMesssageDetails>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetMesssageDetailsU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3576507106 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MessageContentManager/<GetMesssageDetails>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetMesssageDetailsU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3377422202 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageContentManager/<GetMesssageDetails>c__Iterator13::MoveNext()
extern "C"  bool U3CGetMesssageDetailsU3Ec__Iterator13_MoveNext_m3634734172 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager/<GetMesssageDetails>c__Iterator13::Dispose()
extern "C"  void U3CGetMesssageDetailsU3Ec__Iterator13_Dispose_m1170520715 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageContentManager/<GetMesssageDetails>c__Iterator13::Reset()
extern "C"  void U3CGetMesssageDetailsU3Ec__Iterator13_Reset_m542186809 (U3CGetMesssageDetailsU3Ec__Iterator13_t85124049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
