﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource SoundManager::music
	AudioSource_t1135106623 * ___music_3;

public:
	inline static int32_t get_offset_of_music_3() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___music_3)); }
	inline AudioSource_t1135106623 * get_music_3() const { return ___music_3; }
	inline AudioSource_t1135106623 ** get_address_of_music_3() { return &___music_3; }
	inline void set_music_3(AudioSource_t1135106623 * value)
	{
		___music_3 = value;
		Il2CppCodeGenWriteBarrier(&___music_3, value);
	}
};

struct SoundManager_t654432262_StaticFields
{
public:
	// SoundManager SoundManager::instance
	SoundManager_t654432262 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SoundManager_t654432262_StaticFields, ___instance_2)); }
	inline SoundManager_t654432262 * get_instance_2() const { return ___instance_2; }
	inline SoundManager_t654432262 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SoundManager_t654432262 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
