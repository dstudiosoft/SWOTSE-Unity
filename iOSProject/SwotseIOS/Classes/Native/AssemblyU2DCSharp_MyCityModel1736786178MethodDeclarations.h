﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyCityModel
struct MyCityModel_t1736786178;

#include "codegen/il2cpp-codegen.h"

// System.Void MyCityModel::.ctor()
extern "C"  void MyCityModel__ctor_m4107773287 (MyCityModel_t1736786178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
