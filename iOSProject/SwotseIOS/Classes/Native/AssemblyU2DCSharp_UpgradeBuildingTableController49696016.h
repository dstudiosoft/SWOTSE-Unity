﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UpgradeBuildingTableLine
struct UpgradeBuildingTableLine_t750928550;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// IReloadable
struct IReloadable_t861412162;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeBuildingTableController
struct  UpgradeBuildingTableController_t49696016  : public MonoBehaviour_t1158329972
{
public:
	// UpgradeBuildingTableLine UpgradeBuildingTableController::m_cellPrefab
	UpgradeBuildingTableLine_t750928550 * ___m_cellPrefab_2;
	// Tacticsoft.TableView UpgradeBuildingTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.String UpgradeBuildingTableController::content
	String_t* ___content_4;
	// System.Collections.ArrayList UpgradeBuildingTableController::buildingsList
	ArrayList_t4252133567 * ___buildingsList_5;
	// System.Int64 UpgradeBuildingTableController::item_id
	int64_t ___item_id_6;
	// IReloadable UpgradeBuildingTableController::owner
	Il2CppObject * ___owner_7;
	// System.Int64 UpgradeBuildingTableController::building_id
	int64_t ___building_id_8;
	// System.Int32 UpgradeBuildingTableController::m_numRows
	int32_t ___m_numRows_9;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___m_cellPrefab_2)); }
	inline UpgradeBuildingTableLine_t750928550 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline UpgradeBuildingTableLine_t750928550 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(UpgradeBuildingTableLine_t750928550 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___content_4)); }
	inline String_t* get_content_4() const { return ___content_4; }
	inline String_t** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(String_t* value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}

	inline static int32_t get_offset_of_buildingsList_5() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___buildingsList_5)); }
	inline ArrayList_t4252133567 * get_buildingsList_5() const { return ___buildingsList_5; }
	inline ArrayList_t4252133567 ** get_address_of_buildingsList_5() { return &___buildingsList_5; }
	inline void set_buildingsList_5(ArrayList_t4252133567 * value)
	{
		___buildingsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___buildingsList_5, value);
	}

	inline static int32_t get_offset_of_item_id_6() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___item_id_6)); }
	inline int64_t get_item_id_6() const { return ___item_id_6; }
	inline int64_t* get_address_of_item_id_6() { return &___item_id_6; }
	inline void set_item_id_6(int64_t value)
	{
		___item_id_6 = value;
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___owner_7)); }
	inline Il2CppObject * get_owner_7() const { return ___owner_7; }
	inline Il2CppObject ** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(Il2CppObject * value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier(&___owner_7, value);
	}

	inline static int32_t get_offset_of_building_id_8() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___building_id_8)); }
	inline int64_t get_building_id_8() const { return ___building_id_8; }
	inline int64_t* get_address_of_building_id_8() { return &___building_id_8; }
	inline void set_building_id_8(int64_t value)
	{
		___building_id_8 = value;
	}

	inline static int32_t get_offset_of_m_numRows_9() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016, ___m_numRows_9)); }
	inline int32_t get_m_numRows_9() const { return ___m_numRows_9; }
	inline int32_t* get_address_of_m_numRows_9() { return &___m_numRows_9; }
	inline void set_m_numRows_9(int32_t value)
	{
		___m_numRows_9 = value;
	}
};

struct UpgradeBuildingTableController_t49696016_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UpgradeBuildingTableController::<>f__switch$map28
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map28_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UpgradeBuildingTableController::<>f__switch$map29
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map29_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_10() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016_StaticFields, ___U3CU3Ef__switchU24map28_10)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map28_10() const { return ___U3CU3Ef__switchU24map28_10; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map28_10() { return &___U3CU3Ef__switchU24map28_10; }
	inline void set_U3CU3Ef__switchU24map28_10(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map28_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map28_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map29_11() { return static_cast<int32_t>(offsetof(UpgradeBuildingTableController_t49696016_StaticFields, ___U3CU3Ef__switchU24map29_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map29_11() const { return ___U3CU3Ef__switchU24map29_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map29_11() { return &___U3CU3Ef__switchU24map29_11; }
	inline void set_U3CU3Ef__switchU24map29_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map29_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map29_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
