﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t2046943414;
// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t1850351419;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t1236821014;
// TUIOsharp.DataProcessors.IDataProcessor
struct IDataProcessor_t2214055399;

#include "mscorlib_System_Array3829468939.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioBlob2046943414.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioCursor1850351419.h"
#include "TUIOsharp_TUIOsharp_Entities_TuioObject1236821014.h"

#pragma once
// TUIOsharp.Entities.TuioBlob[]
struct TuioBlobU5BU5D_t3918161843  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TuioBlob_t2046943414 * m_Items[1];

public:
	inline TuioBlob_t2046943414 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TuioBlob_t2046943414 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TuioBlob_t2046943414 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TUIOsharp.Entities.TuioCursor[]
struct TuioCursorU5BU5D_t885656954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TuioCursor_t1850351419 * m_Items[1];

public:
	inline TuioCursor_t1850351419 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TuioCursor_t1850351419 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TuioCursor_t1850351419 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TUIOsharp.Entities.TuioObject[]
struct TuioObjectU5BU5D_t3851389651  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TuioObject_t1236821014 * m_Items[1];

public:
	inline TuioObject_t1236821014 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TuioObject_t1236821014 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TuioObject_t1236821014 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TUIOsharp.DataProcessors.IDataProcessor[]
struct IDataProcessorU5BU5D_t215962270  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
