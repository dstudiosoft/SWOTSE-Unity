﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t1165316627;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;
// SmartLocalization.ILocalizedAssetLoader
struct ILocalizedAssetLoader_t2275178333;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageDataHandler
struct  LanguageDataHandler_t3279109820  : public Il2CppObject
{
public:
	// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageDataHandler::loadedValuesDictionary
	SortedDictionary_2_t1165316627 * ___loadedValuesDictionary_0;
	// System.Boolean SmartLocalization.LanguageDataHandler::verboseLogging
	bool ___verboseLogging_1;
	// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageDataHandler::loadedCultureInfo
	SmartCultureInfo_t2361725737 * ___loadedCultureInfo_2;
	// SmartLocalization.ILocalizedAssetLoader SmartLocalization.LanguageDataHandler::assetLoader
	Il2CppObject * ___assetLoader_3;

public:
	inline static int32_t get_offset_of_loadedValuesDictionary_0() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3279109820, ___loadedValuesDictionary_0)); }
	inline SortedDictionary_2_t1165316627 * get_loadedValuesDictionary_0() const { return ___loadedValuesDictionary_0; }
	inline SortedDictionary_2_t1165316627 ** get_address_of_loadedValuesDictionary_0() { return &___loadedValuesDictionary_0; }
	inline void set_loadedValuesDictionary_0(SortedDictionary_2_t1165316627 * value)
	{
		___loadedValuesDictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&___loadedValuesDictionary_0, value);
	}

	inline static int32_t get_offset_of_verboseLogging_1() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3279109820, ___verboseLogging_1)); }
	inline bool get_verboseLogging_1() const { return ___verboseLogging_1; }
	inline bool* get_address_of_verboseLogging_1() { return &___verboseLogging_1; }
	inline void set_verboseLogging_1(bool value)
	{
		___verboseLogging_1 = value;
	}

	inline static int32_t get_offset_of_loadedCultureInfo_2() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3279109820, ___loadedCultureInfo_2)); }
	inline SmartCultureInfo_t2361725737 * get_loadedCultureInfo_2() const { return ___loadedCultureInfo_2; }
	inline SmartCultureInfo_t2361725737 ** get_address_of_loadedCultureInfo_2() { return &___loadedCultureInfo_2; }
	inline void set_loadedCultureInfo_2(SmartCultureInfo_t2361725737 * value)
	{
		___loadedCultureInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadedCultureInfo_2, value);
	}

	inline static int32_t get_offset_of_assetLoader_3() { return static_cast<int32_t>(offsetof(LanguageDataHandler_t3279109820, ___assetLoader_3)); }
	inline Il2CppObject * get_assetLoader_3() const { return ___assetLoader_3; }
	inline Il2CppObject ** get_address_of_assetLoader_3() { return &___assetLoader_3; }
	inline void set_assetLoader_3(Il2CppObject * value)
	{
		___assetLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___assetLoader_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
