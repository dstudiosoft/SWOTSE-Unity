﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<TouchScript.Gestures.UI.UIGesture/TouchData>
struct DefaultComparer_t1207562840;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor()
extern "C"  void DefaultComparer__ctor_m1460774997_gshared (DefaultComparer_t1207562840 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1460774997(__this, method) ((  void (*) (DefaultComparer_t1207562840 *, const MethodInfo*))DefaultComparer__ctor_m1460774997_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<TouchScript.Gestures.UI.UIGesture/TouchData>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3452631736_gshared (DefaultComparer_t1207562840 * __this, TouchData_t3880599975  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3452631736(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1207562840 *, TouchData_t3880599975 , const MethodInfo*))DefaultComparer_GetHashCode_m3452631736_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<TouchScript.Gestures.UI.UIGesture/TouchData>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4199423640_gshared (DefaultComparer_t1207562840 * __this, TouchData_t3880599975  ___x0, TouchData_t3880599975  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4199423640(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1207562840 *, TouchData_t3880599975 , TouchData_t3880599975 , const MethodInfo*))DefaultComparer_Equals_m4199423640_gshared)(__this, ___x0, ___y1, method)
