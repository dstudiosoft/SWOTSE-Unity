﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>
struct DefaultComparer_t2871417849;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>::.ctor()
extern "C"  void DefaultComparer__ctor_m951731477_gshared (DefaultComparer_t2871417849 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m951731477(__this, method) ((  void (*) (DefaultComparer_t2871417849 *, const MethodInfo*))DefaultComparer__ctor_m951731477_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1870212546_gshared (DefaultComparer_t2871417849 * __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1870212546(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2871417849 *, RaycastHit_t87180320 , RaycastHit_t87180320 , const MethodInfo*))DefaultComparer_Compare_m1870212546_gshared)(__this, ___x0, ___y1, method)
