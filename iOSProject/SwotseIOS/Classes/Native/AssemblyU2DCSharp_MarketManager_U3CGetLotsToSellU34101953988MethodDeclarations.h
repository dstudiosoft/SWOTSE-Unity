﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager/<GetLotsToSell>c__IteratorF
struct U3CGetLotsToSellU3Ec__IteratorF_t4101953988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketManager/<GetLotsToSell>c__IteratorF::.ctor()
extern "C"  void U3CGetLotsToSellU3Ec__IteratorF__ctor_m727873081 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<GetLotsToSell>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetLotsToSellU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3071163775 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<GetLotsToSell>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetLotsToSellU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2114371559 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketManager/<GetLotsToSell>c__IteratorF::MoveNext()
extern "C"  bool U3CGetLotsToSellU3Ec__IteratorF_MoveNext_m1040308811 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<GetLotsToSell>c__IteratorF::Dispose()
extern "C"  void U3CGetLotsToSellU3Ec__IteratorF_Dispose_m200595254 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<GetLotsToSell>c__IteratorF::Reset()
extern "C"  void U3CGetLotsToSellU3Ec__IteratorF_Reset_m1350652708 (U3CGetLotsToSellU3Ec__IteratorF_t4101953988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
