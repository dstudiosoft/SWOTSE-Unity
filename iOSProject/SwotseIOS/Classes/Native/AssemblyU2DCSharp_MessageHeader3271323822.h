﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageHeader
struct  MessageHeader_t3271323822  : public Il2CppObject
{
public:
	// System.Int64 MessageHeader::id
	int64_t ___id_0;
	// System.String MessageHeader::fromUserName
	String_t* ___fromUserName_1;
	// System.String MessageHeader::subject
	String_t* ___subject_2;
	// System.Boolean MessageHeader::isRead
	bool ___isRead_3;
	// System.DateTime MessageHeader::creation_date
	DateTime_t693205669  ___creation_date_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MessageHeader_t3271323822, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_fromUserName_1() { return static_cast<int32_t>(offsetof(MessageHeader_t3271323822, ___fromUserName_1)); }
	inline String_t* get_fromUserName_1() const { return ___fromUserName_1; }
	inline String_t** get_address_of_fromUserName_1() { return &___fromUserName_1; }
	inline void set_fromUserName_1(String_t* value)
	{
		___fromUserName_1 = value;
		Il2CppCodeGenWriteBarrier(&___fromUserName_1, value);
	}

	inline static int32_t get_offset_of_subject_2() { return static_cast<int32_t>(offsetof(MessageHeader_t3271323822, ___subject_2)); }
	inline String_t* get_subject_2() const { return ___subject_2; }
	inline String_t** get_address_of_subject_2() { return &___subject_2; }
	inline void set_subject_2(String_t* value)
	{
		___subject_2 = value;
		Il2CppCodeGenWriteBarrier(&___subject_2, value);
	}

	inline static int32_t get_offset_of_isRead_3() { return static_cast<int32_t>(offsetof(MessageHeader_t3271323822, ___isRead_3)); }
	inline bool get_isRead_3() const { return ___isRead_3; }
	inline bool* get_address_of_isRead_3() { return &___isRead_3; }
	inline void set_isRead_3(bool value)
	{
		___isRead_3 = value;
	}

	inline static int32_t get_offset_of_creation_date_4() { return static_cast<int32_t>(offsetof(MessageHeader_t3271323822, ___creation_date_4)); }
	inline DateTime_t693205669  get_creation_date_4() const { return ___creation_date_4; }
	inline DateTime_t693205669 * get_address_of_creation_date_4() { return &___creation_date_4; }
	inline void set_creation_date_4(DateTime_t693205669  value)
	{
		___creation_date_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
