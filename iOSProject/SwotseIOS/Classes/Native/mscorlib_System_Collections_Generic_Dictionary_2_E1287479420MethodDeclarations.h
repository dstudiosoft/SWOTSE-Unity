﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1522658287(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1287479420 *, Dictionary_2_t4262422014 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1428619568(__this, method) ((  Il2CppObject * (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m521181828(__this, method) ((  void (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2713784297(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2978882574(__this, method) ((  Il2CppObject * (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m362404396(__this, method) ((  Il2CppObject * (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::MoveNext()
#define Enumerator_MoveNext_m2291564716(__this, method) ((  bool (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::get_Current()
#define Enumerator_get_Current_m2269138708(__this, method) ((  KeyValuePair_2_t2019767236  (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1814599731(__this, method) ((  int32_t (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3826610963(__this, method) ((  TouchPoint_t959629083 * (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::Reset()
#define Enumerator_Reset_m1307366121(__this, method) ((  void (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::VerifyState()
#define Enumerator_VerifyState_m156948072(__this, method) ((  void (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1453854932(__this, method) ((  void (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.TouchPoint>::Dispose()
#define Enumerator_Dispose_m1876555395(__this, method) ((  void (*) (Enumerator_t1287479420 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
