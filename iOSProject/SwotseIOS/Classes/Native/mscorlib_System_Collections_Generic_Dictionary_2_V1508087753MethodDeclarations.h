﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1062944964(__this, ___host0, method) ((  void (*) (Enumerator_t1508087753 *, Dictionary_2_t4116522285 *, const MethodInfo*))Enumerator__ctor_m419014865_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2034076883(__this, method) ((  Il2CppObject * (*) (Enumerator_t1508087753 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2714845987(__this, method) ((  void (*) (Enumerator_t1508087753 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::Dispose()
#define Enumerator_Dispose_m3482412440(__this, method) ((  void (*) (Enumerator_t1508087753 *, const MethodInfo*))Enumerator_Dispose_m942415041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::MoveNext()
#define Enumerator_MoveNext_m1284082850(__this, method) ((  bool (*) (Enumerator_t1508087753 *, const MethodInfo*))Enumerator_MoveNext_m2045321910_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,WorldMapTimer>::get_Current()
#define Enumerator_get_Current_m3126140425(__this, method) ((  WorldMapTimer_t3127003883 * (*) (Enumerator_t1508087753 *, const MethodInfo*))Enumerator_get_Current_m1952080304_gshared)(__this, method)
