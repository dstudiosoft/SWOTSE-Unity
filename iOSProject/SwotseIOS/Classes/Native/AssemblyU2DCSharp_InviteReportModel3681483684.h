﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InviteReportModel
struct  InviteReportModel_t3681483684  : public Il2CppObject
{
public:
	// System.Int64 InviteReportModel::id
	int64_t ___id_0;
	// System.Int64 InviteReportModel::inviteFromId
	int64_t ___inviteFromId_1;
	// System.String InviteReportModel::inviteFromName
	String_t* ___inviteFromName_2;
	// System.Int64 InviteReportModel::inviteToId
	int64_t ___inviteToId_3;
	// System.String InviteReportModel::inviteToName
	String_t* ___inviteToName_4;
	// System.Boolean InviteReportModel::isRead
	bool ___isRead_5;
	// System.Int64 InviteReportModel::allianceId
	int64_t ___allianceId_6;
	// System.String InviteReportModel::allianceName
	String_t* ___allianceName_7;
	// System.Int64 InviteReportModel::allianceRank
	int64_t ___allianceRank_8;
	// System.String InviteReportModel::type
	String_t* ___type_9;
	// System.DateTime InviteReportModel::creationDate
	DateTime_t693205669  ___creationDate_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_inviteFromId_1() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___inviteFromId_1)); }
	inline int64_t get_inviteFromId_1() const { return ___inviteFromId_1; }
	inline int64_t* get_address_of_inviteFromId_1() { return &___inviteFromId_1; }
	inline void set_inviteFromId_1(int64_t value)
	{
		___inviteFromId_1 = value;
	}

	inline static int32_t get_offset_of_inviteFromName_2() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___inviteFromName_2)); }
	inline String_t* get_inviteFromName_2() const { return ___inviteFromName_2; }
	inline String_t** get_address_of_inviteFromName_2() { return &___inviteFromName_2; }
	inline void set_inviteFromName_2(String_t* value)
	{
		___inviteFromName_2 = value;
		Il2CppCodeGenWriteBarrier(&___inviteFromName_2, value);
	}

	inline static int32_t get_offset_of_inviteToId_3() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___inviteToId_3)); }
	inline int64_t get_inviteToId_3() const { return ___inviteToId_3; }
	inline int64_t* get_address_of_inviteToId_3() { return &___inviteToId_3; }
	inline void set_inviteToId_3(int64_t value)
	{
		___inviteToId_3 = value;
	}

	inline static int32_t get_offset_of_inviteToName_4() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___inviteToName_4)); }
	inline String_t* get_inviteToName_4() const { return ___inviteToName_4; }
	inline String_t** get_address_of_inviteToName_4() { return &___inviteToName_4; }
	inline void set_inviteToName_4(String_t* value)
	{
		___inviteToName_4 = value;
		Il2CppCodeGenWriteBarrier(&___inviteToName_4, value);
	}

	inline static int32_t get_offset_of_isRead_5() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___isRead_5)); }
	inline bool get_isRead_5() const { return ___isRead_5; }
	inline bool* get_address_of_isRead_5() { return &___isRead_5; }
	inline void set_isRead_5(bool value)
	{
		___isRead_5 = value;
	}

	inline static int32_t get_offset_of_allianceId_6() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___allianceId_6)); }
	inline int64_t get_allianceId_6() const { return ___allianceId_6; }
	inline int64_t* get_address_of_allianceId_6() { return &___allianceId_6; }
	inline void set_allianceId_6(int64_t value)
	{
		___allianceId_6 = value;
	}

	inline static int32_t get_offset_of_allianceName_7() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___allianceName_7)); }
	inline String_t* get_allianceName_7() const { return ___allianceName_7; }
	inline String_t** get_address_of_allianceName_7() { return &___allianceName_7; }
	inline void set_allianceName_7(String_t* value)
	{
		___allianceName_7 = value;
		Il2CppCodeGenWriteBarrier(&___allianceName_7, value);
	}

	inline static int32_t get_offset_of_allianceRank_8() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___allianceRank_8)); }
	inline int64_t get_allianceRank_8() const { return ___allianceRank_8; }
	inline int64_t* get_address_of_allianceRank_8() { return &___allianceRank_8; }
	inline void set_allianceRank_8(int64_t value)
	{
		___allianceRank_8 = value;
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___type_9)); }
	inline String_t* get_type_9() const { return ___type_9; }
	inline String_t** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(String_t* value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier(&___type_9, value);
	}

	inline static int32_t get_offset_of_creationDate_10() { return static_cast<int32_t>(offsetof(InviteReportModel_t3681483684, ___creationDate_10)); }
	inline DateTime_t693205669  get_creationDate_10() const { return ___creationDate_10; }
	inline DateTime_t693205669 * get_address_of_creationDate_10() { return &___creationDate_10; }
	inline void set_creationDate_10(DateTime_t693205669  value)
	{
		___creationDate_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
