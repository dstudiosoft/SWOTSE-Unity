﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Net.OscBundleReceivedEventArgs
struct OscBundleReceivedEventArgs_t3673224441;
// OSCsharp.Data.OscBundle
struct OscBundle_t1126010605;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscBundle1126010605.h"

// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::set_Bundle(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs_set_Bundle_m2468455950 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::.ctor(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs__ctor_m2002404183 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
