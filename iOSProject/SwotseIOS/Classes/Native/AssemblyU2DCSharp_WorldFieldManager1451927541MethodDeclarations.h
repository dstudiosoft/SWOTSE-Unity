﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldFieldManager
struct WorldFieldManager_t1451927541;
// WorldFieldModel
struct WorldFieldModel_t3469935653;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WorldFieldModel3469935653.h"

// System.Void WorldFieldManager::.ctor()
extern "C"  void WorldFieldManager__ctor_m1616194410 (WorldFieldManager_t1451927541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldFieldManager::DisableField()
extern "C"  void WorldFieldManager_DisableField_m1527341372 (WorldFieldManager_t1451927541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldFieldManager::InitFieldState(WorldFieldModel)
extern "C"  void WorldFieldManager_InitFieldState_m3002682576 (WorldFieldManager_t1451927541 * __this, WorldFieldModel_t3469935653 * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldFieldManager::OnMouseUpAsButton()
extern "C"  void WorldFieldManager_OnMouseUpAsButton_m3140507657 (WorldFieldManager_t1451927541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
