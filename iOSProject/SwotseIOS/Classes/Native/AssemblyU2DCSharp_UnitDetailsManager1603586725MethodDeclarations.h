﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitDetailsManager
struct UnitDetailsManager_t1603586725;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnitDetailsManager::.ctor()
extern "C"  void UnitDetailsManager__ctor_m248825222 (UnitDetailsManager_t1603586725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitDetailsManager::OnEnable()
extern "C"  void UnitDetailsManager_OnEnable_m1407109350 (UnitDetailsManager_t1603586725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitDetailsManager::UpdatePrice(System.String)
extern "C"  void UnitDetailsManager_UpdatePrice_m1618272430 (UnitDetailsManager_t1603586725 * __this, String_t* ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitDetailsManager::FixedUpdate()
extern "C"  void UnitDetailsManager_FixedUpdate_m645821275 (UnitDetailsManager_t1603586725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitDetailsManager::HireUnits()
extern "C"  void UnitDetailsManager_HireUnits_m1188639075 (UnitDetailsManager_t1603586725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitDetailsManager::UnitsHired(System.Object,System.String)
extern "C"  void UnitDetailsManager_UnitsHired_m2773499295 (UnitDetailsManager_t1603586725 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnitDetailsManager::Format(System.Int64)
extern "C"  String_t* UnitDetailsManager_Format_m368023122 (UnitDetailsManager_t1603586725 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
