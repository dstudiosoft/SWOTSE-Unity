﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.Sockets.SocketPolicyClient::.cctor()
extern "C"  void SocketPolicyClient__cctor_m466136659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.SocketPolicyClient::Log(System.String)
extern "C"  void SocketPolicyClient_Log_m66248274 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.Sockets.SocketPolicyClient::GetPolicyStreamForIP(System.String,System.Int32,System.Int32)
extern "C"  Stream_t3255436806 * SocketPolicyClient_GetPolicyStreamForIP_m3947091558 (Il2CppObject * __this /* static, unused */, String_t* ___ip0, int32_t ___policyport1, int32_t ___timeout2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
