﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnightInfoContentManager
struct  KnightInfoContentManager_t2780670597  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text KnightInfoContentManager::knightName
	Text_t356221433 * ___knightName_2;
	// UnityEngine.UI.Text KnightInfoContentManager::knightPoint
	Text_t356221433 * ___knightPoint_3;
	// UnityEngine.UI.Text KnightInfoContentManager::knightMarches
	Text_t356221433 * ___knightMarches_4;
	// UnityEngine.UI.Text KnightInfoContentManager::knightPolitics
	Text_t356221433 * ___knightPolitics_5;
	// UnityEngine.UI.Text KnightInfoContentManager::knightSpeed
	Text_t356221433 * ___knightSpeed_6;
	// UnityEngine.UI.Text KnightInfoContentManager::knightLevel
	Text_t356221433 * ___knightLevel_7;
	// UnityEngine.UI.Text KnightInfoContentManager::knightLoyalty
	Text_t356221433 * ___knightLoyalty_8;
	// UnityEngine.UI.Text KnightInfoContentManager::knightSalary
	Text_t356221433 * ___knightSalary_9;
	// ArmyGeneralModel KnightInfoContentManager::general
	ArmyGeneralModel_t609759248 * ___general_10;
	// System.Int64 KnightInfoContentManager::increaseMarches
	int64_t ___increaseMarches_11;
	// System.Int64 KnightInfoContentManager::increasePolitics
	int64_t ___increasePolitics_12;
	// System.Int64 KnightInfoContentManager::increaseSpeed
	int64_t ___increaseSpeed_13;
	// System.Int64 KnightInfoContentManager::increaseSalary
	int64_t ___increaseSalary_14;
	// System.Int64 KnightInfoContentManager::increaseLoyalty
	int64_t ___increaseLoyalty_15;
	// System.Int64 KnightInfoContentManager::increaseLevel
	int64_t ___increaseLevel_16;
	// System.Int64 KnightInfoContentManager::decreaseExperience
	int64_t ___decreaseExperience_17;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightName_2)); }
	inline Text_t356221433 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t356221433 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t356221433 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier(&___knightName_2, value);
	}

	inline static int32_t get_offset_of_knightPoint_3() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightPoint_3)); }
	inline Text_t356221433 * get_knightPoint_3() const { return ___knightPoint_3; }
	inline Text_t356221433 ** get_address_of_knightPoint_3() { return &___knightPoint_3; }
	inline void set_knightPoint_3(Text_t356221433 * value)
	{
		___knightPoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___knightPoint_3, value);
	}

	inline static int32_t get_offset_of_knightMarches_4() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightMarches_4)); }
	inline Text_t356221433 * get_knightMarches_4() const { return ___knightMarches_4; }
	inline Text_t356221433 ** get_address_of_knightMarches_4() { return &___knightMarches_4; }
	inline void set_knightMarches_4(Text_t356221433 * value)
	{
		___knightMarches_4 = value;
		Il2CppCodeGenWriteBarrier(&___knightMarches_4, value);
	}

	inline static int32_t get_offset_of_knightPolitics_5() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightPolitics_5)); }
	inline Text_t356221433 * get_knightPolitics_5() const { return ___knightPolitics_5; }
	inline Text_t356221433 ** get_address_of_knightPolitics_5() { return &___knightPolitics_5; }
	inline void set_knightPolitics_5(Text_t356221433 * value)
	{
		___knightPolitics_5 = value;
		Il2CppCodeGenWriteBarrier(&___knightPolitics_5, value);
	}

	inline static int32_t get_offset_of_knightSpeed_6() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightSpeed_6)); }
	inline Text_t356221433 * get_knightSpeed_6() const { return ___knightSpeed_6; }
	inline Text_t356221433 ** get_address_of_knightSpeed_6() { return &___knightSpeed_6; }
	inline void set_knightSpeed_6(Text_t356221433 * value)
	{
		___knightSpeed_6 = value;
		Il2CppCodeGenWriteBarrier(&___knightSpeed_6, value);
	}

	inline static int32_t get_offset_of_knightLevel_7() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightLevel_7)); }
	inline Text_t356221433 * get_knightLevel_7() const { return ___knightLevel_7; }
	inline Text_t356221433 ** get_address_of_knightLevel_7() { return &___knightLevel_7; }
	inline void set_knightLevel_7(Text_t356221433 * value)
	{
		___knightLevel_7 = value;
		Il2CppCodeGenWriteBarrier(&___knightLevel_7, value);
	}

	inline static int32_t get_offset_of_knightLoyalty_8() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightLoyalty_8)); }
	inline Text_t356221433 * get_knightLoyalty_8() const { return ___knightLoyalty_8; }
	inline Text_t356221433 ** get_address_of_knightLoyalty_8() { return &___knightLoyalty_8; }
	inline void set_knightLoyalty_8(Text_t356221433 * value)
	{
		___knightLoyalty_8 = value;
		Il2CppCodeGenWriteBarrier(&___knightLoyalty_8, value);
	}

	inline static int32_t get_offset_of_knightSalary_9() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___knightSalary_9)); }
	inline Text_t356221433 * get_knightSalary_9() const { return ___knightSalary_9; }
	inline Text_t356221433 ** get_address_of_knightSalary_9() { return &___knightSalary_9; }
	inline void set_knightSalary_9(Text_t356221433 * value)
	{
		___knightSalary_9 = value;
		Il2CppCodeGenWriteBarrier(&___knightSalary_9, value);
	}

	inline static int32_t get_offset_of_general_10() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___general_10)); }
	inline ArmyGeneralModel_t609759248 * get_general_10() const { return ___general_10; }
	inline ArmyGeneralModel_t609759248 ** get_address_of_general_10() { return &___general_10; }
	inline void set_general_10(ArmyGeneralModel_t609759248 * value)
	{
		___general_10 = value;
		Il2CppCodeGenWriteBarrier(&___general_10, value);
	}

	inline static int32_t get_offset_of_increaseMarches_11() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increaseMarches_11)); }
	inline int64_t get_increaseMarches_11() const { return ___increaseMarches_11; }
	inline int64_t* get_address_of_increaseMarches_11() { return &___increaseMarches_11; }
	inline void set_increaseMarches_11(int64_t value)
	{
		___increaseMarches_11 = value;
	}

	inline static int32_t get_offset_of_increasePolitics_12() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increasePolitics_12)); }
	inline int64_t get_increasePolitics_12() const { return ___increasePolitics_12; }
	inline int64_t* get_address_of_increasePolitics_12() { return &___increasePolitics_12; }
	inline void set_increasePolitics_12(int64_t value)
	{
		___increasePolitics_12 = value;
	}

	inline static int32_t get_offset_of_increaseSpeed_13() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increaseSpeed_13)); }
	inline int64_t get_increaseSpeed_13() const { return ___increaseSpeed_13; }
	inline int64_t* get_address_of_increaseSpeed_13() { return &___increaseSpeed_13; }
	inline void set_increaseSpeed_13(int64_t value)
	{
		___increaseSpeed_13 = value;
	}

	inline static int32_t get_offset_of_increaseSalary_14() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increaseSalary_14)); }
	inline int64_t get_increaseSalary_14() const { return ___increaseSalary_14; }
	inline int64_t* get_address_of_increaseSalary_14() { return &___increaseSalary_14; }
	inline void set_increaseSalary_14(int64_t value)
	{
		___increaseSalary_14 = value;
	}

	inline static int32_t get_offset_of_increaseLoyalty_15() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increaseLoyalty_15)); }
	inline int64_t get_increaseLoyalty_15() const { return ___increaseLoyalty_15; }
	inline int64_t* get_address_of_increaseLoyalty_15() { return &___increaseLoyalty_15; }
	inline void set_increaseLoyalty_15(int64_t value)
	{
		___increaseLoyalty_15 = value;
	}

	inline static int32_t get_offset_of_increaseLevel_16() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___increaseLevel_16)); }
	inline int64_t get_increaseLevel_16() const { return ___increaseLevel_16; }
	inline int64_t* get_address_of_increaseLevel_16() { return &___increaseLevel_16; }
	inline void set_increaseLevel_16(int64_t value)
	{
		___increaseLevel_16 = value;
	}

	inline static int32_t get_offset_of_decreaseExperience_17() { return static_cast<int32_t>(offsetof(KnightInfoContentManager_t2780670597, ___decreaseExperience_17)); }
	inline int64_t get_decreaseExperience_17() const { return ___decreaseExperience_17; }
	inline int64_t* get_address_of_decreaseExperience_17() { return &___decreaseExperience_17; }
	inline void set_decreaseExperience_17(int64_t value)
	{
		___decreaseExperience_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
