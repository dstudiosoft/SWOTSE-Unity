﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FutureVoid
struct FutureVoid_t3180437;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// Firebase.FutureVoid/Action
struct Action_t2847228577;
// Firebase.FutureVoid/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t2903358321;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_FutureVoid3180437.h"
#include "Firebase_App_Firebase_FutureVoid_Action2847228577.h"
#include "Firebase_App_Firebase_FutureVoid_SWIG_CompletionDe2903358321.h"

// System.Void Firebase.FutureVoid::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FutureVoid__ctor_m707398650 (FutureVoid_t3180437 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::.ctor()
extern "C"  void FutureVoid__ctor_m3557937851 (FutureVoid_t3180437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.FutureVoid::getCPtr(Firebase.FutureVoid)
extern "C"  HandleRef_t2419939847  FutureVoid_getCPtr_m2775653288 (Il2CppObject * __this /* static, unused */, FutureVoid_t3180437 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::Finalize()
extern "C"  void FutureVoid_Finalize_m2368220389 (FutureVoid_t3180437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::Dispose()
extern "C"  void FutureVoid_Dispose_m4055496018 (FutureVoid_t3180437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task Firebase.FutureVoid::GetTask(Firebase.FutureVoid)
extern "C"  Task_t1843236107 * FutureVoid_GetTask_m2313534407 (Il2CppObject * __this /* static, unused */, FutureVoid_t3180437 * ___fu0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::SetOnCompletionCallback(Firebase.FutureVoid/Action)
extern "C"  void FutureVoid_SetOnCompletionCallback_m704426974 (FutureVoid_t3180437 * __this, Action_t2847228577 * ___userCompletionCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::SetCompletionData(System.IntPtr)
extern "C"  void FutureVoid_SetCompletionData_m1878550807 (FutureVoid_t3180437 * __this, IntPtr_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::SWIG_CompletionDispatcher(System.Int32)
extern "C"  void FutureVoid_SWIG_CompletionDispatcher_m669473866 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FutureVoid_SWIG_CompletionDispatcher_m669473866(int32_t ___index0);
// System.IntPtr Firebase.FutureVoid::SWIG_OnCompletion(Firebase.FutureVoid/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t FutureVoid_SWIG_OnCompletion_m3898710862 (FutureVoid_t3180437 * __this, SWIG_CompletionDelegate_t2903358321 * ___cs_callback0, int32_t ___cs_key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::SWIG_FreeCompletionData(System.IntPtr)
extern "C"  void FutureVoid_SWIG_FreeCompletionData_m2610808278 (FutureVoid_t3180437 * __this, IntPtr_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FutureVoid::.cctor()
extern "C"  void FutureVoid__cctor_m2433656382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
