﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShopTableController
struct ShopTableController_t3730920584;
// SimpleEvent
struct SimpleEvent_t1509017202;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void ShopTableController::.ctor()
extern "C"  void ShopTableController__ctor_m50628673 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::add_onGetShopItems(SimpleEvent)
extern "C"  void ShopTableController_add_onGetShopItems_m1150739598 (ShopTableController_t3730920584 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::remove_onGetShopItems(SimpleEvent)
extern "C"  void ShopTableController_remove_onGetShopItems_m789977491 (ShopTableController_t3730920584 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::set_shop(System.Boolean)
extern "C"  void ShopTableController_set_shop_m3455710983 (ShopTableController_t3730920584 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShopTableController::get_shop()
extern "C"  bool ShopTableController_get_shop_m1130783100 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::Start()
extern "C"  void ShopTableController_Start_m1537203513 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::FixedUpdate()
extern "C"  void ShopTableController_FixedUpdate_m521942160 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::BuyMoreShillings()
extern "C"  void ShopTableController_BuyMoreShillings_m1573305187 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ShopTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t ShopTableController_GetNumberOfRowsForTableView_m891263317 (ShopTableController_t3730920584 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ShopTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float ShopTableController_GetHeightForRowInTableView_m1809429089 (ShopTableController_t3730920584 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell ShopTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * ShopTableController_GetCellForRowInTableView_m2466981854 (ShopTableController_t3730920584 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::categoryButtonClicked(System.String)
extern "C"  void ShopTableController_categoryButtonClicked_m2722893008 (ShopTableController_t3730920584 * __this, String_t* ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShopTableController::GetShopItems()
extern "C"  Il2CppObject * ShopTableController_GetShopItems_m2537561291 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShopTableController::GetPlayerShopItems()
extern "C"  Il2CppObject * ShopTableController_GetPlayerShopItems_m751854128 (ShopTableController_t3730920584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::GotShopItems(System.Object,System.String)
extern "C"  void ShopTableController_GotShopItems_m2003270487 (ShopTableController_t3730920584 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::TeleportUsed(System.Object,System.String)
extern "C"  void ShopTableController_TeleportUsed_m1695231295 (ShopTableController_t3730920584 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::ExpandUsed(System.Object,System.String)
extern "C"  void ShopTableController_ExpandUsed_m90579494 (ShopTableController_t3730920584 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShopTableController::ReloadTable(System.Object,System.String)
extern "C"  void ShopTableController_ReloadTable_m2245364020 (ShopTableController_t3730920584 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
