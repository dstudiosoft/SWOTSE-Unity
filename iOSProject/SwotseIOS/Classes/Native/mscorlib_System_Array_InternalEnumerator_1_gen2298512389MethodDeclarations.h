﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m953335181(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2298512389 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2754631209(__this, method) ((  void (*) (InternalEnumerator_1_t2298512389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1939693121(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2298512389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::Dispose()
#define InternalEnumerator_1_Dispose_m1582085054(__this, method) ((  void (*) (InternalEnumerator_1_t2298512389 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1729756425(__this, method) ((  bool (*) (InternalEnumerator_1_t2298512389 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.X509.X509Extension>::get_Current()
#define InternalEnumerator_1_get_Current_m2929188988(__this, method) ((  X509Extension_t1439760128 * (*) (InternalEnumerator_1_t2298512389 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
