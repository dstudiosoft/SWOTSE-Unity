﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler
struct NetworkAvailabilityChangedEventHandler_t3480120991;
// System.Object
struct Il2CppObject;
// System.Net.NetworkInformation.NetworkAvailabilityEventArgs
struct NetworkAvailabilityEventArgs_t182009988;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_System_Net_NetworkInformation_NetworkAvailab182009988.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void NetworkAvailabilityChangedEventHandler__ctor_m50239237 (NetworkAvailabilityChangedEventHandler_t3480120991 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler::Invoke(System.Object,System.Net.NetworkInformation.NetworkAvailabilityEventArgs)
extern "C"  void NetworkAvailabilityChangedEventHandler_Invoke_m225164653 (NetworkAvailabilityChangedEventHandler_t3480120991 * __this, Il2CppObject * ___sender0, NetworkAvailabilityEventArgs_t182009988 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler::BeginInvoke(System.Object,System.Net.NetworkInformation.NetworkAvailabilityEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NetworkAvailabilityChangedEventHandler_BeginInvoke_m244497076 (NetworkAvailabilityChangedEventHandler_t3480120991 * __this, Il2CppObject * ___sender0, NetworkAvailabilityEventArgs_t182009988 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void NetworkAvailabilityChangedEventHandler_EndInvoke_m258468495 (NetworkAvailabilityChangedEventHandler_t3480120991 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
