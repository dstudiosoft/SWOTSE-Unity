﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ResidenceCitiesTableController
struct ResidenceCitiesTableController_t1287619937;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceCitiesTableLine
struct  ResidenceCitiesTableLine_t1138332587  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text ResidenceCitiesTableLine::cityName
	Text_t356221433 * ___cityName_2;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::region
	Text_t356221433 * ___region_3;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::cityCoords
	Text_t356221433 * ___cityCoords_4;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::mayerLevel
	Text_t356221433 * ___mayerLevel_5;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::fieldsCount
	Text_t356221433 * ___fieldsCount_6;
	// UnityEngine.UI.Text ResidenceCitiesTableLine::valleysCount
	Text_t356221433 * ___valleysCount_7;
	// ResidenceCitiesTableController ResidenceCitiesTableLine::owner
	ResidenceCitiesTableController_t1287619937 * ___owner_8;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___cityName_2)); }
	inline Text_t356221433 * get_cityName_2() const { return ___cityName_2; }
	inline Text_t356221433 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(Text_t356221433 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_2, value);
	}

	inline static int32_t get_offset_of_region_3() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___region_3)); }
	inline Text_t356221433 * get_region_3() const { return ___region_3; }
	inline Text_t356221433 ** get_address_of_region_3() { return &___region_3; }
	inline void set_region_3(Text_t356221433 * value)
	{
		___region_3 = value;
		Il2CppCodeGenWriteBarrier(&___region_3, value);
	}

	inline static int32_t get_offset_of_cityCoords_4() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___cityCoords_4)); }
	inline Text_t356221433 * get_cityCoords_4() const { return ___cityCoords_4; }
	inline Text_t356221433 ** get_address_of_cityCoords_4() { return &___cityCoords_4; }
	inline void set_cityCoords_4(Text_t356221433 * value)
	{
		___cityCoords_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityCoords_4, value);
	}

	inline static int32_t get_offset_of_mayerLevel_5() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___mayerLevel_5)); }
	inline Text_t356221433 * get_mayerLevel_5() const { return ___mayerLevel_5; }
	inline Text_t356221433 ** get_address_of_mayerLevel_5() { return &___mayerLevel_5; }
	inline void set_mayerLevel_5(Text_t356221433 * value)
	{
		___mayerLevel_5 = value;
		Il2CppCodeGenWriteBarrier(&___mayerLevel_5, value);
	}

	inline static int32_t get_offset_of_fieldsCount_6() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___fieldsCount_6)); }
	inline Text_t356221433 * get_fieldsCount_6() const { return ___fieldsCount_6; }
	inline Text_t356221433 ** get_address_of_fieldsCount_6() { return &___fieldsCount_6; }
	inline void set_fieldsCount_6(Text_t356221433 * value)
	{
		___fieldsCount_6 = value;
		Il2CppCodeGenWriteBarrier(&___fieldsCount_6, value);
	}

	inline static int32_t get_offset_of_valleysCount_7() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___valleysCount_7)); }
	inline Text_t356221433 * get_valleysCount_7() const { return ___valleysCount_7; }
	inline Text_t356221433 ** get_address_of_valleysCount_7() { return &___valleysCount_7; }
	inline void set_valleysCount_7(Text_t356221433 * value)
	{
		___valleysCount_7 = value;
		Il2CppCodeGenWriteBarrier(&___valleysCount_7, value);
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(ResidenceCitiesTableLine_t1138332587, ___owner_8)); }
	inline ResidenceCitiesTableController_t1287619937 * get_owner_8() const { return ___owner_8; }
	inline ResidenceCitiesTableController_t1287619937 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(ResidenceCitiesTableController_t1287619937 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier(&___owner_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
