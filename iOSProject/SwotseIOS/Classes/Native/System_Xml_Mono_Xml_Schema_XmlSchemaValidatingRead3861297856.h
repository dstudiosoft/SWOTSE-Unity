﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t3434391819;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.Schema.XmlSchemaValidator
struct XmlSchemaValidator_t1978690704;
// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_t1685472371;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t2864028808;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t135184468;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader
struct  XmlSchemaValidatingReader_t3861297856  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XmlSchemaValidatingReader::reader
	XmlReader_t3675626668 * ___reader_3;
	// System.Xml.Schema.XmlSchemaValidationFlags Mono.Xml.Schema.XmlSchemaValidatingReader::options
	int32_t ___options_4;
	// System.Xml.Schema.XmlSchemaValidator Mono.Xml.Schema.XmlSchemaValidatingReader::v
	XmlSchemaValidator_t1978690704 * ___v_5;
	// System.Xml.Schema.XmlValueGetter Mono.Xml.Schema.XmlSchemaValidatingReader::getter
	XmlValueGetter_t1685472371 * ___getter_6;
	// System.Xml.Schema.XmlSchemaInfo Mono.Xml.Schema.XmlSchemaValidatingReader::xsinfo
	XmlSchemaInfo_t2864028808 * ___xsinfo_7;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XmlSchemaValidatingReader::readerLineInfo
	Il2CppObject * ___readerLineInfo_8;
	// System.Xml.IXmlNamespaceResolver Mono.Xml.Schema.XmlSchemaValidatingReader::nsResolver
	Il2CppObject * ___nsResolver_9;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_t3434391819* ___defaultAttributes_10;
	// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_11;
	// System.Collections.ArrayList Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributesCache
	ArrayList_t4252133567 * ___defaultAttributesCache_12;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_13;
	// System.Xml.Schema.XmlSchemaType Mono.Xml.Schema.XmlSchemaValidatingReader::currentAttrType
	XmlSchemaType_t1795078578 * ___currentAttrType_14;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::validationDone
	bool ___validationDone_15;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XmlSchemaValidatingReader::element
	XmlSchemaElement_t2433337156 * ___element_16;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___reader_3)); }
	inline XmlReader_t3675626668 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_t3675626668 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_t3675626668 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier(&___reader_3, value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___options_4)); }
	inline int32_t get_options_4() const { return ___options_4; }
	inline int32_t* get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(int32_t value)
	{
		___options_4 = value;
	}

	inline static int32_t get_offset_of_v_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___v_5)); }
	inline XmlSchemaValidator_t1978690704 * get_v_5() const { return ___v_5; }
	inline XmlSchemaValidator_t1978690704 ** get_address_of_v_5() { return &___v_5; }
	inline void set_v_5(XmlSchemaValidator_t1978690704 * value)
	{
		___v_5 = value;
		Il2CppCodeGenWriteBarrier(&___v_5, value);
	}

	inline static int32_t get_offset_of_getter_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___getter_6)); }
	inline XmlValueGetter_t1685472371 * get_getter_6() const { return ___getter_6; }
	inline XmlValueGetter_t1685472371 ** get_address_of_getter_6() { return &___getter_6; }
	inline void set_getter_6(XmlValueGetter_t1685472371 * value)
	{
		___getter_6 = value;
		Il2CppCodeGenWriteBarrier(&___getter_6, value);
	}

	inline static int32_t get_offset_of_xsinfo_7() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___xsinfo_7)); }
	inline XmlSchemaInfo_t2864028808 * get_xsinfo_7() const { return ___xsinfo_7; }
	inline XmlSchemaInfo_t2864028808 ** get_address_of_xsinfo_7() { return &___xsinfo_7; }
	inline void set_xsinfo_7(XmlSchemaInfo_t2864028808 * value)
	{
		___xsinfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___xsinfo_7, value);
	}

	inline static int32_t get_offset_of_readerLineInfo_8() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___readerLineInfo_8)); }
	inline Il2CppObject * get_readerLineInfo_8() const { return ___readerLineInfo_8; }
	inline Il2CppObject ** get_address_of_readerLineInfo_8() { return &___readerLineInfo_8; }
	inline void set_readerLineInfo_8(Il2CppObject * value)
	{
		___readerLineInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___readerLineInfo_8, value);
	}

	inline static int32_t get_offset_of_nsResolver_9() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___nsResolver_9)); }
	inline Il2CppObject * get_nsResolver_9() const { return ___nsResolver_9; }
	inline Il2CppObject ** get_address_of_nsResolver_9() { return &___nsResolver_9; }
	inline void set_nsResolver_9(Il2CppObject * value)
	{
		___nsResolver_9 = value;
		Il2CppCodeGenWriteBarrier(&___nsResolver_9, value);
	}

	inline static int32_t get_offset_of_defaultAttributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributes_10)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_defaultAttributes_10() const { return ___defaultAttributes_10; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_defaultAttributes_10() { return &___defaultAttributes_10; }
	inline void set_defaultAttributes_10(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___defaultAttributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultAttributes_10, value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___currentDefaultAttribute_11)); }
	inline int32_t get_currentDefaultAttribute_11() const { return ___currentDefaultAttribute_11; }
	inline int32_t* get_address_of_currentDefaultAttribute_11() { return &___currentDefaultAttribute_11; }
	inline void set_currentDefaultAttribute_11(int32_t value)
	{
		___currentDefaultAttribute_11 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_12() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributesCache_12)); }
	inline ArrayList_t4252133567 * get_defaultAttributesCache_12() const { return ___defaultAttributesCache_12; }
	inline ArrayList_t4252133567 ** get_address_of_defaultAttributesCache_12() { return &___defaultAttributesCache_12; }
	inline void set_defaultAttributesCache_12(ArrayList_t4252133567 * value)
	{
		___defaultAttributesCache_12 = value;
		Il2CppCodeGenWriteBarrier(&___defaultAttributesCache_12, value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_13() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributeConsumed_13)); }
	inline bool get_defaultAttributeConsumed_13() const { return ___defaultAttributeConsumed_13; }
	inline bool* get_address_of_defaultAttributeConsumed_13() { return &___defaultAttributeConsumed_13; }
	inline void set_defaultAttributeConsumed_13(bool value)
	{
		___defaultAttributeConsumed_13 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_14() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___currentAttrType_14)); }
	inline XmlSchemaType_t1795078578 * get_currentAttrType_14() const { return ___currentAttrType_14; }
	inline XmlSchemaType_t1795078578 ** get_address_of_currentAttrType_14() { return &___currentAttrType_14; }
	inline void set_currentAttrType_14(XmlSchemaType_t1795078578 * value)
	{
		___currentAttrType_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentAttrType_14, value);
	}

	inline static int32_t get_offset_of_validationDone_15() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___validationDone_15)); }
	inline bool get_validationDone_15() const { return ___validationDone_15; }
	inline bool* get_address_of_validationDone_15() { return &___validationDone_15; }
	inline void set_validationDone_15(bool value)
	{
		___validationDone_15 = value;
	}

	inline static int32_t get_offset_of_element_16() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___element_16)); }
	inline XmlSchemaElement_t2433337156 * get_element_16() const { return ___element_16; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_element_16() { return &___element_16; }
	inline void set_element_16(XmlSchemaElement_t2433337156 * value)
	{
		___element_16 = value;
		Il2CppCodeGenWriteBarrier(&___element_16, value);
	}
};

struct XmlSchemaValidatingReader_t3861297856_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_t3434391819* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_18;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___emptyAttributeArray_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_17() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___U3CU3Ef__switchU24map0_17)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_17() const { return ___U3CU3Ef__switchU24map0_17; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_17() { return &___U3CU3Ef__switchU24map0_17; }
	inline void set_U3CU3Ef__switchU24map0_17(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_18() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___U3CU3Ef__switchU24map1_18)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_18() const { return ___U3CU3Ef__switchU24map1_18; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_18() { return &___U3CU3Ef__switchU24map1_18; }
	inline void set_U3CU3Ef__switchU24map1_18(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
