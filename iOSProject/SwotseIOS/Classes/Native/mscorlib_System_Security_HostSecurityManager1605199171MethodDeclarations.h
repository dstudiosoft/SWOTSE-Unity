﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.HostSecurityManager
struct HostSecurityManager_t1605199171;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t43919632;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t3968282840;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t3342600394;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Security.PermissionSet
struct PermissionSet_t1941658161;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_HostSecurityManagerOption2988201557.h"
#include "mscorlib_System_Security_Policy_Evidence1407710183.h"
#include "mscorlib_System_Security_Policy_TrustManagerContex3342600394.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"

// System.Void System.Security.HostSecurityManager::.ctor()
extern "C"  void HostSecurityManager__ctor_m3173864036 (HostSecurityManager_t1605199171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.HostSecurityManager::get_DomainPolicy()
extern "C"  PolicyLevel_t43919632 * HostSecurityManager_get_DomainPolicy_m850286799 (HostSecurityManager_t1605199171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.HostSecurityManagerOptions System.Security.HostSecurityManager::get_Flags()
extern "C"  int32_t HostSecurityManager_get_Flags_m245873585 (HostSecurityManager_t1605199171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.HostSecurityManager::DetermineApplicationTrust(System.Security.Policy.Evidence,System.Security.Policy.Evidence,System.Security.Policy.TrustManagerContext)
extern "C"  ApplicationTrust_t3968282840 * HostSecurityManager_DetermineApplicationTrust_m2249965174 (HostSecurityManager_t1605199171 * __this, Evidence_t1407710183 * ___applicationEvidence0, Evidence_t1407710183 * ___activatorEvidence1, TrustManagerContext_t3342600394 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.HostSecurityManager::ProvideAppDomainEvidence(System.Security.Policy.Evidence)
extern "C"  Evidence_t1407710183 * HostSecurityManager_ProvideAppDomainEvidence_m2594345418 (HostSecurityManager_t1605199171 * __this, Evidence_t1407710183 * ___inputEvidence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.HostSecurityManager::ProvideAssemblyEvidence(System.Reflection.Assembly,System.Security.Policy.Evidence)
extern "C"  Evidence_t1407710183 * HostSecurityManager_ProvideAssemblyEvidence_m922894567 (HostSecurityManager_t1605199171 * __this, Assembly_t4268412390 * ___loadedAssembly0, Evidence_t1407710183 * ___inputEvidence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.HostSecurityManager::ResolvePolicy(System.Security.Policy.Evidence)
extern "C"  PermissionSet_t1941658161 * HostSecurityManager_ResolvePolicy_m50763913 (HostSecurityManager_t1605199171 * __this, Evidence_t1407710183 * ___evidence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
