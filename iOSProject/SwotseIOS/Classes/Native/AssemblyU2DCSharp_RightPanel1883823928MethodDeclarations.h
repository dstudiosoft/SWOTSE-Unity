﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RightPanel
struct RightPanel_t1883823928;

#include "codegen/il2cpp-codegen.h"

// System.Void RightPanel::.ctor()
extern "C"  void RightPanel__ctor_m2402836901 (RightPanel_t1883823928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RightPanel::Start()
extern "C"  void RightPanel_Start_m2516075781 (RightPanel_t1883823928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RightPanel::OnEnable()
extern "C"  void RightPanel_OnEnable_m237487141 (RightPanel_t1883823928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RightPanel::openOrClose()
extern "C"  void RightPanel_openOrClose_m3099363152 (RightPanel_t1883823928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
