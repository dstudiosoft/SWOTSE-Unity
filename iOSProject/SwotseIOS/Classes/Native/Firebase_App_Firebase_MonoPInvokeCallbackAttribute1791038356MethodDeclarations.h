﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1791038356;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Firebase.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m809834131 (MonoPInvokeCallbackAttribute_t1791038356 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
