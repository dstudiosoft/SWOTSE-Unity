﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Compat_System_Tuple_2_gen1036200771MethodDeclarations.h"

// System.Void System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::.ctor(T1,T2)
#define Tuple_2__ctor_m4133476842(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t460172552 *, SendOrPostCallback_t296893742 *, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m3248227570_gshared)(__this, ___item10, ___item21, method)
// T1 System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::get_Item1()
#define Tuple_2_get_Item1_m2647242292(__this, method) ((  SendOrPostCallback_t296893742 * (*) (Tuple_2_t460172552 *, const MethodInfo*))Tuple_2_get_Item1_m3565184246_gshared)(__this, method)
// System.Void System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::set_Item1(T1)
#define Tuple_2_set_Item1_m2858728396(__this, ___value0, method) ((  void (*) (Tuple_2_t460172552 *, SendOrPostCallback_t296893742 *, const MethodInfo*))Tuple_2_set_Item1_m135613129_gshared)(__this, ___value0, method)
// T2 System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::get_Item2()
#define Tuple_2_get_Item2_m3295131920(__this, method) ((  Il2CppObject * (*) (Tuple_2_t460172552 *, const MethodInfo*))Tuple_2_get_Item2_m26266116_gshared)(__this, method)
// System.Void System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::set_Item2(T2)
#define Tuple_2_set_Item2_m1292641190(__this, ___value0, method) ((  void (*) (Tuple_2_t460172552 *, Il2CppObject *, const MethodInfo*))Tuple_2_set_Item2_m2864499617_gshared)(__this, ___value0, method)
// System.Boolean System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::Equals(System.Object)
#define Tuple_2_Equals_m1657677093(__this, ___obj0, method) ((  bool (*) (Tuple_2_t460172552 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m1387835382_gshared)(__this, ___obj0, method)
// System.Int32 System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::GetHashCode()
#define Tuple_2_GetHashCode_m3919721823(__this, method) ((  int32_t (*) (Tuple_2_t460172552 *, const MethodInfo*))Tuple_2_GetHashCode_m3929157358_gshared)(__this, method)
