﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3
struct U3CFOVKickDownU3Ec__Iterator3_t1597325332;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::.ctor()
extern "C"  void U3CFOVKickDownU3Ec__Iterator3__ctor_m2217252719 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFOVKickDownU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1721599005 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFOVKickDownU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1936966277 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::MoveNext()
extern "C"  bool U3CFOVKickDownU3Ec__Iterator3_MoveNext_m4258326513 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::Dispose()
extern "C"  void U3CFOVKickDownU3Ec__Iterator3_Dispose_m4279203844 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator3::Reset()
extern "C"  void U3CFOVKickDownU3Ec__Iterator3_Reset_m2880287362 (U3CFOVKickDownU3Ec__Iterator3_t1597325332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
