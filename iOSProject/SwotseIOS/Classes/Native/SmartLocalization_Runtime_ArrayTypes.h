﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;
// SmartLocalization.LocalizedObject
struct LocalizedObject_t1895892772;

#include "mscorlib_System_Array3829468939.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"
#include "SmartLocalization_Runtime_SmartLocalization_Locali1895892772.h"

#pragma once
// SmartLocalization.SmartCultureInfo[]
struct SmartCultureInfoU5BU5D_t1421956628  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SmartCultureInfo_t2361725737 * m_Items[1];

public:
	inline SmartCultureInfo_t2361725737 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SmartCultureInfo_t2361725737 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SmartCultureInfo_t2361725737 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SmartLocalization.LocalizedObject[]
struct LocalizedObjectU5BU5D_t999154637  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalizedObject_t1895892772 * m_Items[1];

public:
	inline LocalizedObject_t1895892772 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalizedObject_t1895892772 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalizedObject_t1895892772 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
