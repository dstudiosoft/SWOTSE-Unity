﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3809826670(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3572586747 *, Dictionary_2_t2252562045 *, const MethodInfo*))Enumerator__ctor_m3045959731_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m645977849(__this, method) ((  Il2CppObject * (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m524777314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3780724069(__this, method) ((  void (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3684036852_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3638109946(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m930063167(__this, method) ((  Il2CppObject * (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1166972359(__this, method) ((  Il2CppObject * (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::MoveNext()
#define Enumerator_MoveNext_m1219684749(__this, method) ((  bool (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_MoveNext_m3139459564_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::get_Current()
#define Enumerator_get_Current_m1995080405(__this, method) ((  KeyValuePair_2_t9907267  (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_get_Current_m3988231868_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1996466278(__this, method) ((  int64_t (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_get_CurrentKey_m4167196999_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3432899846(__this, method) ((  UnitTimer_t1263043643 * (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_get_CurrentValue_m659195263_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::Reset()
#define Enumerator_Reset_m239931672(__this, method) ((  void (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_Reset_m598725905_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::VerifyState()
#define Enumerator_VerifyState_m1630803747(__this, method) ((  void (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_VerifyState_m3803534940_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3799119981(__this, method) ((  void (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_VerifyCurrent_m1011807396_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,UnitTimer>::Dispose()
#define Enumerator_Dispose_m618148054(__this, method) ((  void (*) (Enumerator_t3572586747 *, const MethodInfo*))Enumerator_Dispose_m3446908287_gshared)(__this, method)
