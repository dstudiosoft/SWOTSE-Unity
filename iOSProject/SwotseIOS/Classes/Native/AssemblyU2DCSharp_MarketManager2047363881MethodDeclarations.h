﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager
struct MarketManager_t2047363881;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void MarketManager::.ctor()
extern "C"  void MarketManager__ctor_m292323394 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::add_onBuyLotsUpdated(SimpleEvent)
extern "C"  void MarketManager_add_onBuyLotsUpdated_m3840068030 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::remove_onBuyLotsUpdated(SimpleEvent)
extern "C"  void MarketManager_remove_onBuyLotsUpdated_m3346396945 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::add_onSellLotsUpdated(SimpleEvent)
extern "C"  void MarketManager_add_onSellLotsUpdated_m1237228238 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::remove_onSellLotsUpdated(SimpleEvent)
extern "C"  void MarketManager_remove_onSellLotsUpdated_m2775970093 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::add_onResourceBought(SimpleEvent)
extern "C"  void MarketManager_add_onResourceBought_m4145965310 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::remove_onResourceBought(SimpleEvent)
extern "C"  void MarketManager_remove_onResourceBought_m36523563 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::add_onResourceSold(SimpleEvent)
extern "C"  void MarketManager_add_onResourceSold_m2437069355 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::remove_onResourceSold(SimpleEvent)
extern "C"  void MarketManager_remove_onResourceSold_m2874675884 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::add_onLotRetracted(SimpleEvent)
extern "C"  void MarketManager_add_onLotRetracted_m3501476624 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::remove_onLotRetracted(SimpleEvent)
extern "C"  void MarketManager_remove_onLotRetracted_m3939174223 (MarketManager_t2047363881 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::Start()
extern "C"  void MarketManager_Start_m994005530 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::FixedUpdate()
extern "C"  void MarketManager_FixedUpdate_m1814086191 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::OpenPanel(System.String)
extern "C"  void MarketManager_OpenPanel_m1900317306 (MarketManager_t2047363881 * __this, String_t* ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::UpdatedBuyingValues()
extern "C"  void MarketManager_UpdatedBuyingValues_m3847089807 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::UpdateSellingValues()
extern "C"  void MarketManager_UpdateSellingValues_m35950283 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::RetractLot(System.Int64)
extern "C"  void MarketManager_RetractLot_m1395404924 (MarketManager_t2047363881 * __this, int64_t ___lotId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::LotRetracted(System.Object,System.String)
extern "C"  void MarketManager_LotRetracted_m1057622193 (MarketManager_t2047363881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::BuyLots()
extern "C"  void MarketManager_BuyLots_m1292270446 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::ResourcesBought(System.Object,System.String)
extern "C"  void MarketManager_ResourcesBought_m1850251504 (MarketManager_t2047363881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::LotsToBuyUpdated(System.Object,System.String)
extern "C"  void MarketManager_LotsToBuyUpdated_m2527086450 (MarketManager_t2047363881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::SellLot()
extern "C"  void MarketManager_SellLot_m3451047829 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::ResourcesSold(System.Object,System.String)
extern "C"  void MarketManager_ResourcesSold_m2353563429 (MarketManager_t2047363881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager::LotsToSellUpdated(System.Object,System.String)
extern "C"  void MarketManager_LotsToSellUpdated_m747229658 (MarketManager_t2047363881 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketManager::GetLotsToBuy()
extern "C"  Il2CppObject * MarketManager_GetLotsToBuy_m1239554341 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketManager::GetLotsToSell()
extern "C"  Il2CppObject * MarketManager_GetLotsToSell_m2427921761 (MarketManager_t2047363881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketManager::AddLot(System.Int64,System.Int64)
extern "C"  Il2CppObject * MarketManager_AddLot_m713860436 (MarketManager_t2047363881 * __this, int64_t ___resourceCount0, int64_t ___unitPrice1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketManager::RemoveLots(System.Int64)
extern "C"  Il2CppObject * MarketManager_RemoveLots_m2057038820 (MarketManager_t2047363881 * __this, int64_t ___lotId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketManager::BuyLots(System.String)
extern "C"  Il2CppObject * MarketManager_BuyLots_m2974922616 (MarketManager_t2047363881 * __this, String_t* ___lotsDict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MarketManager::Format(System.Int64)
extern "C"  String_t* MarketManager_Format_m685743912 (MarketManager_t2047363881 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
