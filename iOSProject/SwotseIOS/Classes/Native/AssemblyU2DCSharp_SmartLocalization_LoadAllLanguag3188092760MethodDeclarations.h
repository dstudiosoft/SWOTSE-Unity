﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LoadAllLanguages
struct LoadAllLanguages_t3188092760;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_Languag132697751.h"

// System.Void SmartLocalization.LoadAllLanguages::.ctor()
extern "C"  void LoadAllLanguages__ctor_m342205393 (LoadAllLanguages_t3188092760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LoadAllLanguages::Start()
extern "C"  void LoadAllLanguages_Start_m1862327537 (LoadAllLanguages_t3188092760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LoadAllLanguages::OnDestroy()
extern "C"  void LoadAllLanguages_OnDestroy_m127158706 (LoadAllLanguages_t3188092760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LoadAllLanguages::OnLanguageChanged(SmartLocalization.LanguageManager)
extern "C"  void LoadAllLanguages_OnLanguageChanged_m590825669 (LoadAllLanguages_t3188092760 * __this, LanguageManager_t132697751 * ___languageManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LoadAllLanguages::OnGUI()
extern "C"  void LoadAllLanguages_OnGUI_m2983942503 (LoadAllLanguages_t3188092760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
