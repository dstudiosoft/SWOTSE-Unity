﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// PanelTableController
struct PanelTableController_t1117991214;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTableLine
struct  PanelTableLine_t30214712  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Image PanelTableLine::actionImage
	Image_t2042527209 * ___actionImage_2;
	// UnityEngine.UI.Image PanelTableLine::itemImage
	Image_t2042527209 * ___itemImage_3;
	// UnityEngine.UI.Text PanelTableLine::itemText
	Text_t356221433 * ___itemText_4;
	// UnityEngine.UI.Text PanelTableLine::itemTimer
	Text_t356221433 * ___itemTimer_5;
	// UnityEngine.GameObject PanelTableLine::speedUpBTN
	GameObject_t1756533147 * ___speedUpBTN_6;
	// UnityEngine.GameObject PanelTableLine::cancelBTN
	GameObject_t1756533147 * ___cancelBTN_7;
	// UnityEngine.RectTransform PanelTableLine::progressBar
	RectTransform_t3349966182 * ___progressBar_8;
	// System.Int64 PanelTableLine::eventId
	int64_t ___eventId_9;
	// System.Int64 PanelTableLine::itemId
	int64_t ___itemId_10;
	// PanelTableController PanelTableLine::owner
	PanelTableController_t1117991214 * ___owner_11;
	// System.DateTime PanelTableLine::startTime
	DateTime_t693205669  ___startTime_12;
	// System.DateTime PanelTableLine::finishTime
	DateTime_t693205669  ___finishTime_13;
	// System.Boolean PanelTableLine::cityBuilding
	bool ___cityBuilding_14;
	// System.TimeSpan PanelTableLine::timespan
	TimeSpan_t3430258949  ___timespan_15;
	// System.Single PanelTableLine::valuePerSecond
	float ___valuePerSecond_16;

public:
	inline static int32_t get_offset_of_actionImage_2() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___actionImage_2)); }
	inline Image_t2042527209 * get_actionImage_2() const { return ___actionImage_2; }
	inline Image_t2042527209 ** get_address_of_actionImage_2() { return &___actionImage_2; }
	inline void set_actionImage_2(Image_t2042527209 * value)
	{
		___actionImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___actionImage_2, value);
	}

	inline static int32_t get_offset_of_itemImage_3() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___itemImage_3)); }
	inline Image_t2042527209 * get_itemImage_3() const { return ___itemImage_3; }
	inline Image_t2042527209 ** get_address_of_itemImage_3() { return &___itemImage_3; }
	inline void set_itemImage_3(Image_t2042527209 * value)
	{
		___itemImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_3, value);
	}

	inline static int32_t get_offset_of_itemText_4() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___itemText_4)); }
	inline Text_t356221433 * get_itemText_4() const { return ___itemText_4; }
	inline Text_t356221433 ** get_address_of_itemText_4() { return &___itemText_4; }
	inline void set_itemText_4(Text_t356221433 * value)
	{
		___itemText_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemText_4, value);
	}

	inline static int32_t get_offset_of_itemTimer_5() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___itemTimer_5)); }
	inline Text_t356221433 * get_itemTimer_5() const { return ___itemTimer_5; }
	inline Text_t356221433 ** get_address_of_itemTimer_5() { return &___itemTimer_5; }
	inline void set_itemTimer_5(Text_t356221433 * value)
	{
		___itemTimer_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemTimer_5, value);
	}

	inline static int32_t get_offset_of_speedUpBTN_6() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___speedUpBTN_6)); }
	inline GameObject_t1756533147 * get_speedUpBTN_6() const { return ___speedUpBTN_6; }
	inline GameObject_t1756533147 ** get_address_of_speedUpBTN_6() { return &___speedUpBTN_6; }
	inline void set_speedUpBTN_6(GameObject_t1756533147 * value)
	{
		___speedUpBTN_6 = value;
		Il2CppCodeGenWriteBarrier(&___speedUpBTN_6, value);
	}

	inline static int32_t get_offset_of_cancelBTN_7() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___cancelBTN_7)); }
	inline GameObject_t1756533147 * get_cancelBTN_7() const { return ___cancelBTN_7; }
	inline GameObject_t1756533147 ** get_address_of_cancelBTN_7() { return &___cancelBTN_7; }
	inline void set_cancelBTN_7(GameObject_t1756533147 * value)
	{
		___cancelBTN_7 = value;
		Il2CppCodeGenWriteBarrier(&___cancelBTN_7, value);
	}

	inline static int32_t get_offset_of_progressBar_8() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___progressBar_8)); }
	inline RectTransform_t3349966182 * get_progressBar_8() const { return ___progressBar_8; }
	inline RectTransform_t3349966182 ** get_address_of_progressBar_8() { return &___progressBar_8; }
	inline void set_progressBar_8(RectTransform_t3349966182 * value)
	{
		___progressBar_8 = value;
		Il2CppCodeGenWriteBarrier(&___progressBar_8, value);
	}

	inline static int32_t get_offset_of_eventId_9() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___eventId_9)); }
	inline int64_t get_eventId_9() const { return ___eventId_9; }
	inline int64_t* get_address_of_eventId_9() { return &___eventId_9; }
	inline void set_eventId_9(int64_t value)
	{
		___eventId_9 = value;
	}

	inline static int32_t get_offset_of_itemId_10() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___itemId_10)); }
	inline int64_t get_itemId_10() const { return ___itemId_10; }
	inline int64_t* get_address_of_itemId_10() { return &___itemId_10; }
	inline void set_itemId_10(int64_t value)
	{
		___itemId_10 = value;
	}

	inline static int32_t get_offset_of_owner_11() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___owner_11)); }
	inline PanelTableController_t1117991214 * get_owner_11() const { return ___owner_11; }
	inline PanelTableController_t1117991214 ** get_address_of_owner_11() { return &___owner_11; }
	inline void set_owner_11(PanelTableController_t1117991214 * value)
	{
		___owner_11 = value;
		Il2CppCodeGenWriteBarrier(&___owner_11, value);
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___startTime_12)); }
	inline DateTime_t693205669  get_startTime_12() const { return ___startTime_12; }
	inline DateTime_t693205669 * get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(DateTime_t693205669  value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_finishTime_13() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___finishTime_13)); }
	inline DateTime_t693205669  get_finishTime_13() const { return ___finishTime_13; }
	inline DateTime_t693205669 * get_address_of_finishTime_13() { return &___finishTime_13; }
	inline void set_finishTime_13(DateTime_t693205669  value)
	{
		___finishTime_13 = value;
	}

	inline static int32_t get_offset_of_cityBuilding_14() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___cityBuilding_14)); }
	inline bool get_cityBuilding_14() const { return ___cityBuilding_14; }
	inline bool* get_address_of_cityBuilding_14() { return &___cityBuilding_14; }
	inline void set_cityBuilding_14(bool value)
	{
		___cityBuilding_14 = value;
	}

	inline static int32_t get_offset_of_timespan_15() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___timespan_15)); }
	inline TimeSpan_t3430258949  get_timespan_15() const { return ___timespan_15; }
	inline TimeSpan_t3430258949 * get_address_of_timespan_15() { return &___timespan_15; }
	inline void set_timespan_15(TimeSpan_t3430258949  value)
	{
		___timespan_15 = value;
	}

	inline static int32_t get_offset_of_valuePerSecond_16() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712, ___valuePerSecond_16)); }
	inline float get_valuePerSecond_16() const { return ___valuePerSecond_16; }
	inline float* get_address_of_valuePerSecond_16() { return &___valuePerSecond_16; }
	inline void set_valuePerSecond_16(float value)
	{
		___valuePerSecond_16 = value;
	}
};

struct PanelTableLine_t30214712_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableLine::<>f__switch$map2A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2A_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PanelTableLine::<>f__switch$map2B
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2B_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_17() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712_StaticFields, ___U3CU3Ef__switchU24map2A_17)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2A_17() const { return ___U3CU3Ef__switchU24map2A_17; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2A_17() { return &___U3CU3Ef__switchU24map2A_17; }
	inline void set_U3CU3Ef__switchU24map2A_17(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2A_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2A_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_18() { return static_cast<int32_t>(offsetof(PanelTableLine_t30214712_StaticFields, ___U3CU3Ef__switchU24map2B_18)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2B_18() const { return ___U3CU3Ef__switchU24map2B_18; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2B_18() { return &___U3CU3Ef__switchU24map2B_18; }
	inline void set_U3CU3Ef__switchU24map2B_18(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2B_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2B_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
