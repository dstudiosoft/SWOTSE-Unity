﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FixGUITextCS
struct FixGUITextCS_t473243723;

#include "codegen/il2cpp-codegen.h"

// System.Void FixGUITextCS::.ctor()
extern "C"  void FixGUITextCS__ctor_m473027926 (FixGUITextCS_t473243723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixGUITextCS::Start()
extern "C"  void FixGUITextCS_Start_m1304999306 (FixGUITextCS_t473243723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FixGUITextCS::Update()
extern "C"  void FixGUITextCS_Update_m2677957431 (FixGUITextCS_t473243723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
