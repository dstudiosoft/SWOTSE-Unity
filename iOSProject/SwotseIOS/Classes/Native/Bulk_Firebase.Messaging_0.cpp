﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t2372265022;
// Firebase.Messaging.FirebaseNotification
struct FirebaseNotification_t1925808173;
// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_t3000907612;
// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct EventHandler_1_t3466559488;
// System.Delegate
struct Delegate_t1188392813;
// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct EventHandler_1_t2074869383;
// System.Type
struct Type_t;
// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t2474724020;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t1016457320;
// System.Exception
struct Exception_t;
// System.String
struct String_t;
// Firebase.FirebaseApp
struct FirebaseApp_t2526288410;
// Firebase.Messaging.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t1247432759;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1004265597;
// Firebase.Messaging.TokenReceivedEventArgs
struct TokenReceivedEventArgs_t4150709950;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Firebase.Messaging.FirebaseMessaging/Listener/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1582016909;
// System.Attribute
struct Attribute_t861562559;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t2033454307;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1399197998;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t1699519678;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t1079801895;
// System.ApplicationException
struct ApplicationException_t2339761290;
// System.ArithmeticException
struct ArithmeticException_t4283546778;
// System.DivideByZeroException
struct DivideByZeroException_t1856388118;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t1578797820;
// System.InvalidCastException
struct InvalidCastException_t3927145244;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.IO.IOException
struct IOException_t4088381929;
// System.NullReferenceException
struct NullReferenceException_t1023182353;
// System.OutOfMemoryException
struct OutOfMemoryException_t2437671686;
// System.OverflowException
struct OverflowException_t2020128637;
// System.SystemException
struct SystemException_t176217640;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t3107596264;
// System.EventArgs
struct EventArgs_t3591816995;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>
struct Dictionary_2_t2311544709;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>
struct Dictionary_2_t748154129;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t1172811472;
// Firebase.FirebaseApp/DestroyDelegate
struct DestroyDelegate_t2525580959;
// Firebase.FirebaseApp/CreateDelegate
struct CreateDelegate_t3131870060;
// System.Func`1<Firebase.DependencyStatus>
struct Func_1_t2062041240;
// System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Threading.Tasks.Task`1<Firebase.DependencyStatus>>
struct Func_2_t680774166;
// System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus>
struct Func_2_t724047911;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;

extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessage_Dispose_m2412216490_MetadataUsageId;
extern RuntimeClass* FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var;
extern RuntimeClass* FirebaseNotification_t1925808173_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessage_get_Notification_m1175428059_MetadataUsageId;
extern const uint32_t FirebaseMessaging__cctor_m4186368094_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t3466559488_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessaging_add_MessageReceivedInternal_m2579176863_MetadataUsageId;
extern const uint32_t FirebaseMessaging_remove_MessageReceivedInternal_m4200848127_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t2074869383_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessaging_add_TokenReceivedInternal_m4122425653_MetadataUsageId;
extern const uint32_t FirebaseMessaging_remove_TokenReceivedInternal_m310964643_MetadataUsageId;
extern const RuntimeType* Listener_t3000907612_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessaging_CreateOrDestroyListener_m3443812056_MetadataUsageId;
extern const uint32_t FirebaseMessaging_add_MessageReceived_m930790674_MetadataUsageId;
extern const uint32_t FirebaseMessaging_remove_MessageReceived_m1476839649_MetadataUsageId;
extern const uint32_t FirebaseMessaging_add_TokenReceived_m1941433509_MetadataUsageId;
extern const uint32_t FirebaseMessaging_remove_TokenReceived_m1479240622_MetadataUsageId;
extern RuntimeClass* SWIGPendingException_t3190247900_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessaging_SetListenerCallbacks_m3664453505_MetadataUsageId;
extern const uint32_t FirebaseMessaging_SetListenerCallbacksEnabled_m3772050241_MetadataUsageId;
extern const uint32_t FirebaseMessaging_SendPendingEvents_m758011710_MetadataUsageId;
extern const uint32_t FirebaseMessaging_MessageCopyNotification_m1047014189_MetadataUsageId;
extern RuntimeClass* MessageReceivedDelegate_t2474724020_il2cpp_TypeInfo_var;
extern RuntimeClass* TokenReceivedDelegate_t1016457320_il2cpp_TypeInfo_var;
extern RuntimeClass* FirebaseApp_t2526288410_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Listener_MessageReceivedDelegateMethod_m689406986_RuntimeMethod_var;
extern const RuntimeMethod* Listener_TokenReceivedDelegateMethod_m1616435326_RuntimeMethod_var;
extern const uint32_t Listener__ctor_m650935776_MetadataUsageId;
extern RuntimeClass* Listener_t3000907612_il2cpp_TypeInfo_var;
extern const uint32_t Listener_Create_m3695792870_MetadataUsageId;
extern const uint32_t Listener_Destroy_m3668999734_MetadataUsageId;
extern const uint32_t Listener_Dispose_m3683418081_MetadataUsageId;
extern RuntimeClass* FirebaseMessage_t2372265022_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageReceivedEventArgs_t1247432759_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m3274546564_RuntimeMethod_var;
extern const uint32_t Listener_MessageReceivedDelegateMethod_m689406986_MetadataUsageId;
extern RuntimeClass* TokenReceivedEventArgs_t4150709950_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m546052383_RuntimeMethod_var;
extern const uint32_t Listener_TokenReceivedDelegateMethod_m1616435326_MetadataUsageId;
extern const uint32_t MessageReceivedDelegate_BeginInvoke_m1700133236_MetadataUsageId;
extern RuntimeClass* SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var;
extern RuntimeClass* SWIGStringHelper_t1399197998_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseMessagingPINVOKE__cctor_m4180122606_MetadataUsageId;
extern RuntimeClass* ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var;
extern RuntimeClass* ExceptionArgumentDelegate_t1079801895_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m1295556158_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m3149638596_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m992672499_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m886111931_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_m483363777_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m863674504_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m273094494_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642_RuntimeMethod_var;
extern const uint32_t SWIGExceptionHelper__cctor_m2293912218_MetadataUsageId;
extern RuntimeClass* SWIGPendingException_t717714779_il2cpp_TypeInfo_var;
extern RuntimeClass* ApplicationException_t2339761290_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingApplicationException_m1295556158_MetadataUsageId;
extern RuntimeClass* ArithmeticException_t4283546778_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArithmeticException_m3149638596_MetadataUsageId;
extern RuntimeClass* DivideByZeroException_t1856388118_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101_MetadataUsageId;
extern RuntimeClass* InvalidCastException_t3927145244_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508_MetadataUsageId;
extern RuntimeClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161_MetadataUsageId;
extern RuntimeClass* IOException_t4088381929_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIOException_m992672499_MetadataUsageId;
extern RuntimeClass* NullReferenceException_t1023182353_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059_MetadataUsageId;
extern RuntimeClass* OutOfMemoryException_t2437671686_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152_MetadataUsageId;
extern RuntimeClass* OverflowException_t2020128637_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOverflowException_m886111931_MetadataUsageId;
extern RuntimeClass* SystemException_t176217640_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingSystemException_m483363777_MetadataUsageId;
extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentException_m863674504_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral88632345;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentNullException_m273094494_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642_MetadataUsageId;
extern const RuntimeType* FirebaseMessagingPINVOKE_t151993683_0_0_0_var;
extern String_t* _stringLiteral1985503341;
extern String_t* _stringLiteral3452614535;
extern const uint32_t SWIGPendingException_Set_m2508649586_MetadataUsageId;
extern const uint32_t SWIGPendingException_Retrieve_m1387100625_MetadataUsageId;
extern RuntimeClass* SWIGStringDelegate_t3107596264_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m2294391909_RuntimeMethod_var;
extern const uint32_t SWIGStringHelper__cctor_m2475557427_MetadataUsageId;
extern const uint32_t FirebaseNotification_Dispose_m2323517056_MetadataUsageId;
extern const uint32_t FirebaseNotification_get_Body_m748811829_MetadataUsageId;
extern RuntimeClass* EventArgs_t3591816995_il2cpp_TypeInfo_var;
extern const uint32_t MessageReceivedEventArgs__ctor_m1445459718_MetadataUsageId;
extern const uint32_t TokenReceivedEventArgs__ctor_m4233221851_MetadataUsageId;



#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FIREBASEMESSAGINGPINVOKE_T151993683_H
#define FIREBASEMESSAGINGPINVOKE_T151993683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE
struct  FirebaseMessagingPINVOKE_t151993683  : public RuntimeObject
{
public:

public:
};

struct FirebaseMessagingPINVOKE_t151993683_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t2033454307 * ___swigExceptionHelper_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigStringHelper
	SWIGStringHelper_t1399197998 * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t151993683_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t2033454307 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t2033454307 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t2033454307 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier((&___swigExceptionHelper_0), value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t151993683_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t1399197998 * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t1399197998 ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t1399197998 * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier((&___swigStringHelper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGINGPINVOKE_T151993683_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef SWIGEXCEPTIONHELPER_T2033454307_H
#define SWIGEXCEPTIONHELPER_T2033454307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct  SWIGExceptionHelper_t2033454307  : public RuntimeObject
{
public:

public:
};

struct SWIGExceptionHelper_t2033454307_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t1699519678 * ___applicationDelegate_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t1699519678 * ___arithmeticDelegate_1;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t1699519678 * ___divideByZeroDelegate_2;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t1699519678 * ___indexOutOfRangeDelegate_3;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t1699519678 * ___invalidCastDelegate_4;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t1699519678 * ___invalidOperationDelegate_5;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t1699519678 * ___ioDelegate_6;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t1699519678 * ___nullReferenceDelegate_7;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t1699519678 * ___outOfMemoryDelegate_8;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t1699519678 * ___overflowDelegate_9;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t1699519678 * ___systemDelegate_10;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentDelegate_11;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentNullDelegate_12;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentOutOfRangeDelegate_13;

public:
	inline static int32_t get_offset_of_applicationDelegate_0() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___applicationDelegate_0)); }
	inline ExceptionDelegate_t1699519678 * get_applicationDelegate_0() const { return ___applicationDelegate_0; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_applicationDelegate_0() { return &___applicationDelegate_0; }
	inline void set_applicationDelegate_0(ExceptionDelegate_t1699519678 * value)
	{
		___applicationDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___applicationDelegate_0), value);
	}

	inline static int32_t get_offset_of_arithmeticDelegate_1() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___arithmeticDelegate_1)); }
	inline ExceptionDelegate_t1699519678 * get_arithmeticDelegate_1() const { return ___arithmeticDelegate_1; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_arithmeticDelegate_1() { return &___arithmeticDelegate_1; }
	inline void set_arithmeticDelegate_1(ExceptionDelegate_t1699519678 * value)
	{
		___arithmeticDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___arithmeticDelegate_1), value);
	}

	inline static int32_t get_offset_of_divideByZeroDelegate_2() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___divideByZeroDelegate_2)); }
	inline ExceptionDelegate_t1699519678 * get_divideByZeroDelegate_2() const { return ___divideByZeroDelegate_2; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_divideByZeroDelegate_2() { return &___divideByZeroDelegate_2; }
	inline void set_divideByZeroDelegate_2(ExceptionDelegate_t1699519678 * value)
	{
		___divideByZeroDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___divideByZeroDelegate_2), value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeDelegate_3() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___indexOutOfRangeDelegate_3)); }
	inline ExceptionDelegate_t1699519678 * get_indexOutOfRangeDelegate_3() const { return ___indexOutOfRangeDelegate_3; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_indexOutOfRangeDelegate_3() { return &___indexOutOfRangeDelegate_3; }
	inline void set_indexOutOfRangeDelegate_3(ExceptionDelegate_t1699519678 * value)
	{
		___indexOutOfRangeDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRangeDelegate_3), value);
	}

	inline static int32_t get_offset_of_invalidCastDelegate_4() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___invalidCastDelegate_4)); }
	inline ExceptionDelegate_t1699519678 * get_invalidCastDelegate_4() const { return ___invalidCastDelegate_4; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_invalidCastDelegate_4() { return &___invalidCastDelegate_4; }
	inline void set_invalidCastDelegate_4(ExceptionDelegate_t1699519678 * value)
	{
		___invalidCastDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidCastDelegate_4), value);
	}

	inline static int32_t get_offset_of_invalidOperationDelegate_5() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___invalidOperationDelegate_5)); }
	inline ExceptionDelegate_t1699519678 * get_invalidOperationDelegate_5() const { return ___invalidOperationDelegate_5; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_invalidOperationDelegate_5() { return &___invalidOperationDelegate_5; }
	inline void set_invalidOperationDelegate_5(ExceptionDelegate_t1699519678 * value)
	{
		___invalidOperationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___invalidOperationDelegate_5), value);
	}

	inline static int32_t get_offset_of_ioDelegate_6() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___ioDelegate_6)); }
	inline ExceptionDelegate_t1699519678 * get_ioDelegate_6() const { return ___ioDelegate_6; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_ioDelegate_6() { return &___ioDelegate_6; }
	inline void set_ioDelegate_6(ExceptionDelegate_t1699519678 * value)
	{
		___ioDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___ioDelegate_6), value);
	}

	inline static int32_t get_offset_of_nullReferenceDelegate_7() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___nullReferenceDelegate_7)); }
	inline ExceptionDelegate_t1699519678 * get_nullReferenceDelegate_7() const { return ___nullReferenceDelegate_7; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_nullReferenceDelegate_7() { return &___nullReferenceDelegate_7; }
	inline void set_nullReferenceDelegate_7(ExceptionDelegate_t1699519678 * value)
	{
		___nullReferenceDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___nullReferenceDelegate_7), value);
	}

	inline static int32_t get_offset_of_outOfMemoryDelegate_8() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___outOfMemoryDelegate_8)); }
	inline ExceptionDelegate_t1699519678 * get_outOfMemoryDelegate_8() const { return ___outOfMemoryDelegate_8; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_outOfMemoryDelegate_8() { return &___outOfMemoryDelegate_8; }
	inline void set_outOfMemoryDelegate_8(ExceptionDelegate_t1699519678 * value)
	{
		___outOfMemoryDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___outOfMemoryDelegate_8), value);
	}

	inline static int32_t get_offset_of_overflowDelegate_9() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___overflowDelegate_9)); }
	inline ExceptionDelegate_t1699519678 * get_overflowDelegate_9() const { return ___overflowDelegate_9; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_overflowDelegate_9() { return &___overflowDelegate_9; }
	inline void set_overflowDelegate_9(ExceptionDelegate_t1699519678 * value)
	{
		___overflowDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___overflowDelegate_9), value);
	}

	inline static int32_t get_offset_of_systemDelegate_10() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___systemDelegate_10)); }
	inline ExceptionDelegate_t1699519678 * get_systemDelegate_10() const { return ___systemDelegate_10; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_systemDelegate_10() { return &___systemDelegate_10; }
	inline void set_systemDelegate_10(ExceptionDelegate_t1699519678 * value)
	{
		___systemDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemDelegate_10), value);
	}

	inline static int32_t get_offset_of_argumentDelegate_11() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentDelegate_11)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentDelegate_11() const { return ___argumentDelegate_11; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentDelegate_11() { return &___argumentDelegate_11; }
	inline void set_argumentDelegate_11(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___argumentDelegate_11), value);
	}

	inline static int32_t get_offset_of_argumentNullDelegate_12() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentNullDelegate_12)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentNullDelegate_12() const { return ___argumentNullDelegate_12; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentNullDelegate_12() { return &___argumentNullDelegate_12; }
	inline void set_argumentNullDelegate_12(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentNullDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___argumentNullDelegate_12), value);
	}

	inline static int32_t get_offset_of_argumentOutOfRangeDelegate_13() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentOutOfRangeDelegate_13)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentOutOfRangeDelegate_13() const { return ___argumentOutOfRangeDelegate_13; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentOutOfRangeDelegate_13() { return &___argumentOutOfRangeDelegate_13; }
	inline void set_argumentOutOfRangeDelegate_13(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentOutOfRangeDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___argumentOutOfRangeDelegate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGEXCEPTIONHELPER_T2033454307_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef SWIGSTRINGHELPER_T1399197998_H
#define SWIGSTRINGHELPER_T1399197998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct  SWIGStringHelper_t1399197998  : public RuntimeObject
{
public:

public:
};

struct SWIGStringHelper_t1399197998_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t3107596264 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t1399197998_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_t3107596264 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_t3107596264 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_t3107596264 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGHELPER_T1399197998_H
#ifndef SWIGPENDINGEXCEPTION_T717714779_H
#define SWIGPENDINGEXCEPTION_T717714779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException
struct  SWIGPendingException_t717714779  : public RuntimeObject
{
public:

public:
};

struct SWIGPendingException_t717714779_StaticFields
{
public:
	// System.Int32 Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;

public:
	inline static int32_t get_offset_of_numExceptionsPending_1() { return static_cast<int32_t>(offsetof(SWIGPendingException_t717714779_StaticFields, ___numExceptionsPending_1)); }
	inline int32_t get_numExceptionsPending_1() const { return ___numExceptionsPending_1; }
	inline int32_t* get_address_of_numExceptionsPending_1() { return &___numExceptionsPending_1; }
	inline void set_numExceptionsPending_1(int32_t value)
	{
		___numExceptionsPending_1 = value;
	}
};

struct SWIGPendingException_t717714779_ThreadStaticFields
{
public:
	// System.Exception Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::pendingException
	Exception_t * ___pendingException_0;

public:
	inline static int32_t get_offset_of_pendingException_0() { return static_cast<int32_t>(offsetof(SWIGPendingException_t717714779_ThreadStaticFields, ___pendingException_0)); }
	inline Exception_t * get_pendingException_0() const { return ___pendingException_0; }
	inline Exception_t ** get_address_of_pendingException_0() { return &___pendingException_0; }
	inline void set_pendingException_0(Exception_t * value)
	{
		___pendingException_0 = value;
		Il2CppCodeGenWriteBarrier((&___pendingException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGPENDINGEXCEPTION_T717714779_H
#ifndef FIREBASEMESSAGING_T2538764775_H
#define FIREBASEMESSAGING_T2538764775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging
struct  FirebaseMessaging_t2538764775  : public RuntimeObject
{
public:

public:
};

struct FirebaseMessaging_t2538764775_StaticFields
{
public:
	// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::MessageReceivedInternal
	EventHandler_1_t3466559488 * ___MessageReceivedInternal_0;
	// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::TokenReceivedInternal
	EventHandler_1_t2074869383 * ___TokenReceivedInternal_1;
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging::listener
	Listener_t3000907612 * ___listener_2;

public:
	inline static int32_t get_offset_of_MessageReceivedInternal_0() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___MessageReceivedInternal_0)); }
	inline EventHandler_1_t3466559488 * get_MessageReceivedInternal_0() const { return ___MessageReceivedInternal_0; }
	inline EventHandler_1_t3466559488 ** get_address_of_MessageReceivedInternal_0() { return &___MessageReceivedInternal_0; }
	inline void set_MessageReceivedInternal_0(EventHandler_1_t3466559488 * value)
	{
		___MessageReceivedInternal_0 = value;
		Il2CppCodeGenWriteBarrier((&___MessageReceivedInternal_0), value);
	}

	inline static int32_t get_offset_of_TokenReceivedInternal_1() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___TokenReceivedInternal_1)); }
	inline EventHandler_1_t2074869383 * get_TokenReceivedInternal_1() const { return ___TokenReceivedInternal_1; }
	inline EventHandler_1_t2074869383 ** get_address_of_TokenReceivedInternal_1() { return &___TokenReceivedInternal_1; }
	inline void set_TokenReceivedInternal_1(EventHandler_1_t2074869383 * value)
	{
		___TokenReceivedInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&___TokenReceivedInternal_1), value);
	}

	inline static int32_t get_offset_of_listener_2() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___listener_2)); }
	inline Listener_t3000907612 * get_listener_2() const { return ___listener_2; }
	inline Listener_t3000907612 ** get_address_of_listener_2() { return &___listener_2; }
	inline void set_listener_2(Listener_t3000907612 * value)
	{
		___listener_2 = value;
		Il2CppCodeGenWriteBarrier((&___listener_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGING_T2538764775_H
#ifndef LISTENER_T3000907612_H
#define LISTENER_T3000907612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener
struct  Listener_t3000907612  : public RuntimeObject
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::messageReceivedDelegate
	MessageReceivedDelegate_t2474724020 * ___messageReceivedDelegate_0;
	// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::tokenReceivedDelegate
	TokenReceivedDelegate_t1016457320 * ___tokenReceivedDelegate_1;
	// Firebase.FirebaseApp Firebase.Messaging.FirebaseMessaging/Listener::app
	FirebaseApp_t2526288410 * ___app_2;

public:
	inline static int32_t get_offset_of_messageReceivedDelegate_0() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___messageReceivedDelegate_0)); }
	inline MessageReceivedDelegate_t2474724020 * get_messageReceivedDelegate_0() const { return ___messageReceivedDelegate_0; }
	inline MessageReceivedDelegate_t2474724020 ** get_address_of_messageReceivedDelegate_0() { return &___messageReceivedDelegate_0; }
	inline void set_messageReceivedDelegate_0(MessageReceivedDelegate_t2474724020 * value)
	{
		___messageReceivedDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedDelegate_0), value);
	}

	inline static int32_t get_offset_of_tokenReceivedDelegate_1() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___tokenReceivedDelegate_1)); }
	inline TokenReceivedDelegate_t1016457320 * get_tokenReceivedDelegate_1() const { return ___tokenReceivedDelegate_1; }
	inline TokenReceivedDelegate_t1016457320 ** get_address_of_tokenReceivedDelegate_1() { return &___tokenReceivedDelegate_1; }
	inline void set_tokenReceivedDelegate_1(TokenReceivedDelegate_t1016457320 * value)
	{
		___tokenReceivedDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokenReceivedDelegate_1), value);
	}

	inline static int32_t get_offset_of_app_2() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___app_2)); }
	inline FirebaseApp_t2526288410 * get_app_2() const { return ___app_2; }
	inline FirebaseApp_t2526288410 ** get_address_of_app_2() { return &___app_2; }
	inline void set_app_2(FirebaseApp_t2526288410 * value)
	{
		___app_2 = value;
		Il2CppCodeGenWriteBarrier((&___app_2), value);
	}
};

struct Listener_t3000907612_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::listener
	Listener_t3000907612 * ___listener_3;

public:
	inline static int32_t get_offset_of_listener_3() { return static_cast<int32_t>(offsetof(Listener_t3000907612_StaticFields, ___listener_3)); }
	inline Listener_t3000907612 * get_listener_3() const { return ___listener_3; }
	inline Listener_t3000907612 ** get_address_of_listener_3() { return &___listener_3; }
	inline void set_listener_3(Listener_t3000907612 * value)
	{
		___listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENER_T3000907612_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#define MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t1582016909  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T1582016909_H
#ifndef TOKENRECEIVEDEVENTARGS_T4150709950_H
#define TOKENRECEIVEDEVENTARGS_T4150709950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.TokenReceivedEventArgs
struct  TokenReceivedEventArgs_t4150709950  : public EventArgs_t3591816995
{
public:
	// System.String Firebase.Messaging.TokenReceivedEventArgs::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TokenReceivedEventArgs_t4150709950, ___U3CTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_1() const { return ___U3CTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_1() { return &___U3CTokenU3Ek__BackingField_1; }
	inline void set_U3CTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENRECEIVEDEVENTARGS_T4150709950_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef MESSAGERECEIVEDEVENTARGS_T1247432759_H
#define MESSAGERECEIVEDEVENTARGS_T1247432759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.MessageReceivedEventArgs
struct  MessageReceivedEventArgs_t1247432759  : public EventArgs_t3591816995
{
public:
	// Firebase.Messaging.FirebaseMessage Firebase.Messaging.MessageReceivedEventArgs::<Message>k__BackingField
	FirebaseMessage_t2372265022 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageReceivedEventArgs_t1247432759, ___U3CMessageU3Ek__BackingField_1)); }
	inline FirebaseMessage_t2372265022 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline FirebaseMessage_t2372265022 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(FirebaseMessage_t2372265022 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDEVENTARGS_T1247432759_H
#ifndef APPLICATIONEXCEPTION_T2339761290_H
#define APPLICATIONEXCEPTION_T2339761290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t2339761290  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T2339761290_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#define INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t1578797820  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifndef INVALIDCASTEXCEPTION_T3927145244_H
#define INVALIDCASTEXCEPTION_T3927145244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t3927145244  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T3927145244_H
#ifndef NULLREFERENCEEXCEPTION_T1023182353_H
#define NULLREFERENCEEXCEPTION_T1023182353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NullReferenceException
struct  NullReferenceException_t1023182353  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLREFERENCEEXCEPTION_T1023182353_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef OUTOFMEMORYEXCEPTION_T2437671686_H
#define OUTOFMEMORYEXCEPTION_T2437671686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.OutOfMemoryException
struct  OutOfMemoryException_t2437671686  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTOFMEMORYEXCEPTION_T2437671686_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef HANDLEREF_T3745784362_H
#define HANDLEREF_T3745784362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784362 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	RuntimeObject * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	intptr_t ___handle_1;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___wrapper_0)); }
	inline RuntimeObject * get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject ** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject * value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T3745784362_H
#ifndef ARITHMETICEXCEPTION_T4283546778_H
#define ARITHMETICEXCEPTION_T4283546778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArithmeticException
struct  ArithmeticException_t4283546778  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARITHMETICEXCEPTION_T4283546778_H
#ifndef IOEXCEPTION_T4088381929_H
#define IOEXCEPTION_T4088381929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t4088381929  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T4088381929_H
#ifndef FIREBASENOTIFICATION_T1925808173_H
#define FIREBASENOTIFICATION_T1925808173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseNotification
struct  FirebaseNotification_t1925808173  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseNotification::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseNotification::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseNotification_t1925808173, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseNotification_t1925808173, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASENOTIFICATION_T1925808173_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t777629997  : public ArgumentException_t132251570
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t777629997, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifndef FIREBASEAPP_T2526288410_H
#define FIREBASEAPP_T2526288410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.FirebaseApp
struct  FirebaseApp_t2526288410  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.FirebaseApp::swigCMemOwn
	bool ___swigCMemOwn_1;
	// System.EventHandler`1<System.EventArgs> Firebase.FirebaseApp::Disposed
	EventHandler_1_t1515976428 * ___Disposed_8;
	// Firebase.FirebaseApp/DestroyDelegate Firebase.FirebaseApp::destroy
	DestroyDelegate_t2525580959 * ___destroy_12;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_Disposed_8() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___Disposed_8)); }
	inline EventHandler_1_t1515976428 * get_Disposed_8() const { return ___Disposed_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_Disposed_8() { return &___Disposed_8; }
	inline void set_Disposed_8(EventHandler_1_t1515976428 * value)
	{
		___Disposed_8 = value;
		Il2CppCodeGenWriteBarrier((&___Disposed_8), value);
	}

	inline static int32_t get_offset_of_destroy_12() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410, ___destroy_12)); }
	inline DestroyDelegate_t2525580959 * get_destroy_12() const { return ___destroy_12; }
	inline DestroyDelegate_t2525580959 ** get_address_of_destroy_12() { return &___destroy_12; }
	inline void set_destroy_12(DestroyDelegate_t2525580959 * value)
	{
		___destroy_12 = value;
		Il2CppCodeGenWriteBarrier((&___destroy_12), value);
	}
};

struct FirebaseApp_t2526288410_StaticFields
{
public:
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_ANDROID
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2;
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_IOS
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3;
	// System.String Firebase.FirebaseApp::DEPENDENCY_NOT_FOUND_ERROR_GENERIC
	String_t* ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_ANDROID
	String_t* ___DLL_NOT_FOUND_ERROR_ANDROID_5;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_IOS
	String_t* ___DLL_NOT_FOUND_ERROR_IOS_6;
	// System.String Firebase.FirebaseApp::DLL_NOT_FOUND_ERROR_GENERIC
	String_t* ___DLL_NOT_FOUND_ERROR_GENERIC_7;
	// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp> Firebase.FirebaseApp::nameToProxy
	Dictionary_2_t2311544709 * ___nameToProxy_9;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp> Firebase.FirebaseApp::cPtrToProxy
	Dictionary_2_t748154129 * ___cPtrToProxy_10;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32> Firebase.FirebaseApp::cPtrRefCount
	Dictionary_2_t1172811472 * ___cPtrRefCount_11;
	// Firebase.FirebaseApp/CreateDelegate Firebase.FirebaseApp::<>f__am$cache0
	CreateDelegate_t3131870060 * ___U3CU3Ef__amU24cache0_13;
	// System.Func`1<Firebase.DependencyStatus> Firebase.FirebaseApp::<>f__am$cache1
	Func_1_t2062041240 * ___U3CU3Ef__amU24cache1_14;
	// System.Func`2<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>,System.Threading.Tasks.Task`1<Firebase.DependencyStatus>> Firebase.FirebaseApp::<>f__am$cache2
	Func_2_t680774166 * ___U3CU3Ef__amU24cache2_15;
	// System.Func`2<System.Threading.Tasks.Task,Firebase.DependencyStatus> Firebase.FirebaseApp::<>f__am$cache3
	Func_2_t724047911 * ___U3CU3Ef__amU24cache3_16;

public:
	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() const { return ___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2() { return &___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2), value);
	}

	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() const { return ___DEPENDENCY_NOT_FOUND_ERROR_IOS_3; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3() { return &___DEPENDENCY_NOT_FOUND_ERROR_IOS_3; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_IOS_3(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_IOS_3 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_IOS_3), value);
	}

	inline static int32_t get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4)); }
	inline String_t* get_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() const { return ___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4; }
	inline String_t** get_address_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4() { return &___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4; }
	inline void set_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4(String_t* value)
	{
		___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4 = value;
		Il2CppCodeGenWriteBarrier((&___DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_ANDROID_5() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_ANDROID_5)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_ANDROID_5() const { return ___DLL_NOT_FOUND_ERROR_ANDROID_5; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_ANDROID_5() { return &___DLL_NOT_FOUND_ERROR_ANDROID_5; }
	inline void set_DLL_NOT_FOUND_ERROR_ANDROID_5(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_ANDROID_5 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_ANDROID_5), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_IOS_6() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_IOS_6)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_IOS_6() const { return ___DLL_NOT_FOUND_ERROR_IOS_6; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_IOS_6() { return &___DLL_NOT_FOUND_ERROR_IOS_6; }
	inline void set_DLL_NOT_FOUND_ERROR_IOS_6(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_IOS_6 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_IOS_6), value);
	}

	inline static int32_t get_offset_of_DLL_NOT_FOUND_ERROR_GENERIC_7() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___DLL_NOT_FOUND_ERROR_GENERIC_7)); }
	inline String_t* get_DLL_NOT_FOUND_ERROR_GENERIC_7() const { return ___DLL_NOT_FOUND_ERROR_GENERIC_7; }
	inline String_t** get_address_of_DLL_NOT_FOUND_ERROR_GENERIC_7() { return &___DLL_NOT_FOUND_ERROR_GENERIC_7; }
	inline void set_DLL_NOT_FOUND_ERROR_GENERIC_7(String_t* value)
	{
		___DLL_NOT_FOUND_ERROR_GENERIC_7 = value;
		Il2CppCodeGenWriteBarrier((&___DLL_NOT_FOUND_ERROR_GENERIC_7), value);
	}

	inline static int32_t get_offset_of_nameToProxy_9() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___nameToProxy_9)); }
	inline Dictionary_2_t2311544709 * get_nameToProxy_9() const { return ___nameToProxy_9; }
	inline Dictionary_2_t2311544709 ** get_address_of_nameToProxy_9() { return &___nameToProxy_9; }
	inline void set_nameToProxy_9(Dictionary_2_t2311544709 * value)
	{
		___nameToProxy_9 = value;
		Il2CppCodeGenWriteBarrier((&___nameToProxy_9), value);
	}

	inline static int32_t get_offset_of_cPtrToProxy_10() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___cPtrToProxy_10)); }
	inline Dictionary_2_t748154129 * get_cPtrToProxy_10() const { return ___cPtrToProxy_10; }
	inline Dictionary_2_t748154129 ** get_address_of_cPtrToProxy_10() { return &___cPtrToProxy_10; }
	inline void set_cPtrToProxy_10(Dictionary_2_t748154129 * value)
	{
		___cPtrToProxy_10 = value;
		Il2CppCodeGenWriteBarrier((&___cPtrToProxy_10), value);
	}

	inline static int32_t get_offset_of_cPtrRefCount_11() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___cPtrRefCount_11)); }
	inline Dictionary_2_t1172811472 * get_cPtrRefCount_11() const { return ___cPtrRefCount_11; }
	inline Dictionary_2_t1172811472 ** get_address_of_cPtrRefCount_11() { return &___cPtrRefCount_11; }
	inline void set_cPtrRefCount_11(Dictionary_2_t1172811472 * value)
	{
		___cPtrRefCount_11 = value;
		Il2CppCodeGenWriteBarrier((&___cPtrRefCount_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline CreateDelegate_t3131870060 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline CreateDelegate_t3131870060 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(CreateDelegate_t3131870060 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_14() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache1_14)); }
	inline Func_1_t2062041240 * get_U3CU3Ef__amU24cache1_14() const { return ___U3CU3Ef__amU24cache1_14; }
	inline Func_1_t2062041240 ** get_address_of_U3CU3Ef__amU24cache1_14() { return &___U3CU3Ef__amU24cache1_14; }
	inline void set_U3CU3Ef__amU24cache1_14(Func_1_t2062041240 * value)
	{
		___U3CU3Ef__amU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_15() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache2_15)); }
	inline Func_2_t680774166 * get_U3CU3Ef__amU24cache2_15() const { return ___U3CU3Ef__amU24cache2_15; }
	inline Func_2_t680774166 ** get_address_of_U3CU3Ef__amU24cache2_15() { return &___U3CU3Ef__amU24cache2_15; }
	inline void set_U3CU3Ef__amU24cache2_15(Func_2_t680774166 * value)
	{
		___U3CU3Ef__amU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_16() { return static_cast<int32_t>(offsetof(FirebaseApp_t2526288410_StaticFields, ___U3CU3Ef__amU24cache3_16)); }
	inline Func_2_t724047911 * get_U3CU3Ef__amU24cache3_16() const { return ___U3CU3Ef__amU24cache3_16; }
	inline Func_2_t724047911 ** get_address_of_U3CU3Ef__amU24cache3_16() { return &___U3CU3Ef__amU24cache3_16; }
	inline void set_U3CU3Ef__amU24cache3_16(Func_2_t724047911 * value)
	{
		___U3CU3Ef__amU24cache3_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEAPP_T2526288410_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef FIREBASEMESSAGE_T2372265022_H
#define FIREBASEMESSAGE_T2372265022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessage
struct  FirebaseMessage_t2372265022  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseMessage::swigCPtr
	HandleRef_t3745784362  ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseMessage::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseMessage_t2372265022, ___swigCPtr_0)); }
	inline HandleRef_t3745784362  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t3745784362 * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t3745784362  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseMessage_t2372265022, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGE_T2372265022_H
#ifndef DIVIDEBYZEROEXCEPTION_T1856388118_H
#define DIVIDEBYZEROEXCEPTION_T1856388118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DivideByZeroException
struct  DivideByZeroException_t1856388118  : public ArithmeticException_t4283546778
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIVIDEBYZEROEXCEPTION_T1856388118_H
#ifndef OVERFLOWEXCEPTION_T2020128637_H
#define OVERFLOWEXCEPTION_T2020128637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.OverflowException
struct  OverflowException_t2020128637  : public ArithmeticException_t4283546778
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOWEXCEPTION_T2020128637_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#define EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct  ExceptionArgumentDelegate_t1079801895  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef EVENTHANDLER_1_T2074869383_H
#define EVENTHANDLER_1_T2074869383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct  EventHandler_1_t2074869383  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2074869383_H
#ifndef MESSAGERECEIVEDDELEGATE_T2474724020_H
#define MESSAGERECEIVEDDELEGATE_T2474724020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct  MessageReceivedDelegate_t2474724020  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDDELEGATE_T2474724020_H
#ifndef EVENTHANDLER_1_T3466559488_H
#define EVENTHANDLER_1_T3466559488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct  EventHandler_1_t3466559488  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T3466559488_H
#ifndef TOKENRECEIVEDDELEGATE_T1016457320_H
#define TOKENRECEIVEDDELEGATE_T1016457320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct  TokenReceivedDelegate_t1016457320  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENRECEIVEDDELEGATE_T1016457320_H
#ifndef EXCEPTIONDELEGATE_T1699519678_H
#define EXCEPTIONDELEGATE_T1699519678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct  ExceptionDelegate_t1699519678  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONDELEGATE_T1699519678_H
#ifndef SWIGSTRINGDELEGATE_T3107596264_H
#define SWIGSTRINGDELEGATE_T3107596264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct  SWIGStringDelegate_t3107596264  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGDELEGATE_T3107596264_H


// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m4124830036_gshared (EventHandler_1_t1004265597 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C"  void HandleRef__ctor_m868850155 (HandleRef_t3745784362 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessage::Dispose()
extern "C"  void FirebaseMessage_Dispose_m2412216490 (FirebaseMessage_t2372265022 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m3076187857 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C"  intptr_t HandleRef_get_Handle_m4239591804 (HandleRef_t3745784362 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3063970704 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseMessage(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseMessage_m1056514804 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1177400158 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.Messaging.FirebaseMessaging::MessageCopyNotification(System.IntPtr)
extern "C"  intptr_t FirebaseMessaging_MessageCopyNotification_m1047014189 (RuntimeObject * __this /* static, unused */, intptr_t ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseNotification::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseNotification__ctor_m927513772 (FirebaseNotification_t1925808173 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::Create()
extern "C"  Listener_t3000907612 * Listener_Create_m3695792870 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Destroy()
extern "C"  void Listener_Destroy_m3668999734 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessaging_SetListenerCallbacksEnabled_m3772050241 (RuntimeObject * __this /* static, unused */, bool ___message_callback_enabled0, bool ___token_callback_enabled1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SendPendingEvents()
extern "C"  void FirebaseMessaging_SendPendingEvents_m758011710 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_MessageReceivedInternal_m2579176863 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::CreateOrDestroyListener()
extern "C"  void FirebaseMessaging_CreateOrDestroyListener_m3443812056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_MessageReceivedInternal_m4200848127 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::add_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_TokenReceivedInternal_m4122425653 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::remove_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_TokenReceivedInternal_m310964643 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacks_m85020952 (RuntimeObject * __this /* static, unused */, MessageReceivedDelegate_t2474724020 * ___jarg10, TokenReceivedDelegate_t1016457320 * ___jarg21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern "C"  bool SWIGPendingException_get_Pending_m582413763 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t * SWIGPendingException_Retrieve_m2171998990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacksEnabled_m2241337789 (RuntimeObject * __this /* static, unused */, bool ___jarg10, bool ___jarg21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SendPendingEvents()
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SendPendingEvents_m2573344962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_MessageCopyNotification(System.IntPtr)
extern "C"  intptr_t FirebaseMessagingPINVOKE_Firebase_Messaging_MessageCopyNotification_m848985824 (RuntimeObject * __this /* static, unused */, intptr_t ___jarg10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener::MessageReceivedDelegateMethod(System.IntPtr)
extern "C"  int32_t Listener_MessageReceivedDelegateMethod_m689406986 (RuntimeObject * __this /* static, unused */, intptr_t ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::TokenReceivedDelegateMethod(System.String)
extern "C"  void Listener_TokenReceivedDelegateMethod_m1616435326 (RuntimeObject * __this /* static, unused */, String_t* ___token0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MessageReceivedDelegate__ctor_m3696635674 (MessageReceivedDelegate_t2474724020 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void TokenReceivedDelegate__ctor_m3826971582 (TokenReceivedDelegate_t1016457320 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern "C"  FirebaseApp_t2526288410 * FirebaseApp_get_DefaultInstance_m3436350982 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessaging_SetListenerCallbacks_m3664453505 (RuntimeObject * __this /* static, unused */, MessageReceivedDelegate_t2474724020 * ___messageCallback0, TokenReceivedDelegate_t1016457320 * ___tokenCallback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::.ctor()
extern "C"  void Listener__ctor_m650935776 (Listener_t3000907612 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Dispose()
extern "C"  void Listener_Dispose_m3683418081 (Listener_t3000907612 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessage::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseMessage__ctor_m4090485566 (FirebaseMessage_t2372265022 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.MessageReceivedEventArgs::.ctor(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs__ctor_m1445459718 (MessageReceivedEventArgs_t1247432759 * __this, FirebaseMessage_t2372265022 * ___msg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3274546564(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3466559488 *, RuntimeObject *, MessageReceivedEventArgs_t1247432759 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void Firebase.Messaging.TokenReceivedEventArgs::.ctor(System.String)
extern "C"  void TokenReceivedEventArgs__ctor_m4233221851 (TokenReceivedEventArgs_t4150709950 * __this, String_t* ___token0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m546052383(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2074869383 *, RuntimeObject *, TokenReceivedEventArgs_t4150709950 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::Invoke(System.IntPtr)
extern "C"  int32_t MessageReceivedDelegate_Invoke_m1271845415 (MessageReceivedDelegate_t2474724020 * __this, intptr_t ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::Invoke(System.String)
extern "C"  void TokenReceivedDelegate_Invoke_m758289265 (TokenReceivedDelegate_t1016457320 * __this, String_t* ___token0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m3940038267 (SWIGExceptionHelper_t2033454307 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1398490094 (SWIGStringHelper_t1399197998 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m1295556158 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m3149638596 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m992672499 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m886111931 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m483363777 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m863674504 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m273094494 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m3847540867 (ExceptionDelegate_t1699519678 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m692720776 (ExceptionArgumentDelegate_t1079801895 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirebaseMessaging_m2584444840 (RuntimeObject * __this /* static, unused */, ExceptionDelegate_t1699519678 * ___applicationDelegate0, ExceptionDelegate_t1699519678 * ___arithmeticDelegate1, ExceptionDelegate_t1699519678 * ___divideByZeroDelegate2, ExceptionDelegate_t1699519678 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t1699519678 * ___invalidCastDelegate4, ExceptionDelegate_t1699519678 * ___invalidOperationDelegate5, ExceptionDelegate_t1699519678 * ___ioDelegate6, ExceptionDelegate_t1699519678 * ___nullReferenceDelegate7, ExceptionDelegate_t1699519678 * ___outOfMemoryDelegate8, ExceptionDelegate_t1699519678 * ___overflowDelegate9, ExceptionDelegate_t1699519678 * ___systemExceptionDelegate10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging_m2780727381 (RuntimeObject * __this /* static, unused */, ExceptionArgumentDelegate_t1079801895 * ___argumentDelegate0, ExceptionArgumentDelegate_t1079801895 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t1079801895 * ___argumentOutOfRangeDelegate2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t * SWIGPendingException_Retrieve_m1387100625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
extern "C"  void ApplicationException__ctor_m692455299 (ApplicationException_t2339761290 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::Set(System.Exception)
extern "C"  void SWIGPendingException_Set_m2508649586 (RuntimeObject * __this /* static, unused */, Exception_t * ___e0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArithmeticException::.ctor(System.String,System.Exception)
extern "C"  void ArithmeticException__ctor_m3829702723 (ArithmeticException_t4283546778 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DivideByZeroException::.ctor(System.String,System.Exception)
extern "C"  void DivideByZeroException__ctor_m3132723944 (DivideByZeroException_t1856388118 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String,System.Exception)
extern "C"  void IndexOutOfRangeException__ctor_m3342077230 (IndexOutOfRangeException_t1578797820 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidCastException::.ctor(System.String,System.Exception)
extern "C"  void InvalidCastException__ctor_m2604994586 (InvalidCastException_t3927145244 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
extern "C"  void InvalidOperationException__ctor_m1685032583 (InvalidOperationException_t56020091 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IOException::.ctor(System.String,System.Exception)
extern "C"  void IOException__ctor_m3246761956 (IOException_t4088381929 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NullReferenceException::.ctor(System.String,System.Exception)
extern "C"  void NullReferenceException__ctor_m3182281066 (NullReferenceException_t1023182353 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.OutOfMemoryException::.ctor(System.String,System.Exception)
extern "C"  void OutOfMemoryException__ctor_m4180974057 (OutOfMemoryException_t2437671686 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.OverflowException::.ctor(System.String,System.Exception)
extern "C"  void OverflowException__ctor_m2524654454 (OverflowException_t2020128637 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern "C"  void SystemException__ctor_m4132668650 (SystemException_t176217640 * __this, String_t* p0, Exception_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String,System.Exception)
extern "C"  void ArgumentException__ctor_m3761792013 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, Exception_t * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m2009621981 (ArgumentNullException_t1615371798 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m282481429 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m2458815942 (ExceptionArgumentDelegate_t1079801895 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m255100690 (ExceptionDelegate_t1699519678 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m2294391909 (RuntimeObject * __this /* static, unused */, String_t* ___cString0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m965772466 (SWIGStringDelegate_t3107596264 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_FirebaseMessaging_m2853007358 (RuntimeObject * __this /* static, unused */, SWIGStringDelegate_t3107596264 * ___stringDelegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m3788666468 (SWIGStringDelegate_t3107596264 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseNotification::Dispose()
extern "C"  void FirebaseNotification_Dispose_m2323517056 (FirebaseNotification_t1925808173 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseNotification(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseNotification_m2260423228 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_FirebaseNotification_Body_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* FirebaseMessagingPINVOKE_Firebase_Messaging_FirebaseNotification_Body_get_m2595163183 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m32674013 (EventArgs_t3591816995 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.MessageReceivedEventArgs::set_Message(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs_set_Message_m4177955988 (MessageReceivedEventArgs_t1247432759 * __this, FirebaseMessage_t2372265022 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.TokenReceivedEventArgs::set_Token(System.String)
extern "C"  void TokenReceivedEventArgs_set_Token_m4270615688 (TokenReceivedEventArgs_t4150709950 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseMessage::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseMessage__ctor_m4090485566 (FirebaseMessage_t2372265022 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t3745784362  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m868850155((&L_2), __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessage::Finalize()
extern "C"  void FirebaseMessage_Finalize_m1499093322 (FirebaseMessage_t2372265022 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseMessage_Dispose_m2412216490(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessage::Dispose()
extern "C"  void FirebaseMessage_Dispose_m2412216490 (FirebaseMessage_t2372265022 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessage_Dispose_m2412216490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		RuntimeObject * L_0 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t3745784362 * L_1 = __this->get_address_of_swigCPtr_0();
			intptr_t L_2 = HandleRef_get_Handle_m4239591804(L_1, /*hidden argument*/NULL);
			bool L_3 = IntPtr_op_Inequality_m3063970704(NULL /*static, unused*/, L_2, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_4 = __this->get_swigCMemOwn_1();
			if (!L_4)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t3745784362  L_5 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
			FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseMessage_m1056514804(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		}

IL_003f:
		{
			HandleRef_t3745784362  L_6;
			memset(&L_6, 0, sizeof(L_6));
			HandleRef__ctor_m868850155((&L_6), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_6);
		}

IL_0050:
		{
			GC_SuppressFinalize_m1177400158(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0062:
	{
		return;
	}
}
// Firebase.Messaging.FirebaseNotification Firebase.Messaging.FirebaseMessage::get_Notification()
extern "C"  FirebaseNotification_t1925808173 * FirebaseMessage_get_Notification_m1175428059 (FirebaseMessage_t2372265022 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessage_get_Notification_m1175428059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t3745784362 * L_0 = __this->get_address_of_swigCPtr_0();
		intptr_t L_1 = HandleRef_get_Handle_m4239591804(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		intptr_t L_2 = FirebaseMessaging_MessageCopyNotification_m1047014189(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		intptr_t L_3 = V_0;
		bool L_4 = IntPtr_op_Inequality_m3063970704(NULL /*static, unused*/, L_3, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		intptr_t L_5 = V_0;
		FirebaseNotification_t1925808173 * L_6 = (FirebaseNotification_t1925808173 *)il2cpp_codegen_object_new(FirebaseNotification_t1925808173_il2cpp_TypeInfo_var);
		FirebaseNotification__ctor_m927513772(L_6, L_5, (bool)1, /*hidden argument*/NULL);
		return L_6;
	}

IL_0029:
	{
		return (FirebaseNotification_t1925808173 *)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseMessaging::.cctor()
extern "C"  void FirebaseMessaging__cctor_m4186368094 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging__cctor_m4186368094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Listener_t3000907612 * L_0 = Listener_Create_m3695792870(NULL /*static, unused*/, /*hidden argument*/NULL);
		((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->set_listener_2(L_0);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::add_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_MessageReceivedInternal_m2579176863 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_add_MessageReceivedInternal_m2579176863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3466559488 * V_0 = NULL;
	EventHandler_1_t3466559488 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t3466559488 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_MessageReceivedInternal_0();
		V_0 = L_0;
	}

IL_0006:
	{
		EventHandler_1_t3466559488 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t3466559488 * L_2 = V_1;
		EventHandler_1_t3466559488 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		EventHandler_1_t3466559488 * L_5 = V_0;
		EventHandler_1_t3466559488 * L_6 = InterlockedCompareExchangeImpl<EventHandler_1_t3466559488 *>((((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_address_of_MessageReceivedInternal_0()), ((EventHandler_1_t3466559488 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3466559488_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		EventHandler_1_t3466559488 * L_7 = V_0;
		EventHandler_1_t3466559488 * L_8 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3466559488 *)L_7) == ((RuntimeObject*)(EventHandler_1_t3466559488 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::remove_MessageReceivedInternal(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_MessageReceivedInternal_m4200848127 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_remove_MessageReceivedInternal_m4200848127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3466559488 * V_0 = NULL;
	EventHandler_1_t3466559488 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t3466559488 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_MessageReceivedInternal_0();
		V_0 = L_0;
	}

IL_0006:
	{
		EventHandler_1_t3466559488 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t3466559488 * L_2 = V_1;
		EventHandler_1_t3466559488 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		EventHandler_1_t3466559488 * L_5 = V_0;
		EventHandler_1_t3466559488 * L_6 = InterlockedCompareExchangeImpl<EventHandler_1_t3466559488 *>((((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_address_of_MessageReceivedInternal_0()), ((EventHandler_1_t3466559488 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3466559488_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		EventHandler_1_t3466559488 * L_7 = V_0;
		EventHandler_1_t3466559488 * L_8 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3466559488 *)L_7) == ((RuntimeObject*)(EventHandler_1_t3466559488 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::add_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_TokenReceivedInternal_m4122425653 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_add_TokenReceivedInternal_m4122425653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2074869383 * V_0 = NULL;
	EventHandler_1_t2074869383 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t2074869383 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_TokenReceivedInternal_1();
		V_0 = L_0;
	}

IL_0006:
	{
		EventHandler_1_t2074869383 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t2074869383 * L_2 = V_1;
		EventHandler_1_t2074869383 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		EventHandler_1_t2074869383 * L_5 = V_0;
		EventHandler_1_t2074869383 * L_6 = InterlockedCompareExchangeImpl<EventHandler_1_t2074869383 *>((((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_address_of_TokenReceivedInternal_1()), ((EventHandler_1_t2074869383 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2074869383_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		EventHandler_1_t2074869383 * L_7 = V_0;
		EventHandler_1_t2074869383 * L_8 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2074869383 *)L_7) == ((RuntimeObject*)(EventHandler_1_t2074869383 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::remove_TokenReceivedInternal(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_TokenReceivedInternal_m310964643 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_remove_TokenReceivedInternal_m310964643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2074869383 * V_0 = NULL;
	EventHandler_1_t2074869383 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t2074869383 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_TokenReceivedInternal_1();
		V_0 = L_0;
	}

IL_0006:
	{
		EventHandler_1_t2074869383 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t2074869383 * L_2 = V_1;
		EventHandler_1_t2074869383 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		EventHandler_1_t2074869383 * L_5 = V_0;
		EventHandler_1_t2074869383 * L_6 = InterlockedCompareExchangeImpl<EventHandler_1_t2074869383 *>((((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_address_of_TokenReceivedInternal_1()), ((EventHandler_1_t2074869383 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2074869383_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		EventHandler_1_t2074869383 * L_7 = V_0;
		EventHandler_1_t2074869383 * L_8 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2074869383 *)L_7) == ((RuntimeObject*)(EventHandler_1_t2074869383 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::CreateOrDestroyListener()
extern "C"  void FirebaseMessaging_CreateOrDestroyListener_m3443812056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_CreateOrDestroyListener_m3443812056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
			EventHandler_1_t3466559488 * L_3 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_MessageReceivedInternal_0();
			V_1 = (bool)((((int32_t)((((RuntimeObject*)(EventHandler_1_t3466559488 *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			EventHandler_1_t2074869383 * L_4 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_TokenReceivedInternal_1();
			V_2 = (bool)((((int32_t)((((RuntimeObject*)(EventHandler_1_t2074869383 *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_5 = V_1;
			if (L_5)
			{
				goto IL_0035;
			}
		}

IL_002f:
		{
			bool L_6 = V_2;
			if (!L_6)
			{
				goto IL_0040;
			}
		}

IL_0035:
		{
			Listener_Create_m3695792870(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0045;
		}

IL_0040:
		{
			Listener_Destroy_m3668999734(NULL /*static, unused*/, /*hidden argument*/NULL);
		}

IL_0045:
		{
			bool L_7 = V_1;
			bool L_8 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
			FirebaseMessaging_SetListenerCallbacksEnabled_m3772050241(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
			bool L_9 = V_1;
			if (L_9)
			{
				goto IL_0058;
			}
		}

IL_0052:
		{
			bool L_10 = V_2;
			if (!L_10)
			{
				goto IL_005d;
			}
		}

IL_0058:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
			FirebaseMessaging_SendPendingEvents_m758011710(NULL /*static, unused*/, /*hidden argument*/NULL);
		}

IL_005d:
		{
			IL2CPP_LEAVE(0x69, FINALLY_0062);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0069:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::add_MessageReceived(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_MessageReceived_m930790674 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_add_MessageReceived_m930790674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t3466559488 * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		FirebaseMessaging_add_MessageReceivedInternal_m2579176863(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FirebaseMessaging_CreateOrDestroyListener_m3443812056(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::remove_MessageReceived(System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_MessageReceived_m1476839649 (RuntimeObject * __this /* static, unused */, EventHandler_1_t3466559488 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_remove_MessageReceived_m1476839649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t3466559488 * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		FirebaseMessaging_remove_MessageReceivedInternal_m4200848127(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FirebaseMessaging_CreateOrDestroyListener_m3443812056(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::add_TokenReceived(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_add_TokenReceived_m1941433509 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_add_TokenReceived_m1941433509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t2074869383 * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		FirebaseMessaging_add_TokenReceivedInternal_m4122425653(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FirebaseMessaging_CreateOrDestroyListener_m3443812056(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::remove_TokenReceived(System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>)
extern "C"  void FirebaseMessaging_remove_TokenReceived_m1479240622 (RuntimeObject * __this /* static, unused */, EventHandler_1_t2074869383 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_remove_TokenReceived_m1479240622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t2074869383 * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		FirebaseMessaging_remove_TokenReceivedInternal_m310964643(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FirebaseMessaging_CreateOrDestroyListener_m3443812056(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessaging_SetListenerCallbacks_m3664453505 (RuntimeObject * __this /* static, unused */, MessageReceivedDelegate_t2474724020 * ___messageCallback0, TokenReceivedDelegate_t1016457320 * ___tokenCallback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_SetListenerCallbacks_m3664453505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MessageReceivedDelegate_t2474724020 * L_0 = ___messageCallback0;
		TokenReceivedDelegate_t1016457320 * L_1 = ___tokenCallback1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
		FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacks_m85020952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m582413763(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m2171998990(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessaging_SetListenerCallbacksEnabled_m3772050241 (RuntimeObject * __this /* static, unused */, bool ___message_callback_enabled0, bool ___token_callback_enabled1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_SetListenerCallbacksEnabled_m3772050241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___message_callback_enabled0;
		bool L_1 = ___token_callback_enabled1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
		FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacksEnabled_m2241337789(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m582413763(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m2171998990(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging::SendPendingEvents()
extern "C"  void FirebaseMessaging_SendPendingEvents_m758011710 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_SendPendingEvents_m758011710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
		FirebaseMessagingPINVOKE_Firebase_Messaging_SendPendingEvents_m2573344962(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		bool L_0 = SWIGPendingException_get_Pending_m582413763(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m2171998990(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		return;
	}
}
// System.IntPtr Firebase.Messaging.FirebaseMessaging::MessageCopyNotification(System.IntPtr)
extern "C"  intptr_t FirebaseMessaging_MessageCopyNotification_m1047014189 (RuntimeObject * __this /* static, unused */, intptr_t ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessaging_MessageCopyNotification_m1047014189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		intptr_t L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
		intptr_t L_1 = FirebaseMessagingPINVOKE_Firebase_Messaging_MessageCopyNotification_m848985824(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m582413763(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m2171998990(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		intptr_t L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_Listener_MessageReceivedDelegateMethod_m689406986(intptr_t ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	int32_t returnValue = Listener_MessageReceivedDelegateMethod_m689406986(NULL, ___message0, NULL);

	return returnValue;
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Listener_TokenReceivedDelegateMethod_m1616435326(char* ___token0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___token0' to managed representation
	String_t* ____token0_unmarshaled = NULL;
	____token0_unmarshaled = il2cpp_codegen_marshal_string_result(___token0);

	// Managed method invocation
	Listener_TokenReceivedDelegateMethod_m1616435326(NULL, ____token0_unmarshaled, NULL);

}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::.ctor()
extern "C"  void Listener__ctor_m650935776 (Listener_t3000907612 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener__ctor_m650935776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)Listener_MessageReceivedDelegateMethod_m689406986_RuntimeMethod_var;
		MessageReceivedDelegate_t2474724020 * L_1 = (MessageReceivedDelegate_t2474724020 *)il2cpp_codegen_object_new(MessageReceivedDelegate_t2474724020_il2cpp_TypeInfo_var);
		MessageReceivedDelegate__ctor_m3696635674(L_1, NULL, L_0, /*hidden argument*/NULL);
		__this->set_messageReceivedDelegate_0(L_1);
		intptr_t L_2 = (intptr_t)Listener_TokenReceivedDelegateMethod_m1616435326_RuntimeMethod_var;
		TokenReceivedDelegate_t1016457320 * L_3 = (TokenReceivedDelegate_t1016457320 *)il2cpp_codegen_object_new(TokenReceivedDelegate_t1016457320_il2cpp_TypeInfo_var);
		TokenReceivedDelegate__ctor_m3826971582(L_3, NULL, L_2, /*hidden argument*/NULL);
		__this->set_tokenReceivedDelegate_1(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t2526288410_il2cpp_TypeInfo_var);
		FirebaseApp_t2526288410 * L_4 = FirebaseApp_get_DefaultInstance_m3436350982(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_app_2(L_4);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		MessageReceivedDelegate_t2474724020 * L_5 = __this->get_messageReceivedDelegate_0();
		TokenReceivedDelegate_t1016457320 * L_6 = __this->get_tokenReceivedDelegate_1();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		FirebaseMessaging_SetListenerCallbacks_m3664453505(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::Create()
extern "C"  Listener_t3000907612 * Listener_Create_m3695792870 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener_Create_m3695792870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Listener_t3000907612 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			Listener_t3000907612 * L_3 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			if (!L_3)
			{
				goto IL_0026;
			}
		}

IL_001b:
		{
			Listener_t3000907612 * L_4 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			V_1 = L_4;
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}

IL_0026:
		{
			Listener_t3000907612 * L_5 = (Listener_t3000907612 *)il2cpp_codegen_object_new(Listener_t3000907612_il2cpp_TypeInfo_var);
			Listener__ctor_m650935776(L_5, /*hidden argument*/NULL);
			((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->set_listener_3(L_5);
			Listener_t3000907612 * L_6 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			V_1 = L_6;
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0042:
	{
		Listener_t3000907612 * L_8 = V_1;
		return L_8;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Destroy()
extern "C"  void Listener_Destroy_m3668999734 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener_Destroy_m3668999734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			Listener_t3000907612 * L_3 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			if (L_3)
			{
				goto IL_0020;
			}
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_0020:
		{
			Listener_t3000907612 * L_4 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			NullCheck(L_4);
			Listener_Dispose_m3683418081(L_4, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_5 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0036:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Finalize()
extern "C"  void Listener_Finalize_m2396768479 (Listener_t3000907612 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Listener_Dispose_m3683418081(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::Dispose()
extern "C"  void Listener_Dispose_m3683418081 (Listener_t3000907612 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener_Dispose_m3683418081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Listener_t3000907612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			Listener_t3000907612 * L_3 = ((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->get_listener_3();
			if ((!(((RuntimeObject*)(Listener_t3000907612 *)L_3) == ((RuntimeObject*)(Listener_t3000907612 *)__this))))
			{
				goto IL_0030;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
			FirebaseMessaging_SetListenerCallbacks_m3664453505(NULL /*static, unused*/, (MessageReceivedDelegate_t2474724020 *)NULL, (TokenReceivedDelegate_t1016457320 *)NULL, /*hidden argument*/NULL);
			((Listener_t3000907612_StaticFields*)il2cpp_codegen_static_fields_for(Listener_t3000907612_il2cpp_TypeInfo_var))->set_listener_3((Listener_t3000907612 *)NULL);
			__this->set_app_2((FirebaseApp_t2526288410 *)NULL);
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003c:
	{
		return;
	}
}
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener::MessageReceivedDelegateMethod(System.IntPtr)
extern "C"  int32_t Listener_MessageReceivedDelegateMethod_m689406986 (RuntimeObject * __this /* static, unused */, intptr_t ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener_MessageReceivedDelegateMethod_m689406986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3466559488 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t3466559488 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_MessageReceivedInternal_0();
		V_0 = L_0;
		EventHandler_1_t3466559488 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		EventHandler_1_t3466559488 * L_2 = V_0;
		intptr_t L_3 = ___message0;
		FirebaseMessage_t2372265022 * L_4 = (FirebaseMessage_t2372265022 *)il2cpp_codegen_object_new(FirebaseMessage_t2372265022_il2cpp_TypeInfo_var);
		FirebaseMessage__ctor_m4090485566(L_4, L_3, (bool)1, /*hidden argument*/NULL);
		MessageReceivedEventArgs_t1247432759 * L_5 = (MessageReceivedEventArgs_t1247432759 *)il2cpp_codegen_object_new(MessageReceivedEventArgs_t1247432759_il2cpp_TypeInfo_var);
		MessageReceivedEventArgs__ctor_m1445459718(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m3274546564(L_2, NULL, L_5, /*hidden argument*/EventHandler_1_Invoke_m3274546564_RuntimeMethod_var);
		return 1;
	}

IL_0021:
	{
		return 0;
	}
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener::TokenReceivedDelegateMethod(System.String)
extern "C"  void Listener_TokenReceivedDelegateMethod_m1616435326 (RuntimeObject * __this /* static, unused */, String_t* ___token0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Listener_TokenReceivedDelegateMethod_m1616435326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2074869383 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var);
		EventHandler_1_t2074869383 * L_0 = ((FirebaseMessaging_t2538764775_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessaging_t2538764775_il2cpp_TypeInfo_var))->get_TokenReceivedInternal_1();
		V_0 = L_0;
		EventHandler_1_t2074869383 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		EventHandler_1_t2074869383 * L_2 = V_0;
		String_t* L_3 = ___token0;
		TokenReceivedEventArgs_t4150709950 * L_4 = (TokenReceivedEventArgs_t4150709950 *)il2cpp_codegen_object_new(TokenReceivedEventArgs_t4150709950_il2cpp_TypeInfo_var);
		TokenReceivedEventArgs__ctor_m4233221851(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m546052383(L_2, NULL, L_4, /*hidden argument*/EventHandler_1_Invoke_m546052383_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  int32_t DelegatePInvokeWrapper_MessageReceivedDelegate_t2474724020 (MessageReceivedDelegate_t2474724020 * __this, intptr_t ___message0, const RuntimeMethod* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(___message0);

	return returnValue;
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MessageReceivedDelegate__ctor_m3696635674 (MessageReceivedDelegate_t2474724020 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::Invoke(System.IntPtr)
extern "C"  int32_t MessageReceivedDelegate_Invoke_m1271845415 (MessageReceivedDelegate_t2474724020 * __this, intptr_t ___message0, const RuntimeMethod* method)
{
	int32_t result = 0;
	if(__this->get_prev_9() != NULL)
	{
		MessageReceivedDelegate_Invoke_m1271845415((MessageReceivedDelegate_t2474724020 *)__this->get_prev_9(), ___message0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			typedef int32_t (*FunctionPointerType) (RuntimeObject *, intptr_t, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(NULL, ___message0, targetMethod);
		}
		else
		{
			// closed
			typedef int32_t (*FunctionPointerType) (RuntimeObject *, void*, intptr_t, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___message0, targetMethod);
		}
	}
	else
	{
		{
			// closed
			typedef int32_t (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
		}
	}
	return result;
}
// System.IAsyncResult Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* MessageReceivedDelegate_BeginInvoke_m1700133236 (MessageReceivedDelegate_t2474724020 * __this, intptr_t ___message0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageReceivedDelegate_BeginInvoke_m1700133236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___message0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Int32 Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  int32_t MessageReceivedDelegate_EndInvoke_m3405877713 (MessageReceivedDelegate_t2474724020 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m3792651002 (MonoPInvokeCallbackAttribute_t1582016909 * __this, Type_t * ___t0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_TokenReceivedDelegate_t1016457320 (TokenReceivedDelegate_t1016457320 * __this, String_t* ___token0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___token0' to native representation
	char* ____token0_marshaled = NULL;
	____token0_marshaled = il2cpp_codegen_marshal_string(___token0);

	// Native function invocation
	il2cppPInvokeFunc(____token0_marshaled);

	// Marshaling cleanup of parameter '___token0' native representation
	il2cpp_codegen_marshal_free(____token0_marshaled);
	____token0_marshaled = NULL;

}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void TokenReceivedDelegate__ctor_m3826971582 (TokenReceivedDelegate_t1016457320 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::Invoke(System.String)
extern "C"  void TokenReceivedDelegate_Invoke_m758289265 (TokenReceivedDelegate_t1016457320 * __this, String_t* ___token0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TokenReceivedDelegate_Invoke_m758289265((TokenReceivedDelegate_t1016457320 *)__this->get_prev_9(), ___token0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, ___token0, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___token0, targetMethod);
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, ___token0, targetMethod);
		}
		else
		{
			// open
			typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(___token0, targetMethod);
		}
	}
}
// System.IAsyncResult Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* TokenReceivedDelegate_BeginInvoke_m2337446016 (TokenReceivedDelegate_t1016457320 * __this, String_t* ___token0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___token0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void TokenReceivedDelegate_EndInvoke_m3310325283 (TokenReceivedDelegate_t1016457320 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::.cctor()
extern "C"  void FirebaseMessagingPINVOKE__cctor_m4180122606 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseMessagingPINVOKE__cctor_m4180122606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t2033454307 * L_0 = (SWIGExceptionHelper_t2033454307 *)il2cpp_codegen_object_new(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var);
		SWIGExceptionHelper__ctor_m3940038267(L_0, /*hidden argument*/NULL);
		((FirebaseMessagingPINVOKE_t151993683_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var))->set_swigExceptionHelper_0(L_0);
		SWIGStringHelper_t1399197998 * L_1 = (SWIGStringHelper_t1399197998 *)il2cpp_codegen_object_new(SWIGStringHelper_t1399197998_il2cpp_TypeInfo_var);
		SWIGStringHelper__ctor_m1398490094(L_1, /*hidden argument*/NULL);
		((FirebaseMessagingPINVOKE_t151993683_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var))->set_swigStringHelper_1(L_1);
		return;
	}
}
extern "C" char* DEFAULT_CALL Firebase_Messaging_FirebaseNotification_Body_get(void*);
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_FirebaseNotification_Body_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* FirebaseMessagingPINVOKE_Firebase_Messaging_FirebaseNotification_Body_get_m2595163183 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Messaging_FirebaseNotification_Body_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_Messaging_delete_FirebaseNotification(void*);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseNotification(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseNotification_m2260423228 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Messaging_delete_FirebaseNotification)(____jarg10_marshaled);

}
extern "C" void DEFAULT_CALL Firebase_Messaging_delete_FirebaseMessage(void*);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseMessage(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseMessage_m1056514804 (RuntimeObject * __this /* static, unused */, HandleRef_t3745784362  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Messaging_delete_FirebaseMessage)(____jarg10_marshaled);

}
extern "C" void DEFAULT_CALL Firebase_Messaging_SetListenerCallbacks(Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacks_m85020952 (RuntimeObject * __this /* static, unused */, MessageReceivedDelegate_t2474724020 * ___jarg10, TokenReceivedDelegate_t1016457320 * ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	Il2CppMethodPointer ____jarg10_marshaled = NULL;
	____jarg10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___jarg10));

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___jarg21));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Messaging_SetListenerCallbacks)(____jarg10_marshaled, ____jarg21_marshaled);

}
extern "C" void DEFAULT_CALL Firebase_Messaging_SetListenerCallbacksEnabled(int32_t, int32_t);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacksEnabled_m2241337789 (RuntimeObject * __this /* static, unused */, bool ___jarg10, bool ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Messaging_SetListenerCallbacksEnabled)(static_cast<int32_t>(___jarg10), static_cast<int32_t>(___jarg21));

}
extern "C" void DEFAULT_CALL Firebase_Messaging_SendPendingEvents();
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SendPendingEvents()
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SendPendingEvents_m2573344962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Messaging_SendPendingEvents)();

}
extern "C" intptr_t DEFAULT_CALL Firebase_Messaging_MessageCopyNotification(intptr_t);
// System.IntPtr Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_MessageCopyNotification(System.IntPtr)
extern "C"  intptr_t FirebaseMessagingPINVOKE_Firebase_Messaging_MessageCopyNotification_m848985824 (RuntimeObject * __this /* static, unused */, intptr_t ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Messaging_MessageCopyNotification)(___jarg10);

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m1295556158(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingApplicationException_m1295556158(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m3149638596(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArithmeticException_m3149638596(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m992672499(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIOException_m992672499(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m886111931(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOverflowException_m886111931(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m483363777(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingSystemException_m483363777(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m863674504(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentException_m863674504(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m273094494(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentNullException_m273094494(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::.cctor()
extern "C"  void SWIGExceptionHelper__cctor_m2293912218 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper__cctor_m2293912218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)SWIGExceptionHelper_SetPendingApplicationException_m1295556158_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_1 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_applicationDelegate_0(L_1);
		intptr_t L_2 = (intptr_t)SWIGExceptionHelper_SetPendingArithmeticException_m3149638596_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_3 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_3, NULL, L_2, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_arithmeticDelegate_1(L_3);
		intptr_t L_4 = (intptr_t)SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_5 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_5, NULL, L_4, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_divideByZeroDelegate_2(L_5);
		intptr_t L_6 = (intptr_t)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_7 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_7, NULL, L_6, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_indexOutOfRangeDelegate_3(L_7);
		intptr_t L_8 = (intptr_t)SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_9 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_9, NULL, L_8, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_invalidCastDelegate_4(L_9);
		intptr_t L_10 = (intptr_t)SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_11 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_11, NULL, L_10, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_invalidOperationDelegate_5(L_11);
		intptr_t L_12 = (intptr_t)SWIGExceptionHelper_SetPendingIOException_m992672499_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_13 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_13, NULL, L_12, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_ioDelegate_6(L_13);
		intptr_t L_14 = (intptr_t)SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_15 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_15, NULL, L_14, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_nullReferenceDelegate_7(L_15);
		intptr_t L_16 = (intptr_t)SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_17 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_17, NULL, L_16, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_outOfMemoryDelegate_8(L_17);
		intptr_t L_18 = (intptr_t)SWIGExceptionHelper_SetPendingOverflowException_m886111931_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_19 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_19, NULL, L_18, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_overflowDelegate_9(L_19);
		intptr_t L_20 = (intptr_t)SWIGExceptionHelper_SetPendingSystemException_m483363777_RuntimeMethod_var;
		ExceptionDelegate_t1699519678 * L_21 = (ExceptionDelegate_t1699519678 *)il2cpp_codegen_object_new(ExceptionDelegate_t1699519678_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m3847540867(L_21, NULL, L_20, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_systemDelegate_10(L_21);
		intptr_t L_22 = (intptr_t)SWIGExceptionHelper_SetPendingArgumentException_m863674504_RuntimeMethod_var;
		ExceptionArgumentDelegate_t1079801895 * L_23 = (ExceptionArgumentDelegate_t1079801895 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1079801895_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m692720776(L_23, NULL, L_22, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_argumentDelegate_11(L_23);
		intptr_t L_24 = (intptr_t)SWIGExceptionHelper_SetPendingArgumentNullException_m273094494_RuntimeMethod_var;
		ExceptionArgumentDelegate_t1079801895 * L_25 = (ExceptionArgumentDelegate_t1079801895 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1079801895_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m692720776(L_25, NULL, L_24, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_argumentNullDelegate_12(L_25);
		intptr_t L_26 = (intptr_t)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642_RuntimeMethod_var;
		ExceptionArgumentDelegate_t1079801895 * L_27 = (ExceptionArgumentDelegate_t1079801895 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1079801895_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m692720776(L_27, NULL, L_26, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->set_argumentOutOfRangeDelegate_13(L_27);
		ExceptionDelegate_t1699519678 * L_28 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_applicationDelegate_0();
		ExceptionDelegate_t1699519678 * L_29 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_arithmeticDelegate_1();
		ExceptionDelegate_t1699519678 * L_30 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_divideByZeroDelegate_2();
		ExceptionDelegate_t1699519678 * L_31 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_indexOutOfRangeDelegate_3();
		ExceptionDelegate_t1699519678 * L_32 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_invalidCastDelegate_4();
		ExceptionDelegate_t1699519678 * L_33 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_invalidOperationDelegate_5();
		ExceptionDelegate_t1699519678 * L_34 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_ioDelegate_6();
		ExceptionDelegate_t1699519678 * L_35 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_nullReferenceDelegate_7();
		ExceptionDelegate_t1699519678 * L_36 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_outOfMemoryDelegate_8();
		ExceptionDelegate_t1699519678 * L_37 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_overflowDelegate_9();
		ExceptionDelegate_t1699519678 * L_38 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_systemDelegate_10();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirebaseMessaging_m2584444840(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		ExceptionArgumentDelegate_t1079801895 * L_39 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_argumentDelegate_11();
		ExceptionArgumentDelegate_t1079801895 * L_40 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_argumentNullDelegate_12();
		ExceptionArgumentDelegate_t1079801895 * L_41 = ((SWIGExceptionHelper_t2033454307_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t2033454307_il2cpp_TypeInfo_var))->get_argumentOutOfRangeDelegate_13();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging_m2780727381(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m3940038267 (SWIGExceptionHelper_t2033454307 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacks_FirebaseMessaging(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirebaseMessaging_m2584444840 (RuntimeObject * __this /* static, unused */, ExceptionDelegate_t1699519678 * ___applicationDelegate0, ExceptionDelegate_t1699519678 * ___arithmeticDelegate1, ExceptionDelegate_t1699519678 * ___divideByZeroDelegate2, ExceptionDelegate_t1699519678 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t1699519678 * ___invalidCastDelegate4, ExceptionDelegate_t1699519678 * ___invalidOperationDelegate5, ExceptionDelegate_t1699519678 * ___ioDelegate6, ExceptionDelegate_t1699519678 * ___nullReferenceDelegate7, ExceptionDelegate_t1699519678 * ___outOfMemoryDelegate8, ExceptionDelegate_t1699519678 * ___overflowDelegate9, ExceptionDelegate_t1699519678 * ___systemExceptionDelegate10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___systemExceptionDelegate10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_FirebaseMessaging)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);

}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging_m2780727381 (RuntimeObject * __this /* static, unused */, ExceptionArgumentDelegate_t1079801895 * ___argumentDelegate0, ExceptionArgumentDelegate_t1079801895 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t1079801895 * ___argumentOutOfRangeDelegate2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacksArgument_FirebaseMessaging)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);

}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m1295556158 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingApplicationException_m1295556158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationException_t2339761290 * L_2 = (ApplicationException_t2339761290 *)il2cpp_codegen_object_new(ApplicationException_t2339761290_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m692455299(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m3149638596 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArithmeticException_m3149638596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArithmeticException_t4283546778 * L_2 = (ArithmeticException_t4283546778 *)il2cpp_codegen_object_new(ArithmeticException_t4283546778_il2cpp_TypeInfo_var);
		ArithmeticException__ctor_m3829702723(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingDivideByZeroException_m138309830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		DivideByZeroException_t1856388118 * L_2 = (DivideByZeroException_t1856388118 *)il2cpp_codegen_object_new(DivideByZeroException_t1856388118_il2cpp_TypeInfo_var);
		DivideByZeroException__ctor_m3132723944(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m3824163101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		IndexOutOfRangeException_t1578797820 * L_2 = (IndexOutOfRangeException_t1578797820 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3342077230(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidCastException_m2584038508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m2604994586(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidOperationException_m2784639161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidOperationException_t56020091 * L_2 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1685032583(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m992672499 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIOException_m992672499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOException_t4088381929 * L_2 = (IOException_t4088381929 *)il2cpp_codegen_object_new(IOException_t4088381929_il2cpp_TypeInfo_var);
		IOException__ctor_m3246761956(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingNullReferenceException_m4022760059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullReferenceException_t1023182353 * L_2 = (NullReferenceException_t1023182353 *)il2cpp_codegen_object_new(NullReferenceException_t1023182353_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m3182281066(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOutOfMemoryException_m2025517152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		OutOfMemoryException_t2437671686 * L_2 = (OutOfMemoryException_t2437671686 *)il2cpp_codegen_object_new(OutOfMemoryException_t2437671686_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m4180974057(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m886111931 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOverflowException_m886111931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		OverflowException_t2020128637 * L_2 = (OverflowException_t2020128637 *)il2cpp_codegen_object_new(OverflowException_t2020128637_il2cpp_TypeInfo_var);
		OverflowException__ctor_m2524654454(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m483363777 (RuntimeObject * __this /* static, unused */, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingSystemException_m483363777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		SystemException_t176217640 * L_2 = (SystemException_t176217640 *)il2cpp_codegen_object_new(SystemException_t176217640_il2cpp_TypeInfo_var);
		SystemException__ctor_m4132668650(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m863674504 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentException_m863674504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_2 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_3 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3761792013(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m273094494 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentNullException_m273094494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_0 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, L_2, _stringLiteral88632345, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentNullException_t1615371798 * L_8 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2009621981(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3401096642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_0 = SWIGPendingException_Retrieve_m1387100625(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, L_2, _stringLiteral88632345, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentOutOfRangeException_t777629997 * L_8 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2508649586(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1079801895 (ExceptionArgumentDelegate_t1079801895 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m692720776 (ExceptionArgumentDelegate_t1079801895 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m2458815942 (ExceptionArgumentDelegate_t1079801895 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionArgumentDelegate_Invoke_m2458815942((ExceptionArgumentDelegate_t1079801895 *)__this->get_prev_9(), ___message0, ___paramName1, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, String_t*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, ___message0, ___paramName1, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, String_t*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___message0, ___paramName1, targetMethod);
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
		{
			// closed
			typedef void (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, ___paramName1, targetMethod);
		}
		else
		{
			// open
			typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(___message0, ___paramName1, targetMethod);
		}
	}
}
// System.IAsyncResult Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ExceptionArgumentDelegate_BeginInvoke_m3926788802 (ExceptionArgumentDelegate_t1079801895 * __this, String_t* ___message0, String_t* ___paramName1, AsyncCallback_t3962456242 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___message0;
	__d_args[1] = ___paramName1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionArgumentDelegate_EndInvoke_m2263525531 (ExceptionArgumentDelegate_t1079801895 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_ExceptionDelegate_t1699519678 (ExceptionDelegate_t1699519678 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m3847540867 (ExceptionDelegate_t1699519678 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m255100690 (ExceptionDelegate_t1699519678 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionDelegate_Invoke_m255100690((ExceptionDelegate_t1699519678 *)__this->get_prev_9(), ___message0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, ___message0, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___message0, targetMethod);
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
		}
		else
		{
			// open
			typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
		}
	}
}
// System.IAsyncResult Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ExceptionDelegate_BeginInvoke_m2268660395 (ExceptionDelegate_t1699519678 * __this, String_t* ___message0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionDelegate_EndInvoke_m3819104561 (ExceptionDelegate_t1699519678 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::Set(System.Exception)
extern "C"  void SWIGPendingException_Set_m2508649586 (RuntimeObject * __this /* static, unused */, Exception_t * ___e0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Set_m2508649586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_0 = ((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = ((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_pendingException_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1985503341, L_2, _stringLiteral3452614535, /*hidden argument*/NULL);
		Exception_t * L_4 = ___e0;
		ApplicationException_t2339761290 * L_5 = (ApplicationException_t2339761290 *)il2cpp_codegen_object_new(ApplicationException_t2339761290_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m692455299(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		Exception_t * L_6 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->set_pendingException_0(L_6);
		RuntimeTypeHandle_t3027515415  L_7 = { reinterpret_cast<intptr_t> (FirebaseMessagingPINVOKE_t151993683_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		RuntimeObject * L_9 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		int32_t L_10 = ((SWIGPendingException_t717714779_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		((SWIGPendingException_t717714779_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->set_numExceptionsPending_1(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0059:
	{
		return;
	}
}
// System.Exception Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t * SWIGPendingException_Retrieve_m1387100625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Retrieve_m1387100625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t717714779_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_1 = ((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		Exception_t * L_2 = ((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = L_2;
		((SWIGPendingException_t717714779_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t *)NULL);
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (FirebaseMessagingPINVOKE_t151993683_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		RuntimeObject * L_5 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0034:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t717714779_il2cpp_TypeInfo_var);
		int32_t L_6 = ((SWIGPendingException_t717714779_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		((SWIGPendingException_t717714779_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t717714779_il2cpp_TypeInfo_var))->set_numExceptionsPending_1(((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)));
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004c:
	{
		Exception_t * L_8 = V_0;
		return L_8;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::.cctor()
extern "C"  void SWIGPendingException__cctor_m3193115150 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m2294391909(char* ___cString0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue = SWIGStringHelper_CreateString_m2294391909(NULL, ____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::.cctor()
extern "C"  void SWIGStringHelper__cctor_m2475557427 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGStringHelper__cctor_m2475557427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)SWIGStringHelper_CreateString_m2294391909_RuntimeMethod_var;
		SWIGStringDelegate_t3107596264 * L_1 = (SWIGStringDelegate_t3107596264 *)il2cpp_codegen_object_new(SWIGStringDelegate_t3107596264_il2cpp_TypeInfo_var);
		SWIGStringDelegate__ctor_m965772466(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGStringHelper_t1399197998_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t1399197998_il2cpp_TypeInfo_var))->set_stringDelegate_0(L_1);
		SWIGStringDelegate_t3107596264 * L_2 = ((SWIGStringHelper_t1399197998_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t1399197998_il2cpp_TypeInfo_var))->get_stringDelegate_0();
		SWIGStringHelper_SWIGRegisterStringCallback_FirebaseMessaging_m2853007358(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1398490094 (SWIGStringHelper_t1399197998 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterStringCallback_FirebaseMessaging(Il2CppMethodPointer);
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_FirebaseMessaging(Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_FirebaseMessaging_m2853007358 (RuntimeObject * __this /* static, unused */, SWIGStringDelegate_t3107596264 * ___stringDelegate0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___stringDelegate0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_FirebaseMessaging)(____stringDelegate0_marshaled);

}
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m2294391909 (RuntimeObject * __this /* static, unused */, String_t* ___cString0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___cString0;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_t3107596264 (SWIGStringDelegate_t3107596264 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef char* (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m965772466 (SWIGStringDelegate_t3107596264 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m3788666468 (SWIGStringDelegate_t3107596264 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	String_t* result = NULL;
	if(__this->get_prev_9() != NULL)
	{
		SWIGStringDelegate_Invoke_m3788666468((SWIGStringDelegate_t3107596264 *)__this->get_prev_9(), ___message0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			typedef String_t* (*FunctionPointerType) (RuntimeObject *, String_t*, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(NULL, ___message0, targetMethod);
		}
		else
		{
			// closed
			typedef String_t* (*FunctionPointerType) (RuntimeObject *, void*, String_t*, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___message0, targetMethod);
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			typedef String_t* (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
		}
		else
		{
			// open
			typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
			result = ((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
		}
	}
	return result;
}
// System.IAsyncResult Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* SWIGStringDelegate_BeginInvoke_m976710221 (SWIGStringDelegate_t3107596264 * __this, String_t* ___message0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern "C"  String_t* SWIGStringDelegate_EndInvoke_m1184135800 (SWIGStringDelegate_t3107596264 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (String_t*)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.FirebaseNotification::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseNotification__ctor_m927513772 (FirebaseNotification_t1925808173 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t3745784362  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m868850155((&L_2), __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseNotification::Finalize()
extern "C"  void FirebaseNotification_Finalize_m412738460 (FirebaseNotification_t1925808173 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseNotification_Dispose_m2323517056(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Messaging.FirebaseNotification::Dispose()
extern "C"  void FirebaseNotification_Dispose_m2323517056 (FirebaseNotification_t1925808173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseNotification_Dispose_m2323517056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		RuntimeObject * L_0 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t3745784362 * L_1 = __this->get_address_of_swigCPtr_0();
			intptr_t L_2 = HandleRef_get_Handle_m4239591804(L_1, /*hidden argument*/NULL);
			bool L_3 = IntPtr_op_Inequality_m3063970704(NULL /*static, unused*/, L_2, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_4 = __this->get_swigCMemOwn_1();
			if (!L_4)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t3745784362  L_5 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
			FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseNotification_m2260423228(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		}

IL_003f:
		{
			HandleRef_t3745784362  L_6;
			memset(&L_6, 0, sizeof(L_6));
			HandleRef__ctor_m868850155((&L_6), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_6);
		}

IL_0050:
		{
			GC_SuppressFinalize_m1177400158(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0062:
	{
		return;
	}
}
// System.String Firebase.Messaging.FirebaseNotification::get_Body()
extern "C"  String_t* FirebaseNotification_get_Body_m748811829 (FirebaseNotification_t1925808173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseNotification_get_Body_m748811829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t3745784362  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseMessagingPINVOKE_t151993683_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseMessagingPINVOKE_Firebase_Messaging_FirebaseNotification_Body_get_m2595163183(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m582413763(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3190247900_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m2171998990(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.MessageReceivedEventArgs::.ctor(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs__ctor_m1445459718 (MessageReceivedEventArgs_t1247432759 * __this, FirebaseMessage_t2372265022 * ___msg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageReceivedEventArgs__ctor_m1445459718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		FirebaseMessage_t2372265022 * L_0 = ___msg0;
		MessageReceivedEventArgs_set_Message_m4177955988(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.Messaging.FirebaseMessage Firebase.Messaging.MessageReceivedEventArgs::get_Message()
extern "C"  FirebaseMessage_t2372265022 * MessageReceivedEventArgs_get_Message_m4237298457 (MessageReceivedEventArgs_t1247432759 * __this, const RuntimeMethod* method)
{
	{
		FirebaseMessage_t2372265022 * L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Firebase.Messaging.MessageReceivedEventArgs::set_Message(Firebase.Messaging.FirebaseMessage)
extern "C"  void MessageReceivedEventArgs_set_Message_m4177955988 (MessageReceivedEventArgs_t1247432759 * __this, FirebaseMessage_t2372265022 * ___value0, const RuntimeMethod* method)
{
	{
		FirebaseMessage_t2372265022 * L_0 = ___value0;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Messaging.TokenReceivedEventArgs::.ctor(System.String)
extern "C"  void TokenReceivedEventArgs__ctor_m4233221851 (TokenReceivedEventArgs_t4150709950 * __this, String_t* ___token0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TokenReceivedEventArgs__ctor_m4233221851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___token0;
		TokenReceivedEventArgs_set_Token_m4270615688(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Messaging.TokenReceivedEventArgs::get_Token()
extern "C"  String_t* TokenReceivedEventArgs_get_Token_m2383727513 (TokenReceivedEventArgs_t4150709950 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTokenU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Firebase.Messaging.TokenReceivedEventArgs::set_Token(System.String)
extern "C"  void TokenReceivedEventArgs_set_Token_m4270615688 (TokenReceivedEventArgs_t4150709950 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
