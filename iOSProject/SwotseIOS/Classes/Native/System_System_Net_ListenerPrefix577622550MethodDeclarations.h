﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ListenerPrefix
struct ListenerPrefix_t577622550;
// System.String
struct String_t;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.ListenerPrefix::.ctor(System.String)
extern "C"  void ListenerPrefix__ctor_m821803696 (ListenerPrefix_t577622550 * __this, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.ListenerPrefix::ToString()
extern "C"  String_t* ListenerPrefix_ToString_m1895123285 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress[] System.Net.ListenerPrefix::get_Addresses()
extern "C"  IPAddressU5BU5D_t4087230954* ListenerPrefix_get_Addresses_m3918967335 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerPrefix::set_Addresses(System.Net.IPAddress[])
extern "C"  void ListenerPrefix_set_Addresses_m1864182378 (ListenerPrefix_t577622550 * __this, IPAddressU5BU5D_t4087230954* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ListenerPrefix::get_Secure()
extern "C"  bool ListenerPrefix_get_Secure_m1949083008 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.ListenerPrefix::get_Host()
extern "C"  String_t* ListenerPrefix_get_Host_m870985920 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ListenerPrefix::get_Port()
extern "C"  int32_t ListenerPrefix_get_Port_m4043881394 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.ListenerPrefix::get_Path()
extern "C"  String_t* ListenerPrefix_get_Path_m624941475 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ListenerPrefix::Equals(System.Object)
extern "C"  bool ListenerPrefix_Equals_m974974115 (ListenerPrefix_t577622550 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ListenerPrefix::GetHashCode()
extern "C"  int32_t ListenerPrefix_GetHashCode_m4219037465 (ListenerPrefix_t577622550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerPrefix::Parse(System.String)
extern "C"  void ListenerPrefix_Parse_m1857467029 (ListenerPrefix_t577622550 * __this, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ListenerPrefix::CheckUri(System.String)
extern "C"  void ListenerPrefix_CheckUri_m1289803452 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
