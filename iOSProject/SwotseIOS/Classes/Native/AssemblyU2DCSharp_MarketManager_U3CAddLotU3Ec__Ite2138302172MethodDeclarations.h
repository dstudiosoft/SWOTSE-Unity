﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketManager/<AddLot>c__Iterator10
struct U3CAddLotU3Ec__Iterator10_t2138302172;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketManager/<AddLot>c__Iterator10::.ctor()
extern "C"  void U3CAddLotU3Ec__Iterator10__ctor_m3488323331 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<AddLot>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAddLotU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m328992109 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketManager/<AddLot>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAddLotU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m1219787653 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketManager/<AddLot>c__Iterator10::MoveNext()
extern "C"  bool U3CAddLotU3Ec__Iterator10_MoveNext_m1250227029 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<AddLot>c__Iterator10::Dispose()
extern "C"  void U3CAddLotU3Ec__Iterator10_Dispose_m3310373790 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketManager/<AddLot>c__Iterator10::Reset()
extern "C"  void U3CAddLotU3Ec__Iterator10_Reset_m3318571040 (U3CAddLotU3Ec__Iterator10_t2138302172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
