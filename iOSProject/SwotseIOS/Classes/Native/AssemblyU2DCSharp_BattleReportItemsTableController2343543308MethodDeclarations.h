﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportItemsTableController
struct BattleReportItemsTableController_t2343543308;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void BattleReportItemsTableController::.ctor()
extern "C"  void BattleReportItemsTableController__ctor_m2159936371 (BattleReportItemsTableController_t2343543308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportItemsTableController::Start()
extern "C"  void BattleReportItemsTableController_Start_m963045239 (BattleReportItemsTableController_t2343543308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BattleReportItemsTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t BattleReportItemsTableController_GetNumberOfRowsForTableView_m1195500047 (BattleReportItemsTableController_t2343543308 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BattleReportItemsTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float BattleReportItemsTableController_GetHeightForRowInTableView_m1203370775 (BattleReportItemsTableController_t2343543308 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell BattleReportItemsTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * BattleReportItemsTableController_GetCellForRowInTableView_m2967524122 (BattleReportItemsTableController_t2343543308 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
