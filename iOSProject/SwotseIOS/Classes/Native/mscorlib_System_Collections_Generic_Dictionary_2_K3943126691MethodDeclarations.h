﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3279677322(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3943126691 *, Dictionary_2_t1459628920 *, const MethodInfo*))KeyCollection__ctor_m3885543862_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1587178948(__this, ___item0, method) ((  void (*) (KeyCollection_t3943126691 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1794347641(__this, method) ((  void (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1566842684(__this, ___item0, method) ((  bool (*) (KeyCollection_t3943126691 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4009900989(__this, ___item0, method) ((  bool (*) (KeyCollection_t3943126691 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1193414815(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2810296999(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3943126691 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2870796176(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2980468465(__this, method) ((  bool (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4181516287(__this, method) ((  bool (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m691501875(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3981366205(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3943126691 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1238003947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1430943112(__this, method) ((  Enumerator_t4149132358  (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_GetEnumerator_m3687526536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,ArmyTimer>::get_Count()
#define KeyCollection_get_Count_m3274870071(__this, method) ((  int32_t (*) (KeyCollection_t3943126691 *, const MethodInfo*))KeyCollection_get_Count_m972382317_gshared)(__this, method)
