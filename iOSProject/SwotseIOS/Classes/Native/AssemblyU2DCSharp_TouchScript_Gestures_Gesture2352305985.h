﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>
struct EventHandler_1_t2091288363;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t328750215;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TouchScript.Gestures.Gesture
struct Gesture_t2352305985;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1721427117;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint>
struct ReadOnlyCollection_1_t1145414775;
// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>
struct TimedSequence_1_t2737250283;
// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t505647059;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t4252506175;
// TouchScript.ITouchManager
struct ITouchManager_t2552034033;

#include "AssemblyU2DCSharp_TouchScript_DebuggableMonoBehavi3136086048.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Ges2128095272.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Touc594133898.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture
struct  Gesture_t2352305985  : public DebuggableMonoBehaviour_t3136086048
{
public:
	// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs> TouchScript.Gestures.Gesture::stateChangedInvoker
	EventHandler_1_t2091288363 * ___stateChangedInvoker_4;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Gesture::cancelledInvoker
	EventHandler_1_t1880931879 * ___cancelledInvoker_5;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::activeTouches
	List_1_t328750215 * ___activeTouches_6;
	// UnityEngine.Transform TouchScript.Gestures.Gesture::cachedTransform
	Transform_t3275118058 * ___cachedTransform_7;
	// System.Boolean TouchScript.Gestures.Gesture::advancedProps
	bool ___advancedProps_8;
	// System.Int32 TouchScript.Gestures.Gesture::minTouches
	int32_t ___minTouches_9;
	// System.Int32 TouchScript.Gestures.Gesture::maxTouches
	int32_t ___maxTouches_10;
	// System.Boolean TouchScript.Gestures.Gesture::combineTouches
	bool ___combineTouches_11;
	// System.Single TouchScript.Gestures.Gesture::combineTouchesInterval
	float ___combineTouchesInterval_12;
	// System.Boolean TouchScript.Gestures.Gesture::useSendMessage
	bool ___useSendMessage_13;
	// System.Boolean TouchScript.Gestures.Gesture::sendStateChangeMessages
	bool ___sendStateChangeMessages_14;
	// UnityEngine.GameObject TouchScript.Gestures.Gesture::sendMessageTarget
	GameObject_t1756533147 * ___sendMessageTarget_15;
	// TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::requireGestureToFail
	Gesture_t2352305985 * ___requireGestureToFail_16;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Gestures.Gesture::friendlyGestures
	List_1_t1721427117 * ___friendlyGestures_17;
	// System.Int32 TouchScript.Gestures.Gesture::numTouches
	int32_t ___numTouches_18;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.Gesture::layer
	TouchLayer_t2635439978 * ___layer_19;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::readonlyActiveTouches
	ReadOnlyCollection_1_t1145414775 * ___readonlyActiveTouches_20;
	// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::touchSequence
	TimedSequence_1_t2737250283 * ___touchSequence_21;
	// TouchScript.GestureManagerInstance TouchScript.Gestures.Gesture::gestureManagerInstance
	GestureManagerInstance_t505647059 * ___gestureManagerInstance_22;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::delayedStateChange
	int32_t ___delayedStateChange_23;
	// System.Boolean TouchScript.Gestures.Gesture::requiredGestureFailed
	bool ___requiredGestureFailed_24;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::state
	int32_t ___state_25;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedScreenPosition
	Vector2_t2243707579  ___cachedScreenPosition_26;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedPreviousScreenPosition
	Vector2_t2243707579  ___cachedPreviousScreenPosition_27;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_28;
	// TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::<Delegate>k__BackingField
	Il2CppObject * ___U3CDelegateU3Ek__BackingField_29;
	// TouchScript.ITouchManager TouchScript.Gestures.Gesture::<touchManager>k__BackingField
	Il2CppObject * ___U3CtouchManagerU3Ek__BackingField_30;
	// TouchScript.Gestures.Gesture/TouchesNumState TouchScript.Gestures.Gesture::<touchesNumState>k__BackingField
	int32_t ___U3CtouchesNumStateU3Ek__BackingField_31;

public:
	inline static int32_t get_offset_of_stateChangedInvoker_4() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___stateChangedInvoker_4)); }
	inline EventHandler_1_t2091288363 * get_stateChangedInvoker_4() const { return ___stateChangedInvoker_4; }
	inline EventHandler_1_t2091288363 ** get_address_of_stateChangedInvoker_4() { return &___stateChangedInvoker_4; }
	inline void set_stateChangedInvoker_4(EventHandler_1_t2091288363 * value)
	{
		___stateChangedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier(&___stateChangedInvoker_4, value);
	}

	inline static int32_t get_offset_of_cancelledInvoker_5() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cancelledInvoker_5)); }
	inline EventHandler_1_t1880931879 * get_cancelledInvoker_5() const { return ___cancelledInvoker_5; }
	inline EventHandler_1_t1880931879 ** get_address_of_cancelledInvoker_5() { return &___cancelledInvoker_5; }
	inline void set_cancelledInvoker_5(EventHandler_1_t1880931879 * value)
	{
		___cancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier(&___cancelledInvoker_5, value);
	}

	inline static int32_t get_offset_of_activeTouches_6() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___activeTouches_6)); }
	inline List_1_t328750215 * get_activeTouches_6() const { return ___activeTouches_6; }
	inline List_1_t328750215 ** get_address_of_activeTouches_6() { return &___activeTouches_6; }
	inline void set_activeTouches_6(List_1_t328750215 * value)
	{
		___activeTouches_6 = value;
		Il2CppCodeGenWriteBarrier(&___activeTouches_6, value);
	}

	inline static int32_t get_offset_of_cachedTransform_7() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedTransform_7)); }
	inline Transform_t3275118058 * get_cachedTransform_7() const { return ___cachedTransform_7; }
	inline Transform_t3275118058 ** get_address_of_cachedTransform_7() { return &___cachedTransform_7; }
	inline void set_cachedTransform_7(Transform_t3275118058 * value)
	{
		___cachedTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___cachedTransform_7, value);
	}

	inline static int32_t get_offset_of_advancedProps_8() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___advancedProps_8)); }
	inline bool get_advancedProps_8() const { return ___advancedProps_8; }
	inline bool* get_address_of_advancedProps_8() { return &___advancedProps_8; }
	inline void set_advancedProps_8(bool value)
	{
		___advancedProps_8 = value;
	}

	inline static int32_t get_offset_of_minTouches_9() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___minTouches_9)); }
	inline int32_t get_minTouches_9() const { return ___minTouches_9; }
	inline int32_t* get_address_of_minTouches_9() { return &___minTouches_9; }
	inline void set_minTouches_9(int32_t value)
	{
		___minTouches_9 = value;
	}

	inline static int32_t get_offset_of_maxTouches_10() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___maxTouches_10)); }
	inline int32_t get_maxTouches_10() const { return ___maxTouches_10; }
	inline int32_t* get_address_of_maxTouches_10() { return &___maxTouches_10; }
	inline void set_maxTouches_10(int32_t value)
	{
		___maxTouches_10 = value;
	}

	inline static int32_t get_offset_of_combineTouches_11() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___combineTouches_11)); }
	inline bool get_combineTouches_11() const { return ___combineTouches_11; }
	inline bool* get_address_of_combineTouches_11() { return &___combineTouches_11; }
	inline void set_combineTouches_11(bool value)
	{
		___combineTouches_11 = value;
	}

	inline static int32_t get_offset_of_combineTouchesInterval_12() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___combineTouchesInterval_12)); }
	inline float get_combineTouchesInterval_12() const { return ___combineTouchesInterval_12; }
	inline float* get_address_of_combineTouchesInterval_12() { return &___combineTouchesInterval_12; }
	inline void set_combineTouchesInterval_12(float value)
	{
		___combineTouchesInterval_12 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_13() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___useSendMessage_13)); }
	inline bool get_useSendMessage_13() const { return ___useSendMessage_13; }
	inline bool* get_address_of_useSendMessage_13() { return &___useSendMessage_13; }
	inline void set_useSendMessage_13(bool value)
	{
		___useSendMessage_13 = value;
	}

	inline static int32_t get_offset_of_sendStateChangeMessages_14() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___sendStateChangeMessages_14)); }
	inline bool get_sendStateChangeMessages_14() const { return ___sendStateChangeMessages_14; }
	inline bool* get_address_of_sendStateChangeMessages_14() { return &___sendStateChangeMessages_14; }
	inline void set_sendStateChangeMessages_14(bool value)
	{
		___sendStateChangeMessages_14 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_15() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___sendMessageTarget_15)); }
	inline GameObject_t1756533147 * get_sendMessageTarget_15() const { return ___sendMessageTarget_15; }
	inline GameObject_t1756533147 ** get_address_of_sendMessageTarget_15() { return &___sendMessageTarget_15; }
	inline void set_sendMessageTarget_15(GameObject_t1756533147 * value)
	{
		___sendMessageTarget_15 = value;
		Il2CppCodeGenWriteBarrier(&___sendMessageTarget_15, value);
	}

	inline static int32_t get_offset_of_requireGestureToFail_16() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___requireGestureToFail_16)); }
	inline Gesture_t2352305985 * get_requireGestureToFail_16() const { return ___requireGestureToFail_16; }
	inline Gesture_t2352305985 ** get_address_of_requireGestureToFail_16() { return &___requireGestureToFail_16; }
	inline void set_requireGestureToFail_16(Gesture_t2352305985 * value)
	{
		___requireGestureToFail_16 = value;
		Il2CppCodeGenWriteBarrier(&___requireGestureToFail_16, value);
	}

	inline static int32_t get_offset_of_friendlyGestures_17() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___friendlyGestures_17)); }
	inline List_1_t1721427117 * get_friendlyGestures_17() const { return ___friendlyGestures_17; }
	inline List_1_t1721427117 ** get_address_of_friendlyGestures_17() { return &___friendlyGestures_17; }
	inline void set_friendlyGestures_17(List_1_t1721427117 * value)
	{
		___friendlyGestures_17 = value;
		Il2CppCodeGenWriteBarrier(&___friendlyGestures_17, value);
	}

	inline static int32_t get_offset_of_numTouches_18() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___numTouches_18)); }
	inline int32_t get_numTouches_18() const { return ___numTouches_18; }
	inline int32_t* get_address_of_numTouches_18() { return &___numTouches_18; }
	inline void set_numTouches_18(int32_t value)
	{
		___numTouches_18 = value;
	}

	inline static int32_t get_offset_of_layer_19() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___layer_19)); }
	inline TouchLayer_t2635439978 * get_layer_19() const { return ___layer_19; }
	inline TouchLayer_t2635439978 ** get_address_of_layer_19() { return &___layer_19; }
	inline void set_layer_19(TouchLayer_t2635439978 * value)
	{
		___layer_19 = value;
		Il2CppCodeGenWriteBarrier(&___layer_19, value);
	}

	inline static int32_t get_offset_of_readonlyActiveTouches_20() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___readonlyActiveTouches_20)); }
	inline ReadOnlyCollection_1_t1145414775 * get_readonlyActiveTouches_20() const { return ___readonlyActiveTouches_20; }
	inline ReadOnlyCollection_1_t1145414775 ** get_address_of_readonlyActiveTouches_20() { return &___readonlyActiveTouches_20; }
	inline void set_readonlyActiveTouches_20(ReadOnlyCollection_1_t1145414775 * value)
	{
		___readonlyActiveTouches_20 = value;
		Il2CppCodeGenWriteBarrier(&___readonlyActiveTouches_20, value);
	}

	inline static int32_t get_offset_of_touchSequence_21() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___touchSequence_21)); }
	inline TimedSequence_1_t2737250283 * get_touchSequence_21() const { return ___touchSequence_21; }
	inline TimedSequence_1_t2737250283 ** get_address_of_touchSequence_21() { return &___touchSequence_21; }
	inline void set_touchSequence_21(TimedSequence_1_t2737250283 * value)
	{
		___touchSequence_21 = value;
		Il2CppCodeGenWriteBarrier(&___touchSequence_21, value);
	}

	inline static int32_t get_offset_of_gestureManagerInstance_22() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___gestureManagerInstance_22)); }
	inline GestureManagerInstance_t505647059 * get_gestureManagerInstance_22() const { return ___gestureManagerInstance_22; }
	inline GestureManagerInstance_t505647059 ** get_address_of_gestureManagerInstance_22() { return &___gestureManagerInstance_22; }
	inline void set_gestureManagerInstance_22(GestureManagerInstance_t505647059 * value)
	{
		___gestureManagerInstance_22 = value;
		Il2CppCodeGenWriteBarrier(&___gestureManagerInstance_22, value);
	}

	inline static int32_t get_offset_of_delayedStateChange_23() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___delayedStateChange_23)); }
	inline int32_t get_delayedStateChange_23() const { return ___delayedStateChange_23; }
	inline int32_t* get_address_of_delayedStateChange_23() { return &___delayedStateChange_23; }
	inline void set_delayedStateChange_23(int32_t value)
	{
		___delayedStateChange_23 = value;
	}

	inline static int32_t get_offset_of_requiredGestureFailed_24() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___requiredGestureFailed_24)); }
	inline bool get_requiredGestureFailed_24() const { return ___requiredGestureFailed_24; }
	inline bool* get_address_of_requiredGestureFailed_24() { return &___requiredGestureFailed_24; }
	inline void set_requiredGestureFailed_24(bool value)
	{
		___requiredGestureFailed_24 = value;
	}

	inline static int32_t get_offset_of_state_25() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___state_25)); }
	inline int32_t get_state_25() const { return ___state_25; }
	inline int32_t* get_address_of_state_25() { return &___state_25; }
	inline void set_state_25(int32_t value)
	{
		___state_25 = value;
	}

	inline static int32_t get_offset_of_cachedScreenPosition_26() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedScreenPosition_26)); }
	inline Vector2_t2243707579  get_cachedScreenPosition_26() const { return ___cachedScreenPosition_26; }
	inline Vector2_t2243707579 * get_address_of_cachedScreenPosition_26() { return &___cachedScreenPosition_26; }
	inline void set_cachedScreenPosition_26(Vector2_t2243707579  value)
	{
		___cachedScreenPosition_26 = value;
	}

	inline static int32_t get_offset_of_cachedPreviousScreenPosition_27() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedPreviousScreenPosition_27)); }
	inline Vector2_t2243707579  get_cachedPreviousScreenPosition_27() const { return ___cachedPreviousScreenPosition_27; }
	inline Vector2_t2243707579 * get_address_of_cachedPreviousScreenPosition_27() { return &___cachedPreviousScreenPosition_27; }
	inline void set_cachedPreviousScreenPosition_27(Vector2_t2243707579  value)
	{
		___cachedPreviousScreenPosition_27 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CPreviousStateU3Ek__BackingField_28)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_28() const { return ___U3CPreviousStateU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_28() { return &___U3CPreviousStateU3Ek__BackingField_28; }
	inline void set_U3CPreviousStateU3Ek__BackingField_28(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CDelegateU3Ek__BackingField_29)); }
	inline Il2CppObject * get_U3CDelegateU3Ek__BackingField_29() const { return ___U3CDelegateU3Ek__BackingField_29; }
	inline Il2CppObject ** get_address_of_U3CDelegateU3Ek__BackingField_29() { return &___U3CDelegateU3Ek__BackingField_29; }
	inline void set_U3CDelegateU3Ek__BackingField_29(Il2CppObject * value)
	{
		___U3CDelegateU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDelegateU3Ek__BackingField_29, value);
	}

	inline static int32_t get_offset_of_U3CtouchManagerU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CtouchManagerU3Ek__BackingField_30)); }
	inline Il2CppObject * get_U3CtouchManagerU3Ek__BackingField_30() const { return ___U3CtouchManagerU3Ek__BackingField_30; }
	inline Il2CppObject ** get_address_of_U3CtouchManagerU3Ek__BackingField_30() { return &___U3CtouchManagerU3Ek__BackingField_30; }
	inline void set_U3CtouchManagerU3Ek__BackingField_30(Il2CppObject * value)
	{
		___U3CtouchManagerU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtouchManagerU3Ek__BackingField_30, value);
	}

	inline static int32_t get_offset_of_U3CtouchesNumStateU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CtouchesNumStateU3Ek__BackingField_31)); }
	inline int32_t get_U3CtouchesNumStateU3Ek__BackingField_31() const { return ___U3CtouchesNumStateU3Ek__BackingField_31; }
	inline int32_t* get_address_of_U3CtouchesNumStateU3Ek__BackingField_31() { return &___U3CtouchesNumStateU3Ek__BackingField_31; }
	inline void set_U3CtouchesNumStateU3Ek__BackingField_31(int32_t value)
	{
		___U3CtouchesNumStateU3Ek__BackingField_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
