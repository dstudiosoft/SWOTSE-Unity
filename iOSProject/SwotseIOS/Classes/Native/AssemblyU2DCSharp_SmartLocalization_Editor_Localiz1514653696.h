﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.Editor.LocalizedText
struct  LocalizedText_t1514653696  : public MonoBehaviour_t1158329972
{
public:
	// System.String SmartLocalization.Editor.LocalizedText::localizedKey
	String_t* ___localizedKey_2;
	// UnityEngine.UI.Text SmartLocalization.Editor.LocalizedText::textObject
	Text_t356221433 * ___textObject_3;

public:
	inline static int32_t get_offset_of_localizedKey_2() { return static_cast<int32_t>(offsetof(LocalizedText_t1514653696, ___localizedKey_2)); }
	inline String_t* get_localizedKey_2() const { return ___localizedKey_2; }
	inline String_t** get_address_of_localizedKey_2() { return &___localizedKey_2; }
	inline void set_localizedKey_2(String_t* value)
	{
		___localizedKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___localizedKey_2, value);
	}

	inline static int32_t get_offset_of_textObject_3() { return static_cast<int32_t>(offsetof(LocalizedText_t1514653696, ___textObject_3)); }
	inline Text_t356221433 * get_textObject_3() const { return ___textObject_3; }
	inline Text_t356221433 ** get_address_of_textObject_3() { return &___textObject_3; }
	inline void set_textObject_3(Text_t356221433 * value)
	{
		___textObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___textObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
