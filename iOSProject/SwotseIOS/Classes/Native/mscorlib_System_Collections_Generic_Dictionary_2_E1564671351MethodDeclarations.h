﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3108404182(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1564671351 *, Dictionary_2_t244646649 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2658696023(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1194950343(__this, method) ((  void (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3421513814(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3576160157(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3509604221(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::MoveNext()
#define Enumerator_MoveNext_m3089698347(__this, method) ((  bool (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::get_Current()
#define Enumerator_get_Current_m874299548(__this, method) ((  KeyValuePair_2_t2296959167  (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1059945558(__this, method) ((  int32_t (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m825853046(__this, method) ((  TuioObject_t1236821014 * (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::Reset()
#define Enumerator_Reset_m3333369888(__this, method) ((  void (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::VerifyState()
#define Enumerator_VerifyState_m2594324365(__this, method) ((  void (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2206713999(__this, method) ((  void (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::Dispose()
#define Enumerator_Dispose_m4106138594(__this, method) ((  void (*) (Enumerator_t1564671351 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
