﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel>
struct Dictionary_2_t3820150789;
// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel>
struct Dictionary_2_t164486759;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingsManager
struct  BuildingsManager_t1345368432  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityBuildings
	Dictionary_2_t3820150789 * ___cityBuildings_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,BuildingModel> BuildingsManager::cityFields
	Dictionary_2_t3820150789 * ___cityFields_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,WorldFieldModel> BuildingsManager::cityValleys
	Dictionary_2_t164486759 * ___cityValleys_2;
	// SimpleEvent BuildingsManager::onBuildingUpdate
	SimpleEvent_t1509017202 * ___onBuildingUpdate_3;
	// SimpleEvent BuildingsManager::onGotValleys
	SimpleEvent_t1509017202 * ___onGotValleys_4;
	// SimpleEvent BuildingsManager::onBuildingCancelled
	SimpleEvent_t1509017202 * ___onBuildingCancelled_5;

public:
	inline static int32_t get_offset_of_cityBuildings_0() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___cityBuildings_0)); }
	inline Dictionary_2_t3820150789 * get_cityBuildings_0() const { return ___cityBuildings_0; }
	inline Dictionary_2_t3820150789 ** get_address_of_cityBuildings_0() { return &___cityBuildings_0; }
	inline void set_cityBuildings_0(Dictionary_2_t3820150789 * value)
	{
		___cityBuildings_0 = value;
		Il2CppCodeGenWriteBarrier(&___cityBuildings_0, value);
	}

	inline static int32_t get_offset_of_cityFields_1() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___cityFields_1)); }
	inline Dictionary_2_t3820150789 * get_cityFields_1() const { return ___cityFields_1; }
	inline Dictionary_2_t3820150789 ** get_address_of_cityFields_1() { return &___cityFields_1; }
	inline void set_cityFields_1(Dictionary_2_t3820150789 * value)
	{
		___cityFields_1 = value;
		Il2CppCodeGenWriteBarrier(&___cityFields_1, value);
	}

	inline static int32_t get_offset_of_cityValleys_2() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___cityValleys_2)); }
	inline Dictionary_2_t164486759 * get_cityValleys_2() const { return ___cityValleys_2; }
	inline Dictionary_2_t164486759 ** get_address_of_cityValleys_2() { return &___cityValleys_2; }
	inline void set_cityValleys_2(Dictionary_2_t164486759 * value)
	{
		___cityValleys_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityValleys_2, value);
	}

	inline static int32_t get_offset_of_onBuildingUpdate_3() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___onBuildingUpdate_3)); }
	inline SimpleEvent_t1509017202 * get_onBuildingUpdate_3() const { return ___onBuildingUpdate_3; }
	inline SimpleEvent_t1509017202 ** get_address_of_onBuildingUpdate_3() { return &___onBuildingUpdate_3; }
	inline void set_onBuildingUpdate_3(SimpleEvent_t1509017202 * value)
	{
		___onBuildingUpdate_3 = value;
		Il2CppCodeGenWriteBarrier(&___onBuildingUpdate_3, value);
	}

	inline static int32_t get_offset_of_onGotValleys_4() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___onGotValleys_4)); }
	inline SimpleEvent_t1509017202 * get_onGotValleys_4() const { return ___onGotValleys_4; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGotValleys_4() { return &___onGotValleys_4; }
	inline void set_onGotValleys_4(SimpleEvent_t1509017202 * value)
	{
		___onGotValleys_4 = value;
		Il2CppCodeGenWriteBarrier(&___onGotValleys_4, value);
	}

	inline static int32_t get_offset_of_onBuildingCancelled_5() { return static_cast<int32_t>(offsetof(BuildingsManager_t1345368432, ___onBuildingCancelled_5)); }
	inline SimpleEvent_t1509017202 * get_onBuildingCancelled_5() const { return ___onBuildingCancelled_5; }
	inline SimpleEvent_t1509017202 ** get_address_of_onBuildingCancelled_5() { return &___onBuildingCancelled_5; }
	inline void set_onBuildingCancelled_5(SimpleEvent_t1509017202 * value)
	{
		___onBuildingCancelled_5 = value;
		Il2CppCodeGenWriteBarrier(&___onBuildingCancelled_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
