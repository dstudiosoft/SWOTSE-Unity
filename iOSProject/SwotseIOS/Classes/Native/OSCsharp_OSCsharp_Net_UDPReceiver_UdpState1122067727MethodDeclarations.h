﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Net.UDPReceiver/UdpState
struct UdpState_t1122067727;
// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_UdpClient1278197702.h"
#include "System_System_Net_IPEndPoint2615413766.h"

// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::get_Client()
extern "C"  UdpClient_t1278197702 * UdpState_get_Client_m1371175984 (UdpState_t1122067727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_Client(System.Net.Sockets.UdpClient)
extern "C"  void UdpState_set_Client_m3548438795 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UdpState_get_IPEndPoint_m1752194537 (UdpState_t1122067727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UdpState_set_IPEndPoint_m3072178698 (UdpState_t1122067727 * __this, IPEndPoint_t2615413766 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::.ctor(System.Net.Sockets.UdpClient,System.Net.IPEndPoint)
extern "C"  void UdpState__ctor_m3537245299 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___client0, IPEndPoint_t2615413766 * ___ipEndPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
