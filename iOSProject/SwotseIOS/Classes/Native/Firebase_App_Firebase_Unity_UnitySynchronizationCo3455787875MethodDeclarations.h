﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0
struct U3CSignaledCoroutineU3Ec__Iterator0_t3455787875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0__ctor_m1561982014 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m4225372014 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3687469730 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1658054906 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Dispose_m3945247383 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Reset()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Reset_m1983087673 (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
