﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1
struct  U3CIsCultureSupportedU3Ec__AnonStorey1_t611194050  : public Il2CppObject
{
public:
	// System.String SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::languageCode
	String_t* ___languageCode_0;

public:
	inline static int32_t get_offset_of_languageCode_0() { return static_cast<int32_t>(offsetof(U3CIsCultureSupportedU3Ec__AnonStorey1_t611194050, ___languageCode_0)); }
	inline String_t* get_languageCode_0() const { return ___languageCode_0; }
	inline String_t** get_address_of_languageCode_0() { return &___languageCode_0; }
	inline void set_languageCode_0(String_t* value)
	{
		___languageCode_0 = value;
		Il2CppCodeGenWriteBarrier(&___languageCode_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
