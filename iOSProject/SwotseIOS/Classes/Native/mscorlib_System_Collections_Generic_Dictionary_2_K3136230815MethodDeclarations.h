﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1235043815(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3136230815 *, Dictionary_2_t652733044 *, const MethodInfo*))KeyCollection__ctor_m40025794_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m705251673(__this, ___item0, method) ((  void (*) (KeyCollection_t3136230815 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2757911188(__this, method) ((  void (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2757724131(__this, ___item0, method) ((  bool (*) (KeyCollection_t3136230815 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381613528(__this, ___item0, method) ((  bool (*) (KeyCollection_t3136230815 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1419120496(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3394079414(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3136230815 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2291990081(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3192091778(__this, method) ((  bool (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2597796470(__this, method) ((  bool (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m459448432(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3712229748(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3136230815 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3820790603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1176492831(__this, method) ((  Enumerator_t3342236482  (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_GetEnumerator_m349130886_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.FirebaseApp>::get_Count()
#define KeyCollection_get_Count_m2533698778(__this, method) ((  int32_t (*) (KeyCollection_t3136230815 *, const MethodInfo*))KeyCollection_get_Count_m1219588529_gshared)(__this, method)
