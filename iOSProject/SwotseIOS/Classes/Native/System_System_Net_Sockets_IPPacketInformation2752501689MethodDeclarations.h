﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.IPPacketInformation
struct IPPacketInformation_t2752501689;
struct IPPacketInformation_t2752501689_marshaled_pinvoke;
struct IPPacketInformation_t2752501689_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_IPPacketInformation2752501689.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.Sockets.IPPacketInformation::.ctor(System.Net.IPAddress,System.Int32)
extern "C"  void IPPacketInformation__ctor_m3107734049 (IPPacketInformation_t2752501689 * __this, IPAddress_t1399971723 * ___address0, int32_t ___iface1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.Sockets.IPPacketInformation::get_Address()
extern "C"  IPAddress_t1399971723 * IPPacketInformation_get_Address_m3228192330 (IPPacketInformation_t2752501689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.IPPacketInformation::get_Interface()
extern "C"  int32_t IPPacketInformation_get_Interface_m1047841239 (IPPacketInformation_t2752501689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.IPPacketInformation::Equals(System.Object)
extern "C"  bool IPPacketInformation_Equals_m109905294 (IPPacketInformation_t2752501689 * __this, Il2CppObject * ___comparand0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.IPPacketInformation::GetHashCode()
extern "C"  int32_t IPPacketInformation_GetHashCode_m2028669054 (IPPacketInformation_t2752501689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.IPPacketInformation::op_Equality(System.Net.Sockets.IPPacketInformation,System.Net.Sockets.IPPacketInformation)
extern "C"  bool IPPacketInformation_op_Equality_m3964506053 (Il2CppObject * __this /* static, unused */, IPPacketInformation_t2752501689  ___p10, IPPacketInformation_t2752501689  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Sockets.IPPacketInformation::op_Inequality(System.Net.Sockets.IPPacketInformation,System.Net.Sockets.IPPacketInformation)
extern "C"  bool IPPacketInformation_op_Inequality_m3142177368 (Il2CppObject * __this /* static, unused */, IPPacketInformation_t2752501689  ___p10, IPPacketInformation_t2752501689  ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct IPPacketInformation_t2752501689;
struct IPPacketInformation_t2752501689_marshaled_pinvoke;

extern "C" void IPPacketInformation_t2752501689_marshal_pinvoke(const IPPacketInformation_t2752501689& unmarshaled, IPPacketInformation_t2752501689_marshaled_pinvoke& marshaled);
extern "C" void IPPacketInformation_t2752501689_marshal_pinvoke_back(const IPPacketInformation_t2752501689_marshaled_pinvoke& marshaled, IPPacketInformation_t2752501689& unmarshaled);
extern "C" void IPPacketInformation_t2752501689_marshal_pinvoke_cleanup(IPPacketInformation_t2752501689_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IPPacketInformation_t2752501689;
struct IPPacketInformation_t2752501689_marshaled_com;

extern "C" void IPPacketInformation_t2752501689_marshal_com(const IPPacketInformation_t2752501689& unmarshaled, IPPacketInformation_t2752501689_marshaled_com& marshaled);
extern "C" void IPPacketInformation_t2752501689_marshal_com_back(const IPPacketInformation_t2752501689_marshaled_com& marshaled, IPPacketInformation_t2752501689& unmarshaled);
extern "C" void IPPacketInformation_t2752501689_marshal_com_cleanup(IPPacketInformation_t2752501689_marshaled_com& marshaled);
