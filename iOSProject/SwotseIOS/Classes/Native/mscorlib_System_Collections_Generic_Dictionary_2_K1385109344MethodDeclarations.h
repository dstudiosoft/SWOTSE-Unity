﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1234468986(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1385109344 *, Dictionary_2_t3196578869 *, const MethodInfo*))KeyCollection__ctor_m3555561037_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3397590688(__this, ___item0, method) ((  void (*) (KeyCollection_t1385109344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m837626793(__this, method) ((  void (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3375420460(__this, ___item0, method) ((  bool (*) (KeyCollection_t1385109344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m802692973(__this, ___item0, method) ((  bool (*) (KeyCollection_t1385109344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m775330963(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m439866395(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1385109344 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2455365412(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1785530849(__this, method) ((  bool (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m358785963(__this, method) ((  bool (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4111845063(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1650562909(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1385109344 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4056130090_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3037780240(__this, method) ((  Enumerator_t1591115011  (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_GetEnumerator_m627495629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>::get_Count()
#define KeyCollection_get_Count_m3290923027(__this, method) ((  int32_t (*) (KeyCollection_t1385109344 *, const MethodInfo*))KeyCollection_get_Count_m2942903136_gshared)(__this, method)
