﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationWindowManager
struct NotificationWindowManager_t257729770;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NotificationWindowManager::.ctor()
extern "C"  void NotificationWindowManager__ctor_m4139791265 (NotificationWindowManager_t257729770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationWindowManager::SetNotificationMessage(System.String)
extern "C"  void NotificationWindowManager_SetNotificationMessage_m3302381355 (NotificationWindowManager_t257729770 * __this, String_t* ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
