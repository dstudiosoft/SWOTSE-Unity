﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailAllianceReportTableLine
struct MailAllianceReportTableLine_t3318082430;
// MailAllianceReportTableController
struct MailAllianceReportTableController_t518573504;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MailAllianceReportTableController518573504.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailAllianceReportTableLine::.ctor()
extern "C"  void MailAllianceReportTableLine__ctor_m309139981 (MailAllianceReportTableLine_t3318082430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetOwner(MailAllianceReportTableController)
extern "C"  void MailAllianceReportTableLine_SetOwner_m567756136 (MailAllianceReportTableLine_t3318082430 * __this, MailAllianceReportTableController_t518573504 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetId(System.Int64)
extern "C"  void MailAllianceReportTableLine_SetId_m3812290636 (MailAllianceReportTableLine_t3318082430 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetUserName(System.String)
extern "C"  void MailAllianceReportTableLine_SetUserName_m1412571103 (MailAllianceReportTableLine_t3318082430 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetType(System.String)
extern "C"  void MailAllianceReportTableLine_SetType_m3477576341 (MailAllianceReportTableLine_t3318082430 * __this, String_t* ___typeValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetSource(System.String)
extern "C"  void MailAllianceReportTableLine_SetSource_m1684886570 (MailAllianceReportTableLine_t3318082430 * __this, String_t* ___sourceValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetTarget(System.String)
extern "C"  void MailAllianceReportTableLine_SetTarget_m3960645730 (MailAllianceReportTableLine_t3318082430 * __this, String_t* ___targetValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::SetTime(System.String)
extern "C"  void MailAllianceReportTableLine_SetTime_m2241471842 (MailAllianceReportTableLine_t3318082430 * __this, String_t* ___timeValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::ShowReport()
extern "C"  void MailAllianceReportTableLine_ShowReport_m3764054434 (MailAllianceReportTableLine_t3318082430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailAllianceReportTableLine::DeleteReport()
extern "C"  void MailAllianceReportTableLine_DeleteReport_m1756940920 (MailAllianceReportTableLine_t3318082430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
