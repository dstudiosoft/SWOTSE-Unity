﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2391338403(__this, ___l0, method) ((  void (*) (Enumerator_t548886295 *, List_1_t1014156621 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3622728231(__this, method) ((  void (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2366477815(__this, method) ((  Il2CppObject * (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::Dispose()
#define Enumerator_Dispose_m4077794540(__this, method) ((  void (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::VerifyState()
#define Enumerator_VerifyState_m1732556941(__this, method) ((  void (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::MoveNext()
#define Enumerator_MoveNext_m3897385155(__this, method) ((  bool (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Threading.Tasks.Task>>::get_Current()
#define Enumerator_get_Current_m2041565632(__this, method) ((  Action_1_t1645035489 * (*) (Enumerator_t548886295 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
