﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.CanvasProjectionParams
struct CanvasProjectionParams_t880996221;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

// System.Void TouchScript.Layers.CanvasProjectionParams::.ctor(UnityEngine.Canvas)
extern "C"  void CanvasProjectionParams__ctor_m704464063 (CanvasProjectionParams_t880996221 * __this, Canvas_t209405766 * ___canvas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.CanvasProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern "C"  Vector3_t2243707580  CanvasProjectionParams_ProjectTo_m638715969 (CanvasProjectionParams_t880996221 * __this, Vector2_t2243707579  ___screenPosition0, Plane_t3727654732  ___projectionPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Layers.CanvasProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  CanvasProjectionParams_ProjectFrom_m440657309 (CanvasProjectionParams_t880996221 * __this, Vector3_t2243707580  ___worldPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
