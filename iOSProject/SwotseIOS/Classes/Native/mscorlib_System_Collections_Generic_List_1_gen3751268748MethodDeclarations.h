﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t3751268748;
// System.Collections.Generic.IEnumerable`1<UnityEngine.RaycastHit>
struct IEnumerable_1_t379307365;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1857671443;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.RaycastHit>
struct ICollection_1_t1039255625;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>
struct ReadOnlyCollection_1_t272966012;
// System.Predicate`1<UnityEngine.RaycastHit>
struct Predicate_1_t2825117731;
// System.Action`1<UnityEngine.RaycastHit>
struct Action_1_t4183946998;
// System.Collections.Generic.IComparer`1<UnityEngine.RaycastHit>
struct IComparer_1_t2336610738;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t1348919171;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3285998422.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor()
extern "C"  void List_1__ctor_m638629251_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1__ctor_m638629251(__this, method) ((  void (*) (List_1_t3751268748 *, const MethodInfo*))List_1__ctor_m638629251_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1601042910_gshared (List_1_t3751268748 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1601042910(__this, ___collection0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1601042910_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m350053921_gshared (List_1_t3751268748 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m350053921(__this, ___capacity0, method) ((  void (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1__ctor_m350053921_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2280526512_gshared (List_1_t3751268748 * __this, RaycastHitU5BU5D_t1214023521* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2280526512(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3751268748 *, RaycastHitU5BU5D_t1214023521*, int32_t, const MethodInfo*))List_1__ctor_m2280526512_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.cctor()
extern "C"  void List_1__cctor_m256611188_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m256611188(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m256611188_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m435241403_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m435241403(__this, method) ((  Il2CppObject* (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m435241403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4286730591_gshared (List_1_t3751268748 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m4286730591(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3751268748 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4286730591_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1195533558_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1195533558(__this, method) ((  Il2CppObject * (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1195533558_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2331525023_gshared (List_1_t3751268748 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2331525023(__this, ___item0, method) ((  int32_t (*) (List_1_t3751268748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2331525023_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1011070647_gshared (List_1_t3751268748 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1011070647(__this, ___item0, method) ((  bool (*) (List_1_t3751268748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1011070647_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3554865677_gshared (List_1_t3751268748 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3554865677(__this, ___item0, method) ((  int32_t (*) (List_1_t3751268748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3554865677_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4087766092_gshared (List_1_t3751268748 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m4087766092(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3751268748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m4087766092_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m161370178_gshared (List_1_t3751268748 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m161370178(__this, ___item0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m161370178_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1732710418_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1732710418(__this, method) ((  bool (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1732710418_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2748161143_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2748161143(__this, method) ((  bool (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2748161143_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2960814419_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2960814419(__this, method) ((  Il2CppObject * (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2960814419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m718025400_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m718025400(__this, method) ((  bool (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m718025400_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1732763835_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1732763835(__this, method) ((  bool (*) (List_1_t3751268748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1732763835_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1620041654_gshared (List_1_t3751268748 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1620041654(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1620041654_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m809029089_gshared (List_1_t3751268748 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m809029089(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3751268748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m809029089_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Add(T)
extern "C"  void List_1_Add_m2532025784_gshared (List_1_t3751268748 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define List_1_Add_m2532025784(__this, ___item0, method) ((  void (*) (List_1_t3751268748 *, RaycastHit_t87180320 , const MethodInfo*))List_1_Add_m2532025784_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2437380979_gshared (List_1_t3751268748 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2437380979(__this, ___newCount0, method) ((  void (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2437380979_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1609040787_gshared (List_1_t3751268748 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1609040787(__this, ___collection0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1609040787_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2684984611_gshared (List_1_t3751268748 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2684984611(__this, ___enumerable0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2684984611_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3215121890_gshared (List_1_t3751268748 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3215121890(__this, ___collection0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3215121890_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t272966012 * List_1_AsReadOnly_m1404582835_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1404582835(__this, method) ((  ReadOnlyCollection_1_t272966012 * (*) (List_1_t3751268748 *, const MethodInfo*))List_1_AsReadOnly_m1404582835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Clear()
extern "C"  void List_1_Clear_m3448501953_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_Clear_m3448501953(__this, method) ((  void (*) (List_1_t3751268748 *, const MethodInfo*))List_1_Clear_m3448501953_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool List_1_Contains_m4198637106_gshared (List_1_t3751268748 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define List_1_Contains_m4198637106(__this, ___item0, method) ((  bool (*) (List_1_t3751268748 *, RaycastHit_t87180320 , const MethodInfo*))List_1_Contains_m4198637106_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4087324692_gshared (List_1_t3751268748 * __this, RaycastHitU5BU5D_t1214023521* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4087324692(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3751268748 *, RaycastHitU5BU5D_t1214023521*, int32_t, const MethodInfo*))List_1_CopyTo_m4087324692_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Find(System.Predicate`1<T>)
extern "C"  RaycastHit_t87180320  List_1_Find_m3014885870_gshared (List_1_t3751268748 * __this, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_Find_m3014885870(__this, ___match0, method) ((  RaycastHit_t87180320  (*) (List_1_t3751268748 *, Predicate_1_t2825117731 *, const MethodInfo*))List_1_Find_m3014885870_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3145978951_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3145978951(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2825117731 *, const MethodInfo*))List_1_CheckMatch_m3145978951_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3751268748 * List_1_FindAll_m177001543_gshared (List_1_t3751268748 * __this, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m177001543(__this, ___match0, method) ((  List_1_t3751268748 * (*) (List_1_t3751268748 *, Predicate_1_t2825117731 *, const MethodInfo*))List_1_FindAll_m177001543_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3751268748 * List_1_FindAllStackBits_m2992918105_gshared (List_1_t3751268748 * __this, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m2992918105(__this, ___match0, method) ((  List_1_t3751268748 * (*) (List_1_t3751268748 *, Predicate_1_t2825117731 *, const MethodInfo*))List_1_FindAllStackBits_m2992918105_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3751268748 * List_1_FindAllList_m2141489961_gshared (List_1_t3751268748 * __this, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2141489961(__this, ___match0, method) ((  List_1_t3751268748 * (*) (List_1_t3751268748 *, Predicate_1_t2825117731 *, const MethodInfo*))List_1_FindAllList_m2141489961_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2837339064_gshared (List_1_t3751268748 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2825117731 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2837339064(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3751268748 *, int32_t, int32_t, Predicate_1_t2825117731 *, const MethodInfo*))List_1_GetIndex_m2837339064_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3659308993_gshared (List_1_t3751268748 * __this, Action_1_t4183946998 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3659308993(__this, ___action0, method) ((  void (*) (List_1_t3751268748 *, Action_1_t4183946998 *, const MethodInfo*))List_1_ForEach_m3659308993_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Enumerator_t3285998422  List_1_GetEnumerator_m3887574931_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3887574931(__this, method) ((  Enumerator_t3285998422  (*) (List_1_t3751268748 *, const MethodInfo*))List_1_GetEnumerator_m3887574931_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3081638930_gshared (List_1_t3751268748 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3081638930(__this, ___item0, method) ((  int32_t (*) (List_1_t3751268748 *, RaycastHit_t87180320 , const MethodInfo*))List_1_IndexOf_m3081638930_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4230522371_gshared (List_1_t3751268748 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4230522371(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3751268748 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4230522371_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4132922068_gshared (List_1_t3751268748 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4132922068(__this, ___index0, method) ((  void (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4132922068_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2817814629_gshared (List_1_t3751268748 * __this, int32_t ___index0, RaycastHit_t87180320  ___item1, const MethodInfo* method);
#define List_1_Insert_m2817814629(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3751268748 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))List_1_Insert_m2817814629_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2217629742_gshared (List_1_t3751268748 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2217629742(__this, ___collection0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2217629742_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Remove(T)
extern "C"  bool List_1_Remove_m1359071349_gshared (List_1_t3751268748 * __this, RaycastHit_t87180320  ___item0, const MethodInfo* method);
#define List_1_Remove_m1359071349(__this, ___item0, method) ((  bool (*) (List_1_t3751268748 *, RaycastHit_t87180320 , const MethodInfo*))List_1_Remove_m1359071349_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1129888959_gshared (List_1_t3751268748 * __this, Predicate_1_t2825117731 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1129888959(__this, ___match0, method) ((  int32_t (*) (List_1_t3751268748 *, Predicate_1_t2825117731 *, const MethodInfo*))List_1_RemoveAll_m1129888959_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m915716649_gshared (List_1_t3751268748 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m915716649(__this, ___index0, method) ((  void (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_RemoveAt_m915716649_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Reverse()
extern "C"  void List_1_Reverse_m2395273211_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_Reverse_m2395273211(__this, method) ((  void (*) (List_1_t3751268748 *, const MethodInfo*))List_1_Reverse_m2395273211_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort()
extern "C"  void List_1_Sort_m2508297821_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_Sort_m2508297821(__this, method) ((  void (*) (List_1_t3751268748 *, const MethodInfo*))List_1_Sort_m2508297821_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3952392113_gshared (List_1_t3751268748 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3952392113(__this, ___comparer0, method) ((  void (*) (List_1_t3751268748 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3952392113_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2244241858_gshared (List_1_t3751268748 * __this, Comparison_1_t1348919171 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2244241858(__this, ___comparison0, method) ((  void (*) (List_1_t3751268748 *, Comparison_1_t1348919171 *, const MethodInfo*))List_1_Sort_m2244241858_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.RaycastHit>::ToArray()
extern "C"  RaycastHitU5BU5D_t1214023521* List_1_ToArray_m431472722_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_ToArray_m431472722(__this, method) ((  RaycastHitU5BU5D_t1214023521* (*) (List_1_t3751268748 *, const MethodInfo*))List_1_ToArray_m431472722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3999404294_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3999404294(__this, method) ((  void (*) (List_1_t3751268748 *, const MethodInfo*))List_1_TrimExcess_m3999404294_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1495647868_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1495647868(__this, method) ((  int32_t (*) (List_1_t3751268748 *, const MethodInfo*))List_1_get_Capacity_m1495647868_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3191972307_gshared (List_1_t3751268748 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3191972307(__this, ___value0, method) ((  void (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3191972307_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t List_1_get_Count_m2023405546_gshared (List_1_t3751268748 * __this, const MethodInfo* method);
#define List_1_get_Count_m2023405546(__this, method) ((  int32_t (*) (List_1_t3751268748 *, const MethodInfo*))List_1_get_Count_m2023405546_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t87180320  List_1_get_Item_m3013374895_gshared (List_1_t3751268748 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3013374895(__this, ___index0, method) ((  RaycastHit_t87180320  (*) (List_1_t3751268748 *, int32_t, const MethodInfo*))List_1_get_Item_m3013374895_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2157850870_gshared (List_1_t3751268748 * __this, int32_t ___index0, RaycastHit_t87180320  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2157850870(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3751268748 *, int32_t, RaycastHit_t87180320 , const MethodInfo*))List_1_set_Item_m2157850870_gshared)(__this, ___index0, ___value1, method)
