﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4132278975(__this, ___host0, method) ((  void (*) (Enumerator_t2062567554 *, Dictionary_2_t376034790 *, const MethodInfo*))Enumerator__ctor_m419014865_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m925738830(__this, method) ((  Il2CppObject * (*) (Enumerator_t2062567554 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2284072592(__this, method) ((  void (*) (Enumerator_t2062567554 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::Dispose()
#define Enumerator_Dispose_m71650107(__this, method) ((  void (*) (Enumerator_t2062567554 *, const MethodInfo*))Enumerator_Dispose_m942415041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::MoveNext()
#define Enumerator_MoveNext_m3872536832(__this, method) ((  bool (*) (Enumerator_t2062567554 *, const MethodInfo*))Enumerator_MoveNext_m2045321910_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,InviteReportModel>::get_Current()
#define Enumerator_get_Current_m3270879686(__this, method) ((  InviteReportModel_t3681483684 * (*) (Enumerator_t2062567554 *, const MethodInfo*))Enumerator_get_Current_m1952080304_gshared)(__this, method)
