﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t549488518;
// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t18001273;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern "C"  void SWIGStringHelper__cctor_m3676208588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1408369181 (SWIGStringHelper_t549488518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t18001273 * ___stringDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m687016595 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m687016595(char* ___message0);
