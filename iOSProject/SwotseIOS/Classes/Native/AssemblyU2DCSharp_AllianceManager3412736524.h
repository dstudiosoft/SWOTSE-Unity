﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AllianceModel
struct AllianceModel_t1834891198;
// System.Collections.Generic.Dictionary`2<System.Int64,UserModel>
struct Dictionary_2_t4015087618;
// System.Collections.Generic.Dictionary`2<System.Int64,AllianceModel>
struct Dictionary_2_t2824409600;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceManager
struct  AllianceManager_t3412736524  : public Il2CppObject
{
public:
	// AllianceModel AllianceManager::userAlliance
	AllianceModel_t1834891198 * ___userAlliance_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,UserModel> AllianceManager::allianceMembers
	Dictionary_2_t4015087618 * ___allianceMembers_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,AllianceModel> AllianceManager::allAlliances
	Dictionary_2_t2824409600 * ___allAlliances_2;
	// System.String AllianceManager::dateFormat
	String_t* ___dateFormat_3;
	// SimpleEvent AllianceManager::onGetAllianceMembers
	SimpleEvent_t1509017202 * ___onGetAllianceMembers_4;
	// SimpleEvent AllianceManager::onGetAlliances
	SimpleEvent_t1509017202 * ___onGetAlliances_5;
	// SimpleEvent AllianceManager::onExcludeUser
	SimpleEvent_t1509017202 * ___onExcludeUser_6;
	// SimpleEvent AllianceManager::onInviteUser
	SimpleEvent_t1509017202 * ___onInviteUser_7;
	// SimpleEvent AllianceManager::onRequestJoin
	SimpleEvent_t1509017202 * ___onRequestJoin_8;
	// SimpleEvent AllianceManager::onQuitAlliance
	SimpleEvent_t1509017202 * ___onQuitAlliance_9;
	// SimpleEvent AllianceManager::onUpdateGuideline
	SimpleEvent_t1509017202 * ___onUpdateGuideline_10;
	// SimpleEvent AllianceManager::onAcceptInvite
	SimpleEvent_t1509017202 * ___onAcceptInvite_11;
	// SimpleEvent AllianceManager::onCancelInvite
	SimpleEvent_t1509017202 * ___onCancelInvite_12;
	// SimpleEvent AllianceManager::onAddAlly
	SimpleEvent_t1509017202 * ___onAddAlly_13;
	// SimpleEvent AllianceManager::onAddEnemy
	SimpleEvent_t1509017202 * ___onAddEnemy_14;
	// SimpleEvent AllianceManager::onRemoveAlly
	SimpleEvent_t1509017202 * ___onRemoveAlly_15;
	// SimpleEvent AllianceManager::onRemoveEnemy
	SimpleEvent_t1509017202 * ___onRemoveEnemy_16;
	// SimpleEvent AllianceManager::onCreateNewAlliance
	SimpleEvent_t1509017202 * ___onCreateNewAlliance_17;

public:
	inline static int32_t get_offset_of_userAlliance_0() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___userAlliance_0)); }
	inline AllianceModel_t1834891198 * get_userAlliance_0() const { return ___userAlliance_0; }
	inline AllianceModel_t1834891198 ** get_address_of_userAlliance_0() { return &___userAlliance_0; }
	inline void set_userAlliance_0(AllianceModel_t1834891198 * value)
	{
		___userAlliance_0 = value;
		Il2CppCodeGenWriteBarrier(&___userAlliance_0, value);
	}

	inline static int32_t get_offset_of_allianceMembers_1() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___allianceMembers_1)); }
	inline Dictionary_2_t4015087618 * get_allianceMembers_1() const { return ___allianceMembers_1; }
	inline Dictionary_2_t4015087618 ** get_address_of_allianceMembers_1() { return &___allianceMembers_1; }
	inline void set_allianceMembers_1(Dictionary_2_t4015087618 * value)
	{
		___allianceMembers_1 = value;
		Il2CppCodeGenWriteBarrier(&___allianceMembers_1, value);
	}

	inline static int32_t get_offset_of_allAlliances_2() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___allAlliances_2)); }
	inline Dictionary_2_t2824409600 * get_allAlliances_2() const { return ___allAlliances_2; }
	inline Dictionary_2_t2824409600 ** get_address_of_allAlliances_2() { return &___allAlliances_2; }
	inline void set_allAlliances_2(Dictionary_2_t2824409600 * value)
	{
		___allAlliances_2 = value;
		Il2CppCodeGenWriteBarrier(&___allAlliances_2, value);
	}

	inline static int32_t get_offset_of_dateFormat_3() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___dateFormat_3)); }
	inline String_t* get_dateFormat_3() const { return ___dateFormat_3; }
	inline String_t** get_address_of_dateFormat_3() { return &___dateFormat_3; }
	inline void set_dateFormat_3(String_t* value)
	{
		___dateFormat_3 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_3, value);
	}

	inline static int32_t get_offset_of_onGetAllianceMembers_4() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onGetAllianceMembers_4)); }
	inline SimpleEvent_t1509017202 * get_onGetAllianceMembers_4() const { return ___onGetAllianceMembers_4; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetAllianceMembers_4() { return &___onGetAllianceMembers_4; }
	inline void set_onGetAllianceMembers_4(SimpleEvent_t1509017202 * value)
	{
		___onGetAllianceMembers_4 = value;
		Il2CppCodeGenWriteBarrier(&___onGetAllianceMembers_4, value);
	}

	inline static int32_t get_offset_of_onGetAlliances_5() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onGetAlliances_5)); }
	inline SimpleEvent_t1509017202 * get_onGetAlliances_5() const { return ___onGetAlliances_5; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetAlliances_5() { return &___onGetAlliances_5; }
	inline void set_onGetAlliances_5(SimpleEvent_t1509017202 * value)
	{
		___onGetAlliances_5 = value;
		Il2CppCodeGenWriteBarrier(&___onGetAlliances_5, value);
	}

	inline static int32_t get_offset_of_onExcludeUser_6() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onExcludeUser_6)); }
	inline SimpleEvent_t1509017202 * get_onExcludeUser_6() const { return ___onExcludeUser_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onExcludeUser_6() { return &___onExcludeUser_6; }
	inline void set_onExcludeUser_6(SimpleEvent_t1509017202 * value)
	{
		___onExcludeUser_6 = value;
		Il2CppCodeGenWriteBarrier(&___onExcludeUser_6, value);
	}

	inline static int32_t get_offset_of_onInviteUser_7() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onInviteUser_7)); }
	inline SimpleEvent_t1509017202 * get_onInviteUser_7() const { return ___onInviteUser_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onInviteUser_7() { return &___onInviteUser_7; }
	inline void set_onInviteUser_7(SimpleEvent_t1509017202 * value)
	{
		___onInviteUser_7 = value;
		Il2CppCodeGenWriteBarrier(&___onInviteUser_7, value);
	}

	inline static int32_t get_offset_of_onRequestJoin_8() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onRequestJoin_8)); }
	inline SimpleEvent_t1509017202 * get_onRequestJoin_8() const { return ___onRequestJoin_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onRequestJoin_8() { return &___onRequestJoin_8; }
	inline void set_onRequestJoin_8(SimpleEvent_t1509017202 * value)
	{
		___onRequestJoin_8 = value;
		Il2CppCodeGenWriteBarrier(&___onRequestJoin_8, value);
	}

	inline static int32_t get_offset_of_onQuitAlliance_9() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onQuitAlliance_9)); }
	inline SimpleEvent_t1509017202 * get_onQuitAlliance_9() const { return ___onQuitAlliance_9; }
	inline SimpleEvent_t1509017202 ** get_address_of_onQuitAlliance_9() { return &___onQuitAlliance_9; }
	inline void set_onQuitAlliance_9(SimpleEvent_t1509017202 * value)
	{
		___onQuitAlliance_9 = value;
		Il2CppCodeGenWriteBarrier(&___onQuitAlliance_9, value);
	}

	inline static int32_t get_offset_of_onUpdateGuideline_10() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onUpdateGuideline_10)); }
	inline SimpleEvent_t1509017202 * get_onUpdateGuideline_10() const { return ___onUpdateGuideline_10; }
	inline SimpleEvent_t1509017202 ** get_address_of_onUpdateGuideline_10() { return &___onUpdateGuideline_10; }
	inline void set_onUpdateGuideline_10(SimpleEvent_t1509017202 * value)
	{
		___onUpdateGuideline_10 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateGuideline_10, value);
	}

	inline static int32_t get_offset_of_onAcceptInvite_11() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onAcceptInvite_11)); }
	inline SimpleEvent_t1509017202 * get_onAcceptInvite_11() const { return ___onAcceptInvite_11; }
	inline SimpleEvent_t1509017202 ** get_address_of_onAcceptInvite_11() { return &___onAcceptInvite_11; }
	inline void set_onAcceptInvite_11(SimpleEvent_t1509017202 * value)
	{
		___onAcceptInvite_11 = value;
		Il2CppCodeGenWriteBarrier(&___onAcceptInvite_11, value);
	}

	inline static int32_t get_offset_of_onCancelInvite_12() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onCancelInvite_12)); }
	inline SimpleEvent_t1509017202 * get_onCancelInvite_12() const { return ___onCancelInvite_12; }
	inline SimpleEvent_t1509017202 ** get_address_of_onCancelInvite_12() { return &___onCancelInvite_12; }
	inline void set_onCancelInvite_12(SimpleEvent_t1509017202 * value)
	{
		___onCancelInvite_12 = value;
		Il2CppCodeGenWriteBarrier(&___onCancelInvite_12, value);
	}

	inline static int32_t get_offset_of_onAddAlly_13() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onAddAlly_13)); }
	inline SimpleEvent_t1509017202 * get_onAddAlly_13() const { return ___onAddAlly_13; }
	inline SimpleEvent_t1509017202 ** get_address_of_onAddAlly_13() { return &___onAddAlly_13; }
	inline void set_onAddAlly_13(SimpleEvent_t1509017202 * value)
	{
		___onAddAlly_13 = value;
		Il2CppCodeGenWriteBarrier(&___onAddAlly_13, value);
	}

	inline static int32_t get_offset_of_onAddEnemy_14() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onAddEnemy_14)); }
	inline SimpleEvent_t1509017202 * get_onAddEnemy_14() const { return ___onAddEnemy_14; }
	inline SimpleEvent_t1509017202 ** get_address_of_onAddEnemy_14() { return &___onAddEnemy_14; }
	inline void set_onAddEnemy_14(SimpleEvent_t1509017202 * value)
	{
		___onAddEnemy_14 = value;
		Il2CppCodeGenWriteBarrier(&___onAddEnemy_14, value);
	}

	inline static int32_t get_offset_of_onRemoveAlly_15() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onRemoveAlly_15)); }
	inline SimpleEvent_t1509017202 * get_onRemoveAlly_15() const { return ___onRemoveAlly_15; }
	inline SimpleEvent_t1509017202 ** get_address_of_onRemoveAlly_15() { return &___onRemoveAlly_15; }
	inline void set_onRemoveAlly_15(SimpleEvent_t1509017202 * value)
	{
		___onRemoveAlly_15 = value;
		Il2CppCodeGenWriteBarrier(&___onRemoveAlly_15, value);
	}

	inline static int32_t get_offset_of_onRemoveEnemy_16() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onRemoveEnemy_16)); }
	inline SimpleEvent_t1509017202 * get_onRemoveEnemy_16() const { return ___onRemoveEnemy_16; }
	inline SimpleEvent_t1509017202 ** get_address_of_onRemoveEnemy_16() { return &___onRemoveEnemy_16; }
	inline void set_onRemoveEnemy_16(SimpleEvent_t1509017202 * value)
	{
		___onRemoveEnemy_16 = value;
		Il2CppCodeGenWriteBarrier(&___onRemoveEnemy_16, value);
	}

	inline static int32_t get_offset_of_onCreateNewAlliance_17() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524, ___onCreateNewAlliance_17)); }
	inline SimpleEvent_t1509017202 * get_onCreateNewAlliance_17() const { return ___onCreateNewAlliance_17; }
	inline SimpleEvent_t1509017202 ** get_address_of_onCreateNewAlliance_17() { return &___onCreateNewAlliance_17; }
	inline void set_onCreateNewAlliance_17(SimpleEvent_t1509017202 * value)
	{
		___onCreateNewAlliance_17 = value;
		Il2CppCodeGenWriteBarrier(&___onCreateNewAlliance_17, value);
	}
};

struct AllianceManager_t3412736524_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AllianceManager::<>f__switch$map19
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map19_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_18() { return static_cast<int32_t>(offsetof(AllianceManager_t3412736524_StaticFields, ___U3CU3Ef__switchU24map19_18)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map19_18() const { return ___U3CU3Ef__switchU24map19_18; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map19_18() { return &___U3CU3Ef__switchU24map19_18; }
	inline void set_U3CU3Ef__switchU24map19_18(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map19_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map19_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
