﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.UnixIPInterfaceProperties
struct UnixIPInterfaceProperties_t2418091637;
// System.Net.NetworkInformation.UnixNetworkInterface
struct UnixNetworkInterface_t1000704527;
// System.Collections.Generic.List`1<System.Net.IPAddress>
struct List_1_t769092855;
// System.Net.NetworkInformation.IPv6InterfaceProperties
struct IPv6InterfaceProperties_t1930412363;
// System.String
struct String_t;
// System.Net.NetworkInformation.IPAddressInformationCollection
struct IPAddressInformationCollection_t3688726299;
// System.Net.NetworkInformation.IPAddressCollection
struct IPAddressCollection_t2986660307;
// System.Net.NetworkInformation.GatewayIPAddressInformationCollection
struct GatewayIPAddressInformationCollection_t3537717171;
// System.Net.NetworkInformation.MulticastIPAddressInformationCollection
struct MulticastIPAddressInformationCollection_t2350102231;
// System.Net.NetworkInformation.UnicastIPAddressInformationCollection
struct UnicastIPAddressInformationCollection_t347163204;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.NetworkInformation.UnixIPInterfaceProperties::.ctor(System.Net.NetworkInformation.UnixNetworkInterface,System.Collections.Generic.List`1<System.Net.IPAddress>)
extern "C"  void UnixIPInterfaceProperties__ctor_m2539088987 (UnixIPInterfaceProperties_t2418091637 * __this, UnixNetworkInterface_t1000704527 * ___iface0, List_1_t769092855 * ___addresses1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.UnixIPInterfaceProperties::.cctor()
extern "C"  void UnixIPInterfaceProperties__cctor_m2472170512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPv6InterfaceProperties System.Net.NetworkInformation.UnixIPInterfaceProperties::GetIPv6Properties()
extern "C"  IPv6InterfaceProperties_t1930412363 * UnixIPInterfaceProperties_GetIPv6Properties_m2980625829 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.UnixIPInterfaceProperties::ParseRouteInfo(System.String)
extern "C"  void UnixIPInterfaceProperties_ParseRouteInfo_m3187707635 (UnixIPInterfaceProperties_t2418091637 * __this, String_t* ___iface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.UnixIPInterfaceProperties::ParseResolvConf()
extern "C"  void UnixIPInterfaceProperties_ParseResolvConf_m1769674119 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressInformationCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_AnycastAddresses()
extern "C"  IPAddressInformationCollection_t3688726299 * UnixIPInterfaceProperties_get_AnycastAddresses_m1185663977 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_DhcpServerAddresses()
extern "C"  IPAddressCollection_t2986660307 * UnixIPInterfaceProperties_get_DhcpServerAddresses_m3828064994 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_DnsAddresses()
extern "C"  IPAddressCollection_t2986660307 * UnixIPInterfaceProperties_get_DnsAddresses_m2661625179 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.NetworkInformation.UnixIPInterfaceProperties::get_DnsSuffix()
extern "C"  String_t* UnixIPInterfaceProperties_get_DnsSuffix_m399310525 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.GatewayIPAddressInformationCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_GatewayAddresses()
extern "C"  GatewayIPAddressInformationCollection_t3537717171 * UnixIPInterfaceProperties_get_GatewayAddresses_m2871647616 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPInterfaceProperties::get_IsDnsEnabled()
extern "C"  bool UnixIPInterfaceProperties_get_IsDnsEnabled_m1187034728 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.UnixIPInterfaceProperties::get_IsDynamicDnsEnabled()
extern "C"  bool UnixIPInterfaceProperties_get_IsDynamicDnsEnabled_m804247561 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.MulticastIPAddressInformationCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_MulticastAddresses()
extern "C"  MulticastIPAddressInformationCollection_t2350102231 * UnixIPInterfaceProperties_get_MulticastAddresses_m3342849016 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.UnicastIPAddressInformationCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_UnicastAddresses()
extern "C"  UnicastIPAddressInformationCollection_t347163204 * UnixIPInterfaceProperties_get_UnicastAddresses_m3939955578 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPAddressCollection System.Net.NetworkInformation.UnixIPInterfaceProperties::get_WinsServersAddresses()
extern "C"  IPAddressCollection_t2986660307 * UnixIPInterfaceProperties_get_WinsServersAddresses_m3441214819 (UnixIPInterfaceProperties_t2418091637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
