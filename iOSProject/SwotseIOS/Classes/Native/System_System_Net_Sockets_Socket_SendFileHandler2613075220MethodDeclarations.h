﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.Socket/SendFileHandler
struct SendFileHandler_t2613075220;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Net_Sockets_TransmitFileOptions994250290.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.Sockets.Socket/SendFileHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SendFileHandler__ctor_m1119446332 (SendFileHandler_t2613075220 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket/SendFileHandler::Invoke(System.String,System.Byte[],System.Byte[],System.Net.Sockets.TransmitFileOptions)
extern "C"  void SendFileHandler_Invoke_m1629279524 (SendFileHandler_t2613075220 * __this, String_t* ___fileName0, ByteU5BU5D_t3397334013* ___preBuffer1, ByteU5BU5D_t3397334013* ___postBuffer2, int32_t ___flags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.Socket/SendFileHandler::BeginInvoke(System.String,System.Byte[],System.Byte[],System.Net.Sockets.TransmitFileOptions,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SendFileHandler_BeginInvoke_m41704615 (SendFileHandler_t2613075220 * __this, String_t* ___fileName0, ByteU5BU5D_t3397334013* ___preBuffer1, ByteU5BU5D_t3397334013* ___postBuffer2, int32_t ___flags3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket/SendFileHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SendFileHandler_EndInvoke_m1106165766 (SendFileHandler_t2613075220 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
