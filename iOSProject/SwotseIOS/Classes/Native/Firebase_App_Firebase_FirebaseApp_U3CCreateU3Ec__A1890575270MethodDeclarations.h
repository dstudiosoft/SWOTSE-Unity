﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/<Create>c__AnonStorey1
struct U3CCreateU3Ec__AnonStorey1_t1890575270;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.FirebaseApp/<Create>c__AnonStorey1::.ctor()
extern "C"  void U3CCreateU3Ec__AnonStorey1__ctor_m2714155873 (U3CCreateU3Ec__AnonStorey1_t1890575270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.FirebaseApp Firebase.FirebaseApp/<Create>c__AnonStorey1::<>m__0()
extern "C"  FirebaseApp_t210707726 * U3CCreateU3Ec__AnonStorey1_U3CU3Em__0_m2997562906 (U3CCreateU3Ec__AnonStorey1_t1890575270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
