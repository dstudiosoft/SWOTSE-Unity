﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// ResidenceBuildingsTableController
struct ResidenceBuildingsTableController_t3217399565;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceBuildingsTableLine
struct  ResidenceBuildingsTableLine_t325800007  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Image ResidenceBuildingsTableLine::buildingIcon
	Image_t2042527209 * ___buildingIcon_2;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::buildingName
	Text_t356221433 * ___buildingName_3;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::buildingLevel
	Text_t356221433 * ___buildingLevel_4;
	// UnityEngine.UI.Text ResidenceBuildingsTableLine::status
	Text_t356221433 * ___status_5;
	// ResidenceBuildingsTableController ResidenceBuildingsTableLine::owner
	ResidenceBuildingsTableController_t3217399565 * ___owner_6;
	// System.Int64 ResidenceBuildingsTableLine::pitId
	int64_t ___pitId_7;

public:
	inline static int32_t get_offset_of_buildingIcon_2() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___buildingIcon_2)); }
	inline Image_t2042527209 * get_buildingIcon_2() const { return ___buildingIcon_2; }
	inline Image_t2042527209 ** get_address_of_buildingIcon_2() { return &___buildingIcon_2; }
	inline void set_buildingIcon_2(Image_t2042527209 * value)
	{
		___buildingIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___buildingIcon_2, value);
	}

	inline static int32_t get_offset_of_buildingName_3() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___buildingName_3)); }
	inline Text_t356221433 * get_buildingName_3() const { return ___buildingName_3; }
	inline Text_t356221433 ** get_address_of_buildingName_3() { return &___buildingName_3; }
	inline void set_buildingName_3(Text_t356221433 * value)
	{
		___buildingName_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildingName_3, value);
	}

	inline static int32_t get_offset_of_buildingLevel_4() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___buildingLevel_4)); }
	inline Text_t356221433 * get_buildingLevel_4() const { return ___buildingLevel_4; }
	inline Text_t356221433 ** get_address_of_buildingLevel_4() { return &___buildingLevel_4; }
	inline void set_buildingLevel_4(Text_t356221433 * value)
	{
		___buildingLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___buildingLevel_4, value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___status_5)); }
	inline Text_t356221433 * get_status_5() const { return ___status_5; }
	inline Text_t356221433 ** get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(Text_t356221433 * value)
	{
		___status_5 = value;
		Il2CppCodeGenWriteBarrier(&___status_5, value);
	}

	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___owner_6)); }
	inline ResidenceBuildingsTableController_t3217399565 * get_owner_6() const { return ___owner_6; }
	inline ResidenceBuildingsTableController_t3217399565 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(ResidenceBuildingsTableController_t3217399565 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier(&___owner_6, value);
	}

	inline static int32_t get_offset_of_pitId_7() { return static_cast<int32_t>(offsetof(ResidenceBuildingsTableLine_t325800007, ___pitId_7)); }
	inline int64_t get_pitId_7() const { return ___pitId_7; }
	inline int64_t* get_address_of_pitId_7() { return &___pitId_7; }
	inline void set_pitId_7(int64_t value)
	{
		___pitId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
