﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestWorldChunksManager
struct TestWorldChunksManager_t3581849513;

#include "codegen/il2cpp-codegen.h"

// System.Void TestWorldChunksManager::.ctor()
extern "C"  void TestWorldChunksManager__ctor_m2852767200 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::GoToCoords(System.Int64,System.Int64)
extern "C"  void TestWorldChunksManager_GoToCoords_m2026082981 (TestWorldChunksManager_t3581849513 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::InitWorldMap()
extern "C"  void TestWorldChunksManager_InitWorldMap_m68808550 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::UpdateWorldMap()
extern "C"  void TestWorldChunksManager_UpdateWorldMap_m2985614831 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::UpdateChunkCoords()
extern "C"  void TestWorldChunksManager_UpdateChunkCoords_m366495852 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::RightTransform()
extern "C"  void TestWorldChunksManager_RightTransform_m2297532136 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::LeftTransform()
extern "C"  void TestWorldChunksManager_LeftTransform_m4106608447 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::UpTransform()
extern "C"  void TestWorldChunksManager_UpTransform_m2202044773 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::DownTransform()
extern "C"  void TestWorldChunksManager_DownTransform_m2487748984 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::LowerRightTransform()
extern "C"  void TestWorldChunksManager_LowerRightTransform_m2123653599 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::LowerLeftTransform()
extern "C"  void TestWorldChunksManager_LowerLeftTransform_m3906368214 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::UpperRightTransform()
extern "C"  void TestWorldChunksManager_UpperRightTransform_m1539747962 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::UpperLeftTransform()
extern "C"  void TestWorldChunksManager_UpperLeftTransform_m1065502533 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestWorldChunksManager::CheckChunkGrid()
extern "C"  void TestWorldChunksManager_CheckChunkGrid_m2272151757 (TestWorldChunksManager_t3581849513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
