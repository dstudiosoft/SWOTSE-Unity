﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RightPanel
struct RightPanel_t1883823928;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RightPanel
struct  RightPanel_t1883823928  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform RightPanel::RightButtonsTransform
	RectTransform_t3349966182 * ___RightButtonsTransform_3;
	// UnityEngine.UI.Image RightPanel::cityIcon
	Image_t2042527209 * ___cityIcon_4;
	// UnityEngine.UI.Text RightPanel::cityCoords
	Text_t356221433 * ___cityCoords_5;
	// UnityEngine.UI.Text RightPanel::citiesCount
	Text_t356221433 * ___citiesCount_6;
	// System.Boolean RightPanel::isOpened
	bool ___isOpened_7;

public:
	inline static int32_t get_offset_of_RightButtonsTransform_3() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928, ___RightButtonsTransform_3)); }
	inline RectTransform_t3349966182 * get_RightButtonsTransform_3() const { return ___RightButtonsTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of_RightButtonsTransform_3() { return &___RightButtonsTransform_3; }
	inline void set_RightButtonsTransform_3(RectTransform_t3349966182 * value)
	{
		___RightButtonsTransform_3 = value;
		Il2CppCodeGenWriteBarrier(&___RightButtonsTransform_3, value);
	}

	inline static int32_t get_offset_of_cityIcon_4() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928, ___cityIcon_4)); }
	inline Image_t2042527209 * get_cityIcon_4() const { return ___cityIcon_4; }
	inline Image_t2042527209 ** get_address_of_cityIcon_4() { return &___cityIcon_4; }
	inline void set_cityIcon_4(Image_t2042527209 * value)
	{
		___cityIcon_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityIcon_4, value);
	}

	inline static int32_t get_offset_of_cityCoords_5() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928, ___cityCoords_5)); }
	inline Text_t356221433 * get_cityCoords_5() const { return ___cityCoords_5; }
	inline Text_t356221433 ** get_address_of_cityCoords_5() { return &___cityCoords_5; }
	inline void set_cityCoords_5(Text_t356221433 * value)
	{
		___cityCoords_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityCoords_5, value);
	}

	inline static int32_t get_offset_of_citiesCount_6() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928, ___citiesCount_6)); }
	inline Text_t356221433 * get_citiesCount_6() const { return ___citiesCount_6; }
	inline Text_t356221433 ** get_address_of_citiesCount_6() { return &___citiesCount_6; }
	inline void set_citiesCount_6(Text_t356221433 * value)
	{
		___citiesCount_6 = value;
		Il2CppCodeGenWriteBarrier(&___citiesCount_6, value);
	}

	inline static int32_t get_offset_of_isOpened_7() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928, ___isOpened_7)); }
	inline bool get_isOpened_7() const { return ___isOpened_7; }
	inline bool* get_address_of_isOpened_7() { return &___isOpened_7; }
	inline void set_isOpened_7(bool value)
	{
		___isOpened_7 = value;
	}
};

struct RightPanel_t1883823928_StaticFields
{
public:
	// RightPanel RightPanel::instance
	RightPanel_t1883823928 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(RightPanel_t1883823928_StaticFields, ___instance_2)); }
	inline RightPanel_t1883823928 * get_instance_2() const { return ___instance_2; }
	inline RightPanel_t1883823928 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RightPanel_t1883823928 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
