﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceChat
struct AllianceChat_t2362063621;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void AllianceChat::.ctor()
extern "C"  void AllianceChat__ctor_m2003110882 (AllianceChat_t2362063621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceChat::Start()
extern "C"  void AllianceChat_Start_m3080357394 (AllianceChat_t2362063621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceChat::BeginChat()
extern "C"  Il2CppObject * AllianceChat_BeginChat_m3250638691 (AllianceChat_t2362063621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceChat::OnApplicationPause(System.Boolean)
extern "C"  void AllianceChat_OnApplicationPause_m1000465728 (AllianceChat_t2362063621 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceChat::SendMessage()
extern "C"  void AllianceChat_SendMessage_m2028285687 (AllianceChat_t2362063621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
