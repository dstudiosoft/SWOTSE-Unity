﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m1899397640(__this, ___parent0, method) ((  void (*) (Enumerator_t404228874 *, LinkedList_1_t1581322852 *, const MethodInfo*))Enumerator__ctor_m1586864815_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2690961185(__this, method) ((  Il2CppObject * (*) (Enumerator_t404228874 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1254627285(__this, method) ((  void (*) (Enumerator_t404228874 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::get_Current()
#define Enumerator_get_Current_m1825200644(__this, method) ((  TableViewCell_t1276614623 * (*) (Enumerator_t404228874 *, const MethodInfo*))Enumerator_get_Current_m3158498407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::MoveNext()
#define Enumerator_MoveNext_m1843897265(__this, method) ((  bool (*) (Enumerator_t404228874 *, const MethodInfo*))Enumerator_MoveNext_m1957727328_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Tacticsoft.TableViewCell>::Dispose()
#define Enumerator_Dispose_m3920922164(__this, method) ((  void (*) (Enumerator_t404228874 *, const MethodInfo*))Enumerator_Dispose_m3217456423_gshared)(__this, method)
