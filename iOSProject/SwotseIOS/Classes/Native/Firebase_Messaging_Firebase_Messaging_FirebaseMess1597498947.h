﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t3038239007;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t1095792431;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_t1597498947;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener
struct  Listener_t1597498947  : public Il2CppObject
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::messageReceivedDelegate
	MessageReceivedDelegate_t3038239007 * ___messageReceivedDelegate_0;
	// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::tokenReceivedDelegate
	TokenReceivedDelegate_t1095792431 * ___tokenReceivedDelegate_1;
	// Firebase.FirebaseApp Firebase.Messaging.FirebaseMessaging/Listener::app
	FirebaseApp_t210707726 * ___app_2;

public:
	inline static int32_t get_offset_of_messageReceivedDelegate_0() { return static_cast<int32_t>(offsetof(Listener_t1597498947, ___messageReceivedDelegate_0)); }
	inline MessageReceivedDelegate_t3038239007 * get_messageReceivedDelegate_0() const { return ___messageReceivedDelegate_0; }
	inline MessageReceivedDelegate_t3038239007 ** get_address_of_messageReceivedDelegate_0() { return &___messageReceivedDelegate_0; }
	inline void set_messageReceivedDelegate_0(MessageReceivedDelegate_t3038239007 * value)
	{
		___messageReceivedDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&___messageReceivedDelegate_0, value);
	}

	inline static int32_t get_offset_of_tokenReceivedDelegate_1() { return static_cast<int32_t>(offsetof(Listener_t1597498947, ___tokenReceivedDelegate_1)); }
	inline TokenReceivedDelegate_t1095792431 * get_tokenReceivedDelegate_1() const { return ___tokenReceivedDelegate_1; }
	inline TokenReceivedDelegate_t1095792431 ** get_address_of_tokenReceivedDelegate_1() { return &___tokenReceivedDelegate_1; }
	inline void set_tokenReceivedDelegate_1(TokenReceivedDelegate_t1095792431 * value)
	{
		___tokenReceivedDelegate_1 = value;
		Il2CppCodeGenWriteBarrier(&___tokenReceivedDelegate_1, value);
	}

	inline static int32_t get_offset_of_app_2() { return static_cast<int32_t>(offsetof(Listener_t1597498947, ___app_2)); }
	inline FirebaseApp_t210707726 * get_app_2() const { return ___app_2; }
	inline FirebaseApp_t210707726 ** get_address_of_app_2() { return &___app_2; }
	inline void set_app_2(FirebaseApp_t210707726 * value)
	{
		___app_2 = value;
		Il2CppCodeGenWriteBarrier(&___app_2, value);
	}
};

struct Listener_t1597498947_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::listener
	Listener_t1597498947 * ___listener_3;

public:
	inline static int32_t get_offset_of_listener_3() { return static_cast<int32_t>(offsetof(Listener_t1597498947_StaticFields, ___listener_3)); }
	inline Listener_t1597498947 * get_listener_3() const { return ___listener_3; }
	inline Listener_t1597498947 ** get_address_of_listener_3() { return &___listener_3; }
	inline void set_listener_3(Listener_t1597498947 * value)
	{
		___listener_3 = value;
		Il2CppCodeGenWriteBarrier(&___listener_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
