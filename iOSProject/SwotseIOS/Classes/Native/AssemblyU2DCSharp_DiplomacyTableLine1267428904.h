﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DiplomacyTableLine
struct  DiplomacyTableLine_t1267428904  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text DiplomacyTableLine::nickName
	Text_t356221433 * ___nickName_2;
	// UnityEngine.UI.Text DiplomacyTableLine::knight
	Text_t356221433 * ___knight_3;
	// UnityEngine.UI.Text DiplomacyTableLine::city
	Text_t356221433 * ___city_4;
	// UnityEngine.UI.Text DiplomacyTableLine::coord
	Text_t356221433 * ___coord_5;
	// UnityEngine.UI.Text DiplomacyTableLine::date
	Text_t356221433 * ___date_6;

public:
	inline static int32_t get_offset_of_nickName_2() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1267428904, ___nickName_2)); }
	inline Text_t356221433 * get_nickName_2() const { return ___nickName_2; }
	inline Text_t356221433 ** get_address_of_nickName_2() { return &___nickName_2; }
	inline void set_nickName_2(Text_t356221433 * value)
	{
		___nickName_2 = value;
		Il2CppCodeGenWriteBarrier(&___nickName_2, value);
	}

	inline static int32_t get_offset_of_knight_3() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1267428904, ___knight_3)); }
	inline Text_t356221433 * get_knight_3() const { return ___knight_3; }
	inline Text_t356221433 ** get_address_of_knight_3() { return &___knight_3; }
	inline void set_knight_3(Text_t356221433 * value)
	{
		___knight_3 = value;
		Il2CppCodeGenWriteBarrier(&___knight_3, value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1267428904, ___city_4)); }
	inline Text_t356221433 * get_city_4() const { return ___city_4; }
	inline Text_t356221433 ** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(Text_t356221433 * value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier(&___city_4, value);
	}

	inline static int32_t get_offset_of_coord_5() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1267428904, ___coord_5)); }
	inline Text_t356221433 * get_coord_5() const { return ___coord_5; }
	inline Text_t356221433 ** get_address_of_coord_5() { return &___coord_5; }
	inline void set_coord_5(Text_t356221433 * value)
	{
		___coord_5 = value;
		Il2CppCodeGenWriteBarrier(&___coord_5, value);
	}

	inline static int32_t get_offset_of_date_6() { return static_cast<int32_t>(offsetof(DiplomacyTableLine_t1267428904, ___date_6)); }
	inline Text_t356221433 * get_date_6() const { return ___date_6; }
	inline Text_t356221433 ** get_address_of_date_6() { return &___date_6; }
	inline void set_date_6(Text_t356221433 * value)
	{
		___date_6 = value;
		Il2CppCodeGenWriteBarrier(&___date_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
