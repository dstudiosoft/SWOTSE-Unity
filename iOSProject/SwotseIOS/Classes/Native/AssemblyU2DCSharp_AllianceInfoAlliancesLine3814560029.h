﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AllianceInfoTableContoller
struct AllianceInfoTableContoller_t4166874261;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInfoAlliancesLine
struct  AllianceInfoAlliancesLine_t3814560029  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::allianceName
	Text_t356221433 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::rank
	Text_t356221433 * ___rank_3;
	// UnityEngine.UI.Text AllianceInfoAlliancesLine::members
	Text_t356221433 * ___members_4;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::neutralBTN
	GameObject_t1756533147 * ___neutralBTN_5;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::allyBTN
	GameObject_t1756533147 * ___allyBTN_6;
	// UnityEngine.GameObject AllianceInfoAlliancesLine::enemyBTN
	GameObject_t1756533147 * ___enemyBTN_7;
	// AllianceInfoTableContoller AllianceInfoAlliancesLine::owner
	AllianceInfoTableContoller_t4166874261 * ___owner_8;
	// System.Int64 AllianceInfoAlliancesLine::allianceId
	int64_t ___allianceId_9;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___allianceName_2)); }
	inline Text_t356221433 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t356221433 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t356221433 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier(&___allianceName_2, value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___rank_3)); }
	inline Text_t356221433 * get_rank_3() const { return ___rank_3; }
	inline Text_t356221433 ** get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(Text_t356221433 * value)
	{
		___rank_3 = value;
		Il2CppCodeGenWriteBarrier(&___rank_3, value);
	}

	inline static int32_t get_offset_of_members_4() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___members_4)); }
	inline Text_t356221433 * get_members_4() const { return ___members_4; }
	inline Text_t356221433 ** get_address_of_members_4() { return &___members_4; }
	inline void set_members_4(Text_t356221433 * value)
	{
		___members_4 = value;
		Il2CppCodeGenWriteBarrier(&___members_4, value);
	}

	inline static int32_t get_offset_of_neutralBTN_5() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___neutralBTN_5)); }
	inline GameObject_t1756533147 * get_neutralBTN_5() const { return ___neutralBTN_5; }
	inline GameObject_t1756533147 ** get_address_of_neutralBTN_5() { return &___neutralBTN_5; }
	inline void set_neutralBTN_5(GameObject_t1756533147 * value)
	{
		___neutralBTN_5 = value;
		Il2CppCodeGenWriteBarrier(&___neutralBTN_5, value);
	}

	inline static int32_t get_offset_of_allyBTN_6() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___allyBTN_6)); }
	inline GameObject_t1756533147 * get_allyBTN_6() const { return ___allyBTN_6; }
	inline GameObject_t1756533147 ** get_address_of_allyBTN_6() { return &___allyBTN_6; }
	inline void set_allyBTN_6(GameObject_t1756533147 * value)
	{
		___allyBTN_6 = value;
		Il2CppCodeGenWriteBarrier(&___allyBTN_6, value);
	}

	inline static int32_t get_offset_of_enemyBTN_7() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___enemyBTN_7)); }
	inline GameObject_t1756533147 * get_enemyBTN_7() const { return ___enemyBTN_7; }
	inline GameObject_t1756533147 ** get_address_of_enemyBTN_7() { return &___enemyBTN_7; }
	inline void set_enemyBTN_7(GameObject_t1756533147 * value)
	{
		___enemyBTN_7 = value;
		Il2CppCodeGenWriteBarrier(&___enemyBTN_7, value);
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___owner_8)); }
	inline AllianceInfoTableContoller_t4166874261 * get_owner_8() const { return ___owner_8; }
	inline AllianceInfoTableContoller_t4166874261 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(AllianceInfoTableContoller_t4166874261 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier(&___owner_8, value);
	}

	inline static int32_t get_offset_of_allianceId_9() { return static_cast<int32_t>(offsetof(AllianceInfoAlliancesLine_t3814560029, ___allianceId_9)); }
	inline int64_t get_allianceId_9() const { return ___allianceId_9; }
	inline int64_t* get_address_of_allianceId_9() { return &___allianceId_9; }
	inline void set_allianceId_9(int64_t value)
	{
		___allianceId_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
