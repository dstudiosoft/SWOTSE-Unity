﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.CameraProjectionParams
struct CameraProjectionParams_t285425166;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

// System.Void TouchScript.Layers.CameraProjectionParams::.ctor(UnityEngine.Camera)
extern "C"  void CameraProjectionParams__ctor_m2316772447 (CameraProjectionParams_t285425166 * __this, Camera_t189460977 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.CameraProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern "C"  Vector3_t2243707580  CameraProjectionParams_ProjectTo_m1628125712 (CameraProjectionParams_t285425166 * __this, Vector2_t2243707579  ___screenPosition0, Plane_t3727654732  ___projectionPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Layers.CameraProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  CameraProjectionParams_ProjectFrom_m3600738284 (CameraProjectionParams_t285425166 * __this, Vector3_t2243707580  ___worldPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
