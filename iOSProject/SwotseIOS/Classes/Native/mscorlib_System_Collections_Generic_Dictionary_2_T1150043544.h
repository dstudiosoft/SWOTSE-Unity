﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t1850351419;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint,System.Collections.DictionaryEntry>
struct  Transform_1_t1150043544  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
