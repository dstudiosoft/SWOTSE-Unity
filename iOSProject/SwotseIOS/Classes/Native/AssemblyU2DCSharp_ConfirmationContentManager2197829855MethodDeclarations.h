﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfirmationContentManager
struct ConfirmationContentManager_t2197829855;

#include "codegen/il2cpp-codegen.h"

// System.Void ConfirmationContentManager::.ctor()
extern "C"  void ConfirmationContentManager__ctor_m3645794784 (ConfirmationContentManager_t2197829855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfirmationContentManager::Ok()
extern "C"  void ConfirmationContentManager_Ok_m916700016 (ConfirmationContentManager_t2197829855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfirmationContentManager::Cancel()
extern "C"  void ConfirmationContentManager_Cancel_m1021301584 (ConfirmationContentManager_t2197829855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
