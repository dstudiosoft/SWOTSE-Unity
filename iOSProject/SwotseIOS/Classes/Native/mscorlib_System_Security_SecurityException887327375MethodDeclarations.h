﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityException
struct SecurityException_t887327375;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Exception
struct Exception_t1927440687;
// System.Type
struct Type_t;
// System.Security.PermissionSet
struct PermissionSet_t1941658161;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Reflection.AssemblyName
struct AssemblyName_t894705941;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Security_PermissionSet1941658161.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_AssemblyName894705941.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"
#include "mscorlib_System_Security_Policy_Evidence1407710183.h"
#include "mscorlib_System_Security_SecurityZone140334334.h"

// System.Void System.Security.SecurityException::.ctor()
extern "C"  void SecurityException__ctor_m3135486104 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String)
extern "C"  void SecurityException__ctor_m1484114982 (SecurityException_t887327375 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SecurityException__ctor_m230238597 (SecurityException_t887327375 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Exception)
extern "C"  void SecurityException__ctor_m3447307494 (SecurityException_t887327375 * __this, String_t* ___message0, Exception_t1927440687 * ___inner1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Type)
extern "C"  void SecurityException__ctor_m1072192231 (SecurityException_t887327375 * __this, String_t* ___message0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Type,System.String)
extern "C"  void SecurityException__ctor_m2806902409 (SecurityException_t887327375 * __this, String_t* ___message0, Type_t * ___type1, String_t* ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C"  void SecurityException__ctor_m1552409510 (SecurityException_t887327375 * __this, String_t* ___message0, PermissionSet_t1941658161 * ___granted1, PermissionSet_t1941658161 * ___refused2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Object,System.Object,System.Reflection.MethodInfo,System.Object,System.Security.IPermission)
extern "C"  void SecurityException__ctor_m3754972836 (SecurityException_t887327375 * __this, String_t* ___message0, Il2CppObject * ___deny1, Il2CppObject * ___permitOnly2, MethodInfo_t * ___method3, Il2CppObject * ___demanded4, Il2CppObject * ___permThatFailed5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Reflection.AssemblyName,System.Security.PermissionSet,System.Security.PermissionSet,System.Reflection.MethodInfo,System.Security.Permissions.SecurityAction,System.Object,System.Security.IPermission,System.Security.Policy.Evidence)
extern "C"  void SecurityException__ctor_m1519941250 (SecurityException_t887327375 * __this, String_t* ___message0, AssemblyName_t894705941 * ___assemblyName1, PermissionSet_t1941658161 * ___grant2, PermissionSet_t1941658161 * ___refused3, MethodInfo_t * ___method4, int32_t ___action5, Il2CppObject * ___demanded6, Il2CppObject * ___permThatFailed7, Evidence_t1407710183 * ___evidence8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.SecurityAction System.Security.SecurityException::get_Action()
extern "C"  int32_t SecurityException_get_Action_m3142634879 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Action(System.Security.Permissions.SecurityAction)
extern "C"  void SecurityException_set_Action_m1486469406 (SecurityException_t887327375 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_DenySetInstance()
extern "C"  Il2CppObject * SecurityException_get_DenySetInstance_m371552947 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_DenySetInstance(System.Object)
extern "C"  void SecurityException_set_DenySetInstance_m3859039518 (SecurityException_t887327375 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Security.SecurityException::get_FailedAssemblyInfo()
extern "C"  AssemblyName_t894705941 * SecurityException_get_FailedAssemblyInfo_m3109169312 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_FailedAssemblyInfo(System.Reflection.AssemblyName)
extern "C"  void SecurityException_set_FailedAssemblyInfo_m1279170799 (SecurityException_t887327375 * __this, AssemblyName_t894705941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Security.SecurityException::get_Method()
extern "C"  MethodInfo_t * SecurityException_get_Method_m3662124986 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Method(System.Reflection.MethodInfo)
extern "C"  void SecurityException_set_Method_m3351946911 (SecurityException_t887327375 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_PermitOnlySetInstance()
extern "C"  Il2CppObject * SecurityException_get_PermitOnlySetInstance_m5261972 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermitOnlySetInstance(System.Object)
extern "C"  void SecurityException_set_PermitOnlySetInstance_m3687937025 (SecurityException_t887327375 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_Url()
extern "C"  String_t* SecurityException_get_Url_m845377629 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Url(System.String)
extern "C"  void SecurityException_set_Url_m2352999908 (SecurityException_t887327375 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.SecurityException::get_Zone()
extern "C"  int32_t SecurityException_get_Zone_m3087585761 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Zone(System.Security.SecurityZone)
extern "C"  void SecurityException_set_Zone_m2544569372 (SecurityException_t887327375 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_Demanded()
extern "C"  Il2CppObject * SecurityException_get_Demanded_m3914466388 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Demanded(System.Object)
extern "C"  void SecurityException_set_Demanded_m2227665503 (SecurityException_t887327375 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityException::get_FirstPermissionThatFailed()
extern "C"  Il2CppObject * SecurityException_get_FirstPermissionThatFailed_m3311583176 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_FirstPermissionThatFailed(System.Security.IPermission)
extern "C"  void SecurityException_set_FirstPermissionThatFailed_m4023561201 (SecurityException_t887327375 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_PermissionState()
extern "C"  String_t* SecurityException_get_PermissionState_m824616682 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermissionState(System.String)
extern "C"  void SecurityException_set_PermissionState_m1290140613 (SecurityException_t887327375 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.SecurityException::get_PermissionType()
extern "C"  Type_t * SecurityException_get_PermissionType_m3543350822 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermissionType(System.Type)
extern "C"  void SecurityException_set_PermissionType_m3936831419 (SecurityException_t887327375 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_GrantedSet()
extern "C"  String_t* SecurityException_get_GrantedSet_m1238160247 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_GrantedSet(System.String)
extern "C"  void SecurityException_set_GrantedSet_m3068551000 (SecurityException_t887327375 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_RefusedSet()
extern "C"  String_t* SecurityException_get_RefusedSet_m1580594168 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_RefusedSet(System.String)
extern "C"  void SecurityException_set_RefusedSet_m646736809 (SecurityException_t887327375 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SecurityException_GetObjectData_m487588480 (SecurityException_t887327375 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::ToString()
extern "C"  String_t* SecurityException_ToString_m937659401 (SecurityException_t887327375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
