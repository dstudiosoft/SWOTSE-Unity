﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_ObjectPool_1_U1709969761MethodDeclarations.h"

// System.Void TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::.ctor(System.Object,System.IntPtr)
#define UnityFunc_1__ctor_m2285455663(__this, ___object0, ___method1, method) ((  void (*) (UnityFunc_1_t2053600641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityFunc_1__ctor_m3483258383_gshared)(__this, ___object0, ___method1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::Invoke()
#define UnityFunc_1_Invoke_m1656520546(__this, method) ((  List_1_t328750215 * (*) (UnityFunc_1_t2053600641 *, const MethodInfo*))UnityFunc_1_Invoke_m4133775042_gshared)(__this, method)
// System.IAsyncResult TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::BeginInvoke(System.AsyncCallback,System.Object)
#define UnityFunc_1_BeginInvoke_m1626162090(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (UnityFunc_1_t2053600641 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_BeginInvoke_m3032171082_gshared)(__this, ___callback0, ___object1, method)
// T0 TouchScript.Utils.ObjectPool`1/UnityFunc`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::EndInvoke(System.IAsyncResult)
#define UnityFunc_1_EndInvoke_m1163107574(__this, ___result0, method) ((  List_1_t328750215 * (*) (UnityFunc_1_t2053600641 *, Il2CppObject *, const MethodInfo*))UnityFunc_1_EndInvoke_m455851222_gshared)(__this, ___result0, method)
