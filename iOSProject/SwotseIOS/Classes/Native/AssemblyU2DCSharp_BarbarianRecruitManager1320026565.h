﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// SimpleEvent
struct SimpleEvent_t1509017202;
// UnitsModel
struct UnitsModel_t1926818124;
// ResourcesModel
struct ResourcesModel_t2978985958;
// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel>
struct Dictionary_2_t3722048895;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarbarianRecruitManager
struct  BarbarianRecruitManager_t1320026565  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text BarbarianRecruitManager::citySilver
	Text_t356221433 * ___citySilver_2;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityGold
	Text_t356221433 * ___cityGold_3;
	// UnityEngine.UI.Text BarbarianRecruitManager::priceSilver
	Text_t356221433 * ___priceSilver_4;
	// UnityEngine.UI.Text BarbarianRecruitManager::priceGold
	Text_t356221433 * ___priceGold_5;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityWorker
	Text_t356221433 * ___cityWorker_6;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySpy
	Text_t356221433 * ___citySpy_7;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySwordsman
	Text_t356221433 * ___citySwordsman_8;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySpearman
	Text_t356221433 * ___citySpearman_9;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityPikeman
	Text_t356221433 * ___cityPikeman_10;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityScoutRider
	Text_t356221433 * ___cityScoutRider_11;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityLightCavalry
	Text_t356221433 * ___cityLightCavalry_12;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityHeavyCavalry
	Text_t356221433 * ___cityHeavyCavalry_13;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityArcher
	Text_t356221433 * ___cityArcher_14;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityArcherRider
	Text_t356221433 * ___cityArcherRider_15;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityHollyman
	Text_t356221433 * ___cityHollyman_16;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityWagon
	Text_t356221433 * ___cityWagon_17;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityTrebuchet
	Text_t356221433 * ___cityTrebuchet_18;
	// UnityEngine.UI.Text BarbarianRecruitManager::citySiegeTower
	Text_t356221433 * ___citySiegeTower_19;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityBatteringRam
	Text_t356221433 * ___cityBatteringRam_20;
	// UnityEngine.UI.Text BarbarianRecruitManager::cityBallista
	Text_t356221433 * ___cityBallista_21;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchWorker
	InputField_t1631627530 * ___dispatchWorker_22;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSpy
	InputField_t1631627530 * ___dispatchSpy_23;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSwordsman
	InputField_t1631627530 * ___dispatchSwordsman_24;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSpearman
	InputField_t1631627530 * ___dispatchSpearman_25;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchPikeman
	InputField_t1631627530 * ___dispatchPikeman_26;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchScoutRider
	InputField_t1631627530 * ___dispatchScoutRider_27;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchLightCavalry
	InputField_t1631627530 * ___dispatchLightCavalry_28;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchHeavyCavalry
	InputField_t1631627530 * ___dispatchHeavyCavalry_29;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchArcher
	InputField_t1631627530 * ___dispatchArcher_30;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchArcherRider
	InputField_t1631627530 * ___dispatchArcherRider_31;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchHollyman
	InputField_t1631627530 * ___dispatchHollyman_32;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchWagon
	InputField_t1631627530 * ___dispatchWagon_33;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchTrebuchet
	InputField_t1631627530 * ___dispatchTrebuchet_34;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchSiegeTower
	InputField_t1631627530 * ___dispatchSiegeTower_35;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchBatteringRam
	InputField_t1631627530 * ___dispatchBatteringRam_36;
	// UnityEngine.UI.InputField BarbarianRecruitManager::dispatchBallista
	InputField_t1631627530 * ___dispatchBallista_37;
	// UnityEngine.UI.InputField BarbarianRecruitManager::xInput
	InputField_t1631627530 * ___xInput_38;
	// UnityEngine.UI.InputField BarbarianRecruitManager::yInput
	InputField_t1631627530 * ___yInput_39;
	// WorldFieldModel BarbarianRecruitManager::targetField
	WorldFieldModel_t3469935653 * ___targetField_40;
	// SimpleEvent BarbarianRecruitManager::onGetBarbariansArmy
	SimpleEvent_t1509017202 * ___onGetBarbariansArmy_41;
	// UnitsModel BarbarianRecruitManager::presentArmy
	UnitsModel_t1926818124 * ___presentArmy_42;
	// UnitsModel BarbarianRecruitManager::army
	UnitsModel_t1926818124 * ___army_43;
	// ResourcesModel BarbarianRecruitManager::cityResources
	ResourcesModel_t2978985958 * ___cityResources_44;
	// System.Collections.Generic.Dictionary`2<System.String,UnitConstantModel> BarbarianRecruitManager::unitConstants
	Dictionary_2_t3722048895 * ___unitConstants_45;

public:
	inline static int32_t get_offset_of_citySilver_2() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___citySilver_2)); }
	inline Text_t356221433 * get_citySilver_2() const { return ___citySilver_2; }
	inline Text_t356221433 ** get_address_of_citySilver_2() { return &___citySilver_2; }
	inline void set_citySilver_2(Text_t356221433 * value)
	{
		___citySilver_2 = value;
		Il2CppCodeGenWriteBarrier(&___citySilver_2, value);
	}

	inline static int32_t get_offset_of_cityGold_3() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityGold_3)); }
	inline Text_t356221433 * get_cityGold_3() const { return ___cityGold_3; }
	inline Text_t356221433 ** get_address_of_cityGold_3() { return &___cityGold_3; }
	inline void set_cityGold_3(Text_t356221433 * value)
	{
		___cityGold_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityGold_3, value);
	}

	inline static int32_t get_offset_of_priceSilver_4() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___priceSilver_4)); }
	inline Text_t356221433 * get_priceSilver_4() const { return ___priceSilver_4; }
	inline Text_t356221433 ** get_address_of_priceSilver_4() { return &___priceSilver_4; }
	inline void set_priceSilver_4(Text_t356221433 * value)
	{
		___priceSilver_4 = value;
		Il2CppCodeGenWriteBarrier(&___priceSilver_4, value);
	}

	inline static int32_t get_offset_of_priceGold_5() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___priceGold_5)); }
	inline Text_t356221433 * get_priceGold_5() const { return ___priceGold_5; }
	inline Text_t356221433 ** get_address_of_priceGold_5() { return &___priceGold_5; }
	inline void set_priceGold_5(Text_t356221433 * value)
	{
		___priceGold_5 = value;
		Il2CppCodeGenWriteBarrier(&___priceGold_5, value);
	}

	inline static int32_t get_offset_of_cityWorker_6() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityWorker_6)); }
	inline Text_t356221433 * get_cityWorker_6() const { return ___cityWorker_6; }
	inline Text_t356221433 ** get_address_of_cityWorker_6() { return &___cityWorker_6; }
	inline void set_cityWorker_6(Text_t356221433 * value)
	{
		___cityWorker_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorker_6, value);
	}

	inline static int32_t get_offset_of_citySpy_7() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___citySpy_7)); }
	inline Text_t356221433 * get_citySpy_7() const { return ___citySpy_7; }
	inline Text_t356221433 ** get_address_of_citySpy_7() { return &___citySpy_7; }
	inline void set_citySpy_7(Text_t356221433 * value)
	{
		___citySpy_7 = value;
		Il2CppCodeGenWriteBarrier(&___citySpy_7, value);
	}

	inline static int32_t get_offset_of_citySwordsman_8() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___citySwordsman_8)); }
	inline Text_t356221433 * get_citySwordsman_8() const { return ___citySwordsman_8; }
	inline Text_t356221433 ** get_address_of_citySwordsman_8() { return &___citySwordsman_8; }
	inline void set_citySwordsman_8(Text_t356221433 * value)
	{
		___citySwordsman_8 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsman_8, value);
	}

	inline static int32_t get_offset_of_citySpearman_9() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___citySpearman_9)); }
	inline Text_t356221433 * get_citySpearman_9() const { return ___citySpearman_9; }
	inline Text_t356221433 ** get_address_of_citySpearman_9() { return &___citySpearman_9; }
	inline void set_citySpearman_9(Text_t356221433 * value)
	{
		___citySpearman_9 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearman_9, value);
	}

	inline static int32_t get_offset_of_cityPikeman_10() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityPikeman_10)); }
	inline Text_t356221433 * get_cityPikeman_10() const { return ___cityPikeman_10; }
	inline Text_t356221433 ** get_address_of_cityPikeman_10() { return &___cityPikeman_10; }
	inline void set_cityPikeman_10(Text_t356221433 * value)
	{
		___cityPikeman_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikeman_10, value);
	}

	inline static int32_t get_offset_of_cityScoutRider_11() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityScoutRider_11)); }
	inline Text_t356221433 * get_cityScoutRider_11() const { return ___cityScoutRider_11; }
	inline Text_t356221433 ** get_address_of_cityScoutRider_11() { return &___cityScoutRider_11; }
	inline void set_cityScoutRider_11(Text_t356221433 * value)
	{
		___cityScoutRider_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRider_11, value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_12() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityLightCavalry_12)); }
	inline Text_t356221433 * get_cityLightCavalry_12() const { return ___cityLightCavalry_12; }
	inline Text_t356221433 ** get_address_of_cityLightCavalry_12() { return &___cityLightCavalry_12; }
	inline void set_cityLightCavalry_12(Text_t356221433 * value)
	{
		___cityLightCavalry_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalry_12, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_13() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityHeavyCavalry_13)); }
	inline Text_t356221433 * get_cityHeavyCavalry_13() const { return ___cityHeavyCavalry_13; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalry_13() { return &___cityHeavyCavalry_13; }
	inline void set_cityHeavyCavalry_13(Text_t356221433 * value)
	{
		___cityHeavyCavalry_13 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalry_13, value);
	}

	inline static int32_t get_offset_of_cityArcher_14() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityArcher_14)); }
	inline Text_t356221433 * get_cityArcher_14() const { return ___cityArcher_14; }
	inline Text_t356221433 ** get_address_of_cityArcher_14() { return &___cityArcher_14; }
	inline void set_cityArcher_14(Text_t356221433 * value)
	{
		___cityArcher_14 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcher_14, value);
	}

	inline static int32_t get_offset_of_cityArcherRider_15() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityArcherRider_15)); }
	inline Text_t356221433 * get_cityArcherRider_15() const { return ___cityArcherRider_15; }
	inline Text_t356221433 ** get_address_of_cityArcherRider_15() { return &___cityArcherRider_15; }
	inline void set_cityArcherRider_15(Text_t356221433 * value)
	{
		___cityArcherRider_15 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRider_15, value);
	}

	inline static int32_t get_offset_of_cityHollyman_16() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityHollyman_16)); }
	inline Text_t356221433 * get_cityHollyman_16() const { return ___cityHollyman_16; }
	inline Text_t356221433 ** get_address_of_cityHollyman_16() { return &___cityHollyman_16; }
	inline void set_cityHollyman_16(Text_t356221433 * value)
	{
		___cityHollyman_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollyman_16, value);
	}

	inline static int32_t get_offset_of_cityWagon_17() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityWagon_17)); }
	inline Text_t356221433 * get_cityWagon_17() const { return ___cityWagon_17; }
	inline Text_t356221433 ** get_address_of_cityWagon_17() { return &___cityWagon_17; }
	inline void set_cityWagon_17(Text_t356221433 * value)
	{
		___cityWagon_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagon_17, value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_18() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityTrebuchet_18)); }
	inline Text_t356221433 * get_cityTrebuchet_18() const { return ___cityTrebuchet_18; }
	inline Text_t356221433 ** get_address_of_cityTrebuchet_18() { return &___cityTrebuchet_18; }
	inline void set_cityTrebuchet_18(Text_t356221433 * value)
	{
		___cityTrebuchet_18 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchet_18, value);
	}

	inline static int32_t get_offset_of_citySiegeTower_19() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___citySiegeTower_19)); }
	inline Text_t356221433 * get_citySiegeTower_19() const { return ___citySiegeTower_19; }
	inline Text_t356221433 ** get_address_of_citySiegeTower_19() { return &___citySiegeTower_19; }
	inline void set_citySiegeTower_19(Text_t356221433 * value)
	{
		___citySiegeTower_19 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTower_19, value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_20() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityBatteringRam_20)); }
	inline Text_t356221433 * get_cityBatteringRam_20() const { return ___cityBatteringRam_20; }
	inline Text_t356221433 ** get_address_of_cityBatteringRam_20() { return &___cityBatteringRam_20; }
	inline void set_cityBatteringRam_20(Text_t356221433 * value)
	{
		___cityBatteringRam_20 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRam_20, value);
	}

	inline static int32_t get_offset_of_cityBallista_21() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityBallista_21)); }
	inline Text_t356221433 * get_cityBallista_21() const { return ___cityBallista_21; }
	inline Text_t356221433 ** get_address_of_cityBallista_21() { return &___cityBallista_21; }
	inline void set_cityBallista_21(Text_t356221433 * value)
	{
		___cityBallista_21 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallista_21, value);
	}

	inline static int32_t get_offset_of_dispatchWorker_22() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchWorker_22)); }
	inline InputField_t1631627530 * get_dispatchWorker_22() const { return ___dispatchWorker_22; }
	inline InputField_t1631627530 ** get_address_of_dispatchWorker_22() { return &___dispatchWorker_22; }
	inline void set_dispatchWorker_22(InputField_t1631627530 * value)
	{
		___dispatchWorker_22 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchWorker_22, value);
	}

	inline static int32_t get_offset_of_dispatchSpy_23() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchSpy_23)); }
	inline InputField_t1631627530 * get_dispatchSpy_23() const { return ___dispatchSpy_23; }
	inline InputField_t1631627530 ** get_address_of_dispatchSpy_23() { return &___dispatchSpy_23; }
	inline void set_dispatchSpy_23(InputField_t1631627530 * value)
	{
		___dispatchSpy_23 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSpy_23, value);
	}

	inline static int32_t get_offset_of_dispatchSwordsman_24() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchSwordsman_24)); }
	inline InputField_t1631627530 * get_dispatchSwordsman_24() const { return ___dispatchSwordsman_24; }
	inline InputField_t1631627530 ** get_address_of_dispatchSwordsman_24() { return &___dispatchSwordsman_24; }
	inline void set_dispatchSwordsman_24(InputField_t1631627530 * value)
	{
		___dispatchSwordsman_24 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSwordsman_24, value);
	}

	inline static int32_t get_offset_of_dispatchSpearman_25() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchSpearman_25)); }
	inline InputField_t1631627530 * get_dispatchSpearman_25() const { return ___dispatchSpearman_25; }
	inline InputField_t1631627530 ** get_address_of_dispatchSpearman_25() { return &___dispatchSpearman_25; }
	inline void set_dispatchSpearman_25(InputField_t1631627530 * value)
	{
		___dispatchSpearman_25 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSpearman_25, value);
	}

	inline static int32_t get_offset_of_dispatchPikeman_26() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchPikeman_26)); }
	inline InputField_t1631627530 * get_dispatchPikeman_26() const { return ___dispatchPikeman_26; }
	inline InputField_t1631627530 ** get_address_of_dispatchPikeman_26() { return &___dispatchPikeman_26; }
	inline void set_dispatchPikeman_26(InputField_t1631627530 * value)
	{
		___dispatchPikeman_26 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchPikeman_26, value);
	}

	inline static int32_t get_offset_of_dispatchScoutRider_27() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchScoutRider_27)); }
	inline InputField_t1631627530 * get_dispatchScoutRider_27() const { return ___dispatchScoutRider_27; }
	inline InputField_t1631627530 ** get_address_of_dispatchScoutRider_27() { return &___dispatchScoutRider_27; }
	inline void set_dispatchScoutRider_27(InputField_t1631627530 * value)
	{
		___dispatchScoutRider_27 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchScoutRider_27, value);
	}

	inline static int32_t get_offset_of_dispatchLightCavalry_28() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchLightCavalry_28)); }
	inline InputField_t1631627530 * get_dispatchLightCavalry_28() const { return ___dispatchLightCavalry_28; }
	inline InputField_t1631627530 ** get_address_of_dispatchLightCavalry_28() { return &___dispatchLightCavalry_28; }
	inline void set_dispatchLightCavalry_28(InputField_t1631627530 * value)
	{
		___dispatchLightCavalry_28 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchLightCavalry_28, value);
	}

	inline static int32_t get_offset_of_dispatchHeavyCavalry_29() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchHeavyCavalry_29)); }
	inline InputField_t1631627530 * get_dispatchHeavyCavalry_29() const { return ___dispatchHeavyCavalry_29; }
	inline InputField_t1631627530 ** get_address_of_dispatchHeavyCavalry_29() { return &___dispatchHeavyCavalry_29; }
	inline void set_dispatchHeavyCavalry_29(InputField_t1631627530 * value)
	{
		___dispatchHeavyCavalry_29 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchHeavyCavalry_29, value);
	}

	inline static int32_t get_offset_of_dispatchArcher_30() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchArcher_30)); }
	inline InputField_t1631627530 * get_dispatchArcher_30() const { return ___dispatchArcher_30; }
	inline InputField_t1631627530 ** get_address_of_dispatchArcher_30() { return &___dispatchArcher_30; }
	inline void set_dispatchArcher_30(InputField_t1631627530 * value)
	{
		___dispatchArcher_30 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchArcher_30, value);
	}

	inline static int32_t get_offset_of_dispatchArcherRider_31() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchArcherRider_31)); }
	inline InputField_t1631627530 * get_dispatchArcherRider_31() const { return ___dispatchArcherRider_31; }
	inline InputField_t1631627530 ** get_address_of_dispatchArcherRider_31() { return &___dispatchArcherRider_31; }
	inline void set_dispatchArcherRider_31(InputField_t1631627530 * value)
	{
		___dispatchArcherRider_31 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchArcherRider_31, value);
	}

	inline static int32_t get_offset_of_dispatchHollyman_32() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchHollyman_32)); }
	inline InputField_t1631627530 * get_dispatchHollyman_32() const { return ___dispatchHollyman_32; }
	inline InputField_t1631627530 ** get_address_of_dispatchHollyman_32() { return &___dispatchHollyman_32; }
	inline void set_dispatchHollyman_32(InputField_t1631627530 * value)
	{
		___dispatchHollyman_32 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchHollyman_32, value);
	}

	inline static int32_t get_offset_of_dispatchWagon_33() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchWagon_33)); }
	inline InputField_t1631627530 * get_dispatchWagon_33() const { return ___dispatchWagon_33; }
	inline InputField_t1631627530 ** get_address_of_dispatchWagon_33() { return &___dispatchWagon_33; }
	inline void set_dispatchWagon_33(InputField_t1631627530 * value)
	{
		___dispatchWagon_33 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchWagon_33, value);
	}

	inline static int32_t get_offset_of_dispatchTrebuchet_34() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchTrebuchet_34)); }
	inline InputField_t1631627530 * get_dispatchTrebuchet_34() const { return ___dispatchTrebuchet_34; }
	inline InputField_t1631627530 ** get_address_of_dispatchTrebuchet_34() { return &___dispatchTrebuchet_34; }
	inline void set_dispatchTrebuchet_34(InputField_t1631627530 * value)
	{
		___dispatchTrebuchet_34 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchTrebuchet_34, value);
	}

	inline static int32_t get_offset_of_dispatchSiegeTower_35() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchSiegeTower_35)); }
	inline InputField_t1631627530 * get_dispatchSiegeTower_35() const { return ___dispatchSiegeTower_35; }
	inline InputField_t1631627530 ** get_address_of_dispatchSiegeTower_35() { return &___dispatchSiegeTower_35; }
	inline void set_dispatchSiegeTower_35(InputField_t1631627530 * value)
	{
		___dispatchSiegeTower_35 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchSiegeTower_35, value);
	}

	inline static int32_t get_offset_of_dispatchBatteringRam_36() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchBatteringRam_36)); }
	inline InputField_t1631627530 * get_dispatchBatteringRam_36() const { return ___dispatchBatteringRam_36; }
	inline InputField_t1631627530 ** get_address_of_dispatchBatteringRam_36() { return &___dispatchBatteringRam_36; }
	inline void set_dispatchBatteringRam_36(InputField_t1631627530 * value)
	{
		___dispatchBatteringRam_36 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchBatteringRam_36, value);
	}

	inline static int32_t get_offset_of_dispatchBallista_37() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___dispatchBallista_37)); }
	inline InputField_t1631627530 * get_dispatchBallista_37() const { return ___dispatchBallista_37; }
	inline InputField_t1631627530 ** get_address_of_dispatchBallista_37() { return &___dispatchBallista_37; }
	inline void set_dispatchBallista_37(InputField_t1631627530 * value)
	{
		___dispatchBallista_37 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchBallista_37, value);
	}

	inline static int32_t get_offset_of_xInput_38() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___xInput_38)); }
	inline InputField_t1631627530 * get_xInput_38() const { return ___xInput_38; }
	inline InputField_t1631627530 ** get_address_of_xInput_38() { return &___xInput_38; }
	inline void set_xInput_38(InputField_t1631627530 * value)
	{
		___xInput_38 = value;
		Il2CppCodeGenWriteBarrier(&___xInput_38, value);
	}

	inline static int32_t get_offset_of_yInput_39() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___yInput_39)); }
	inline InputField_t1631627530 * get_yInput_39() const { return ___yInput_39; }
	inline InputField_t1631627530 ** get_address_of_yInput_39() { return &___yInput_39; }
	inline void set_yInput_39(InputField_t1631627530 * value)
	{
		___yInput_39 = value;
		Il2CppCodeGenWriteBarrier(&___yInput_39, value);
	}

	inline static int32_t get_offset_of_targetField_40() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___targetField_40)); }
	inline WorldFieldModel_t3469935653 * get_targetField_40() const { return ___targetField_40; }
	inline WorldFieldModel_t3469935653 ** get_address_of_targetField_40() { return &___targetField_40; }
	inline void set_targetField_40(WorldFieldModel_t3469935653 * value)
	{
		___targetField_40 = value;
		Il2CppCodeGenWriteBarrier(&___targetField_40, value);
	}

	inline static int32_t get_offset_of_onGetBarbariansArmy_41() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___onGetBarbariansArmy_41)); }
	inline SimpleEvent_t1509017202 * get_onGetBarbariansArmy_41() const { return ___onGetBarbariansArmy_41; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetBarbariansArmy_41() { return &___onGetBarbariansArmy_41; }
	inline void set_onGetBarbariansArmy_41(SimpleEvent_t1509017202 * value)
	{
		___onGetBarbariansArmy_41 = value;
		Il2CppCodeGenWriteBarrier(&___onGetBarbariansArmy_41, value);
	}

	inline static int32_t get_offset_of_presentArmy_42() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___presentArmy_42)); }
	inline UnitsModel_t1926818124 * get_presentArmy_42() const { return ___presentArmy_42; }
	inline UnitsModel_t1926818124 ** get_address_of_presentArmy_42() { return &___presentArmy_42; }
	inline void set_presentArmy_42(UnitsModel_t1926818124 * value)
	{
		___presentArmy_42 = value;
		Il2CppCodeGenWriteBarrier(&___presentArmy_42, value);
	}

	inline static int32_t get_offset_of_army_43() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___army_43)); }
	inline UnitsModel_t1926818124 * get_army_43() const { return ___army_43; }
	inline UnitsModel_t1926818124 ** get_address_of_army_43() { return &___army_43; }
	inline void set_army_43(UnitsModel_t1926818124 * value)
	{
		___army_43 = value;
		Il2CppCodeGenWriteBarrier(&___army_43, value);
	}

	inline static int32_t get_offset_of_cityResources_44() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___cityResources_44)); }
	inline ResourcesModel_t2978985958 * get_cityResources_44() const { return ___cityResources_44; }
	inline ResourcesModel_t2978985958 ** get_address_of_cityResources_44() { return &___cityResources_44; }
	inline void set_cityResources_44(ResourcesModel_t2978985958 * value)
	{
		___cityResources_44 = value;
		Il2CppCodeGenWriteBarrier(&___cityResources_44, value);
	}

	inline static int32_t get_offset_of_unitConstants_45() { return static_cast<int32_t>(offsetof(BarbarianRecruitManager_t1320026565, ___unitConstants_45)); }
	inline Dictionary_2_t3722048895 * get_unitConstants_45() const { return ___unitConstants_45; }
	inline Dictionary_2_t3722048895 ** get_address_of_unitConstants_45() { return &___unitConstants_45; }
	inline void set_unitConstants_45(Dictionary_2_t3722048895 * value)
	{
		___unitConstants_45 = value;
		Il2CppCodeGenWriteBarrier(&___unitConstants_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
