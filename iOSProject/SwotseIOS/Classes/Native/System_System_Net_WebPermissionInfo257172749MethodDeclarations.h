﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermissionInfo
struct WebPermissionInfo_t257172749;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_WebPermissionInfoType435897941.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"

// System.Void System.Net.WebPermissionInfo::.ctor(System.Net.WebPermissionInfoType,System.String)
extern "C"  void WebPermissionInfo__ctor_m4178404710 (WebPermissionInfo_t257172749 * __this, int32_t ___type0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionInfo::.ctor(System.Text.RegularExpressions.Regex)
extern "C"  void WebPermissionInfo__ctor_m3573721761 (WebPermissionInfo_t257172749 * __this, Regex_t1803876613 * ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionInfo::get_Info()
extern "C"  String_t* WebPermissionInfo_get_Info_m1979205491 (WebPermissionInfo_t257172749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
