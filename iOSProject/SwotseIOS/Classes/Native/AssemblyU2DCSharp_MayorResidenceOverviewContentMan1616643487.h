﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MayorResidenceOverviewContentManager
struct  MayorResidenceOverviewContentManager_t1616643487  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityName
	Text_t356221433 * ___cityName_2;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityRegion
	Text_t356221433 * ___cityRegion_3;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityCarrage
	Text_t356221433 * ___cityCarrage_4;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityHappiness
	Text_t356221433 * ___cityHappiness_5;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityPopulation
	Text_t356221433 * ___cityPopulation_6;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityIdlePopulation
	Text_t356221433 * ___cityIdlePopulation_7;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::cityTax
	Text_t356221433 * ___cityTax_8;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::playerWins
	Text_t356221433 * ___playerWins_9;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::barbarianWins
	Text_t356221433 * ___barbarianWins_10;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::playerLoses
	Text_t356221433 * ___playerLoses_11;
	// UnityEngine.UI.Text MayorResidenceOverviewContentManager::barbarianLoses
	Text_t356221433 * ___barbarianLoses_12;
	// UnityEngine.UI.Dropdown MayorResidenceOverviewContentManager::taxDropdown
	Dropdown_t1985816271 * ___taxDropdown_13;
	// System.Int64 MayorResidenceOverviewContentManager::taxIndex
	int64_t ___taxIndex_14;
	// UnityEngine.UI.InputField MayorResidenceOverviewContentManager::cityNameInput
	InputField_t1631627530 * ___cityNameInput_15;
	// UnityEngine.UI.Image MayorResidenceOverviewContentManager::cityIcon
	Image_t2042527209 * ___cityIcon_16;
	// System.Int64 MayorResidenceOverviewContentManager::iconIndex
	int64_t ___iconIndex_17;
	// System.String MayorResidenceOverviewContentManager::userImageName
	String_t* ___userImageName_18;

public:
	inline static int32_t get_offset_of_cityName_2() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityName_2)); }
	inline Text_t356221433 * get_cityName_2() const { return ___cityName_2; }
	inline Text_t356221433 ** get_address_of_cityName_2() { return &___cityName_2; }
	inline void set_cityName_2(Text_t356221433 * value)
	{
		___cityName_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_2, value);
	}

	inline static int32_t get_offset_of_cityRegion_3() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityRegion_3)); }
	inline Text_t356221433 * get_cityRegion_3() const { return ___cityRegion_3; }
	inline Text_t356221433 ** get_address_of_cityRegion_3() { return &___cityRegion_3; }
	inline void set_cityRegion_3(Text_t356221433 * value)
	{
		___cityRegion_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityRegion_3, value);
	}

	inline static int32_t get_offset_of_cityCarrage_4() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityCarrage_4)); }
	inline Text_t356221433 * get_cityCarrage_4() const { return ___cityCarrage_4; }
	inline Text_t356221433 ** get_address_of_cityCarrage_4() { return &___cityCarrage_4; }
	inline void set_cityCarrage_4(Text_t356221433 * value)
	{
		___cityCarrage_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityCarrage_4, value);
	}

	inline static int32_t get_offset_of_cityHappiness_5() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityHappiness_5)); }
	inline Text_t356221433 * get_cityHappiness_5() const { return ___cityHappiness_5; }
	inline Text_t356221433 ** get_address_of_cityHappiness_5() { return &___cityHappiness_5; }
	inline void set_cityHappiness_5(Text_t356221433 * value)
	{
		___cityHappiness_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityHappiness_5, value);
	}

	inline static int32_t get_offset_of_cityPopulation_6() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityPopulation_6)); }
	inline Text_t356221433 * get_cityPopulation_6() const { return ___cityPopulation_6; }
	inline Text_t356221433 ** get_address_of_cityPopulation_6() { return &___cityPopulation_6; }
	inline void set_cityPopulation_6(Text_t356221433 * value)
	{
		___cityPopulation_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityPopulation_6, value);
	}

	inline static int32_t get_offset_of_cityIdlePopulation_7() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityIdlePopulation_7)); }
	inline Text_t356221433 * get_cityIdlePopulation_7() const { return ___cityIdlePopulation_7; }
	inline Text_t356221433 ** get_address_of_cityIdlePopulation_7() { return &___cityIdlePopulation_7; }
	inline void set_cityIdlePopulation_7(Text_t356221433 * value)
	{
		___cityIdlePopulation_7 = value;
		Il2CppCodeGenWriteBarrier(&___cityIdlePopulation_7, value);
	}

	inline static int32_t get_offset_of_cityTax_8() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityTax_8)); }
	inline Text_t356221433 * get_cityTax_8() const { return ___cityTax_8; }
	inline Text_t356221433 ** get_address_of_cityTax_8() { return &___cityTax_8; }
	inline void set_cityTax_8(Text_t356221433 * value)
	{
		___cityTax_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityTax_8, value);
	}

	inline static int32_t get_offset_of_playerWins_9() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___playerWins_9)); }
	inline Text_t356221433 * get_playerWins_9() const { return ___playerWins_9; }
	inline Text_t356221433 ** get_address_of_playerWins_9() { return &___playerWins_9; }
	inline void set_playerWins_9(Text_t356221433 * value)
	{
		___playerWins_9 = value;
		Il2CppCodeGenWriteBarrier(&___playerWins_9, value);
	}

	inline static int32_t get_offset_of_barbarianWins_10() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___barbarianWins_10)); }
	inline Text_t356221433 * get_barbarianWins_10() const { return ___barbarianWins_10; }
	inline Text_t356221433 ** get_address_of_barbarianWins_10() { return &___barbarianWins_10; }
	inline void set_barbarianWins_10(Text_t356221433 * value)
	{
		___barbarianWins_10 = value;
		Il2CppCodeGenWriteBarrier(&___barbarianWins_10, value);
	}

	inline static int32_t get_offset_of_playerLoses_11() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___playerLoses_11)); }
	inline Text_t356221433 * get_playerLoses_11() const { return ___playerLoses_11; }
	inline Text_t356221433 ** get_address_of_playerLoses_11() { return &___playerLoses_11; }
	inline void set_playerLoses_11(Text_t356221433 * value)
	{
		___playerLoses_11 = value;
		Il2CppCodeGenWriteBarrier(&___playerLoses_11, value);
	}

	inline static int32_t get_offset_of_barbarianLoses_12() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___barbarianLoses_12)); }
	inline Text_t356221433 * get_barbarianLoses_12() const { return ___barbarianLoses_12; }
	inline Text_t356221433 ** get_address_of_barbarianLoses_12() { return &___barbarianLoses_12; }
	inline void set_barbarianLoses_12(Text_t356221433 * value)
	{
		___barbarianLoses_12 = value;
		Il2CppCodeGenWriteBarrier(&___barbarianLoses_12, value);
	}

	inline static int32_t get_offset_of_taxDropdown_13() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___taxDropdown_13)); }
	inline Dropdown_t1985816271 * get_taxDropdown_13() const { return ___taxDropdown_13; }
	inline Dropdown_t1985816271 ** get_address_of_taxDropdown_13() { return &___taxDropdown_13; }
	inline void set_taxDropdown_13(Dropdown_t1985816271 * value)
	{
		___taxDropdown_13 = value;
		Il2CppCodeGenWriteBarrier(&___taxDropdown_13, value);
	}

	inline static int32_t get_offset_of_taxIndex_14() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___taxIndex_14)); }
	inline int64_t get_taxIndex_14() const { return ___taxIndex_14; }
	inline int64_t* get_address_of_taxIndex_14() { return &___taxIndex_14; }
	inline void set_taxIndex_14(int64_t value)
	{
		___taxIndex_14 = value;
	}

	inline static int32_t get_offset_of_cityNameInput_15() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityNameInput_15)); }
	inline InputField_t1631627530 * get_cityNameInput_15() const { return ___cityNameInput_15; }
	inline InputField_t1631627530 ** get_address_of_cityNameInput_15() { return &___cityNameInput_15; }
	inline void set_cityNameInput_15(InputField_t1631627530 * value)
	{
		___cityNameInput_15 = value;
		Il2CppCodeGenWriteBarrier(&___cityNameInput_15, value);
	}

	inline static int32_t get_offset_of_cityIcon_16() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___cityIcon_16)); }
	inline Image_t2042527209 * get_cityIcon_16() const { return ___cityIcon_16; }
	inline Image_t2042527209 ** get_address_of_cityIcon_16() { return &___cityIcon_16; }
	inline void set_cityIcon_16(Image_t2042527209 * value)
	{
		___cityIcon_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityIcon_16, value);
	}

	inline static int32_t get_offset_of_iconIndex_17() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___iconIndex_17)); }
	inline int64_t get_iconIndex_17() const { return ___iconIndex_17; }
	inline int64_t* get_address_of_iconIndex_17() { return &___iconIndex_17; }
	inline void set_iconIndex_17(int64_t value)
	{
		___iconIndex_17 = value;
	}

	inline static int32_t get_offset_of_userImageName_18() { return static_cast<int32_t>(offsetof(MayorResidenceOverviewContentManager_t1616643487, ___userImageName_18)); }
	inline String_t* get_userImageName_18() const { return ___userImageName_18; }
	inline String_t** get_address_of_userImageName_18() { return &___userImageName_18; }
	inline void set_userImageName_18(String_t* value)
	{
		___userImageName_18 = value;
		Il2CppCodeGenWriteBarrier(&___userImageName_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
