﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermissionAttribute
struct WebPermissionAttribute_t1695034971;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t182075948;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.WebPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C"  void WebPermissionAttribute__ctor_m1403912392 (WebPermissionAttribute_t1695034971 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_Accept()
extern "C"  String_t* WebPermissionAttribute_get_Accept_m4095187359 (WebPermissionAttribute_t1695034971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_Accept(System.String)
extern "C"  void WebPermissionAttribute_set_Accept_m1546938096 (WebPermissionAttribute_t1695034971 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_AcceptPattern()
extern "C"  String_t* WebPermissionAttribute_get_AcceptPattern_m974710631 (WebPermissionAttribute_t1695034971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_AcceptPattern(System.String)
extern "C"  void WebPermissionAttribute_set_AcceptPattern_m867324824 (WebPermissionAttribute_t1695034971 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_Connect()
extern "C"  String_t* WebPermissionAttribute_get_Connect_m2154526997 (WebPermissionAttribute_t1695034971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_Connect(System.String)
extern "C"  void WebPermissionAttribute_set_Connect_m670629100 (WebPermissionAttribute_t1695034971 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_ConnectPattern()
extern "C"  String_t* WebPermissionAttribute_get_ConnectPattern_m4100731881 (WebPermissionAttribute_t1695034971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_ConnectPattern(System.String)
extern "C"  void WebPermissionAttribute_set_ConnectPattern_m51327092 (WebPermissionAttribute_t1695034971 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermissionAttribute::CreatePermission()
extern "C"  Il2CppObject * WebPermissionAttribute_CreatePermission_m224295946 (WebPermissionAttribute_t1695034971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::AlreadySet(System.String,System.String)
extern "C"  void WebPermissionAttribute_AlreadySet_m244746311 (WebPermissionAttribute_t1695034971 * __this, String_t* ___parameter0, String_t* ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
