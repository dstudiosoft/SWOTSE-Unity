﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task`1<Firebase.DependencyStatus>
struct Task_1_t1872448422;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Action`1<System.Threading.Tasks.Task`1<Firebase.DependencyStatus>>
struct Action_1_t1674247804;
// System.AggregateException
struct AggregateException_t420812976;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_DependencyStatus2752419415.h"
#include "Unity_Tasks_System_AggregateException420812976.h"

// System.Void System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::.ctor()
extern "C"  void Task_1__ctor_m2327588621_gshared (Task_1_t1872448422 * __this, const MethodInfo* method);
#define Task_1__ctor_m2327588621(__this, method) ((  void (*) (Task_1_t1872448422 *, const MethodInfo*))Task_1__ctor_m2327588621_gshared)(__this, method)
// T System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::get_Result()
extern "C"  int32_t Task_1_get_Result_m1906501416_gshared (Task_1_t1872448422 * __this, const MethodInfo* method);
#define Task_1_get_Result_m1906501416(__this, method) ((  int32_t (*) (Task_1_t1872448422 *, const MethodInfo*))Task_1_get_Result_m1906501416_gshared)(__this, method)
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<T>>)
extern "C"  Task_t1843236107 * Task_1_ContinueWith_m1196060538_gshared (Task_1_t1872448422 * __this, Action_1_t1674247804 * ___continuation0, const MethodInfo* method);
#define Task_1_ContinueWith_m1196060538(__this, ___continuation0, method) ((  Task_t1843236107 * (*) (Task_1_t1872448422 *, Action_1_t1674247804 *, const MethodInfo*))Task_1_ContinueWith_m1196060538_gshared)(__this, ___continuation0, method)
// System.Void System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::RunContinuations()
extern "C"  void Task_1_RunContinuations_m2238342776_gshared (Task_1_t1872448422 * __this, const MethodInfo* method);
#define Task_1_RunContinuations_m2238342776(__this, method) ((  void (*) (Task_1_t1872448422 *, const MethodInfo*))Task_1_RunContinuations_m2238342776_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::TrySetResult(T)
extern "C"  bool Task_1_TrySetResult_m3293807305_gshared (Task_1_t1872448422 * __this, int32_t ___result0, const MethodInfo* method);
#define Task_1_TrySetResult_m3293807305(__this, ___result0, method) ((  bool (*) (Task_1_t1872448422 *, int32_t, const MethodInfo*))Task_1_TrySetResult_m3293807305_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::TrySetCanceled()
extern "C"  bool Task_1_TrySetCanceled_m4247399799_gshared (Task_1_t1872448422 * __this, const MethodInfo* method);
#define Task_1_TrySetCanceled_m4247399799(__this, method) ((  bool (*) (Task_1_t1872448422 *, const MethodInfo*))Task_1_TrySetCanceled_m4247399799_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.DependencyStatus>::TrySetException(System.AggregateException)
extern "C"  bool Task_1_TrySetException_m3546807364_gshared (Task_1_t1872448422 * __this, AggregateException_t420812976 * ___exception0, const MethodInfo* method);
#define Task_1_TrySetException_m3546807364(__this, ___exception0, method) ((  bool (*) (Task_1_t1872448422 *, AggregateException_t420812976 *, const MethodInfo*))Task_1_TrySetException_m3546807364_gshared)(__this, ___exception0, method)
