﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnitySynchronizationContext
struct UnitySynchronizationContext_t2114871256;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t296893742;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Unity.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext__ctor_m1511338833 (UnitySynchronizationContext_t2114871256 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnitySynchronizationContext::Install(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext_Install_m2797918412 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern "C"  void UnitySynchronizationContext_Post_m4183392490 (UnitySynchronizationContext_t2114871256 * __this, SendOrPostCallback_t296893742 * ___d0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Unity.UnitySynchronizationContext::.cctor()
extern "C"  void UnitySynchronizationContext__cctor_m2277019916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
