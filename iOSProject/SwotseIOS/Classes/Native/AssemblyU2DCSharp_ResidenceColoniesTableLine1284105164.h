﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ResidenceColoniesTableController
struct ResidenceColoniesTableController_t2206090190;
// ResourcesModel
struct ResourcesModel_t2978985958;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResidenceColoniesTableLine
struct  ResidenceColoniesTableLine_t1284105164  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text ResidenceColoniesTableLine::playerName
	Text_t356221433 * ___playerName_2;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::cityName
	Text_t356221433 * ___cityName_3;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::region
	Text_t356221433 * ___region_4;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::cityCoords
	Text_t356221433 * ___cityCoords_5;
	// UnityEngine.UI.Text ResidenceColoniesTableLine::mayerLevel
	Text_t356221433 * ___mayerLevel_6;
	// ResidenceColoniesTableController ResidenceColoniesTableLine::owner
	ResidenceColoniesTableController_t2206090190 * ___owner_7;
	// ResourcesModel ResidenceColoniesTableLine::tributes
	ResourcesModel_t2978985958 * ___tributes_8;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___playerName_2)); }
	inline Text_t356221433 * get_playerName_2() const { return ___playerName_2; }
	inline Text_t356221433 ** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(Text_t356221433 * value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_2, value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___cityName_3)); }
	inline Text_t356221433 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t356221433 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t356221433 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_3, value);
	}

	inline static int32_t get_offset_of_region_4() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___region_4)); }
	inline Text_t356221433 * get_region_4() const { return ___region_4; }
	inline Text_t356221433 ** get_address_of_region_4() { return &___region_4; }
	inline void set_region_4(Text_t356221433 * value)
	{
		___region_4 = value;
		Il2CppCodeGenWriteBarrier(&___region_4, value);
	}

	inline static int32_t get_offset_of_cityCoords_5() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___cityCoords_5)); }
	inline Text_t356221433 * get_cityCoords_5() const { return ___cityCoords_5; }
	inline Text_t356221433 ** get_address_of_cityCoords_5() { return &___cityCoords_5; }
	inline void set_cityCoords_5(Text_t356221433 * value)
	{
		___cityCoords_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityCoords_5, value);
	}

	inline static int32_t get_offset_of_mayerLevel_6() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___mayerLevel_6)); }
	inline Text_t356221433 * get_mayerLevel_6() const { return ___mayerLevel_6; }
	inline Text_t356221433 ** get_address_of_mayerLevel_6() { return &___mayerLevel_6; }
	inline void set_mayerLevel_6(Text_t356221433 * value)
	{
		___mayerLevel_6 = value;
		Il2CppCodeGenWriteBarrier(&___mayerLevel_6, value);
	}

	inline static int32_t get_offset_of_owner_7() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___owner_7)); }
	inline ResidenceColoniesTableController_t2206090190 * get_owner_7() const { return ___owner_7; }
	inline ResidenceColoniesTableController_t2206090190 ** get_address_of_owner_7() { return &___owner_7; }
	inline void set_owner_7(ResidenceColoniesTableController_t2206090190 * value)
	{
		___owner_7 = value;
		Il2CppCodeGenWriteBarrier(&___owner_7, value);
	}

	inline static int32_t get_offset_of_tributes_8() { return static_cast<int32_t>(offsetof(ResidenceColoniesTableLine_t1284105164, ___tributes_8)); }
	inline ResourcesModel_t2978985958 * get_tributes_8() const { return ___tributes_8; }
	inline ResourcesModel_t2978985958 ** get_address_of_tributes_8() { return &___tributes_8; }
	inline void set_tributes_8(ResourcesModel_t2978985958 * value)
	{
		___tributes_8 = value;
		Il2CppCodeGenWriteBarrier(&___tributes_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
