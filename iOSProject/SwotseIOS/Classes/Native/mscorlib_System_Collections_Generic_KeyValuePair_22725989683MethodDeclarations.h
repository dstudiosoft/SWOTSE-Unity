﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1599650755(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2725989683 *, Gesture_t2352305985 *, List_1_t328750215 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::get_Key()
#define KeyValuePair_2_get_Key_m1742675165(__this, method) ((  Gesture_t2352305985 * (*) (KeyValuePair_2_t2725989683 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m562586832(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2725989683 *, Gesture_t2352305985 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::get_Value()
#define KeyValuePair_2_get_Value_m5999845(__this, method) ((  List_1_t328750215 * (*) (KeyValuePair_2_t2725989683 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1210753832(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2725989683 *, List_1_t328750215 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>::ToString()
#define KeyValuePair_2_ToString_m1358767218(__this, method) ((  String_t* (*) (KeyValuePair_2_t2725989683 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
