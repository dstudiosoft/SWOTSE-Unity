﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// CommandCenterArmiesTableController
struct CommandCenterArmiesTableController_t2450766557;
// ArmyModel
struct ArmyModel_t2157120938;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArmyDetailsContentManager
struct  ArmyDetailsContentManager_t106850373  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ArmyDetailsContentManager::coords
	Text_t356221433 * ___coords_2;
	// UnityEngine.UI.Text ArmyDetailsContentManager::cityName
	Text_t356221433 * ___cityName_3;
	// UnityEngine.UI.Text ArmyDetailsContentManager::targetPlayer
	Text_t356221433 * ___targetPlayer_4;
	// System.Int64 ArmyDetailsContentManager::armyId
	int64_t ___armyId_5;
	// CommandCenterArmiesTableController ArmyDetailsContentManager::owner
	CommandCenterArmiesTableController_t2450766557 * ___owner_6;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyWorker
	Text_t356221433 * ___armyWorker_7;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySpy
	Text_t356221433 * ___armySpy_8;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySwordsman
	Text_t356221433 * ___armySwordsman_9;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySpearman
	Text_t356221433 * ___armySpearman_10;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyPikeman
	Text_t356221433 * ___armyPikeman_11;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyScoutRider
	Text_t356221433 * ___armyScoutRider_12;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyLightCavalry
	Text_t356221433 * ___armyLightCavalry_13;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyHeavyCavalry
	Text_t356221433 * ___armyHeavyCavalry_14;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyArcher
	Text_t356221433 * ___armyArcher_15;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyArcherRider
	Text_t356221433 * ___armyArcherRider_16;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyHollyman
	Text_t356221433 * ___armyHollyman_17;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyWagon
	Text_t356221433 * ___armyWagon_18;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyTrebuchet
	Text_t356221433 * ___armyTrebuchet_19;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armySiegeTower
	Text_t356221433 * ___armySiegeTower_20;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyBatteringRam
	Text_t356221433 * ___armyBatteringRam_21;
	// UnityEngine.UI.Text ArmyDetailsContentManager::armyBallista
	Text_t356221433 * ___armyBallista_22;
	// ArmyModel ArmyDetailsContentManager::army
	ArmyModel_t2157120938 * ___army_23;
	// SimpleEvent ArmyDetailsContentManager::gotReturnArmy
	SimpleEvent_t1509017202 * ___gotReturnArmy_24;

public:
	inline static int32_t get_offset_of_coords_2() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___coords_2)); }
	inline Text_t356221433 * get_coords_2() const { return ___coords_2; }
	inline Text_t356221433 ** get_address_of_coords_2() { return &___coords_2; }
	inline void set_coords_2(Text_t356221433 * value)
	{
		___coords_2 = value;
		Il2CppCodeGenWriteBarrier(&___coords_2, value);
	}

	inline static int32_t get_offset_of_cityName_3() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___cityName_3)); }
	inline Text_t356221433 * get_cityName_3() const { return ___cityName_3; }
	inline Text_t356221433 ** get_address_of_cityName_3() { return &___cityName_3; }
	inline void set_cityName_3(Text_t356221433 * value)
	{
		___cityName_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_3, value);
	}

	inline static int32_t get_offset_of_targetPlayer_4() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___targetPlayer_4)); }
	inline Text_t356221433 * get_targetPlayer_4() const { return ___targetPlayer_4; }
	inline Text_t356221433 ** get_address_of_targetPlayer_4() { return &___targetPlayer_4; }
	inline void set_targetPlayer_4(Text_t356221433 * value)
	{
		___targetPlayer_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetPlayer_4, value);
	}

	inline static int32_t get_offset_of_armyId_5() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyId_5)); }
	inline int64_t get_armyId_5() const { return ___armyId_5; }
	inline int64_t* get_address_of_armyId_5() { return &___armyId_5; }
	inline void set_armyId_5(int64_t value)
	{
		___armyId_5 = value;
	}

	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___owner_6)); }
	inline CommandCenterArmiesTableController_t2450766557 * get_owner_6() const { return ___owner_6; }
	inline CommandCenterArmiesTableController_t2450766557 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(CommandCenterArmiesTableController_t2450766557 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier(&___owner_6, value);
	}

	inline static int32_t get_offset_of_armyWorker_7() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyWorker_7)); }
	inline Text_t356221433 * get_armyWorker_7() const { return ___armyWorker_7; }
	inline Text_t356221433 ** get_address_of_armyWorker_7() { return &___armyWorker_7; }
	inline void set_armyWorker_7(Text_t356221433 * value)
	{
		___armyWorker_7 = value;
		Il2CppCodeGenWriteBarrier(&___armyWorker_7, value);
	}

	inline static int32_t get_offset_of_armySpy_8() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armySpy_8)); }
	inline Text_t356221433 * get_armySpy_8() const { return ___armySpy_8; }
	inline Text_t356221433 ** get_address_of_armySpy_8() { return &___armySpy_8; }
	inline void set_armySpy_8(Text_t356221433 * value)
	{
		___armySpy_8 = value;
		Il2CppCodeGenWriteBarrier(&___armySpy_8, value);
	}

	inline static int32_t get_offset_of_armySwordsman_9() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armySwordsman_9)); }
	inline Text_t356221433 * get_armySwordsman_9() const { return ___armySwordsman_9; }
	inline Text_t356221433 ** get_address_of_armySwordsman_9() { return &___armySwordsman_9; }
	inline void set_armySwordsman_9(Text_t356221433 * value)
	{
		___armySwordsman_9 = value;
		Il2CppCodeGenWriteBarrier(&___armySwordsman_9, value);
	}

	inline static int32_t get_offset_of_armySpearman_10() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armySpearman_10)); }
	inline Text_t356221433 * get_armySpearman_10() const { return ___armySpearman_10; }
	inline Text_t356221433 ** get_address_of_armySpearman_10() { return &___armySpearman_10; }
	inline void set_armySpearman_10(Text_t356221433 * value)
	{
		___armySpearman_10 = value;
		Il2CppCodeGenWriteBarrier(&___armySpearman_10, value);
	}

	inline static int32_t get_offset_of_armyPikeman_11() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyPikeman_11)); }
	inline Text_t356221433 * get_armyPikeman_11() const { return ___armyPikeman_11; }
	inline Text_t356221433 ** get_address_of_armyPikeman_11() { return &___armyPikeman_11; }
	inline void set_armyPikeman_11(Text_t356221433 * value)
	{
		___armyPikeman_11 = value;
		Il2CppCodeGenWriteBarrier(&___armyPikeman_11, value);
	}

	inline static int32_t get_offset_of_armyScoutRider_12() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyScoutRider_12)); }
	inline Text_t356221433 * get_armyScoutRider_12() const { return ___armyScoutRider_12; }
	inline Text_t356221433 ** get_address_of_armyScoutRider_12() { return &___armyScoutRider_12; }
	inline void set_armyScoutRider_12(Text_t356221433 * value)
	{
		___armyScoutRider_12 = value;
		Il2CppCodeGenWriteBarrier(&___armyScoutRider_12, value);
	}

	inline static int32_t get_offset_of_armyLightCavalry_13() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyLightCavalry_13)); }
	inline Text_t356221433 * get_armyLightCavalry_13() const { return ___armyLightCavalry_13; }
	inline Text_t356221433 ** get_address_of_armyLightCavalry_13() { return &___armyLightCavalry_13; }
	inline void set_armyLightCavalry_13(Text_t356221433 * value)
	{
		___armyLightCavalry_13 = value;
		Il2CppCodeGenWriteBarrier(&___armyLightCavalry_13, value);
	}

	inline static int32_t get_offset_of_armyHeavyCavalry_14() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyHeavyCavalry_14)); }
	inline Text_t356221433 * get_armyHeavyCavalry_14() const { return ___armyHeavyCavalry_14; }
	inline Text_t356221433 ** get_address_of_armyHeavyCavalry_14() { return &___armyHeavyCavalry_14; }
	inline void set_armyHeavyCavalry_14(Text_t356221433 * value)
	{
		___armyHeavyCavalry_14 = value;
		Il2CppCodeGenWriteBarrier(&___armyHeavyCavalry_14, value);
	}

	inline static int32_t get_offset_of_armyArcher_15() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyArcher_15)); }
	inline Text_t356221433 * get_armyArcher_15() const { return ___armyArcher_15; }
	inline Text_t356221433 ** get_address_of_armyArcher_15() { return &___armyArcher_15; }
	inline void set_armyArcher_15(Text_t356221433 * value)
	{
		___armyArcher_15 = value;
		Il2CppCodeGenWriteBarrier(&___armyArcher_15, value);
	}

	inline static int32_t get_offset_of_armyArcherRider_16() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyArcherRider_16)); }
	inline Text_t356221433 * get_armyArcherRider_16() const { return ___armyArcherRider_16; }
	inline Text_t356221433 ** get_address_of_armyArcherRider_16() { return &___armyArcherRider_16; }
	inline void set_armyArcherRider_16(Text_t356221433 * value)
	{
		___armyArcherRider_16 = value;
		Il2CppCodeGenWriteBarrier(&___armyArcherRider_16, value);
	}

	inline static int32_t get_offset_of_armyHollyman_17() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyHollyman_17)); }
	inline Text_t356221433 * get_armyHollyman_17() const { return ___armyHollyman_17; }
	inline Text_t356221433 ** get_address_of_armyHollyman_17() { return &___armyHollyman_17; }
	inline void set_armyHollyman_17(Text_t356221433 * value)
	{
		___armyHollyman_17 = value;
		Il2CppCodeGenWriteBarrier(&___armyHollyman_17, value);
	}

	inline static int32_t get_offset_of_armyWagon_18() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyWagon_18)); }
	inline Text_t356221433 * get_armyWagon_18() const { return ___armyWagon_18; }
	inline Text_t356221433 ** get_address_of_armyWagon_18() { return &___armyWagon_18; }
	inline void set_armyWagon_18(Text_t356221433 * value)
	{
		___armyWagon_18 = value;
		Il2CppCodeGenWriteBarrier(&___armyWagon_18, value);
	}

	inline static int32_t get_offset_of_armyTrebuchet_19() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyTrebuchet_19)); }
	inline Text_t356221433 * get_armyTrebuchet_19() const { return ___armyTrebuchet_19; }
	inline Text_t356221433 ** get_address_of_armyTrebuchet_19() { return &___armyTrebuchet_19; }
	inline void set_armyTrebuchet_19(Text_t356221433 * value)
	{
		___armyTrebuchet_19 = value;
		Il2CppCodeGenWriteBarrier(&___armyTrebuchet_19, value);
	}

	inline static int32_t get_offset_of_armySiegeTower_20() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armySiegeTower_20)); }
	inline Text_t356221433 * get_armySiegeTower_20() const { return ___armySiegeTower_20; }
	inline Text_t356221433 ** get_address_of_armySiegeTower_20() { return &___armySiegeTower_20; }
	inline void set_armySiegeTower_20(Text_t356221433 * value)
	{
		___armySiegeTower_20 = value;
		Il2CppCodeGenWriteBarrier(&___armySiegeTower_20, value);
	}

	inline static int32_t get_offset_of_armyBatteringRam_21() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyBatteringRam_21)); }
	inline Text_t356221433 * get_armyBatteringRam_21() const { return ___armyBatteringRam_21; }
	inline Text_t356221433 ** get_address_of_armyBatteringRam_21() { return &___armyBatteringRam_21; }
	inline void set_armyBatteringRam_21(Text_t356221433 * value)
	{
		___armyBatteringRam_21 = value;
		Il2CppCodeGenWriteBarrier(&___armyBatteringRam_21, value);
	}

	inline static int32_t get_offset_of_armyBallista_22() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___armyBallista_22)); }
	inline Text_t356221433 * get_armyBallista_22() const { return ___armyBallista_22; }
	inline Text_t356221433 ** get_address_of_armyBallista_22() { return &___armyBallista_22; }
	inline void set_armyBallista_22(Text_t356221433 * value)
	{
		___armyBallista_22 = value;
		Il2CppCodeGenWriteBarrier(&___armyBallista_22, value);
	}

	inline static int32_t get_offset_of_army_23() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___army_23)); }
	inline ArmyModel_t2157120938 * get_army_23() const { return ___army_23; }
	inline ArmyModel_t2157120938 ** get_address_of_army_23() { return &___army_23; }
	inline void set_army_23(ArmyModel_t2157120938 * value)
	{
		___army_23 = value;
		Il2CppCodeGenWriteBarrier(&___army_23, value);
	}

	inline static int32_t get_offset_of_gotReturnArmy_24() { return static_cast<int32_t>(offsetof(ArmyDetailsContentManager_t106850373, ___gotReturnArmy_24)); }
	inline SimpleEvent_t1509017202 * get_gotReturnArmy_24() const { return ___gotReturnArmy_24; }
	inline SimpleEvent_t1509017202 ** get_address_of_gotReturnArmy_24() { return &___gotReturnArmy_24; }
	inline void set_gotReturnArmy_24(SimpleEvent_t1509017202 * value)
	{
		___gotReturnArmy_24 = value;
		Il2CppCodeGenWriteBarrier(&___gotReturnArmy_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
