﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// WorldFieldModel
struct WorldFieldModel_t3469935653;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldManager
struct  WorldFieldManager_t1451927541  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 WorldFieldManager::internalPitId
	int32_t ___internalPitId_2;
	// UnityEngine.GameObject WorldFieldManager::fieldObject
	GameObject_t1756533147 * ___fieldObject_3;
	// UnityEngine.GameObject WorldFieldManager::flag
	GameObject_t1756533147 * ___flag_4;
	// UnityEngine.GameObject WorldFieldManager::occupationIcon
	GameObject_t1756533147 * ___occupationIcon_5;
	// WorldFieldModel WorldFieldManager::fieldInfo
	WorldFieldModel_t3469935653 * ___fieldInfo_6;

public:
	inline static int32_t get_offset_of_internalPitId_2() { return static_cast<int32_t>(offsetof(WorldFieldManager_t1451927541, ___internalPitId_2)); }
	inline int32_t get_internalPitId_2() const { return ___internalPitId_2; }
	inline int32_t* get_address_of_internalPitId_2() { return &___internalPitId_2; }
	inline void set_internalPitId_2(int32_t value)
	{
		___internalPitId_2 = value;
	}

	inline static int32_t get_offset_of_fieldObject_3() { return static_cast<int32_t>(offsetof(WorldFieldManager_t1451927541, ___fieldObject_3)); }
	inline GameObject_t1756533147 * get_fieldObject_3() const { return ___fieldObject_3; }
	inline GameObject_t1756533147 ** get_address_of_fieldObject_3() { return &___fieldObject_3; }
	inline void set_fieldObject_3(GameObject_t1756533147 * value)
	{
		___fieldObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___fieldObject_3, value);
	}

	inline static int32_t get_offset_of_flag_4() { return static_cast<int32_t>(offsetof(WorldFieldManager_t1451927541, ___flag_4)); }
	inline GameObject_t1756533147 * get_flag_4() const { return ___flag_4; }
	inline GameObject_t1756533147 ** get_address_of_flag_4() { return &___flag_4; }
	inline void set_flag_4(GameObject_t1756533147 * value)
	{
		___flag_4 = value;
		Il2CppCodeGenWriteBarrier(&___flag_4, value);
	}

	inline static int32_t get_offset_of_occupationIcon_5() { return static_cast<int32_t>(offsetof(WorldFieldManager_t1451927541, ___occupationIcon_5)); }
	inline GameObject_t1756533147 * get_occupationIcon_5() const { return ___occupationIcon_5; }
	inline GameObject_t1756533147 ** get_address_of_occupationIcon_5() { return &___occupationIcon_5; }
	inline void set_occupationIcon_5(GameObject_t1756533147 * value)
	{
		___occupationIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___occupationIcon_5, value);
	}

	inline static int32_t get_offset_of_fieldInfo_6() { return static_cast<int32_t>(offsetof(WorldFieldManager_t1451927541, ___fieldInfo_6)); }
	inline WorldFieldModel_t3469935653 * get_fieldInfo_6() const { return ___fieldInfo_6; }
	inline WorldFieldModel_t3469935653 ** get_address_of_fieldInfo_6() { return &___fieldInfo_6; }
	inline void set_fieldInfo_6(WorldFieldModel_t3469935653 * value)
	{
		___fieldInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___fieldInfo_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
