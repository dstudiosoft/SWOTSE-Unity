﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/OrganizationName
struct OrganizationName_t3660474869;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/OrganizationName::.ctor()
extern "C"  void OrganizationName__ctor_m1219782191 (OrganizationName_t3660474869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
