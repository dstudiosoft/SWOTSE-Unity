﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Firebase.Variant
struct Variant_t4275788079;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;
// Firebase.VariantVariantMap
struct VariantVariantMap_t4097173110;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_Variant4275788079.h"
#include "Firebase_App_Firebase_VariantVariantMap4097173110.h"

// System.Object Firebase.VariantExtension::ToObject(Firebase.Variant)
extern "C"  Il2CppObject * VariantExtension_ToObject_m3593960939 (Il2CppObject * __this /* static, unused */, Variant_t4275788079 * ___variant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Firebase.VariantExtension::ToStringVariantMap(Firebase.VariantVariantMap)
extern "C"  Il2CppObject* VariantExtension_ToStringVariantMap_m3457145879 (Il2CppObject * __this /* static, unused */, VariantVariantMap_t4097173110 * ___originalMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
