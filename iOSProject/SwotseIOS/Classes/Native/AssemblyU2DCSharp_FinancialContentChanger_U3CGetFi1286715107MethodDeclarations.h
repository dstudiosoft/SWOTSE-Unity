﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinancialContentChanger/<GetFinanceReports>c__IteratorB
struct U3CGetFinanceReportsU3Ec__IteratorB_t1286715107;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FinancialContentChanger/<GetFinanceReports>c__IteratorB::.ctor()
extern "C"  void U3CGetFinanceReportsU3Ec__IteratorB__ctor_m3035933428 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<GetFinanceReports>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetFinanceReportsU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m71029502 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<GetFinanceReports>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetFinanceReportsU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m559753430 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FinancialContentChanger/<GetFinanceReports>c__IteratorB::MoveNext()
extern "C"  bool U3CGetFinanceReportsU3Ec__IteratorB_MoveNext_m356646488 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<GetFinanceReports>c__IteratorB::Dispose()
extern "C"  void U3CGetFinanceReportsU3Ec__IteratorB_Dispose_m3393531725 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<GetFinanceReports>c__IteratorB::Reset()
extern "C"  void U3CGetFinanceReportsU3Ec__IteratorB_Reset_m1170576303 (U3CGetFinanceReportsU3Ec__IteratorB_t1286715107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
