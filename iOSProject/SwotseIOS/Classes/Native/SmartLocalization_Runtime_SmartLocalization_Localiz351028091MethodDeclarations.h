﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LocalizedGUIText
struct LocalizedGUIText_t351028091;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_Languag132697751.h"

// System.Void SmartLocalization.LocalizedGUIText::.ctor()
extern "C"  void LocalizedGUIText__ctor_m1001050239 (LocalizedGUIText_t351028091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUIText::Start()
extern "C"  void LocalizedGUIText_Start_m1755967563 (LocalizedGUIText_t351028091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUIText::OnDestroy()
extern "C"  void LocalizedGUIText_OnDestroy_m4005405874 (LocalizedGUIText_t351028091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LocalizedGUIText::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedGUIText_OnChangeLanguage_m3057872715 (LocalizedGUIText_t351028091 * __this, LanguageManager_t132697751 * ___languageManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
