﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpVersion
struct HttpVersion_t1276659706;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.HttpVersion::.ctor()
extern "C"  void HttpVersion__ctor_m1260589616 (HttpVersion_t1276659706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpVersion::.cctor()
extern "C"  void HttpVersion__cctor_m144051953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
