﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.IPv6MulticastOption
struct IPv6MulticastOption_t1621622330;
// System.Net.IPAddress
struct IPAddress_t1399971723;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"

// System.Void System.Net.Sockets.IPv6MulticastOption::.ctor(System.Net.IPAddress)
extern "C"  void IPv6MulticastOption__ctor_m1870748331 (IPv6MulticastOption_t1621622330 * __this, IPAddress_t1399971723 * ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.IPv6MulticastOption::.ctor(System.Net.IPAddress,System.Int64)
extern "C"  void IPv6MulticastOption__ctor_m2360554825 (IPv6MulticastOption_t1621622330 * __this, IPAddress_t1399971723 * ___group0, int64_t ___ifindex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.Sockets.IPv6MulticastOption::get_Group()
extern "C"  IPAddress_t1399971723 * IPv6MulticastOption_get_Group_m1154443470 (IPv6MulticastOption_t1621622330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.IPv6MulticastOption::set_Group(System.Net.IPAddress)
extern "C"  void IPv6MulticastOption_set_Group_m3660843953 (IPv6MulticastOption_t1621622330 * __this, IPAddress_t1399971723 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.Sockets.IPv6MulticastOption::get_InterfaceIndex()
extern "C"  int64_t IPv6MulticastOption_get_InterfaceIndex_m681045185 (IPv6MulticastOption_t1621622330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.IPv6MulticastOption::set_InterfaceIndex(System.Int64)
extern "C"  void IPv6MulticastOption_set_InterfaceIndex_m792099526 (IPv6MulticastOption_t1621622330 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
