﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit2D>
struct DefaultComparer_t2553179007;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void DefaultComparer__ctor_m3195697707_gshared (DefaultComparer_t2553179007 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3195697707(__this, method) ((  void (*) (DefaultComparer_t2553179007 *, const MethodInfo*))DefaultComparer__ctor_m3195697707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4174579808_gshared (DefaultComparer_t2553179007 * __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4174579808(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2553179007 *, RaycastHit2D_t4063908774 , RaycastHit2D_t4063908774 , const MethodInfo*))DefaultComparer_Compare_m4174579808_gshared)(__this, ___x0, ___y1, method)
