﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m697773088(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3716606344 *, FirebaseApp_t210707726 *, X509CertificateCollection_t1197680765 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_Key()
#define KeyValuePair_2_get_Key_m1343224746(__this, method) ((  FirebaseApp_t210707726 * (*) (KeyValuePair_2_t3716606344 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4207983867(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3716606344 *, FirebaseApp_t210707726 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_Value()
#define KeyValuePair_2_get_Value_m2490503274(__this, method) ((  X509CertificateCollection_t1197680765 * (*) (KeyValuePair_2_t3716606344 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2371756875(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3716606344 *, X509CertificateCollection_t1197680765 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::ToString()
#define KeyValuePair_2_ToString_m849173825(__this, method) ((  String_t* (*) (KeyValuePair_2_t3716606344 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
