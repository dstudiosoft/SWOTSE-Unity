﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator
struct OrderedCollectionEnumerator_t826004808;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::.ctor(System.Collections.IEnumerator,System.Boolean)
extern "C"  void OrderedCollectionEnumerator__ctor_m74530536 (OrderedCollectionEnumerator_t826004808 * __this, Il2CppObject * ___listEnumerator0, bool ___isKeyList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::get_Current()
extern "C"  Il2CppObject * OrderedCollectionEnumerator_get_Current_m620549377 (OrderedCollectionEnumerator_t826004808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::MoveNext()
extern "C"  bool OrderedCollectionEnumerator_MoveNext_m1658564124 (OrderedCollectionEnumerator_t826004808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.OrderedDictionary/OrderedCollection/OrderedCollectionEnumerator::Reset()
extern "C"  void OrderedCollectionEnumerator_Reset_m1587953705 (OrderedCollectionEnumerator_t826004808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
