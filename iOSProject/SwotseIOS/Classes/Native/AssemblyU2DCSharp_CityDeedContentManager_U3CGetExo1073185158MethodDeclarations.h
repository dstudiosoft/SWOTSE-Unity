﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityDeedContentManager/<GetExoticInfo>c__Iterator9
struct U3CGetExoticInfoU3Ec__Iterator9_t1073185158;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CityDeedContentManager/<GetExoticInfo>c__Iterator9::.ctor()
extern "C"  void U3CGetExoticInfoU3Ec__Iterator9__ctor_m4189854619 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CityDeedContentManager/<GetExoticInfo>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetExoticInfoU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2283837773 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CityDeedContentManager/<GetExoticInfo>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetExoticInfoU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2646997013 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CityDeedContentManager/<GetExoticInfo>c__Iterator9::MoveNext()
extern "C"  bool U3CGetExoticInfoU3Ec__Iterator9_MoveNext_m2443513361 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityDeedContentManager/<GetExoticInfo>c__Iterator9::Dispose()
extern "C"  void U3CGetExoticInfoU3Ec__Iterator9_Dispose_m3821854540 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityDeedContentManager/<GetExoticInfo>c__Iterator9::Reset()
extern "C"  void U3CGetExoticInfoU3Ec__Iterator9_Reset_m687864130 (U3CGetExoticInfoU3Ec__Iterator9_t1073185158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
