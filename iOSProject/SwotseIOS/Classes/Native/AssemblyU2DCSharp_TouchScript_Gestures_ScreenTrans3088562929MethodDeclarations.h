﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.ScreenTransformGesture
struct ScreenTransformGesture_t3088562929;
// UnityEngine.Transform
struct Transform_t3275118058;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_ProjectionPar2712959773.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TouchScript.Gestures.ScreenTransformGesture::.ctor()
extern "C"  void ScreenTransformGesture__ctor_m622660702 (ScreenTransformGesture_t3088562929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::ApplyTransform(UnityEngine.Transform)
extern "C"  void ScreenTransformGesture_ApplyTransform_m910331233 (ScreenTransformGesture_t3088562929 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.ScreenTransformGesture::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float ScreenTransformGesture_doRotation_m3156174606 (ScreenTransformGesture_t3088562929 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.ScreenTransformGesture::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float ScreenTransformGesture_doScaling_m1286261469 (ScreenTransformGesture_t3088562929 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.ScreenTransformGesture::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  ScreenTransformGesture_doOnePointTranslation_m3081768143 (ScreenTransformGesture_t3088562929 * __this, Vector2_t2243707579  ___oldScreenPos0, Vector2_t2243707579  ___newScreenPos1, ProjectionParams_t2712959773 * ___projectionParams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.ScreenTransformGesture::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  ScreenTransformGesture_doTwoPointTranslation_m1925357299 (ScreenTransformGesture_t3088562929 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, float ___dR4, float ___dS5, ProjectionParams_t2712959773 * ___projectionParams6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.ScreenTransformGesture::scaleAndRotate(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern "C"  Vector2_t2243707579  ScreenTransformGesture_scaleAndRotate_m4269048211 (ScreenTransformGesture_t3088562929 * __this, Vector2_t2243707579  ___point0, Vector2_t2243707579  ___center1, float ___dR2, float ___dS3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformStarted_m650587643 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformStarted_m1812088070 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_add_Transformed_m3402199251 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_remove_Transformed_m67379202 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformCompleted_m3586448151 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.ScreenTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void ScreenTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformCompleted_m3722519474 (ScreenTransformGesture_t3088562929 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
