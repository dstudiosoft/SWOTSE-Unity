﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// ShopTableCell
struct ShopTableCell_t4262618216;
// Tacticsoft.TableView
struct TableView_t3179510217;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// PanelTableController
struct PanelTableController_t1117991214;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedUpTableController
struct  SpeedUpTableController_t2017201746  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.ArrayList SpeedUpTableController::currentSourceArray
	ArrayList_t4252133567 * ___currentSourceArray_2;
	// ShopTableCell SpeedUpTableController::m_cellPrefab
	ShopTableCell_t4262618216 * ___m_cellPrefab_3;
	// Tacticsoft.TableView SpeedUpTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_4;
	// UnityEngine.UI.Image SpeedUpTableController::itemImage
	Image_t2042527209 * ___itemImage_5;
	// UnityEngine.UI.Text SpeedUpTableController::timeLeft
	Text_t356221433 * ___timeLeft_6;
	// System.Int64 SpeedUpTableController::eventId
	int64_t ___eventId_7;
	// System.DateTime SpeedUpTableController::startTime
	DateTime_t693205669  ___startTime_8;
	// System.DateTime SpeedUpTableController::finishTime
	DateTime_t693205669  ___finishTime_9;
	// PanelTableController SpeedUpTableController::owner
	PanelTableController_t1117991214 * ___owner_10;
	// System.TimeSpan SpeedUpTableController::timespan
	TimeSpan_t3430258949  ___timespan_11;

public:
	inline static int32_t get_offset_of_currentSourceArray_2() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___currentSourceArray_2)); }
	inline ArrayList_t4252133567 * get_currentSourceArray_2() const { return ___currentSourceArray_2; }
	inline ArrayList_t4252133567 ** get_address_of_currentSourceArray_2() { return &___currentSourceArray_2; }
	inline void set_currentSourceArray_2(ArrayList_t4252133567 * value)
	{
		___currentSourceArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentSourceArray_2, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___m_cellPrefab_3)); }
	inline ShopTableCell_t4262618216 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline ShopTableCell_t4262618216 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(ShopTableCell_t4262618216 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___m_tableView_4)); }
	inline TableView_t3179510217 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t3179510217 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_4, value);
	}

	inline static int32_t get_offset_of_itemImage_5() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___itemImage_5)); }
	inline Image_t2042527209 * get_itemImage_5() const { return ___itemImage_5; }
	inline Image_t2042527209 ** get_address_of_itemImage_5() { return &___itemImage_5; }
	inline void set_itemImage_5(Image_t2042527209 * value)
	{
		___itemImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_5, value);
	}

	inline static int32_t get_offset_of_timeLeft_6() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___timeLeft_6)); }
	inline Text_t356221433 * get_timeLeft_6() const { return ___timeLeft_6; }
	inline Text_t356221433 ** get_address_of_timeLeft_6() { return &___timeLeft_6; }
	inline void set_timeLeft_6(Text_t356221433 * value)
	{
		___timeLeft_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeLeft_6, value);
	}

	inline static int32_t get_offset_of_eventId_7() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___eventId_7)); }
	inline int64_t get_eventId_7() const { return ___eventId_7; }
	inline int64_t* get_address_of_eventId_7() { return &___eventId_7; }
	inline void set_eventId_7(int64_t value)
	{
		___eventId_7 = value;
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___startTime_8)); }
	inline DateTime_t693205669  get_startTime_8() const { return ___startTime_8; }
	inline DateTime_t693205669 * get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(DateTime_t693205669  value)
	{
		___startTime_8 = value;
	}

	inline static int32_t get_offset_of_finishTime_9() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___finishTime_9)); }
	inline DateTime_t693205669  get_finishTime_9() const { return ___finishTime_9; }
	inline DateTime_t693205669 * get_address_of_finishTime_9() { return &___finishTime_9; }
	inline void set_finishTime_9(DateTime_t693205669  value)
	{
		___finishTime_9 = value;
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___owner_10)); }
	inline PanelTableController_t1117991214 * get_owner_10() const { return ___owner_10; }
	inline PanelTableController_t1117991214 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(PanelTableController_t1117991214 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier(&___owner_10, value);
	}

	inline static int32_t get_offset_of_timespan_11() { return static_cast<int32_t>(offsetof(SpeedUpTableController_t2017201746, ___timespan_11)); }
	inline TimeSpan_t3430258949  get_timespan_11() const { return ___timespan_11; }
	inline TimeSpan_t3430258949 * get_address_of_timespan_11() { return &___timespan_11; }
	inline void set_timespan_11(TimeSpan_t3430258949  value)
	{
		___timespan_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
