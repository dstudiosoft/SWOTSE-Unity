﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColonyModel
struct ColonyModel_t2271864985;

#include "codegen/il2cpp-codegen.h"

// System.Void ColonyModel::.ctor()
extern "C"  void ColonyModel__ctor_m3148015954 (ColonyModel_t2271864985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
