﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceColoniesTableController
struct ResidenceColoniesTableController_t2206090190;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResidenceColoniesTableController::.ctor()
extern "C"  void ResidenceColoniesTableController__ctor_m548894887 (ResidenceColoniesTableController_t2206090190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableController::OnEnable()
extern "C"  void ResidenceColoniesTableController_OnEnable_m624439675 (ResidenceColoniesTableController_t2206090190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ResidenceColoniesTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t ResidenceColoniesTableController_GetNumberOfRowsForTableView_m3163534131 (ResidenceColoniesTableController_t2206090190 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ResidenceColoniesTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float ResidenceColoniesTableController_GetHeightForRowInTableView_m3337862163 (ResidenceColoniesTableController_t2206090190 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell ResidenceColoniesTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * ResidenceColoniesTableController_GetCellForRowInTableView_m2658829128 (ResidenceColoniesTableController_t2206090190 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResidenceColoniesTableController::GetCityColonies()
extern "C"  Il2CppObject * ResidenceColoniesTableController_GetCityColonies_m2255251428 (ResidenceColoniesTableController_t2206090190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceColoniesTableController::OnGetColonies(System.Object,System.String)
extern "C"  void ResidenceColoniesTableController_OnGetColonies_m580003988 (ResidenceColoniesTableController_t2206090190 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
