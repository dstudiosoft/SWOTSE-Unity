﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnitsModel
struct UnitsModel_t1926818124;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DismissContentManager
struct  DismissContentManager_t1983743404  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text DismissContentManager::cityWorker
	Text_t356221433 * ___cityWorker_2;
	// UnityEngine.UI.Text DismissContentManager::citySpy
	Text_t356221433 * ___citySpy_3;
	// UnityEngine.UI.Text DismissContentManager::citySwordsman
	Text_t356221433 * ___citySwordsman_4;
	// UnityEngine.UI.Text DismissContentManager::citySpearman
	Text_t356221433 * ___citySpearman_5;
	// UnityEngine.UI.Text DismissContentManager::cityPikeman
	Text_t356221433 * ___cityPikeman_6;
	// UnityEngine.UI.Text DismissContentManager::cityScoutRider
	Text_t356221433 * ___cityScoutRider_7;
	// UnityEngine.UI.Text DismissContentManager::cityLightCavalry
	Text_t356221433 * ___cityLightCavalry_8;
	// UnityEngine.UI.Text DismissContentManager::cityHeavyCavalry
	Text_t356221433 * ___cityHeavyCavalry_9;
	// UnityEngine.UI.Text DismissContentManager::cityArcher
	Text_t356221433 * ___cityArcher_10;
	// UnityEngine.UI.Text DismissContentManager::cityArcherRider
	Text_t356221433 * ___cityArcherRider_11;
	// UnityEngine.UI.Text DismissContentManager::cityHollyman
	Text_t356221433 * ___cityHollyman_12;
	// UnityEngine.UI.Text DismissContentManager::cityWagon
	Text_t356221433 * ___cityWagon_13;
	// UnityEngine.UI.Text DismissContentManager::cityTrebuchet
	Text_t356221433 * ___cityTrebuchet_14;
	// UnityEngine.UI.Text DismissContentManager::citySiegeTower
	Text_t356221433 * ___citySiegeTower_15;
	// UnityEngine.UI.Text DismissContentManager::cityBatteringRam
	Text_t356221433 * ___cityBatteringRam_16;
	// UnityEngine.UI.Text DismissContentManager::cityBallista
	Text_t356221433 * ___cityBallista_17;
	// UnityEngine.UI.InputField DismissContentManager::dismissWorker
	InputField_t1631627530 * ___dismissWorker_18;
	// UnityEngine.UI.InputField DismissContentManager::dismissSpy
	InputField_t1631627530 * ___dismissSpy_19;
	// UnityEngine.UI.InputField DismissContentManager::dismissSwordsman
	InputField_t1631627530 * ___dismissSwordsman_20;
	// UnityEngine.UI.InputField DismissContentManager::dismissSpearman
	InputField_t1631627530 * ___dismissSpearman_21;
	// UnityEngine.UI.InputField DismissContentManager::dismissPikeman
	InputField_t1631627530 * ___dismissPikeman_22;
	// UnityEngine.UI.InputField DismissContentManager::dismissScoutRider
	InputField_t1631627530 * ___dismissScoutRider_23;
	// UnityEngine.UI.InputField DismissContentManager::dismissLightCavalry
	InputField_t1631627530 * ___dismissLightCavalry_24;
	// UnityEngine.UI.InputField DismissContentManager::dismissHeavyCavalry
	InputField_t1631627530 * ___dismissHeavyCavalry_25;
	// UnityEngine.UI.InputField DismissContentManager::dismissArcher
	InputField_t1631627530 * ___dismissArcher_26;
	// UnityEngine.UI.InputField DismissContentManager::dismissArcherRider
	InputField_t1631627530 * ___dismissArcherRider_27;
	// UnityEngine.UI.InputField DismissContentManager::dismissHollyman
	InputField_t1631627530 * ___dismissHollyman_28;
	// UnityEngine.UI.InputField DismissContentManager::dismissWagon
	InputField_t1631627530 * ___dismissWagon_29;
	// UnityEngine.UI.InputField DismissContentManager::dismissTrebuchet
	InputField_t1631627530 * ___dismissTrebuchet_30;
	// UnityEngine.UI.InputField DismissContentManager::dismissSiegeTower
	InputField_t1631627530 * ___dismissSiegeTower_31;
	// UnityEngine.UI.InputField DismissContentManager::dismissBatteringRam
	InputField_t1631627530 * ___dismissBatteringRam_32;
	// UnityEngine.UI.InputField DismissContentManager::dismissBallista
	InputField_t1631627530 * ___dismissBallista_33;
	// UnitsModel DismissContentManager::units
	UnitsModel_t1926818124 * ___units_34;

public:
	inline static int32_t get_offset_of_cityWorker_2() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityWorker_2)); }
	inline Text_t356221433 * get_cityWorker_2() const { return ___cityWorker_2; }
	inline Text_t356221433 ** get_address_of_cityWorker_2() { return &___cityWorker_2; }
	inline void set_cityWorker_2(Text_t356221433 * value)
	{
		___cityWorker_2 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorker_2, value);
	}

	inline static int32_t get_offset_of_citySpy_3() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___citySpy_3)); }
	inline Text_t356221433 * get_citySpy_3() const { return ___citySpy_3; }
	inline Text_t356221433 ** get_address_of_citySpy_3() { return &___citySpy_3; }
	inline void set_citySpy_3(Text_t356221433 * value)
	{
		___citySpy_3 = value;
		Il2CppCodeGenWriteBarrier(&___citySpy_3, value);
	}

	inline static int32_t get_offset_of_citySwordsman_4() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___citySwordsman_4)); }
	inline Text_t356221433 * get_citySwordsman_4() const { return ___citySwordsman_4; }
	inline Text_t356221433 ** get_address_of_citySwordsman_4() { return &___citySwordsman_4; }
	inline void set_citySwordsman_4(Text_t356221433 * value)
	{
		___citySwordsman_4 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsman_4, value);
	}

	inline static int32_t get_offset_of_citySpearman_5() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___citySpearman_5)); }
	inline Text_t356221433 * get_citySpearman_5() const { return ___citySpearman_5; }
	inline Text_t356221433 ** get_address_of_citySpearman_5() { return &___citySpearman_5; }
	inline void set_citySpearman_5(Text_t356221433 * value)
	{
		___citySpearman_5 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearman_5, value);
	}

	inline static int32_t get_offset_of_cityPikeman_6() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityPikeman_6)); }
	inline Text_t356221433 * get_cityPikeman_6() const { return ___cityPikeman_6; }
	inline Text_t356221433 ** get_address_of_cityPikeman_6() { return &___cityPikeman_6; }
	inline void set_cityPikeman_6(Text_t356221433 * value)
	{
		___cityPikeman_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikeman_6, value);
	}

	inline static int32_t get_offset_of_cityScoutRider_7() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityScoutRider_7)); }
	inline Text_t356221433 * get_cityScoutRider_7() const { return ___cityScoutRider_7; }
	inline Text_t356221433 ** get_address_of_cityScoutRider_7() { return &___cityScoutRider_7; }
	inline void set_cityScoutRider_7(Text_t356221433 * value)
	{
		___cityScoutRider_7 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRider_7, value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_8() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityLightCavalry_8)); }
	inline Text_t356221433 * get_cityLightCavalry_8() const { return ___cityLightCavalry_8; }
	inline Text_t356221433 ** get_address_of_cityLightCavalry_8() { return &___cityLightCavalry_8; }
	inline void set_cityLightCavalry_8(Text_t356221433 * value)
	{
		___cityLightCavalry_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalry_8, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_9() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityHeavyCavalry_9)); }
	inline Text_t356221433 * get_cityHeavyCavalry_9() const { return ___cityHeavyCavalry_9; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalry_9() { return &___cityHeavyCavalry_9; }
	inline void set_cityHeavyCavalry_9(Text_t356221433 * value)
	{
		___cityHeavyCavalry_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalry_9, value);
	}

	inline static int32_t get_offset_of_cityArcher_10() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityArcher_10)); }
	inline Text_t356221433 * get_cityArcher_10() const { return ___cityArcher_10; }
	inline Text_t356221433 ** get_address_of_cityArcher_10() { return &___cityArcher_10; }
	inline void set_cityArcher_10(Text_t356221433 * value)
	{
		___cityArcher_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcher_10, value);
	}

	inline static int32_t get_offset_of_cityArcherRider_11() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityArcherRider_11)); }
	inline Text_t356221433 * get_cityArcherRider_11() const { return ___cityArcherRider_11; }
	inline Text_t356221433 ** get_address_of_cityArcherRider_11() { return &___cityArcherRider_11; }
	inline void set_cityArcherRider_11(Text_t356221433 * value)
	{
		___cityArcherRider_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRider_11, value);
	}

	inline static int32_t get_offset_of_cityHollyman_12() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityHollyman_12)); }
	inline Text_t356221433 * get_cityHollyman_12() const { return ___cityHollyman_12; }
	inline Text_t356221433 ** get_address_of_cityHollyman_12() { return &___cityHollyman_12; }
	inline void set_cityHollyman_12(Text_t356221433 * value)
	{
		___cityHollyman_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollyman_12, value);
	}

	inline static int32_t get_offset_of_cityWagon_13() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityWagon_13)); }
	inline Text_t356221433 * get_cityWagon_13() const { return ___cityWagon_13; }
	inline Text_t356221433 ** get_address_of_cityWagon_13() { return &___cityWagon_13; }
	inline void set_cityWagon_13(Text_t356221433 * value)
	{
		___cityWagon_13 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagon_13, value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_14() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityTrebuchet_14)); }
	inline Text_t356221433 * get_cityTrebuchet_14() const { return ___cityTrebuchet_14; }
	inline Text_t356221433 ** get_address_of_cityTrebuchet_14() { return &___cityTrebuchet_14; }
	inline void set_cityTrebuchet_14(Text_t356221433 * value)
	{
		___cityTrebuchet_14 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchet_14, value);
	}

	inline static int32_t get_offset_of_citySiegeTower_15() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___citySiegeTower_15)); }
	inline Text_t356221433 * get_citySiegeTower_15() const { return ___citySiegeTower_15; }
	inline Text_t356221433 ** get_address_of_citySiegeTower_15() { return &___citySiegeTower_15; }
	inline void set_citySiegeTower_15(Text_t356221433 * value)
	{
		___citySiegeTower_15 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTower_15, value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_16() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityBatteringRam_16)); }
	inline Text_t356221433 * get_cityBatteringRam_16() const { return ___cityBatteringRam_16; }
	inline Text_t356221433 ** get_address_of_cityBatteringRam_16() { return &___cityBatteringRam_16; }
	inline void set_cityBatteringRam_16(Text_t356221433 * value)
	{
		___cityBatteringRam_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRam_16, value);
	}

	inline static int32_t get_offset_of_cityBallista_17() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___cityBallista_17)); }
	inline Text_t356221433 * get_cityBallista_17() const { return ___cityBallista_17; }
	inline Text_t356221433 ** get_address_of_cityBallista_17() { return &___cityBallista_17; }
	inline void set_cityBallista_17(Text_t356221433 * value)
	{
		___cityBallista_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallista_17, value);
	}

	inline static int32_t get_offset_of_dismissWorker_18() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissWorker_18)); }
	inline InputField_t1631627530 * get_dismissWorker_18() const { return ___dismissWorker_18; }
	inline InputField_t1631627530 ** get_address_of_dismissWorker_18() { return &___dismissWorker_18; }
	inline void set_dismissWorker_18(InputField_t1631627530 * value)
	{
		___dismissWorker_18 = value;
		Il2CppCodeGenWriteBarrier(&___dismissWorker_18, value);
	}

	inline static int32_t get_offset_of_dismissSpy_19() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissSpy_19)); }
	inline InputField_t1631627530 * get_dismissSpy_19() const { return ___dismissSpy_19; }
	inline InputField_t1631627530 ** get_address_of_dismissSpy_19() { return &___dismissSpy_19; }
	inline void set_dismissSpy_19(InputField_t1631627530 * value)
	{
		___dismissSpy_19 = value;
		Il2CppCodeGenWriteBarrier(&___dismissSpy_19, value);
	}

	inline static int32_t get_offset_of_dismissSwordsman_20() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissSwordsman_20)); }
	inline InputField_t1631627530 * get_dismissSwordsman_20() const { return ___dismissSwordsman_20; }
	inline InputField_t1631627530 ** get_address_of_dismissSwordsman_20() { return &___dismissSwordsman_20; }
	inline void set_dismissSwordsman_20(InputField_t1631627530 * value)
	{
		___dismissSwordsman_20 = value;
		Il2CppCodeGenWriteBarrier(&___dismissSwordsman_20, value);
	}

	inline static int32_t get_offset_of_dismissSpearman_21() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissSpearman_21)); }
	inline InputField_t1631627530 * get_dismissSpearman_21() const { return ___dismissSpearman_21; }
	inline InputField_t1631627530 ** get_address_of_dismissSpearman_21() { return &___dismissSpearman_21; }
	inline void set_dismissSpearman_21(InputField_t1631627530 * value)
	{
		___dismissSpearman_21 = value;
		Il2CppCodeGenWriteBarrier(&___dismissSpearman_21, value);
	}

	inline static int32_t get_offset_of_dismissPikeman_22() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissPikeman_22)); }
	inline InputField_t1631627530 * get_dismissPikeman_22() const { return ___dismissPikeman_22; }
	inline InputField_t1631627530 ** get_address_of_dismissPikeman_22() { return &___dismissPikeman_22; }
	inline void set_dismissPikeman_22(InputField_t1631627530 * value)
	{
		___dismissPikeman_22 = value;
		Il2CppCodeGenWriteBarrier(&___dismissPikeman_22, value);
	}

	inline static int32_t get_offset_of_dismissScoutRider_23() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissScoutRider_23)); }
	inline InputField_t1631627530 * get_dismissScoutRider_23() const { return ___dismissScoutRider_23; }
	inline InputField_t1631627530 ** get_address_of_dismissScoutRider_23() { return &___dismissScoutRider_23; }
	inline void set_dismissScoutRider_23(InputField_t1631627530 * value)
	{
		___dismissScoutRider_23 = value;
		Il2CppCodeGenWriteBarrier(&___dismissScoutRider_23, value);
	}

	inline static int32_t get_offset_of_dismissLightCavalry_24() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissLightCavalry_24)); }
	inline InputField_t1631627530 * get_dismissLightCavalry_24() const { return ___dismissLightCavalry_24; }
	inline InputField_t1631627530 ** get_address_of_dismissLightCavalry_24() { return &___dismissLightCavalry_24; }
	inline void set_dismissLightCavalry_24(InputField_t1631627530 * value)
	{
		___dismissLightCavalry_24 = value;
		Il2CppCodeGenWriteBarrier(&___dismissLightCavalry_24, value);
	}

	inline static int32_t get_offset_of_dismissHeavyCavalry_25() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissHeavyCavalry_25)); }
	inline InputField_t1631627530 * get_dismissHeavyCavalry_25() const { return ___dismissHeavyCavalry_25; }
	inline InputField_t1631627530 ** get_address_of_dismissHeavyCavalry_25() { return &___dismissHeavyCavalry_25; }
	inline void set_dismissHeavyCavalry_25(InputField_t1631627530 * value)
	{
		___dismissHeavyCavalry_25 = value;
		Il2CppCodeGenWriteBarrier(&___dismissHeavyCavalry_25, value);
	}

	inline static int32_t get_offset_of_dismissArcher_26() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissArcher_26)); }
	inline InputField_t1631627530 * get_dismissArcher_26() const { return ___dismissArcher_26; }
	inline InputField_t1631627530 ** get_address_of_dismissArcher_26() { return &___dismissArcher_26; }
	inline void set_dismissArcher_26(InputField_t1631627530 * value)
	{
		___dismissArcher_26 = value;
		Il2CppCodeGenWriteBarrier(&___dismissArcher_26, value);
	}

	inline static int32_t get_offset_of_dismissArcherRider_27() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissArcherRider_27)); }
	inline InputField_t1631627530 * get_dismissArcherRider_27() const { return ___dismissArcherRider_27; }
	inline InputField_t1631627530 ** get_address_of_dismissArcherRider_27() { return &___dismissArcherRider_27; }
	inline void set_dismissArcherRider_27(InputField_t1631627530 * value)
	{
		___dismissArcherRider_27 = value;
		Il2CppCodeGenWriteBarrier(&___dismissArcherRider_27, value);
	}

	inline static int32_t get_offset_of_dismissHollyman_28() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissHollyman_28)); }
	inline InputField_t1631627530 * get_dismissHollyman_28() const { return ___dismissHollyman_28; }
	inline InputField_t1631627530 ** get_address_of_dismissHollyman_28() { return &___dismissHollyman_28; }
	inline void set_dismissHollyman_28(InputField_t1631627530 * value)
	{
		___dismissHollyman_28 = value;
		Il2CppCodeGenWriteBarrier(&___dismissHollyman_28, value);
	}

	inline static int32_t get_offset_of_dismissWagon_29() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissWagon_29)); }
	inline InputField_t1631627530 * get_dismissWagon_29() const { return ___dismissWagon_29; }
	inline InputField_t1631627530 ** get_address_of_dismissWagon_29() { return &___dismissWagon_29; }
	inline void set_dismissWagon_29(InputField_t1631627530 * value)
	{
		___dismissWagon_29 = value;
		Il2CppCodeGenWriteBarrier(&___dismissWagon_29, value);
	}

	inline static int32_t get_offset_of_dismissTrebuchet_30() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissTrebuchet_30)); }
	inline InputField_t1631627530 * get_dismissTrebuchet_30() const { return ___dismissTrebuchet_30; }
	inline InputField_t1631627530 ** get_address_of_dismissTrebuchet_30() { return &___dismissTrebuchet_30; }
	inline void set_dismissTrebuchet_30(InputField_t1631627530 * value)
	{
		___dismissTrebuchet_30 = value;
		Il2CppCodeGenWriteBarrier(&___dismissTrebuchet_30, value);
	}

	inline static int32_t get_offset_of_dismissSiegeTower_31() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissSiegeTower_31)); }
	inline InputField_t1631627530 * get_dismissSiegeTower_31() const { return ___dismissSiegeTower_31; }
	inline InputField_t1631627530 ** get_address_of_dismissSiegeTower_31() { return &___dismissSiegeTower_31; }
	inline void set_dismissSiegeTower_31(InputField_t1631627530 * value)
	{
		___dismissSiegeTower_31 = value;
		Il2CppCodeGenWriteBarrier(&___dismissSiegeTower_31, value);
	}

	inline static int32_t get_offset_of_dismissBatteringRam_32() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissBatteringRam_32)); }
	inline InputField_t1631627530 * get_dismissBatteringRam_32() const { return ___dismissBatteringRam_32; }
	inline InputField_t1631627530 ** get_address_of_dismissBatteringRam_32() { return &___dismissBatteringRam_32; }
	inline void set_dismissBatteringRam_32(InputField_t1631627530 * value)
	{
		___dismissBatteringRam_32 = value;
		Il2CppCodeGenWriteBarrier(&___dismissBatteringRam_32, value);
	}

	inline static int32_t get_offset_of_dismissBallista_33() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___dismissBallista_33)); }
	inline InputField_t1631627530 * get_dismissBallista_33() const { return ___dismissBallista_33; }
	inline InputField_t1631627530 ** get_address_of_dismissBallista_33() { return &___dismissBallista_33; }
	inline void set_dismissBallista_33(InputField_t1631627530 * value)
	{
		___dismissBallista_33 = value;
		Il2CppCodeGenWriteBarrier(&___dismissBallista_33, value);
	}

	inline static int32_t get_offset_of_units_34() { return static_cast<int32_t>(offsetof(DismissContentManager_t1983743404, ___units_34)); }
	inline UnitsModel_t1926818124 * get_units_34() const { return ___units_34; }
	inline UnitsModel_t1926818124 ** get_address_of_units_34() { return &___units_34; }
	inline void set_units_34(UnitsModel_t1926818124 * value)
	{
		___units_34 = value;
		Il2CppCodeGenWriteBarrier(&___units_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
