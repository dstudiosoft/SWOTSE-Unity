﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Utils.TimedSequence`1<System.Object>
struct TimedSequence_1_t172103199;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void TouchScript.Utils.TimedSequence`1<System.Object>::.ctor()
extern "C"  void TimedSequence_1__ctor_m3458058545_gshared (TimedSequence_1_t172103199 * __this, const MethodInfo* method);
#define TimedSequence_1__ctor_m3458058545(__this, method) ((  void (*) (TimedSequence_1_t172103199 *, const MethodInfo*))TimedSequence_1__ctor_m3458058545_gshared)(__this, method)
// System.Void TouchScript.Utils.TimedSequence`1<System.Object>::Add(T)
extern "C"  void TimedSequence_1_Add_m2046982790_gshared (TimedSequence_1_t172103199 * __this, Il2CppObject * ___element0, const MethodInfo* method);
#define TimedSequence_1_Add_m2046982790(__this, ___element0, method) ((  void (*) (TimedSequence_1_t172103199 *, Il2CppObject *, const MethodInfo*))TimedSequence_1_Add_m2046982790_gshared)(__this, ___element0, method)
// System.Void TouchScript.Utils.TimedSequence`1<System.Object>::Add(T,System.Single)
extern "C"  void TimedSequence_1_Add_m1389480837_gshared (TimedSequence_1_t172103199 * __this, Il2CppObject * ___element0, float ___time1, const MethodInfo* method);
#define TimedSequence_1_Add_m1389480837(__this, ___element0, ___time1, method) ((  void (*) (TimedSequence_1_t172103199 *, Il2CppObject *, float, const MethodInfo*))TimedSequence_1_Add_m1389480837_gshared)(__this, ___element0, ___time1, method)
// System.Void TouchScript.Utils.TimedSequence`1<System.Object>::Clear()
extern "C"  void TimedSequence_1_Clear_m2637068538_gshared (TimedSequence_1_t172103199 * __this, const MethodInfo* method);
#define TimedSequence_1_Clear_m2637068538(__this, method) ((  void (*) (TimedSequence_1_t172103199 *, const MethodInfo*))TimedSequence_1_Clear_m2637068538_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<System.Object>::FindElementsLaterThan(System.Single)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m1419642373_gshared (TimedSequence_1_t172103199 * __this, float ___time0, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m1419642373(__this, ___time0, method) ((  Il2CppObject* (*) (TimedSequence_1_t172103199 *, float, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1419642373_gshared)(__this, ___time0, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<System.Object>::FindElementsLaterThan(System.Single,System.Single&)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m543828678_gshared (TimedSequence_1_t172103199 * __this, float ___time0, float* ___lastTime1, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m543828678(__this, ___time0, ___lastTime1, method) ((  Il2CppObject* (*) (TimedSequence_1_t172103199 *, float, float*, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m543828678_gshared)(__this, ___time0, ___lastTime1, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<System.Object>::FindElementsLaterThan(System.Single,System.Predicate`1<T>)
extern "C"  Il2CppObject* TimedSequence_1_FindElementsLaterThan_m1289799938_gshared (TimedSequence_1_t172103199 * __this, float ___time0, Predicate_1_t1132419410 * ___predicate1, const MethodInfo* method);
#define TimedSequence_1_FindElementsLaterThan_m1289799938(__this, ___time0, ___predicate1, method) ((  Il2CppObject* (*) (TimedSequence_1_t172103199 *, float, Predicate_1_t1132419410 *, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1289799938_gshared)(__this, ___time0, ___predicate1, method)
