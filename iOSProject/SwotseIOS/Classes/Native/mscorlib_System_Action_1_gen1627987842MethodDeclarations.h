﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<WebSocketSharp.Net.Cookie>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m406984422(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1627987842 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<WebSocketSharp.Net.Cookie>::Invoke(T)
#define Action_1_Invoke_m3555837686(__this, ___obj0, method) ((  void (*) (Action_1_t1627987842 *, Cookie_t1826188460 *, const MethodInfo*))Action_1_Invoke_m1684652980_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<WebSocketSharp.Net.Cookie>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1923119719(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1627987842 *, Cookie_t1826188460 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<WebSocketSharp.Net.Cookie>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m776451764(__this, ___result0, method) ((  void (*) (Action_1_t1627987842 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
