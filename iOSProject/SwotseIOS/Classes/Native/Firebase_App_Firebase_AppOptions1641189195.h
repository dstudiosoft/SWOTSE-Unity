﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.AppOptions
struct  AppOptions_t1641189195  : public Il2CppObject
{
public:
	// System.Uri Firebase.AppOptions::<DatabaseUrl>k__BackingField
	Uri_t19570940 * ___U3CDatabaseUrlU3Ek__BackingField_0;
	// System.String Firebase.AppOptions::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_1;
	// System.String Firebase.AppOptions::<ApiKey>k__BackingField
	String_t* ___U3CApiKeyU3Ek__BackingField_2;
	// System.String Firebase.AppOptions::<MessageSenderId>k__BackingField
	String_t* ___U3CMessageSenderIdU3Ek__BackingField_3;
	// System.String Firebase.AppOptions::<StorageBucket>k__BackingField
	String_t* ___U3CStorageBucketU3Ek__BackingField_4;
	// System.String Firebase.AppOptions::<ProjectId>k__BackingField
	String_t* ___U3CProjectIdU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CDatabaseUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CDatabaseUrlU3Ek__BackingField_0)); }
	inline Uri_t19570940 * get_U3CDatabaseUrlU3Ek__BackingField_0() const { return ___U3CDatabaseUrlU3Ek__BackingField_0; }
	inline Uri_t19570940 ** get_address_of_U3CDatabaseUrlU3Ek__BackingField_0() { return &___U3CDatabaseUrlU3Ek__BackingField_0; }
	inline void set_U3CDatabaseUrlU3Ek__BackingField_0(Uri_t19570940 * value)
	{
		___U3CDatabaseUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDatabaseUrlU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CAppIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_1() const { return ___U3CAppIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_1() { return &___U3CAppIdU3Ek__BackingField_1; }
	inline void set_U3CAppIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAppIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CApiKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CApiKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CApiKeyU3Ek__BackingField_2() const { return ___U3CApiKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CApiKeyU3Ek__BackingField_2() { return &___U3CApiKeyU3Ek__BackingField_2; }
	inline void set_U3CApiKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CApiKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CApiKeyU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CMessageSenderIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CMessageSenderIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CMessageSenderIdU3Ek__BackingField_3() const { return ___U3CMessageSenderIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMessageSenderIdU3Ek__BackingField_3() { return &___U3CMessageSenderIdU3Ek__BackingField_3; }
	inline void set_U3CMessageSenderIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CMessageSenderIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageSenderIdU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CStorageBucketU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CStorageBucketU3Ek__BackingField_4)); }
	inline String_t* get_U3CStorageBucketU3Ek__BackingField_4() const { return ___U3CStorageBucketU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CStorageBucketU3Ek__BackingField_4() { return &___U3CStorageBucketU3Ek__BackingField_4; }
	inline void set_U3CStorageBucketU3Ek__BackingField_4(String_t* value)
	{
		___U3CStorageBucketU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStorageBucketU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CProjectIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AppOptions_t1641189195, ___U3CProjectIdU3Ek__BackingField_5)); }
	inline String_t* get_U3CProjectIdU3Ek__BackingField_5() const { return ___U3CProjectIdU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CProjectIdU3Ek__BackingField_5() { return &___U3CProjectIdU3Ek__BackingField_5; }
	inline void set_U3CProjectIdU3Ek__BackingField_5(String_t* value)
	{
		___U3CProjectIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProjectIdU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
