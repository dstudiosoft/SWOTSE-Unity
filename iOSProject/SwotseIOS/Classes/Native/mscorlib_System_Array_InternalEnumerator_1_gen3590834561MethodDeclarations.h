﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3590834561.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"

// System.Void System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2631948708_gshared (InternalEnumerator_1_t3590834561 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2631948708(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3590834561 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2631948708_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3359582084_gshared (InternalEnumerator_1_t3590834561 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3359582084(__this, method) ((  void (*) (InternalEnumerator_1_t3590834561 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3359582084_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3099133832_gshared (InternalEnumerator_1_t3590834561 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3099133832(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3590834561 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3099133832_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3889781171_gshared (InternalEnumerator_1_t3590834561 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3889781171(__this, method) ((  void (*) (InternalEnumerator_1_t3590834561 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3889781171_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3600991820_gshared (InternalEnumerator_1_t3590834561 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3600991820(__this, method) ((  bool (*) (InternalEnumerator_1_t3590834561 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3600991820_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Current()
extern "C"  TouchState_t2732082299  InternalEnumerator_1_get_Current_m3631478691_gshared (InternalEnumerator_1_t3590834561 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3631478691(__this, method) ((  TouchState_t2732082299  (*) (InternalEnumerator_1_t3590834561 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3631478691_gshared)(__this, method)
