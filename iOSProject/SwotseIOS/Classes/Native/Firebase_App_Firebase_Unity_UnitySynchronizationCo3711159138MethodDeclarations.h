﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct U3CSendCoroutineU3Ec__AnonStorey2_t3711159138;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__AnonStorey2__ctor_m3756328939 (U3CSendCoroutineU3Ec__AnonStorey2_t3711159138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
