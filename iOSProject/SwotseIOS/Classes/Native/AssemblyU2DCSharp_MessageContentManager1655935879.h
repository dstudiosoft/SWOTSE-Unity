﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UserMessageReportModel
struct UserMessageReportModel_t835005669;
// MailInboxTableController
struct MailInboxTableController_t27963299;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageContentManager
struct  MessageContentManager_t1655935879  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MessageContentManager::readContent
	GameObject_t1756533147 * ___readContent_2;
	// UnityEngine.GameObject MessageContentManager::replyContent
	GameObject_t1756533147 * ___replyContent_3;
	// UnityEngine.UI.Text MessageContentManager::username
	Text_t356221433 * ___username_4;
	// UnityEngine.UI.Image MessageContentManager::userIcon
	Image_t2042527209 * ___userIcon_5;
	// UnityEngine.UI.Text MessageContentManager::userRank
	Text_t356221433 * ___userRank_6;
	// UnityEngine.UI.Text MessageContentManager::userAlliance
	Text_t356221433 * ___userAlliance_7;
	// UnityEngine.UI.Text MessageContentManager::userPosition
	Text_t356221433 * ___userPosition_8;
	// UnityEngine.UI.Text MessageContentManager::userExperiance
	Text_t356221433 * ___userExperiance_9;
	// UnityEngine.UI.Text MessageContentManager::messageSubject
	Text_t356221433 * ___messageSubject_10;
	// UnityEngine.UI.Text MessageContentManager::messageText
	Text_t356221433 * ___messageText_11;
	// UnityEngine.UI.Text MessageContentManager::messageDate
	Text_t356221433 * ___messageDate_12;
	// UnityEngine.UI.Text MessageContentManager::initialSubject
	Text_t356221433 * ___initialSubject_13;
	// UnityEngine.UI.InputField MessageContentManager::initialMessage
	InputField_t1631627530 * ___initialMessage_14;
	// UnityEngine.UI.InputField MessageContentManager::replySubject
	InputField_t1631627530 * ___replySubject_15;
	// UnityEngine.UI.InputField MessageContentManager::replyMessage
	InputField_t1631627530 * ___replyMessage_16;
	// System.Int64 MessageContentManager::messageId
	int64_t ___messageId_17;
	// System.Boolean MessageContentManager::reply
	bool ___reply_18;
	// UserMessageReportModel MessageContentManager::message
	UserMessageReportModel_t835005669 * ___message_19;
	// MailInboxTableController MessageContentManager::ownerTable
	MailInboxTableController_t27963299 * ___ownerTable_20;
	// System.String MessageContentManager::dateFormat
	String_t* ___dateFormat_21;
	// SimpleEvent MessageContentManager::gotMessageDetails
	SimpleEvent_t1509017202 * ___gotMessageDetails_22;

public:
	inline static int32_t get_offset_of_readContent_2() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___readContent_2)); }
	inline GameObject_t1756533147 * get_readContent_2() const { return ___readContent_2; }
	inline GameObject_t1756533147 ** get_address_of_readContent_2() { return &___readContent_2; }
	inline void set_readContent_2(GameObject_t1756533147 * value)
	{
		___readContent_2 = value;
		Il2CppCodeGenWriteBarrier(&___readContent_2, value);
	}

	inline static int32_t get_offset_of_replyContent_3() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___replyContent_3)); }
	inline GameObject_t1756533147 * get_replyContent_3() const { return ___replyContent_3; }
	inline GameObject_t1756533147 ** get_address_of_replyContent_3() { return &___replyContent_3; }
	inline void set_replyContent_3(GameObject_t1756533147 * value)
	{
		___replyContent_3 = value;
		Il2CppCodeGenWriteBarrier(&___replyContent_3, value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___username_4)); }
	inline Text_t356221433 * get_username_4() const { return ___username_4; }
	inline Text_t356221433 ** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(Text_t356221433 * value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier(&___username_4, value);
	}

	inline static int32_t get_offset_of_userIcon_5() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___userIcon_5)); }
	inline Image_t2042527209 * get_userIcon_5() const { return ___userIcon_5; }
	inline Image_t2042527209 ** get_address_of_userIcon_5() { return &___userIcon_5; }
	inline void set_userIcon_5(Image_t2042527209 * value)
	{
		___userIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___userIcon_5, value);
	}

	inline static int32_t get_offset_of_userRank_6() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___userRank_6)); }
	inline Text_t356221433 * get_userRank_6() const { return ___userRank_6; }
	inline Text_t356221433 ** get_address_of_userRank_6() { return &___userRank_6; }
	inline void set_userRank_6(Text_t356221433 * value)
	{
		___userRank_6 = value;
		Il2CppCodeGenWriteBarrier(&___userRank_6, value);
	}

	inline static int32_t get_offset_of_userAlliance_7() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___userAlliance_7)); }
	inline Text_t356221433 * get_userAlliance_7() const { return ___userAlliance_7; }
	inline Text_t356221433 ** get_address_of_userAlliance_7() { return &___userAlliance_7; }
	inline void set_userAlliance_7(Text_t356221433 * value)
	{
		___userAlliance_7 = value;
		Il2CppCodeGenWriteBarrier(&___userAlliance_7, value);
	}

	inline static int32_t get_offset_of_userPosition_8() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___userPosition_8)); }
	inline Text_t356221433 * get_userPosition_8() const { return ___userPosition_8; }
	inline Text_t356221433 ** get_address_of_userPosition_8() { return &___userPosition_8; }
	inline void set_userPosition_8(Text_t356221433 * value)
	{
		___userPosition_8 = value;
		Il2CppCodeGenWriteBarrier(&___userPosition_8, value);
	}

	inline static int32_t get_offset_of_userExperiance_9() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___userExperiance_9)); }
	inline Text_t356221433 * get_userExperiance_9() const { return ___userExperiance_9; }
	inline Text_t356221433 ** get_address_of_userExperiance_9() { return &___userExperiance_9; }
	inline void set_userExperiance_9(Text_t356221433 * value)
	{
		___userExperiance_9 = value;
		Il2CppCodeGenWriteBarrier(&___userExperiance_9, value);
	}

	inline static int32_t get_offset_of_messageSubject_10() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___messageSubject_10)); }
	inline Text_t356221433 * get_messageSubject_10() const { return ___messageSubject_10; }
	inline Text_t356221433 ** get_address_of_messageSubject_10() { return &___messageSubject_10; }
	inline void set_messageSubject_10(Text_t356221433 * value)
	{
		___messageSubject_10 = value;
		Il2CppCodeGenWriteBarrier(&___messageSubject_10, value);
	}

	inline static int32_t get_offset_of_messageText_11() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___messageText_11)); }
	inline Text_t356221433 * get_messageText_11() const { return ___messageText_11; }
	inline Text_t356221433 ** get_address_of_messageText_11() { return &___messageText_11; }
	inline void set_messageText_11(Text_t356221433 * value)
	{
		___messageText_11 = value;
		Il2CppCodeGenWriteBarrier(&___messageText_11, value);
	}

	inline static int32_t get_offset_of_messageDate_12() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___messageDate_12)); }
	inline Text_t356221433 * get_messageDate_12() const { return ___messageDate_12; }
	inline Text_t356221433 ** get_address_of_messageDate_12() { return &___messageDate_12; }
	inline void set_messageDate_12(Text_t356221433 * value)
	{
		___messageDate_12 = value;
		Il2CppCodeGenWriteBarrier(&___messageDate_12, value);
	}

	inline static int32_t get_offset_of_initialSubject_13() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___initialSubject_13)); }
	inline Text_t356221433 * get_initialSubject_13() const { return ___initialSubject_13; }
	inline Text_t356221433 ** get_address_of_initialSubject_13() { return &___initialSubject_13; }
	inline void set_initialSubject_13(Text_t356221433 * value)
	{
		___initialSubject_13 = value;
		Il2CppCodeGenWriteBarrier(&___initialSubject_13, value);
	}

	inline static int32_t get_offset_of_initialMessage_14() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___initialMessage_14)); }
	inline InputField_t1631627530 * get_initialMessage_14() const { return ___initialMessage_14; }
	inline InputField_t1631627530 ** get_address_of_initialMessage_14() { return &___initialMessage_14; }
	inline void set_initialMessage_14(InputField_t1631627530 * value)
	{
		___initialMessage_14 = value;
		Il2CppCodeGenWriteBarrier(&___initialMessage_14, value);
	}

	inline static int32_t get_offset_of_replySubject_15() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___replySubject_15)); }
	inline InputField_t1631627530 * get_replySubject_15() const { return ___replySubject_15; }
	inline InputField_t1631627530 ** get_address_of_replySubject_15() { return &___replySubject_15; }
	inline void set_replySubject_15(InputField_t1631627530 * value)
	{
		___replySubject_15 = value;
		Il2CppCodeGenWriteBarrier(&___replySubject_15, value);
	}

	inline static int32_t get_offset_of_replyMessage_16() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___replyMessage_16)); }
	inline InputField_t1631627530 * get_replyMessage_16() const { return ___replyMessage_16; }
	inline InputField_t1631627530 ** get_address_of_replyMessage_16() { return &___replyMessage_16; }
	inline void set_replyMessage_16(InputField_t1631627530 * value)
	{
		___replyMessage_16 = value;
		Il2CppCodeGenWriteBarrier(&___replyMessage_16, value);
	}

	inline static int32_t get_offset_of_messageId_17() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___messageId_17)); }
	inline int64_t get_messageId_17() const { return ___messageId_17; }
	inline int64_t* get_address_of_messageId_17() { return &___messageId_17; }
	inline void set_messageId_17(int64_t value)
	{
		___messageId_17 = value;
	}

	inline static int32_t get_offset_of_reply_18() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___reply_18)); }
	inline bool get_reply_18() const { return ___reply_18; }
	inline bool* get_address_of_reply_18() { return &___reply_18; }
	inline void set_reply_18(bool value)
	{
		___reply_18 = value;
	}

	inline static int32_t get_offset_of_message_19() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___message_19)); }
	inline UserMessageReportModel_t835005669 * get_message_19() const { return ___message_19; }
	inline UserMessageReportModel_t835005669 ** get_address_of_message_19() { return &___message_19; }
	inline void set_message_19(UserMessageReportModel_t835005669 * value)
	{
		___message_19 = value;
		Il2CppCodeGenWriteBarrier(&___message_19, value);
	}

	inline static int32_t get_offset_of_ownerTable_20() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___ownerTable_20)); }
	inline MailInboxTableController_t27963299 * get_ownerTable_20() const { return ___ownerTable_20; }
	inline MailInboxTableController_t27963299 ** get_address_of_ownerTable_20() { return &___ownerTable_20; }
	inline void set_ownerTable_20(MailInboxTableController_t27963299 * value)
	{
		___ownerTable_20 = value;
		Il2CppCodeGenWriteBarrier(&___ownerTable_20, value);
	}

	inline static int32_t get_offset_of_dateFormat_21() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___dateFormat_21)); }
	inline String_t* get_dateFormat_21() const { return ___dateFormat_21; }
	inline String_t** get_address_of_dateFormat_21() { return &___dateFormat_21; }
	inline void set_dateFormat_21(String_t* value)
	{
		___dateFormat_21 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_21, value);
	}

	inline static int32_t get_offset_of_gotMessageDetails_22() { return static_cast<int32_t>(offsetof(MessageContentManager_t1655935879, ___gotMessageDetails_22)); }
	inline SimpleEvent_t1509017202 * get_gotMessageDetails_22() const { return ___gotMessageDetails_22; }
	inline SimpleEvent_t1509017202 ** get_address_of_gotMessageDetails_22() { return &___gotMessageDetails_22; }
	inline void set_gotMessageDetails_22(SimpleEvent_t1509017202 * value)
	{
		___gotMessageDetails_22 = value;
		Il2CppCodeGenWriteBarrier(&___gotMessageDetails_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
