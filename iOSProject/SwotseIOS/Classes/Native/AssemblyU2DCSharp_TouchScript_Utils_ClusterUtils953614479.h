﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.ClusterUtils
struct  ClusterUtils_t953614479  : public Il2CppObject
{
public:

public:
};

struct ClusterUtils_t953614479_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.ClusterUtils::hashString
	StringBuilder_t1221177846 * ___hashString_0;

public:
	inline static int32_t get_offset_of_hashString_0() { return static_cast<int32_t>(offsetof(ClusterUtils_t953614479_StaticFields, ___hashString_0)); }
	inline StringBuilder_t1221177846 * get_hashString_0() const { return ___hashString_0; }
	inline StringBuilder_t1221177846 ** get_address_of_hashString_0() { return &___hashString_0; }
	inline void set_hashString_0(StringBuilder_t1221177846 * value)
	{
		___hashString_0 = value;
		Il2CppCodeGenWriteBarrier(&___hashString_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
