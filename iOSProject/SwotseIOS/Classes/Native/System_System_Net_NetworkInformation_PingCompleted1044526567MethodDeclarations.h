﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.PingCompletedEventHandler
struct PingCompletedEventHandler_t1044526567;
// System.Object
struct Il2CppObject;
// System.Net.NetworkInformation.PingCompletedEventArgs
struct PingCompletedEventArgs_t841787612;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_System_Net_NetworkInformation_PingCompletedE841787612.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.NetworkInformation.PingCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PingCompletedEventHandler__ctor_m917895965 (PingCompletedEventHandler_t1044526567 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.PingCompletedEventHandler::Invoke(System.Object,System.Net.NetworkInformation.PingCompletedEventArgs)
extern "C"  void PingCompletedEventHandler_Invoke_m2377391675 (PingCompletedEventHandler_t1044526567 * __this, Il2CppObject * ___sender0, PingCompletedEventArgs_t841787612 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.NetworkInformation.PingCompletedEventHandler::BeginInvoke(System.Object,System.Net.NetworkInformation.PingCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PingCompletedEventHandler_BeginInvoke_m3702415020 (PingCompletedEventHandler_t1044526567 * __this, Il2CppObject * ___sender0, PingCompletedEventArgs_t841787612 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.PingCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PingCompletedEventHandler_EndInvoke_m1139170619 (PingCompletedEventHandler_t1044526567 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
