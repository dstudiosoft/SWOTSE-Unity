﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceInfoAlliancesLine
struct AllianceInfoAlliancesLine_t3814560029;
// AllianceInfoTableContoller
struct AllianceInfoTableContoller_t4166874261;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AllianceInfoTableContoller4166874261.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceInfoAlliancesLine::.ctor()
extern "C"  void AllianceInfoAlliancesLine__ctor_m2846363862 (AllianceInfoAlliancesLine_t3814560029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetOwner(AllianceInfoTableContoller)
extern "C"  void AllianceInfoAlliancesLine_SetOwner_m2091388594 (AllianceInfoAlliancesLine_t3814560029 * __this, AllianceInfoTableContoller_t4166874261 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetAllianceId(System.Int64)
extern "C"  void AllianceInfoAlliancesLine_SetAllianceId_m3366044874 (AllianceInfoAlliancesLine_t3814560029 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetAllianceName(System.String)
extern "C"  void AllianceInfoAlliancesLine_SetAllianceName_m2009646346 (AllianceInfoAlliancesLine_t3814560029 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetRank(System.Int64)
extern "C"  void AllianceInfoAlliancesLine_SetRank_m3035896244 (AllianceInfoAlliancesLine_t3814560029 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetMembers(System.Int64)
extern "C"  void AllianceInfoAlliancesLine_SetMembers_m425876015 (AllianceInfoAlliancesLine_t3814560029 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::SetNeutral(System.Boolean)
extern "C"  void AllianceInfoAlliancesLine_SetNeutral_m1867940314 (AllianceInfoAlliancesLine_t3814560029 * __this, bool ___neutral0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::MakeNeutral()
extern "C"  void AllianceInfoAlliancesLine_MakeNeutral_m3965620679 (AllianceInfoAlliancesLine_t3814560029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::MakeEnemy()
extern "C"  void AllianceInfoAlliancesLine_MakeEnemy_m3447388288 (AllianceInfoAlliancesLine_t3814560029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceInfoAlliancesLine::MakeAlly()
extern "C"  void AllianceInfoAlliancesLine_MakeAlly_m594036944 (AllianceInfoAlliancesLine_t3814560029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
