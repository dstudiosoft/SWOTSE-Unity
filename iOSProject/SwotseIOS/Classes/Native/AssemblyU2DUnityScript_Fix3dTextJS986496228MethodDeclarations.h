﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fix3dTextJS
struct Fix3dTextJS_t986496228;

#include "codegen/il2cpp-codegen.h"

// System.Void Fix3dTextJS::.ctor()
extern "C"  void Fix3dTextJS__ctor_m2799989664 (Fix3dTextJS_t986496228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fix3dTextJS::Start()
extern "C"  void Fix3dTextJS_Start_m241462068 (Fix3dTextJS_t986496228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fix3dTextJS::Update()
extern "C"  void Fix3dTextJS_Update_m1673743639 (Fix3dTextJS_t986496228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fix3dTextJS::Main()
extern "C"  void Fix3dTextJS_Main_m2300694381 (Fix3dTextJS_t986496228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
