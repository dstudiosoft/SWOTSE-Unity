﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager
struct SoundManager_t654432262;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m3417712111 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Start()
extern "C"  void SoundManager_Start_m640423507 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::switchMusic(System.Boolean)
extern "C"  void SoundManager_switchMusic_m2953669963 (SoundManager_t654432262 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
