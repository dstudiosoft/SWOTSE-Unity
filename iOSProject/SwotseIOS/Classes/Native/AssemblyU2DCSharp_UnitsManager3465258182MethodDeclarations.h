﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitsManager
struct UnitsManager_t3465258182;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;
// System.String
struct String_t;
// UnitsModel
struct UnitsModel_t1926818124;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_ArmyGeneralModel609759248.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_UnitsModel1926818124.h"

// System.Void UnitsManager::.ctor()
extern "C"  void UnitsManager__ctor_m1077974437 (UnitsManager_t3465258182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onUnitsHired(SimpleEvent)
extern "C"  void UnitsManager_add_onUnitsHired_m1574502791 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onUnitsHired(SimpleEvent)
extern "C"  void UnitsManager_remove_onUnitsHired_m531018282 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onGeneralHired(SimpleEvent)
extern "C"  void UnitsManager_add_onGeneralHired_m2432172322 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onGeneralHired(SimpleEvent)
extern "C"  void UnitsManager_remove_onGeneralHired_m4061680519 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onGeneralDismissed(SimpleEvent)
extern "C"  void UnitsManager_add_onGeneralDismissed_m2011863745 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onGeneralDismissed(SimpleEvent)
extern "C"  void UnitsManager_remove_onGeneralDismissed_m3153623248 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onUnitsUpdated(SimpleEvent)
extern "C"  void UnitsManager_add_onUnitsUpdated_m1129292918 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onUnitsUpdated(SimpleEvent)
extern "C"  void UnitsManager_remove_onUnitsUpdated_m625282891 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onGeneralUpgraded(SimpleEvent)
extern "C"  void UnitsManager_add_onGeneralUpgraded_m3434378978 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onGeneralUpgraded(SimpleEvent)
extern "C"  void UnitsManager_remove_onGeneralUpgraded_m827717593 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onTrainingCancelled(SimpleEvent)
extern "C"  void UnitsManager_add_onTrainingCancelled_m3288843023 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onTrainingCancelled(SimpleEvent)
extern "C"  void UnitsManager_remove_onTrainingCancelled_m760564880 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onUnitsDismissed(SimpleEvent)
extern "C"  void UnitsManager_add_onUnitsDismissed_m2849813708 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onUnitsDismissed(SimpleEvent)
extern "C"  void UnitsManager_remove_onUnitsDismissed_m546600637 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::add_onGetGenerals(SimpleEvent)
extern "C"  void UnitsManager_add_onGetGenerals_m2705913327 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitsManager::remove_onGetGenerals(SimpleEvent)
extern "C"  void UnitsManager_remove_onGetGenerals_m1908815192 (UnitsManager_t3465258182 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::GetInitialCityUnits()
extern "C"  Il2CppObject * UnitsManager_GetInitialCityUnits_m1031132953 (UnitsManager_t3465258182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::GetInitialCityGenerals()
extern "C"  Il2CppObject * UnitsManager_GetInitialCityGenerals_m3851746293 (UnitsManager_t3465258182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::UpdateCityUnits()
extern "C"  Il2CppObject * UnitsManager_UpdateCityUnits_m1267531788 (UnitsManager_t3465258182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::UpdateCityGenerals()
extern "C"  Il2CppObject * UnitsManager_UpdateCityGenerals_m2286314274 (UnitsManager_t3465258182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::HireGeneral(ArmyGeneralModel)
extern "C"  Il2CppObject * UnitsManager_HireGeneral_m4239308669 (UnitsManager_t3465258182 * __this, ArmyGeneralModel_t609759248 * ___general0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::DismissGeneral(ArmyGeneralModel)
extern "C"  Il2CppObject * UnitsManager_DismissGeneral_m2571211997 (UnitsManager_t3465258182 * __this, ArmyGeneralModel_t609759248 * ___general0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::HireUnits(System.Int64,System.String)
extern "C"  Il2CppObject * UnitsManager_HireUnits_m76300852 (UnitsManager_t3465258182 * __this, int64_t ___count0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::UpgradeGeneral(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64,System.Int64)
extern "C"  Il2CppObject * UnitsManager_UpgradeGeneral_m235361461 (UnitsManager_t3465258182 * __this, int64_t ___heroId0, int64_t ___march1, int64_t ___politics2, int64_t ___speed3, int64_t ___salary4, int64_t ___loyalty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::CancelTraining(System.Int64)
extern "C"  Il2CppObject * UnitsManager_CancelTraining_m932447435 (UnitsManager_t3465258182 * __this, int64_t ___event_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitsManager::DismissUnits(UnitsModel)
extern "C"  Il2CppObject * UnitsManager_DismissUnits_m4088281322 (UnitsManager_t3465258182 * __this, UnitsModel_t1926818124 * ___army0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
