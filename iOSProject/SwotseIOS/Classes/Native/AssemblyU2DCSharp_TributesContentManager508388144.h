﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResourcesModel
struct ResourcesModel_t2978985958;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TributesContentManager
struct  TributesContentManager_t508388144  : public MonoBehaviour_t1158329972
{
public:
	// ResourcesModel TributesContentManager::tributes
	ResourcesModel_t2978985958 * ___tributes_2;
	// UnityEngine.UI.Text TributesContentManager::tributesFood
	Text_t356221433 * ___tributesFood_3;
	// UnityEngine.UI.Text TributesContentManager::tributesWood
	Text_t356221433 * ___tributesWood_4;
	// UnityEngine.UI.Text TributesContentManager::tributesIron
	Text_t356221433 * ___tributesIron_5;
	// UnityEngine.UI.Text TributesContentManager::tributesStone
	Text_t356221433 * ___tributesStone_6;
	// UnityEngine.UI.Text TributesContentManager::tributesCopper
	Text_t356221433 * ___tributesCopper_7;
	// UnityEngine.UI.Text TributesContentManager::tributesSilver
	Text_t356221433 * ___tributesSilver_8;
	// UnityEngine.UI.Text TributesContentManager::tributesGold
	Text_t356221433 * ___tributesGold_9;

public:
	inline static int32_t get_offset_of_tributes_2() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributes_2)); }
	inline ResourcesModel_t2978985958 * get_tributes_2() const { return ___tributes_2; }
	inline ResourcesModel_t2978985958 ** get_address_of_tributes_2() { return &___tributes_2; }
	inline void set_tributes_2(ResourcesModel_t2978985958 * value)
	{
		___tributes_2 = value;
		Il2CppCodeGenWriteBarrier(&___tributes_2, value);
	}

	inline static int32_t get_offset_of_tributesFood_3() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesFood_3)); }
	inline Text_t356221433 * get_tributesFood_3() const { return ___tributesFood_3; }
	inline Text_t356221433 ** get_address_of_tributesFood_3() { return &___tributesFood_3; }
	inline void set_tributesFood_3(Text_t356221433 * value)
	{
		___tributesFood_3 = value;
		Il2CppCodeGenWriteBarrier(&___tributesFood_3, value);
	}

	inline static int32_t get_offset_of_tributesWood_4() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesWood_4)); }
	inline Text_t356221433 * get_tributesWood_4() const { return ___tributesWood_4; }
	inline Text_t356221433 ** get_address_of_tributesWood_4() { return &___tributesWood_4; }
	inline void set_tributesWood_4(Text_t356221433 * value)
	{
		___tributesWood_4 = value;
		Il2CppCodeGenWriteBarrier(&___tributesWood_4, value);
	}

	inline static int32_t get_offset_of_tributesIron_5() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesIron_5)); }
	inline Text_t356221433 * get_tributesIron_5() const { return ___tributesIron_5; }
	inline Text_t356221433 ** get_address_of_tributesIron_5() { return &___tributesIron_5; }
	inline void set_tributesIron_5(Text_t356221433 * value)
	{
		___tributesIron_5 = value;
		Il2CppCodeGenWriteBarrier(&___tributesIron_5, value);
	}

	inline static int32_t get_offset_of_tributesStone_6() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesStone_6)); }
	inline Text_t356221433 * get_tributesStone_6() const { return ___tributesStone_6; }
	inline Text_t356221433 ** get_address_of_tributesStone_6() { return &___tributesStone_6; }
	inline void set_tributesStone_6(Text_t356221433 * value)
	{
		___tributesStone_6 = value;
		Il2CppCodeGenWriteBarrier(&___tributesStone_6, value);
	}

	inline static int32_t get_offset_of_tributesCopper_7() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesCopper_7)); }
	inline Text_t356221433 * get_tributesCopper_7() const { return ___tributesCopper_7; }
	inline Text_t356221433 ** get_address_of_tributesCopper_7() { return &___tributesCopper_7; }
	inline void set_tributesCopper_7(Text_t356221433 * value)
	{
		___tributesCopper_7 = value;
		Il2CppCodeGenWriteBarrier(&___tributesCopper_7, value);
	}

	inline static int32_t get_offset_of_tributesSilver_8() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesSilver_8)); }
	inline Text_t356221433 * get_tributesSilver_8() const { return ___tributesSilver_8; }
	inline Text_t356221433 ** get_address_of_tributesSilver_8() { return &___tributesSilver_8; }
	inline void set_tributesSilver_8(Text_t356221433 * value)
	{
		___tributesSilver_8 = value;
		Il2CppCodeGenWriteBarrier(&___tributesSilver_8, value);
	}

	inline static int32_t get_offset_of_tributesGold_9() { return static_cast<int32_t>(offsetof(TributesContentManager_t508388144, ___tributesGold_9)); }
	inline Text_t356221433 * get_tributesGold_9() const { return ___tributesGold_9; }
	inline Text_t356221433 ** get_address_of_tributesGold_9() { return &___tributesGold_9; }
	inline void set_tributesGold_9(Text_t356221433 * value)
	{
		___tributesGold_9 = value;
		Il2CppCodeGenWriteBarrier(&___tributesGold_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
