﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.OrthographicCameraTransformGesture
struct OrthographicCameraTransformGesture_t1062121804;
// UnityEngine.Transform
struct Transform_t3275118058;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_ProjectionPar2712959773.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::.ctor()
extern "C"  void OrthographicCameraTransformGesture__ctor_m3169183959 (OrthographicCameraTransformGesture_t1062121804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::Start()
extern "C"  void OrthographicCameraTransformGesture_Start_m4055086827 (OrthographicCameraTransformGesture_t1062121804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::ApplyTransform(UnityEngine.Transform)
extern "C"  void OrthographicCameraTransformGesture_ApplyTransform_m2854099790 (OrthographicCameraTransformGesture_t1062121804 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.OrthographicCameraTransformGesture::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float OrthographicCameraTransformGesture_doRotation_m3176231097 (OrthographicCameraTransformGesture_t1062121804 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.OrthographicCameraTransformGesture::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  float OrthographicCameraTransformGesture_doScaling_m3889964282 (OrthographicCameraTransformGesture_t1062121804 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, ProjectionParams_t2712959773 * ___projectionParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.OrthographicCameraTransformGesture::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  OrthographicCameraTransformGesture_doOnePointTranslation_m1944103630 (OrthographicCameraTransformGesture_t1062121804 * __this, Vector2_t2243707579  ___oldScreenPos0, Vector2_t2243707579  ___newScreenPos1, ProjectionParams_t2712959773 * ___projectionParams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Gestures.OrthographicCameraTransformGesture::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern "C"  Vector3_t2243707580  OrthographicCameraTransformGesture_doTwoPointTranslation_m42889292 (OrthographicCameraTransformGesture_t1062121804 * __this, Vector2_t2243707579  ___oldScreenPos10, Vector2_t2243707579  ___oldScreenPos21, Vector2_t2243707579  ___newScreenPos12, Vector2_t2243707579  ___newScreenPos23, float ___dR4, float ___dS5, ProjectionParams_t2712959773 * ___projectionParams6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Gestures.OrthographicCameraTransformGesture::scaleAndRotate(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern "C"  Vector2_t2243707579  OrthographicCameraTransformGesture_scaleAndRotate_m2654185122 (OrthographicCameraTransformGesture_t1062121804 * __this, Vector2_t2243707579  ___point0, Vector2_t2243707579  ___center1, float ___dR2, float ___dS3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformStarted_m4193191186 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformStarted_m3723028969 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.add_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_add_Transformed_m569702058 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_remove_Transformed_m2171851901 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_add_TransformCompleted_m535148670 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.OrthographicCameraTransformGesture::TouchScript.Gestures.ITransformGesture.remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern "C"  void OrthographicCameraTransformGesture_TouchScript_Gestures_ITransformGesture_remove_TransformCompleted_m230767853 (OrthographicCameraTransformGesture_t1062121804 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
