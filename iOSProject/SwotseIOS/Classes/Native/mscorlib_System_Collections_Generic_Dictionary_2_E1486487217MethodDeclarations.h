﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1700262710(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1486487217 *, Dictionary_2_t166462515 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1374515863(__this, method) ((  Il2CppObject * (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1200715955(__this, method) ((  void (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m544904608(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1607248705(__this, method) ((  Il2CppObject * (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3918230321(__this, method) ((  Il2CppObject * (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::MoveNext()
#define Enumerator_MoveNext_m251524183(__this, method) ((  bool (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::get_Current()
#define Enumerator_get_Current_m2774682687(__this, method) ((  KeyValuePair_2_t2218775033  (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2017306806(__this, method) ((  String_t* (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4245291062(__this, method) ((  BuildingConstantModel_t2546650549 * (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::Reset()
#define Enumerator_Reset_m2205040412(__this, method) ((  void (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::VerifyState()
#define Enumerator_VerifyState_m2653698205(__this, method) ((  void (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4186682395(__this, method) ((  void (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,BuildingConstantModel>::Dispose()
#define Enumerator_Dispose_m2709038098(__this, method) ((  void (*) (Enumerator_t1486487217 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
