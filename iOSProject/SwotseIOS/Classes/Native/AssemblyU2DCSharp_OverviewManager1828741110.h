﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverviewManager
struct  OverviewManager_t1828741110  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject OverviewManager::lordPanel
	GameObject_t1756533147 * ___lordPanel_2;
	// UnityEngine.GameObject OverviewManager::overviewPanel
	GameObject_t1756533147 * ___overviewPanel_3;
	// UnityEngine.UI.Image OverviewManager::playerImage
	Image_t2042527209 * ___playerImage_4;
	// UnityEngine.UI.Text OverviewManager::playerName
	Text_t356221433 * ___playerName_5;
	// UnityEngine.UI.Text OverviewManager::playerRank
	Text_t356221433 * ___playerRank_6;
	// UnityEngine.UI.Text OverviewManager::playerAlliance
	Text_t356221433 * ___playerAlliance_7;
	// UnityEngine.UI.Text OverviewManager::playerExperience
	Text_t356221433 * ___playerExperience_8;
	// UnityEngine.UI.Text OverviewManager::cityName
	Text_t356221433 * ___cityName_9;
	// UnityEngine.UI.Text OverviewManager::cityRegion
	Text_t356221433 * ___cityRegion_10;
	// UnityEngine.UI.Text OverviewManager::cityCarrage
	Text_t356221433 * ___cityCarrage_11;
	// UnityEngine.UI.Text OverviewManager::cityHappiness
	Text_t356221433 * ___cityHappiness_12;
	// UnityEngine.UI.Text OverviewManager::cityPopulation
	Text_t356221433 * ___cityPopulation_13;
	// UnityEngine.UI.Text OverviewManager::cityIdlePopulation
	Text_t356221433 * ___cityIdlePopulation_14;
	// UnityEngine.UI.Text OverviewManager::cityTax
	Text_t356221433 * ___cityTax_15;
	// System.String OverviewManager::userImageName
	String_t* ___userImageName_16;

public:
	inline static int32_t get_offset_of_lordPanel_2() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___lordPanel_2)); }
	inline GameObject_t1756533147 * get_lordPanel_2() const { return ___lordPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_lordPanel_2() { return &___lordPanel_2; }
	inline void set_lordPanel_2(GameObject_t1756533147 * value)
	{
		___lordPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___lordPanel_2, value);
	}

	inline static int32_t get_offset_of_overviewPanel_3() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___overviewPanel_3)); }
	inline GameObject_t1756533147 * get_overviewPanel_3() const { return ___overviewPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_overviewPanel_3() { return &___overviewPanel_3; }
	inline void set_overviewPanel_3(GameObject_t1756533147 * value)
	{
		___overviewPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___overviewPanel_3, value);
	}

	inline static int32_t get_offset_of_playerImage_4() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___playerImage_4)); }
	inline Image_t2042527209 * get_playerImage_4() const { return ___playerImage_4; }
	inline Image_t2042527209 ** get_address_of_playerImage_4() { return &___playerImage_4; }
	inline void set_playerImage_4(Image_t2042527209 * value)
	{
		___playerImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerImage_4, value);
	}

	inline static int32_t get_offset_of_playerName_5() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___playerName_5)); }
	inline Text_t356221433 * get_playerName_5() const { return ___playerName_5; }
	inline Text_t356221433 ** get_address_of_playerName_5() { return &___playerName_5; }
	inline void set_playerName_5(Text_t356221433 * value)
	{
		___playerName_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_5, value);
	}

	inline static int32_t get_offset_of_playerRank_6() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___playerRank_6)); }
	inline Text_t356221433 * get_playerRank_6() const { return ___playerRank_6; }
	inline Text_t356221433 ** get_address_of_playerRank_6() { return &___playerRank_6; }
	inline void set_playerRank_6(Text_t356221433 * value)
	{
		___playerRank_6 = value;
		Il2CppCodeGenWriteBarrier(&___playerRank_6, value);
	}

	inline static int32_t get_offset_of_playerAlliance_7() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___playerAlliance_7)); }
	inline Text_t356221433 * get_playerAlliance_7() const { return ___playerAlliance_7; }
	inline Text_t356221433 ** get_address_of_playerAlliance_7() { return &___playerAlliance_7; }
	inline void set_playerAlliance_7(Text_t356221433 * value)
	{
		___playerAlliance_7 = value;
		Il2CppCodeGenWriteBarrier(&___playerAlliance_7, value);
	}

	inline static int32_t get_offset_of_playerExperience_8() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___playerExperience_8)); }
	inline Text_t356221433 * get_playerExperience_8() const { return ___playerExperience_8; }
	inline Text_t356221433 ** get_address_of_playerExperience_8() { return &___playerExperience_8; }
	inline void set_playerExperience_8(Text_t356221433 * value)
	{
		___playerExperience_8 = value;
		Il2CppCodeGenWriteBarrier(&___playerExperience_8, value);
	}

	inline static int32_t get_offset_of_cityName_9() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityName_9)); }
	inline Text_t356221433 * get_cityName_9() const { return ___cityName_9; }
	inline Text_t356221433 ** get_address_of_cityName_9() { return &___cityName_9; }
	inline void set_cityName_9(Text_t356221433 * value)
	{
		___cityName_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityName_9, value);
	}

	inline static int32_t get_offset_of_cityRegion_10() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityRegion_10)); }
	inline Text_t356221433 * get_cityRegion_10() const { return ___cityRegion_10; }
	inline Text_t356221433 ** get_address_of_cityRegion_10() { return &___cityRegion_10; }
	inline void set_cityRegion_10(Text_t356221433 * value)
	{
		___cityRegion_10 = value;
		Il2CppCodeGenWriteBarrier(&___cityRegion_10, value);
	}

	inline static int32_t get_offset_of_cityCarrage_11() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityCarrage_11)); }
	inline Text_t356221433 * get_cityCarrage_11() const { return ___cityCarrage_11; }
	inline Text_t356221433 ** get_address_of_cityCarrage_11() { return &___cityCarrage_11; }
	inline void set_cityCarrage_11(Text_t356221433 * value)
	{
		___cityCarrage_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityCarrage_11, value);
	}

	inline static int32_t get_offset_of_cityHappiness_12() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityHappiness_12)); }
	inline Text_t356221433 * get_cityHappiness_12() const { return ___cityHappiness_12; }
	inline Text_t356221433 ** get_address_of_cityHappiness_12() { return &___cityHappiness_12; }
	inline void set_cityHappiness_12(Text_t356221433 * value)
	{
		___cityHappiness_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityHappiness_12, value);
	}

	inline static int32_t get_offset_of_cityPopulation_13() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityPopulation_13)); }
	inline Text_t356221433 * get_cityPopulation_13() const { return ___cityPopulation_13; }
	inline Text_t356221433 ** get_address_of_cityPopulation_13() { return &___cityPopulation_13; }
	inline void set_cityPopulation_13(Text_t356221433 * value)
	{
		___cityPopulation_13 = value;
		Il2CppCodeGenWriteBarrier(&___cityPopulation_13, value);
	}

	inline static int32_t get_offset_of_cityIdlePopulation_14() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityIdlePopulation_14)); }
	inline Text_t356221433 * get_cityIdlePopulation_14() const { return ___cityIdlePopulation_14; }
	inline Text_t356221433 ** get_address_of_cityIdlePopulation_14() { return &___cityIdlePopulation_14; }
	inline void set_cityIdlePopulation_14(Text_t356221433 * value)
	{
		___cityIdlePopulation_14 = value;
		Il2CppCodeGenWriteBarrier(&___cityIdlePopulation_14, value);
	}

	inline static int32_t get_offset_of_cityTax_15() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___cityTax_15)); }
	inline Text_t356221433 * get_cityTax_15() const { return ___cityTax_15; }
	inline Text_t356221433 ** get_address_of_cityTax_15() { return &___cityTax_15; }
	inline void set_cityTax_15(Text_t356221433 * value)
	{
		___cityTax_15 = value;
		Il2CppCodeGenWriteBarrier(&___cityTax_15, value);
	}

	inline static int32_t get_offset_of_userImageName_16() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110, ___userImageName_16)); }
	inline String_t* get_userImageName_16() const { return ___userImageName_16; }
	inline String_t** get_address_of_userImageName_16() { return &___userImageName_16; }
	inline void set_userImageName_16(String_t* value)
	{
		___userImageName_16 = value;
		Il2CppCodeGenWriteBarrier(&___userImageName_16, value);
	}
};

struct OverviewManager_t1828741110_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OverviewManager::<>f__switch$mapF
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapF_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_17() { return static_cast<int32_t>(offsetof(OverviewManager_t1828741110_StaticFields, ___U3CU3Ef__switchU24mapF_17)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapF_17() const { return ___U3CU3Ef__switchU24mapF_17; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapF_17() { return &___U3CU3Ef__switchU24mapF_17; }
	inline void set_U3CU3Ef__switchU24mapF_17(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapF_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapF_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
