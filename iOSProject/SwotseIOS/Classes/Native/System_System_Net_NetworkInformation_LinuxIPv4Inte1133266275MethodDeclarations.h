﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics
struct LinuxIPv4InterfaceStatistics_t1133266275;
// System.Net.NetworkInformation.LinuxNetworkInterface
struct LinuxNetworkInterface_t3864470295;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::.ctor(System.Net.NetworkInformation.LinuxNetworkInterface)
extern "C"  void LinuxIPv4InterfaceStatistics__ctor_m1804543410 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, LinuxNetworkInterface_t3864470295 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::Read(System.String)
extern "C"  int64_t LinuxIPv4InterfaceStatistics_Read_m2653521628 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_BytesReceived()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_BytesReceived_m1677725307 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_BytesSent()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_BytesSent_m4260072414 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_IncomingPacketsDiscarded()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_IncomingPacketsDiscarded_m3700980703 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_IncomingPacketsWithErrors()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_IncomingPacketsWithErrors_m1961321409 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_IncomingUnknownProtocolPackets()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_IncomingUnknownProtocolPackets_m3781418560 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_NonUnicastPacketsReceived()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_NonUnicastPacketsReceived_m2128324005 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_NonUnicastPacketsSent()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_NonUnicastPacketsSent_m3703858820 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_OutgoingPacketsDiscarded()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_OutgoingPacketsDiscarded_m2826633739 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_OutgoingPacketsWithErrors()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_OutgoingPacketsWithErrors_m1154923977 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_OutputQueueLength()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_OutputQueueLength_m3843459815 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_UnicastPacketsReceived()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_UnicastPacketsReceived_m805357122 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.LinuxIPv4InterfaceStatistics::get_UnicastPacketsSent()
extern "C"  int64_t LinuxIPv4InterfaceStatistics_get_UnicastPacketsSent_m3847585725 (LinuxIPv4InterfaceStatistics_t1133266275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
