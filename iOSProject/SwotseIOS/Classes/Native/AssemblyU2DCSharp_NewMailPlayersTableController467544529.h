﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// NewMailContentManager
struct NewMailContentManager_t2045651277;
// NewMailPlayerLine
struct NewMailPlayerLine_t3159573886;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<UserModel>
struct List_1_t2394690348;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewMailPlayersTableController
struct  NewMailPlayersTableController_t467544529  : public MonoBehaviour_t1158329972
{
public:
	// NewMailContentManager NewMailPlayersTableController::owner
	NewMailContentManager_t2045651277 * ___owner_2;
	// NewMailPlayerLine NewMailPlayersTableController::m_cellPrefab
	NewMailPlayerLine_t3159573886 * ___m_cellPrefab_3;
	// Tacticsoft.TableView NewMailPlayersTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_4;
	// System.Collections.Generic.List`1<UserModel> NewMailPlayersTableController::allUsers
	List_1_t2394690348 * ___allUsers_5;
	// System.Int32 NewMailPlayersTableController::m_numRows
	int32_t ___m_numRows_6;
	// SimpleEvent NewMailPlayersTableController::onGetAllUsers
	SimpleEvent_t1509017202 * ___onGetAllUsers_7;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___owner_2)); }
	inline NewMailContentManager_t2045651277 * get_owner_2() const { return ___owner_2; }
	inline NewMailContentManager_t2045651277 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(NewMailContentManager_t2045651277 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___m_cellPrefab_3)); }
	inline NewMailPlayerLine_t3159573886 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline NewMailPlayerLine_t3159573886 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(NewMailPlayerLine_t3159573886 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___m_tableView_4)); }
	inline TableView_t3179510217 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t3179510217 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_4, value);
	}

	inline static int32_t get_offset_of_allUsers_5() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___allUsers_5)); }
	inline List_1_t2394690348 * get_allUsers_5() const { return ___allUsers_5; }
	inline List_1_t2394690348 ** get_address_of_allUsers_5() { return &___allUsers_5; }
	inline void set_allUsers_5(List_1_t2394690348 * value)
	{
		___allUsers_5 = value;
		Il2CppCodeGenWriteBarrier(&___allUsers_5, value);
	}

	inline static int32_t get_offset_of_m_numRows_6() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___m_numRows_6)); }
	inline int32_t get_m_numRows_6() const { return ___m_numRows_6; }
	inline int32_t* get_address_of_m_numRows_6() { return &___m_numRows_6; }
	inline void set_m_numRows_6(int32_t value)
	{
		___m_numRows_6 = value;
	}

	inline static int32_t get_offset_of_onGetAllUsers_7() { return static_cast<int32_t>(offsetof(NewMailPlayersTableController_t467544529, ___onGetAllUsers_7)); }
	inline SimpleEvent_t1509017202 * get_onGetAllUsers_7() const { return ___onGetAllUsers_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetAllUsers_7() { return &___onGetAllUsers_7; }
	inline void set_onGetAllUsers_7(SimpleEvent_t1509017202 * value)
	{
		___onGetAllUsers_7 = value;
		Il2CppCodeGenWriteBarrier(&___onGetAllUsers_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
