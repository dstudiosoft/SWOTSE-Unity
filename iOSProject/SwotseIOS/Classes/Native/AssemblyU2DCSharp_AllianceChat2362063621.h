﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceChat
struct  AllianceChat_t2362063621  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text AllianceChat::chatWindow
	Text_t356221433 * ___chatWindow_2;
	// UnityEngine.UI.InputField AllianceChat::input
	InputField_t1631627530 * ___input_3;
	// UnityEngine.UI.Scrollbar AllianceChat::Scroll
	Scrollbar_t3248359358 * ___Scroll_4;
	// System.String AllianceChat::MessageBuffer
	String_t* ___MessageBuffer_5;
	// System.Boolean AllianceChat::chatWorking
	bool ___chatWorking_6;
	// System.Boolean AllianceChat::readyToChat
	bool ___readyToChat_7;

public:
	inline static int32_t get_offset_of_chatWindow_2() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___chatWindow_2)); }
	inline Text_t356221433 * get_chatWindow_2() const { return ___chatWindow_2; }
	inline Text_t356221433 ** get_address_of_chatWindow_2() { return &___chatWindow_2; }
	inline void set_chatWindow_2(Text_t356221433 * value)
	{
		___chatWindow_2 = value;
		Il2CppCodeGenWriteBarrier(&___chatWindow_2, value);
	}

	inline static int32_t get_offset_of_input_3() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___input_3)); }
	inline InputField_t1631627530 * get_input_3() const { return ___input_3; }
	inline InputField_t1631627530 ** get_address_of_input_3() { return &___input_3; }
	inline void set_input_3(InputField_t1631627530 * value)
	{
		___input_3 = value;
		Il2CppCodeGenWriteBarrier(&___input_3, value);
	}

	inline static int32_t get_offset_of_Scroll_4() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___Scroll_4)); }
	inline Scrollbar_t3248359358 * get_Scroll_4() const { return ___Scroll_4; }
	inline Scrollbar_t3248359358 ** get_address_of_Scroll_4() { return &___Scroll_4; }
	inline void set_Scroll_4(Scrollbar_t3248359358 * value)
	{
		___Scroll_4 = value;
		Il2CppCodeGenWriteBarrier(&___Scroll_4, value);
	}

	inline static int32_t get_offset_of_MessageBuffer_5() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___MessageBuffer_5)); }
	inline String_t* get_MessageBuffer_5() const { return ___MessageBuffer_5; }
	inline String_t** get_address_of_MessageBuffer_5() { return &___MessageBuffer_5; }
	inline void set_MessageBuffer_5(String_t* value)
	{
		___MessageBuffer_5 = value;
		Il2CppCodeGenWriteBarrier(&___MessageBuffer_5, value);
	}

	inline static int32_t get_offset_of_chatWorking_6() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___chatWorking_6)); }
	inline bool get_chatWorking_6() const { return ___chatWorking_6; }
	inline bool* get_address_of_chatWorking_6() { return &___chatWorking_6; }
	inline void set_chatWorking_6(bool value)
	{
		___chatWorking_6 = value;
	}

	inline static int32_t get_offset_of_readyToChat_7() { return static_cast<int32_t>(offsetof(AllianceChat_t2362063621, ___readyToChat_7)); }
	inline bool get_readyToChat_7() const { return ___readyToChat_7; }
	inline bool* get_address_of_readyToChat_7() { return &___readyToChat_7; }
	inline void set_readyToChat_7(bool value)
	{
		___readyToChat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
