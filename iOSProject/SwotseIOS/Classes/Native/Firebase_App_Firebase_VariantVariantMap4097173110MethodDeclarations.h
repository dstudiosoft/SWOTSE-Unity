﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.VariantVariantMap
struct VariantVariantMap_t4097173110;
// Firebase.Variant
struct Variant_t4275788079;
// System.Collections.Generic.ICollection`1<Firebase.Variant>
struct ICollection_1_t932896088;
// System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>[]
struct KeyValuePair_2U5BU5D_t3941927448;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>>
struct IEnumerator_1_t49160552;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.VariantVariantMap/VariantVariantMapEnumerator
struct VariantVariantMapEnumerator_t647394310;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_VariantVariantMap4097173110.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_Variant4275788079.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22573636725.h"

// System.Void Firebase.VariantVariantMap::.ctor(System.IntPtr,System.Boolean)
extern "C"  void VariantVariantMap__ctor_m3688336943 (VariantVariantMap_t4097173110 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::.ctor()
extern "C"  void VariantVariantMap__ctor_m1624292122 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::.ctor(Firebase.VariantVariantMap)
extern "C"  void VariantVariantMap__ctor_m62891437 (VariantVariantMap_t4097173110 * __this, VariantVariantMap_t4097173110 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.VariantVariantMap::getCPtr(Firebase.VariantVariantMap)
extern "C"  HandleRef_t2419939847  VariantVariantMap_getCPtr_m2573621934 (Il2CppObject * __this /* static, unused */, VariantVariantMap_t4097173110 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::Finalize()
extern "C"  void VariantVariantMap_Finalize_m786151362 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::Dispose()
extern "C"  void VariantVariantMap_Dispose_m285674345 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantVariantMap::get_Item(Firebase.Variant)
extern "C"  Variant_t4275788079 * VariantVariantMap_get_Item_m1172217265 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::set_Item(Firebase.Variant,Firebase.Variant)
extern "C"  void VariantVariantMap_set_Item_m3977757888 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, Variant_t4275788079 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::TryGetValue(Firebase.Variant,Firebase.Variant&)
extern "C"  bool VariantVariantMap_TryGetValue_m478234614 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, Variant_t4275788079 ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.VariantVariantMap::get_Count()
extern "C"  int32_t VariantVariantMap_get_Count_m35770112 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::get_IsReadOnly()
extern "C"  bool VariantVariantMap_get_IsReadOnly_m1177786335 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<Firebase.Variant> Firebase.VariantVariantMap::get_Keys()
extern "C"  Il2CppObject* VariantVariantMap_get_Keys_m268021939 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<Firebase.Variant> Firebase.VariantVariantMap::get_Values()
extern "C"  Il2CppObject* VariantVariantMap_get_Values_m3956183331 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::Add(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern "C"  void VariantVariantMap_Add_m1598290968 (VariantVariantMap_t4097173110 * __this, KeyValuePair_2_t2573636725  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::Remove(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern "C"  bool VariantVariantMap_Remove_m1681526281 (VariantVariantMap_t4097173110 * __this, KeyValuePair_2_t2573636725  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::Contains(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>)
extern "C"  bool VariantVariantMap_Contains_m2476831922 (VariantVariantMap_t4097173110 * __this, KeyValuePair_2_t2573636725  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::CopyTo(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>[])
extern "C"  void VariantVariantMap_CopyTo_m713830243 (VariantVariantMap_t4097173110 * __this, KeyValuePair_2U5BU5D_t3941927448* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::CopyTo(System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>[],System.Int32)
extern "C"  void VariantVariantMap_CopyTo_m3971478788 (VariantVariantMap_t4097173110 * __this, KeyValuePair_2U5BU5D_t3941927448* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Variant,Firebase.Variant>> Firebase.VariantVariantMap::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<Firebase.Variant,Firebase.Variant>>.GetEnumerator()
extern "C"  Il2CppObject* VariantVariantMap_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CFirebase_VariantU2CFirebase_VariantU3EU3E_GetEnumerator_m4082591774 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.VariantVariantMap::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * VariantVariantMap_System_Collections_IEnumerable_GetEnumerator_m2990119891 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantVariantMap/VariantVariantMapEnumerator Firebase.VariantVariantMap::GetEnumerator()
extern "C"  VariantVariantMapEnumerator_t647394310 * VariantVariantMap_GetEnumerator_m2980613391 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.VariantVariantMap::size()
extern "C"  uint32_t VariantVariantMap_size_m3605588100 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::empty()
extern "C"  bool VariantVariantMap_empty_m4266813707 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::Clear()
extern "C"  void VariantVariantMap_Clear_m4016910215 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantVariantMap::getitem(Firebase.Variant)
extern "C"  Variant_t4275788079 * VariantVariantMap_getitem_m4007331056 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::setitem(Firebase.Variant,Firebase.Variant)
extern "C"  void VariantVariantMap_setitem_m1893973153 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, Variant_t4275788079 * ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::ContainsKey(Firebase.Variant)
extern "C"  bool VariantVariantMap_ContainsKey_m349634310 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::Add(Firebase.Variant,Firebase.Variant)
extern "C"  void VariantVariantMap_Add_m3700904635 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, Variant_t4275788079 * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantVariantMap::Remove(Firebase.Variant)
extern "C"  bool VariantVariantMap_Remove_m770432576 (VariantVariantMap_t4097173110 * __this, Variant_t4275788079 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.VariantVariantMap::create_iterator_begin()
extern "C"  IntPtr_t VariantVariantMap_create_iterator_begin_m1343112420 (VariantVariantMap_t4097173110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantVariantMap::get_next_key(System.IntPtr)
extern "C"  Variant_t4275788079 * VariantVariantMap_get_next_key_m3619905509 (VariantVariantMap_t4097173110 * __this, IntPtr_t ___swigiterator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantVariantMap::destroy_iterator(System.IntPtr)
extern "C"  void VariantVariantMap_destroy_iterator_m1097436859 (VariantVariantMap_t4097173110 * __this, IntPtr_t ___swigiterator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
