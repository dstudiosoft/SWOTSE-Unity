﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceProductionContentManager/<GetCityProduction>c__Iterator14
struct U3CGetCityProductionU3Ec__Iterator14_t2873994934;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::.ctor()
extern "C"  void U3CGetCityProductionU3Ec__Iterator14__ctor_m3004737589 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCityProductionU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1757160847 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCityProductionU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m1806579031 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::MoveNext()
extern "C"  bool U3CGetCityProductionU3Ec__Iterator14_MoveNext_m2215489735 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::Dispose()
extern "C"  void U3CGetCityProductionU3Ec__Iterator14_Dispose_m1139330888 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceProductionContentManager/<GetCityProduction>c__Iterator14::Reset()
extern "C"  void U3CGetCityProductionU3Ec__Iterator14_Reset_m3191948718 (U3CGetCityProductionU3Ec__Iterator14_t2873994934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
