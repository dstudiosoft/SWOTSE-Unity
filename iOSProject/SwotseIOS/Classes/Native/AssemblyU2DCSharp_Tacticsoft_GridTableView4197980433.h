﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1515633077;

#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.GridTableView
struct  GridTableView_t4197980433  : public TableView_t3179510217
{
public:
	// UnityEngine.UI.GridLayoutGroup Tacticsoft.GridTableView::m_gridLayoutGroup
	GridLayoutGroup_t1515633077 * ___m_gridLayoutGroup_19;

public:
	inline static int32_t get_offset_of_m_gridLayoutGroup_19() { return static_cast<int32_t>(offsetof(GridTableView_t4197980433, ___m_gridLayoutGroup_19)); }
	inline GridLayoutGroup_t1515633077 * get_m_gridLayoutGroup_19() const { return ___m_gridLayoutGroup_19; }
	inline GridLayoutGroup_t1515633077 ** get_address_of_m_gridLayoutGroup_19() { return &___m_gridLayoutGroup_19; }
	inline void set_m_gridLayoutGroup_19(GridLayoutGroup_t1515633077 * value)
	{
		___m_gridLayoutGroup_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_gridLayoutGroup_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
