﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppOptionsInternal
struct AppOptionsInternal_t2434721572;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_AppOptionsInternal2434721572.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.AppOptionsInternal::.ctor(System.IntPtr,System.Boolean)
extern "C"  void AppOptionsInternal__ctor_m2847065333 (AppOptionsInternal_t2434721572 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::.ctor()
extern "C"  void AppOptionsInternal__ctor_m333784130 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.AppOptionsInternal::getCPtr(Firebase.AppOptionsInternal)
extern "C"  HandleRef_t2419939847  AppOptionsInternal_getCPtr_m711475890 (Il2CppObject * __this /* static, unused */, AppOptionsInternal_t2434721572 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::Finalize()
extern "C"  void AppOptionsInternal_Finalize_m4000683038 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::Dispose()
extern "C"  void AppOptionsInternal_Dispose_m3762226895 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Firebase.AppOptionsInternal::get_DatabaseUrl()
extern "C"  Uri_t19570940 * AppOptionsInternal_get_DatabaseUrl_m3312969757 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_DatabaseUrl(System.Uri)
extern "C"  void AppOptionsInternal_set_DatabaseUrl_m195450616 (AppOptionsInternal_t2434721572 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::SetDatabaseUrlInternal(System.String)
extern "C"  void AppOptionsInternal_SetDatabaseUrlInternal_m655136461 (AppOptionsInternal_t2434721572 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::GetDatabaseUrlInternal()
extern "C"  String_t* AppOptionsInternal_GetDatabaseUrlInternal_m4230784328 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_AppId(System.String)
extern "C"  void AppOptionsInternal_set_AppId_m2875412083 (AppOptionsInternal_t2434721572 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::get_AppId()
extern "C"  String_t* AppOptionsInternal_get_AppId_m425947262 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_ApiKey(System.String)
extern "C"  void AppOptionsInternal_set_ApiKey_m3369248828 (AppOptionsInternal_t2434721572 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::get_ApiKey()
extern "C"  String_t* AppOptionsInternal_get_ApiKey_m3684871463 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_MessageSenderId(System.String)
extern "C"  void AppOptionsInternal_set_MessageSenderId_m2983284840 (AppOptionsInternal_t2434721572 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::get_MessageSenderId()
extern "C"  String_t* AppOptionsInternal_get_MessageSenderId_m3537383513 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_StorageBucket(System.String)
extern "C"  void AppOptionsInternal_set_StorageBucket_m1059518708 (AppOptionsInternal_t2434721572 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::get_StorageBucket()
extern "C"  String_t* AppOptionsInternal_get_StorageBucket_m44624719 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptionsInternal::set_ProjectId(System.String)
extern "C"  void AppOptionsInternal_set_ProjectId_m3609430699 (AppOptionsInternal_t2434721572 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptionsInternal::get_ProjectId()
extern "C"  String_t* AppOptionsInternal_get_ProjectId_m2005227640 (AppOptionsInternal_t2434721572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
