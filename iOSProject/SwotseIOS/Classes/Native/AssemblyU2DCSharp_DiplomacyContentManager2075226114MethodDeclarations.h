﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiplomacyContentManager
struct DiplomacyContentManager_t2075226114;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DiplomacyContentManager::.ctor()
extern "C"  void DiplomacyContentManager__ctor_m3057517329 (DiplomacyContentManager_t2075226114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyContentManager::OnEnable()
extern "C"  void DiplomacyContentManager_OnEnable_m1100339529 (DiplomacyContentManager_t2075226114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyContentManager::ToggleStateChnaged()
extern "C"  void DiplomacyContentManager_ToggleStateChnaged_m1919695252 (DiplomacyContentManager_t2075226114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiplomacyContentManager::AllowTroopsSet(System.Object,System.String)
extern "C"  void DiplomacyContentManager_AllowTroopsSet_m749823379 (DiplomacyContentManager_t2075226114 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
