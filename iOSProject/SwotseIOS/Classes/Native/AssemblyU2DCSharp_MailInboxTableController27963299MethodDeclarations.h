﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailInboxTableController
struct MailInboxTableController_t27963299;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailInboxTableController::.ctor()
extern "C"  void MailInboxTableController__ctor_m3888215838 (MailInboxTableController_t27963299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::Start()
extern "C"  void MailInboxTableController_Start_m1903132002 (MailInboxTableController_t27963299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::OnEnable()
extern "C"  void MailInboxTableController_OnEnable_m1709902306 (MailInboxTableController_t27963299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MailInboxTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t MailInboxTableController_GetNumberOfRowsForTableView_m1099569584 (MailInboxTableController_t27963299 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MailInboxTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float MailInboxTableController_GetHeightForRowInTableView_m3660193900 (MailInboxTableController_t27963299 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell MailInboxTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * MailInboxTableController_GetCellForRowInTableView_m2418949467 (MailInboxTableController_t27963299 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::SearchUserMessages()
extern "C"  void MailInboxTableController_SearchUserMessages_m3902123361 (MailInboxTableController_t27963299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::DeleteMessage(System.Int64)
extern "C"  void MailInboxTableController_DeleteMessage_m1848336232 (MailInboxTableController_t27963299 * __this, int64_t ___messageId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::MessageDeleted(System.Object,System.String)
extern "C"  void MailInboxTableController_MessageDeleted_m3826326312 (MailInboxTableController_t27963299 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::ReloadTable()
extern "C"  void MailInboxTableController_ReloadTable_m2232444597 (MailInboxTableController_t27963299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInboxTableController::GotAllReports(System.Object,System.String)
extern "C"  void MailInboxTableController_GotAllReports_m3601566886 (MailInboxTableController_t27963299 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
