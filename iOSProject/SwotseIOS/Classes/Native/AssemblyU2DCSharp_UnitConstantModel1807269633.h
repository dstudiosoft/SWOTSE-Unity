﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitConstantModel
struct  UnitConstantModel_t1807269633  : public Il2CppObject
{
public:
	// System.String UnitConstantModel::name
	String_t* ___name_0;
	// System.String UnitConstantModel::unit_name
	String_t* ___unit_name_1;
	// System.Int64 UnitConstantModel::food_price
	int64_t ___food_price_2;
	// System.Int64 UnitConstantModel::wood_price
	int64_t ___wood_price_3;
	// System.Int64 UnitConstantModel::stone_price
	int64_t ___stone_price_4;
	// System.Int64 UnitConstantModel::iron_price
	int64_t ___iron_price_5;
	// System.Int64 UnitConstantModel::cooper_price
	int64_t ___cooper_price_6;
	// System.Int64 UnitConstantModel::gold_price
	int64_t ___gold_price_7;
	// System.Int64 UnitConstantModel::silver_price
	int64_t ___silver_price_8;
	// System.Int64 UnitConstantModel::time_seconds
	int64_t ___time_seconds_9;
	// System.Int64 UnitConstantModel::population_price
	int64_t ___population_price_10;
	// System.Int64 UnitConstantModel::speed
	int64_t ___speed_11;
	// System.Int64 UnitConstantModel::range
	int64_t ___range_12;
	// System.Int64 UnitConstantModel::attack
	int64_t ___attack_13;
	// System.Int64 UnitConstantModel::defence
	int64_t ___defence_14;
	// System.Int64 UnitConstantModel::life
	int64_t ___life_15;
	// System.Int64 UnitConstantModel::capacity
	int64_t ___capacity_16;
	// System.String UnitConstantModel::description
	String_t* ___description_17;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_unit_name_1() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___unit_name_1)); }
	inline String_t* get_unit_name_1() const { return ___unit_name_1; }
	inline String_t** get_address_of_unit_name_1() { return &___unit_name_1; }
	inline void set_unit_name_1(String_t* value)
	{
		___unit_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___unit_name_1, value);
	}

	inline static int32_t get_offset_of_food_price_2() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___food_price_2)); }
	inline int64_t get_food_price_2() const { return ___food_price_2; }
	inline int64_t* get_address_of_food_price_2() { return &___food_price_2; }
	inline void set_food_price_2(int64_t value)
	{
		___food_price_2 = value;
	}

	inline static int32_t get_offset_of_wood_price_3() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___wood_price_3)); }
	inline int64_t get_wood_price_3() const { return ___wood_price_3; }
	inline int64_t* get_address_of_wood_price_3() { return &___wood_price_3; }
	inline void set_wood_price_3(int64_t value)
	{
		___wood_price_3 = value;
	}

	inline static int32_t get_offset_of_stone_price_4() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___stone_price_4)); }
	inline int64_t get_stone_price_4() const { return ___stone_price_4; }
	inline int64_t* get_address_of_stone_price_4() { return &___stone_price_4; }
	inline void set_stone_price_4(int64_t value)
	{
		___stone_price_4 = value;
	}

	inline static int32_t get_offset_of_iron_price_5() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___iron_price_5)); }
	inline int64_t get_iron_price_5() const { return ___iron_price_5; }
	inline int64_t* get_address_of_iron_price_5() { return &___iron_price_5; }
	inline void set_iron_price_5(int64_t value)
	{
		___iron_price_5 = value;
	}

	inline static int32_t get_offset_of_cooper_price_6() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___cooper_price_6)); }
	inline int64_t get_cooper_price_6() const { return ___cooper_price_6; }
	inline int64_t* get_address_of_cooper_price_6() { return &___cooper_price_6; }
	inline void set_cooper_price_6(int64_t value)
	{
		___cooper_price_6 = value;
	}

	inline static int32_t get_offset_of_gold_price_7() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___gold_price_7)); }
	inline int64_t get_gold_price_7() const { return ___gold_price_7; }
	inline int64_t* get_address_of_gold_price_7() { return &___gold_price_7; }
	inline void set_gold_price_7(int64_t value)
	{
		___gold_price_7 = value;
	}

	inline static int32_t get_offset_of_silver_price_8() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___silver_price_8)); }
	inline int64_t get_silver_price_8() const { return ___silver_price_8; }
	inline int64_t* get_address_of_silver_price_8() { return &___silver_price_8; }
	inline void set_silver_price_8(int64_t value)
	{
		___silver_price_8 = value;
	}

	inline static int32_t get_offset_of_time_seconds_9() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___time_seconds_9)); }
	inline int64_t get_time_seconds_9() const { return ___time_seconds_9; }
	inline int64_t* get_address_of_time_seconds_9() { return &___time_seconds_9; }
	inline void set_time_seconds_9(int64_t value)
	{
		___time_seconds_9 = value;
	}

	inline static int32_t get_offset_of_population_price_10() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___population_price_10)); }
	inline int64_t get_population_price_10() const { return ___population_price_10; }
	inline int64_t* get_address_of_population_price_10() { return &___population_price_10; }
	inline void set_population_price_10(int64_t value)
	{
		___population_price_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___speed_11)); }
	inline int64_t get_speed_11() const { return ___speed_11; }
	inline int64_t* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(int64_t value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_range_12() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___range_12)); }
	inline int64_t get_range_12() const { return ___range_12; }
	inline int64_t* get_address_of_range_12() { return &___range_12; }
	inline void set_range_12(int64_t value)
	{
		___range_12 = value;
	}

	inline static int32_t get_offset_of_attack_13() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___attack_13)); }
	inline int64_t get_attack_13() const { return ___attack_13; }
	inline int64_t* get_address_of_attack_13() { return &___attack_13; }
	inline void set_attack_13(int64_t value)
	{
		___attack_13 = value;
	}

	inline static int32_t get_offset_of_defence_14() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___defence_14)); }
	inline int64_t get_defence_14() const { return ___defence_14; }
	inline int64_t* get_address_of_defence_14() { return &___defence_14; }
	inline void set_defence_14(int64_t value)
	{
		___defence_14 = value;
	}

	inline static int32_t get_offset_of_life_15() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___life_15)); }
	inline int64_t get_life_15() const { return ___life_15; }
	inline int64_t* get_address_of_life_15() { return &___life_15; }
	inline void set_life_15(int64_t value)
	{
		___life_15 = value;
	}

	inline static int32_t get_offset_of_capacity_16() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___capacity_16)); }
	inline int64_t get_capacity_16() const { return ___capacity_16; }
	inline int64_t* get_address_of_capacity_16() { return &___capacity_16; }
	inline void set_capacity_16(int64_t value)
	{
		___capacity_16 = value;
	}

	inline static int32_t get_offset_of_description_17() { return static_cast<int32_t>(offsetof(UnitConstantModel_t1807269633, ___description_17)); }
	inline String_t* get_description_17() const { return ___description_17; }
	inline String_t** get_address_of_description_17() { return &___description_17; }
	inline void set_description_17(String_t* value)
	{
		___description_17 = value;
		Il2CppCodeGenWriteBarrier(&___description_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
