﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1
struct U3CIsCultureSupportedU3Ec__AnonStorey1_t611194050;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"

// System.Void SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::.ctor()
extern "C"  void U3CIsCultureSupportedU3Ec__AnonStorey1__ctor_m90243950 (U3CIsCultureSupportedU3Ec__AnonStorey1_t611194050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager/<IsCultureSupported>c__AnonStorey1::<>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool U3CIsCultureSupportedU3Ec__AnonStorey1_U3CU3Em__0_m3766000192 (U3CIsCultureSupportedU3Ec__AnonStorey1_t611194050 * __this, SmartCultureInfo_t2361725737 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
