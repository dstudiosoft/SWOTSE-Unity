﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<TutorialPageModel>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m470297497(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3769983157 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<TutorialPageModel>::Invoke(T,T)
#define Comparison_1_Invoke_m3088712651(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3769983157 *, TutorialPageModel_t2508244306 *, TutorialPageModel_t2508244306 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<TutorialPageModel>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2755721030(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3769983157 *, TutorialPageModel_t2508244306 *, TutorialPageModel_t2508244306 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<TutorialPageModel>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1755099673(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3769983157 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
