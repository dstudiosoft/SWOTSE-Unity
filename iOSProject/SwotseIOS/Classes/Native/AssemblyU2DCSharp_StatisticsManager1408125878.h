﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatisticsManager
struct  StatisticsManager_t1408125878  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject StatisticsManager::resourcesPanel
	GameObject_t1756533147 * ___resourcesPanel_2;
	// UnityEngine.GameObject StatisticsManager::knightsPanel
	GameObject_t1756533147 * ___knightsPanel_3;
	// UnityEngine.GameObject StatisticsManager::unitsPanel
	GameObject_t1756533147 * ___unitsPanel_4;
	// UnityEngine.UI.Text StatisticsManager::cityFood
	Text_t356221433 * ___cityFood_5;
	// UnityEngine.UI.Text StatisticsManager::cityWood
	Text_t356221433 * ___cityWood_6;
	// UnityEngine.UI.Text StatisticsManager::cityIron
	Text_t356221433 * ___cityIron_7;
	// UnityEngine.UI.Text StatisticsManager::cityStone
	Text_t356221433 * ___cityStone_8;
	// UnityEngine.UI.Text StatisticsManager::cityCopper
	Text_t356221433 * ___cityCopper_9;
	// UnityEngine.UI.Text StatisticsManager::citySilver
	Text_t356221433 * ___citySilver_10;
	// UnityEngine.UI.Text StatisticsManager::cityGold
	Text_t356221433 * ___cityGold_11;
	// UnityEngine.UI.Text StatisticsManager::cityWorker
	Text_t356221433 * ___cityWorker_12;
	// UnityEngine.UI.Text StatisticsManager::citySpy
	Text_t356221433 * ___citySpy_13;
	// UnityEngine.UI.Text StatisticsManager::citySwordsman
	Text_t356221433 * ___citySwordsman_14;
	// UnityEngine.UI.Text StatisticsManager::citySpearman
	Text_t356221433 * ___citySpearman_15;
	// UnityEngine.UI.Text StatisticsManager::cityPikeman
	Text_t356221433 * ___cityPikeman_16;
	// UnityEngine.UI.Text StatisticsManager::cityScoutRider
	Text_t356221433 * ___cityScoutRider_17;
	// UnityEngine.UI.Text StatisticsManager::cityLightCavalry
	Text_t356221433 * ___cityLightCavalry_18;
	// UnityEngine.UI.Text StatisticsManager::cityHeavyCavalry
	Text_t356221433 * ___cityHeavyCavalry_19;
	// UnityEngine.UI.Text StatisticsManager::cityArcher
	Text_t356221433 * ___cityArcher_20;
	// UnityEngine.UI.Text StatisticsManager::cityArcherRider
	Text_t356221433 * ___cityArcherRider_21;
	// UnityEngine.UI.Text StatisticsManager::cityHollyman
	Text_t356221433 * ___cityHollyman_22;
	// UnityEngine.UI.Text StatisticsManager::cityWagon
	Text_t356221433 * ___cityWagon_23;
	// UnityEngine.UI.Text StatisticsManager::cityTrebuchet
	Text_t356221433 * ___cityTrebuchet_24;
	// UnityEngine.UI.Text StatisticsManager::citySiegeTower
	Text_t356221433 * ___citySiegeTower_25;
	// UnityEngine.UI.Text StatisticsManager::cityBatteringRam
	Text_t356221433 * ___cityBatteringRam_26;
	// UnityEngine.UI.Text StatisticsManager::cityBallista
	Text_t356221433 * ___cityBallista_27;
	// UnityEngine.UI.Text StatisticsManager::cityBeaconTower
	Text_t356221433 * ___cityBeaconTower_28;
	// UnityEngine.UI.Text StatisticsManager::cityArcherTower
	Text_t356221433 * ___cityArcherTower_29;
	// UnityEngine.UI.Text StatisticsManager::cityHotOilHole
	Text_t356221433 * ___cityHotOilHole_30;
	// UnityEngine.UI.Text StatisticsManager::cityTrebuchetTower
	Text_t356221433 * ___cityTrebuchetTower_31;
	// UnityEngine.UI.Text StatisticsManager::cityBallistaTower
	Text_t356221433 * ___cityBallistaTower_32;

public:
	inline static int32_t get_offset_of_resourcesPanel_2() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___resourcesPanel_2)); }
	inline GameObject_t1756533147 * get_resourcesPanel_2() const { return ___resourcesPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_resourcesPanel_2() { return &___resourcesPanel_2; }
	inline void set_resourcesPanel_2(GameObject_t1756533147 * value)
	{
		___resourcesPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___resourcesPanel_2, value);
	}

	inline static int32_t get_offset_of_knightsPanel_3() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___knightsPanel_3)); }
	inline GameObject_t1756533147 * get_knightsPanel_3() const { return ___knightsPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_knightsPanel_3() { return &___knightsPanel_3; }
	inline void set_knightsPanel_3(GameObject_t1756533147 * value)
	{
		___knightsPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___knightsPanel_3, value);
	}

	inline static int32_t get_offset_of_unitsPanel_4() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___unitsPanel_4)); }
	inline GameObject_t1756533147 * get_unitsPanel_4() const { return ___unitsPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_unitsPanel_4() { return &___unitsPanel_4; }
	inline void set_unitsPanel_4(GameObject_t1756533147 * value)
	{
		___unitsPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___unitsPanel_4, value);
	}

	inline static int32_t get_offset_of_cityFood_5() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityFood_5)); }
	inline Text_t356221433 * get_cityFood_5() const { return ___cityFood_5; }
	inline Text_t356221433 ** get_address_of_cityFood_5() { return &___cityFood_5; }
	inline void set_cityFood_5(Text_t356221433 * value)
	{
		___cityFood_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityFood_5, value);
	}

	inline static int32_t get_offset_of_cityWood_6() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityWood_6)); }
	inline Text_t356221433 * get_cityWood_6() const { return ___cityWood_6; }
	inline Text_t356221433 ** get_address_of_cityWood_6() { return &___cityWood_6; }
	inline void set_cityWood_6(Text_t356221433 * value)
	{
		___cityWood_6 = value;
		Il2CppCodeGenWriteBarrier(&___cityWood_6, value);
	}

	inline static int32_t get_offset_of_cityIron_7() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityIron_7)); }
	inline Text_t356221433 * get_cityIron_7() const { return ___cityIron_7; }
	inline Text_t356221433 ** get_address_of_cityIron_7() { return &___cityIron_7; }
	inline void set_cityIron_7(Text_t356221433 * value)
	{
		___cityIron_7 = value;
		Il2CppCodeGenWriteBarrier(&___cityIron_7, value);
	}

	inline static int32_t get_offset_of_cityStone_8() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityStone_8)); }
	inline Text_t356221433 * get_cityStone_8() const { return ___cityStone_8; }
	inline Text_t356221433 ** get_address_of_cityStone_8() { return &___cityStone_8; }
	inline void set_cityStone_8(Text_t356221433 * value)
	{
		___cityStone_8 = value;
		Il2CppCodeGenWriteBarrier(&___cityStone_8, value);
	}

	inline static int32_t get_offset_of_cityCopper_9() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityCopper_9)); }
	inline Text_t356221433 * get_cityCopper_9() const { return ___cityCopper_9; }
	inline Text_t356221433 ** get_address_of_cityCopper_9() { return &___cityCopper_9; }
	inline void set_cityCopper_9(Text_t356221433 * value)
	{
		___cityCopper_9 = value;
		Il2CppCodeGenWriteBarrier(&___cityCopper_9, value);
	}

	inline static int32_t get_offset_of_citySilver_10() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___citySilver_10)); }
	inline Text_t356221433 * get_citySilver_10() const { return ___citySilver_10; }
	inline Text_t356221433 ** get_address_of_citySilver_10() { return &___citySilver_10; }
	inline void set_citySilver_10(Text_t356221433 * value)
	{
		___citySilver_10 = value;
		Il2CppCodeGenWriteBarrier(&___citySilver_10, value);
	}

	inline static int32_t get_offset_of_cityGold_11() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityGold_11)); }
	inline Text_t356221433 * get_cityGold_11() const { return ___cityGold_11; }
	inline Text_t356221433 ** get_address_of_cityGold_11() { return &___cityGold_11; }
	inline void set_cityGold_11(Text_t356221433 * value)
	{
		___cityGold_11 = value;
		Il2CppCodeGenWriteBarrier(&___cityGold_11, value);
	}

	inline static int32_t get_offset_of_cityWorker_12() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityWorker_12)); }
	inline Text_t356221433 * get_cityWorker_12() const { return ___cityWorker_12; }
	inline Text_t356221433 ** get_address_of_cityWorker_12() { return &___cityWorker_12; }
	inline void set_cityWorker_12(Text_t356221433 * value)
	{
		___cityWorker_12 = value;
		Il2CppCodeGenWriteBarrier(&___cityWorker_12, value);
	}

	inline static int32_t get_offset_of_citySpy_13() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___citySpy_13)); }
	inline Text_t356221433 * get_citySpy_13() const { return ___citySpy_13; }
	inline Text_t356221433 ** get_address_of_citySpy_13() { return &___citySpy_13; }
	inline void set_citySpy_13(Text_t356221433 * value)
	{
		___citySpy_13 = value;
		Il2CppCodeGenWriteBarrier(&___citySpy_13, value);
	}

	inline static int32_t get_offset_of_citySwordsman_14() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___citySwordsman_14)); }
	inline Text_t356221433 * get_citySwordsman_14() const { return ___citySwordsman_14; }
	inline Text_t356221433 ** get_address_of_citySwordsman_14() { return &___citySwordsman_14; }
	inline void set_citySwordsman_14(Text_t356221433 * value)
	{
		___citySwordsman_14 = value;
		Il2CppCodeGenWriteBarrier(&___citySwordsman_14, value);
	}

	inline static int32_t get_offset_of_citySpearman_15() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___citySpearman_15)); }
	inline Text_t356221433 * get_citySpearman_15() const { return ___citySpearman_15; }
	inline Text_t356221433 ** get_address_of_citySpearman_15() { return &___citySpearman_15; }
	inline void set_citySpearman_15(Text_t356221433 * value)
	{
		___citySpearman_15 = value;
		Il2CppCodeGenWriteBarrier(&___citySpearman_15, value);
	}

	inline static int32_t get_offset_of_cityPikeman_16() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityPikeman_16)); }
	inline Text_t356221433 * get_cityPikeman_16() const { return ___cityPikeman_16; }
	inline Text_t356221433 ** get_address_of_cityPikeman_16() { return &___cityPikeman_16; }
	inline void set_cityPikeman_16(Text_t356221433 * value)
	{
		___cityPikeman_16 = value;
		Il2CppCodeGenWriteBarrier(&___cityPikeman_16, value);
	}

	inline static int32_t get_offset_of_cityScoutRider_17() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityScoutRider_17)); }
	inline Text_t356221433 * get_cityScoutRider_17() const { return ___cityScoutRider_17; }
	inline Text_t356221433 ** get_address_of_cityScoutRider_17() { return &___cityScoutRider_17; }
	inline void set_cityScoutRider_17(Text_t356221433 * value)
	{
		___cityScoutRider_17 = value;
		Il2CppCodeGenWriteBarrier(&___cityScoutRider_17, value);
	}

	inline static int32_t get_offset_of_cityLightCavalry_18() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityLightCavalry_18)); }
	inline Text_t356221433 * get_cityLightCavalry_18() const { return ___cityLightCavalry_18; }
	inline Text_t356221433 ** get_address_of_cityLightCavalry_18() { return &___cityLightCavalry_18; }
	inline void set_cityLightCavalry_18(Text_t356221433 * value)
	{
		___cityLightCavalry_18 = value;
		Il2CppCodeGenWriteBarrier(&___cityLightCavalry_18, value);
	}

	inline static int32_t get_offset_of_cityHeavyCavalry_19() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityHeavyCavalry_19)); }
	inline Text_t356221433 * get_cityHeavyCavalry_19() const { return ___cityHeavyCavalry_19; }
	inline Text_t356221433 ** get_address_of_cityHeavyCavalry_19() { return &___cityHeavyCavalry_19; }
	inline void set_cityHeavyCavalry_19(Text_t356221433 * value)
	{
		___cityHeavyCavalry_19 = value;
		Il2CppCodeGenWriteBarrier(&___cityHeavyCavalry_19, value);
	}

	inline static int32_t get_offset_of_cityArcher_20() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityArcher_20)); }
	inline Text_t356221433 * get_cityArcher_20() const { return ___cityArcher_20; }
	inline Text_t356221433 ** get_address_of_cityArcher_20() { return &___cityArcher_20; }
	inline void set_cityArcher_20(Text_t356221433 * value)
	{
		___cityArcher_20 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcher_20, value);
	}

	inline static int32_t get_offset_of_cityArcherRider_21() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityArcherRider_21)); }
	inline Text_t356221433 * get_cityArcherRider_21() const { return ___cityArcherRider_21; }
	inline Text_t356221433 ** get_address_of_cityArcherRider_21() { return &___cityArcherRider_21; }
	inline void set_cityArcherRider_21(Text_t356221433 * value)
	{
		___cityArcherRider_21 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherRider_21, value);
	}

	inline static int32_t get_offset_of_cityHollyman_22() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityHollyman_22)); }
	inline Text_t356221433 * get_cityHollyman_22() const { return ___cityHollyman_22; }
	inline Text_t356221433 ** get_address_of_cityHollyman_22() { return &___cityHollyman_22; }
	inline void set_cityHollyman_22(Text_t356221433 * value)
	{
		___cityHollyman_22 = value;
		Il2CppCodeGenWriteBarrier(&___cityHollyman_22, value);
	}

	inline static int32_t get_offset_of_cityWagon_23() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityWagon_23)); }
	inline Text_t356221433 * get_cityWagon_23() const { return ___cityWagon_23; }
	inline Text_t356221433 ** get_address_of_cityWagon_23() { return &___cityWagon_23; }
	inline void set_cityWagon_23(Text_t356221433 * value)
	{
		___cityWagon_23 = value;
		Il2CppCodeGenWriteBarrier(&___cityWagon_23, value);
	}

	inline static int32_t get_offset_of_cityTrebuchet_24() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityTrebuchet_24)); }
	inline Text_t356221433 * get_cityTrebuchet_24() const { return ___cityTrebuchet_24; }
	inline Text_t356221433 ** get_address_of_cityTrebuchet_24() { return &___cityTrebuchet_24; }
	inline void set_cityTrebuchet_24(Text_t356221433 * value)
	{
		___cityTrebuchet_24 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchet_24, value);
	}

	inline static int32_t get_offset_of_citySiegeTower_25() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___citySiegeTower_25)); }
	inline Text_t356221433 * get_citySiegeTower_25() const { return ___citySiegeTower_25; }
	inline Text_t356221433 ** get_address_of_citySiegeTower_25() { return &___citySiegeTower_25; }
	inline void set_citySiegeTower_25(Text_t356221433 * value)
	{
		___citySiegeTower_25 = value;
		Il2CppCodeGenWriteBarrier(&___citySiegeTower_25, value);
	}

	inline static int32_t get_offset_of_cityBatteringRam_26() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityBatteringRam_26)); }
	inline Text_t356221433 * get_cityBatteringRam_26() const { return ___cityBatteringRam_26; }
	inline Text_t356221433 ** get_address_of_cityBatteringRam_26() { return &___cityBatteringRam_26; }
	inline void set_cityBatteringRam_26(Text_t356221433 * value)
	{
		___cityBatteringRam_26 = value;
		Il2CppCodeGenWriteBarrier(&___cityBatteringRam_26, value);
	}

	inline static int32_t get_offset_of_cityBallista_27() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityBallista_27)); }
	inline Text_t356221433 * get_cityBallista_27() const { return ___cityBallista_27; }
	inline Text_t356221433 ** get_address_of_cityBallista_27() { return &___cityBallista_27; }
	inline void set_cityBallista_27(Text_t356221433 * value)
	{
		___cityBallista_27 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallista_27, value);
	}

	inline static int32_t get_offset_of_cityBeaconTower_28() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityBeaconTower_28)); }
	inline Text_t356221433 * get_cityBeaconTower_28() const { return ___cityBeaconTower_28; }
	inline Text_t356221433 ** get_address_of_cityBeaconTower_28() { return &___cityBeaconTower_28; }
	inline void set_cityBeaconTower_28(Text_t356221433 * value)
	{
		___cityBeaconTower_28 = value;
		Il2CppCodeGenWriteBarrier(&___cityBeaconTower_28, value);
	}

	inline static int32_t get_offset_of_cityArcherTower_29() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityArcherTower_29)); }
	inline Text_t356221433 * get_cityArcherTower_29() const { return ___cityArcherTower_29; }
	inline Text_t356221433 ** get_address_of_cityArcherTower_29() { return &___cityArcherTower_29; }
	inline void set_cityArcherTower_29(Text_t356221433 * value)
	{
		___cityArcherTower_29 = value;
		Il2CppCodeGenWriteBarrier(&___cityArcherTower_29, value);
	}

	inline static int32_t get_offset_of_cityHotOilHole_30() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityHotOilHole_30)); }
	inline Text_t356221433 * get_cityHotOilHole_30() const { return ___cityHotOilHole_30; }
	inline Text_t356221433 ** get_address_of_cityHotOilHole_30() { return &___cityHotOilHole_30; }
	inline void set_cityHotOilHole_30(Text_t356221433 * value)
	{
		___cityHotOilHole_30 = value;
		Il2CppCodeGenWriteBarrier(&___cityHotOilHole_30, value);
	}

	inline static int32_t get_offset_of_cityTrebuchetTower_31() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityTrebuchetTower_31)); }
	inline Text_t356221433 * get_cityTrebuchetTower_31() const { return ___cityTrebuchetTower_31; }
	inline Text_t356221433 ** get_address_of_cityTrebuchetTower_31() { return &___cityTrebuchetTower_31; }
	inline void set_cityTrebuchetTower_31(Text_t356221433 * value)
	{
		___cityTrebuchetTower_31 = value;
		Il2CppCodeGenWriteBarrier(&___cityTrebuchetTower_31, value);
	}

	inline static int32_t get_offset_of_cityBallistaTower_32() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878, ___cityBallistaTower_32)); }
	inline Text_t356221433 * get_cityBallistaTower_32() const { return ___cityBallistaTower_32; }
	inline Text_t356221433 ** get_address_of_cityBallistaTower_32() { return &___cityBallistaTower_32; }
	inline void set_cityBallistaTower_32(Text_t356221433 * value)
	{
		___cityBallistaTower_32 = value;
		Il2CppCodeGenWriteBarrier(&___cityBallistaTower_32, value);
	}
};

struct StatisticsManager_t1408125878_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> StatisticsManager::<>f__switch$map14
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map14_33;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_33() { return static_cast<int32_t>(offsetof(StatisticsManager_t1408125878_StaticFields, ___U3CU3Ef__switchU24map14_33)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map14_33() const { return ___U3CU3Ef__switchU24map14_33; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map14_33() { return &___U3CU3Ef__switchU24map14_33; }
	inline void set_U3CU3Ef__switchU24map14_33(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map14_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map14_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
