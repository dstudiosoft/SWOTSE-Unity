﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9
struct U3CReloadLevelU3Ec__Iterator9_t3686531518;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::.ctor()
extern "C"  void U3CReloadLevelU3Ec__Iterator9__ctor_m727311629 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CReloadLevelU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250926043 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReloadLevelU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m4166554851 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::MoveNext()
extern "C"  bool U3CReloadLevelU3Ec__Iterator9_MoveNext_m593351867 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::Dispose()
extern "C"  void U3CReloadLevelU3Ec__Iterator9_Dispose_m3477399818 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator9::Reset()
extern "C"  void U3CReloadLevelU3Ec__Iterator9_Reset_m1803176964 (U3CReloadLevelU3Ec__Iterator9_t3686531518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
