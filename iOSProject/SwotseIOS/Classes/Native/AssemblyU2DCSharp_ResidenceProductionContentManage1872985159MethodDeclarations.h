﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResidenceProductionContentManager
struct ResidenceProductionContentManager_t1872985159;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ResidenceProductionContentManager::.ctor()
extern "C"  void ResidenceProductionContentManager__ctor_m1351504194 (ResidenceProductionContentManager_t1872985159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResidenceProductionContentManager::OnEnable()
extern "C"  void ResidenceProductionContentManager_OnEnable_m1588322574 (ResidenceProductionContentManager_t1872985159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResidenceProductionContentManager::GetCityProduction()
extern "C"  Il2CppObject * ResidenceProductionContentManager_GetCityProduction_m1152867326 (ResidenceProductionContentManager_t1872985159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
